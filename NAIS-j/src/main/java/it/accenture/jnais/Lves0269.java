package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.MathFunctions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Ivvc0211DatiInput;
import it.accenture.jnais.copy.Ivvc0211RestoDati;
import it.accenture.jnais.copy.Ivvc0212;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WkFrazMm;
import it.accenture.jnais.ws.enums.WkQuestsan;
import it.accenture.jnais.ws.enums.WkTpRatPerf;
import it.accenture.jnais.ws.Idsv0301Lves0269;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Lccc0490;
import it.accenture.jnais.ws.Lccc1901;
import it.accenture.jnais.ws.Lves0269Data;
import it.accenture.jnais.ws.occurs.Lccc0490TabGrz;
import it.accenture.jnais.ws.occurs.WpagDatiTranche;
import it.accenture.jnais.ws.occurs.WpagTabAsset;
import it.accenture.jnais.ws.ptr.WallTabella;
import it.accenture.jnais.ws.redefines.Ivvc0211TabInfo1;
import it.accenture.jnais.ws.redefines.WcntTabVar;
import it.accenture.jnais.ws.redefines.WpagAaDiffPror;
import it.accenture.jnais.ws.redefines.WpagAliqAcq;
import it.accenture.jnais.ws.redefines.WpagAliqCommisInt;
import it.accenture.jnais.ws.redefines.WpagAliqIncas;
import it.accenture.jnais.ws.redefines.WpagAliqRenAss;
import it.accenture.jnais.ws.redefines.WpagAliqRicor;
import it.accenture.jnais.ws.redefines.WpagAlqScon;
import it.accenture.jnais.ws.redefines.WpagContAcq1oAnno;
import it.accenture.jnais.ws.redefines.WpagContAcq2oAnno;
import it.accenture.jnais.ws.redefines.WpagContAltriSopr;
import it.accenture.jnais.ws.redefines.WpagContDiritti;
import it.accenture.jnais.ws.redefines.WpagContImpAcqExp;
import it.accenture.jnais.ws.redefines.WpagContImpCommisInt;
import it.accenture.jnais.ws.redefines.WpagContImpRenAss;
import it.accenture.jnais.ws.redefines.WpagContIncas;
import it.accenture.jnais.ws.redefines.WpagContIntFraz;
import it.accenture.jnais.ws.redefines.WpagContIntRetrodt;
import it.accenture.jnais.ws.redefines.WpagContPremioNetto;
import it.accenture.jnais.ws.redefines.WpagContPremioPuroIas;
import it.accenture.jnais.ws.redefines.WpagContPremioRisc;
import it.accenture.jnais.ws.redefines.WpagContPremioTot;
import it.accenture.jnais.ws.redefines.WpagContRicor;
import it.accenture.jnais.ws.redefines.WpagContSoprProfes;
import it.accenture.jnais.ws.redefines.WpagContSoprSanit;
import it.accenture.jnais.ws.redefines.WpagContSoprSport;
import it.accenture.jnais.ws.redefines.WpagContSoprTecn;
import it.accenture.jnais.ws.redefines.WpagContSpeseMediche;
import it.accenture.jnais.ws.redefines.WpagContTasse;
import it.accenture.jnais.ws.redefines.WpagCptInOpzRivto;
import it.accenture.jnais.ws.redefines.WpagCptRshMor;
import it.accenture.jnais.ws.redefines.WpagDtEndCopTit;
import it.accenture.jnais.ws.redefines.WpagDtProsBnsFed;
import it.accenture.jnais.ws.redefines.WpagDtProsBnsRic;
import it.accenture.jnais.ws.redefines.WpagDtProsCedola;
import it.accenture.jnais.ws.redefines.WpagDtValQuote;
import it.accenture.jnais.ws.redefines.WpagDurAbbAnni;
import it.accenture.jnais.ws.redefines.WpagImpAbbAnnoUlt;
import it.accenture.jnais.ws.redefines.WpagImpAbbTotIni;
import it.accenture.jnais.ws.redefines.WpagImpAcq;
import it.accenture.jnais.ws.redefines.WpagImpAderTdr;
import it.accenture.jnais.ws.redefines.WpagImpAderTga;
import it.accenture.jnais.ws.redefines.WpagImpAderTit;
import it.accenture.jnais.ws.redefines.WpagImpAltSopr;
import it.accenture.jnais.ws.redefines.WpagImpAzTdr;
import it.accenture.jnais.ws.redefines.WpagImpAzTga;
import it.accenture.jnais.ws.redefines.WpagImpAzTit;
import it.accenture.jnais.ws.redefines.WpagImpBnsAntic;
import it.accenture.jnais.ws.redefines.WpagImpCarAcqTdr;
import it.accenture.jnais.ws.redefines.WpagImpCarAcqTga;
import it.accenture.jnais.ws.redefines.WpagImpCarAcqTit;
import it.accenture.jnais.ws.redefines.WpagImpCarGestTdr;
import it.accenture.jnais.ws.redefines.WpagImpCarGestTga;
import it.accenture.jnais.ws.redefines.WpagImpCarGestTit;
import it.accenture.jnais.ws.redefines.WpagImpCarIncTdr;
import it.accenture.jnais.ws.redefines.WpagImpCarIncTga;
import it.accenture.jnais.ws.redefines.WpagImpCarIncTit;
import it.accenture.jnais.ws.redefines.WpagImpCommisInt;
import it.accenture.jnais.ws.redefines.WpagImpIncas;
import it.accenture.jnais.ws.redefines.WpagImpIntRiatt;
import it.accenture.jnais.ws.redefines.WpagImpMovi;
import it.accenture.jnais.ws.redefines.WpagImpMoviNeg;
import it.accenture.jnais.ws.redefines.WpagImpPrePattuito;
import it.accenture.jnais.ws.redefines.WpagImpPreRival;
import it.accenture.jnais.ws.redefines.WpagImpPrestUlt;
import it.accenture.jnais.ws.redefines.WpagImpPrstzAggIni;
import it.accenture.jnais.ws.redefines.WpagImpRenAss;
import it.accenture.jnais.ws.redefines.WpagImpRicor;
import it.accenture.jnais.ws.redefines.WpagImpScon;
import it.accenture.jnais.ws.redefines.WpagImpSoprProf;
import it.accenture.jnais.ws.redefines.WpagImpSoprSan;
import it.accenture.jnais.ws.redefines.WpagImpSoprSpo;
import it.accenture.jnais.ws.redefines.WpagImpSoprTec;
import it.accenture.jnais.ws.redefines.WpagImpSovrap;
import it.accenture.jnais.ws.redefines.WpagImpTfrTdr;
import it.accenture.jnais.ws.redefines.WpagImpTfrTga;
import it.accenture.jnais.ws.redefines.WpagImpTfrTit;
import it.accenture.jnais.ws.redefines.WpagImpVoloTdr;
import it.accenture.jnais.ws.redefines.WpagImpVoloTga;
import it.accenture.jnais.ws.redefines.WpagImpVoloTit;
import it.accenture.jnais.ws.redefines.WpagManfeeAntic;
import it.accenture.jnais.ws.redefines.WpagMmDiffPror;
import it.accenture.jnais.ws.redefines.WpagPercFondo;
import it.accenture.jnais.ws.redefines.WpagPercSovrap;
import it.accenture.jnais.ws.redefines.WpagPreCasoMor;
import it.accenture.jnais.ws.redefines.WpagPreIniNet;
import it.accenture.jnais.ws.redefines.WpagPreInvrioIni;
import it.accenture.jnais.ws.redefines.WpagPreInvrioUlt;
import it.accenture.jnais.ws.redefines.WpagPreLrd;
import it.accenture.jnais.ws.redefines.WpagPrePpIni;
import it.accenture.jnais.ws.redefines.WpagPrePpUlt;
import it.accenture.jnais.ws.redefines.WpagPreTariIni;
import it.accenture.jnais.ws.redefines.WpagPreTariUlt;
import it.accenture.jnais.ws.redefines.WpagPrstzIni;
import it.accenture.jnais.ws.redefines.WpagPrstzUlt;
import it.accenture.jnais.ws.redefines.WpagPrvAcq1oAnno;
import it.accenture.jnais.ws.redefines.WpagPrvAcq2oAnno;
import it.accenture.jnais.ws.redefines.WpagPrvCommisInt;
import it.accenture.jnais.ws.redefines.WpagPrvIncas;
import it.accenture.jnais.ws.redefines.WpagPrvRenAss;
import it.accenture.jnais.ws.redefines.WpagPrvRicor;
import it.accenture.jnais.ws.redefines.WpagRataAcq1oAnno;
import it.accenture.jnais.ws.redefines.WpagRataAcq2oAnno;
import it.accenture.jnais.ws.redefines.WpagRataAltriSopr;
import it.accenture.jnais.ws.redefines.WpagRataDiritti;
import it.accenture.jnais.ws.redefines.WpagRataImpAcqExp;
import it.accenture.jnais.ws.redefines.WpagRataImpCommisInt;
import it.accenture.jnais.ws.redefines.WpagRataImpRenAss;
import it.accenture.jnais.ws.redefines.WpagRataIncas;
import it.accenture.jnais.ws.redefines.WpagRataIntFraz;
import it.accenture.jnais.ws.redefines.WpagRataIntRetrodt;
import it.accenture.jnais.ws.redefines.WpagRataPremioNetto;
import it.accenture.jnais.ws.redefines.WpagRataPremioPuroIas;
import it.accenture.jnais.ws.redefines.WpagRataPremioRisc;
import it.accenture.jnais.ws.redefines.WpagRataPremioTot;
import it.accenture.jnais.ws.redefines.WpagRataRicor;
import it.accenture.jnais.ws.redefines.WpagRataSoprProfes;
import it.accenture.jnais.ws.redefines.WpagRataSoprSanit;
import it.accenture.jnais.ws.redefines.WpagRataSoprSport;
import it.accenture.jnais.ws.redefines.WpagRataSoprTecn;
import it.accenture.jnais.ws.redefines.WpagRataSpeseMediche;
import it.accenture.jnais.ws.redefines.WpagRataTasse;
import it.accenture.jnais.ws.redefines.WpagRatLrd;
import it.accenture.jnais.ws.redefines.WpagSovram;
import it.accenture.jnais.ws.redefines.WpagTgaAliqAcq;
import it.accenture.jnais.ws.redefines.WpagTgaAliqCommisInt;
import it.accenture.jnais.ws.redefines.WpagTgaAliqIncas;
import it.accenture.jnais.ws.redefines.WpagTgaAliqRenAss;
import it.accenture.jnais.ws.redefines.WpagTgaAliqRicor;
import it.accenture.jnais.ws.redefines.WpagTgaImpAcq;
import it.accenture.jnais.ws.redefines.WpagTgaImpAcqExp;
import it.accenture.jnais.ws.redefines.WpagTgaImpbCommisInt;
import it.accenture.jnais.ws.redefines.WpagTgaImpbRenAss;
import it.accenture.jnais.ws.redefines.WpagTgaImpCommisInt;
import it.accenture.jnais.ws.redefines.WpagTgaImpIncas;
import it.accenture.jnais.ws.redefines.WpagTgaImpRenAss;
import it.accenture.jnais.ws.redefines.WpagTgaImpRicor;
import it.accenture.jnais.ws.redefines.WpagTsRivalFis;
import it.accenture.jnais.ws.redefines.WpagValDt;
import it.accenture.jnais.ws.redefines.WpagValImp;
import it.accenture.jnais.ws.redefines.WpagValNum;
import it.accenture.jnais.ws.redefines.WpagValPc;
import it.accenture.jnais.ws.redefines.WpagValTs;
import it.accenture.jnais.ws.redefines.WtgaPreIniNet;
import it.accenture.jnais.ws.redefines.WtgaPrstzIni;
import it.accenture.jnais.ws.WadeAreaAdesioneLccs0005;
import it.accenture.jnais.ws.WallAreaAsset;
import it.accenture.jnais.ws.WbepAreaBeneficiari;
import it.accenture.jnais.ws.WcltAreaClausole;
import it.accenture.jnais.ws.WcomAreaPagina;
import it.accenture.jnais.ws.WdadAreaDefAdes;
import it.accenture.jnais.ws.WdcoAreaDtCollettiva;
import it.accenture.jnais.ws.WdeqAreaDettQuestLves0269;
import it.accenture.jnais.ws.WdfaAreaDtFiscAdes;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.WkVariabiliLves0269;
import it.accenture.jnais.ws.Wl23AreaVincPeg;
import it.accenture.jnais.ws.WocoAreaOggCollg;
import it.accenture.jnais.ws.Wp67AreaEstPoliCpiPr;
import it.accenture.jnais.ws.WpagAreaIas;
import it.accenture.jnais.ws.WpagAreaPagina;
import it.accenture.jnais.ws.WpmoAreaParamMovi;
import it.accenture.jnais.ws.WpogAreaParamOggLves0245;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WqueAreaQuest;
import it.accenture.jnais.ws.WranAreaRappAnag;
import it.accenture.jnais.ws.WrreAreaRappRete;
import it.accenture.jnais.ws.WsdiAreaStraInv;
import it.accenture.jnais.ws.WspgAreaSoprGar;
import it.accenture.jnais.ws.WtgaAreaTranche;
import javax.inject.Inject;

/**Original name: LVES0269<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA  VER 1.0                          **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       Marzo 2008.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *     PROGRAMMA ..... LVES0269
 *     TIPOLOGIA...... PREPARA MAPPA
 *     PROCESSO....... XXX
 *     FUNZIONE....... XXX
 *     DESCRIZIONE.... CREAZIONE PROPOSTA INDIVIDUALE
 *     PAGINA WEB..... CALCOLI
 * **------------------------------------------------------------***</pre>*/
public class Lves0269 extends Program {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    //Original name: WORKING-STORAGE
    private Lves0269Data ws = new Lves0269Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WDCO-AREA-DT-COLLETTIVA
    private WdcoAreaDtCollettiva wdcoAreaDtCollettiva;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLccs0005 wadeAreaAdesione;
    //Original name: WDAD-AREA-DEF-ADES
    private WdadAreaDefAdes wdadAreaDefAdes;
    //Original name: WRAN-AREA-RAPP-ANAG
    private WranAreaRappAnag wranAreaRappAnag;
    //Original name: WDFA-AREA-DT-FISC-ADES
    private WdfaAreaDtFiscAdes wdfaAreaDtFiscAdes;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTranche wtgaAreaTranche;
    //Original name: WSPG-AREA-SOPR-GAR
    private WspgAreaSoprGar wspgAreaSoprGar;
    //Original name: WSDI-AREA-STRA-INV
    private WsdiAreaStraInv wsdiAreaStraInv;
    //Original name: WALL-AREA-ASSET
    private WallAreaAsset wallAreaAsset;
    //Original name: WPMO-AREA-PARAM-MOV
    private WpmoAreaParamMovi wpmoAreaParamMov;
    //Original name: WPOG-AREA-PARAM-OGG
    private WpogAreaParamOggLves0245 wpogAreaParamOgg;
    //Original name: WBEP-AREA-BENEFICIARI
    private WbepAreaBeneficiari wbepAreaBeneficiari;
    //Original name: WQUE-AREA-QUEST
    private WqueAreaQuest wqueAreaQuest;
    //Original name: WDEQ-AREA-DETT-QUEST
    private WdeqAreaDettQuestLves0269 wdeqAreaDettQuest;
    //Original name: WCLT-AREA-CLAUSOLE
    private WcltAreaClausole wcltAreaClausole;
    //Original name: WOCO-AREA-OGG-COLLG
    private WocoAreaOggCollg wocoAreaOggCollg;
    //Original name: WRRE-AREA-RAP-RETE
    private WrreAreaRappRete wrreAreaRapRete;
    //Original name: WL23-AREA-VINC-PEG
    private Wl23AreaVincPeg wl23AreaVincPeg;
    //Original name: WP67-AREA-EST-POLI-CPI-PR
    private Wp67AreaEstPoliCpiPr wp67AreaEstPoliCpiPr;
    //Original name: WPAG-AREA-PAGINA
    private WpagAreaPagina wpagAreaPagina;
    //Original name: WPAG-AREA-IAS
    private WpagAreaIas wpagAreaIas;

    //==== METHODS ====
    /**Original name: PROGRAM_LVES0269_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WdcoAreaDtCollettiva wdcoAreaDtCollettiva, WadeAreaAdesioneLccs0005 wadeAreaAdesione, WdadAreaDefAdes wdadAreaDefAdes, WranAreaRappAnag wranAreaRappAnag, WdfaAreaDtFiscAdes wdfaAreaDtFiscAdes, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WtgaAreaTranche wtgaAreaTranche, WspgAreaSoprGar wspgAreaSoprGar, WsdiAreaStraInv wsdiAreaStraInv, WallAreaAsset wallAreaAsset, WpmoAreaParamMovi wpmoAreaParamMov, WpogAreaParamOggLves0245 wpogAreaParamOgg, WbepAreaBeneficiari wbepAreaBeneficiari, WqueAreaQuest wqueAreaQuest, WdeqAreaDettQuestLves0269 wdeqAreaDettQuest, WcltAreaClausole wcltAreaClausole, WocoAreaOggCollg wocoAreaOggCollg, WrreAreaRappRete wrreAreaRapRete, Wl23AreaVincPeg wl23AreaVincPeg, Wp67AreaEstPoliCpiPr wp67AreaEstPoliCpiPr, WpagAreaPagina wpagAreaPagina, WpagAreaIas wpagAreaIas) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wdcoAreaDtCollettiva = wdcoAreaDtCollettiva;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wdadAreaDefAdes = wdadAreaDefAdes;
        this.wranAreaRappAnag = wranAreaRappAnag;
        this.wdfaAreaDtFiscAdes = wdfaAreaDtFiscAdes;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaTranche = wtgaAreaTranche;
        this.wspgAreaSoprGar = wspgAreaSoprGar;
        this.wsdiAreaStraInv = wsdiAreaStraInv;
        this.wallAreaAsset = wallAreaAsset;
        this.wpmoAreaParamMov = wpmoAreaParamMov;
        this.wpogAreaParamOgg = wpogAreaParamOgg;
        this.wbepAreaBeneficiari = wbepAreaBeneficiari;
        this.wqueAreaQuest = wqueAreaQuest;
        this.wdeqAreaDettQuest = wdeqAreaDettQuest;
        this.wcltAreaClausole = wcltAreaClausole;
        this.wocoAreaOggCollg = wocoAreaOggCollg;
        this.wrreAreaRapRete = wrreAreaRapRete;
        this.wl23AreaVincPeg = wl23AreaVincPeg;
        this.wp67AreaEstPoliCpiPr = wp67AreaEstPoliCpiPr;
        this.wpagAreaPagina = wpagAreaPagina;
        this.wpagAreaIas = wpagAreaIas;
        // COB_CODE: PERFORM S00000-OPERAZIONI-INIZIALI
        //              THRU EX-S00000.
        s00000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S10000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S10000-ELABORAZIONE
            //              THRU EX-S10000
            s10000Elaborazione();
        }
        // COB_CODE: PERFORM S90000-OPERAZIONI-FINALI
        //              THRU EX-S90000.
        s90000OperazioniFinali();
        return 0;
    }

    public static Lves0269 getInstance() {
        return ((Lves0269)Programs.getInstance(Lves0269.class));
    }

    /**Original name: S00000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s00000OperazioniIniziali() {
        // COB_CODE: SET WK-GRZ-NON-TROVATA          TO TRUE.
        ws.getWkRicGrz().setNonTrovata();
        // COB_CODE: SET WK-TGA-NON-TROVATA          TO TRUE.
        ws.getWkRicTga().setNonTrovata();
        // COB_CODE: INITIALIZE                        WK-VARIABILI
        //                                             WPAG-AREA-IAS
        //                                             WCNT-AREA-DATI-CONTEST.
        initWkVariabili();
        initWpagAreaIas();
        initWcntAreaDatiContest();
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA   TO WS-COMPAGNIA.
        ws.getWsCompagnia().setWsCompagniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA(1:8) TO WK-DATA-COMPETENZA.
        ws.getWkVariabili().setWkDataCompetenzaFormatted(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenzaFormatted().substring((1) - 1, 8));
        //--  INIZIALIZZAZIONE DEI DATI DELL'AREA DI PAGINA
        // COB_CODE: PERFORM S00010-INITIALIZE-PAGINA
        //              THRU EX-S00010.
        s00010InitializePagina();
        // COB_CODE: PERFORM S10230-LETTURA-PCO    THRU EX-S10230.
        s10230LetturaPco();
        // COB_CODE: MOVE PCO-TP-RAT-PERF TO WK-TP-RAT-PERF.
        ws.getWkVariabili().getWkTpRatPerf().setWkTpRatPerf(ws.getParamComp().getPcoTpRatPerf());
    }

    /**Original name: S00010-INITIALIZE-PAGINA<br>
	 * <pre>----------------------------------------------------------------*
	 * --> INIZIALIZZAZIONE DEI DATI DELL'AREA DI PAGINA
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZAZIONE TABELLA ASSET</pre>*/
    private void s00010InitializePagina() {
        // COB_CODE: MOVE ZEROES                TO WPAG-ELE-ASSET-MAX
        wpagAreaPagina.getDatiOuput().setWpagEleAssetMax(((short)0));
        // COB_CODE: PERFORM VARYING IX-TAB-ASS  FROM 1 BY 1
        //                     UNTIL IX-TAB-ASS > WK-ASSET-ELE-MAX
        //               TO WPAG-PERC-FONDO-NULL(IX-TAB-ASS)
        //           END-PERFORM.
        ws.getIxIndici().setTabAss(((short)1));
        while (!(ws.getIxIndici().getTabAss() > ws.getWkAssetEleMax())) {
            // COB_CODE: MOVE SPACES
            //             TO WPAG-COD-FONDO(IX-TAB-ASS)
            //                WPAG-TP-FND(IX-TAB-ASS)
            wpagAreaPagina.getDatiOuput().getWpagTabAsset(ws.getIxIndici().getTabAss()).setWpagCodFondo("");
            wpagAreaPagina.getDatiOuput().getWpagTabAsset(ws.getIxIndici().getTabAss()).setWpagTpFnd(Types.SPACE_CHAR);
            // COB_CODE: MOVE HIGH-VALUE
            //             TO WPAG-PERC-FONDO-NULL(IX-TAB-ASS)
            wpagAreaPagina.getDatiOuput().getWpagTabAsset(ws.getIxIndici().getTabAss()).getWpagPercFondo().setWpagPercFondoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPercFondo.Len.WPAG_PERC_FONDO_NULL));
            ws.getIxIndici().setTabAss(Trunc.toShort(ws.getIxIndici().getTabAss() + 1, 4));
        }
        //--> INIZIALIZZAZIONE DATI GARANZIA
        // COB_CODE: MOVE ZEROES                TO WPAG-ELE-CALCOLI-MAX
        wpagAreaPagina.getDatiOuput().setWpagEleCalcoliMax(((short)0));
        // COB_CODE: PERFORM VARYING IX-TAB-CALC FROM 1 BY 1
        //                     UNTIL IX-TAB-CALC > WK-ELE-MAX-TRCH-DI-GAR
        //                  WPAG-DT-PROS-CEDOLA-NULL(IX-TAB-CALC)
        //           END-PERFORM.
        ws.getIxIndici().setTabCalc(((short)1));
        while (!(ws.getIxIndici().getTabCalc() > ws.getWkEleMaxTrchDiGar())) {
            // COB_CODE: MOVE SPACES       TO WPAG-COD-GARANZIA(IX-TAB-CALC)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).setWpagCodGaranzia("");
            // COB_CODE: MOVE HIGH-VALUE   TO WPAG-IMP-MOVI-NULL(IX-TAB-CALC)
            //                                WPAG-IMP-MOVI-NEG-NULL(IX-TAB-CALC)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagImpMovi().setWpagImpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpMovi.Len.WPAG_IMP_MOVI_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagImpMoviNeg().setWpagImpMoviNegNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpMoviNeg.Len.WPAG_IMP_MOVI_NEG_NULL));
            // COB_CODE: MOVE HIGH-VALUE   TO WPAG-AA-DIFF-PROR-NULL(IX-TAB-CALC)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagAaDiffPror().setWpagAaDiffProrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagAaDiffPror.Len.WPAG_AA_DIFF_PROR_NULL));
            // COB_CODE: MOVE HIGH-VALUE   TO WPAG-MM-DIFF-PROR-NULL(IX-TAB-CALC)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagMmDiffPror().setWpagMmDiffProrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagMmDiffPror.Len.WPAG_MM_DIFF_PROR_NULL));
            //--> INIZIALIZZAZIONE ELE MAX
            // COB_CODE: MOVE ZEROES       TO WPAG-ELE-TRANCHE-MAX (IX-TAB-CALC)
            //                                WPAG-ELE-SOPR-GAR-MAX(IX-TAB-CALC)
            //                                WPAG-ELE-PAR-CALC-MAX(IX-TAB-CALC)
            //                                WPAG-ELE-TIT-CONT-MAX(IX-TAB-CALC)
            //                                WPAG-ELE-TIT-RATA-MAX(IX-TAB-CALC)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).setWpagEleTrancheMax(((short)0));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).setWpagEleSoprGarMax(((short)0));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).setWpagEleParCalcMax(((short)0));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).setWpagEleTitContMax(((short)0));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).setWpagEleTitRataMax(((short)0));
            //-->  INIZIALIZZAZIONE DATI TRANCHE DI GARANZIA (OCCURS 2)
            // COB_CODE: PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
            //                   UNTIL IX-TAB-TGA > WK-ELE-MAX-TRCH-DI-GAR
            //                  (IX-TAB-CALC, IX-TAB-TGA)
            //           END-PERFORM
            ws.getIxIndici().setTabTga(((short)1));
            while (!(ws.getIxIndici().getTabTga() > ws.getWkEleMaxTrchDiGar())) {
                // COB_CODE: MOVE SPACES
                //             TO WPAG-TP-RGM-FISC(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-FL-PREL-RIS(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-FL-RICALCOLO(IX-TAB-CALC, IX-TAB-TGA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).setWpagTpRgmFisc("");
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).setWpagFlPrelRis("");
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).setWpagFlRicalcolo("");
                // COB_CODE: MOVE HIGH-VALUE
                //             TO WPAG-TP-TRCH-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-PRE-CASO-MOR-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-BNS-ANTIC-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-PRE-INI-NET-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-PRE-PP-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-PRE-PP-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-PRE-TARI-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-PRE-TARI-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-PRE-INVRIO-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-PRE-INVRIO-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-SOPR-PROF-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-SOPR-SAN-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-SOPR-SPO-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-SOPR-TEC-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-ALT-SOPR-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TS-RIVAL-FIS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-RAT-LRD-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-PRE-LRD-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-PRSTZ-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-PRSTZ-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-CPT-RSH-MOR-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-SCON-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-ALQ-SCON-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-CAR-ACQ-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-CAR-INC-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-CAR-GEST-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-AZ-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-ADER-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-TFR-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-VOLO-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-ABB-ANNO-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-ABB-TOT-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-DUR-ABB-ANNI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-PRE-PATTUITO-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-PREST-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-PRE-RIVAL-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-ALIQ-INCAS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-ALIQ-ACQ-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-ALIQ-RICOR-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-IMP-INCAS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-IMP-ACQ-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-IMP-RICOR-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-IMP-PRSTZ-AGG-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-MANFEE-ANTIC-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-IMP-ACQ-EXP-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-IMP-REN-ASS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-IMP-COMMIS-INT-NULL
                //                (IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-ALIQ-REN-ASS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-ALIQ-COMMIS-INT-NULL
                //                (IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-IMPB-REN-ASS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                //                WPAG-TGA-IMPB-COMMIS-INT-NULL
                //                (IX-TAB-CALC, IX-TAB-TGA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).setWpagTpTrch(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagDatiTranche.Len.WPAG_TP_TRCH));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreCasoMor().setWpagPreCasoMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPreCasoMor.Len.WPAG_PRE_CASO_MOR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpBnsAntic().setWpagImpBnsAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpBnsAntic.Len.WPAG_IMP_BNS_ANTIC_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreIniNet().setWpagPreIniNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPreIniNet.Len.WPAG_PRE_INI_NET_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrePpIni().setWpagPrePpIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPrePpIni.Len.WPAG_PRE_PP_INI_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrePpUlt().setWpagPrePpUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPrePpUlt.Len.WPAG_PRE_PP_ULT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreTariIni().setWpagPreTariIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPreTariIni.Len.WPAG_PRE_TARI_INI_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreTariUlt().setWpagPreTariUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPreTariUlt.Len.WPAG_PRE_TARI_ULT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreInvrioIni().setWpagPreInvrioIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPreInvrioIni.Len.WPAG_PRE_INVRIO_INI_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreInvrioUlt().setWpagPreInvrioUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPreInvrioUlt.Len.WPAG_PRE_INVRIO_ULT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprProf().setWpagImpSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpSoprProf.Len.WPAG_IMP_SOPR_PROF_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprSan().setWpagImpSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpSoprSan.Len.WPAG_IMP_SOPR_SAN_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprSpo().setWpagImpSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpSoprSpo.Len.WPAG_IMP_SOPR_SPO_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprTec().setWpagImpSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpSoprTec.Len.WPAG_IMP_SOPR_TEC_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAltSopr().setWpagImpAltSoprNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpAltSopr.Len.WPAG_IMP_ALT_SOPR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTsRivalFis().setWpagTsRivalFisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTsRivalFis.Len.WPAG_TS_RIVAL_FIS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagRatLrd().setWpagRatLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRatLrd.Len.WPAG_RAT_LRD_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreLrd().setWpagPreLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPreLrd.Len.WPAG_PRE_LRD_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrstzIni().setWpagPrstzIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPrstzIni.Len.WPAG_PRSTZ_INI_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrstzUlt().setWpagPrstzUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPrstzUlt.Len.WPAG_PRSTZ_ULT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagCptInOpzRivto().setWpagCptInOpzRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagCptInOpzRivto.Len.WPAG_CPT_IN_OPZ_RIVTO_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagCptRshMor().setWpagCptRshMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagCptRshMor.Len.WPAG_CPT_RSH_MOR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpScon().setWpagImpSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpScon.Len.WPAG_IMP_SCON_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagAlqScon().setWpagAlqSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagAlqScon.Len.WPAG_ALQ_SCON_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarAcqTga().setWpagImpCarAcqTgaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpCarAcqTga.Len.WPAG_IMP_CAR_ACQ_TGA_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarIncTga().setWpagImpCarIncTgaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpCarIncTga.Len.WPAG_IMP_CAR_INC_TGA_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarGestTga().setWpagImpCarGestTgaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpCarGestTga.Len.WPAG_IMP_CAR_GEST_TGA_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAzTga().setWpagImpAzTgaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpAzTga.Len.WPAG_IMP_AZ_TGA_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAderTga().setWpagImpAderTgaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpAderTga.Len.WPAG_IMP_ADER_TGA_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpTfrTga().setWpagImpTfrTgaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpTfrTga.Len.WPAG_IMP_TFR_TGA_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpVoloTga().setWpagImpVoloTgaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpVoloTga.Len.WPAG_IMP_VOLO_TGA_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAbbAnnoUlt().setWpagImpAbbAnnoUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpAbbAnnoUlt.Len.WPAG_IMP_ABB_ANNO_ULT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAbbTotIni().setWpagImpAbbTotIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpAbbTotIni.Len.WPAG_IMP_ABB_TOT_INI_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagDurAbbAnni().setWpagDurAbbAnniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagDurAbbAnni.Len.WPAG_DUR_ABB_ANNI_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpPrePattuito().setWpagImpPrePattuitoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpPrePattuito.Len.WPAG_IMP_PRE_PATTUITO_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpPrestUlt().setWpagImpPrestUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpPrestUlt.Len.WPAG_IMP_PREST_ULT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpPreRival().setWpagImpPreRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpPreRival.Len.WPAG_IMP_PRE_RIVAL_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaAliqIncas().setWpagTgaAliqIncasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaAliqIncas.Len.WPAG_TGA_ALIQ_INCAS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaAliqAcq().setWpagTgaAliqAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaAliqAcq.Len.WPAG_TGA_ALIQ_ACQ_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaAliqRicor().setWpagTgaAliqRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaAliqRicor.Len.WPAG_TGA_ALIQ_RICOR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpIncas().setWpagTgaImpIncasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaImpIncas.Len.WPAG_TGA_IMP_INCAS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpAcq().setWpagTgaImpAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaImpAcq.Len.WPAG_TGA_IMP_ACQ_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpRicor().setWpagTgaImpRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaImpRicor.Len.WPAG_TGA_IMP_RICOR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpPrstzAggIni().setWpagImpPrstzAggIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpPrstzAggIni.Len.WPAG_IMP_PRSTZ_AGG_INI_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagManfeeAntic().setWpagManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagManfeeAntic.Len.WPAG_MANFEE_ANTIC_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpAcqExp().setWpagTgaImpAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaImpAcqExp.Len.WPAG_TGA_IMP_ACQ_EXP_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpRenAss().setWpagTgaImpRenAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaImpRenAss.Len.WPAG_TGA_IMP_REN_ASS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpCommisInt().setWpagTgaImpCommisIntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaImpCommisInt.Len.WPAG_TGA_IMP_COMMIS_INT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaAliqRenAss().setWpagTgaAliqRenAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaAliqRenAss.Len.WPAG_TGA_ALIQ_REN_ASS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaAliqCommisInt().setWpagTgaAliqCommisIntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaAliqCommisInt.Len.WPAG_TGA_ALIQ_COMMIS_INT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpbRenAss().setWpagTgaImpbRenAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaImpbRenAss.Len.WPAG_TGA_IMPB_REN_ASS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpbCommisInt().setWpagTgaImpbCommisIntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTgaImpbCommisInt.Len.WPAG_TGA_IMPB_COMMIS_INT_NULL));
                ws.getIxIndici().setTabTga(Trunc.toShort(ws.getIxIndici().getTabTga() + 1, 4));
            }
            //--> INIZIALIZZAZIONE DATI SOPRAPPREMI  (OCCURS 5)
            // COB_CODE: PERFORM VARYING IX-TAB-SPG FROM 1 BY 1
            //                     UNTIL IX-TAB-SPG > WK-SOPR-GAR-ELE-MAX
            //                  WPAG-SOVRAM-NULL(IX-TAB-CALC, IX-TAB-SPG)
            //           END-PERFORM
            ws.getIxIndici().setTabSpg(((short)1));
            while (!(ws.getIxIndici().getTabSpg() > ws.getWkSoprGarEleMax())) {
                // COB_CODE: MOVE SPACES
                //             TO WPAG-COD-SOVRAP(IX-TAB-CALC, IX-TAB-SPG)
                //                WPAG-TP-D(IX-TAB-CALC, IX-TAB-SPG)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).setWpagCodSovrap("");
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParCalc(ws.getIxIndici().getTabSpg()).setWpagTpD("");
                // COB_CODE: MOVE HIGH-VALUE
                //             TO WPAG-PERC-SOVRAP-NULL(IX-TAB-CALC, IX-TAB-SPG)
                //                WPAG-IMP-SOVRAP-NULL(IX-TAB-CALC, IX-TAB-SPG)
                //                WPAG-SOVRAM-NULL(IX-TAB-CALC, IX-TAB-SPG)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagPercSovrap().setWpagPercSovrapNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPercSovrap.Len.WPAG_PERC_SOVRAP_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagImpSovrap().setWpagImpSovrapNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpSovrap.Len.WPAG_IMP_SOVRAP_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagSovram().setWpagSovramNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagSovram.Len.WPAG_SOVRAM_NULL));
                ws.getIxIndici().setTabSpg(Trunc.toShort(ws.getIxIndici().getTabSpg() + 1, 4));
            }
            //--> INIZIALIZZAZIONE DATI PARAMETRO DI CALCOLO  (OCCURS 20)
            // COB_CODE: PERFORM VARYING IX-TAB-PAR FROM 1 BY 1
            //                     UNTIL IX-TAB-PAR > WK-ELE-MAX-PARAM-D-CALC
            //                  WPAG-VAL-FL(IX-TAB-CALC, IX-TAB-PAR)
            //           END-PERFORM
            ws.getIxIndici().setTabPar(((short)1));
            while (!(ws.getIxIndici().getTabPar() > ws.getWkEleMaxParamDCalc())) {
                // COB_CODE: MOVE SPACES
                //             TO WPAG-COD-PARAM(IX-TAB-CALC, IX-TAB-PAR)
                //                WPAG-TP-D(IX-TAB-CALC, IX-TAB-PAR)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagCodParam("");
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagTpD("");
                // COB_CODE: MOVE HIGH-VALUE
                //             TO WPAG-VAL-DT-NULL(IX-TAB-CALC, IX-TAB-PAR)
                //                WPAG-VAL-IMP-NULL(IX-TAB-CALC, IX-TAB-PAR)
                //                WPAG-VAL-TS-NULL(IX-TAB-CALC, IX-TAB-PAR)
                //                WPAG-VAL-NUM-NULL(IX-TAB-CALC, IX-TAB-PAR)
                //                WPAG-VAL-PC-NULL(IX-TAB-CALC, IX-TAB-PAR)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValDt().setWpagValDtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagValDt.Len.WPAG_VAL_DT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValImp().setWpagValImpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagValImp.Len.WPAG_VAL_IMP_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValTs().setWpagValTsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagValTs.Len.WPAG_VAL_TS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValNum().setWpagValNumNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagValNum.Len.WPAG_VAL_NUM_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValPc().setWpagValPcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagValPc.Len.WPAG_VAL_PC_NULL));
                // COB_CODE: MOVE SPACES
                //             TO WPAG-VAL-STR(IX-TAB-CALC, IX-TAB-PAR)
                //                WPAG-VAL-FL(IX-TAB-CALC, IX-TAB-PAR)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagValStr("");
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagValFl(Types.SPACE_CHAR);
                ws.getIxIndici().setTabPar(Trunc.toShort(ws.getIxIndici().getTabPar() + 1, 4));
            }
            //-->   DATI PROVVIGIONI
            // COB_CODE: MOVE HIGH-VALUE
            //             TO WPAG-PRV-ACQ-1O-ANNO-NULL(IX-TAB-CALC)
            //                WPAG-PRV-ACQ-2O-ANNO-NULL(IX-TAB-CALC)
            //                WPAG-PRV-RICOR-NULL(IX-TAB-CALC)
            //                WPAG-PRV-INCAS-NULL(IX-TAB-CALC)
            //                WPAG-ALIQ-INCAS-NULL(IX-TAB-CALC)
            //                WPAG-ALIQ-ACQ-NULL(IX-TAB-CALC)
            //                WPAG-ALIQ-RICOR-NULL(IX-TAB-CALC)
            //                WPAG-IMP-INCAS-NULL(IX-TAB-CALC)
            //                WPAG-IMP-ACQ-NULL(IX-TAB-CALC)
            //                WPAG-IMP-RICOR-NULL(IX-TAB-CALC)
            //                WPAG-PRV-REN-ASS-NULL(IX-TAB-CALC)
            //                WPAG-PRV-COMMIS-INT-NULL(IX-TAB-CALC)
            //                WPAG-ALIQ-REN-ASS-NULL(IX-TAB-CALC)
            //                WPAG-ALIQ-COMMIS-INT-NULL(IX-TAB-CALC)
            //                WPAG-IMP-REN-ASS-NULL(IX-TAB-CALC)
            //                WPAG-IMP-COMMIS-INT-NULL(IX-TAB-CALC)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagPrvAcq1oAnno().setWpagPrvAcq1oAnnoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPrvAcq1oAnno.Len.WPAG_PRV_ACQ1O_ANNO_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagPrvAcq2oAnno().setWpagPrvAcq2oAnnoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPrvAcq2oAnno.Len.WPAG_PRV_ACQ2O_ANNO_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagPrvRicor().setWpagPrvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPrvRicor.Len.WPAG_PRV_RICOR_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagPrvIncas().setWpagPrvIncasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPrvIncas.Len.WPAG_PRV_INCAS_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagAliqIncas().setWpagAliqIncasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagAliqIncas.Len.WPAG_ALIQ_INCAS_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagAliqAcq().setWpagAliqAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagAliqAcq.Len.WPAG_ALIQ_ACQ_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagAliqRicor().setWpagAliqRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagAliqRicor.Len.WPAG_ALIQ_RICOR_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagImpIncas().setWpagImpIncasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpIncas.Len.WPAG_IMP_INCAS_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagImpAcq().setWpagImpAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpAcq.Len.WPAG_IMP_ACQ_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagImpRicor().setWpagImpRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpRicor.Len.WPAG_IMP_RICOR_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagPrvRenAss().setWpagPrvRenAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPrvRenAss.Len.WPAG_PRV_REN_ASS_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagPrvCommisInt().setWpagPrvCommisIntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagPrvCommisInt.Len.WPAG_PRV_COMMIS_INT_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagAliqRenAss().setWpagAliqRenAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagAliqRenAss.Len.WPAG_ALIQ_REN_ASS_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagAliqCommisInt().setWpagAliqCommisIntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagAliqCommisInt.Len.WPAG_ALIQ_COMMIS_INT_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagImpRenAss().setWpagImpRenAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpRenAss.Len.WPAG_IMP_REN_ASS_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiProvvig().getWpagImpCommisInt().setWpagImpCommisIntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpCommisInt.Len.WPAG_IMP_COMMIS_INT_NULL));
            // COB_CODE:        PERFORM VARYING IX-TAB-TIT FROM 1 BY 1
            //                            UNTIL IX-TAB-TIT > WK-ELE-MAX-TIT-CONT
            //           *-->     DATI TITOLO CONTABILE  (OCCURS 12)
            //                         (IX-TAB-CALC, IX-TAB-TIT)
            //                  END-PERFORM
            ws.getIxIndici().setTabTit(((short)1));
            while (!(ws.getIxIndici().getTabTit() > ws.getWkEleMaxTitCont())) {
                //-->     DATI TITOLO CONTABILE  (OCCURS 12)
                // COB_CODE: MOVE HIGH-VALUE
                //             TO WPAG-CONT-PREMIO-NETTO-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-INT-FRAZ-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-INT-RETRODT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-DIRITTI-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //               WPAG-CONT-SPESE-MEDICHE-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-TASSE-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-SOPR-SANIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-SOPR-PROFES-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-SOPR-SPORT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-SOPR-TECN-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-ALTRI-SOPR-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-PREMIO-TOT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-PREMIO-PURO-IAS-NULL
                //                                          (IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-PREMIO-RISC-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-IMP-CAR-ACQ-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-IMP-CAR-INC-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-IMP-CAR-GEST-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-ACQ-1O-ANNO-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-ACQ-2O-ANNO-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-RICOR-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-INCAS-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-IMP-AZ-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-IMP-ADER-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-IMP-TFR-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-IMP-VOLO-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-IMP-ACQ-EXP-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-IMP-REN-ASS-NULL(IX-TAB-CALC, IX-TAB-TIT)
                //                WPAG-CONT-IMP-COMMIS-INT-NULL
                //                (IX-TAB-CALC, IX-TAB-TIT)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioNetto().setWpagContPremioNettoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContPremioNetto.Len.WPAG_CONT_PREMIO_NETTO_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIntFraz().setWpagContIntFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContIntFraz.Len.WPAG_CONT_INT_FRAZ_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIntRetrodt().setWpagContIntRetrodtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContIntRetrodt.Len.WPAG_CONT_INT_RETRODT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().setWpagContDirittiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContDiritti.Len.WPAG_CONT_DIRITTI_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSpeseMediche().setWpagContSpeseMedicheNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContSpeseMediche.Len.WPAG_CONT_SPESE_MEDICHE_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContTasse().setWpagContTasseNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContTasse.Len.WPAG_CONT_TASSE_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprSanit().setWpagContSoprSanitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContSoprSanit.Len.WPAG_CONT_SOPR_SANIT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprProfes().setWpagContSoprProfesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContSoprProfes.Len.WPAG_CONT_SOPR_PROFES_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprSport().setWpagContSoprSportNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContSoprSport.Len.WPAG_CONT_SOPR_SPORT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprTecn().setWpagContSoprTecnNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContSoprTecn.Len.WPAG_CONT_SOPR_TECN_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAltriSopr().setWpagContAltriSoprNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContAltriSopr.Len.WPAG_CONT_ALTRI_SOPR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioTot().setWpagContPremioTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContPremioTot.Len.WPAG_CONT_PREMIO_TOT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioPuroIas().setWpagContPremioPuroIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContPremioPuroIas.Len.WPAG_CONT_PREMIO_PURO_IAS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioRisc().setWpagContPremioRiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContPremioRisc.Len.WPAG_CONT_PREMIO_RISC_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarAcqTit().setWpagImpCarAcqTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpCarAcqTit.Len.WPAG_IMP_CAR_ACQ_TIT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarIncTit().setWpagImpCarIncTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpCarIncTit.Len.WPAG_IMP_CAR_INC_TIT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarGestTit().setWpagImpCarGestTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpCarGestTit.Len.WPAG_IMP_CAR_GEST_TIT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAcq1oAnno().setWpagContAcq1oAnnoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContAcq1oAnno.Len.WPAG_CONT_ACQ1O_ANNO_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAcq2oAnno().setWpagContAcq2oAnnoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContAcq2oAnno.Len.WPAG_CONT_ACQ2O_ANNO_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContRicor().setWpagContRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContRicor.Len.WPAG_CONT_RICOR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIncas().setWpagContIncasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContIncas.Len.WPAG_CONT_INCAS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpAzTit().setWpagImpAzTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpAzTit.Len.WPAG_IMP_AZ_TIT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpAderTit().setWpagImpAderTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpAderTit.Len.WPAG_IMP_ADER_TIT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpTfrTit().setWpagImpTfrTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpTfrTit.Len.WPAG_IMP_TFR_TIT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpVoloTit().setWpagImpVoloTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpVoloTit.Len.WPAG_IMP_VOLO_TIT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpAcqExp().setWpagContImpAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContImpAcqExp.Len.WPAG_CONT_IMP_ACQ_EXP_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpRenAss().setWpagContImpRenAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContImpRenAss.Len.WPAG_CONT_IMP_REN_ASS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpCommisInt().setWpagContImpCommisIntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagContImpCommisInt.Len.WPAG_CONT_IMP_COMMIS_INT_NULL));
                ws.getIxIndici().setTabTit(Trunc.toShort(ws.getIxIndici().getTabTit() + 1, 4));
            }
            //-->   DATI TITOLO DI RATA
            // COB_CODE: PERFORM VARYING IX-TAB-RATA FROM 1 BY 1
            //                     UNTIL IX-TAB-RATA > WK-ELE-MAX-TIT-RAT
            //                  (IX-TAB-CALC, IX-TAB-RATA)
            //           END-PERFORM
            ws.getIxIndici().setTabRata(((short)1));
            while (!(ws.getIxIndici().getTabRata() > ws.getWkEleMaxTitRat())) {
                // COB_CODE: MOVE HIGH-VALUE
                //             TO WPAG-RATA-PREMIO-NETTO-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-INT-FRAZ-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-INT-RETRODT-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-DIRITTI-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-SPESE-MEDICHE-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-TASSE-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-SOPR-SANIT-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-SOPR-PROFES-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-SOPR-SPORT-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-SOPR-TECN-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-ALTRI-SOPR-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-PREMIO-TOT-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-PREMIO-PURO-IAS-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-PREMIO-RISC-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-IMP-CAR-ACQ-TDR-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-IMP-CAR-INC-TDR-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-IMP-CAR-GEST-TDR-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-ACQ-1O-ANNO-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-ACQ-2O-ANNO-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-RICOR-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-INCAS-NULL
                //               (IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-IMP-AZ-TDR-NULL(IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-IMP-ADER-TDR-NULL(IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-IMP-TFR-TDR-NULL(IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-IMP-VOLO-TDR-NULL(IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-IMP-INT-RIATT-NULL(IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-IMP-ACQ-EXP-NULL(IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-IMP-REN-ASS-NULL(IX-TAB-CALC, IX-TAB-RATA)
                //                WPAG-RATA-IMP-COMMIS-INT-NULL
                //                (IX-TAB-CALC, IX-TAB-RATA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioNetto().setWpagRataPremioNettoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataPremioNetto.Len.WPAG_RATA_PREMIO_NETTO_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIntFraz().setWpagRataIntFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataIntFraz.Len.WPAG_RATA_INT_FRAZ_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIntRetrodt().setWpagRataIntRetrodtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataIntRetrodt.Len.WPAG_RATA_INT_RETRODT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().setWpagRataDirittiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataDiritti.Len.WPAG_RATA_DIRITTI_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSpeseMediche().setWpagRataSpeseMedicheNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataSpeseMediche.Len.WPAG_RATA_SPESE_MEDICHE_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataTasse().setWpagRataTasseNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataTasse.Len.WPAG_RATA_TASSE_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprSanit().setWpagRataSoprSanitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataSoprSanit.Len.WPAG_RATA_SOPR_SANIT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprProfes().setWpagRataSoprProfesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataSoprProfes.Len.WPAG_RATA_SOPR_PROFES_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprSport().setWpagRataSoprSportNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataSoprSport.Len.WPAG_RATA_SOPR_SPORT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprTecn().setWpagRataSoprTecnNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataSoprTecn.Len.WPAG_RATA_SOPR_TECN_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAltriSopr().setWpagRataAltriSoprNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataAltriSopr.Len.WPAG_RATA_ALTRI_SOPR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioTot().setWpagRataPremioTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataPremioTot.Len.WPAG_RATA_PREMIO_TOT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioPuroIas().setWpagRataPremioPuroIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataPremioPuroIas.Len.WPAG_RATA_PREMIO_PURO_IAS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioRisc().setWpagRataPremioRiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataPremioRisc.Len.WPAG_RATA_PREMIO_RISC_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarAcqTdr().setWpagImpCarAcqTdrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpCarAcqTdr.Len.WPAG_IMP_CAR_ACQ_TDR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarIncTdr().setWpagImpCarIncTdrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpCarIncTdr.Len.WPAG_IMP_CAR_INC_TDR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarGestTdr().setWpagImpCarGestTdrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpCarGestTdr.Len.WPAG_IMP_CAR_GEST_TDR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAcq1oAnno().setWpagRataAcq1oAnnoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataAcq1oAnno.Len.WPAG_RATA_ACQ1O_ANNO_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAcq2oAnno().setWpagRataAcq2oAnnoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataAcq2oAnno.Len.WPAG_RATA_ACQ2O_ANNO_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataRicor().setWpagRataRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataRicor.Len.WPAG_RATA_RICOR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIncas().setWpagRataIncasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataIncas.Len.WPAG_RATA_INCAS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpAzTdr().setWpagImpAzTdrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpAzTdr.Len.WPAG_IMP_AZ_TDR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpAderTdr().setWpagImpAderTdrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpAderTdr.Len.WPAG_IMP_ADER_TDR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpTfrTdr().setWpagImpTfrTdrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpTfrTdr.Len.WPAG_IMP_TFR_TDR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpVoloTdr().setWpagImpVoloTdrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpVoloTdr.Len.WPAG_IMP_VOLO_TDR_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpIntRiatt().setWpagImpIntRiattNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagImpIntRiatt.Len.WPAG_IMP_INT_RIATT_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpAcqExp().setWpagRataImpAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataImpAcqExp.Len.WPAG_RATA_IMP_ACQ_EXP_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpRenAss().setWpagRataImpRenAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataImpRenAss.Len.WPAG_RATA_IMP_REN_ASS_NULL));
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpCommisInt().setWpagRataImpCommisIntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagRataImpCommisInt.Len.WPAG_RATA_IMP_COMMIS_INT_NULL));
                ws.getIxIndici().setTabRata(Trunc.toShort(ws.getIxIndici().getTabRata() + 1, 4));
            }
            //-->   DATI ALTRI
            // COB_CODE: MOVE SPACES
            //             TO WPAG-TP-IAS(IX-TAB-CALC)
            //                WPAG-FL-FRAZ-PROVV(IX-TAB-CALC)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiAltri().setWpagTpIas("");
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiAltri().setWpagFlFrazProvv(Types.SPACE_CHAR);
            // COB_CODE: MOVE HIGH-VALUE
            //             TO WPAG-DT-VAL-QUOTE-NULL(IX-TAB-CALC)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiAltri().getWpagDtValQuote().setWpagDtValQuoteNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagDtValQuote.Len.WPAG_DT_VAL_QUOTE_NULL));
            //-->   DATI PARAMETRO MOVIMENTO
            // COB_CODE: MOVE HIGH-VALUE
            //             TO WPAG-DT-PROS-BNS-FED-NULL(IX-TAB-CALC)
            //                WPAG-DT-PROS-BNS-RIC-NULL(IX-TAB-CALC)
            //                WPAG-DT-PROS-CEDOLA-NULL(IX-TAB-CALC)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParMovim().getWpagDtProsBnsFed().setWpagDtProsBnsFedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagDtProsBnsFed.Len.WPAG_DT_PROS_BNS_FED_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParMovim().getWpagDtProsBnsRic().setWpagDtProsBnsRicNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagDtProsBnsRic.Len.WPAG_DT_PROS_BNS_RIC_NULL));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiParMovim().getWpagDtProsCedola().setWpagDtProsCedolaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagDtProsCedola.Len.WPAG_DT_PROS_CEDOLA_NULL));
            ws.getIxIndici().setTabCalc(Trunc.toShort(ws.getIxIndici().getTabCalc() + 1, 4));
        }
        //--> Data fine copertura titolo contabile
        // COB_CODE: MOVE HIGH-VALUE
        //             TO WPAG-DT-END-COP-TIT-NULL.
        wpagAreaPagina.getDatiOuput().getWpagDtEndCopTit().setWpagDtEndCopTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagDtEndCopTit.Len.WPAG_DT_END_COP_TIT_NULL));
    }

    /**Original name: S10000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s10000Elaborazione() {
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //              TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: IF VERSAM-AGGIUNTIVO
        //           END-IF
        //           END-IF
        if (ws.getWsMovimento().isVersamAggiuntivo()) {
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM S1110-CALL-LCCS1900
                //              THRU EX-S1110
                s1110CallLccs1900();
                // COB_CODE: IF LCCC1901-FL-ESEGUIBILE-SI
                //              CONTINUE
                //           ELSE
                //                 THRU EX-S0300
                //           END-IF
                if (ws.getLccc1901().getFlEseguibile().isLccc1901FlEseguibileSi()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: MOVE WK-PGM
                    //              TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1000-ELABORAZIONE'
                    //                    TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1000-ELABORAZIONE");
                    // COB_CODE: MOVE '005404'
                    //                    TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005404");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S11000
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM CALL-LVES0245
            //             THRU  CALL-LVES0245-EX
            callLves0245();
            //--  le informazioni da passare al valorizzatore variabili
            //--  vengono filtrate
            // COB_CODE: PERFORM S10100-FILTRO-DCLGEN
            //              THRU EX-S10100
            s10100FiltroDclgen();
            //--  ROUTINE SERVIZIO VALORIZZATORE VARIABILI
            // COB_CODE: PERFORM S11000-PREPARA-AREA-IVVS0211
            //              THRU EX-S11000
            s11000PreparaAreaIvvs0211();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU CALL-VALORIZZATORE-EX
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM CALL-VALORIZZATORE
            //              THRU CALL-VALORIZZATORE-EX
            callValorizzatore();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              MOVE S211-BUFFER-DATI    TO WSKD-AREA-SCHEDA
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE S211-BUFFER-DATI    TO WSKD-AREA-SCHEDA
            ws.setWskdAreaSchedaFormatted(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getBufferDatiFormatted());
        }
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *--     ROUTINE SERVIZIO DI PRODOTTO CALCOLI E CONTROLLI
        //                   END-IF
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //--     ROUTINE SERVIZIO DI PRODOTTO CALCOLI E CONTROLLI
            // COB_CODE: PERFORM S12000-PREPARA-AREA-CALC-CONTR
            //              THRU EX-S12000
            s12000PreparaAreaCalcContr();
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU EX-S12500
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM S12500-CALL-CALC-CONTR
                //              THRU EX-S12500
                s12500CallCalcContr();
            }
        }
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *-->    ROUTINE VALORIZZA AREA DATI PAGINA
        //                   END-IF
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //-->    ROUTINE VALORIZZA AREA DATI PAGINA
            // COB_CODE: PERFORM S14000-VAL-AREA-PAGINA-P
            //              THRU EX-S14000
            //           VARYING IX-AREA-ISPC0140 FROM 1 BY 1
            //             UNTIL IX-AREA-ISPC0140 > ISPC0140-DATI-NUM-MAX-ELE-P
            ws.getIxIndici().setAreaIspc0140(((short)1));
            while (!(ws.getIxIndici().getAreaIspc0140() > ws.getAreaIoCalcContr().getDatiNumMaxEleP())) {
                s14000ValAreaPaginaP();
                ws.getIxIndici().setAreaIspc0140(Trunc.toShort(ws.getIxIndici().getAreaIspc0140() + 1, 4));
            }
            // COB_CODE: PERFORM S14005-VAL-AREA-PAGINA-T
            //              THRU EX-S14005
            //           VARYING IX-AREA-ISPC0140 FROM 1 BY 1
            //             UNTIL IX-AREA-ISPC0140 > ISPC0140-DATI-NUM-MAX-ELE-T
            ws.getIxIndici().setAreaIspc0140(((short)1));
            while (!(ws.getIxIndici().getAreaIspc0140() > ws.getAreaIoCalcContr().getDatiNumMaxEleT())) {
                s14005ValAreaPaginaT();
                ws.getIxIndici().setAreaIspc0140(Trunc.toShort(ws.getIxIndici().getAreaIspc0140() + 1, 4));
            }
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU VALORIZZA-FLAG-AUTOGEN-EX
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM VALORIZZA-FLAG-AUTOGEN
                //              THRU VALORIZZA-FLAG-AUTOGEN-EX
                valorizzaFlagAutogen();
            }
            //--> VALORIZZAZIONE AREA PAGINA PER IAS GARANZIE
            // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO
            ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: IF INCLU-GARANZ         OR
                //              VERSAM-AGGIUNTIVO    OR
                //              VERSAM-AGGIUNTIVO-REINV
                //                UNTIL IX-PAG-IAS > WGRZ-ELE-GARANZIA-MAX
                //           END-IF
                if (ws.getWsMovimento().isIncluGaranz() || ws.getWsMovimento().isVersamAggiuntivo() || ws.getWsMovimento().isVersamAggiuntivoReinv()) {
                    // COB_CODE: PERFORM S14100-AREA-PAGINA-IAS-GAR
                    //              THRU S14100-AREA-PAGINA-IAS-GAR-EX
                    //           VARYING IX-PAG-IAS FROM 1 BY 1
                    //             UNTIL IX-PAG-IAS > WGRZ-ELE-GARANZIA-MAX
                    ws.getIxIndici().setPagIas(((short)1));
                    while (!(ws.getIxIndici().getPagIas() > wgrzAreaGaranzia.getEleGarMax())) {
                        s14100AreaPaginaIasGar();
                        ws.getIxIndici().setPagIas(Trunc.toShort(ws.getIxIndici().getPagIas() + 1, 4));
                    }
                }
            }
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU EX-S15000
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM S14500-GESTIONE-FONDI
                //              THRU EX-S14500
                s14500GestioneFondi();
                // COB_CODE: PERFORM S14700-GESTIONE-RATE
                //              THRU S14700-EX
                s14700GestioneRate();
                //-->    ROUTINE INIZIALIZZAZIONE TASTI
                // COB_CODE: PERFORM S15000-INIZIALIZZA-TASTI
                //              THRU EX-S15000
                s15000InizializzaTasti();
            }
        }
    }

    /**Original name: S1110-CALL-LCCS1900<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIAMATA ROUTINE VERIFICA CONCOMITANZA GETRA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1110CallLccs1900() {
        Lccs1900 lccs1900 = null;
        // COB_CODE: INITIALIZE                        LCCC1901-AREA.
        initLccc1901();
        // COB_CODE: PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
        //             UNTIL IX-TAB-GRZ > WGRZ-ELE-GARANZIA-MAX
        //             END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTabGrz(((short)1));
        while (!(ws.getIxIndici().getTabGrz() > wgrzAreaGaranzia.getEleGarMax())) {
            // COB_CODE: IF WGRZ-ST-ADD(IX-TAB-GRZ)
            //                TO LCCC1901-COD-TARI(LCCC1901-GAR-MAX)
            //           END-IF
            if (wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getStatus().isAdd()) {
                // COB_CODE: ADD 1             TO LCCC1901-GAR-MAX
                ws.getLccc1901().setGarMax(Trunc.toShort(1 + ws.getLccc1901().getGarMax(), 4));
                // COB_CODE: MOVE WGRZ-COD-TARI(IX-TAB-GRZ)
                //             TO LCCC1901-COD-TARI(LCCC1901-GAR-MAX)
                ws.getLccc1901().getCodTariOccurs(ws.getLccc1901().getGarMax()).setLccc1901CodTari(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzCodTari());
            }
            ws.getIxIndici().setTabGrz(Trunc.toShort(ws.getIxIndici().getTabGrz() + 1, 4));
        }
        // COB_CODE: CALL LCCS1900 USING AREA-IDSV0001
        //                               WCOM-AREA-STATI
        //                               WPOL-AREA-POLIZZA
        //                               WADE-AREA-ADESIONE
        //                               LCCC1901-AREA
        //           ON EXCEPTION
        //                     THRU EX-S0290
        //           END-CALL.
        try {
            lccs1900 = Lccs1900.getInstance();
            lccs1900.run(areaIdsv0001, lccc0001, wpolAreaPolizza, wadeAreaAdesione, ws.getLccc1901());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'FONDI'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("FONDI");
            // COB_CODE: MOVE 'S1110-CALL-LCCS1900'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1110-CALL-LCCS1900");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: CALL-LVES0245<br>
	 * <pre>----------------------------------------------------------------*
	 *     Forzatura Valore RATEANTIC nel caso di frazionamento annuale
	 * ----------------------------------------------------------------*</pre>*/
    private void callLves0245() {
        Lves0245 lves0245 = null;
        // COB_CODE: CALL LVES0245 USING  AREA-IDSV0001
        //                                WCOM-AREA-STATI
        //                                WPOL-AREA-POLIZZA
        //                                WADE-AREA-ADESIONE
        //                                WPMO-AREA-PARAM-MOV
        //                                WPOG-AREA-PARAM-OGG
        //                                WRAN-AREA-RAPP-ANAG
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lves0245 = Lves0245.getInstance();
            lves0245.run(new Object[] {areaIdsv0001, lccc0001, wpolAreaPolizza, wadeAreaAdesione, wpmoAreaParamMov, wpogAreaParamOgg, wranAreaRappAnag});
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'INIZIALIZZAZIONE DELLA PAGINA'
            //                                          TO CALL-DESC
            ws.getIdsv0002().setCallDesc("INIZIALIZZAZIONE DELLA PAGINA");
            // COB_CODE: MOVE 'S10010-CALL-LVES0245'    TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S10010-CALL-LVES0245");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: S10100-FILTRO-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     FILTRO DELLE OCCORRENZE CON STATUS DIVERSO DA 'D'
	 * ----------------------------------------------------------------*
	 * --> AREA RAPPORTO ANAGRAFICO</pre>*/
    private void s10100FiltroDclgen() {
        // COB_CODE: MOVE ZEROES     TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //           UNTIL IX-DCLGEN > WRAN-ELE-RAPP-ANAG-MAX
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > wranAreaRappAnag.getEleRappAnagMax())) {
            // COB_CODE: IF  NOT WRAN-ST-DEL(IX-DCLGEN)
            //           AND NOT WRAN-ST-CON(IX-DCLGEN)
            //                TO VRAN-TAB-RAPP-ANAG(IX-APPO)
            //           END-IF
            if (!wranAreaRappAnag.getTabRappAnag(ws.getIxIndici().getDclgen()).getLccvran1().getStatus().isDel() && !wranAreaRappAnag.getTabRappAnag(ws.getIxIndici().getDclgen()).getLccvran1().getStatus().isWpmoStCon()) {
                // COB_CODE: ADD 1     TO IX-APPO
                ws.getIxIndici().setAppo(Trunc.toShort(1 + ws.getIxIndici().getAppo(), 4));
                // COB_CODE: MOVE WRAN-TAB-RAPP-ANAG(IX-DCLGEN)
                //             TO VRAN-TAB-RAPP-ANAG(IX-APPO)
                ws.getVranTabRappAnag(ws.getIxIndici().getAppo()).setVranTabRappAnagBytes(wranAreaRappAnag.getTabRappAnag(ws.getIxIndici().getDclgen()).getTabRappAnagBytes());
            }
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE IX-APPO      TO  VRAN-ELE-RAPP-ANAG-MAX.
        ws.setVranEleRappAnagMax(ws.getIxIndici().getAppo());
        // COB_CODE: MOVE ZEROES       TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
        //--> AREA GARANZIE
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > WGRZ-ELE-GARANZIA-MAX
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > wgrzAreaGaranzia.getEleGarMax())) {
            // COB_CODE: IF  NOT WGRZ-ST-DEL(IX-DCLGEN)
            //              END-IF
            //           END-IF
            if (!wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getDclgen()).getLccvgrz1().getStatus().isDel()) {
                // COB_CODE: IF (NOT WGRZ-ST-CON(IX-DCLGEN)) OR
                //              (WGRZ-ST-CON(IX-DCLGEN) AND INCLU-GARANZ)
                //             END-IF
                //           END-IF
                if (!wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getDclgen()).getLccvgrz1().getStatus().isWpmoStCon() || wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getDclgen()).getLccvgrz1().getStatus().isWpmoStCon() && ws.getWsMovimento().isIncluGaranz()) {
                    // COB_CODE: IF     (VERSAM-AGGIUNTIVO  AND
                    //                   WGRZ-ST-INV(IX-DCLGEN) AND
                    //                WGRZ-TP-GAR(IX-DCLGEN) = 4 )
                    //             CONTINUE
                    //           ELSE
                    //              TO VGRZ-TAB-GAR(IX-APPO)
                    //           END-IF
                    if (ws.getWsMovimento().isVersamAggiuntivo() && wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getDclgen()).getLccvgrz1().getStatus().isInv() && wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getDclgen()).getLccvgrz1().getDati().getWgrzTpGar().getWgrzTpGar() == 4) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: ADD 1     TO IX-APPO
                        ws.getIxIndici().setAppo(Trunc.toShort(1 + ws.getIxIndici().getAppo(), 4));
                        // COB_CODE: MOVE WGRZ-TAB-GAR(IX-DCLGEN)
                        //            TO VGRZ-TAB-GAR(IX-APPO)
                        ws.getVgrzTabGar(ws.getIxIndici().getAppo()).setWgarTabGarBytes(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getDclgen()).getTabGarBytes());
                    }
                }
            }
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE IX-APPO      TO  VGRZ-ELE-GARANZIA-MAX.
        ws.setVgrzEleGaranziaMax(ws.getIxIndici().getAppo());
        // COB_CODE: MOVE ZEROES       TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
        //--> AREA TRANCHE DI GARANZIE
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > WTGA-ELE-TRAN-MAX
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > wtgaAreaTranche.getEleTranMax())) {
            // COB_CODE: IF  NOT WTGA-ST-DEL(IX-DCLGEN)
            //           AND NOT WTGA-TP-TRCH(IX-DCLGEN) = '9'
            //              END-IF
            //           END-IF
            if (!wtgaAreaTranche.getTabTran(ws.getIxIndici().getDclgen()).getLccvtga1().getStatus().isDel() && !Conditions.eq(wtgaAreaTranche.getTabTran(ws.getIxIndici().getDclgen()).getLccvtga1().getDati().getWtgaTpTrch(), "9")) {
                // COB_CODE: IF (NOT WTGA-ST-CON(IX-DCLGEN)) OR
                //              (WTGA-ST-CON(IX-DCLGEN) AND INCLU-GARANZ)
                //             TO VTGA-TAB-TRAN(IX-APPO)
                //           END-IF
                if (!wtgaAreaTranche.getTabTran(ws.getIxIndici().getDclgen()).getLccvtga1().getStatus().isWpmoStCon() || wtgaAreaTranche.getTabTran(ws.getIxIndici().getDclgen()).getLccvtga1().getStatus().isWpmoStCon() && ws.getWsMovimento().isIncluGaranz()) {
                    // COB_CODE: ADD 1    TO IX-APPO
                    ws.getIxIndici().setAppo(Trunc.toShort(1 + ws.getIxIndici().getAppo(), 4));
                    // COB_CODE: MOVE WTGA-TAB-TRAN(IX-DCLGEN)
                    //             TO VTGA-TAB-TRAN(IX-APPO)
                    ws.getVtgaTabTran(ws.getIxIndici().getAppo()).setWtgaTabTranBytes(wtgaAreaTranche.getTabTran(ws.getIxIndici().getDclgen()).getTabTranBytes());
                }
            }
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE IX-APPO      TO  VTGA-ELE-TRAN-MAX.
        ws.setVtgaEleTranMax(ws.getIxIndici().getAppo());
        // COB_CODE: MOVE ZEROES       TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
        //--> AREA SOPRAPREMIO DI GARANZIA
        // COB_CODE:      PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //                  UNTIL IX-DCLGEN > WSPG-ELE-SOPR-GAR-MAX
        //           ****** ATTENZIONE: I SOPRAPREMI CHE SONO STATI ESCLUSI
        //           ******            (FL-ESCL-SOPR = 'S') NON DEVONO  ESSERE PRESI
        //           ******             IN CONSIDERAZIONE
        //                     END-IF
        //                END-PERFORM.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > wspgAreaSoprGar.getEleSoprGarMax())) {
            //***** ATTENZIONE: I SOPRAPREMI CHE SONO STATI ESCLUSI
            //*****            (FL-ESCL-SOPR = 'S') NON DEVONO  ESSERE PRESI
            //*****             IN CONSIDERAZIONE
            // COB_CODE: IF  NOT WSPG-ST-DEL(IX-DCLGEN)
            //           AND NOT WSPG-ST-CON(IX-DCLGEN)
            //           AND NOT WSPG-FL-ESCL-SOPR(IX-DCLGEN) = 'S'
            //                TO VSPG-TAB-SPG(IX-APPO)
            //           END-IF
            if (!wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getDclgen()).getLccvspg1().getStatus().isDel() && !wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getDclgen()).getLccvspg1().getStatus().isWpmoStCon() && !(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getDclgen()).getLccvspg1().getDati().getWspgFlEsclSopr() == 'S')) {
                // COB_CODE: ADD 1    TO IX-APPO
                ws.getIxIndici().setAppo(Trunc.toShort(1 + ws.getIxIndici().getAppo(), 4));
                // COB_CODE: MOVE WSPG-TAB-SPG(IX-DCLGEN)
                //             TO VSPG-TAB-SPG(IX-APPO)
                ws.getVspgTabSpg(ws.getIxIndici().getAppo()).setVspgTabSpgBytes(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getDclgen()).getTabSpgBytes());
            }
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE IX-APPO      TO  VSPG-ELE-SOPR-GAR-MAX.
        ws.setVspgEleSoprGarMax(ws.getIxIndici().getAppo());
        // COB_CODE: MOVE ZEROES       TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
        //--> AREA SRATEGIA D'INVESTIMENTO
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > WSDI-ELE-STRA-INV-MAX
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > wsdiAreaStraInv.getEleStraInvMax())) {
            // COB_CODE: IF  NOT WSDI-ST-DEL(IX-DCLGEN)
            //           AND NOT WSDI-ST-CON(IX-DCLGEN)
            //                TO VSDI-TAB-STRA-INV(IX-APPO)
            //           END-IF
            if (!wsdiAreaStraInv.getTabStraInv(ws.getIxIndici().getDclgen()).getLccvsdi1().getStatus().isDel() && !wsdiAreaStraInv.getTabStraInv(ws.getIxIndici().getDclgen()).getLccvsdi1().getStatus().isWpmoStCon()) {
                // COB_CODE: ADD 1    TO IX-APPO
                ws.getIxIndici().setAppo(Trunc.toShort(1 + ws.getIxIndici().getAppo(), 4));
                // COB_CODE: MOVE WSDI-TAB-STRA-INV(IX-DCLGEN)
                //             TO VSDI-TAB-STRA-INV(IX-APPO)
                ws.getVsdiTabStraInv(ws.getIxIndici().getAppo()).setVsdiTabStraInvBytes(wsdiAreaStraInv.getTabStraInv(ws.getIxIndici().getDclgen()).getTabStraInvBytes());
            }
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE IX-APPO      TO  VSDI-ELE-STRA-INV-MAX.
        ws.setVsdiEleStraInvMax(ws.getIxIndici().getAppo());
        // COB_CODE: MOVE ZEROES       TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
        //--> AREA PARAMETRO MOVIMENTO
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > WPMO-ELE-PARAM-MOV-MAX
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > wpmoAreaParamMov.getEleParamMovMax())) {
            // COB_CODE: IF  NOT WPMO-ST-DEL(IX-DCLGEN)
            //           AND NOT WPMO-ST-CON(IX-DCLGEN)
            //                TO VPMO-TAB-PARAM-MOV (IX-APPO)
            //           END-IF
            if (!wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getDclgen()).getLccvpmo1().getStatus().isDel() && !wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getDclgen()).getLccvpmo1().getStatus().isWpmoStCon()) {
                // COB_CODE: ADD 1    TO IX-APPO
                ws.getIxIndici().setAppo(Trunc.toShort(1 + ws.getIxIndici().getAppo(), 4));
                // COB_CODE: MOVE WPMO-TAB-PARAM-MOV (IX-DCLGEN)
                //             TO VPMO-TAB-PARAM-MOV (IX-APPO)
                ws.getVpmoTabParamMov(ws.getIxIndici().getAppo()).setTabParamMovBytes(wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getDclgen()).getWpmoTabParamMovBytes());
            }
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO    TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IX-APPO                    TO  VPMO-ELE-PARAM-MOV-MAX.
        ws.setVpmoEleParamMovMax(ws.getIxIndici().getAppo());
        // COB_CODE: MOVE ZEROES                     TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
        //--> AREA PARAMETRO OGGETTO
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //                     UNTIL IX-DCLGEN > WPOG-ELE-PARAM-OGG-MAX
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > wpogAreaParamOgg.getEleParamOggMax())) {
            // COB_CODE: IF  NOT WPOG-ST-DEL(IX-DCLGEN)
            //           AND NOT WPOG-ST-CON(IX-DCLGEN)
            //                 TO VPOG-TAB-PARAM-OGG (IX-APPO)
            //           END-IF
            if (!wpogAreaParamOgg.getTabParamOgg(ws.getIxIndici().getDclgen()).getLccvpog1().getStatus().isDel() && !wpogAreaParamOgg.getTabParamOgg(ws.getIxIndici().getDclgen()).getLccvpog1().getStatus().isWpmoStCon()) {
                // COB_CODE: ADD 1    TO IX-APPO
                ws.getIxIndici().setAppo(Trunc.toShort(1 + ws.getIxIndici().getAppo(), 4));
                // COB_CODE: MOVE WPOG-TAB-PARAM-OGG (IX-DCLGEN)
                //             TO VPOG-TAB-PARAM-OGG (IX-APPO)
                ws.getVpogTabParamOgg(ws.getIxIndici().getAppo()).setVpogTabParamOggBytes(wpogAreaParamOgg.getTabParamOgg(ws.getIxIndici().getDclgen()).getTabParamOggBytes());
            }
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE IX-APPO      TO  VPOG-ELE-PARAM-OGG-MAX .
        ws.setVpogEleParamOggMax(ws.getIxIndici().getAppo());
        // COB_CODE: MOVE ZEROES       TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
        //--> AREA BENEFICIARI
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > WBEP-ELE-BENEFICIARI
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > wbepAreaBeneficiari.getEleBeneficiari())) {
            // COB_CODE: IF  NOT WBEP-ST-DEL(IX-DCLGEN)
            //           AND NOT WBEP-ST-CON(IX-DCLGEN)
            //                TO VBEP-TAB-BENEFICIARI (IX-APPO)
            //           END-IF
            if (!wbepAreaBeneficiari.getTabBeneficiari(ws.getIxIndici().getDclgen()).getLccvbep1().getStatus().isDel() && !wbepAreaBeneficiari.getTabBeneficiari(ws.getIxIndici().getDclgen()).getLccvbep1().getStatus().isWpmoStCon()) {
                // COB_CODE: ADD 1    TO IX-APPO
                ws.getIxIndici().setAppo(Trunc.toShort(1 + ws.getIxIndici().getAppo(), 4));
                // COB_CODE: MOVE WBEP-TAB-BENEFICIARI(IX-DCLGEN)
                //             TO VBEP-TAB-BENEFICIARI (IX-APPO)
                ws.getVbepTabBeneficiari(ws.getIxIndici().getAppo()).setVbepTabBeneficiariBytes(wbepAreaBeneficiari.getTabBeneficiari(ws.getIxIndici().getDclgen()).getTabBeneficiariBytes());
            }
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE IX-APPO      TO  VBEP-ELE-BENEFICIARI.
        ws.setVbepEleBeneficiari(ws.getIxIndici().getAppo());
        // COB_CODE: MOVE ZEROES       TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
        //--> AREA QUESTIONARIO
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > WQUE-ELE-QUEST-MAX
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > wqueAreaQuest.getEleQuestMax())) {
            // COB_CODE: IF  NOT WQUE-ST-DEL(IX-DCLGEN)
            //           AND NOT WQUE-ST-CON(IX-DCLGEN)
            //                TO VQUE-TAB-QUEST (IX-APPO)
            //           END-IF
            if (!wqueAreaQuest.getTabQuest(ws.getIxIndici().getDclgen()).getLccvque1().getStatus().isDel() && !wqueAreaQuest.getTabQuest(ws.getIxIndici().getDclgen()).getLccvque1().getStatus().isWpmoStCon()) {
                // COB_CODE: ADD 1    TO IX-APPO
                ws.getIxIndici().setAppo(Trunc.toShort(1 + ws.getIxIndici().getAppo(), 4));
                // COB_CODE: MOVE WQUE-TAB-QUEST(IX-DCLGEN)
                //             TO VQUE-TAB-QUEST (IX-APPO)
                ws.getVqueTabQuest(ws.getIxIndici().getAppo()).setVqueTabQuestBytes(wqueAreaQuest.getTabQuest(ws.getIxIndici().getDclgen()).getTabQuestBytes());
            }
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE IX-APPO      TO  VQUE-ELE-QUEST-MAX.
        ws.setVqueEleQuestMax(ws.getIxIndici().getAppo());
        // COB_CODE: MOVE ZEROES       TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
        //--> AREA DETTAGLIO QUESTIONARIO
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > WDEQ-ELE-DETT-QUEST-MAX
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > wdeqAreaDettQuest.getEleDettQuestMax())) {
            // COB_CODE: IF  NOT WDEQ-ST-DEL(IX-DCLGEN)
            //           AND NOT WDEQ-ST-CON(IX-DCLGEN)
            //                TO VDEQ-TAB-DETT-QUEST (IX-APPO)
            //           END-IF
            if (!wdeqAreaDettQuest.getTabDettQuest(ws.getIxIndici().getDclgen()).getLccvdeq1().getStatus().isDel() && !wdeqAreaDettQuest.getTabDettQuest(ws.getIxIndici().getDclgen()).getLccvdeq1().getStatus().isWpmoStCon()) {
                // COB_CODE: ADD 1    TO IX-APPO
                ws.getIxIndici().setAppo(Trunc.toShort(1 + ws.getIxIndici().getAppo(), 4));
                // COB_CODE: MOVE WDEQ-TAB-DETT-QUEST (IX-DCLGEN)
                //             TO VDEQ-TAB-DETT-QUEST (IX-APPO)
                ws.getVdeqTabDettQuest(ws.getIxIndici().getAppo()).setVdeqTabDettQuestBytes(wdeqAreaDettQuest.getTabDettQuest(ws.getIxIndici().getDclgen()).getTabDettQuestBytes());
            }
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE IX-APPO      TO  VDEQ-ELE-DETT-QUEST-MAX.
        ws.setVdeqEleDettQuestMax(ws.getIxIndici().getAppo());
        // COB_CODE: MOVE ZEROES       TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
        //--> AREA OGGETTO COLLEGATO
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > WOCO-ELE-OGG-COLLG-MAX
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > wocoAreaOggCollg.getEleOggCollgMax())) {
            // COB_CODE: IF  NOT WOCO-ST-DEL(IX-DCLGEN)
            //           AND NOT WOCO-ST-CON(IX-DCLGEN)
            //                TO VOCO-TAB-OGG-COLLG (IX-APPO)
            //           END-IF
            if (!wocoAreaOggCollg.getTabOggCollg(ws.getIxIndici().getDclgen()).getLccvoco1().getStatus().isDel() && !wocoAreaOggCollg.getTabOggCollg(ws.getIxIndici().getDclgen()).getLccvoco1().getStatus().isWpmoStCon()) {
                // COB_CODE: ADD 1    TO IX-APPO
                ws.getIxIndici().setAppo(Trunc.toShort(1 + ws.getIxIndici().getAppo(), 4));
                // COB_CODE: MOVE WOCO-TAB-OGG-COLLG (IX-DCLGEN)
                //             TO VOCO-TAB-OGG-COLLG (IX-APPO)
                ws.getVocoTabOggCollg(ws.getIxIndici().getAppo()).setVocoTabOggCollgBytes(wocoAreaOggCollg.getTabOggCollg(ws.getIxIndici().getDclgen()).getTabOggCollgBytes());
            }
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE IX-APPO      TO  VOCO-ELE-OGG-COLLG-MAX.
        ws.setVocoEleOggCollgMax(ws.getIxIndici().getAppo());
        // COB_CODE: MOVE ZEROES       TO  IX-APPO.
        ws.getIxIndici().setAppo(((short)0));
    }

    /**Original name: S10230-LETTURA-PCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     Lettura in ptf della parametro compagnia
	 * ----------------------------------------------------------------*</pre>*/
    private void s10230LetturaPco() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE HIGH-VALUE              TO PARAM-COMP.
        ws.getParamComp().initParamCompHighValues();
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                           IDSI0011-DATA-FINE-EFFETTO
        //                                           IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO PCO-COD-COMP-ANIA.
        ws.getParamComp().setPcoCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE 'PARAM-COMP'                TO IDSI0011-CODICE-STR-DATO
        //                                               WK-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("PARAM-COMP");
        ws.setWkTabella("PARAM-COMP");
        // COB_CODE: MOVE PARAM-COMP                  TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getParamComp().getParamCompFormatted());
        // COB_CODE: SET IDSI0011-SELECT              TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY         TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-NOT-FOUND
            //                       THRU EX-S0300
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                  MOVE IDSO0011-BUFFER-DATI TO PARAM-COMP
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: MOVE WK-PGM          TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S10230-LETTURA-PCO'
                    //                                TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S10230-LETTURA-PCO");
                    // COB_CODE: MOVE '005069'        TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005069");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI TO PARAM-COMP
                    ws.getParamComp().setParamCompFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S10230-LETTURA-PCO'
            //                                   TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S10230-LETTURA-PCO");
            // COB_CODE: MOVE '005016'           TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S11000-PREPARA-AREA-IVVS0211<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPAREA AREA SERVIZIO VALORIZZATORE VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private void s11000PreparaAreaIvvs0211() {
        // COB_CODE: INITIALIZE                          AREA-IO-IVVS0211.
        initAreaIoIvvs0211();
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO S211-COD-COMPAGNIA-ANIA.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211CodCompagniaAniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO      TO S211-TIPO-MOVIMENTO.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE WCOM-DT-ULT-VERS-PROD        TO S211-DATA-ULT-VERS-PROD
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataUltVersProd(lccc0001.getDtUltVersProd());
        // COB_CODE: IF INCLU-SOVRAP
        //           OR MOVIM-QUIETA
        //              END-IF
        //           ELSE
        //              SET S211-AREA-VE                 TO TRUE
        //           END-IF.
        if (ws.getWsMovimento().isIncluSovrap() || ws.getWsMovimento().isMovimQuieta()) {
            // COB_CODE: SET S211-AREA-PV                 TO TRUE
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFlgArea().setS211AreaPv();
            // COB_CODE: IF INCLU-SOVRAP
            //              PERFORM S14600-GESTIONE-SOPRA THRU EX-S14600
            //           END-IF
            if (ws.getWsMovimento().isIncluSovrap()) {
                // COB_CODE: PERFORM S14600-GESTIONE-SOPRA THRU EX-S14600
                s14600GestioneSopra();
            }
        }
        else {
            // COB_CODE: SET S211-AREA-VE                 TO TRUE
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFlgArea().setS211AreaVe();
        }
        // COB_CODE: SET S211-IN-CONV                    TO TRUE.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getStepElab().setS211InConv();
        // COB_CODE: MOVE ZEROES                         TO IX-MAX
        //                                                  WK-APPO-LUNGHEZZA.
        ws.getIxIndici().setMax(((short)0));
        ws.getWkVariabili().setWkAppoLunghezza(0);
        // COB_CODE: SET S211-FLAG-GAR-OPZIONE-NO        TO TRUE.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFlagGarOpzione().setS211FlagGarOpzioneNo();
        //-->  Valorizzazione della struttura di mapping
        // COB_CODE: PERFORM S11010-VAL-VAR-CONTESTO
        //              THRU S11010-EX
        s11010ValVarContesto();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU VAL-STR-DATI-PTF-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM VAL-STR-DATI-PTF
            //              THRU VAL-STR-DATI-PTF-EX
            valStrDatiPtf();
        }
    }

    /**Original name: S11010-VAL-VAR-CONTESTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELLE VARIBILI DI CONTESTO CALCOLATE
	 * ----------------------------------------------------------------*</pre>*/
    private void s11010ValVarContesto() {
        // COB_CODE: MOVE ZERO    TO WCNT-ELE-VAR-CONT-MAX.
        ws.getIvvc0212().setWcntEleVarContMax(((short)0));
        // COB_CODE: MOVE ZERO    TO WK-FRQ-MOVI
        ws.getWkVariabili().setWkFrqMovi(0);
        // COB_CODE: MOVE ZERO    TO WK-FRAZ-MM
        ws.getWkVariabili().getWkFrazMm().setFrazMm(0);
        // COB_CODE: MOVE ZERO    TO WK-DATA-LIMITE
        ws.getWkVariabili().setWkDataLimite(0);
        //--> occorre verificare se h presente la pmo di quietanzamento
        //--> 6101 o la GETRA  6002 (se esiste una PMO non puo
        //--> esistee l'altra) per calcolare le rate da recuperare.
        //--  Recupero frazionamento polizza del movimento di
        //--  Quietanzamento
        // COB_CODE: SET QUIETANZAMENTO-NO  TO TRUE
        ws.setWkQuietanzamento(false);
        // COB_CODE: SET GETRA-NO           TO TRUE
        ws.setWkGetra(false);
        //*** QUIETANZAMENTO
        // COB_CODE: SET MOVIM-QUIETA       TO TRUE
        ws.getWsMovimento().setMovimQuieta();
        // COB_CODE: PERFORM RIC-TAB-PMO
        //              THRU RIC-TAB-PMO-EX
        ricTabPmo();
        // COB_CODE:      IF WK-TROVATO
        //                   MOVE PCO-DT-ULT-QTZO-IN  TO WK-DATA-LIMITE
        //                ELSE
        //           **** GENERAZIONE TRANCHE
        //                   END-IF
        //                END-IF
        if (ws.getWkFlg().isTrovato()) {
            // COB_CODE: SET QUIETANZAMENTO-SI    TO TRUE
            ws.setWkQuietanzamento(true);
            // COB_CODE: MOVE PCO-DT-ULT-QTZO-IN  TO WK-DATA-LIMITE
            ws.getWkVariabili().setWkDataLimite(TruncAbs.toInt(ws.getParamComp().getPcoDtUltQtzoIn().getPcoDtUltQtzoIn(), 8));
        }
        else {
            //*** GENERAZIONE TRANCHE
            // COB_CODE: SET GENER-TRANCH         TO TRUE
            ws.getWsMovimento().setGenerTranch();
            // COB_CODE: PERFORM RIC-TAB-PMO
            //              THRU RIC-TAB-PMO-EX
            ricTabPmo();
            // COB_CODE: IF WK-TROVATO
            //              MOVE PCO-DT-ULTGZ-TRCH-E-IN  TO WK-DATA-LIMITE
            //           END-IF
            if (ws.getWkFlg().isTrovato()) {
                // COB_CODE: SET  GETRA-SI         TO TRUE
                ws.setWkGetra(true);
                // COB_CODE: MOVE PCO-DT-ULTGZ-TRCH-E-IN  TO WK-DATA-LIMITE
                ws.getWkVariabili().setWkDataLimite(TruncAbs.toInt(ws.getParamComp().getPcoDtUltgzTrchEIn().getPcoDtUltgzTrchEIn(), 8));
            }
        }
        //--  La variabile RATEDARECUP ha senso solo se la POLIZZA
        //--    Quietanzabile o se prevede la GETRA
        // COB_CODE: IF QUIETANZAMENTO-SI OR GETRA-SI
        //                 THRU S11011-EX
        //           END-IF.
        if (ws.isWkQuietanzamento() || ws.isWkGetra()) {
            // COB_CODE: IF WPMO-FRQ-MOVI-NULL(IX-RIC-PMO) = HIGH-VALUE
            //              MOVE ZERO  TO WK-FRQ-MOVI
            //           ELSE
            //              COMPUTE WK-FRAZ-MM = 12 / WK-FRQ-MOVI
            //           END-IF
            if (Characters.EQ_HIGH.test(wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getRicPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMoviNullFormatted())) {
                // COB_CODE: MOVE ZERO  TO WK-FRQ-MOVI
                ws.getWkVariabili().setWkFrqMovi(0);
            }
            else {
                // COB_CODE: MOVE WPMO-FRQ-MOVI(IX-RIC-PMO) TO WK-FRQ-MOVI
                ws.getWkVariabili().setWkFrqMovi(TruncAbs.toInt(wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getRicPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMovi(), 5));
                // COB_CODE: COMPUTE WK-FRAZ-MM = 12 / WK-FRQ-MOVI
                ws.getWkVariabili().getWkFrazMm().setFrazMm((new AfDecimal(((((double)12)) / ws.getWkVariabili().getWkFrqMovi()), 2, 0)).toInt());
            }
            // COB_CODE: PERFORM S11011-VAR-RATEDARECUP
            //              THRU S11011-EX
            s11011VarRatedarecup();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU CALC-VAR-SUMPAPSTC-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM CALC-VAR-SUMPAPSTC
            //              THRU CALC-VAR-SUMPAPSTC-EX
            calcVarSumpapstc();
        }
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: IF IDSV0001-ESITO-OK AND INCLU-GARANZ
        //                 THRU CALCOLO-RATEO-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getWsMovimento().isIncluGaranz()) {
            // COB_CODE: PERFORM CALCOLO-RATEO
            //              THRU CALCOLO-RATEO-EX
            calcoloRateo();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU VALORIZZA-CAUSCONT-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM VALORIZZA-CAUSCONT
            //              THRU VALORIZZA-CAUSCONT-EX
            valorizzaCauscont();
        }
    }

    /**Original name: VALORIZZA-CAUSCONT<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void valorizzaCauscont() {
        // COB_CODE: ADD  1                     TO WCNT-ELE-VAR-CONT-MAX
        ws.getIvvc0212().setWcntEleVarContMax(Trunc.toShort(1 + ws.getIvvc0212().getWcntEleVarContMax(), 4));
        // COB_CODE: MOVE WCNT-ELE-VAR-CONT-MAX TO IX-CNT
        ws.getIxIndici().setCnt(ws.getIvvc0212().getWcntEleVarContMax());
        // COB_CODE: MOVE 'CAUSCONT$'           TO WCNT-COD-VAR-CONT(IX-CNT)
        ws.getIvvc0212().getWcntTabVar().setWcntCodVarCont(ws.getIxIndici().getCnt(), "CAUSCONT$");
        // COB_CODE: MOVE 'S'                   TO WCNT-TP-DATO-CONT(IX-CNT)
        ws.getIvvc0212().getWcntTabVar().setWcntTpDatoContFormatted(ws.getIxIndici().getCnt(), "S");
        // COB_CODE: MOVE ZEROES                TO WCNT-VAL-IMP-CONT(IX-CNT)
        //                                         WCNT-VAL-PERC-CONT(IX-CNT)
        ws.getIvvc0212().getWcntTabVar().setWcntValImpCont(ws.getIxIndici().getCnt(), new AfDecimal(0, 18, 7));
        ws.getIvvc0212().getWcntTabVar().setWcntValPercCont(ws.getIxIndici().getCnt(), new AfDecimal(0, 14, 9));
        // COB_CODE: MOVE WCOM-CODICE-INIZIATIVA
        //                                      TO WCNT-VAL-STR-CONT(IX-CNT)
        ws.getIvvc0212().getWcntTabVar().setWcntValStrCont(ws.getIxIndici().getCnt(), lccc0001.getCodiceIniziativa());
        // COB_CODE: MOVE WS-LIV-PROD           TO WCNT-TP-LIVELLO(IX-CNT)
        ws.getIvvc0212().getWcntTabVar().setWcntTpLivello(ws.getIxIndici().getCnt(), ws.getWsLivProd());
        // COB_CODE: MOVE SPACES                TO WCNT-COD-LIVELLO(IX-CNT)
        ws.getIvvc0212().getWcntTabVar().setWcntCodLivello(ws.getIxIndici().getCnt(), "");
        // COB_CODE: MOVE ZEROES                TO WCNT-ID-LIVELLO(IX-CNT).
        ws.getIvvc0212().getWcntTabVar().setWcntIdLivello(ws.getIxIndici().getCnt(), 0);
    }

    /**Original name: S11011-VAR-RATEDARECUP<br>
	 * <pre>----------------------------------------------------------------*
	 * --  In questa fase la variabile viene calcolata
	 * --  come differenza tra la data dell'ultimo quietanzamento
	 * --  oppure data ultima generazione tranche e la data
	 * --  decorrenza/data effetto della polizza il tutto
	 * --  rapportato al frazionamento
	 * ----------------------------------------------------------------*
	 * --  Recupero parametro RATENATIC</pre>*/
    private void s11011VarRatedarecup() {
        // COB_CODE: SET WK-PR-RATEANTIC TO TRUE
        ws.getWkVariabili().setWkPrRateantic();
        // COB_CODE: PERFORM RIC-TAB-POG
        //              THRU RIC-TAB-POG-EX
        ricTabPog();
        // COB_CODE: IF WK-TROVATO
        //              END-EVALUATE
        //           END-IF
        if (ws.getWkFlg().isTrovato()) {
            // COB_CODE: EVALUATE WPOG-TP-D(IX-RIC-POG)
            //               WHEN 'S'
            //               WHEN 'I'
            //               WHEN 'P'
            //               WHEN 'N'
            //                     TO WK-RATE-ANTIC
            //           END-EVALUATE
            switch (wpogAreaParamOgg.getTabParamOgg(ws.getIxIndici().getRicPog()).getLccvpog1().getDati().getWpogTpD()) {

                case "S":
                case "I":
                case "P":
                case "N":// COB_CODE: MOVE WPOG-VAL-NUM(IX-RIC-POG)
                    //             TO WK-RATE-ANTIC
                    ws.getWkVariabili().setWkRateAntic(wpogAreaParamOgg.getTabParamOgg(ws.getIxIndici().getRicPog()).getLccvpog1().getDati().getWpogValNum().getWpogValNum());
                    break;

                default:break;
            }
        }
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *--     calcola il numero di rate retrodatate e successive
        //           *--     compresa la rata di perfezionamento
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //--     calcola il numero di rate retrodatate e successive
            //--     compresa la rata di perfezionamento
            // COB_CODE: PERFORM CALL-LCCS0062
            //              THRU CALL-LCCS0062-EX
            callLccs0062();
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF.
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: COMPUTE WK-RATE-RECUP = LCCC0062-TOT-NUM-RATE
                //                                 - WK-RATE-ANTIC - 1
                ws.getWkVariabili().setWkRateRecup(Trunc.toLong(ws.getAreaLccc0062().getTotNumRate() - ws.getWkVariabili().getWkRateAntic() - 1, 14));
                // COB_CODE: IF WK-RATE-RECUP < ZERO
                //              MOVE ZERO           TO WK-RATE-RECUP
                //           END-IF
                if (ws.getWkVariabili().getWkRateRecup() < 0) {
                    // COB_CODE: MOVE ZERO           TO WK-RATE-RECUP
                    ws.getWkVariabili().setWkRateRecup(0);
                }
                // COB_CODE: IF RECUPRATE-UNICA
                //              MOVE IX-CNT            TO WCNT-ELE-VAR-CONT-MAX
                //           END-IF
                if (ws.getWkVariabili().getWkTpRatPerf().isUnica()) {
                    // COB_CODE: MOVE WK-RATE-RECUP     TO WK-VAL-VAR
                    ws.getWkVariabili().setWkValVar(ws.getWkVariabili().getWkRateRecup());
                    // COB_CODE: ADD  1                 TO IX-CNT
                    ws.getIxIndici().setCnt(Trunc.toShort(1 + ws.getIxIndici().getCnt(), 4));
                    // COB_CODE: MOVE 'RATEDARECUP'     TO WCNT-COD-VAR-CONT(IX-CNT)
                    ws.getIvvc0212().getWcntTabVar().setWcntCodVarCont(ws.getIxIndici().getCnt(), "RATEDARECUP");
                    // COB_CODE: MOVE WS-NUMERO         TO WCNT-TP-DATO-CONT(IX-CNT)
                    ws.getIvvc0212().getWcntTabVar().setWcntTpDatoCont(ws.getIxIndici().getCnt(), ws.getWsTpDato().getNumero());
                    // COB_CODE: MOVE WK-VAL-VAR        TO WCNT-VAL-IMP-CONT(IX-CNT)
                    ws.getIvvc0212().getWcntTabVar().setWcntValImpCont(ws.getIxIndici().getCnt(), Trunc.toDecimal(ws.getWkVariabili().getWkValVar(), 18, 7));
                    // COB_CODE: MOVE IX-CNT            TO WCNT-ELE-VAR-CONT-MAX
                    ws.getIvvc0212().setWcntEleVarContMax(ws.getIxIndici().getCnt());
                }
            }
        }
    }

    /**Original name: CALC-VAR-SUMPAPSTC<br>
	 * <pre>----------------------------------------------------------------*
	 *     Variabile utilizzata per il calcolo delle provvigioni
	 *     tenendo conto di altre polizze di ptf.
	 * ----------------------------------------------------------------*
	 * --  Ricerca codice soggetto del contraente della polizza emessa</pre>*/
    private void calcVarSumpapstc() {
        Lccs0033 lccs0033 = null;
        // COB_CODE: SET CONTRAENTE       TO TRUE
        ws.getWsTpRappAna().setContraente();
        // COB_CODE: PERFORM RIC-TAB-RAN
        //              THRU RIC-TAB-RAN-EX
        ricTabRan();
        // COB_CODE: IF WK-TROVATO
        //              END-IF
        //           END-IF.
        if (ws.getWkFlg().isTrovato()) {
            // COB_CODE: SET LCCC0033-ESTR-POL-REC-PROVV TO TRUE
            ws.getAreaIoLccs0033().getModalitaServizio().setLccc0033EstrPolRecProvv();
            // COB_CODE: SET LCCC0033-VENDITA            TO TRUE
            ws.getAreaIoLccs0033().getMacroFunzione().setLccc0033Vendita();
            // COB_CODE: MOVE WRAN-COD-SOGG(IX-RIC-RAN) TO LCCC0033-COD-SOGG
            ws.getAreaIoLccs0033().setCodSogg(wranAreaRappAnag.getTabRappAnag(ws.getIxIndici().getRicRan()).getLccvran1().getDati().getWranCodSogg());
            // COB_CODE: MOVE 'CO'                      TO LCCC0033-TP-RAPP-ANA
            ws.getAreaIoLccs0033().setTpRappAna("CO");
            // COB_CODE: MOVE WPOL-DT-DECOR             TO LCCC0033-DT-DECOR-POL
            ws.getAreaIoLccs0033().setDtDecorPol(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtDecor());
            // COB_CODE: CALL LCCS0033 USING  AREA-IDSV0001
            //                                WCOM-AREA-STATI
            //                                AREA-IO-LCCS0033
            //           ON EXCEPTION
            //                 THRU EX-S0290
            //           END-CALL
            try {
                lccs0033 = Lccs0033.getInstance();
                lccs0033.run(areaIdsv0001, lccc0001, ws.getAreaIoLccs0033());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'VERIFICA-RECUP-PROVV' TO CALL-DESC
                ws.getIdsv0002().setCallDesc("VERIFICA-RECUP-PROVV");
                // COB_CODE: MOVE 'CALC-VAR-SUMPAPSTC'   TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("CALC-VAR-SUMPAPSTC");
                // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
                //              THRU EX-S0290
                s0290ErroreDiSistema();
            }
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              MOVE IX-CNT               TO WCNT-ELE-VAR-CONT-MAX
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: COMPUTE WS-TOT-PREMIO-ANNUO =
                //           FUNCTION SUM(LCCC0033-IMP-PREMIO-ANNUO(ALL))
                ws.getWkVariabili().setWsTotPremioAnnuo(Trunc.toDecimal(MathFunctions.sum(((AfDecimal[])ws.getAreaIoLccs0033().getAllRecuperoProvvLccc0033ImpPremioAnnuo(-1))), 15, 3));
                // COB_CODE: ADD  1                    TO IX-CNT
                ws.getIxIndici().setCnt(Trunc.toShort(1 + ws.getIxIndici().getCnt(), 4));
                // COB_CODE: MOVE 'SUMPAPSTC'          TO WCNT-COD-VAR-CONT(IX-CNT)
                ws.getIvvc0212().getWcntTabVar().setWcntCodVarCont(ws.getIxIndici().getCnt(), "SUMPAPSTC");
                // COB_CODE: MOVE WS-IMPORTO           TO WCNT-TP-DATO-CONT(IX-CNT)
                ws.getIvvc0212().getWcntTabVar().setWcntTpDatoCont(ws.getIxIndici().getCnt(), ws.getWsTpDato().getImporto());
                // COB_CODE: MOVE WS-TOT-PREMIO-ANNUO
                //                                     TO WCNT-VAL-IMP-CONT(IX-CNT)
                ws.getIvvc0212().getWcntTabVar().setWcntValImpCont(ws.getIxIndici().getCnt(), Trunc.toDecimal(ws.getWkVariabili().getWsTotPremioAnnuo(), 18, 7));
                // COB_CODE: MOVE IX-CNT               TO WCNT-ELE-VAR-CONT-MAX
                ws.getIvvc0212().setWcntEleVarContMax(ws.getIxIndici().getCnt());
            }
        }
    }

    /**Original name: CALCOLO-RATEO<br>
	 * <pre>----------------------------------------------------------------*
	 *     Variabile utilizzata per il calcolo del rateo.
	 * ----------------------------------------------------------------*</pre>*/
    private void calcoloRateo() {
        Lccs0490 lccs0490 = null;
        // COB_CODE: INITIALIZE LCCC0490-AREA-INPUT.
        initAreaInput();
        // COB_CODE: MOVE WPOL-ID-POLI        TO LCCC0490-ID-POLI.
        ws.getLccc0490().setIdPoli(TruncAbs.toInt(wpolAreaPolizza.getLccvpol1().getDati().getWpolIdPoli(), 9));
        // COB_CODE: MOVE WPOL-DT-DECOR       TO LCCC0490-DT-DECOR-POLI.
        ws.getLccc0490().setDtDecorPoli(TruncAbs.toInt(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtDecor(), 8));
        // Ricerca garanzia base
        // COB_CODE: SET TP-GAR-BASE          TO TRUE.
        ws.getLccc0006().getActTpGaranzia().setTpGarBase();
        // COB_CODE: PERFORM RIC-TAB-GAR
        //              THRU RIC-TAB-GAR-EX.
        ricTabGar();
        // Ricerca garanzia complementare
        // COB_CODE: SET TP-GAR-COMPLEM       TO TRUE.
        ws.getLccc0006().getActTpGaranzia().setTpGarComplem();
        // COB_CODE: PERFORM RIC-TAB-GAR
        //              THRU RIC-TAB-GAR-EX.
        ricTabGar();
        // Richiamo modulo calcolo rateo
        // COB_CODE: CALL LCCS0490 USING  AREA-IDSV0001
        //                                WCOM-AREA-STATI
        //                                LCCC0490
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0490 = Lccs0490.getInstance();
            lccs0490.run(areaIdsv0001, lccc0001, ws.getLccc0490());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CALCOLO RATEO'  TO CALL-DESC
            ws.getIdsv0002().setCallDesc("CALCOLO RATEO");
            // COB_CODE: MOVE 'CALCOLO-RATEO'  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALCOLO-RATEO");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE LCCC0490-DT-FINE-COP    TO WPAG-DT-END-COP-TIT
            wpagAreaPagina.getDatiOuput().getWpagDtEndCopTit().setWpagDtEndCopTit(ws.getLccc0490().getDtFineCop());
            // COB_CODE: IF LCCC0490-SI-PRESENZA-RATEO
            //              MOVE IX-CNT               TO WCNT-ELE-VAR-CONT-MAX
            //           END-IF
            if (ws.getLccc0490().getPresenzaRateo().isLccc0490SiPresenzaRateo()) {
                // COB_CODE: ADD  1                    TO IX-CNT
                ws.getIxIndici().setCnt(Trunc.toShort(1 + ws.getIxIndici().getCnt(), 4));
                // COB_CODE: MOVE 'RATEO'              TO WCNT-COD-VAR-CONT(IX-CNT)
                ws.getIvvc0212().getWcntTabVar().setWcntCodVarCont(ws.getIxIndici().getCnt(), "RATEO");
                // COB_CODE: MOVE WS-NUMERO            TO WCNT-TP-DATO-CONT(IX-CNT)
                ws.getIvvc0212().getWcntTabVar().setWcntTpDatoCont(ws.getIxIndici().getCnt(), ws.getWsTpDato().getNumero());
                // COB_CODE: ADD  1                    TO LCCC0490-RATEO
                ws.getLccc0490().setRateo(Trunc.toInt(1 + ws.getLccc0490().getRateo(), 9));
                // COB_CODE: COMPUTE WCNT-VAL-IMP-CONT(IX-CNT) =
                //                   LCCC0490-RATEO / 360
                ws.getIvvc0212().getWcntTabVar().setWcntValImpCont(ws.getIxIndici().getCnt(), Trunc.toDecimal(new AfDecimal(((((double)(ws.getLccc0490().getRateo()))) / 360), 16, 7), 18, 7));
                // COB_CODE: MOVE IX-CNT               TO WCNT-ELE-VAR-CONT-MAX
                ws.getIvvc0212().setWcntEleVarContMax(ws.getIxIndici().getCnt());
            }
            // COB_CODE: IF LCCC0490-SI-TIT-EMESSI
            //                 THRU EX-S0300
            //           END-IF
            if (ws.getLccc0490().getTitoliEmessi().isLccc0490SiTitEmessi()) {
                // COB_CODE: MOVE WK-PGM               TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S11010-VAL-VAR-CONTESTO' TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S11010-VAL-VAR-CONTESTO");
                // COB_CODE: MOVE '005166'             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005166");
                // COB_CODE: MOVE SPACES               TO IEAI9901-PARAMETRI-ERR
                //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                // COB_CODE: STRING 'NON SI POSSONO INCLUDERE GARANZIE '
                //                  DELIMITED BY SIZE
                //                 'A QUESTA DATA EFFETTO PER LA PRESENZA DI TITOLI'
                //                  DELIMITED BY SIZE
                //                  ' EMESSI'
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append("NON SI POSSONO INCLUDERE GARANZIE ").append("A QUESTA DATA EFFETTO PER LA PRESENZA DI TITOLI").append(" EMESSI").toString());
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: VAL-STR-DATI-PTF<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void valStrDatiPtf() {
        // COB_CODE: IF WPOL-ELE-POLI-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wpolAreaPolizza.getWpolElePoliMax() > 0) {
            // COB_CODE: MOVE 1                           TO IX-MAX
            ws.getIxIndici().setMax(((short)1));
            // COB_CODE: MOVE S211-ALIAS-POLI             TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasPoli());
            // COB_CODE: MOVE 1                           TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), 1);
            // COB_CODE: MOVE LENGTH OF WPOL-AREA-POLIZZA TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), WpolAreaPolizzaLccs0005.Len.WPOL_AREA_POLIZZA);
            // COB_CODE: MOVE WPOL-AREA-POLIZZA
            //             TO S211-BUFFER-DATI
            //               (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(wpolAreaPolizza.getWpolAreaPolizzaFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WADE-ELE-ADES-MAX > 0
        //                 (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wadeAreaAdesione.getWadeEleAdesMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //--> OCCORRENZA 2 AREA ADESIONE
            // COB_CODE: MOVE S211-ALIAS-ADES              TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasAdes());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF WADE-AREA-ADESIONE TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), WadeAreaAdesioneLccs0005.Len.WADE_AREA_ADESIONE);
            // COB_CODE: MOVE WADE-AREA-ADESIONE
            //             TO S211-BUFFER-DATI
            //               (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(wadeAreaAdesione.getWadeAreaAdesioneFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WDAD-ELE-DEF-ADES-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wdadAreaDefAdes.getWdadEleDefAdesMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 3 AREA DEFAULT ADESIONE
            // COB_CODE: MOVE S211-ALIAS-DFLT-ADES         TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasDfltAdes());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF WDAD-AREA-DEF-ADES TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), WdadAreaDefAdes.Len.WDAD_AREA_DEF_ADES);
            // COB_CODE: MOVE WDAD-AREA-DEF-ADES
            //              TO S211-BUFFER-DATI
            //                (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(wdadAreaDefAdes.getWdadAreaDefAdesFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WDCO-ELE-COLL-MAX > 0
        //                 (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wdcoAreaDtCollettiva.getWdcoEleCollMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //--> OCCORRENZA 4 AREA DATI COLLETTIVA
            // COB_CODE: MOVE S211-ALIAS-DT-COLL           TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasDtColl());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF WDCO-AREA-DT-COLLETTIVA
            //                                             TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), WdcoAreaDtCollettiva.Len.WDCO_AREA_DT_COLLETTIVA);
            // COB_CODE: MOVE WDCO-AREA-DT-COLLETTIVA
            //             TO S211-BUFFER-DATI
            //               (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(wdcoAreaDtCollettiva.getWdcoAreaDtCollettivaFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WRAN-ELE-RAPP-ANAG-MAX > 0
        //                 (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wranAreaRappAnag.getEleRappAnagMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 5 RAPPORTO ANAGRAFICO
            // COB_CODE: MOVE S211-ALIAS-RAPP-ANAG         TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasRappAnag());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VRAN-AREA-RAPP-ANAG
            //                                             TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VRAN_AREA_RAPP_ANAG);
            // COB_CODE: MOVE VRAN-AREA-RAPP-ANAG
            //             TO S211-BUFFER-DATI
            //               (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVranAreaRappAnagFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WDFA-ELE-FISC-ADES-MAX > 0
        //                 (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wdfaAreaDtFiscAdes.getWdfaEleFiscAdesMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 6 DATO FISCALE ADESIONE
            // COB_CODE: MOVE S211-ALIAS-DT-FISC-ADES      TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasDtFiscAdes());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VDFA-TAB-FISC-ADES TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VDFA_TAB_FISC_ADES);
            // COB_CODE: MOVE VDFA-TAB-FISC-ADES
            //             TO S211-BUFFER-DATI
            //               (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVdfaTabFiscAdesFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WGRZ-ELE-GARANZIA-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wgrzAreaGaranzia.getEleGarMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 7 AREA GARANZIA
            // COB_CODE: MOVE S211-ALIAS-GARANZIA          TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasGaranzia());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VGRZ-AREA-GARANZIA TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VGRZ_AREA_GARANZIA);
            // COB_CODE: MOVE VGRZ-AREA-GARANZIA
            //              TO S211-BUFFER-DATI
            //                (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVgrzAreaGaranziaFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        //    IF WTGA-ELE-TRAN-MAX > 0
        // COB_CODE: IF VTGA-ELE-TRAN-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (ws.getVtgaEleTranMax() > 0) {
            // COB_CODE: PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
            //             UNTIL IX-TAB-TGA > VTGA-ELE-TRAN-MAX
            //                                 VTGA-PRSTZ-INI-NULL(IX-TAB-TGA)
            //           END-PERFORM
            ws.getIxIndici().setTabTga(((short)1));
            while (!(ws.getIxIndici().getTabTga() > ws.getVtgaEleTranMax())) {
                // COB_CODE: MOVE HIGH-VALUES TO VTGA-PRE-INI-NET-NULL(IX-TAB-TGA)
                //                               VTGA-PRSTZ-INI-NULL(IX-TAB-TGA)
                ws.getVtgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreIniNet.Len.WTGA_PRE_INI_NET_NULL));
                ws.getVtgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPrstzIni.Len.WTGA_PRSTZ_INI_NULL));
                ws.getIxIndici().setTabTga(Trunc.toShort(ws.getIxIndici().getTabTga() + 1, 4));
            }
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                            TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->    OCCORRENZA 8 AREA TRANCHE DI GARANZIA
            // COB_CODE: MOVE S211-ALIAS-TRCH-GAR         TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasTrchGar());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VTGA-AREA-TRANCHE TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VTGA_AREA_TRANCHE);
            // COB_CODE: MOVE VTGA-AREA-TRANCHE
            //             TO S211-BUFFER-DATI
            //               (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVtgaAreaTrancheFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WSPG-ELE-SOPR-GAR-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wspgAreaSoprGar.getEleSoprGarMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                            TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->    OCCORRENZA 9 AREA SOPRAPREMIO DI GAR
            // COB_CODE: MOVE S211-ALIAS-SOPRAP-GAR       TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasSoprapGar());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VSPG-AREA-SOPR-GAR
            //                                            TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VSPG_AREA_SOPR_GAR);
            // COB_CODE: MOVE VSPG-AREA-SOPR-GAR
            //             TO S211-BUFFER-DATI
            //               (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVspgAreaSoprGarFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WSDI-ELE-STRA-INV-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wsdiAreaStraInv.getEleStraInvMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                            TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->    OCCORRENZA 10 AREA STRATEGIA D'INVESTIMENTO
            // COB_CODE: MOVE S211-ALIAS-STRA-INV         TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasStraInv());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VSDI-AREA-STRA-INV
            //                                            TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VSDI_AREA_STRA_INV);
            // COB_CODE: MOVE VSDI-AREA-STRA-INV
            //             TO S211-BUFFER-DATI
            //               (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVsdiAreaStraInvFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WPMO-ELE-PARAM-MOV-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wpmoAreaParamMov.getEleParamMovMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                            TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->    OCCORRENZA 13 AREA PARAMETRO MOVIMENTO
            // COB_CODE: MOVE S211-ALIAS-PARAM-MOV        TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasParamMov());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VPMO-AREA-PARAM-MOV
            //                                            TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VPMO_AREA_PARAM_MOV);
            // COB_CODE: MOVE VPMO-AREA-PARAM-MOV
            //             TO S211-BUFFER-DATI
            //               (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVpmoAreaParamMovFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WPOG-ELE-PARAM-OGG-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wpogAreaParamOgg.getEleParamOggMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 14 AREA PARAMETRO OGGETTO
            // COB_CODE: MOVE S211-ALIAS-PARAM-OGG         TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasParamOgg());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VPOG-AREA-PARAM-OGG
            //                                             TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VPOG_AREA_PARAM_OGG);
            // COB_CODE: MOVE VPOG-AREA-PARAM-OGG
            //             TO S211-BUFFER-DATI
            //                (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVpogAreaParamOggFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WBEP-ELE-BENEFICIARI > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wbepAreaBeneficiari.getEleBeneficiari() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 15 AREA BENEFICIARI
            // COB_CODE: MOVE S211-ALIAS-BENEF             TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasBenef());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VBEP-AREA-BENEFICIARI
            //                                             TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VBEP_AREA_BENEFICIARI);
            // COB_CODE: MOVE VBEP-AREA-BENEFICIARI
            //             TO S211-BUFFER-DATI
            //                (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVbepAreaBeneficiariFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WQUE-ELE-QUEST-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wqueAreaQuest.getEleQuestMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 16 AREA QUESTIONARIO
            // COB_CODE: MOVE S211-ALIAS-QUEST             TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasQuest());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VQUE-AREA-QUEST    TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VQUE_AREA_QUEST);
            // COB_CODE: MOVE VQUE-AREA-QUEST
            //             TO S211-BUFFER-DATI
            //                (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVqueAreaQuestFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WDEQ-ELE-DETT-QUEST-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wdeqAreaDettQuest.getEleDettQuestMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 17 AREA DETTAGLIO QUESTIONARIO
            // COB_CODE: MOVE S211-ALIAS-DETT-QUEST        TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasDettQuest());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VDEQ-AREA-DETT-QUEST
            //                                             TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VDEQ_AREA_DETT_QUEST);
            // COB_CODE: MOVE VDEQ-AREA-DETT-QUEST
            //             TO S211-BUFFER-DATI
            //                (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVdeqAreaDettQuestFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF VOCO-ELE-OGG-COLLG-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (ws.getVocoEleOggCollgMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 18 AREA OGGETTO COLLEGATO
            // COB_CODE: MOVE S211-ALIAS-OGG-COLL          TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasOggColl());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VOCO-AREA-OGG-COLLG
            //                                             TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VOCO_AREA_OGG_COLLG);
            // COB_CODE: MOVE VOCO-AREA-OGG-COLLG
            //             TO S211-BUFFER-DATI
            //                (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVocoAreaOggCollgFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WRRE-ELE-RAP-RETE-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wrreAreaRapRete.getEleRappReteMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 19 AREA RAPPORTO RETE
            // COB_CODE: MOVE S211-ALIAS-RAPP-RETE         TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasRappRete());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF WRRE-AREA-RAP-RETE TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), WrreAreaRappRete.Len.WRRE_AREA_RAPP_RETE);
            // COB_CODE: MOVE WRRE-AREA-RAP-RETE
            //             TO S211-BUFFER-DATI
            //                (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(wrreAreaRapRete.getWrreAreaRappReteFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF WL23-ELE-VINC-PEG-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wl23AreaVincPeg.getEleVincPegMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 19 AREA RAPPORTO RETE
            // COB_CODE: MOVE S211-ALIAS-VINC-PEGN         TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasVincPegn());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF WL23-AREA-VINC-PEG TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Wl23AreaVincPeg.Len.WL23_AREA_VINC_PEG);
            // COB_CODE: MOVE WL23-AREA-VINC-PEG
            //             TO S211-BUFFER-DATI
            //                (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(wl23AreaVincPeg.getWl23AreaVincPegFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: IF INCLU-SOVRAP
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (ws.getWsMovimento().isIncluSovrap()) {
            // COB_CODE: PERFORM  VARYING IX-SPG FROM 1 BY 1
            //             UNTIL IX-SPG >  WSPG-ELE-SOPR-GAR-MAX
            //                END-IF
            //           END-PERFORM
            ws.getIxIndici().setSpg(((short)1));
            while (!(ws.getIxIndici().getSpg() > wspgAreaSoprGar.getEleSoprGarMax())) {
                // COB_CODE: IF VSPG-ST-ADD(IX-SPG)
                //                TO IX-SPG
                //           END-IF
                if (ws.getVspgTabSpg(ws.getIxIndici().getSpg()).getLccvspg1().getStatus().isAdd()) {
                    // COB_CODE: MOVE VSPG-DT-INI-EFF(IX-SPG)
                    //             TO VMOV-DT-EFF
                    ws.getLccvmov1().getDati().setWmovDtEff(ws.getVspgTabSpg(ws.getIxIndici().getSpg()).getLccvspg1().getDati().getWspgDtIniEff());
                    // COB_CODE: MOVE VSPG-ELE-SOPR-GAR-MAX
                    //             TO IX-SPG
                    ws.getIxIndici().setSpg(ws.getVspgEleSoprGarMax());
                }
                ws.getIxIndici().setSpg(Trunc.toShort(ws.getIxIndici().getSpg() + 1, 4));
            }
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                             TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            //-->   OCCORRENZA 1 AREA MOVIMENTO
            // COB_CODE: MOVE S211-ALIAS-MOVIMENTO         TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasMovimento());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF VMOV-AREA-MOVIMENTO
            //                                             TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Lves0269Data.Len.VMOV_AREA_MOVIMENTO);
            // COB_CODE: MOVE VMOV-AREA-MOVIMENTO
            //             TO S211-BUFFER-DATI
            //                (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVmovAreaMovimentoFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        //--> OCCORRENZA 1 AREA VARIABILI CONTESTO
        // COB_CODE: IF WCNT-ELE-VAR-CONT-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (ws.getIvvc0212().getWcntEleVarContMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                            TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            // COB_CODE: MOVE S211-ALIAS-DATI-CONTEST     TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasDatiContest());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF WCNT-AREA-VARIABILI-CONT
            //                                            TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Ivvc0212.Len.WCNT_AREA_VARIABILI_CONT);
            // COB_CODE: MOVE WCNT-AREA-VARIABILI-CONT
            //             TO S211-BUFFER-DATI
            //               (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getIvvc0212().getWcntAreaVariabiliContFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        //--> OCCORRENZA 1 AREA VARIABILI CONTESTO
        // COB_CODE: IF WP67-EST-POLI-CPI-PR-MAX > 0
        //                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
        //           END-IF.
        if (wp67AreaEstPoliCpiPr.getWp67EstPoliCpiPrMax() > 0) {
            // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
            //                                       S211-LUNGHEZZA(IX-MAX) + 1
            ws.getWkVariabili().setWkAppoLunghezza(Trunc.toInt(ws.getWkVariabili().getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()) + 1, 9));
            // COB_CODE: ADD 1                            TO IX-MAX
            ws.getIxIndici().setMax(Trunc.toShort(1 + ws.getIxIndici().getMax(), 4));
            // COB_CODE: MOVE S211-ALIAS-EST-POLI-CPI-PR  TO S211-TAB-ALIAS(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(ws.getIxIndici().getMax(), ws.getIvvc0218().getAliasEstPoliCpiPr());
            // COB_CODE: MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(ws.getIxIndici().getMax(), ws.getWkVariabili().getWkAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF WP67-AREA-EST-POLI-CPI-PR
            //                                            TO S211-LUNGHEZZA(IX-MAX)
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(ws.getIxIndici().getMax(), Wp67AreaEstPoliCpiPr.Len.WP67_AREA_EST_POLI_CPI_PR);
            // COB_CODE: MOVE WP67-AREA-EST-POLI-CPI-PR
            //             TO S211-BUFFER-DATI
            //               (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(wp67AreaEstPoliCpiPr.getWp67AreaEstPoliCpiPrFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getMax()), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getMax()));
        }
        // COB_CODE: MOVE  IX-MAX          TO S211-ELE-INFO-MAX.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setEleInfoMax(ws.getIxIndici().getMax());
    }

    /**Original name: PREP-TAB-LCCC0490<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPAREA AREA TABELLA GARANZIA DEL MODULO CALCOLO RATEO
	 * ----------------------------------------------------------------*</pre>*/
    private void prepTabLccc0490() {
        // COB_CODE: ADD 1 TO IX-TAB-LCCC0490.
        ws.getIxIndici().setTabLccc0490(Trunc.toShort(1 + ws.getIxIndici().getTabLccc0490(), 4));
        // COB_CODE: MOVE WGRZ-COD-TARI(IX-RIC-GAR)
        //             TO LCCC0490-COD-TARI(IX-TAB-LCCC0490).
        ws.getLccc0490().getTabGrz(ws.getIxIndici().getTabLccc0490()).setCodTari(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getRicGar()).getLccvgrz1().getDati().getWgrzCodTari());
        // COB_CODE: MOVE WGRZ-TP-GAR(IX-RIC-GAR)
        //             TO LCCC0490-TP-GAR(IX-TAB-LCCC0490).
        ws.getLccc0490().getTabGrz(ws.getIxIndici().getTabLccc0490()).setLccc0490TpGar(TruncAbs.toShort(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getRicGar()).getLccvgrz1().getDati().getWgrzTpGar().getWgrzTpGar(), 1));
        // COB_CODE: MOVE WK-FRAZ-MM
        //             TO LCCC0490-FRAZIONAMENTO(IX-TAB-LCCC0490).
        ws.getLccc0490().getTabGrz(ws.getIxIndici().getTabLccc0490()).setLccc0490FrazionamentoFormatted(ws.getWkVariabili().getWkFrazMm().getFrazMmFormatted());
        // COB_CODE: MOVE WGRZ-TP-PER-PRE(IX-RIC-GAR)
        //             TO LCCC0490-TP-PER-PRE(IX-TAB-LCCC0490).
        ws.getLccc0490().getTabGrz(ws.getIxIndici().getTabLccc0490()).setLccc0490TpPerPreFormatted(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getRicGar()).getLccvgrz1().getDati().getWgrzTpPerPreFormatted());
        // COB_CODE: MOVE WGRZ-DT-DECOR(IX-RIC-GAR)
        //             TO LCCC0490-DT-DECOR(IX-TAB-LCCC0490).
        ws.getLccc0490().getTabGrz(ws.getIxIndici().getTabLccc0490()).setLccc0490DtDecor(TruncAbs.toInt(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getRicGar()).getLccvgrz1().getDati().getWgrzDtDecor().getWgrzDtDecor(), 8));
        // COB_CODE: EVALUATE TRUE
        //            WHEN QUIETANZAMENTO-SI
        //                TO LCCC0490-TIPO-RICORRENZA(IX-TAB-LCCC0490)
        //            WHEN GETRA-SI
        //                TO LCCC0490-TIPO-RICORRENZA(IX-TAB-LCCC0490)
        //           END-EVALUATE.
        if (ws.isWkQuietanzamento()) {
            // COB_CODE: MOVE 'Q'
            //             TO LCCC0490-TIPO-RICORRENZA(IX-TAB-LCCC0490)
            ws.getLccc0490().getTabGrz(ws.getIxIndici().getTabLccc0490()).setLccc0490TipoRicorrenzaFormatted("Q");
        }
        else if (ws.isWkGetra()) {
            // COB_CODE: MOVE 'G'
            //             TO LCCC0490-TIPO-RICORRENZA(IX-TAB-LCCC0490)
            ws.getLccc0490().getTabGrz(ws.getIxIndici().getTabLccc0490()).setLccc0490TipoRicorrenzaFormatted("G");
        }
    }

    /**Original name: S12000-PREPARA-AREA-CALC-CONTR<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPAREA AREA SERVIZIO CALCOLI E CONTROLLI ISPC0140
	 * ----------------------------------------------------------------*</pre>*/
    private void s12000PreparaAreaCalcContr() {
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO ISPC0140-COD-COMPAGNIA.
        ws.getAreaIoCalcContr().getDatiInput().setIspc0140CodCompagniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        // COB_CODE: MOVE WPOL-COD-PROD
        //             TO ISPC0140-COD-PRODOTTO.
        ws.getAreaIoCalcContr().getDatiInput().setCodProdotto(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodProd());
        // COB_CODE: MOVE WSKD-DEE                     TO ISPC0140-DEE.
        ws.getAreaIoCalcContr().getDatiInput().setDee(ws.getIvvc0216().getWskdDee());
        // COB_CODE: IF WPOL-COD-CONV-NULL = HIGH-VALUES
        //              MOVE SPACES       TO ISPC0140-COD-CONVENZIONE
        //           ELSE
        //                TO ISPC0140-COD-CONVENZIONE
        //           END-IF.
        if (Characters.EQ_HIGH.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodConvFormatted())) {
            // COB_CODE: MOVE SPACES       TO ISPC0140-COD-CONVENZIONE
            ws.getAreaIoCalcContr().getDatiInput().setCodConvenzione("");
        }
        else {
            // COB_CODE: MOVE WPOL-COD-CONV
            //             TO ISPC0140-COD-CONVENZIONE
            ws.getAreaIoCalcContr().getDatiInput().setCodConvenzione(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodConv());
        }
        // COB_CODE: IF WPOL-DT-INI-VLDT-CONV-NULL = HIGH-VALUE
        //              MOVE SPACES          TO ISPC0140-DATA-INIZ-VALID-CONV
        //           ELSE
        //                                   TO ISPC0140-DATA-INIZ-VALID-CONV
        //           END-IF.
        if (Characters.EQ_HIGH.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtIniVldtConv().getWpolDtIniVldtConvNullFormatted())) {
            // COB_CODE: MOVE SPACES          TO ISPC0140-DATA-INIZ-VALID-CONV
            ws.getAreaIoCalcContr().getDatiInput().setDataInizValidConv("");
        }
        else {
            // COB_CODE: MOVE WPOL-DT-INI-VLDT-CONV
            //                                TO ISPC0140-DATA-INIZ-VALID-CONV
            ws.getAreaIoCalcContr().getDatiInput().setDataInizValidConv(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtIniVldtConv().getWpolDtIniVldtConvFormatted());
        }
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: IF WPOL-FL-VER-PROD = 'E'
        //              MOVE WPOL-DT-INI-VLDT-PROD    TO ISPC0140-DATA-RIFERIMENTO
        //           ELSE
        //              END-IF
        //           END-IF.
        if (Conditions.eq(wpolAreaPolizza.getLccvpol1().getDati().getWpolFlVerProd(), "E")) {
            // COB_CODE: MOVE WPOL-DT-INI-VLDT-PROD    TO ISPC0140-DATA-RIFERIMENTO
            ws.getAreaIoCalcContr().getDatiInput().setDataRiferimento(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtIniVldtProdFormatted());
        }
        else if (Conditions.eq(wpolAreaPolizza.getLccvpol1().getDati().getWpolFlVerProd(), "U")) {
            // COB_CODE: IF WPOL-FL-VER-PROD = 'U'
            //              MOVE WCOM-DT-ULT-VERS-PROD TO ISPC0140-DATA-RIFERIMENTO
            //           END-IF
            // COB_CODE: MOVE WCOM-DT-ULT-VERS-PROD TO ISPC0140-DATA-RIFERIMENTO
            ws.getAreaIoCalcContr().getDatiInput().setDataRiferimento(lccc0001.getDtUltVersProdFormatted());
        }
        // COB_CODE: MOVE WCOM-COD-LIV-AUT-PROFIL
        //             TO ISPC0140-LIVELLO-UTENTE.
        ws.getAreaIoCalcContr().getDatiInput().setIspc0140LivelloUtente(TruncAbs.toShort(lccc0001.getDatiDeroghe().getCodLivAutProfil(), 2));
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //             TO ISPC0140-SESSION-ID.
        ws.getAreaIoCalcContr().getDatiInput().setSessionId(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE:      IF  VERSAM-AGGIUNTIVO    OR
        //                    VERSAM-AGGIUNTIVO-REINV
        //                      TO ISPC0140-DATA-DECORR-POLIZZA
        //                ELSE
        //           *    IF WGRZ-DT-DECOR-NULL(1) NOT = HIGH-VALUES
        //           *       MOVE WGRZ-DT-DECOR(1)
        //                 END-IF
        //                END-IF.
        if (ws.getWsMovimento().isVersamAggiuntivo() || ws.getWsMovimento().isVersamAggiuntivoReinv()) {
            // COB_CODE: MOVE WPOL-DT-DECOR
            //             TO ISPC0140-DATA-DECORR-POLIZZA
            ws.getAreaIoCalcContr().getDatiInput().setDataDecorrPolizza(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtDecorFormatted());
        }
        else if (!Characters.EQ_HIGH.test(ws.getVgrzTabGar(1).getLccvgrz1().getDati().getWgrzDtDecor().getVgrzDtDecorNullFormatted())) {
            //    IF WGRZ-DT-DECOR-NULL(1) NOT = HIGH-VALUES
            //       MOVE WGRZ-DT-DECOR(1)
            // COB_CODE: IF VGRZ-DT-DECOR-NULL(1) NOT = HIGH-VALUES
            //               TO ISPC0140-DATA-DECORR-POLIZZA
            //           END-IF
            // COB_CODE: MOVE VGRZ-DT-DECOR(1)
            //             TO ISPC0140-DATA-DECORR-POLIZZA
            ws.getAreaIoCalcContr().getDatiInput().setDataDecorrPolizza(ws.getVgrzTabGar(1).getLccvgrz1().getDati().getWgrzDtDecor().getWgrzDtDecorFormatted());
        }
        // COB_CODE: MOVE 'G'      TO ISPC0140-MODALITA-CALCOLO.
        ws.getAreaIoCalcContr().getDatiInput().setIspc0140ModalitaCalcoloFormatted("G");
        //--> CHIAMATA AL SERVIZIO DI TRASCODIFICA DEL TIPO-MOVIM-PTF CON
        //--> TIPO-MOVIM-ACTUATOR
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //             TO LCCV0021-TP-MOV-PTF.
        ws.getLccv0021().setTpMovPtfFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: PERFORM CALL-MATR-MOVIMENTO
        //              THRU CALL-MATR-MOVIMENTO-EX.
        callMatrMovimento();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                TO ISPC0140-FUNZIONALITA
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE LCCV0021-TP-MOV-ACT
            //             TO ISPC0140-FUNZIONALITA
            ws.getAreaIoCalcContr().getDatiInput().setIspc0140FunzionalitaFormatted(ws.getLccv0021().getAreaOutput().getLccv0021TpMovActFormatted());
        }
        //--> VALORIZZAZIONE OCCURS AREA INPUT SERVIZIO CALCOLI E CONTROLLI
        //--> CON L'OUTPUT DEL VALORIZZATORE VARIABILI
        // COB_CODE: PERFORM VAL-SCHEDE-ISPC0140
        //              THRU VAL-SCHEDE-ISPC0140-EX.
        valSchedeIspc0140();
        //-->    ROUTINE VALORIZZA AREA DATI PAGINA
        // COB_CODE: PERFORM S14000-VAL-AREA-PAGINA-P
        //              THRU EX-S14000
        //           VARYING IX-AREA-ISPC0140 FROM 1 BY 1
        //             UNTIL IX-AREA-ISPC0140 > ISPC0140-DATI-NUM-MAX-ELE-P
        ws.getIxIndici().setAreaIspc0140(((short)1));
        while (!(ws.getIxIndici().getAreaIspc0140() > ws.getAreaIoCalcContr().getDatiNumMaxEleP())) {
            s14000ValAreaPaginaP();
            ws.getIxIndici().setAreaIspc0140(Trunc.toShort(ws.getIxIndici().getAreaIspc0140() + 1, 4));
        }
        // COB_CODE:  PERFORM S14005-VAL-AREA-PAGINA-T
        //               THRU EX-S14005
        //            VARYING IX-AREA-ISPC0140 FROM 1 BY 1
        //           UNTIL IX-AREA-ISPC0140 > ISPC0140-DATI-NUM-MAX-ELE-T.
        ws.getIxIndici().setAreaIspc0140(((short)1));
        while (!(ws.getIxIndici().getAreaIspc0140() > ws.getAreaIoCalcContr().getDatiNumMaxEleT())) {
            s14005ValAreaPaginaT();
            ws.getIxIndici().setAreaIspc0140(Trunc.toShort(ws.getIxIndici().getAreaIspc0140() + 1, 4));
        }
    }

    /**Original name: S12500-CALL-CALC-CONTR<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICHIAMA IL SERVIZIO CALCOLI E CONTROLLI
	 * ----------------------------------------------------------------*
	 *     MOVE LENGTH OF AREA-IO-CALC-CONTR
	 *       TO IJCCMQ01-LENGTH-DATI-SERVIZIO.
	 *     MOVE AREA-IO-CALC-CONTR
	 *       TO IJCCMQ01-AREA-DATI-SERVIZIO.
	 *     SET WS-ADDRESS TO  ADDRESS OF AREA-PRODUCT-SERVICES.
	 *     MOVE IDSV0001-LIVELLO-DEBUG   TO IJCCMQ00-LIVELLO-DEBUG
	 *     MOVE IDSV0001-AREA-ADDRESSES  TO IJCCMQ01-AREA-ADDRESSES
	 *     MOVE IDSV0001-USER-NAME       TO IJCCMQ01-USER-NAME
	 *     CALL INTERF-MQSERIES USING MQ01-ADDRESS
	 *                                AREA-PRODUCT-SERVICES</pre>*/
    private void s12500CallCalcContr() {
        Isps0140 isps0140 = null;
        // COB_CODE: CALL ISPS0140 USING AREA-IDSV0001
        //                               WCOM-AREA-STATI
        //                               AREA-IO-CALC-CONTR
        //           ON EXCEPTION
        //                  THRU EX-S0290
        //           END-CALL.
        try {
            isps0140 = Isps0140.getInstance();
            isps0140.run(areaIdsv0001, lccc0001, ws.getAreaIoCalcContr());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO CALCOLI E CONTROLLI' TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO CALCOLI E CONTROLLI");
            // COB_CODE: MOVE 'S12500-CALL-CALC-CONTR'       TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S12500-CALL-CALC-CONTR");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: S14000-VAL-AREA-PAGINA-P<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELLA AREA PAGINA
	 * ----------------------------------------------------------------*</pre>*/
    private void s14000ValAreaPaginaP() {
        // COB_CODE: IF ISPC0140-O-TIPO-LIVELLO-P(IX-AREA-ISPC0140) = 'P'
        //                 THRU S14050-EX
        //           END-IF.
        if (ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getoTipoLivelloP() == 'P') {
            // COB_CODE: PERFORM S14050-GEST-COMP-PRODOTTO
            //              THRU S14050-EX
            s14050GestCompProdotto();
        }
    }

    /**Original name: S14005-VAL-AREA-PAGINA-T<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELLA AREA PAGINA
	 * ----------------------------------------------------------------*</pre>*/
    private void s14005ValAreaPaginaT() {
        // COB_CODE: IF ISPC0140-O-TIPO-LIVELLO-T(IX-AREA-ISPC0140) = 'G'
        //                 THRU S14025-EX
        //           END-IF.
        if (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getoTipoLivelloT() == 'G') {
            // COB_CODE: PERFORM S14025-GEST-COMP-GARANZIA
            //              THRU S14025-EX
            s14025GestCompGaranzia();
        }
    }

    /**Original name: S14025-GEST-COMP-GARANZIA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELLA AREA PAGINA E DELLA DCLGEN
	 *     TRANCHE DI GARANZIA PER PREMIO ANNUO
	 * ----------------------------------------------------------------*
	 * --> PER L'EMISSIONE PROPOSTA INDIVIDUALE:
	 * --> SOLO PER LA GARANZIA BASE
	 * --> SE E' PRESENTE LA COMPONENTE 'PRESTNEG' OLTRE ALLA TRANCHE
	 * --> POSITIVA SARA' SEMPRE ASSOCIATA UNA TRANCHE DI TIPO NEGATIVO
	 * --> CON TIPO TRANCHE = 9(NEGATIVA PRELIEVO COSTI).
	 * --> L'AREA DI PAGINA E' STATA MODIFICATA PER PREVEDERE 2 TRANCHE
	 * --> PER OGNI GARANZIA ELABORATA
	 *     SET QUESTSAN-NO TRCH-NEG-NO TO TRUE</pre>*/
    private void s14025GestCompGaranzia() {
        // COB_CODE: SET  TRCH-NEG-NO            TO TRUE
        ws.getWkFlgTrch().setNo();
        // COB_CODE: SET  FLAG-RATALO-NO         TO TRUE
        ws.getWkFlagRatalo().setNo();
        // COB_CODE: MOVE SPACES                 TO WS-TP-TRCH
        ws.setWsTpTrch("");
        // COB_CODE: MOVE ZEROES                 TO IX-TAB-PAR
        //                                          IX-TAB-SPG
        ws.getIxIndici().setTabPar(((short)0));
        ws.getIxIndici().setTabSpg(((short)0));
        // COB_CODE: MOVE 1                      TO IX-TAB-TGA
        ws.getIxIndici().setTabTga(((short)1));
        // COB_CODE: code not available
        ws.setIntRegister1(((short)1));
        ws.getIxIndici().setAreaPagina(Trunc.toShort(ws.getIntRegister1() + ws.getIxIndici().getAreaPagina(), 4));
        wpagAreaPagina.getDatiOuput().setWpagEleCalcoliMax(Trunc.toShort(ws.getIntRegister1() + wpagAreaPagina.getDatiOuput().getWpagEleCalcoliMax(), 4));
        // COB_CODE: MOVE ISPC0140-O-CODICE-LIVELLO-T(IX-AREA-ISPC0140)
        //             TO WPAG-COD-GARANZIA(IX-AREA-PAGINA)
        //                WK-COD-GAR(IX-AREA-PAGINA)
        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).setWpagCodGaranzia(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getoCodiceLivelloT());
        ws.getWkVariabili().getWkTabGar(ws.getIxIndici().getAreaPagina()).setCodGar(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getoCodiceLivelloT());
        //--  CONTROLLO SU TIPO PREMIO (ANNUO-RATA-FIRMA)
        // COB_CODE: PERFORM VARYING IX-TP-PREMIO FROM 1 BY 1
        //                     UNTIL IX-TP-PREMIO > ISPC0140-PRE-NUM-MAX-ELE-T
        //                                         (IX-AREA-ISPC0140)
        //             END-EVALUATE
        //           END-PERFORM.
        ws.getIxIndici().setTpPremio(((short)1));
        while (!(ws.getIxIndici().getTpPremio() > ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getIspc0140PreNumMaxEleT())) {
            // COB_CODE: EVALUATE ISPC0140-CODICE-TP-PREMIO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO)
            //             WHEN 'ANNUO'
            //                  THRU EX-S14180
            //             WHEN 'RATA'
            //                                   (IX-AREA-ISPC0140, IX-TP-PREMIO)
            //             WHEN 'FIRMA'
            //                                   (IX-AREA-ISPC0140, IX-TP-PREMIO)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getCodiceTpPremioT().getCodiceTpPremioT()) {

                case "ANNUO":// COB_CODE: PERFORM S14100-PREMIO-ANNUO
                    //              THRU EX-S14100
                    //           VARYING IX-COMPONENTE FROM 1 BY 1
                    //             UNTIL IX-COMPONENTE > ISPC0140-O-NUM-COMP-MAX-ELE-T
                    //                               (IX-AREA-ISPC0140, IX-TP-PREMIO)
                    ws.getIxIndici().setComponente(((short)1));
                    while (!(ws.getIxIndici().getComponente() > ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getIspc0140ONumCompMaxEleT())) {
                        s14100PremioAnnuo();
                        ws.getIxIndici().setComponente(Trunc.toShort(ws.getIxIndici().getComponente() + 1, 4));
                    }
                    // COB_CODE: IF FLAG-RATALO-SI
                    //              CONTINUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    if (ws.getWkFlagRatalo().isSi()) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: SET FLAG-GAR-NO TO TRUE
                        ws.getWkFlagGar().setNo();
                        // COB_CODE: MOVE ZEROES     TO WS-TP-GAR
                        ws.setWsTpGar(((short)0));
                        // COB_CODE: PERFORM VARYING IX-IND-GAR FROM 1 BY 1
                        //             UNTIL IX-IND-GAR > WGRZ-ELE-GARANZIA-MAX
                        //                OR FLAG-GAR-SI
                        //                END-IF
                        //           END-PERFORM
                        ws.getIxIndici().setIndGar(((short)1));
                        while (!(ws.getIxIndici().getIndGar() > wgrzAreaGaranzia.getEleGarMax() || ws.getWkFlagGar().isSi())) {
                            // COB_CODE: IF NOT WGRZ-ST-DEL(IX-IND-GAR)
                            //              END-IF
                            //           END-IF
                            if (!wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getIndGar()).getLccvgrz1().getStatus().isDel()) {
                                // COB_CODE: IF WGRZ-COD-TARI(IX-IND-GAR) =
                                //              WPAG-COD-GARANZIA(IX-AREA-PAGINA)
                                //              SET FLAG-GAR-SI TO TRUE
                                //           END-IF
                                if (Conditions.eq(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getIndGar()).getLccvgrz1().getDati().getWgrzCodTari(), wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagCodGaranzia())) {
                                    // COB_CODE: MOVE WGRZ-TP-GAR(IX-IND-GAR)
                                    //             TO WS-TP-GAR
                                    ws.setWsTpGar(TruncAbs.toShort(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getIndGar()).getLccvgrz1().getDati().getWgrzTpGar().getWgrzTpGar(), 2));
                                    // COB_CODE: SET FLAG-GAR-SI TO TRUE
                                    ws.getWkFlagGar().setSi();
                                }
                            }
                            ws.getIxIndici().setIndGar(Trunc.toShort(ws.getIxIndici().getIndGar() + 1, 4));
                        }
                        // COB_CODE: IF FLAG-GAR-SI
                        //            AND WS-TP-GAR = 1 OR 2
                        //            AND WS-TP-TRCH = '01'
                        //                TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                        //           END-IF
                        if (ws.getWkFlagGar().isSi() && ws.getWsTpGar() == 1 || ws.getWsTpGar() == 2 && Conditions.eq(ws.getWsTpTrch(), "01")) {
                            // COB_CODE: MOVE WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                            //             TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagRatLrd().setWpagRatLrd(Trunc.toDecimal(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreLrd().getWpagPreLrd(), 15, 3));
                        }
                    }
                    //--        SCRITTURA DELLA WPAG PER LA TRANCHE NEGATIVA
                    // COB_CODE: PERFORM S14180-VAL-TRCH-NEGATIVA
                    //              THRU EX-S14180
                    s14180ValTrchNegativa();
                    break;

                case "RATA":// COB_CODE: PERFORM S14200-PREMIO-RATA
                    //              THRU EX-S14200
                    //           VARYING IX-COMPONENTE FROM 1 BY 1
                    //             UNTIL IX-COMPONENTE > ISPC0140-O-NUM-COMP-MAX-ELE-T
                    //                               (IX-AREA-ISPC0140, IX-TP-PREMIO)
                    ws.getIxIndici().setComponente(((short)1));
                    while (!(ws.getIxIndici().getComponente() > ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getIspc0140ONumCompMaxEleT())) {
                        s14200PremioRata();
                        ws.getIxIndici().setComponente(Trunc.toShort(ws.getIxIndici().getComponente() + 1, 4));
                    }
                    break;

                case "FIRMA":// COB_CODE: PERFORM S14300-PREMIO-FIRMA
                    //              THRU EX-S14300
                    //           VARYING IX-COMPONENTE FROM 1 BY 1
                    //             UNTIL IX-COMPONENTE > ISPC0140-O-NUM-COMP-MAX-ELE-T
                    //                               (IX-AREA-ISPC0140, IX-TP-PREMIO)
                    ws.getIxIndici().setComponente(((short)1));
                    while (!(ws.getIxIndici().getComponente() > ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getIspc0140ONumCompMaxEleT())) {
                        s14300PremioFirma();
                        ws.getIxIndici().setComponente(Trunc.toShort(ws.getIxIndici().getComponente() + 1, 4));
                    }
                    break;

                default:break;
            }
            ws.getIxIndici().setTpPremio(Trunc.toShort(ws.getIxIndici().getTpPremio() + 1, 4));
        }
    }

    /**Original name: S14050-GEST-COMP-PRODOTTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     Gestione delle componenti di ACTUATOR generale a livello
	 *     di prodotto
	 * ----------------------------------------------------------------*
	 * --  CONTROLLO SU TIPO PREMIO (ANNUO-RATA-FIRMA)</pre>*/
    private void s14050GestCompProdotto() {
        // COB_CODE: PERFORM VARYING IX-TP-PREMIO FROM 1 BY 1
        //                     UNTIL IX-TP-PREMIO > ISPC0140-PRE-NUM-MAX-ELE-P
        //                                         (IX-AREA-ISPC0140)
        //             END-EVALUATE
        //           END-PERFORM.
        ws.getIxIndici().setTpPremio(((short)1));
        while (!(ws.getIxIndici().getTpPremio() > ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getIspc0140PreNumMaxEleP())) {
            // COB_CODE: EVALUATE ISPC0140-CODICE-TP-PREMIO-P
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO)
            //             WHEN 'ANNUO'
            //                                   (IX-AREA-ISPC0140, IX-TP-PREMIO)
            //             WHEN 'RATA'
            //             WHEN 'FIRMA'
            //                CONTINUE
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getCodiceTpPremioP().getCodiceTpPremioP()) {

                case "ANNUO":// COB_CODE: PERFORM S14052-PREMIO-ANNUO-PROD
                    //              THRU S14052-EX
                    //           VARYING IX-COMPONENTE FROM 1 BY 1
                    //             UNTIL IX-COMPONENTE > ISPC0140-O-NUM-COMP-MAX-ELE-P
                    //                               (IX-AREA-ISPC0140, IX-TP-PREMIO)
                    ws.getIxIndici().setComponente(((short)1));
                    while (!(ws.getIxIndici().getComponente() > ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getIspc0140ONumCompMaxEleP())) {
                        s14052PremioAnnuoProd();
                        ws.getIxIndici().setComponente(Trunc.toShort(ws.getIxIndici().getComponente() + 1, 4));
                    }
                    break;

                case "RATA":
                case "FIRMA":// COB_CODE: CONTINUE
                //continue
                    break;

                default:break;
            }
            ws.getIxIndici().setTpPremio(Trunc.toShort(ws.getIxIndici().getTpPremio() + 1, 4));
        }
        // COB_CODE: EVALUATE TRUE
        //             WHEN QUESTSAN-SI
        //               SET WPAG-FL-PARAM-QUESTSAN-SI     TO TRUE
        //             WHEN QUESTSAN-SPECIALE
        //               SET WPAG-FL-PARAM-QUESTSAN-NO     TO TRUE
        //             WHEN OTHER
        //               SET WPAG-FL-PARAM-QUESTSAN-NO     TO TRUE
        //           END-EVALUATE.
        switch (ws.getWkQuestsan().getWkQuestsan()) {

            case WkQuestsan.SI:// COB_CODE: SET WPAG-FL-QUESTSAN-SPECIALE-NO  TO TRUE
                wpagAreaPagina.getDatiOuput().getWpagParamQuestsanSpeciale().setNo();
                // COB_CODE: SET WPAG-FL-PARAM-QUESTSAN-SI     TO TRUE
                wpagAreaPagina.getDatiOuput().getWpagParamQuestsan().setParamQuestsanSi();
                break;

            case WkQuestsan.SPECIALE:// COB_CODE: SET WPAG-FL-QUESTSAN-SPECIALE-SI  TO TRUE
                wpagAreaPagina.getDatiOuput().getWpagParamQuestsanSpeciale().setSi();
                // COB_CODE: SET WPAG-FL-PARAM-QUESTSAN-NO     TO TRUE
                wpagAreaPagina.getDatiOuput().getWpagParamQuestsan().setParamQuestsanNo();
                break;

            default:// COB_CODE: SET WPAG-FL-QUESTSAN-SPECIALE-NO  TO TRUE
                wpagAreaPagina.getDatiOuput().getWpagParamQuestsanSpeciale().setNo();
                // COB_CODE: SET WPAG-FL-PARAM-QUESTSAN-NO     TO TRUE
                wpagAreaPagina.getDatiOuput().getWpagParamQuestsan().setParamQuestsanNo();
                break;
        }
        // COB_CODE: IF VMED-OBBLIGATORIA
        //              SET WPAG-FL-VISITA-MEDICA-OBB  TO TRUE
        //           END-IF.
        if (ws.getWkVisitaMedica().isObbligatoria()) {
            // COB_CODE: SET WPAG-FL-VISITA-MEDICA-OBB  TO TRUE
            wpagAreaPagina.getDatiOuput().getWpagParamQuestsan().setVisitaMedicaObb();
        }
    }

    /**Original name: S14052-PREMIO-ANNUO-PROD<br>
	 * <pre>----------------------------------------------------------------*
	 *     Gestione delle componenti relative al prodotto e premio
	 *     Annuo
	 * ----------------------------------------------------------------*</pre>*/
    private void s14052PremioAnnuoProd() {
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-P
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'TIPIAS'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteP(), "TIPIAS")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-P
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-TP-IAS-ADE
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoP()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-P
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WK-APPO-IAS
                    ws.getWkVariabili().getWkAppoIas().setWkAppoIas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoP().getIspc0140CompValoreImpP(), 18, 7));
                    // COB_CODE: MOVE WK-IAS-2
                    //             TO WPAG-TP-IAS-ADE
                    wpagAreaPagina.getDatiOuput().setWpagTpIasAde(ws.getWkVariabili().getWkAppoIas().getIas2Formatted());
                    break;

                default:break;
            }
        }
        //--> SE IL VALORE DEL QUESTSAN h ZERO ALLORA IL QUESTIONARIO h
        //--> AMMESSO
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-P
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'QUESTSAN'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteP(), "QUESTSAN")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-P
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //           WHEN 'N'
            //           WHEN 'I'
            //              END-IF
            //           WHEN 'S'
            //              END-IF
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoP()) {

                case 'N':
                case 'I':// COB_CODE: IF ISPC0140-COMP-VALORE-IMP-P
                    //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 1
                    //              SET  QUESTSAN-SI           TO TRUE
                    //           ELSE
                    //             END-IF
                    //           END-IF
                    if (ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoP().getIspc0140CompValoreImpP().compareTo(1) == 0) {
                        // COB_CODE: SET  QUESTSAN-SI           TO TRUE
                        ws.getWkQuestsan().setSi();
                    }
                    else if (ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoP().getIspc0140CompValoreImpP().compareTo(2) == 0) {
                        // COB_CODE: IF ISPC0140-COMP-VALORE-IMP-P
                        //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 2
                        //              SET  QUESTSAN-SPECIALE   TO TRUE
                        //           END-IF
                        // COB_CODE: SET  QUESTSAN-SPECIALE   TO TRUE
                        ws.getWkQuestsan().setSpeciale();
                    }
                    break;

                case 'S':// COB_CODE: IF ISPC0140-COMP-VALORE-STR-P
                    //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = '1'
                    //              SET  QUESTSAN-SI           TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoP().getIspc0140CompValoreStrP(), "1")) {
                        // COB_CODE: SET  QUESTSAN-SI           TO TRUE
                        ws.getWkQuestsan().setSi();
                    }
                    else if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoP().getIspc0140CompValoreStrP(), "2")) {
                        // COB_CODE:  IF ISPC0140-COMP-VALORE-STR-P
                        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = '2'
                        //               SET  QUESTSAN-SPECIALE  TO TRUE
                        //            END-IF
                        // COB_CODE: SET  QUESTSAN-SPECIALE  TO TRUE
                        ws.getWkQuestsan().setSpeciale();
                    }
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-P
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'VISITAMED'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteP(), "VISITAMED")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-P
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //           WHEN 'N'
            //           WHEN 'I'
            //                 TO WK-VISITA-MEDICA
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoP()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-P
                    //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //              TO WK-VISITA-MEDICA
                    ws.getWkVisitaMedica().setWkVisitaMedica(TruncAbs.toShort(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoP().getIspc0140CompValoreImpP(), 1));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-P
        //              (IX-AREA-ISPC0140, IX-TP-PREMIO,
        //               IX-COMPONENTE) = 'MODRICPROV'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteP(), "MODRICPROV")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-P
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                  TO WPAG-TP-MOD-PROV
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoP()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-P
                    //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-TP-MOD-PROV
                    wpagAreaPagina.getDatiOuput().setWpagTpModProvFormatted(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoP().getIspc0140CompValoreStrPFormatted());
                    break;

                default:break;
            }
        }
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // INIZIO SIR 11254 IN SEGUITO ALLA SEGNALAZIONE DI BNL
        // I CONTROLLI SUL TIPO IAS DI GARANZIA E ADESIONE DEVONO ESSERE
        // EFFETTUATI SOLO NEL CASO DI INCLUSIONE DI NUOVA GARANZIA
        // I DETT. DEI CONTROLLI  DA EFFETTUARE IN CASO DI INCLUSIONE
        // GARANZIA SARANNO DEFINITI SUCCESSIVAMENTE
        // COB_CODE:      IF  VERSAM-AGGIUNTIVO    OR
        //                   VERSAM-AGGIUNTIVO-REINV
        //           * REGRESSIONE DEL 04/12/2010 INIZIO ====>
        //                    SET WPAG-NO-MOD-IAS(WPAG-ELE-MAX-IAS) TO TRUE
        //                END-IF
        if (ws.getWsMovimento().isVersamAggiuntivo() || ws.getWsMovimento().isVersamAggiuntivoReinv()) {
            // REGRESSIONE DEL 04/12/2010 INIZIO ====>
            // COB_CODE: MOVE 1 TO WPAG-ELE-MAX-IAS
            wpagAreaIas.setEleMaxIas(((short)1));
            // REGRESSIONE DEL 04/12/2010 FINE   <====
            // COB_CODE: SET WPAG-NO-MOD-IAS(WPAG-ELE-MAX-IAS) TO TRUE
            wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).getModIas().setNoModIas();
        }
        // FINE SIR 11254
        // COB_CODE:      IF INCLU-GARANZ
        //           *    OR VERSAM-AGGIUNTIVO    OR
        //           *       VERSAM-AGGIUNTIVO-REINV
        //                   END-IF
        //                END-IF.
        if (ws.getWsMovimento().isIncluGaranz()) {
            //    OR VERSAM-AGGIUNTIVO    OR
            //       VERSAM-AGGIUNTIVO-REINV
            // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-P
            //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'TIPIAS'
            //              END-IF
            //           END-IF
            if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteP(), "TIPIAS")) {
                // COB_CODE: ADD  1                 TO WPAG-ELE-MAX-IAS
                wpagAreaIas.setEleMaxIas(Trunc.toShort(1 + wpagAreaIas.getEleMaxIas(), 4));
                // COB_CODE: MOVE WADE-ID-ADES      TO WPAG-ID-OGG(WPAG-ELE-MAX-IAS)
                wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).setIdOgg(wadeAreaAdesione.getLccvade1().getDati().getWadeIdAdes());
                // COB_CODE: MOVE 'AD'              TO WPAG-TP-OGG(WPAG-ELE-MAX-IAS)
                wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).setTpOgg("AD");
                // COB_CODE: MOVE WADE-TP-IAS
                //             TO WPAG-TP-IAS-OLD(WPAG-ELE-MAX-IAS)
                wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).setTpIasOld(wadeAreaAdesione.getLccvade1().getDati().getWadeTpIas());
                // COB_CODE: MOVE WPAG-TP-IAS-ADE
                //             TO WPAG-TP-IAS-NEW(WPAG-ELE-MAX-IAS)
                wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).setTpIasNew(wpagAreaPagina.getDatiOuput().getWpagTpIasAde());
                // COB_CODE: IF WPAG-TP-IAS-OLD(WPAG-ELE-MAX-IAS)  =
                //              WPAG-TP-IAS-NEW(WPAG-ELE-MAX-IAS)
                //              SET WPAG-NO-MOD-IAS(WPAG-ELE-MAX-IAS)      TO  TRUE
                //           ELSE
                //              SET WPAG-SI-MOD-IAS(WPAG-ELE-MAX-IAS)      TO  TRUE
                //           END-IF
                if (Conditions.eq(wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).getTpIasOld(), wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).getTpIasNew())) {
                    // COB_CODE: SET WPAG-NO-MOD-IAS(WPAG-ELE-MAX-IAS)      TO  TRUE
                    wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).getModIas().setNoModIas();
                }
                else {
                    // COB_CODE: SET WPAG-SI-MOD-IAS(WPAG-ELE-MAX-IAS)      TO  TRUE
                    wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).getModIas().setSiModIas();
                }
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-P
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //                                                            'INCAUTOGEN'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteP(), "INCAUTOGEN")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-P
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //                 TO WK-FL-INC-AUTOGEN
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoP()) {

                case 'S':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-P
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WK-FL-INC-AUTOGEN
                    ws.getWkVariabili().setWkFlIncAutogenFormatted(ws.getAreaIoCalcContr().getCalcoliP(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioP(ws.getIxIndici().getTpPremio()).getComponenteP(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoP().getIspc0140CompValoreStrPFormatted());
                    break;

                default:break;
            }
        }
    }

    /**Original name: S14100-PREMIO-ANNUO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELLA AREA PAGINA E DELLA DCLGEN
	 *     TRANCHE DI GARANZIA PER PREMIO ANNUO
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
	 * --> VALORIZZARE LA DCLGEN TRANCHE DI GARANZIA</pre>*/
    private void s14100PremioAnnuo() {
        // COB_CODE: PERFORM S14110-CONTR-COMP-TRANCHE
        //              THRU EX-S14110.
        s14110ContrCompTranche();
        //--> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
        //--> VALORIZZARE I DATI PARAMETRO CALCOLO DELL'AREA DI PAGINA
        // COB_CODE: PERFORM S14120-CONTR-COMP-PAR-CALC
        //              THRU EX-S14120.
        s14120ContrCompParCalc();
        //--> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
        //--> VALORIZZARE I DATI PROVVIGIONI DELL'AREA DI PAGINA
        // COB_CODE: PERFORM S14130-CONTR-COMP-PROVV
        //              THRU EX-S14130.
        s14130ContrCompProvv();
        //--> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
        //--> VALORIZZARE GLI ALTRI DATI DELL'AREA DI PAGINA
        // COB_CODE: PERFORM S14140-CONTR-COMP-ALTRI
        //              THRU EX-S14140.
        s14140ContrCompAltri();
        //--> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
        //--> VALORIZZARE I DATI PARAMETRO MOVIMENTO DELL'AREA DI PAGINA
        // COB_CODE: PERFORM S14145-CONTR-COMP-PAR-MOVIM
        //              THRU EX-S14145.
        s14145ContrCompParMovim();
        //--> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER VALORIZ.
        //--> I DATI SOPRAPPREMI DI GARANZIA DELL'AREA DI PAGINA
        // COB_CODE: PERFORM S14150-CONTR-COMP-SOPR-GAR
        //              THRU EX-S14150.
        s14150ContrCompSoprGar();
    }

    /**Original name: S14180-VAL-TRCH-NEGATIVA<br>
	 * <pre>----------------------------------------------------------------*
	 *     PARAGRAFO DI VALORIZZAZIONE TRANCHE NEGATIVA
	 * ----------------------------------------------------------------*
	 * --> GESTIONE TRANCHE NEGATIVA</pre>*/
    private void s14180ValTrchNegativa() {
        // COB_CODE: IF TRCH-NEG-SI
        //                TO WPAG-TP-TRCH(IX-AREA-PAGINA, IX-TAB-TGA)
        //           END-IF.
        if (ws.getWkFlgTrch().isSi()) {
            // COB_CODE: ADD 1 TO IX-TAB-TGA
            ws.getIxIndici().setTabTga(Trunc.toShort(1 + ws.getIxIndici().getTabTga(), 4));
            // COB_CODE: MOVE WK-PREMIO-NEG
            //             TO WPAG-PRE-INI-NET(IX-AREA-PAGINA, IX-TAB-TGA)
            //                WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
            //                WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
            //                WPAG-PRSTZ-INI(IX-AREA-PAGINA, IX-TAB-TGA)
            //                WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                WPAG-PRE-PP-INI(IX-AREA-PAGINA, IX-TAB-TGA)
            //                WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                WPAG-IMP-MOVI-NEG(IX-AREA-PAGINA)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreIniNet().setWpagPreIniNet(Trunc.toDecimal(ws.getWkVariabili().getWkPremioNeg(), 15, 3));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagRatLrd().setWpagRatLrd(Trunc.toDecimal(ws.getWkVariabili().getWkPremioNeg(), 15, 3));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreLrd().setWpagPreLrd(Trunc.toDecimal(ws.getWkVariabili().getWkPremioNeg(), 15, 3));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrstzIni().setWpagPrstzIni(Trunc.toDecimal(ws.getWkVariabili().getWkPremioNeg(), 15, 3));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrstzUlt().setWpagPrstzUlt(Trunc.toDecimal(ws.getWkVariabili().getWkPremioNeg(), 15, 3));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrePpIni().setWpagPrePpIni(Trunc.toDecimal(ws.getWkVariabili().getWkPremioNeg(), 15, 3));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrePpUlt().setWpagPrePpUlt(Trunc.toDecimal(ws.getWkVariabili().getWkPremioNeg(), 15, 3));
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagImpMoviNeg().setWpagImpMoviNeg(Trunc.toDecimal(ws.getWkVariabili().getWkPremioNeg(), 15, 3));
            // COB_CODE: MOVE 'S'
            //             TO WPAG-FL-PREL-RIS(IX-AREA-PAGINA, IX-TAB-TGA)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).setWpagFlPrelRis("S");
            // COB_CODE: MOVE '9'
            //             TO WPAG-TP-TRCH(IX-AREA-PAGINA, IX-TAB-TGA)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).setWpagTpTrch("9");
        }
        // COB_CODE: MOVE IX-TAB-TGA   TO WPAG-ELE-TRANCHE-MAX(IX-AREA-PAGINA).
        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).setWpagEleTrancheMax(ws.getIxIndici().getTabTga());
    }

    /**Original name: S14110-CONTR-COMP-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
	 *     VALORIZZARE LA DCLGEN TRANCHE DI GARANZIA
	 * ----------------------------------------------------------------*
	 * --> interessi di frazionamento da visualizzare in elenco tranche
	 * --> deve essere relativo al premio annuo.</pre>*/
    private void s14110ContrCompTranche() {
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'INTFRAZ'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "INTFRAZ")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //           WHEN 'N'
            //           WHEN 'I'
            //                 TO WPAG-TGA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //              TO WPAG-TGA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).setWpagTgaIntFraz(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> DATI TRANCHE DI GARANZIA
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'CODREGFISC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CODREGFISC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-TP-RGM-FISC(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //               (10:2)
                    //             TO WPAG-TP-RGM-FISC(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).setWpagTpRgmFisc(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpTFormatted().substring((10) - 1, 11));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'TIPOTR'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "TIPOTR")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //                    WS-TP-TRCH
            //                    WS-TP-TRCH
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TP-TRCH(IX-AREA-PAGINA, IX-TAB-TGA)
                    //                WS-TP-TRCH
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).setWpagTpTrch(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrT());
                    ws.setWsTpTrch(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrT());
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'PCASOM'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PCASOM")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-PRE-CASO-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-PRE-CASO-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'A'
            //             WHEN 'P'
            //             WHEN 'M'
            //                 TO WPAG-PRE-CASO-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-CASO-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreCasoMor().setWpagPreCasoMorFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-CASO-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreCasoMor().setWpagPreCasoMor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'A':
                case 'P':
                case 'M':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-CASO-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreCasoMor().setWpagPreCasoMor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'BONANT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "BONANT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-BNS-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-BNS-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-BNS-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-BNS-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpBnsAntic().setWpagImpBnsAnticFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-BNS-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpBnsAntic().setWpagImpBnsAntic(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-BNS-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpBnsAntic().setWpagImpBnsAntic(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'PNETTO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PNETTO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-PRE-INI-NET(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-PRE-INI-NET(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-PRE-INI-NET(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-INI-NET(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreIniNet().setWpagPreIniNetFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-INI-NET(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreIniNet().setWpagPreIniNet(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-INI-NET(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreIniNet().setWpagPreIniNet(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'PPUROI'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PPUROI")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                    WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                    WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                    WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                    WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                    WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                    WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-PP-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    //                WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrePpIni().setWpagPrePpIniFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrePpUlt().setWpagPrePpUltFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-PP-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    //                WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrePpIni().setWpagPrePpIni(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrePpUlt().setWpagPrePpUlt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-PP-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    //                WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrePpIni().setWpagPrePpIni(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrePpUlt().setWpagPrePpUlt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'PTARIF'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PTARIF")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                    WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                    WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                    WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                    WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                    WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                    WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-TARI-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    //                WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreTariIni().setWpagPreTariIniFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreTariUlt().setWpagPreTariUltFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-TARI-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    //                WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreTariIni().setWpagPreTariIni(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreTariUlt().setWpagPreTariUlt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-TARI-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    //                WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreTariIni().setWpagPreTariIni(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreTariUlt().setWpagPreTariUlt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'PINVEN'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PINVEN")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                    WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                    WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                    WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                    WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                    WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                    WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-INVRIO-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    //                WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreInvrioIni().setWpagPreInvrioIniFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreInvrioUlt().setWpagPreInvrioUltFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-INVRIO-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    //                WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreInvrioIni().setWpagPreInvrioIni(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreInvrioUlt().setWpagPreInvrioUlt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-INVRIO-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    //                WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreInvrioIni().setWpagPreInvrioIni(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreInvrioUlt().setWpagPreInvrioUlt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'SOPSAN'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPSAN")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-SOPR-SAN(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-SOPR-SAN(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-SOPR-SAN(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-SAN(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprSan().setWpagImpSoprSanFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-SAN(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprSan().setWpagImpSoprSan(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-SAN(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprSan().setWpagImpSoprSan(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'SOPPRO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPPRO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-SOPR-PROF(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-SOPR-PROF(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-SOPR-PROF(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-PROF(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprProf().setWpagImpSoprProfFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-PROF(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprProf().setWpagImpSoprProf(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-PROF(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprProf().setWpagImpSoprProf(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'SOPSPO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPSPO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-SOPR-SPO(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-SOPR-SPO(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-SOPR-SPO(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-SPO(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprSpo().setWpagImpSoprSpoFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-SPO(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprSpo().setWpagImpSoprSpo(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-SPO(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprSpo().setWpagImpSoprSpo(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'SOPTEC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPTEC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-SOPR-TEC(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-SOPR-TEC(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-SOPR-TEC(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-TEC(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprTec().setWpagImpSoprTecFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-TEC(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprTec().setWpagImpSoprTec(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SOPR-TEC(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpSoprTec().setWpagImpSoprTec(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'SOPALT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPALT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-ALT-SOPR(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-ALT-SOPR(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-ALT-SOPR(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-ALT-SOPR(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAltSopr().setWpagImpAltSoprFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-ALT-SOPR(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAltSopr().setWpagImpAltSopr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-ALT-SOPR(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAltSopr().setWpagImpAltSopr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'RIVFIX'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "RIVFIX")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-TS-RIVAL-FIS(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-TS-RIVAL-FIS(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-TS-RIVAL-FIS(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TS-RIVAL-FIS(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTsRivalFis().setWpagTsRivalFisFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TS-RIVAL-FIS(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTsRivalFis().setWpagTsRivalFis(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 6, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TS-RIVAL-FIS(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTsRivalFis().setWpagTsRivalFis(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 6, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'RATALO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "RATALO")) {
            // COB_CODE: SET FLAG-RATALO-SI                    TO TRUE
            ws.getWkFlagRatalo().setSi();
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagRatLrd().setWpagRatLrdFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagRatLrd().setWpagRatLrd(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagRatLrd().setWpagRatLrd(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PRETOT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PRETOT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreLrd().setWpagPreLrdFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreLrd().setWpagPreLrd(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPreLrd().setWpagPreLrd(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'PRESTINI'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PRESTINI")) {
            // COB_CODE:         EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                        (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //                     WHEN 'S'
            //                     WHEN 'D'
            //                         TO WPAG-PRSTZ-INI(IX-AREA-PAGINA, IX-TAB-TGA)
            //           *                 WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                     WHEN 'N'
            //                     WHEN 'I'
            //                         TO WPAG-PRSTZ-INI(IX-AREA-PAGINA, IX-TAB-TGA)
            //           *                 WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                     WHEN 'P'
            //                     WHEN 'M'
            //                     WHEN 'A'
            //                         TO WPAG-PRSTZ-INI(IX-AREA-PAGINA, IX-TAB-TGA)
            //           *                 WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                   END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRSTZ-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrstzIni().setWpagPrstzIniFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    //                 WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRSTZ-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrstzIni().setWpagPrstzIni(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    //                 WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRSTZ-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrstzIni().setWpagPrstzIni(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    //                 WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'CAPINI'
        //              END-EVALUATE
        //            END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CAPINI")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrstzUlt().setWpagPrstzUltFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrstzUlt().setWpagPrstzUlt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrstzUlt().setWpagPrstzUlt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE:  IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'CAPOPZ'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CAPOPZ")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CPT-IN-OPZ-RIVTO(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CPT-IN-OPZ-RIVTO(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CPT-IN-OPZ-RIVTO(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CPT-IN-OPZ-RIVTO(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagCptInOpzRivto().setWpagCptInOpzRivtoFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CPT-IN-OPZ-RIVTO(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagCptInOpzRivto().setWpagCptInOpzRivto(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CPT-IN-OPZ-RIVTO(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagCptInOpzRivto().setWpagCptInOpzRivto(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'CAPMOR'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CAPMOR")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CPT-RSH-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CPT-RSH-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CPT-RSH-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CPT-RSH-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagCptRshMor().setWpagCptRshMorFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CPT-RSH-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagCptRshMor().setWpagCptRshMor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CPT-RSH-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagCptRshMor().setWpagCptRshMor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'IMPSCO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPSCO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpScon().setWpagImpSconFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpScon().setWpagImpScon(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpScon().setWpagImpScon(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'ALISCO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALISCO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-ALQ-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-ALQ-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-ALQ-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALQ-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagAlqScon().setWpagAlqSconFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALQ-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagAlqScon().setWpagAlqScon(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 6, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALQ-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagAlqScon().setWpagAlqScon(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 6, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'CARACQ'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CARACQ")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-CAR-ACQ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-CAR-ACQ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-CAR-ACQ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-CAR-ACQ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarAcqTga().setWpagImpCarAcqTgaFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-CAR-ACQ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarAcqTga().setWpagImpCarAcqTga(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-CAR-ACQ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarAcqTga().setWpagImpCarAcqTga(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'ACQEXP'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ACQEXP")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-TGA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-TGA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-TGA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TGA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpAcqExp().setWpagTgaImpAcqExpFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TGA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpAcqExp().setWpagTgaImpAcqExp(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TGA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpAcqExp().setWpagTgaImpAcqExp(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'CARINC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CARINC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-CAR-INC-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-CAR-INC-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-CAR-INC-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-CAR-INC-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarIncTga().setWpagImpCarIncTgaFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-CAR-INC-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarIncTga().setWpagImpCarIncTga(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-CAR-INC-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarIncTga().setWpagImpCarIncTga(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'CARGES'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CARGES")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                TO WPAG-IMP-CAR-GEST-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                TO WPAG-IMP-CAR-GEST-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                TO WPAG-IMP-CAR-GEST-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-IMP-CAR-GEST-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarGestTga().setWpagImpCarGestTgaFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-IMP-CAR-GEST-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarGestTga().setWpagImpCarGestTga(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-IMP-CAR-GEST-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpCarGestTga().setWpagImpCarGestTga(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO AZIENDA (FONTI CONTRIBUTIVE)
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'IMPAZIENDA'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPAZIENDA")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                  TO WPAG-IMP-AZ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-AZ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAzTga().setWpagImpAzTga(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO ADERENTE (FONTI CONTRIBUTIVE)
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'IMPADERENTE'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPADERENTE")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                  TO WPAG-IMP-ADER-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-ADER-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAderTga().setWpagImpAderTga(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO TFR (FONTI CONTRIBUTIVE)
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'IMPTFR'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPTFR")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                  TO WPAG-IMP-TFR-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-TFR-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpTfrTga().setWpagImpTfrTga(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO VOLONTARIO (FONTI CONTRIBUTIVE)
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'IMPVOLONT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPVOLONT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                  TO WPAG-IMP-VOLO-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-VOLO-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpVoloTga().setWpagImpVoloTga(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO ABBUONO ANNUO ULTIMO
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'ABBANNUO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ABBANNUO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-ABB-ANNO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-IMP-ABB-ANNO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAbbAnnoUlt().setWpagImpAbbAnnoUlt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO ABBUONO TOTALE INIZIALE
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'ABBTOT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ABBTOT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                  TO WPAG-IMP-ABB-TOT-INI(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-ABB-TOT-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpAbbTotIni().setWpagImpAbbTotIni(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> DURATA ANNI ABBUONO
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'ANNIABB'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ANNIABB")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                  TO WPAG-DUR-ABB-ANNI(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-DUR-ABB-ANNI(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagDurAbbAnni().setWpagDurAbbAnni(Trunc.toInt(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 6));
                    break;

                default:break;
            }
        }
        //--> IMPORTO PREMIO PATTUITO
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'PATTUITO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PATTUITO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-PRE-PATTUITO(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-IMP-PRE-PATTUITO(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpPrePattuito().setWpagImpPrePattuito(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO PRESTAZIONE ULTIMA
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'PRESTULT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PRESTULT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                     WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //                     WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-PREST-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    //                WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpPrestUlt().setWpagImpPrestUlt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagPrstzUlt().setWpagPrstzUlt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO PREMIO NETTO ULTIMO RIVALUTATO
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'PREULT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PREULT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                  TO WPAG-IMP-PRE-RIVAL(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-PRE-RIVAL(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpPreRival().setWpagImpPreRival(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> ALIQUOTA PROVVIGIONE INCASSO
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQINC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALIQINC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-TGA-ALIQ-INCAS(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TGA-ALIQ-INCAS(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaAliqIncas().setWpagTgaAliqIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 14, 9));
                    break;

                default:break;
            }
        }
        //--> ALIQUOTA PROVVIGIONE ACQUISTO
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQACQ'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALIQACQ")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-TGA-ALIQ-ACQ(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TGA-ALIQ-ACQ(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaAliqAcq().setWpagTgaAliqAcq(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 14, 9));
                    break;

                default:break;
            }
        }
        //--> ALIQUOTA PROVVIGIONE RICORRENTE
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQRIC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALIQRIC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-TGA-ALIQ-RICOR(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TGA-ALIQ-RICOR(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaAliqRicor().setWpagTgaAliqRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 14, 9));
                    break;

                default:break;
            }
        }
        //--> IMPORTO IMPONIBILE PROVVIGIONE INCASSO
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPOINC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPOINC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-TGA-IMP-INCAS(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TGA-IMP-INCAS(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpIncas().setWpagTgaImpIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO IMPONIBILE PROVVIGIONE ACQUISTO
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPOACQ'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPOACQ")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-TGA-IMP-ACQ(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TGA-IMP-ACQ(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpAcq().setWpagTgaImpAcq(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO IMPONIBILE PROVVIGIONE RICORRENTE
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPORIC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPORIC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-TGA-IMP-RICOR(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-TGA-IMP-RICOR(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagTgaImpRicor().setWpagTgaImpRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO PRESTAZIONE AGGIUNTIVA X TRASF.COMMERCIALE INIZIALE
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'PRESTAGG'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PRESTAGG")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                TO WPAG-IMP-PRSTZ-AGG-INI(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-IMP-PRSTZ-AGG-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagImpPrstzAggIni().setWpagImpPrstzAggIni(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //                 = 'PRESTNEG'
        //           *--> SE E' PRESENTE QUESTA COMPONENTE LA GARANZIA BASE PREVEDE
        //           *--> UNA TRANCHE NEGATIVA
        //                   END-EVALUATE
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PRESTNEG")) {
            //--> SE E' PRESENTE QUESTA COMPONENTE LA GARANZIA BASE PREVEDE
            //--> UNA TRANCHE NEGATIVA
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //           WHEN 'I'
            //              SET TRCH-NEG-SI            TO TRUE
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WK-PREMIO-NEG
                    ws.getWkVariabili().setWkPremioNeg(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    // COB_CODE: SET TRCH-NEG-SI            TO TRUE
                    ws.getWkFlgTrch().setSi();
                    break;

                default:break;
            }
        }
        //--  Importo Managment fee
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'MANFEE'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "MANFEE")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //           WHEN 'I'
            //                TO WPAG-MANFEE-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-MANFEE-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).getWpagManfeeAntic().setWpagManfeeAntic(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--  Rendita Iniziale a Tasso 0
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'PRESTINI0'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PRESTINI0")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-REN-INI-TS-TEC-0(IX-AREA-PAGINA, IX-TAB-TGA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-REN-INI-TS-TEC-0(IX-AREA-PAGINA, IX-TAB-TGA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTranche(ws.getIxIndici().getTabTga()).setWpagRenIniTsTec0(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: MOVE IX-TAB-TGA   TO WPAG-ELE-TRANCHE-MAX(IX-AREA-PAGINA).
        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).setWpagEleTrancheMax(ws.getIxIndici().getTabTga());
    }

    /**Original name: S14120-CONTR-COMP-PAR-CALC<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
	 *     VALORIZZARE I DATI PARAMETRO CALCOLO DELL'AREA DI PAGINA
	 * ----------------------------------------------------------------*</pre>*/
    private void s14120ContrCompParCalc() {
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            =  'PPI' OR 'PII' OR 'PTI' OR 'PNI'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PPI") || Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PII") || Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PTI") || Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PNI")) {
            // COB_CODE: ADD 1         TO IX-TAB-PAR
            ws.getIxIndici().setTabPar(Trunc.toShort(1 + ws.getIxIndici().getTabPar(), 4));
            // COB_CODE: MOVE ISPC0140-CODICE-COMPONENTE-T
            //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WPAG-COD-PARAM(IX-AREA-PAGINA, IX-TAB-PAR)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagCodParam(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT());
            //        MOVE 'A' TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)
            // COB_CODE: MOVE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagTpD(String.valueOf(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()));
            //---      date e stringhe
            // COB_CODE:         EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //           *---      date e stringhe
            //                     WHEN 'S'
            //                     WHEN 'D'
            //                         TO WPAG-VAL-STR(IX-AREA-PAGINA, IX-TAB-PAR)
            //                     WHEN 'N'
            //                     WHEN 'I'
            //                         TO WPAG-VAL-IMP(IX-AREA-PAGINA, IX-TAB-PAR)
            //           *--       percentuali
            //                     WHEN 'P'
            //                     WHEN 'M'
            //                         TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
            //           *--       tasso
            //                     WHEN 'A'
            //                         TO WPAG-VAL-TS(IX-AREA-PAGINA, IX-TAB-PAR)
            //                   END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-STR(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagValStr(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrT());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-IMP(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValImp().setWpagValImp(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    //--       percentuali
                    break;

                case 'P':
                case 'M':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValPc().setWpagValPc(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 6, 3));
                    //--       tasso
                    break;

                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-TS(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValTs().setWpagValTs(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 14, 9));
                    break;

                default:break;
            }
        }
        //--  Aliquote
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //             'ACARACQ' OR 'ACARINC' OR 'ACARGES'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ACARACQ") || Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ACARINC") || Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ACARGES")) {
            // COB_CODE: ADD 1         TO IX-TAB-PAR
            ws.getIxIndici().setTabPar(Trunc.toShort(1 + ws.getIxIndici().getTabPar(), 4));
            // COB_CODE: MOVE ISPC0140-CODICE-COMPONENTE-T
            //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WPAG-COD-PARAM(IX-AREA-PAGINA, IX-TAB-PAR)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagCodParam(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT());
            //        MOVE 'P'      TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)
            // COB_CODE: MOVE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagTpD(String.valueOf(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()));
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-VAL-STR(IX-AREA-PAGINA, IX-TAB-PAR)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-VAL-IMP(IX-AREA-PAGINA, IX-TAB-PAR)
            //             WHEN 'P'
            //             WHEN 'M'
            //                 TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
            //             WHEN 'A'
            //                 TO WPAG-VAL-TS(IX-AREA-PAGINA, IX-TAB-PAR)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-STR(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagValStr(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrT());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-IMP(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValImp().setWpagValImp(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValPc().setWpagValPc(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 6, 3));
                    break;

                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-TS(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValTs().setWpagValTs(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 14, 9));
                    break;

                default:break;
            }
        }
        //--  Aliquote
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQACQ'
        //              OR 'ALIQINC'  OR 'ALIQGES'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALIQACQ") || Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALIQINC") || Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALIQGES")) {
            // COB_CODE: ADD 1         TO IX-TAB-PAR
            ws.getIxIndici().setTabPar(Trunc.toShort(1 + ws.getIxIndici().getTabPar(), 4));
            // COB_CODE: MOVE ISPC0140-CODICE-COMPONENTE-T
            //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WPAG-COD-PARAM(IX-AREA-PAGINA, IX-TAB-PAR)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagCodParam(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT());
            //        MOVE 'P'      TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)
            // COB_CODE: MOVE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagTpD(String.valueOf(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()));
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValPc().setWpagValPcFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValPc().setWpagValPc(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 6, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValPc().setWpagValPc(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 6, 3));
                    break;

                default:break;
            }
        }
        //--  Tasso Tecnico
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'TASTEC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "TASTEC")) {
            // COB_CODE: ADD 1         TO IX-TAB-PAR
            ws.getIxIndici().setTabPar(Trunc.toShort(1 + ws.getIxIndici().getTabPar(), 4));
            // COB_CODE: MOVE ISPC0140-CODICE-COMPONENTE-T
            //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WPAG-COD-PARAM(IX-AREA-PAGINA, IX-TAB-PAR)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagCodParam(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT());
            // COB_CODE: MOVE ISPC0140-COMP-TIPO-DATO-T
            //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagTpD(String.valueOf(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()));
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValPc().setWpagValPcFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValPc().setWpagValPc(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 6, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).getWpagValPc().setWpagValPc(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 6, 3));
                    break;

                default:break;
            }
        }
        //--  Tavola mortalit`
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //             'TAVMORTM' OR 'TAVMORTF' OR 'TAVMORTR'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "TAVMORTM") || Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "TAVMORTF") || Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "TAVMORTR")) {
            // COB_CODE: ADD 1         TO IX-TAB-PAR
            ws.getIxIndici().setTabPar(Trunc.toShort(1 + ws.getIxIndici().getTabPar(), 4));
            // COB_CODE: MOVE ISPC0140-CODICE-COMPONENTE-T
            //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WPAG-COD-PARAM(IX-AREA-PAGINA, IX-TAB-PAR)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagCodParam(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT());
            // COB_CODE: MOVE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagTpD(String.valueOf(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()));
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //                 TO WPAG-VAL-STR(IX-AREA-PAGINA, IX-TAB-PAR)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-VAL-STR(IX-AREA-PAGINA, IX-TAB-PAR)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParCalc(ws.getIxIndici().getTabPar()).setWpagValStr(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrT());
                    break;

                default:break;
            }
        }
        // COB_CODE: MOVE IX-TAB-PAR    TO WPAG-ELE-PAR-CALC-MAX(IX-AREA-PAGINA).
        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).setWpagEleParCalcMax(ws.getIxIndici().getTabPar());
    }

    /**Original name: S14130-CONTR-COMP-PROVV<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
	 *     VALORIZZARE I DATI PROVVIGIONI DELL'AREA DI PAGINA
	 * ----------------------------------------------------------------*
	 * --> DATI PROVVIGIONI</pre>*/
    private void s14130ContrCompProvv() {
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ1A'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVACQ1A")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-PRV-ACQ-1O-ANNO(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-PRV-ACQ-1O-ANNO(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-PRV-ACQ-1O-ANNO(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-ACQ-1O-ANNO(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvAcq1oAnno().setWpagPrvAcq1oAnnoFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-ACQ-1O-ANNO(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvAcq1oAnno().setWpagPrvAcq1oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-ACQ-1O-ANNO(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvAcq1oAnno().setWpagPrvAcq1oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ2A'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVACQ2A")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-PRV-ACQ-2O-ANNO(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-PRV-ACQ-2O-ANNO(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-PRV-ACQ-2O-ANNO(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-ACQ-2O-ANNO(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvAcq2oAnno().setWpagPrvAcq2oAnnoFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-ACQ-2O-ANNO(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvAcq2oAnno().setWpagPrvAcq2oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-ACQ-2O-ANNO(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvAcq2oAnno().setWpagPrvAcq2oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVRIC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVRIC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-PRV-RICOR(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-PRV-RICOR(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-PRV-RICOR(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-RICOR(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvRicor().setWpagPrvRicorFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-RICOR(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvRicor().setWpagPrvRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-RICOR(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvRicor().setWpagPrvRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVINC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVINC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-PRV-INCAS(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-PRV-INCAS(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-PRV-INCAS(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-INCAS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvIncas().setWpagPrvIncasFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-INCAS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvIncas().setWpagPrvIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-INCAS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvIncas().setWpagPrvIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQINC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALIQINC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-ALIQ-INCAS(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-ALIQ-INCAS(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-ALIQ-INCAS(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-INCAS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqIncas().setWpagAliqIncasFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-INCAS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqIncas().setWpagAliqIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 14, 9));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-INCAS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqIncas().setWpagAliqIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 14, 9));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQACQ'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALIQACQ")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-ALIQ-ACQ(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-ALIQ-ACQ(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-ALIQ-ACQ(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-ACQ(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqAcq().setWpagAliqAcqFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-ACQ(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqAcq().setWpagAliqAcq(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 14, 9));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-ACQ(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqAcq().setWpagAliqAcq(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 14, 9));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQRIC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALIQRIC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-ALIQ-RICOR(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-ALIQ-RICOR(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-ALIQ-RICOR(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-RICOR(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqRicor().setWpagAliqRicorFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-RICOR(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqRicor().setWpagAliqRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 14, 9));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-RICOR(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqRicor().setWpagAliqRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 14, 9));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPOINC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPOINC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-INCAS(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-INCAS(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-INCAS(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-INCAS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpIncas().setWpagImpIncasFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-INCAS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpIncas().setWpagImpIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-INCAS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpIncas().setWpagImpIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPOACQ'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPOACQ")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-ACQ(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-ACQ(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-ACQ(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-ACQ(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpAcq().setWpagImpAcqFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-ACQ(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpAcq().setWpagImpAcq(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-ACQ(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpAcq().setWpagImpAcq(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPORIC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPORIC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-RICOR(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-RICOR(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-RICOR(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-RICOR(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpRicor().setWpagImpRicorFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-RICOR(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpRicor().setWpagImpRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-RICOR(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpRicor().setWpagImpRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //            'ALIQASSICUR'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALIQASSICUR")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-ALIQ-REN-ASS(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-ALIQ-REN-ASS(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-ALIQ-REN-ASS(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-REN-ASS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqRenAss().setWpagAliqRenAssFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-REN-ASS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqRenAss().setWpagAliqRenAss(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 14, 9));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-REN-ASS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqRenAss().setWpagAliqRenAss(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 14, 9));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'REMUNASS'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "REMUNASS")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-PRV-REN-ASS(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-PRV-REN-ASS(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-PRV-REN-ASS(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-REN-ASS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvRenAss().setWpagPrvRenAssFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-REN-ASS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvRenAss().setWpagPrvRenAss(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-REN-ASS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvRenAss().setWpagPrvRenAss(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //            'ALIQCOMINT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ALIQCOMINT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-ALIQ-COMMIS-INT(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-ALIQ-COMMIS-INT(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-ALIQ-COMMIS-INT(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-COMMIS-INT(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqCommisInt().setWpagAliqCommisIntFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-COMMIS-INT(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqCommisInt().setWpagAliqCommisInt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 14, 9));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-ALIQ-COMMIS-INT(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagAliqCommisInt().setWpagAliqCommisInt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 14, 9));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'COMMINTER'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "COMMINTER")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-PRV-COMMIS-INT(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-PRV-COMMIS-INT(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-PRV-COMMIS-INT(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-COMMIS-INT(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvCommisInt().setWpagPrvCommisIntFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-COMMIS-INT(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvCommisInt().setWpagPrvCommisInt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-PRV-COMMIS-INT(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagPrvCommisInt().setWpagPrvCommisInt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //           'IMPOREMUNASS'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPOREMUNASS")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-REN-ASS(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-REN-ASS(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-REN-ASS(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-REN-ASS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpRenAss().setWpagImpRenAssFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-REN-ASS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpRenAss().setWpagImpRenAss(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-REN-ASS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpRenAss().setWpagImpRenAss(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //           'IMPOCOMINT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPOCOMINT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-COMMIS-INT(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-COMMIS-INT(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-COMMIS-INT(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-COMMIS-INT(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpCommisInt().setWpagImpCommisIntFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-COMMIS-INT(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpCommisInt().setWpagImpCommisInt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-COMMIS-INT(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiProvvig().getWpagImpCommisInt().setWpagImpCommisInt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
    }

    /**Original name: S14140-CONTR-COMP-ALTRI<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
	 *     VALORIZZARE GLI ALTRI DATI DELL'AREA DI PAGINA
	 * ----------------------------------------------------------------*</pre>*/
    private void s14140ContrCompAltri() {
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'TIPIAS'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "TIPIAS")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                    WK-TP-IAS(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WK-APPO-IAS
                    ws.getWkVariabili().getWkAppoIas().setWkAppoIas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 18, 7));
                    // COB_CODE: MOVE WK-IAS-2
                    //             TO WPAG-TP-IAS(IX-AREA-PAGINA)
                    //                WK-TP-IAS(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiAltri().setWpagTpIas(ws.getWkVariabili().getWkAppoIas().getIas2Formatted());
                    ws.getWkVariabili().getWkTabGar(ws.getIxIndici().getAreaPagina()).setTpIas(ws.getWkVariabili().getWkAppoIas().getIas2Formatted());
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //                                                        'DATAVALQUOTE'
        //              END-IF
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DATAVALQUOTE")) {
            // COB_CODE: IF VERSAM-AGGIUNTIVO OR VERSAM-AGGIUNTIVO-REINV
            //              END-IF
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getWsMovimento().isVersamAggiuntivo() || ws.getWsMovimento().isVersamAggiuntivoReinv()) {
                // COB_CODE: MOVE WSKD-DEE
                //             TO NEW-DT-VAL-QUOTE
                ws.getWkVariabili().getNewDtValQuote().setNewDtValQuote(ws.getIvvc0216().getWskdDee());
                // COB_CODE: IF NEW-DT-VAL-QUOTE-RED NUMERIC
                //                TO WPAG-DT-VAL-QUOTE(IX-AREA-PAGINA)
                //           END-IF
                if (Functions.isNumber(ws.getWkVariabili().getNewDtValQuote().getNewDtValQuoteRedFormatted())) {
                    // COB_CODE: MOVE NEW-DT-VAL-QUOTE-RED
                    //             TO WPAG-DT-VAL-QUOTE(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiAltri().getWpagDtValQuote().setWpagDtValQuote(ws.getWkVariabili().getNewDtValQuote().getNewDtValQuoteRed());
                }
            }
            else {
                // COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                //             TO WK-APPO-DT
                ws.getWkVariabili().getWkAppoDt().setWkAppoDt(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrT());
                // COB_CODE: IF WK-APPO-DT-RED NUMERIC
                //                TO WPAG-DT-VAL-QUOTE(IX-AREA-PAGINA)
                //           END-IF
                if (Functions.isNumber(ws.getWkVariabili().getWkAppoDt().getWkAppoDtRedFormatted())) {
                    // COB_CODE: MOVE WK-APPO-DT-RED
                    //             TO WPAG-DT-VAL-QUOTE(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiAltri().getWpagDtValQuote().setWpagDtValQuote(ws.getWkVariabili().getWkAppoDt().getWkAppoDtRed());
                }
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'FRAZPROVV'
        //                TO WPAG-FL-FRAZ-PROVV(IX-AREA-PAGINA)
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "FRAZPROVV")) {
            // COB_CODE: MOVE ISPC0140-TRATTAMENTO-PROVV-T
            //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WPAG-FL-FRAZ-PROVV(IX-AREA-PAGINA)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiAltri().setWpagFlFrazProvv(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140TrattamentoProvvT().getFlagModCalcPreP());
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PINVEST'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PINVEST")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-IMP-MOVI(IX-AREA-PAGINA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-MOVI(IX-AREA-PAGINA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-IMP-MOVI(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-MOVI(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagImpMovi().setWpagImpMoviFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-MOVI(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagImpMovi().setWpagImpMovi(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-MOVI(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagImpMovi().setWpagImpMovi(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'ANMESIPRO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ANMESIPRO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //                 TO WPAG-MM-DIFF-PROR(IX-AREA-PAGINA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WK-DIFF-PROR
                    ws.getWkVariabili().getWkDiffPror().setWkDiffPror(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 4, 2));
                    // COB_CODE: MOVE WK-DIFF-AA
                    //             TO WPAG-AA-DIFF-PROR(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagAaDiffPror().setWpagAaDiffProrFormatted(ws.getWkVariabili().getWkDiffPror().getAaFormatted());
                    // COB_CODE: MOVE WK-DIFF-MM
                    //             TO WPAG-MM-DIFF-PROR(IX-AREA-PAGINA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagMmDiffPror().setWpagMmDiffProrFormatted(ws.getWkVariabili().getWkDiffPror().getMmFormatted());
                    break;

                default:break;
            }
        }
    }

    /**Original name: S14145-CONTR-COMP-PAR-MOVIM<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
	 *     VALORIZZARE GLI ALTRI DATI DELL'AREA DI PAGINA
	 * ----------------------------------------------------------------*</pre>*/
    private void s14145ContrCompParMovim() {
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'DPROXBONFED'
        //              END-IF
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DPROXBONFED")) {
            // COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
            //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WK-APPO-DT
            ws.getWkVariabili().getWkAppoDt().setWkAppoDt(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrT());
            // COB_CODE: IF WK-APPO-DT-RED NUMERIC
            //                TO WPAG-DT-PROS-BNS-FED(IX-AREA-PAGINA)
            //           END-IF
            if (Functions.isNumber(ws.getWkVariabili().getWkAppoDt().getWkAppoDtRedFormatted())) {
                // COB_CODE: MOVE WK-APPO-DT-RED
                //             TO WPAG-DT-PROS-BNS-FED(IX-AREA-PAGINA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParMovim().getWpagDtProsBnsFed().setWpagDtProsBnsFed(ws.getWkVariabili().getWkAppoDt().getWkAppoDtRed());
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'DPROXBONRIC'
        //              END-IF
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DPROXBONRIC")) {
            // COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
            //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WK-APPO-DT
            ws.getWkVariabili().getWkAppoDt().setWkAppoDt(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrT());
            // COB_CODE: IF WK-APPO-DT-RED NUMERIC
            //                TO WPAG-DT-PROS-BNS-RIC(IX-AREA-PAGINA)
            //           END-IF
            if (Functions.isNumber(ws.getWkVariabili().getWkAppoDt().getWkAppoDtRedFormatted())) {
                // COB_CODE: MOVE WK-APPO-DT-RED
                //             TO WPAG-DT-PROS-BNS-RIC(IX-AREA-PAGINA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParMovim().getWpagDtProsBnsRic().setWpagDtProsBnsRic(ws.getWkVariabili().getWkAppoDt().getWkAppoDtRed());
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
        //            = 'DPROXCED'
        //              END-IF
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DPROXCED")) {
            // COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
            //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             TO WK-APPO-DT
            ws.getWkVariabili().getWkAppoDt().setWkAppoDt(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrT());
            // COB_CODE: IF WK-APPO-DT-RED NUMERIC
            //                TO WPAG-DT-PROS-CEDOLA(IX-AREA-PAGINA)
            //           END-IF
            if (Functions.isNumber(ws.getWkVariabili().getWkAppoDt().getWkAppoDtRedFormatted())) {
                // COB_CODE: MOVE WK-APPO-DT-RED
                //             TO WPAG-DT-PROS-CEDOLA(IX-AREA-PAGINA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiParMovim().getWpagDtProsCedola().setWpagDtProsCedola(ws.getWkVariabili().getWkAppoDt().getWkAppoDtRed());
            }
        }
    }

    /**Original name: S14150-CONTR-COMP-SOPR-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
	 *     VALORIZZARE I DATI SOPRAPPREMI DI GARANZIA DELL'AREA DI PAG.
	 * ----------------------------------------------------------------*
	 * --> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
	 *     FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL</pre>*/
    private void s14150ContrCompSoprGar() {
        // COB_CODE: SET WK-GRZ-NON-TROVATA         TO TRUE
        ws.getWkRicGrz().setNonTrovata();
        // COB_CODE: PERFORM VARYING IX-APPO-GRZ FROM 1 BY 1
        //             UNTIL IX-APPO-GRZ > WGRZ-ELE-GARANZIA-MAX
        //                OR WK-GRZ-TROVATA
        //             END-IF
        //           END-PERFORM.
        ws.getIxIndici().setAppoGrz(((short)1));
        while (!(ws.getIxIndici().getAppoGrz() > wgrzAreaGaranzia.getEleGarMax() || ws.getWkRicGrz().isTrovata())) {
            // COB_CODE: IF WGRZ-COD-TARI(IX-APPO-GRZ)
            //                                  = WPAG-COD-GARANZIA(IX-AREA-PAGINA)
            //              SET WK-GRZ-TROVATA            TO TRUE
            //           END-IF
            if (Conditions.eq(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getAppoGrz()).getLccvgrz1().getDati().getWgrzCodTari(), wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagCodGaranzia())) {
                // COB_CODE: MOVE WGRZ-ID-GAR(IX-APPO-GRZ) TO WK-ID-GAR
                ws.getWkVariabili().setWkIdGar(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getAppoGrz()).getLccvgrz1().getDati().getWgrzIdGar());
                // COB_CODE: SET WK-GRZ-TROVATA            TO TRUE
                ws.getWkRicGrz().setTrovata();
            }
            ws.getIxIndici().setAppoGrz(Trunc.toShort(ws.getIxIndici().getAppoGrz() + 1, 4));
        }
        // COB_CODE: IF WK-GRZ-NON-TROVATA
        //              MOVE ZEROES TO WK-ID-GAR
        //           END-IF.
        if (ws.getWkRicGrz().isNonTrovata()) {
            // COB_CODE: MOVE ZEROES TO WK-ID-GAR
            ws.getWkVariabili().setWkIdGar(0);
        }
        //--> ATTUALMENTE PRODOTTO PER I SOVRAPPREMI GESTISCE SOLO GLI
        //--> IMPORTI
        //--> A BREVE SARA' RILASCIATA UNA NUOVA GESTIONE CHE PREVEDE
        //--> L'UTILIZZO DELLA PERCENTUALE DI SOVRAMMORTALITA'
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSAN'
        //              END-PERFORM
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPSAN")) {
            // COB_CODE: ADD 1                       TO IX-TAB-SPG
            ws.getIxIndici().setTabSpg(Trunc.toShort(1 + ws.getIxIndici().getTabSpg(), 4));
            // COB_CODE: SET WK-SOPSAN               TO TRUE
            ws.getWkCodSovrap().setSopsan();
            // COB_CODE: MOVE WK-COD-SOVRAP
            //             TO WPAG-COD-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).setWpagCodSovrap(ws.getWkCodSovrap().getWkCodSovrap());
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //               END-IF
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: IF ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //              IS NUMERIC
                    //                TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                    //           END-IF
                    if (Functions.isNumber(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT())) {
                        // COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                        //             TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagImpSovrap().setWpagImpSovrap(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    }
                    break;

                default:break;
            }
            //--> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
            //    FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL
            // COB_CODE: SET WK-SPG-NON-TROVATA           TO TRUE
            ws.getWkRicSpg().setNonTrovata();
            // COB_CODE: PERFORM VARYING IX-APPO-SPG FROM 1 BY 1
            //             UNTIL IX-APPO-SPG > WK-SPG-MAX-A
            //                OR WK-SPG-TROVATA
            //             END-IF
            //           END-PERFORM
            ws.getIxIndici().setAppoSpg(((short)1));
            while (!(ws.getIxIndici().getAppoSpg() > ws.getWkSpgMaxA() || ws.getWkRicSpg().isTrovata())) {
                // COB_CODE: IF WSPG-COD-SOPR(IX-APPO-SPG) = WK-COD-SOVRAP
                //           AND WSPG-ID-GAR(IX-APPO-SPG) = WK-ID-GAR
                //           AND WSPG-VAL-PC-NULL(IX-APPO-SPG) NOT = HIGH-VALUES
                //             SET WK-SPG-TROVATA           TO TRUE
                //           END-IF
                if (Conditions.eq(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgCodSopr(), ws.getWkCodSovrap().getWkCodSovrap()) && wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgIdGar().getWspgIdGar() == ws.getWkVariabili().getWkIdGar() && !Characters.EQ_HIGH.test(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgValPc().getWspgValPcNullFormatted())) {
                    // COB_CODE: MOVE WSPG-VAL-PC-NULL(IX-APPO-SPG)
                    //            TO WPAG-PERC-SOVRAP-NULL(IX-AREA-PAGINA, IX-TAB-SPG)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagPercSovrap().setWpagPercSovrapNull(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgValPc().getWspgValPcNull());
                    // COB_CODE: SET WK-SPG-TROVATA           TO TRUE
                    ws.getWkRicSpg().setTrovata();
                }
                ws.getIxIndici().setAppoSpg(Trunc.toShort(ws.getIxIndici().getAppoSpg() + 1, 4));
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPPRO'
        //              END-PERFORM
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPPRO")) {
            // COB_CODE: ADD 1         TO IX-TAB-SPG
            ws.getIxIndici().setTabSpg(Trunc.toShort(1 + ws.getIxIndici().getTabSpg(), 4));
            // COB_CODE: SET WK-SOPPRO               TO TRUE
            ws.getWkCodSovrap().setSoppro();
            // COB_CODE: MOVE WK-COD-SOVRAP
            //             TO WPAG-COD-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).setWpagCodSovrap(ws.getWkCodSovrap().getWkCodSovrap());
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //               END-IF
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: IF ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //              IS NUMERIC
                    //                TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                    //           END-IF
                    if (Functions.isNumber(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT())) {
                        // COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                        //             TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagImpSovrap().setWpagImpSovrap(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    }
                    break;

                default:break;
            }
            //--> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
            //    FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL
            // COB_CODE: SET WK-SPG-NON-TROVATA           TO TRUE
            ws.getWkRicSpg().setNonTrovata();
            // COB_CODE: PERFORM VARYING IX-APPO-SPG FROM 1 BY 1
            //             UNTIL IX-APPO-SPG > WK-SPG-MAX-A
            //                OR WK-SPG-TROVATA
            //             END-IF
            //           END-PERFORM
            ws.getIxIndici().setAppoSpg(((short)1));
            while (!(ws.getIxIndici().getAppoSpg() > ws.getWkSpgMaxA() || ws.getWkRicSpg().isTrovata())) {
                // COB_CODE: IF WSPG-COD-SOPR(IX-APPO-SPG) = WK-COD-SOVRAP
                //           AND WSPG-ID-GAR(IX-APPO-SPG) = WK-ID-GAR
                //           AND WSPG-VAL-PC-NULL(IX-APPO-SPG) NOT = HIGH-VALUES
                //             SET WK-SPG-TROVATA           TO TRUE
                //           END-IF
                if (Conditions.eq(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgCodSopr(), ws.getWkCodSovrap().getWkCodSovrap()) && wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgIdGar().getWspgIdGar() == ws.getWkVariabili().getWkIdGar() && !Characters.EQ_HIGH.test(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgValPc().getWspgValPcNullFormatted())) {
                    // COB_CODE: MOVE WSPG-VAL-PC-NULL(IX-APPO-SPG)
                    //            TO WPAG-PERC-SOVRAP-NULL(IX-AREA-PAGINA, IX-TAB-SPG)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagPercSovrap().setWpagPercSovrapNull(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgValPc().getWspgValPcNull());
                    // COB_CODE: SET WK-SPG-TROVATA           TO TRUE
                    ws.getWkRicSpg().setTrovata();
                }
                ws.getIxIndici().setAppoSpg(Trunc.toShort(ws.getIxIndici().getAppoSpg() + 1, 4));
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSPO'
        //              END-PERFORM
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPSPO")) {
            // COB_CODE: ADD 1         TO IX-TAB-SPG
            ws.getIxIndici().setTabSpg(Trunc.toShort(1 + ws.getIxIndici().getTabSpg(), 4));
            // COB_CODE: SET WK-SOPSPO               TO TRUE
            ws.getWkCodSovrap().setSopspo();
            // COB_CODE: MOVE WK-COD-SOVRAP
            //             TO WPAG-COD-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).setWpagCodSovrap(ws.getWkCodSovrap().getWkCodSovrap());
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //               END-IF
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: IF ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //              IS NUMERIC
                    //                TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                    //           END-IF
                    if (Functions.isNumber(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT())) {
                        // COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                        //             TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagImpSovrap().setWpagImpSovrap(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    }
                    break;

                default:break;
            }
            //--> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
            //    FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL
            // COB_CODE: SET WK-SPG-NON-TROVATA           TO TRUE
            ws.getWkRicSpg().setNonTrovata();
            // COB_CODE: PERFORM VARYING IX-APPO-SPG FROM 1 BY 1
            //             UNTIL IX-APPO-SPG > WK-SPG-MAX-A
            //                OR WK-SPG-TROVATA
            //             END-IF
            //           END-PERFORM
            ws.getIxIndici().setAppoSpg(((short)1));
            while (!(ws.getIxIndici().getAppoSpg() > ws.getWkSpgMaxA() || ws.getWkRicSpg().isTrovata())) {
                // COB_CODE: IF WSPG-COD-SOPR(IX-APPO-SPG) = WK-COD-SOVRAP
                //           AND WSPG-ID-GAR(IX-APPO-SPG) = WK-ID-GAR
                //           AND WSPG-VAL-PC-NULL(IX-APPO-SPG) NOT = HIGH-VALUES
                //             SET WK-SPG-TROVATA           TO TRUE
                //           END-IF
                if (Conditions.eq(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgCodSopr(), ws.getWkCodSovrap().getWkCodSovrap()) && wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgIdGar().getWspgIdGar() == ws.getWkVariabili().getWkIdGar() && !Characters.EQ_HIGH.test(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgValPc().getWspgValPcNullFormatted())) {
                    // COB_CODE: MOVE WSPG-VAL-PC-NULL(IX-APPO-SPG)
                    //            TO WPAG-PERC-SOVRAP-NULL(IX-AREA-PAGINA, IX-TAB-SPG)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagPercSovrap().setWpagPercSovrapNull(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgValPc().getWspgValPcNull());
                    // COB_CODE: SET WK-SPG-TROVATA           TO TRUE
                    ws.getWkRicSpg().setTrovata();
                }
                ws.getIxIndici().setAppoSpg(Trunc.toShort(ws.getIxIndici().getAppoSpg() + 1, 4));
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPTEC'
        //              END-PERFORM
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPTEC")) {
            // COB_CODE: ADD 1         TO IX-TAB-SPG
            ws.getIxIndici().setTabSpg(Trunc.toShort(1 + ws.getIxIndici().getTabSpg(), 4));
            // COB_CODE: SET WK-SOPTEC               TO TRUE
            ws.getWkCodSovrap().setSoptec();
            // COB_CODE: MOVE WK-COD-SOVRAP
            //             TO WPAG-COD-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).setWpagCodSovrap(ws.getWkCodSovrap().getWkCodSovrap());
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //               END-IF
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: IF ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //              IS NUMERIC
                    //                TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                    //           END-IF
                    if (Functions.isNumber(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT())) {
                        // COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                        //             TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagImpSovrap().setWpagImpSovrap(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    }
                    break;

                default:break;
            }
            //--> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
            //    FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL
            // COB_CODE: SET WK-SPG-NON-TROVATA           TO TRUE
            ws.getWkRicSpg().setNonTrovata();
            // COB_CODE: PERFORM VARYING IX-APPO-SPG FROM 1 BY 1
            //             UNTIL IX-APPO-SPG > WK-SPG-MAX-A
            //                OR WK-SPG-TROVATA
            //             END-IF
            //           END-PERFORM
            ws.getIxIndici().setAppoSpg(((short)1));
            while (!(ws.getIxIndici().getAppoSpg() > ws.getWkSpgMaxA() || ws.getWkRicSpg().isTrovata())) {
                // COB_CODE: IF WSPG-COD-SOPR(IX-APPO-SPG) = WK-COD-SOVRAP
                //           AND WSPG-ID-GAR(IX-APPO-SPG) = WK-ID-GAR
                //           AND WSPG-VAL-PC-NULL(IX-APPO-SPG) NOT = HIGH-VALUES
                //             SET WK-SPG-TROVATA           TO TRUE
                //           END-IF
                if (Conditions.eq(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgCodSopr(), ws.getWkCodSovrap().getWkCodSovrap()) && wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgIdGar().getWspgIdGar() == ws.getWkVariabili().getWkIdGar() && !Characters.EQ_HIGH.test(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgValPc().getWspgValPcNullFormatted())) {
                    // COB_CODE: MOVE WSPG-VAL-PC-NULL(IX-APPO-SPG)
                    //            TO WPAG-PERC-SOVRAP-NULL(IX-AREA-PAGINA, IX-TAB-SPG)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagPercSovrap().setWpagPercSovrapNull(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgValPc().getWspgValPcNull());
                    // COB_CODE: SET WK-SPG-TROVATA           TO TRUE
                    ws.getWkRicSpg().setTrovata();
                }
                ws.getIxIndici().setAppoSpg(Trunc.toShort(ws.getIxIndici().getAppoSpg() + 1, 4));
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPALT'
        //              END-PERFORM
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPALT")) {
            // COB_CODE: ADD 1         TO IX-TAB-SPG
            ws.getIxIndici().setTabSpg(Trunc.toShort(1 + ws.getIxIndici().getTabSpg(), 4));
            // COB_CODE: SET WK-SOPALT               TO TRUE
            ws.getWkCodSovrap().setSopalt();
            // COB_CODE: MOVE WK-COD-SOVRAP
            //             TO WPAG-COD-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).setWpagCodSovrap(ws.getWkCodSovrap().getWkCodSovrap());
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //               END-IF
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: IF ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //              IS NUMERIC
                    //                TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                    //           END-IF
                    if (Functions.isNumber(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT())) {
                        // COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                        //             TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagImpSovrap().setWpagImpSovrap(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    }
                    break;

                default:break;
            }
            //--> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
            //    FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL
            // COB_CODE: SET WK-SPG-NON-TROVATA           TO TRUE
            ws.getWkRicSpg().setNonTrovata();
            // COB_CODE: PERFORM VARYING IX-APPO-SPG FROM 1 BY 1
            //             UNTIL IX-APPO-SPG > WK-SPG-MAX-A
            //                OR WK-SPG-TROVATA
            //             END-IF
            //           END-PERFORM
            ws.getIxIndici().setAppoSpg(((short)1));
            while (!(ws.getIxIndici().getAppoSpg() > ws.getWkSpgMaxA() || ws.getWkRicSpg().isTrovata())) {
                // COB_CODE: IF WSPG-COD-SOPR(IX-APPO-SPG) = WK-COD-SOVRAP
                //           AND WSPG-ID-GAR(IX-APPO-SPG) = WK-ID-GAR
                //           AND WSPG-VAL-PC-NULL(IX-APPO-SPG) NOT = HIGH-VALUES
                //             SET WK-SPG-TROVATA           TO TRUE
                //           END-IF
                if (Conditions.eq(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgCodSopr(), ws.getWkCodSovrap().getWkCodSovrap()) && wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgIdGar().getWspgIdGar() == ws.getWkVariabili().getWkIdGar() && !Characters.EQ_HIGH.test(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgValPc().getWspgValPcNullFormatted())) {
                    // COB_CODE: MOVE WSPG-VAL-PC-NULL(IX-APPO-SPG)
                    //            TO WPAG-PERC-SOVRAP-NULL(IX-AREA-PAGINA, IX-TAB-SPG)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiSoprGar(ws.getIxIndici().getTabSpg()).getWpagPercSovrap().setWpagPercSovrapNull(wspgAreaSoprGar.getTabSpg(ws.getIxIndici().getAppoSpg()).getLccvspg1().getDati().getWspgValPc().getWspgValPcNull());
                    // COB_CODE: SET WK-SPG-TROVATA           TO TRUE
                    ws.getWkRicSpg().setTrovata();
                }
                ws.getIxIndici().setAppoSpg(Trunc.toShort(ws.getIxIndici().getAppoSpg() + 1, 4));
            }
        }
        // COB_CODE: MOVE IX-TAB-SPG   TO WPAG-ELE-SOPR-GAR-MAX(IX-AREA-PAGINA).
        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).setWpagEleSoprGarMax(ws.getIxIndici().getTabSpg());
    }

    /**Original name: S14200-PREMIO-RATA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DEI DATI TITOLO DI RATA DELL'AREA DI PAGINA
	 *     PER PREMIO RATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s14200PremioRata() {
        // COB_CODE: MOVE 1           TO WPAG-ELE-TIT-RATA-MAX(IX-AREA-PAGINA)
        //                               IX-TAB-RATA.
        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).setWpagEleTitRataMax(((short)1));
        ws.getIxIndici().setTabRata(((short)1));
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PNETTO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PNETTO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //               TO WPAG-RATA-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //               TO WPAG-RATA-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //               TO WPAG-RATA-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-RATA-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioNetto().setWpagRataPremioNettoFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-RATA-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioNetto().setWpagRataPremioNetto(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-RATA-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioNetto().setWpagRataPremioNetto(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'INTFRAZ'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "INTFRAZ")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-RATA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-RATA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-RATA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIntFraz().setWpagRataIntFrazFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIntFraz().setWpagRataIntFraz(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIntFraz().setWpagRataIntFraz(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'INTRET'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "INTRET")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                TO WPAG-RATA-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                TO WPAG-RATA-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                TO WPAG-RATA-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIntRetrodt().setWpagRataIntRetrodtFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIntRetrodt().setWpagRataIntRetrodt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIntRetrodt().setWpagRataIntRetrodt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIREMI'
        //           *       IF WK-SI-PRIMO-RATA-DIR
        //                    TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DIREMI")) {
            //       IF WK-SI-PRIMO-RATA-DIR
            // COB_CODE:         IF WPAG-RATA-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
            //                                                                 = HIGH-VALUE
            //                        TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
            //           *          SET WK-NO-PRIMO-RATA-DIR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().getWpagRataDirittiNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().setWpagRataDiritti(new AfDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-RATA-DIR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().setWpagRataDiritti(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().getWpagRataDiritti()), 15, 3));
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRQUI'
        //           *       IF WK-SI-PRIMO-RATA-DIR
        //                    TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DIRQUI")) {
            //       IF WK-SI-PRIMO-RATA-DIR
            // COB_CODE:         IF WPAG-RATA-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
            //                                                                 = HIGH-VALUE
            //                        TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
            //           *          SET WK-NO-PRIMO-RATA-DIR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().getWpagRataDirittiNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().setWpagRataDiritti(new AfDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-RATA-DIR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().setWpagRataDiritti(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().getWpagRataDiritti()), 15, 3));
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRVIS'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DIRVIS")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //              TO WPAG-RATA-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //              TO WPAG-RATA-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //              TO WPAG-RATA-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-RATA-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSpeseMediche().setWpagRataSpeseMedicheFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-RATA-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSpeseMediche().setWpagRataSpeseMediche(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-RATA-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSpeseMediche().setWpagRataSpeseMediche(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRAGG'
        //               TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DIRAGG")) {
            // COB_CODE: IF WPAG-RATA-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
            //            = HIGH-VALUE
            //                TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().getWpagRataDirittiNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().setWpagRataDiritti(new AfDecimal(0, 15, 3));
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().setWpagRataDiritti(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataDiritti().getWpagRataDiritti()), 15, 3));
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPPRE'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPPRE")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-RATA-TASSE(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-RATA-TASSE(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-RATA-TASSE(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-TASSE(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataTasse().setWpagRataTasseFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-TASSE(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataTasse().setWpagRataTasse(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-TASSE(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataTasse().setWpagRataTasse(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSAN'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPSAN")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-RATA-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-RATA-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-RATA-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprSanit().setWpagRataSoprSanitFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprSanit().setWpagRataSoprSanit(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprSanit().setWpagRataSoprSanit(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPPRO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPPRO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                TO WPAG-RATA-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                TO WPAG-RATA-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                TO WPAG-RATA-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprProfes().setWpagRataSoprProfesFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprProfes().setWpagRataSoprProfes(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprProfes().setWpagRataSoprProfes(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSPO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPSPO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-RATA-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-RATA-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-RATA-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprSport().setWpagRataSoprSportFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprSport().setWpagRataSoprSport(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprSport().setWpagRataSoprSport(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPTEC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPTEC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-RATA-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-RATA-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-RATA-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprTecn().setWpagRataSoprTecnFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprTecn().setWpagRataSoprTecn(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataSoprTecn().setWpagRataSoprTecn(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPALT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPALT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-RATA-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-RATA-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-RATA-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAltriSopr().setWpagRataAltriSoprFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAltriSopr().setWpagRataAltriSopr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAltriSopr().setWpagRataAltriSopr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PRETOT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PRETOT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-RATA-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-RATA-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-RATA-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioTot().setWpagRataPremioTotFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioTot().setWpagRataPremioTot(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioTot().setWpagRataPremioTot(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PPUROI'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PPUROI")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                    (IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                   (IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                   (IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //                (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-PREMIO-PURO-IAS
                    //                (IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioPuroIas().setWpagRataPremioPuroIasFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-PREMIO-PURO-IAS
                    //               (IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioPuroIas().setWpagRataPremioPuroIas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-PREMIO-PURO-IAS
                    //               (IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioPuroIas().setWpagRataPremioPuroIas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PCASOM'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PCASOM")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                TO WPAG-RATA-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                TO WPAG-RATA-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                TO WPAG-RATA-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioRisc().setWpagRataPremioRiscFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioRisc().setWpagRataPremioRisc(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataPremioRisc().setWpagRataPremioRisc(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARACQ'
        //           *       IF WK-SI-PRIMO-RATA-CAR
        //                    TO WPAG-IMP-CAR-ACQ-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CARACQ")) {
            //       IF WK-SI-PRIMO-RATA-CAR
            // COB_CODE:         IF WPAG-IMP-CAR-ACQ-TDR-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
            //                    = HIGH-VALUE
            //                        TO WPAG-IMP-CAR-ACQ-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
            //           *          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarAcqTdr().getWpagImpCarAcqTdrNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-IMP-CAR-ACQ-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarAcqTdr().setWpagImpCarAcqTdr(new AfDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-IMP-CAR-ACQ-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarAcqTdr().setWpagImpCarAcqTdr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarAcqTdr().getWpagImpCarAcqTdr()), 15, 3));
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ACQEXP'
        //           *       IF WK-SI-PRIMO-RATA-CAR
        //                    TO WPAG-RATA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-RATA)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ACQEXP")) {
            //       IF WK-SI-PRIMO-RATA-CAR
            // COB_CODE:         IF WPAG-RATA-IMP-ACQ-EXP-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
            //                    = HIGH-VALUE
            //                        TO WPAG-RATA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-RATA)
            //           *          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpAcqExp().getWpagRataImpAcqExpNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-RATA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-RATA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpAcqExp().setWpagRataImpAcqExp(new AfDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-RATA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-RATA)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpAcqExp().setWpagRataImpAcqExp(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpAcqExp().getWpagRataImpAcqExp()), 15, 3));
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARINC'
        //           *       IF WK-SI-PRIMO-RATA-CAR
        //                    TO WPAG-IMP-CAR-INC-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CARINC")) {
            //       IF WK-SI-PRIMO-RATA-CAR
            // COB_CODE:         IF WPAG-IMP-CAR-INC-TDR-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
            //                    = HIGH-VALUE
            //                        TO WPAG-IMP-CAR-INC-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
            //           *          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarIncTdr().getWpagImpCarIncTdrNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-IMP-CAR-INC-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarIncTdr().setWpagImpCarIncTdr(new AfDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-IMP-CAR-INC-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarIncTdr().setWpagImpCarIncTdr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarIncTdr().getWpagImpCarIncTdr()), 15, 3));
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARGES'
        //           *       IF WK-SI-PRIMO-RATA-CAR
        //                    TO WPAG-IMP-CAR-GEST-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CARGES")) {
            //       IF WK-SI-PRIMO-RATA-CAR
            // COB_CODE:         IF WPAG-IMP-CAR-GEST-TDR-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
            //                    = HIGH-VALUE
            //                        TO WPAG-IMP-CAR-GEST-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
            //           *          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarGestTdr().getWpagImpCarGestTdrNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-IMP-CAR-GEST-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarGestTdr().setWpagImpCarGestTdr(new AfDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-IMP-CAR-GEST-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarGestTdr().setWpagImpCarGestTdr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpCarGestTdr().getWpagImpCarGestTdr()), 15, 3));
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ1A'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVACQ1A")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                TO WPAG-RATA-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                TO WPAG-RATA-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                TO WPAG-RATA-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAcq1oAnno().setWpagRataAcq1oAnnoFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAcq1oAnno().setWpagRataAcq1oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAcq1oAnno().setWpagRataAcq1oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ2A'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVACQ2A")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                TO WPAG-RATA-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                TO WPAG-RATA-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                TO WPAG-RATA-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAcq2oAnno().setWpagRataAcq2oAnnoFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAcq2oAnno().setWpagRataAcq2oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-RATA-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataAcq2oAnno().setWpagRataAcq2oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVRIC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVRIC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-RATA-RICOR(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-RATA-RICOR(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-RATA-RICOR(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-RICOR(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataRicor().setWpagRataRicorFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-RICOR(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataRicor().setWpagRataRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-RICOR(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataRicor().setWpagRataRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVINC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVINC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-RATA-INCAS(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-RATA-INCAS(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-RATA-INCAS(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-INCAS(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIncas().setWpagRataIncasFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-INCAS(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIncas().setWpagRataIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-INCAS(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataIncas().setWpagRataIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO AZIENDA (FONTI CONTRIBUTIVE)
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //            'IMPAZIENDA'
        //             END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPAZIENDA")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-AZ-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-AZ-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpAzTdr().setWpagImpAzTdr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO ADERENTE (FONTI CONTRIBUTIVE)
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //            'IMPADERENTE'
        //             END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPADERENTE")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-ADER-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-ADER-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpAderTdr().setWpagImpAderTdr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO TFR (FONTI CONTRIBUTIVE)
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPTFR'
        //             END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPTFR")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-TFR-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-TFR-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpTfrTdr().setWpagImpTfrTdr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO VOLONTARIO (FONTI CONTRIBUTIVE)
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //            'IMPVOLONT'
        //             END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPVOLONT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-VOLO-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-VOLO-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpVoloTdr().setWpagImpVoloTdr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        //--> IMPORTO INTERESSI DI RIATTIVAZIONE
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //            'INTRIA'
        //             END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "INTRIA")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-INT-RIATT(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-INT-RIATT(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagImpIntRiatt().setWpagImpIntRiatt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //             'REMUNASS'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "REMUNASS")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                   (IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                   (IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                   (IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-IMP-REN-ASS
                    //               (IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpRenAss().setWpagRataImpRenAssFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-IMP-REN-ASS
                    //               (IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpRenAss().setWpagRataImpRenAss(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-IMP-REN-ASS
                    //               (IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpRenAss().setWpagRataImpRenAss(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //             'COMMINTER'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "COMMINTER")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                   (IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                   (IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                   (IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-IMP-COMMIS-INT
                    //               (IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpCommisInt().setWpagRataImpCommisIntFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-IMP-COMMIS-INT
                    //               (IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpCommisInt().setWpagRataImpCommisInt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-IMP-COMMIS-INT
                    //               (IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).getWpagRataImpCommisInt().setWpagRataImpCommisInt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //                                                          'CTRANTRACKET'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CTRANTRACKET")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-RATA-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-RATA-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-RATA-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).setWpagRataAntiracketFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).setWpagRataAntiracket(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-RATA-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitRata(ws.getIxIndici().getTabRata()).setWpagRataAntiracket(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
    }

    /**Original name: S14300-PREMIO-FIRMA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DEI DATI DETTAGLIO TITOLO CONTABILE
	 *     DELL'AREA DI PAGINA PER PREMIO FIRMA
	 * ----------------------------------------------------------------*</pre>*/
    private void s14300PremioFirma() {
        // COB_CODE: MOVE 1           TO WPAG-ELE-TIT-CONT-MAX(IX-AREA-PAGINA)
        //                               IX-TAB-TIT.
        wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).setWpagEleTitContMax(((short)1));
        ws.getIxIndici().setTabTit(((short)1));
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PNETTO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PNETTO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                TO WPAG-CONT-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                TO WPAG-CONT-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                TO WPAG-CONT-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-CONT-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioNetto().setWpagContPremioNettoFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-CONT-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioNetto().setWpagContPremioNetto(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //            TO WPAG-CONT-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioNetto().setWpagContPremioNetto(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'INTFRAZ'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "INTFRAZ")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIntFraz().setWpagContIntFrazFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIntFraz().setWpagContIntFraz(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIntFraz().setWpagContIntFraz(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'INTRET'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "INTRET")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIntRetrodt().setWpagContIntRetrodtFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIntRetrodt().setWpagContIntRetrodt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIntRetrodt().setWpagContIntRetrodt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIREMI'
        //           *       IF WK-SI-PRIMO-CONT-DIR
        //                    TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DIREMI")) {
            //       IF WK-SI-PRIMO-CONT-DIR
            // COB_CODE:         IF WPAG-CONT-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
            //                                                                 = HIGH-VALUE
            //                        TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
            //           *          SET WK-NO-PRIMO-CONT-DIR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().getWpagContDirittiNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().setWpagContDiritti(new AfDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-CONT-DIR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().setWpagContDiritti(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().getWpagContDiritti()), 15, 3));
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRQUI'
        //           *       IF WK-SI-PRIMO-CONT-DIR
        //                    TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DIRQUI")) {
            //       IF WK-SI-PRIMO-CONT-DIR
            // COB_CODE:         IF WPAG-CONT-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
            //                                                                 = HIGH-VALUE
            //                        TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
            //           *          SET WK-NO-PRIMO-CONT-DIR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().getWpagContDirittiNullFormatted())) {
                // COB_CODE: MOVE 0
                //             TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().setWpagContDiritti(Trunc.toDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-CONT-DIR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().setWpagContDiritti(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().getWpagContDiritti()), 15, 3));
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRVIS'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DIRVIS")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //               TO WPAG-CONT-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //               TO WPAG-CONT-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //               TO WPAG-CONT-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-CONT-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSpeseMediche().setWpagContSpeseMedicheFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-CONT-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSpeseMediche().setWpagContSpeseMediche(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-CONT-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSpeseMediche().setWpagContSpeseMediche(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRAGG'
        //               TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "DIRAGG")) {
            // COB_CODE: IF WPAG-CONT-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
            //            = HIGH-VALUE
            //                TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().getWpagContDirittiNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().setWpagContDiritti(new AfDecimal(0, 15, 3));
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().setWpagContDiritti(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContDiritti().getWpagContDiritti()), 15, 3));
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPPRE'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPPRE")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-TASSE(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-TASSE(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-TASSE(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-TASSE(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContTasse().setWpagContTasseFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-TASSE(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContTasse().setWpagContTasse(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-TASSE(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContTasse().setWpagContTasse(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSAN'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPSAN")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprSanit().setWpagContSoprSanitFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprSanit().setWpagContSoprSanit(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprSanit().setWpagContSoprSanit(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPPRO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPPRO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprProfes().setWpagContSoprProfesFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprProfes().setWpagContSoprProfes(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprProfes().setWpagContSoprProfes(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSPO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPSPO")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprSport().setWpagContSoprSportFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprSport().setWpagContSoprSport(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprSport().setWpagContSoprSport(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPTEC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPTEC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprTecn().setWpagContSoprTecnFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprTecn().setWpagContSoprTecn(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContSoprTecn().setWpagContSoprTecn(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPALT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "SOPALT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAltriSopr().setWpagContAltriSoprFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAltriSopr().setWpagContAltriSopr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAltriSopr().setWpagContAltriSopr(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PRETOT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PRETOT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioTot().setWpagContPremioTotFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioTot().setWpagContPremioTot(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioTot().setWpagContPremioTot(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PPUROI'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PPUROI")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //             TO WPAG-CONT-PREMIO-PURO-IAS(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //             TO WPAG-CONT-PREMIO-PURO-IAS(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //             TO WPAG-CONT-PREMIO-PURO-IAS(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE:   MOVE ISPC0140-COMP-VALORE-STR-T
                    //                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-CONT-PREMIO-PURO-IAS(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioPuroIas().setWpagContPremioPuroIasFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE:   MOVE ISPC0140-COMP-VALORE-IMP-T
                    //                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-CONT-PREMIO-PURO-IAS(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioPuroIas().setWpagContPremioPuroIas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE:   MOVE ISPC0140-COMP-VALORE-PERC-T
                    //                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //           TO WPAG-CONT-PREMIO-PURO-IAS(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioPuroIas().setWpagContPremioPuroIas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PCASOM'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PCASOM")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioRisc().setWpagContPremioRiscFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioRisc().setWpagContPremioRisc(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContPremioRisc().setWpagContPremioRisc(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARACQ'
        //           *       IF WK-SI-PRIMO-CONT-CAR
        //                    TO WPAG-IMP-CAR-ACQ-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CARACQ")) {
            //       IF WK-SI-PRIMO-CONT-CAR
            // COB_CODE:         IF WPAG-IMP-CAR-ACQ-TIT-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
            //                    = HIGH-VALUE
            //                        TO WPAG-IMP-CAR-ACQ-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
            //           *          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarAcqTit().getWpagImpCarAcqTitNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-IMP-CAR-ACQ-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarAcqTit().setWpagImpCarAcqTit(new AfDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-IMP-CAR-ACQ-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarAcqTit().setWpagImpCarAcqTit(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarAcqTit().getWpagImpCarAcqTit()), 15, 3));
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ACQEXP'
        //           *       IF WK-SI-PRIMO-CONT-CAR
        //                    TO WPAG-CONT-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TIT)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "ACQEXP")) {
            //       IF WK-SI-PRIMO-CONT-CAR
            // COB_CODE:         IF WPAG-CONT-IMP-ACQ-EXP-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
            //                    = HIGH-VALUE
            //                        TO WPAG-CONT-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TIT)
            //           *          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpAcqExp().getWpagContImpAcqExpNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-CONT-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TIT)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpAcqExp().setWpagContImpAcqExp(new AfDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-CONT-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TIT)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpAcqExp().setWpagContImpAcqExp(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpAcqExp().getWpagContImpAcqExp()), 15, 3));
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARINC'
        //           *       IF WK-SI-PRIMO-CONT-CAR
        //                    TO WPAG-IMP-CAR-INC-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CARINC")) {
            //       IF WK-SI-PRIMO-CONT-CAR
            // COB_CODE:         IF WPAG-IMP-CAR-INC-TIT-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
            //                    = HIGH-VALUE
            //                        TO WPAG-IMP-CAR-INC-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
            //           *          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarIncTit().getWpagImpCarIncTitNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-IMP-CAR-INC-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarIncTit().setWpagImpCarIncTit(new AfDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-IMP-CAR-INC-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarIncTit().setWpagImpCarIncTit(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarIncTit().getWpagImpCarIncTit()), 15, 3));
        }
        // COB_CODE:      IF ISPC0140-CODICE-COMPONENTE-T
        //                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARGES'
        //           *       IF WK-SI-PRIMO-CONT-CAR
        //                    TO WPAG-IMP-CAR-GEST-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CARGES")) {
            //       IF WK-SI-PRIMO-CONT-CAR
            // COB_CODE:         IF WPAG-IMP-CAR-GEST-TIT-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
            //                    = HIGH-VALUE
            //                        TO WPAG-IMP-CAR-GEST-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
            //           *          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarGestTit().getWpagImpCarGestTitNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO WPAG-IMP-CAR-GEST-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
                wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarGestTit().setWpagImpCarGestTit(new AfDecimal(0, 15, 3));
                //          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
            }
            // COB_CODE: ADD ISPC0140-COMP-VALORE-IMP-T
            //              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //            TO WPAG-IMP-CAR-GEST-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
            wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarGestTit().setWpagImpCarGestTit(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT().add(wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpCarGestTit().getWpagImpCarGestTit()), 15, 3));
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ1A'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVACQ1A")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAcq1oAnno().setWpagContAcq1oAnnoFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAcq1oAnno().setWpagContAcq1oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAcq1oAnno().setWpagContAcq1oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ2A'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVACQ2A")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAcq2oAnno().setWpagContAcq2oAnnoFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAcq2oAnno().setWpagContAcq2oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContAcq2oAnno().setWpagContAcq2oAnno(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVRIC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVRIC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-RICOR(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-RICOR(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-RICOR(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-RICOR(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContRicor().setWpagContRicorFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-RICOR(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContRicor().setWpagContRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-RICOR(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContRicor().setWpagContRicor(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVINC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "PROVINC")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-INCAS(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-INCAS(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-INCAS(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-INCAS(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIncas().setWpagContIncasFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-INCAS(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIncas().setWpagContIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-INCAS(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContIncas().setWpagContIncas(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //             'IMPAZIENDA'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPAZIENDA")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-AZ-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-AZ-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpAzTit().setWpagImpAzTit(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //             'IMPADERENTE'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPADERENTE")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-ADER-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-ADER-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpAderTit().setWpagImpAderTit(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPTFR'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPTFR")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-TFR-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-TFR-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpTfrTit().setWpagImpTfrTit(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //             'IMPVOLONT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "IMPVOLONT")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-IMP-VOLO-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-IMP-VOLO-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagImpVoloTit().setWpagImpVoloTit(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //             'REMUNASS'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "REMUNASS")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-IMP-REN-ASS(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-IMP-REN-ASS(IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-IMP-REN-ASS(IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-IMP-REN-ASS(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpRenAss().setWpagContImpRenAssFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-IMP-REN-ASS(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpRenAss().setWpagContImpRenAss(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-IMP-REN-ASS(IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpRenAss().setWpagContImpRenAss(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //             'COMMINTER'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "COMMINTER")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                    (IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'N'
            //             WHEN 'I'
            //                    (IX-AREA-PAGINA, IX-TAB-TIT)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                    (IX-AREA-PAGINA, IX-TAB-TIT)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-IMP-COMMIS-INT
                    //                (IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpCommisInt().setWpagContImpCommisIntFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-IMP-COMMIS-INT
                    //                (IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpCommisInt().setWpagContImpCommisInt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-IMP-COMMIS-INT
                    //                (IX-AREA-PAGINA, IX-TAB-TIT)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabTit()).getWpagContImpCommisInt().setWpagContImpCommisInt(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
        // COB_CODE: IF ISPC0140-CODICE-COMPONENTE-T
        //             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
        //                                                          'CTRANTRACKET'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CodiceComponenteT(), "CTRANTRACKET")) {
            // COB_CODE: EVALUATE ISPC0140-COMP-TIPO-DATO-T
            //                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            //             WHEN 'S'
            //             WHEN 'D'
            //                 TO WPAG-CONT-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'N'
            //             WHEN 'I'
            //                 TO WPAG-CONT-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
            //             WHEN 'P'
            //             WHEN 'M'
            //             WHEN 'A'
            //                 TO WPAG-CONT-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
            //           END-EVALUATE
            switch (ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompTipoDatoT()) {

                case 'S':
                case 'D':// COB_CODE: MOVE ISPC0140-COMP-VALORE-STR-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabRata()).setWpagContAntiracketFormatted(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreStrTFormatted());
                    break;

                case 'N':
                case 'I':// COB_CODE: MOVE ISPC0140-COMP-VALORE-IMP-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabRata()).setWpagContAntiracket(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValoreImpT(), 15, 3));
                    break;

                case 'P':
                case 'M':
                case 'A':// COB_CODE: MOVE ISPC0140-COMP-VALORE-PERC-T
                    //               (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    //             TO WPAG-CONT-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getAreaPagina()).getWpagDatiTitCont(ws.getIxIndici().getTabRata()).setWpagContAntiracket(Trunc.toDecimal(ws.getAreaIoCalcContr().getCalcoliT(ws.getIxIndici().getAreaIspc0140()).getAreaTpPremioT(ws.getIxIndici().getTpPremio()).getComponenteT(ws.getIxIndici().getComponente()).getIspc0140CompValGenericoT().getIspc0140CompValorePercT(), 15, 3));
                    break;

                default:break;
            }
        }
    }

    /**Original name: S14100-AREA-PAGINA-IAS-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELLA AREA PAGINA IAS
	 * ----------------------------------------------------------------*</pre>*/
    private void s14100AreaPaginaIasGar() {
        // COB_CODE: IF WGRZ-COD-TARI(IX-PAG-IAS) = WK-COD-GAR(IX-PAG-IAS)
        //              END-IF
        //           END-IF.
        if (Conditions.eq(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getPagIas()).getLccvgrz1().getDati().getWgrzCodTari(), ws.getWkVariabili().getWkTabGar(ws.getIxIndici().getPagIas()).getCodGar())) {
            // COB_CODE: ADD  1                      TO WPAG-ELE-MAX-IAS
            wpagAreaIas.setEleMaxIas(Trunc.toShort(1 + wpagAreaIas.getEleMaxIas(), 4));
            // COB_CODE: MOVE WGRZ-ID-GAR(IX-PAG-IAS)
            //             TO WPAG-ID-OGG(WPAG-ELE-MAX-IAS)
            wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).setIdOgg(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getPagIas()).getLccvgrz1().getDati().getWgrzIdGar());
            // COB_CODE: MOVE 'GA'                 TO WPAG-TP-OGG(WPAG-ELE-MAX-IAS)
            wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).setTpOgg("GA");
            // COB_CODE: MOVE WGRZ-TP-IAS(IX-PAG-IAS)
            //             TO WPAG-TP-IAS-OLD(WPAG-ELE-MAX-IAS)
            wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).setTpIasOld(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getPagIas()).getLccvgrz1().getDati().getWgrzTpIas());
            // COB_CODE: MOVE WK-TP-IAS(IX-PAG-IAS)
            //             TO WPAG-TP-IAS-NEW(WPAG-ELE-MAX-IAS)
            wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).setTpIasNew(ws.getWkVariabili().getWkTabGar(ws.getIxIndici().getPagIas()).getTpIas());
            // COB_CODE: IF WPAG-TP-IAS-OLD(WPAG-ELE-MAX-IAS)  =
            //              WPAG-TP-IAS-NEW(WPAG-ELE-MAX-IAS)
            //              SET WPAG-NO-MOD-IAS(WPAG-ELE-MAX-IAS)         TO  TRUE
            //           ELSE
            //              SET WPAG-SI-MOD-IAS(WPAG-ELE-MAX-IAS)         TO  TRUE
            //           END-IF
            if (Conditions.eq(wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).getTpIasOld(), wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).getTpIasNew())) {
                // COB_CODE: SET WPAG-NO-MOD-IAS(WPAG-ELE-MAX-IAS)         TO  TRUE
                wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).getModIas().setNoModIas();
            }
            else {
                // COB_CODE: SET WPAG-SI-MOD-IAS(WPAG-ELE-MAX-IAS)         TO  TRUE
                wpagAreaIas.getTabIas(wpagAreaIas.getEleMaxIas()).getModIas().setSiModIas();
            }
        }
    }

    /**Original name: S14500-GESTIONE-FONDI<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA DI PAGINA TAB ASSET
	 * ----------------------------------------------------------------*</pre>*/
    private void s14500GestioneFondi() {
        // COB_CODE: MOVE ZERO TO IX-AREA-PAGINA.
        ws.getIxIndici().setAreaPagina(((short)0));
        //
        // COB_CODE: IF WALL-ELE-ASSET-ALL-MAX EQUAL ZERO
        //              PERFORM S14501-GESTIONE-FONDI-TEMP THRU EX-S14501
        //           ELSE
        //              PERFORM S14502-GESTIONE-FONDI      THRU EX-S14502
        //           END-IF.
        if (wallAreaAsset.getEleAssetAllMax() == 0) {
            // COB_CODE: PERFORM S14501-GESTIONE-FONDI-TEMP THRU EX-S14501
            s14501GestioneFondiTemp();
        }
        else {
            // COB_CODE: PERFORM S14502-GESTIONE-FONDI      THRU EX-S14502
            s14502GestioneFondi();
        }
    }

    /**Original name: S14501-GESTIONE-FONDI-TEMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA DI PAGINA TAB ASSET
	 * ----------------------------------------------------------------*</pre>*/
    private void s14501GestioneFondiTemp() {
        // COB_CODE: PERFORM S14503-IMPOSTA-TEMP      THRU EX-S14503.
        s14503ImpostaTemp();
        // COB_CODE: PERFORM UNTIL NOT IDSV0301-ESITO-OK
        //                      OR NOT IDSV0301-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (!(!ws.getIdsv0301().getEsito().isOk() || !ws.getIdsv0301().getSqlcodeSigned().isSuccessfulSql())) {
            // COB_CODE: PERFORM GESTIONE-TEMP-TABLE
            //              THRU GESTIONE-TEMP-TABLE-EX
            gestioneTempTable();
            // COB_CODE: IF IDSV0301-ESITO-OK
            //              END-IF
            //           END-IF
            if (ws.getIdsv0301().getEsito().isOk()) {
                // COB_CODE: IF IDSV0301-SUCCESSFUL-SQL
                //              INITIALIZE WALL-AREA-ASSET
                //           END-IF
                if (ws.getIdsv0301().getSqlcodeSigned().isSuccessfulSql()) {
                    // COB_CODE: PERFORM GESTIONE-ELE-MAX-TEMP
                    //              THRU GESTIONE-ELE-MAX-TEMP-EX
                    gestioneEleMaxTemp();
                    // COB_CODE: MOVE IDSV0303-ELE-MAX-TOT
                    //             TO WALL-ELE-ASSET-ALL-MAX
                    wallAreaAsset.setEleAssetAllMax(Trunc.toShort(ws.getIdsv0303().getIdsv0303EleMaxTot(), 4));
                    // COB_CODE: PERFORM S14502-GESTIONE-FONDI
                    //              THRU EX-S14502
                    s14502GestioneFondi();
                    // COB_CODE: INITIALIZE WALL-AREA-ASSET
                    initWallAreaAsset();
                }
            }
        }
    }

    /**Original name: S14503-IMPOSTA-TEMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     IMPOSTA AREA PER CALL TEMP
	 * ----------------------------------------------------------------*</pre>*/
    private void s14503ImpostaTemp() {
        // COB_CODE: INITIALIZE IDSV0301
        //                      IDSV0303.
        initIdsv0301();
        initIdsv0303();
        // COB_CODE: MOVE 'ALL'                   TO IDSV0301-ALIAS-STR-DATO.
        ws.getIdsv0301().setAliasStrDato("ALL");
        // COB_CODE: MOVE LENGTH OF WALL-TABELLA
        //                                        TO IDSV0301-BUFFER-DATA-LEN.
        ws.getIdsv0301().setBufferDataLen(WallTabella.Len.TABELLA);
        // COB_CODE: SET IDSV0301-READ            TO TRUE.
        ws.getIdsv0301().getOperazione().setRead();
        // COB_CODE: SET IDSV0301-ESITO-OK        TO TRUE.
        ws.getIdsv0301().getEsito().setOk();
        // COB_CODE: SET IDSV0301-SUCCESSFUL-SQL  TO TRUE.
        ws.getIdsv0301().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSV0301-ADDRESS         TO ADDRESS OF WALL-TABELLA.
        ws.getIdsv0301().setAddress(pointerManager.addressOf(wallAreaAsset.getTabella()));
    }

    /**Original name: S14502-GESTIONE-FONDI<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA DI PAGINA TAB ASSET
	 * ----------------------------------------------------------------*
	 * --  A partire dalla data decorrenza della garanzia vengono
	 * --  estratti dal pft i fondi di tipo Unit/Index
	 * --  Per la venidta la data decorrenza della Garanzia e per il
	 * --  versamento aggiuntivo la data decorrenza della tranche</pre>*/
    private void s14502GestioneFondi() {
        // COB_CODE: MOVE WGRZ-DT-DECOR(1) TO WK-DT-DECORRENZA
        ws.getWkVariabili().setWkDtDecorrenza(TruncAbs.toInt(wgrzAreaGaranzia.getTabGar(1).getLccvgrz1().getDati().getWgrzDtDecor().getWgrzDtDecor(), 8));
        // COB_CODE: PERFORM VARYING IX-ALL FROM 1 BY 1
        //                     UNTIL IX-ALL > WALL-ELE-ASSET-ALL-MAX
        //             END-IF
        //           END-PERFORM.
        ws.getIxIndici().setAll(((short)1));
        while (!(ws.getIxIndici().getAll() > wallAreaAsset.getEleAssetAllMax())) {
            // COB_CODE: IF NOT WALL-ST-DEL(IX-ALL) AND NOT WALL-ST-CON(IX-ALL)
            //              END-IF
            //           END-IF
            if (!wallAreaAsset.getTabella().isStDel(ws.getIxIndici().getAll()) && !wallAreaAsset.getTabella().isStCon(ws.getIxIndici().getAll())) {
                // COB_CODE: IF   WK-DT-DECORRENZA  >=  WALL-DT-INI-VLDT(IX-ALL)
                //           AND (WALL-DT-END-VLDT-NULL(IX-ALL) = HIGH-VALUE OR
                //                WK-DT-DECORRENZA  < WALL-DT-END-VLDT(IX-ALL))
                //               END-IF
                //           END-IF
                if (ws.getWkVariabili().getWkDtDecorrenza() >= wallAreaAsset.getTabella().getDtIniVldt(ws.getIxIndici().getAll()) && (Characters.EQ_HIGH.test(wallAreaAsset.getTabella().getDtEndVldtNullFormatted(ws.getIxIndici().getAll())) || ws.getWkVariabili().getWkDtDecorrenza() < wallAreaAsset.getTabella().getDtEndVldt(ws.getIxIndici().getAll()))) {
                    // COB_CODE: MOVE WALL-TP-FND(IX-ALL) TO WK-TP-FONDO
                    ws.getWkTpFondo().setWkTpFondo(wallAreaAsset.getTabella().getTpFnd(ws.getIxIndici().getAll()));
                    // COB_CODE: IF FND-UNIT-OICR OR FND-UNIT-ASSIC OR FND-INDEX
                    //              ADD 1      TO    WPAG-ELE-ASSET-MAX
                    //           END-IF
                    if (ws.getWkTpFondo().isUnitOicr() || ws.getWkTpFondo().isUnitAssic() || ws.getWkTpFondo().isIndex2()) {
                        // COB_CODE: ADD 1 TO IX-AREA-PAGINA
                        ws.getIxIndici().setAreaPagina(Trunc.toShort(1 + ws.getIxIndici().getAreaPagina(), 4));
                        // COB_CODE: MOVE WALL-COD-FND(IX-ALL)
                        //                      TO WPAG-COD-FONDO(IX-AREA-PAGINA)
                        wpagAreaPagina.getDatiOuput().getWpagTabAsset(ws.getIxIndici().getAreaPagina()).setWpagCodFondo(wallAreaAsset.getTabella().getCodFnd(ws.getIxIndici().getAll()));
                        // COB_CODE: MOVE WALL-TP-FND(IX-ALL)
                        //                      TO WPAG-TP-FND(IX-AREA-PAGINA)
                        wpagAreaPagina.getDatiOuput().getWpagTabAsset(ws.getIxIndici().getAreaPagina()).setWpagTpFnd(wallAreaAsset.getTabella().getTpFnd(ws.getIxIndici().getAll()));
                        // COB_CODE: MOVE WALL-PC-RIP-AST(IX-ALL)
                        //                      TO WPAG-PERC-FONDO(IX-AREA-PAGINA)
                        wpagAreaPagina.getDatiOuput().getWpagTabAsset(ws.getIxIndici().getAreaPagina()).getWpagPercFondo().setWpagPercFondo(Trunc.toDecimal(wallAreaAsset.getTabella().getPcRipAst(ws.getIxIndici().getAll()), 6, 3));
                        // COB_CODE: IF WALL-COD-TARI-NULL(IX-ALL) = HIGH-VALUE
                        //                TO WPAG-COD-TARI-NULL(IX-AREA-PAGINA)
                        //           ELSE
                        //                TO WPAG-COD-TARI(IX-AREA-PAGINA)
                        //           END-IF
                        if (Characters.EQ_HIGH.test(wallAreaAsset.getTabella().getCodTariNullFormatted(ws.getIxIndici().getAll()))) {
                            // COB_CODE: MOVE HIGH-VALUE
                            //             TO WPAG-COD-TARI-NULL(IX-AREA-PAGINA)
                            wpagAreaPagina.getDatiOuput().getWpagTabAsset(ws.getIxIndici().getAreaPagina()).setWpagCodTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpagTabAsset.Len.WPAG_COD_TARI));
                        }
                        else {
                            // COB_CODE: MOVE WALL-COD-TARI(IX-ALL)
                            //             TO WPAG-COD-TARI(IX-AREA-PAGINA)
                            wpagAreaPagina.getDatiOuput().getWpagTabAsset(ws.getIxIndici().getAreaPagina()).setWpagCodTari(wallAreaAsset.getTabella().getCodTari(ws.getIxIndici().getAll()));
                        }
                        // COB_CODE: ADD 1      TO    WPAG-ELE-ASSET-MAX
                        wpagAreaPagina.getDatiOuput().setWpagEleAssetMax(Trunc.toShort(1 + wpagAreaPagina.getDatiOuput().getWpagEleAssetMax(), 4));
                    }
                }
            }
            ws.getIxIndici().setAll(Trunc.toShort(ws.getIxIndici().getAll() + 1, 4));
        }
    }

    /**Original name: S14600-GESTIONE-SOPRA<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE INCLUSIONE SOPRAPPREMI
	 * ----------------------------------------------------------------*
	 * RAFFAELE
	 * Estrazione di tutte le tranche di garanzia (in vigore) associate
	 * alle adesione</pre>*/
    private void s14600GestioneSopra() {
        // COB_CODE: INITIALIZE                       VTGA-AREA-TRANCHE.
        initVtgaAreaTranche();
        // COB_CODE: PERFORM S1660-FETCH-FIRST-TRANCHE
        //              THRU S1660-EX
        s1660FetchFirstTranche();
        // COB_CODE: PERFORM S1680-FETCH-NEXT-TRANCHE
        //              THRU S1680-EX
        //             UNTIL FINE-ELEMENTI-TRANCHE
        //                OR IDSV0001-ESITO-KO.
        while (!(ws.getWsFlagFineEle().isFineElementiTranche() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            s1680FetchNextTranche();
        }
        //Individuazione delle garanzie sulle quali sono stati inclusi
        //soprappremi e delle tranche di garanzia in vigore all'inclusione
        //soprappremio
        // COB_CODE: MOVE ZEROES                   TO IX-APPO-GRZ
        //                                            IX-APPO-SPG
        //                                            IX-APPO-TGA
        //                                            VS-GRZ-ELE-GARANZIA-MAX
        ws.getIxIndici().setAppoGrz(((short)0));
        ws.getIxIndici().setAppoSpg(((short)0));
        ws.getIxIndici().setAppoTga(((short)0));
        ws.setVsGrzEleGaranziaMax(((short)0));
        // COB_CODE: PERFORM VARYING IX-SPG FROM 1 BY 1
        //             UNTIL IX-SPG > VSPG-ELE-SOPR-GAR-MAX
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setSpg(((short)1));
        while (!(ws.getIxIndici().getSpg() > ws.getVspgEleSoprGarMax())) {
            // COB_CODE: IF VSPG-ST-ADD(IX-SPG)
            //                   OR WK-GRZ-TROVATA
            //           END-IF
            if (ws.getVspgTabSpg(ws.getIxIndici().getSpg()).getLccvspg1().getStatus().isAdd()) {
                // COB_CODE: ADD 1                 TO IX-APPO-SPG
                ws.getIxIndici().setAppoSpg(Trunc.toShort(1 + ws.getIxIndici().getAppoSpg(), 4));
                // COB_CODE: MOVE VSPG-TAB-SPG(IX-SPG)
                //             TO VS-SPG-TAB-SPG(IX-APPO-SPG)
                ws.getVsSpgTabSpg(ws.getIxIndici().getAppoSpg()).setVspgTabSpgBytes(ws.getVspgTabSpg(ws.getIxIndici().getSpg()).getTabSpgBytes());
                // COB_CODE: SET WK-GRZ-NON-TROVATA
                //                                 TO TRUE
                ws.getWkRicGrz().setNonTrovata();
                // COB_CODE: PERFORM S14620-GESTIONE-GAR-SOPRA
                //              THRU EX-S14620
                //           VARYING IX-TAB-GRZ FROM 1 BY 1
                //             UNTIL IX-TAB-GRZ > VGRZ-ELE-GARANZIA-MAX
                //                OR WK-GRZ-TROVATA
                ws.getIxIndici().setTabGrz(((short)1));
                while (!(ws.getIxIndici().getTabGrz() > ws.getVgrzEleGaranziaMax() || ws.getWkRicGrz().isTrovata())) {
                    s14620GestioneGarSopra();
                    ws.getIxIndici().setTabGrz(Trunc.toShort(ws.getIxIndici().getTabGrz() + 1, 4));
                }
            }
            ws.getIxIndici().setSpg(Trunc.toShort(ws.getIxIndici().getSpg() + 1, 4));
        }
        //
        // COB_CODE: MOVE IX-APPO-SPG              TO VS-SPG-ELE-SOPR-GAR-MAX.
        ws.setVsSpgEleSoprGarMax(ws.getIxIndici().getAppoSpg());
        // COB_CODE: MOVE IX-APPO-GRZ              TO VS-GRZ-ELE-GARANZIA-MAX.
        ws.setVsGrzEleGaranziaMax(ws.getIxIndici().getAppoGrz());
        // COB_CODE: MOVE IX-APPO-TGA              TO VTGA-ELE-TRAN-MAX.
        ws.setVtgaEleTranMax(ws.getIxIndici().getAppoTga());
        //
        // COB_CODE: INITIALIZE                       VGRZ-AREA-GARANZIA
        //                                            VSPG-AREA-SOPR-GAR.
        initVgrzAreaGaranzia();
        initVspgAreaSoprGar();
        // COB_CODE: MOVE VS-GRZ-AREA-GARANZIA     TO VGRZ-AREA-GARANZIA.
        ws.setVgrzAreaGaranziaBytes(ws.getVsGrzAreaGaranziaBytes());
        // COB_CODE: MOVE VS-SPG-AREA-SOPR-GAR     TO VSPG-AREA-SOPR-GAR.
        ws.setVspgAreaSoprGarBytes(ws.getVsSpgAreaSoprGarBytes());
    }

    /**Original name: S14620-GESTIONE-GAR-SOPRA<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE GARANZIE INCLUSIONE SOPRAPPREMI
	 * ----------------------------------------------------------------*</pre>*/
    private void s14620GestioneGarSopra() {
        // COB_CODE:      IF VSPG-ID-GAR(IX-SPG) = VGRZ-ID-GAR(IX-TAB-GRZ)
        //           *       RICERCA GARANZIA SU CUI E' GESTITO IL SOPRAPPREMIO
        //                   END-IF
        //                END-IF.
        if (ws.getVspgTabSpg(ws.getIxIndici().getSpg()).getLccvspg1().getDati().getWspgIdGar().getWspgIdGar() == ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
            //       RICERCA GARANZIA SU CUI E' GESTITO IL SOPRAPPREMIO
            // COB_CODE: PERFORM S14640-RICERCA-GAR-SOPRA
            //              THRU EX-S14640
            //           VARYING IX-RIC-GRZ FROM 1 BY 1
            //             UNTIL IX-RIC-GRZ > VS-GRZ-ELE-GARANZIA-MAX
            //                OR WK-GRZ-TROVATA
            ws.getIxIndici().setRicGrz(((short)1));
            while (!(ws.getIxIndici().getRicGrz() > ws.getVsGrzEleGaranziaMax() || ws.getWkRicGrz().isTrovata())) {
                s14640RicercaGarSopra();
                ws.getIxIndici().setRicGrz(Trunc.toShort(ws.getIxIndici().getRicGrz() + 1, 4));
            }
            // COB_CODE: IF WK-GRZ-NON-TROVATA
            //              END-PERFORM
            //           END-IF
            if (ws.getWkRicGrz().isNonTrovata()) {
                // COB_CODE: ADD 1                   TO IX-APPO-GRZ
                ws.getIxIndici().setAppoGrz(Trunc.toShort(1 + ws.getIxIndici().getAppoGrz(), 4));
                // COB_CODE: MOVE IX-APPO-GRZ        TO VS-GRZ-ELE-GARANZIA-MAX
                ws.setVsGrzEleGaranziaMax(ws.getIxIndici().getAppoGrz());
                // COB_CODE: MOVE VGRZ-TAB-GAR(IX-TAB-GRZ)
                //             TO VS-GRZ-TAB-GAR(IX-APPO-GRZ)
                ws.getVsGrzTabGar(ws.getIxIndici().getAppoGrz()).setWgarTabGarBytes(ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getTabGarBytes());
                // COB_CODE: SET WK-GRZ-TROVATA      TO TRUE
                ws.getWkRicGrz().setTrovata();
                // COB_CODE: SET WK-TGA-NON-TROVATA  TO TRUE
                ws.getWkRicTga().setNonTrovata();
                //          RICERCA DELLA TRANCGE DI GARANZIA IN VIGORE
                //          ALL'INCLUSIONE SOPRAPPREMI
                // COB_CODE: PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
                //             UNTIL IX-TAB-TGA > VS-TGA-ELE-TRAN-MAX
                //                OR WK-TGA-TROVATA
                //                   END-IF
                //           END-PERFORM
                ws.getIxIndici().setTabTga(((short)1));
                while (!(ws.getIxIndici().getTabTga() > ws.getVsTgaEleTranMax() || ws.getWkRicTga().isTrovata())) {
                    // COB_CODE: IF VGRZ-ID-GAR(IX-TAB-GRZ) =
                    //              VS-TGA-ID-GAR(IX-TAB-TGA)   AND
                    //              VSPG-DT-INI-EFF(IX-SPG) >=
                    //              VS-TGA-DT-DECOR(IX-TAB-TGA) AND
                    //              VSPG-DT-INI-EFF(IX-SPG) <=
                    //              VS-TGA-DT-SCAD(IX-TAB-TGA)  AND
                    //              VS-TGA-TP-TRCH(IX-TAB-TGA)  = '1'
                    //                           TO TRUE
                    //           END-IF
                    if (ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar() == ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdGar() && ws.getVspgTabSpg(ws.getIxIndici().getSpg()).getLccvspg1().getDati().getWspgDtIniEff() >= ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtDecor() && ws.getVspgTabSpg(ws.getIxIndici().getSpg()).getLccvspg1().getDati().getWspgDtIniEff() <= ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtScad().getWtgaDtScad() && Conditions.eq(ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaTpTrch(), "1")) {
                        // COB_CODE: ADD 1        TO IX-APPO-TGA
                        ws.getIxIndici().setAppoTga(Trunc.toShort(1 + ws.getIxIndici().getAppoTga(), 4));
                        // COB_CODE: MOVE VS-TGA-TAB-TRAN(IX-TAB-TGA)
                        //             TO VTGA-TAB-TRAN(IX-APPO-TGA)
                        ws.getVtgaTabTran(ws.getIxIndici().getAppoTga()).setWtgaTabTranBytes(ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getW1tgaTabTranBytes());
                        // COB_CODE: SET WK-TGA-TROVATA
                        //                        TO TRUE
                        ws.getWkRicTga().setTrovata();
                    }
                    ws.getIxIndici().setTabTga(Trunc.toShort(ws.getIxIndici().getTabTga() + 1, 4));
                }
            }
        }
    }

    /**Original name: S14640-RICERCA-GAR-SOPRA<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA GARANZIE INCLUSIONE SOPRAPPREMI
	 * ----------------------------------------------------------------*</pre>*/
    private void s14640RicercaGarSopra() {
        // COB_CODE: IF VGRZ-ID-GAR(IX-TAB-GRZ) = VS-GRZ-ID-GAR(IX-RIC-GRZ)
        //              SET WK-GRZ-TROVATA         TO TRUE
        //           END-IF.
        if (ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar() == ws.getVsGrzTabGar(ws.getIxIndici().getRicGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
            // COB_CODE: SET WK-GRZ-TROVATA         TO TRUE
            ws.getWkRicGrz().setTrovata();
        }
    }

    /**Original name: S14700-GESTIONE-RATE<br>
	 * <pre>----------------------------------------------------------------*
	 *     Calcolo del numero di rate che vengono accorpate nel TITOLO
	 *     di perfezionamento
	 * ----------------------------------------------------------------*
	 * --      Gestione Unica</pre>*/
    private void s14700GestioneRate() {
        // COB_CODE:      EVALUATE TRUE
        //           *--      Gestione Unica
        //                    WHEN RECUPRATE-UNICA
        //                               WK-RATE-ANTIC + WK-RATE-RECUP + 1
        //           *--      Gestione Separata
        //                    WHEN RECUPRATE-SEPARATE
        //                       COMPUTE WPAG-NUM-RATE-ACC = WK-RATE-ANTIC + 1
        //           *--      Gestione Batch
        //                    WHEN RECUPRATE-BATCH
        //                       MOVE 1 TO WPAG-NUM-RATE-ACC
        //                END-EVALUATE.
        switch (ws.getWkVariabili().getWkTpRatPerf().getWkTpRatPerf()) {

            case WkTpRatPerf.UNICA:// COB_CODE: COMPUTE WPAG-NUM-RATE-ACC =
                //                   WK-RATE-ANTIC + WK-RATE-RECUP + 1
                wpagAreaPagina.getDatiOuput().setWpagNumRateAcc(Trunc.toShort(ws.getWkVariabili().getWkRateAntic() + ws.getWkVariabili().getWkRateRecup() + 1, 4));
                //--      Gestione Separata
                break;

            case WkTpRatPerf.SEPARATE:// COB_CODE: COMPUTE WPAG-NUM-RATE-ACC = WK-RATE-ANTIC + 1
                wpagAreaPagina.getDatiOuput().setWpagNumRateAcc(Trunc.toShort(ws.getWkVariabili().getWkRateAntic() + 1, 4));
                //--      Gestione Batch
                break;

            case WkTpRatPerf.BATCH:// COB_CODE: MOVE 1 TO WPAG-NUM-RATE-ACC
                wpagAreaPagina.getDatiOuput().setWpagNumRateAcc(((short)1));
                break;

            default:break;
        }
    }

    /**Original name: S1660-FETCH-FIRST-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1660FetchFirstTranche() {
        // COB_CODE: INITIALIZE LDBI0731.
        initLdbi0731();
        // COB_CODE: INITIALIZE LDBO0731.
        initLdbo07311();
        //--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        //
        // COB_CODE: MOVE VGRZ-ID-POLI(1)          TO LDBI0731-ID-POLI
        ws.getLdbi0731().setIdPoli(ws.getVgrzTabGar(1).getLccvgrz1().getDati().getWgrzIdPoli());
        // COB_CODE: MOVE VGRZ-ID-ADES(1)          TO LDBI0731-ID-ADES
        ws.getLdbi0731().setIdAdes(ws.getVgrzTabGar(1).getLccvgrz1().getDati().getWgrzIdAdes().getWgrzIdAdes());
        // COB_CODE: MOVE '02'                     TO LDBI0731-TP-CALL
        ws.getLdbi0731().setTpCall("02");
        // COB_CODE: SET IN-VIGORE                 TO TRUE
        ws.getWsTpStatBus().setInVigore();
        // COB_CODE: MOVE WS-TP-STAT-BUS           TO LDBI0731-TP-STAT-BUS
        ws.getLdbi0731().setTpStatBus(ws.getWsTpStatBus().getWsTpStatBus());
        // COB_CODE: MOVE SPACES                   TO LDBI0731-TRASFORMATA.
        ws.getLdbi0731().setTrasformata(Types.SPACE_CHAR);
        // COB_CODE: MOVE 'LDBS0730'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS0730");
        //
        // COB_CODE: MOVE LDBI0731
        //             TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbi0731().getLdbi0731Formatted());
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: PERFORM S1690-CALL-LDBS0730   THRU S1690-EX.
        s1690CallLdbs0730();
        // COB_CODE: MOVE IX-TAB-TGA               TO VS-TGA-ELE-TRAN-MAX.
        ws.setVsTgaEleTranMax(ws.getIxIndici().getTabTga());
    }

    /**Original name: S1680-FETCH-NEXT-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1680FetchNextTranche() {
        // COB_CODE: INITIALIZE LDBI0731
        initLdbi0731();
        // COB_CODE: INITIALIZE LDBO0731
        initLdbo07311();
        //--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
        // COB_CODE: MOVE ZEROES
        //             TO IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        //
        // COB_CODE: MOVE VGRZ-ID-POLI(1)          TO LDBI0731-ID-POLI
        ws.getLdbi0731().setIdPoli(ws.getVgrzTabGar(1).getLccvgrz1().getDati().getWgrzIdPoli());
        // COB_CODE: MOVE VGRZ-ID-ADES(1)          TO LDBI0731-ID-ADES
        ws.getLdbi0731().setIdAdes(ws.getVgrzTabGar(1).getLccvgrz1().getDati().getWgrzIdAdes().getWgrzIdAdes());
        // COB_CODE: MOVE '02'                     TO LDBI0731-TP-CALL
        ws.getLdbi0731().setTpCall("02");
        // COB_CODE: SET IN-VIGORE                 TO TRUE
        ws.getWsTpStatBus().setInVigore();
        // COB_CODE: MOVE WS-TP-STAT-BUS           TO LDBI0731-TP-STAT-BUS
        ws.getLdbi0731().setTpStatBus(ws.getWsTpStatBus().getWsTpStatBus());
        // COB_CODE: MOVE SPACES                   TO LDBI0731-TRASFORMATA
        ws.getLdbi0731().setTrasformata(Types.SPACE_CHAR);
        // COB_CODE: MOVE 'LDBS0730'               TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS0730");
        //
        // COB_CODE: MOVE LDBI0731
        //             TO IDSI0011-BUFFER-WHERE-COND
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbi0731().getLdbi0731Formatted());
        // COB_CODE: SET IDSI0011-FETCH-NEXT       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
        // COB_CODE: PERFORM S1690-CALL-LDBS0730   THRU S1690-EX.
        s1690CallLdbs0730();
        // COB_CODE: MOVE IX-TAB-TGA               TO VS-TGA-ELE-TRAN-MAX.
        ws.setVsTgaEleTranMax(ws.getIxIndici().getTabTga());
    }

    /**Original name: S1690-CALL-LDBS0730<br>
	 * <pre>*****************************************************************
	 *  CHIAMATA AL PROGRAMMA LDBS0730 CHE EFFETTUA
	 *  L'ESTRAZIONE DEI DATI DALLA TABELLA  STATO-OGGETTO-BUSINESS
	 * *****************************************************************</pre>*/
    private void s1690CallLdbs0730() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSI0011-TRATT-X-EFFETTO TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattXEffetto();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIONE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->        NON TROVATA
            //                                             TO TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                               THRU VALORIZZA-OUTPUT-TGA-EX
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://-->        NON TROVATA
                    // COB_CODE: SET FINE-ELEMENTI-TRANCHE
                    //                            TO TRUE
                    ws.getWsFlagFineEle().setFineElementiTranche();
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: SET ALTRI-ELEMENTI
                    //                            TO TRUE
                    ws.getWsFlagFineEle().setAltriElementi();
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //                            TO LDBO0731
                    ws.setLdbo07311Formatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE LDBO0731-TGA
                    //                            TO TRCH-DI-GAR
                    ws.getTrchDiGar().setTrchDiGarFormatted(ws.getLdbo07312().getLdbo0731TgaFormatted());
                    // COB_CODE: ADD 1            TO IX-TAB-TGA
                    ws.getIxIndici().setTabTga(Trunc.toShort(1 + ws.getIxIndici().getTabTga(), 4));
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-TGA
                    //              THRU VALORIZZA-OUTPUT-TGA-EX
                    valorizzaOutputTga();
                    break;

                default:break;
            }
        }
        else {
            //-->    GESTIONE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1690-CALL-LDBS0730' TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1690-CALL-LDBS0730");
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'LDBS0730'           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBS0730", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S15000-INIZIALIZZA-TASTI<br>
	 * <pre>----------------------------------------------------------------*
	 *     INIZIALIZZAZIONE TASTI PER DATI GENERALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s15000InizializzaTasti() {
        // COB_CODE: MOVE 'A'                  TO  WCOM-TASTO-ANNULLA
        //                                         WCOM-TASTO-AVANTI
        //                                         WCOM-TASTO-INDIETRO
        //                                         WCOM-TASTO-NOTE.
        lccc0001.getTastiDaAbilitare().setWcomTastoAnnullaFormatted("A");
        lccc0001.getTastiDaAbilitare().setWcomTastoAvantiFormatted("A");
        lccc0001.getTastiDaAbilitare().setWcomTastoIndietroFormatted("A");
        lccc0001.getTastiDaAbilitare().setWcomTastoNoteFormatted("A");
        // COB_CODE: MOVE 'D'                  TO  WCOM-TASTO-DA-AUTORIZZARE
        //                                         WCOM-TASTO-AUTORIZZA
        //                                         WCOM-TASTO-AGGIORNA-PTF
        //                                         WCOM-TASTO-RESPINGI
        //                                         WCOM-TASTO-ELIMINA.
        lccc0001.getTastiDaAbilitare().setWcomTastoDaAutorizzareFormatted("D");
        lccc0001.getTastiDaAbilitare().setWcomTastoAutorizzaFormatted("D");
        lccc0001.getTastiDaAbilitare().setWcomTastoAggiornaPtfFormatted("D");
        lccc0001.getTastiDaAbilitare().setWcomTastoRespingiFormatted("D");
        lccc0001.getTastiDaAbilitare().setWcomTastoEliminaFormatted("D");
    }

    /**Original name: S90000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s90000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: RIC-TAB-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *     Ricerca della tabella parametro movimento
	 *     Chiave di ricerca : Tipo Movimento
	 * ----------------------------------------------------------------*</pre>*/
    private void ricTabPmo() {
        // COB_CODE: SET WK-NON-TROVATO TO TRUE
        ws.getWkFlg().setNonTrovato();
        // COB_CODE: MOVE ZERO          TO IX-RIC-PMO
        ws.getIxIndici().setRicPmo(((short)0));
        // COB_CODE: PERFORM UNTIL IX-RIC-PMO >= WPMO-ELE-PARAM-MOV-MAX
        //                      OR WK-TROVATO
        //              END-IF
        //           END-PERFORM.
        while (!(ws.getIxIndici().getRicPmo() >= wpmoAreaParamMov.getEleParamMovMax() || ws.getWkFlg().isTrovato())) {
            // COB_CODE: ADD 1 TO IX-RIC-PMO
            ws.getIxIndici().setRicPmo(Trunc.toShort(1 + ws.getIxIndici().getRicPmo(), 4));
            // COB_CODE: IF WPMO-TP-MOVI(IX-RIC-PMO) = WS-MOVIMENTO
            //              AND NOT WPMO-ST-INV(IX-RIC-PMO)
            //              SET WK-TROVATO TO TRUE
            //           END-IF
            if (wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getRicPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi() == ws.getWsMovimento().getWsMovimento() && !wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getRicPmo()).getLccvpmo1().getStatus().isInv()) {
                // COB_CODE: SET WK-TROVATO TO TRUE
                ws.getWkFlg().setTrovato();
            }
        }
    }

    /**Original name: RIC-TAB-POG<br>
	 * <pre>----------------------------------------------------------------*
	 *     Ricerca della tabella parametro oggetto
	 *     Chiave di ricerca : Codice parametro
	 * ----------------------------------------------------------------*</pre>*/
    private void ricTabPog() {
        // COB_CODE: SET WK-NON-TROVATO TO TRUE
        ws.getWkFlg().setNonTrovato();
        // COB_CODE: MOVE ZERO TO IX-RIC-POG
        ws.getIxIndici().setRicPog(((short)0));
        // COB_CODE: PERFORM UNTIL IX-RIC-POG >= WPOG-ELE-PARAM-OGG-MAX
        //                      OR WK-TROVATO
        //              END-IF
        //           END-PERFORM.
        while (!(ws.getIxIndici().getRicPog() >= wpogAreaParamOgg.getEleParamOggMax() || ws.getWkFlg().isTrovato())) {
            // COB_CODE: ADD 1 TO IX-RIC-POG
            ws.getIxIndici().setRicPog(Trunc.toShort(1 + ws.getIxIndici().getRicPog(), 4));
            // COB_CODE: IF WPOG-COD-PARAM(IX-RIC-POG) = WK-PARAMETRO
            //              SET WK-TROVATO TO TRUE
            //           END-IF
            if (Conditions.eq(wpogAreaParamOgg.getTabParamOgg(ws.getIxIndici().getRicPog()).getLccvpog1().getDati().getWpogCodParam(), ws.getWkVariabili().getWkParametro())) {
                // COB_CODE: SET WK-TROVATO TO TRUE
                ws.getWkFlg().setTrovato();
            }
        }
    }

    /**Original name: RIC-TAB-RAN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Ricerca della tabella Rapporto Anagrafico
	 *     Tipologia Ricerca
	 *     Chiave di ricerca
	 *                     - Tipo rapporto anagrafico
	 * ----------------------------------------------------------------*</pre>*/
    private void ricTabRan() {
        // COB_CODE: SET WK-NON-TROVATO TO TRUE
        ws.getWkFlg().setNonTrovato();
        // COB_CODE: MOVE ZERO          TO IX-RIC-RAN
        ws.getIxIndici().setRicRan(((short)0));
        // COB_CODE: PERFORM UNTIL IX-RIC-RAN >= WRAN-ELE-RAPP-ANAG-MAX
        //                      OR WK-TROVATO
        //              END-IF
        //           END-PERFORM.
        while (!(ws.getIxIndici().getRicRan() >= wranAreaRappAnag.getEleRappAnagMax() || ws.getWkFlg().isTrovato())) {
            // COB_CODE: ADD 1 TO IX-RIC-RAN
            ws.getIxIndici().setRicRan(Trunc.toShort(1 + ws.getIxIndici().getRicRan(), 4));
            // COB_CODE: IF WRAN-TP-RAPP-ANA(IX-RIC-RAN) = WS-TP-RAPP-ANA
            //              SET WK-TROVATO TO TRUE
            //           END-IF
            if (Conditions.eq(wranAreaRappAnag.getTabRappAnag(ws.getIxIndici().getRicRan()).getLccvran1().getDati().getWranTpRappAna(), ws.getWsTpRappAna().getWsTpRappAna())) {
                // COB_CODE: SET WK-TROVATO TO TRUE
                ws.getWkFlg().setTrovato();
            }
        }
    }

    /**Original name: RIC-TAB-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     Ricerca della tabella garanzie
	 *     Chiave di ricerca : Tipo Garanzia
	 *     Per le garanzie complementari si verifica che almeno una sia
	 *     da includere.
	 * ----------------------------------------------------------------*</pre>*/
    private void ricTabGar() {
        // COB_CODE: SET WK-NON-TROVATO TO TRUE.
        ws.getWkFlg().setNonTrovato();
        // COB_CODE: MOVE ZERO TO IX-RIC-GAR.
        ws.getIxIndici().setRicGar(((short)0));
        // COB_CODE: PERFORM UNTIL IX-RIC-GAR >= WGRZ-ELE-GARANZIA-MAX
        //                      OR WK-TROVATO
        //              END-IF
        //           END-PERFORM.
        while (!(ws.getIxIndici().getRicGar() >= wgrzAreaGaranzia.getEleGarMax() || ws.getWkFlg().isTrovato())) {
            // COB_CODE: ADD 1 TO IX-RIC-GAR
            ws.getIxIndici().setRicGar(Trunc.toShort(1 + ws.getIxIndici().getRicGar(), 4));
            // COB_CODE: IF WGRZ-TP-GAR(IX-RIC-GAR) = ACT-TP-GARANZIA
            //              END-IF
            //           END-IF
            if (wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getRicGar()).getLccvgrz1().getDati().getWgrzTpGar().getWgrzTpGar() == ws.getLccc0006().getActTpGaranzia().getActTpGaranzia()) {
                // COB_CODE: IF TP-GAR-COMPLEM
                //              END-IF
                //           ELSE
                //                 THRU PREP-TAB-LCCC0490-EX
                //           END-IF
                if (ws.getLccc0006().getActTpGaranzia().isTpGarComplem()) {
                    // COB_CODE: MOVE WGRZ-COD-TARI(IX-RIC-GAR) TO WS-COD-TARIFFA
                    ws.getWkVariabili().setWsCodTariffa(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getRicGar()).getLccvgrz1().getDati().getWgrzCodTari());
                    // COB_CODE: PERFORM CTRL-GAR-INCLUSA
                    //              THRU CTRL-GAR-INCLUSA-EX
                    ctrlGarInclusa();
                    // COB_CODE: IF TROVATO
                    //              END-IF
                    //           END-IF
                    if (ws.getWsFlagRicerca().isTrovato()) {
                        // COB_CODE: IF WPAG-GAR-INCLUSA-SI(IX-RIC-GRZ)
                        //                 THRU PREP-TAB-LCCC0490-EX
                        //           END-IF
                        if (wpagAreaPagina.getTabGaranzie(ws.getIxIndici().getRicGrz()).getFlGarInclusaI().isSi()) {
                            // COB_CODE: SET WK-TROVATO TO TRUE
                            ws.getWkFlg().setTrovato();
                            // COB_CODE: PERFORM PREP-TAB-LCCC0490
                            //              THRU PREP-TAB-LCCC0490-EX
                            prepTabLccc0490();
                        }
                    }
                }
                else {
                    // COB_CODE: SET WK-TROVATO TO TRUE
                    ws.getWkFlg().setTrovato();
                    // COB_CODE: PERFORM PREP-TAB-LCCC0490
                    //              THRU PREP-TAB-LCCC0490-EX
                    prepTabLccc0490();
                }
            }
        }
    }

    /**Original name: CALL-LCCS0062<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES PER IL CALCOLO DELLE RATE RETRODATATE/SUCCESSIVE
	 * ----------------------------------------------------------------*</pre>*/
    private void callLccs0062() {
        Lccs0062 lccs0062 = null;
        // COB_CODE: INITIALIZE AREA-LCCC0062.
        initAreaLccc0062();
        // COB_CODE: MOVE WPOL-DT-DECOR              TO LCCC0062-DT-DECORRENZA
        ws.getAreaLccc0062().setDtDecorrenza(TruncAbs.toInt(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtDecor(), 8));
        // COB_CODE: MOVE WK-DATA-COMPETENZA         TO LCCC0062-DT-COMPETENZA
        ws.getAreaLccc0062().setDtCompetenza(ws.getWkVariabili().getWkDataCompetenza());
        // COB_CODE: IF QUIETANZAMENTO-SI
        //              MOVE ZEROES                  TO LCCC0062-DT-ULTGZ-TRCH
        //           END-IF
        if (ws.isWkQuietanzamento()) {
            // COB_CODE: MOVE PCO-DT-ULT-QTZO-IN      TO LCCC0062-DT-ULT-QTZO
            ws.getAreaLccc0062().setDtUltQtzo(TruncAbs.toInt(ws.getParamComp().getPcoDtUltQtzoIn().getPcoDtUltQtzoIn(), 8));
            // COB_CODE: MOVE ZEROES                  TO LCCC0062-DT-ULTGZ-TRCH
            ws.getAreaLccc0062().setDtUltgzTrch(0);
        }
        // COB_CODE: IF GETRA-SI
        //              MOVE ZEROES                  TO LCCC0062-DT-ULT-QTZO
        //           END-IF
        if (ws.isWkGetra()) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-TRCH-E-IN  TO LCCC0062-DT-ULTGZ-TRCH
            ws.getAreaLccc0062().setDtUltgzTrch(TruncAbs.toInt(ws.getParamComp().getPcoDtUltgzTrchEIn().getPcoDtUltgzTrchEIn(), 8));
            // COB_CODE: MOVE ZEROES                  TO LCCC0062-DT-ULT-QTZO
            ws.getAreaLccc0062().setDtUltQtzo(0);
        }
        // COB_CODE: MOVE WK-FRAZ-MM                 TO LCCC0062-FRAZIONAMENTO.
        ws.getAreaLccc0062().setFrazionamentoFormatted(ws.getWkVariabili().getWkFrazMm().getFrazMmFormatted());
        // COB_CODE: CALL LCCS0062 USING  AREA-IDSV0001
        //                                WCOM-AREA-STATI
        //                                AREA-LCCC0062
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0062 = Lccs0062.getInstance();
            lccs0062.run(areaIdsv0001, lccc0001, ws.getAreaLccc0062());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO CALCOLA RATE'   TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO CALCOLA RATE");
            // COB_CODE: MOVE 'CALL-LCCS0062'           TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-LCCS0062");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: VALORIZZA-FLAG-AUTOGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA FLAG AUTOGENERAZIONE TITOLI
	 * ----------------------------------------------------------------*</pre>*/
    private void valorizzaFlagAutogen() {
        // COB_CODE: IF WK-FL-INC-AUTOGEN-NULL NOT = HIGH-VALUE AND LOW-VALUE
        //                                                      AND SPACES
        //              END-PERFORM
        //           END-IF.
        if (!Conditions.eq(ws.getWkVariabili().getWkFlIncAutogen(), Types.HIGH_CHAR_VAL) && !Conditions.eq(ws.getWkVariabili().getWkFlIncAutogen(), Types.LOW_CHAR_VAL) && !Conditions.eq(ws.getWkVariabili().getWkFlIncAutogen(), Types.SPACE_CHAR)) {
            // COB_CODE: PERFORM VARYING IX-TAB-CALC FROM 1 BY 1
            //                     UNTIL IX-TAB-CALC > WPAG-ELE-CALCOLI-MAX
            //             END-PERFORM
            //           END-PERFORM
            ws.getIxIndici().setTabCalc(((short)1));
            while (!(ws.getIxIndici().getTabCalc() > wpagAreaPagina.getDatiOuput().getWpagEleCalcoliMax())) {
                // COB_CODE: PERFORM VARYING IX-AREA-ISPC0140 FROM 1 BY 1
                //                     UNTIL IX-AREA-ISPC0140 > WPAG-ELE-TIT-CONT-MAX
                //                                              (IX-TAB-CALC)
                //                                (IX-TAB-CALC, IX-AREA-ISPC0140)
                //           END-PERFORM
                ws.getIxIndici().setAreaIspc0140(((short)1));
                while (!(ws.getIxIndici().getAreaIspc0140() > wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagEleTitContMax())) {
                    // COB_CODE: MOVE WK-FL-INC-AUTOGEN
                    //             TO WPAG-CONT-AUTOGEN-INC
                    //                (IX-TAB-CALC, IX-AREA-ISPC0140)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitCont(ws.getIxIndici().getAreaIspc0140()).setWpagContAutogenInc(ws.getWkVariabili().getWkFlIncAutogen());
                    ws.getIxIndici().setAreaIspc0140(Trunc.toShort(ws.getIxIndici().getAreaIspc0140() + 1, 4));
                }
                // COB_CODE: PERFORM VARYING IX-AREA-ISPC0140 FROM 1 BY 1
                //                     UNTIL IX-AREA-ISPC0140 > WPAG-ELE-TIT-RATA-MAX
                //                                              (IX-TAB-CALC)
                //                                (IX-TAB-CALC, IX-AREA-ISPC0140)
                //           END-PERFORM
                ws.getIxIndici().setAreaIspc0140(((short)1));
                while (!(ws.getIxIndici().getAreaIspc0140() > wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagEleTitRataMax())) {
                    // COB_CODE: MOVE WK-FL-INC-AUTOGEN
                    //             TO WPAG-RATA-AUTOGEN-INC
                    //                (IX-TAB-CALC, IX-AREA-ISPC0140)
                    wpagAreaPagina.getDatiOuput().getWpagTabCalcoli(ws.getIxIndici().getTabCalc()).getWpagDatiTitRata(ws.getIxIndici().getAreaIspc0140()).setWpagRataAutogenInc(ws.getWkVariabili().getWkFlIncAutogen());
                    ws.getIxIndici().setAreaIspc0140(Trunc.toShort(ws.getIxIndici().getAreaIspc0140() + 1, 4));
                }
                ws.getIxIndici().setTabCalc(Trunc.toShort(ws.getIxIndici().getTabCalc() + 1, 4));
            }
        }
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES GESTIONE ERRORI
	 * ----------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-VALORIZZATORE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL VALORIZZATORE VARIABILI
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     CALL VALORIZZATORE VARIABILI
	 * *****************************************************************</pre>*/
    private void callValorizzatore() {
        Ivvs0211 ivvs0211 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                TO (SF)-MODALITA-ESECUTIVA
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getModalitaEsecutiva().setModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: IF (SF)-COD-COMPAGNIA-ANIA = 0
        //                TO (SF)-COD-COMPAGNIA-ANIA
        //           END-IF
        if (ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getCodCompagniaAnia() == 0) {
            // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
            //             TO (SF)-COD-COMPAGNIA-ANIA
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211CodCompagniaAniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        }
        // COB_CODE: IF (SF)-TIPO-MOVIMENTO = 0
        //                TO (SF)-TIPO-MOVIMENTO
        //           END-IF
        if (ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getTipoMovimento() == 0) {
            // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
            //             TO (SF)-TIPO-MOVIMENTO
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        }
        // COB_CODE: IF (SF)-COD-MAIN-BATCH = SPACES
        //                TO (SF)-COD-MAIN-BATCH
        //           END-IF
        if (Characters.EQ_SPACE.test(ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getCodMainBatch())) {
            // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
            //             TO (SF)-COD-MAIN-BATCH
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setCodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        }
        // COB_CODE: IF (SF)-COD-SERVIZIO-BE = SPACES
        //                TO (SF)-COD-SERVIZIO-BE
        //           END-IF
        if (Characters.EQ_SPACE.test(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().getCodServizioBe())) {
            // COB_CODE: MOVE IDSV0001-COD-SERVIZIO-BE
            //             TO (SF)-COD-SERVIZIO-BE
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(areaIdsv0001.getLogErrore().getCodServizioBe());
        }
        // COB_CODE: IF (SF)-DATA-EFFETTO = 0
        //                TO (SF)-DATA-EFFETTO
        //           END-IF
        if (ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getDataEffetto() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
            //             TO (SF)-DATA-EFFETTO
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
        }
        // COB_CODE: IF (SF)-DATA-COMPETENZA = 0
        //                TO (SF)-DATA-COMPETENZA
        //           END-IF
        if (ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getDataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //             TO (SF)-DATA-COMPETENZA
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                TO (SF)-DATA-COMP-AGG-STOR
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
        //                TO (SF)-TRATTAMENTO-STORICITA
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                TO (SF)-FORMATO-DATA-DB
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFormatoDataDb().setFormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-AREA-ADDRESSES
        //                TO (SF)-AREA-ADDRESSES
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211AreaAddressesBytes(areaIdsv0001.getAreaComune().getIdsv0001AreaAddressesBytes());
        // COB_CODE: MOVE 'IVVS0211' TO (SF)-PGM
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setPgm("IVVS0211");
        // COB_CODE: CALL (SF)-PGM USING AREA-IO-IVVS0211
        //           ON EXCEPTION
        //                      THRU EX-S0290
        //           END-CALL.
        try {
            ivvs0211 = Ivvs0211.getInstance();
            ivvs0211.run(ws.getAreaIoIvvs0211());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CALL VALORIZZATORE'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("CALL VALORIZZATORE");
            // COB_CODE: MOVE 'CALL-VALORIZZATORE'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-VALORIZZATORE");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF NOT (SF)-SUCCESSFUL-RC
        //           END-IF.
        if (!ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         IF (SF)-CALCOLO-NON-COMPLETO
            //           *
            //                      END-IF
            //                   ELSE
            //                   END-IF
            //                END-IF.
            if (ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().isCalcoloNonCompleto()) {
                //
                // COB_CODE: IF (SF)-ELE-MAX-NOT-FOUND GREATER ZEROES
                //                 THRU EX-S0300
                //           END-IF
                if (ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getEleMaxNotFound() > 0) {
                    // COB_CODE: MOVE (SF)-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getPgm());
                    // COB_CODE: MOVE 'CALL-VALORIZZATORE'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("CALL-VALORIZZATORE");
                    // COB_CODE: MOVE (SF)-AREA-VAR-NOT-FOUND
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getS211AreaVarNotFoundFormatted());
                    // COB_CODE: MOVE '005236'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005236");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
                //
                // COB_CODE: IF (SF)-ELE-MAX-CALC-KO GREATER ZEROES
                //                 THRU EX-S0300
                //           END-IF
                if (ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getEleMaxCalcKo() > 0) {
                    // COB_CODE: MOVE (SF)-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getPgm());
                    // COB_CODE: MOVE 'CALL-VALORIZZATORE'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("CALL-VALORIZZATORE");
                    // COB_CODE: MOVE (SF)-AREA-CALC-KO
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getS211AreaCalcKoFormatted());
                    // COB_CODE: MOVE '005237'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005237");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
            else if (ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().isCachePiena()) {
                // COB_CODE:   IF (SF)-CACHE-PIENA
                //                 THRU EX-S0300
                //             ELSE
                //             END-IF
                //           END-IF
                // COB_CODE: MOVE (SF)-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getPgm());
                // COB_CODE: MOVE 'CALL-VALORIZZATORE'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("CALL-VALORIZZATORE");
                // COB_CODE: MOVE SPACES
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("");
                // COB_CODE: MOVE '005283'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005283");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
            else if (ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().isS211GlovarlistVuota() || ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().isS211SkipCallActuator()) {
            // COB_CODE:  IF (SF)-GLOVARLIST-VUOTA
            //            OR (SF)-SKIP-CALL-ACTUATOR
            //               CONTINUE
            //            ELSE
            //                 THRU EX-S0300
            //           END-IF
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE (SF)-COD-SERVIZIO-BE
                //                                     TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().getCodServizioBe());
                // COB_CODE: MOVE 'CALL-VALORIZZATORE' TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("CALL-VALORIZZATORE");
                // COB_CODE: MOVE '001114'             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("001114");
                // COB_CODE: STRING (SF)-DESCRIZ-ERR  DELIMITED BY '     '
                //                   ' - '
                //                  (SF)-NOME-TABELLA DELIMITED BY SIZE
                //           INTO IEAI9901-PARAMETRI-ERR
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, Functions.substringBefore(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted(), "     "), " - ", ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().getS211NomeTabellaFormatted());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: VALORIZZA-OUTPUT-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONI DA IDBVTGA ---> LCCVTGA
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVTGA3
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputTga() {
        // COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
        //             TO (SF)-ID-PTF(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().setIdPtf(ws.getTrchDiGar().getTgaIdTrchDiGar());
        // COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
        //             TO (SF)-ID-TRCH-DI-GAR(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdTrchDiGar(ws.getTrchDiGar().getTgaIdTrchDiGar());
        // COB_CODE: MOVE TGA-ID-GAR
        //             TO (SF)-ID-GAR(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdGar(ws.getTrchDiGar().getTgaIdGar());
        // COB_CODE: MOVE TGA-ID-ADES
        //             TO (SF)-ID-ADES(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdAdes(ws.getTrchDiGar().getTgaIdAdes());
        // COB_CODE: MOVE TGA-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdPoli(ws.getTrchDiGar().getTgaIdPoli());
        // COB_CODE: MOVE TGA-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaIdMoviCrz(ws.getTrchDiGar().getTgaIdMoviCrz());
        // COB_CODE: IF TGA-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE TGA-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiuNull(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE TGA-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiu());
        }
        // COB_CODE: MOVE TGA-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaDtIniEff(ws.getTrchDiGar().getTgaDtIniEff());
        // COB_CODE: MOVE TGA-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaDtEndEff(ws.getTrchDiGar().getTgaDtEndEff());
        // COB_CODE: MOVE TGA-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaCodCompAnia(ws.getTrchDiGar().getTgaCodCompAnia());
        // COB_CODE: MOVE TGA-DT-DECOR
        //             TO (SF)-DT-DECOR(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaDtDecor(ws.getTrchDiGar().getTgaDtDecor());
        // COB_CODE: IF TGA-DT-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-SCAD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtScad().getTgaDtScadNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-SCAD-NULL
            //             TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScadNull(ws.getTrchDiGar().getTgaDtScad().getTgaDtScadNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-SCAD
            //             TO (SF)-DT-SCAD(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(ws.getTrchDiGar().getTgaDtScad().getTgaDtScad());
        }
        // COB_CODE: IF TGA-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IB-OGG(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIbOgg(), TrchDiGarIvvs0216.Len.TGA_IB_OGG)) {
            // COB_CODE: MOVE TGA-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaIbOgg(ws.getTrchDiGar().getTgaIbOgg());
        }
        else {
            // COB_CODE: MOVE TGA-IB-OGG
            //             TO (SF)-IB-OGG(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaIbOgg(ws.getTrchDiGar().getTgaIbOgg());
        }
        // COB_CODE: MOVE TGA-TP-RGM-FISC
        //             TO (SF)-TP-RGM-FISC(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpRgmFisc(ws.getTrchDiGar().getTgaTpRgmFisc());
        // COB_CODE: IF TGA-DT-EMIS-NULL = HIGH-VALUES
        //                TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-EMIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmisNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-EMIS-NULL
            //             TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmisNull(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmisNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-EMIS
            //             TO (SF)-DT-EMIS(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmis());
        }
        // COB_CODE: MOVE TGA-TP-TRCH
        //             TO (SF)-TP-TRCH(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpTrch(ws.getTrchDiGar().getTgaTpTrch());
        // COB_CODE: IF TGA-DUR-AA-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-AA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurAa().getTgaDurAaNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-AA-NULL
            //             TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAaNull(ws.getTrchDiGar().getTgaDurAa().getTgaDurAaNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-AA
            //             TO (SF)-DUR-AA(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(ws.getTrchDiGar().getTgaDurAa().getTgaDurAa());
        }
        // COB_CODE: IF TGA-DUR-MM-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-MM(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurMm().getTgaDurMmNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-MM-NULL
            //             TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMmNull(ws.getTrchDiGar().getTgaDurMm().getTgaDurMmNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-MM
            //             TO (SF)-DUR-MM(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(ws.getTrchDiGar().getTgaDurMm().getTgaDurMm());
        }
        // COB_CODE: IF TGA-DUR-GG-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-GG(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurGg().getTgaDurGgNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-GG-NULL
            //             TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGgNull(ws.getTrchDiGar().getTgaDurGg().getTgaDurGgNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-GG
            //             TO (SF)-DUR-GG(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(ws.getTrchDiGar().getTgaDurGg().getTgaDurGg());
        }
        // COB_CODE: IF TGA-PRE-CASO-MOR-NULL = HIGH-VALUES
        //                TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-CASO-MOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMorNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-CASO-MOR-NULL
            //             TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMorNull(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMorNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-CASO-MOR
            //             TO (SF)-PRE-CASO-MOR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMor(), 15, 3));
        }
        // COB_CODE: IF TGA-PC-INTR-RIAT-NULL = HIGH-VALUES
        //                TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-INTR-RIAT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiatNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-INTR-RIAT-NULL
            //             TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiatNull(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-INTR-RIAT
            //             TO (SF)-PC-INTR-RIAT(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiat(), 6, 3));
        }
        // COB_CODE: IF TGA-IMP-BNS-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAnticNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-BNS-ANTIC-NULL
            //             TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAnticNull(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAnticNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-BNS-ANTIC
            //             TO (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAntic(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INI-NET-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INI-NET(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNetNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INI-NET-NULL
            //             TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNetNull(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNetNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INI-NET
            //             TO (SF)-PRE-INI-NET(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNet(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-PP-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PP-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PP-INI-NULL
            //             TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIniNull(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PP-INI
            //             TO (SF)-PRE-PP-INI(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-PP-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PP-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PP-ULT-NULL
            //             TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUltNull(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PP-ULT
            //             TO (SF)-PRE-PP-ULT(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-TARI-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-TARI-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-TARI-INI-NULL
            //             TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIniNull(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-TARI-INI
            //             TO (SF)-PRE-TARI-INI(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-TARI-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-TARI-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-TARI-ULT-NULL
            //             TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUltNull(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-TARI-ULT
            //             TO (SF)-PRE-TARI-ULT(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INVRIO-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INVRIO-INI-NULL
            //             TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIniNull(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INVRIO-INI
            //             TO (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INVRIO-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INVRIO-ULT-NULL
            //             TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUltNull(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INVRIO-ULT
            //             TO (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-RIVTO-NULL
            //             TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivtoNull(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-RIVTO
            //             TO (SF)-PRE-RIVTO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-PROF-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProfNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-PROF-NULL
            //             TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProfNull(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProfNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-PROF
            //             TO (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProf(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-SAN-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSanNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-SAN-NULL
            //             TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSanNull(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSanNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-SAN
            //             TO (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSan(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-SPO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpoNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-SPO-NULL
            //             TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpoNull(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-SPO
            //             TO (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpo(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-TEC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTecNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-TEC-NULL
            //             TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTecNull(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTecNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-TEC
            //             TO (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTec(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-ALT-SOPR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSoprNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-ALT-SOPR-NULL
            //             TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSoprNull(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSoprNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-ALT-SOPR
            //             TO (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSopr(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-STAB-NULL = HIGH-VALUES
        //                TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreStab().getTgaPreStabNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-STAB-NULL
            //             TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStabNull(ws.getTrchDiGar().getTgaPreStab().getTgaPreStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-STAB
            //             TO (SF)-PRE-STAB(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreStab().getTgaPreStab(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-EFF-STAB-NULL = HIGH-VALUES
        //                TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-EFF-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStabNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-EFF-STAB-NULL
            //             TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStabNull(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-EFF-STAB
            //             TO (SF)-DT-EFF-STAB(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStab());
        }
        // COB_CODE: IF TGA-TS-RIVAL-FIS-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFisNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-FIS-NULL
            //             TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFisNull(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFisNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-FIS
            //             TO (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFis(), 14, 9));
        }
        // COB_CODE: IF TGA-TS-RIVAL-INDICIZ-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndicizNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-INDICIZ-NULL
            //             TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndicizNull(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndicizNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-INDICIZ
            //             TO (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndiciz(), 14, 9));
        }
        // COB_CODE: IF TGA-OLD-TS-TEC-NULL = HIGH-VALUES
        //                TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-OLD-TS-TEC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTecNullFormatted())) {
            // COB_CODE: MOVE TGA-OLD-TS-TEC-NULL
            //             TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTecNull(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTecNull());
        }
        else {
            // COB_CODE: MOVE TGA-OLD-TS-TEC
            //             TO (SF)-OLD-TS-TEC(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(Trunc.toDecimal(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTec(), 14, 9));
        }
        // COB_CODE: IF TGA-RAT-LRD-NULL = HIGH-VALUES
        //                TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RAT-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-RAT-LRD-NULL
            //             TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrdNull(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-RAT-LRD
            //             TO (SF)-RAT-LRD(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrd(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-LRD-NULL = HIGH-VALUES
        //                TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-LRD-NULL
            //             TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrdNull(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-LRD
            //             TO (SF)-PRE-LRD(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrd(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NULL
            //             TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIniNull(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI
            //             TO (SF)-PRSTZ-INI(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-ULT-NULL
            //             TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUltNull(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-ULT
            //             TO (SF)-PRSTZ-ULT(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-IN-OPZ-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-IN-OPZ-RIVTO-NULL
            //             TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivtoNull(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-IN-OPZ-RIVTO
            //             TO (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-INI-STAB-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStabNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-STAB-NULL
            //             TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStabNull(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-STAB
            //             TO (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStab(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-RSH-MOR-NULL = HIGH-VALUES
        //                TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-RSH-MOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMorNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-RSH-MOR-NULL
            //             TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMorNull(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMorNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-RSH-MOR
            //             TO (SF)-CPT-RSH-MOR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMor(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-RID-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-RID-INI-NULL
            //             TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIniNull(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-RID-INI
            //             TO (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIni(), 15, 3));
        }
        // COB_CODE: IF TGA-FL-CAR-CONT-NULL = HIGH-VALUES
        //                TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-CAR-CONT(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlCarCont(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-CAR-CONT-NULL
            //             TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlCarCont(ws.getTrchDiGar().getTgaFlCarCont());
        }
        else {
            // COB_CODE: MOVE TGA-FL-CAR-CONT
            //             TO (SF)-FL-CAR-CONT(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlCarCont(ws.getTrchDiGar().getTgaFlCarCont());
        }
        // COB_CODE: IF TGA-BNS-GIA-LIQTO-NULL = HIGH-VALUES
        //                TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqtoNullFormatted())) {
            // COB_CODE: MOVE TGA-BNS-GIA-LIQTO-NULL
            //             TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqtoNull(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-BNS-GIA-LIQTO
            //             TO (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(Trunc.toDecimal(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqto(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-BNS-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-BNS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpBns().getTgaImpBnsNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-BNS-NULL
            //             TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBnsNull(ws.getTrchDiGar().getTgaImpBns().getTgaImpBnsNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-BNS
            //             TO (SF)-IMP-BNS(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpBns().getTgaImpBns(), 15, 3));
        }
        // COB_CODE: MOVE TGA-COD-DVS
        //             TO (SF)-COD-DVS(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaCodDvs(ws.getTrchDiGar().getTgaCodDvs());
        // COB_CODE: IF TGA-PRSTZ-INI-NEWFIS-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfisNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NEWFIS-NULL
            //             TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfisNull(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfisNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NEWFIS
            //             TO (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfis(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SCON-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SCON(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpScon().getTgaImpSconNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SCON-NULL
            //             TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpSconNull(ws.getTrchDiGar().getTgaImpScon().getTgaImpSconNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SCON
            //             TO (SF)-IMP-SCON(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpScon().getTgaImpScon(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-SCON-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-SCON(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqSconNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-SCON-NULL
            //             TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqSconNull(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqSconNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-SCON
            //             TO (SF)-ALQ-SCON(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqScon(), 6, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-ACQ-NULL
            //             TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcqNull(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-ACQ
            //             TO (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-INC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarIncNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-INC-NULL
            //             TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarIncNull(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-INC
            //             TO (SF)-IMP-CAR-INC(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarInc(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-GEST-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGestNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-GEST-NULL
            //             TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGestNull(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-GEST
            //             TO (SF)-IMP-CAR-GEST(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGest(), 15, 3));
        }
        // COB_CODE: IF TGA-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-1O-ASSTO-NULL
            //             TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAsstoNull(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-1O-ASSTO
            //             TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-1O-ASSTO-NULL
            //             TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAsstoNull(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-1O-ASSTO
            //             TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAssto());
        }
        // COB_CODE: IF TGA-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-2O-ASSTO-NULL
            //             TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAsstoNull(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-2O-ASSTO
            //             TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-2O-ASSTO-NULL
            //             TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAsstoNull(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-2O-ASSTO
            //             TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAssto());
        }
        // COB_CODE: IF TGA-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-3O-ASSTO-NULL
            //             TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAsstoNull(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-3O-ASSTO
            //             TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-3O-ASSTO-NULL
            //             TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAsstoNull(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-3O-ASSTO
            //             TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAssto());
        }
        // COB_CODE: IF TGA-RENDTO-LRD-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RENDTO-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-RENDTO-LRD-NULL
            //             TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrdNull(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-RENDTO-LRD
            //             TO (SF)-RENDTO-LRD(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrd(), 14, 9));
        }
        // COB_CODE: IF TGA-PC-RETR-NULL = HIGH-VALUES
        //                TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-RETR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetrNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-RETR-NULL
            //             TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetrNull(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetrNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-RETR
            //             TO (SF)-PC-RETR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetr(), 6, 3));
        }
        // COB_CODE: IF TGA-RENDTO-RETR-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RENDTO-RETR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetrNullFormatted())) {
            // COB_CODE: MOVE TGA-RENDTO-RETR-NULL
            //             TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetrNull(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetrNull());
        }
        else {
            // COB_CODE: MOVE TGA-RENDTO-RETR
            //             TO (SF)-RENDTO-RETR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(Trunc.toDecimal(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetr(), 14, 9));
        }
        // COB_CODE: IF TGA-MIN-GARTO-NULL = HIGH-VALUES
        //                TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MIN-GARTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGartoNullFormatted())) {
            // COB_CODE: MOVE TGA-MIN-GARTO-NULL
            //             TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGartoNull(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGartoNull());
        }
        else {
            // COB_CODE: MOVE TGA-MIN-GARTO
            //             TO (SF)-MIN-GARTO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(Trunc.toDecimal(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGarto(), 14, 9));
        }
        // COB_CODE: IF TGA-MIN-TRNUT-NULL = HIGH-VALUES
        //                TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MIN-TRNUT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnutNullFormatted())) {
            // COB_CODE: MOVE TGA-MIN-TRNUT-NULL
            //             TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnutNull(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnutNull());
        }
        else {
            // COB_CODE: MOVE TGA-MIN-TRNUT
            //             TO (SF)-MIN-TRNUT(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(Trunc.toDecimal(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnut(), 14, 9));
        }
        // COB_CODE: IF TGA-PRE-ATT-DI-TRCH-NULL = HIGH-VALUES
        //                TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrchNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-ATT-DI-TRCH-NULL
            //             TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrchNull(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrchNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-ATT-DI-TRCH
            //             TO (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrch(), 15, 3));
        }
        // COB_CODE: IF TGA-MATU-END2000-NULL = HIGH-VALUES
        //                TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MATU-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-MATU-END2000-NULL
            //             TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000Null(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-MATU-END2000
            //             TO (SF)-MATU-END2000(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-TOT-INI-NULL = HIGH-VALUES
        //                TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-TOT-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIniNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-TOT-INI-NULL
            //             TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIniNull(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-TOT-INI
            //             TO (SF)-ABB-TOT-INI(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIni(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-TOT-ULT-NULL = HIGH-VALUES
        //                TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-TOT-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUltNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-TOT-ULT-NULL
            //             TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUltNull(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-TOT-ULT
            //             TO (SF)-ABB-TOT-ULT(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-ANNU-ULT-NULL = HIGH-VALUES
        //                TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUltNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-ANNU-ULT-NULL
            //             TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUltNull(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-ANNU-ULT
            //             TO (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-DUR-ABB-NULL = HIGH-VALUES
        //                TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-ABB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbbNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-ABB-NULL
            //             TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbbNull(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbbNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-ABB
            //             TO (SF)-DUR-ABB(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbb());
        }
        // COB_CODE: IF TGA-TP-ADEG-ABB-NULL = HIGH-VALUES
        //                TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-ADEG-ABB(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaTpAdegAbb(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-TP-ADEG-ABB-NULL
            //             TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpAdegAbb(ws.getTrchDiGar().getTgaTpAdegAbb());
        }
        else {
            // COB_CODE: MOVE TGA-TP-ADEG-ABB
            //             TO (SF)-TP-ADEG-ABB(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpAdegAbb(ws.getTrchDiGar().getTgaTpAdegAbb());
        }
        // COB_CODE: IF TGA-MOD-CALC-NULL = HIGH-VALUES
        //                TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MOD-CALC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaModCalcFormatted())) {
            // COB_CODE: MOVE TGA-MOD-CALC-NULL
            //             TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaModCalc(ws.getTrchDiGar().getTgaModCalc());
        }
        else {
            // COB_CODE: MOVE TGA-MOD-CALC
            //             TO (SF)-MOD-CALC(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaModCalc(ws.getTrchDiGar().getTgaModCalc());
        }
        // COB_CODE: IF TGA-IMP-AZ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-AZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAz().getTgaImpAzNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-AZ-NULL
            //             TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAzNull(ws.getTrchDiGar().getTgaImpAz().getTgaImpAzNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-AZ
            //             TO (SF)-IMP-AZ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAz().getTgaImpAz(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-ADER-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-ADER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAder().getTgaImpAderNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-ADER-NULL
            //             TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAderNull(ws.getTrchDiGar().getTgaImpAder().getTgaImpAderNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-ADER
            //             TO (SF)-IMP-ADER(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAder().getTgaImpAder(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-TFR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TFR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfrNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TFR-NULL
            //             TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfrNull(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfrNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TFR
            //             TO (SF)-IMP-TFR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfr(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-VOLO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVoloNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-VOLO-NULL
            //             TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVoloNull(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVoloNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-VOLO
            //             TO (SF)-IMP-VOLO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVolo(), 15, 3));
        }
        // COB_CODE: IF TGA-VIS-END2000-NULL = HIGH-VALUES
        //                TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-VIS-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-VIS-END2000-NULL
            //             TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000Null(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-VIS-END2000
            //             TO (SF)-VIS-END2000(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-VLDT-PROD-NULL = HIGH-VALUES
        //                TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-VLDT-PROD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProdNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD-NULL
            //             TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProdNull(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProdNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD
            //             TO (SF)-DT-VLDT-PROD(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProd());
        }
        // COB_CODE: IF TGA-DT-INI-VAL-TAR-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTarNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR-NULL
            //             TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTarNull(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTarNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR
            //             TO (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTar());
        }
        // COB_CODE: IF TGA-IMPB-VIS-END2000-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-VIS-END2000-NULL
            //             TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000Null(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-VIS-END2000
            //             TO (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-REN-INI-TS-TEC-0-NULL = HIGH-VALUES
        //                TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0NullFormatted())) {
            // COB_CODE: MOVE TGA-REN-INI-TS-TEC-0-NULL
            //             TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0Null(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0Null());
        }
        else {
            // COB_CODE: MOVE TGA-REN-INI-TS-TEC-0
            //             TO (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(Trunc.toDecimal(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0(), 15, 3));
        }
        // COB_CODE: IF TGA-PC-RIP-PRE-NULL = HIGH-VALUES
        //                TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-RIP-PRE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPreNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-RIP-PRE-NULL
            //             TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPreNull(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPreNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-RIP-PRE
            //             TO (SF)-PC-RIP-PRE(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPre(), 6, 3));
        }
        // COB_CODE: IF TGA-FL-IMPORTI-FORZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlImportiForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-IMPORTI-FORZ-NULL
            //             TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlImportiForz(ws.getTrchDiGar().getTgaFlImportiForz());
        }
        else {
            // COB_CODE: MOVE TGA-FL-IMPORTI-FORZ
            //             TO (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlImportiForz(ws.getTrchDiGar().getTgaFlImportiForz());
        }
        // COB_CODE: IF TGA-PRSTZ-INI-NFORZ-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforzNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NFORZ-NULL
            //             TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforzNull(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforzNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NFORZ
            //             TO (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforz(), 15, 3));
        }
        // COB_CODE: IF TGA-VIS-END2000-NFORZ-NULL = HIGH-VALUES
        //                TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000NforzNullFormatted())) {
            // COB_CODE: MOVE TGA-VIS-END2000-NFORZ-NULL
            //             TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000NforzNull(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000NforzNull());
        }
        else {
            // COB_CODE: MOVE TGA-VIS-END2000-NFORZ
            //             TO (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(Trunc.toDecimal(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000Nforz(), 15, 3));
        }
        // COB_CODE: IF TGA-INTR-MORA-NULL = HIGH-VALUES
        //                TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INTR-MORA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMoraNullFormatted())) {
            // COB_CODE: MOVE TGA-INTR-MORA-NULL
            //             TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMoraNull(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE TGA-INTR-MORA
            //             TO (SF)-INTR-MORA(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(Trunc.toDecimal(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMora(), 15, 3));
        }
        // COB_CODE: IF TGA-MANFEE-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MANFEE-ANTIC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE TGA-MANFEE-ANTIC-NULL
            //             TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAnticNull(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE TGA-MANFEE-ANTIC
            //             TO (SF)-MANFEE-ANTIC(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(Trunc.toDecimal(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF TGA-MANFEE-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MANFEE-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-MANFEE-RICOR-NULL
            //             TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicorNull(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-MANFEE-RICOR
            //             TO (SF)-MANFEE-RICOR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-UNI-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-UNI-RIVTO-NULL
            //             TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivtoNull(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-UNI-RIVTO
            //             TO (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-1AA-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-1AA-ACQ-NULL
            //             TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcqNull(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-1AA-ACQ
            //             TO (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-2AA-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-2AA-ACQ-NULL
            //             TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcqNull(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-2AA-ACQ
            //             TO (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-RICOR-NULL
            //             TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicorNull(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-RICOR
            //             TO (SF)-PROV-RICOR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProvInc().getTgaProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-INC-NULL
            //             TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvIncNull(ws.getTrchDiGar().getTgaProvInc().getTgaProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-INC
            //             TO (SF)-PROV-INC(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaProvInc().getTgaProvInc(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-ACQ-NULL
            //             TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcqNull(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-ACQ
            //             TO (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcq(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-INC-NULL
            //             TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvIncNull(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-INC
            //             TO (SF)-ALQ-PROV-INC(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvInc(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-RICOR-NULL
            //             TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicorNull(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-RICOR
            //             TO (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicor(), 6, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-ACQ-NULL
            //             TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcqNull(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-ACQ
            //             TO (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-INC-NULL
            //             TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvIncNull(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-INC
            //             TO (SF)-IMPB-PROV-INC(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvInc(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-RICOR-NULL
            //             TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicorNull(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-RICOR
            //             TO (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-FL-PROV-FORZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-PROV-FORZ(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlProvForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-PROV-FORZ-NULL
            //             TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlProvForz(ws.getTrchDiGar().getTgaFlProvForz());
        }
        else {
            // COB_CODE: MOVE TGA-FL-PROV-FORZ
            //             TO (SF)-FL-PROV-FORZ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaFlProvForz(ws.getTrchDiGar().getTgaFlProvForz());
        }
        // COB_CODE: IF TGA-PRSTZ-AGG-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-INI-NULL
            //             TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIniNull(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-INI
            //             TO (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIni(), 15, 3));
        }
        // COB_CODE: IF TGA-INCR-PRE-NULL = HIGH-VALUES
        //                TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INCR-PRE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPreNullFormatted())) {
            // COB_CODE: MOVE TGA-INCR-PRE-NULL
            //             TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPreNull(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPreNull());
        }
        else {
            // COB_CODE: MOVE TGA-INCR-PRE
            //             TO (SF)-INCR-PRE(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(Trunc.toDecimal(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPre(), 15, 3));
        }
        // COB_CODE: IF TGA-INCR-PRSTZ-NULL = HIGH-VALUES
        //                TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INCR-PRSTZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstzNullFormatted())) {
            // COB_CODE: MOVE TGA-INCR-PRSTZ-NULL
            //             TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstzNull(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstzNull());
        }
        else {
            // COB_CODE: MOVE TGA-INCR-PRSTZ
            //             TO (SF)-INCR-PRSTZ(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(Trunc.toDecimal(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstz(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-ULT-ADEG-PRE-PR-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePrNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR-NULL
            //             TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePrNull(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePrNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR
            //             TO (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePr());
        }
        // COB_CODE: IF TGA-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-ULT-NULL
            //             TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUltNull(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-ULT
            //             TO (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-TS-RIVAL-NET-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-NET(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNetNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-NET-NULL
            //             TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNetNull(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNetNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-NET
            //             TO (SF)-TS-RIVAL-NET(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNet(), 14, 9));
        }
        // COB_CODE: IF TGA-PRE-PATTUITO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PATTUITO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuitoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PATTUITO-NULL
            //             TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuitoNull(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuitoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PATTUITO
            //             TO (SF)-PRE-PATTUITO(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuito(), 15, 3));
        }
        // COB_CODE: IF TGA-TP-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-RIVAL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTpRivalFormatted())) {
            // COB_CODE: MOVE TGA-TP-RIVAL-NULL
            //             TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpRival(ws.getTrchDiGar().getTgaTpRival());
        }
        else {
            // COB_CODE: MOVE TGA-TP-RIVAL
            //             TO (SF)-TP-RIVAL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpRival(ws.getTrchDiGar().getTgaTpRival());
        }
        // COB_CODE: IF TGA-RIS-MAT-NULL = HIGH-VALUES
        //                TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RIS-MAT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRisMat().getTgaRisMatNullFormatted())) {
            // COB_CODE: MOVE TGA-RIS-MAT-NULL
            //             TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMatNull(ws.getTrchDiGar().getTgaRisMat().getTgaRisMatNull());
        }
        else {
            // COB_CODE: MOVE TGA-RIS-MAT
            //             TO (SF)-RIS-MAT(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(Trunc.toDecimal(ws.getTrchDiGar().getTgaRisMat().getTgaRisMat(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-MIN-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScadNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-MIN-SCAD-NULL
            //             TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScadNull(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScadNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-MIN-SCAD
            //             TO (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScad(), 15, 3));
        }
        // COB_CODE: IF TGA-COMMIS-GEST-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COMMIS-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGestNullFormatted())) {
            // COB_CODE: MOVE TGA-COMMIS-GEST-NULL
            //             TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGestNull(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-COMMIS-GEST
            //             TO (SF)-COMMIS-GEST(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGest(), 15, 3));
        }
        // COB_CODE: IF TGA-TP-MANFEE-APPL-NULL = HIGH-VALUES
        //                TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTpManfeeApplFormatted())) {
            // COB_CODE: MOVE TGA-TP-MANFEE-APPL-NULL
            //             TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl(ws.getTrchDiGar().getTgaTpManfeeAppl());
        }
        else {
            // COB_CODE: MOVE TGA-TP-MANFEE-APPL
            //             TO (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl(ws.getTrchDiGar().getTgaTpManfeeAppl());
        }
        // COB_CODE: MOVE TGA-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsRiga(ws.getTrchDiGar().getTgaDsRiga());
        // COB_CODE: MOVE TGA-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsOperSql(ws.getTrchDiGar().getTgaDsOperSql());
        // COB_CODE: MOVE TGA-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsVer(ws.getTrchDiGar().getTgaDsVer());
        // COB_CODE: MOVE TGA-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsTsIniCptz(ws.getTrchDiGar().getTgaDsTsIniCptz());
        // COB_CODE: MOVE TGA-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsTsEndCptz(ws.getTrchDiGar().getTgaDsTsEndCptz());
        // COB_CODE: MOVE TGA-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsUtente(ws.getTrchDiGar().getTgaDsUtente());
        // COB_CODE: MOVE TGA-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-TGA)
        ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaDsStatoElab(ws.getTrchDiGar().getTgaDsStatoElab());
        // COB_CODE: IF TGA-PC-COMMIS-GEST-NULL = HIGH-VALUES
        //                TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGestNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-COMMIS-GEST-NULL
            //             TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGestNull(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-COMMIS-GEST
            //             TO (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGest(), 6, 3));
        }
        // COB_CODE: IF TGA-NUM-GG-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRivalNullFormatted())) {
            // COB_CODE: MOVE TGA-NUM-GG-RIVAL-NULL
            //             TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRivalNull(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRivalNull());
        }
        else {
            // COB_CODE: MOVE TGA-NUM-GG-RIVAL
            //             TO (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRival());
        }
        // COB_CODE: IF TGA-IMP-TRASFE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TRASFE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TRASFE-NULL
            //             TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfeNull(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TRASFE
            //             TO (SF)-IMP-TRASFE(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-TFR-STRC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TFR-STRC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TFR-STRC-NULL
            //             TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrcNull(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TFR-STRC
            //             TO (SF)-IMP-TFR-STRC(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF TGA-ACQ-EXP-NULL = HIGH-VALUES
        //                TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ACQ-EXP(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExpNullFormatted())) {
            // COB_CODE: MOVE TGA-ACQ-EXP-NULL
            //             TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExpNull(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExpNull());
        }
        else {
            // COB_CODE: MOVE TGA-ACQ-EXP
            //             TO (SF)-ACQ-EXP(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(Trunc.toDecimal(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExp(), 15, 3));
        }
        // COB_CODE: IF TGA-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-REMUN-ASS-NULL
            //             TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAssNull(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-REMUN-ASS
            //             TO (SF)-REMUN-ASS(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAss(), 15, 3));
        }
        // COB_CODE: IF TGA-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-COMMIS-INTER-NULL
            //             TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInterNull(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-COMMIS-INTER
            //             TO (SF)-COMMIS-INTER(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInter(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-REMUN-ASS-NULL
            //             TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAssNull(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-REMUN-ASS
            //             TO (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAss(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-COMMIS-INTER-NULL
            //             TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInterNull(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-COMMIS-INTER
            //             TO (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInter(), 6, 3));
        }
        // COB_CODE: IF TGA-IMPB-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-REMUN-ASS-NULL
            //             TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAssNull(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-REMUN-ASS
            //             TO (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAss(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-COMMIS-INTER-NULL
            //             TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInterNull(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-COMMIS-INTER
            //             TO (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInter(), 15, 3));
        }
        // COB_CODE: IF TGA-COS-RUN-ASSVA-NULL = HIGH-VALUES
        //                TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssvaNullFormatted())) {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-NULL
            //             TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssvaNull(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssvaNull());
        }
        else {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA
            //             TO (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(Trunc.toDecimal(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssva(), 15, 3));
        }
        // COB_CODE: IF TGA-COS-RUN-ASSVA-IDC-NULL = HIGH-VALUES
        //                TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdcNullFormatted())) {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-IDC-NULL
            //             TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdcNull(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdcNull());
        }
        else {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-IDC
            //             TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
            ws.getVsTgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(Trunc.toDecimal(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdc(), 15, 3));
        }
    }

    /**Original name: CALL-MATR-MOVIMENTO<br>
	 * <pre>    COPY LCCVPCO3 REPLACING ==(SF)== BY ==VS-PCO==.
	 * ----------------------------------------------------------------*
	 *     COPY PER LETTURA MATRICE MOVIMENTO
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     CALL MATRICE MOVIMENTO
	 * *****************************************************************</pre>*/
    private void callMatrMovimento() {
        Lccs0020 lccs0020 = null;
        // COB_CODE: MOVE 'LCCS0020' TO LCCV0021-PGM
        ws.getLccv0021().setPgm("LCCS0020");
        // COB_CODE: CALL LCCV0021-PGM USING AREA-IDSV0001 LCCV0021
        //           ON EXCEPTION
        //                     THRU EX-S0290
        //           END-CALL.
        try {
            lccs0020 = Lccs0020.getInstance();
            lccs0020.run(areaIdsv0001, ws.getLccv0021());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CALL-MATR-MOVIMENTO'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("CALL-MATR-MOVIMENTO");
            // COB_CODE: MOVE 'CALL-MATR-MOVIMENTO'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-MATR-MOVIMENTO");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF NOT LCCV0021-SUCCESSFUL-RC
            //           OR NOT LCCV0021-SUCCESSFUL-SQL
            //                 THRU EX-S0290
            //           END-IF
            if (!ws.getLccv0021().getAreaOutput().getReturnCode().isSuccessfulRc() || !ws.getLccv0021().getAreaOutput().getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0001-ESITO-KO
                //            TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // COB_CODE: MOVE LCCV0021-COD-SERVIZIO-BE
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getLccv0021().getAreaOutput().getCodServizioBe());
                // COB_CODE: MOVE LCCV0021-DESCRIZ-ERR
                //             TO CALL-DESC
                ws.getIdsv0002().setCallDesc(ws.getLccv0021().getAreaOutput().getDescrizErr());
                // COB_CODE: MOVE 'CALL-MATR-MOVIMENTO'
                //              TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("CALL-MATR-MOVIMENTO");
                // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
                //              THRU EX-S0290
                s0290ErroreDiSistema();
            }
        }
    }

    /**Original name: GESTIONE-TEMP-TABLE<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PER LA GESTIONE DELLA TABELLA TEMP.
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     SALVO AREA SU TABELLA TEMPORANEA
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneTempTable() {
        Idss0300 idss0300 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO IDSV0301-COD-COMP-ANIA
        ws.getIdsv0301().setCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-ID-TEMPORARY-DATA
        //             TO IDSV0301-ID-TEMPORARY-DATA
        ws.getIdsv0301().setIdTemporaryData(areaIdsv0001.getAreaComune().getIdTemporaryData());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //             TO IDSV0301-ID-SESSION.
        ws.getIdsv0301().setIdSession(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: IF IDSV0001-ON-LINE
        //               TO TRUE
        //           ELSE
        //                TO IDSV0301-TEMPORARY-TABLE
        //           END-IF.
        if (areaIdsv0001.getAreaComune().getModalitaEsecutiva().isIdsv0001OnLine()) {
            // COB_CODE: SET IDSV0301-STATIC-TEMP-TABLE
            //            TO TRUE
            ws.getIdsv0301().getTemporaryTable().setStaticTempTable();
        }
        else {
            // COB_CODE: MOVE IDSV0001-TEMPORARY-TABLE
            //             TO IDSV0301-TEMPORARY-TABLE
            ws.getIdsv0301().getTemporaryTable().setTemporaryTable(areaIdsv0001.getAreaComune().getTemporaryTable().getTemporaryTable());
        }
        // COB_CODE: CALL IDSS0300   USING IDSV0301
        //           ON EXCEPTION
        //               MOVE IDSV0001-ESITO           TO IDSV0301-ESITO
        //           END-CALL.
        try {
            idss0300 = Idss0300.getInstance();
            idss0300.run(ws.getIdsv0301());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE IDSS0300               TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getIdss0300());
            // COB_CODE: MOVE 'CALL SERVIZIO TEMPORARY TABLE'
            //                                       TO CALL-DESC
            ws.getIdsv0002().setCallDesc("CALL SERVIZIO TEMPORARY TABLE");
            // COB_CODE: MOVE 'GESTIONE-TEMP-TABLE'    TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("GESTIONE-TEMP-TABLE");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
            // COB_CODE: MOVE IDSV0001-ESITO           TO IDSV0301-ESITO
            ws.getIdsv0301().getEsito().setEsito(areaIdsv0001.getEsito().getEsito());
        }
        // COB_CODE: IF IDSV0301-ESITO-KO AND IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIdsv0301().getEsito().isKo() && areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF NOT IDSV0301-NOT-FOUND
            //                 THRU EX-S0300
            //           END-IF
            if (!ws.getIdsv0301().getSqlcodeSigned().isNotFound()) {
                // COB_CODE: MOVE IDSS0300              TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getIdss0300());
                // COB_CODE: MOVE 'GESTIONE-TEMP-TABLE'   TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("GESTIONE-TEMP-TABLE");
                // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING 'TAB TEMPORANEA SQLCODE : '
                //                  IDSV0301-SQLCODE  ' ;'
                //                  IDSV0301-DESC-ERRORE-ESTESA
                //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "TAB TEMPORANEA SQLCODE : ", ws.getIdsv0301().getSqlcodeAsString(), " ;", ws.getIdsv0301().getDescErroreEstesaFormatted());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: GESTIONE-ELE-MAX-TEMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     SALVO ELEMETI DA ELABORATE SU ELE MAX TABELLA APPOGGIO
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneEleMaxTemp() {
        // COB_CODE: MOVE ZERO                      TO IDSV0303-ELE-MAX-TOT
        //                                             IDSV0303-ELE-MAX-APPO.
        ws.getIdsv0303().setIdsv0303EleMaxTot(0);
        ws.getIdsv0303().setIdsv0303EleMaxAppo(0);
        // COB_CODE: IF IDSV0301-PARTIAL-RECURRENCE NOT GREATER
        //              IDSV0301-ACTUAL-RECURRENCE
        //              END-IF
        //           ELSE
        //                TO IDSV0303-ELE-MAX-TOT
        //           END-IF.
        if (ws.getIdsv0301().getPartialRecurrence() <= ws.getIdsv0301().getActualRecurrence()) {
            // COB_CODE: ADD IDSV0301-PARTIAL-RECURRENCE
            //             TO IDSV0303-ELE-MAX-APPO
            ws.getIdsv0303().setIdsv0303EleMaxAppo(Trunc.toInt(ws.getIdsv0301().getPartialRecurrence() + ws.getIdsv0303().getIdsv0303EleMaxAppo(), 5));
            // COB_CODE: IF IDSV0303-ELE-MAX-APPO NOT GREATER
            //              IDSV0301-ACTUAL-RECURRENCE
            //                TO IDSV0303-ELE-MAX-TOT
            //           ELSE
            //                      IDSV0301-ACTUAL-RECURRENCE
            //           END-IF
            if (ws.getIdsv0303().getIdsv0303EleMaxAppo() <= ws.getIdsv0301().getActualRecurrence()) {
                // COB_CODE: MOVE IDSV0301-PARTIAL-RECURRENCE
                //             TO IDSV0303-ELE-MAX-TOT
                ws.getIdsv0303().setIdsv0303EleMaxTot(ws.getIdsv0301().getPartialRecurrence());
            }
            else {
                // COB_CODE: COMPUTE IDSV0303-ELE-MAX-TOT =
                //                   IDSV0303-ELE-MAX-APPO -
                //                   IDSV0301-ACTUAL-RECURRENCE
                ws.getIdsv0303().setIdsv0303EleMaxTot(Trunc.toInt(ws.getIdsv0303().getIdsv0303EleMaxAppo() - ws.getIdsv0301().getActualRecurrence(), 5));
            }
        }
        else {
            // COB_CODE: MOVE IDSV0301-ACTUAL-RECURRENCE
            //             TO IDSV0303-ELE-MAX-TOT
            ws.getIdsv0303().setIdsv0303EleMaxTot(ws.getIdsv0301().getActualRecurrence());
        }
    }

    /**Original name: VAL-SCHEDE-ISPC0140<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PER VALORIZZAZIONE SCHEDE SERVIZIO ISPS0140
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     VALORIZZAZIONE SCHEDE ISPC0140
	 * ----------------------------------------------------------------*
	 * --> VALORIZZAZIONE OCCURS AREA INPUT SERVIZIO CALCOLI E CONTROLLI
	 * --> CON L'OUTPUT DEL VALORIZZATORE VARIABILI</pre>*/
    private void valSchedeIspc0140() {
        // COB_CODE: MOVE WSKD-DEE
        //             TO ISPC0140-DEE
        ws.getAreaIoCalcContr().getDatiInput().setDee(ws.getIvvc0216().getWskdDee());
        // COB_CODE: PERFORM AREA-SCHEDA-P-ISPC0140
        //              THRU AREA-SCHEDA-P-ISPC0140-EX
        //           VARYING IX-AREA-SCHEDA-P FROM 1 BY 1
        //             UNTIL IX-AREA-SCHEDA-P > WSKD-ELE-LIVELLO-MAX-P.
        ws.getIxIndici().setAreaSchedaP(((short)1));
        while (!(ws.getIxIndici().getAreaSchedaP() > ws.getIvvc0216().getWskdEleLivelloMaxP())) {
            areaSchedaPIspc0140();
            ws.getIxIndici().setAreaSchedaP(Trunc.toShort(ws.getIxIndici().getAreaSchedaP() + 1, 4));
        }
        // COB_CODE: MOVE WSKD-ELE-LIVELLO-MAX-P   TO ISPC0140-ELE-MAX-SCHEDA-P
        ws.getAreaIoCalcContr().getDatiInput().setIspc0140EleMaxSchedaP(TruncAbs.toShort(ws.getIvvc0216().getWskdEleLivelloMaxP(), 4));
        // COB_CODE: PERFORM AREA-SCHEDA-T-ISPC0140
        //              THRU AREA-SCHEDA-T-ISPC0140-EX
        //           VARYING IX-AREA-SCHEDA-T FROM 1 BY 1
        //             UNTIL IX-AREA-SCHEDA-T > WSKD-ELE-LIVELLO-MAX-T.
        ws.getIxIndici().setAreaSchedaT(((short)1));
        while (!(ws.getIxIndici().getAreaSchedaT() > ws.getIvvc0216().getWskdEleLivelloMaxT())) {
            areaSchedaTIspc0140();
            ws.getIxIndici().setAreaSchedaT(Trunc.toShort(ws.getIxIndici().getAreaSchedaT() + 1, 4));
        }
        // COB_CODE: MOVE WSKD-ELE-LIVELLO-MAX-T   TO ISPC0140-ELE-MAX-SCHEDA-T.
        ws.getAreaIoCalcContr().getDatiInput().setIspc0140EleMaxSchedaT(TruncAbs.toShort(ws.getIvvc0216().getWskdEleLivelloMaxT(), 4));
    }

    /**Original name: AREA-SCHEDA-P-ISPC0140<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE AREA SCHEDA P
	 * ----------------------------------------------------------------*</pre>*/
    private void areaSchedaPIspc0140() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WSKD-TP-LIVELLO-P(IX-AREA-SCHEDA-P)
        //                     TO ISPC0140-TIPO-LIVELLO-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaP(ws.getIxIndici().getAreaSchedaP()).setTipoLivelloP(ws.getIvvc0216().getWskdTabValP().getTpLivelloP(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE: MOVE WSKD-COD-LIVELLO-P(IX-AREA-SCHEDA-P)
        //                     TO ISPC0140-CODICE-LIVELLO-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaP(ws.getIxIndici().getAreaSchedaP()).setCodiceLivelloP(ws.getIvvc0216().getWskdTabValP().getCodLivelloP(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE: IF WSKD-TP-LIVELLO-P(IX-AREA-SCHEDA-P) = 'P'
        //                TO ISPC0140-FLAG-MOD-CALC-PRE-P(IX-AREA-SCHEDA-P)
        //           END-IF.
        if (ws.getIvvc0216().getWskdTabValP().getTpLivelloP(ws.getIxIndici().getAreaSchedaP()) == 'P') {
            // COB_CODE: MOVE WADE-MOD-CALC
            //             TO ISPC0140-FLAG-MOD-CALC-PRE-P(IX-AREA-SCHEDA-P)
            ws.getAreaIoCalcContr().getDatiInput().getSchedaP(ws.getIxIndici().getAreaSchedaP()).getFlagModCalcPreP().setIspc0140FlagModCalcPrePFormatted(wadeAreaAdesione.getLccvade1().getDati().getWadeModCalcFormatted());
        }
        // COB_CODE: PERFORM AREA-VAR-P-ISPC0140
        //              THRU AREA-VAR-P-ISPC0140-EX
        //               VARYING IX-TAB-VAR-P FROM 1 BY 1
        //                 UNTIL IX-TAB-VAR-P >
        //                       WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA-P)
        //                    OR IX-TAB-VAR-P > WK-ISPC0140-NUM-COMPON-MAX-P.
        ws.getIxIndici().setTabVarP(((short)1));
        while (!(ws.getIxIndici().getTabVarP() > ws.getIvvc0216().getWskdTabValP().getEleVariabiliMaxP(ws.getIxIndici().getAreaSchedaP()) || ws.getIxIndici().getTabVarP() > ws.getWkIspcMax().getIspc0140NumComponMaxP())) {
            areaVarPIspc0140();
            ws.getIxIndici().setTabVarP(Trunc.toShort(ws.getIxIndici().getTabVarP() + 1, 4));
        }
        // COB_CODE: IF WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA-P) >
        //                             WK-ISPC0140-NUM-COMPON-MAX-P
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getIvvc0216().getWskdTabValP().getEleVariabiliMaxP(ws.getIxIndici().getAreaSchedaP()) > ws.getWkIspcMax().getIspc0140NumComponMaxP()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S12100-AREA-SCHEDA'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S12100-AREA-SCHEDA");
            // COB_CODE: MOVE '005247'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005247");
            // COB_CODE: STRING 'NUMERO VARIABILI SUPERA LIMITE PREVISTO'
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr("NUMERO VARIABILI SUPERA LIMITE PREVISTO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: AREA-VAR-P-ISPC0140<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELL'AREA VARIABILI DI PRODOTTO
	 *     DI INPUT DEL SERVIZIO CALCOLI E CONTROLLI
	 * ----------------------------------------------------------------*</pre>*/
    private void areaVarPIspc0140() {
        // COB_CODE: ADD 1
        //            TO ISPC0140-NUM-COMPON-MAX-ELE-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaP(ws.getIxIndici().getAreaSchedaP()).setIspc0140NumComponMaxEleP(Trunc.toShort(1 + ws.getAreaIoCalcContr().getDatiInput().getSchedaP(ws.getIxIndici().getAreaSchedaP()).getIspc0140NumComponMaxEleP(), 3));
        // COB_CODE: MOVE WSKD-COD-VARIABILE-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        //             TO ISPC0140-CODICE-VARIABILE-P
        //                                    (IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaP(ws.getIxIndici().getAreaSchedaP()).getAreaVariabiliP(ws.getIxIndici().getTabVarP()).setCodiceVariabileP(ws.getIvvc0216().getWskdTabValP().getCodVariabileP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP()));
        // COB_CODE: MOVE WSKD-TP-DATO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        //             TO ISPC0140-TIPO-DATO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaP(ws.getIxIndici().getAreaSchedaP()).getAreaVariabiliP(ws.getIxIndici().getTabVarP()).setTipoDatoP(ws.getIvvc0216().getWskdTabValP().getTpDatoP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP()));
        // COB_CODE: MOVE WSKD-VAL-GENERICO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        //             TO ISPC0140-VAL-GENERICO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P).
        ws.getAreaIoCalcContr().getDatiInput().getSchedaP(ws.getIxIndici().getAreaSchedaP()).getAreaVariabiliP(ws.getIxIndici().getTabVarP()).setValGenericoP(ws.getIvvc0216().getWskdTabValP().getValGenericoP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP()));
    }

    /**Original name: AREA-SCHEDA-T-ISPC0140<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE AREA SCHEDA T
	 * ----------------------------------------------------------------*</pre>*/
    private void areaSchedaTIspc0140() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WSKD-TP-LIVELLO-T(IX-AREA-SCHEDA-T)
        //                     TO ISPC0140-TIPO-LIVELLO-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).setTipoLivelloT(ws.getIvvc0216().getWskdTabValT().getTpLivelloT(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-COD-LIVELLO-T(IX-AREA-SCHEDA-T)
        //                     TO ISPC0140-CODICE-LIVELLO-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).setCodiceLivelloT(ws.getIvvc0216().getWskdTabValT().getCodLivelloT(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-TIPO-TRCH(IX-AREA-SCHEDA-T)
        //                     TO ISPC0140-TIPO-TRCH(IX-AREA-SCHEDA-T)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).setIspc0140TipoTrchFormatted(ws.getIvvc0216().getWskdTabValT().getTipoTrchFormatted(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-FLG-ITN(IX-AREA-SCHEDA-T)
        //                     TO ISPC0140-FLG-ITN(IX-AREA-SCHEDA-T)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).getFlgItn().setIspc0140FlgItnFormatted(ws.getIvvc0216().getWskdTabValT().getFlgItnFormatted(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: IF WSKD-TP-LIVELLO-T(IX-AREA-SCHEDA-T) NOT = 'P'
        //                   OR WK-GRZ-TROVATA
        //           END-IF.
        if (ws.getIvvc0216().getWskdTabValT().getTpLivelloT(ws.getIxIndici().getAreaSchedaT()) != 'P') {
            // COB_CODE: SET WK-GRZ-NON-TROVATA    TO TRUE
            ws.getWkRicGrz().setNonTrovata();
            //-->    RICERCA DELL'OCCORRENZA ASSOCIATA AL CODICE LIVELLO DEL
            //-->    VALORIZZATORE VARIABILI SULLA DCLGEN GARANZIA
            // COB_CODE: PERFORM S12110-AREA-GARANZIA
            //              THRU EX-S12110
            //           VARYING IX-TAB-GRZ FROM 1 BY 1
            //             UNTIL IX-TAB-GRZ > VGRZ-ELE-GARANZIA-MAX
            //                OR WK-GRZ-TROVATA
            ws.getIxIndici().setTabGrz(((short)1));
            while (!(ws.getIxIndici().getTabGrz() > ws.getVgrzEleGaranziaMax() || ws.getWkRicGrz().isTrovata())) {
                s12110AreaGaranzia();
                ws.getIxIndici().setTabGrz(Trunc.toShort(ws.getIxIndici().getTabGrz() + 1, 4));
            }
        }
        // COB_CODE: PERFORM AREA-VAR-T-ISPC0140
        //              THRU AREA-VAR-T-ISPC0140-EX
        //               VARYING IX-TAB-VAR-T FROM 1 BY 1
        //                 UNTIL IX-TAB-VAR-T >
        //                  WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA-T)
        //                    OR IDSV0001-ESITO-KO.
        ws.getIxIndici().setTabVarT(((short)1));
        while (!(ws.getIxIndici().getTabVarT() > ws.getIvvc0216().getWskdTabValT().getEleVariabiliMaxT(ws.getIxIndici().getAreaSchedaT()) || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            areaVarTIspc0140();
            ws.getIxIndici().setTabVarT(Trunc.toShort(ws.getIxIndici().getTabVarT() + 1, 4));
        }
        // COB_CODE: IF WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA-T) >
        //                             WK-ISPC0140-NUM-COMPON-MAX-T
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getIvvc0216().getWskdTabValT().getEleVariabiliMaxT(ws.getIxIndici().getAreaSchedaT()) > ws.getWkIspcMax().getIspc0140NumComponMaxT()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S12100-AREA-SCHEDA'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S12100-AREA-SCHEDA");
            // COB_CODE: MOVE '005247'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005247");
            // COB_CODE: STRING 'NUMERO VARIABILI SUPERA LIMITE PREVISTO'
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr("NUMERO VARIABILI SUPERA LIMITE PREVISTO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S12110-AREA-GARANZIA<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA DELL'OCCORRENZA ASSOCIATA AL CODICE LIVELLO DEL
	 *     VALORIZZATORE VARIABILI SULLA DCLGEN GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void s12110AreaGaranzia() {
        // COB_CODE: IF  WSKD-COD-LIVELLO-T(IX-AREA-SCHEDA-T) =
        //               VGRZ-COD-TARI(IX-TAB-GRZ)
        //           AND NOT VGRZ-ST-DEL(IX-TAB-GRZ)
        //               END-IF
        //           END-IF.
        if (Conditions.eq(ws.getIvvc0216().getWskdTabValT().getCodLivelloT(ws.getIxIndici().getAreaSchedaT()), ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzCodTari()) && !ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getStatus().isDel()) {
            // COB_CODE: SET WK-GRZ-TROVATA    TO TRUE
            ws.getWkRicGrz().setTrovata();
            // COB_CODE: IF VGRZ-TP-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUES
            //                TO ISPC0140-TIPO-PREMIO-T(IX-AREA-SCHEDA-T)
            //           ELSE
            //                TO ISPC0140-TIPO-PREMIO-T(IX-AREA-SCHEDA-T)
            //           END-IF
            if (Conditions.eq(ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTpPre(), Types.HIGH_CHAR_VAL)) {
                // COB_CODE: MOVE SPACES
                //             TO ISPC0140-TIPO-PREMIO-T(IX-AREA-SCHEDA-T)
                ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).setTipoPremioT(Types.SPACE_CHAR);
            }
            else {
                // COB_CODE: MOVE VGRZ-TP-PRE(IX-TAB-GRZ)
                //             TO ISPC0140-TIPO-PREMIO-T(IX-AREA-SCHEDA-T)
                ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).setTipoPremioT(ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTpPre());
            }
            // COB_CODE: MOVE VGRZ-COD-TARI(IX-TAB-GRZ) TO WS-COD-TARIFFA
            ws.getWkVariabili().setWsCodTariffa(ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzCodTari());
            // COB_CODE: PERFORM CTRL-GAR-INCLUSA
            //              THRU CTRL-GAR-INCLUSA-EX
            ctrlGarInclusa();
            // COB_CODE: IF TROVATO
            //                TO ISPC0140-GARANZIA-INCLUSA-T(IX-AREA-SCHEDA-T)
            //           END-IF
            if (ws.getWsFlagRicerca().isTrovato()) {
                // COB_CODE: MOVE WPAG-FL-GAR-INCLUSA-I(IX-RIC-GRZ)
                //             TO ISPC0140-GARANZIA-INCLUSA-T(IX-AREA-SCHEDA-T)
                ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).setGaranziaInclusaT(wpagAreaPagina.getTabGaranzie(ws.getIxIndici().getRicGrz()).getFlGarInclusaI().getFlGarInclusaI());
            }
            // COB_CODE: SET WK-TGA-NON-TROVATA    TO TRUE
            ws.getWkRicTga().setNonTrovata();
            // COB_CODE: PERFORM S12111-AREA-TRANCHE
            //              THRU EX-S12111
            //           VARYING IX-TAB-TGA FROM 1 BY 1
            //             UNTIL IX-TAB-TGA > VTGA-ELE-TRAN-MAX
            //                OR WK-TGA-TROVATA
            ws.getIxIndici().setTabTga(((short)1));
            while (!(ws.getIxIndici().getTabTga() > ws.getVtgaEleTranMax() || ws.getWkRicTga().isTrovata())) {
                s12111AreaTranche();
                ws.getIxIndici().setTabTga(Trunc.toShort(ws.getIxIndici().getTabTga() + 1, 4));
            }
            // COB_CODE: IF VGRZ-PC-RIP-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUE
            //                TO ISPC0140-PERC-RIP-PREMIO-T(IX-AREA-SCHEDA-T)
            //           ELSE
            //                TO ISPC0140-PERC-RIP-PREMIO-T(IX-AREA-SCHEDA-T)
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPcRipPre().getVgrzPcRipPreNullFormatted())) {
                // COB_CODE: MOVE ZERO
                //             TO ISPC0140-PERC-RIP-PREMIO-T(IX-AREA-SCHEDA-T)
                ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).setPercRipPremioT(new AfDecimal(0, 6, 3));
            }
            else {
                // COB_CODE: MOVE VGRZ-PC-RIP-PRE(IX-TAB-GRZ)
                //             TO ISPC0140-PERC-RIP-PREMIO-T(IX-AREA-SCHEDA-T)
                ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).setPercRipPremioT(Trunc.toDecimal(ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPcRipPre().getWgrzPcRipPre(), 6, 3));
            }
        }
    }

    /**Original name: CTRL-GAR-INCLUSA<br>
	 * <pre>----------------------------------------------------------------*
	 *     Controllo delle garanzie da includere
	 * ----------------------------------------------------------------*</pre>*/
    private void ctrlGarInclusa() {
        // COB_CODE: SET NON-TROVATO TO TRUE
        ws.getWsFlagRicerca().setNonTrovato();
        // COB_CODE: MOVE ZERO TO IX-RIC-GRZ
        ws.getIxIndici().setRicGrz(((short)0));
        // COB_CODE: PERFORM UNTIL IX-RIC-GRZ >= WPAG-ELE-MAX-GARNZIE
        //                      OR TROVATO
        //              END-IF
        //           END-PERFORM.
        while (!(ws.getIxIndici().getRicGrz() >= wpagAreaPagina.getEleMaxGarnzie() || ws.getWsFlagRicerca().isTrovato())) {
            // COB_CODE: ADD 1 TO IX-RIC-GRZ
            ws.getIxIndici().setRicGrz(Trunc.toShort(1 + ws.getIxIndici().getRicGrz(), 4));
            // COB_CODE: IF WPAG-COD-GARANZIA-I(IX-RIC-GRZ) = WS-COD-TARIFFA
            //              SET TROVATO TO TRUE
            //           END-IF
            if (Conditions.eq(wpagAreaPagina.getTabGaranzie(ws.getIxIndici().getRicGrz()).getCodGaranziaI(), ws.getWkVariabili().getWsCodTariffa())) {
                // COB_CODE: SET TROVATO TO TRUE
                ws.getWsFlagRicerca().setTrovato();
            }
        }
    }

    /**Original name: S12111-AREA-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA DELLA TRANCHE ASSOCIATA ALLA GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void s12111AreaTranche() {
        // COB_CODE: IF  VGRZ-ID-GAR(IX-TAB-GRZ) = VTGA-ID-GAR(IX-TAB-TGA)
        //           AND NOT VTGA-ST-DEL(IX-TAB-TGA)
        //           AND NOT VTGA-TP-TRCH(IX-TAB-TGA) = '9'
        //                 TO ISPC0140-FLAG-MOD-CALC-PRE-T(IX-AREA-SCHEDA-T)
        //            END-IF.
        if (ws.getVgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar() == ws.getVtgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdGar() && !ws.getVtgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getStatus().isDel() && !Conditions.eq(ws.getVtgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaTpTrch(), "9")) {
            // COB_CODE: SET WK-TGA-TROVATA      TO TRUE
            ws.getWkRicTga().setTrovata();
            // COB_CODE: MOVE VTGA-MOD-CALC(IX-TAB-TGA)
            //             TO ISPC0140-FLAG-MOD-CALC-PRE-T(IX-AREA-SCHEDA-T)
            ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).getFlagModCalcPreT().setIspc0140FlagModCalcPrePFormatted(ws.getVtgaTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaModCalcFormatted());
        }
    }

    /**Original name: AREA-VAR-T-ISPC0140<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELL'AREA VARIABILI DI TARIFFA
	 *     DI INPUT DEL SERVIZIO CALCOLI E CONTROLLI
	 * ----------------------------------------------------------------*</pre>*/
    private void areaVarTIspc0140() {
        // COB_CODE: ADD 1
        //            TO ISPC0140-NUM-COMPON-MAX-ELE-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).setIspc0140NumComponMaxEleT(Trunc.toShort(1 + ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).getIspc0140NumComponMaxEleT(), 3));
        // COB_CODE: MOVE WSKD-COD-VARIABILE-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        //             TO ISPC0140-CODICE-VARIABILE-T
        //                                    (IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).getAreaVariabiliT(ws.getIxIndici().getTabVarT()).setCodiceVariabileP(ws.getIvvc0216().getWskdTabValT().getCodVariabileT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT()));
        // COB_CODE: MOVE WSKD-TP-DATO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        //             TO ISPC0140-TIPO-DATO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).getAreaVariabiliT(ws.getIxIndici().getTabVarT()).setTipoDatoP(ws.getIvvc0216().getWskdTabValT().getTpDatoT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT()));
        // COB_CODE: MOVE WSKD-VAL-GENERICO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        //             TO ISPC0140-VAL-GENERICO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T).
        ws.getAreaIoCalcContr().getDatiInput().getSchedaT(ws.getIxIndici().getAreaSchedaT()).getAreaVariabiliT(ws.getIxIndici().getTabVarT()).setValGenericoP(ws.getIvvc0216().getWskdTabValT().getValGenericoT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT()));
    }

    public void initWkVariabili() {
        ws.getWkVariabili().getWkAppoDt().setWkAppoDt("");
        ws.getWkVariabili().getNewDtValQuote().setNewDtValQuote("");
        ws.getWkVariabili().setWkAppoLunghezza(0);
        ws.getWkVariabili().setWkIdGar(0);
        ws.getWkVariabili().setWkDtDecorrenza(0);
        ws.getWkVariabili().setWkPremioNeg(new AfDecimal(0, 15, 3));
        ws.getWkVariabili().getWkDiffPror().setWkDiffPror(new AfDecimal(0, 4, 2));
        ws.getWkVariabili().getWkAppoIas().setWkAppoIas(new AfDecimal(0, 18, 7));
        ws.getWkVariabili().setWkParametro("");
        ws.getWkVariabili().setWkRateAntic(0);
        ws.getWkVariabili().setWkRateRecup(0);
        ws.getWkVariabili().setWkValVar(0);
        ws.getWkVariabili().setWkDataLimite(0);
        ws.getWkVariabili().setWkDataCompetenza(0);
        ws.getWkVariabili().setWkFrqMoviFormatted("00000");
        ws.getWkVariabili().getWkFrazMm().setFrazMmFormatted("00000");
        ws.getWkVariabili().getWkTpRatPerf().setWkTpRatPerf(Types.SPACE_CHAR);
        for (int idx0 = 1; idx0 <= WkVariabiliLves0269.WK_TAB_GAR_MAXOCCURS; idx0++) {
            ws.getWkVariabili().getWkTabGar(idx0).setCodGar("");
            ws.getWkVariabili().getWkTabGar(idx0).setTpIas("");
        }
        ws.getWkVariabili().setWsTotPremioAnnuo(new AfDecimal(0, 15, 3));
        ws.getWkVariabili().setWsCodTariffa("");
        ws.getWkVariabili().setWkFlIncAutogen(Types.SPACE_CHAR);
    }

    public void initWpagAreaIas() {
        wpagAreaIas.setEleMaxIas(((short)0));
        for (int idx0 = 1; idx0 <= WpagAreaIas.TAB_IAS_MAXOCCURS; idx0++) {
            wpagAreaIas.getTabIas(idx0).setIdOgg(0);
            wpagAreaIas.getTabIas(idx0).setTpOgg("");
            wpagAreaIas.getTabIas(idx0).setTpIasOld("");
            wpagAreaIas.getTabIas(idx0).setTpIasNew("");
            wpagAreaIas.getTabIas(idx0).getModIas().setModIas(Types.SPACE_CHAR);
        }
    }

    public void initWcntAreaDatiContest() {
        ws.getIvvc0212().setWcntEleVarContMax(((short)0));
        for (int idx0 = 1; idx0 <= WcntTabVar.TAB_VAR_CONT_MAXOCCURS; idx0++) {
            ws.getIvvc0212().getWcntTabVar().setWcntCodVarCont(idx0, "");
            ws.getIvvc0212().getWcntTabVar().setWcntTpDatoCont(idx0, Types.SPACE_CHAR);
            ws.getIvvc0212().getWcntTabVar().setWcntValImpCont(idx0, new AfDecimal(0, 18, 7));
            ws.getIvvc0212().getWcntTabVar().setWcntValPercCont(idx0, new AfDecimal(0, 14, 9));
            ws.getIvvc0212().getWcntTabVar().setWcntValStrCont(idx0, "");
            ws.getIvvc0212().getWcntTabVar().setWcntTpLivello(idx0, Types.SPACE_CHAR);
            ws.getIvvc0212().getWcntTabVar().setWcntCodLivello(idx0, "");
            ws.getIvvc0212().getWcntTabVar().setWcntIdLivelloFormatted(idx0, "000000000");
        }
    }

    public void initLccc1901() {
        ws.getLccc1901().setGarMaxFormatted("0000");
        for (int idx0 = 1; idx0 <= Lccc1901.COD_TARI_OCCURS_MAXOCCURS; idx0++) {
            ws.getLccc1901().getCodTariOccurs(idx0).setLccc1901CodTari("");
        }
        ws.getLccc1901().getFlEseguibile().setFlEseguibile(Types.SPACE_CHAR);
    }

    public void initAreaIoIvvs0211() {
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setPgm("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFlgArea().setFlgArea("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getModalitaEsecutiva().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211CodCompagniaAniaFormatted("00000");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211TipoMovimentoFormatted("00000");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211TipoMoviOrigFormatted("00000");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setCodMainBatch("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataEffetto(0);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataCompetenza(0);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataCompAggStor(0);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataUltVersProd(0);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataUltTitInc(0);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setTrattamentoStoricita("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFormatoDataDb().setFormatoDataDb("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getStepElab().setStepElab(Types.SPACE_CHAR);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setKeyAutOper1("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setKeyAutOper2("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setKeyAutOper3("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setKeyAutOper4("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setKeyAutOper5("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFlagGarOpzione().setFlagGarOpzione(Types.SPACE_CHAR);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setEleInfoMax(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0211TabInfo1.TAB_INFO_MAXOCCURS; idx0++) {
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(idx0, "");
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211NumEleTabAlias(idx0, 0);
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(idx0, 0);
            ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(idx0, 0);
        }
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setBufferDati("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setIdTabTemp(0);
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().setReturnCode("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().setDescrizErr("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().setCodServizioBe("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().setNomeTabella("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().setKeyTabella("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setEleMaxNotFound(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0211RestoDati.TAB_VAR_NOT_FOUND_MAXOCCURS; idx0++) {
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getTabVarNotFound(idx0).setFound("");
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getTabVarNotFound(idx0).setFoundSp(Types.SPACE_CHAR);
        }
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setEleMaxCalcKo(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0211RestoDati.TAB_CALCOLO_KO_MAXOCCURS; idx0++) {
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getTabCalcoloKo(idx0).setCalcoloKo("");
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getTabCalcoloKo(idx0).setNotFoundSp(Types.SPACE_CHAR);
        }
        for (int idx0 = 1; idx0 <= Ivvc0211RestoDati.ADDRESSES_MAXOCCURS; idx0++) {
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getAddresses(idx0).setType(Types.SPACE_CHAR);
        }
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setCodiceIniziativa("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setCodiceTrattato("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getFlAreaVarExtra().setFlAreaVarExtra(Types.SPACE_CHAR);
    }

    public void initAreaInput() {
        ws.getLccc0490().setIdPoliFormatted("000000000");
        ws.getLccc0490().setDtDecorPoliFormatted("00000000");
        for (int idx0 = 1; idx0 <= Lccc0490.TAB_GRZ_MAXOCCURS; idx0++) {
            ws.getLccc0490().getTabGrz(idx0).setCodTari("");
            ws.getLccc0490().getTabGrz(idx0).setLccc0490TpGarFormatted("0");
            ws.getLccc0490().getTabGrz(idx0).setLccc0490FrazionamentoFormatted("00000");
            ws.getLccc0490().getTabGrz(idx0).setTpPerPre(Types.SPACE_CHAR);
            ws.getLccc0490().getTabGrz(idx0).setLccc0490DtDecorFormatted("00000000");
            ws.getLccc0490().getTabGrz(idx0).setTipoRicorrenza(Types.SPACE_CHAR);
        }
    }

    public void initWallAreaAsset() {
        wallAreaAsset.setEleAssetAllMax(((short)0));
        for (int idx0 = 1; idx0 <= WallTabella.TAB_ASSET_ALL_MAXOCCURS; idx0++) {
            wallAreaAsset.getTabella().setStatus(idx0, Types.SPACE_CHAR);
            wallAreaAsset.getTabella().setIdPtf(idx0, 0);
            wallAreaAsset.getTabella().setIdAstAlloc(idx0, 0);
            wallAreaAsset.getTabella().setIdStraDiInvst(idx0, 0);
            wallAreaAsset.getTabella().setIdMoviCrz(idx0, 0);
            wallAreaAsset.getTabella().setIdMoviChiu(idx0, 0);
            wallAreaAsset.getTabella().setDtIniVldt(idx0, 0);
            wallAreaAsset.getTabella().setDtEndVldt(idx0, 0);
            wallAreaAsset.getTabella().setDtIniEff(idx0, 0);
            wallAreaAsset.getTabella().setDtEndEff(idx0, 0);
            wallAreaAsset.getTabella().setCodCompAnia(idx0, 0);
            wallAreaAsset.getTabella().setCodFnd(idx0, "");
            wallAreaAsset.getTabella().setCodTari(idx0, "");
            wallAreaAsset.getTabella().setTpApplzAst(idx0, "");
            wallAreaAsset.getTabella().setPcRipAst(idx0, new AfDecimal(0, 6, 3));
            wallAreaAsset.getTabella().setTpFnd(idx0, Types.SPACE_CHAR);
            wallAreaAsset.getTabella().setDsRiga(idx0, 0);
            wallAreaAsset.getTabella().setDsOperSql(idx0, Types.SPACE_CHAR);
            wallAreaAsset.getTabella().setDsVer(idx0, 0);
            wallAreaAsset.getTabella().setDsTsIniCptz(idx0, 0);
            wallAreaAsset.getTabella().setDsTsEndCptz(idx0, 0);
            wallAreaAsset.getTabella().setDsUtente(idx0, "");
            wallAreaAsset.getTabella().setDsStatoElab(idx0, Types.SPACE_CHAR);
            wallAreaAsset.getTabella().setPeriodo(idx0, ((short)0));
            wallAreaAsset.getTabella().setTpRibilFnd(idx0, "");
        }
    }

    public void initIdsv0301() {
        ws.getIdsv0301().setCodCompAnia(0);
        ws.getIdsv0301().setIdTemporaryData(0);
        ws.getIdsv0301().setAliasStrDato("");
        ws.getIdsv0301().setNumFrame(0);
        ws.getIdsv0301().setIdSession("");
        ws.getIdsv0301().getFlContiguous().setFlContiguous(Types.SPACE_CHAR);
        ws.getIdsv0301().setTotalRecurrence(0);
        ws.getIdsv0301().setPartialRecurrence(0);
        ws.getIdsv0301().setActualRecurrence(0);
        ws.getIdsv0301().setTypeRecord("");
        ws.getIdsv0301().getOperazione().setOperazione("");
        ws.getIdsv0301().getActionType().setActionType(Types.SPACE_CHAR);
        ws.getIdsv0301().getTemporaryTable().setTemporaryTable(Types.SPACE_CHAR);
        ws.getIdsv0301().setBufferDataLenFormatted("0000000");
        ws.getIdsv0301().getEsito().setEsito("");
        ws.getIdsv0301().setCodServizioBe("");
        ws.getIdsv0301().setDescErroreEstesa("");
        ws.getIdsv0301().getSqlcodeSigned().setSqlcodeSigned(0);
        ws.getIdsv0301().setSqlcodeFormatted("000000000");
    }

    public void initIdsv0303() {
        ws.getIdsv0303().setIdsv0303EleMaxAppo(0);
        ws.getIdsv0303().setIdsv0303EleMaxTot(0);
    }

    public void initVtgaAreaTranche() {
        ws.setVtgaEleTranMax(((short)0));
        for (int idx0 = 1; idx0 <= Lves0269Data.VTGA_TAB_TRAN_MAXOCCURS; idx0++) {
            ws.getVtgaTabTran(idx0).getLccvtga1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getVtgaTabTran(idx0).getLccvtga1().setIdPtf(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdTrchDiGar(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdGar(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdAdes(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdPoli(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdMoviCrz(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtIniEff(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtEndEff(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodCompAnia(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtDecor(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIbOgg("");
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRgmFisc("");
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpTrch("");
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(new AfDecimal(0, 6, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(new AfDecimal(0, 14, 9));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(new AfDecimal(0, 14, 9));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlCarCont(Types.SPACE_CHAR);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodDvs("");
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(new AfDecimal(0, 6, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(((short)0));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(((short)0));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(((short)0));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(((short)0));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(((short)0));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(((short)0));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(new AfDecimal(0, 14, 9));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(new AfDecimal(0, 6, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(new AfDecimal(0, 14, 9));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(new AfDecimal(0, 14, 9));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(new AfDecimal(0, 14, 9));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpAdegAbb(Types.SPACE_CHAR);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaModCalc("");
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(new AfDecimal(0, 6, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlImportiForz(Types.SPACE_CHAR);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(new AfDecimal(0, 6, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(new AfDecimal(0, 6, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(new AfDecimal(0, 6, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlProvForz(Types.SPACE_CHAR);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(new AfDecimal(0, 14, 9));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRival("");
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpManfeeAppl("");
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsRiga(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsOperSql(Types.SPACE_CHAR);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsVer(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsIniCptz(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsEndCptz(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsUtente("");
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsStatoElab(Types.SPACE_CHAR);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(new AfDecimal(0, 6, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(0);
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(new AfDecimal(0, 6, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(new AfDecimal(0, 6, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(new AfDecimal(0, 15, 3));
            ws.getVtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
        }
    }

    public void initVgrzAreaGaranzia() {
        ws.setVgrzEleGaranziaMax(((short)0));
        for (int idx0 = 1; idx0 <= Lves0269Data.VGRZ_TAB_GAR_MAXOCCURS; idx0++) {
            ws.getVgrzTabGar(idx0).getLccvgrz1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getVgrzTabGar(idx0).getLccvgrz1().setIdPtf(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdGar(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdPoli(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdMoviCrz(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtIniEff(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtEndEff(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodCompAnia(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIbOgg("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodSez("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTari("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRamoBila("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(((short)0));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRsh("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(((short)0));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzModPagGarcol("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPerPre("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(((short)0));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(((short)0));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(((short)0));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(((short)0));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(((short)0));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(((short)0));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpEmisPur(Types.SPACE_CHAR);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPre(Types.SPACE_CHAR);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDur("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(((short)0));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(new AfDecimal(0, 6, 3));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPrstzAssta("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(new AfDecimal(0, 6, 3));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodFnd("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzAaRenCer("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPcRip("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(new AfDecimal(0, 6, 3));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpIas("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpStab("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpAdegPre(Types.SPACE_CHAR);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTratRiass("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDtEmisRiass("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCessRiass("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsRiga(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsOperSql(Types.SPACE_CHAR);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsVer(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsIniCptz(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsEndCptz(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsUtente("");
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsStatoElab(Types.SPACE_CHAR);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(new AfDecimal(0, 14, 9));
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(0);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRshInvst(Types.SPACE_CHAR);
            ws.getVgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRamoBila("");
        }
    }

    public void initVspgAreaSoprGar() {
        ws.setVspgEleSoprGarMax(((short)0));
        for (int idx0 = 1; idx0 <= Lves0269Data.VSPG_TAB_SPG_MAXOCCURS; idx0++) {
            ws.getVspgTabSpg(idx0).getLccvspg1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getVspgTabSpg(idx0).getLccvspg1().setIdPtf(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgIdSoprDiGar(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().getWspgIdGar().setWspgIdGar(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgIdMoviCrz(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().getWspgIdMoviChiu().setWspgIdMoviChiu(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgDtIniEff(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgDtEndEff(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgCodCompAnia(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgCodSopr("");
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgTpD("");
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().getWspgValPc().setWspgValPc(new AfDecimal(0, 14, 9));
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().getWspgValImp().setWspgValImp(new AfDecimal(0, 15, 3));
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().getWspgPcSopram().setWspgPcSopram(new AfDecimal(0, 14, 9));
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgFlEsclSopr(Types.SPACE_CHAR);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgDescEscl("");
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsRiga(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsOperSql(Types.SPACE_CHAR);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsVer(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsTsIniCptz(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsTsEndCptz(0);
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsUtente("");
            ws.getVspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsStatoElab(Types.SPACE_CHAR);
        }
    }

    public void initLdbi0731() {
        ws.getLdbi0731().setIdPoli(0);
        ws.getLdbi0731().setIdAdes(0);
        ws.getLdbi0731().setIdTrch(0);
        ws.getLdbi0731().setTpCall("");
        ws.getLdbi0731().setTpStatBus("");
        ws.getLdbi0731().setTrasformata(Types.SPACE_CHAR);
    }

    public void initLdbo07311() {
        ws.getLdbo07312().setLdbo0731IdPadre(0);
        ws.getLdbo07312().setLdbo0731Stb("");
        ws.getLdbo07312().setLdbo0731Tga("");
    }

    public void initAreaLccc0062() {
        ws.getAreaLccc0062().setDtCompetenza(0);
        ws.getAreaLccc0062().setDtDecorrenza(0);
        ws.getAreaLccc0062().setDtUltQtzo(0);
        ws.getAreaLccc0062().setDtUltgzTrch(0);
        ws.getAreaLccc0062().setFrazionamentoFormatted("00000");
        ws.getAreaLccc0062().setTotNumRate(0);
        for (int idx0 = 1; idx0 <= WcomAreaPagina.TAB_RATE_MAXOCCURS; idx0++) {
            ws.getAreaLccc0062().getTabRate(idx0).setDtInf(0);
            ws.getAreaLccc0062().getTabRate(idx0).setDtSup(0);
        }
    }
}
