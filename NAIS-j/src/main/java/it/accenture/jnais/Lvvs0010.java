package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.Lvvc0000;
import it.accenture.jnais.copy.Lvvc0000DatiInput2;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0010Data;

/**Original name: LVVS0010<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0010
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... DIFFERENZA TRA DATE
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0010 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0010Data ws = new Lvvs0010Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0010
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0010_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0010 getInstance() {
        return ((Lvvs0010)Programs.getInstance(Lvvs0010.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE AREA-IO-ADES
        //                      WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        initAreaIoAdes();
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
        //--> DCLGEN DI WORKING
        // COB_CODE: PERFORM S1200-CONTROLLO-DATI
        //              THRU S1200-CONTROLLO-DATI-EX.
        s1200ControlloDati();
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //           *--> CALL MODULO PER RECUPERO DATA
        //                    END-IF
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            //--> CALL MODULO PER RECUPERO DATA
            // COB_CODE: PERFORM S1300-CALL-LCCS0010
            //              THRU S1300-CALL-LCCS0010-EX
            s1300CallLccs0010();
            // COB_CODE:          IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                    ELSE
            //           *-->       GESTIRE ERRORE DISPATCHER
            //                      END-STRING
            //                    END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:           EVALUATE TRUE
                //                         WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                //           *                   MOVE LVVC0000-DATA-OUTPUT
                //           *                     TO IVVC0213-VAL-IMP-O
                //                              CONTINUE
                //                         WHEN OTHER
                //           *--->         ERRORE DI ACCESSO AL DB
                //                              END-STRING
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                   MOVE LVVC0000-DATA-OUTPUT
                    //                     TO IVVC0213-VAL-IMP-O
                    // COB_CODE: CONTINUE
                    //continue
                        break;

                    default://--->         ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: STRING IDSV0003-RETURN-CODE  ';'
                        //                  IDSV0003-SQLCODE
                        //           DELIMITED BY SIZE
                        //           INTO IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
            else {
                //-->       GESTIRE ERRORE DISPATCHER
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING IDSV0003-RETURN-CODE  ';'
                //                  IDSV0003-SQLCODE
                //           DELIMITED BY SIZE
                //           INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-ADES
        //                TO DADE-AREA-ADES
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasAdes())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DADE-AREA-ADES
            ws.setDadeAreaAdesFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF DADE-DT-DECOR NOT NUMERIC
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           ELSE
        //              END-IF
        //           END-IF.
        if (!Functions.isNumber(ws.getLccvade1().getDati().getWadeDtDecor().getWadeDtDecor())) {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'DATA-DECORRENZA-POLIZZA NON NUMERICA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("DATA-DECORRENZA-POLIZZA NON NUMERICA");
        }
        else if (ws.getLccvade1().getDati().getWadeDtDecor().getWadeDtDecor() == 0) {
            // COB_CODE: IF DADE-DT-DECOR = 0
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'DATA-DECORRENZA-POLIZZA NON VALORIZZATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("DATA-DECORRENZA-POLIZZA NON VALORIZZATA");
        }
    }

    /**Original name: S1300-CALL-LCCS0010<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIAMATA LCCS0010
	 * ----------------------------------------------------------------*
	 * --> COPY LCCS0010</pre>*/
    private void s1300CallLccs0010() {
        Lccs0010 lccs0010 = null;
        GenericParam formato = null;
        GenericParam ggDiff = null;
        GenericParam codiceRitorno = null;
        // COB_CODE: INITIALIZE INPUT-LCCS0010.
        initInputLccs0010();
        // COB_CODE: MOVE 'A'                              TO FORMATO.
        ws.setFormatoFormatted("A");
        // COB_CODE: MOVE DADE-DT-DECOR                    TO WS-DATA-APPO
        ws.setWsDataAppo(TruncAbs.toInt(ws.getLccvade1().getDati().getWadeDtDecor().getWadeDtDecor(), 8));
        // COB_CODE: MOVE WS-DATA-APPO                     TO DATA-INFERIORE.
        ws.getDataInferiore().setDataInferioreFormatted(ws.getWsDataAppoFormatted());
        // COB_CODE: INITIALIZE                               WS-DATA-APPO
        ws.setWsDataAppoFormatted("00000000");
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO     TO WS-DATA-APPO
        ws.setWsDataAppo(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
        // COB_CODE: MOVE WS-DATA-APPO                     TO DATA-SUPERIORE.
        ws.getDataSuperiore().setDataSuperioreFormatted(ws.getWsDataAppoFormatted());
        // COB_CODE: CALL LCCS0010 USING FORMATO,
        //                               DATA-INFERIORE,
        //                               DATA-SUPERIORE,
        //                               GG-DIFF,
        //                               CODICE-RITORNO
        lccs0010 = Lccs0010.getInstance();
        formato = new GenericParam(MarshalByteExt.chToBuffer(ws.getFormato()));
        ggDiff = new GenericParam(MarshalByteExt.strToBuffer(ws.getGgDiffFormatted(), Lvvs0010Data.Len.GG_DIFF));
        codiceRitorno = new GenericParam(MarshalByteExt.chToBuffer(ws.getCodiceRitorno()));
        lccs0010.run(formato, ws.getDataInferiore(), ws.getDataSuperiore(), ggDiff, codiceRitorno);
        ws.setFormatoFromBuffer(formato.getByteData());
        ws.setGgDiffFromBuffer(ggDiff.getByteData());
        ws.setCodiceRitornoFromBuffer(codiceRitorno.getByteData());
        // COB_CODE: IF CODICE-RITORNO = '0'
        //              COMPUTE IVVC0213-VAL-IMP-O = GG-DIFF / 360
        //           ELSE
        //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           END-IF.
        if (ws.getCodiceRitorno() == '0') {
            // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O = GG-DIFF / 360
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(new AfDecimal(((((double)(ws.getGgDiff()))) / 360), 12, 7), 18, 7));
        }
        else {
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        //
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoAdes() {
        ws.setDadeEleAdesMax(((short)0));
        ws.getLccvade1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvade1().setIdPtf(0);
        ws.getLccvade1().getDati().setWadeIdAdes(0);
        ws.getLccvade1().getDati().setWadeIdPoli(0);
        ws.getLccvade1().getDati().setWadeIdMoviCrz(0);
        ws.getLccvade1().getDati().getWadeIdMoviChiu().setWadeIdMoviChiu(0);
        ws.getLccvade1().getDati().setWadeDtIniEff(0);
        ws.getLccvade1().getDati().setWadeDtEndEff(0);
        ws.getLccvade1().getDati().setWadeIbPrev("");
        ws.getLccvade1().getDati().setWadeIbOgg("");
        ws.getLccvade1().getDati().setWadeCodCompAnia(0);
        ws.getLccvade1().getDati().getWadeDtDecor().setWadeDtDecor(0);
        ws.getLccvade1().getDati().getWadeDtScad().setWadeDtScad(0);
        ws.getLccvade1().getDati().getWadeEtaAScad().setWadeEtaAScad(0);
        ws.getLccvade1().getDati().getWadeDurAa().setWadeDurAa(0);
        ws.getLccvade1().getDati().getWadeDurMm().setWadeDurMm(0);
        ws.getLccvade1().getDati().getWadeDurGg().setWadeDurGg(0);
        ws.getLccvade1().getDati().setWadeTpRgmFisc("");
        ws.getLccvade1().getDati().setWadeTpRiat("");
        ws.getLccvade1().getDati().setWadeTpModPagTit("");
        ws.getLccvade1().getDati().setWadeTpIas("");
        ws.getLccvade1().getDati().getWadeDtVarzTpIas().setWadeDtVarzTpIas(0);
        ws.getLccvade1().getDati().getWadePreNetInd().setWadePreNetInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePreLrdInd().setWadePreLrdInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeRatLrdInd().setWadeRatLrdInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePrstzIniInd().setWadePrstzIniInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().setWadeFlCoincAssto(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeIbDflt("");
        ws.getLccvade1().getDati().setWadeModCalc("");
        ws.getLccvade1().getDati().setWadeTpFntCnbtva("");
        ws.getLccvade1().getDati().getWadeImpAz().setWadeImpAz(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpAder().setWadeImpAder(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpTfr().setWadeImpTfr(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpVolo().setWadeImpVolo(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePcAz().setWadePcAz(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcAder().setWadePcAder(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcTfr().setWadePcTfr(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcVolo().setWadePcVolo(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadeDtNovaRgmFisc().setWadeDtNovaRgmFisc(0);
        ws.getLccvade1().getDati().setWadeFlAttiv(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeImpRecRitVis().setWadeImpRecRitVis(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpRecRitAcc().setWadeImpRecRitAcc(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().setWadeFlVarzStatTbgc(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeFlProvzaMigraz(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeImpbVisDaRec().setWadeImpbVisDaRec(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeDtDecorPrestBan().setWadeDtDecorPrestBan(0);
        ws.getLccvade1().getDati().getWadeDtEffVarzStatT().setWadeDtEffVarzStatT(0);
        ws.getLccvade1().getDati().setWadeDsRiga(0);
        ws.getLccvade1().getDati().setWadeDsOperSql(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeDsVer(0);
        ws.getLccvade1().getDati().setWadeDsTsIniCptz(0);
        ws.getLccvade1().getDati().setWadeDsTsEndCptz(0);
        ws.getLccvade1().getDati().setWadeDsUtente("");
        ws.getLccvade1().getDati().setWadeDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeCumCnbtCap().setWadeCumCnbtCap(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpGarCnbt().setWadeImpGarCnbt(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeDtUltConsCnbt().setWadeDtUltConsCnbt(0);
        ws.getLccvade1().getDati().setWadeIdenIscFnd("");
        ws.getLccvade1().getDati().getWadeNumRatPian().setWadeNumRatPian(new AfDecimal(0, 12, 5));
        ws.getLccvade1().getDati().getWadeDtPresc().setWadeDtPresc(0);
        ws.getLccvade1().getDati().setWadeConcsPrest(Types.SPACE_CHAR);
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }

    public void initInputLccs0010() {
        ws.getLvvc0000().getFormatDate().setFormatDate(Types.SPACE_CHAR);
        ws.getLvvc0000().setDataInputFormatted("00000000");
        ws.getLvvc0000().setDataInput1Formatted("00000000");
        ws.getLvvc0000().getDatiInput2().setLvvc0000AnniInput2Formatted("00000");
        ws.getLvvc0000().getDatiInput2().setLvvc0000MesiInput2Formatted("00000");
        ws.getLvvc0000().getDatiInput2().setLvvc0000GiorniInput2Formatted("00000");
        ws.getLvvc0000().setAnniInput3Formatted("00000");
        ws.getLvvc0000().setMesiInput3Formatted("00000");
        ws.getLvvc0000().setDataOutput(new AfDecimal(0, 11, 7));
    }
}
