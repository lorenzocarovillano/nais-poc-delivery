package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DettTitContDao;
import it.accenture.jnais.commons.data.to.IDettTitCont;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DettTitContIdbsdtc0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs7300Data;
import it.accenture.jnais.ws.redefines.DtcAcqExp;
import it.accenture.jnais.ws.redefines.DtcCarAcq;
import it.accenture.jnais.ws.redefines.DtcCarGest;
import it.accenture.jnais.ws.redefines.DtcCarIas;
import it.accenture.jnais.ws.redefines.DtcCarInc;
import it.accenture.jnais.ws.redefines.DtcCnbtAntirac;
import it.accenture.jnais.ws.redefines.DtcCommisInter;
import it.accenture.jnais.ws.redefines.DtcDir;
import it.accenture.jnais.ws.redefines.DtcDtEndCop;
import it.accenture.jnais.ws.redefines.DtcDtEsiTit;
import it.accenture.jnais.ws.redefines.DtcDtIniCop;
import it.accenture.jnais.ws.redefines.DtcFrqMovi;
import it.accenture.jnais.ws.redefines.DtcIdMoviChiu;
import it.accenture.jnais.ws.redefines.DtcImpAder;
import it.accenture.jnais.ws.redefines.DtcImpAz;
import it.accenture.jnais.ws.redefines.DtcImpTfr;
import it.accenture.jnais.ws.redefines.DtcImpTfrStrc;
import it.accenture.jnais.ws.redefines.DtcImpTrasfe;
import it.accenture.jnais.ws.redefines.DtcImpVolo;
import it.accenture.jnais.ws.redefines.DtcIntrFraz;
import it.accenture.jnais.ws.redefines.DtcIntrMora;
import it.accenture.jnais.ws.redefines.DtcIntrRetdt;
import it.accenture.jnais.ws.redefines.DtcIntrRiat;
import it.accenture.jnais.ws.redefines.DtcManfeeAntic;
import it.accenture.jnais.ws.redefines.DtcManfeeRec;
import it.accenture.jnais.ws.redefines.DtcManfeeRicor;
import it.accenture.jnais.ws.redefines.DtcNumGgRitardoPag;
import it.accenture.jnais.ws.redefines.DtcNumGgRival;
import it.accenture.jnais.ws.redefines.DtcPreNet;
import it.accenture.jnais.ws.redefines.DtcPrePpIas;
import it.accenture.jnais.ws.redefines.DtcPreSoloRsh;
import it.accenture.jnais.ws.redefines.DtcPreTot;
import it.accenture.jnais.ws.redefines.DtcProvAcq1aa;
import it.accenture.jnais.ws.redefines.DtcProvAcq2aa;
import it.accenture.jnais.ws.redefines.DtcProvDaRec;
import it.accenture.jnais.ws.redefines.DtcProvInc;
import it.accenture.jnais.ws.redefines.DtcProvRicor;
import it.accenture.jnais.ws.redefines.DtcRemunAss;
import it.accenture.jnais.ws.redefines.DtcSoprAlt;
import it.accenture.jnais.ws.redefines.DtcSoprProf;
import it.accenture.jnais.ws.redefines.DtcSoprSan;
import it.accenture.jnais.ws.redefines.DtcSoprSpo;
import it.accenture.jnais.ws.redefines.DtcSoprTec;
import it.accenture.jnais.ws.redefines.DtcSpeAge;
import it.accenture.jnais.ws.redefines.DtcSpeMed;
import it.accenture.jnais.ws.redefines.DtcTax;
import it.accenture.jnais.ws.redefines.DtcTotIntrPrest;

/**Original name: LDBS7300<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  07 DIC 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs7300 extends Program implements IDettTitCont {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DettTitContDao dettTitContDao = new DettTitContDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs7300Data ws = new Ldbs7300Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: DETT-TIT-CONT
    private DettTitContIdbsdtc0 dettTitCont;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS7300_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, DettTitContIdbsdtc0 dettTitCont) {
        this.idsv0003 = idsv0003;
        this.dettTitCont = dettTitCont;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs7300 getInstance() {
        return ((Ldbs7300)Programs.getInstance(Ldbs7300.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS7300'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS7300");
        // COB_CODE: MOVE 'DETT-TIT-CONT' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("DETT-TIT-CONT");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT MAX (DT_ESI_TIT)
        //              INTO :DTC-DT-ESI-TIT-DB
        //                   :IND-DTC-DT-ESI-TIT
        //            FROM DETT_TIT_CONT
        //            WHERE
        //            TP_OGG = :DTC-TP-OGG
        //            AND ID_OGG =  :DTC-ID-OGG
        //            AND TP_STAT_TIT = :DTC-TP-STAT-TIT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dettTitContDao.selectRec14(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT MAX (DT_ESI_TIT)
        //              INTO :DTC-DT-ESI-TIT-DB
        //                   :IND-DTC-DT-ESI-TIT
        //            FROM DETT_TIT_CONT
        //            WHERE
        //            TP_OGG = :DTC-TP-OGG
        //            AND ID_OGG =  :DTC-ID-OGG
        //            AND TP_STAT_TIT = :DTC-TP-STAT-TIT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dettTitContDao.selectRec15(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-DTC-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO DTC-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-ID-MOVI-CHIU-NULL
            dettTitCont.getDtcIdMoviChiu().setDtcIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIdMoviChiu.Len.DTC_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-DTC-DT-INI-COP = -1
        //              MOVE HIGH-VALUES TO DTC-DT-INI-COP-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DT-INI-COP-NULL
            dettTitCont.getDtcDtIniCop().setDtcDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDtIniCop.Len.DTC_DT_INI_COP_NULL));
        }
        // COB_CODE: IF IND-DTC-DT-END-COP = -1
        //              MOVE HIGH-VALUES TO DTC-DT-END-COP-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DT-END-COP-NULL
            dettTitCont.getDtcDtEndCop().setDtcDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDtEndCop.Len.DTC_DT_END_COP_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-NET = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-NET-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-NET-NULL
            dettTitCont.getDtcPreNet().setDtcPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPreNet.Len.DTC_PRE_NET_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-FRAZ = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-FRAZ-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-FRAZ-NULL
            dettTitCont.getDtcIntrFraz().setDtcIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrFraz.Len.DTC_INTR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-MORA-NULL
            dettTitCont.getDtcIntrMora().setDtcIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrMora.Len.DTC_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-RETDT = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-RETDT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-RETDT-NULL
            dettTitCont.getDtcIntrRetdt().setDtcIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrRetdt.Len.DTC_INTR_RETDT_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-RIAT-NULL
            dettTitCont.getDtcIntrRiat().setDtcIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrRiat.Len.DTC_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-DTC-DIR = -1
        //              MOVE HIGH-VALUES TO DTC-DIR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DIR-NULL
            dettTitCont.getDtcDir().setDtcDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDir.Len.DTC_DIR_NULL));
        }
        // COB_CODE: IF IND-DTC-SPE-MED = -1
        //              MOVE HIGH-VALUES TO DTC-SPE-MED-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SPE-MED-NULL
            dettTitCont.getDtcSpeMed().setDtcSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSpeMed.Len.DTC_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-DTC-TAX = -1
        //              MOVE HIGH-VALUES TO DTC-TAX-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-TAX-NULL
            dettTitCont.getDtcTax().setDtcTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcTax.Len.DTC_TAX_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-SAN-NULL
            dettTitCont.getDtcSoprSan().setDtcSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprSan.Len.DTC_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-SPO-NULL
            dettTitCont.getDtcSoprSpo().setDtcSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprSpo.Len.DTC_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-TEC-NULL
            dettTitCont.getDtcSoprTec().setDtcSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprTec.Len.DTC_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-PROF-NULL
            dettTitCont.getDtcSoprProf().setDtcSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprProf.Len.DTC_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-ALT = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-ALT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-ALT-NULL
            dettTitCont.getDtcSoprAlt().setDtcSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprAlt.Len.DTC_SOPR_ALT_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-TOT = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-TOT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-TOT-NULL
            dettTitCont.getDtcPreTot().setDtcPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPreTot.Len.DTC_PRE_TOT_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-PP-IAS = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-PP-IAS-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-PP-IAS-NULL
            dettTitCont.getDtcPrePpIas().setDtcPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPrePpIas.Len.DTC_PRE_PP_IAS_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-SOLO-RSH = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-SOLO-RSH-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-SOLO-RSH-NULL
            dettTitCont.getDtcPreSoloRsh().setDtcPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPreSoloRsh.Len.DTC_PRE_SOLO_RSH_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-ACQ-NULL
            dettTitCont.getDtcCarAcq().setDtcCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarAcq.Len.DTC_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-GEST-NULL
            dettTitCont.getDtcCarGest().setDtcCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarGest.Len.DTC_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-INC = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-INC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-INC-NULL
            dettTitCont.getDtcCarInc().setDtcCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarInc.Len.DTC_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-ACQ-1AA = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-ACQ-1AA-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-ACQ-1AA-NULL
            dettTitCont.getDtcProvAcq1aa().setDtcProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvAcq1aa.Len.DTC_PROV_ACQ1AA_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-ACQ-2AA = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-ACQ-2AA-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-ACQ-2AA-NULL
            dettTitCont.getDtcProvAcq2aa().setDtcProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvAcq2aa.Len.DTC_PROV_ACQ2AA_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-RICOR-NULL
            dettTitCont.getDtcProvRicor().setDtcProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvRicor.Len.DTC_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-INC = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-INC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-INC-NULL
            dettTitCont.getDtcProvInc().setDtcProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvInc.Len.DTC_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-DA-REC = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-DA-REC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-DA-REC-NULL
            dettTitCont.getDtcProvDaRec().setDtcProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvDaRec.Len.DTC_PROV_DA_REC_NULL));
        }
        // COB_CODE: IF IND-DTC-COD-DVS = -1
        //              MOVE HIGH-VALUES TO DTC-COD-DVS-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-COD-DVS-NULL
            dettTitCont.setDtcCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitContIdbsdtc0.Len.DTC_COD_DVS));
        }
        // COB_CODE: IF IND-DTC-FRQ-MOVI = -1
        //              MOVE HIGH-VALUES TO DTC-FRQ-MOVI-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getFrqMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-FRQ-MOVI-NULL
            dettTitCont.getDtcFrqMovi().setDtcFrqMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcFrqMovi.Len.DTC_FRQ_MOVI_NULL));
        }
        // COB_CODE: IF IND-DTC-COD-TARI = -1
        //              MOVE HIGH-VALUES TO DTC-COD-TARI-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-COD-TARI-NULL
            dettTitCont.setDtcCodTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitContIdbsdtc0.Len.DTC_COD_TARI));
        }
        // COB_CODE: IF IND-DTC-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-AZ-NULL
            dettTitCont.getDtcImpAz().setDtcImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpAz.Len.DTC_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-ADER-NULL
            dettTitCont.getDtcImpAder().setDtcImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpAder.Len.DTC_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-TFR-NULL
            dettTitCont.getDtcImpTfr().setDtcImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpTfr.Len.DTC_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-VOLO-NULL
            dettTitCont.getDtcImpVolo().setDtcImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpVolo.Len.DTC_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-DTC-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO DTC-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-MANFEE-ANTIC-NULL
            dettTitCont.getDtcManfeeAntic().setDtcManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcManfeeAntic.Len.DTC_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DTC-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO DTC-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-MANFEE-RICOR-NULL
            dettTitCont.getDtcManfeeRicor().setDtcManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcManfeeRicor.Len.DTC_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-DTC-MANFEE-REC = -1
        //              MOVE HIGH-VALUES TO DTC-MANFEE-REC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-MANFEE-REC-NULL
            dettTitCont.getDtcManfeeRec().setDtcManfeeRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcManfeeRec.Len.DTC_MANFEE_REC_NULL));
        }
        // COB_CODE: IF IND-DTC-DT-ESI-TIT = -1
        //              MOVE HIGH-VALUES TO DTC-DT-ESI-TIT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DT-ESI-TIT-NULL
            dettTitCont.getDtcDtEsiTit().setDtcDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDtEsiTit.Len.DTC_DT_ESI_TIT_NULL));
        }
        // COB_CODE: IF IND-DTC-SPE-AGE = -1
        //              MOVE HIGH-VALUES TO DTC-SPE-AGE-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SPE-AGE-NULL
            dettTitCont.getDtcSpeAge().setDtcSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSpeAge.Len.DTC_SPE_AGE_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-IAS = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-IAS-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-IAS-NULL
            dettTitCont.getDtcCarIas().setDtcCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarIas.Len.DTC_CAR_IAS_NULL));
        }
        // COB_CODE: IF IND-DTC-TOT-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO DTC-TOT-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-TOT-INTR-PREST-NULL
            dettTitCont.getDtcTotIntrPrest().setDtcTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcTotIntrPrest.Len.DTC_TOT_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-TRASFE-NULL
            dettTitCont.getDtcImpTrasfe().setDtcImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpTrasfe.Len.DTC_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-TFR-STRC-NULL
            dettTitCont.getDtcImpTfrStrc().setDtcImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpTfrStrc.Len.DTC_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-DTC-NUM-GG-RITARDO-PAG = -1
        //              MOVE HIGH-VALUES TO DTC-NUM-GG-RITARDO-PAG-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getNumGgRitardoPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-NUM-GG-RITARDO-PAG-NULL
            dettTitCont.getDtcNumGgRitardoPag().setDtcNumGgRitardoPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcNumGgRitardoPag.Len.DTC_NUM_GG_RITARDO_PAG_NULL));
        }
        // COB_CODE: IF IND-DTC-NUM-GG-RIVAL = -1
        //              MOVE HIGH-VALUES TO DTC-NUM-GG-RIVAL-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getNumGgRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-NUM-GG-RIVAL-NULL
            dettTitCont.getDtcNumGgRival().setDtcNumGgRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcNumGgRival.Len.DTC_NUM_GG_RIVAL_NULL));
        }
        // COB_CODE: IF IND-DTC-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO DTC-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-ACQ-EXP-NULL
            dettTitCont.getDtcAcqExp().setDtcAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcAcqExp.Len.DTC_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-DTC-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO DTC-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-REMUN-ASS-NULL
            dettTitCont.getDtcRemunAss().setDtcRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcRemunAss.Len.DTC_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-DTC-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO DTC-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-COMMIS-INTER-NULL
            dettTitCont.getDtcCommisInter().setDtcCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCommisInter.Len.DTC_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-DTC-CNBT-ANTIRAC = -1
        //              MOVE HIGH-VALUES TO DTC-CNBT-ANTIRAC-NULL
        //           END-IF.
        if (ws.getIndDettTitCont().getCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CNBT-ANTIRAC-NULL
            dettTitCont.getDtcCnbtAntirac().setDtcCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCnbtAntirac.Len.DTC_CNBT_ANTIRAC_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE DTC-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettTitContDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DTC-DT-INI-EFF
        dettTitCont.setDtcDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE DTC-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettTitContDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DTC-DT-END-EFF
        dettTitCont.setDtcDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-DTC-DT-INI-COP = 0
        //               MOVE WS-DATE-N      TO DTC-DT-INI-COP
        //           END-IF
        if (ws.getIndDettTitCont().getDtIniCop() == 0) {
            // COB_CODE: MOVE DTC-DT-INI-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettTitContDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DTC-DT-INI-COP
            dettTitCont.getDtcDtIniCop().setDtcDtIniCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-DTC-DT-END-COP = 0
        //               MOVE WS-DATE-N      TO DTC-DT-END-COP
        //           END-IF
        if (ws.getIndDettTitCont().getDtEndCop() == 0) {
            // COB_CODE: MOVE DTC-DT-END-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettTitContDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DTC-DT-END-COP
            dettTitCont.getDtcDtEndCop().setDtcDtEndCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-DTC-DT-ESI-TIT = 0
        //               MOVE WS-DATE-N      TO DTC-DT-ESI-TIT
        //           END-IF.
        if (ws.getIndDettTitCont().getDtEsiTit() == 0) {
            // COB_CODE: MOVE DTC-DT-ESI-TIT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettTitContDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DTC-DT-ESI-TIT
            dettTitCont.getDtcDtEsiTit().setDtcDtEsiTit(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
        // COB_CODE: IF IND-DTC-DT-ESI-TIT = -1
        //              MOVE HIGH-VALUES TO DTC-DT-ESI-TIT-NULL
        //           ELSE
        //              MOVE WS-DATE-N      TO DTC-DT-ESI-TIT
        //           END-IF.
        if (ws.getIndDettTitCont().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DT-ESI-TIT-NULL
            dettTitCont.getDtcDtEsiTit().setDtcDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDtEsiTit.Len.DTC_DT_ESI_TIT_NULL));
        }
        else {
            // COB_CODE: MOVE DTC-DT-ESI-TIT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettTitContDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DTC-DT-ESI-TIT
            dettTitCont.getDtcDtEsiTit().setDtcDtEsiTit(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getAcqExp() {
        throw new FieldNotMappedException("acqExp");
    }

    @Override
    public void setAcqExp(AfDecimal acqExp) {
        throw new FieldNotMappedException("acqExp");
    }

    @Override
    public AfDecimal getAcqExpObj() {
        return getAcqExp();
    }

    @Override
    public void setAcqExpObj(AfDecimal acqExpObj) {
        setAcqExp(new AfDecimal(acqExpObj, 15, 3));
    }

    @Override
    public AfDecimal getCarAcq() {
        throw new FieldNotMappedException("carAcq");
    }

    @Override
    public void setCarAcq(AfDecimal carAcq) {
        throw new FieldNotMappedException("carAcq");
    }

    @Override
    public AfDecimal getCarAcqObj() {
        return getCarAcq();
    }

    @Override
    public void setCarAcqObj(AfDecimal carAcqObj) {
        setCarAcq(new AfDecimal(carAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getCarGest() {
        throw new FieldNotMappedException("carGest");
    }

    @Override
    public void setCarGest(AfDecimal carGest) {
        throw new FieldNotMappedException("carGest");
    }

    @Override
    public AfDecimal getCarGestObj() {
        return getCarGest();
    }

    @Override
    public void setCarGestObj(AfDecimal carGestObj) {
        setCarGest(new AfDecimal(carGestObj, 15, 3));
    }

    @Override
    public AfDecimal getCarIas() {
        throw new FieldNotMappedException("carIas");
    }

    @Override
    public void setCarIas(AfDecimal carIas) {
        throw new FieldNotMappedException("carIas");
    }

    @Override
    public AfDecimal getCarIasObj() {
        return getCarIas();
    }

    @Override
    public void setCarIasObj(AfDecimal carIasObj) {
        setCarIas(new AfDecimal(carIasObj, 15, 3));
    }

    @Override
    public AfDecimal getCarInc() {
        throw new FieldNotMappedException("carInc");
    }

    @Override
    public void setCarInc(AfDecimal carInc) {
        throw new FieldNotMappedException("carInc");
    }

    @Override
    public AfDecimal getCarIncObj() {
        return getCarInc();
    }

    @Override
    public void setCarIncObj(AfDecimal carIncObj) {
        setCarInc(new AfDecimal(carIncObj, 15, 3));
    }

    @Override
    public AfDecimal getCnbtAntirac() {
        throw new FieldNotMappedException("cnbtAntirac");
    }

    @Override
    public void setCnbtAntirac(AfDecimal cnbtAntirac) {
        throw new FieldNotMappedException("cnbtAntirac");
    }

    @Override
    public AfDecimal getCnbtAntiracObj() {
        return getCnbtAntirac();
    }

    @Override
    public void setCnbtAntiracObj(AfDecimal cnbtAntiracObj) {
        setCnbtAntirac(new AfDecimal(cnbtAntiracObj, 15, 3));
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public String getCodDvs() {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public void setCodDvs(String codDvs) {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public String getCodDvsObj() {
        return getCodDvs();
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        setCodDvs(codDvsObj);
    }

    @Override
    public String getCodTari() {
        throw new FieldNotMappedException("codTari");
    }

    @Override
    public void setCodTari(String codTari) {
        throw new FieldNotMappedException("codTari");
    }

    @Override
    public String getCodTariObj() {
        return getCodTari();
    }

    @Override
    public void setCodTariObj(String codTariObj) {
        setCodTari(codTariObj);
    }

    @Override
    public AfDecimal getCommisInter() {
        throw new FieldNotMappedException("commisInter");
    }

    @Override
    public void setCommisInter(AfDecimal commisInter) {
        throw new FieldNotMappedException("commisInter");
    }

    @Override
    public AfDecimal getCommisInterObj() {
        return getCommisInter();
    }

    @Override
    public void setCommisInterObj(AfDecimal commisInterObj) {
        setCommisInter(new AfDecimal(commisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getDir() {
        throw new FieldNotMappedException("dir");
    }

    @Override
    public void setDir(AfDecimal dir) {
        throw new FieldNotMappedException("dir");
    }

    @Override
    public AfDecimal getDirObj() {
        return getDir();
    }

    @Override
    public void setDirObj(AfDecimal dirObj) {
        setDir(new AfDecimal(dirObj, 15, 3));
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsEndCptz() {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public long getDsTsIniCptz() {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtEndCopDb() {
        throw new FieldNotMappedException("dtEndCopDb");
    }

    @Override
    public void setDtEndCopDb(String dtEndCopDb) {
        throw new FieldNotMappedException("dtEndCopDb");
    }

    @Override
    public String getDtEndCopDbObj() {
        return getDtEndCopDb();
    }

    @Override
    public void setDtEndCopDbObj(String dtEndCopDbObj) {
        setDtEndCopDb(dtEndCopDbObj);
    }

    @Override
    public String getDtEndEffDb() {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public String getDtEsiTitDb() {
        return ws.getDettTitContDb().getEsiTitDb();
    }

    @Override
    public void setDtEsiTitDb(String dtEsiTitDb) {
        this.ws.getDettTitContDb().setEsiTitDb(dtEsiTitDb);
    }

    @Override
    public String getDtEsiTitDbObj() {
        if (ws.getIndDettTitCont().getDtEsiTit() >= 0) {
            return getDtEsiTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEsiTitDbObj(String dtEsiTitDbObj) {
        if (dtEsiTitDbObj != null) {
            setDtEsiTitDb(dtEsiTitDbObj);
            ws.getIndDettTitCont().setDtEsiTit(((short)0));
        }
        else {
            ws.getIndDettTitCont().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getDtIniEffDb() {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public long getDtcDsRiga() {
        throw new FieldNotMappedException("dtcDsRiga");
    }

    @Override
    public void setDtcDsRiga(long dtcDsRiga) {
        throw new FieldNotMappedException("dtcDsRiga");
    }

    @Override
    public String getDtcDtIniCopDb() {
        throw new FieldNotMappedException("dtcDtIniCopDb");
    }

    @Override
    public void setDtcDtIniCopDb(String dtcDtIniCopDb) {
        throw new FieldNotMappedException("dtcDtIniCopDb");
    }

    @Override
    public String getDtcDtIniCopDbObj() {
        return getDtcDtIniCopDb();
    }

    @Override
    public void setDtcDtIniCopDbObj(String dtcDtIniCopDbObj) {
        setDtcDtIniCopDb(dtcDtIniCopDbObj);
    }

    @Override
    public int getDtcIdOgg() {
        return dettTitCont.getDtcIdOgg();
    }

    @Override
    public void setDtcIdOgg(int dtcIdOgg) {
        this.dettTitCont.setDtcIdOgg(dtcIdOgg);
    }

    @Override
    public String getDtcTpOgg() {
        return dettTitCont.getDtcTpOgg();
    }

    @Override
    public void setDtcTpOgg(String dtcTpOgg) {
        this.dettTitCont.setDtcTpOgg(dtcTpOgg);
    }

    @Override
    public String getDtcTpStatTit() {
        return dettTitCont.getDtcTpStatTit();
    }

    @Override
    public void setDtcTpStatTit(String dtcTpStatTit) {
        this.dettTitCont.setDtcTpStatTit(dtcTpStatTit);
    }

    @Override
    public int getFrqMovi() {
        throw new FieldNotMappedException("frqMovi");
    }

    @Override
    public void setFrqMovi(int frqMovi) {
        throw new FieldNotMappedException("frqMovi");
    }

    @Override
    public Integer getFrqMoviObj() {
        return ((Integer)getFrqMovi());
    }

    @Override
    public void setFrqMoviObj(Integer frqMoviObj) {
        setFrqMovi(((int)frqMoviObj));
    }

    @Override
    public int getIdDettTitCont() {
        throw new FieldNotMappedException("idDettTitCont");
    }

    @Override
    public void setIdDettTitCont(int idDettTitCont) {
        throw new FieldNotMappedException("idDettTitCont");
    }

    @Override
    public int getIdMoviChiu() {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public Integer getIdMoviChiuObj() {
        return ((Integer)getIdMoviChiu());
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        setIdMoviChiu(((int)idMoviChiuObj));
    }

    @Override
    public int getIdMoviCrz() {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public int getIdTitCont() {
        throw new FieldNotMappedException("idTitCont");
    }

    @Override
    public void setIdTitCont(int idTitCont) {
        throw new FieldNotMappedException("idTitCont");
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpAder() {
        throw new FieldNotMappedException("impAder");
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        throw new FieldNotMappedException("impAder");
    }

    @Override
    public AfDecimal getImpAderObj() {
        return getImpAder();
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        setImpAder(new AfDecimal(impAderObj, 15, 3));
    }

    @Override
    public AfDecimal getImpAz() {
        throw new FieldNotMappedException("impAz");
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        throw new FieldNotMappedException("impAz");
    }

    @Override
    public AfDecimal getImpAzObj() {
        return getImpAz();
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        setImpAz(new AfDecimal(impAzObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTfr() {
        throw new FieldNotMappedException("impTfr");
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        throw new FieldNotMappedException("impTfr");
    }

    @Override
    public AfDecimal getImpTfrObj() {
        return getImpTfr();
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        setImpTfr(new AfDecimal(impTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTfrStrc() {
        throw new FieldNotMappedException("impTfrStrc");
    }

    @Override
    public void setImpTfrStrc(AfDecimal impTfrStrc) {
        throw new FieldNotMappedException("impTfrStrc");
    }

    @Override
    public AfDecimal getImpTfrStrcObj() {
        return getImpTfrStrc();
    }

    @Override
    public void setImpTfrStrcObj(AfDecimal impTfrStrcObj) {
        setImpTfrStrc(new AfDecimal(impTfrStrcObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTrasfe() {
        throw new FieldNotMappedException("impTrasfe");
    }

    @Override
    public void setImpTrasfe(AfDecimal impTrasfe) {
        throw new FieldNotMappedException("impTrasfe");
    }

    @Override
    public AfDecimal getImpTrasfeObj() {
        return getImpTrasfe();
    }

    @Override
    public void setImpTrasfeObj(AfDecimal impTrasfeObj) {
        setImpTrasfe(new AfDecimal(impTrasfeObj, 15, 3));
    }

    @Override
    public AfDecimal getImpVolo() {
        throw new FieldNotMappedException("impVolo");
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        throw new FieldNotMappedException("impVolo");
    }

    @Override
    public AfDecimal getImpVoloObj() {
        return getImpVolo();
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        setImpVolo(new AfDecimal(impVoloObj, 15, 3));
    }

    @Override
    public AfDecimal getIntrFraz() {
        throw new FieldNotMappedException("intrFraz");
    }

    @Override
    public void setIntrFraz(AfDecimal intrFraz) {
        throw new FieldNotMappedException("intrFraz");
    }

    @Override
    public AfDecimal getIntrFrazObj() {
        return getIntrFraz();
    }

    @Override
    public void setIntrFrazObj(AfDecimal intrFrazObj) {
        setIntrFraz(new AfDecimal(intrFrazObj, 15, 3));
    }

    @Override
    public AfDecimal getIntrMora() {
        throw new FieldNotMappedException("intrMora");
    }

    @Override
    public void setIntrMora(AfDecimal intrMora) {
        throw new FieldNotMappedException("intrMora");
    }

    @Override
    public AfDecimal getIntrMoraObj() {
        return getIntrMora();
    }

    @Override
    public void setIntrMoraObj(AfDecimal intrMoraObj) {
        setIntrMora(new AfDecimal(intrMoraObj, 15, 3));
    }

    @Override
    public AfDecimal getIntrRetdt() {
        throw new FieldNotMappedException("intrRetdt");
    }

    @Override
    public void setIntrRetdt(AfDecimal intrRetdt) {
        throw new FieldNotMappedException("intrRetdt");
    }

    @Override
    public AfDecimal getIntrRetdtObj() {
        return getIntrRetdt();
    }

    @Override
    public void setIntrRetdtObj(AfDecimal intrRetdtObj) {
        setIntrRetdt(new AfDecimal(intrRetdtObj, 15, 3));
    }

    @Override
    public AfDecimal getIntrRiat() {
        throw new FieldNotMappedException("intrRiat");
    }

    @Override
    public void setIntrRiat(AfDecimal intrRiat) {
        throw new FieldNotMappedException("intrRiat");
    }

    @Override
    public AfDecimal getIntrRiatObj() {
        return getIntrRiat();
    }

    @Override
    public void setIntrRiatObj(AfDecimal intrRiatObj) {
        setIntrRiat(new AfDecimal(intrRiatObj, 15, 3));
    }

    @Override
    public int getLdbv2131IdOgg() {
        throw new FieldNotMappedException("ldbv2131IdOgg");
    }

    @Override
    public void setLdbv2131IdOgg(int ldbv2131IdOgg) {
        throw new FieldNotMappedException("ldbv2131IdOgg");
    }

    @Override
    public AfDecimal getLdbv2131TotPremi() {
        throw new FieldNotMappedException("ldbv2131TotPremi");
    }

    @Override
    public void setLdbv2131TotPremi(AfDecimal ldbv2131TotPremi) {
        throw new FieldNotMappedException("ldbv2131TotPremi");
    }

    @Override
    public AfDecimal getLdbv2131TotPremiObj() {
        return getLdbv2131TotPremi();
    }

    @Override
    public void setLdbv2131TotPremiObj(AfDecimal ldbv2131TotPremiObj) {
        setLdbv2131TotPremi(new AfDecimal(ldbv2131TotPremiObj, 18, 3));
    }

    @Override
    public String getLdbv2131TpOgg() {
        throw new FieldNotMappedException("ldbv2131TpOgg");
    }

    @Override
    public void setLdbv2131TpOgg(String ldbv2131TpOgg) {
        throw new FieldNotMappedException("ldbv2131TpOgg");
    }

    @Override
    public String getLdbv2131TpStatTit01() {
        throw new FieldNotMappedException("ldbv2131TpStatTit01");
    }

    @Override
    public void setLdbv2131TpStatTit01(String ldbv2131TpStatTit01) {
        throw new FieldNotMappedException("ldbv2131TpStatTit01");
    }

    @Override
    public String getLdbv2131TpStatTit02() {
        throw new FieldNotMappedException("ldbv2131TpStatTit02");
    }

    @Override
    public void setLdbv2131TpStatTit02(String ldbv2131TpStatTit02) {
        throw new FieldNotMappedException("ldbv2131TpStatTit02");
    }

    @Override
    public String getLdbv2131TpStatTit03() {
        throw new FieldNotMappedException("ldbv2131TpStatTit03");
    }

    @Override
    public void setLdbv2131TpStatTit03(String ldbv2131TpStatTit03) {
        throw new FieldNotMappedException("ldbv2131TpStatTit03");
    }

    @Override
    public String getLdbv2131TpStatTit04() {
        throw new FieldNotMappedException("ldbv2131TpStatTit04");
    }

    @Override
    public void setLdbv2131TpStatTit04(String ldbv2131TpStatTit04) {
        throw new FieldNotMappedException("ldbv2131TpStatTit04");
    }

    @Override
    public String getLdbv2131TpStatTit05() {
        throw new FieldNotMappedException("ldbv2131TpStatTit05");
    }

    @Override
    public void setLdbv2131TpStatTit05(String ldbv2131TpStatTit05) {
        throw new FieldNotMappedException("ldbv2131TpStatTit05");
    }

    @Override
    public String getLdbv4151DataInizPeriodoDb() {
        throw new FieldNotMappedException("ldbv4151DataInizPeriodoDb");
    }

    @Override
    public void setLdbv4151DataInizPeriodoDb(String ldbv4151DataInizPeriodoDb) {
        throw new FieldNotMappedException("ldbv4151DataInizPeriodoDb");
    }

    @Override
    public int getLdbv4151IdOgg() {
        throw new FieldNotMappedException("ldbv4151IdOgg");
    }

    @Override
    public void setLdbv4151IdOgg(int ldbv4151IdOgg) {
        throw new FieldNotMappedException("ldbv4151IdOgg");
    }

    @Override
    public AfDecimal getLdbv4151PreTot() {
        throw new FieldNotMappedException("ldbv4151PreTot");
    }

    @Override
    public void setLdbv4151PreTot(AfDecimal ldbv4151PreTot) {
        throw new FieldNotMappedException("ldbv4151PreTot");
    }

    @Override
    public String getLdbv4151TpOgg() {
        throw new FieldNotMappedException("ldbv4151TpOgg");
    }

    @Override
    public void setLdbv4151TpOgg(String ldbv4151TpOgg) {
        throw new FieldNotMappedException("ldbv4151TpOgg");
    }

    @Override
    public String getLdbv4151TpStatTit() {
        throw new FieldNotMappedException("ldbv4151TpStatTit");
    }

    @Override
    public void setLdbv4151TpStatTit(String ldbv4151TpStatTit) {
        throw new FieldNotMappedException("ldbv4151TpStatTit");
    }

    @Override
    public AfDecimal getManfeeAntic() {
        throw new FieldNotMappedException("manfeeAntic");
    }

    @Override
    public void setManfeeAntic(AfDecimal manfeeAntic) {
        throw new FieldNotMappedException("manfeeAntic");
    }

    @Override
    public AfDecimal getManfeeAnticObj() {
        return getManfeeAntic();
    }

    @Override
    public void setManfeeAnticObj(AfDecimal manfeeAnticObj) {
        setManfeeAntic(new AfDecimal(manfeeAnticObj, 15, 3));
    }

    @Override
    public AfDecimal getManfeeRec() {
        throw new FieldNotMappedException("manfeeRec");
    }

    @Override
    public void setManfeeRec(AfDecimal manfeeRec) {
        throw new FieldNotMappedException("manfeeRec");
    }

    @Override
    public AfDecimal getManfeeRecObj() {
        return getManfeeRec();
    }

    @Override
    public void setManfeeRecObj(AfDecimal manfeeRecObj) {
        setManfeeRec(new AfDecimal(manfeeRecObj, 15, 3));
    }

    @Override
    public AfDecimal getManfeeRicor() {
        throw new FieldNotMappedException("manfeeRicor");
    }

    @Override
    public void setManfeeRicor(AfDecimal manfeeRicor) {
        throw new FieldNotMappedException("manfeeRicor");
    }

    @Override
    public AfDecimal getManfeeRicorObj() {
        return getManfeeRicor();
    }

    @Override
    public void setManfeeRicorObj(AfDecimal manfeeRicorObj) {
        setManfeeRicor(new AfDecimal(manfeeRicorObj, 15, 3));
    }

    @Override
    public int getNumGgRitardoPag() {
        throw new FieldNotMappedException("numGgRitardoPag");
    }

    @Override
    public void setNumGgRitardoPag(int numGgRitardoPag) {
        throw new FieldNotMappedException("numGgRitardoPag");
    }

    @Override
    public Integer getNumGgRitardoPagObj() {
        return ((Integer)getNumGgRitardoPag());
    }

    @Override
    public void setNumGgRitardoPagObj(Integer numGgRitardoPagObj) {
        setNumGgRitardoPag(((int)numGgRitardoPagObj));
    }

    @Override
    public int getNumGgRival() {
        throw new FieldNotMappedException("numGgRival");
    }

    @Override
    public void setNumGgRival(int numGgRival) {
        throw new FieldNotMappedException("numGgRival");
    }

    @Override
    public Integer getNumGgRivalObj() {
        return ((Integer)getNumGgRival());
    }

    @Override
    public void setNumGgRivalObj(Integer numGgRivalObj) {
        setNumGgRival(((int)numGgRivalObj));
    }

    @Override
    public AfDecimal getPreNet() {
        throw new FieldNotMappedException("preNet");
    }

    @Override
    public void setPreNet(AfDecimal preNet) {
        throw new FieldNotMappedException("preNet");
    }

    @Override
    public AfDecimal getPreNetObj() {
        return getPreNet();
    }

    @Override
    public void setPreNetObj(AfDecimal preNetObj) {
        setPreNet(new AfDecimal(preNetObj, 15, 3));
    }

    @Override
    public AfDecimal getPrePpIas() {
        throw new FieldNotMappedException("prePpIas");
    }

    @Override
    public void setPrePpIas(AfDecimal prePpIas) {
        throw new FieldNotMappedException("prePpIas");
    }

    @Override
    public AfDecimal getPrePpIasObj() {
        return getPrePpIas();
    }

    @Override
    public void setPrePpIasObj(AfDecimal prePpIasObj) {
        setPrePpIas(new AfDecimal(prePpIasObj, 15, 3));
    }

    @Override
    public AfDecimal getPreSoloRsh() {
        throw new FieldNotMappedException("preSoloRsh");
    }

    @Override
    public void setPreSoloRsh(AfDecimal preSoloRsh) {
        throw new FieldNotMappedException("preSoloRsh");
    }

    @Override
    public AfDecimal getPreSoloRshObj() {
        return getPreSoloRsh();
    }

    @Override
    public void setPreSoloRshObj(AfDecimal preSoloRshObj) {
        setPreSoloRsh(new AfDecimal(preSoloRshObj, 15, 3));
    }

    @Override
    public AfDecimal getPreTot() {
        throw new FieldNotMappedException("preTot");
    }

    @Override
    public void setPreTot(AfDecimal preTot) {
        throw new FieldNotMappedException("preTot");
    }

    @Override
    public AfDecimal getPreTotObj() {
        return getPreTot();
    }

    @Override
    public void setPreTotObj(AfDecimal preTotObj) {
        setPreTot(new AfDecimal(preTotObj, 15, 3));
    }

    @Override
    public AfDecimal getProvAcq1aa() {
        throw new FieldNotMappedException("provAcq1aa");
    }

    @Override
    public void setProvAcq1aa(AfDecimal provAcq1aa) {
        throw new FieldNotMappedException("provAcq1aa");
    }

    @Override
    public AfDecimal getProvAcq1aaObj() {
        return getProvAcq1aa();
    }

    @Override
    public void setProvAcq1aaObj(AfDecimal provAcq1aaObj) {
        setProvAcq1aa(new AfDecimal(provAcq1aaObj, 15, 3));
    }

    @Override
    public AfDecimal getProvAcq2aa() {
        throw new FieldNotMappedException("provAcq2aa");
    }

    @Override
    public void setProvAcq2aa(AfDecimal provAcq2aa) {
        throw new FieldNotMappedException("provAcq2aa");
    }

    @Override
    public AfDecimal getProvAcq2aaObj() {
        return getProvAcq2aa();
    }

    @Override
    public void setProvAcq2aaObj(AfDecimal provAcq2aaObj) {
        setProvAcq2aa(new AfDecimal(provAcq2aaObj, 15, 3));
    }

    @Override
    public AfDecimal getProvDaRec() {
        throw new FieldNotMappedException("provDaRec");
    }

    @Override
    public void setProvDaRec(AfDecimal provDaRec) {
        throw new FieldNotMappedException("provDaRec");
    }

    @Override
    public AfDecimal getProvDaRecObj() {
        return getProvDaRec();
    }

    @Override
    public void setProvDaRecObj(AfDecimal provDaRecObj) {
        setProvDaRec(new AfDecimal(provDaRecObj, 15, 3));
    }

    @Override
    public AfDecimal getProvInc() {
        throw new FieldNotMappedException("provInc");
    }

    @Override
    public void setProvInc(AfDecimal provInc) {
        throw new FieldNotMappedException("provInc");
    }

    @Override
    public AfDecimal getProvIncObj() {
        return getProvInc();
    }

    @Override
    public void setProvIncObj(AfDecimal provIncObj) {
        setProvInc(new AfDecimal(provIncObj, 15, 3));
    }

    @Override
    public AfDecimal getProvRicor() {
        throw new FieldNotMappedException("provRicor");
    }

    @Override
    public void setProvRicor(AfDecimal provRicor) {
        throw new FieldNotMappedException("provRicor");
    }

    @Override
    public AfDecimal getProvRicorObj() {
        return getProvRicor();
    }

    @Override
    public void setProvRicorObj(AfDecimal provRicorObj) {
        setProvRicor(new AfDecimal(provRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getRemunAss() {
        throw new FieldNotMappedException("remunAss");
    }

    @Override
    public void setRemunAss(AfDecimal remunAss) {
        throw new FieldNotMappedException("remunAss");
    }

    @Override
    public AfDecimal getRemunAssObj() {
        return getRemunAss();
    }

    @Override
    public void setRemunAssObj(AfDecimal remunAssObj) {
        setRemunAss(new AfDecimal(remunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getSoprAlt() {
        throw new FieldNotMappedException("soprAlt");
    }

    @Override
    public void setSoprAlt(AfDecimal soprAlt) {
        throw new FieldNotMappedException("soprAlt");
    }

    @Override
    public AfDecimal getSoprAltObj() {
        return getSoprAlt();
    }

    @Override
    public void setSoprAltObj(AfDecimal soprAltObj) {
        setSoprAlt(new AfDecimal(soprAltObj, 15, 3));
    }

    @Override
    public AfDecimal getSoprProf() {
        throw new FieldNotMappedException("soprProf");
    }

    @Override
    public void setSoprProf(AfDecimal soprProf) {
        throw new FieldNotMappedException("soprProf");
    }

    @Override
    public AfDecimal getSoprProfObj() {
        return getSoprProf();
    }

    @Override
    public void setSoprProfObj(AfDecimal soprProfObj) {
        setSoprProf(new AfDecimal(soprProfObj, 15, 3));
    }

    @Override
    public AfDecimal getSoprSan() {
        throw new FieldNotMappedException("soprSan");
    }

    @Override
    public void setSoprSan(AfDecimal soprSan) {
        throw new FieldNotMappedException("soprSan");
    }

    @Override
    public AfDecimal getSoprSanObj() {
        return getSoprSan();
    }

    @Override
    public void setSoprSanObj(AfDecimal soprSanObj) {
        setSoprSan(new AfDecimal(soprSanObj, 15, 3));
    }

    @Override
    public AfDecimal getSoprSpo() {
        throw new FieldNotMappedException("soprSpo");
    }

    @Override
    public void setSoprSpo(AfDecimal soprSpo) {
        throw new FieldNotMappedException("soprSpo");
    }

    @Override
    public AfDecimal getSoprSpoObj() {
        return getSoprSpo();
    }

    @Override
    public void setSoprSpoObj(AfDecimal soprSpoObj) {
        setSoprSpo(new AfDecimal(soprSpoObj, 15, 3));
    }

    @Override
    public AfDecimal getSoprTec() {
        throw new FieldNotMappedException("soprTec");
    }

    @Override
    public void setSoprTec(AfDecimal soprTec) {
        throw new FieldNotMappedException("soprTec");
    }

    @Override
    public AfDecimal getSoprTecObj() {
        return getSoprTec();
    }

    @Override
    public void setSoprTecObj(AfDecimal soprTecObj) {
        setSoprTec(new AfDecimal(soprTecObj, 15, 3));
    }

    @Override
    public AfDecimal getSpeAge() {
        throw new FieldNotMappedException("speAge");
    }

    @Override
    public void setSpeAge(AfDecimal speAge) {
        throw new FieldNotMappedException("speAge");
    }

    @Override
    public AfDecimal getSpeAgeObj() {
        return getSpeAge();
    }

    @Override
    public void setSpeAgeObj(AfDecimal speAgeObj) {
        setSpeAge(new AfDecimal(speAgeObj, 15, 3));
    }

    @Override
    public AfDecimal getSpeMed() {
        throw new FieldNotMappedException("speMed");
    }

    @Override
    public void setSpeMed(AfDecimal speMed) {
        throw new FieldNotMappedException("speMed");
    }

    @Override
    public AfDecimal getSpeMedObj() {
        return getSpeMed();
    }

    @Override
    public void setSpeMedObj(AfDecimal speMedObj) {
        setSpeMed(new AfDecimal(speMedObj, 15, 3));
    }

    @Override
    public AfDecimal getTax() {
        throw new FieldNotMappedException("tax");
    }

    @Override
    public void setTax(AfDecimal tax) {
        throw new FieldNotMappedException("tax");
    }

    @Override
    public AfDecimal getTaxObj() {
        return getTax();
    }

    @Override
    public void setTaxObj(AfDecimal taxObj) {
        setTax(new AfDecimal(taxObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIntrPrest() {
        throw new FieldNotMappedException("totIntrPrest");
    }

    @Override
    public void setTotIntrPrest(AfDecimal totIntrPrest) {
        throw new FieldNotMappedException("totIntrPrest");
    }

    @Override
    public AfDecimal getTotIntrPrestObj() {
        return getTotIntrPrest();
    }

    @Override
    public void setTotIntrPrestObj(AfDecimal totIntrPrestObj) {
        setTotIntrPrest(new AfDecimal(totIntrPrestObj, 15, 3));
    }

    @Override
    public String getTpRgmFisc() {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
