package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0002Data;

/**Original name: LVVS0002<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0002
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... ESTRAZIONE PREMIO NETTO
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0002 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0002Data ws = new Lvvs0002Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0002
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0002_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU EX-S1000
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc() && this.idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0002 getInstance() {
        return ((Lvvs0002)Programs.getInstance(Lvvs0002.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI.
        initIxIndici();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: IF IVVC0213-ID-ADE-FITTZIO
        //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
        //           END-IF.
        if (ivvc0213.getModalitaIdAde().isIvvc0213IdAdeFittzio()) {
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
        //--> DCLGEN DI WORKING
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-CONTROLLO-DATI-EX
        //           END-IF
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG          TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
        // COB_CODE: SET COMUN-TROV-NO                  TO TRUE
        ws.getFlagComunTrov().setNo();
        // COB_CODE:      IF ( LIQUI-RISPAR-POLIND
        //                OR LIQUI-RISPAR-ADE
        //                OR LIQUI-RPP-TAKE-PROFIT
        //                OR LIQUI-RPP-REDDITO-PROGR )
        //           *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
        //                   THRU RECUP-MOVI-COMUN-EX
        //                END-IF
        if (ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiRisparAde() || ws.getWsMovimento().isLiquiRppTakeProfit() || ws.getWsMovimento().isLiquiRppRedditoProgr()) {
            //--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
            // COB_CODE: PERFORM RECUP-MOVI-COMUN
            //              THRU RECUP-MOVI-COMUN-EX
            recupMoviComun();
        }
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG          TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
        // COB_CODE: SET  IDSV0003-TRATT-X-COMPETENZA       TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //--> SE SONO IN FASE DI LIQUIDAZIONE DEVO ESCLUDERE DAL CALCOLO
        //--> IL VALORE CALCOLATO IN FASE DI COMUNICAZIONE
        // COB_CODE: IF (LIQUI-RISPAR-POLIND
        //           OR LIQUI-RISPAR-ADE
        //           OR LIQUI-RPP-TAKE-PROFIT
        //           OR LIQUI-RPP-REDDITO-PROGR) AND COMUN-TROV-SI
        //               MOVE WK-DATA-CPTZ-PREC    TO IDSV0003-DATA-COMPETENZA
        //           END-IF.
        if ((ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiRisparAde() || ws.getWsMovimento().isLiquiRppTakeProfit() || ws.getWsMovimento().isLiquiRppRedditoProgr()) && ws.getFlagComunTrov().isSi()) {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-DT-INI-EFF-TEMP
            ws.setWkDtIniEffTemp(idsv0003.getDataInizioEffetto());
            // COB_CODE: IF IDSV0003-DATA-COMPETENZA IS NUMERIC
            //              MOVE IDSV0003-DATA-COMPETENZA     TO WK-DT-CPTZ-TEMP
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataCompetenza())) {
                // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA     TO WK-DT-CPTZ-TEMP
                ws.setWkDtCptzTemp(idsv0003.getDataCompetenza());
            }
            // COB_CODE: MOVE WK-DATA-EFF-PREC     TO IDSV0003-DATA-INIZIO-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkDataEffPrec());
            // COB_CODE: MOVE WK-DATA-CPTZ-PREC    TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkDataCptzPrec());
        }
        //--> LETTURA TRCH POSITIVE
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1301-LEGGI-TRCH-POS           THRU S1301-EX
        //           END-IF
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1301-LEGGI-TRCH-POS           THRU S1301-EX
            s1301LeggiTrchPos();
        }
        //--> LETTURA TRCH NEGATIVE
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1302-LEGGI-TRCH-NEG           THRU S1302-EX
        //           END-IF
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1302-LEGGI-TRCH-NEG           THRU S1302-EX
            s1302LeggiTrchNeg();
        }
        // COB_CODE: IF (LIQUI-RISPAR-POLIND
        //           OR LIQUI-RISPAR-ADE
        //           OR LIQUI-RPP-REDDITO-PROGR
        //           OR LIQUI-RPP-TAKE-PROFIT) AND COMUN-TROV-SI
        //               MOVE WK-DT-CPTZ-TEMP     TO IDSV0003-DATA-COMPETENZA
        //           END-IF.
        if ((ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiRisparAde() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRppTakeProfit()) && ws.getFlagComunTrov().isSi()) {
            // COB_CODE: MOVE WK-DT-INI-EFF-TEMP  TO IDSV0003-DATA-INIZIO-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkDtIniEffTemp());
            // COB_CODE: MOVE WK-DT-CPTZ-TEMP     TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkDtCptzTemp());
        }
    }

    /**Original name: RECUP-MOVI-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Recupero il movimento di comunicazione
	 * ----------------------------------------------------------------*</pre>*/
    private void recupMoviComun() {
        Ldbs6040 ldbs6040 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-FETCH-FIRST           TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET NO-ULTIMA-LETTURA              TO TRUE
        ws.getFlagUltimaLettura().setNoUltimaLettura();
        // COB_CODE: SET INIT-CUR-MOV                   TO TRUE
        ws.getFlagCurMov().setInitCurMov();
        // COB_CODE: SET COMUN-TROV-NO                  TO TRUE
        ws.getFlagComunTrov().setNo();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-CUR-MOV
        //                      OR COMUN-TROV-SI
        //              END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagCurMov().isFineCurMov() || ws.getFlagComunTrov().isSi())) {
            // COB_CODE: INITIALIZE MOVI
            initMovi();
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA            TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                           TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR       TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION       TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: MOVE 'LDBS6040'            TO WK-PGM-CALL
            ws.setWkPgmCall("LDBS6040");
            // COB_CODE: CALL WK-PGM-CALL   USING  IDSV0003 MOVI
            //           ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbs6040 = Ldbs6040.getInstance();
                ldbs6040.run(idsv0003, ws.getMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE 'LDBS6040'
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe("LDBS6040");
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA ");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //             END-EVALUATE
            //           ELSE
            //             END-IF
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE: EVALUATE TRUE
                //               WHEN IDSV0003-NOT-FOUND
                //                 END-IF
                //               WHEN IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //               WHEN OTHER
                //                   END-STRING
                //           END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: SET FINE-CUR-MOV TO TRUE
                        ws.getFlagCurMov().setFineCurMov();
                        // COB_CODE: IF IDSV0003-FETCH-FIRST
                        //              END-STRING
                        //           ELSE
                        //              SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
                        //           END-IF
                        if (idsv0003.getOperazione().isFetchFirst()) {
                            // COB_CODE: MOVE 'LDBS6040'     TO IDSV0003-COD-SERVIZIO-BE
                            idsv0003.getCampiEsito().setCodServizioBe("LDBS6040");
                            // COB_CODE: STRING 'CHIAMATA LDBS6040 ;'
                            //                  IDSV0003-RETURN-CODE ';'
                            //                  IDSV0003-SQLCODE
                            //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        }
                        else {
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
                            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
                            idsv0003.getSqlcode().setSuccessfulSql();
                        }
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: IF SI-ULTIMA-LETTURA
                        //              SET FINE-CUR-MOV   TO TRUE
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-IF
                        if (ws.getFlagUltimaLettura().isSiUltimaLettura()) {
                            // COB_CODE: SET COMUN-TROV-SI   TO TRUE
                            ws.getFlagComunTrov().setSi();
                            // COB_CODE: PERFORM CLOSE-MOVI
                            //              THRU CLOSE-MOVI-EX
                            closeMovi();
                            // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                            ws.getFlagCurMov().setFineCurMov();
                        }
                        else {
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            idsv0003.getOperazione().setFetchNext();
                        }
                        // COB_CODE: MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                        ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                        // COB_CODE: IF ( COMUN-RISPAR-IND
                        //           OR COMUN-RISPAR-ADE
                        //           OR RPP-TAKE-PROFIT
                        //           OR RPP-REDDITO-PROGRAMMATO )
                        //           AND ( MOV-DT-EFF = IDSV0003-DATA-INIZIO-EFFETTO )
                        //                                        - 1
                        //           END-IF
                        if ((ws.getWsMovimento().isComunRisparInd() || ws.getWsMovimento().isComunRisparAde() || ws.getWsMovimento().isRppTakeProfit() || ws.getWsMovimento().isRppRedditoProgrammato()) && ws.getMovi().getMovDtEff() == idsv0003.getDataInizioEffetto()) {
                            // COB_CODE: MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                            ws.setWkIdMoviComun(ws.getMovi().getMovIdMovi());
                            // COB_CODE: SET SI-ULTIMA-LETTURA  TO TRUE
                            ws.getFlagUltimaLettura().setSiUltimaLettura();
                            // COB_CODE: MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                            ws.setWkDataEffPrec(ws.getMovi().getMovDtEff());
                            // COB_CODE: COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                            //                                     - 1
                            ws.setWkDataCptzPrec(Trunc.toLong(ws.getMovi().getMovDsTsCptz() - 1, 18));
                        }
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE WK-PGM-CALL
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCall());
                        // COB_CODE: STRING 'ERRORE RECUP MOVI COMUN ;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE RECUP MOVI COMUN ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
            else {
                // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'CHIAMATA LDBS6040 ;'
                //                  IDSV0003-RETURN-CODE ';'
                //                  IDSV0003-SQLCODE
                //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: IF IDSV0003-NOT-FOUND
                //           OR IDSV0003-SQLCODE = -305
                //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                //           ELSE
                //              SET IDSV0003-INVALID-OPER            TO TRUE
                //           END-IF
                if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                    // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                }
                else {
                    // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
            }
        }
    }

    /**Original name: CLOSE-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
	 * ----------------------------------------------------------------*</pre>*/
    private void closeMovi() {
        Ldbs6040 ldbs6040 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR   TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: MOVE SPACES                     TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond("");
        //
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR       TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        //
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE 'LDBS6040'            TO WK-PGM-CALL
        ws.setWkPgmCall("LDBS6040");
        // COB_CODE: CALL WK-PGM-CALL    USING  IDSV0003 MOVI
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbs6040 = Ldbs6040.getInstance();
            ldbs6040.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE 'LDBS6040'
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe("LDBS6040");
            // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSV0003-SUCCESSFUL-SQL
            //                   CONTINUE
            //               WHEN OTHER
            //                   END-IF
            //           END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                    break;

                default:// COB_CODE: MOVE WK-CALL-PGM       TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                    // COB_CODE: STRING 'CHIAMATA LDBS5140 ;'
                    //                  IDSV0003-RETURN-CODE ';'
                    //                  IDSV0003-SQLCODE
                    //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS5140 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    // COB_CODE: IF IDSV0003-NOT-FOUND
                    //           OR IDSV0003-SQLCODE = -305
                    //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    //           ELSE
                    //              SET IDSV0003-INVALID-OPER            TO TRUE
                    //           END-IF
                    if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                    }
                    else {
                        // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                    }
                    break;
            }
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS5140 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS5140 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POLIZZA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POLIZZA
            ws.setDpolAreaPolizzaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*
	 *     MOVE DPOL-TP-FRM-ASSVA            TO WS-TP-FRM-ASSVA</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF DPOL-ID-POLI IS NUMERIC
        //              END-IF
        //           ELSE
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           END-IF.
        if (Functions.isNumber(ws.getLccvpol1().getDati().getWpolIdPoli())) {
            // COB_CODE: IF DPOL-ID-POLI = 0
            //               TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (ws.getLccvpol1().getDati().getWpolIdPoli() == 0) {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
            }
        }
        else {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //             END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF COLLETTIVA  AND
            //              IVVC0213-ID-ADESIONE  = ZEROES
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (ws.getWsTpFrmAssva().isCollettiva() && Characters.EQ_ZERO.test(ivvc0213.getIdAdesioneFormatted())) {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-ADESIONE NO VALORIZZATO PER POLIZZA COLLETTIVA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-ADESIONE NO VALORIZZATO PER POLIZZA COLLETTIVA");
            }
        }
    }

    /**Original name: S1301-LEGGI-TRCH-POS<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1301LeggiTrchPos() {
        Ldbs5140 ldbs5140 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-SELECT           TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION  TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        //
        // COB_CODE: INITIALIZE LDBV5141.
        initLdbv5141();
        //
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE     TO LDBV5141-ID-ADES.
        ws.getLdbv5141().setIdAdes(ivvc0213.getIdAdesione());
        // COB_CODE: MOVE WS-TRCH-POS              TO LDBV5141-TP-TRCH.
        ws.getLdbv5141().getTpTrch().setTpTrchBytes(ws.getWsTrchPos().getWsTrchPosBytes());
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV5141
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs5140 = Ldbs5140.getInstance();
            ldbs5140.run(idsv0003, ws.getLdbv5141());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS5140 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS5140 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF LDBV5141-CUM-PRE-ATT IS NUMERIC
            //                TO WS-CUM-PRE-ATT
            //           END-IF
            if (Functions.isNumber(ws.getLdbv5141().getCumPreAtt())) {
                // COB_CODE: MOVE LDBV5141-CUM-PRE-ATT
                //             TO WS-CUM-PRE-ATT
                ws.setWsCumPreAtt(Trunc.toDecimal(ws.getLdbv5141().getCumPreAtt(), 15, 3));
            }
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS5140 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS5140 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1302-LEGGI-TRCH-NEG<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1302LeggiTrchNeg() {
        Ldbs5140 ldbs5140 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-SELECT           TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION  TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        //
        // COB_CODE: INITIALIZE LDBV5141.
        initLdbv5141();
        //
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE     TO LDBV5141-ID-ADES.
        ws.getLdbv5141().setIdAdes(ivvc0213.getIdAdesione());
        // COB_CODE: MOVE WS-TRCH-NEG              TO LDBV5141-TP-TRCH.
        ws.getLdbv5141().getTpTrch().setTpTrchBytes(ws.getWsTrchNeg().getWsTrchNegBytes());
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV5141
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs5140 = Ldbs5140.getInstance();
            ldbs5140.run(idsv0003, ws.getLdbv5141());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS5140 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS5140 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF LDBV5141-CUM-PRE-ATT IS NUMERIC
            //                                           LDBV5141-CUM-PRE-ATT
            //           END-IF
            if (Functions.isNumber(ws.getLdbv5141().getCumPreAtt())) {
                // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O = WS-CUM-PRE-ATT -
                //                                        LDBV5141-CUM-PRE-ATT
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWsCumPreAtt().subtract(ws.getLdbv5141().getCumPreAtt()), 18, 7));
            }
        }
        else if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              MOVE WS-CUM-PRE-ATT         TO IVVC0213-VAL-IMP-O
            //           ELSE
            //              END-STRING
            //           END-IF
            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: MOVE WS-CUM-PRE-ATT         TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWsCumPreAtt(), 18, 7));
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS5140 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS5140 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }

    public void initLdbv5141() {
        ws.getLdbv5141().getTpTrch().setTrch1("");
        ws.getLdbv5141().getTpTrch().setTrch2("");
        ws.getLdbv5141().getTpTrch().setTrch3("");
        ws.getLdbv5141().getTpTrch().setTrch4("");
        ws.getLdbv5141().getTpTrch().setTrch5("");
        ws.getLdbv5141().getTpTrch().setTrch6("");
        ws.getLdbv5141().getTpTrch().setTrch7("");
        ws.getLdbv5141().getTpTrch().setTrch8("");
        ws.getLdbv5141().getTpTrch().setTrch9("");
        ws.getLdbv5141().getTpTrch().setTrch10("");
        ws.getLdbv5141().getTpTrch().setTrch11("");
        ws.getLdbv5141().getTpTrch().setTrch12("");
        ws.getLdbv5141().getTpTrch().setTrch13("");
        ws.getLdbv5141().getTpTrch().setTrch14("");
        ws.getLdbv5141().getTpTrch().setTrch15("");
        ws.getLdbv5141().getTpTrch().setTrch16("");
        ws.getLdbv5141().setIdAdes(0);
        ws.getLdbv5141().setCumPreAtt(new AfDecimal(0, 15, 3));
    }
}
