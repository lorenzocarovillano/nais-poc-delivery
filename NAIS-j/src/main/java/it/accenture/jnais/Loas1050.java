package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Wl71Dati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaLoas1050;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Loas1050Data;

/**Original name: LOAS1050<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             AISS.
 * DATE-WRITTEN.       2010.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *     PROGRAMMA ..... LOAS1050
 *     TIPOLOGIA...... GESTIONE MATR_ELAB_BATCH
 *     PROCESSO....... OPERAZIONI AUTOMATICA
 *     FUNZIONE....... TRASVERSALI
 *     DESCRIZIONE.... AGGIORNAMENTO DATA ELABORAZIONE
 * **------------------------------------------------------------***</pre>*/
public class Loas1050 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Loas1050Data ws = new Loas1050Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: AREA-LOAS1050
    private AreaLoas1050 areaLoas1050;

    //==== METHODS ====
    /**Original name: PROGRAM_LOAS1050_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, AreaLoas1050 areaLoas1050) {
        this.areaIdsv0001 = areaIdsv0001;
        this.areaLoas1050 = areaLoas1050;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Loas1050 getInstance() {
        return ((Loas1050)Programs.getInstance(Loas1050.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI                                         *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: PERFORM INIZIA-TOT-L71
        //              THRU INIZIA-TOT-L71-EX.
        iniziaTotL71();
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S2100-LEGGI-MATR-ELAB
        //              THRU EX-S2100
        s2100LeggiMatrElab();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU AGGIORNA-MATR-ELAB-BATCH-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE IX-TAB-L71
            //             TO WL71-ELE-MATR-ELAB-MAX
            ws.setWl71EleMatrElabMax(TruncAbs.toInt(ws.getIxTabL71(), 4));
            // COB_CODE: PERFORM AGGIORNA-MATR-ELAB-BATCH
            //              THRU AGGIORNA-MATR-ELAB-BATCH-EX
            aggiornaMatrElabBatch();
        }
    }

    /**Original name: S2100-LEGGI-MATR-ELAB<br>
	 * <pre>----------------------------------------------------------------
	 *      LETTURA MATR_ELAB_BATCH
	 * ----------------------------------------------------------------</pre>*/
    private void s2100LeggiMatrElab() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE                       MATR-ELAB-BATCH.
        initMatrElabBatch();
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: MOVE 'LDBS6590'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS6590");
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE S1050-COD-RAMO           TO L71-COD-RAMO.
        ws.getMatrElabBatch().setCodRamo(areaLoas1050.getCodRamo());
        // COB_CODE: MOVE S1050-FORMA-ASS          TO L71-TP-FRM-ASSVA.
        ws.getMatrElabBatch().setTpFrmAssva(areaLoas1050.getFormaAss());
        // COB_CODE: MOVE S1050-TP-MOVI            TO L71-TP-MOVI.
        ws.getMatrElabBatch().setTpMovi(areaLoas1050.getTpMovi());
        // COB_CODE: MOVE MATR-ELAB-BATCH          TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getMatrElabBatch().getMatrElabBatchFormatted());
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:         IF IDSO0011-SUCCESSFUL-RC
        //                      END-EVALUATE
        //                   ELSE
        //           *-->       GESTIONE ERRORE DISPATCHER
        //                         THRU EX-S0300
        //                   END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:            EVALUATE TRUE
            //                          WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
            //                             SET  WL71-ST-MOD TO TRUE
            //                          WHEN IDSO0011-NOT-FOUND
            //           *--->          CHIAVE NON TROVATA
            //                               TO WL71-DT-ULT-ELAB
            //                          WHEN OTHER
            //           *--->          ERRORE DI ACCESSO AL DB
            //                                  THRU EX-S0300
            //                      END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO MATR-ELAB-BATCH
                    ws.getMatrElabBatch().setMatrElabBatchFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE 1           TO IX-TAB-L71
                    ws.setIxTabL71(((short)1));
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-L71
                    //              THRU VALORIZZA-OUTPUT-L71-EX
                    valorizzaOutputL71();
                    // COB_CODE: MOVE S1050-DT-ULT-ELABORAZIONE
                    //             TO WL71-DT-ULT-ELAB
                    ws.getLccvl711().getDati().setDtUltElab(areaLoas1050.getDtUltElaborazione());
                    // COB_CODE: SET  WL71-ST-MOD TO TRUE
                    ws.getLccvl711().getStatus().setMod();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->          CHIAVE NON TROVATA
                    // COB_CODE: MOVE 1                      TO IX-TAB-L71
                    ws.setIxTabL71(((short)1));
                    // COB_CODE: SET WL71-ST-ADD             TO TRUE
                    ws.getLccvl711().getStatus().setWcomStAdd();
                    // COB_CODE: MOVE S1050-COD-RAMO         TO WL71-COD-RAMO
                    ws.getLccvl711().getDati().setCodRamo(areaLoas1050.getCodRamo());
                    // COB_CODE: MOVE S1050-FORMA-ASS        TO WL71-TP-FRM-ASSVA
                    ws.getLccvl711().getDati().setTpFrmAssva(areaLoas1050.getFormaAss());
                    // COB_CODE: MOVE S1050-TP-MOVI          TO WL71-TP-MOVI
                    ws.getLccvl711().getDati().setTpMovi(areaLoas1050.getTpMovi());
                    // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
                    //             TO WL71-COD-COMP-ANIA
                    ws.getLccvl711().getDati().setCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
                    // COB_CODE: MOVE S1050-DT-ULT-ELABORAZIONE
                    //             TO WL71-DT-ULT-ELAB
                    ws.getLccvl711().getDati().setDtUltElab(areaLoas1050.getDtUltElaborazione());
                    break;

                default://--->          ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'LDBS6590'    TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("LDBS6590");
                    // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'MATR-ELAB-BATCH'    ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "MATR-ELAB-BATCH", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->       GESTIONE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'LDBS6590'    TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("LDBS6590");
            // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'MATR-ELAB-BATCH'     ';'
            //                   IDSO0011-RETURN-CODE ';'
            //                   IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "MATR-ELAB-BATCH", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: AGGIORNA-TABELLA<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PROCEDURE (COMPONENTI COMUNI)
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     COPY      ..... LCCP0001
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void aggiornaTabella() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            // COB_CODE:         EVALUATE TRUE
            //           *-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            //                     WHEN IDSO0011-SUCCESSFUL-SQL
            //                        CONTINUE
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                     WHEN OTHER
            //                           THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    break;

                default:// COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'AGGIORNA-TABELLA'
                    //                                 TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
                    // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA            ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE   INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'AGGIORNA-TABELLA'
            //                                      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA            ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE        INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: ESTR-SEQUENCE<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCP0002
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*</pre>*/
    private void estrSequence() {
        Lccs0090 lccs0090 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA TO S090-NOME-TABELLA.
        ws.getAreaIoLccs0090().setNomeTabella(ws.getWkTabella());
        // COB_CODE: CALL LCCS0090 USING AREA-IO-LCCS0090
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0090 = Lccs0090.getInstance();
            lccs0090.run(ws.getAreaIoLccs0090());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO ESTRAZIONE SEQUENCE'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO ESTRAZIONE SEQUENCE");
            // COB_CODE: MOVE 'ESTR-SEQUENCE'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("ESTR-SEQUENCE");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-EVALUATE
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         EVALUATE S090-RETURN-CODE
            //                       WHEN '00'
            //                            CONTINUE
            //                       WHEN 'S1'
            //           *-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
            //                             THRU EX-S0300
            //                       WHEN 'D3'
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getAreaIoLccs0090().getReturnCode().getReturnCode()) {

                case "00":// COB_CODE: CONTINUE
                //continue
                    break;

                case "S1"://-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005015'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case "D3"://-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005016'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default:break;
            }
        }
    }

    /**Original name: VALORIZZA-OUTPUT-L71<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PROCEDURE LETTURA PORTAFOGLIO
	 * ----------------------------------------------------------------*
	 *      COPY LCCVL712              REPLACING ==(SF)== BY ==WL71==.
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVL713
	 *    ULTIMO AGG. 22 APR 2010
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputL71() {
        // COB_CODE: MOVE L71-ID-MATR-ELAB-BATCH
        //             TO (SF)-ID-PTF
        ws.getLccvl711().setIdPtf(ws.getMatrElabBatch().getIdMatrElabBatch());
        // COB_CODE: MOVE L71-ID-MATR-ELAB-BATCH
        //             TO (SF)-ID-MATR-ELAB-BATCH
        ws.getLccvl711().getDati().setIdMatrElabBatch(ws.getMatrElabBatch().getIdMatrElabBatch());
        // COB_CODE: MOVE L71-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA
        ws.getLccvl711().getDati().setCodCompAnia(ws.getMatrElabBatch().getCodCompAnia());
        // COB_CODE: MOVE L71-TP-FRM-ASSVA
        //             TO (SF)-TP-FRM-ASSVA
        ws.getLccvl711().getDati().setTpFrmAssva(ws.getMatrElabBatch().getTpFrmAssva());
        // COB_CODE: MOVE L71-TP-MOVI
        //             TO (SF)-TP-MOVI
        ws.getLccvl711().getDati().setTpMovi(ws.getMatrElabBatch().getTpMovi());
        // COB_CODE: IF L71-COD-RAMO-NULL = HIGH-VALUES
        //                TO (SF)-COD-RAMO-NULL
        //           ELSE
        //                TO (SF)-COD-RAMO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getMatrElabBatch().getCodRamoFormatted())) {
            // COB_CODE: MOVE L71-COD-RAMO-NULL
            //             TO (SF)-COD-RAMO-NULL
            ws.getLccvl711().getDati().setCodRamo(ws.getMatrElabBatch().getCodRamo());
        }
        else {
            // COB_CODE: MOVE L71-COD-RAMO
            //             TO (SF)-COD-RAMO
            ws.getLccvl711().getDati().setCodRamo(ws.getMatrElabBatch().getCodRamo());
        }
        // COB_CODE: MOVE L71-DT-ULT-ELAB
        //             TO (SF)-DT-ULT-ELAB
        ws.getLccvl711().getDati().setDtUltElab(ws.getMatrElabBatch().getDtUltElab());
        // COB_CODE: MOVE L71-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL
        ws.getLccvl711().getDati().setDsOperSql(ws.getMatrElabBatch().getDsOperSql());
        // COB_CODE: MOVE L71-DS-VER
        //             TO (SF)-DS-VER
        ws.getLccvl711().getDati().setDsVer(ws.getMatrElabBatch().getDsVer());
        // COB_CODE: MOVE L71-DS-TS-CPTZ
        //             TO (SF)-DS-TS-CPTZ
        ws.getLccvl711().getDati().setDsTsCptz(ws.getMatrElabBatch().getDsTsCptz());
        // COB_CODE: MOVE L71-DS-UTENTE
        //             TO (SF)-DS-UTENTE
        ws.getLccvl711().getDati().setDsUtente(ws.getMatrElabBatch().getDsUtente());
        // COB_CODE: MOVE L71-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB.
        ws.getLccvl711().getDati().setDsStatoElab(ws.getMatrElabBatch().getDsStatoElab());
    }

    /**Original name: INIZIA-TOT-L71<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVL714
	 *    ULTIMO AGG. 22 APR 2010
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotL71() {
        // COB_CODE: PERFORM INIZIA-ZEROES-L71 THRU INIZIA-ZEROES-L71-EX
        iniziaZeroesL71();
        // COB_CODE: PERFORM INIZIA-SPACES-L71 THRU INIZIA-SPACES-L71-EX
        iniziaSpacesL71();
        // COB_CODE: PERFORM INIZIA-NULL-L71 THRU INIZIA-NULL-L71-EX.
        iniziaNullL71();
    }

    /**Original name: INIZIA-NULL-L71<br>*/
    private void iniziaNullL71() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-RAMO-NULL.
        ws.getLccvl711().getDati().setCodRamo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wl71Dati.Len.COD_RAMO));
    }

    /**Original name: INIZIA-ZEROES-L71<br>*/
    private void iniziaZeroesL71() {
        // COB_CODE: MOVE 0 TO (SF)-ID-MATR-ELAB-BATCH
        ws.getLccvl711().getDati().setIdMatrElabBatch(0);
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA
        ws.getLccvl711().getDati().setCodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-TP-MOVI
        ws.getLccvl711().getDati().setTpMovi(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-ULT-ELAB
        ws.getLccvl711().getDati().setDtUltElab(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER
        ws.getLccvl711().getDati().setDsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-CPTZ.
        ws.getLccvl711().getDati().setDsTsCptz(0);
    }

    /**Original name: INIZIA-SPACES-L71<br>*/
    private void iniziaSpacesL71() {
        // COB_CODE: MOVE SPACES TO (SF)-TP-FRM-ASSVA
        ws.getLccvl711().getDati().setTpFrmAssva("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-OPER-SQL
        ws.getLccvl711().getDati().setDsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-UTENTE
        ws.getLccvl711().getDati().setDsUtente("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-STATO-ELAB.
        ws.getLccvl711().getDati().setDsStatoElab(Types.SPACE_CHAR);
    }

    /**Original name: VAL-DCLGEN-L71<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PROCEDURE AGGIORNAMENTO PORTAFOGLIO
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVL715
	 *    ULTIMO AGG. 22 APR 2010
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenL71() {
        // COB_CODE: MOVE (SF)-ID-MATR-ELAB-BATCH
        //              TO L71-ID-MATR-ELAB-BATCH
        ws.getMatrElabBatch().setIdMatrElabBatch(ws.getLccvl711().getDati().getIdMatrElabBatch());
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA
        //              TO L71-COD-COMP-ANIA
        ws.getMatrElabBatch().setCodCompAnia(ws.getLccvl711().getDati().getCodCompAnia());
        // COB_CODE: MOVE (SF)-TP-FRM-ASSVA
        //              TO L71-TP-FRM-ASSVA
        ws.getMatrElabBatch().setTpFrmAssva(ws.getLccvl711().getDati().getTpFrmAssva());
        // COB_CODE: MOVE (SF)-TP-MOVI
        //              TO L71-TP-MOVI
        ws.getMatrElabBatch().setTpMovi(ws.getLccvl711().getDati().getTpMovi());
        // COB_CODE: IF (SF)-COD-RAMO-NULL = HIGH-VALUES
        //              TO L71-COD-RAMO-NULL
        //           ELSE
        //              TO L71-COD-RAMO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvl711().getDati().getCodRamoFormatted())) {
            // COB_CODE: MOVE (SF)-COD-RAMO-NULL
            //           TO L71-COD-RAMO-NULL
            ws.getMatrElabBatch().setCodRamo(ws.getLccvl711().getDati().getCodRamo());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-RAMO
            //           TO L71-COD-RAMO
            ws.getMatrElabBatch().setCodRamo(ws.getLccvl711().getDati().getCodRamo());
        }
        // COB_CODE: MOVE (SF)-DT-ULT-ELAB
        //              TO L71-DT-ULT-ELAB
        ws.getMatrElabBatch().setDtUltElab(ws.getLccvl711().getDati().getDtUltElab());
        // COB_CODE: MOVE (SF)-DS-OPER-SQL
        //              TO L71-DS-OPER-SQL
        ws.getMatrElabBatch().setDsOperSql(ws.getLccvl711().getDati().getDsOperSql());
        // COB_CODE: IF (SF)-DS-VER NOT NUMERIC
        //              MOVE 0 TO L71-DS-VER
        //           ELSE
        //              TO L71-DS-VER
        //           END-IF
        if (!Functions.isNumber(ws.getLccvl711().getDati().getDsVer())) {
            // COB_CODE: MOVE 0 TO L71-DS-VER
            ws.getMatrElabBatch().setDsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER
            //           TO L71-DS-VER
            ws.getMatrElabBatch().setDsVer(ws.getLccvl711().getDati().getDsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-CPTZ NOT NUMERIC
        //              MOVE 0 TO L71-DS-TS-CPTZ
        //           ELSE
        //              TO L71-DS-TS-CPTZ
        //           END-IF
        if (!Functions.isNumber(ws.getLccvl711().getDati().getDsTsCptz())) {
            // COB_CODE: MOVE 0 TO L71-DS-TS-CPTZ
            ws.getMatrElabBatch().setDsTsCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-CPTZ
            //           TO L71-DS-TS-CPTZ
            ws.getMatrElabBatch().setDsTsCptz(ws.getLccvl711().getDati().getDsTsCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE
        //              TO L71-DS-UTENTE
        ws.getMatrElabBatch().setDsUtente(ws.getLccvl711().getDati().getDsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB
        //              TO L71-DS-STATO-ELAB.
        ws.getMatrElabBatch().setDsStatoElab(ws.getLccvl711().getDati().getDsStatoElab());
    }

    /**Original name: AGGIORNA-MATR-ELAB-BATCH<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCVL716
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO MATRICE ELABORAZIONE BATCH
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCVL715 (VALORIZZAZIONE DCLGEN)
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN MATRICE ELABORAZIONE BATCH (LCCVL711)
	 *   - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI</pre>*/
    private void aggiornaMatrElabBatch() {
        // COB_CODE: INITIALIZE MATR-ELAB-BATCH.
        initMatrElabBatch();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'MATR-ELAB-BATCH'                  TO WK-TABELLA.
        ws.setWkTabella("MATR-ELAB-BATCH");
        //--> SE LO STATUS NON E' "INVARIATO"
        // COB_CODE:      IF NOT WL71-ST-INV
        //                   AND NOT WL71-ST-CON
        //                   AND WL71-ELE-MATR-ELAB-MAX NOT = 0
        //           *-->    CONTROLLO DELLO STATUS
        //                   END-IF
        //                END-IF.
        if (!ws.getLccvl711().getStatus().isInv() && !ws.getLccvl711().getStatus().isWpmoStCon() && ws.getWl71EleMatrElabMax() != 0) {
            //-->    CONTROLLO DELLO STATUS
            //-->        INSERIMENTO
            // COB_CODE:         EVALUATE TRUE
            //           *-->        INSERIMENTO
            //                       WHEN WL71-ST-ADD
            //           *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
            //                          SET IDSI0011-INSERT              TO TRUE
            //           *-->        MODIFICA
            //                       WHEN WL71-ST-MOD
            //           *-->           TIPO OPERAZIONE DISPATCHER
            //                          SET IDSI0011-UPDATE              TO TRUE
            //           *-->        CANCELLAZIONE
            //                       WHEN WL71-ST-DEL
            //           *-->           TIPO OPERAZIONE DISPATCHER
            //                          SET IDSI0011-DELETE              TO TRUE
            //                   END-EVALUATE
            switch (ws.getLccvl711().getStatus().getStatus()) {

                case WpolStatus.ADD://-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                    // COB_CODE: PERFORM ESTR-SEQUENCE
                    //              THRU ESTR-SEQUENCE-EX
                    estrSequence();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //                                       WL71-ID-MATR-ELAB-BATCH
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: MOVE S090-SEQ-TABELLA TO WL71-ID-PTF
                        //                                    WL71-ID-MATR-ELAB-BATCH
                        ws.getLccvl711().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
                        ws.getLccvl711().getDati().setIdMatrElabBatch(ws.getAreaIoLccs0090().getSeqTabella());
                    }
                    //-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET IDSI0011-INSERT              TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setInsert();
                    //-->        MODIFICA
                    break;

                case WpolStatus.MOD://-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET IDSI0011-UPDATE              TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Update();
                    //-->        CANCELLAZIONE
                    break;

                case WpolStatus.DEL://-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET IDSI0011-DELETE              TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Delete();
                    break;

                default:break;
            }
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       VALORIZZA DCLGEN ADESIONE
            //                         THRU AGGIORNA-TABELLA-EX
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->       VALORIZZA DCLGEN ADESIONE
                // COB_CODE: PERFORM VAL-DCLGEN-L71
                //              THRU VAL-DCLGEN-L71-EX
                valDclgenL71();
                //-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                // COB_CODE: PERFORM VALORIZZA-AREA-DSH-L71
                //              THRU VALORIZZA-AREA-DSH-L71-EX
                valorizzaAreaDshL71();
                //-->       CALL DISPATCHER PER AGGIORNAMENTO
                // COB_CODE: PERFORM AGGIORNA-TABELLA
                //              THRU AGGIORNA-TABELLA-EX
                aggiornaTabella();
            }
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-L71<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> DCLGEN TABELLA</pre>*/
    private void valorizzaAreaDshL71() {
        // COB_CODE: MOVE MATR-ELAB-BATCH          TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getMatrElabBatch().getMatrElabBatchFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-PRIMARY-KEY      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>----------------------------------------------------------------*
	 *  ROUTINES GESTIONE ERRORI                                       *
	 * ----------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    public void initMatrElabBatch() {
        ws.getMatrElabBatch().setIdMatrElabBatch(0);
        ws.getMatrElabBatch().setCodCompAnia(0);
        ws.getMatrElabBatch().setTpFrmAssva("");
        ws.getMatrElabBatch().setTpMovi(0);
        ws.getMatrElabBatch().setCodRamo("");
        ws.getMatrElabBatch().setDtUltElab(0);
        ws.getMatrElabBatch().setDsOperSql(Types.SPACE_CHAR);
        ws.getMatrElabBatch().setDsVer(0);
        ws.getMatrElabBatch().setDsTsCptz(0);
        ws.getMatrElabBatch().setDsUtente("");
        ws.getMatrElabBatch().setDsStatoElab(Types.SPACE_CHAR);
    }
}
