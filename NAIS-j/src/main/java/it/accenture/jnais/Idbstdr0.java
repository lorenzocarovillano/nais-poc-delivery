package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.TitRatDao;
import it.accenture.jnais.commons.data.to.ITitRat;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbstdr0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.TdrDtApplzMora;
import it.accenture.jnais.ws.redefines.TdrDtEndCop;
import it.accenture.jnais.ws.redefines.TdrDtEsiTit;
import it.accenture.jnais.ws.redefines.TdrDtIniCop;
import it.accenture.jnais.ws.redefines.TdrFraz;
import it.accenture.jnais.ws.redefines.TdrIdMoviChiu;
import it.accenture.jnais.ws.redefines.TdrIdRappAna;
import it.accenture.jnais.ws.redefines.TdrIdRappRete;
import it.accenture.jnais.ws.redefines.TdrImpAder;
import it.accenture.jnais.ws.redefines.TdrImpAz;
import it.accenture.jnais.ws.redefines.TdrImpPag;
import it.accenture.jnais.ws.redefines.TdrImpTfr;
import it.accenture.jnais.ws.redefines.TdrImpTfrStrc;
import it.accenture.jnais.ws.redefines.TdrImpTrasfe;
import it.accenture.jnais.ws.redefines.TdrImpVolo;
import it.accenture.jnais.ws.redefines.TdrProgTit;
import it.accenture.jnais.ws.redefines.TdrTotAcqExp;
import it.accenture.jnais.ws.redefines.TdrTotCarAcq;
import it.accenture.jnais.ws.redefines.TdrTotCarGest;
import it.accenture.jnais.ws.redefines.TdrTotCarIas;
import it.accenture.jnais.ws.redefines.TdrTotCarInc;
import it.accenture.jnais.ws.redefines.TdrTotCnbtAntirac;
import it.accenture.jnais.ws.redefines.TdrTotCommisInter;
import it.accenture.jnais.ws.redefines.TdrTotDir;
import it.accenture.jnais.ws.redefines.TdrTotIntrFraz;
import it.accenture.jnais.ws.redefines.TdrTotIntrMora;
import it.accenture.jnais.ws.redefines.TdrTotIntrPrest;
import it.accenture.jnais.ws.redefines.TdrTotIntrRetdt;
import it.accenture.jnais.ws.redefines.TdrTotIntrRiat;
import it.accenture.jnais.ws.redefines.TdrTotManfeeAntic;
import it.accenture.jnais.ws.redefines.TdrTotManfeeRec;
import it.accenture.jnais.ws.redefines.TdrTotManfeeRicor;
import it.accenture.jnais.ws.redefines.TdrTotPreNet;
import it.accenture.jnais.ws.redefines.TdrTotPrePpIas;
import it.accenture.jnais.ws.redefines.TdrTotPreSoloRsh;
import it.accenture.jnais.ws.redefines.TdrTotPreTot;
import it.accenture.jnais.ws.redefines.TdrTotProvAcq1aa;
import it.accenture.jnais.ws.redefines.TdrTotProvAcq2aa;
import it.accenture.jnais.ws.redefines.TdrTotProvDaRec;
import it.accenture.jnais.ws.redefines.TdrTotProvInc;
import it.accenture.jnais.ws.redefines.TdrTotProvRicor;
import it.accenture.jnais.ws.redefines.TdrTotRemunAss;
import it.accenture.jnais.ws.redefines.TdrTotSoprAlt;
import it.accenture.jnais.ws.redefines.TdrTotSoprProf;
import it.accenture.jnais.ws.redefines.TdrTotSoprSan;
import it.accenture.jnais.ws.redefines.TdrTotSoprSpo;
import it.accenture.jnais.ws.redefines.TdrTotSoprTec;
import it.accenture.jnais.ws.redefines.TdrTotSpeAge;
import it.accenture.jnais.ws.redefines.TdrTotSpeMed;
import it.accenture.jnais.ws.redefines.TdrTotTax;
import it.accenture.jnais.ws.TitRatIdbstdr0;

/**Original name: IDBSTDR0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  05 DIC 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbstdr0 extends Program implements ITitRat {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private TitRatDao titRatDao = new TitRatDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbstdr0Data ws = new Idbstdr0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: TIT-RAT
    private TitRatIdbstdr0 titRat;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSTDR0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, TitRatIdbstdr0 titRat) {
        this.idsv0003 = idsv0003;
        this.titRat = titRat;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbstdr0 getInstance() {
        return ((Idbstdr0)Programs.getInstance(Idbstdr0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSTDR0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSTDR0");
        // COB_CODE: MOVE 'TIT_RAT' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("TIT_RAT");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_TIT
        //                ,PROG_TIT
        //                ,TP_PRE_TIT
        //                ,TP_STAT_TIT
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,IMP_PAG
        //                ,FL_SOLL
        //                ,FRAZ
        //                ,DT_APPLZ_MORA
        //                ,FL_MORA
        //                ,ID_RAPP_RETE
        //                ,ID_RAPP_ANA
        //                ,COD_DVS
        //                ,DT_EMIS_TIT
        //                ,DT_ESI_TIT
        //                ,TOT_PRE_NET
        //                ,TOT_INTR_FRAZ
        //                ,TOT_INTR_MORA
        //                ,TOT_INTR_PREST
        //                ,TOT_INTR_RETDT
        //                ,TOT_INTR_RIAT
        //                ,TOT_DIR
        //                ,TOT_SPE_MED
        //                ,TOT_SPE_AGE
        //                ,TOT_TAX
        //                ,TOT_SOPR_SAN
        //                ,TOT_SOPR_TEC
        //                ,TOT_SOPR_SPO
        //                ,TOT_SOPR_PROF
        //                ,TOT_SOPR_ALT
        //                ,TOT_PRE_TOT
        //                ,TOT_PRE_PP_IAS
        //                ,TOT_CAR_IAS
        //                ,TOT_PRE_SOLO_RSH
        //                ,TOT_PROV_ACQ_1AA
        //                ,TOT_PROV_ACQ_2AA
        //                ,TOT_PROV_RICOR
        //                ,TOT_PROV_INC
        //                ,TOT_PROV_DA_REC
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,TOT_CAR_ACQ
        //                ,TOT_CAR_GEST
        //                ,TOT_CAR_INC
        //                ,TOT_MANFEE_ANTIC
        //                ,TOT_MANFEE_RICOR
        //                ,TOT_MANFEE_REC
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,TOT_ACQ_EXP
        //                ,TOT_REMUN_ASS
        //                ,TOT_COMMIS_INTER
        //                ,TOT_CNBT_ANTIRAC
        //                ,FL_INC_AUTOGEN
        //             INTO
        //                :TDR-ID-TIT-RAT
        //               ,:TDR-ID-OGG
        //               ,:TDR-TP-OGG
        //               ,:TDR-ID-MOVI-CRZ
        //               ,:TDR-ID-MOVI-CHIU
        //                :IND-TDR-ID-MOVI-CHIU
        //               ,:TDR-DT-INI-EFF-DB
        //               ,:TDR-DT-END-EFF-DB
        //               ,:TDR-COD-COMP-ANIA
        //               ,:TDR-TP-TIT
        //               ,:TDR-PROG-TIT
        //                :IND-TDR-PROG-TIT
        //               ,:TDR-TP-PRE-TIT
        //               ,:TDR-TP-STAT-TIT
        //               ,:TDR-DT-INI-COP-DB
        //                :IND-TDR-DT-INI-COP
        //               ,:TDR-DT-END-COP-DB
        //                :IND-TDR-DT-END-COP
        //               ,:TDR-IMP-PAG
        //                :IND-TDR-IMP-PAG
        //               ,:TDR-FL-SOLL
        //                :IND-TDR-FL-SOLL
        //               ,:TDR-FRAZ
        //                :IND-TDR-FRAZ
        //               ,:TDR-DT-APPLZ-MORA-DB
        //                :IND-TDR-DT-APPLZ-MORA
        //               ,:TDR-FL-MORA
        //                :IND-TDR-FL-MORA
        //               ,:TDR-ID-RAPP-RETE
        //                :IND-TDR-ID-RAPP-RETE
        //               ,:TDR-ID-RAPP-ANA
        //                :IND-TDR-ID-RAPP-ANA
        //               ,:TDR-COD-DVS
        //                :IND-TDR-COD-DVS
        //               ,:TDR-DT-EMIS-TIT-DB
        //               ,:TDR-DT-ESI-TIT-DB
        //                :IND-TDR-DT-ESI-TIT
        //               ,:TDR-TOT-PRE-NET
        //                :IND-TDR-TOT-PRE-NET
        //               ,:TDR-TOT-INTR-FRAZ
        //                :IND-TDR-TOT-INTR-FRAZ
        //               ,:TDR-TOT-INTR-MORA
        //                :IND-TDR-TOT-INTR-MORA
        //               ,:TDR-TOT-INTR-PREST
        //                :IND-TDR-TOT-INTR-PREST
        //               ,:TDR-TOT-INTR-RETDT
        //                :IND-TDR-TOT-INTR-RETDT
        //               ,:TDR-TOT-INTR-RIAT
        //                :IND-TDR-TOT-INTR-RIAT
        //               ,:TDR-TOT-DIR
        //                :IND-TDR-TOT-DIR
        //               ,:TDR-TOT-SPE-MED
        //                :IND-TDR-TOT-SPE-MED
        //               ,:TDR-TOT-SPE-AGE
        //                :IND-TDR-TOT-SPE-AGE
        //               ,:TDR-TOT-TAX
        //                :IND-TDR-TOT-TAX
        //               ,:TDR-TOT-SOPR-SAN
        //                :IND-TDR-TOT-SOPR-SAN
        //               ,:TDR-TOT-SOPR-TEC
        //                :IND-TDR-TOT-SOPR-TEC
        //               ,:TDR-TOT-SOPR-SPO
        //                :IND-TDR-TOT-SOPR-SPO
        //               ,:TDR-TOT-SOPR-PROF
        //                :IND-TDR-TOT-SOPR-PROF
        //               ,:TDR-TOT-SOPR-ALT
        //                :IND-TDR-TOT-SOPR-ALT
        //               ,:TDR-TOT-PRE-TOT
        //                :IND-TDR-TOT-PRE-TOT
        //               ,:TDR-TOT-PRE-PP-IAS
        //                :IND-TDR-TOT-PRE-PP-IAS
        //               ,:TDR-TOT-CAR-IAS
        //                :IND-TDR-TOT-CAR-IAS
        //               ,:TDR-TOT-PRE-SOLO-RSH
        //                :IND-TDR-TOT-PRE-SOLO-RSH
        //               ,:TDR-TOT-PROV-ACQ-1AA
        //                :IND-TDR-TOT-PROV-ACQ-1AA
        //               ,:TDR-TOT-PROV-ACQ-2AA
        //                :IND-TDR-TOT-PROV-ACQ-2AA
        //               ,:TDR-TOT-PROV-RICOR
        //                :IND-TDR-TOT-PROV-RICOR
        //               ,:TDR-TOT-PROV-INC
        //                :IND-TDR-TOT-PROV-INC
        //               ,:TDR-TOT-PROV-DA-REC
        //                :IND-TDR-TOT-PROV-DA-REC
        //               ,:TDR-IMP-AZ
        //                :IND-TDR-IMP-AZ
        //               ,:TDR-IMP-ADER
        //                :IND-TDR-IMP-ADER
        //               ,:TDR-IMP-TFR
        //                :IND-TDR-IMP-TFR
        //               ,:TDR-IMP-VOLO
        //                :IND-TDR-IMP-VOLO
        //               ,:TDR-FL-VLDT-TIT
        //                :IND-TDR-FL-VLDT-TIT
        //               ,:TDR-TOT-CAR-ACQ
        //                :IND-TDR-TOT-CAR-ACQ
        //               ,:TDR-TOT-CAR-GEST
        //                :IND-TDR-TOT-CAR-GEST
        //               ,:TDR-TOT-CAR-INC
        //                :IND-TDR-TOT-CAR-INC
        //               ,:TDR-TOT-MANFEE-ANTIC
        //                :IND-TDR-TOT-MANFEE-ANTIC
        //               ,:TDR-TOT-MANFEE-RICOR
        //                :IND-TDR-TOT-MANFEE-RICOR
        //               ,:TDR-TOT-MANFEE-REC
        //                :IND-TDR-TOT-MANFEE-REC
        //               ,:TDR-DS-RIGA
        //               ,:TDR-DS-OPER-SQL
        //               ,:TDR-DS-VER
        //               ,:TDR-DS-TS-INI-CPTZ
        //               ,:TDR-DS-TS-END-CPTZ
        //               ,:TDR-DS-UTENTE
        //               ,:TDR-DS-STATO-ELAB
        //               ,:TDR-IMP-TRASFE
        //                :IND-TDR-IMP-TRASFE
        //               ,:TDR-IMP-TFR-STRC
        //                :IND-TDR-IMP-TFR-STRC
        //               ,:TDR-TOT-ACQ-EXP
        //                :IND-TDR-TOT-ACQ-EXP
        //               ,:TDR-TOT-REMUN-ASS
        //                :IND-TDR-TOT-REMUN-ASS
        //               ,:TDR-TOT-COMMIS-INTER
        //                :IND-TDR-TOT-COMMIS-INTER
        //               ,:TDR-TOT-CNBT-ANTIRAC
        //                :IND-TDR-TOT-CNBT-ANTIRAC
        //               ,:TDR-FL-INC-AUTOGEN
        //                :IND-TDR-FL-INC-AUTOGEN
        //             FROM TIT_RAT
        //             WHERE     DS_RIGA = :TDR-DS-RIGA
        //           END-EXEC.
        titRatDao.selectByTdrDsRiga(titRat.getTdrDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO TIT_RAT
            //                  (
            //                     ID_TIT_RAT
            //                    ,ID_OGG
            //                    ,TP_OGG
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,TP_TIT
            //                    ,PROG_TIT
            //                    ,TP_PRE_TIT
            //                    ,TP_STAT_TIT
            //                    ,DT_INI_COP
            //                    ,DT_END_COP
            //                    ,IMP_PAG
            //                    ,FL_SOLL
            //                    ,FRAZ
            //                    ,DT_APPLZ_MORA
            //                    ,FL_MORA
            //                    ,ID_RAPP_RETE
            //                    ,ID_RAPP_ANA
            //                    ,COD_DVS
            //                    ,DT_EMIS_TIT
            //                    ,DT_ESI_TIT
            //                    ,TOT_PRE_NET
            //                    ,TOT_INTR_FRAZ
            //                    ,TOT_INTR_MORA
            //                    ,TOT_INTR_PREST
            //                    ,TOT_INTR_RETDT
            //                    ,TOT_INTR_RIAT
            //                    ,TOT_DIR
            //                    ,TOT_SPE_MED
            //                    ,TOT_SPE_AGE
            //                    ,TOT_TAX
            //                    ,TOT_SOPR_SAN
            //                    ,TOT_SOPR_TEC
            //                    ,TOT_SOPR_SPO
            //                    ,TOT_SOPR_PROF
            //                    ,TOT_SOPR_ALT
            //                    ,TOT_PRE_TOT
            //                    ,TOT_PRE_PP_IAS
            //                    ,TOT_CAR_IAS
            //                    ,TOT_PRE_SOLO_RSH
            //                    ,TOT_PROV_ACQ_1AA
            //                    ,TOT_PROV_ACQ_2AA
            //                    ,TOT_PROV_RICOR
            //                    ,TOT_PROV_INC
            //                    ,TOT_PROV_DA_REC
            //                    ,IMP_AZ
            //                    ,IMP_ADER
            //                    ,IMP_TFR
            //                    ,IMP_VOLO
            //                    ,FL_VLDT_TIT
            //                    ,TOT_CAR_ACQ
            //                    ,TOT_CAR_GEST
            //                    ,TOT_CAR_INC
            //                    ,TOT_MANFEE_ANTIC
            //                    ,TOT_MANFEE_RICOR
            //                    ,TOT_MANFEE_REC
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,IMP_TRASFE
            //                    ,IMP_TFR_STRC
            //                    ,TOT_ACQ_EXP
            //                    ,TOT_REMUN_ASS
            //                    ,TOT_COMMIS_INTER
            //                    ,TOT_CNBT_ANTIRAC
            //                    ,FL_INC_AUTOGEN
            //                  )
            //              VALUES
            //                  (
            //                    :TDR-ID-TIT-RAT
            //                    ,:TDR-ID-OGG
            //                    ,:TDR-TP-OGG
            //                    ,:TDR-ID-MOVI-CRZ
            //                    ,:TDR-ID-MOVI-CHIU
            //                     :IND-TDR-ID-MOVI-CHIU
            //                    ,:TDR-DT-INI-EFF-DB
            //                    ,:TDR-DT-END-EFF-DB
            //                    ,:TDR-COD-COMP-ANIA
            //                    ,:TDR-TP-TIT
            //                    ,:TDR-PROG-TIT
            //                     :IND-TDR-PROG-TIT
            //                    ,:TDR-TP-PRE-TIT
            //                    ,:TDR-TP-STAT-TIT
            //                    ,:TDR-DT-INI-COP-DB
            //                     :IND-TDR-DT-INI-COP
            //                    ,:TDR-DT-END-COP-DB
            //                     :IND-TDR-DT-END-COP
            //                    ,:TDR-IMP-PAG
            //                     :IND-TDR-IMP-PAG
            //                    ,:TDR-FL-SOLL
            //                     :IND-TDR-FL-SOLL
            //                    ,:TDR-FRAZ
            //                     :IND-TDR-FRAZ
            //                    ,:TDR-DT-APPLZ-MORA-DB
            //                     :IND-TDR-DT-APPLZ-MORA
            //                    ,:TDR-FL-MORA
            //                     :IND-TDR-FL-MORA
            //                    ,:TDR-ID-RAPP-RETE
            //                     :IND-TDR-ID-RAPP-RETE
            //                    ,:TDR-ID-RAPP-ANA
            //                     :IND-TDR-ID-RAPP-ANA
            //                    ,:TDR-COD-DVS
            //                     :IND-TDR-COD-DVS
            //                    ,:TDR-DT-EMIS-TIT-DB
            //                    ,:TDR-DT-ESI-TIT-DB
            //                     :IND-TDR-DT-ESI-TIT
            //                    ,:TDR-TOT-PRE-NET
            //                     :IND-TDR-TOT-PRE-NET
            //                    ,:TDR-TOT-INTR-FRAZ
            //                     :IND-TDR-TOT-INTR-FRAZ
            //                    ,:TDR-TOT-INTR-MORA
            //                     :IND-TDR-TOT-INTR-MORA
            //                    ,:TDR-TOT-INTR-PREST
            //                     :IND-TDR-TOT-INTR-PREST
            //                    ,:TDR-TOT-INTR-RETDT
            //                     :IND-TDR-TOT-INTR-RETDT
            //                    ,:TDR-TOT-INTR-RIAT
            //                     :IND-TDR-TOT-INTR-RIAT
            //                    ,:TDR-TOT-DIR
            //                     :IND-TDR-TOT-DIR
            //                    ,:TDR-TOT-SPE-MED
            //                     :IND-TDR-TOT-SPE-MED
            //                    ,:TDR-TOT-SPE-AGE
            //                     :IND-TDR-TOT-SPE-AGE
            //                    ,:TDR-TOT-TAX
            //                     :IND-TDR-TOT-TAX
            //                    ,:TDR-TOT-SOPR-SAN
            //                     :IND-TDR-TOT-SOPR-SAN
            //                    ,:TDR-TOT-SOPR-TEC
            //                     :IND-TDR-TOT-SOPR-TEC
            //                    ,:TDR-TOT-SOPR-SPO
            //                     :IND-TDR-TOT-SOPR-SPO
            //                    ,:TDR-TOT-SOPR-PROF
            //                     :IND-TDR-TOT-SOPR-PROF
            //                    ,:TDR-TOT-SOPR-ALT
            //                     :IND-TDR-TOT-SOPR-ALT
            //                    ,:TDR-TOT-PRE-TOT
            //                     :IND-TDR-TOT-PRE-TOT
            //                    ,:TDR-TOT-PRE-PP-IAS
            //                     :IND-TDR-TOT-PRE-PP-IAS
            //                    ,:TDR-TOT-CAR-IAS
            //                     :IND-TDR-TOT-CAR-IAS
            //                    ,:TDR-TOT-PRE-SOLO-RSH
            //                     :IND-TDR-TOT-PRE-SOLO-RSH
            //                    ,:TDR-TOT-PROV-ACQ-1AA
            //                     :IND-TDR-TOT-PROV-ACQ-1AA
            //                    ,:TDR-TOT-PROV-ACQ-2AA
            //                     :IND-TDR-TOT-PROV-ACQ-2AA
            //                    ,:TDR-TOT-PROV-RICOR
            //                     :IND-TDR-TOT-PROV-RICOR
            //                    ,:TDR-TOT-PROV-INC
            //                     :IND-TDR-TOT-PROV-INC
            //                    ,:TDR-TOT-PROV-DA-REC
            //                     :IND-TDR-TOT-PROV-DA-REC
            //                    ,:TDR-IMP-AZ
            //                     :IND-TDR-IMP-AZ
            //                    ,:TDR-IMP-ADER
            //                     :IND-TDR-IMP-ADER
            //                    ,:TDR-IMP-TFR
            //                     :IND-TDR-IMP-TFR
            //                    ,:TDR-IMP-VOLO
            //                     :IND-TDR-IMP-VOLO
            //                    ,:TDR-FL-VLDT-TIT
            //                     :IND-TDR-FL-VLDT-TIT
            //                    ,:TDR-TOT-CAR-ACQ
            //                     :IND-TDR-TOT-CAR-ACQ
            //                    ,:TDR-TOT-CAR-GEST
            //                     :IND-TDR-TOT-CAR-GEST
            //                    ,:TDR-TOT-CAR-INC
            //                     :IND-TDR-TOT-CAR-INC
            //                    ,:TDR-TOT-MANFEE-ANTIC
            //                     :IND-TDR-TOT-MANFEE-ANTIC
            //                    ,:TDR-TOT-MANFEE-RICOR
            //                     :IND-TDR-TOT-MANFEE-RICOR
            //                    ,:TDR-TOT-MANFEE-REC
            //                     :IND-TDR-TOT-MANFEE-REC
            //                    ,:TDR-DS-RIGA
            //                    ,:TDR-DS-OPER-SQL
            //                    ,:TDR-DS-VER
            //                    ,:TDR-DS-TS-INI-CPTZ
            //                    ,:TDR-DS-TS-END-CPTZ
            //                    ,:TDR-DS-UTENTE
            //                    ,:TDR-DS-STATO-ELAB
            //                    ,:TDR-IMP-TRASFE
            //                     :IND-TDR-IMP-TRASFE
            //                    ,:TDR-IMP-TFR-STRC
            //                     :IND-TDR-IMP-TFR-STRC
            //                    ,:TDR-TOT-ACQ-EXP
            //                     :IND-TDR-TOT-ACQ-EXP
            //                    ,:TDR-TOT-REMUN-ASS
            //                     :IND-TDR-TOT-REMUN-ASS
            //                    ,:TDR-TOT-COMMIS-INTER
            //                     :IND-TDR-TOT-COMMIS-INTER
            //                    ,:TDR-TOT-CNBT-ANTIRAC
            //                     :IND-TDR-TOT-CNBT-ANTIRAC
            //                    ,:TDR-FL-INC-AUTOGEN
            //                     :IND-TDR-FL-INC-AUTOGEN
            //                  )
            //           END-EXEC
            titRatDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE TIT_RAT SET
        //                   ID_TIT_RAT             =
        //                :TDR-ID-TIT-RAT
        //                  ,ID_OGG                 =
        //                :TDR-ID-OGG
        //                  ,TP_OGG                 =
        //                :TDR-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :TDR-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :TDR-ID-MOVI-CHIU
        //                                       :IND-TDR-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :TDR-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :TDR-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :TDR-COD-COMP-ANIA
        //                  ,TP_TIT                 =
        //                :TDR-TP-TIT
        //                  ,PROG_TIT               =
        //                :TDR-PROG-TIT
        //                                       :IND-TDR-PROG-TIT
        //                  ,TP_PRE_TIT             =
        //                :TDR-TP-PRE-TIT
        //                  ,TP_STAT_TIT            =
        //                :TDR-TP-STAT-TIT
        //                  ,DT_INI_COP             =
        //           :TDR-DT-INI-COP-DB
        //                                       :IND-TDR-DT-INI-COP
        //                  ,DT_END_COP             =
        //           :TDR-DT-END-COP-DB
        //                                       :IND-TDR-DT-END-COP
        //                  ,IMP_PAG                =
        //                :TDR-IMP-PAG
        //                                       :IND-TDR-IMP-PAG
        //                  ,FL_SOLL                =
        //                :TDR-FL-SOLL
        //                                       :IND-TDR-FL-SOLL
        //                  ,FRAZ                   =
        //                :TDR-FRAZ
        //                                       :IND-TDR-FRAZ
        //                  ,DT_APPLZ_MORA          =
        //           :TDR-DT-APPLZ-MORA-DB
        //                                       :IND-TDR-DT-APPLZ-MORA
        //                  ,FL_MORA                =
        //                :TDR-FL-MORA
        //                                       :IND-TDR-FL-MORA
        //                  ,ID_RAPP_RETE           =
        //                :TDR-ID-RAPP-RETE
        //                                       :IND-TDR-ID-RAPP-RETE
        //                  ,ID_RAPP_ANA            =
        //                :TDR-ID-RAPP-ANA
        //                                       :IND-TDR-ID-RAPP-ANA
        //                  ,COD_DVS                =
        //                :TDR-COD-DVS
        //                                       :IND-TDR-COD-DVS
        //                  ,DT_EMIS_TIT            =
        //           :TDR-DT-EMIS-TIT-DB
        //                  ,DT_ESI_TIT             =
        //           :TDR-DT-ESI-TIT-DB
        //                                       :IND-TDR-DT-ESI-TIT
        //                  ,TOT_PRE_NET            =
        //                :TDR-TOT-PRE-NET
        //                                       :IND-TDR-TOT-PRE-NET
        //                  ,TOT_INTR_FRAZ          =
        //                :TDR-TOT-INTR-FRAZ
        //                                       :IND-TDR-TOT-INTR-FRAZ
        //                  ,TOT_INTR_MORA          =
        //                :TDR-TOT-INTR-MORA
        //                                       :IND-TDR-TOT-INTR-MORA
        //                  ,TOT_INTR_PREST         =
        //                :TDR-TOT-INTR-PREST
        //                                       :IND-TDR-TOT-INTR-PREST
        //                  ,TOT_INTR_RETDT         =
        //                :TDR-TOT-INTR-RETDT
        //                                       :IND-TDR-TOT-INTR-RETDT
        //                  ,TOT_INTR_RIAT          =
        //                :TDR-TOT-INTR-RIAT
        //                                       :IND-TDR-TOT-INTR-RIAT
        //                  ,TOT_DIR                =
        //                :TDR-TOT-DIR
        //                                       :IND-TDR-TOT-DIR
        //                  ,TOT_SPE_MED            =
        //                :TDR-TOT-SPE-MED
        //                                       :IND-TDR-TOT-SPE-MED
        //                  ,TOT_SPE_AGE            =
        //                :TDR-TOT-SPE-AGE
        //                                       :IND-TDR-TOT-SPE-AGE
        //                  ,TOT_TAX                =
        //                :TDR-TOT-TAX
        //                                       :IND-TDR-TOT-TAX
        //                  ,TOT_SOPR_SAN           =
        //                :TDR-TOT-SOPR-SAN
        //                                       :IND-TDR-TOT-SOPR-SAN
        //                  ,TOT_SOPR_TEC           =
        //                :TDR-TOT-SOPR-TEC
        //                                       :IND-TDR-TOT-SOPR-TEC
        //                  ,TOT_SOPR_SPO           =
        //                :TDR-TOT-SOPR-SPO
        //                                       :IND-TDR-TOT-SOPR-SPO
        //                  ,TOT_SOPR_PROF          =
        //                :TDR-TOT-SOPR-PROF
        //                                       :IND-TDR-TOT-SOPR-PROF
        //                  ,TOT_SOPR_ALT           =
        //                :TDR-TOT-SOPR-ALT
        //                                       :IND-TDR-TOT-SOPR-ALT
        //                  ,TOT_PRE_TOT            =
        //                :TDR-TOT-PRE-TOT
        //                                       :IND-TDR-TOT-PRE-TOT
        //                  ,TOT_PRE_PP_IAS         =
        //                :TDR-TOT-PRE-PP-IAS
        //                                       :IND-TDR-TOT-PRE-PP-IAS
        //                  ,TOT_CAR_IAS            =
        //                :TDR-TOT-CAR-IAS
        //                                       :IND-TDR-TOT-CAR-IAS
        //                  ,TOT_PRE_SOLO_RSH       =
        //                :TDR-TOT-PRE-SOLO-RSH
        //                                       :IND-TDR-TOT-PRE-SOLO-RSH
        //                  ,TOT_PROV_ACQ_1AA       =
        //                :TDR-TOT-PROV-ACQ-1AA
        //                                       :IND-TDR-TOT-PROV-ACQ-1AA
        //                  ,TOT_PROV_ACQ_2AA       =
        //                :TDR-TOT-PROV-ACQ-2AA
        //                                       :IND-TDR-TOT-PROV-ACQ-2AA
        //                  ,TOT_PROV_RICOR         =
        //                :TDR-TOT-PROV-RICOR
        //                                       :IND-TDR-TOT-PROV-RICOR
        //                  ,TOT_PROV_INC           =
        //                :TDR-TOT-PROV-INC
        //                                       :IND-TDR-TOT-PROV-INC
        //                  ,TOT_PROV_DA_REC        =
        //                :TDR-TOT-PROV-DA-REC
        //                                       :IND-TDR-TOT-PROV-DA-REC
        //                  ,IMP_AZ                 =
        //                :TDR-IMP-AZ
        //                                       :IND-TDR-IMP-AZ
        //                  ,IMP_ADER               =
        //                :TDR-IMP-ADER
        //                                       :IND-TDR-IMP-ADER
        //                  ,IMP_TFR                =
        //                :TDR-IMP-TFR
        //                                       :IND-TDR-IMP-TFR
        //                  ,IMP_VOLO               =
        //                :TDR-IMP-VOLO
        //                                       :IND-TDR-IMP-VOLO
        //                  ,FL_VLDT_TIT            =
        //                :TDR-FL-VLDT-TIT
        //                                       :IND-TDR-FL-VLDT-TIT
        //                  ,TOT_CAR_ACQ            =
        //                :TDR-TOT-CAR-ACQ
        //                                       :IND-TDR-TOT-CAR-ACQ
        //                  ,TOT_CAR_GEST           =
        //                :TDR-TOT-CAR-GEST
        //                                       :IND-TDR-TOT-CAR-GEST
        //                  ,TOT_CAR_INC            =
        //                :TDR-TOT-CAR-INC
        //                                       :IND-TDR-TOT-CAR-INC
        //                  ,TOT_MANFEE_ANTIC       =
        //                :TDR-TOT-MANFEE-ANTIC
        //                                       :IND-TDR-TOT-MANFEE-ANTIC
        //                  ,TOT_MANFEE_RICOR       =
        //                :TDR-TOT-MANFEE-RICOR
        //                                       :IND-TDR-TOT-MANFEE-RICOR
        //                  ,TOT_MANFEE_REC         =
        //                :TDR-TOT-MANFEE-REC
        //                                       :IND-TDR-TOT-MANFEE-REC
        //                  ,DS_RIGA                =
        //                :TDR-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :TDR-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :TDR-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :TDR-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :TDR-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :TDR-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :TDR-DS-STATO-ELAB
        //                  ,IMP_TRASFE             =
        //                :TDR-IMP-TRASFE
        //                                       :IND-TDR-IMP-TRASFE
        //                  ,IMP_TFR_STRC           =
        //                :TDR-IMP-TFR-STRC
        //                                       :IND-TDR-IMP-TFR-STRC
        //                  ,TOT_ACQ_EXP            =
        //                :TDR-TOT-ACQ-EXP
        //                                       :IND-TDR-TOT-ACQ-EXP
        //                  ,TOT_REMUN_ASS          =
        //                :TDR-TOT-REMUN-ASS
        //                                       :IND-TDR-TOT-REMUN-ASS
        //                  ,TOT_COMMIS_INTER       =
        //                :TDR-TOT-COMMIS-INTER
        //                                       :IND-TDR-TOT-COMMIS-INTER
        //                  ,TOT_CNBT_ANTIRAC       =
        //                :TDR-TOT-CNBT-ANTIRAC
        //                                       :IND-TDR-TOT-CNBT-ANTIRAC
        //                  ,FL_INC_AUTOGEN         =
        //                :TDR-FL-INC-AUTOGEN
        //                                       :IND-TDR-FL-INC-AUTOGEN
        //                WHERE     DS_RIGA = :TDR-DS-RIGA
        //           END-EXEC.
        titRatDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM TIT_RAT
        //                WHERE     DS_RIGA = :TDR-DS-RIGA
        //           END-EXEC.
        titRatDao.deleteByTdrDsRiga(titRat.getTdrDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-TDR CURSOR FOR
        //              SELECT
        //                     ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_TIT
        //                    ,PROG_TIT
        //                    ,TP_PRE_TIT
        //                    ,TP_STAT_TIT
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,IMP_PAG
        //                    ,FL_SOLL
        //                    ,FRAZ
        //                    ,DT_APPLZ_MORA
        //                    ,FL_MORA
        //                    ,ID_RAPP_RETE
        //                    ,ID_RAPP_ANA
        //                    ,COD_DVS
        //                    ,DT_EMIS_TIT
        //                    ,DT_ESI_TIT
        //                    ,TOT_PRE_NET
        //                    ,TOT_INTR_FRAZ
        //                    ,TOT_INTR_MORA
        //                    ,TOT_INTR_PREST
        //                    ,TOT_INTR_RETDT
        //                    ,TOT_INTR_RIAT
        //                    ,TOT_DIR
        //                    ,TOT_SPE_MED
        //                    ,TOT_SPE_AGE
        //                    ,TOT_TAX
        //                    ,TOT_SOPR_SAN
        //                    ,TOT_SOPR_TEC
        //                    ,TOT_SOPR_SPO
        //                    ,TOT_SOPR_PROF
        //                    ,TOT_SOPR_ALT
        //                    ,TOT_PRE_TOT
        //                    ,TOT_PRE_PP_IAS
        //                    ,TOT_CAR_IAS
        //                    ,TOT_PRE_SOLO_RSH
        //                    ,TOT_PROV_ACQ_1AA
        //                    ,TOT_PROV_ACQ_2AA
        //                    ,TOT_PROV_RICOR
        //                    ,TOT_PROV_INC
        //                    ,TOT_PROV_DA_REC
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,TOT_CAR_ACQ
        //                    ,TOT_CAR_GEST
        //                    ,TOT_CAR_INC
        //                    ,TOT_MANFEE_ANTIC
        //                    ,TOT_MANFEE_RICOR
        //                    ,TOT_MANFEE_REC
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,TOT_ACQ_EXP
        //                    ,TOT_REMUN_ASS
        //                    ,TOT_COMMIS_INTER
        //                    ,TOT_CNBT_ANTIRAC
        //                    ,FL_INC_AUTOGEN
        //              FROM TIT_RAT
        //              WHERE     ID_TIT_RAT = :TDR-ID-TIT-RAT
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_TIT
        //                ,PROG_TIT
        //                ,TP_PRE_TIT
        //                ,TP_STAT_TIT
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,IMP_PAG
        //                ,FL_SOLL
        //                ,FRAZ
        //                ,DT_APPLZ_MORA
        //                ,FL_MORA
        //                ,ID_RAPP_RETE
        //                ,ID_RAPP_ANA
        //                ,COD_DVS
        //                ,DT_EMIS_TIT
        //                ,DT_ESI_TIT
        //                ,TOT_PRE_NET
        //                ,TOT_INTR_FRAZ
        //                ,TOT_INTR_MORA
        //                ,TOT_INTR_PREST
        //                ,TOT_INTR_RETDT
        //                ,TOT_INTR_RIAT
        //                ,TOT_DIR
        //                ,TOT_SPE_MED
        //                ,TOT_SPE_AGE
        //                ,TOT_TAX
        //                ,TOT_SOPR_SAN
        //                ,TOT_SOPR_TEC
        //                ,TOT_SOPR_SPO
        //                ,TOT_SOPR_PROF
        //                ,TOT_SOPR_ALT
        //                ,TOT_PRE_TOT
        //                ,TOT_PRE_PP_IAS
        //                ,TOT_CAR_IAS
        //                ,TOT_PRE_SOLO_RSH
        //                ,TOT_PROV_ACQ_1AA
        //                ,TOT_PROV_ACQ_2AA
        //                ,TOT_PROV_RICOR
        //                ,TOT_PROV_INC
        //                ,TOT_PROV_DA_REC
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,TOT_CAR_ACQ
        //                ,TOT_CAR_GEST
        //                ,TOT_CAR_INC
        //                ,TOT_MANFEE_ANTIC
        //                ,TOT_MANFEE_RICOR
        //                ,TOT_MANFEE_REC
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,TOT_ACQ_EXP
        //                ,TOT_REMUN_ASS
        //                ,TOT_COMMIS_INTER
        //                ,TOT_CNBT_ANTIRAC
        //                ,FL_INC_AUTOGEN
        //             INTO
        //                :TDR-ID-TIT-RAT
        //               ,:TDR-ID-OGG
        //               ,:TDR-TP-OGG
        //               ,:TDR-ID-MOVI-CRZ
        //               ,:TDR-ID-MOVI-CHIU
        //                :IND-TDR-ID-MOVI-CHIU
        //               ,:TDR-DT-INI-EFF-DB
        //               ,:TDR-DT-END-EFF-DB
        //               ,:TDR-COD-COMP-ANIA
        //               ,:TDR-TP-TIT
        //               ,:TDR-PROG-TIT
        //                :IND-TDR-PROG-TIT
        //               ,:TDR-TP-PRE-TIT
        //               ,:TDR-TP-STAT-TIT
        //               ,:TDR-DT-INI-COP-DB
        //                :IND-TDR-DT-INI-COP
        //               ,:TDR-DT-END-COP-DB
        //                :IND-TDR-DT-END-COP
        //               ,:TDR-IMP-PAG
        //                :IND-TDR-IMP-PAG
        //               ,:TDR-FL-SOLL
        //                :IND-TDR-FL-SOLL
        //               ,:TDR-FRAZ
        //                :IND-TDR-FRAZ
        //               ,:TDR-DT-APPLZ-MORA-DB
        //                :IND-TDR-DT-APPLZ-MORA
        //               ,:TDR-FL-MORA
        //                :IND-TDR-FL-MORA
        //               ,:TDR-ID-RAPP-RETE
        //                :IND-TDR-ID-RAPP-RETE
        //               ,:TDR-ID-RAPP-ANA
        //                :IND-TDR-ID-RAPP-ANA
        //               ,:TDR-COD-DVS
        //                :IND-TDR-COD-DVS
        //               ,:TDR-DT-EMIS-TIT-DB
        //               ,:TDR-DT-ESI-TIT-DB
        //                :IND-TDR-DT-ESI-TIT
        //               ,:TDR-TOT-PRE-NET
        //                :IND-TDR-TOT-PRE-NET
        //               ,:TDR-TOT-INTR-FRAZ
        //                :IND-TDR-TOT-INTR-FRAZ
        //               ,:TDR-TOT-INTR-MORA
        //                :IND-TDR-TOT-INTR-MORA
        //               ,:TDR-TOT-INTR-PREST
        //                :IND-TDR-TOT-INTR-PREST
        //               ,:TDR-TOT-INTR-RETDT
        //                :IND-TDR-TOT-INTR-RETDT
        //               ,:TDR-TOT-INTR-RIAT
        //                :IND-TDR-TOT-INTR-RIAT
        //               ,:TDR-TOT-DIR
        //                :IND-TDR-TOT-DIR
        //               ,:TDR-TOT-SPE-MED
        //                :IND-TDR-TOT-SPE-MED
        //               ,:TDR-TOT-SPE-AGE
        //                :IND-TDR-TOT-SPE-AGE
        //               ,:TDR-TOT-TAX
        //                :IND-TDR-TOT-TAX
        //               ,:TDR-TOT-SOPR-SAN
        //                :IND-TDR-TOT-SOPR-SAN
        //               ,:TDR-TOT-SOPR-TEC
        //                :IND-TDR-TOT-SOPR-TEC
        //               ,:TDR-TOT-SOPR-SPO
        //                :IND-TDR-TOT-SOPR-SPO
        //               ,:TDR-TOT-SOPR-PROF
        //                :IND-TDR-TOT-SOPR-PROF
        //               ,:TDR-TOT-SOPR-ALT
        //                :IND-TDR-TOT-SOPR-ALT
        //               ,:TDR-TOT-PRE-TOT
        //                :IND-TDR-TOT-PRE-TOT
        //               ,:TDR-TOT-PRE-PP-IAS
        //                :IND-TDR-TOT-PRE-PP-IAS
        //               ,:TDR-TOT-CAR-IAS
        //                :IND-TDR-TOT-CAR-IAS
        //               ,:TDR-TOT-PRE-SOLO-RSH
        //                :IND-TDR-TOT-PRE-SOLO-RSH
        //               ,:TDR-TOT-PROV-ACQ-1AA
        //                :IND-TDR-TOT-PROV-ACQ-1AA
        //               ,:TDR-TOT-PROV-ACQ-2AA
        //                :IND-TDR-TOT-PROV-ACQ-2AA
        //               ,:TDR-TOT-PROV-RICOR
        //                :IND-TDR-TOT-PROV-RICOR
        //               ,:TDR-TOT-PROV-INC
        //                :IND-TDR-TOT-PROV-INC
        //               ,:TDR-TOT-PROV-DA-REC
        //                :IND-TDR-TOT-PROV-DA-REC
        //               ,:TDR-IMP-AZ
        //                :IND-TDR-IMP-AZ
        //               ,:TDR-IMP-ADER
        //                :IND-TDR-IMP-ADER
        //               ,:TDR-IMP-TFR
        //                :IND-TDR-IMP-TFR
        //               ,:TDR-IMP-VOLO
        //                :IND-TDR-IMP-VOLO
        //               ,:TDR-FL-VLDT-TIT
        //                :IND-TDR-FL-VLDT-TIT
        //               ,:TDR-TOT-CAR-ACQ
        //                :IND-TDR-TOT-CAR-ACQ
        //               ,:TDR-TOT-CAR-GEST
        //                :IND-TDR-TOT-CAR-GEST
        //               ,:TDR-TOT-CAR-INC
        //                :IND-TDR-TOT-CAR-INC
        //               ,:TDR-TOT-MANFEE-ANTIC
        //                :IND-TDR-TOT-MANFEE-ANTIC
        //               ,:TDR-TOT-MANFEE-RICOR
        //                :IND-TDR-TOT-MANFEE-RICOR
        //               ,:TDR-TOT-MANFEE-REC
        //                :IND-TDR-TOT-MANFEE-REC
        //               ,:TDR-DS-RIGA
        //               ,:TDR-DS-OPER-SQL
        //               ,:TDR-DS-VER
        //               ,:TDR-DS-TS-INI-CPTZ
        //               ,:TDR-DS-TS-END-CPTZ
        //               ,:TDR-DS-UTENTE
        //               ,:TDR-DS-STATO-ELAB
        //               ,:TDR-IMP-TRASFE
        //                :IND-TDR-IMP-TRASFE
        //               ,:TDR-IMP-TFR-STRC
        //                :IND-TDR-IMP-TFR-STRC
        //               ,:TDR-TOT-ACQ-EXP
        //                :IND-TDR-TOT-ACQ-EXP
        //               ,:TDR-TOT-REMUN-ASS
        //                :IND-TDR-TOT-REMUN-ASS
        //               ,:TDR-TOT-COMMIS-INTER
        //                :IND-TDR-TOT-COMMIS-INTER
        //               ,:TDR-TOT-CNBT-ANTIRAC
        //                :IND-TDR-TOT-CNBT-ANTIRAC
        //               ,:TDR-FL-INC-AUTOGEN
        //                :IND-TDR-FL-INC-AUTOGEN
        //             FROM TIT_RAT
        //             WHERE     ID_TIT_RAT = :TDR-ID-TIT-RAT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        titRatDao.selectRec(titRat.getTdrIdTitRat(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE TIT_RAT SET
        //                   ID_TIT_RAT             =
        //                :TDR-ID-TIT-RAT
        //                  ,ID_OGG                 =
        //                :TDR-ID-OGG
        //                  ,TP_OGG                 =
        //                :TDR-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :TDR-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :TDR-ID-MOVI-CHIU
        //                                       :IND-TDR-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :TDR-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :TDR-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :TDR-COD-COMP-ANIA
        //                  ,TP_TIT                 =
        //                :TDR-TP-TIT
        //                  ,PROG_TIT               =
        //                :TDR-PROG-TIT
        //                                       :IND-TDR-PROG-TIT
        //                  ,TP_PRE_TIT             =
        //                :TDR-TP-PRE-TIT
        //                  ,TP_STAT_TIT            =
        //                :TDR-TP-STAT-TIT
        //                  ,DT_INI_COP             =
        //           :TDR-DT-INI-COP-DB
        //                                       :IND-TDR-DT-INI-COP
        //                  ,DT_END_COP             =
        //           :TDR-DT-END-COP-DB
        //                                       :IND-TDR-DT-END-COP
        //                  ,IMP_PAG                =
        //                :TDR-IMP-PAG
        //                                       :IND-TDR-IMP-PAG
        //                  ,FL_SOLL                =
        //                :TDR-FL-SOLL
        //                                       :IND-TDR-FL-SOLL
        //                  ,FRAZ                   =
        //                :TDR-FRAZ
        //                                       :IND-TDR-FRAZ
        //                  ,DT_APPLZ_MORA          =
        //           :TDR-DT-APPLZ-MORA-DB
        //                                       :IND-TDR-DT-APPLZ-MORA
        //                  ,FL_MORA                =
        //                :TDR-FL-MORA
        //                                       :IND-TDR-FL-MORA
        //                  ,ID_RAPP_RETE           =
        //                :TDR-ID-RAPP-RETE
        //                                       :IND-TDR-ID-RAPP-RETE
        //                  ,ID_RAPP_ANA            =
        //                :TDR-ID-RAPP-ANA
        //                                       :IND-TDR-ID-RAPP-ANA
        //                  ,COD_DVS                =
        //                :TDR-COD-DVS
        //                                       :IND-TDR-COD-DVS
        //                  ,DT_EMIS_TIT            =
        //           :TDR-DT-EMIS-TIT-DB
        //                  ,DT_ESI_TIT             =
        //           :TDR-DT-ESI-TIT-DB
        //                                       :IND-TDR-DT-ESI-TIT
        //                  ,TOT_PRE_NET            =
        //                :TDR-TOT-PRE-NET
        //                                       :IND-TDR-TOT-PRE-NET
        //                  ,TOT_INTR_FRAZ          =
        //                :TDR-TOT-INTR-FRAZ
        //                                       :IND-TDR-TOT-INTR-FRAZ
        //                  ,TOT_INTR_MORA          =
        //                :TDR-TOT-INTR-MORA
        //                                       :IND-TDR-TOT-INTR-MORA
        //                  ,TOT_INTR_PREST         =
        //                :TDR-TOT-INTR-PREST
        //                                       :IND-TDR-TOT-INTR-PREST
        //                  ,TOT_INTR_RETDT         =
        //                :TDR-TOT-INTR-RETDT
        //                                       :IND-TDR-TOT-INTR-RETDT
        //                  ,TOT_INTR_RIAT          =
        //                :TDR-TOT-INTR-RIAT
        //                                       :IND-TDR-TOT-INTR-RIAT
        //                  ,TOT_DIR                =
        //                :TDR-TOT-DIR
        //                                       :IND-TDR-TOT-DIR
        //                  ,TOT_SPE_MED            =
        //                :TDR-TOT-SPE-MED
        //                                       :IND-TDR-TOT-SPE-MED
        //                  ,TOT_SPE_AGE            =
        //                :TDR-TOT-SPE-AGE
        //                                       :IND-TDR-TOT-SPE-AGE
        //                  ,TOT_TAX                =
        //                :TDR-TOT-TAX
        //                                       :IND-TDR-TOT-TAX
        //                  ,TOT_SOPR_SAN           =
        //                :TDR-TOT-SOPR-SAN
        //                                       :IND-TDR-TOT-SOPR-SAN
        //                  ,TOT_SOPR_TEC           =
        //                :TDR-TOT-SOPR-TEC
        //                                       :IND-TDR-TOT-SOPR-TEC
        //                  ,TOT_SOPR_SPO           =
        //                :TDR-TOT-SOPR-SPO
        //                                       :IND-TDR-TOT-SOPR-SPO
        //                  ,TOT_SOPR_PROF          =
        //                :TDR-TOT-SOPR-PROF
        //                                       :IND-TDR-TOT-SOPR-PROF
        //                  ,TOT_SOPR_ALT           =
        //                :TDR-TOT-SOPR-ALT
        //                                       :IND-TDR-TOT-SOPR-ALT
        //                  ,TOT_PRE_TOT            =
        //                :TDR-TOT-PRE-TOT
        //                                       :IND-TDR-TOT-PRE-TOT
        //                  ,TOT_PRE_PP_IAS         =
        //                :TDR-TOT-PRE-PP-IAS
        //                                       :IND-TDR-TOT-PRE-PP-IAS
        //                  ,TOT_CAR_IAS            =
        //                :TDR-TOT-CAR-IAS
        //                                       :IND-TDR-TOT-CAR-IAS
        //                  ,TOT_PRE_SOLO_RSH       =
        //                :TDR-TOT-PRE-SOLO-RSH
        //                                       :IND-TDR-TOT-PRE-SOLO-RSH
        //                  ,TOT_PROV_ACQ_1AA       =
        //                :TDR-TOT-PROV-ACQ-1AA
        //                                       :IND-TDR-TOT-PROV-ACQ-1AA
        //                  ,TOT_PROV_ACQ_2AA       =
        //                :TDR-TOT-PROV-ACQ-2AA
        //                                       :IND-TDR-TOT-PROV-ACQ-2AA
        //                  ,TOT_PROV_RICOR         =
        //                :TDR-TOT-PROV-RICOR
        //                                       :IND-TDR-TOT-PROV-RICOR
        //                  ,TOT_PROV_INC           =
        //                :TDR-TOT-PROV-INC
        //                                       :IND-TDR-TOT-PROV-INC
        //                  ,TOT_PROV_DA_REC        =
        //                :TDR-TOT-PROV-DA-REC
        //                                       :IND-TDR-TOT-PROV-DA-REC
        //                  ,IMP_AZ                 =
        //                :TDR-IMP-AZ
        //                                       :IND-TDR-IMP-AZ
        //                  ,IMP_ADER               =
        //                :TDR-IMP-ADER
        //                                       :IND-TDR-IMP-ADER
        //                  ,IMP_TFR                =
        //                :TDR-IMP-TFR
        //                                       :IND-TDR-IMP-TFR
        //                  ,IMP_VOLO               =
        //                :TDR-IMP-VOLO
        //                                       :IND-TDR-IMP-VOLO
        //                  ,FL_VLDT_TIT            =
        //                :TDR-FL-VLDT-TIT
        //                                       :IND-TDR-FL-VLDT-TIT
        //                  ,TOT_CAR_ACQ            =
        //                :TDR-TOT-CAR-ACQ
        //                                       :IND-TDR-TOT-CAR-ACQ
        //                  ,TOT_CAR_GEST           =
        //                :TDR-TOT-CAR-GEST
        //                                       :IND-TDR-TOT-CAR-GEST
        //                  ,TOT_CAR_INC            =
        //                :TDR-TOT-CAR-INC
        //                                       :IND-TDR-TOT-CAR-INC
        //                  ,TOT_MANFEE_ANTIC       =
        //                :TDR-TOT-MANFEE-ANTIC
        //                                       :IND-TDR-TOT-MANFEE-ANTIC
        //                  ,TOT_MANFEE_RICOR       =
        //                :TDR-TOT-MANFEE-RICOR
        //                                       :IND-TDR-TOT-MANFEE-RICOR
        //                  ,TOT_MANFEE_REC         =
        //                :TDR-TOT-MANFEE-REC
        //                                       :IND-TDR-TOT-MANFEE-REC
        //                  ,DS_RIGA                =
        //                :TDR-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :TDR-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :TDR-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :TDR-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :TDR-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :TDR-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :TDR-DS-STATO-ELAB
        //                  ,IMP_TRASFE             =
        //                :TDR-IMP-TRASFE
        //                                       :IND-TDR-IMP-TRASFE
        //                  ,IMP_TFR_STRC           =
        //                :TDR-IMP-TFR-STRC
        //                                       :IND-TDR-IMP-TFR-STRC
        //                  ,TOT_ACQ_EXP            =
        //                :TDR-TOT-ACQ-EXP
        //                                       :IND-TDR-TOT-ACQ-EXP
        //                  ,TOT_REMUN_ASS          =
        //                :TDR-TOT-REMUN-ASS
        //                                       :IND-TDR-TOT-REMUN-ASS
        //                  ,TOT_COMMIS_INTER       =
        //                :TDR-TOT-COMMIS-INTER
        //                                       :IND-TDR-TOT-COMMIS-INTER
        //                  ,TOT_CNBT_ANTIRAC       =
        //                :TDR-TOT-CNBT-ANTIRAC
        //                                       :IND-TDR-TOT-CNBT-ANTIRAC
        //                  ,FL_INC_AUTOGEN         =
        //                :TDR-FL-INC-AUTOGEN
        //                                       :IND-TDR-FL-INC-AUTOGEN
        //                WHERE     DS_RIGA = :TDR-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        titRatDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-TDR
        //           END-EXEC.
        titRatDao.openCIdUpdEffTdr(titRat.getTdrIdTitRat(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-TDR
        //           END-EXEC.
        titRatDao.closeCIdUpdEffTdr();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-TDR
        //           INTO
        //                :TDR-ID-TIT-RAT
        //               ,:TDR-ID-OGG
        //               ,:TDR-TP-OGG
        //               ,:TDR-ID-MOVI-CRZ
        //               ,:TDR-ID-MOVI-CHIU
        //                :IND-TDR-ID-MOVI-CHIU
        //               ,:TDR-DT-INI-EFF-DB
        //               ,:TDR-DT-END-EFF-DB
        //               ,:TDR-COD-COMP-ANIA
        //               ,:TDR-TP-TIT
        //               ,:TDR-PROG-TIT
        //                :IND-TDR-PROG-TIT
        //               ,:TDR-TP-PRE-TIT
        //               ,:TDR-TP-STAT-TIT
        //               ,:TDR-DT-INI-COP-DB
        //                :IND-TDR-DT-INI-COP
        //               ,:TDR-DT-END-COP-DB
        //                :IND-TDR-DT-END-COP
        //               ,:TDR-IMP-PAG
        //                :IND-TDR-IMP-PAG
        //               ,:TDR-FL-SOLL
        //                :IND-TDR-FL-SOLL
        //               ,:TDR-FRAZ
        //                :IND-TDR-FRAZ
        //               ,:TDR-DT-APPLZ-MORA-DB
        //                :IND-TDR-DT-APPLZ-MORA
        //               ,:TDR-FL-MORA
        //                :IND-TDR-FL-MORA
        //               ,:TDR-ID-RAPP-RETE
        //                :IND-TDR-ID-RAPP-RETE
        //               ,:TDR-ID-RAPP-ANA
        //                :IND-TDR-ID-RAPP-ANA
        //               ,:TDR-COD-DVS
        //                :IND-TDR-COD-DVS
        //               ,:TDR-DT-EMIS-TIT-DB
        //               ,:TDR-DT-ESI-TIT-DB
        //                :IND-TDR-DT-ESI-TIT
        //               ,:TDR-TOT-PRE-NET
        //                :IND-TDR-TOT-PRE-NET
        //               ,:TDR-TOT-INTR-FRAZ
        //                :IND-TDR-TOT-INTR-FRAZ
        //               ,:TDR-TOT-INTR-MORA
        //                :IND-TDR-TOT-INTR-MORA
        //               ,:TDR-TOT-INTR-PREST
        //                :IND-TDR-TOT-INTR-PREST
        //               ,:TDR-TOT-INTR-RETDT
        //                :IND-TDR-TOT-INTR-RETDT
        //               ,:TDR-TOT-INTR-RIAT
        //                :IND-TDR-TOT-INTR-RIAT
        //               ,:TDR-TOT-DIR
        //                :IND-TDR-TOT-DIR
        //               ,:TDR-TOT-SPE-MED
        //                :IND-TDR-TOT-SPE-MED
        //               ,:TDR-TOT-SPE-AGE
        //                :IND-TDR-TOT-SPE-AGE
        //               ,:TDR-TOT-TAX
        //                :IND-TDR-TOT-TAX
        //               ,:TDR-TOT-SOPR-SAN
        //                :IND-TDR-TOT-SOPR-SAN
        //               ,:TDR-TOT-SOPR-TEC
        //                :IND-TDR-TOT-SOPR-TEC
        //               ,:TDR-TOT-SOPR-SPO
        //                :IND-TDR-TOT-SOPR-SPO
        //               ,:TDR-TOT-SOPR-PROF
        //                :IND-TDR-TOT-SOPR-PROF
        //               ,:TDR-TOT-SOPR-ALT
        //                :IND-TDR-TOT-SOPR-ALT
        //               ,:TDR-TOT-PRE-TOT
        //                :IND-TDR-TOT-PRE-TOT
        //               ,:TDR-TOT-PRE-PP-IAS
        //                :IND-TDR-TOT-PRE-PP-IAS
        //               ,:TDR-TOT-CAR-IAS
        //                :IND-TDR-TOT-CAR-IAS
        //               ,:TDR-TOT-PRE-SOLO-RSH
        //                :IND-TDR-TOT-PRE-SOLO-RSH
        //               ,:TDR-TOT-PROV-ACQ-1AA
        //                :IND-TDR-TOT-PROV-ACQ-1AA
        //               ,:TDR-TOT-PROV-ACQ-2AA
        //                :IND-TDR-TOT-PROV-ACQ-2AA
        //               ,:TDR-TOT-PROV-RICOR
        //                :IND-TDR-TOT-PROV-RICOR
        //               ,:TDR-TOT-PROV-INC
        //                :IND-TDR-TOT-PROV-INC
        //               ,:TDR-TOT-PROV-DA-REC
        //                :IND-TDR-TOT-PROV-DA-REC
        //               ,:TDR-IMP-AZ
        //                :IND-TDR-IMP-AZ
        //               ,:TDR-IMP-ADER
        //                :IND-TDR-IMP-ADER
        //               ,:TDR-IMP-TFR
        //                :IND-TDR-IMP-TFR
        //               ,:TDR-IMP-VOLO
        //                :IND-TDR-IMP-VOLO
        //               ,:TDR-FL-VLDT-TIT
        //                :IND-TDR-FL-VLDT-TIT
        //               ,:TDR-TOT-CAR-ACQ
        //                :IND-TDR-TOT-CAR-ACQ
        //               ,:TDR-TOT-CAR-GEST
        //                :IND-TDR-TOT-CAR-GEST
        //               ,:TDR-TOT-CAR-INC
        //                :IND-TDR-TOT-CAR-INC
        //               ,:TDR-TOT-MANFEE-ANTIC
        //                :IND-TDR-TOT-MANFEE-ANTIC
        //               ,:TDR-TOT-MANFEE-RICOR
        //                :IND-TDR-TOT-MANFEE-RICOR
        //               ,:TDR-TOT-MANFEE-REC
        //                :IND-TDR-TOT-MANFEE-REC
        //               ,:TDR-DS-RIGA
        //               ,:TDR-DS-OPER-SQL
        //               ,:TDR-DS-VER
        //               ,:TDR-DS-TS-INI-CPTZ
        //               ,:TDR-DS-TS-END-CPTZ
        //               ,:TDR-DS-UTENTE
        //               ,:TDR-DS-STATO-ELAB
        //               ,:TDR-IMP-TRASFE
        //                :IND-TDR-IMP-TRASFE
        //               ,:TDR-IMP-TFR-STRC
        //                :IND-TDR-IMP-TFR-STRC
        //               ,:TDR-TOT-ACQ-EXP
        //                :IND-TDR-TOT-ACQ-EXP
        //               ,:TDR-TOT-REMUN-ASS
        //                :IND-TDR-TOT-REMUN-ASS
        //               ,:TDR-TOT-COMMIS-INTER
        //                :IND-TDR-TOT-COMMIS-INTER
        //               ,:TDR-TOT-CNBT-ANTIRAC
        //                :IND-TDR-TOT-CNBT-ANTIRAC
        //               ,:TDR-FL-INC-AUTOGEN
        //                :IND-TDR-FL-INC-AUTOGEN
        //           END-EXEC.
        titRatDao.fetchCIdUpdEffTdr(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-EFF-TDR CURSOR FOR
        //              SELECT
        //                     ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_TIT
        //                    ,PROG_TIT
        //                    ,TP_PRE_TIT
        //                    ,TP_STAT_TIT
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,IMP_PAG
        //                    ,FL_SOLL
        //                    ,FRAZ
        //                    ,DT_APPLZ_MORA
        //                    ,FL_MORA
        //                    ,ID_RAPP_RETE
        //                    ,ID_RAPP_ANA
        //                    ,COD_DVS
        //                    ,DT_EMIS_TIT
        //                    ,DT_ESI_TIT
        //                    ,TOT_PRE_NET
        //                    ,TOT_INTR_FRAZ
        //                    ,TOT_INTR_MORA
        //                    ,TOT_INTR_PREST
        //                    ,TOT_INTR_RETDT
        //                    ,TOT_INTR_RIAT
        //                    ,TOT_DIR
        //                    ,TOT_SPE_MED
        //                    ,TOT_SPE_AGE
        //                    ,TOT_TAX
        //                    ,TOT_SOPR_SAN
        //                    ,TOT_SOPR_TEC
        //                    ,TOT_SOPR_SPO
        //                    ,TOT_SOPR_PROF
        //                    ,TOT_SOPR_ALT
        //                    ,TOT_PRE_TOT
        //                    ,TOT_PRE_PP_IAS
        //                    ,TOT_CAR_IAS
        //                    ,TOT_PRE_SOLO_RSH
        //                    ,TOT_PROV_ACQ_1AA
        //                    ,TOT_PROV_ACQ_2AA
        //                    ,TOT_PROV_RICOR
        //                    ,TOT_PROV_INC
        //                    ,TOT_PROV_DA_REC
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,TOT_CAR_ACQ
        //                    ,TOT_CAR_GEST
        //                    ,TOT_CAR_INC
        //                    ,TOT_MANFEE_ANTIC
        //                    ,TOT_MANFEE_RICOR
        //                    ,TOT_MANFEE_REC
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,TOT_ACQ_EXP
        //                    ,TOT_REMUN_ASS
        //                    ,TOT_COMMIS_INTER
        //                    ,TOT_CNBT_ANTIRAC
        //                    ,FL_INC_AUTOGEN
        //              FROM TIT_RAT
        //              WHERE     ID_OGG = :TDR-ID-OGG
        //                    AND TP_OGG = :TDR-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_TIT_RAT ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_TIT
        //                ,PROG_TIT
        //                ,TP_PRE_TIT
        //                ,TP_STAT_TIT
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,IMP_PAG
        //                ,FL_SOLL
        //                ,FRAZ
        //                ,DT_APPLZ_MORA
        //                ,FL_MORA
        //                ,ID_RAPP_RETE
        //                ,ID_RAPP_ANA
        //                ,COD_DVS
        //                ,DT_EMIS_TIT
        //                ,DT_ESI_TIT
        //                ,TOT_PRE_NET
        //                ,TOT_INTR_FRAZ
        //                ,TOT_INTR_MORA
        //                ,TOT_INTR_PREST
        //                ,TOT_INTR_RETDT
        //                ,TOT_INTR_RIAT
        //                ,TOT_DIR
        //                ,TOT_SPE_MED
        //                ,TOT_SPE_AGE
        //                ,TOT_TAX
        //                ,TOT_SOPR_SAN
        //                ,TOT_SOPR_TEC
        //                ,TOT_SOPR_SPO
        //                ,TOT_SOPR_PROF
        //                ,TOT_SOPR_ALT
        //                ,TOT_PRE_TOT
        //                ,TOT_PRE_PP_IAS
        //                ,TOT_CAR_IAS
        //                ,TOT_PRE_SOLO_RSH
        //                ,TOT_PROV_ACQ_1AA
        //                ,TOT_PROV_ACQ_2AA
        //                ,TOT_PROV_RICOR
        //                ,TOT_PROV_INC
        //                ,TOT_PROV_DA_REC
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,TOT_CAR_ACQ
        //                ,TOT_CAR_GEST
        //                ,TOT_CAR_INC
        //                ,TOT_MANFEE_ANTIC
        //                ,TOT_MANFEE_RICOR
        //                ,TOT_MANFEE_REC
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,TOT_ACQ_EXP
        //                ,TOT_REMUN_ASS
        //                ,TOT_COMMIS_INTER
        //                ,TOT_CNBT_ANTIRAC
        //                ,FL_INC_AUTOGEN
        //             INTO
        //                :TDR-ID-TIT-RAT
        //               ,:TDR-ID-OGG
        //               ,:TDR-TP-OGG
        //               ,:TDR-ID-MOVI-CRZ
        //               ,:TDR-ID-MOVI-CHIU
        //                :IND-TDR-ID-MOVI-CHIU
        //               ,:TDR-DT-INI-EFF-DB
        //               ,:TDR-DT-END-EFF-DB
        //               ,:TDR-COD-COMP-ANIA
        //               ,:TDR-TP-TIT
        //               ,:TDR-PROG-TIT
        //                :IND-TDR-PROG-TIT
        //               ,:TDR-TP-PRE-TIT
        //               ,:TDR-TP-STAT-TIT
        //               ,:TDR-DT-INI-COP-DB
        //                :IND-TDR-DT-INI-COP
        //               ,:TDR-DT-END-COP-DB
        //                :IND-TDR-DT-END-COP
        //               ,:TDR-IMP-PAG
        //                :IND-TDR-IMP-PAG
        //               ,:TDR-FL-SOLL
        //                :IND-TDR-FL-SOLL
        //               ,:TDR-FRAZ
        //                :IND-TDR-FRAZ
        //               ,:TDR-DT-APPLZ-MORA-DB
        //                :IND-TDR-DT-APPLZ-MORA
        //               ,:TDR-FL-MORA
        //                :IND-TDR-FL-MORA
        //               ,:TDR-ID-RAPP-RETE
        //                :IND-TDR-ID-RAPP-RETE
        //               ,:TDR-ID-RAPP-ANA
        //                :IND-TDR-ID-RAPP-ANA
        //               ,:TDR-COD-DVS
        //                :IND-TDR-COD-DVS
        //               ,:TDR-DT-EMIS-TIT-DB
        //               ,:TDR-DT-ESI-TIT-DB
        //                :IND-TDR-DT-ESI-TIT
        //               ,:TDR-TOT-PRE-NET
        //                :IND-TDR-TOT-PRE-NET
        //               ,:TDR-TOT-INTR-FRAZ
        //                :IND-TDR-TOT-INTR-FRAZ
        //               ,:TDR-TOT-INTR-MORA
        //                :IND-TDR-TOT-INTR-MORA
        //               ,:TDR-TOT-INTR-PREST
        //                :IND-TDR-TOT-INTR-PREST
        //               ,:TDR-TOT-INTR-RETDT
        //                :IND-TDR-TOT-INTR-RETDT
        //               ,:TDR-TOT-INTR-RIAT
        //                :IND-TDR-TOT-INTR-RIAT
        //               ,:TDR-TOT-DIR
        //                :IND-TDR-TOT-DIR
        //               ,:TDR-TOT-SPE-MED
        //                :IND-TDR-TOT-SPE-MED
        //               ,:TDR-TOT-SPE-AGE
        //                :IND-TDR-TOT-SPE-AGE
        //               ,:TDR-TOT-TAX
        //                :IND-TDR-TOT-TAX
        //               ,:TDR-TOT-SOPR-SAN
        //                :IND-TDR-TOT-SOPR-SAN
        //               ,:TDR-TOT-SOPR-TEC
        //                :IND-TDR-TOT-SOPR-TEC
        //               ,:TDR-TOT-SOPR-SPO
        //                :IND-TDR-TOT-SOPR-SPO
        //               ,:TDR-TOT-SOPR-PROF
        //                :IND-TDR-TOT-SOPR-PROF
        //               ,:TDR-TOT-SOPR-ALT
        //                :IND-TDR-TOT-SOPR-ALT
        //               ,:TDR-TOT-PRE-TOT
        //                :IND-TDR-TOT-PRE-TOT
        //               ,:TDR-TOT-PRE-PP-IAS
        //                :IND-TDR-TOT-PRE-PP-IAS
        //               ,:TDR-TOT-CAR-IAS
        //                :IND-TDR-TOT-CAR-IAS
        //               ,:TDR-TOT-PRE-SOLO-RSH
        //                :IND-TDR-TOT-PRE-SOLO-RSH
        //               ,:TDR-TOT-PROV-ACQ-1AA
        //                :IND-TDR-TOT-PROV-ACQ-1AA
        //               ,:TDR-TOT-PROV-ACQ-2AA
        //                :IND-TDR-TOT-PROV-ACQ-2AA
        //               ,:TDR-TOT-PROV-RICOR
        //                :IND-TDR-TOT-PROV-RICOR
        //               ,:TDR-TOT-PROV-INC
        //                :IND-TDR-TOT-PROV-INC
        //               ,:TDR-TOT-PROV-DA-REC
        //                :IND-TDR-TOT-PROV-DA-REC
        //               ,:TDR-IMP-AZ
        //                :IND-TDR-IMP-AZ
        //               ,:TDR-IMP-ADER
        //                :IND-TDR-IMP-ADER
        //               ,:TDR-IMP-TFR
        //                :IND-TDR-IMP-TFR
        //               ,:TDR-IMP-VOLO
        //                :IND-TDR-IMP-VOLO
        //               ,:TDR-FL-VLDT-TIT
        //                :IND-TDR-FL-VLDT-TIT
        //               ,:TDR-TOT-CAR-ACQ
        //                :IND-TDR-TOT-CAR-ACQ
        //               ,:TDR-TOT-CAR-GEST
        //                :IND-TDR-TOT-CAR-GEST
        //               ,:TDR-TOT-CAR-INC
        //                :IND-TDR-TOT-CAR-INC
        //               ,:TDR-TOT-MANFEE-ANTIC
        //                :IND-TDR-TOT-MANFEE-ANTIC
        //               ,:TDR-TOT-MANFEE-RICOR
        //                :IND-TDR-TOT-MANFEE-RICOR
        //               ,:TDR-TOT-MANFEE-REC
        //                :IND-TDR-TOT-MANFEE-REC
        //               ,:TDR-DS-RIGA
        //               ,:TDR-DS-OPER-SQL
        //               ,:TDR-DS-VER
        //               ,:TDR-DS-TS-INI-CPTZ
        //               ,:TDR-DS-TS-END-CPTZ
        //               ,:TDR-DS-UTENTE
        //               ,:TDR-DS-STATO-ELAB
        //               ,:TDR-IMP-TRASFE
        //                :IND-TDR-IMP-TRASFE
        //               ,:TDR-IMP-TFR-STRC
        //                :IND-TDR-IMP-TFR-STRC
        //               ,:TDR-TOT-ACQ-EXP
        //                :IND-TDR-TOT-ACQ-EXP
        //               ,:TDR-TOT-REMUN-ASS
        //                :IND-TDR-TOT-REMUN-ASS
        //               ,:TDR-TOT-COMMIS-INTER
        //                :IND-TDR-TOT-COMMIS-INTER
        //               ,:TDR-TOT-CNBT-ANTIRAC
        //                :IND-TDR-TOT-CNBT-ANTIRAC
        //               ,:TDR-FL-INC-AUTOGEN
        //                :IND-TDR-FL-INC-AUTOGEN
        //             FROM TIT_RAT
        //             WHERE     ID_OGG = :TDR-ID-OGG
        //                    AND TP_OGG = :TDR-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        titRatDao.selectRec1(titRat.getTdrIdOgg(), titRat.getTdrTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-EFF-TDR
        //           END-EXEC.
        titRatDao.openCIdoEffTdr(titRat.getTdrIdOgg(), titRat.getTdrTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-EFF-TDR
        //           END-EXEC.
        titRatDao.closeCIdoEffTdr();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-EFF-TDR
        //           INTO
        //                :TDR-ID-TIT-RAT
        //               ,:TDR-ID-OGG
        //               ,:TDR-TP-OGG
        //               ,:TDR-ID-MOVI-CRZ
        //               ,:TDR-ID-MOVI-CHIU
        //                :IND-TDR-ID-MOVI-CHIU
        //               ,:TDR-DT-INI-EFF-DB
        //               ,:TDR-DT-END-EFF-DB
        //               ,:TDR-COD-COMP-ANIA
        //               ,:TDR-TP-TIT
        //               ,:TDR-PROG-TIT
        //                :IND-TDR-PROG-TIT
        //               ,:TDR-TP-PRE-TIT
        //               ,:TDR-TP-STAT-TIT
        //               ,:TDR-DT-INI-COP-DB
        //                :IND-TDR-DT-INI-COP
        //               ,:TDR-DT-END-COP-DB
        //                :IND-TDR-DT-END-COP
        //               ,:TDR-IMP-PAG
        //                :IND-TDR-IMP-PAG
        //               ,:TDR-FL-SOLL
        //                :IND-TDR-FL-SOLL
        //               ,:TDR-FRAZ
        //                :IND-TDR-FRAZ
        //               ,:TDR-DT-APPLZ-MORA-DB
        //                :IND-TDR-DT-APPLZ-MORA
        //               ,:TDR-FL-MORA
        //                :IND-TDR-FL-MORA
        //               ,:TDR-ID-RAPP-RETE
        //                :IND-TDR-ID-RAPP-RETE
        //               ,:TDR-ID-RAPP-ANA
        //                :IND-TDR-ID-RAPP-ANA
        //               ,:TDR-COD-DVS
        //                :IND-TDR-COD-DVS
        //               ,:TDR-DT-EMIS-TIT-DB
        //               ,:TDR-DT-ESI-TIT-DB
        //                :IND-TDR-DT-ESI-TIT
        //               ,:TDR-TOT-PRE-NET
        //                :IND-TDR-TOT-PRE-NET
        //               ,:TDR-TOT-INTR-FRAZ
        //                :IND-TDR-TOT-INTR-FRAZ
        //               ,:TDR-TOT-INTR-MORA
        //                :IND-TDR-TOT-INTR-MORA
        //               ,:TDR-TOT-INTR-PREST
        //                :IND-TDR-TOT-INTR-PREST
        //               ,:TDR-TOT-INTR-RETDT
        //                :IND-TDR-TOT-INTR-RETDT
        //               ,:TDR-TOT-INTR-RIAT
        //                :IND-TDR-TOT-INTR-RIAT
        //               ,:TDR-TOT-DIR
        //                :IND-TDR-TOT-DIR
        //               ,:TDR-TOT-SPE-MED
        //                :IND-TDR-TOT-SPE-MED
        //               ,:TDR-TOT-SPE-AGE
        //                :IND-TDR-TOT-SPE-AGE
        //               ,:TDR-TOT-TAX
        //                :IND-TDR-TOT-TAX
        //               ,:TDR-TOT-SOPR-SAN
        //                :IND-TDR-TOT-SOPR-SAN
        //               ,:TDR-TOT-SOPR-TEC
        //                :IND-TDR-TOT-SOPR-TEC
        //               ,:TDR-TOT-SOPR-SPO
        //                :IND-TDR-TOT-SOPR-SPO
        //               ,:TDR-TOT-SOPR-PROF
        //                :IND-TDR-TOT-SOPR-PROF
        //               ,:TDR-TOT-SOPR-ALT
        //                :IND-TDR-TOT-SOPR-ALT
        //               ,:TDR-TOT-PRE-TOT
        //                :IND-TDR-TOT-PRE-TOT
        //               ,:TDR-TOT-PRE-PP-IAS
        //                :IND-TDR-TOT-PRE-PP-IAS
        //               ,:TDR-TOT-CAR-IAS
        //                :IND-TDR-TOT-CAR-IAS
        //               ,:TDR-TOT-PRE-SOLO-RSH
        //                :IND-TDR-TOT-PRE-SOLO-RSH
        //               ,:TDR-TOT-PROV-ACQ-1AA
        //                :IND-TDR-TOT-PROV-ACQ-1AA
        //               ,:TDR-TOT-PROV-ACQ-2AA
        //                :IND-TDR-TOT-PROV-ACQ-2AA
        //               ,:TDR-TOT-PROV-RICOR
        //                :IND-TDR-TOT-PROV-RICOR
        //               ,:TDR-TOT-PROV-INC
        //                :IND-TDR-TOT-PROV-INC
        //               ,:TDR-TOT-PROV-DA-REC
        //                :IND-TDR-TOT-PROV-DA-REC
        //               ,:TDR-IMP-AZ
        //                :IND-TDR-IMP-AZ
        //               ,:TDR-IMP-ADER
        //                :IND-TDR-IMP-ADER
        //               ,:TDR-IMP-TFR
        //                :IND-TDR-IMP-TFR
        //               ,:TDR-IMP-VOLO
        //                :IND-TDR-IMP-VOLO
        //               ,:TDR-FL-VLDT-TIT
        //                :IND-TDR-FL-VLDT-TIT
        //               ,:TDR-TOT-CAR-ACQ
        //                :IND-TDR-TOT-CAR-ACQ
        //               ,:TDR-TOT-CAR-GEST
        //                :IND-TDR-TOT-CAR-GEST
        //               ,:TDR-TOT-CAR-INC
        //                :IND-TDR-TOT-CAR-INC
        //               ,:TDR-TOT-MANFEE-ANTIC
        //                :IND-TDR-TOT-MANFEE-ANTIC
        //               ,:TDR-TOT-MANFEE-RICOR
        //                :IND-TDR-TOT-MANFEE-RICOR
        //               ,:TDR-TOT-MANFEE-REC
        //                :IND-TDR-TOT-MANFEE-REC
        //               ,:TDR-DS-RIGA
        //               ,:TDR-DS-OPER-SQL
        //               ,:TDR-DS-VER
        //               ,:TDR-DS-TS-INI-CPTZ
        //               ,:TDR-DS-TS-END-CPTZ
        //               ,:TDR-DS-UTENTE
        //               ,:TDR-DS-STATO-ELAB
        //               ,:TDR-IMP-TRASFE
        //                :IND-TDR-IMP-TRASFE
        //               ,:TDR-IMP-TFR-STRC
        //                :IND-TDR-IMP-TFR-STRC
        //               ,:TDR-TOT-ACQ-EXP
        //                :IND-TDR-TOT-ACQ-EXP
        //               ,:TDR-TOT-REMUN-ASS
        //                :IND-TDR-TOT-REMUN-ASS
        //               ,:TDR-TOT-COMMIS-INTER
        //                :IND-TDR-TOT-COMMIS-INTER
        //               ,:TDR-TOT-CNBT-ANTIRAC
        //                :IND-TDR-TOT-CNBT-ANTIRAC
        //               ,:TDR-FL-INC-AUTOGEN
        //                :IND-TDR-FL-INC-AUTOGEN
        //           END-EXEC.
        titRatDao.fetchCIdoEffTdr(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX
            a770CloseCursorIdo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_TIT
        //                ,PROG_TIT
        //                ,TP_PRE_TIT
        //                ,TP_STAT_TIT
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,IMP_PAG
        //                ,FL_SOLL
        //                ,FRAZ
        //                ,DT_APPLZ_MORA
        //                ,FL_MORA
        //                ,ID_RAPP_RETE
        //                ,ID_RAPP_ANA
        //                ,COD_DVS
        //                ,DT_EMIS_TIT
        //                ,DT_ESI_TIT
        //                ,TOT_PRE_NET
        //                ,TOT_INTR_FRAZ
        //                ,TOT_INTR_MORA
        //                ,TOT_INTR_PREST
        //                ,TOT_INTR_RETDT
        //                ,TOT_INTR_RIAT
        //                ,TOT_DIR
        //                ,TOT_SPE_MED
        //                ,TOT_SPE_AGE
        //                ,TOT_TAX
        //                ,TOT_SOPR_SAN
        //                ,TOT_SOPR_TEC
        //                ,TOT_SOPR_SPO
        //                ,TOT_SOPR_PROF
        //                ,TOT_SOPR_ALT
        //                ,TOT_PRE_TOT
        //                ,TOT_PRE_PP_IAS
        //                ,TOT_CAR_IAS
        //                ,TOT_PRE_SOLO_RSH
        //                ,TOT_PROV_ACQ_1AA
        //                ,TOT_PROV_ACQ_2AA
        //                ,TOT_PROV_RICOR
        //                ,TOT_PROV_INC
        //                ,TOT_PROV_DA_REC
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,TOT_CAR_ACQ
        //                ,TOT_CAR_GEST
        //                ,TOT_CAR_INC
        //                ,TOT_MANFEE_ANTIC
        //                ,TOT_MANFEE_RICOR
        //                ,TOT_MANFEE_REC
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,TOT_ACQ_EXP
        //                ,TOT_REMUN_ASS
        //                ,TOT_COMMIS_INTER
        //                ,TOT_CNBT_ANTIRAC
        //                ,FL_INC_AUTOGEN
        //             INTO
        //                :TDR-ID-TIT-RAT
        //               ,:TDR-ID-OGG
        //               ,:TDR-TP-OGG
        //               ,:TDR-ID-MOVI-CRZ
        //               ,:TDR-ID-MOVI-CHIU
        //                :IND-TDR-ID-MOVI-CHIU
        //               ,:TDR-DT-INI-EFF-DB
        //               ,:TDR-DT-END-EFF-DB
        //               ,:TDR-COD-COMP-ANIA
        //               ,:TDR-TP-TIT
        //               ,:TDR-PROG-TIT
        //                :IND-TDR-PROG-TIT
        //               ,:TDR-TP-PRE-TIT
        //               ,:TDR-TP-STAT-TIT
        //               ,:TDR-DT-INI-COP-DB
        //                :IND-TDR-DT-INI-COP
        //               ,:TDR-DT-END-COP-DB
        //                :IND-TDR-DT-END-COP
        //               ,:TDR-IMP-PAG
        //                :IND-TDR-IMP-PAG
        //               ,:TDR-FL-SOLL
        //                :IND-TDR-FL-SOLL
        //               ,:TDR-FRAZ
        //                :IND-TDR-FRAZ
        //               ,:TDR-DT-APPLZ-MORA-DB
        //                :IND-TDR-DT-APPLZ-MORA
        //               ,:TDR-FL-MORA
        //                :IND-TDR-FL-MORA
        //               ,:TDR-ID-RAPP-RETE
        //                :IND-TDR-ID-RAPP-RETE
        //               ,:TDR-ID-RAPP-ANA
        //                :IND-TDR-ID-RAPP-ANA
        //               ,:TDR-COD-DVS
        //                :IND-TDR-COD-DVS
        //               ,:TDR-DT-EMIS-TIT-DB
        //               ,:TDR-DT-ESI-TIT-DB
        //                :IND-TDR-DT-ESI-TIT
        //               ,:TDR-TOT-PRE-NET
        //                :IND-TDR-TOT-PRE-NET
        //               ,:TDR-TOT-INTR-FRAZ
        //                :IND-TDR-TOT-INTR-FRAZ
        //               ,:TDR-TOT-INTR-MORA
        //                :IND-TDR-TOT-INTR-MORA
        //               ,:TDR-TOT-INTR-PREST
        //                :IND-TDR-TOT-INTR-PREST
        //               ,:TDR-TOT-INTR-RETDT
        //                :IND-TDR-TOT-INTR-RETDT
        //               ,:TDR-TOT-INTR-RIAT
        //                :IND-TDR-TOT-INTR-RIAT
        //               ,:TDR-TOT-DIR
        //                :IND-TDR-TOT-DIR
        //               ,:TDR-TOT-SPE-MED
        //                :IND-TDR-TOT-SPE-MED
        //               ,:TDR-TOT-SPE-AGE
        //                :IND-TDR-TOT-SPE-AGE
        //               ,:TDR-TOT-TAX
        //                :IND-TDR-TOT-TAX
        //               ,:TDR-TOT-SOPR-SAN
        //                :IND-TDR-TOT-SOPR-SAN
        //               ,:TDR-TOT-SOPR-TEC
        //                :IND-TDR-TOT-SOPR-TEC
        //               ,:TDR-TOT-SOPR-SPO
        //                :IND-TDR-TOT-SOPR-SPO
        //               ,:TDR-TOT-SOPR-PROF
        //                :IND-TDR-TOT-SOPR-PROF
        //               ,:TDR-TOT-SOPR-ALT
        //                :IND-TDR-TOT-SOPR-ALT
        //               ,:TDR-TOT-PRE-TOT
        //                :IND-TDR-TOT-PRE-TOT
        //               ,:TDR-TOT-PRE-PP-IAS
        //                :IND-TDR-TOT-PRE-PP-IAS
        //               ,:TDR-TOT-CAR-IAS
        //                :IND-TDR-TOT-CAR-IAS
        //               ,:TDR-TOT-PRE-SOLO-RSH
        //                :IND-TDR-TOT-PRE-SOLO-RSH
        //               ,:TDR-TOT-PROV-ACQ-1AA
        //                :IND-TDR-TOT-PROV-ACQ-1AA
        //               ,:TDR-TOT-PROV-ACQ-2AA
        //                :IND-TDR-TOT-PROV-ACQ-2AA
        //               ,:TDR-TOT-PROV-RICOR
        //                :IND-TDR-TOT-PROV-RICOR
        //               ,:TDR-TOT-PROV-INC
        //                :IND-TDR-TOT-PROV-INC
        //               ,:TDR-TOT-PROV-DA-REC
        //                :IND-TDR-TOT-PROV-DA-REC
        //               ,:TDR-IMP-AZ
        //                :IND-TDR-IMP-AZ
        //               ,:TDR-IMP-ADER
        //                :IND-TDR-IMP-ADER
        //               ,:TDR-IMP-TFR
        //                :IND-TDR-IMP-TFR
        //               ,:TDR-IMP-VOLO
        //                :IND-TDR-IMP-VOLO
        //               ,:TDR-FL-VLDT-TIT
        //                :IND-TDR-FL-VLDT-TIT
        //               ,:TDR-TOT-CAR-ACQ
        //                :IND-TDR-TOT-CAR-ACQ
        //               ,:TDR-TOT-CAR-GEST
        //                :IND-TDR-TOT-CAR-GEST
        //               ,:TDR-TOT-CAR-INC
        //                :IND-TDR-TOT-CAR-INC
        //               ,:TDR-TOT-MANFEE-ANTIC
        //                :IND-TDR-TOT-MANFEE-ANTIC
        //               ,:TDR-TOT-MANFEE-RICOR
        //                :IND-TDR-TOT-MANFEE-RICOR
        //               ,:TDR-TOT-MANFEE-REC
        //                :IND-TDR-TOT-MANFEE-REC
        //               ,:TDR-DS-RIGA
        //               ,:TDR-DS-OPER-SQL
        //               ,:TDR-DS-VER
        //               ,:TDR-DS-TS-INI-CPTZ
        //               ,:TDR-DS-TS-END-CPTZ
        //               ,:TDR-DS-UTENTE
        //               ,:TDR-DS-STATO-ELAB
        //               ,:TDR-IMP-TRASFE
        //                :IND-TDR-IMP-TRASFE
        //               ,:TDR-IMP-TFR-STRC
        //                :IND-TDR-IMP-TFR-STRC
        //               ,:TDR-TOT-ACQ-EXP
        //                :IND-TDR-TOT-ACQ-EXP
        //               ,:TDR-TOT-REMUN-ASS
        //                :IND-TDR-TOT-REMUN-ASS
        //               ,:TDR-TOT-COMMIS-INTER
        //                :IND-TDR-TOT-COMMIS-INTER
        //               ,:TDR-TOT-CNBT-ANTIRAC
        //                :IND-TDR-TOT-CNBT-ANTIRAC
        //               ,:TDR-FL-INC-AUTOGEN
        //                :IND-TDR-FL-INC-AUTOGEN
        //             FROM TIT_RAT
        //             WHERE     ID_TIT_RAT = :TDR-ID-TIT-RAT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        titRatDao.selectRec2(titRat.getTdrIdTitRat(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-CPZ-TDR CURSOR FOR
        //              SELECT
        //                     ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_TIT
        //                    ,PROG_TIT
        //                    ,TP_PRE_TIT
        //                    ,TP_STAT_TIT
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,IMP_PAG
        //                    ,FL_SOLL
        //                    ,FRAZ
        //                    ,DT_APPLZ_MORA
        //                    ,FL_MORA
        //                    ,ID_RAPP_RETE
        //                    ,ID_RAPP_ANA
        //                    ,COD_DVS
        //                    ,DT_EMIS_TIT
        //                    ,DT_ESI_TIT
        //                    ,TOT_PRE_NET
        //                    ,TOT_INTR_FRAZ
        //                    ,TOT_INTR_MORA
        //                    ,TOT_INTR_PREST
        //                    ,TOT_INTR_RETDT
        //                    ,TOT_INTR_RIAT
        //                    ,TOT_DIR
        //                    ,TOT_SPE_MED
        //                    ,TOT_SPE_AGE
        //                    ,TOT_TAX
        //                    ,TOT_SOPR_SAN
        //                    ,TOT_SOPR_TEC
        //                    ,TOT_SOPR_SPO
        //                    ,TOT_SOPR_PROF
        //                    ,TOT_SOPR_ALT
        //                    ,TOT_PRE_TOT
        //                    ,TOT_PRE_PP_IAS
        //                    ,TOT_CAR_IAS
        //                    ,TOT_PRE_SOLO_RSH
        //                    ,TOT_PROV_ACQ_1AA
        //                    ,TOT_PROV_ACQ_2AA
        //                    ,TOT_PROV_RICOR
        //                    ,TOT_PROV_INC
        //                    ,TOT_PROV_DA_REC
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,TOT_CAR_ACQ
        //                    ,TOT_CAR_GEST
        //                    ,TOT_CAR_INC
        //                    ,TOT_MANFEE_ANTIC
        //                    ,TOT_MANFEE_RICOR
        //                    ,TOT_MANFEE_REC
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,TOT_ACQ_EXP
        //                    ,TOT_REMUN_ASS
        //                    ,TOT_COMMIS_INTER
        //                    ,TOT_CNBT_ANTIRAC
        //                    ,FL_INC_AUTOGEN
        //              FROM TIT_RAT
        //              WHERE     ID_OGG = :TDR-ID-OGG
        //           AND TP_OGG = :TDR-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_TIT_RAT ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_TIT
        //                ,PROG_TIT
        //                ,TP_PRE_TIT
        //                ,TP_STAT_TIT
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,IMP_PAG
        //                ,FL_SOLL
        //                ,FRAZ
        //                ,DT_APPLZ_MORA
        //                ,FL_MORA
        //                ,ID_RAPP_RETE
        //                ,ID_RAPP_ANA
        //                ,COD_DVS
        //                ,DT_EMIS_TIT
        //                ,DT_ESI_TIT
        //                ,TOT_PRE_NET
        //                ,TOT_INTR_FRAZ
        //                ,TOT_INTR_MORA
        //                ,TOT_INTR_PREST
        //                ,TOT_INTR_RETDT
        //                ,TOT_INTR_RIAT
        //                ,TOT_DIR
        //                ,TOT_SPE_MED
        //                ,TOT_SPE_AGE
        //                ,TOT_TAX
        //                ,TOT_SOPR_SAN
        //                ,TOT_SOPR_TEC
        //                ,TOT_SOPR_SPO
        //                ,TOT_SOPR_PROF
        //                ,TOT_SOPR_ALT
        //                ,TOT_PRE_TOT
        //                ,TOT_PRE_PP_IAS
        //                ,TOT_CAR_IAS
        //                ,TOT_PRE_SOLO_RSH
        //                ,TOT_PROV_ACQ_1AA
        //                ,TOT_PROV_ACQ_2AA
        //                ,TOT_PROV_RICOR
        //                ,TOT_PROV_INC
        //                ,TOT_PROV_DA_REC
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,TOT_CAR_ACQ
        //                ,TOT_CAR_GEST
        //                ,TOT_CAR_INC
        //                ,TOT_MANFEE_ANTIC
        //                ,TOT_MANFEE_RICOR
        //                ,TOT_MANFEE_REC
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,TOT_ACQ_EXP
        //                ,TOT_REMUN_ASS
        //                ,TOT_COMMIS_INTER
        //                ,TOT_CNBT_ANTIRAC
        //                ,FL_INC_AUTOGEN
        //             INTO
        //                :TDR-ID-TIT-RAT
        //               ,:TDR-ID-OGG
        //               ,:TDR-TP-OGG
        //               ,:TDR-ID-MOVI-CRZ
        //               ,:TDR-ID-MOVI-CHIU
        //                :IND-TDR-ID-MOVI-CHIU
        //               ,:TDR-DT-INI-EFF-DB
        //               ,:TDR-DT-END-EFF-DB
        //               ,:TDR-COD-COMP-ANIA
        //               ,:TDR-TP-TIT
        //               ,:TDR-PROG-TIT
        //                :IND-TDR-PROG-TIT
        //               ,:TDR-TP-PRE-TIT
        //               ,:TDR-TP-STAT-TIT
        //               ,:TDR-DT-INI-COP-DB
        //                :IND-TDR-DT-INI-COP
        //               ,:TDR-DT-END-COP-DB
        //                :IND-TDR-DT-END-COP
        //               ,:TDR-IMP-PAG
        //                :IND-TDR-IMP-PAG
        //               ,:TDR-FL-SOLL
        //                :IND-TDR-FL-SOLL
        //               ,:TDR-FRAZ
        //                :IND-TDR-FRAZ
        //               ,:TDR-DT-APPLZ-MORA-DB
        //                :IND-TDR-DT-APPLZ-MORA
        //               ,:TDR-FL-MORA
        //                :IND-TDR-FL-MORA
        //               ,:TDR-ID-RAPP-RETE
        //                :IND-TDR-ID-RAPP-RETE
        //               ,:TDR-ID-RAPP-ANA
        //                :IND-TDR-ID-RAPP-ANA
        //               ,:TDR-COD-DVS
        //                :IND-TDR-COD-DVS
        //               ,:TDR-DT-EMIS-TIT-DB
        //               ,:TDR-DT-ESI-TIT-DB
        //                :IND-TDR-DT-ESI-TIT
        //               ,:TDR-TOT-PRE-NET
        //                :IND-TDR-TOT-PRE-NET
        //               ,:TDR-TOT-INTR-FRAZ
        //                :IND-TDR-TOT-INTR-FRAZ
        //               ,:TDR-TOT-INTR-MORA
        //                :IND-TDR-TOT-INTR-MORA
        //               ,:TDR-TOT-INTR-PREST
        //                :IND-TDR-TOT-INTR-PREST
        //               ,:TDR-TOT-INTR-RETDT
        //                :IND-TDR-TOT-INTR-RETDT
        //               ,:TDR-TOT-INTR-RIAT
        //                :IND-TDR-TOT-INTR-RIAT
        //               ,:TDR-TOT-DIR
        //                :IND-TDR-TOT-DIR
        //               ,:TDR-TOT-SPE-MED
        //                :IND-TDR-TOT-SPE-MED
        //               ,:TDR-TOT-SPE-AGE
        //                :IND-TDR-TOT-SPE-AGE
        //               ,:TDR-TOT-TAX
        //                :IND-TDR-TOT-TAX
        //               ,:TDR-TOT-SOPR-SAN
        //                :IND-TDR-TOT-SOPR-SAN
        //               ,:TDR-TOT-SOPR-TEC
        //                :IND-TDR-TOT-SOPR-TEC
        //               ,:TDR-TOT-SOPR-SPO
        //                :IND-TDR-TOT-SOPR-SPO
        //               ,:TDR-TOT-SOPR-PROF
        //                :IND-TDR-TOT-SOPR-PROF
        //               ,:TDR-TOT-SOPR-ALT
        //                :IND-TDR-TOT-SOPR-ALT
        //               ,:TDR-TOT-PRE-TOT
        //                :IND-TDR-TOT-PRE-TOT
        //               ,:TDR-TOT-PRE-PP-IAS
        //                :IND-TDR-TOT-PRE-PP-IAS
        //               ,:TDR-TOT-CAR-IAS
        //                :IND-TDR-TOT-CAR-IAS
        //               ,:TDR-TOT-PRE-SOLO-RSH
        //                :IND-TDR-TOT-PRE-SOLO-RSH
        //               ,:TDR-TOT-PROV-ACQ-1AA
        //                :IND-TDR-TOT-PROV-ACQ-1AA
        //               ,:TDR-TOT-PROV-ACQ-2AA
        //                :IND-TDR-TOT-PROV-ACQ-2AA
        //               ,:TDR-TOT-PROV-RICOR
        //                :IND-TDR-TOT-PROV-RICOR
        //               ,:TDR-TOT-PROV-INC
        //                :IND-TDR-TOT-PROV-INC
        //               ,:TDR-TOT-PROV-DA-REC
        //                :IND-TDR-TOT-PROV-DA-REC
        //               ,:TDR-IMP-AZ
        //                :IND-TDR-IMP-AZ
        //               ,:TDR-IMP-ADER
        //                :IND-TDR-IMP-ADER
        //               ,:TDR-IMP-TFR
        //                :IND-TDR-IMP-TFR
        //               ,:TDR-IMP-VOLO
        //                :IND-TDR-IMP-VOLO
        //               ,:TDR-FL-VLDT-TIT
        //                :IND-TDR-FL-VLDT-TIT
        //               ,:TDR-TOT-CAR-ACQ
        //                :IND-TDR-TOT-CAR-ACQ
        //               ,:TDR-TOT-CAR-GEST
        //                :IND-TDR-TOT-CAR-GEST
        //               ,:TDR-TOT-CAR-INC
        //                :IND-TDR-TOT-CAR-INC
        //               ,:TDR-TOT-MANFEE-ANTIC
        //                :IND-TDR-TOT-MANFEE-ANTIC
        //               ,:TDR-TOT-MANFEE-RICOR
        //                :IND-TDR-TOT-MANFEE-RICOR
        //               ,:TDR-TOT-MANFEE-REC
        //                :IND-TDR-TOT-MANFEE-REC
        //               ,:TDR-DS-RIGA
        //               ,:TDR-DS-OPER-SQL
        //               ,:TDR-DS-VER
        //               ,:TDR-DS-TS-INI-CPTZ
        //               ,:TDR-DS-TS-END-CPTZ
        //               ,:TDR-DS-UTENTE
        //               ,:TDR-DS-STATO-ELAB
        //               ,:TDR-IMP-TRASFE
        //                :IND-TDR-IMP-TRASFE
        //               ,:TDR-IMP-TFR-STRC
        //                :IND-TDR-IMP-TFR-STRC
        //               ,:TDR-TOT-ACQ-EXP
        //                :IND-TDR-TOT-ACQ-EXP
        //               ,:TDR-TOT-REMUN-ASS
        //                :IND-TDR-TOT-REMUN-ASS
        //               ,:TDR-TOT-COMMIS-INTER
        //                :IND-TDR-TOT-COMMIS-INTER
        //               ,:TDR-TOT-CNBT-ANTIRAC
        //                :IND-TDR-TOT-CNBT-ANTIRAC
        //               ,:TDR-FL-INC-AUTOGEN
        //                :IND-TDR-FL-INC-AUTOGEN
        //             FROM TIT_RAT
        //             WHERE     ID_OGG = :TDR-ID-OGG
        //                    AND TP_OGG = :TDR-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        titRatDao.selectRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-CPZ-TDR
        //           END-EXEC.
        titRatDao.openCIdoCpzTdr(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-CPZ-TDR
        //           END-EXEC.
        titRatDao.closeCIdoCpzTdr();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-CPZ-TDR
        //           INTO
        //                :TDR-ID-TIT-RAT
        //               ,:TDR-ID-OGG
        //               ,:TDR-TP-OGG
        //               ,:TDR-ID-MOVI-CRZ
        //               ,:TDR-ID-MOVI-CHIU
        //                :IND-TDR-ID-MOVI-CHIU
        //               ,:TDR-DT-INI-EFF-DB
        //               ,:TDR-DT-END-EFF-DB
        //               ,:TDR-COD-COMP-ANIA
        //               ,:TDR-TP-TIT
        //               ,:TDR-PROG-TIT
        //                :IND-TDR-PROG-TIT
        //               ,:TDR-TP-PRE-TIT
        //               ,:TDR-TP-STAT-TIT
        //               ,:TDR-DT-INI-COP-DB
        //                :IND-TDR-DT-INI-COP
        //               ,:TDR-DT-END-COP-DB
        //                :IND-TDR-DT-END-COP
        //               ,:TDR-IMP-PAG
        //                :IND-TDR-IMP-PAG
        //               ,:TDR-FL-SOLL
        //                :IND-TDR-FL-SOLL
        //               ,:TDR-FRAZ
        //                :IND-TDR-FRAZ
        //               ,:TDR-DT-APPLZ-MORA-DB
        //                :IND-TDR-DT-APPLZ-MORA
        //               ,:TDR-FL-MORA
        //                :IND-TDR-FL-MORA
        //               ,:TDR-ID-RAPP-RETE
        //                :IND-TDR-ID-RAPP-RETE
        //               ,:TDR-ID-RAPP-ANA
        //                :IND-TDR-ID-RAPP-ANA
        //               ,:TDR-COD-DVS
        //                :IND-TDR-COD-DVS
        //               ,:TDR-DT-EMIS-TIT-DB
        //               ,:TDR-DT-ESI-TIT-DB
        //                :IND-TDR-DT-ESI-TIT
        //               ,:TDR-TOT-PRE-NET
        //                :IND-TDR-TOT-PRE-NET
        //               ,:TDR-TOT-INTR-FRAZ
        //                :IND-TDR-TOT-INTR-FRAZ
        //               ,:TDR-TOT-INTR-MORA
        //                :IND-TDR-TOT-INTR-MORA
        //               ,:TDR-TOT-INTR-PREST
        //                :IND-TDR-TOT-INTR-PREST
        //               ,:TDR-TOT-INTR-RETDT
        //                :IND-TDR-TOT-INTR-RETDT
        //               ,:TDR-TOT-INTR-RIAT
        //                :IND-TDR-TOT-INTR-RIAT
        //               ,:TDR-TOT-DIR
        //                :IND-TDR-TOT-DIR
        //               ,:TDR-TOT-SPE-MED
        //                :IND-TDR-TOT-SPE-MED
        //               ,:TDR-TOT-SPE-AGE
        //                :IND-TDR-TOT-SPE-AGE
        //               ,:TDR-TOT-TAX
        //                :IND-TDR-TOT-TAX
        //               ,:TDR-TOT-SOPR-SAN
        //                :IND-TDR-TOT-SOPR-SAN
        //               ,:TDR-TOT-SOPR-TEC
        //                :IND-TDR-TOT-SOPR-TEC
        //               ,:TDR-TOT-SOPR-SPO
        //                :IND-TDR-TOT-SOPR-SPO
        //               ,:TDR-TOT-SOPR-PROF
        //                :IND-TDR-TOT-SOPR-PROF
        //               ,:TDR-TOT-SOPR-ALT
        //                :IND-TDR-TOT-SOPR-ALT
        //               ,:TDR-TOT-PRE-TOT
        //                :IND-TDR-TOT-PRE-TOT
        //               ,:TDR-TOT-PRE-PP-IAS
        //                :IND-TDR-TOT-PRE-PP-IAS
        //               ,:TDR-TOT-CAR-IAS
        //                :IND-TDR-TOT-CAR-IAS
        //               ,:TDR-TOT-PRE-SOLO-RSH
        //                :IND-TDR-TOT-PRE-SOLO-RSH
        //               ,:TDR-TOT-PROV-ACQ-1AA
        //                :IND-TDR-TOT-PROV-ACQ-1AA
        //               ,:TDR-TOT-PROV-ACQ-2AA
        //                :IND-TDR-TOT-PROV-ACQ-2AA
        //               ,:TDR-TOT-PROV-RICOR
        //                :IND-TDR-TOT-PROV-RICOR
        //               ,:TDR-TOT-PROV-INC
        //                :IND-TDR-TOT-PROV-INC
        //               ,:TDR-TOT-PROV-DA-REC
        //                :IND-TDR-TOT-PROV-DA-REC
        //               ,:TDR-IMP-AZ
        //                :IND-TDR-IMP-AZ
        //               ,:TDR-IMP-ADER
        //                :IND-TDR-IMP-ADER
        //               ,:TDR-IMP-TFR
        //                :IND-TDR-IMP-TFR
        //               ,:TDR-IMP-VOLO
        //                :IND-TDR-IMP-VOLO
        //               ,:TDR-FL-VLDT-TIT
        //                :IND-TDR-FL-VLDT-TIT
        //               ,:TDR-TOT-CAR-ACQ
        //                :IND-TDR-TOT-CAR-ACQ
        //               ,:TDR-TOT-CAR-GEST
        //                :IND-TDR-TOT-CAR-GEST
        //               ,:TDR-TOT-CAR-INC
        //                :IND-TDR-TOT-CAR-INC
        //               ,:TDR-TOT-MANFEE-ANTIC
        //                :IND-TDR-TOT-MANFEE-ANTIC
        //               ,:TDR-TOT-MANFEE-RICOR
        //                :IND-TDR-TOT-MANFEE-RICOR
        //               ,:TDR-TOT-MANFEE-REC
        //                :IND-TDR-TOT-MANFEE-REC
        //               ,:TDR-DS-RIGA
        //               ,:TDR-DS-OPER-SQL
        //               ,:TDR-DS-VER
        //               ,:TDR-DS-TS-INI-CPTZ
        //               ,:TDR-DS-TS-END-CPTZ
        //               ,:TDR-DS-UTENTE
        //               ,:TDR-DS-STATO-ELAB
        //               ,:TDR-IMP-TRASFE
        //                :IND-TDR-IMP-TRASFE
        //               ,:TDR-IMP-TFR-STRC
        //                :IND-TDR-IMP-TFR-STRC
        //               ,:TDR-TOT-ACQ-EXP
        //                :IND-TDR-TOT-ACQ-EXP
        //               ,:TDR-TOT-REMUN-ASS
        //                :IND-TDR-TOT-REMUN-ASS
        //               ,:TDR-TOT-COMMIS-INTER
        //                :IND-TDR-TOT-COMMIS-INTER
        //               ,:TDR-TOT-CNBT-ANTIRAC
        //                :IND-TDR-TOT-CNBT-ANTIRAC
        //               ,:TDR-FL-INC-AUTOGEN
        //                :IND-TDR-FL-INC-AUTOGEN
        //           END-EXEC.
        titRatDao.fetchCIdoCpzTdr(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX
            b770CloseCursorIdoCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-TDR-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO TDR-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndTitRat().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-ID-MOVI-CHIU-NULL
            titRat.getTdrIdMoviChiu().setTdrIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrIdMoviChiu.Len.TDR_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-TDR-PROG-TIT = -1
        //              MOVE HIGH-VALUES TO TDR-PROG-TIT-NULL
        //           END-IF
        if (ws.getIndTitRat().getProgTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-PROG-TIT-NULL
            titRat.getTdrProgTit().setTdrProgTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrProgTit.Len.TDR_PROG_TIT_NULL));
        }
        // COB_CODE: IF IND-TDR-DT-INI-COP = -1
        //              MOVE HIGH-VALUES TO TDR-DT-INI-COP-NULL
        //           END-IF
        if (ws.getIndTitRat().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-DT-INI-COP-NULL
            titRat.getTdrDtIniCop().setTdrDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrDtIniCop.Len.TDR_DT_INI_COP_NULL));
        }
        // COB_CODE: IF IND-TDR-DT-END-COP = -1
        //              MOVE HIGH-VALUES TO TDR-DT-END-COP-NULL
        //           END-IF
        if (ws.getIndTitRat().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-DT-END-COP-NULL
            titRat.getTdrDtEndCop().setTdrDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrDtEndCop.Len.TDR_DT_END_COP_NULL));
        }
        // COB_CODE: IF IND-TDR-IMP-PAG = -1
        //              MOVE HIGH-VALUES TO TDR-IMP-PAG-NULL
        //           END-IF
        if (ws.getIndTitRat().getImpPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-IMP-PAG-NULL
            titRat.getTdrImpPag().setTdrImpPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrImpPag.Len.TDR_IMP_PAG_NULL));
        }
        // COB_CODE: IF IND-TDR-FL-SOLL = -1
        //              MOVE HIGH-VALUES TO TDR-FL-SOLL-NULL
        //           END-IF
        if (ws.getIndTitRat().getFlSoll() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-FL-SOLL-NULL
            titRat.setTdrFlSoll(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TDR-FRAZ = -1
        //              MOVE HIGH-VALUES TO TDR-FRAZ-NULL
        //           END-IF
        if (ws.getIndTitRat().getFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-FRAZ-NULL
            titRat.getTdrFraz().setTdrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrFraz.Len.TDR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-TDR-DT-APPLZ-MORA = -1
        //              MOVE HIGH-VALUES TO TDR-DT-APPLZ-MORA-NULL
        //           END-IF
        if (ws.getIndTitRat().getDtApplzMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-DT-APPLZ-MORA-NULL
            titRat.getTdrDtApplzMora().setTdrDtApplzMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrDtApplzMora.Len.TDR_DT_APPLZ_MORA_NULL));
        }
        // COB_CODE: IF IND-TDR-FL-MORA = -1
        //              MOVE HIGH-VALUES TO TDR-FL-MORA-NULL
        //           END-IF
        if (ws.getIndTitRat().getFlMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-FL-MORA-NULL
            titRat.setTdrFlMora(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TDR-ID-RAPP-RETE = -1
        //              MOVE HIGH-VALUES TO TDR-ID-RAPP-RETE-NULL
        //           END-IF
        if (ws.getIndTitRat().getIdRappRete() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-ID-RAPP-RETE-NULL
            titRat.getTdrIdRappRete().setTdrIdRappReteNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrIdRappRete.Len.TDR_ID_RAPP_RETE_NULL));
        }
        // COB_CODE: IF IND-TDR-ID-RAPP-ANA = -1
        //              MOVE HIGH-VALUES TO TDR-ID-RAPP-ANA-NULL
        //           END-IF
        if (ws.getIndTitRat().getIdRappAna() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-ID-RAPP-ANA-NULL
            titRat.getTdrIdRappAna().setTdrIdRappAnaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrIdRappAna.Len.TDR_ID_RAPP_ANA_NULL));
        }
        // COB_CODE: IF IND-TDR-COD-DVS = -1
        //              MOVE HIGH-VALUES TO TDR-COD-DVS-NULL
        //           END-IF
        if (ws.getIndTitRat().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-COD-DVS-NULL
            titRat.setTdrCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitRatIdbstdr0.Len.TDR_COD_DVS));
        }
        // COB_CODE: IF IND-TDR-DT-ESI-TIT = -1
        //              MOVE HIGH-VALUES TO TDR-DT-ESI-TIT-NULL
        //           END-IF
        if (ws.getIndTitRat().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-DT-ESI-TIT-NULL
            titRat.getTdrDtEsiTit().setTdrDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrDtEsiTit.Len.TDR_DT_ESI_TIT_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-PRE-NET = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-PRE-NET-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-PRE-NET-NULL
            titRat.getTdrTotPreNet().setTdrTotPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotPreNet.Len.TDR_TOT_PRE_NET_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-INTR-FRAZ = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-INTR-FRAZ-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-INTR-FRAZ-NULL
            titRat.getTdrTotIntrFraz().setTdrTotIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotIntrFraz.Len.TDR_TOT_INTR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-INTR-MORA-NULL
            titRat.getTdrTotIntrMora().setTdrTotIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotIntrMora.Len.TDR_TOT_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-INTR-PREST-NULL
            titRat.getTdrTotIntrPrest().setTdrTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotIntrPrest.Len.TDR_TOT_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-INTR-RETDT = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-INTR-RETDT-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-INTR-RETDT-NULL
            titRat.getTdrTotIntrRetdt().setTdrTotIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotIntrRetdt.Len.TDR_TOT_INTR_RETDT_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-INTR-RIAT-NULL
            titRat.getTdrTotIntrRiat().setTdrTotIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotIntrRiat.Len.TDR_TOT_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-DIR = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-DIR-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-DIR-NULL
            titRat.getTdrTotDir().setTdrTotDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotDir.Len.TDR_TOT_DIR_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-SPE-MED = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-SPE-MED-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-SPE-MED-NULL
            titRat.getTdrTotSpeMed().setTdrTotSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotSpeMed.Len.TDR_TOT_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-SPE-AGE = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-SPE-AGE-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-SPE-AGE-NULL
            titRat.getTdrTotSpeAge().setTdrTotSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotSpeAge.Len.TDR_TOT_SPE_AGE_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-TAX = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-TAX-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-TAX-NULL
            titRat.getTdrTotTax().setTdrTotTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotTax.Len.TDR_TOT_TAX_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-SOPR-SAN-NULL
            titRat.getTdrTotSoprSan().setTdrTotSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotSoprSan.Len.TDR_TOT_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-SOPR-TEC-NULL
            titRat.getTdrTotSoprTec().setTdrTotSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotSoprTec.Len.TDR_TOT_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-SOPR-SPO-NULL
            titRat.getTdrTotSoprSpo().setTdrTotSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotSoprSpo.Len.TDR_TOT_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-SOPR-PROF-NULL
            titRat.getTdrTotSoprProf().setTdrTotSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotSoprProf.Len.TDR_TOT_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-SOPR-ALT = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-SOPR-ALT-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-SOPR-ALT-NULL
            titRat.getTdrTotSoprAlt().setTdrTotSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotSoprAlt.Len.TDR_TOT_SOPR_ALT_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-PRE-TOT = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-PRE-TOT-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-PRE-TOT-NULL
            titRat.getTdrTotPreTot().setTdrTotPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotPreTot.Len.TDR_TOT_PRE_TOT_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-PRE-PP-IAS = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-PRE-PP-IAS-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-PRE-PP-IAS-NULL
            titRat.getTdrTotPrePpIas().setTdrTotPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotPrePpIas.Len.TDR_TOT_PRE_PP_IAS_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-CAR-IAS = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-CAR-IAS-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-CAR-IAS-NULL
            titRat.getTdrTotCarIas().setTdrTotCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotCarIas.Len.TDR_TOT_CAR_IAS_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-PRE-SOLO-RSH = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-PRE-SOLO-RSH-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-PRE-SOLO-RSH-NULL
            titRat.getTdrTotPreSoloRsh().setTdrTotPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotPreSoloRsh.Len.TDR_TOT_PRE_SOLO_RSH_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-PROV-ACQ-1AA = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-PROV-ACQ-1AA-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-PROV-ACQ-1AA-NULL
            titRat.getTdrTotProvAcq1aa().setTdrTotProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotProvAcq1aa.Len.TDR_TOT_PROV_ACQ1AA_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-PROV-ACQ-2AA = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-PROV-ACQ-2AA-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-PROV-ACQ-2AA-NULL
            titRat.getTdrTotProvAcq2aa().setTdrTotProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotProvAcq2aa.Len.TDR_TOT_PROV_ACQ2AA_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-PROV-RICOR-NULL
            titRat.getTdrTotProvRicor().setTdrTotProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotProvRicor.Len.TDR_TOT_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-PROV-INC-NULL
            titRat.getTdrTotProvInc().setTdrTotProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotProvInc.Len.TDR_TOT_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-PROV-DA-REC = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-PROV-DA-REC-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-PROV-DA-REC-NULL
            titRat.getTdrTotProvDaRec().setTdrTotProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotProvDaRec.Len.TDR_TOT_PROV_DA_REC_NULL));
        }
        // COB_CODE: IF IND-TDR-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO TDR-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndTitRat().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-IMP-AZ-NULL
            titRat.getTdrImpAz().setTdrImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrImpAz.Len.TDR_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-TDR-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO TDR-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndTitRat().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-IMP-ADER-NULL
            titRat.getTdrImpAder().setTdrImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrImpAder.Len.TDR_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-TDR-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO TDR-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndTitRat().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-IMP-TFR-NULL
            titRat.getTdrImpTfr().setTdrImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrImpTfr.Len.TDR_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-TDR-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO TDR-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndTitRat().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-IMP-VOLO-NULL
            titRat.getTdrImpVolo().setTdrImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrImpVolo.Len.TDR_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-TDR-FL-VLDT-TIT = -1
        //              MOVE HIGH-VALUES TO TDR-FL-VLDT-TIT-NULL
        //           END-IF
        if (ws.getIndTitRat().getFlVldtTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-FL-VLDT-TIT-NULL
            titRat.setTdrFlVldtTit(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TDR-TOT-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-CAR-ACQ-NULL
            titRat.getTdrTotCarAcq().setTdrTotCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotCarAcq.Len.TDR_TOT_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-CAR-GEST-NULL
            titRat.getTdrTotCarGest().setTdrTotCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotCarGest.Len.TDR_TOT_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-CAR-INC = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-CAR-INC-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-CAR-INC-NULL
            titRat.getTdrTotCarInc().setTdrTotCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotCarInc.Len.TDR_TOT_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-MANFEE-ANTIC-NULL
            titRat.getTdrTotManfeeAntic().setTdrTotManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotManfeeAntic.Len.TDR_TOT_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-MANFEE-RICOR-NULL
            titRat.getTdrTotManfeeRicor().setTdrTotManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotManfeeRicor.Len.TDR_TOT_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-MANFEE-REC = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-MANFEE-REC-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-MANFEE-REC-NULL
            titRat.getTdrTotManfeeRec().setTdrTotManfeeRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotManfeeRec.Len.TDR_TOT_MANFEE_REC_NULL));
        }
        // COB_CODE: IF IND-TDR-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO TDR-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndTitRat().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-IMP-TRASFE-NULL
            titRat.getTdrImpTrasfe().setTdrImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrImpTrasfe.Len.TDR_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-TDR-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO TDR-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndTitRat().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-IMP-TFR-STRC-NULL
            titRat.getTdrImpTfrStrc().setTdrImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrImpTfrStrc.Len.TDR_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-ACQ-EXP-NULL
            titRat.getTdrTotAcqExp().setTdrTotAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotAcqExp.Len.TDR_TOT_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-REMUN-ASS-NULL
            titRat.getTdrTotRemunAss().setTdrTotRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotRemunAss.Len.TDR_TOT_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-COMMIS-INTER-NULL
            titRat.getTdrTotCommisInter().setTdrTotCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotCommisInter.Len.TDR_TOT_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TDR-TOT-CNBT-ANTIRAC = -1
        //              MOVE HIGH-VALUES TO TDR-TOT-CNBT-ANTIRAC-NULL
        //           END-IF
        if (ws.getIndTitRat().getTotCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-TOT-CNBT-ANTIRAC-NULL
            titRat.getTdrTotCnbtAntirac().setTdrTotCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrTotCnbtAntirac.Len.TDR_TOT_CNBT_ANTIRAC_NULL));
        }
        // COB_CODE: IF IND-TDR-FL-INC-AUTOGEN = -1
        //              MOVE HIGH-VALUES TO TDR-FL-INC-AUTOGEN-NULL
        //           END-IF.
        if (ws.getIndTitRat().getFlIncAutogen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TDR-FL-INC-AUTOGEN-NULL
            titRat.setTdrFlIncAutogen(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO TDR-DS-OPER-SQL
        titRat.setTdrDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO TDR-DS-VER
        titRat.setTdrDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO TDR-DS-UTENTE
        titRat.setTdrDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO TDR-DS-STATO-ELAB.
        titRat.setTdrDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO TDR-DS-OPER-SQL
        titRat.setTdrDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO TDR-DS-UTENTE.
        titRat.setTdrDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF TDR-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-TDR-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrIdMoviChiu().getTdrIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-ID-MOVI-CHIU
            ws.getIndTitRat().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-ID-MOVI-CHIU
            ws.getIndTitRat().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF TDR-PROG-TIT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-PROG-TIT
        //           ELSE
        //              MOVE 0 TO IND-TDR-PROG-TIT
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrProgTit().getTdrProgTitNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-PROG-TIT
            ws.getIndTitRat().setProgTit(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-PROG-TIT
            ws.getIndTitRat().setProgTit(((short)0));
        }
        // COB_CODE: IF TDR-DT-INI-COP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-DT-INI-COP
        //           ELSE
        //              MOVE 0 TO IND-TDR-DT-INI-COP
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrDtIniCop().getTdrDtIniCopNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-DT-INI-COP
            ws.getIndTitRat().setDtIniCop(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-DT-INI-COP
            ws.getIndTitRat().setDtIniCop(((short)0));
        }
        // COB_CODE: IF TDR-DT-END-COP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-DT-END-COP
        //           ELSE
        //              MOVE 0 TO IND-TDR-DT-END-COP
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrDtEndCop().getTdrDtEndCopNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-DT-END-COP
            ws.getIndTitRat().setDtEndCop(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-DT-END-COP
            ws.getIndTitRat().setDtEndCop(((short)0));
        }
        // COB_CODE: IF TDR-IMP-PAG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-IMP-PAG
        //           ELSE
        //              MOVE 0 TO IND-TDR-IMP-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrImpPag().getTdrImpPagNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-IMP-PAG
            ws.getIndTitRat().setImpPag(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-IMP-PAG
            ws.getIndTitRat().setImpPag(((short)0));
        }
        // COB_CODE: IF TDR-FL-SOLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-FL-SOLL
        //           ELSE
        //              MOVE 0 TO IND-TDR-FL-SOLL
        //           END-IF
        if (Conditions.eq(titRat.getTdrFlSoll(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-TDR-FL-SOLL
            ws.getIndTitRat().setFlSoll(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-FL-SOLL
            ws.getIndTitRat().setFlSoll(((short)0));
        }
        // COB_CODE: IF TDR-FRAZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-FRAZ
        //           ELSE
        //              MOVE 0 TO IND-TDR-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrFraz().getTdrFrazNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-FRAZ
            ws.getIndTitRat().setFraz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-FRAZ
            ws.getIndTitRat().setFraz(((short)0));
        }
        // COB_CODE: IF TDR-DT-APPLZ-MORA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-DT-APPLZ-MORA
        //           ELSE
        //              MOVE 0 TO IND-TDR-DT-APPLZ-MORA
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrDtApplzMora().getTdrDtApplzMoraNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-DT-APPLZ-MORA
            ws.getIndTitRat().setDtApplzMora(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-DT-APPLZ-MORA
            ws.getIndTitRat().setDtApplzMora(((short)0));
        }
        // COB_CODE: IF TDR-FL-MORA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-FL-MORA
        //           ELSE
        //              MOVE 0 TO IND-TDR-FL-MORA
        //           END-IF
        if (Conditions.eq(titRat.getTdrFlMora(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-TDR-FL-MORA
            ws.getIndTitRat().setFlMora(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-FL-MORA
            ws.getIndTitRat().setFlMora(((short)0));
        }
        // COB_CODE: IF TDR-ID-RAPP-RETE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-ID-RAPP-RETE
        //           ELSE
        //              MOVE 0 TO IND-TDR-ID-RAPP-RETE
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrIdRappRete().getTdrIdRappReteNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-ID-RAPP-RETE
            ws.getIndTitRat().setIdRappRete(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-ID-RAPP-RETE
            ws.getIndTitRat().setIdRappRete(((short)0));
        }
        // COB_CODE: IF TDR-ID-RAPP-ANA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-ID-RAPP-ANA
        //           ELSE
        //              MOVE 0 TO IND-TDR-ID-RAPP-ANA
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrIdRappAna().getTdrIdRappAnaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-ID-RAPP-ANA
            ws.getIndTitRat().setIdRappAna(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-ID-RAPP-ANA
            ws.getIndTitRat().setIdRappAna(((short)0));
        }
        // COB_CODE: IF TDR-COD-DVS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-COD-DVS
        //           ELSE
        //              MOVE 0 TO IND-TDR-COD-DVS
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrCodDvsFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-COD-DVS
            ws.getIndTitRat().setCodDvs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-COD-DVS
            ws.getIndTitRat().setCodDvs(((short)0));
        }
        // COB_CODE: IF TDR-DT-ESI-TIT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-DT-ESI-TIT
        //           ELSE
        //              MOVE 0 TO IND-TDR-DT-ESI-TIT
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrDtEsiTit().getTdrDtEsiTitNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-DT-ESI-TIT
            ws.getIndTitRat().setDtEsiTit(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-DT-ESI-TIT
            ws.getIndTitRat().setDtEsiTit(((short)0));
        }
        // COB_CODE: IF TDR-TOT-PRE-NET-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-PRE-NET
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-PRE-NET
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotPreNet().getTdrTotPreNetNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-PRE-NET
            ws.getIndTitRat().setTotPreNet(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-PRE-NET
            ws.getIndTitRat().setTotPreNet(((short)0));
        }
        // COB_CODE: IF TDR-TOT-INTR-FRAZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-INTR-FRAZ
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-INTR-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotIntrFraz().getTdrTotIntrFrazNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-INTR-FRAZ
            ws.getIndTitRat().setTotIntrFraz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-INTR-FRAZ
            ws.getIndTitRat().setTotIntrFraz(((short)0));
        }
        // COB_CODE: IF TDR-TOT-INTR-MORA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-INTR-MORA
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-INTR-MORA
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotIntrMora().getTdrTotIntrMoraNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-INTR-MORA
            ws.getIndTitRat().setTotIntrMora(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-INTR-MORA
            ws.getIndTitRat().setTotIntrMora(((short)0));
        }
        // COB_CODE: IF TDR-TOT-INTR-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-INTR-PREST
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-INTR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotIntrPrest().getTdrTotIntrPrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-INTR-PREST
            ws.getIndTitRat().setTotIntrPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-INTR-PREST
            ws.getIndTitRat().setTotIntrPrest(((short)0));
        }
        // COB_CODE: IF TDR-TOT-INTR-RETDT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-INTR-RETDT
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-INTR-RETDT
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotIntrRetdt().getTdrTotIntrRetdtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-INTR-RETDT
            ws.getIndTitRat().setTotIntrRetdt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-INTR-RETDT
            ws.getIndTitRat().setTotIntrRetdt(((short)0));
        }
        // COB_CODE: IF TDR-TOT-INTR-RIAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-INTR-RIAT
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-INTR-RIAT
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotIntrRiat().getTdrTotIntrRiatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-INTR-RIAT
            ws.getIndTitRat().setTotIntrRiat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-INTR-RIAT
            ws.getIndTitRat().setTotIntrRiat(((short)0));
        }
        // COB_CODE: IF TDR-TOT-DIR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-DIR
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-DIR
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotDir().getTdrTotDirNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-DIR
            ws.getIndTitRat().setTotDir(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-DIR
            ws.getIndTitRat().setTotDir(((short)0));
        }
        // COB_CODE: IF TDR-TOT-SPE-MED-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-SPE-MED
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-SPE-MED
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotSpeMed().getTdrTotSpeMedNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-SPE-MED
            ws.getIndTitRat().setTotSpeMed(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-SPE-MED
            ws.getIndTitRat().setTotSpeMed(((short)0));
        }
        // COB_CODE: IF TDR-TOT-SPE-AGE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-SPE-AGE
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-SPE-AGE
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotSpeAge().getTdrTotSpeAgeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-SPE-AGE
            ws.getIndTitRat().setTotSpeAge(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-SPE-AGE
            ws.getIndTitRat().setTotSpeAge(((short)0));
        }
        // COB_CODE: IF TDR-TOT-TAX-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-TAX
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-TAX
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotTax().getTdrTotTaxNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-TAX
            ws.getIndTitRat().setTotTax(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-TAX
            ws.getIndTitRat().setTotTax(((short)0));
        }
        // COB_CODE: IF TDR-TOT-SOPR-SAN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-SOPR-SAN
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-SOPR-SAN
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotSoprSan().getTdrTotSoprSanNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-SOPR-SAN
            ws.getIndTitRat().setTotSoprSan(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-SOPR-SAN
            ws.getIndTitRat().setTotSoprSan(((short)0));
        }
        // COB_CODE: IF TDR-TOT-SOPR-TEC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-SOPR-TEC
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-SOPR-TEC
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotSoprTec().getTdrTotSoprTecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-SOPR-TEC
            ws.getIndTitRat().setTotSoprTec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-SOPR-TEC
            ws.getIndTitRat().setTotSoprTec(((short)0));
        }
        // COB_CODE: IF TDR-TOT-SOPR-SPO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-SOPR-SPO
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-SOPR-SPO
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotSoprSpo().getTdrTotSoprSpoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-SOPR-SPO
            ws.getIndTitRat().setTotSoprSpo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-SOPR-SPO
            ws.getIndTitRat().setTotSoprSpo(((short)0));
        }
        // COB_CODE: IF TDR-TOT-SOPR-PROF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-SOPR-PROF
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-SOPR-PROF
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotSoprProf().getTdrTotSoprProfNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-SOPR-PROF
            ws.getIndTitRat().setTotSoprProf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-SOPR-PROF
            ws.getIndTitRat().setTotSoprProf(((short)0));
        }
        // COB_CODE: IF TDR-TOT-SOPR-ALT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-SOPR-ALT
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-SOPR-ALT
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotSoprAlt().getTdrTotSoprAltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-SOPR-ALT
            ws.getIndTitRat().setTotSoprAlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-SOPR-ALT
            ws.getIndTitRat().setTotSoprAlt(((short)0));
        }
        // COB_CODE: IF TDR-TOT-PRE-TOT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-PRE-TOT
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-PRE-TOT
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotPreTot().getTdrTotPreTotNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-PRE-TOT
            ws.getIndTitRat().setTotPreTot(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-PRE-TOT
            ws.getIndTitRat().setTotPreTot(((short)0));
        }
        // COB_CODE: IF TDR-TOT-PRE-PP-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-PRE-PP-IAS
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-PRE-PP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotPrePpIas().getTdrTotPrePpIasNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-PRE-PP-IAS
            ws.getIndTitRat().setTotPrePpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-PRE-PP-IAS
            ws.getIndTitRat().setTotPrePpIas(((short)0));
        }
        // COB_CODE: IF TDR-TOT-CAR-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-CAR-IAS
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-CAR-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotCarIas().getTdrTotCarIasNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-CAR-IAS
            ws.getIndTitRat().setTotCarIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-CAR-IAS
            ws.getIndTitRat().setTotCarIas(((short)0));
        }
        // COB_CODE: IF TDR-TOT-PRE-SOLO-RSH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-PRE-SOLO-RSH
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-PRE-SOLO-RSH
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotPreSoloRsh().getTdrTotPreSoloRshNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-PRE-SOLO-RSH
            ws.getIndTitRat().setTotPreSoloRsh(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-PRE-SOLO-RSH
            ws.getIndTitRat().setTotPreSoloRsh(((short)0));
        }
        // COB_CODE: IF TDR-TOT-PROV-ACQ-1AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-PROV-ACQ-1AA
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-PROV-ACQ-1AA
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotProvAcq1aa().getTdrTotProvAcq1aaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-PROV-ACQ-1AA
            ws.getIndTitRat().setTotProvAcq1aa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-PROV-ACQ-1AA
            ws.getIndTitRat().setTotProvAcq1aa(((short)0));
        }
        // COB_CODE: IF TDR-TOT-PROV-ACQ-2AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-PROV-ACQ-2AA
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-PROV-ACQ-2AA
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotProvAcq2aa().getTdrTotProvAcq2aaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-PROV-ACQ-2AA
            ws.getIndTitRat().setTotProvAcq2aa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-PROV-ACQ-2AA
            ws.getIndTitRat().setTotProvAcq2aa(((short)0));
        }
        // COB_CODE: IF TDR-TOT-PROV-RICOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-PROV-RICOR
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-PROV-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotProvRicor().getTdrTotProvRicorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-PROV-RICOR
            ws.getIndTitRat().setTotProvRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-PROV-RICOR
            ws.getIndTitRat().setTotProvRicor(((short)0));
        }
        // COB_CODE: IF TDR-TOT-PROV-INC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-PROV-INC
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-PROV-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotProvInc().getTdrTotProvIncNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-PROV-INC
            ws.getIndTitRat().setTotProvInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-PROV-INC
            ws.getIndTitRat().setTotProvInc(((short)0));
        }
        // COB_CODE: IF TDR-TOT-PROV-DA-REC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-PROV-DA-REC
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-PROV-DA-REC
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotProvDaRec().getTdrTotProvDaRecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-PROV-DA-REC
            ws.getIndTitRat().setTotProvDaRec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-PROV-DA-REC
            ws.getIndTitRat().setTotProvDaRec(((short)0));
        }
        // COB_CODE: IF TDR-IMP-AZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-IMP-AZ
        //           ELSE
        //              MOVE 0 TO IND-TDR-IMP-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrImpAz().getTdrImpAzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-IMP-AZ
            ws.getIndTitRat().setImpAz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-IMP-AZ
            ws.getIndTitRat().setImpAz(((short)0));
        }
        // COB_CODE: IF TDR-IMP-ADER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-IMP-ADER
        //           ELSE
        //              MOVE 0 TO IND-TDR-IMP-ADER
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrImpAder().getTdrImpAderNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-IMP-ADER
            ws.getIndTitRat().setImpAder(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-IMP-ADER
            ws.getIndTitRat().setImpAder(((short)0));
        }
        // COB_CODE: IF TDR-IMP-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-IMP-TFR
        //           ELSE
        //              MOVE 0 TO IND-TDR-IMP-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrImpTfr().getTdrImpTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-IMP-TFR
            ws.getIndTitRat().setImpTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-IMP-TFR
            ws.getIndTitRat().setImpTfr(((short)0));
        }
        // COB_CODE: IF TDR-IMP-VOLO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-IMP-VOLO
        //           ELSE
        //              MOVE 0 TO IND-TDR-IMP-VOLO
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrImpVolo().getTdrImpVoloNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-IMP-VOLO
            ws.getIndTitRat().setImpVolo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-IMP-VOLO
            ws.getIndTitRat().setImpVolo(((short)0));
        }
        // COB_CODE: IF TDR-FL-VLDT-TIT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-FL-VLDT-TIT
        //           ELSE
        //              MOVE 0 TO IND-TDR-FL-VLDT-TIT
        //           END-IF
        if (Conditions.eq(titRat.getTdrFlVldtTit(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-TDR-FL-VLDT-TIT
            ws.getIndTitRat().setFlVldtTit(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-FL-VLDT-TIT
            ws.getIndTitRat().setFlVldtTit(((short)0));
        }
        // COB_CODE: IF TDR-TOT-CAR-ACQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-CAR-ACQ
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-CAR-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotCarAcq().getTdrTotCarAcqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-CAR-ACQ
            ws.getIndTitRat().setTotCarAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-CAR-ACQ
            ws.getIndTitRat().setTotCarAcq(((short)0));
        }
        // COB_CODE: IF TDR-TOT-CAR-GEST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-CAR-GEST
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-CAR-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotCarGest().getTdrTotCarGestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-CAR-GEST
            ws.getIndTitRat().setTotCarGest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-CAR-GEST
            ws.getIndTitRat().setTotCarGest(((short)0));
        }
        // COB_CODE: IF TDR-TOT-CAR-INC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-CAR-INC
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-CAR-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotCarInc().getTdrTotCarIncNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-CAR-INC
            ws.getIndTitRat().setTotCarInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-CAR-INC
            ws.getIndTitRat().setTotCarInc(((short)0));
        }
        // COB_CODE: IF TDR-TOT-MANFEE-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-MANFEE-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-MANFEE-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotManfeeAntic().getTdrTotManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-MANFEE-ANTIC
            ws.getIndTitRat().setTotManfeeAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-MANFEE-ANTIC
            ws.getIndTitRat().setTotManfeeAntic(((short)0));
        }
        // COB_CODE: IF TDR-TOT-MANFEE-RICOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-MANFEE-RICOR
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-MANFEE-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotManfeeRicor().getTdrTotManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-MANFEE-RICOR
            ws.getIndTitRat().setTotManfeeRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-MANFEE-RICOR
            ws.getIndTitRat().setTotManfeeRicor(((short)0));
        }
        // COB_CODE: IF TDR-TOT-MANFEE-REC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-MANFEE-REC
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-MANFEE-REC
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotManfeeRec().getTdrTotManfeeRecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-MANFEE-REC
            ws.getIndTitRat().setTotManfeeRec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-MANFEE-REC
            ws.getIndTitRat().setTotManfeeRec(((short)0));
        }
        // COB_CODE: IF TDR-IMP-TRASFE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-IMP-TRASFE
        //           ELSE
        //              MOVE 0 TO IND-TDR-IMP-TRASFE
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrImpTrasfe().getTdrImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-IMP-TRASFE
            ws.getIndTitRat().setImpTrasfe(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-IMP-TRASFE
            ws.getIndTitRat().setImpTrasfe(((short)0));
        }
        // COB_CODE: IF TDR-IMP-TFR-STRC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-IMP-TFR-STRC
        //           ELSE
        //              MOVE 0 TO IND-TDR-IMP-TFR-STRC
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrImpTfrStrc().getTdrImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-IMP-TFR-STRC
            ws.getIndTitRat().setImpTfrStrc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-IMP-TFR-STRC
            ws.getIndTitRat().setImpTfrStrc(((short)0));
        }
        // COB_CODE: IF TDR-TOT-ACQ-EXP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-ACQ-EXP
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-ACQ-EXP
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotAcqExp().getTdrTotAcqExpNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-ACQ-EXP
            ws.getIndTitRat().setTotAcqExp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-ACQ-EXP
            ws.getIndTitRat().setTotAcqExp(((short)0));
        }
        // COB_CODE: IF TDR-TOT-REMUN-ASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-REMUN-ASS
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-REMUN-ASS
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotRemunAss().getTdrTotRemunAssNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-REMUN-ASS
            ws.getIndTitRat().setTotRemunAss(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-REMUN-ASS
            ws.getIndTitRat().setTotRemunAss(((short)0));
        }
        // COB_CODE: IF TDR-TOT-COMMIS-INTER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-COMMIS-INTER
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-COMMIS-INTER
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotCommisInter().getTdrTotCommisInterNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-COMMIS-INTER
            ws.getIndTitRat().setTotCommisInter(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-COMMIS-INTER
            ws.getIndTitRat().setTotCommisInter(((short)0));
        }
        // COB_CODE: IF TDR-TOT-CNBT-ANTIRAC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-TOT-CNBT-ANTIRAC
        //           ELSE
        //              MOVE 0 TO IND-TDR-TOT-CNBT-ANTIRAC
        //           END-IF
        if (Characters.EQ_HIGH.test(titRat.getTdrTotCnbtAntirac().getTdrTotCnbtAntiracNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TDR-TOT-CNBT-ANTIRAC
            ws.getIndTitRat().setTotCnbtAntirac(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-TOT-CNBT-ANTIRAC
            ws.getIndTitRat().setTotCnbtAntirac(((short)0));
        }
        // COB_CODE: IF TDR-FL-INC-AUTOGEN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TDR-FL-INC-AUTOGEN
        //           ELSE
        //              MOVE 0 TO IND-TDR-FL-INC-AUTOGEN
        //           END-IF.
        if (Conditions.eq(titRat.getTdrFlIncAutogen(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-TDR-FL-INC-AUTOGEN
            ws.getIndTitRat().setFlIncAutogen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TDR-FL-INC-AUTOGEN
            ws.getIndTitRat().setFlIncAutogen(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : TDR-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE TIT-RAT TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(titRat.getTitRatFormatted());
        // COB_CODE: MOVE TDR-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(titRat.getTdrIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO TDR-ID-MOVI-CHIU
                titRat.getTdrIdMoviChiu().setTdrIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO TDR-DS-TS-END-CPTZ
                titRat.setTdrDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO TDR-ID-MOVI-CRZ
                    titRat.setTdrIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO TDR-ID-MOVI-CHIU-NULL
                    titRat.getTdrIdMoviChiu().setTdrIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrIdMoviChiu.Len.TDR_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO TDR-DT-END-EFF
                    titRat.setTdrDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO TDR-DS-TS-INI-CPTZ
                    titRat.setTdrDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO TDR-DS-TS-END-CPTZ
                    titRat.setTdrDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE TIT-RAT TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(titRat.getTitRatFormatted());
        // COB_CODE: MOVE TDR-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(titRat.getTdrIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO TIT-RAT.
        titRat.setTitRatFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO TDR-ID-MOVI-CRZ.
        titRat.setTdrIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO TDR-ID-MOVI-CHIU-NULL.
        titRat.getTdrIdMoviChiu().setTdrIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrIdMoviChiu.Len.TDR_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO TDR-DT-INI-EFF.
        titRat.setTdrDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO TDR-DT-END-EFF.
        titRat.setTdrDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO TDR-DS-TS-INI-CPTZ.
        titRat.setTdrDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO TDR-DS-TS-END-CPTZ.
        titRat.setTdrDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO TDR-COD-COMP-ANIA.
        titRat.setTdrCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE TDR-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(titRat.getTdrDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO TDR-DT-INI-EFF-DB
        ws.getTitRatDb().setNascCliDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE TDR-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(titRat.getTdrDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO TDR-DT-END-EFF-DB
        ws.getTitRatDb().setAcqsPersDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-TDR-DT-INI-COP = 0
        //               MOVE WS-DATE-X      TO TDR-DT-INI-COP-DB
        //           END-IF
        if (ws.getIndTitRat().getDtIniCop() == 0) {
            // COB_CODE: MOVE TDR-DT-INI-COP TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(titRat.getTdrDtIniCop().getTdrDtIniCop(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO TDR-DT-INI-COP-DB
            ws.getTitRatDb().setEndVldtPersDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-TDR-DT-END-COP = 0
        //               MOVE WS-DATE-X      TO TDR-DT-END-COP-DB
        //           END-IF
        if (ws.getIndTitRat().getDtEndCop() == 0) {
            // COB_CODE: MOVE TDR-DT-END-COP TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(titRat.getTdrDtEndCop().getTdrDtEndCop(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO TDR-DT-END-COP-DB
            ws.getTitRatDb().setDeadPersDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-TDR-DT-APPLZ-MORA = 0
        //               MOVE WS-DATE-X      TO TDR-DT-APPLZ-MORA-DB
        //           END-IF
        if (ws.getIndTitRat().getDtApplzMora() == 0) {
            // COB_CODE: MOVE TDR-DT-APPLZ-MORA TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(titRat.getTdrDtApplzMora().getTdrDtApplzMora(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO TDR-DT-APPLZ-MORA-DB
            ws.getTitRatDb().setBlocCliDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: MOVE TDR-DT-EMIS-TIT TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(titRat.getTdrDtEmisTit(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO TDR-DT-EMIS-TIT-DB
        ws.getTitRatDb().setDt1aAtvtDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-TDR-DT-ESI-TIT = 0
        //               MOVE WS-DATE-X      TO TDR-DT-ESI-TIT-DB
        //           END-IF.
        if (ws.getIndTitRat().getDtEsiTit() == 0) {
            // COB_CODE: MOVE TDR-DT-ESI-TIT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(titRat.getTdrDtEsiTit().getTdrDtEsiTit(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO TDR-DT-ESI-TIT-DB
            ws.getTitRatDb().setSegnalPartnerDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE TDR-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTitRatDb().getNascCliDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TDR-DT-INI-EFF
        titRat.setTdrDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE TDR-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTitRatDb().getAcqsPersDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TDR-DT-END-EFF
        titRat.setTdrDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-TDR-DT-INI-COP = 0
        //               MOVE WS-DATE-N      TO TDR-DT-INI-COP
        //           END-IF
        if (ws.getIndTitRat().getDtIniCop() == 0) {
            // COB_CODE: MOVE TDR-DT-INI-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitRatDb().getEndVldtPersDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TDR-DT-INI-COP
            titRat.getTdrDtIniCop().setTdrDtIniCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TDR-DT-END-COP = 0
        //               MOVE WS-DATE-N      TO TDR-DT-END-COP
        //           END-IF
        if (ws.getIndTitRat().getDtEndCop() == 0) {
            // COB_CODE: MOVE TDR-DT-END-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitRatDb().getDeadPersDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TDR-DT-END-COP
            titRat.getTdrDtEndCop().setTdrDtEndCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TDR-DT-APPLZ-MORA = 0
        //               MOVE WS-DATE-N      TO TDR-DT-APPLZ-MORA
        //           END-IF
        if (ws.getIndTitRat().getDtApplzMora() == 0) {
            // COB_CODE: MOVE TDR-DT-APPLZ-MORA-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitRatDb().getBlocCliDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TDR-DT-APPLZ-MORA
            titRat.getTdrDtApplzMora().setTdrDtApplzMora(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE TDR-DT-EMIS-TIT-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTitRatDb().getDt1aAtvtDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TDR-DT-EMIS-TIT
        titRat.setTdrDtEmisTit(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-TDR-DT-ESI-TIT = 0
        //               MOVE WS-DATE-N      TO TDR-DT-ESI-TIT
        //           END-IF.
        if (ws.getIndTitRat().getDtEsiTit() == 0) {
            // COB_CODE: MOVE TDR-DT-ESI-TIT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitRatDb().getSegnalPartnerDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TDR-DT-ESI-TIT
            titRat.getTdrDtEsiTit().setTdrDtEsiTit(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return titRat.getTdrCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.titRat.setTdrCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return titRat.getTdrCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.titRat.setTdrCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndTitRat().getCodDvs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndTitRat().setCodDvs(((short)0));
        }
        else {
            ws.getIndTitRat().setCodDvs(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return titRat.getTdrDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.titRat.setTdrDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return titRat.getTdrDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.titRat.setTdrDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return titRat.getTdrDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.titRat.setTdrDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return titRat.getTdrDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.titRat.setTdrDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return titRat.getTdrDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.titRat.setTdrDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return titRat.getTdrDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.titRat.setTdrDsVer(dsVer);
    }

    @Override
    public String getDtApplzMoraDb() {
        return ws.getTitRatDb().getBlocCliDb();
    }

    @Override
    public void setDtApplzMoraDb(String dtApplzMoraDb) {
        this.ws.getTitRatDb().setBlocCliDb(dtApplzMoraDb);
    }

    @Override
    public String getDtApplzMoraDbObj() {
        if (ws.getIndTitRat().getDtApplzMora() >= 0) {
            return getDtApplzMoraDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtApplzMoraDbObj(String dtApplzMoraDbObj) {
        if (dtApplzMoraDbObj != null) {
            setDtApplzMoraDb(dtApplzMoraDbObj);
            ws.getIndTitRat().setDtApplzMora(((short)0));
        }
        else {
            ws.getIndTitRat().setDtApplzMora(((short)-1));
        }
    }

    @Override
    public String getDtEmisTitDb() {
        return ws.getTitRatDb().getDt1aAtvtDb();
    }

    @Override
    public void setDtEmisTitDb(String dtEmisTitDb) {
        this.ws.getTitRatDb().setDt1aAtvtDb(dtEmisTitDb);
    }

    @Override
    public String getDtEndCopDb() {
        return ws.getTitRatDb().getDeadPersDb();
    }

    @Override
    public void setDtEndCopDb(String dtEndCopDb) {
        this.ws.getTitRatDb().setDeadPersDb(dtEndCopDb);
    }

    @Override
    public String getDtEndCopDbObj() {
        if (ws.getIndTitRat().getDtEndCop() >= 0) {
            return getDtEndCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndCopDbObj(String dtEndCopDbObj) {
        if (dtEndCopDbObj != null) {
            setDtEndCopDb(dtEndCopDbObj);
            ws.getIndTitRat().setDtEndCop(((short)0));
        }
        else {
            ws.getIndTitRat().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getTitRatDb().getAcqsPersDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getTitRatDb().setAcqsPersDb(dtEndEffDb);
    }

    @Override
    public String getDtEsiTitDb() {
        return ws.getTitRatDb().getSegnalPartnerDb();
    }

    @Override
    public void setDtEsiTitDb(String dtEsiTitDb) {
        this.ws.getTitRatDb().setSegnalPartnerDb(dtEsiTitDb);
    }

    @Override
    public String getDtEsiTitDbObj() {
        if (ws.getIndTitRat().getDtEsiTit() >= 0) {
            return getDtEsiTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEsiTitDbObj(String dtEsiTitDbObj) {
        if (dtEsiTitDbObj != null) {
            setDtEsiTitDb(dtEsiTitDbObj);
            ws.getIndTitRat().setDtEsiTit(((short)0));
        }
        else {
            ws.getIndTitRat().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getDtIniCopDb() {
        return ws.getTitRatDb().getEndVldtPersDb();
    }

    @Override
    public void setDtIniCopDb(String dtIniCopDb) {
        this.ws.getTitRatDb().setEndVldtPersDb(dtIniCopDb);
    }

    @Override
    public String getDtIniCopDbObj() {
        if (ws.getIndTitRat().getDtIniCop() >= 0) {
            return getDtIniCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniCopDbObj(String dtIniCopDbObj) {
        if (dtIniCopDbObj != null) {
            setDtIniCopDb(dtIniCopDbObj);
            ws.getIndTitRat().setDtIniCop(((short)0));
        }
        else {
            ws.getIndTitRat().setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getTitRatDb().getNascCliDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getTitRatDb().setNascCliDb(dtIniEffDb);
    }

    @Override
    public char getFlIncAutogen() {
        return titRat.getTdrFlIncAutogen();
    }

    @Override
    public void setFlIncAutogen(char flIncAutogen) {
        this.titRat.setTdrFlIncAutogen(flIncAutogen);
    }

    @Override
    public Character getFlIncAutogenObj() {
        if (ws.getIndTitRat().getFlIncAutogen() >= 0) {
            return ((Character)getFlIncAutogen());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlIncAutogenObj(Character flIncAutogenObj) {
        if (flIncAutogenObj != null) {
            setFlIncAutogen(((char)flIncAutogenObj));
            ws.getIndTitRat().setFlIncAutogen(((short)0));
        }
        else {
            ws.getIndTitRat().setFlIncAutogen(((short)-1));
        }
    }

    @Override
    public char getFlMora() {
        return titRat.getTdrFlMora();
    }

    @Override
    public void setFlMora(char flMora) {
        this.titRat.setTdrFlMora(flMora);
    }

    @Override
    public Character getFlMoraObj() {
        if (ws.getIndTitRat().getFlMora() >= 0) {
            return ((Character)getFlMora());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlMoraObj(Character flMoraObj) {
        if (flMoraObj != null) {
            setFlMora(((char)flMoraObj));
            ws.getIndTitRat().setFlMora(((short)0));
        }
        else {
            ws.getIndTitRat().setFlMora(((short)-1));
        }
    }

    @Override
    public char getFlSoll() {
        return titRat.getTdrFlSoll();
    }

    @Override
    public void setFlSoll(char flSoll) {
        this.titRat.setTdrFlSoll(flSoll);
    }

    @Override
    public Character getFlSollObj() {
        if (ws.getIndTitRat().getFlSoll() >= 0) {
            return ((Character)getFlSoll());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlSollObj(Character flSollObj) {
        if (flSollObj != null) {
            setFlSoll(((char)flSollObj));
            ws.getIndTitRat().setFlSoll(((short)0));
        }
        else {
            ws.getIndTitRat().setFlSoll(((short)-1));
        }
    }

    @Override
    public char getFlVldtTit() {
        return titRat.getTdrFlVldtTit();
    }

    @Override
    public void setFlVldtTit(char flVldtTit) {
        this.titRat.setTdrFlVldtTit(flVldtTit);
    }

    @Override
    public Character getFlVldtTitObj() {
        if (ws.getIndTitRat().getFlVldtTit() >= 0) {
            return ((Character)getFlVldtTit());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlVldtTitObj(Character flVldtTitObj) {
        if (flVldtTitObj != null) {
            setFlVldtTit(((char)flVldtTitObj));
            ws.getIndTitRat().setFlVldtTit(((short)0));
        }
        else {
            ws.getIndTitRat().setFlVldtTit(((short)-1));
        }
    }

    @Override
    public int getFraz() {
        return titRat.getTdrFraz().getTdrFraz();
    }

    @Override
    public void setFraz(int fraz) {
        this.titRat.getTdrFraz().setTdrFraz(fraz);
    }

    @Override
    public Integer getFrazObj() {
        if (ws.getIndTitRat().getFraz() >= 0) {
            return ((Integer)getFraz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazObj(Integer frazObj) {
        if (frazObj != null) {
            setFraz(((int)frazObj));
            ws.getIndTitRat().setFraz(((short)0));
        }
        else {
            ws.getIndTitRat().setFraz(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return titRat.getTdrIdMoviChiu().getTdrIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.titRat.getTdrIdMoviChiu().setTdrIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndTitRat().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndTitRat().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndTitRat().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return titRat.getTdrIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.titRat.setTdrIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdRappAna() {
        return titRat.getTdrIdRappAna().getTdrIdRappAna();
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        this.titRat.getTdrIdRappAna().setTdrIdRappAna(idRappAna);
    }

    @Override
    public Integer getIdRappAnaObj() {
        if (ws.getIndTitRat().getIdRappAna() >= 0) {
            return ((Integer)getIdRappAna());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRappAnaObj(Integer idRappAnaObj) {
        if (idRappAnaObj != null) {
            setIdRappAna(((int)idRappAnaObj));
            ws.getIndTitRat().setIdRappAna(((short)0));
        }
        else {
            ws.getIndTitRat().setIdRappAna(((short)-1));
        }
    }

    @Override
    public int getIdRappRete() {
        return titRat.getTdrIdRappRete().getTdrIdRappRete();
    }

    @Override
    public void setIdRappRete(int idRappRete) {
        this.titRat.getTdrIdRappRete().setTdrIdRappRete(idRappRete);
    }

    @Override
    public Integer getIdRappReteObj() {
        if (ws.getIndTitRat().getIdRappRete() >= 0) {
            return ((Integer)getIdRappRete());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRappReteObj(Integer idRappReteObj) {
        if (idRappReteObj != null) {
            setIdRappRete(((int)idRappReteObj));
            ws.getIndTitRat().setIdRappRete(((short)0));
        }
        else {
            ws.getIndTitRat().setIdRappRete(((short)-1));
        }
    }

    @Override
    public int getIdTitRat() {
        return titRat.getTdrIdTitRat();
    }

    @Override
    public void setIdTitRat(int idTitRat) {
        this.titRat.setTdrIdTitRat(idTitRat);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpAder() {
        return titRat.getTdrImpAder().getTdrImpAder();
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        this.titRat.getTdrImpAder().setTdrImpAder(impAder.copy());
    }

    @Override
    public AfDecimal getImpAderObj() {
        if (ws.getIndTitRat().getImpAder() >= 0) {
            return getImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        if (impAderObj != null) {
            setImpAder(new AfDecimal(impAderObj, 15, 3));
            ws.getIndTitRat().setImpAder(((short)0));
        }
        else {
            ws.getIndTitRat().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAz() {
        return titRat.getTdrImpAz().getTdrImpAz();
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        this.titRat.getTdrImpAz().setTdrImpAz(impAz.copy());
    }

    @Override
    public AfDecimal getImpAzObj() {
        if (ws.getIndTitRat().getImpAz() >= 0) {
            return getImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        if (impAzObj != null) {
            setImpAz(new AfDecimal(impAzObj, 15, 3));
            ws.getIndTitRat().setImpAz(((short)0));
        }
        else {
            ws.getIndTitRat().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPag() {
        return titRat.getTdrImpPag().getTdrImpPag();
    }

    @Override
    public void setImpPag(AfDecimal impPag) {
        this.titRat.getTdrImpPag().setTdrImpPag(impPag.copy());
    }

    @Override
    public AfDecimal getImpPagObj() {
        if (ws.getIndTitRat().getImpPag() >= 0) {
            return getImpPag();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPagObj(AfDecimal impPagObj) {
        if (impPagObj != null) {
            setImpPag(new AfDecimal(impPagObj, 15, 3));
            ws.getIndTitRat().setImpPag(((short)0));
        }
        else {
            ws.getIndTitRat().setImpPag(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfr() {
        return titRat.getTdrImpTfr().getTdrImpTfr();
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        this.titRat.getTdrImpTfr().setTdrImpTfr(impTfr.copy());
    }

    @Override
    public AfDecimal getImpTfrObj() {
        if (ws.getIndTitRat().getImpTfr() >= 0) {
            return getImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        if (impTfrObj != null) {
            setImpTfr(new AfDecimal(impTfrObj, 15, 3));
            ws.getIndTitRat().setImpTfr(((short)0));
        }
        else {
            ws.getIndTitRat().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfrStrc() {
        return titRat.getTdrImpTfrStrc().getTdrImpTfrStrc();
    }

    @Override
    public void setImpTfrStrc(AfDecimal impTfrStrc) {
        this.titRat.getTdrImpTfrStrc().setTdrImpTfrStrc(impTfrStrc.copy());
    }

    @Override
    public AfDecimal getImpTfrStrcObj() {
        if (ws.getIndTitRat().getImpTfrStrc() >= 0) {
            return getImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrStrcObj(AfDecimal impTfrStrcObj) {
        if (impTfrStrcObj != null) {
            setImpTfrStrc(new AfDecimal(impTfrStrcObj, 15, 3));
            ws.getIndTitRat().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndTitRat().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTrasfe() {
        return titRat.getTdrImpTrasfe().getTdrImpTrasfe();
    }

    @Override
    public void setImpTrasfe(AfDecimal impTrasfe) {
        this.titRat.getTdrImpTrasfe().setTdrImpTrasfe(impTrasfe.copy());
    }

    @Override
    public AfDecimal getImpTrasfeObj() {
        if (ws.getIndTitRat().getImpTrasfe() >= 0) {
            return getImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTrasfeObj(AfDecimal impTrasfeObj) {
        if (impTrasfeObj != null) {
            setImpTrasfe(new AfDecimal(impTrasfeObj, 15, 3));
            ws.getIndTitRat().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndTitRat().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpVolo() {
        return titRat.getTdrImpVolo().getTdrImpVolo();
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        this.titRat.getTdrImpVolo().setTdrImpVolo(impVolo.copy());
    }

    @Override
    public AfDecimal getImpVoloObj() {
        if (ws.getIndTitRat().getImpVolo() >= 0) {
            return getImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        if (impVoloObj != null) {
            setImpVolo(new AfDecimal(impVoloObj, 15, 3));
            ws.getIndTitRat().setImpVolo(((short)0));
        }
        else {
            ws.getIndTitRat().setImpVolo(((short)-1));
        }
    }

    @Override
    public int getProgTit() {
        return titRat.getTdrProgTit().getTdrProgTit();
    }

    @Override
    public void setProgTit(int progTit) {
        this.titRat.getTdrProgTit().setTdrProgTit(progTit);
    }

    @Override
    public Integer getProgTitObj() {
        if (ws.getIndTitRat().getProgTit() >= 0) {
            return ((Integer)getProgTit());
        }
        else {
            return null;
        }
    }

    @Override
    public void setProgTitObj(Integer progTitObj) {
        if (progTitObj != null) {
            setProgTit(((int)progTitObj));
            ws.getIndTitRat().setProgTit(((short)0));
        }
        else {
            ws.getIndTitRat().setProgTit(((short)-1));
        }
    }

    @Override
    public long getTdrDsRiga() {
        return titRat.getTdrDsRiga();
    }

    @Override
    public void setTdrDsRiga(long tdrDsRiga) {
        this.titRat.setTdrDsRiga(tdrDsRiga);
    }

    @Override
    public int getTdrIdOgg() {
        return titRat.getTdrIdOgg();
    }

    @Override
    public void setTdrIdOgg(int tdrIdOgg) {
        this.titRat.setTdrIdOgg(tdrIdOgg);
    }

    @Override
    public String getTdrTpOgg() {
        return titRat.getTdrTpOgg();
    }

    @Override
    public void setTdrTpOgg(String tdrTpOgg) {
        this.titRat.setTdrTpOgg(tdrTpOgg);
    }

    @Override
    public AfDecimal getTotAcqExp() {
        return titRat.getTdrTotAcqExp().getTdrTotAcqExp();
    }

    @Override
    public void setTotAcqExp(AfDecimal totAcqExp) {
        this.titRat.getTdrTotAcqExp().setTdrTotAcqExp(totAcqExp.copy());
    }

    @Override
    public AfDecimal getTotAcqExpObj() {
        if (ws.getIndTitRat().getTotAcqExp() >= 0) {
            return getTotAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotAcqExpObj(AfDecimal totAcqExpObj) {
        if (totAcqExpObj != null) {
            setTotAcqExp(new AfDecimal(totAcqExpObj, 15, 3));
            ws.getIndTitRat().setTotAcqExp(((short)0));
        }
        else {
            ws.getIndTitRat().setTotAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCarAcq() {
        return titRat.getTdrTotCarAcq().getTdrTotCarAcq();
    }

    @Override
    public void setTotCarAcq(AfDecimal totCarAcq) {
        this.titRat.getTdrTotCarAcq().setTdrTotCarAcq(totCarAcq.copy());
    }

    @Override
    public AfDecimal getTotCarAcqObj() {
        if (ws.getIndTitRat().getTotCarAcq() >= 0) {
            return getTotCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCarAcqObj(AfDecimal totCarAcqObj) {
        if (totCarAcqObj != null) {
            setTotCarAcq(new AfDecimal(totCarAcqObj, 15, 3));
            ws.getIndTitRat().setTotCarAcq(((short)0));
        }
        else {
            ws.getIndTitRat().setTotCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCarGest() {
        return titRat.getTdrTotCarGest().getTdrTotCarGest();
    }

    @Override
    public void setTotCarGest(AfDecimal totCarGest) {
        this.titRat.getTdrTotCarGest().setTdrTotCarGest(totCarGest.copy());
    }

    @Override
    public AfDecimal getTotCarGestObj() {
        if (ws.getIndTitRat().getTotCarGest() >= 0) {
            return getTotCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCarGestObj(AfDecimal totCarGestObj) {
        if (totCarGestObj != null) {
            setTotCarGest(new AfDecimal(totCarGestObj, 15, 3));
            ws.getIndTitRat().setTotCarGest(((short)0));
        }
        else {
            ws.getIndTitRat().setTotCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCarIas() {
        return titRat.getTdrTotCarIas().getTdrTotCarIas();
    }

    @Override
    public void setTotCarIas(AfDecimal totCarIas) {
        this.titRat.getTdrTotCarIas().setTdrTotCarIas(totCarIas.copy());
    }

    @Override
    public AfDecimal getTotCarIasObj() {
        if (ws.getIndTitRat().getTotCarIas() >= 0) {
            return getTotCarIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCarIasObj(AfDecimal totCarIasObj) {
        if (totCarIasObj != null) {
            setTotCarIas(new AfDecimal(totCarIasObj, 15, 3));
            ws.getIndTitRat().setTotCarIas(((short)0));
        }
        else {
            ws.getIndTitRat().setTotCarIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCarInc() {
        return titRat.getTdrTotCarInc().getTdrTotCarInc();
    }

    @Override
    public void setTotCarInc(AfDecimal totCarInc) {
        this.titRat.getTdrTotCarInc().setTdrTotCarInc(totCarInc.copy());
    }

    @Override
    public AfDecimal getTotCarIncObj() {
        if (ws.getIndTitRat().getTotCarInc() >= 0) {
            return getTotCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCarIncObj(AfDecimal totCarIncObj) {
        if (totCarIncObj != null) {
            setTotCarInc(new AfDecimal(totCarIncObj, 15, 3));
            ws.getIndTitRat().setTotCarInc(((short)0));
        }
        else {
            ws.getIndTitRat().setTotCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCnbtAntirac() {
        return titRat.getTdrTotCnbtAntirac().getTdrTotCnbtAntirac();
    }

    @Override
    public void setTotCnbtAntirac(AfDecimal totCnbtAntirac) {
        this.titRat.getTdrTotCnbtAntirac().setTdrTotCnbtAntirac(totCnbtAntirac.copy());
    }

    @Override
    public AfDecimal getTotCnbtAntiracObj() {
        if (ws.getIndTitRat().getTotCnbtAntirac() >= 0) {
            return getTotCnbtAntirac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCnbtAntiracObj(AfDecimal totCnbtAntiracObj) {
        if (totCnbtAntiracObj != null) {
            setTotCnbtAntirac(new AfDecimal(totCnbtAntiracObj, 15, 3));
            ws.getIndTitRat().setTotCnbtAntirac(((short)0));
        }
        else {
            ws.getIndTitRat().setTotCnbtAntirac(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCommisInter() {
        return titRat.getTdrTotCommisInter().getTdrTotCommisInter();
    }

    @Override
    public void setTotCommisInter(AfDecimal totCommisInter) {
        this.titRat.getTdrTotCommisInter().setTdrTotCommisInter(totCommisInter.copy());
    }

    @Override
    public AfDecimal getTotCommisInterObj() {
        if (ws.getIndTitRat().getTotCommisInter() >= 0) {
            return getTotCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCommisInterObj(AfDecimal totCommisInterObj) {
        if (totCommisInterObj != null) {
            setTotCommisInter(new AfDecimal(totCommisInterObj, 15, 3));
            ws.getIndTitRat().setTotCommisInter(((short)0));
        }
        else {
            ws.getIndTitRat().setTotCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotDir() {
        return titRat.getTdrTotDir().getTdrTotDir();
    }

    @Override
    public void setTotDir(AfDecimal totDir) {
        this.titRat.getTdrTotDir().setTdrTotDir(totDir.copy());
    }

    @Override
    public AfDecimal getTotDirObj() {
        if (ws.getIndTitRat().getTotDir() >= 0) {
            return getTotDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotDirObj(AfDecimal totDirObj) {
        if (totDirObj != null) {
            setTotDir(new AfDecimal(totDirObj, 15, 3));
            ws.getIndTitRat().setTotDir(((short)0));
        }
        else {
            ws.getIndTitRat().setTotDir(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrFraz() {
        return titRat.getTdrTotIntrFraz().getTdrTotIntrFraz();
    }

    @Override
    public void setTotIntrFraz(AfDecimal totIntrFraz) {
        this.titRat.getTdrTotIntrFraz().setTdrTotIntrFraz(totIntrFraz.copy());
    }

    @Override
    public AfDecimal getTotIntrFrazObj() {
        if (ws.getIndTitRat().getTotIntrFraz() >= 0) {
            return getTotIntrFraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrFrazObj(AfDecimal totIntrFrazObj) {
        if (totIntrFrazObj != null) {
            setTotIntrFraz(new AfDecimal(totIntrFrazObj, 15, 3));
            ws.getIndTitRat().setTotIntrFraz(((short)0));
        }
        else {
            ws.getIndTitRat().setTotIntrFraz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrMora() {
        return titRat.getTdrTotIntrMora().getTdrTotIntrMora();
    }

    @Override
    public void setTotIntrMora(AfDecimal totIntrMora) {
        this.titRat.getTdrTotIntrMora().setTdrTotIntrMora(totIntrMora.copy());
    }

    @Override
    public AfDecimal getTotIntrMoraObj() {
        if (ws.getIndTitRat().getTotIntrMora() >= 0) {
            return getTotIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrMoraObj(AfDecimal totIntrMoraObj) {
        if (totIntrMoraObj != null) {
            setTotIntrMora(new AfDecimal(totIntrMoraObj, 15, 3));
            ws.getIndTitRat().setTotIntrMora(((short)0));
        }
        else {
            ws.getIndTitRat().setTotIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrPrest() {
        return titRat.getTdrTotIntrPrest().getTdrTotIntrPrest();
    }

    @Override
    public void setTotIntrPrest(AfDecimal totIntrPrest) {
        this.titRat.getTdrTotIntrPrest().setTdrTotIntrPrest(totIntrPrest.copy());
    }

    @Override
    public AfDecimal getTotIntrPrestObj() {
        if (ws.getIndTitRat().getTotIntrPrest() >= 0) {
            return getTotIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrPrestObj(AfDecimal totIntrPrestObj) {
        if (totIntrPrestObj != null) {
            setTotIntrPrest(new AfDecimal(totIntrPrestObj, 15, 3));
            ws.getIndTitRat().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndTitRat().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrRetdt() {
        return titRat.getTdrTotIntrRetdt().getTdrTotIntrRetdt();
    }

    @Override
    public void setTotIntrRetdt(AfDecimal totIntrRetdt) {
        this.titRat.getTdrTotIntrRetdt().setTdrTotIntrRetdt(totIntrRetdt.copy());
    }

    @Override
    public AfDecimal getTotIntrRetdtObj() {
        if (ws.getIndTitRat().getTotIntrRetdt() >= 0) {
            return getTotIntrRetdt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrRetdtObj(AfDecimal totIntrRetdtObj) {
        if (totIntrRetdtObj != null) {
            setTotIntrRetdt(new AfDecimal(totIntrRetdtObj, 15, 3));
            ws.getIndTitRat().setTotIntrRetdt(((short)0));
        }
        else {
            ws.getIndTitRat().setTotIntrRetdt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrRiat() {
        return titRat.getTdrTotIntrRiat().getTdrTotIntrRiat();
    }

    @Override
    public void setTotIntrRiat(AfDecimal totIntrRiat) {
        this.titRat.getTdrTotIntrRiat().setTdrTotIntrRiat(totIntrRiat.copy());
    }

    @Override
    public AfDecimal getTotIntrRiatObj() {
        if (ws.getIndTitRat().getTotIntrRiat() >= 0) {
            return getTotIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrRiatObj(AfDecimal totIntrRiatObj) {
        if (totIntrRiatObj != null) {
            setTotIntrRiat(new AfDecimal(totIntrRiatObj, 15, 3));
            ws.getIndTitRat().setTotIntrRiat(((short)0));
        }
        else {
            ws.getIndTitRat().setTotIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotManfeeAntic() {
        return titRat.getTdrTotManfeeAntic().getTdrTotManfeeAntic();
    }

    @Override
    public void setTotManfeeAntic(AfDecimal totManfeeAntic) {
        this.titRat.getTdrTotManfeeAntic().setTdrTotManfeeAntic(totManfeeAntic.copy());
    }

    @Override
    public AfDecimal getTotManfeeAnticObj() {
        if (ws.getIndTitRat().getTotManfeeAntic() >= 0) {
            return getTotManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotManfeeAnticObj(AfDecimal totManfeeAnticObj) {
        if (totManfeeAnticObj != null) {
            setTotManfeeAntic(new AfDecimal(totManfeeAnticObj, 15, 3));
            ws.getIndTitRat().setTotManfeeAntic(((short)0));
        }
        else {
            ws.getIndTitRat().setTotManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotManfeeRec() {
        return titRat.getTdrTotManfeeRec().getTdrTotManfeeRec();
    }

    @Override
    public void setTotManfeeRec(AfDecimal totManfeeRec) {
        this.titRat.getTdrTotManfeeRec().setTdrTotManfeeRec(totManfeeRec.copy());
    }

    @Override
    public AfDecimal getTotManfeeRecObj() {
        if (ws.getIndTitRat().getTotManfeeRec() >= 0) {
            return getTotManfeeRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotManfeeRecObj(AfDecimal totManfeeRecObj) {
        if (totManfeeRecObj != null) {
            setTotManfeeRec(new AfDecimal(totManfeeRecObj, 15, 3));
            ws.getIndTitRat().setTotManfeeRec(((short)0));
        }
        else {
            ws.getIndTitRat().setTotManfeeRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotManfeeRicor() {
        return titRat.getTdrTotManfeeRicor().getTdrTotManfeeRicor();
    }

    @Override
    public void setTotManfeeRicor(AfDecimal totManfeeRicor) {
        this.titRat.getTdrTotManfeeRicor().setTdrTotManfeeRicor(totManfeeRicor.copy());
    }

    @Override
    public AfDecimal getTotManfeeRicorObj() {
        if (ws.getIndTitRat().getTotManfeeRicor() >= 0) {
            return getTotManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotManfeeRicorObj(AfDecimal totManfeeRicorObj) {
        if (totManfeeRicorObj != null) {
            setTotManfeeRicor(new AfDecimal(totManfeeRicorObj, 15, 3));
            ws.getIndTitRat().setTotManfeeRicor(((short)0));
        }
        else {
            ws.getIndTitRat().setTotManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotPreNet() {
        return titRat.getTdrTotPreNet().getTdrTotPreNet();
    }

    @Override
    public void setTotPreNet(AfDecimal totPreNet) {
        this.titRat.getTdrTotPreNet().setTdrTotPreNet(totPreNet.copy());
    }

    @Override
    public AfDecimal getTotPreNetObj() {
        if (ws.getIndTitRat().getTotPreNet() >= 0) {
            return getTotPreNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotPreNetObj(AfDecimal totPreNetObj) {
        if (totPreNetObj != null) {
            setTotPreNet(new AfDecimal(totPreNetObj, 15, 3));
            ws.getIndTitRat().setTotPreNet(((short)0));
        }
        else {
            ws.getIndTitRat().setTotPreNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotPrePpIas() {
        return titRat.getTdrTotPrePpIas().getTdrTotPrePpIas();
    }

    @Override
    public void setTotPrePpIas(AfDecimal totPrePpIas) {
        this.titRat.getTdrTotPrePpIas().setTdrTotPrePpIas(totPrePpIas.copy());
    }

    @Override
    public AfDecimal getTotPrePpIasObj() {
        if (ws.getIndTitRat().getTotPrePpIas() >= 0) {
            return getTotPrePpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotPrePpIasObj(AfDecimal totPrePpIasObj) {
        if (totPrePpIasObj != null) {
            setTotPrePpIas(new AfDecimal(totPrePpIasObj, 15, 3));
            ws.getIndTitRat().setTotPrePpIas(((short)0));
        }
        else {
            ws.getIndTitRat().setTotPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotPreSoloRsh() {
        return titRat.getTdrTotPreSoloRsh().getTdrTotPreSoloRsh();
    }

    @Override
    public void setTotPreSoloRsh(AfDecimal totPreSoloRsh) {
        this.titRat.getTdrTotPreSoloRsh().setTdrTotPreSoloRsh(totPreSoloRsh.copy());
    }

    @Override
    public AfDecimal getTotPreSoloRshObj() {
        if (ws.getIndTitRat().getTotPreSoloRsh() >= 0) {
            return getTotPreSoloRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotPreSoloRshObj(AfDecimal totPreSoloRshObj) {
        if (totPreSoloRshObj != null) {
            setTotPreSoloRsh(new AfDecimal(totPreSoloRshObj, 15, 3));
            ws.getIndTitRat().setTotPreSoloRsh(((short)0));
        }
        else {
            ws.getIndTitRat().setTotPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotPreTot() {
        return titRat.getTdrTotPreTot().getTdrTotPreTot();
    }

    @Override
    public void setTotPreTot(AfDecimal totPreTot) {
        this.titRat.getTdrTotPreTot().setTdrTotPreTot(totPreTot.copy());
    }

    @Override
    public AfDecimal getTotPreTotObj() {
        if (ws.getIndTitRat().getTotPreTot() >= 0) {
            return getTotPreTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotPreTotObj(AfDecimal totPreTotObj) {
        if (totPreTotObj != null) {
            setTotPreTot(new AfDecimal(totPreTotObj, 15, 3));
            ws.getIndTitRat().setTotPreTot(((short)0));
        }
        else {
            ws.getIndTitRat().setTotPreTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotProvAcq1aa() {
        return titRat.getTdrTotProvAcq1aa().getTdrTotProvAcq1aa();
    }

    @Override
    public void setTotProvAcq1aa(AfDecimal totProvAcq1aa) {
        this.titRat.getTdrTotProvAcq1aa().setTdrTotProvAcq1aa(totProvAcq1aa.copy());
    }

    @Override
    public AfDecimal getTotProvAcq1aaObj() {
        if (ws.getIndTitRat().getTotProvAcq1aa() >= 0) {
            return getTotProvAcq1aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotProvAcq1aaObj(AfDecimal totProvAcq1aaObj) {
        if (totProvAcq1aaObj != null) {
            setTotProvAcq1aa(new AfDecimal(totProvAcq1aaObj, 15, 3));
            ws.getIndTitRat().setTotProvAcq1aa(((short)0));
        }
        else {
            ws.getIndTitRat().setTotProvAcq1aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotProvAcq2aa() {
        return titRat.getTdrTotProvAcq2aa().getTdrTotProvAcq2aa();
    }

    @Override
    public void setTotProvAcq2aa(AfDecimal totProvAcq2aa) {
        this.titRat.getTdrTotProvAcq2aa().setTdrTotProvAcq2aa(totProvAcq2aa.copy());
    }

    @Override
    public AfDecimal getTotProvAcq2aaObj() {
        if (ws.getIndTitRat().getTotProvAcq2aa() >= 0) {
            return getTotProvAcq2aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotProvAcq2aaObj(AfDecimal totProvAcq2aaObj) {
        if (totProvAcq2aaObj != null) {
            setTotProvAcq2aa(new AfDecimal(totProvAcq2aaObj, 15, 3));
            ws.getIndTitRat().setTotProvAcq2aa(((short)0));
        }
        else {
            ws.getIndTitRat().setTotProvAcq2aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotProvDaRec() {
        return titRat.getTdrTotProvDaRec().getTdrTotProvDaRec();
    }

    @Override
    public void setTotProvDaRec(AfDecimal totProvDaRec) {
        this.titRat.getTdrTotProvDaRec().setTdrTotProvDaRec(totProvDaRec.copy());
    }

    @Override
    public AfDecimal getTotProvDaRecObj() {
        if (ws.getIndTitRat().getTotProvDaRec() >= 0) {
            return getTotProvDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotProvDaRecObj(AfDecimal totProvDaRecObj) {
        if (totProvDaRecObj != null) {
            setTotProvDaRec(new AfDecimal(totProvDaRecObj, 15, 3));
            ws.getIndTitRat().setTotProvDaRec(((short)0));
        }
        else {
            ws.getIndTitRat().setTotProvDaRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotProvInc() {
        return titRat.getTdrTotProvInc().getTdrTotProvInc();
    }

    @Override
    public void setTotProvInc(AfDecimal totProvInc) {
        this.titRat.getTdrTotProvInc().setTdrTotProvInc(totProvInc.copy());
    }

    @Override
    public AfDecimal getTotProvIncObj() {
        if (ws.getIndTitRat().getTotProvInc() >= 0) {
            return getTotProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotProvIncObj(AfDecimal totProvIncObj) {
        if (totProvIncObj != null) {
            setTotProvInc(new AfDecimal(totProvIncObj, 15, 3));
            ws.getIndTitRat().setTotProvInc(((short)0));
        }
        else {
            ws.getIndTitRat().setTotProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotProvRicor() {
        return titRat.getTdrTotProvRicor().getTdrTotProvRicor();
    }

    @Override
    public void setTotProvRicor(AfDecimal totProvRicor) {
        this.titRat.getTdrTotProvRicor().setTdrTotProvRicor(totProvRicor.copy());
    }

    @Override
    public AfDecimal getTotProvRicorObj() {
        if (ws.getIndTitRat().getTotProvRicor() >= 0) {
            return getTotProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotProvRicorObj(AfDecimal totProvRicorObj) {
        if (totProvRicorObj != null) {
            setTotProvRicor(new AfDecimal(totProvRicorObj, 15, 3));
            ws.getIndTitRat().setTotProvRicor(((short)0));
        }
        else {
            ws.getIndTitRat().setTotProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotRemunAss() {
        return titRat.getTdrTotRemunAss().getTdrTotRemunAss();
    }

    @Override
    public void setTotRemunAss(AfDecimal totRemunAss) {
        this.titRat.getTdrTotRemunAss().setTdrTotRemunAss(totRemunAss.copy());
    }

    @Override
    public AfDecimal getTotRemunAssObj() {
        if (ws.getIndTitRat().getTotRemunAss() >= 0) {
            return getTotRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotRemunAssObj(AfDecimal totRemunAssObj) {
        if (totRemunAssObj != null) {
            setTotRemunAss(new AfDecimal(totRemunAssObj, 15, 3));
            ws.getIndTitRat().setTotRemunAss(((short)0));
        }
        else {
            ws.getIndTitRat().setTotRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSoprAlt() {
        return titRat.getTdrTotSoprAlt().getTdrTotSoprAlt();
    }

    @Override
    public void setTotSoprAlt(AfDecimal totSoprAlt) {
        this.titRat.getTdrTotSoprAlt().setTdrTotSoprAlt(totSoprAlt.copy());
    }

    @Override
    public AfDecimal getTotSoprAltObj() {
        if (ws.getIndTitRat().getTotSoprAlt() >= 0) {
            return getTotSoprAlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSoprAltObj(AfDecimal totSoprAltObj) {
        if (totSoprAltObj != null) {
            setTotSoprAlt(new AfDecimal(totSoprAltObj, 15, 3));
            ws.getIndTitRat().setTotSoprAlt(((short)0));
        }
        else {
            ws.getIndTitRat().setTotSoprAlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSoprProf() {
        return titRat.getTdrTotSoprProf().getTdrTotSoprProf();
    }

    @Override
    public void setTotSoprProf(AfDecimal totSoprProf) {
        this.titRat.getTdrTotSoprProf().setTdrTotSoprProf(totSoprProf.copy());
    }

    @Override
    public AfDecimal getTotSoprProfObj() {
        if (ws.getIndTitRat().getTotSoprProf() >= 0) {
            return getTotSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSoprProfObj(AfDecimal totSoprProfObj) {
        if (totSoprProfObj != null) {
            setTotSoprProf(new AfDecimal(totSoprProfObj, 15, 3));
            ws.getIndTitRat().setTotSoprProf(((short)0));
        }
        else {
            ws.getIndTitRat().setTotSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSoprSan() {
        return titRat.getTdrTotSoprSan().getTdrTotSoprSan();
    }

    @Override
    public void setTotSoprSan(AfDecimal totSoprSan) {
        this.titRat.getTdrTotSoprSan().setTdrTotSoprSan(totSoprSan.copy());
    }

    @Override
    public AfDecimal getTotSoprSanObj() {
        if (ws.getIndTitRat().getTotSoprSan() >= 0) {
            return getTotSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSoprSanObj(AfDecimal totSoprSanObj) {
        if (totSoprSanObj != null) {
            setTotSoprSan(new AfDecimal(totSoprSanObj, 15, 3));
            ws.getIndTitRat().setTotSoprSan(((short)0));
        }
        else {
            ws.getIndTitRat().setTotSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSoprSpo() {
        return titRat.getTdrTotSoprSpo().getTdrTotSoprSpo();
    }

    @Override
    public void setTotSoprSpo(AfDecimal totSoprSpo) {
        this.titRat.getTdrTotSoprSpo().setTdrTotSoprSpo(totSoprSpo.copy());
    }

    @Override
    public AfDecimal getTotSoprSpoObj() {
        if (ws.getIndTitRat().getTotSoprSpo() >= 0) {
            return getTotSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSoprSpoObj(AfDecimal totSoprSpoObj) {
        if (totSoprSpoObj != null) {
            setTotSoprSpo(new AfDecimal(totSoprSpoObj, 15, 3));
            ws.getIndTitRat().setTotSoprSpo(((short)0));
        }
        else {
            ws.getIndTitRat().setTotSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSoprTec() {
        return titRat.getTdrTotSoprTec().getTdrTotSoprTec();
    }

    @Override
    public void setTotSoprTec(AfDecimal totSoprTec) {
        this.titRat.getTdrTotSoprTec().setTdrTotSoprTec(totSoprTec.copy());
    }

    @Override
    public AfDecimal getTotSoprTecObj() {
        if (ws.getIndTitRat().getTotSoprTec() >= 0) {
            return getTotSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSoprTecObj(AfDecimal totSoprTecObj) {
        if (totSoprTecObj != null) {
            setTotSoprTec(new AfDecimal(totSoprTecObj, 15, 3));
            ws.getIndTitRat().setTotSoprTec(((short)0));
        }
        else {
            ws.getIndTitRat().setTotSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSpeAge() {
        return titRat.getTdrTotSpeAge().getTdrTotSpeAge();
    }

    @Override
    public void setTotSpeAge(AfDecimal totSpeAge) {
        this.titRat.getTdrTotSpeAge().setTdrTotSpeAge(totSpeAge.copy());
    }

    @Override
    public AfDecimal getTotSpeAgeObj() {
        if (ws.getIndTitRat().getTotSpeAge() >= 0) {
            return getTotSpeAge();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSpeAgeObj(AfDecimal totSpeAgeObj) {
        if (totSpeAgeObj != null) {
            setTotSpeAge(new AfDecimal(totSpeAgeObj, 15, 3));
            ws.getIndTitRat().setTotSpeAge(((short)0));
        }
        else {
            ws.getIndTitRat().setTotSpeAge(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSpeMed() {
        return titRat.getTdrTotSpeMed().getTdrTotSpeMed();
    }

    @Override
    public void setTotSpeMed(AfDecimal totSpeMed) {
        this.titRat.getTdrTotSpeMed().setTdrTotSpeMed(totSpeMed.copy());
    }

    @Override
    public AfDecimal getTotSpeMedObj() {
        if (ws.getIndTitRat().getTotSpeMed() >= 0) {
            return getTotSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSpeMedObj(AfDecimal totSpeMedObj) {
        if (totSpeMedObj != null) {
            setTotSpeMed(new AfDecimal(totSpeMedObj, 15, 3));
            ws.getIndTitRat().setTotSpeMed(((short)0));
        }
        else {
            ws.getIndTitRat().setTotSpeMed(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotTax() {
        return titRat.getTdrTotTax().getTdrTotTax();
    }

    @Override
    public void setTotTax(AfDecimal totTax) {
        this.titRat.getTdrTotTax().setTdrTotTax(totTax.copy());
    }

    @Override
    public AfDecimal getTotTaxObj() {
        if (ws.getIndTitRat().getTotTax() >= 0) {
            return getTotTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotTaxObj(AfDecimal totTaxObj) {
        if (totTaxObj != null) {
            setTotTax(new AfDecimal(totTaxObj, 15, 3));
            ws.getIndTitRat().setTotTax(((short)0));
        }
        else {
            ws.getIndTitRat().setTotTax(((short)-1));
        }
    }

    @Override
    public String getTpPreTit() {
        return titRat.getTdrTpPreTit();
    }

    @Override
    public void setTpPreTit(String tpPreTit) {
        this.titRat.setTdrTpPreTit(tpPreTit);
    }

    @Override
    public String getTpStatTit() {
        return titRat.getTdrTpStatTit();
    }

    @Override
    public void setTpStatTit(String tpStatTit) {
        this.titRat.setTdrTpStatTit(tpStatTit);
    }

    @Override
    public String getTpTit() {
        return titRat.getTdrTpTit();
    }

    @Override
    public void setTpTit(String tpTit) {
        this.titRat.setTdrTpTit(tpTit);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
