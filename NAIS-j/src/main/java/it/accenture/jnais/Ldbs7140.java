package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DettTitCont1Dao;
import it.accenture.jnais.commons.data.to.IDettTitCont1;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs7140Data;
import it.accenture.jnais.ws.redefines.TitDtApplzMora;
import it.accenture.jnais.ws.redefines.TitDtCambioVlt;
import it.accenture.jnais.ws.redefines.TitDtCertFisc;
import it.accenture.jnais.ws.redefines.TitDtEmisTit;
import it.accenture.jnais.ws.redefines.TitDtEndCop;
import it.accenture.jnais.ws.redefines.TitDtEsiTit;
import it.accenture.jnais.ws.redefines.TitDtIniCop;
import it.accenture.jnais.ws.redefines.TitDtRichAddRid;
import it.accenture.jnais.ws.redefines.TitDtVlt;
import it.accenture.jnais.ws.redefines.TitFraz;
import it.accenture.jnais.ws.redefines.TitIdMoviChiu;
import it.accenture.jnais.ws.redefines.TitIdRappAna;
import it.accenture.jnais.ws.redefines.TitIdRappRete;
import it.accenture.jnais.ws.redefines.TitImpAder;
import it.accenture.jnais.ws.redefines.TitImpAz;
import it.accenture.jnais.ws.redefines.TitImpPag;
import it.accenture.jnais.ws.redefines.TitImpTfr;
import it.accenture.jnais.ws.redefines.TitImpTfrStrc;
import it.accenture.jnais.ws.redefines.TitImpTrasfe;
import it.accenture.jnais.ws.redefines.TitImpVolo;
import it.accenture.jnais.ws.redefines.TitNumRatAccorpate;
import it.accenture.jnais.ws.redefines.TitProgTit;
import it.accenture.jnais.ws.redefines.TitTotAcqExp;
import it.accenture.jnais.ws.redefines.TitTotCarAcq;
import it.accenture.jnais.ws.redefines.TitTotCarGest;
import it.accenture.jnais.ws.redefines.TitTotCarIas;
import it.accenture.jnais.ws.redefines.TitTotCarInc;
import it.accenture.jnais.ws.redefines.TitTotCnbtAntirac;
import it.accenture.jnais.ws.redefines.TitTotCommisInter;
import it.accenture.jnais.ws.redefines.TitTotDir;
import it.accenture.jnais.ws.redefines.TitTotIntrFraz;
import it.accenture.jnais.ws.redefines.TitTotIntrMora;
import it.accenture.jnais.ws.redefines.TitTotIntrPrest;
import it.accenture.jnais.ws.redefines.TitTotIntrRetdt;
import it.accenture.jnais.ws.redefines.TitTotIntrRiat;
import it.accenture.jnais.ws.redefines.TitTotManfeeAntic;
import it.accenture.jnais.ws.redefines.TitTotManfeeRec;
import it.accenture.jnais.ws.redefines.TitTotManfeeRicor;
import it.accenture.jnais.ws.redefines.TitTotPreNet;
import it.accenture.jnais.ws.redefines.TitTotPrePpIas;
import it.accenture.jnais.ws.redefines.TitTotPreSoloRsh;
import it.accenture.jnais.ws.redefines.TitTotPreTot;
import it.accenture.jnais.ws.redefines.TitTotProvAcq1aa;
import it.accenture.jnais.ws.redefines.TitTotProvAcq2aa;
import it.accenture.jnais.ws.redefines.TitTotProvDaRec;
import it.accenture.jnais.ws.redefines.TitTotProvInc;
import it.accenture.jnais.ws.redefines.TitTotProvRicor;
import it.accenture.jnais.ws.redefines.TitTotRemunAss;
import it.accenture.jnais.ws.redefines.TitTotSoprAlt;
import it.accenture.jnais.ws.redefines.TitTotSoprProf;
import it.accenture.jnais.ws.redefines.TitTotSoprSan;
import it.accenture.jnais.ws.redefines.TitTotSoprSpo;
import it.accenture.jnais.ws.redefines.TitTotSoprTec;
import it.accenture.jnais.ws.redefines.TitTotSpeAge;
import it.accenture.jnais.ws.redefines.TitTotSpeMed;
import it.accenture.jnais.ws.redefines.TitTotTax;
import it.accenture.jnais.ws.redefines.TitTpCausStor;
import it.accenture.jnais.ws.TitContIdbstit0;

/**Original name: LDBS7140<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  07 DIC 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs7140 extends Program implements IDettTitCont1 {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DettTitCont1Dao dettTitCont1Dao = new DettTitCont1Dao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs7140Data ws = new Ldbs7140Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: TIT-CONT
    private TitContIdbstit0 titCont;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS7140_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, TitContIdbstit0 titCont) {
        this.idsv0003 = idsv0003;
        this.titCont = titCont;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV7141.
        ws.getLdbv7141().setLdbv7141Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: MOVE LDBV7141 TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getLdbv7141().getLdbv7141Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs7140 getInstance() {
        return ((Ldbs7140)Programs.getInstance(Ldbs7140.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS7140'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS7140");
        // COB_CODE: MOVE 'TIT-CONT' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("TIT-CONT");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     A.ID_TIT_CONT
        //                    ,A.ID_OGG
        //                    ,A.TP_OGG
        //                    ,A.IB_RICH
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.TP_TIT
        //                    ,A.PROG_TIT
        //                    ,A.TP_PRE_TIT
        //                    ,A.TP_STAT_TIT
        //                    ,A.DT_INI_COP
        //                    ,A.DT_END_COP
        //                    ,A.IMP_PAG
        //                    ,A.FL_SOLL
        //                    ,A.FRAZ
        //                    ,A.DT_APPLZ_MORA
        //                    ,A.FL_MORA
        //                    ,A.ID_RAPP_RETE
        //                    ,A.ID_RAPP_ANA
        //                    ,A.COD_DVS
        //                    ,A.DT_EMIS_TIT
        //                    ,A.DT_ESI_TIT
        //                    ,A.TOT_PRE_NET
        //                    ,A.TOT_INTR_FRAZ
        //                    ,A.TOT_INTR_MORA
        //                    ,A.TOT_INTR_PREST
        //                    ,A.TOT_INTR_RETDT
        //                    ,A.TOT_INTR_RIAT
        //                    ,A.TOT_DIR
        //                    ,A.TOT_SPE_MED
        //                    ,A.TOT_TAX
        //                    ,A.TOT_SOPR_SAN
        //                    ,A.TOT_SOPR_TEC
        //                    ,A.TOT_SOPR_SPO
        //                    ,A.TOT_SOPR_PROF
        //                    ,A.TOT_SOPR_ALT
        //                    ,A.TOT_PRE_TOT
        //                    ,A.TOT_PRE_PP_IAS
        //                    ,A.TOT_CAR_ACQ
        //                    ,A.TOT_CAR_GEST
        //                    ,A.TOT_CAR_INC
        //                    ,A.TOT_PRE_SOLO_RSH
        //                    ,A.TOT_PROV_ACQ_1AA
        //                    ,A.TOT_PROV_ACQ_2AA
        //                    ,A.TOT_PROV_RICOR
        //                    ,A.TOT_PROV_INC
        //                    ,A.TOT_PROV_DA_REC
        //                    ,A.IMP_AZ
        //                    ,A.IMP_ADER
        //                    ,A.IMP_TFR
        //                    ,A.IMP_VOLO
        //                    ,A.TOT_MANFEE_ANTIC
        //                    ,A.TOT_MANFEE_RICOR
        //                    ,A.TOT_MANFEE_REC
        //                    ,A.TP_MEZ_PAG_ADD
        //                    ,A.ESTR_CNT_CORR_ADD
        //                    ,A.DT_VLT
        //                    ,A.FL_FORZ_DT_VLT
        //                    ,A.DT_CAMBIO_VLT
        //                    ,A.TOT_SPE_AGE
        //                    ,A.TOT_CAR_IAS
        //                    ,A.NUM_RAT_ACCORPATE
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.FL_TIT_DA_REINVST
        //                    ,A.DT_RICH_ADD_RID
        //                    ,A.TP_ESI_RID
        //                    ,A.COD_IBAN
        //                    ,A.IMP_TRASFE
        //                    ,A.IMP_TFR_STRC
        //                    ,A.DT_CERT_FISC
        //                    ,A.TP_CAUS_STOR
        //                    ,A.TP_CAUS_DISP_STOR
        //                    ,A.TP_TIT_MIGRAZ
        //                    ,A.TOT_ACQ_EXP
        //                    ,A.TOT_REMUN_ASS
        //                    ,A.TOT_COMMIS_INTER
        //                    ,A.TP_CAUS_RIMB
        //                    ,A.TOT_CNBT_ANTIRAC
        //                    ,A.FL_INC_AUTOGEN
        //              FROM TIT_CONT A,
        //                   DETT_TIT_CONT B
        //              WHERE      B.TP_OGG          =  :LDBV7141-TP-OGG
        //                    AND B.ID_OGG          =  :LDBV7141-ID-OGG
        //                    AND B.ID_TIT_CONT     =   A.ID_TIT_CONT
        //                    AND A.TP_STAT_TIT  IN ( :LDBV7141-TP-STA-TIT1 ,
        //                                            :LDBV7141-TP-STA-TIT2 )
        //                    AND A.TP_TIT          =  :LDBV7141-TP-TIT
        //                    AND A.DT_INI_COP     <=  :LDBV7141-DT-A-DB
        //                    AND A.DT_INI_COP     >=  :LDBV7141-DT-DA-DB
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_TIT_CONT
        //                    ,A.ID_OGG
        //                    ,A.TP_OGG
        //                    ,A.IB_RICH
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.TP_TIT
        //                    ,A.PROG_TIT
        //                    ,A.TP_PRE_TIT
        //                    ,A.TP_STAT_TIT
        //                    ,A.DT_INI_COP
        //                    ,A.DT_END_COP
        //                    ,A.IMP_PAG
        //                    ,A.FL_SOLL
        //                    ,A.FRAZ
        //                    ,A.DT_APPLZ_MORA
        //                    ,A.FL_MORA
        //                    ,A.ID_RAPP_RETE
        //                    ,A.ID_RAPP_ANA
        //                    ,A.COD_DVS
        //                    ,A.DT_EMIS_TIT
        //                    ,A.DT_ESI_TIT
        //                    ,A.TOT_PRE_NET
        //                    ,A.TOT_INTR_FRAZ
        //                    ,A.TOT_INTR_MORA
        //                    ,A.TOT_INTR_PREST
        //                    ,A.TOT_INTR_RETDT
        //                    ,A.TOT_INTR_RIAT
        //                    ,A.TOT_DIR
        //                    ,A.TOT_SPE_MED
        //                    ,A.TOT_TAX
        //                    ,A.TOT_SOPR_SAN
        //                    ,A.TOT_SOPR_TEC
        //                    ,A.TOT_SOPR_SPO
        //                    ,A.TOT_SOPR_PROF
        //                    ,A.TOT_SOPR_ALT
        //                    ,A.TOT_PRE_TOT
        //                    ,A.TOT_PRE_PP_IAS
        //                    ,A.TOT_CAR_ACQ
        //                    ,A.TOT_CAR_GEST
        //                    ,A.TOT_CAR_INC
        //                    ,A.TOT_PRE_SOLO_RSH
        //                    ,A.TOT_PROV_ACQ_1AA
        //                    ,A.TOT_PROV_ACQ_2AA
        //                    ,A.TOT_PROV_RICOR
        //                    ,A.TOT_PROV_INC
        //                    ,A.TOT_PROV_DA_REC
        //                    ,A.IMP_AZ
        //                    ,A.IMP_ADER
        //                    ,A.IMP_TFR
        //                    ,A.IMP_VOLO
        //                    ,A.TOT_MANFEE_ANTIC
        //                    ,A.TOT_MANFEE_RICOR
        //                    ,A.TOT_MANFEE_REC
        //                    ,A.TP_MEZ_PAG_ADD
        //                    ,A.ESTR_CNT_CORR_ADD
        //                    ,A.DT_VLT
        //                    ,A.FL_FORZ_DT_VLT
        //                    ,A.DT_CAMBIO_VLT
        //                    ,A.TOT_SPE_AGE
        //                    ,A.TOT_CAR_IAS
        //                    ,A.NUM_RAT_ACCORPATE
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.FL_TIT_DA_REINVST
        //                    ,A.DT_RICH_ADD_RID
        //                    ,A.TP_ESI_RID
        //                    ,A.COD_IBAN
        //                    ,A.IMP_TRASFE
        //                    ,A.IMP_TFR_STRC
        //                    ,A.DT_CERT_FISC
        //                    ,A.TP_CAUS_STOR
        //                    ,A.TP_CAUS_DISP_STOR
        //                    ,A.TP_TIT_MIGRAZ
        //                    ,A.TOT_ACQ_EXP
        //                    ,A.TOT_REMUN_ASS
        //                    ,A.TOT_COMMIS_INTER
        //                    ,A.TP_CAUS_RIMB
        //                    ,A.TOT_CNBT_ANTIRAC
        //                    ,A.FL_INC_AUTOGEN
        //             INTO
        //                :TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //             FROM TIT_CONT A,
        //                  DETT_TIT_CONT B
        //             WHERE      B.TP_OGG          =  :LDBV7141-TP-OGG
        //                    AND B.ID_OGG          =  :LDBV7141-ID-OGG
        //                    AND B.ID_TIT_CONT     =   A.ID_TIT_CONT
        //                    AND A.TP_STAT_TIT  IN ( :LDBV7141-TP-STA-TIT1 ,
        //                                            :LDBV7141-TP-STA-TIT2 )
        //                    AND A.TP_TIT          =  :LDBV7141-TP-TIT
        //                    AND A.DT_INI_COP     <=  :LDBV7141-DT-A-DB
        //                    AND A.DT_INI_COP     >=  :LDBV7141-DT-DA-DB
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dettTitCont1Dao.selectRec6(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        dettTitCont1Dao.openCEff35(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        dettTitCont1Dao.closeCEff35();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //           END-EXEC.
        dettTitCont1Dao.fetchCEff35(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     A.ID_TIT_CONT
        //                    ,A.ID_OGG
        //                    ,A.TP_OGG
        //                    ,A.IB_RICH
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.TP_TIT
        //                    ,A.PROG_TIT
        //                    ,A.TP_PRE_TIT
        //                    ,A.TP_STAT_TIT
        //                    ,A.DT_INI_COP
        //                    ,A.DT_END_COP
        //                    ,A.IMP_PAG
        //                    ,A.FL_SOLL
        //                    ,A.FRAZ
        //                    ,A.DT_APPLZ_MORA
        //                    ,A.FL_MORA
        //                    ,A.ID_RAPP_RETE
        //                    ,A.ID_RAPP_ANA
        //                    ,A.COD_DVS
        //                    ,A.DT_EMIS_TIT
        //                    ,A.DT_ESI_TIT
        //                    ,A.TOT_PRE_NET
        //                    ,A.TOT_INTR_FRAZ
        //                    ,A.TOT_INTR_MORA
        //                    ,A.TOT_INTR_PREST
        //                    ,A.TOT_INTR_RETDT
        //                    ,A.TOT_INTR_RIAT
        //                    ,A.TOT_DIR
        //                    ,A.TOT_SPE_MED
        //                    ,A.TOT_TAX
        //                    ,A.TOT_SOPR_SAN
        //                    ,A.TOT_SOPR_TEC
        //                    ,A.TOT_SOPR_SPO
        //                    ,A.TOT_SOPR_PROF
        //                    ,A.TOT_SOPR_ALT
        //                    ,A.TOT_PRE_TOT
        //                    ,A.TOT_PRE_PP_IAS
        //                    ,A.TOT_CAR_ACQ
        //                    ,A.TOT_CAR_GEST
        //                    ,A.TOT_CAR_INC
        //                    ,A.TOT_PRE_SOLO_RSH
        //                    ,A.TOT_PROV_ACQ_1AA
        //                    ,A.TOT_PROV_ACQ_2AA
        //                    ,A.TOT_PROV_RICOR
        //                    ,A.TOT_PROV_INC
        //                    ,A.TOT_PROV_DA_REC
        //                    ,A.IMP_AZ
        //                    ,A.IMP_ADER
        //                    ,A.IMP_TFR
        //                    ,A.IMP_VOLO
        //                    ,A.TOT_MANFEE_ANTIC
        //                    ,A.TOT_MANFEE_RICOR
        //                    ,A.TOT_MANFEE_REC
        //                    ,A.TP_MEZ_PAG_ADD
        //                    ,A.ESTR_CNT_CORR_ADD
        //                    ,A.DT_VLT
        //                    ,A.FL_FORZ_DT_VLT
        //                    ,A.DT_CAMBIO_VLT
        //                    ,A.TOT_SPE_AGE
        //                    ,A.TOT_CAR_IAS
        //                    ,A.NUM_RAT_ACCORPATE
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.FL_TIT_DA_REINVST
        //                    ,A.DT_RICH_ADD_RID
        //                    ,A.TP_ESI_RID
        //                    ,A.COD_IBAN
        //                    ,A.IMP_TRASFE
        //                    ,A.IMP_TFR_STRC
        //                    ,A.DT_CERT_FISC
        //                    ,A.TP_CAUS_STOR
        //                    ,A.TP_CAUS_DISP_STOR
        //                    ,A.TP_TIT_MIGRAZ
        //                    ,A.TOT_ACQ_EXP
        //                    ,A.TOT_REMUN_ASS
        //                    ,A.TOT_COMMIS_INTER
        //                    ,A.TP_CAUS_RIMB
        //                    ,A.TOT_CNBT_ANTIRAC
        //                    ,A.FL_INC_AUTOGEN
        //              FROM TIT_CONT A,
        //                   DETT_TIT_CONT B
        //              WHERE      B.TP_OGG          =  :LDBV7141-TP-OGG
        //                        AND B.ID_OGG          =  :LDBV7141-ID-OGG
        //                        AND B.ID_TIT_CONT     =   A.ID_TIT_CONT
        //                        AND A.TP_STAT_TIT  IN ( :LDBV7141-TP-STA-TIT1 ,
        //                                                :LDBV7141-TP-STA-TIT2 )
        //                        AND A.TP_TIT          =  :LDBV7141-TP-TIT
        //                        AND A.DT_INI_COP     <=  :LDBV7141-DT-A-DB
        //                        AND A.DT_INI_COP     >=  :LDBV7141-DT-DA-DB
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_TIT_CONT
        //                    ,A.ID_OGG
        //                    ,A.TP_OGG
        //                    ,A.IB_RICH
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.TP_TIT
        //                    ,A.PROG_TIT
        //                    ,A.TP_PRE_TIT
        //                    ,A.TP_STAT_TIT
        //                    ,A.DT_INI_COP
        //                    ,A.DT_END_COP
        //                    ,A.IMP_PAG
        //                    ,A.FL_SOLL
        //                    ,A.FRAZ
        //                    ,A.DT_APPLZ_MORA
        //                    ,A.FL_MORA
        //                    ,A.ID_RAPP_RETE
        //                    ,A.ID_RAPP_ANA
        //                    ,A.COD_DVS
        //                    ,A.DT_EMIS_TIT
        //                    ,A.DT_ESI_TIT
        //                    ,A.TOT_PRE_NET
        //                    ,A.TOT_INTR_FRAZ
        //                    ,A.TOT_INTR_MORA
        //                    ,A.TOT_INTR_PREST
        //                    ,A.TOT_INTR_RETDT
        //                    ,A.TOT_INTR_RIAT
        //                    ,A.TOT_DIR
        //                    ,A.TOT_SPE_MED
        //                    ,A.TOT_TAX
        //                    ,A.TOT_SOPR_SAN
        //                    ,A.TOT_SOPR_TEC
        //                    ,A.TOT_SOPR_SPO
        //                    ,A.TOT_SOPR_PROF
        //                    ,A.TOT_SOPR_ALT
        //                    ,A.TOT_PRE_TOT
        //                    ,A.TOT_PRE_PP_IAS
        //                    ,A.TOT_CAR_ACQ
        //                    ,A.TOT_CAR_GEST
        //                    ,A.TOT_CAR_INC
        //                    ,A.TOT_PRE_SOLO_RSH
        //                    ,A.TOT_PROV_ACQ_1AA
        //                    ,A.TOT_PROV_ACQ_2AA
        //                    ,A.TOT_PROV_RICOR
        //                    ,A.TOT_PROV_INC
        //                    ,A.TOT_PROV_DA_REC
        //                    ,A.IMP_AZ
        //                    ,A.IMP_ADER
        //                    ,A.IMP_TFR
        //                    ,A.IMP_VOLO
        //                    ,A.TOT_MANFEE_ANTIC
        //                    ,A.TOT_MANFEE_RICOR
        //                    ,A.TOT_MANFEE_REC
        //                    ,A.TP_MEZ_PAG_ADD
        //                    ,A.ESTR_CNT_CORR_ADD
        //                    ,A.DT_VLT
        //                    ,A.FL_FORZ_DT_VLT
        //                    ,A.DT_CAMBIO_VLT
        //                    ,A.TOT_SPE_AGE
        //                    ,A.TOT_CAR_IAS
        //                    ,A.NUM_RAT_ACCORPATE
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.FL_TIT_DA_REINVST
        //                    ,A.DT_RICH_ADD_RID
        //                    ,A.TP_ESI_RID
        //                    ,A.COD_IBAN
        //                    ,A.IMP_TRASFE
        //                    ,A.IMP_TFR_STRC
        //                    ,A.DT_CERT_FISC
        //                    ,A.TP_CAUS_STOR
        //                    ,A.TP_CAUS_DISP_STOR
        //                    ,A.TP_TIT_MIGRAZ
        //                    ,A.TOT_ACQ_EXP
        //                    ,A.TOT_REMUN_ASS
        //                    ,A.TOT_COMMIS_INTER
        //                    ,A.TP_CAUS_RIMB
        //                    ,A.TOT_CNBT_ANTIRAC
        //                    ,A.FL_INC_AUTOGEN
        //             INTO
        //                :TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //             FROM TIT_CONT A,
        //                  DETT_TIT_CONT B
        //             WHERE      B.TP_OGG          =  :LDBV7141-TP-OGG
        //                    AND B.ID_OGG          =  :LDBV7141-ID-OGG
        //                    AND B.ID_TIT_CONT     =   A.ID_TIT_CONT
        //                    AND A.TP_STAT_TIT  IN ( :LDBV7141-TP-STA-TIT1 ,
        //                                            :LDBV7141-TP-STA-TIT2 )
        //                    AND A.TP_TIT          =  :LDBV7141-TP-TIT
        //                    AND A.DT_INI_COP     <=  :LDBV7141-DT-A-DB
        //                    AND A.DT_INI_COP     >=  :LDBV7141-DT-DA-DB
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dettTitCont1Dao.selectRec7(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        dettTitCont1Dao.openCCpz35(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        dettTitCont1Dao.closeCCpz35();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //           END-EXEC.
        dettTitCont1Dao.fetchCCpz35(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-TIT-IB-RICH = -1
        //              MOVE HIGH-VALUES TO TIT-IB-RICH-NULL
        //           END-IF
        if (ws.getIndTitCont().getIbRich() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IB-RICH-NULL
            titCont.setTitIbRich(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_IB_RICH));
        }
        // COB_CODE: IF IND-TIT-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO TIT-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-MOVI-CHIU-NULL
            titCont.getTitIdMoviChiu().setTitIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdMoviChiu.Len.TIT_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-TIT-PROG-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-PROG-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getProgTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-PROG-TIT-NULL
            titCont.getTitProgTit().setTitProgTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitProgTit.Len.TIT_PROG_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-INI-COP = -1
        //              MOVE HIGH-VALUES TO TIT-DT-INI-COP-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-INI-COP-NULL
            titCont.getTitDtIniCop().setTitDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtIniCop.Len.TIT_DT_INI_COP_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-END-COP = -1
        //              MOVE HIGH-VALUES TO TIT-DT-END-COP-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-END-COP-NULL
            titCont.getTitDtEndCop().setTitDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEndCop.Len.TIT_DT_END_COP_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-PAG = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-PAG-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-PAG-NULL
            titCont.getTitImpPag().setTitImpPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpPag.Len.TIT_IMP_PAG_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-SOLL = -1
        //              MOVE HIGH-VALUES TO TIT-FL-SOLL-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlSoll() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-SOLL-NULL
            titCont.setTitFlSoll(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-FRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-FRAZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FRAZ-NULL
            titCont.getTitFraz().setTitFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitFraz.Len.TIT_FRAZ_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-APPLZ-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-DT-APPLZ-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtApplzMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-APPLZ-MORA-NULL
            titCont.getTitDtApplzMora().setTitDtApplzMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtApplzMora.Len.TIT_DT_APPLZ_MORA_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-FL-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-MORA-NULL
            titCont.setTitFlMora(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-ID-RAPP-RETE = -1
        //              MOVE HIGH-VALUES TO TIT-ID-RAPP-RETE-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdRappRete() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-RAPP-RETE-NULL
            titCont.getTitIdRappRete().setTitIdRappReteNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdRappRete.Len.TIT_ID_RAPP_RETE_NULL));
        }
        // COB_CODE: IF IND-TIT-ID-RAPP-ANA = -1
        //              MOVE HIGH-VALUES TO TIT-ID-RAPP-ANA-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdRappAna() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-RAPP-ANA-NULL
            titCont.getTitIdRappAna().setTitIdRappAnaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdRappAna.Len.TIT_ID_RAPP_ANA_NULL));
        }
        // COB_CODE: IF IND-TIT-COD-DVS = -1
        //              MOVE HIGH-VALUES TO TIT-COD-DVS-NULL
        //           END-IF
        if (ws.getIndTitCont().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-COD-DVS-NULL
            titCont.setTitCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_COD_DVS));
        }
        // COB_CODE: IF IND-TIT-DT-EMIS-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-EMIS-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEmisTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-EMIS-TIT-NULL
            titCont.getTitDtEmisTit().setTitDtEmisTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEmisTit.Len.TIT_DT_EMIS_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-ESI-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-ESI-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-ESI-TIT-NULL
            titCont.getTitDtEsiTit().setTitDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEsiTit.Len.TIT_DT_ESI_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-NET = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-NET-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-NET-NULL
            titCont.getTitTotPreNet().setTitTotPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreNet.Len.TIT_TOT_PRE_NET_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-FRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-FRAZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-FRAZ-NULL
            titCont.getTitTotIntrFraz().setTitTotIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrFraz.Len.TIT_TOT_INTR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-MORA-NULL
            titCont.getTitTotIntrMora().setTitTotIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrMora.Len.TIT_TOT_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-PREST-NULL
            titCont.getTitTotIntrPrest().setTitTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrPrest.Len.TIT_TOT_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-RETDT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-RETDT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-RETDT-NULL
            titCont.getTitTotIntrRetdt().setTitTotIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrRetdt.Len.TIT_TOT_INTR_RETDT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-RIAT-NULL
            titCont.getTitTotIntrRiat().setTitTotIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrRiat.Len.TIT_TOT_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-DIR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-DIR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-DIR-NULL
            titCont.getTitTotDir().setTitTotDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotDir.Len.TIT_TOT_DIR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SPE-MED = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SPE-MED-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SPE-MED-NULL
            titCont.getTitTotSpeMed().setTitTotSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSpeMed.Len.TIT_TOT_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-TAX = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-TAX-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-TAX-NULL
            titCont.getTitTotTax().setTitTotTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotTax.Len.TIT_TOT_TAX_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-SAN-NULL
            titCont.getTitTotSoprSan().setTitTotSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprSan.Len.TIT_TOT_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-TEC-NULL
            titCont.getTitTotSoprTec().setTitTotSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprTec.Len.TIT_TOT_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-SPO-NULL
            titCont.getTitTotSoprSpo().setTitTotSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprSpo.Len.TIT_TOT_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-PROF-NULL
            titCont.getTitTotSoprProf().setTitTotSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprProf.Len.TIT_TOT_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-ALT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-ALT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-ALT-NULL
            titCont.getTitTotSoprAlt().setTitTotSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprAlt.Len.TIT_TOT_SOPR_ALT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-TOT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-TOT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-TOT-NULL
            titCont.getTitTotPreTot().setTitTotPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreTot.Len.TIT_TOT_PRE_TOT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-PP-IAS = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-PP-IAS-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-PP-IAS-NULL
            titCont.getTitTotPrePpIas().setTitTotPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPrePpIas.Len.TIT_TOT_PRE_PP_IAS_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-ACQ-NULL
            titCont.getTitTotCarAcq().setTitTotCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarAcq.Len.TIT_TOT_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-GEST-NULL
            titCont.getTitTotCarGest().setTitTotCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarGest.Len.TIT_TOT_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-INC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-INC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-INC-NULL
            titCont.getTitTotCarInc().setTitTotCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarInc.Len.TIT_TOT_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-SOLO-RSH = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-SOLO-RSH-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-SOLO-RSH-NULL
            titCont.getTitTotPreSoloRsh().setTitTotPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreSoloRsh.Len.TIT_TOT_PRE_SOLO_RSH_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-ACQ-1AA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-1AA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-1AA-NULL
            titCont.getTitTotProvAcq1aa().setTitTotProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvAcq1aa.Len.TIT_TOT_PROV_ACQ1AA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-ACQ-2AA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-2AA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-2AA-NULL
            titCont.getTitTotProvAcq2aa().setTitTotProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvAcq2aa.Len.TIT_TOT_PROV_ACQ2AA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-RICOR-NULL
            titCont.getTitTotProvRicor().setTitTotProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvRicor.Len.TIT_TOT_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-INC-NULL
            titCont.getTitTotProvInc().setTitTotProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvInc.Len.TIT_TOT_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-DA-REC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-DA-REC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-DA-REC-NULL
            titCont.getTitTotProvDaRec().setTitTotProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvDaRec.Len.TIT_TOT_PROV_DA_REC_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-AZ-NULL
            titCont.getTitImpAz().setTitImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpAz.Len.TIT_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-ADER-NULL
            titCont.getTitImpAder().setTitImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpAder.Len.TIT_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TFR-NULL
            titCont.getTitImpTfr().setTitImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTfr.Len.TIT_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-VOLO-NULL
            titCont.getTitImpVolo().setTitImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpVolo.Len.TIT_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-ANTIC-NULL
            titCont.getTitTotManfeeAntic().setTitTotManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeAntic.Len.TIT_TOT_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-RICOR-NULL
            titCont.getTitTotManfeeRicor().setTitTotManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeRicor.Len.TIT_TOT_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-REC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-REC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-REC-NULL
            titCont.getTitTotManfeeRec().setTitTotManfeeRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeRec.Len.TIT_TOT_MANFEE_REC_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-MEZ-PAG-ADD = -1
        //              MOVE HIGH-VALUES TO TIT-TP-MEZ-PAG-ADD-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpMezPagAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-MEZ-PAG-ADD-NULL
            titCont.setTitTpMezPagAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_TP_MEZ_PAG_ADD));
        }
        // COB_CODE: IF IND-TIT-ESTR-CNT-CORR-ADD = -1
        //              MOVE HIGH-VALUES TO TIT-ESTR-CNT-CORR-ADD-NULL
        //           END-IF
        if (ws.getIndTitCont().getEstrCntCorrAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ESTR-CNT-CORR-ADD-NULL
            titCont.setTitEstrCntCorrAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_ESTR_CNT_CORR_ADD));
        }
        // COB_CODE: IF IND-TIT-DT-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-VLT-NULL
            titCont.getTitDtVlt().setTitDtVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtVlt.Len.TIT_DT_VLT_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-FORZ-DT-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-FL-FORZ-DT-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlForzDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-FORZ-DT-VLT-NULL
            titCont.setTitFlForzDtVlt(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-DT-CAMBIO-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-CAMBIO-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtCambioVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-CAMBIO-VLT-NULL
            titCont.getTitDtCambioVlt().setTitDtCambioVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtCambioVlt.Len.TIT_DT_CAMBIO_VLT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SPE-AGE = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SPE-AGE-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SPE-AGE-NULL
            titCont.getTitTotSpeAge().setTitTotSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSpeAge.Len.TIT_TOT_SPE_AGE_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-IAS = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-IAS-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-IAS-NULL
            titCont.getTitTotCarIas().setTitTotCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarIas.Len.TIT_TOT_CAR_IAS_NULL));
        }
        // COB_CODE: IF IND-TIT-NUM-RAT-ACCORPATE = -1
        //              MOVE HIGH-VALUES TO TIT-NUM-RAT-ACCORPATE-NULL
        //           END-IF
        if (ws.getIndTitCont().getNumRatAccorpate() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-NUM-RAT-ACCORPATE-NULL
            titCont.getTitNumRatAccorpate().setTitNumRatAccorpateNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitNumRatAccorpate.Len.TIT_NUM_RAT_ACCORPATE_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-TIT-DA-REINVST = -1
        //              MOVE HIGH-VALUES TO TIT-FL-TIT-DA-REINVST-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlTitDaReinvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-TIT-DA-REINVST-NULL
            titCont.setTitFlTitDaReinvst(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-DT-RICH-ADD-RID = -1
        //              MOVE HIGH-VALUES TO TIT-DT-RICH-ADD-RID-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtRichAddRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-RICH-ADD-RID-NULL
            titCont.getTitDtRichAddRid().setTitDtRichAddRidNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtRichAddRid.Len.TIT_DT_RICH_ADD_RID_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-ESI-RID = -1
        //              MOVE HIGH-VALUES TO TIT-TP-ESI-RID-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpEsiRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-ESI-RID-NULL
            titCont.setTitTpEsiRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_TP_ESI_RID));
        }
        // COB_CODE: IF IND-TIT-COD-IBAN = -1
        //              MOVE HIGH-VALUES TO TIT-COD-IBAN-NULL
        //           END-IF
        if (ws.getIndTitCont().getCodIban() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-COD-IBAN-NULL
            titCont.setTitCodIban(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_COD_IBAN));
        }
        // COB_CODE: IF IND-TIT-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TRASFE-NULL
            titCont.getTitImpTrasfe().setTitImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTrasfe.Len.TIT_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TFR-STRC-NULL
            titCont.getTitImpTfrStrc().setTitImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTfrStrc.Len.TIT_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-CERT-FISC = -1
        //              MOVE HIGH-VALUES TO TIT-DT-CERT-FISC-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtCertFisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-CERT-FISC-NULL
            titCont.getTitDtCertFisc().setTitDtCertFiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtCertFisc.Len.TIT_DT_CERT_FISC_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-CAUS-STOR = -1
        //              MOVE HIGH-VALUES TO TIT-TP-CAUS-STOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpCausStor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-CAUS-STOR-NULL
            titCont.getTitTpCausStor().setTitTpCausStorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTpCausStor.Len.TIT_TP_CAUS_STOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-CAUS-DISP-STOR = -1
        //              MOVE HIGH-VALUES TO TIT-TP-CAUS-DISP-STOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpCausDispStor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-CAUS-DISP-STOR-NULL
            titCont.setTitTpCausDispStor(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_TP_CAUS_DISP_STOR));
        }
        // COB_CODE: IF IND-TIT-TP-TIT-MIGRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-TP-TIT-MIGRAZ-NULL
        //           END-IF.
        if (ws.getIndTitCont().getTpTitMigraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-TIT-MIGRAZ-NULL
            titCont.setTitTpTitMigraz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_TP_TIT_MIGRAZ));
        }
        // COB_CODE: IF IND-TIT-TOT-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-ACQ-EXP-NULL
        //           END-IF.
        if (ws.getIndTitCont().getTotAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-ACQ-EXP-NULL
            titCont.getTitTotAcqExp().setTitTotAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotAcqExp.Len.TIT_TOT_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-REMUN-ASS-NULL
        //           END-IF.
        if (ws.getIndTitCont().getTotRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-REMUN-ASS-NULL
            titCont.getTitTotRemunAss().setTitTotRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotRemunAss.Len.TIT_TOT_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-COMMIS-INTER-NULL
        //           END-IF.
        if (ws.getIndTitCont().getTotCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-COMMIS-INTER-NULL
            titCont.getTitTotCommisInter().setTitTotCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCommisInter.Len.TIT_TOT_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-CAUS-RIMB = -1
        //              MOVE HIGH-VALUES TO TIT-TP-CAUS-RIMB-NULL
        //           END-IF.
        if (ws.getIndTitCont().getTpCausRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-CAUS-RIMB-NULL
            titCont.setTitTpCausRimb(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_TP_CAUS_RIMB));
        }
        // COB_CODE: IF IND-TIT-TOT-CNBT-ANTIRAC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CNBT-ANTIRAC-NULL
        //           END-IF.
        if (ws.getIndTitCont().getTotCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CNBT-ANTIRAC-NULL
            titCont.getTitTotCnbtAntirac().setTitTotCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCnbtAntirac.Len.TIT_TOT_CNBT_ANTIRAC_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-INC-AUTOGEN = -1
        //              MOVE HIGH-VALUES TO TIT-FL-INC-AUTOGEN-NULL
        //           END-IF.
        if (ws.getIndTitCont().getFlIncAutogen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-INC-AUTOGEN-NULL
            titCont.setTitFlIncAutogen(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE TIT-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTitContDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-INI-EFF
        titCont.setTitDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE TIT-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTitContDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-END-EFF
        titCont.setTitDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-TIT-DT-INI-COP = 0
        //               MOVE WS-DATE-N      TO TIT-DT-INI-COP
        //           END-IF
        if (ws.getIndTitCont().getDtIniCop() == 0) {
            // COB_CODE: MOVE TIT-DT-INI-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-INI-COP
            titCont.getTitDtIniCop().setTitDtIniCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-END-COP = 0
        //               MOVE WS-DATE-N      TO TIT-DT-END-COP
        //           END-IF
        if (ws.getIndTitCont().getDtEndCop() == 0) {
            // COB_CODE: MOVE TIT-DT-END-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-END-COP
            titCont.getTitDtEndCop().setTitDtEndCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-APPLZ-MORA = 0
        //               MOVE WS-DATE-N      TO TIT-DT-APPLZ-MORA
        //           END-IF
        if (ws.getIndTitCont().getDtApplzMora() == 0) {
            // COB_CODE: MOVE TIT-DT-APPLZ-MORA-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getApplzMoraDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-APPLZ-MORA
            titCont.getTitDtApplzMora().setTitDtApplzMora(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-EMIS-TIT = 0
        //               MOVE WS-DATE-N      TO TIT-DT-EMIS-TIT
        //           END-IF
        if (ws.getIndTitCont().getDtEmisTit() == 0) {
            // COB_CODE: MOVE TIT-DT-EMIS-TIT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getEmisTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-EMIS-TIT
            titCont.getTitDtEmisTit().setTitDtEmisTit(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-ESI-TIT = 0
        //               MOVE WS-DATE-N      TO TIT-DT-ESI-TIT
        //           END-IF
        if (ws.getIndTitCont().getDtEsiTit() == 0) {
            // COB_CODE: MOVE TIT-DT-ESI-TIT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-ESI-TIT
            titCont.getTitDtEsiTit().setTitDtEsiTit(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-VLT = 0
        //               MOVE WS-DATE-N      TO TIT-DT-VLT
        //           END-IF
        if (ws.getIndTitCont().getDtVlt() == 0) {
            // COB_CODE: MOVE TIT-DT-VLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getVltDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-VLT
            titCont.getTitDtVlt().setTitDtVlt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-CAMBIO-VLT = 0
        //               MOVE WS-DATE-N      TO TIT-DT-CAMBIO-VLT
        //           END-IF
        if (ws.getIndTitCont().getDtCambioVlt() == 0) {
            // COB_CODE: MOVE TIT-DT-CAMBIO-VLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getCambioVltDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-CAMBIO-VLT
            titCont.getTitDtCambioVlt().setTitDtCambioVlt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-RICH-ADD-RID = 0
        //               MOVE WS-DATE-N      TO TIT-DT-RICH-ADD-RID
        //           END-IF
        if (ws.getIndTitCont().getDtRichAddRid() == 0) {
            // COB_CODE: MOVE TIT-DT-RICH-ADD-RID-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getRichAddRidDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-RICH-ADD-RID
            titCont.getTitDtRichAddRid().setTitDtRichAddRid(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-CERT-FISC = 0
        //               MOVE WS-DATE-N      TO TIT-DT-CERT-FISC
        //           END-IF.
        if (ws.getIndTitCont().getDtCertFisc() == 0) {
            // COB_CODE: MOVE TIT-DT-CERT-FISC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getCertFiscDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-CERT-FISC
            titCont.getTitDtCertFisc().setTitDtCertFisc(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
        // COB_CODE: MOVE LDBV7141-DT-DA    TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ws.getLdbv7141().getDtDa(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X         TO LDBV7141-DT-DA-DB.
        ws.getLdbv7141().setDtDaDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE LDBV7141-DT-A     TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ws.getLdbv7141().getDtA(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X         TO LDBV7141-DT-A-DB.
        ws.getLdbv7141().setDtADb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getDtcAcqExp() {
        throw new FieldNotMappedException("dtcAcqExp");
    }

    @Override
    public void setDtcAcqExp(AfDecimal dtcAcqExp) {
        throw new FieldNotMappedException("dtcAcqExp");
    }

    @Override
    public AfDecimal getDtcAcqExpObj() {
        return getDtcAcqExp();
    }

    @Override
    public void setDtcAcqExpObj(AfDecimal dtcAcqExpObj) {
        setDtcAcqExp(new AfDecimal(dtcAcqExpObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcCarAcq() {
        throw new FieldNotMappedException("dtcCarAcq");
    }

    @Override
    public void setDtcCarAcq(AfDecimal dtcCarAcq) {
        throw new FieldNotMappedException("dtcCarAcq");
    }

    @Override
    public AfDecimal getDtcCarAcqObj() {
        return getDtcCarAcq();
    }

    @Override
    public void setDtcCarAcqObj(AfDecimal dtcCarAcqObj) {
        setDtcCarAcq(new AfDecimal(dtcCarAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcCarGest() {
        throw new FieldNotMappedException("dtcCarGest");
    }

    @Override
    public void setDtcCarGest(AfDecimal dtcCarGest) {
        throw new FieldNotMappedException("dtcCarGest");
    }

    @Override
    public AfDecimal getDtcCarGestObj() {
        return getDtcCarGest();
    }

    @Override
    public void setDtcCarGestObj(AfDecimal dtcCarGestObj) {
        setDtcCarGest(new AfDecimal(dtcCarGestObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcCarIas() {
        throw new FieldNotMappedException("dtcCarIas");
    }

    @Override
    public void setDtcCarIas(AfDecimal dtcCarIas) {
        throw new FieldNotMappedException("dtcCarIas");
    }

    @Override
    public AfDecimal getDtcCarIasObj() {
        return getDtcCarIas();
    }

    @Override
    public void setDtcCarIasObj(AfDecimal dtcCarIasObj) {
        setDtcCarIas(new AfDecimal(dtcCarIasObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcCarInc() {
        throw new FieldNotMappedException("dtcCarInc");
    }

    @Override
    public void setDtcCarInc(AfDecimal dtcCarInc) {
        throw new FieldNotMappedException("dtcCarInc");
    }

    @Override
    public AfDecimal getDtcCarIncObj() {
        return getDtcCarInc();
    }

    @Override
    public void setDtcCarIncObj(AfDecimal dtcCarIncObj) {
        setDtcCarInc(new AfDecimal(dtcCarIncObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcCnbtAntirac() {
        throw new FieldNotMappedException("dtcCnbtAntirac");
    }

    @Override
    public void setDtcCnbtAntirac(AfDecimal dtcCnbtAntirac) {
        throw new FieldNotMappedException("dtcCnbtAntirac");
    }

    @Override
    public AfDecimal getDtcCnbtAntiracObj() {
        return getDtcCnbtAntirac();
    }

    @Override
    public void setDtcCnbtAntiracObj(AfDecimal dtcCnbtAntiracObj) {
        setDtcCnbtAntirac(new AfDecimal(dtcCnbtAntiracObj, 15, 3));
    }

    @Override
    public int getDtcCodCompAnia() {
        throw new FieldNotMappedException("dtcCodCompAnia");
    }

    @Override
    public void setDtcCodCompAnia(int dtcCodCompAnia) {
        throw new FieldNotMappedException("dtcCodCompAnia");
    }

    @Override
    public String getDtcCodDvs() {
        throw new FieldNotMappedException("dtcCodDvs");
    }

    @Override
    public void setDtcCodDvs(String dtcCodDvs) {
        throw new FieldNotMappedException("dtcCodDvs");
    }

    @Override
    public String getDtcCodDvsObj() {
        return getDtcCodDvs();
    }

    @Override
    public void setDtcCodDvsObj(String dtcCodDvsObj) {
        setDtcCodDvs(dtcCodDvsObj);
    }

    @Override
    public String getDtcCodTari() {
        throw new FieldNotMappedException("dtcCodTari");
    }

    @Override
    public void setDtcCodTari(String dtcCodTari) {
        throw new FieldNotMappedException("dtcCodTari");
    }

    @Override
    public String getDtcCodTariObj() {
        return getDtcCodTari();
    }

    @Override
    public void setDtcCodTariObj(String dtcCodTariObj) {
        setDtcCodTari(dtcCodTariObj);
    }

    @Override
    public AfDecimal getDtcCommisInter() {
        throw new FieldNotMappedException("dtcCommisInter");
    }

    @Override
    public void setDtcCommisInter(AfDecimal dtcCommisInter) {
        throw new FieldNotMappedException("dtcCommisInter");
    }

    @Override
    public AfDecimal getDtcCommisInterObj() {
        return getDtcCommisInter();
    }

    @Override
    public void setDtcCommisInterObj(AfDecimal dtcCommisInterObj) {
        setDtcCommisInter(new AfDecimal(dtcCommisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcDir() {
        throw new FieldNotMappedException("dtcDir");
    }

    @Override
    public void setDtcDir(AfDecimal dtcDir) {
        throw new FieldNotMappedException("dtcDir");
    }

    @Override
    public AfDecimal getDtcDirObj() {
        return getDtcDir();
    }

    @Override
    public void setDtcDirObj(AfDecimal dtcDirObj) {
        setDtcDir(new AfDecimal(dtcDirObj, 15, 3));
    }

    @Override
    public char getDtcDsOperSql() {
        throw new FieldNotMappedException("dtcDsOperSql");
    }

    @Override
    public void setDtcDsOperSql(char dtcDsOperSql) {
        throw new FieldNotMappedException("dtcDsOperSql");
    }

    @Override
    public long getDtcDsRiga() {
        throw new FieldNotMappedException("dtcDsRiga");
    }

    @Override
    public void setDtcDsRiga(long dtcDsRiga) {
        throw new FieldNotMappedException("dtcDsRiga");
    }

    @Override
    public char getDtcDsStatoElab() {
        throw new FieldNotMappedException("dtcDsStatoElab");
    }

    @Override
    public void setDtcDsStatoElab(char dtcDsStatoElab) {
        throw new FieldNotMappedException("dtcDsStatoElab");
    }

    @Override
    public long getDtcDsTsEndCptz() {
        throw new FieldNotMappedException("dtcDsTsEndCptz");
    }

    @Override
    public void setDtcDsTsEndCptz(long dtcDsTsEndCptz) {
        throw new FieldNotMappedException("dtcDsTsEndCptz");
    }

    @Override
    public long getDtcDsTsIniCptz() {
        throw new FieldNotMappedException("dtcDsTsIniCptz");
    }

    @Override
    public void setDtcDsTsIniCptz(long dtcDsTsIniCptz) {
        throw new FieldNotMappedException("dtcDsTsIniCptz");
    }

    @Override
    public String getDtcDsUtente() {
        throw new FieldNotMappedException("dtcDsUtente");
    }

    @Override
    public void setDtcDsUtente(String dtcDsUtente) {
        throw new FieldNotMappedException("dtcDsUtente");
    }

    @Override
    public int getDtcDsVer() {
        throw new FieldNotMappedException("dtcDsVer");
    }

    @Override
    public void setDtcDsVer(int dtcDsVer) {
        throw new FieldNotMappedException("dtcDsVer");
    }

    @Override
    public String getDtcDtEndCopDb() {
        throw new FieldNotMappedException("dtcDtEndCopDb");
    }

    @Override
    public void setDtcDtEndCopDb(String dtcDtEndCopDb) {
        throw new FieldNotMappedException("dtcDtEndCopDb");
    }

    @Override
    public String getDtcDtEndCopDbObj() {
        return getDtcDtEndCopDb();
    }

    @Override
    public void setDtcDtEndCopDbObj(String dtcDtEndCopDbObj) {
        setDtcDtEndCopDb(dtcDtEndCopDbObj);
    }

    @Override
    public String getDtcDtEndEffDb() {
        throw new FieldNotMappedException("dtcDtEndEffDb");
    }

    @Override
    public void setDtcDtEndEffDb(String dtcDtEndEffDb) {
        throw new FieldNotMappedException("dtcDtEndEffDb");
    }

    @Override
    public String getDtcDtEsiTitDb() {
        throw new FieldNotMappedException("dtcDtEsiTitDb");
    }

    @Override
    public void setDtcDtEsiTitDb(String dtcDtEsiTitDb) {
        throw new FieldNotMappedException("dtcDtEsiTitDb");
    }

    @Override
    public String getDtcDtEsiTitDbObj() {
        return getDtcDtEsiTitDb();
    }

    @Override
    public void setDtcDtEsiTitDbObj(String dtcDtEsiTitDbObj) {
        setDtcDtEsiTitDb(dtcDtEsiTitDbObj);
    }

    @Override
    public String getDtcDtIniCopDb() {
        throw new FieldNotMappedException("dtcDtIniCopDb");
    }

    @Override
    public void setDtcDtIniCopDb(String dtcDtIniCopDb) {
        throw new FieldNotMappedException("dtcDtIniCopDb");
    }

    @Override
    public String getDtcDtIniCopDbObj() {
        return getDtcDtIniCopDb();
    }

    @Override
    public void setDtcDtIniCopDbObj(String dtcDtIniCopDbObj) {
        setDtcDtIniCopDb(dtcDtIniCopDbObj);
    }

    @Override
    public String getDtcDtIniEffDb() {
        throw new FieldNotMappedException("dtcDtIniEffDb");
    }

    @Override
    public void setDtcDtIniEffDb(String dtcDtIniEffDb) {
        throw new FieldNotMappedException("dtcDtIniEffDb");
    }

    @Override
    public int getDtcFrqMovi() {
        throw new FieldNotMappedException("dtcFrqMovi");
    }

    @Override
    public void setDtcFrqMovi(int dtcFrqMovi) {
        throw new FieldNotMappedException("dtcFrqMovi");
    }

    @Override
    public Integer getDtcFrqMoviObj() {
        return ((Integer)getDtcFrqMovi());
    }

    @Override
    public void setDtcFrqMoviObj(Integer dtcFrqMoviObj) {
        setDtcFrqMovi(((int)dtcFrqMoviObj));
    }

    @Override
    public int getDtcIdDettTitCont() {
        throw new FieldNotMappedException("dtcIdDettTitCont");
    }

    @Override
    public void setDtcIdDettTitCont(int dtcIdDettTitCont) {
        throw new FieldNotMappedException("dtcIdDettTitCont");
    }

    @Override
    public int getDtcIdMoviChiu() {
        throw new FieldNotMappedException("dtcIdMoviChiu");
    }

    @Override
    public void setDtcIdMoviChiu(int dtcIdMoviChiu) {
        throw new FieldNotMappedException("dtcIdMoviChiu");
    }

    @Override
    public Integer getDtcIdMoviChiuObj() {
        return ((Integer)getDtcIdMoviChiu());
    }

    @Override
    public void setDtcIdMoviChiuObj(Integer dtcIdMoviChiuObj) {
        setDtcIdMoviChiu(((int)dtcIdMoviChiuObj));
    }

    @Override
    public int getDtcIdMoviCrz() {
        throw new FieldNotMappedException("dtcIdMoviCrz");
    }

    @Override
    public void setDtcIdMoviCrz(int dtcIdMoviCrz) {
        throw new FieldNotMappedException("dtcIdMoviCrz");
    }

    @Override
    public int getDtcIdOgg() {
        throw new FieldNotMappedException("dtcIdOgg");
    }

    @Override
    public void setDtcIdOgg(int dtcIdOgg) {
        throw new FieldNotMappedException("dtcIdOgg");
    }

    @Override
    public int getDtcIdTitCont() {
        throw new FieldNotMappedException("dtcIdTitCont");
    }

    @Override
    public void setDtcIdTitCont(int dtcIdTitCont) {
        throw new FieldNotMappedException("dtcIdTitCont");
    }

    @Override
    public AfDecimal getDtcImpAder() {
        throw new FieldNotMappedException("dtcImpAder");
    }

    @Override
    public void setDtcImpAder(AfDecimal dtcImpAder) {
        throw new FieldNotMappedException("dtcImpAder");
    }

    @Override
    public AfDecimal getDtcImpAderObj() {
        return getDtcImpAder();
    }

    @Override
    public void setDtcImpAderObj(AfDecimal dtcImpAderObj) {
        setDtcImpAder(new AfDecimal(dtcImpAderObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcImpAz() {
        throw new FieldNotMappedException("dtcImpAz");
    }

    @Override
    public void setDtcImpAz(AfDecimal dtcImpAz) {
        throw new FieldNotMappedException("dtcImpAz");
    }

    @Override
    public AfDecimal getDtcImpAzObj() {
        return getDtcImpAz();
    }

    @Override
    public void setDtcImpAzObj(AfDecimal dtcImpAzObj) {
        setDtcImpAz(new AfDecimal(dtcImpAzObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcImpTfr() {
        throw new FieldNotMappedException("dtcImpTfr");
    }

    @Override
    public void setDtcImpTfr(AfDecimal dtcImpTfr) {
        throw new FieldNotMappedException("dtcImpTfr");
    }

    @Override
    public AfDecimal getDtcImpTfrObj() {
        return getDtcImpTfr();
    }

    @Override
    public void setDtcImpTfrObj(AfDecimal dtcImpTfrObj) {
        setDtcImpTfr(new AfDecimal(dtcImpTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcImpTfrStrc() {
        throw new FieldNotMappedException("dtcImpTfrStrc");
    }

    @Override
    public void setDtcImpTfrStrc(AfDecimal dtcImpTfrStrc) {
        throw new FieldNotMappedException("dtcImpTfrStrc");
    }

    @Override
    public AfDecimal getDtcImpTfrStrcObj() {
        return getDtcImpTfrStrc();
    }

    @Override
    public void setDtcImpTfrStrcObj(AfDecimal dtcImpTfrStrcObj) {
        setDtcImpTfrStrc(new AfDecimal(dtcImpTfrStrcObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcImpTrasfe() {
        throw new FieldNotMappedException("dtcImpTrasfe");
    }

    @Override
    public void setDtcImpTrasfe(AfDecimal dtcImpTrasfe) {
        throw new FieldNotMappedException("dtcImpTrasfe");
    }

    @Override
    public AfDecimal getDtcImpTrasfeObj() {
        return getDtcImpTrasfe();
    }

    @Override
    public void setDtcImpTrasfeObj(AfDecimal dtcImpTrasfeObj) {
        setDtcImpTrasfe(new AfDecimal(dtcImpTrasfeObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcImpVolo() {
        throw new FieldNotMappedException("dtcImpVolo");
    }

    @Override
    public void setDtcImpVolo(AfDecimal dtcImpVolo) {
        throw new FieldNotMappedException("dtcImpVolo");
    }

    @Override
    public AfDecimal getDtcImpVoloObj() {
        return getDtcImpVolo();
    }

    @Override
    public void setDtcImpVoloObj(AfDecimal dtcImpVoloObj) {
        setDtcImpVolo(new AfDecimal(dtcImpVoloObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcIntrFraz() {
        throw new FieldNotMappedException("dtcIntrFraz");
    }

    @Override
    public void setDtcIntrFraz(AfDecimal dtcIntrFraz) {
        throw new FieldNotMappedException("dtcIntrFraz");
    }

    @Override
    public AfDecimal getDtcIntrFrazObj() {
        return getDtcIntrFraz();
    }

    @Override
    public void setDtcIntrFrazObj(AfDecimal dtcIntrFrazObj) {
        setDtcIntrFraz(new AfDecimal(dtcIntrFrazObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcIntrMora() {
        throw new FieldNotMappedException("dtcIntrMora");
    }

    @Override
    public void setDtcIntrMora(AfDecimal dtcIntrMora) {
        throw new FieldNotMappedException("dtcIntrMora");
    }

    @Override
    public AfDecimal getDtcIntrMoraObj() {
        return getDtcIntrMora();
    }

    @Override
    public void setDtcIntrMoraObj(AfDecimal dtcIntrMoraObj) {
        setDtcIntrMora(new AfDecimal(dtcIntrMoraObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcIntrRetdt() {
        throw new FieldNotMappedException("dtcIntrRetdt");
    }

    @Override
    public void setDtcIntrRetdt(AfDecimal dtcIntrRetdt) {
        throw new FieldNotMappedException("dtcIntrRetdt");
    }

    @Override
    public AfDecimal getDtcIntrRetdtObj() {
        return getDtcIntrRetdt();
    }

    @Override
    public void setDtcIntrRetdtObj(AfDecimal dtcIntrRetdtObj) {
        setDtcIntrRetdt(new AfDecimal(dtcIntrRetdtObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcIntrRiat() {
        throw new FieldNotMappedException("dtcIntrRiat");
    }

    @Override
    public void setDtcIntrRiat(AfDecimal dtcIntrRiat) {
        throw new FieldNotMappedException("dtcIntrRiat");
    }

    @Override
    public AfDecimal getDtcIntrRiatObj() {
        return getDtcIntrRiat();
    }

    @Override
    public void setDtcIntrRiatObj(AfDecimal dtcIntrRiatObj) {
        setDtcIntrRiat(new AfDecimal(dtcIntrRiatObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcManfeeAntic() {
        throw new FieldNotMappedException("dtcManfeeAntic");
    }

    @Override
    public void setDtcManfeeAntic(AfDecimal dtcManfeeAntic) {
        throw new FieldNotMappedException("dtcManfeeAntic");
    }

    @Override
    public AfDecimal getDtcManfeeAnticObj() {
        return getDtcManfeeAntic();
    }

    @Override
    public void setDtcManfeeAnticObj(AfDecimal dtcManfeeAnticObj) {
        setDtcManfeeAntic(new AfDecimal(dtcManfeeAnticObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcManfeeRec() {
        throw new FieldNotMappedException("dtcManfeeRec");
    }

    @Override
    public void setDtcManfeeRec(AfDecimal dtcManfeeRec) {
        throw new FieldNotMappedException("dtcManfeeRec");
    }

    @Override
    public AfDecimal getDtcManfeeRecObj() {
        return getDtcManfeeRec();
    }

    @Override
    public void setDtcManfeeRecObj(AfDecimal dtcManfeeRecObj) {
        setDtcManfeeRec(new AfDecimal(dtcManfeeRecObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcManfeeRicor() {
        throw new FieldNotMappedException("dtcManfeeRicor");
    }

    @Override
    public void setDtcManfeeRicor(AfDecimal dtcManfeeRicor) {
        throw new FieldNotMappedException("dtcManfeeRicor");
    }

    @Override
    public AfDecimal getDtcManfeeRicorObj() {
        return getDtcManfeeRicor();
    }

    @Override
    public void setDtcManfeeRicorObj(AfDecimal dtcManfeeRicorObj) {
        setDtcManfeeRicor(new AfDecimal(dtcManfeeRicorObj, 15, 3));
    }

    @Override
    public int getDtcNumGgRitardoPag() {
        throw new FieldNotMappedException("dtcNumGgRitardoPag");
    }

    @Override
    public void setDtcNumGgRitardoPag(int dtcNumGgRitardoPag) {
        throw new FieldNotMappedException("dtcNumGgRitardoPag");
    }

    @Override
    public Integer getDtcNumGgRitardoPagObj() {
        return ((Integer)getDtcNumGgRitardoPag());
    }

    @Override
    public void setDtcNumGgRitardoPagObj(Integer dtcNumGgRitardoPagObj) {
        setDtcNumGgRitardoPag(((int)dtcNumGgRitardoPagObj));
    }

    @Override
    public int getDtcNumGgRival() {
        throw new FieldNotMappedException("dtcNumGgRival");
    }

    @Override
    public void setDtcNumGgRival(int dtcNumGgRival) {
        throw new FieldNotMappedException("dtcNumGgRival");
    }

    @Override
    public Integer getDtcNumGgRivalObj() {
        return ((Integer)getDtcNumGgRival());
    }

    @Override
    public void setDtcNumGgRivalObj(Integer dtcNumGgRivalObj) {
        setDtcNumGgRival(((int)dtcNumGgRivalObj));
    }

    @Override
    public AfDecimal getDtcPreNet() {
        throw new FieldNotMappedException("dtcPreNet");
    }

    @Override
    public void setDtcPreNet(AfDecimal dtcPreNet) {
        throw new FieldNotMappedException("dtcPreNet");
    }

    @Override
    public AfDecimal getDtcPreNetObj() {
        return getDtcPreNet();
    }

    @Override
    public void setDtcPreNetObj(AfDecimal dtcPreNetObj) {
        setDtcPreNet(new AfDecimal(dtcPreNetObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcPrePpIas() {
        throw new FieldNotMappedException("dtcPrePpIas");
    }

    @Override
    public void setDtcPrePpIas(AfDecimal dtcPrePpIas) {
        throw new FieldNotMappedException("dtcPrePpIas");
    }

    @Override
    public AfDecimal getDtcPrePpIasObj() {
        return getDtcPrePpIas();
    }

    @Override
    public void setDtcPrePpIasObj(AfDecimal dtcPrePpIasObj) {
        setDtcPrePpIas(new AfDecimal(dtcPrePpIasObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcPreSoloRsh() {
        throw new FieldNotMappedException("dtcPreSoloRsh");
    }

    @Override
    public void setDtcPreSoloRsh(AfDecimal dtcPreSoloRsh) {
        throw new FieldNotMappedException("dtcPreSoloRsh");
    }

    @Override
    public AfDecimal getDtcPreSoloRshObj() {
        return getDtcPreSoloRsh();
    }

    @Override
    public void setDtcPreSoloRshObj(AfDecimal dtcPreSoloRshObj) {
        setDtcPreSoloRsh(new AfDecimal(dtcPreSoloRshObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcPreTot() {
        throw new FieldNotMappedException("dtcPreTot");
    }

    @Override
    public void setDtcPreTot(AfDecimal dtcPreTot) {
        throw new FieldNotMappedException("dtcPreTot");
    }

    @Override
    public AfDecimal getDtcPreTotObj() {
        return getDtcPreTot();
    }

    @Override
    public void setDtcPreTotObj(AfDecimal dtcPreTotObj) {
        setDtcPreTot(new AfDecimal(dtcPreTotObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcProvAcq1aa() {
        throw new FieldNotMappedException("dtcProvAcq1aa");
    }

    @Override
    public void setDtcProvAcq1aa(AfDecimal dtcProvAcq1aa) {
        throw new FieldNotMappedException("dtcProvAcq1aa");
    }

    @Override
    public AfDecimal getDtcProvAcq1aaObj() {
        return getDtcProvAcq1aa();
    }

    @Override
    public void setDtcProvAcq1aaObj(AfDecimal dtcProvAcq1aaObj) {
        setDtcProvAcq1aa(new AfDecimal(dtcProvAcq1aaObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcProvAcq2aa() {
        throw new FieldNotMappedException("dtcProvAcq2aa");
    }

    @Override
    public void setDtcProvAcq2aa(AfDecimal dtcProvAcq2aa) {
        throw new FieldNotMappedException("dtcProvAcq2aa");
    }

    @Override
    public AfDecimal getDtcProvAcq2aaObj() {
        return getDtcProvAcq2aa();
    }

    @Override
    public void setDtcProvAcq2aaObj(AfDecimal dtcProvAcq2aaObj) {
        setDtcProvAcq2aa(new AfDecimal(dtcProvAcq2aaObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcProvDaRec() {
        throw new FieldNotMappedException("dtcProvDaRec");
    }

    @Override
    public void setDtcProvDaRec(AfDecimal dtcProvDaRec) {
        throw new FieldNotMappedException("dtcProvDaRec");
    }

    @Override
    public AfDecimal getDtcProvDaRecObj() {
        return getDtcProvDaRec();
    }

    @Override
    public void setDtcProvDaRecObj(AfDecimal dtcProvDaRecObj) {
        setDtcProvDaRec(new AfDecimal(dtcProvDaRecObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcProvInc() {
        throw new FieldNotMappedException("dtcProvInc");
    }

    @Override
    public void setDtcProvInc(AfDecimal dtcProvInc) {
        throw new FieldNotMappedException("dtcProvInc");
    }

    @Override
    public AfDecimal getDtcProvIncObj() {
        return getDtcProvInc();
    }

    @Override
    public void setDtcProvIncObj(AfDecimal dtcProvIncObj) {
        setDtcProvInc(new AfDecimal(dtcProvIncObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcProvRicor() {
        throw new FieldNotMappedException("dtcProvRicor");
    }

    @Override
    public void setDtcProvRicor(AfDecimal dtcProvRicor) {
        throw new FieldNotMappedException("dtcProvRicor");
    }

    @Override
    public AfDecimal getDtcProvRicorObj() {
        return getDtcProvRicor();
    }

    @Override
    public void setDtcProvRicorObj(AfDecimal dtcProvRicorObj) {
        setDtcProvRicor(new AfDecimal(dtcProvRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcRemunAss() {
        throw new FieldNotMappedException("dtcRemunAss");
    }

    @Override
    public void setDtcRemunAss(AfDecimal dtcRemunAss) {
        throw new FieldNotMappedException("dtcRemunAss");
    }

    @Override
    public AfDecimal getDtcRemunAssObj() {
        return getDtcRemunAss();
    }

    @Override
    public void setDtcRemunAssObj(AfDecimal dtcRemunAssObj) {
        setDtcRemunAss(new AfDecimal(dtcRemunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSoprAlt() {
        throw new FieldNotMappedException("dtcSoprAlt");
    }

    @Override
    public void setDtcSoprAlt(AfDecimal dtcSoprAlt) {
        throw new FieldNotMappedException("dtcSoprAlt");
    }

    @Override
    public AfDecimal getDtcSoprAltObj() {
        return getDtcSoprAlt();
    }

    @Override
    public void setDtcSoprAltObj(AfDecimal dtcSoprAltObj) {
        setDtcSoprAlt(new AfDecimal(dtcSoprAltObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSoprProf() {
        throw new FieldNotMappedException("dtcSoprProf");
    }

    @Override
    public void setDtcSoprProf(AfDecimal dtcSoprProf) {
        throw new FieldNotMappedException("dtcSoprProf");
    }

    @Override
    public AfDecimal getDtcSoprProfObj() {
        return getDtcSoprProf();
    }

    @Override
    public void setDtcSoprProfObj(AfDecimal dtcSoprProfObj) {
        setDtcSoprProf(new AfDecimal(dtcSoprProfObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSoprSan() {
        throw new FieldNotMappedException("dtcSoprSan");
    }

    @Override
    public void setDtcSoprSan(AfDecimal dtcSoprSan) {
        throw new FieldNotMappedException("dtcSoprSan");
    }

    @Override
    public AfDecimal getDtcSoprSanObj() {
        return getDtcSoprSan();
    }

    @Override
    public void setDtcSoprSanObj(AfDecimal dtcSoprSanObj) {
        setDtcSoprSan(new AfDecimal(dtcSoprSanObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSoprSpo() {
        throw new FieldNotMappedException("dtcSoprSpo");
    }

    @Override
    public void setDtcSoprSpo(AfDecimal dtcSoprSpo) {
        throw new FieldNotMappedException("dtcSoprSpo");
    }

    @Override
    public AfDecimal getDtcSoprSpoObj() {
        return getDtcSoprSpo();
    }

    @Override
    public void setDtcSoprSpoObj(AfDecimal dtcSoprSpoObj) {
        setDtcSoprSpo(new AfDecimal(dtcSoprSpoObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSoprTec() {
        throw new FieldNotMappedException("dtcSoprTec");
    }

    @Override
    public void setDtcSoprTec(AfDecimal dtcSoprTec) {
        throw new FieldNotMappedException("dtcSoprTec");
    }

    @Override
    public AfDecimal getDtcSoprTecObj() {
        return getDtcSoprTec();
    }

    @Override
    public void setDtcSoprTecObj(AfDecimal dtcSoprTecObj) {
        setDtcSoprTec(new AfDecimal(dtcSoprTecObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSpeAge() {
        throw new FieldNotMappedException("dtcSpeAge");
    }

    @Override
    public void setDtcSpeAge(AfDecimal dtcSpeAge) {
        throw new FieldNotMappedException("dtcSpeAge");
    }

    @Override
    public AfDecimal getDtcSpeAgeObj() {
        return getDtcSpeAge();
    }

    @Override
    public void setDtcSpeAgeObj(AfDecimal dtcSpeAgeObj) {
        setDtcSpeAge(new AfDecimal(dtcSpeAgeObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcSpeMed() {
        throw new FieldNotMappedException("dtcSpeMed");
    }

    @Override
    public void setDtcSpeMed(AfDecimal dtcSpeMed) {
        throw new FieldNotMappedException("dtcSpeMed");
    }

    @Override
    public AfDecimal getDtcSpeMedObj() {
        return getDtcSpeMed();
    }

    @Override
    public void setDtcSpeMedObj(AfDecimal dtcSpeMedObj) {
        setDtcSpeMed(new AfDecimal(dtcSpeMedObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcTax() {
        throw new FieldNotMappedException("dtcTax");
    }

    @Override
    public void setDtcTax(AfDecimal dtcTax) {
        throw new FieldNotMappedException("dtcTax");
    }

    @Override
    public AfDecimal getDtcTaxObj() {
        return getDtcTax();
    }

    @Override
    public void setDtcTaxObj(AfDecimal dtcTaxObj) {
        setDtcTax(new AfDecimal(dtcTaxObj, 15, 3));
    }

    @Override
    public AfDecimal getDtcTotIntrPrest() {
        throw new FieldNotMappedException("dtcTotIntrPrest");
    }

    @Override
    public void setDtcTotIntrPrest(AfDecimal dtcTotIntrPrest) {
        throw new FieldNotMappedException("dtcTotIntrPrest");
    }

    @Override
    public AfDecimal getDtcTotIntrPrestObj() {
        return getDtcTotIntrPrest();
    }

    @Override
    public void setDtcTotIntrPrestObj(AfDecimal dtcTotIntrPrestObj) {
        setDtcTotIntrPrest(new AfDecimal(dtcTotIntrPrestObj, 15, 3));
    }

    @Override
    public String getDtcTpOgg() {
        throw new FieldNotMappedException("dtcTpOgg");
    }

    @Override
    public void setDtcTpOgg(String dtcTpOgg) {
        throw new FieldNotMappedException("dtcTpOgg");
    }

    @Override
    public String getDtcTpRgmFisc() {
        throw new FieldNotMappedException("dtcTpRgmFisc");
    }

    @Override
    public void setDtcTpRgmFisc(String dtcTpRgmFisc) {
        throw new FieldNotMappedException("dtcTpRgmFisc");
    }

    @Override
    public String getDtcTpStatTit() {
        throw new FieldNotMappedException("dtcTpStatTit");
    }

    @Override
    public void setDtcTpStatTit(String dtcTpStatTit) {
        throw new FieldNotMappedException("dtcTpStatTit");
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public int getLdbv0371IdOgg() {
        throw new FieldNotMappedException("ldbv0371IdOgg");
    }

    @Override
    public void setLdbv0371IdOgg(int ldbv0371IdOgg) {
        throw new FieldNotMappedException("ldbv0371IdOgg");
    }

    @Override
    public String getLdbv0371TpOgg() {
        throw new FieldNotMappedException("ldbv0371TpOgg");
    }

    @Override
    public void setLdbv0371TpOgg(String ldbv0371TpOgg) {
        throw new FieldNotMappedException("ldbv0371TpOgg");
    }

    @Override
    public String getLdbv0371TpPreTit() {
        throw new FieldNotMappedException("ldbv0371TpPreTit");
    }

    @Override
    public void setLdbv0371TpPreTit(String ldbv0371TpPreTit) {
        throw new FieldNotMappedException("ldbv0371TpPreTit");
    }

    @Override
    public String getLdbv0371TpStatTit1() {
        throw new FieldNotMappedException("ldbv0371TpStatTit1");
    }

    @Override
    public void setLdbv0371TpStatTit1(String ldbv0371TpStatTit1) {
        throw new FieldNotMappedException("ldbv0371TpStatTit1");
    }

    @Override
    public String getLdbv0371TpStatTit2() {
        throw new FieldNotMappedException("ldbv0371TpStatTit2");
    }

    @Override
    public void setLdbv0371TpStatTit2(String ldbv0371TpStatTit2) {
        throw new FieldNotMappedException("ldbv0371TpStatTit2");
    }

    @Override
    public int getLdbv2971IdOgg() {
        throw new FieldNotMappedException("ldbv2971IdOgg");
    }

    @Override
    public void setLdbv2971IdOgg(int ldbv2971IdOgg) {
        throw new FieldNotMappedException("ldbv2971IdOgg");
    }

    @Override
    public String getLdbv2971TpOgg() {
        throw new FieldNotMappedException("ldbv2971TpOgg");
    }

    @Override
    public void setLdbv2971TpOgg(String ldbv2971TpOgg) {
        throw new FieldNotMappedException("ldbv2971TpOgg");
    }

    @Override
    public String getLdbv3101DtADb() {
        throw new FieldNotMappedException("ldbv3101DtADb");
    }

    @Override
    public void setLdbv3101DtADb(String ldbv3101DtADb) {
        throw new FieldNotMappedException("ldbv3101DtADb");
    }

    @Override
    public String getLdbv3101DtDaDb() {
        throw new FieldNotMappedException("ldbv3101DtDaDb");
    }

    @Override
    public void setLdbv3101DtDaDb(String ldbv3101DtDaDb) {
        throw new FieldNotMappedException("ldbv3101DtDaDb");
    }

    @Override
    public int getLdbv3101IdOgg() {
        throw new FieldNotMappedException("ldbv3101IdOgg");
    }

    @Override
    public void setLdbv3101IdOgg(int ldbv3101IdOgg) {
        throw new FieldNotMappedException("ldbv3101IdOgg");
    }

    @Override
    public String getLdbv3101TpOgg() {
        throw new FieldNotMappedException("ldbv3101TpOgg");
    }

    @Override
    public void setLdbv3101TpOgg(String ldbv3101TpOgg) {
        throw new FieldNotMappedException("ldbv3101TpOgg");
    }

    @Override
    public String getLdbv3101TpStaTit() {
        throw new FieldNotMappedException("ldbv3101TpStaTit");
    }

    @Override
    public void setLdbv3101TpStaTit(String ldbv3101TpStaTit) {
        throw new FieldNotMappedException("ldbv3101TpStaTit");
    }

    @Override
    public String getLdbv3101TpTit() {
        throw new FieldNotMappedException("ldbv3101TpTit");
    }

    @Override
    public void setLdbv3101TpTit(String ldbv3101TpTit) {
        throw new FieldNotMappedException("ldbv3101TpTit");
    }

    @Override
    public String getLdbv7141DtADb() {
        return ws.getLdbv7141().getDtADb();
    }

    @Override
    public void setLdbv7141DtADb(String ldbv7141DtADb) {
        this.ws.getLdbv7141().setDtADb(ldbv7141DtADb);
    }

    @Override
    public String getLdbv7141DtDaDb() {
        return ws.getLdbv7141().getDtDaDb();
    }

    @Override
    public void setLdbv7141DtDaDb(String ldbv7141DtDaDb) {
        this.ws.getLdbv7141().setDtDaDb(ldbv7141DtDaDb);
    }

    @Override
    public int getLdbv7141IdOgg() {
        return ws.getLdbv7141().getIdOgg();
    }

    @Override
    public void setLdbv7141IdOgg(int ldbv7141IdOgg) {
        this.ws.getLdbv7141().setIdOgg(ldbv7141IdOgg);
    }

    @Override
    public String getLdbv7141TpOgg() {
        return ws.getLdbv7141().getTpOgg();
    }

    @Override
    public void setLdbv7141TpOgg(String ldbv7141TpOgg) {
        this.ws.getLdbv7141().setTpOgg(ldbv7141TpOgg);
    }

    @Override
    public String getLdbv7141TpStaTit1() {
        return ws.getLdbv7141().getTpStaTit1();
    }

    @Override
    public void setLdbv7141TpStaTit1(String ldbv7141TpStaTit1) {
        this.ws.getLdbv7141().setTpStaTit1(ldbv7141TpStaTit1);
    }

    @Override
    public String getLdbv7141TpStaTit2() {
        return ws.getLdbv7141().getTpStaTit2();
    }

    @Override
    public void setLdbv7141TpStaTit2(String ldbv7141TpStaTit2) {
        this.ws.getLdbv7141().setTpStaTit2(ldbv7141TpStaTit2);
    }

    @Override
    public String getLdbv7141TpTit() {
        return ws.getLdbv7141().getTpTit();
    }

    @Override
    public void setLdbv7141TpTit(String ldbv7141TpTit) {
        this.ws.getLdbv7141().setTpTit(ldbv7141TpTit);
    }

    @Override
    public int getLdbvd601IdOgg() {
        throw new FieldNotMappedException("ldbvd601IdOgg");
    }

    @Override
    public void setLdbvd601IdOgg(int ldbvd601IdOgg) {
        throw new FieldNotMappedException("ldbvd601IdOgg");
    }

    @Override
    public String getLdbvd601TpOgg() {
        throw new FieldNotMappedException("ldbvd601TpOgg");
    }

    @Override
    public void setLdbvd601TpOgg(String ldbvd601TpOgg) {
        throw new FieldNotMappedException("ldbvd601TpOgg");
    }

    @Override
    public String getLdbvd601TpStaTit() {
        throw new FieldNotMappedException("ldbvd601TpStaTit");
    }

    @Override
    public void setLdbvd601TpStaTit(String ldbvd601TpStaTit) {
        throw new FieldNotMappedException("ldbvd601TpStaTit");
    }

    @Override
    public String getLdbvd601TpTit() {
        throw new FieldNotMappedException("ldbvd601TpTit");
    }

    @Override
    public void setLdbvd601TpTit(String ldbvd601TpTit) {
        throw new FieldNotMappedException("ldbvd601TpTit");
    }

    @Override
    public int getTitCodCompAnia() {
        return titCont.getTitCodCompAnia();
    }

    @Override
    public void setTitCodCompAnia(int titCodCompAnia) {
        this.titCont.setTitCodCompAnia(titCodCompAnia);
    }

    @Override
    public String getTitCodDvs() {
        return titCont.getTitCodDvs();
    }

    @Override
    public void setTitCodDvs(String titCodDvs) {
        this.titCont.setTitCodDvs(titCodDvs);
    }

    @Override
    public String getTitCodDvsObj() {
        if (ws.getIndTitCont().getCodDvs() >= 0) {
            return getTitCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitCodDvsObj(String titCodDvsObj) {
        if (titCodDvsObj != null) {
            setTitCodDvs(titCodDvsObj);
            ws.getIndTitCont().setCodDvs(((short)0));
        }
        else {
            ws.getIndTitCont().setCodDvs(((short)-1));
        }
    }

    @Override
    public String getTitCodIban() {
        return titCont.getTitCodIban();
    }

    @Override
    public void setTitCodIban(String titCodIban) {
        this.titCont.setTitCodIban(titCodIban);
    }

    @Override
    public String getTitCodIbanObj() {
        if (ws.getIndTitCont().getCodIban() >= 0) {
            return getTitCodIban();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitCodIbanObj(String titCodIbanObj) {
        if (titCodIbanObj != null) {
            setTitCodIban(titCodIbanObj);
            ws.getIndTitCont().setCodIban(((short)0));
        }
        else {
            ws.getIndTitCont().setCodIban(((short)-1));
        }
    }

    @Override
    public char getTitDsOperSql() {
        return titCont.getTitDsOperSql();
    }

    @Override
    public void setTitDsOperSql(char titDsOperSql) {
        this.titCont.setTitDsOperSql(titDsOperSql);
    }

    @Override
    public long getTitDsRiga() {
        return titCont.getTitDsRiga();
    }

    @Override
    public void setTitDsRiga(long titDsRiga) {
        this.titCont.setTitDsRiga(titDsRiga);
    }

    @Override
    public char getTitDsStatoElab() {
        return titCont.getTitDsStatoElab();
    }

    @Override
    public void setTitDsStatoElab(char titDsStatoElab) {
        this.titCont.setTitDsStatoElab(titDsStatoElab);
    }

    @Override
    public long getTitDsTsEndCptz() {
        return titCont.getTitDsTsEndCptz();
    }

    @Override
    public void setTitDsTsEndCptz(long titDsTsEndCptz) {
        this.titCont.setTitDsTsEndCptz(titDsTsEndCptz);
    }

    @Override
    public long getTitDsTsIniCptz() {
        return titCont.getTitDsTsIniCptz();
    }

    @Override
    public void setTitDsTsIniCptz(long titDsTsIniCptz) {
        this.titCont.setTitDsTsIniCptz(titDsTsIniCptz);
    }

    @Override
    public String getTitDsUtente() {
        return titCont.getTitDsUtente();
    }

    @Override
    public void setTitDsUtente(String titDsUtente) {
        this.titCont.setTitDsUtente(titDsUtente);
    }

    @Override
    public int getTitDsVer() {
        return titCont.getTitDsVer();
    }

    @Override
    public void setTitDsVer(int titDsVer) {
        this.titCont.setTitDsVer(titDsVer);
    }

    @Override
    public String getTitDtApplzMoraDb() {
        return ws.getTitContDb().getApplzMoraDb();
    }

    @Override
    public void setTitDtApplzMoraDb(String titDtApplzMoraDb) {
        this.ws.getTitContDb().setApplzMoraDb(titDtApplzMoraDb);
    }

    @Override
    public String getTitDtApplzMoraDbObj() {
        if (ws.getIndTitCont().getDtApplzMora() >= 0) {
            return getTitDtApplzMoraDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtApplzMoraDbObj(String titDtApplzMoraDbObj) {
        if (titDtApplzMoraDbObj != null) {
            setTitDtApplzMoraDb(titDtApplzMoraDbObj);
            ws.getIndTitCont().setDtApplzMora(((short)0));
        }
        else {
            ws.getIndTitCont().setDtApplzMora(((short)-1));
        }
    }

    @Override
    public String getTitDtCambioVltDb() {
        return ws.getTitContDb().getCambioVltDb();
    }

    @Override
    public void setTitDtCambioVltDb(String titDtCambioVltDb) {
        this.ws.getTitContDb().setCambioVltDb(titDtCambioVltDb);
    }

    @Override
    public String getTitDtCambioVltDbObj() {
        if (ws.getIndTitCont().getDtCambioVlt() >= 0) {
            return getTitDtCambioVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtCambioVltDbObj(String titDtCambioVltDbObj) {
        if (titDtCambioVltDbObj != null) {
            setTitDtCambioVltDb(titDtCambioVltDbObj);
            ws.getIndTitCont().setDtCambioVlt(((short)0));
        }
        else {
            ws.getIndTitCont().setDtCambioVlt(((short)-1));
        }
    }

    @Override
    public String getTitDtCertFiscDb() {
        return ws.getTitContDb().getCertFiscDb();
    }

    @Override
    public void setTitDtCertFiscDb(String titDtCertFiscDb) {
        this.ws.getTitContDb().setCertFiscDb(titDtCertFiscDb);
    }

    @Override
    public String getTitDtCertFiscDbObj() {
        if (ws.getIndTitCont().getDtCertFisc() >= 0) {
            return getTitDtCertFiscDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtCertFiscDbObj(String titDtCertFiscDbObj) {
        if (titDtCertFiscDbObj != null) {
            setTitDtCertFiscDb(titDtCertFiscDbObj);
            ws.getIndTitCont().setDtCertFisc(((short)0));
        }
        else {
            ws.getIndTitCont().setDtCertFisc(((short)-1));
        }
    }

    @Override
    public String getTitDtEmisTitDb() {
        return ws.getTitContDb().getEmisTitDb();
    }

    @Override
    public void setTitDtEmisTitDb(String titDtEmisTitDb) {
        this.ws.getTitContDb().setEmisTitDb(titDtEmisTitDb);
    }

    @Override
    public String getTitDtEmisTitDbObj() {
        if (ws.getIndTitCont().getDtEmisTit() >= 0) {
            return getTitDtEmisTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtEmisTitDbObj(String titDtEmisTitDbObj) {
        if (titDtEmisTitDbObj != null) {
            setTitDtEmisTitDb(titDtEmisTitDbObj);
            ws.getIndTitCont().setDtEmisTit(((short)0));
        }
        else {
            ws.getIndTitCont().setDtEmisTit(((short)-1));
        }
    }

    @Override
    public String getTitDtEndCopDb() {
        return ws.getTitContDb().getEndCopDb();
    }

    @Override
    public void setTitDtEndCopDb(String titDtEndCopDb) {
        this.ws.getTitContDb().setEndCopDb(titDtEndCopDb);
    }

    @Override
    public String getTitDtEndCopDbObj() {
        if (ws.getIndTitCont().getDtEndCop() >= 0) {
            return getTitDtEndCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtEndCopDbObj(String titDtEndCopDbObj) {
        if (titDtEndCopDbObj != null) {
            setTitDtEndCopDb(titDtEndCopDbObj);
            ws.getIndTitCont().setDtEndCop(((short)0));
        }
        else {
            ws.getIndTitCont().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getTitDtEndEffDb() {
        return ws.getTitContDb().getEndEffDb();
    }

    @Override
    public void setTitDtEndEffDb(String titDtEndEffDb) {
        this.ws.getTitContDb().setEndEffDb(titDtEndEffDb);
    }

    @Override
    public String getTitDtEsiTitDb() {
        return ws.getTitContDb().getEsiTitDb();
    }

    @Override
    public void setTitDtEsiTitDb(String titDtEsiTitDb) {
        this.ws.getTitContDb().setEsiTitDb(titDtEsiTitDb);
    }

    @Override
    public String getTitDtEsiTitDbObj() {
        if (ws.getIndTitCont().getDtEsiTit() >= 0) {
            return getTitDtEsiTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtEsiTitDbObj(String titDtEsiTitDbObj) {
        if (titDtEsiTitDbObj != null) {
            setTitDtEsiTitDb(titDtEsiTitDbObj);
            ws.getIndTitCont().setDtEsiTit(((short)0));
        }
        else {
            ws.getIndTitCont().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getTitDtIniCopDb() {
        return ws.getTitContDb().getIniCopDb();
    }

    @Override
    public void setTitDtIniCopDb(String titDtIniCopDb) {
        this.ws.getTitContDb().setIniCopDb(titDtIniCopDb);
    }

    @Override
    public String getTitDtIniCopDbObj() {
        if (ws.getIndTitCont().getDtIniCop() >= 0) {
            return getTitDtIniCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtIniCopDbObj(String titDtIniCopDbObj) {
        if (titDtIniCopDbObj != null) {
            setTitDtIniCopDb(titDtIniCopDbObj);
            ws.getIndTitCont().setDtIniCop(((short)0));
        }
        else {
            ws.getIndTitCont().setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getTitDtIniEffDb() {
        return ws.getTitContDb().getIniEffDb();
    }

    @Override
    public void setTitDtIniEffDb(String titDtIniEffDb) {
        this.ws.getTitContDb().setIniEffDb(titDtIniEffDb);
    }

    @Override
    public String getTitDtRichAddRidDb() {
        return ws.getTitContDb().getRichAddRidDb();
    }

    @Override
    public void setTitDtRichAddRidDb(String titDtRichAddRidDb) {
        this.ws.getTitContDb().setRichAddRidDb(titDtRichAddRidDb);
    }

    @Override
    public String getTitDtRichAddRidDbObj() {
        if (ws.getIndTitCont().getDtRichAddRid() >= 0) {
            return getTitDtRichAddRidDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtRichAddRidDbObj(String titDtRichAddRidDbObj) {
        if (titDtRichAddRidDbObj != null) {
            setTitDtRichAddRidDb(titDtRichAddRidDbObj);
            ws.getIndTitCont().setDtRichAddRid(((short)0));
        }
        else {
            ws.getIndTitCont().setDtRichAddRid(((short)-1));
        }
    }

    @Override
    public String getTitDtVltDb() {
        return ws.getTitContDb().getVltDb();
    }

    @Override
    public void setTitDtVltDb(String titDtVltDb) {
        this.ws.getTitContDb().setVltDb(titDtVltDb);
    }

    @Override
    public String getTitDtVltDbObj() {
        if (ws.getIndTitCont().getDtVlt() >= 0) {
            return getTitDtVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtVltDbObj(String titDtVltDbObj) {
        if (titDtVltDbObj != null) {
            setTitDtVltDb(titDtVltDbObj);
            ws.getIndTitCont().setDtVlt(((short)0));
        }
        else {
            ws.getIndTitCont().setDtVlt(((short)-1));
        }
    }

    @Override
    public String getTitEstrCntCorrAdd() {
        return titCont.getTitEstrCntCorrAdd();
    }

    @Override
    public void setTitEstrCntCorrAdd(String titEstrCntCorrAdd) {
        this.titCont.setTitEstrCntCorrAdd(titEstrCntCorrAdd);
    }

    @Override
    public String getTitEstrCntCorrAddObj() {
        if (ws.getIndTitCont().getEstrCntCorrAdd() >= 0) {
            return getTitEstrCntCorrAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitEstrCntCorrAddObj(String titEstrCntCorrAddObj) {
        if (titEstrCntCorrAddObj != null) {
            setTitEstrCntCorrAdd(titEstrCntCorrAddObj);
            ws.getIndTitCont().setEstrCntCorrAdd(((short)0));
        }
        else {
            ws.getIndTitCont().setEstrCntCorrAdd(((short)-1));
        }
    }

    @Override
    public char getTitFlForzDtVlt() {
        return titCont.getTitFlForzDtVlt();
    }

    @Override
    public void setTitFlForzDtVlt(char titFlForzDtVlt) {
        this.titCont.setTitFlForzDtVlt(titFlForzDtVlt);
    }

    @Override
    public Character getTitFlForzDtVltObj() {
        if (ws.getIndTitCont().getFlForzDtVlt() >= 0) {
            return ((Character)getTitFlForzDtVlt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlForzDtVltObj(Character titFlForzDtVltObj) {
        if (titFlForzDtVltObj != null) {
            setTitFlForzDtVlt(((char)titFlForzDtVltObj));
            ws.getIndTitCont().setFlForzDtVlt(((short)0));
        }
        else {
            ws.getIndTitCont().setFlForzDtVlt(((short)-1));
        }
    }

    @Override
    public char getTitFlIncAutogen() {
        return titCont.getTitFlIncAutogen();
    }

    @Override
    public void setTitFlIncAutogen(char titFlIncAutogen) {
        this.titCont.setTitFlIncAutogen(titFlIncAutogen);
    }

    @Override
    public Character getTitFlIncAutogenObj() {
        if (ws.getIndTitCont().getFlIncAutogen() >= 0) {
            return ((Character)getTitFlIncAutogen());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlIncAutogenObj(Character titFlIncAutogenObj) {
        if (titFlIncAutogenObj != null) {
            setTitFlIncAutogen(((char)titFlIncAutogenObj));
            ws.getIndTitCont().setFlIncAutogen(((short)0));
        }
        else {
            ws.getIndTitCont().setFlIncAutogen(((short)-1));
        }
    }

    @Override
    public char getTitFlMora() {
        return titCont.getTitFlMora();
    }

    @Override
    public void setTitFlMora(char titFlMora) {
        this.titCont.setTitFlMora(titFlMora);
    }

    @Override
    public Character getTitFlMoraObj() {
        if (ws.getIndTitCont().getFlMora() >= 0) {
            return ((Character)getTitFlMora());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlMoraObj(Character titFlMoraObj) {
        if (titFlMoraObj != null) {
            setTitFlMora(((char)titFlMoraObj));
            ws.getIndTitCont().setFlMora(((short)0));
        }
        else {
            ws.getIndTitCont().setFlMora(((short)-1));
        }
    }

    @Override
    public char getTitFlSoll() {
        return titCont.getTitFlSoll();
    }

    @Override
    public void setTitFlSoll(char titFlSoll) {
        this.titCont.setTitFlSoll(titFlSoll);
    }

    @Override
    public Character getTitFlSollObj() {
        if (ws.getIndTitCont().getFlSoll() >= 0) {
            return ((Character)getTitFlSoll());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlSollObj(Character titFlSollObj) {
        if (titFlSollObj != null) {
            setTitFlSoll(((char)titFlSollObj));
            ws.getIndTitCont().setFlSoll(((short)0));
        }
        else {
            ws.getIndTitCont().setFlSoll(((short)-1));
        }
    }

    @Override
    public char getTitFlTitDaReinvst() {
        return titCont.getTitFlTitDaReinvst();
    }

    @Override
    public void setTitFlTitDaReinvst(char titFlTitDaReinvst) {
        this.titCont.setTitFlTitDaReinvst(titFlTitDaReinvst);
    }

    @Override
    public Character getTitFlTitDaReinvstObj() {
        if (ws.getIndTitCont().getFlTitDaReinvst() >= 0) {
            return ((Character)getTitFlTitDaReinvst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlTitDaReinvstObj(Character titFlTitDaReinvstObj) {
        if (titFlTitDaReinvstObj != null) {
            setTitFlTitDaReinvst(((char)titFlTitDaReinvstObj));
            ws.getIndTitCont().setFlTitDaReinvst(((short)0));
        }
        else {
            ws.getIndTitCont().setFlTitDaReinvst(((short)-1));
        }
    }

    @Override
    public int getTitFraz() {
        return titCont.getTitFraz().getTitFraz();
    }

    @Override
    public void setTitFraz(int titFraz) {
        this.titCont.getTitFraz().setTitFraz(titFraz);
    }

    @Override
    public Integer getTitFrazObj() {
        if (ws.getIndTitCont().getFraz() >= 0) {
            return ((Integer)getTitFraz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFrazObj(Integer titFrazObj) {
        if (titFrazObj != null) {
            setTitFraz(((int)titFrazObj));
            ws.getIndTitCont().setFraz(((short)0));
        }
        else {
            ws.getIndTitCont().setFraz(((short)-1));
        }
    }

    @Override
    public String getTitIbRich() {
        return titCont.getTitIbRich();
    }

    @Override
    public void setTitIbRich(String titIbRich) {
        this.titCont.setTitIbRich(titIbRich);
    }

    @Override
    public String getTitIbRichObj() {
        if (ws.getIndTitCont().getIbRich() >= 0) {
            return getTitIbRich();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIbRichObj(String titIbRichObj) {
        if (titIbRichObj != null) {
            setTitIbRich(titIbRichObj);
            ws.getIndTitCont().setIbRich(((short)0));
        }
        else {
            ws.getIndTitCont().setIbRich(((short)-1));
        }
    }

    @Override
    public int getTitIdMoviChiu() {
        return titCont.getTitIdMoviChiu().getTitIdMoviChiu();
    }

    @Override
    public void setTitIdMoviChiu(int titIdMoviChiu) {
        this.titCont.getTitIdMoviChiu().setTitIdMoviChiu(titIdMoviChiu);
    }

    @Override
    public Integer getTitIdMoviChiuObj() {
        if (ws.getIndTitCont().getIdMoviChiu() >= 0) {
            return ((Integer)getTitIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIdMoviChiuObj(Integer titIdMoviChiuObj) {
        if (titIdMoviChiuObj != null) {
            setTitIdMoviChiu(((int)titIdMoviChiuObj));
            ws.getIndTitCont().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndTitCont().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getTitIdMoviCrz() {
        return titCont.getTitIdMoviCrz();
    }

    @Override
    public void setTitIdMoviCrz(int titIdMoviCrz) {
        this.titCont.setTitIdMoviCrz(titIdMoviCrz);
    }

    @Override
    public int getTitIdOgg() {
        return titCont.getTitIdOgg();
    }

    @Override
    public void setTitIdOgg(int titIdOgg) {
        this.titCont.setTitIdOgg(titIdOgg);
    }

    @Override
    public int getTitIdRappAna() {
        return titCont.getTitIdRappAna().getTitIdRappAna();
    }

    @Override
    public void setTitIdRappAna(int titIdRappAna) {
        this.titCont.getTitIdRappAna().setTitIdRappAna(titIdRappAna);
    }

    @Override
    public Integer getTitIdRappAnaObj() {
        if (ws.getIndTitCont().getIdRappAna() >= 0) {
            return ((Integer)getTitIdRappAna());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIdRappAnaObj(Integer titIdRappAnaObj) {
        if (titIdRappAnaObj != null) {
            setTitIdRappAna(((int)titIdRappAnaObj));
            ws.getIndTitCont().setIdRappAna(((short)0));
        }
        else {
            ws.getIndTitCont().setIdRappAna(((short)-1));
        }
    }

    @Override
    public int getTitIdRappRete() {
        return titCont.getTitIdRappRete().getTitIdRappRete();
    }

    @Override
    public void setTitIdRappRete(int titIdRappRete) {
        this.titCont.getTitIdRappRete().setTitIdRappRete(titIdRappRete);
    }

    @Override
    public Integer getTitIdRappReteObj() {
        if (ws.getIndTitCont().getIdRappRete() >= 0) {
            return ((Integer)getTitIdRappRete());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIdRappReteObj(Integer titIdRappReteObj) {
        if (titIdRappReteObj != null) {
            setTitIdRappRete(((int)titIdRappReteObj));
            ws.getIndTitCont().setIdRappRete(((short)0));
        }
        else {
            ws.getIndTitCont().setIdRappRete(((short)-1));
        }
    }

    @Override
    public int getTitIdTitCont() {
        return titCont.getTitIdTitCont();
    }

    @Override
    public void setTitIdTitCont(int titIdTitCont) {
        this.titCont.setTitIdTitCont(titIdTitCont);
    }

    @Override
    public AfDecimal getTitImpAder() {
        return titCont.getTitImpAder().getTitImpAder();
    }

    @Override
    public void setTitImpAder(AfDecimal titImpAder) {
        this.titCont.getTitImpAder().setTitImpAder(titImpAder.copy());
    }

    @Override
    public AfDecimal getTitImpAderObj() {
        if (ws.getIndTitCont().getImpAder() >= 0) {
            return getTitImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpAderObj(AfDecimal titImpAderObj) {
        if (titImpAderObj != null) {
            setTitImpAder(new AfDecimal(titImpAderObj, 15, 3));
            ws.getIndTitCont().setImpAder(((short)0));
        }
        else {
            ws.getIndTitCont().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpAz() {
        return titCont.getTitImpAz().getTitImpAz();
    }

    @Override
    public void setTitImpAz(AfDecimal titImpAz) {
        this.titCont.getTitImpAz().setTitImpAz(titImpAz.copy());
    }

    @Override
    public AfDecimal getTitImpAzObj() {
        if (ws.getIndTitCont().getImpAz() >= 0) {
            return getTitImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpAzObj(AfDecimal titImpAzObj) {
        if (titImpAzObj != null) {
            setTitImpAz(new AfDecimal(titImpAzObj, 15, 3));
            ws.getIndTitCont().setImpAz(((short)0));
        }
        else {
            ws.getIndTitCont().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpPag() {
        return titCont.getTitImpPag().getTitImpPag();
    }

    @Override
    public void setTitImpPag(AfDecimal titImpPag) {
        this.titCont.getTitImpPag().setTitImpPag(titImpPag.copy());
    }

    @Override
    public AfDecimal getTitImpPagObj() {
        if (ws.getIndTitCont().getImpPag() >= 0) {
            return getTitImpPag();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpPagObj(AfDecimal titImpPagObj) {
        if (titImpPagObj != null) {
            setTitImpPag(new AfDecimal(titImpPagObj, 15, 3));
            ws.getIndTitCont().setImpPag(((short)0));
        }
        else {
            ws.getIndTitCont().setImpPag(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpTfr() {
        return titCont.getTitImpTfr().getTitImpTfr();
    }

    @Override
    public void setTitImpTfr(AfDecimal titImpTfr) {
        this.titCont.getTitImpTfr().setTitImpTfr(titImpTfr.copy());
    }

    @Override
    public AfDecimal getTitImpTfrObj() {
        if (ws.getIndTitCont().getImpTfr() >= 0) {
            return getTitImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpTfrObj(AfDecimal titImpTfrObj) {
        if (titImpTfrObj != null) {
            setTitImpTfr(new AfDecimal(titImpTfrObj, 15, 3));
            ws.getIndTitCont().setImpTfr(((short)0));
        }
        else {
            ws.getIndTitCont().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpTfrStrc() {
        return titCont.getTitImpTfrStrc().getTitImpTfrStrc();
    }

    @Override
    public void setTitImpTfrStrc(AfDecimal titImpTfrStrc) {
        this.titCont.getTitImpTfrStrc().setTitImpTfrStrc(titImpTfrStrc.copy());
    }

    @Override
    public AfDecimal getTitImpTfrStrcObj() {
        if (ws.getIndTitCont().getImpTfrStrc() >= 0) {
            return getTitImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpTfrStrcObj(AfDecimal titImpTfrStrcObj) {
        if (titImpTfrStrcObj != null) {
            setTitImpTfrStrc(new AfDecimal(titImpTfrStrcObj, 15, 3));
            ws.getIndTitCont().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndTitCont().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpTrasfe() {
        return titCont.getTitImpTrasfe().getTitImpTrasfe();
    }

    @Override
    public void setTitImpTrasfe(AfDecimal titImpTrasfe) {
        this.titCont.getTitImpTrasfe().setTitImpTrasfe(titImpTrasfe.copy());
    }

    @Override
    public AfDecimal getTitImpTrasfeObj() {
        if (ws.getIndTitCont().getImpTrasfe() >= 0) {
            return getTitImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpTrasfeObj(AfDecimal titImpTrasfeObj) {
        if (titImpTrasfeObj != null) {
            setTitImpTrasfe(new AfDecimal(titImpTrasfeObj, 15, 3));
            ws.getIndTitCont().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndTitCont().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpVolo() {
        return titCont.getTitImpVolo().getTitImpVolo();
    }

    @Override
    public void setTitImpVolo(AfDecimal titImpVolo) {
        this.titCont.getTitImpVolo().setTitImpVolo(titImpVolo.copy());
    }

    @Override
    public AfDecimal getTitImpVoloObj() {
        if (ws.getIndTitCont().getImpVolo() >= 0) {
            return getTitImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpVoloObj(AfDecimal titImpVoloObj) {
        if (titImpVoloObj != null) {
            setTitImpVolo(new AfDecimal(titImpVoloObj, 15, 3));
            ws.getIndTitCont().setImpVolo(((short)0));
        }
        else {
            ws.getIndTitCont().setImpVolo(((short)-1));
        }
    }

    @Override
    public int getTitNumRatAccorpate() {
        return titCont.getTitNumRatAccorpate().getTitNumRatAccorpate();
    }

    @Override
    public void setTitNumRatAccorpate(int titNumRatAccorpate) {
        this.titCont.getTitNumRatAccorpate().setTitNumRatAccorpate(titNumRatAccorpate);
    }

    @Override
    public Integer getTitNumRatAccorpateObj() {
        if (ws.getIndTitCont().getNumRatAccorpate() >= 0) {
            return ((Integer)getTitNumRatAccorpate());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitNumRatAccorpateObj(Integer titNumRatAccorpateObj) {
        if (titNumRatAccorpateObj != null) {
            setTitNumRatAccorpate(((int)titNumRatAccorpateObj));
            ws.getIndTitCont().setNumRatAccorpate(((short)0));
        }
        else {
            ws.getIndTitCont().setNumRatAccorpate(((short)-1));
        }
    }

    @Override
    public int getTitProgTit() {
        return titCont.getTitProgTit().getTitProgTit();
    }

    @Override
    public void setTitProgTit(int titProgTit) {
        this.titCont.getTitProgTit().setTitProgTit(titProgTit);
    }

    @Override
    public Integer getTitProgTitObj() {
        if (ws.getIndTitCont().getProgTit() >= 0) {
            return ((Integer)getTitProgTit());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitProgTitObj(Integer titProgTitObj) {
        if (titProgTitObj != null) {
            setTitProgTit(((int)titProgTitObj));
            ws.getIndTitCont().setProgTit(((short)0));
        }
        else {
            ws.getIndTitCont().setProgTit(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotAcqExp() {
        return titCont.getTitTotAcqExp().getTitTotAcqExp();
    }

    @Override
    public void setTitTotAcqExp(AfDecimal titTotAcqExp) {
        this.titCont.getTitTotAcqExp().setTitTotAcqExp(titTotAcqExp.copy());
    }

    @Override
    public AfDecimal getTitTotAcqExpObj() {
        if (ws.getIndTitCont().getTotAcqExp() >= 0) {
            return getTitTotAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotAcqExpObj(AfDecimal titTotAcqExpObj) {
        if (titTotAcqExpObj != null) {
            setTitTotAcqExp(new AfDecimal(titTotAcqExpObj, 15, 3));
            ws.getIndTitCont().setTotAcqExp(((short)0));
        }
        else {
            ws.getIndTitCont().setTotAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarAcq() {
        return titCont.getTitTotCarAcq().getTitTotCarAcq();
    }

    @Override
    public void setTitTotCarAcq(AfDecimal titTotCarAcq) {
        this.titCont.getTitTotCarAcq().setTitTotCarAcq(titTotCarAcq.copy());
    }

    @Override
    public AfDecimal getTitTotCarAcqObj() {
        if (ws.getIndTitCont().getTotCarAcq() >= 0) {
            return getTitTotCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarAcqObj(AfDecimal titTotCarAcqObj) {
        if (titTotCarAcqObj != null) {
            setTitTotCarAcq(new AfDecimal(titTotCarAcqObj, 15, 3));
            ws.getIndTitCont().setTotCarAcq(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarGest() {
        return titCont.getTitTotCarGest().getTitTotCarGest();
    }

    @Override
    public void setTitTotCarGest(AfDecimal titTotCarGest) {
        this.titCont.getTitTotCarGest().setTitTotCarGest(titTotCarGest.copy());
    }

    @Override
    public AfDecimal getTitTotCarGestObj() {
        if (ws.getIndTitCont().getTotCarGest() >= 0) {
            return getTitTotCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarGestObj(AfDecimal titTotCarGestObj) {
        if (titTotCarGestObj != null) {
            setTitTotCarGest(new AfDecimal(titTotCarGestObj, 15, 3));
            ws.getIndTitCont().setTotCarGest(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarIas() {
        return titCont.getTitTotCarIas().getTitTotCarIas();
    }

    @Override
    public void setTitTotCarIas(AfDecimal titTotCarIas) {
        this.titCont.getTitTotCarIas().setTitTotCarIas(titTotCarIas.copy());
    }

    @Override
    public AfDecimal getTitTotCarIasObj() {
        if (ws.getIndTitCont().getTotCarIas() >= 0) {
            return getTitTotCarIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarIasObj(AfDecimal titTotCarIasObj) {
        if (titTotCarIasObj != null) {
            setTitTotCarIas(new AfDecimal(titTotCarIasObj, 15, 3));
            ws.getIndTitCont().setTotCarIas(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarInc() {
        return titCont.getTitTotCarInc().getTitTotCarInc();
    }

    @Override
    public void setTitTotCarInc(AfDecimal titTotCarInc) {
        this.titCont.getTitTotCarInc().setTitTotCarInc(titTotCarInc.copy());
    }

    @Override
    public AfDecimal getTitTotCarIncObj() {
        if (ws.getIndTitCont().getTotCarInc() >= 0) {
            return getTitTotCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarIncObj(AfDecimal titTotCarIncObj) {
        if (titTotCarIncObj != null) {
            setTitTotCarInc(new AfDecimal(titTotCarIncObj, 15, 3));
            ws.getIndTitCont().setTotCarInc(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCnbtAntirac() {
        return titCont.getTitTotCnbtAntirac().getTitTotCnbtAntirac();
    }

    @Override
    public void setTitTotCnbtAntirac(AfDecimal titTotCnbtAntirac) {
        this.titCont.getTitTotCnbtAntirac().setTitTotCnbtAntirac(titTotCnbtAntirac.copy());
    }

    @Override
    public AfDecimal getTitTotCnbtAntiracObj() {
        if (ws.getIndTitCont().getTotCnbtAntirac() >= 0) {
            return getTitTotCnbtAntirac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCnbtAntiracObj(AfDecimal titTotCnbtAntiracObj) {
        if (titTotCnbtAntiracObj != null) {
            setTitTotCnbtAntirac(new AfDecimal(titTotCnbtAntiracObj, 15, 3));
            ws.getIndTitCont().setTotCnbtAntirac(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCnbtAntirac(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCommisInter() {
        return titCont.getTitTotCommisInter().getTitTotCommisInter();
    }

    @Override
    public void setTitTotCommisInter(AfDecimal titTotCommisInter) {
        this.titCont.getTitTotCommisInter().setTitTotCommisInter(titTotCommisInter.copy());
    }

    @Override
    public AfDecimal getTitTotCommisInterObj() {
        if (ws.getIndTitCont().getTotCommisInter() >= 0) {
            return getTitTotCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCommisInterObj(AfDecimal titTotCommisInterObj) {
        if (titTotCommisInterObj != null) {
            setTitTotCommisInter(new AfDecimal(titTotCommisInterObj, 15, 3));
            ws.getIndTitCont().setTotCommisInter(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotDir() {
        return titCont.getTitTotDir().getTitTotDir();
    }

    @Override
    public void setTitTotDir(AfDecimal titTotDir) {
        this.titCont.getTitTotDir().setTitTotDir(titTotDir.copy());
    }

    @Override
    public AfDecimal getTitTotDirObj() {
        if (ws.getIndTitCont().getTotDir() >= 0) {
            return getTitTotDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotDirObj(AfDecimal titTotDirObj) {
        if (titTotDirObj != null) {
            setTitTotDir(new AfDecimal(titTotDirObj, 15, 3));
            ws.getIndTitCont().setTotDir(((short)0));
        }
        else {
            ws.getIndTitCont().setTotDir(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrFraz() {
        return titCont.getTitTotIntrFraz().getTitTotIntrFraz();
    }

    @Override
    public void setTitTotIntrFraz(AfDecimal titTotIntrFraz) {
        this.titCont.getTitTotIntrFraz().setTitTotIntrFraz(titTotIntrFraz.copy());
    }

    @Override
    public AfDecimal getTitTotIntrFrazObj() {
        if (ws.getIndTitCont().getTotIntrFraz() >= 0) {
            return getTitTotIntrFraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrFrazObj(AfDecimal titTotIntrFrazObj) {
        if (titTotIntrFrazObj != null) {
            setTitTotIntrFraz(new AfDecimal(titTotIntrFrazObj, 15, 3));
            ws.getIndTitCont().setTotIntrFraz(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrFraz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrMora() {
        return titCont.getTitTotIntrMora().getTitTotIntrMora();
    }

    @Override
    public void setTitTotIntrMora(AfDecimal titTotIntrMora) {
        this.titCont.getTitTotIntrMora().setTitTotIntrMora(titTotIntrMora.copy());
    }

    @Override
    public AfDecimal getTitTotIntrMoraObj() {
        if (ws.getIndTitCont().getTotIntrMora() >= 0) {
            return getTitTotIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrMoraObj(AfDecimal titTotIntrMoraObj) {
        if (titTotIntrMoraObj != null) {
            setTitTotIntrMora(new AfDecimal(titTotIntrMoraObj, 15, 3));
            ws.getIndTitCont().setTotIntrMora(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrPrest() {
        return titCont.getTitTotIntrPrest().getTitTotIntrPrest();
    }

    @Override
    public void setTitTotIntrPrest(AfDecimal titTotIntrPrest) {
        this.titCont.getTitTotIntrPrest().setTitTotIntrPrest(titTotIntrPrest.copy());
    }

    @Override
    public AfDecimal getTitTotIntrPrestObj() {
        if (ws.getIndTitCont().getTotIntrPrest() >= 0) {
            return getTitTotIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrPrestObj(AfDecimal titTotIntrPrestObj) {
        if (titTotIntrPrestObj != null) {
            setTitTotIntrPrest(new AfDecimal(titTotIntrPrestObj, 15, 3));
            ws.getIndTitCont().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrRetdt() {
        return titCont.getTitTotIntrRetdt().getTitTotIntrRetdt();
    }

    @Override
    public void setTitTotIntrRetdt(AfDecimal titTotIntrRetdt) {
        this.titCont.getTitTotIntrRetdt().setTitTotIntrRetdt(titTotIntrRetdt.copy());
    }

    @Override
    public AfDecimal getTitTotIntrRetdtObj() {
        if (ws.getIndTitCont().getTotIntrRetdt() >= 0) {
            return getTitTotIntrRetdt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrRetdtObj(AfDecimal titTotIntrRetdtObj) {
        if (titTotIntrRetdtObj != null) {
            setTitTotIntrRetdt(new AfDecimal(titTotIntrRetdtObj, 15, 3));
            ws.getIndTitCont().setTotIntrRetdt(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrRetdt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrRiat() {
        return titCont.getTitTotIntrRiat().getTitTotIntrRiat();
    }

    @Override
    public void setTitTotIntrRiat(AfDecimal titTotIntrRiat) {
        this.titCont.getTitTotIntrRiat().setTitTotIntrRiat(titTotIntrRiat.copy());
    }

    @Override
    public AfDecimal getTitTotIntrRiatObj() {
        if (ws.getIndTitCont().getTotIntrRiat() >= 0) {
            return getTitTotIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrRiatObj(AfDecimal titTotIntrRiatObj) {
        if (titTotIntrRiatObj != null) {
            setTitTotIntrRiat(new AfDecimal(titTotIntrRiatObj, 15, 3));
            ws.getIndTitCont().setTotIntrRiat(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotManfeeAntic() {
        return titCont.getTitTotManfeeAntic().getTitTotManfeeAntic();
    }

    @Override
    public void setTitTotManfeeAntic(AfDecimal titTotManfeeAntic) {
        this.titCont.getTitTotManfeeAntic().setTitTotManfeeAntic(titTotManfeeAntic.copy());
    }

    @Override
    public AfDecimal getTitTotManfeeAnticObj() {
        if (ws.getIndTitCont().getTotManfeeAntic() >= 0) {
            return getTitTotManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotManfeeAnticObj(AfDecimal titTotManfeeAnticObj) {
        if (titTotManfeeAnticObj != null) {
            setTitTotManfeeAntic(new AfDecimal(titTotManfeeAnticObj, 15, 3));
            ws.getIndTitCont().setTotManfeeAntic(((short)0));
        }
        else {
            ws.getIndTitCont().setTotManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotManfeeRec() {
        return titCont.getTitTotManfeeRec().getTitTotManfeeRec();
    }

    @Override
    public void setTitTotManfeeRec(AfDecimal titTotManfeeRec) {
        this.titCont.getTitTotManfeeRec().setTitTotManfeeRec(titTotManfeeRec.copy());
    }

    @Override
    public AfDecimal getTitTotManfeeRecObj() {
        if (ws.getIndTitCont().getTotManfeeRec() >= 0) {
            return getTitTotManfeeRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotManfeeRecObj(AfDecimal titTotManfeeRecObj) {
        if (titTotManfeeRecObj != null) {
            setTitTotManfeeRec(new AfDecimal(titTotManfeeRecObj, 15, 3));
            ws.getIndTitCont().setTotManfeeRec(((short)0));
        }
        else {
            ws.getIndTitCont().setTotManfeeRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotManfeeRicor() {
        return titCont.getTitTotManfeeRicor().getTitTotManfeeRicor();
    }

    @Override
    public void setTitTotManfeeRicor(AfDecimal titTotManfeeRicor) {
        this.titCont.getTitTotManfeeRicor().setTitTotManfeeRicor(titTotManfeeRicor.copy());
    }

    @Override
    public AfDecimal getTitTotManfeeRicorObj() {
        if (ws.getIndTitCont().getTotManfeeRicor() >= 0) {
            return getTitTotManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotManfeeRicorObj(AfDecimal titTotManfeeRicorObj) {
        if (titTotManfeeRicorObj != null) {
            setTitTotManfeeRicor(new AfDecimal(titTotManfeeRicorObj, 15, 3));
            ws.getIndTitCont().setTotManfeeRicor(((short)0));
        }
        else {
            ws.getIndTitCont().setTotManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPreNet() {
        return titCont.getTitTotPreNet().getTitTotPreNet();
    }

    @Override
    public void setTitTotPreNet(AfDecimal titTotPreNet) {
        this.titCont.getTitTotPreNet().setTitTotPreNet(titTotPreNet.copy());
    }

    @Override
    public AfDecimal getTitTotPreNetObj() {
        if (ws.getIndTitCont().getTotPreNet() >= 0) {
            return getTitTotPreNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPreNetObj(AfDecimal titTotPreNetObj) {
        if (titTotPreNetObj != null) {
            setTitTotPreNet(new AfDecimal(titTotPreNetObj, 15, 3));
            ws.getIndTitCont().setTotPreNet(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPreNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPrePpIas() {
        return titCont.getTitTotPrePpIas().getTitTotPrePpIas();
    }

    @Override
    public void setTitTotPrePpIas(AfDecimal titTotPrePpIas) {
        this.titCont.getTitTotPrePpIas().setTitTotPrePpIas(titTotPrePpIas.copy());
    }

    @Override
    public AfDecimal getTitTotPrePpIasObj() {
        if (ws.getIndTitCont().getTotPrePpIas() >= 0) {
            return getTitTotPrePpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPrePpIasObj(AfDecimal titTotPrePpIasObj) {
        if (titTotPrePpIasObj != null) {
            setTitTotPrePpIas(new AfDecimal(titTotPrePpIasObj, 15, 3));
            ws.getIndTitCont().setTotPrePpIas(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPreSoloRsh() {
        return titCont.getTitTotPreSoloRsh().getTitTotPreSoloRsh();
    }

    @Override
    public void setTitTotPreSoloRsh(AfDecimal titTotPreSoloRsh) {
        this.titCont.getTitTotPreSoloRsh().setTitTotPreSoloRsh(titTotPreSoloRsh.copy());
    }

    @Override
    public AfDecimal getTitTotPreSoloRshObj() {
        if (ws.getIndTitCont().getTotPreSoloRsh() >= 0) {
            return getTitTotPreSoloRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPreSoloRshObj(AfDecimal titTotPreSoloRshObj) {
        if (titTotPreSoloRshObj != null) {
            setTitTotPreSoloRsh(new AfDecimal(titTotPreSoloRshObj, 15, 3));
            ws.getIndTitCont().setTotPreSoloRsh(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPreTot() {
        return titCont.getTitTotPreTot().getTitTotPreTot();
    }

    @Override
    public void setTitTotPreTot(AfDecimal titTotPreTot) {
        this.titCont.getTitTotPreTot().setTitTotPreTot(titTotPreTot.copy());
    }

    @Override
    public AfDecimal getTitTotPreTotObj() {
        if (ws.getIndTitCont().getTotPreTot() >= 0) {
            return getTitTotPreTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPreTotObj(AfDecimal titTotPreTotObj) {
        if (titTotPreTotObj != null) {
            setTitTotPreTot(new AfDecimal(titTotPreTotObj, 15, 3));
            ws.getIndTitCont().setTotPreTot(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPreTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvAcq1aa() {
        return titCont.getTitTotProvAcq1aa().getTitTotProvAcq1aa();
    }

    @Override
    public void setTitTotProvAcq1aa(AfDecimal titTotProvAcq1aa) {
        this.titCont.getTitTotProvAcq1aa().setTitTotProvAcq1aa(titTotProvAcq1aa.copy());
    }

    @Override
    public AfDecimal getTitTotProvAcq1aaObj() {
        if (ws.getIndTitCont().getTotProvAcq1aa() >= 0) {
            return getTitTotProvAcq1aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvAcq1aaObj(AfDecimal titTotProvAcq1aaObj) {
        if (titTotProvAcq1aaObj != null) {
            setTitTotProvAcq1aa(new AfDecimal(titTotProvAcq1aaObj, 15, 3));
            ws.getIndTitCont().setTotProvAcq1aa(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvAcq1aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvAcq2aa() {
        return titCont.getTitTotProvAcq2aa().getTitTotProvAcq2aa();
    }

    @Override
    public void setTitTotProvAcq2aa(AfDecimal titTotProvAcq2aa) {
        this.titCont.getTitTotProvAcq2aa().setTitTotProvAcq2aa(titTotProvAcq2aa.copy());
    }

    @Override
    public AfDecimal getTitTotProvAcq2aaObj() {
        if (ws.getIndTitCont().getTotProvAcq2aa() >= 0) {
            return getTitTotProvAcq2aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvAcq2aaObj(AfDecimal titTotProvAcq2aaObj) {
        if (titTotProvAcq2aaObj != null) {
            setTitTotProvAcq2aa(new AfDecimal(titTotProvAcq2aaObj, 15, 3));
            ws.getIndTitCont().setTotProvAcq2aa(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvAcq2aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvDaRec() {
        return titCont.getTitTotProvDaRec().getTitTotProvDaRec();
    }

    @Override
    public void setTitTotProvDaRec(AfDecimal titTotProvDaRec) {
        this.titCont.getTitTotProvDaRec().setTitTotProvDaRec(titTotProvDaRec.copy());
    }

    @Override
    public AfDecimal getTitTotProvDaRecObj() {
        if (ws.getIndTitCont().getTotProvDaRec() >= 0) {
            return getTitTotProvDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvDaRecObj(AfDecimal titTotProvDaRecObj) {
        if (titTotProvDaRecObj != null) {
            setTitTotProvDaRec(new AfDecimal(titTotProvDaRecObj, 15, 3));
            ws.getIndTitCont().setTotProvDaRec(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvDaRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvInc() {
        return titCont.getTitTotProvInc().getTitTotProvInc();
    }

    @Override
    public void setTitTotProvInc(AfDecimal titTotProvInc) {
        this.titCont.getTitTotProvInc().setTitTotProvInc(titTotProvInc.copy());
    }

    @Override
    public AfDecimal getTitTotProvIncObj() {
        if (ws.getIndTitCont().getTotProvInc() >= 0) {
            return getTitTotProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvIncObj(AfDecimal titTotProvIncObj) {
        if (titTotProvIncObj != null) {
            setTitTotProvInc(new AfDecimal(titTotProvIncObj, 15, 3));
            ws.getIndTitCont().setTotProvInc(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvRicor() {
        return titCont.getTitTotProvRicor().getTitTotProvRicor();
    }

    @Override
    public void setTitTotProvRicor(AfDecimal titTotProvRicor) {
        this.titCont.getTitTotProvRicor().setTitTotProvRicor(titTotProvRicor.copy());
    }

    @Override
    public AfDecimal getTitTotProvRicorObj() {
        if (ws.getIndTitCont().getTotProvRicor() >= 0) {
            return getTitTotProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvRicorObj(AfDecimal titTotProvRicorObj) {
        if (titTotProvRicorObj != null) {
            setTitTotProvRicor(new AfDecimal(titTotProvRicorObj, 15, 3));
            ws.getIndTitCont().setTotProvRicor(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotRemunAss() {
        return titCont.getTitTotRemunAss().getTitTotRemunAss();
    }

    @Override
    public void setTitTotRemunAss(AfDecimal titTotRemunAss) {
        this.titCont.getTitTotRemunAss().setTitTotRemunAss(titTotRemunAss.copy());
    }

    @Override
    public AfDecimal getTitTotRemunAssObj() {
        if (ws.getIndTitCont().getTotRemunAss() >= 0) {
            return getTitTotRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotRemunAssObj(AfDecimal titTotRemunAssObj) {
        if (titTotRemunAssObj != null) {
            setTitTotRemunAss(new AfDecimal(titTotRemunAssObj, 15, 3));
            ws.getIndTitCont().setTotRemunAss(((short)0));
        }
        else {
            ws.getIndTitCont().setTotRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprAlt() {
        return titCont.getTitTotSoprAlt().getTitTotSoprAlt();
    }

    @Override
    public void setTitTotSoprAlt(AfDecimal titTotSoprAlt) {
        this.titCont.getTitTotSoprAlt().setTitTotSoprAlt(titTotSoprAlt.copy());
    }

    @Override
    public AfDecimal getTitTotSoprAltObj() {
        if (ws.getIndTitCont().getTotSoprAlt() >= 0) {
            return getTitTotSoprAlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprAltObj(AfDecimal titTotSoprAltObj) {
        if (titTotSoprAltObj != null) {
            setTitTotSoprAlt(new AfDecimal(titTotSoprAltObj, 15, 3));
            ws.getIndTitCont().setTotSoprAlt(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprAlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprProf() {
        return titCont.getTitTotSoprProf().getTitTotSoprProf();
    }

    @Override
    public void setTitTotSoprProf(AfDecimal titTotSoprProf) {
        this.titCont.getTitTotSoprProf().setTitTotSoprProf(titTotSoprProf.copy());
    }

    @Override
    public AfDecimal getTitTotSoprProfObj() {
        if (ws.getIndTitCont().getTotSoprProf() >= 0) {
            return getTitTotSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprProfObj(AfDecimal titTotSoprProfObj) {
        if (titTotSoprProfObj != null) {
            setTitTotSoprProf(new AfDecimal(titTotSoprProfObj, 15, 3));
            ws.getIndTitCont().setTotSoprProf(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprSan() {
        return titCont.getTitTotSoprSan().getTitTotSoprSan();
    }

    @Override
    public void setTitTotSoprSan(AfDecimal titTotSoprSan) {
        this.titCont.getTitTotSoprSan().setTitTotSoprSan(titTotSoprSan.copy());
    }

    @Override
    public AfDecimal getTitTotSoprSanObj() {
        if (ws.getIndTitCont().getTotSoprSan() >= 0) {
            return getTitTotSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprSanObj(AfDecimal titTotSoprSanObj) {
        if (titTotSoprSanObj != null) {
            setTitTotSoprSan(new AfDecimal(titTotSoprSanObj, 15, 3));
            ws.getIndTitCont().setTotSoprSan(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprSpo() {
        return titCont.getTitTotSoprSpo().getTitTotSoprSpo();
    }

    @Override
    public void setTitTotSoprSpo(AfDecimal titTotSoprSpo) {
        this.titCont.getTitTotSoprSpo().setTitTotSoprSpo(titTotSoprSpo.copy());
    }

    @Override
    public AfDecimal getTitTotSoprSpoObj() {
        if (ws.getIndTitCont().getTotSoprSpo() >= 0) {
            return getTitTotSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprSpoObj(AfDecimal titTotSoprSpoObj) {
        if (titTotSoprSpoObj != null) {
            setTitTotSoprSpo(new AfDecimal(titTotSoprSpoObj, 15, 3));
            ws.getIndTitCont().setTotSoprSpo(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprTec() {
        return titCont.getTitTotSoprTec().getTitTotSoprTec();
    }

    @Override
    public void setTitTotSoprTec(AfDecimal titTotSoprTec) {
        this.titCont.getTitTotSoprTec().setTitTotSoprTec(titTotSoprTec.copy());
    }

    @Override
    public AfDecimal getTitTotSoprTecObj() {
        if (ws.getIndTitCont().getTotSoprTec() >= 0) {
            return getTitTotSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprTecObj(AfDecimal titTotSoprTecObj) {
        if (titTotSoprTecObj != null) {
            setTitTotSoprTec(new AfDecimal(titTotSoprTecObj, 15, 3));
            ws.getIndTitCont().setTotSoprTec(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSpeAge() {
        return titCont.getTitTotSpeAge().getTitTotSpeAge();
    }

    @Override
    public void setTitTotSpeAge(AfDecimal titTotSpeAge) {
        this.titCont.getTitTotSpeAge().setTitTotSpeAge(titTotSpeAge.copy());
    }

    @Override
    public AfDecimal getTitTotSpeAgeObj() {
        if (ws.getIndTitCont().getTotSpeAge() >= 0) {
            return getTitTotSpeAge();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSpeAgeObj(AfDecimal titTotSpeAgeObj) {
        if (titTotSpeAgeObj != null) {
            setTitTotSpeAge(new AfDecimal(titTotSpeAgeObj, 15, 3));
            ws.getIndTitCont().setTotSpeAge(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSpeAge(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSpeMed() {
        return titCont.getTitTotSpeMed().getTitTotSpeMed();
    }

    @Override
    public void setTitTotSpeMed(AfDecimal titTotSpeMed) {
        this.titCont.getTitTotSpeMed().setTitTotSpeMed(titTotSpeMed.copy());
    }

    @Override
    public AfDecimal getTitTotSpeMedObj() {
        if (ws.getIndTitCont().getTotSpeMed() >= 0) {
            return getTitTotSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSpeMedObj(AfDecimal titTotSpeMedObj) {
        if (titTotSpeMedObj != null) {
            setTitTotSpeMed(new AfDecimal(titTotSpeMedObj, 15, 3));
            ws.getIndTitCont().setTotSpeMed(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSpeMed(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotTax() {
        return titCont.getTitTotTax().getTitTotTax();
    }

    @Override
    public void setTitTotTax(AfDecimal titTotTax) {
        this.titCont.getTitTotTax().setTitTotTax(titTotTax.copy());
    }

    @Override
    public AfDecimal getTitTotTaxObj() {
        if (ws.getIndTitCont().getTotTax() >= 0) {
            return getTitTotTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotTaxObj(AfDecimal titTotTaxObj) {
        if (titTotTaxObj != null) {
            setTitTotTax(new AfDecimal(titTotTaxObj, 15, 3));
            ws.getIndTitCont().setTotTax(((short)0));
        }
        else {
            ws.getIndTitCont().setTotTax(((short)-1));
        }
    }

    @Override
    public String getTitTpCausDispStor() {
        return titCont.getTitTpCausDispStor();
    }

    @Override
    public void setTitTpCausDispStor(String titTpCausDispStor) {
        this.titCont.setTitTpCausDispStor(titTpCausDispStor);
    }

    @Override
    public String getTitTpCausDispStorObj() {
        if (ws.getIndTitCont().getTpCausDispStor() >= 0) {
            return getTitTpCausDispStor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpCausDispStorObj(String titTpCausDispStorObj) {
        if (titTpCausDispStorObj != null) {
            setTitTpCausDispStor(titTpCausDispStorObj);
            ws.getIndTitCont().setTpCausDispStor(((short)0));
        }
        else {
            ws.getIndTitCont().setTpCausDispStor(((short)-1));
        }
    }

    @Override
    public String getTitTpCausRimb() {
        return titCont.getTitTpCausRimb();
    }

    @Override
    public void setTitTpCausRimb(String titTpCausRimb) {
        this.titCont.setTitTpCausRimb(titTpCausRimb);
    }

    @Override
    public String getTitTpCausRimbObj() {
        if (ws.getIndTitCont().getTpCausRimb() >= 0) {
            return getTitTpCausRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpCausRimbObj(String titTpCausRimbObj) {
        if (titTpCausRimbObj != null) {
            setTitTpCausRimb(titTpCausRimbObj);
            ws.getIndTitCont().setTpCausRimb(((short)0));
        }
        else {
            ws.getIndTitCont().setTpCausRimb(((short)-1));
        }
    }

    @Override
    public int getTitTpCausStor() {
        return titCont.getTitTpCausStor().getTitTpCausStor();
    }

    @Override
    public void setTitTpCausStor(int titTpCausStor) {
        this.titCont.getTitTpCausStor().setTitTpCausStor(titTpCausStor);
    }

    @Override
    public Integer getTitTpCausStorObj() {
        if (ws.getIndTitCont().getTpCausStor() >= 0) {
            return ((Integer)getTitTpCausStor());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpCausStorObj(Integer titTpCausStorObj) {
        if (titTpCausStorObj != null) {
            setTitTpCausStor(((int)titTpCausStorObj));
            ws.getIndTitCont().setTpCausStor(((short)0));
        }
        else {
            ws.getIndTitCont().setTpCausStor(((short)-1));
        }
    }

    @Override
    public String getTitTpEsiRid() {
        return titCont.getTitTpEsiRid();
    }

    @Override
    public void setTitTpEsiRid(String titTpEsiRid) {
        this.titCont.setTitTpEsiRid(titTpEsiRid);
    }

    @Override
    public String getTitTpEsiRidObj() {
        if (ws.getIndTitCont().getTpEsiRid() >= 0) {
            return getTitTpEsiRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpEsiRidObj(String titTpEsiRidObj) {
        if (titTpEsiRidObj != null) {
            setTitTpEsiRid(titTpEsiRidObj);
            ws.getIndTitCont().setTpEsiRid(((short)0));
        }
        else {
            ws.getIndTitCont().setTpEsiRid(((short)-1));
        }
    }

    @Override
    public String getTitTpMezPagAdd() {
        return titCont.getTitTpMezPagAdd();
    }

    @Override
    public void setTitTpMezPagAdd(String titTpMezPagAdd) {
        this.titCont.setTitTpMezPagAdd(titTpMezPagAdd);
    }

    @Override
    public String getTitTpMezPagAddObj() {
        if (ws.getIndTitCont().getTpMezPagAdd() >= 0) {
            return getTitTpMezPagAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpMezPagAddObj(String titTpMezPagAddObj) {
        if (titTpMezPagAddObj != null) {
            setTitTpMezPagAdd(titTpMezPagAddObj);
            ws.getIndTitCont().setTpMezPagAdd(((short)0));
        }
        else {
            ws.getIndTitCont().setTpMezPagAdd(((short)-1));
        }
    }

    @Override
    public String getTitTpOgg() {
        return titCont.getTitTpOgg();
    }

    @Override
    public void setTitTpOgg(String titTpOgg) {
        this.titCont.setTitTpOgg(titTpOgg);
    }

    @Override
    public String getTitTpPreTit() {
        return titCont.getTitTpPreTit();
    }

    @Override
    public void setTitTpPreTit(String titTpPreTit) {
        this.titCont.setTitTpPreTit(titTpPreTit);
    }

    @Override
    public String getTitTpStatTit() {
        return titCont.getTitTpStatTit();
    }

    @Override
    public void setTitTpStatTit(String titTpStatTit) {
        this.titCont.setTitTpStatTit(titTpStatTit);
    }

    @Override
    public String getTitTpTit() {
        return titCont.getTitTpTit();
    }

    @Override
    public void setTitTpTit(String titTpTit) {
        this.titCont.setTitTpTit(titTpTit);
    }

    @Override
    public String getTitTpTitMigraz() {
        return titCont.getTitTpTitMigraz();
    }

    @Override
    public void setTitTpTitMigraz(String titTpTitMigraz) {
        this.titCont.setTitTpTitMigraz(titTpTitMigraz);
    }

    @Override
    public String getTitTpTitMigrazObj() {
        if (ws.getIndTitCont().getTpTitMigraz() >= 0) {
            return getTitTpTitMigraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpTitMigrazObj(String titTpTitMigrazObj) {
        if (titTpTitMigrazObj != null) {
            setTitTpTitMigraz(titTpTitMigrazObj);
            ws.getIndTitCont().setTpTitMigraz(((short)0));
        }
        else {
            ws.getIndTitCont().setTpTitMigraz(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
