package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.StraDiInvstDao;
import it.accenture.jnais.commons.data.to.IStraDiInvst;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbssdi0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.SdiDtFis;
import it.accenture.jnais.ws.redefines.SdiFrqValut;
import it.accenture.jnais.ws.redefines.SdiIdMoviChiu;
import it.accenture.jnais.ws.redefines.SdiModGest;
import it.accenture.jnais.ws.redefines.SdiPcProtezione;
import it.accenture.jnais.ws.StraDiInvstIdbssdi0;

/**Original name: IDBSSDI0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  07 DIC 2017.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbssdi0 extends Program implements IStraDiInvst {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private StraDiInvstDao straDiInvstDao = new StraDiInvstDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbssdi0Data ws = new Idbssdi0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: STRA-DI-INVST
    private StraDiInvstIdbssdi0 straDiInvst;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSSDI0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, StraDiInvstIdbssdi0 straDiInvst) {
        this.idsv0003 = idsv0003;
        this.straDiInvst = straDiInvst;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbssdi0 getInstance() {
        return ((Idbssdi0)Programs.getInstance(Idbssdi0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSSDI0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSSDI0");
        // COB_CODE: MOVE 'STRA_DI_INVST' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("STRA_DI_INVST");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_STRA_DI_INVST
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_STRA
        //                ,MOD_GEST
        //                ,VAR_RIFTO
        //                ,VAL_VAR_RIFTO
        //                ,DT_FIS
        //                ,FRQ_VALUT
        //                ,FL_INI_END_PER
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_STRA
        //                ,TP_PROVZA_STRA
        //                ,PC_PROTEZIONE
        //             INTO
        //                :SDI-ID-STRA-DI-INVST
        //               ,:SDI-ID-OGG
        //               ,:SDI-TP-OGG
        //               ,:SDI-ID-MOVI-CRZ
        //               ,:SDI-ID-MOVI-CHIU
        //                :IND-SDI-ID-MOVI-CHIU
        //               ,:SDI-DT-INI-EFF-DB
        //               ,:SDI-DT-END-EFF-DB
        //               ,:SDI-COD-COMP-ANIA
        //               ,:SDI-COD-STRA
        //                :IND-SDI-COD-STRA
        //               ,:SDI-MOD-GEST
        //                :IND-SDI-MOD-GEST
        //               ,:SDI-VAR-RIFTO
        //                :IND-SDI-VAR-RIFTO
        //               ,:SDI-VAL-VAR-RIFTO
        //                :IND-SDI-VAL-VAR-RIFTO
        //               ,:SDI-DT-FIS
        //                :IND-SDI-DT-FIS
        //               ,:SDI-FRQ-VALUT
        //                :IND-SDI-FRQ-VALUT
        //               ,:SDI-FL-INI-END-PER
        //                :IND-SDI-FL-INI-END-PER
        //               ,:SDI-DS-RIGA
        //               ,:SDI-DS-OPER-SQL
        //               ,:SDI-DS-VER
        //               ,:SDI-DS-TS-INI-CPTZ
        //               ,:SDI-DS-TS-END-CPTZ
        //               ,:SDI-DS-UTENTE
        //               ,:SDI-DS-STATO-ELAB
        //               ,:SDI-TP-STRA
        //                :IND-SDI-TP-STRA
        //               ,:SDI-TP-PROVZA-STRA
        //                :IND-SDI-TP-PROVZA-STRA
        //               ,:SDI-PC-PROTEZIONE
        //                :IND-SDI-PC-PROTEZIONE
        //             FROM STRA_DI_INVST
        //             WHERE     DS_RIGA = :SDI-DS-RIGA
        //           END-EXEC.
        straDiInvstDao.selectBySdiDsRiga(straDiInvst.getSdiDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO STRA_DI_INVST
            //                  (
            //                     ID_STRA_DI_INVST
            //                    ,ID_OGG
            //                    ,TP_OGG
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,COD_STRA
            //                    ,MOD_GEST
            //                    ,VAR_RIFTO
            //                    ,VAL_VAR_RIFTO
            //                    ,DT_FIS
            //                    ,FRQ_VALUT
            //                    ,FL_INI_END_PER
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,TP_STRA
            //                    ,TP_PROVZA_STRA
            //                    ,PC_PROTEZIONE
            //                  )
            //              VALUES
            //                  (
            //                    :SDI-ID-STRA-DI-INVST
            //                    ,:SDI-ID-OGG
            //                    ,:SDI-TP-OGG
            //                    ,:SDI-ID-MOVI-CRZ
            //                    ,:SDI-ID-MOVI-CHIU
            //                     :IND-SDI-ID-MOVI-CHIU
            //                    ,:SDI-DT-INI-EFF-DB
            //                    ,:SDI-DT-END-EFF-DB
            //                    ,:SDI-COD-COMP-ANIA
            //                    ,:SDI-COD-STRA
            //                     :IND-SDI-COD-STRA
            //                    ,:SDI-MOD-GEST
            //                     :IND-SDI-MOD-GEST
            //                    ,:SDI-VAR-RIFTO
            //                     :IND-SDI-VAR-RIFTO
            //                    ,:SDI-VAL-VAR-RIFTO
            //                     :IND-SDI-VAL-VAR-RIFTO
            //                    ,:SDI-DT-FIS
            //                     :IND-SDI-DT-FIS
            //                    ,:SDI-FRQ-VALUT
            //                     :IND-SDI-FRQ-VALUT
            //                    ,:SDI-FL-INI-END-PER
            //                     :IND-SDI-FL-INI-END-PER
            //                    ,:SDI-DS-RIGA
            //                    ,:SDI-DS-OPER-SQL
            //                    ,:SDI-DS-VER
            //                    ,:SDI-DS-TS-INI-CPTZ
            //                    ,:SDI-DS-TS-END-CPTZ
            //                    ,:SDI-DS-UTENTE
            //                    ,:SDI-DS-STATO-ELAB
            //                    ,:SDI-TP-STRA
            //                     :IND-SDI-TP-STRA
            //                    ,:SDI-TP-PROVZA-STRA
            //                     :IND-SDI-TP-PROVZA-STRA
            //                    ,:SDI-PC-PROTEZIONE
            //                     :IND-SDI-PC-PROTEZIONE
            //                  )
            //           END-EXEC
            straDiInvstDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE STRA_DI_INVST SET
        //                   ID_STRA_DI_INVST       =
        //                :SDI-ID-STRA-DI-INVST
        //                  ,ID_OGG                 =
        //                :SDI-ID-OGG
        //                  ,TP_OGG                 =
        //                :SDI-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :SDI-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :SDI-ID-MOVI-CHIU
        //                                       :IND-SDI-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :SDI-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :SDI-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :SDI-COD-COMP-ANIA
        //                  ,COD_STRA               =
        //                :SDI-COD-STRA
        //                                       :IND-SDI-COD-STRA
        //                  ,MOD_GEST               =
        //                :SDI-MOD-GEST
        //                                       :IND-SDI-MOD-GEST
        //                  ,VAR_RIFTO              =
        //                :SDI-VAR-RIFTO
        //                                       :IND-SDI-VAR-RIFTO
        //                  ,VAL_VAR_RIFTO          =
        //                :SDI-VAL-VAR-RIFTO
        //                                       :IND-SDI-VAL-VAR-RIFTO
        //                  ,DT_FIS                 =
        //                :SDI-DT-FIS
        //                                       :IND-SDI-DT-FIS
        //                  ,FRQ_VALUT              =
        //                :SDI-FRQ-VALUT
        //                                       :IND-SDI-FRQ-VALUT
        //                  ,FL_INI_END_PER         =
        //                :SDI-FL-INI-END-PER
        //                                       :IND-SDI-FL-INI-END-PER
        //                  ,DS_RIGA                =
        //                :SDI-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :SDI-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :SDI-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :SDI-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :SDI-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :SDI-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :SDI-DS-STATO-ELAB
        //                  ,TP_STRA                =
        //                :SDI-TP-STRA
        //                                       :IND-SDI-TP-STRA
        //                  ,TP_PROVZA_STRA         =
        //                :SDI-TP-PROVZA-STRA
        //                                       :IND-SDI-TP-PROVZA-STRA
        //                  ,PC_PROTEZIONE          =
        //                :SDI-PC-PROTEZIONE
        //                                       :IND-SDI-PC-PROTEZIONE
        //                WHERE     DS_RIGA = :SDI-DS-RIGA
        //           END-EXEC.
        straDiInvstDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM STRA_DI_INVST
        //                WHERE     DS_RIGA = :SDI-DS-RIGA
        //           END-EXEC.
        straDiInvstDao.deleteBySdiDsRiga(straDiInvst.getSdiDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-SDI CURSOR FOR
        //              SELECT
        //                     ID_STRA_DI_INVST
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_STRA
        //                    ,MOD_GEST
        //                    ,VAR_RIFTO
        //                    ,VAL_VAR_RIFTO
        //                    ,DT_FIS
        //                    ,FRQ_VALUT
        //                    ,FL_INI_END_PER
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_STRA
        //                    ,TP_PROVZA_STRA
        //                    ,PC_PROTEZIONE
        //              FROM STRA_DI_INVST
        //              WHERE     ID_STRA_DI_INVST = :SDI-ID-STRA-DI-INVST
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_STRA_DI_INVST
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_STRA
        //                ,MOD_GEST
        //                ,VAR_RIFTO
        //                ,VAL_VAR_RIFTO
        //                ,DT_FIS
        //                ,FRQ_VALUT
        //                ,FL_INI_END_PER
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_STRA
        //                ,TP_PROVZA_STRA
        //                ,PC_PROTEZIONE
        //             INTO
        //                :SDI-ID-STRA-DI-INVST
        //               ,:SDI-ID-OGG
        //               ,:SDI-TP-OGG
        //               ,:SDI-ID-MOVI-CRZ
        //               ,:SDI-ID-MOVI-CHIU
        //                :IND-SDI-ID-MOVI-CHIU
        //               ,:SDI-DT-INI-EFF-DB
        //               ,:SDI-DT-END-EFF-DB
        //               ,:SDI-COD-COMP-ANIA
        //               ,:SDI-COD-STRA
        //                :IND-SDI-COD-STRA
        //               ,:SDI-MOD-GEST
        //                :IND-SDI-MOD-GEST
        //               ,:SDI-VAR-RIFTO
        //                :IND-SDI-VAR-RIFTO
        //               ,:SDI-VAL-VAR-RIFTO
        //                :IND-SDI-VAL-VAR-RIFTO
        //               ,:SDI-DT-FIS
        //                :IND-SDI-DT-FIS
        //               ,:SDI-FRQ-VALUT
        //                :IND-SDI-FRQ-VALUT
        //               ,:SDI-FL-INI-END-PER
        //                :IND-SDI-FL-INI-END-PER
        //               ,:SDI-DS-RIGA
        //               ,:SDI-DS-OPER-SQL
        //               ,:SDI-DS-VER
        //               ,:SDI-DS-TS-INI-CPTZ
        //               ,:SDI-DS-TS-END-CPTZ
        //               ,:SDI-DS-UTENTE
        //               ,:SDI-DS-STATO-ELAB
        //               ,:SDI-TP-STRA
        //                :IND-SDI-TP-STRA
        //               ,:SDI-TP-PROVZA-STRA
        //                :IND-SDI-TP-PROVZA-STRA
        //               ,:SDI-PC-PROTEZIONE
        //                :IND-SDI-PC-PROTEZIONE
        //             FROM STRA_DI_INVST
        //             WHERE     ID_STRA_DI_INVST = :SDI-ID-STRA-DI-INVST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        straDiInvstDao.selectRec(straDiInvst.getSdiIdStraDiInvst(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE STRA_DI_INVST SET
        //                   ID_STRA_DI_INVST       =
        //                :SDI-ID-STRA-DI-INVST
        //                  ,ID_OGG                 =
        //                :SDI-ID-OGG
        //                  ,TP_OGG                 =
        //                :SDI-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :SDI-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :SDI-ID-MOVI-CHIU
        //                                       :IND-SDI-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :SDI-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :SDI-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :SDI-COD-COMP-ANIA
        //                  ,COD_STRA               =
        //                :SDI-COD-STRA
        //                                       :IND-SDI-COD-STRA
        //                  ,MOD_GEST               =
        //                :SDI-MOD-GEST
        //                                       :IND-SDI-MOD-GEST
        //                  ,VAR_RIFTO              =
        //                :SDI-VAR-RIFTO
        //                                       :IND-SDI-VAR-RIFTO
        //                  ,VAL_VAR_RIFTO          =
        //                :SDI-VAL-VAR-RIFTO
        //                                       :IND-SDI-VAL-VAR-RIFTO
        //                  ,DT_FIS                 =
        //                :SDI-DT-FIS
        //                                       :IND-SDI-DT-FIS
        //                  ,FRQ_VALUT              =
        //                :SDI-FRQ-VALUT
        //                                       :IND-SDI-FRQ-VALUT
        //                  ,FL_INI_END_PER         =
        //                :SDI-FL-INI-END-PER
        //                                       :IND-SDI-FL-INI-END-PER
        //                  ,DS_RIGA                =
        //                :SDI-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :SDI-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :SDI-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :SDI-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :SDI-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :SDI-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :SDI-DS-STATO-ELAB
        //                  ,TP_STRA                =
        //                :SDI-TP-STRA
        //                                       :IND-SDI-TP-STRA
        //                  ,TP_PROVZA_STRA         =
        //                :SDI-TP-PROVZA-STRA
        //                                       :IND-SDI-TP-PROVZA-STRA
        //                  ,PC_PROTEZIONE          =
        //                :SDI-PC-PROTEZIONE
        //                                       :IND-SDI-PC-PROTEZIONE
        //                WHERE     DS_RIGA = :SDI-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        straDiInvstDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-SDI
        //           END-EXEC.
        straDiInvstDao.openCIdUpdEffSdi(straDiInvst.getSdiIdStraDiInvst(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-SDI
        //           END-EXEC.
        straDiInvstDao.closeCIdUpdEffSdi();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-SDI
        //           INTO
        //                :SDI-ID-STRA-DI-INVST
        //               ,:SDI-ID-OGG
        //               ,:SDI-TP-OGG
        //               ,:SDI-ID-MOVI-CRZ
        //               ,:SDI-ID-MOVI-CHIU
        //                :IND-SDI-ID-MOVI-CHIU
        //               ,:SDI-DT-INI-EFF-DB
        //               ,:SDI-DT-END-EFF-DB
        //               ,:SDI-COD-COMP-ANIA
        //               ,:SDI-COD-STRA
        //                :IND-SDI-COD-STRA
        //               ,:SDI-MOD-GEST
        //                :IND-SDI-MOD-GEST
        //               ,:SDI-VAR-RIFTO
        //                :IND-SDI-VAR-RIFTO
        //               ,:SDI-VAL-VAR-RIFTO
        //                :IND-SDI-VAL-VAR-RIFTO
        //               ,:SDI-DT-FIS
        //                :IND-SDI-DT-FIS
        //               ,:SDI-FRQ-VALUT
        //                :IND-SDI-FRQ-VALUT
        //               ,:SDI-FL-INI-END-PER
        //                :IND-SDI-FL-INI-END-PER
        //               ,:SDI-DS-RIGA
        //               ,:SDI-DS-OPER-SQL
        //               ,:SDI-DS-VER
        //               ,:SDI-DS-TS-INI-CPTZ
        //               ,:SDI-DS-TS-END-CPTZ
        //               ,:SDI-DS-UTENTE
        //               ,:SDI-DS-STATO-ELAB
        //               ,:SDI-TP-STRA
        //                :IND-SDI-TP-STRA
        //               ,:SDI-TP-PROVZA-STRA
        //                :IND-SDI-TP-PROVZA-STRA
        //               ,:SDI-PC-PROTEZIONE
        //                :IND-SDI-PC-PROTEZIONE
        //           END-EXEC.
        straDiInvstDao.fetchCIdUpdEffSdi(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-EFF-SDI CURSOR FOR
        //              SELECT
        //                     ID_STRA_DI_INVST
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_STRA
        //                    ,MOD_GEST
        //                    ,VAR_RIFTO
        //                    ,VAL_VAR_RIFTO
        //                    ,DT_FIS
        //                    ,FRQ_VALUT
        //                    ,FL_INI_END_PER
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_STRA
        //                    ,TP_PROVZA_STRA
        //                    ,PC_PROTEZIONE
        //              FROM STRA_DI_INVST
        //              WHERE     ID_OGG = :SDI-ID-OGG
        //                    AND TP_OGG = :SDI-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_STRA_DI_INVST ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_STRA_DI_INVST
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_STRA
        //                ,MOD_GEST
        //                ,VAR_RIFTO
        //                ,VAL_VAR_RIFTO
        //                ,DT_FIS
        //                ,FRQ_VALUT
        //                ,FL_INI_END_PER
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_STRA
        //                ,TP_PROVZA_STRA
        //                ,PC_PROTEZIONE
        //             INTO
        //                :SDI-ID-STRA-DI-INVST
        //               ,:SDI-ID-OGG
        //               ,:SDI-TP-OGG
        //               ,:SDI-ID-MOVI-CRZ
        //               ,:SDI-ID-MOVI-CHIU
        //                :IND-SDI-ID-MOVI-CHIU
        //               ,:SDI-DT-INI-EFF-DB
        //               ,:SDI-DT-END-EFF-DB
        //               ,:SDI-COD-COMP-ANIA
        //               ,:SDI-COD-STRA
        //                :IND-SDI-COD-STRA
        //               ,:SDI-MOD-GEST
        //                :IND-SDI-MOD-GEST
        //               ,:SDI-VAR-RIFTO
        //                :IND-SDI-VAR-RIFTO
        //               ,:SDI-VAL-VAR-RIFTO
        //                :IND-SDI-VAL-VAR-RIFTO
        //               ,:SDI-DT-FIS
        //                :IND-SDI-DT-FIS
        //               ,:SDI-FRQ-VALUT
        //                :IND-SDI-FRQ-VALUT
        //               ,:SDI-FL-INI-END-PER
        //                :IND-SDI-FL-INI-END-PER
        //               ,:SDI-DS-RIGA
        //               ,:SDI-DS-OPER-SQL
        //               ,:SDI-DS-VER
        //               ,:SDI-DS-TS-INI-CPTZ
        //               ,:SDI-DS-TS-END-CPTZ
        //               ,:SDI-DS-UTENTE
        //               ,:SDI-DS-STATO-ELAB
        //               ,:SDI-TP-STRA
        //                :IND-SDI-TP-STRA
        //               ,:SDI-TP-PROVZA-STRA
        //                :IND-SDI-TP-PROVZA-STRA
        //               ,:SDI-PC-PROTEZIONE
        //                :IND-SDI-PC-PROTEZIONE
        //             FROM STRA_DI_INVST
        //             WHERE     ID_OGG = :SDI-ID-OGG
        //                    AND TP_OGG = :SDI-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        straDiInvstDao.selectRec1(straDiInvst.getSdiIdOgg(), straDiInvst.getSdiTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-EFF-SDI
        //           END-EXEC.
        straDiInvstDao.openCIdoEffSdi(straDiInvst.getSdiIdOgg(), straDiInvst.getSdiTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-EFF-SDI
        //           END-EXEC.
        straDiInvstDao.closeCIdoEffSdi();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-EFF-SDI
        //           INTO
        //                :SDI-ID-STRA-DI-INVST
        //               ,:SDI-ID-OGG
        //               ,:SDI-TP-OGG
        //               ,:SDI-ID-MOVI-CRZ
        //               ,:SDI-ID-MOVI-CHIU
        //                :IND-SDI-ID-MOVI-CHIU
        //               ,:SDI-DT-INI-EFF-DB
        //               ,:SDI-DT-END-EFF-DB
        //               ,:SDI-COD-COMP-ANIA
        //               ,:SDI-COD-STRA
        //                :IND-SDI-COD-STRA
        //               ,:SDI-MOD-GEST
        //                :IND-SDI-MOD-GEST
        //               ,:SDI-VAR-RIFTO
        //                :IND-SDI-VAR-RIFTO
        //               ,:SDI-VAL-VAR-RIFTO
        //                :IND-SDI-VAL-VAR-RIFTO
        //               ,:SDI-DT-FIS
        //                :IND-SDI-DT-FIS
        //               ,:SDI-FRQ-VALUT
        //                :IND-SDI-FRQ-VALUT
        //               ,:SDI-FL-INI-END-PER
        //                :IND-SDI-FL-INI-END-PER
        //               ,:SDI-DS-RIGA
        //               ,:SDI-DS-OPER-SQL
        //               ,:SDI-DS-VER
        //               ,:SDI-DS-TS-INI-CPTZ
        //               ,:SDI-DS-TS-END-CPTZ
        //               ,:SDI-DS-UTENTE
        //               ,:SDI-DS-STATO-ELAB
        //               ,:SDI-TP-STRA
        //                :IND-SDI-TP-STRA
        //               ,:SDI-TP-PROVZA-STRA
        //                :IND-SDI-TP-PROVZA-STRA
        //               ,:SDI-PC-PROTEZIONE
        //                :IND-SDI-PC-PROTEZIONE
        //           END-EXEC.
        straDiInvstDao.fetchCIdoEffSdi(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX
            a770CloseCursorIdo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_STRA_DI_INVST
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_STRA
        //                ,MOD_GEST
        //                ,VAR_RIFTO
        //                ,VAL_VAR_RIFTO
        //                ,DT_FIS
        //                ,FRQ_VALUT
        //                ,FL_INI_END_PER
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_STRA
        //                ,TP_PROVZA_STRA
        //                ,PC_PROTEZIONE
        //             INTO
        //                :SDI-ID-STRA-DI-INVST
        //               ,:SDI-ID-OGG
        //               ,:SDI-TP-OGG
        //               ,:SDI-ID-MOVI-CRZ
        //               ,:SDI-ID-MOVI-CHIU
        //                :IND-SDI-ID-MOVI-CHIU
        //               ,:SDI-DT-INI-EFF-DB
        //               ,:SDI-DT-END-EFF-DB
        //               ,:SDI-COD-COMP-ANIA
        //               ,:SDI-COD-STRA
        //                :IND-SDI-COD-STRA
        //               ,:SDI-MOD-GEST
        //                :IND-SDI-MOD-GEST
        //               ,:SDI-VAR-RIFTO
        //                :IND-SDI-VAR-RIFTO
        //               ,:SDI-VAL-VAR-RIFTO
        //                :IND-SDI-VAL-VAR-RIFTO
        //               ,:SDI-DT-FIS
        //                :IND-SDI-DT-FIS
        //               ,:SDI-FRQ-VALUT
        //                :IND-SDI-FRQ-VALUT
        //               ,:SDI-FL-INI-END-PER
        //                :IND-SDI-FL-INI-END-PER
        //               ,:SDI-DS-RIGA
        //               ,:SDI-DS-OPER-SQL
        //               ,:SDI-DS-VER
        //               ,:SDI-DS-TS-INI-CPTZ
        //               ,:SDI-DS-TS-END-CPTZ
        //               ,:SDI-DS-UTENTE
        //               ,:SDI-DS-STATO-ELAB
        //               ,:SDI-TP-STRA
        //                :IND-SDI-TP-STRA
        //               ,:SDI-TP-PROVZA-STRA
        //                :IND-SDI-TP-PROVZA-STRA
        //               ,:SDI-PC-PROTEZIONE
        //                :IND-SDI-PC-PROTEZIONE
        //             FROM STRA_DI_INVST
        //             WHERE     ID_STRA_DI_INVST = :SDI-ID-STRA-DI-INVST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        straDiInvstDao.selectRec2(straDiInvst.getSdiIdStraDiInvst(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-CPZ-SDI CURSOR FOR
        //              SELECT
        //                     ID_STRA_DI_INVST
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_STRA
        //                    ,MOD_GEST
        //                    ,VAR_RIFTO
        //                    ,VAL_VAR_RIFTO
        //                    ,DT_FIS
        //                    ,FRQ_VALUT
        //                    ,FL_INI_END_PER
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_STRA
        //                    ,TP_PROVZA_STRA
        //                    ,PC_PROTEZIONE
        //              FROM STRA_DI_INVST
        //              WHERE     ID_OGG = :SDI-ID-OGG
        //           AND TP_OGG = :SDI-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_STRA_DI_INVST ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_STRA_DI_INVST
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_STRA
        //                ,MOD_GEST
        //                ,VAR_RIFTO
        //                ,VAL_VAR_RIFTO
        //                ,DT_FIS
        //                ,FRQ_VALUT
        //                ,FL_INI_END_PER
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_STRA
        //                ,TP_PROVZA_STRA
        //                ,PC_PROTEZIONE
        //             INTO
        //                :SDI-ID-STRA-DI-INVST
        //               ,:SDI-ID-OGG
        //               ,:SDI-TP-OGG
        //               ,:SDI-ID-MOVI-CRZ
        //               ,:SDI-ID-MOVI-CHIU
        //                :IND-SDI-ID-MOVI-CHIU
        //               ,:SDI-DT-INI-EFF-DB
        //               ,:SDI-DT-END-EFF-DB
        //               ,:SDI-COD-COMP-ANIA
        //               ,:SDI-COD-STRA
        //                :IND-SDI-COD-STRA
        //               ,:SDI-MOD-GEST
        //                :IND-SDI-MOD-GEST
        //               ,:SDI-VAR-RIFTO
        //                :IND-SDI-VAR-RIFTO
        //               ,:SDI-VAL-VAR-RIFTO
        //                :IND-SDI-VAL-VAR-RIFTO
        //               ,:SDI-DT-FIS
        //                :IND-SDI-DT-FIS
        //               ,:SDI-FRQ-VALUT
        //                :IND-SDI-FRQ-VALUT
        //               ,:SDI-FL-INI-END-PER
        //                :IND-SDI-FL-INI-END-PER
        //               ,:SDI-DS-RIGA
        //               ,:SDI-DS-OPER-SQL
        //               ,:SDI-DS-VER
        //               ,:SDI-DS-TS-INI-CPTZ
        //               ,:SDI-DS-TS-END-CPTZ
        //               ,:SDI-DS-UTENTE
        //               ,:SDI-DS-STATO-ELAB
        //               ,:SDI-TP-STRA
        //                :IND-SDI-TP-STRA
        //               ,:SDI-TP-PROVZA-STRA
        //                :IND-SDI-TP-PROVZA-STRA
        //               ,:SDI-PC-PROTEZIONE
        //                :IND-SDI-PC-PROTEZIONE
        //             FROM STRA_DI_INVST
        //             WHERE     ID_OGG = :SDI-ID-OGG
        //                    AND TP_OGG = :SDI-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        straDiInvstDao.selectRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-CPZ-SDI
        //           END-EXEC.
        straDiInvstDao.openCIdoCpzSdi(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-CPZ-SDI
        //           END-EXEC.
        straDiInvstDao.closeCIdoCpzSdi();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-CPZ-SDI
        //           INTO
        //                :SDI-ID-STRA-DI-INVST
        //               ,:SDI-ID-OGG
        //               ,:SDI-TP-OGG
        //               ,:SDI-ID-MOVI-CRZ
        //               ,:SDI-ID-MOVI-CHIU
        //                :IND-SDI-ID-MOVI-CHIU
        //               ,:SDI-DT-INI-EFF-DB
        //               ,:SDI-DT-END-EFF-DB
        //               ,:SDI-COD-COMP-ANIA
        //               ,:SDI-COD-STRA
        //                :IND-SDI-COD-STRA
        //               ,:SDI-MOD-GEST
        //                :IND-SDI-MOD-GEST
        //               ,:SDI-VAR-RIFTO
        //                :IND-SDI-VAR-RIFTO
        //               ,:SDI-VAL-VAR-RIFTO
        //                :IND-SDI-VAL-VAR-RIFTO
        //               ,:SDI-DT-FIS
        //                :IND-SDI-DT-FIS
        //               ,:SDI-FRQ-VALUT
        //                :IND-SDI-FRQ-VALUT
        //               ,:SDI-FL-INI-END-PER
        //                :IND-SDI-FL-INI-END-PER
        //               ,:SDI-DS-RIGA
        //               ,:SDI-DS-OPER-SQL
        //               ,:SDI-DS-VER
        //               ,:SDI-DS-TS-INI-CPTZ
        //               ,:SDI-DS-TS-END-CPTZ
        //               ,:SDI-DS-UTENTE
        //               ,:SDI-DS-STATO-ELAB
        //               ,:SDI-TP-STRA
        //                :IND-SDI-TP-STRA
        //               ,:SDI-TP-PROVZA-STRA
        //                :IND-SDI-TP-PROVZA-STRA
        //               ,:SDI-PC-PROTEZIONE
        //                :IND-SDI-PC-PROTEZIONE
        //           END-EXEC.
        straDiInvstDao.fetchCIdoCpzSdi(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX
            b770CloseCursorIdoCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-SDI-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO SDI-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndStraDiInvst().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO SDI-ID-MOVI-CHIU-NULL
            straDiInvst.getSdiIdMoviChiu().setSdiIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, SdiIdMoviChiu.Len.SDI_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-SDI-COD-STRA = -1
        //              MOVE HIGH-VALUES TO SDI-COD-STRA-NULL
        //           END-IF
        if (ws.getIndStraDiInvst().getCodParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO SDI-COD-STRA-NULL
            straDiInvst.setSdiCodStra(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StraDiInvstIdbssdi0.Len.SDI_COD_STRA));
        }
        // COB_CODE: IF IND-SDI-MOD-GEST = -1
        //              MOVE HIGH-VALUES TO SDI-MOD-GEST-NULL
        //           END-IF
        if (ws.getIndStraDiInvst().getTpParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO SDI-MOD-GEST-NULL
            straDiInvst.getSdiModGest().setSdiModGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, SdiModGest.Len.SDI_MOD_GEST_NULL));
        }
        // COB_CODE: IF IND-SDI-VAR-RIFTO = -1
        //              MOVE HIGH-VALUES TO SDI-VAR-RIFTO-NULL
        //           END-IF
        if (ws.getIndStraDiInvst().getTpD() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO SDI-VAR-RIFTO-NULL
            straDiInvst.setSdiVarRifto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StraDiInvstIdbssdi0.Len.SDI_VAR_RIFTO));
        }
        // COB_CODE: IF IND-SDI-VAL-VAR-RIFTO = -1
        //              MOVE HIGH-VALUES TO SDI-VAL-VAR-RIFTO-NULL
        //           END-IF
        if (ws.getIndStraDiInvst().getValImp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO SDI-VAL-VAR-RIFTO-NULL
            straDiInvst.setSdiValVarRifto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StraDiInvstIdbssdi0.Len.SDI_VAL_VAR_RIFTO));
        }
        // COB_CODE: IF IND-SDI-DT-FIS = -1
        //              MOVE HIGH-VALUES TO SDI-DT-FIS-NULL
        //           END-IF
        if (ws.getIndStraDiInvst().getValDt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO SDI-DT-FIS-NULL
            straDiInvst.getSdiDtFis().setSdiDtFisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, SdiDtFis.Len.SDI_DT_FIS_NULL));
        }
        // COB_CODE: IF IND-SDI-FRQ-VALUT = -1
        //              MOVE HIGH-VALUES TO SDI-FRQ-VALUT-NULL
        //           END-IF
        if (ws.getIndStraDiInvst().getValTs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO SDI-FRQ-VALUT-NULL
            straDiInvst.getSdiFrqValut().setSdiFrqValutNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, SdiFrqValut.Len.SDI_FRQ_VALUT_NULL));
        }
        // COB_CODE: IF IND-SDI-FL-INI-END-PER = -1
        //              MOVE HIGH-VALUES TO SDI-FL-INI-END-PER-NULL
        //           END-IF
        if (ws.getIndStraDiInvst().getValTxt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO SDI-FL-INI-END-PER-NULL
            straDiInvst.setSdiFlIniEndPer(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-SDI-TP-STRA = -1
        //              MOVE HIGH-VALUES TO SDI-TP-STRA-NULL
        //           END-IF
        if (ws.getIndStraDiInvst().getValFl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO SDI-TP-STRA-NULL
            straDiInvst.setSdiTpStra(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StraDiInvstIdbssdi0.Len.SDI_TP_STRA));
        }
        // COB_CODE: IF IND-SDI-TP-PROVZA-STRA = -1
        //              MOVE HIGH-VALUES TO SDI-TP-PROVZA-STRA-NULL
        //           END-IF
        if (ws.getIndStraDiInvst().getValNum() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO SDI-TP-PROVZA-STRA-NULL
            straDiInvst.setSdiTpProvzaStra(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StraDiInvstIdbssdi0.Len.SDI_TP_PROVZA_STRA));
        }
        // COB_CODE: IF IND-SDI-PC-PROTEZIONE = -1
        //              MOVE HIGH-VALUES TO SDI-PC-PROTEZIONE-NULL
        //           END-IF.
        if (ws.getIndStraDiInvst().getValPc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO SDI-PC-PROTEZIONE-NULL
            straDiInvst.getSdiPcProtezione().setSdiPcProtezioneNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, SdiPcProtezione.Len.SDI_PC_PROTEZIONE_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO SDI-DS-OPER-SQL
        straDiInvst.setSdiDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO SDI-DS-VER
        straDiInvst.setSdiDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO SDI-DS-UTENTE
        straDiInvst.setSdiDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO SDI-DS-STATO-ELAB.
        straDiInvst.setSdiDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO SDI-DS-OPER-SQL
        straDiInvst.setSdiDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO SDI-DS-UTENTE.
        straDiInvst.setSdiDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF SDI-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-SDI-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-SDI-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(straDiInvst.getSdiIdMoviChiu().getSdiIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-SDI-ID-MOVI-CHIU
            ws.getIndStraDiInvst().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-SDI-ID-MOVI-CHIU
            ws.getIndStraDiInvst().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF SDI-COD-STRA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-SDI-COD-STRA
        //           ELSE
        //              MOVE 0 TO IND-SDI-COD-STRA
        //           END-IF
        if (Characters.EQ_HIGH.test(straDiInvst.getSdiCodStra(), StraDiInvstIdbssdi0.Len.SDI_COD_STRA)) {
            // COB_CODE: MOVE -1 TO IND-SDI-COD-STRA
            ws.getIndStraDiInvst().setCodParam(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-SDI-COD-STRA
            ws.getIndStraDiInvst().setCodParam(((short)0));
        }
        // COB_CODE: IF SDI-MOD-GEST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-SDI-MOD-GEST
        //           ELSE
        //              MOVE 0 TO IND-SDI-MOD-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(straDiInvst.getSdiModGest().getSdiModGestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-SDI-MOD-GEST
            ws.getIndStraDiInvst().setTpParam(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-SDI-MOD-GEST
            ws.getIndStraDiInvst().setTpParam(((short)0));
        }
        // COB_CODE: IF SDI-VAR-RIFTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-SDI-VAR-RIFTO
        //           ELSE
        //              MOVE 0 TO IND-SDI-VAR-RIFTO
        //           END-IF
        if (Characters.EQ_HIGH.test(straDiInvst.getSdiVarRiftoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-SDI-VAR-RIFTO
            ws.getIndStraDiInvst().setTpD(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-SDI-VAR-RIFTO
            ws.getIndStraDiInvst().setTpD(((short)0));
        }
        // COB_CODE: IF SDI-VAL-VAR-RIFTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-SDI-VAL-VAR-RIFTO
        //           ELSE
        //              MOVE 0 TO IND-SDI-VAL-VAR-RIFTO
        //           END-IF
        if (Characters.EQ_HIGH.test(straDiInvst.getSdiValVarRiftoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-SDI-VAL-VAR-RIFTO
            ws.getIndStraDiInvst().setValImp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-SDI-VAL-VAR-RIFTO
            ws.getIndStraDiInvst().setValImp(((short)0));
        }
        // COB_CODE: IF SDI-DT-FIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-SDI-DT-FIS
        //           ELSE
        //              MOVE 0 TO IND-SDI-DT-FIS
        //           END-IF
        if (Characters.EQ_HIGH.test(straDiInvst.getSdiDtFis().getSdiDtFisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-SDI-DT-FIS
            ws.getIndStraDiInvst().setValDt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-SDI-DT-FIS
            ws.getIndStraDiInvst().setValDt(((short)0));
        }
        // COB_CODE: IF SDI-FRQ-VALUT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-SDI-FRQ-VALUT
        //           ELSE
        //              MOVE 0 TO IND-SDI-FRQ-VALUT
        //           END-IF
        if (Characters.EQ_HIGH.test(straDiInvst.getSdiFrqValut().getSdiFrqValutNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-SDI-FRQ-VALUT
            ws.getIndStraDiInvst().setValTs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-SDI-FRQ-VALUT
            ws.getIndStraDiInvst().setValTs(((short)0));
        }
        // COB_CODE: IF SDI-FL-INI-END-PER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-SDI-FL-INI-END-PER
        //           ELSE
        //              MOVE 0 TO IND-SDI-FL-INI-END-PER
        //           END-IF
        if (Conditions.eq(straDiInvst.getSdiFlIniEndPer(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-SDI-FL-INI-END-PER
            ws.getIndStraDiInvst().setValTxt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-SDI-FL-INI-END-PER
            ws.getIndStraDiInvst().setValTxt(((short)0));
        }
        // COB_CODE: IF SDI-TP-STRA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-SDI-TP-STRA
        //           ELSE
        //              MOVE 0 TO IND-SDI-TP-STRA
        //           END-IF
        if (Characters.EQ_HIGH.test(straDiInvst.getSdiTpStraFormatted())) {
            // COB_CODE: MOVE -1 TO IND-SDI-TP-STRA
            ws.getIndStraDiInvst().setValFl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-SDI-TP-STRA
            ws.getIndStraDiInvst().setValFl(((short)0));
        }
        // COB_CODE: IF SDI-TP-PROVZA-STRA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-SDI-TP-PROVZA-STRA
        //           ELSE
        //              MOVE 0 TO IND-SDI-TP-PROVZA-STRA
        //           END-IF
        if (Characters.EQ_HIGH.test(straDiInvst.getSdiTpProvzaStraFormatted())) {
            // COB_CODE: MOVE -1 TO IND-SDI-TP-PROVZA-STRA
            ws.getIndStraDiInvst().setValNum(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-SDI-TP-PROVZA-STRA
            ws.getIndStraDiInvst().setValNum(((short)0));
        }
        // COB_CODE: IF SDI-PC-PROTEZIONE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-SDI-PC-PROTEZIONE
        //           ELSE
        //              MOVE 0 TO IND-SDI-PC-PROTEZIONE
        //           END-IF.
        if (Characters.EQ_HIGH.test(straDiInvst.getSdiPcProtezione().getSdiPcProtezioneNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-SDI-PC-PROTEZIONE
            ws.getIndStraDiInvst().setValPc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-SDI-PC-PROTEZIONE
            ws.getIndStraDiInvst().setValPc(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : SDI-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE STRA-DI-INVST TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(straDiInvst.getStraDiInvstFormatted());
        // COB_CODE: MOVE SDI-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(straDiInvst.getSdiIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO SDI-ID-MOVI-CHIU
                straDiInvst.getSdiIdMoviChiu().setSdiIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO SDI-DS-TS-END-CPTZ
                straDiInvst.setSdiDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO SDI-ID-MOVI-CRZ
                    straDiInvst.setSdiIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO SDI-ID-MOVI-CHIU-NULL
                    straDiInvst.getSdiIdMoviChiu().setSdiIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, SdiIdMoviChiu.Len.SDI_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO SDI-DT-END-EFF
                    straDiInvst.setSdiDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO SDI-DS-TS-INI-CPTZ
                    straDiInvst.setSdiDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO SDI-DS-TS-END-CPTZ
                    straDiInvst.setSdiDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE STRA-DI-INVST TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(straDiInvst.getStraDiInvstFormatted());
        // COB_CODE: MOVE SDI-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(straDiInvst.getSdiIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO STRA-DI-INVST.
        straDiInvst.setStraDiInvstFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO SDI-ID-MOVI-CRZ.
        straDiInvst.setSdiIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO SDI-ID-MOVI-CHIU-NULL.
        straDiInvst.getSdiIdMoviChiu().setSdiIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, SdiIdMoviChiu.Len.SDI_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO SDI-DT-INI-EFF.
        straDiInvst.setSdiDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO SDI-DT-END-EFF.
        straDiInvst.setSdiDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO SDI-DS-TS-INI-CPTZ.
        straDiInvst.setSdiDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO SDI-DS-TS-END-CPTZ.
        straDiInvst.setSdiDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO SDI-COD-COMP-ANIA.
        straDiInvst.setSdiCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE SDI-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(straDiInvst.getSdiDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO SDI-DT-INI-EFF-DB
        ws.getIdbvsdi3().setSdiDtIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE SDI-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(straDiInvst.getSdiDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO SDI-DT-END-EFF-DB.
        ws.getIdbvsdi3().setSdiDtEndEffDb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE SDI-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvsdi3().getSdiDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO SDI-DT-INI-EFF
        straDiInvst.setSdiDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE SDI-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvsdi3().getSdiDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO SDI-DT-END-EFF.
        straDiInvst.setSdiDtEndEff(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return straDiInvst.getSdiCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.straDiInvst.setSdiCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodStra() {
        return straDiInvst.getSdiCodStra();
    }

    @Override
    public void setCodStra(String codStra) {
        this.straDiInvst.setSdiCodStra(codStra);
    }

    @Override
    public String getCodStraObj() {
        if (ws.getIndStraDiInvst().getCodParam() >= 0) {
            return getCodStra();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodStraObj(String codStraObj) {
        if (codStraObj != null) {
            setCodStra(codStraObj);
            ws.getIndStraDiInvst().setCodParam(((short)0));
        }
        else {
            ws.getIndStraDiInvst().setCodParam(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return straDiInvst.getSdiDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.straDiInvst.setSdiDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return straDiInvst.getSdiDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.straDiInvst.setSdiDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return straDiInvst.getSdiDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.straDiInvst.setSdiDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return straDiInvst.getSdiDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.straDiInvst.setSdiDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return straDiInvst.getSdiDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.straDiInvst.setSdiDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return straDiInvst.getSdiDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.straDiInvst.setSdiDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getIdbvsdi3().getSdiDtEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getIdbvsdi3().setSdiDtEndEffDb(dtEndEffDb);
    }

    @Override
    public short getDtFis() {
        return straDiInvst.getSdiDtFis().getSdiDtFis();
    }

    @Override
    public void setDtFis(short dtFis) {
        this.straDiInvst.getSdiDtFis().setSdiDtFis(dtFis);
    }

    @Override
    public Short getDtFisObj() {
        if (ws.getIndStraDiInvst().getValDt() >= 0) {
            return ((Short)getDtFis());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtFisObj(Short dtFisObj) {
        if (dtFisObj != null) {
            setDtFis(((short)dtFisObj));
            ws.getIndStraDiInvst().setValDt(((short)0));
        }
        else {
            ws.getIndStraDiInvst().setValDt(((short)-1));
        }
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getIdbvsdi3().getSdiDtIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getIdbvsdi3().setSdiDtIniEffDb(dtIniEffDb);
    }

    @Override
    public char getFlIniEndPer() {
        return straDiInvst.getSdiFlIniEndPer();
    }

    @Override
    public void setFlIniEndPer(char flIniEndPer) {
        this.straDiInvst.setSdiFlIniEndPer(flIniEndPer);
    }

    @Override
    public Character getFlIniEndPerObj() {
        if (ws.getIndStraDiInvst().getValTxt() >= 0) {
            return ((Character)getFlIniEndPer());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlIniEndPerObj(Character flIniEndPerObj) {
        if (flIniEndPerObj != null) {
            setFlIniEndPer(((char)flIniEndPerObj));
            ws.getIndStraDiInvst().setValTxt(((short)0));
        }
        else {
            ws.getIndStraDiInvst().setValTxt(((short)-1));
        }
    }

    @Override
    public int getFrqValut() {
        return straDiInvst.getSdiFrqValut().getSdiFrqValut();
    }

    @Override
    public void setFrqValut(int frqValut) {
        this.straDiInvst.getSdiFrqValut().setSdiFrqValut(frqValut);
    }

    @Override
    public Integer getFrqValutObj() {
        if (ws.getIndStraDiInvst().getValTs() >= 0) {
            return ((Integer)getFrqValut());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrqValutObj(Integer frqValutObj) {
        if (frqValutObj != null) {
            setFrqValut(((int)frqValutObj));
            ws.getIndStraDiInvst().setValTs(((short)0));
        }
        else {
            ws.getIndStraDiInvst().setValTs(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return straDiInvst.getSdiIdMoviChiu().getSdiIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.straDiInvst.getSdiIdMoviChiu().setSdiIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndStraDiInvst().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndStraDiInvst().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndStraDiInvst().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return straDiInvst.getSdiIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.straDiInvst.setSdiIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdStraDiInvst() {
        return straDiInvst.getSdiIdStraDiInvst();
    }

    @Override
    public void setIdStraDiInvst(int idStraDiInvst) {
        this.straDiInvst.setSdiIdStraDiInvst(idStraDiInvst);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public short getModGest() {
        return straDiInvst.getSdiModGest().getSdiModGest();
    }

    @Override
    public void setModGest(short modGest) {
        this.straDiInvst.getSdiModGest().setSdiModGest(modGest);
    }

    @Override
    public Short getModGestObj() {
        if (ws.getIndStraDiInvst().getTpParam() >= 0) {
            return ((Short)getModGest());
        }
        else {
            return null;
        }
    }

    @Override
    public void setModGestObj(Short modGestObj) {
        if (modGestObj != null) {
            setModGest(((short)modGestObj));
            ws.getIndStraDiInvst().setTpParam(((short)0));
        }
        else {
            ws.getIndStraDiInvst().setTpParam(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcProtezione() {
        return straDiInvst.getSdiPcProtezione().getSdiPcProtezione();
    }

    @Override
    public void setPcProtezione(AfDecimal pcProtezione) {
        this.straDiInvst.getSdiPcProtezione().setSdiPcProtezione(pcProtezione.copy());
    }

    @Override
    public AfDecimal getPcProtezioneObj() {
        if (ws.getIndStraDiInvst().getValPc() >= 0) {
            return getPcProtezione();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcProtezioneObj(AfDecimal pcProtezioneObj) {
        if (pcProtezioneObj != null) {
            setPcProtezione(new AfDecimal(pcProtezioneObj, 6, 3));
            ws.getIndStraDiInvst().setValPc(((short)0));
        }
        else {
            ws.getIndStraDiInvst().setValPc(((short)-1));
        }
    }

    @Override
    public long getSdiDsRiga() {
        return straDiInvst.getSdiDsRiga();
    }

    @Override
    public void setSdiDsRiga(long sdiDsRiga) {
        this.straDiInvst.setSdiDsRiga(sdiDsRiga);
    }

    @Override
    public int getSdiIdOgg() {
        return straDiInvst.getSdiIdOgg();
    }

    @Override
    public void setSdiIdOgg(int sdiIdOgg) {
        this.straDiInvst.setSdiIdOgg(sdiIdOgg);
    }

    @Override
    public String getSdiTpOgg() {
        return straDiInvst.getSdiTpOgg();
    }

    @Override
    public void setSdiTpOgg(String sdiTpOgg) {
        this.straDiInvst.setSdiTpOgg(sdiTpOgg);
    }

    @Override
    public String getTpProvzaStra() {
        return straDiInvst.getSdiTpProvzaStra();
    }

    @Override
    public void setTpProvzaStra(String tpProvzaStra) {
        this.straDiInvst.setSdiTpProvzaStra(tpProvzaStra);
    }

    @Override
    public String getTpProvzaStraObj() {
        if (ws.getIndStraDiInvst().getValNum() >= 0) {
            return getTpProvzaStra();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpProvzaStraObj(String tpProvzaStraObj) {
        if (tpProvzaStraObj != null) {
            setTpProvzaStra(tpProvzaStraObj);
            ws.getIndStraDiInvst().setValNum(((short)0));
        }
        else {
            ws.getIndStraDiInvst().setValNum(((short)-1));
        }
    }

    @Override
    public String getTpStra() {
        return straDiInvst.getSdiTpStra();
    }

    @Override
    public void setTpStra(String tpStra) {
        this.straDiInvst.setSdiTpStra(tpStra);
    }

    @Override
    public String getTpStraObj() {
        if (ws.getIndStraDiInvst().getValFl() >= 0) {
            return getTpStra();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStraObj(String tpStraObj) {
        if (tpStraObj != null) {
            setTpStra(tpStraObj);
            ws.getIndStraDiInvst().setValFl(((short)0));
        }
        else {
            ws.getIndStraDiInvst().setValFl(((short)-1));
        }
    }

    @Override
    public String getValVarRifto() {
        return straDiInvst.getSdiValVarRifto();
    }

    @Override
    public void setValVarRifto(String valVarRifto) {
        this.straDiInvst.setSdiValVarRifto(valVarRifto);
    }

    @Override
    public String getValVarRiftoObj() {
        if (ws.getIndStraDiInvst().getValImp() >= 0) {
            return getValVarRifto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValVarRiftoObj(String valVarRiftoObj) {
        if (valVarRiftoObj != null) {
            setValVarRifto(valVarRiftoObj);
            ws.getIndStraDiInvst().setValImp(((short)0));
        }
        else {
            ws.getIndStraDiInvst().setValImp(((short)-1));
        }
    }

    @Override
    public String getVarRifto() {
        return straDiInvst.getSdiVarRifto();
    }

    @Override
    public void setVarRifto(String varRifto) {
        this.straDiInvst.setSdiVarRifto(varRifto);
    }

    @Override
    public String getVarRiftoObj() {
        if (ws.getIndStraDiInvst().getTpD() >= 0) {
            return getVarRifto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setVarRiftoObj(String varRiftoObj) {
        if (varRiftoObj != null) {
            setVarRifto(varRiftoObj);
            ws.getIndStraDiInvst().setTpD(((short)0));
        }
        else {
            ws.getIndStraDiInvst().setTpD(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
