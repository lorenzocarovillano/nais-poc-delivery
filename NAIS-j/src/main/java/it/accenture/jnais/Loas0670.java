package it.accenture.jnais;

import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Loas0670Data;
import it.accenture.jnais.ws.W670AreaLoas0670;
import it.accenture.jnais.ws.WadeAreaAdesioneLoas0800;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.WpmoAreaParamMoviLoas0800;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WtgaAreaTrancheLoas0800;

/**Original name: LOAS0670<br>
 * <pre>  ============================================================= *
 *                                                                 *
 *         PORTAFOGLIO VITA ITALIA  VER 1.0                        *
 *                                                                 *
 *   ============================================================= *
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       SETTEMBRE 2007.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *      PROGRAMMA ..... LOAS0670                                   *
 *      TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
 *      PROCESSO....... ADEGUAMENTO PREMIO PRESTAZIONE             *
 *      FUNZIONE....... BATCH                                      *
 *      DESCRIZIONE.... GAP RICH090 COLLETTIVE                     *
 *      PAGINA WEB..... N.A.                                       *
 * ----------------------------------------------------------------*</pre>*/
public class Loas0670 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Loas0670Data ws = new Loas0670Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: WPMO-AREA-PARAM-MOVI
    private WpmoAreaParamMoviLoas0800 wpmoAreaParamMovi;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLoas0800 wadeAreaAdesione;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTrancheLoas0800 wtgaAreaTranche;
    //Original name: W670-AREA-LOAS0670
    private W670AreaLoas0670 w670AreaLoas0670;

    //==== METHODS ====
    /**Original name: PROGRAM_LOAS0670_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, WpmoAreaParamMoviLoas0800 wpmoAreaParamMovi, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WadeAreaAdesioneLoas0800 wadeAreaAdesione, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WtgaAreaTrancheLoas0800 wtgaAreaTranche, W670AreaLoas0670 w670AreaLoas0670) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.wpmoAreaParamMovi = wpmoAreaParamMovi;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaTranche = wtgaAreaTranche;
        this.w670AreaLoas0670 = w670AreaLoas0670;
        // COB_CODE: PERFORM S00000-OPERAZ-INIZIALI
        //              THRU S00000-OPERAZ-INIZIALI-EX.
        s00000OperazIniziali();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10000-ELABORAZIONE-EX
        //           *
        //                END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10000-ELABORAZIONE
            //              THRU S10000-ELABORAZIONE-EX
            s10000Elaborazione();
            //
        }
        //
        // COB_CODE: PERFORM S90000-OPERAZ-FINALI
        //              THRU S90000-OPERAZ-FINALI-EX.
        s90000OperazFinali();
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Loas0670 getInstance() {
        return ((Loas0670)Programs.getInstance(Loas0670.class));
    }

    /**Original name: S00000-OPERAZ-INIZIALI<br>
	 * <pre> ============================================================== *
	 *  -->           O P E R Z I O N I   I N I Z I A L I          <-- *
	 *  ============================================================== *</pre>*/
    private void s00000OperazIniziali() {
        // COB_CODE: MOVE 'S00000-OPERAZ-INIZIALI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00000-OPERAZ-INIZIALI");
        //
        // --> Inizializazione delle aree di working storage
        //
        // COB_CODE: PERFORM S00200-INIZIA-AREE-WS
        //              THRU S00200-INIZIA-AREE-WS-EX.
        s00200IniziaAreeWs();
    }

    /**Original name: S00200-INIZIA-AREE-WS<br>
	 * <pre>----------------------------------------------------------------*
	 *      INIZIALIZZAZIONE DELLE AREE DI WORKING STORAGE             *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00200IniziaAreeWs() {
        // COB_CODE: MOVE 'S00200-INIZIA-AREE-WS'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00200-INIZIA-AREE-WS");
        //
        //  Inizializazione di tutti gli indici e le aree di WS
        //
        // COB_CODE: INITIALIZE IX-INDICI.
        initIxIndici();
        //
        // COB_CODE: SET  IDSV0001-ESITO-OK
        //             TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
    }

    /**Original name: S10000-ELABORAZIONE<br>
	 * <pre> ============================================================== *
	 *  -->               E L A B O R A Z I O N E                  <-- *
	 *  ============================================================== *</pre>*/
    private void s10000Elaborazione() {
        // COB_CODE: MOVE 'S10000-ELABORAZIONE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10000-ELABORAZIONE");
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF W670-NUM-MAX-ELE = ZEROES
            //           *
            //                         THRU S10400-CARICA-TIT-DB-EX
            //           *
            //                   END-IF
            if (w670AreaLoas0670.getNumMaxEle() == 0) {
                //
                // COB_CODE: PERFORM S10400-CARICA-TIT-DB
                //              THRU S10400-CARICA-TIT-DB-EX
                s10400CaricaTitDb();
                //
            }
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10500-CTRL-TIT-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10500-CTRL-TIT
            //              THRU S10500-CTRL-TIT-EX
            s10500CtrlTit();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10600-DETERMINA-TP-RIVAL-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10600-DETERMINA-TP-RIVAL
            //              THRU S10600-DETERMINA-TP-RIVAL-EX
            s10600DeterminaTpRival();
            //
        }
    }

    /**Original name: S10600-DETERMINA-TP-RIVAL<br>
	 * <pre>----------------------------------------------------------------*
	 *               GESTIONE DEL TIPO DI RIVALUTAZIONE                *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10600DeterminaTpRival() {
        // COB_CODE: MOVE 'S10600-DETERMINA-TP-RIVAL'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10600-DETERMINA-TP-RIVAL");
        // --> Tipi Rivalutazione :
        //
        //     -> Piena
        //     -> Parziale
        //     -> Nulla
        //     -> Da Rieseguire
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                    END-PERFORM
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTabPvt(((short)1));
        while (!(ws.getIxIndici().getTabPvt() > wtgaAreaTranche.getEleTranMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE:          PERFORM VARYING IX-W670 FROM 1 BY 1
            //                       UNTIL IX-W670 > W670-NUM-MAX-ELE
            //           *
            //                         END-IF
            //           *
            //                    END-PERFORM
            ws.getIxIndici().setTabPag(((short)1));
            while (!(ws.getIxIndici().getTabPag() > w670AreaLoas0670.getNumMaxEle())) {
                //
                // COB_CODE:               IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                //                            W670-ID-TRCH-DI-GAR(IX-W670)
                //           *
                //                               THRU S10290-GESTIONE-TIT-CONT-EX
                //           *
                //                         END-IF
                if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabPvt()).getLccvtga1().getDati().getWtgaIdTrchDiGar() == w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).getW670IdTrchDiGar()) {
                    //
                    // COB_CODE: PERFORM S10290-GESTIONE-TIT-CONT
                    //              THRU S10290-GESTIONE-TIT-CONT-EX
                    s10290GestioneTitCont();
                    //
                }
                //
                ws.getIxIndici().setTabPag(Trunc.toShort(ws.getIxIndici().getTabPag() + 1, 4));
            }
            //
            ws.getIxIndici().setTabPvt(Trunc.toShort(ws.getIxIndici().getTabPvt() + 1, 4));
        }
    }

    /**Original name: S10220-RICERCA-TITOLO<br>
	 * <pre>----------------------------------------------------------------*
	 *                      RICERCA TITOLI CONTABILI                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10220RicercaTitolo() {
        // COB_CODE: MOVE 'S10220-RICERCA-TITOLO'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10220-RICERCA-TITOLO");
        //
        // -->> Indicatore di fine fetch
        //
        // COB_CODE: SET WK-FINE-FETCH-NO
        //             TO TRUE.
        ws.getWkFineFetch().setNo();
        //
        // -->> Flag Ricerca Titolo Contabile Incassato
        //
        // COB_CODE: SET WK-TIT-NO
        //             TO TRUE.
        ws.getWkTitTrovato().setNo();
        //
        // COB_CODE: MOVE 'IN'
        //             TO LDBV3101-TP-STA-TIT.
        ws.getLdbv3101().setTpStaTit("IN");
        //
        // COB_CODE: SET IDSI0011-FETCH-FIRST
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        //
        // COB_CODE: PERFORM S10230-IMPOSTA-TIT-CONT
        //              THRU S10230-IMPOSTA-TIT-CONT-EX
        s10230ImpostaTitCont();
        //
        // COB_CODE: PERFORM S10240-LEGGI-TIT-CONT
        //              THRU S10240-LEGGI-TIT-CONT-EX
        //             UNTIL WK-FINE-FETCH-SI
        //                OR IDSV0001-ESITO-KO
        while (!(ws.getWkFineFetch().isSi() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            s10240LeggiTitCont();
        }
        //
        // --> Non esistono Titoli Contabili con stato INCASSATO
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF WK-TIT-NO
            //           *
            //                   END-IF
            //           *
            //                END-IF.
            if (ws.getWkTitTrovato().isNo()) {
                //
                // COB_CODE: SET WK-FINE-FETCH-NO
                //             TO TRUE
                ws.getWkFineFetch().setNo();
                //
                // COB_CODE: MOVE 'EM'
                //             TO LDBV3101-TP-STA-TIT
                ws.getLdbv3101().setTpStaTit("EM");
                //
                // COB_CODE: SET IDSI0011-FETCH-FIRST
                //             TO TRUE
                ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
                //
                // COB_CODE: PERFORM S10230-IMPOSTA-TIT-CONT
                //              THRU S10230-IMPOSTA-TIT-CONT-EX
                s10230ImpostaTitCont();
                //
                // COB_CODE: PERFORM S10240-LEGGI-TIT-CONT
                //              THRU S10240-LEGGI-TIT-CONT-EX
                //             UNTIL WK-FINE-FETCH-SI
                //                OR IDSV0001-ESITO-KO
                while (!(ws.getWkFineFetch().isSi() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                    s10240LeggiTitCont();
                }
                //
                // --> Non esistono Titoli Contabili con stato EMESSO
                //
                // COB_CODE:            IF IDSV0001-ESITO-OK
                //           *
                //                         END-IF
                //           *
                //                   END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    //
                    // COB_CODE:               IF WK-TIT-NO
                    //           *
                    //           *     SIR UNIPOL: Per le garanzie a premio unico,
                    //           *     se non esistono titoli contabili allora comunque
                    //           *     la rivalutazione h piena (perchi significa che non vi
                    //           *     sono stati versamenti e valgono i titoli di emissione)
                    //                            END-IF
                    //           *
                    //                         END-IF
                    if (ws.getWkTitTrovato().isNo()) {
                        //
                        //     SIR UNIPOL: Per le garanzie a premio unico,
                        //     se non esistono titoli contabili allora comunque
                        //     la rivalutazione h piena (perchi significa che non vi
                        //     sono stati versamenti e valgono i titoli di emissione)
                        // COB_CODE: PERFORM RICERCA-GAR
                        //              THRU RICERCA-GAR-EX
                        ricercaGar();
                        // COB_CODE: IF  WK-GRZ-SI
                        //           AND (WS-PREMIO-UNICO OR
                        //                WS-PREMIO-RICORRENTE)
                        //               CONTINUE
                        //           ELSE
                        //                 THRU GESTIONE-ERR-STD-EX
                        //           END-IF
                        if (ws.getWkGrzTrovata().isSi() && (ws.getWsTpPerPremio().isUnico() || ws.getWsTpPerPremio().isRicorrente())) {
                        // COB_CODE: CONTINUE
                        //continue
                        }
                        else {
                            // COB_CODE: MOVE 'NON ESISTONO TITOLI CONTABILI'
                            //             TO WK-STRING
                            ws.getWkGestioneMsgErr().setStringFld("NON ESISTONO TITOLI CONTABILI");
                            // COB_CODE: MOVE '001114'
                            //             TO WK-COD-ERR
                            ws.getWkGestioneMsgErr().setCodErr("001114");
                            // COB_CODE: PERFORM GESTIONE-ERR-STD
                            //              THRU GESTIONE-ERR-STD-EX
                            gestioneErrStd();
                        }
                        //
                    }
                    //
                }
                //
            }
        }
    }

    /**Original name: RICERCA-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void ricercaGar() {
        // COB_CODE: SET WK-GRZ-NO       TO TRUE
        ws.getWkGrzTrovata().setNo();
        // COB_CODE: PERFORM VARYING IX-GRZ FROM 1 BY 1
        //                     UNTIL IX-GRZ > WGRZ-ELE-GAR-MAX
        //                        OR WK-GRZ-SI
        //              END-IF
        //           END-PERFORM.
        ws.getIxIndici().setGarTari(((short)1));
        while (!(ws.getIxIndici().getGarTari() > wgrzAreaGaranzia.getEleGarMax() || ws.getWkGrzTrovata().isSi())) {
            // COB_CODE: IF WGRZ-ID-GAR(IX-GRZ) = WTGA-ID-GAR(IX-TAB-TGA)
            //                TO WS-TP-PER-PREMIO
            //           END-IF
            if (wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getGarTari()).getLccvgrz1().getDati().getWgrzIdGar() == wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabPvt()).getLccvtga1().getDati().getWtgaIdGar()) {
                // COB_CODE: SET WK-GRZ-SI               TO TRUE
                ws.getWkGrzTrovata().setSi();
                // COB_CODE: MOVE WGRZ-TP-PER-PRE(IX-GRZ)
                //             TO WS-TP-PER-PREMIO
                ws.getWsTpPerPremio().setWsTpPerPremioFormatted(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getGarTari()).getLccvgrz1().getDati().getWgrzTpPerPreFormatted());
            }
            ws.getIxIndici().setGarTari(Trunc.toShort(ws.getIxIndici().getGarTari() + 1, 4));
        }
    }

    /**Original name: S10230-IMPOSTA-TIT-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *      VALORIZZAZIONI PER LA LETTURA CON WHERE CONDITION AD HOC   *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10230ImpostaTitCont() {
        // COB_CODE: MOVE 'S10230-IMPOSTA-TIT-CONT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10230-IMPOSTA-TIT-CONT");
        //
        // --> Vengono Cercati I Titoli Con Effetto Compreso Tra L'Ultima
        // --> Ricorrenza Elaborata e L'Effetto di Elaborazione
        //
        // COB_CODE:      IF WPMO-DT-RICOR-PREC-NULL(1) = HIGH-VALUES
        //           *
        //                     TO LDBV3101-DT-DA
        //           *
        //                ELSE
        //           *
        //                     TO LDBV3101-DT-DA
        //           *
        //                END-IF.
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrecNullFormatted())) {
            //
            // COB_CODE: MOVE WTGA-DT-DECOR(IX-TAB-TGA)
            //             TO LDBV3101-DT-DA
            ws.getLdbv3101().setDtDa(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabPvt()).getLccvtga1().getDati().getWtgaDtDecor());
            //
        }
        else {
            //
            // COB_CODE: MOVE WPMO-DT-RICOR-PREC(1)
            //             TO LDBV3101-DT-DA
            ws.getLdbv3101().setDtDa(wpmoAreaParamMovi.getTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrec());
            //
        }
        //
        // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(1)
        //             TO LDBV3101-DT-A.
        ws.getLdbv3101().setDtA(wpmoAreaParamMovi.getTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc());
        //
        // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
        //             TO LDBV3101-ID-OGG.
        ws.getLdbv3101().setIdOgg(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabPvt()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        // COB_CODE: MOVE 'TG'
        //             TO LDBV3101-TP-OGG.
        ws.getLdbv3101().setTpOgg("TG");
        // COB_CODE: MOVE 'PR'
        //             TO LDBV3101-TP-TIT.
        ws.getLdbv3101().setTpTit("PR");
        //
        //  --> La data effetto viene sempre valorizzata
        //
        //    MOVE ZERO
        //      TO IDSI0011-DATA-INIZIO-EFFETTO
        //         IDSI0011-DATA-FINE-EFFETTO
        //         IDSI0011-DATA-COMPETENZA.
        //
        //    SIR CQPrd00020801: LA DATA INCASSO NON VIENE VALORIZZATA
        //    NEL FILE DI OUTPUT
        // COB_CODE: MOVE WS-DT-INFINITO-1-N
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getIdsv0015().getDtInfinito1N());
        // COB_CODE: MOVE WS-TS-INFINITO-1-N
        //             TO IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(ws.getIdsv0015().getTsInfinito1N());
        // COB_CODE: MOVE ZEROES
        //             TO IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'LDBS3100'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS3100");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE SPACES
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE LDBV3101
        //             TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv3101().getLdbv3101Formatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-WHERE-CONDITION
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        //  --> Inizializzazione Return Code
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S10240-LEGGI-TIT-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *             LETTURA DELLE TABELLA TRANCHE DI GARANZIA           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10240LeggiTitCont() {
        // COB_CODE: MOVE 'S10240-LEGGI-TIT-CONT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10240-LEGGI-TIT-CONT");
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata, fine occorrenze fetch
            //           *
            //                              TO TRUE
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                            END-IF
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata, fine occorrenze fetch
                    //
                    // COB_CODE: SET WK-FINE-FETCH-SI
                    //             TO TRUE
                    ws.getWkFineFetch().setSi();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: SET WK-TIT-SI
                    //             TO TRUE
                    ws.getWkTitTrovato().setSi();
                    //
                    // COB_CODE: SET IDSI0011-FETCH-NEXT
                    //             TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                    //
                    // COB_CODE: MOVE W670-NUM-MAX-ELE
                    //             TO IX-W670
                    ws.getIxIndici().setTabPag(w670AreaLoas0670.getNumMaxEle());
                    //
                    // COB_CODE: ADD 1
                    //             TO IX-W670
                    ws.getIxIndici().setTabPag(Trunc.toShort(1 + ws.getIxIndici().getTabPag(), 4));
                    //
                    // COB_CODE: MOVE IX-W670
                    //             TO W670-NUM-MAX-ELE
                    w670AreaLoas0670.setNumMaxEle(ws.getIxIndici().getTabPag());
                    //
                    // COB_CODE:                  IF IX-W670 > WK-TGA-MAX-C
                    //           *
                    //                                  THRU GESTIONE-ERR-STD-EX
                    //           *
                    //                            ELSE
                    //           *
                    //                                  THRU S10250-CARICA-TAB-W670-EX
                    //           *
                    //                            END-IF
                    if (ws.getIxIndici().getTabPag() > ws.getWkTgaMax().getC()) {
                        //
                        // COB_CODE: MOVE 'OVERFLOW CARICAMENTO TITOLI CONTABILI'
                        //             TO WK-STRING
                        ws.getWkGestioneMsgErr().setStringFld("OVERFLOW CARICAMENTO TITOLI CONTABILI");
                        // COB_CODE: MOVE '005059'
                        //             TO WK-COD-ERR
                        ws.getWkGestioneMsgErr().setCodErr("005059");
                        // COB_CODE: PERFORM GESTIONE-ERR-STD
                        //              THRU GESTIONE-ERR-STD-EX
                        gestioneErrStd();
                        //
                    }
                    else {
                        //
                        // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                        //             TO TIT-CONT
                        ws.getTitCont().setTitContFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        //
                        // COB_CODE: PERFORM S10250-CARICA-TAB-W670
                        //              THRU S10250-CARICA-TAB-W670-EX
                        s10250CaricaTabW670();
                        //
                    }
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    // COB_CODE: MOVE 'ERRORE LETTURA TITOLO CONTABILE'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE LETTURA TITOLO CONTABILE");
                    // COB_CODE: MOVE '005016'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005016");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA TITOLO CONTABILE'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE DISPATCHER LETTURA TITOLO CONTABILE");
            // COB_CODE: MOVE '005016'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("005016");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S10250-CARICA-TAB-W670<br>
	 * <pre>----------------------------------------------------------------*
	 *   CARICAMENTO TABELLA WS
	 * ----------------------------------------------------------------*</pre>*/
    private void s10250CaricaTabW670() {
        // COB_CODE: MOVE 'S10250-CARICA-TAB-W670'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10250-CARICA-TAB-W670");
        //
        // COB_CODE: MOVE TIT-ID-TIT-CONT
        //             TO W670-ID-TIT-CONT(IX-W670).
        w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).setW670IdTitCont(ws.getTitCont().getTitIdTitCont());
        //
        // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
        //             TO W670-ID-TRCH-DI-GAR(IX-W670).
        w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).setW670IdTrchDiGar(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabPvt()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        //
        // COB_CODE:      IF TIT-DT-VLT-NULL = HIGH-VALUE OR LOW-VALUE
        //           *
        //                     TO W670-DT-VLT-NULL(IX-W670)
        //           *
        //                ELSE
        //           *
        //                     TO W670-DT-VLT(IX-W670)
        //           *
        //                END-IF.
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtVlt().getTitDtVltNullFormatted()) || Characters.EQ_LOW.test(ws.getTitCont().getTitDtVlt().getTitDtVltNullFormatted())) {
            //
            // COB_CODE: MOVE TIT-DT-VLT-NULL
            //             TO W670-DT-VLT-NULL(IX-W670)
            w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).getW670DtVlt().setW670DtVltNull(ws.getTitCont().getTitDtVlt().getTitDtVltNull());
            //
        }
        else {
            //
            // COB_CODE: MOVE TIT-DT-VLT
            //             TO W670-DT-VLT(IX-W670)
            w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).getW670DtVlt().setW670DtVlt(ws.getTitCont().getTitDtVlt().getTitDtVlt());
            //
        }
        //
        // COB_CODE: MOVE TIT-DT-INI-COP
        //             TO W670-DT-INI-EFF(IX-W670).
        w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).setW670DtIniEff(ws.getTitCont().getTitDtIniCop().getTitDtIniCop());
        //
        // COB_CODE: MOVE TIT-TP-STAT-TIT
        //             TO W670-TP-STAT-TIT(IX-W670).
        w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).setW670TpStatTit(ws.getTitCont().getTitTpStatTit());
    }

    /**Original name: S10290-GESTIONE-TIT-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *           DETERMINA IL TIPO DI RIVALUTAZIONE DA ESEGUIRE
	 *   PER I TITOLI INCASSATI : PIENA , PARZIALE , NULLL
	 * ----------------------------------------------------------------*</pre>*/
    private void s10290GestioneTitCont() {
        // COB_CODE: MOVE 'S10290-GESTIONE-TIT-CONT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10290-GESTIONE-TIT-CONT");
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF W670-DT-VLT-NULL(IX-W670) = HIGH-VALUES  OR
            //                      W670-DT-VLT(IX-W670)      = ZEROES
            //           *
            //                          THRU GESTIONE-ERR-STD-EX
            //           *
            //                   ELSE
            //           *
            //                         THRU S10300-RIV-PIENA-PARZ-NULLA-EX
            //           *
            //                   END-IF
            if (Characters.EQ_HIGH.test(w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).getW670DtVlt().getW670DtVltNullFormatted()) || w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).getW670DtVlt().getW670DtVlt() == 0) {
                //
                // COB_CODE: MOVE 'TIT.CONT. NON INCASSATO, RIESEGUIRE RIVAL'
                //             TO WK-STRING
                ws.getWkGestioneMsgErr().setStringFld("TIT.CONT. NON INCASSATO, RIESEGUIRE RIVAL");
                // COB_CODE: MOVE '001114'
                //             TO WK-COD-ERR
                ws.getWkGestioneMsgErr().setCodErr("001114");
                // COB_CODE: PERFORM GESTIONE-ERR-STD
                //              THRU GESTIONE-ERR-STD-EX
                gestioneErrStd();
                //
            }
            else {
                //
                // COB_CODE: PERFORM S10300-RIV-PIENA-PARZ-NULLA
                //              THRU S10300-RIV-PIENA-PARZ-NULLA-EX
                s10300RivPienaParzNulla();
                //
            }
            //
        }
    }

    /**Original name: S10300-RIV-PIENA-PARZ-NULLA<br>
	 * <pre>----------------------------------------------------------------*
	 *           LA RIVALUTAZIONE E' PIENA PARZIALE O NULLA            *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10300RivPienaParzNulla() {
        // COB_CODE: MOVE 'S10300-RIV-PIENA-PARZ-NULLA'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10300-RIV-PIENA-PARZ-NULLA");
        //
        // COB_CODE: PERFORM S10310-LEGGE-GG-RIT-PAG
        //              THRU S10310-LEGGE-GG-RIT-PAG-EX.
        s10310LeggeGgRitPag();
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: SET WTGA-ST-MOD(IX-TAB-TGA) TO TRUE
            wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabPvt()).getLccvtga1().getStatus().setMod();
            // COB_CODE: MOVE 'CO'
            //             TO WTGA-TP-RIVAL(IX-TAB-TGA)
            wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabPvt()).getLccvtga1().getDati().setWtgaTpRival("CO");
            // COB_CODE: IF DTC-NUM-GG-RITARDO-PAG-NULL = HIGH-VALUES
            //              MOVE ZERO TO DTC-NUM-GG-RITARDO-PAG
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getDettTitCont().getDtcNumGgRitardoPag().getDtcNumGgRitardoPagNullFormatted())) {
                // COB_CODE: MOVE ZERO TO DTC-NUM-GG-RITARDO-PAG
                ws.getDettTitCont().getDtcNumGgRitardoPag().setDtcNumGgRitardoPag(0);
            }
            // COB_CODE:         IF DTC-NUM-GG-RITARDO-PAG > ZERO
            //           *
            //                      END-IF
            //           *
            //                   END-IF
            if (ws.getDettTitCont().getDtcNumGgRitardoPag().getDtcNumGgRitardoPag() > 0) {
                //
                // COB_CODE:            IF W670-DT-VLT(IX-W670) <
                //                         WPMO-DT-RICOR-SUCC(1)
                //           *
                //                           TO WTGA-TP-RIVAL(IX-TAB-TGA)
                //           *
                //                      ELSE
                //           *
                //                           TO WTGA-TP-RIVAL(IX-TAB-TGA)
                //           *
                //                      END-IF
                if (w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).getW670DtVlt().getW670DtVlt() < wpmoAreaParamMovi.getTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc()) {
                    //
                    // COB_CODE: MOVE 'PA'
                    //             TO WTGA-TP-RIVAL(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabPvt()).getLccvtga1().getDati().setWtgaTpRival("PA");
                    //
                }
                else {
                    //
                    // COB_CODE: MOVE 'NU'
                    //             TO WTGA-TP-RIVAL(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabPvt()).getLccvtga1().getDati().setWtgaTpRival("NU");
                    //
                }
                //
            }
            //
        }
    }

    /**Original name: S10310-LEGGE-GG-RIT-PAG<br>*/
    private void s10310LeggeGgRitPag() {
        // COB_CODE: MOVE 'S10310-LEGGE-GG-RIT-PAG'
        //                TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10310-LEGGE-GG-RIT-PAG");
        // COB_CODE: SET IDSI0011-SELECT       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: MOVE ZERO                 TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                        IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                 TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG.
        ws.getDettTitCont().setDtcIdOgg(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabPvt()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        // COB_CODE: SET TRANCHE                 TO TRUE.
        ws.getWsTpOgg().setTranche();
        // COB_CODE: MOVE WS-TP-OGG              TO DTC-TP-OGG.
        ws.getDettTitCont().setDtcTpOgg(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: SET IDSI0011-ID-OGGETTO     TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: MOVE 'DETT-TIT-CONT'        TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("DETT-TIT-CONT");
        // COB_CODE: MOVE DETT-TIT-CONT          TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getDettTitCont().getDettTitContFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //                    END-IF
        //                 ELSE
        //           *--> GESTIRE ERRORE
        //                         THRU EX-S0300
        //                 END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF IDSO0011-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                    ELSE
            //           *--> GESTIRE ERRORE
            //                            THRU EX-S0300
            //                    END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                             WHEN IDSO0011-NOT-FOUND
                //           *-->    NESSUN DATO ESTRATTO DALLA TABELLA
                //                                     THRU EX-S0300
                //                             WHEN IDSO0011-SUCCESSFUL-SQL
                //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                  MOVE IDSO0011-BUFFER-DATI TO DETT-TIT-CONT
                //                             WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                                     THRU EX-S0300
                //                      END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.NOT_FOUND://-->    NESSUN DATO ESTRATTO DALLA TABELLA
                        // COB_CODE: MOVE 'IDBSDTC0'
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                        // COB_CODE: MOVE 'S11000-LEGGI-DETT-TIT-CONT'
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("S11000-LEGGI-DETT-TIT-CONT");
                        // COB_CODE: MOVE '005166'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005166");
                        // COB_CODE: MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                        //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                        // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                        //                  IDSO0011-RETURN-CODE     ';'
                        //                  IDSO0011-SQLCODE
                        //                  DELIMITED BY SIZE
                        //                  INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO DETT-TIT-CONT
                        ws.getDettTitCont().setDettTitContFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE 'IDBSDTC0'
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                        // COB_CODE: MOVE 'S11000-LEGGI-DETT-TIT-CONT'
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("S11000-LEGGI-DETT-TIT-CONT");
                        // COB_CODE: MOVE '005166'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005166");
                        // COB_CODE: MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                        //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                        // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                        //                  IDSO0011-RETURN-CODE     ';'
                        //                  IDSO0011-SQLCODE
                        //                  DELIMITED BY SIZE
                        //                  INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE 'IDBSDTC0'
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                // COB_CODE: MOVE 'S11000-LEGGI-DETT-TIT-CONT'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S11000-LEGGI-DETT-TIT-CONT");
                // COB_CODE: MOVE '005166'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005166");
                // COB_CODE: MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                //                  IDSO0011-RETURN-CODE     ';'
                //                  IDSO0011-SQLCODE
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        else {
            //--> GESTIRE ERRORE
            // COB_CODE: MOVE 'IDBSDTC0'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
            // COB_CODE: MOVE 'S11000-LEGGI-DETT-TIT-CONT'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S11000-LEGGI-DETT-TIT-CONT");
            // COB_CODE: MOVE '005166'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: MOVE SPACES TO IEAI9901-PARAMETRI-ERR
            //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
            // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
            //                  IDSO0011-RETURN-CODE     ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S10400-CARICA-TIT-DB<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICAMENTO TITOLO CONTABILE DA DB
	 * ----------------------------------------------------------------*</pre>*/
    private void s10400CaricaTitDb() {
        // COB_CODE: MOVE 'S10400-CARICA-TIT-DB'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10400-CARICA-TIT-DB");
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                          THRU S10220-RICERCA-TITOLO-EX
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTabPvt(((short)1));
        while (!(ws.getIxIndici().getTabPvt() > wtgaAreaTranche.getEleTranMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE: PERFORM S10220-RICERCA-TITOLO
            //              THRU S10220-RICERCA-TITOLO-EX
            s10220RicercaTitolo();
            //
            ws.getIxIndici().setTabPvt(Trunc.toShort(ws.getIxIndici().getTabPvt() + 1, 4));
        }
    }

    /**Original name: S10500-CTRL-TIT<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLO CHE PER OGNI TRANCHE VI SIA UN SOLO TITOLO CONTABILE
	 * ----------------------------------------------------------------*</pre>*/
    private void s10500CtrlTit() {
        // COB_CODE: MOVE 'S10500-CTRL-TIT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10500-CTRL-TIT");
        //
        // --> Viene gestito un errore bloccante se esistono
        // --> piy Titoli Cotabili per una Tranche
        //
        // COB_CODE: PERFORM S10510-CTRL-STATO-TIT
        //              THRU S10510-CTRL-STATO-TIT-EX.
        s10510CtrlStatoTit();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10520-CTRL-UNI-TIT-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10520-CTRL-UNI-TIT
            //              THRU S10520-CTRL-UNI-TIT-EX
            s10520CtrlUniTit();
            //
        }
    }

    /**Original name: S10510-CTRL-STATO-TIT<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLI SULLO STATO DEL TITOLO
	 * ----------------------------------------------------------------*</pre>*/
    private void s10510CtrlStatoTit() {
        // COB_CODE: MOVE 'S10510-CTRL-STATO-TIT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10510-CTRL-STATO-TIT");
        //
        // COB_CODE:      PERFORM VARYING IX-W670 FROM 1 BY 1
        //                  UNTIL IX-W670 > W670-NUM-MAX-ELE
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                      END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTabPag(((short)1));
        while (!(ws.getIxIndici().getTabPag() > w670AreaLoas0670.getNumMaxEle() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE:            IF W670-TP-STAT-TIT(IX-W670) = 'EM'
            //           *
            //                            THRU GESTIONE-ERR-STD-EX
            //           *
            //                      END-IF
            if (Conditions.eq(w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).getW670TpStatTit(), "EM")) {
                //
                // COB_CODE: MOVE 'TIT.CONT. NON INCASSATO, RIESEGUIRE RIVAL'
                //             TO WK-STRING
                ws.getWkGestioneMsgErr().setStringFld("TIT.CONT. NON INCASSATO, RIESEGUIRE RIVAL");
                // COB_CODE: MOVE '001114'
                //             TO WK-COD-ERR
                ws.getWkGestioneMsgErr().setCodErr("001114");
                // COB_CODE: PERFORM GESTIONE-ERR-STD
                //              THRU GESTIONE-ERR-STD-EX
                gestioneErrStd();
                //
            }
            //
            ws.getIxIndici().setTabPag(Trunc.toShort(ws.getIxIndici().getTabPag() + 1, 4));
        }
    }

    /**Original name: S10520-CTRL-UNI-TIT<br>
	 * <pre>----------------------------------------------------------------*
	 *  PER UNA TRANCHE NON CI POSSONO ESSERE PIU' TITOLI
	 * ----------------------------------------------------------------*</pre>*/
    private void s10520CtrlUniTit() {
        // COB_CODE: MOVE 'S10520-CTRL-UNI-TIT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10520-CTRL-UNI-TIT");
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                    END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTabPvt(((short)1));
        while (!(ws.getIxIndici().getTabPvt() > wtgaAreaTranche.getEleTranMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE: MOVE ZEROES
            //             TO WK-CTR-TIT
            ws.setWkCtrTit(((short)0));
            //
            // COB_CODE:          PERFORM VARYING IX-W670 FROM 1 BY 1
            //                      UNTIL IX-W670 > W670-NUM-MAX-ELE
            //                         OR WK-CTR-TIT > 1
            //                         OR IDSV0001-ESITO-KO
            //           *
            //                          END-IF
            //           *
            //                    END-PERFORM
            ws.getIxIndici().setTabPag(((short)1));
            while (!(ws.getIxIndici().getTabPag() > w670AreaLoas0670.getNumMaxEle() || ws.getWkCtrTit() > 1 || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                //
                // COB_CODE:                IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                //                             W670-ID-TRCH-DI-GAR(IX-W670)
                //           *
                //                               TO WK-CTR-TIT
                //           *
                //                          END-IF
                if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabPvt()).getLccvtga1().getDati().getWtgaIdTrchDiGar() == w670AreaLoas0670.getDatiTit(ws.getIxIndici().getTabPag()).getW670IdTrchDiGar()) {
                    //
                    // COB_CODE: ADD 1
                    //             TO WK-CTR-TIT
                    ws.setWkCtrTit(Trunc.toShort(1 + ws.getWkCtrTit(), 1));
                    //
                }
                //
                ws.getIxIndici().setTabPag(Trunc.toShort(ws.getIxIndici().getTabPag() + 1, 4));
            }
            //
            // COB_CODE:          IF WK-CTR-TIT > 1
            //           *
            //                          THRU GESTIONE-ERR-STD-EX
            //           *
            //                    END-IF
            if (ws.getWkCtrTit() > 1) {
                //
                // COB_CODE: MOVE 'ESISTONO PIU TIT.CONT. PER UNA TRANCHE'
                //             TO WK-STRING
                ws.getWkGestioneMsgErr().setStringFld("ESISTONO PIU TIT.CONT. PER UNA TRANCHE");
                // COB_CODE: MOVE '001114'
                //             TO WK-COD-ERR
                ws.getWkGestioneMsgErr().setCodErr("001114");
                // COB_CODE: PERFORM GESTIONE-ERR-STD
                //              THRU GESTIONE-ERR-STD-EX
                gestioneErrStd();
                //
            }
            //
            ws.getIxIndici().setTabPvt(Trunc.toShort(ws.getIxIndici().getTabPvt() + 1, 4));
        }
    }

    /**Original name: S90000-OPERAZ-FINALI<br>
	 * <pre> ============================================================== *
	 *  -->           O P E R Z I O N I   F I N A L I              <-- *
	 *  ============================================================== *</pre>*/
    private void s90000OperazFinali() {
        // COB_CODE: MOVE 'S90000-OPERAZ-FINALI'  TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S90000-OPERAZ-FINALI");
        //
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: GESTIONE-ERR-STD<br>
	 * <pre>----------------------------------------------------------------*
	 *                 GESTIONE STANDARD DELL'ERRORE                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneErrStd() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-PGM
        //             TO IEAI9901-COD-SERVIZIO-BE.
        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
        // COB_CODE: MOVE WK-LABEL-ERR
        //             TO IEAI9901-LABEL-ERR.
        ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
        // COB_CODE: MOVE WK-COD-ERR
        //             TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted(ws.getWkGestioneMsgErr().getCodErrFormatted());
        // COB_CODE: STRING WK-STRING ';'
        //                  IDSO0011-RETURN-CODE ';'
        //                  IDSO0011-SQLCODE
        //                  DELIMITED BY SIZE
        //                  INTO IEAI9901-PARAMETRI-ERR
        //           END-STRING.
        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkGestioneMsgErr().getStringFldFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
        //
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *      ROUTINES CALL DISPATCHER                                   *
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *   ROUTINES GESTIONE ERRORI                                      *
	 * ----------------------------------------------------------------*
	 * MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    public void initIxIndici() {
        ws.getIxIndici().setTabPvt(((short)0));
        ws.getIxIndici().setTabPag(((short)0));
        ws.getIxIndici().setGarTari(((short)0));
        ws.getIxIndici().setRicRan(((short)0));
    }
}
