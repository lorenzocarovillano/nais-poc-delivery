package it.accenture.jnais;

import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Lccc0024DatiInput;
import it.accenture.jnais.copy.WpmoDati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaPassaggio;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.Iabv0006;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Loas0110Data;
import it.accenture.jnais.ws.ptr.Idsv8888StrPerformanceDbg;
import it.accenture.jnais.ws.redefines.PmoDtRicorPrec;
import it.accenture.jnais.ws.redefines.PmoDtRicorSucc;
import it.accenture.jnais.ws.redefines.PmoDtUltErogManfee;
import it.accenture.jnais.ws.redefines.PmoIdAdes;
import it.accenture.jnais.ws.WadeAreaAdesioneLccs0005;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.WmovAreaMovimento;
import it.accenture.jnais.ws.WpmoAreaParamMovi;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WricAreaRichiesta;
import it.accenture.jnais.ws.WrreAreaRappRete;
import it.accenture.jnais.ws.WtgaAreaTranche;
import javax.inject.Inject;

/**Original name: LOAS0110<br>
 * <pre>****************************************************************
 * *                                                              *
 * *    PORTAFOGLIO VITA ITALIA VER. 1.0                          *
 * *                                                              *
 * ****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED. 12-Feb-08 22:04.
 *  **-----------------------------------------------------------**
 *  **-----------------------------------------------------------**
 *  **-----------------------------------------------------------**
 *  **  PROGRAMMA ..... LOAS0110                                 **
 *  **  TIPOLOGIA...... DRIVER EOC                               **
 *  **  PROCESSO....... XXX                                      **
 *  **  FUNZIONE....... XXX                                      **
 *  **  DESCRIZIONE.... EOC COMUNE - OPERAZIONI AUTOMATICHE -    **
 *  **-----------------------------------------------------------**</pre>*/
public class Loas0110 extends Program {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    //Original name: WORKING-STORAGE
    private Loas0110Data ws = new Loas0110Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLccs0005 wadeAreaAdesione;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WMOV-AREA-MOVIMENTO
    private WmovAreaMovimento wmovAreaMovimento;
    //Original name: WPMO-AREA-PARAM-MOVI
    private WpmoAreaParamMovi wpmoAreaParamMovi;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WRIC-AREA-RICHIESTA
    private WricAreaRichiesta wricAreaRichiesta;
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTranche wtgaAreaTranche;
    //Original name: AREA-PASSAGGIO
    private AreaPassaggio areaPassaggio;
    //Original name: IABV0006
    private Iabv0006 iabv0006;

    //==== METHODS ====
    /**Original name: PROGRAM_LOAS0110_FIRST_SENTENCES<br>
	 * <pre> ---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, WadeAreaAdesioneLccs0005 wadeAreaAdesione, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WmovAreaMovimento wmovAreaMovimento, WpmoAreaParamMovi wpmoAreaParamMovi, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WricAreaRichiesta wricAreaRichiesta, WtgaAreaTranche wtgaAreaTranche, AreaPassaggio areaPassaggio, Iabv0006 iabv0006) {
        this.areaIdsv0001 = areaIdsv0001;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wmovAreaMovimento = wmovAreaMovimento;
        this.wpmoAreaParamMovi = wpmoAreaParamMovi;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wricAreaRichiesta = wricAreaRichiesta;
        this.wtgaAreaTranche = wtgaAreaTranche;
        this.areaPassaggio = areaPassaggio;
        this.iabv0006 = iabv0006;
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(this.areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: SET WCOM-POST-VENDITA        TO TRUE.
        ws.getWcomFlagConversazione().setPostVendita();
        // COB_CODE: PERFORM S0000B-OPERAZIONI-INIZIALI
        //              THRU EX-S0000B
        s0000bOperazioniIniziali();
        // COB_CODE: IF  IDSV0001-ESITO-OK
        //           AND IABV0006-SIMULAZIONE-NO
        //                 THRU EX-S1000B
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk() && this.iabv0006.getFlagSimulazione().isNo()) {
            // COB_CODE: PERFORM S1000B-ELABORAZIONE
            //              THRU EX-S1000B
            s1000bElaborazione();
        }
        // COB_CODE: PERFORM S9000B-OPERAZIONI-FINALI
        //              THRU EX-S9000B
        s9000bOperazioniFinali();
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Loas0110 getInstance() {
        return ((Loas0110)Programs.getInstance(Loas0110.class));
    }

    /**Original name: S0000B-OPERAZIONI-INIZIALI<br>
	 * <pre> ---------------------------------------------------------------*
	 *                       OPERAZIONI INIZIALI                       *
	 *  ---------------------------------------------------------------*</pre>*/
    private void s0000bOperazioniIniziali() {
        // COB_CODE: MOVE 'S0000B-OPERAZIONI-INIZIALI'
        //             TO WK-LABEL-ERR.
        ws.setWkLabelErr("S0000B-OPERAZIONI-INIZIALI");
        // COB_CODE: PERFORM S0100B-INITIALIZE-AREE
        //              THRU EX-S0100B.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LOAS0110.cbl:line=216, because the code is unreachable.
        //
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO    TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        //
        // COB_CODE: PERFORM S0200-CTRL-DATI  THRU EX-S0200.
        s0200CtrlDati();
    }

    /**Original name: S0200-CTRL-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *   CONTROLLI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0200CtrlDati() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S0200-CTRL-DATI' TO WK-LABEL-ERR.
        ws.setWkLabelErr("S0200-CTRL-DATI");
        // COB_CODE: IF WCOM-STEP-ELAB NOT NUMERIC
        //                 THRU EX-S0300
        //           ELSE
        //              END-IF
        //           END-IF.
        if (!Functions.isNumber(areaPassaggio.getLccc0261().getDatiMbs().getWcomStepElab())) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //            TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: STRING 'STEP DI ELABORAZIONE NON VALORIZZATO'
            //              DELIMITED BY SIZE
            //              INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr("STEP DI ELABORAZIONE NON VALORIZZATO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        else if (!Conditions.eq(String.valueOf(areaPassaggio.getLccc0261().getDatiMbs().getWcomStepElab()), "1") && !Conditions.eq(String.valueOf(areaPassaggio.getLccc0261().getDatiMbs().getWcomStepElab()), "2")) {
            // COB_CODE: IF WCOM-STEP-ELAB NOT = (1 AND 2)
            //                 THRU EX-S0300
            //           END-IF
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //            TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: STRING 'STEP DI ELABORAZIONE VALORE NON AMMESSO'
            //              DELIMITED BY SIZE
            //              INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr("STEP DI ELABORAZIONE VALORE NON AMMESSO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        // COB_CODE: IF WCOM-WRITE-ERR
        //                 THRU EX-S0210
        //           END-IF.
        if (areaPassaggio.getLccc0261().getWrite().isErr()) {
            // COB_CODE: PERFORM S0210-CTRL-DATI-X-ERR
            //              THRU EX-S0210
            s0210CtrlDatiXErr();
        }
    }

    /**Original name: S0210-CTRL-DATI-X-ERR<br>
	 * <pre>----------------------------------------------------------------*
	 *   CONTROLLI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0210CtrlDatiXErr() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF WPOL-ELE-POLI-MAX = 0
        //                 THRU EX-S0300
        //           END-IF.
        if (wpolAreaPolizza.getWpolElePoliMax() == 0) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //            TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: STRING 'OCORRENZA POLIZZA NON VALORIZZATA'
            //              DELIMITED BY SIZE
            //              INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr("OCORRENZA POLIZZA NON VALORIZZATA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        // COB_CODE: IF WCOM-ID-POLIZZA = ZEROES         OR
        //              WCOM-ID-POLIZZA   NOT NUMERIC
        //                 THRU EX-S0300
        //           END-IF.
        if (areaPassaggio.getLccc0261().getDatiPolizza().getIdPolizza() == 0 || !Functions.isNumber(areaPassaggio.getLccc0261().getDatiPolizza().getIdPolizza())) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //            TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: STRING 'ID POLIZZA NON VALORIZZATO'
            //              DELIMITED BY SIZE
            //              INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr("ID POLIZZA NON VALORIZZATO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        // COB_CODE: IF WCOM-ADESIONE-MBS
        //              END-IF
        //           END-IF.
        if (areaPassaggio.getLccc0261().getDatiMbs().getWcomTpOggMbs().isAdesioneMbs()) {
            // COB_CODE: IF WCOM-ID-ADES    = ZEROES         OR
            //              WCOM-ID-POLIZZA   NOT NUMERIC
            //                 THRU EX-S0300
            //           END-IF
            if (areaPassaggio.getLccc0261().getIdAdes() == 0 || !Functions.isNumber(areaPassaggio.getLccc0261().getDatiPolizza().getIdPolizza())) {
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL-ERR
                //            TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
                // COB_CODE: MOVE '001114'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("001114");
                // COB_CODE: STRING 'ID ADESIONE NON VALORIZZATO'
                //              DELIMITED BY SIZE
                //              INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                ws.getIeai9901Area().setParametriErr("ID ADESIONE NON VALORIZZATO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
            // COB_CODE: IF WADE-ELE-ADES-MAX = 0
            //                 THRU EX-S0300
            //           END-IF
            if (wadeAreaAdesione.getWadeEleAdesMax() == 0) {
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL-ERR
                //            TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
                // COB_CODE: MOVE '001114'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("001114");
                // COB_CODE: STRING 'OCORRENZA ADESIONE NON VALORIZZATA'
                //              DELIMITED BY SIZE
                //              INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                ws.getIeai9901Area().setParametriErr("OCORRENZA ADESIONE NON VALORIZZATA");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S1000B-ELABORAZIONE<br>
	 * <pre> ---------------------------------------------------------------*
	 *                          ELABORAZIONE                           *
	 *  ---------------------------------------------------------------*</pre>*/
    private void s1000bElaborazione() {
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU S1300-PREPARA-LCCS0005-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1300-PREPARA-LCCS0005
            //              THRU S1300-PREPARA-LCCS0005-EX
            s1300PreparaLccs0005();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //               TO TRUE
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1350-CALL-LCCS0005
            //              THRU S1350-CALL-LCCS0005-EX
            s1350CallLccs0005();
            // COB_CODE: SET WMOV-ST-INV
            //            TO TRUE
            wmovAreaMovimento.getLccvmov1().getStatus().setInv();
        }
        //
        // COB_CODE: IF STANDARD-CALL
        //               END-IF
        //           END-IF.
        if (areaPassaggio.getLccc0261().getBsCallType().isStandardCall()) {
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU EX-S1900
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM S1900-GESTIONE-PMO
                //              THRU EX-S1900
                s1900GestionePmo();
            }
            //
            // COB_CODE: IF WCOM-WRITE-ERR
            //              END-IF
            //           END-IF
            if (areaPassaggio.getLccc0261().getWrite().isErr()) {
                // COB_CODE: IF IDSV0001-ESITO-OK
                //                 THRU EX-S2000
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: MOVE WCOM-TP-OGG-MBS TO WK-TP-OGG-MBS
                    ws.setWkTpOggMbs(areaPassaggio.getLccc0261().getDatiMbs().getWcomTpOggMbs().getWcomTpOggMbs());
                    // COB_CODE: PERFORM S2100B-IMPOSTA-LCCS0024
                    //              THRU EX-S2100B
                    s2100bImpostaLccs0024();
                    // COB_CODE: PERFORM S2000-CALL-LCCS0024
                    //              THRU EX-S2000
                    s2000CallLccs0024();
                }
            }
        }
    }

    /**Original name: S1300-PREPARA-LCCS0005<br>
	 * <pre>-----------------------------------------------------------------
	 *  PREPARA CHIAMATA AL LCCS0005
	 * -----------------------------------------------------------------</pre>*/
    private void s1300PreparaLccs0005() {
        // COB_CODE: SET LCCV0005-OPER-AUTOM      TO TRUE.
        ws.getLccv0005().getMacroFunzione().setLccv0005OperAutom();
        // COB_CODE: SET LCCV0005-RICHIESTA-NO    TO TRUE.
        ws.getLccv0005().getRichiesta().setLccv0005RichiestaNo();
        // COB_CODE: INITIALIZE  WRRE-AREA-RAP-RETE
        //                       WNOT-AREA-NOTE-OGG.
        initWrreAreaRapRete();
        initWnotAreaNoteOgg();
        // COB_CODE: PERFORM VALORIZZAZIONE-AREA-STATI
        //              THRU VALORIZZAZIONE-AREA-STATI-EX.
        valorizzazioneAreaStati();
    }

    /**Original name: S1350-CALL-LCCS0005<br>
	 * <pre> ---------------------------------------------------------------
	 *  CALL MODULO LCCS0005
	 *  ---------------------------------------------------------------</pre>*/
    private void s1350CallLccs0005() {
        Lccs0005 lccs0005 = null;
        // COB_CODE: MOVE 'S1350-CALL-LCCS0005'   TO WK-LABEL-ERR
        ws.setWkLabelErr("S1350-CALL-LCCS0005");
        // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
        ws.getIdsv8888().getLivelloDebug().setStressTestDbg();
        // COB_CODE: SET  IDSV8888-SONDA-S9           TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setSondaS9();
        // COB_CODE: MOVE IDSI0011-MODALITA-ESECUTIVA
        //                TO IDSV8888-MODALITA-ESECUTIVA
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().getIdsi0011ModalitaEsecutiva());
        // COB_CODE: MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
        ws.getIdsv8888().getStrPerformanceDbg().setUserName(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011UserName());
        // COB_CODE: MOVE 'LCCS0005'                  TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0005");
        // COB_CODE: MOVE 'LCCS0005       '           TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("LCCS0005       ");
        // COB_CODE: SET  IDSV8888-INIZIO             TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LCCS0005'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0005");
        // COB_CODE: MOVE 'Servizio di EOC'              TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Servizio di EOC");
        // COB_CODE: SET  IDSV8888-INIZIO                TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: CALL LCCS0005  USING AREA-IDSV0001
        //                                WCOM-AREA-STATI
        //                                WMOV-AREA-MOVIMENTO
        //                                WRIC-AREA-RICHIESTA
        //                                WPOL-AREA-POLIZZA
        //                                WADE-AREA-ADESIONE
        //                                WGRZ-AREA-GARANZIA
        //                                WTGA-AREA-TRANCHE
        //                                AREA-IO-LCCS0005
        //                                WRRE-AREA-RAP-RETE
        //                                WNOT-AREA-NOTE-OGG
        //           ON EXCEPTION
        //                    THRU EX-S0290
        //           END-CALL.
        try {
            lccs0005 = Lccs0005.getInstance();
            lccs0005.run(new Object[] {areaIdsv0001, areaPassaggio, wmovAreaMovimento, wricAreaRichiesta, wpolAreaPolizza, wadeAreaAdesione, wgrzAreaGaranzia, wtgaAreaTranche, ws.getLccv0005(), ws.getWrreAreaRapRete(), ws.getWnotAreaNoteOgg()});
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'PRE EOC COMUNE'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("PRE EOC COMUNE");
            // COB_CODE: MOVE WK-LABEL-ERR
            //            TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
        ws.getIdsv8888().getLivelloDebug().setStressTestDbg();
        // COB_CODE: SET  IDSV8888-SONDA-S9           TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setSondaS9();
        // COB_CODE: MOVE IDSI0011-MODALITA-ESECUTIVA
        //                TO IDSV8888-MODALITA-ESECUTIVA
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().getIdsi0011ModalitaEsecutiva());
        // COB_CODE: MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
        ws.getIdsv8888().getStrPerformanceDbg().setUserName(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011UserName());
        // COB_CODE: MOVE 'LCCS0005'                  TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0005");
        // COB_CODE: MOVE 'LCCS0005       '           TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("LCCS0005       ");
        // COB_CODE: SET  IDSV8888-FINE               TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LCCS0005'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0005");
        // COB_CODE: MOVE 'Servizio di EOC'              TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Servizio di EOC");
        // COB_CODE: SET  IDSV8888-FINE                  TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: VALORIZZAZIONE-AREA-STATI<br>
	 * <pre> ---------------------------------------------------------------
	 *  VALORIZZAZIONE AREA STATI X DEFINIRE IL MOVIMENTO E LO STATO
	 *  DI WORKFLOW
	 *  ---------------------------------------------------------------</pre>*/
    private void valorizzazioneAreaStati() {
        // COB_CODE: MOVE 'CO'   TO  WCOM-STATO-MOVIMENTO.
        areaPassaggio.getLccc0001().getStati().setStatoMovimento("CO");
        // COB_CODE: MOVE 1      TO  WCOM-MOV-FLAG-ST-FINALE.
        areaPassaggio.getLccc0001().getStati().getMovFlagStFinale().setMovFlagStFinaleFormatted("1");
        // COB_CODE: MOVE ZEROES TO  WCOM-NUM-ELE-MOT-DEROGA.
        areaPassaggio.getLccc0001().getDatiDeroghe().setNumEleMotDeroga(((short)0));
    }

    /**Original name: S1900-GESTIONE-PMO<br>
	 * <pre>-----------------------------------------------------------------
	 *  SCRIVE L''OCCORRENZA FUTURA DI PMO PER ESSERE GESTITA DAL
	 *  SUCCESSIVO PROCESSO BATCH DI OA
	 * -----------------------------------------------------------------</pre>*/
    private void s1900GestionePmo() {
        // COB_CODE: PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
        //                     UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
        //                        OR IDSV0001-ESITO-KO
        //                 THRU AGGIORNA-PARAM-MOVI-EX
        //           END-PERFORM.
        ws.setIxTabPmo(((short)1));
        while (!(ws.getIxTabPmo() > wpmoAreaParamMovi.getEleParamMovMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            // COB_CODE: PERFORM AGGIORNA-PARAM-MOVI
            //              THRU AGGIORNA-PARAM-MOVI-EX
            aggiornaParamMovi();
            ws.setIxTabPmo(Trunc.toShort(ws.getIxTabPmo() + 1, 4));
        }
    }

    /**Original name: S2000-CALL-LCCS0024<br>
	 * <pre>----------------------------------------------------------------*
	 *    SCRITTURA MOVIMENTO BATCH SOSPESI
	 * ----------------------------------------------------------------*</pre>*/
    private void s2000CallLccs0024() {
        Lccs0024 lccs0024 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
        ws.getIdsv8888().getLivelloDebug().setStressTestDbg();
        // COB_CODE: SET  IDSV8888-SONDA-S9           TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setSondaS9();
        // COB_CODE: MOVE IDSI0011-MODALITA-ESECUTIVA
        //                TO IDSV8888-MODALITA-ESECUTIVA
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().getIdsi0011ModalitaEsecutiva());
        // COB_CODE: MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
        ws.getIdsv8888().getStrPerformanceDbg().setUserName(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011UserName());
        // COB_CODE: MOVE 'LCCS0024'                  TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0024");
        // COB_CODE: MOVE 'LCCS0024       '           TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("LCCS0024       ");
        // COB_CODE: SET  IDSV8888-INIZIO             TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LCCS0024'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0024");
        // COB_CODE: MOVE 'Scritt. movim. sospesi'       TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Scritt. movim. sospesi");
        // COB_CODE: SET  IDSV8888-INIZIO                TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: CALL LCCS0024 USING AREA-IDSV0001
        //                              S0024-AREA-COMUNE
        //                              AREA-IO-LCCS0024
        //               ON EXCEPTION
        //                  THRU EX-S0290
        //           END-CALL.
        try {
            lccs0024 = Lccs0024.getInstance();
            lccs0024.run(areaIdsv0001, ws.getS0024AreaComune(), ws.getAreaIoLccs0024());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //           TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'GESTIONE OGGETTO BLOCCO'  TO CALL-DESC
            ws.getIdsv0002().setCallDesc("GESTIONE OGGETTO BLOCCO");
            // COB_CODE: MOVE 'S2000-CALL-LCCS0024'      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S2000-CALL-LCCS0024");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
        ws.getIdsv8888().getLivelloDebug().setStressTestDbg();
        // COB_CODE: SET  IDSV8888-SONDA-S9           TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setSondaS9();
        // COB_CODE: MOVE IDSI0011-MODALITA-ESECUTIVA
        //                TO IDSV8888-MODALITA-ESECUTIVA
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().getIdsi0011ModalitaEsecutiva());
        // COB_CODE: MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
        ws.getIdsv8888().getStrPerformanceDbg().setUserName(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011UserName());
        // COB_CODE: MOVE 'LCCS0024'                  TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0024");
        // COB_CODE: MOVE 'LCCS0024       '           TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("LCCS0024       ");
        // COB_CODE: SET  IDSV8888-FINE               TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LCCS0024'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0024");
        // COB_CODE: MOVE 'Scritt. movim. sospesi'       TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Scritt. movim. sospesi");
        // COB_CODE: SET  IDSV8888-FINE                  TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S2200
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S2200-GESTIONE-OUT
            //              THRU EX-S2200
            s2200GestioneOut();
        }
        else {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S2000-CALL-LCCS0024'
            //            TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S2000-CALL-LCCS0024");
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: STRING 'ERRORE CHIAMATA LCCS0024'
            //              DELIMITED BY SIZE
            //              INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr("ERRORE CHIAMATA LCCS0024");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S2100B-IMPOSTA-LCCS0024<br>
	 * <pre>----------------------------------------------------------------*
	 *  IMPOSTA AREA COMUNE
	 * ----------------------------------------------------------------*
	 *  ANNULLI
	 *  QUANDO PRONTI PER GLI ANNULLI VALORIZZARE LA PRIMA OCCORRENZA</pre>*/
    private void s2100bImpostaLccs0024() {
        // COB_CODE: INITIALIZE              AREA-IO-LCCS0024.
        initAreaIoLccs0024();
        //
        // COB_CODE: MOVE WCOM-AREA-STATI    TO S0024-AREA-STATI.
        ws.getS0024AreaComune().setS0024AreaStatiBytes(areaPassaggio.getWcomAreaStatiBytes());
        //--> DATI OGGETTO BLOCCO
        // COB_CODE: MOVE WCOM-ID-POLIZZA TO LCCC0024-ID-OGG-PRIM.
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024IdOggPrim(areaPassaggio.getLccc0261().getDatiPolizza().getIdPolizza());
        //
        // COB_CODE: SET POLIZZA             TO TRUE.
        ws.getWsTpOgg().setPolizza();
        // COB_CODE: MOVE WS-TP-OGG          TO LCCC0024-TP-OGG-PRIM.
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024TpOggPrim(ws.getWsTpOgg().getWsTpOgg());
        //
        // COB_CODE: IF WCOM-POLIZZA-L11
        //              MOVE WCOM-ID-POLIZZA TO LCCC0024-ID-OGGETTO
        //           ELSE
        //              MOVE WCOM-ID-ADES    TO LCCC0024-ID-OGGETTO
        //           END-IF.
        if (areaPassaggio.getLccc0261().getDatiBlocco().getTpOggBlocco().isPolizzaL11()) {
            // COB_CODE: MOVE WCOM-ID-POLIZZA TO LCCC0024-ID-OGGETTO
            ws.getAreaIoLccs0024().getDatiInput().setLccc0024IdOggetto(areaPassaggio.getLccc0261().getDatiPolizza().getIdPolizza());
        }
        else {
            // COB_CODE: MOVE WCOM-ID-ADES    TO LCCC0024-ID-OGGETTO
            ws.getAreaIoLccs0024().getDatiInput().setLccc0024IdOggetto(areaPassaggio.getLccc0261().getIdAdes());
        }
        //
        // COB_CODE: MOVE WCOM-TP-OGG-BLOCCO TO LCCC0024-TP-OGGETTO
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024TpOggetto(areaPassaggio.getLccc0261().getDatiBlocco().getTpOggBlocco().getTpOggBlocco());
        //
        // COB_CODE: MOVE 'DISPO'            TO LCCC0024-COD-BLOCCO.
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024CodBlocco("DISPO");
        //
        // COB_CODE: IF WMOV-ID-RICH IS NUMERIC AND
        //              WMOV-ID-RICH NOT = ZEROES
        //              MOVE WMOV-ID-RICH       TO LCCC0024-ID-RICH
        //           END-IF
        if (Functions.isNumber(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdRich().getWmovIdRich()) && wmovAreaMovimento.getLccvmov1().getDati().getWmovIdRich().getWmovIdRich() != 0) {
            // COB_CODE: MOVE WMOV-ID-RICH       TO LCCC0024-ID-RICH
            ws.getAreaIoLccs0024().getDatiInput().setLccc0024IdRich(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdRich().getWmovIdRich());
        }
        // POPOLAMENTO DATI MOVIMENTO BATCH SOSPESI
        // COB_CODE: MOVE 1                  TO LCCC0024-ELE-MOV-SOS-MAX.
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024EleMovSosMax(((short)1));
        // COB_CODE: IF WCOM-POLIZZA-MBS
        //              MOVE WCOM-ID-POLIZZA TO LCCC0024-ID-OGG-SOSP(1)
        //           ELSE
        //              MOVE WCOM-ID-ADES    TO LCCC0024-ID-OGG-SOSP(1)
        //           END-IF.
        if (areaPassaggio.getLccc0261().getDatiMbs().getWcomTpOggMbs().isPolizzaMbs()) {
            // COB_CODE: MOVE WCOM-ID-POLIZZA TO LCCC0024-ID-OGG-SOSP(1)
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).setLccc0024IdOggSosp(areaPassaggio.getLccc0261().getDatiPolizza().getIdPolizza());
        }
        else {
            // COB_CODE: MOVE WCOM-ID-ADES    TO LCCC0024-ID-OGG-SOSP(1)
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).setLccc0024IdOggSosp(areaPassaggio.getLccc0261().getIdAdes());
        }
        // COB_CODE: MOVE WK-TP-OGG-MBS      TO LCCC0024-TP-OGG-SOSP(1).
        ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).setLccc0024TpOggSosp(ws.getWkTpOggMbs());
        // COB_CODE: MOVE WCOM-TP-FRM-ASSVA  TO LCCC0024-TP-FRM-ASSVA(1).
        ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).setLccc0024TpFrmAssva(areaPassaggio.getLccc0261().getDatiMbs().getWcomTpFrmAssva().getWcomTpFrmAssva());
        // COB_CODE: IF WCOM-ID-BATCH-NULL = HIGH-VALUE
        //              MOVE WCOM-ID-BATCH-NULL TO LCCC0024-ID-BATCH-NULL(1)
        //           ELSE
        //              MOVE WCOM-ID-BATCH TO LCCC0024-ID-BATCH(1)
        //           END-IF.
        if (Characters.EQ_HIGH.test(areaPassaggio.getLccc0261().getDatiMbs().getWcomIdBatch().getWcomIdBatchNullFormatted())) {
            // COB_CODE: MOVE WCOM-ID-BATCH-NULL TO LCCC0024-ID-BATCH-NULL(1)
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).getLccc0024IdBatch().setLccc0024IdBatchNull(areaPassaggio.getLccc0261().getDatiMbs().getWcomIdBatch().getWcomIdBatchNull());
        }
        else {
            // COB_CODE: MOVE WCOM-ID-BATCH TO LCCC0024-ID-BATCH(1)
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).getLccc0024IdBatch().setLccc0024IdBatch(areaPassaggio.getLccc0261().getDatiMbs().getWcomIdBatch().getWcomIdBatch());
        }
        //
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO  TO LCCC0024-TP-MOVI-SOSP(1).
        ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).getLccc0024TpMoviSosp().setLccc0024TpMoviSosp(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
        //
        // COB_CODE: IF WCOM-ID-JOB-NULL = HIGH-VALUE
        //              MOVE WCOM-ID-JOB-NULL TO LCCC0024-ID-JOB-NULL(1)
        //           ELSE
        //              MOVE WCOM-ID-JOB TO LCCC0024-ID-JOB(1)
        //           END-IF.
        if (Characters.EQ_HIGH.test(areaPassaggio.getLccc0261().getDatiMbs().getWcomIdJob().getWcomIdJobNullFormatted())) {
            // COB_CODE: MOVE WCOM-ID-JOB-NULL TO LCCC0024-ID-JOB-NULL(1)
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).getLccc0024IdJob().setLccc0024IdJobNull(areaPassaggio.getLccc0261().getDatiMbs().getWcomIdJob().getWcomIdJobNull());
        }
        else {
            // COB_CODE: MOVE WCOM-ID-JOB TO LCCC0024-ID-JOB(1)
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).getLccc0024IdJob().setLccc0024IdJob(areaPassaggio.getLccc0261().getDatiMbs().getWcomIdJob().getWcomIdJob());
        }
        // COB_CODE: MOVE WCOM-STEP-ELAB TO LCCC0024-STEP-ELAB(1).
        ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).setLccc0024StepElab(areaPassaggio.getLccc0261().getDatiMbs().getWcomStepElab());
        // COB_CODE: MOVE WCOM-D-INPUT-MOVI-SOSP
        //             TO LCCC0024-D-INPUT-MOVI-SOSP(1).
        ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).setLccc0024DInputMoviSosp(areaPassaggio.getLccc0261().getDatiMbs().getWcomDInputMoviSosp());
        // COB_CODE: MOVE WMOV-ID-PTF
        //             TO LCCC0024-ID-MOVI-SOSP(1).
        ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(1).setLccc0024IdMoviSosp(wmovAreaMovimento.getLccvmov1().getIdPtf());
    }

    /**Original name: S2200-GESTIONE-OUT<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZA OUTPUT LCCS0024
	 * ----------------------------------------------------------------*</pre>*/
    private void s2200GestioneOut() {
        // COB_CODE: IF LCCC0024-BLC-CENSITO-PTF
        //              SET  WCOM-BLC-CENSITO-PTF   TO TRUE
        //           END-IF.
        if (ws.getAreaIoLccs0024().getVerificaBlocco().isLccc0024BlcCensitoPtf()) {
            // COB_CODE: MOVE LCCC0024-ID-OGG-BLOCCO TO WCOM-ID-BLOCCO-CRZ
            areaPassaggio.getLccc0261().getDatiBlocco().setIdBloccoCrz(ws.getAreaIoLccs0024().getIdOggBlocco());
            // COB_CODE: SET  WCOM-BLC-CENSITO-PTF   TO TRUE
            areaPassaggio.getLccc0261().getDatiBlocco().getVerificaBlocco().setCensitoPtf();
        }
    }

    /**Original name: CALL-LCCS0234<br>
	 * <pre>----------------------------------------------------------------*
	 *     CALL SERVIZIO ESTRAZIONE OGGETTO PTF LCCS0234
	 * ----------------------------------------------------------------*</pre>*/
    private void callLccs0234() {
        Lccs0234 lccs0234 = null;
        // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
        ws.getIdsv8888().getLivelloDebug().setStressTestDbg();
        // COB_CODE: SET  IDSV8888-SONDA-S9           TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setSondaS9();
        // COB_CODE: MOVE IDSI0011-MODALITA-ESECUTIVA
        //                TO IDSV8888-MODALITA-ESECUTIVA
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().getIdsi0011ModalitaEsecutiva());
        // COB_CODE: MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
        ws.getIdsv8888().getStrPerformanceDbg().setUserName(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011UserName());
        // COB_CODE: MOVE 'LCCS0234'                  TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0234");
        // COB_CODE: MOVE 'LCCS0234       '           TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("LCCS0234       ");
        // COB_CODE: SET  IDSV8888-INIZIO             TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LCCS0234'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0234");
        // COB_CODE: MOVE 'Serv estraz ogg. PTF'         TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv estraz ogg. PTF");
        // COB_CODE: SET  IDSV8888-INIZIO                TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: CALL LCCS0234  USING AREA-IDSV0001
        //                                WPOL-AREA-POLIZZA
        //                                WADE-AREA-ADESIONE
        //                                WGRZ-AREA-GARANZIA
        //                                WTGA-AREA-TRANCHE
        //                                S234-DATI-INPUT
        //                                S234-DATI-OUTPUT
        //           ON EXCEPTION
        //                     THRU EX-S0290
        //           END-CALL.
        try {
            lccs0234 = Lccs0234.getInstance();
            lccs0234.run(new Object[] {areaIdsv0001, wpolAreaPolizza, wadeAreaAdesione, wgrzAreaGaranzia, wtgaAreaTranche, ws.getS234DatiInput(), ws.getS234DatiOutput()});
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'IL REPERIMENTO ID PORTAFOGLIO'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("IL REPERIMENTO ID PORTAFOGLIO");
            // COB_CODE: MOVE 'S1100-CALL-LCCS0234'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1100-CALL-LCCS0234");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
        ws.getIdsv8888().getLivelloDebug().setStressTestDbg();
        // COB_CODE: SET  IDSV8888-SONDA-S9           TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setSondaS9();
        // COB_CODE: MOVE IDSI0011-MODALITA-ESECUTIVA
        //                TO IDSV8888-MODALITA-ESECUTIVA
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().getIdsi0011ModalitaEsecutiva());
        // COB_CODE: MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
        ws.getIdsv8888().getStrPerformanceDbg().setUserName(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011UserName());
        // COB_CODE: MOVE 'LCCS0234'                  TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0234");
        // COB_CODE: MOVE 'LCCS0234       '           TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("LCCS0234       ");
        // COB_CODE: SET  IDSV8888-FINE               TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LCCS0234'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0234");
        // COB_CODE: MOVE 'Serv estraz ogg. PTF'         TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv estraz ogg. PTF");
        // COB_CODE: SET  IDSV8888-FINE                  TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: S9000B-OPERAZIONI-FINALI<br>
	 * <pre> ----------------------------------------------------------------
	 *     OPERAZIONI FINALI
	 *  ----------------------------------------------------------------</pre>*/
    private void s9000bOperazioniFinali() {
        // COB_CODE: MOVE 'S9000B-OPERAZIONI-FINALI'
        //             TO WK-LABEL-ERR.
        ws.setWkLabelErr("S9000B-OPERAZIONI-FINALI");
        // COB_CODE: MOVE SPACES TO WCOM-TP-OGG-BLOCCO
        areaPassaggio.getLccc0261().getDatiBlocco().getTpOggBlocco().setTpOggBlocco("");
        // COB_CODE: MOVE SPACES TO WCOM-TP-OGG-MBS.
        areaPassaggio.getLccc0261().getDatiMbs().getWcomTpOggMbs().setWcomTpOggMbs("");
    }

    /**Original name: VAL-DCLGEN-PMO<br>
	 * <pre>  --------------------------------------------------------------*
	 *      CONTIENE STATEMENTS PER LA FASE DI EOC                     *
	 *   --------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVPMO5
	 *    ULTIMO AGG. 17 FEB 2015
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenPmo() {
        // COB_CODE: MOVE (SF)-TP-OGG(IX-TAB-PMO)
        //              TO PMO-TP-OGG
        ws.getParamMovi().setPmoTpOgg(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpOgg());
        // COB_CODE: IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-ID-MOVI-CHIU-NULL
        //           ELSE
        //              TO PMO-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdMoviChiu().getWpmoIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PMO)
            //           TO PMO-ID-MOVI-CHIU-NULL
            ws.getParamMovi().getPmoIdMoviChiu().setPmoIdMoviChiuNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdMoviChiu().getWpmoIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU(IX-TAB-PMO)
            //           TO PMO-ID-MOVI-CHIU
            ws.getParamMovi().getPmoIdMoviChiu().setPmoIdMoviChiu(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdMoviChiu().getWpmoIdMoviChiu());
        }
        // COB_CODE: IF (SF)-DT-INI-EFF(IX-TAB-PMO) NOT NUMERIC
        //              MOVE 0 TO PMO-DT-INI-EFF
        //           ELSE
        //              TO PMO-DT-INI-EFF
        //           END-IF
        if (!Functions.isNumber(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtIniEff())) {
            // COB_CODE: MOVE 0 TO PMO-DT-INI-EFF
            ws.getParamMovi().setPmoDtIniEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-INI-EFF(IX-TAB-PMO)
            //           TO PMO-DT-INI-EFF
            ws.getParamMovi().setPmoDtIniEff(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtIniEff());
        }
        // COB_CODE: IF (SF)-DT-END-EFF(IX-TAB-PMO) NOT NUMERIC
        //              MOVE 0 TO PMO-DT-END-EFF
        //           ELSE
        //              TO PMO-DT-END-EFF
        //           END-IF
        if (!Functions.isNumber(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtEndEff())) {
            // COB_CODE: MOVE 0 TO PMO-DT-END-EFF
            ws.getParamMovi().setPmoDtEndEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-END-EFF(IX-TAB-PMO)
            //           TO PMO-DT-END-EFF
            ws.getParamMovi().setPmoDtEndEff(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtEndEff());
        }
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA(IX-TAB-PMO)
        //              TO PMO-COD-COMP-ANIA
        ws.getParamMovi().setPmoCodCompAnia(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCodCompAnia());
        // COB_CODE: IF (SF)-TP-MOVI-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-TP-MOVI-NULL
        //           ELSE
        //              TO PMO-TP-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMoviNullFormatted())) {
            // COB_CODE: MOVE (SF)-TP-MOVI-NULL(IX-TAB-PMO)
            //           TO PMO-TP-MOVI-NULL
            ws.getParamMovi().getPmoTpMovi().setPmoTpMoviNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMoviNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-MOVI(IX-TAB-PMO)
            //           TO PMO-TP-MOVI
            ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi());
        }
        // COB_CODE: IF (SF)-FRQ-MOVI-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-FRQ-MOVI-NULL
        //           ELSE
        //              TO PMO-FRQ-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMoviNullFormatted())) {
            // COB_CODE: MOVE (SF)-FRQ-MOVI-NULL(IX-TAB-PMO)
            //           TO PMO-FRQ-MOVI-NULL
            ws.getParamMovi().getPmoFrqMovi().setPmoFrqMoviNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMoviNull());
        }
        else {
            // COB_CODE: MOVE (SF)-FRQ-MOVI(IX-TAB-PMO)
            //           TO PMO-FRQ-MOVI
            ws.getParamMovi().getPmoFrqMovi().setPmoFrqMovi(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMovi());
        }
        // COB_CODE: IF (SF)-DUR-AA-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-DUR-AA-NULL
        //           ELSE
        //              TO PMO-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurAa().getWpmoDurAaNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-AA-NULL(IX-TAB-PMO)
            //           TO PMO-DUR-AA-NULL
            ws.getParamMovi().getPmoDurAa().setPmoDurAaNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurAa().getWpmoDurAaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-AA(IX-TAB-PMO)
            //           TO PMO-DUR-AA
            ws.getParamMovi().getPmoDurAa().setPmoDurAa(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurAa().getWpmoDurAa());
        }
        // COB_CODE: IF (SF)-DUR-MM-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-DUR-MM-NULL
        //           ELSE
        //              TO PMO-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurMm().getWpmoDurMmNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-MM-NULL(IX-TAB-PMO)
            //           TO PMO-DUR-MM-NULL
            ws.getParamMovi().getPmoDurMm().setPmoDurMmNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurMm().getWpmoDurMmNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-MM(IX-TAB-PMO)
            //           TO PMO-DUR-MM
            ws.getParamMovi().getPmoDurMm().setPmoDurMm(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurMm().getWpmoDurMm());
        }
        // COB_CODE: IF (SF)-DUR-GG-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-DUR-GG-NULL
        //           ELSE
        //              TO PMO-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurGg().getWpmoDurGgNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-GG-NULL(IX-TAB-PMO)
            //           TO PMO-DUR-GG-NULL
            ws.getParamMovi().getPmoDurGg().setPmoDurGgNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurGg().getWpmoDurGgNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-GG(IX-TAB-PMO)
            //           TO PMO-DUR-GG
            ws.getParamMovi().getPmoDurGg().setPmoDurGg(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDurGg().getWpmoDurGg());
        }
        // COB_CODE: IF (SF)-DT-RICOR-PREC-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-DT-RICOR-PREC-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrecNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-RICOR-PREC-NULL(IX-TAB-PMO)
            //           TO PMO-DT-RICOR-PREC-NULL
            ws.getParamMovi().getPmoDtRicorPrec().setPmoDtRicorPrecNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrecNull());
        }
        else if (wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrec() == 0) {
            // COB_CODE: IF (SF)-DT-RICOR-PREC(IX-TAB-PMO) = ZERO
            //              TO PMO-DT-RICOR-PREC-NULL
            //           ELSE
            //            TO PMO-DT-RICOR-PREC
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PMO-DT-RICOR-PREC-NULL
            ws.getParamMovi().getPmoDtRicorPrec().setPmoDtRicorPrecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoDtRicorPrec.Len.PMO_DT_RICOR_PREC_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-RICOR-PREC(IX-TAB-PMO)
            //           TO PMO-DT-RICOR-PREC
            ws.getParamMovi().getPmoDtRicorPrec().setPmoDtRicorPrec(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrec());
        }
        // COB_CODE: IF (SF)-DT-RICOR-SUCC-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-DT-RICOR-SUCC-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSuccNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-RICOR-SUCC-NULL(IX-TAB-PMO)
            //           TO PMO-DT-RICOR-SUCC-NULL
            ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSuccNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSuccNull());
        }
        else if (wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc() == 0) {
            // COB_CODE: IF (SF)-DT-RICOR-SUCC(IX-TAB-PMO) = ZERO
            //              TO PMO-DT-RICOR-SUCC-NULL
            //           ELSE
            //            TO PMO-DT-RICOR-SUCC
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PMO-DT-RICOR-SUCC-NULL
            ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSuccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoDtRicorSucc.Len.PMO_DT_RICOR_SUCC_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-RICOR-SUCC(IX-TAB-PMO)
            //           TO PMO-DT-RICOR-SUCC
            ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSucc(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc());
        }
        // COB_CODE: IF (SF)-PC-INTR-FRAZ-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-PC-INTR-FRAZ-NULL
        //           ELSE
        //              TO PMO-PC-INTR-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcIntrFraz().getWpmoPcIntrFrazNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-INTR-FRAZ-NULL(IX-TAB-PMO)
            //           TO PMO-PC-INTR-FRAZ-NULL
            ws.getParamMovi().getPmoPcIntrFraz().setPmoPcIntrFrazNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcIntrFraz().getWpmoPcIntrFrazNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-INTR-FRAZ(IX-TAB-PMO)
            //           TO PMO-PC-INTR-FRAZ
            ws.getParamMovi().getPmoPcIntrFraz().setPmoPcIntrFraz(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcIntrFraz().getWpmoPcIntrFraz(), 6, 3));
        }
        // COB_CODE: IF (SF)-IMP-BNS-DA-SCO-TOT-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-IMP-BNS-DA-SCO-TOT-NULL
        //           ELSE
        //              TO PMO-IMP-BNS-DA-SCO-TOT
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().getWpmoImpBnsDaScoTotNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-BNS-DA-SCO-TOT-NULL(IX-TAB-PMO)
            //           TO PMO-IMP-BNS-DA-SCO-TOT-NULL
            ws.getParamMovi().getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTotNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().getWpmoImpBnsDaScoTotNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-BNS-DA-SCO-TOT(IX-TAB-PMO)
            //           TO PMO-IMP-BNS-DA-SCO-TOT
            ws.getParamMovi().getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTot(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().getWpmoImpBnsDaScoTot(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-BNS-DA-SCO-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-IMP-BNS-DA-SCO-NULL
        //           ELSE
        //              TO PMO-IMP-BNS-DA-SCO
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaSco().getWpmoImpBnsDaScoNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-BNS-DA-SCO-NULL(IX-TAB-PMO)
            //           TO PMO-IMP-BNS-DA-SCO-NULL
            ws.getParamMovi().getPmoImpBnsDaSco().setPmoImpBnsDaScoNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaSco().getWpmoImpBnsDaScoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-BNS-DA-SCO(IX-TAB-PMO)
            //           TO PMO-IMP-BNS-DA-SCO
            ws.getParamMovi().getPmoImpBnsDaSco().setPmoImpBnsDaSco(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpBnsDaSco().getWpmoImpBnsDaSco(), 15, 3));
        }
        // COB_CODE: IF (SF)-PC-ANTIC-BNS-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-PC-ANTIC-BNS-NULL
        //           ELSE
        //              TO PMO-PC-ANTIC-BNS
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcAnticBns().getWpmoPcAnticBnsNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-ANTIC-BNS-NULL(IX-TAB-PMO)
            //           TO PMO-PC-ANTIC-BNS-NULL
            ws.getParamMovi().getPmoPcAnticBns().setPmoPcAnticBnsNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcAnticBns().getWpmoPcAnticBnsNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-ANTIC-BNS(IX-TAB-PMO)
            //           TO PMO-PC-ANTIC-BNS
            ws.getParamMovi().getPmoPcAnticBns().setPmoPcAnticBns(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcAnticBns().getWpmoPcAnticBns(), 6, 3));
        }
        // COB_CODE: IF (SF)-TP-RINN-COLL-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-TP-RINN-COLL-NULL
        //           ELSE
        //              TO PMO-TP-RINN-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpRinnCollFormatted())) {
            // COB_CODE: MOVE (SF)-TP-RINN-COLL-NULL(IX-TAB-PMO)
            //           TO PMO-TP-RINN-COLL-NULL
            ws.getParamMovi().setPmoTpRinnColl(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpRinnColl());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-RINN-COLL(IX-TAB-PMO)
            //           TO PMO-TP-RINN-COLL
            ws.getParamMovi().setPmoTpRinnColl(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpRinnColl());
        }
        // COB_CODE: IF (SF)-TP-RIVAL-PRE-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-TP-RIVAL-PRE-NULL
        //           ELSE
        //              TO PMO-TP-RIVAL-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpRivalPreFormatted())) {
            // COB_CODE: MOVE (SF)-TP-RIVAL-PRE-NULL(IX-TAB-PMO)
            //           TO PMO-TP-RIVAL-PRE-NULL
            ws.getParamMovi().setPmoTpRivalPre(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpRivalPre());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-RIVAL-PRE(IX-TAB-PMO)
            //           TO PMO-TP-RIVAL-PRE
            ws.getParamMovi().setPmoTpRivalPre(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpRivalPre());
        }
        // COB_CODE: IF (SF)-TP-RIVAL-PRSTZ-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-TP-RIVAL-PRSTZ-NULL
        //           ELSE
        //              TO PMO-TP-RIVAL-PRSTZ
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpRivalPrstzFormatted())) {
            // COB_CODE: MOVE (SF)-TP-RIVAL-PRSTZ-NULL(IX-TAB-PMO)
            //           TO PMO-TP-RIVAL-PRSTZ-NULL
            ws.getParamMovi().setPmoTpRivalPrstz(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpRivalPrstz());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-RIVAL-PRSTZ(IX-TAB-PMO)
            //           TO PMO-TP-RIVAL-PRSTZ
            ws.getParamMovi().setPmoTpRivalPrstz(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpRivalPrstz());
        }
        // COB_CODE: IF (SF)-FL-EVID-RIVAL-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-FL-EVID-RIVAL-NULL
        //           ELSE
        //              TO PMO-FL-EVID-RIVAL
        //           END-IF
        if (Conditions.eq(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFlEvidRival(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-EVID-RIVAL-NULL(IX-TAB-PMO)
            //           TO PMO-FL-EVID-RIVAL-NULL
            ws.getParamMovi().setPmoFlEvidRival(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFlEvidRival());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-EVID-RIVAL(IX-TAB-PMO)
            //           TO PMO-FL-EVID-RIVAL
            ws.getParamMovi().setPmoFlEvidRival(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFlEvidRival());
        }
        // COB_CODE: IF (SF)-ULT-PC-PERD-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-ULT-PC-PERD-NULL
        //           ELSE
        //              TO PMO-ULT-PC-PERD
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoUltPcPerd().getWpmoUltPcPerdNullFormatted())) {
            // COB_CODE: MOVE (SF)-ULT-PC-PERD-NULL(IX-TAB-PMO)
            //           TO PMO-ULT-PC-PERD-NULL
            ws.getParamMovi().getPmoUltPcPerd().setPmoUltPcPerdNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoUltPcPerd().getWpmoUltPcPerdNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ULT-PC-PERD(IX-TAB-PMO)
            //           TO PMO-ULT-PC-PERD
            ws.getParamMovi().getPmoUltPcPerd().setPmoUltPcPerd(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoUltPcPerd().getWpmoUltPcPerd(), 6, 3));
        }
        // COB_CODE: IF (SF)-TOT-AA-GIA-PROR-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-TOT-AA-GIA-PROR-NULL
        //           ELSE
        //              TO PMO-TOT-AA-GIA-PROR
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTotAaGiaPror().getWpmoTotAaGiaProrNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-AA-GIA-PROR-NULL(IX-TAB-PMO)
            //           TO PMO-TOT-AA-GIA-PROR-NULL
            ws.getParamMovi().getPmoTotAaGiaPror().setPmoTotAaGiaProrNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTotAaGiaPror().getWpmoTotAaGiaProrNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-AA-GIA-PROR(IX-TAB-PMO)
            //           TO PMO-TOT-AA-GIA-PROR
            ws.getParamMovi().getPmoTotAaGiaPror().setPmoTotAaGiaPror(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTotAaGiaPror().getWpmoTotAaGiaPror());
        }
        // COB_CODE: IF (SF)-TP-OPZ-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-TP-OPZ-NULL
        //           ELSE
        //              TO PMO-TP-OPZ
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpOpzFormatted())) {
            // COB_CODE: MOVE (SF)-TP-OPZ-NULL(IX-TAB-PMO)
            //           TO PMO-TP-OPZ-NULL
            ws.getParamMovi().setPmoTpOpz(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpOpz());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-OPZ(IX-TAB-PMO)
            //           TO PMO-TP-OPZ
            ws.getParamMovi().setPmoTpOpz(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpOpz());
        }
        // COB_CODE: IF (SF)-AA-REN-CER-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-AA-REN-CER-NULL
        //           ELSE
        //              TO PMO-AA-REN-CER
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoAaRenCer().getWpmoAaRenCerNullFormatted())) {
            // COB_CODE: MOVE (SF)-AA-REN-CER-NULL(IX-TAB-PMO)
            //           TO PMO-AA-REN-CER-NULL
            ws.getParamMovi().getPmoAaRenCer().setPmoAaRenCerNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoAaRenCer().getWpmoAaRenCerNull());
        }
        else {
            // COB_CODE: MOVE (SF)-AA-REN-CER(IX-TAB-PMO)
            //           TO PMO-AA-REN-CER
            ws.getParamMovi().getPmoAaRenCer().setPmoAaRenCer(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoAaRenCer().getWpmoAaRenCer());
        }
        // COB_CODE: IF (SF)-PC-REVRSB-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-PC-REVRSB-NULL
        //           ELSE
        //              TO PMO-PC-REVRSB
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcRevrsb().getWpmoPcRevrsbNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-REVRSB-NULL(IX-TAB-PMO)
            //           TO PMO-PC-REVRSB-NULL
            ws.getParamMovi().getPmoPcRevrsb().setPmoPcRevrsbNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcRevrsb().getWpmoPcRevrsbNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-REVRSB(IX-TAB-PMO)
            //           TO PMO-PC-REVRSB
            ws.getParamMovi().getPmoPcRevrsb().setPmoPcRevrsb(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcRevrsb().getWpmoPcRevrsb(), 6, 3));
        }
        // COB_CODE: IF (SF)-IMP-RISC-PARZ-PRGT-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-IMP-RISC-PARZ-PRGT-NULL
        //           ELSE
        //              TO PMO-IMP-RISC-PARZ-PRGT
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().getWpmoImpRiscParzPrgtNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-RISC-PARZ-PRGT-NULL(IX-TAB-PMO)
            //           TO PMO-IMP-RISC-PARZ-PRGT-NULL
            ws.getParamMovi().getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgtNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().getWpmoImpRiscParzPrgtNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-RISC-PARZ-PRGT(IX-TAB-PMO)
            //           TO PMO-IMP-RISC-PARZ-PRGT
            ws.getParamMovi().getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgt(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().getWpmoImpRiscParzPrgt(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-LRD-DI-RAT-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-IMP-LRD-DI-RAT-NULL
        //           ELSE
        //              TO PMO-IMP-LRD-DI-RAT
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpLrdDiRat().getWpmoImpLrdDiRatNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-LRD-DI-RAT-NULL(IX-TAB-PMO)
            //           TO PMO-IMP-LRD-DI-RAT-NULL
            ws.getParamMovi().getPmoImpLrdDiRat().setPmoImpLrdDiRatNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpLrdDiRat().getWpmoImpLrdDiRatNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-LRD-DI-RAT(IX-TAB-PMO)
            //           TO PMO-IMP-LRD-DI-RAT
            ws.getParamMovi().getPmoImpLrdDiRat().setPmoImpLrdDiRat(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpLrdDiRat().getWpmoImpLrdDiRat(), 15, 3));
        }
        // COB_CODE: IF (SF)-IB-OGG-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-IB-OGG-NULL
        //           ELSE
        //              TO PMO-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIbOgg(), WpmoDati.Len.WPMO_IB_OGG)) {
            // COB_CODE: MOVE (SF)-IB-OGG-NULL(IX-TAB-PMO)
            //           TO PMO-IB-OGG-NULL
            ws.getParamMovi().setPmoIbOgg(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIbOgg());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-OGG(IX-TAB-PMO)
            //           TO PMO-IB-OGG
            ws.getParamMovi().setPmoIbOgg(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIbOgg());
        }
        // COB_CODE: IF (SF)-COS-ONER-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-COS-ONER-NULL
        //           ELSE
        //              TO PMO-COS-ONER
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCosOner().getWpmoCosOnerNullFormatted())) {
            // COB_CODE: MOVE (SF)-COS-ONER-NULL(IX-TAB-PMO)
            //           TO PMO-COS-ONER-NULL
            ws.getParamMovi().getPmoCosOner().setPmoCosOnerNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCosOner().getWpmoCosOnerNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COS-ONER(IX-TAB-PMO)
            //           TO PMO-COS-ONER
            ws.getParamMovi().getPmoCosOner().setPmoCosOner(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCosOner().getWpmoCosOner(), 15, 3));
        }
        // COB_CODE: IF (SF)-SPE-PC-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-SPE-PC-NULL
        //           ELSE
        //              TO PMO-SPE-PC
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoSpePc().getWpmoSpePcNullFormatted())) {
            // COB_CODE: MOVE (SF)-SPE-PC-NULL(IX-TAB-PMO)
            //           TO PMO-SPE-PC-NULL
            ws.getParamMovi().getPmoSpePc().setPmoSpePcNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoSpePc().getWpmoSpePcNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SPE-PC(IX-TAB-PMO)
            //           TO PMO-SPE-PC
            ws.getParamMovi().getPmoSpePc().setPmoSpePc(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoSpePc().getWpmoSpePc(), 6, 3));
        }
        // COB_CODE: IF (SF)-FL-ATTIV-GAR-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-FL-ATTIV-GAR-NULL
        //           ELSE
        //              TO PMO-FL-ATTIV-GAR
        //           END-IF
        if (Conditions.eq(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFlAttivGar(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-ATTIV-GAR-NULL(IX-TAB-PMO)
            //           TO PMO-FL-ATTIV-GAR-NULL
            ws.getParamMovi().setPmoFlAttivGar(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFlAttivGar());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-ATTIV-GAR(IX-TAB-PMO)
            //           TO PMO-FL-ATTIV-GAR
            ws.getParamMovi().setPmoFlAttivGar(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFlAttivGar());
        }
        // COB_CODE: IF (SF)-CAMBIO-VER-PROD-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-CAMBIO-VER-PROD-NULL
        //           ELSE
        //              TO PMO-CAMBIO-VER-PROD
        //           END-IF
        if (Conditions.eq(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCambioVerProd(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-CAMBIO-VER-PROD-NULL(IX-TAB-PMO)
            //           TO PMO-CAMBIO-VER-PROD-NULL
            ws.getParamMovi().setPmoCambioVerProd(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCambioVerProd());
        }
        else {
            // COB_CODE: MOVE (SF)-CAMBIO-VER-PROD(IX-TAB-PMO)
            //           TO PMO-CAMBIO-VER-PROD
            ws.getParamMovi().setPmoCambioVerProd(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCambioVerProd());
        }
        // COB_CODE: IF (SF)-MM-DIFF-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-MM-DIFF-NULL
        //           ELSE
        //              TO PMO-MM-DIFF
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoMmDiff().getWpmoMmDiffNullFormatted())) {
            // COB_CODE: MOVE (SF)-MM-DIFF-NULL(IX-TAB-PMO)
            //           TO PMO-MM-DIFF-NULL
            ws.getParamMovi().getPmoMmDiff().setPmoMmDiffNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoMmDiff().getWpmoMmDiffNull());
        }
        else {
            // COB_CODE: MOVE (SF)-MM-DIFF(IX-TAB-PMO)
            //           TO PMO-MM-DIFF
            ws.getParamMovi().getPmoMmDiff().setPmoMmDiff(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoMmDiff().getWpmoMmDiff());
        }
        // COB_CODE: IF (SF)-IMP-RAT-MANFEE-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-IMP-RAT-MANFEE-NULL
        //           ELSE
        //              TO PMO-IMP-RAT-MANFEE
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRatManfee().getWpmoImpRatManfeeNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-RAT-MANFEE-NULL(IX-TAB-PMO)
            //           TO PMO-IMP-RAT-MANFEE-NULL
            ws.getParamMovi().getPmoImpRatManfee().setPmoImpRatManfeeNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRatManfee().getWpmoImpRatManfeeNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-RAT-MANFEE(IX-TAB-PMO)
            //           TO PMO-IMP-RAT-MANFEE
            ws.getParamMovi().getPmoImpRatManfee().setPmoImpRatManfee(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoImpRatManfee().getWpmoImpRatManfee(), 15, 3));
        }
        // COB_CODE: IF (SF)-DT-ULT-EROG-MANFEE-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-DT-ULT-EROG-MANFEE-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtUltErogManfee().getWpmoDtUltErogManfeeNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-EROG-MANFEE-NULL(IX-TAB-PMO)
            //           TO PMO-DT-ULT-EROG-MANFEE-NULL
            ws.getParamMovi().getPmoDtUltErogManfee().setPmoDtUltErogManfeeNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtUltErogManfee().getWpmoDtUltErogManfeeNull());
        }
        else if (wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtUltErogManfee().getWpmoDtUltErogManfee() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-EROG-MANFEE(IX-TAB-PMO) = ZERO
            //              TO PMO-DT-ULT-EROG-MANFEE-NULL
            //           ELSE
            //            TO PMO-DT-ULT-EROG-MANFEE
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PMO-DT-ULT-EROG-MANFEE-NULL
            ws.getParamMovi().getPmoDtUltErogManfee().setPmoDtUltErogManfeeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoDtUltErogManfee.Len.PMO_DT_ULT_EROG_MANFEE_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-EROG-MANFEE(IX-TAB-PMO)
            //           TO PMO-DT-ULT-EROG-MANFEE
            ws.getParamMovi().getPmoDtUltErogManfee().setPmoDtUltErogManfee(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDtUltErogManfee().getWpmoDtUltErogManfee());
        }
        // COB_CODE: IF (SF)-TP-OGG-RIVAL-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-TP-OGG-RIVAL-NULL
        //           ELSE
        //              TO PMO-TP-OGG-RIVAL
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpOggRivalFormatted())) {
            // COB_CODE: MOVE (SF)-TP-OGG-RIVAL-NULL(IX-TAB-PMO)
            //           TO PMO-TP-OGG-RIVAL-NULL
            ws.getParamMovi().setPmoTpOggRival(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpOggRival());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-OGG-RIVAL(IX-TAB-PMO)
            //           TO PMO-TP-OGG-RIVAL
            ws.getParamMovi().setPmoTpOggRival(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpOggRival());
        }
        // COB_CODE: IF (SF)-SOM-ASSTA-GARAC-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-SOM-ASSTA-GARAC-NULL
        //           ELSE
        //              TO PMO-SOM-ASSTA-GARAC
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoSomAsstaGarac().getWpmoSomAsstaGaracNullFormatted())) {
            // COB_CODE: MOVE (SF)-SOM-ASSTA-GARAC-NULL(IX-TAB-PMO)
            //           TO PMO-SOM-ASSTA-GARAC-NULL
            ws.getParamMovi().getPmoSomAsstaGarac().setPmoSomAsstaGaracNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoSomAsstaGarac().getWpmoSomAsstaGaracNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SOM-ASSTA-GARAC(IX-TAB-PMO)
            //           TO PMO-SOM-ASSTA-GARAC
            ws.getParamMovi().getPmoSomAsstaGarac().setPmoSomAsstaGarac(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoSomAsstaGarac().getWpmoSomAsstaGarac(), 15, 3));
        }
        // COB_CODE: IF (SF)-PC-APPLZ-OPZ-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-PC-APPLZ-OPZ-NULL
        //           ELSE
        //              TO PMO-PC-APPLZ-OPZ
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcApplzOpz().getWpmoPcApplzOpzNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-APPLZ-OPZ-NULL(IX-TAB-PMO)
            //           TO PMO-PC-APPLZ-OPZ-NULL
            ws.getParamMovi().getPmoPcApplzOpz().setPmoPcApplzOpzNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcApplzOpz().getWpmoPcApplzOpzNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-APPLZ-OPZ(IX-TAB-PMO)
            //           TO PMO-PC-APPLZ-OPZ
            ws.getParamMovi().getPmoPcApplzOpz().setPmoPcApplzOpz(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcApplzOpz().getWpmoPcApplzOpz(), 6, 3));
        }
        // COB_CODE: MOVE (SF)-TP-FRM-ASSVA(IX-TAB-PMO)
        //              TO PMO-TP-FRM-ASSVA
        ws.getParamMovi().setPmoTpFrmAssva(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpFrmAssva());
        // COB_CODE: IF (SF)-DS-RIGA(IX-TAB-PMO) NOT NUMERIC
        //              MOVE 0 TO PMO-DS-RIGA
        //           ELSE
        //              TO PMO-DS-RIGA
        //           END-IF
        if (!Functions.isNumber(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDsRiga())) {
            // COB_CODE: MOVE 0 TO PMO-DS-RIGA
            ws.getParamMovi().setPmoDsRiga(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-RIGA(IX-TAB-PMO)
            //           TO PMO-DS-RIGA
            ws.getParamMovi().setPmoDsRiga(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDsRiga());
        }
        // COB_CODE: MOVE (SF)-DS-OPER-SQL(IX-TAB-PMO)
        //              TO PMO-DS-OPER-SQL
        ws.getParamMovi().setPmoDsOperSql(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDsOperSql());
        // COB_CODE: IF (SF)-DS-VER(IX-TAB-PMO) NOT NUMERIC
        //              MOVE 0 TO PMO-DS-VER
        //           ELSE
        //              TO PMO-DS-VER
        //           END-IF
        if (!Functions.isNumber(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDsVer())) {
            // COB_CODE: MOVE 0 TO PMO-DS-VER
            ws.getParamMovi().setPmoDsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER(IX-TAB-PMO)
            //           TO PMO-DS-VER
            ws.getParamMovi().setPmoDsVer(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-INI-CPTZ(IX-TAB-PMO) NOT NUMERIC
        //              MOVE 0 TO PMO-DS-TS-INI-CPTZ
        //           ELSE
        //              TO PMO-DS-TS-INI-CPTZ
        //           END-IF
        if (!Functions.isNumber(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDsTsIniCptz())) {
            // COB_CODE: MOVE 0 TO PMO-DS-TS-INI-CPTZ
            ws.getParamMovi().setPmoDsTsIniCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-PMO)
            //           TO PMO-DS-TS-INI-CPTZ
            ws.getParamMovi().setPmoDsTsIniCptz(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDsTsIniCptz());
        }
        // COB_CODE: IF (SF)-DS-TS-END-CPTZ(IX-TAB-PMO) NOT NUMERIC
        //              MOVE 0 TO PMO-DS-TS-END-CPTZ
        //           ELSE
        //              TO PMO-DS-TS-END-CPTZ
        //           END-IF
        if (!Functions.isNumber(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDsTsEndCptz())) {
            // COB_CODE: MOVE 0 TO PMO-DS-TS-END-CPTZ
            ws.getParamMovi().setPmoDsTsEndCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-PMO)
            //           TO PMO-DS-TS-END-CPTZ
            ws.getParamMovi().setPmoDsTsEndCptz(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDsTsEndCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE(IX-TAB-PMO)
        //              TO PMO-DS-UTENTE
        ws.getParamMovi().setPmoDsUtente(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB(IX-TAB-PMO)
        //              TO PMO-DS-STATO-ELAB
        ws.getParamMovi().setPmoDsStatoElab(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoDsStatoElab());
        // COB_CODE: IF (SF)-TP-ESTR-CNT-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-TP-ESTR-CNT-NULL
        //           ELSE
        //              TO PMO-TP-ESTR-CNT
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpEstrCntFormatted())) {
            // COB_CODE: MOVE (SF)-TP-ESTR-CNT-NULL(IX-TAB-PMO)
            //           TO PMO-TP-ESTR-CNT-NULL
            ws.getParamMovi().setPmoTpEstrCnt(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpEstrCnt());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-ESTR-CNT(IX-TAB-PMO)
            //           TO PMO-TP-ESTR-CNT
            ws.getParamMovi().setPmoTpEstrCnt(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpEstrCnt());
        }
        // COB_CODE: IF (SF)-COD-RAMO-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-COD-RAMO-NULL
        //           ELSE
        //              TO PMO-COD-RAMO
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCodRamoFormatted())) {
            // COB_CODE: MOVE (SF)-COD-RAMO-NULL(IX-TAB-PMO)
            //           TO PMO-COD-RAMO-NULL
            ws.getParamMovi().setPmoCodRamo(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCodRamo());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-RAMO(IX-TAB-PMO)
            //           TO PMO-COD-RAMO
            ws.getParamMovi().setPmoCodRamo(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCodRamo());
        }
        // COB_CODE: IF (SF)-GEN-DA-SIN-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-GEN-DA-SIN-NULL
        //           ELSE
        //              TO PMO-GEN-DA-SIN
        //           END-IF
        if (Conditions.eq(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoGenDaSin(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-GEN-DA-SIN-NULL(IX-TAB-PMO)
            //           TO PMO-GEN-DA-SIN-NULL
            ws.getParamMovi().setPmoGenDaSin(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoGenDaSin());
        }
        else {
            // COB_CODE: MOVE (SF)-GEN-DA-SIN(IX-TAB-PMO)
            //           TO PMO-GEN-DA-SIN
            ws.getParamMovi().setPmoGenDaSin(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoGenDaSin());
        }
        // COB_CODE: IF (SF)-COD-TARI-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-COD-TARI-NULL
        //           ELSE
        //              TO PMO-COD-TARI
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCodTariFormatted())) {
            // COB_CODE: MOVE (SF)-COD-TARI-NULL(IX-TAB-PMO)
            //           TO PMO-COD-TARI-NULL
            ws.getParamMovi().setPmoCodTari(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCodTari());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-TARI(IX-TAB-PMO)
            //           TO PMO-COD-TARI
            ws.getParamMovi().setPmoCodTari(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoCodTari());
        }
        // COB_CODE: IF (SF)-NUM-RAT-PAG-PRE-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-NUM-RAT-PAG-PRE-NULL
        //           ELSE
        //              TO PMO-NUM-RAT-PAG-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoNumRatPagPre().getWpmoNumRatPagPreNullFormatted())) {
            // COB_CODE: MOVE (SF)-NUM-RAT-PAG-PRE-NULL(IX-TAB-PMO)
            //           TO PMO-NUM-RAT-PAG-PRE-NULL
            ws.getParamMovi().getPmoNumRatPagPre().setPmoNumRatPagPreNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoNumRatPagPre().getWpmoNumRatPagPreNull());
        }
        else {
            // COB_CODE: MOVE (SF)-NUM-RAT-PAG-PRE(IX-TAB-PMO)
            //           TO PMO-NUM-RAT-PAG-PRE
            ws.getParamMovi().getPmoNumRatPagPre().setPmoNumRatPagPre(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoNumRatPagPre().getWpmoNumRatPagPre());
        }
        // COB_CODE: IF (SF)-PC-SERV-VAL-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-PC-SERV-VAL-NULL
        //           ELSE
        //              TO PMO-PC-SERV-VAL
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcServVal().getWpmoPcServValNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-SERV-VAL-NULL(IX-TAB-PMO)
            //           TO PMO-PC-SERV-VAL-NULL
            ws.getParamMovi().getPmoPcServVal().setPmoPcServValNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcServVal().getWpmoPcServValNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-SERV-VAL(IX-TAB-PMO)
            //           TO PMO-PC-SERV-VAL
            ws.getParamMovi().getPmoPcServVal().setPmoPcServVal(Trunc.toDecimal(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoPcServVal().getWpmoPcServVal(), 6, 3));
        }
        // COB_CODE: IF (SF)-ETA-AA-SOGL-BNFICR-NULL(IX-TAB-PMO) = HIGH-VALUES
        //              TO PMO-ETA-AA-SOGL-BNFICR-NULL
        //           ELSE
        //              TO PMO-ETA-AA-SOGL-BNFICR
        //           END-IF.
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().getWpmoEtaAaSoglBnficrNullFormatted())) {
            // COB_CODE: MOVE (SF)-ETA-AA-SOGL-BNFICR-NULL(IX-TAB-PMO)
            //           TO PMO-ETA-AA-SOGL-BNFICR-NULL
            ws.getParamMovi().getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficrNull(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().getWpmoEtaAaSoglBnficrNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ETA-AA-SOGL-BNFICR(IX-TAB-PMO)
            //           TO PMO-ETA-AA-SOGL-BNFICR
            ws.getParamMovi().getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficr(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().getWpmoEtaAaSoglBnficr());
        }
    }

    /**Original name: AGGIORNA-PARAM-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCVPMO6
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO PARAMETRO MOVIMENTO
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCVPMO5 (VALORIZZAZIONE DCLGEN)
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN STRATEGIA DI INVESTIMENTO (LCCVPMO1)
	 *   - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI</pre>*/
    private void aggiornaParamMovi() {
        // COB_CODE: INITIALIZE PARAM-MOVI
        initParamMovi();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'PARAM-MOVI'                 TO WK-TABELLA
        ws.setWkTabella("PARAM-MOVI");
        // COB_CODE:      IF NOT WPMO-ST-INV(IX-TAB-PMO)
        //                   AND NOT WPMO-ST-CON(IX-TAB-PMO)
        //                   AND WPMO-ELE-PARAM-MOV-MAX NOT = 0
        //           *-->    CONTROLLO DELLO STATUS
        //                   END-IF
        //                END-IF.
        if (!wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getStatus().isInv() && !wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getStatus().isWpmoStCon() && wpmoAreaParamMovi.getEleParamMovMax() != 0) {
            //-->    CONTROLLO DELLO STATUS
            //-->        INSERIMENTO
            // COB_CODE:         EVALUATE TRUE
            //           *-->        INSERIMENTO
            //                       WHEN WPMO-ST-ADD(IX-TAB-PMO)
            //           *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
            //                          SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
            //           *-->        MODIFICA
            //                       WHEN WPMO-ST-MOD(IX-TAB-PMO)
            //                            SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
            //           *-->        CANCELLAZIONE
            //                       WHEN WPMO-ST-DEL(IX-TAB-PMO)
            //                            SET  IDSI0011-DELETE-LOGICA    TO TRUE
            //                   END-EVALUATE
            switch (wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getStatus().getStatus()) {

                case WpolStatus.ADD://-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                    // COB_CODE: PERFORM ESTR-SEQUENCE
                    //              THRU ESTR-SEQUENCE-EX
                    estrSequence();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //              END-IF
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: MOVE S090-SEQ-TABELLA TO WPMO-ID-PTF(IX-TAB-PMO)
                        //                                    PMO-ID-PARAM-MOVI
                        wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
                        ws.getParamMovi().setPmoIdParamMovi(ws.getAreaIoLccs0090().getSeqTabella());
                        //-->              PREPARAZIONE ED ESTRAZIONE OGGETTO PTF
                        // COB_CODE: PERFORM PREPARA-AREA-LCCS0234-PMO
                        //              THRU PREPARA-AREA-LCCS0234-PMO-EX
                        preparaAreaLccs0234Pmo();
                        // COB_CODE: PERFORM CALL-LCCS0234
                        //              THRU CALL-LCCS0234-EX
                        callLccs0234();
                        // COB_CODE: IF IDSV0001-ESITO-OK
                        //              END-EVALUATE
                        //           END-IF
                        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                            // COB_CODE: MOVE S234-ID-OGG-PTF-EOC   TO PMO-ID-OGG
                            ws.getParamMovi().setPmoIdOgg(ws.getS234DatiOutput().getIdOggPtfEoc());
                            // COB_CODE: MOVE S234-IB-OGG-PTF-EOC
                            //             TO WPMO-IB-OGG(IX-TAB-PMO)
                            wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().setWpmoIbOgg(ws.getS234DatiOutput().getIbOggPtfEoc());
                            // COB_CODE: MOVE WMOV-ID-PTF           TO PMO-ID-MOVI-CRZ
                            ws.getParamMovi().setPmoIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
                            // COB_CODE: EVALUATE WPMO-TP-OGG(IX-TAB-PMO)
                            //               WHEN 'PO'
                            //                  MOVE HIGH-VALUE  TO PMO-ID-ADES-NULL
                            //               WHEN 'AD'
                            //               WHEN 'GA'
                            //               WHEN 'TG'
                            //                  MOVE S234-ID-ADES-PTF TO PMO-ID-ADES
                            //           END-EVALUATE
                            switch (wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpOgg()) {

                                case "PO":// COB_CODE: MOVE S234-ID-POLI-PTF TO PMO-ID-POLI
                                    ws.getParamMovi().setPmoIdPoli(ws.getS234DatiOutput().getIdPoliPtf());
                                    // COB_CODE: MOVE HIGH-VALUE  TO PMO-ID-ADES-NULL
                                    ws.getParamMovi().getPmoIdAdes().setPmoIdAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoIdAdes.Len.PMO_ID_ADES_NULL));
                                    break;

                                case "AD":
                                case "GA":
                                case "TG":// COB_CODE: MOVE S234-ID-POLI-PTF TO PMO-ID-POLI
                                    ws.getParamMovi().setPmoIdPoli(ws.getS234DatiOutput().getIdPoliPtf());
                                    // COB_CODE: MOVE S234-ID-ADES-PTF TO PMO-ID-ADES
                                    ws.getParamMovi().getPmoIdAdes().setPmoIdAdes(ws.getS234DatiOutput().getIdAdesPtf());
                                    break;

                                default:break;
                            }
                        }
                    }
                    //-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    //-->        MODIFICA
                    break;

                case WpolStatus.MOD:// COB_CODE: MOVE WPMO-ID-PTF(IX-TAB-PMO)
                    //             TO PMO-ID-PARAM-MOVI
                    ws.getParamMovi().setPmoIdParamMovi(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getIdPtf());
                    // COB_CODE: MOVE WPMO-ID-OGG(IX-TAB-PMO)
                    //             TO PMO-ID-OGG
                    ws.getParamMovi().setPmoIdOgg(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdOgg());
                    // COB_CODE: MOVE WMOV-ID-PTF
                    //             TO PMO-ID-MOVI-CRZ
                    ws.getParamMovi().setPmoIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
                    // COB_CODE: MOVE WPMO-ID-POLI(IX-TAB-PMO)
                    //             TO PMO-ID-POLI
                    ws.getParamMovi().setPmoIdPoli(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdPoli());
                    // COB_CODE: IF WPMO-ID-ADES-NULL(IX-TAB-PMO) = HIGH-VALUES
                    //                TO PMO-ID-ADES-NULL
                    //           ELSE
                    //                TO PMO-ID-ADES
                    //           END-IF
                    if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdAdes().getWpmoIdAdesNullFormatted())) {
                        // COB_CODE: MOVE HIGH-VALUES
                        //             TO PMO-ID-ADES-NULL
                        ws.getParamMovi().getPmoIdAdes().setPmoIdAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoIdAdes.Len.PMO_ID_ADES_NULL));
                    }
                    else {
                        // COB_CODE: MOVE WPMO-ID-ADES(IX-TAB-PMO)
                        //             TO PMO-ID-ADES
                        ws.getParamMovi().getPmoIdAdes().setPmoIdAdes(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdAdes().getWpmoIdAdes());
                    }
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    //-->        CANCELLAZIONE
                    break;

                case WpolStatus.DEL:// COB_CODE: MOVE WPMO-ID-PTF(IX-TAB-PMO)
                    //             TO PMO-ID-PARAM-MOVI
                    ws.getParamMovi().setPmoIdParamMovi(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getIdPtf());
                    // COB_CODE: MOVE WPMO-ID-OGG(IX-TAB-PMO)
                    //             TO PMO-ID-OGG
                    ws.getParamMovi().setPmoIdOgg(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdOgg());
                    // COB_CODE: MOVE WMOV-ID-PTF
                    //             TO PMO-ID-MOVI-CRZ
                    //                PMO-ID-MOVI-CHIU
                    ws.getParamMovi().setPmoIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
                    ws.getParamMovi().getPmoIdMoviChiu().setPmoIdMoviChiu(wmovAreaMovimento.getLccvmov1().getIdPtf());
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-DELETE-LOGICA    TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011DeleteLogica();
                    break;

                default:break;
            }
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       VALORIZZA DCLGEN ADESIONE
            //                         THRU AGGIORNA-TABELLA-EX
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->       VALORIZZA DCLGEN ADESIONE
                // COB_CODE: PERFORM VAL-DCLGEN-PMO
                //              THRU VAL-DCLGEN-PMO-EX
                valDclgenPmo();
                //-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                // COB_CODE: PERFORM VALORIZZA-AREA-DSH-PMO
                //              THRU VALORIZZA-AREA-DSH-PMO-EX
                valorizzaAreaDshPmo();
                //-->       CALL DISPATCHER PER AGGIORNAMENTO
                // COB_CODE: PERFORM AGGIORNA-TABELLA
                //              THRU AGGIORNA-TABELLA-EX
                aggiornaTabella();
            }
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> DCLGEN TABELLA</pre>*/
    private void valorizzaAreaDshPmo() {
        // COB_CODE: MOVE PARAM-MOVI              TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getParamMovi().getParamMoviFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-ID             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET  IDSI0011-TRATT-DEFAULT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(wmovAreaMovimento.getLccvmov1().getDati().getWmovDtEff());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: PREPARA-AREA-LCCS0234-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER CALL LCCS0234
	 * ----------------------------------------------------------------*</pre>*/
    private void preparaAreaLccs0234Pmo() {
        // COB_CODE: MOVE WPMO-ID-OGG(IX-TAB-PMO)      TO S234-ID-OGG-EOC.
        ws.getS234DatiInput().setIdOggEoc(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoIdOgg());
        // COB_CODE: MOVE WPMO-TP-OGG(IX-TAB-PMO)      TO S234-TIPO-OGG-EOC.
        ws.getS234DatiInput().setTipoOggEoc(wpmoAreaParamMovi.getTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpOgg());
    }

    /**Original name: AGGIORNA-TABELLA<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY VALORIZZAZIONE DCLGEN
	 * ----------------------------------------------------------------*
	 *      COPY LCCVPMO5.
	 * ----------------------------------------------------------------*
	 *     COPY      ..... LCCP0001
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void aggiornaTabella() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            // COB_CODE:         EVALUATE TRUE
            //           *-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            //                     WHEN IDSO0011-SUCCESSFUL-SQL
            //                        CONTINUE
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                     WHEN OTHER
            //                           THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    break;

                default:// COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'AGGIORNA-TABELLA'
                    //                                 TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
                    // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA            ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE   INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'AGGIORNA-TABELLA'
            //                                      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA            ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE        INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: ESTR-SEQUENCE<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCP0002
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*</pre>*/
    private void estrSequence() {
        Lccs0090 lccs0090 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA TO S090-NOME-TABELLA.
        ws.getAreaIoLccs0090().setNomeTabella(ws.getWkTabella());
        // COB_CODE: CALL LCCS0090 USING AREA-IO-LCCS0090
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0090 = Lccs0090.getInstance();
            lccs0090.run(ws.getAreaIoLccs0090());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO ESTRAZIONE SEQUENCE'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO ESTRAZIONE SEQUENCE");
            // COB_CODE: MOVE 'ESTR-SEQUENCE'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("ESTR-SEQUENCE");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-EVALUATE
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         EVALUATE S090-RETURN-CODE
            //                       WHEN '00'
            //                            CONTINUE
            //                       WHEN 'S1'
            //           *-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
            //                             THRU EX-S0300
            //                       WHEN 'D3'
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getAreaIoLccs0090().getReturnCode().getReturnCode()) {

                case "00":// COB_CODE: CONTINUE
                //continue
                    break;

                case "S1"://-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005015'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case "D3"://-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005016'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default:break;
            }
        }
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre> ---------------------------------------------------------------*
	 *   ROUTINES GESTIONE ERRORI                                      *
	 *  ---------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre> ---------------------------------------------------------------*
	 *     ROUTINES DISPATCHER                                         *
	 *  ---------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: ESEGUI-DISPLAY<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI GESTIONE DISPLAY
	 * ----------------------------------------------------------------*</pre>*/
    private void eseguiDisplay() {
        Idss8880 idss8880 = null;
        GenericParam idsv8888DisplayAddress = null;
        // COB_CODE: IF IDSV0001-ANY-TUNING-DBG AND
        //              IDSV8888-ANY-TUNING-DBG
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyTuningDbg() && ws.getIdsv8888().getLivelloDebug().isAnyTuningDbg()) {
            // COB_CODE: IF IDSV0001-TOT-TUNING-DBG
            //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
            //           ELSE
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLivelloDebug().isTotTuningDbg()) {
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isArchBatchDbg()) {
                    // COB_CODE: IF IDSV8888-ARCH-BATCH-DBG
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           ELSE
                    //              MOVE 'ERRO'   TO IDSV8888-FASE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                else {
                    // COB_CODE: MOVE 'ERRO'   TO IDSV8888-FASE
                    ws.getIdsv8888().getStrPerformanceDbg().setFase("ERRO");
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //                TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
            else if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() == areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: IF IDSV8888-LIVELLO-DEBUG =
                //              IDSV0001-LIVELLO-DEBUG
                //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                //           END-IF
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else {
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
        }
        else if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyApplDbg() && ws.getIdsv8888().getLivelloDebug().isAnyApplDbg()) {
            // COB_CODE: IF IDSV0001-ANY-APPL-DBG AND
            //              IDSV8888-ANY-APPL-DBG
            //               END-IF
            //           END-IF
            // COB_CODE: IF IDSV8888-LIVELLO-DEBUG <=
            //              IDSV0001-LIVELLO-DEBUG
            //              MOVE SPACES TO IDSV8888-AREA-DISPLAY
            //           END-IF
            if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() <= areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getAreaDisplay()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: MOVE SPACES TO IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().getAreaDisplay().setAreaDisplay("");
            }
        }
        // COB_CODE: SET IDSV8888-NO-DEBUG              TO TRUE.
        ws.getIdsv8888().getLivelloDebug().setNoDebug();
    }

    public void initWrreAreaRapRete() {
        ws.getWrreAreaRapRete().setEleRappReteMax(((short)0));
        for (int idx0 = 1; idx0 <= WrreAreaRappRete.TAB_RRE_MAXOCCURS; idx0++) {
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().setIdPtf(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreIdRappRete(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().getWrreIdOgg().setWrreIdOgg(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreTpOgg("");
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreIdMoviCrz(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().getWrreIdMoviChiu().setWrreIdMoviChiu(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreDtIniEff(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreDtEndEff(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreCodCompAnia(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreTpRete("");
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().getWrreTpAcqsCntrt().setWrreTpAcqsCntrt(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().getWrreCodAcqsCntrt().setWrreCodAcqsCntrt(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().getWrreCodPntReteIni().setWrreCodPntReteIni(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().getWrreCodPntReteEnd().setWrreCodPntReteEnd(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreFlPntRete1rio(Types.SPACE_CHAR);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().getWrreCodCan().setWrreCodCan(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreDsRiga(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreDsOperSql(Types.SPACE_CHAR);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreDsVer(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreDsTsIniCptz(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreDsTsEndCptz(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreDsUtente("");
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreDsStatoElab(Types.SPACE_CHAR);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().getWrreCodPntReteIniC().setWrreCodPntReteIniC(0);
            ws.getWrreAreaRapRete().getTabRre(idx0).getLccvrre1().getDati().setWrreMatrOprt("");
        }
    }

    public void initWnotAreaNoteOgg() {
        ws.getWnotAreaNoteOgg().setWnotEleNoteOggMax(((short)0));
        ws.getWnotAreaNoteOgg().getLccvnot1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getWnotAreaNoteOgg().getLccvnot1().setIdPtf(0);
        ws.getWnotAreaNoteOgg().getLccvnot1().getDati().setIdNoteOgg(0);
        ws.getWnotAreaNoteOgg().getLccvnot1().getDati().setCodCompAnia(0);
        ws.getWnotAreaNoteOgg().getLccvnot1().getDati().setIdOgg(0);
        ws.getWnotAreaNoteOgg().getLccvnot1().getDati().setTpOgg("");
        ws.getWnotAreaNoteOgg().getLccvnot1().getDati().setNotaOgg("");
        ws.getWnotAreaNoteOgg().getLccvnot1().getDati().setDsOperSql(Types.SPACE_CHAR);
        ws.getWnotAreaNoteOgg().getLccvnot1().getDati().setDsVer(0);
        ws.getWnotAreaNoteOgg().getLccvnot1().getDati().setDsTsCptz(0);
        ws.getWnotAreaNoteOgg().getLccvnot1().getDati().setDsUtente("");
        ws.getWnotAreaNoteOgg().getLccvnot1().getDati().setDsStatoElab(Types.SPACE_CHAR);
    }

    public void initAreaIoLccs0024() {
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024IdOggPrim(0);
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024TpOggPrim("");
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024IdOggetto(0);
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024TpOggetto("");
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024CodBlocco("");
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024IdRich(0);
        ws.getAreaIoLccs0024().getDatiInput().setLccc0024EleMovSosMax(((short)0));
        for (int idx0 = 1; idx0 <= Lccc0024DatiInput.LCCC0024_DATI_MOV_SOSPESI_MAXOCCURS; idx0++) {
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(idx0).setLccc0024IdOggSosp(0);
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(idx0).setLccc0024TpOggSosp("");
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(idx0).setLccc0024IdMoviSosp(0);
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(idx0).getLccc0024TpMoviSosp().setLccc0024TpMoviSosp(0);
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(idx0).setLccc0024TpFrmAssva("");
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(idx0).getLccc0024IdBatch().setLccc0024IdBatch(0);
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(idx0).getLccc0024IdJob().setLccc0024IdJob(0);
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(idx0).setLccc0024StepElab(Types.SPACE_CHAR);
            ws.getAreaIoLccs0024().getDatiInput().getLccc0024DatiMovSospesi(idx0).setLccc0024DInputMoviSosp("");
        }
        ws.getAreaIoLccs0024().setIdOggBlocco(0);
        ws.getAreaIoLccs0024().getVerificaBlocco().setVerificaBlocco(Types.SPACE_CHAR);
    }

    public void initParamMovi() {
        ws.getParamMovi().setPmoIdParamMovi(0);
        ws.getParamMovi().setPmoIdOgg(0);
        ws.getParamMovi().setPmoTpOgg("");
        ws.getParamMovi().setPmoIdMoviCrz(0);
        ws.getParamMovi().getPmoIdMoviChiu().setPmoIdMoviChiu(0);
        ws.getParamMovi().setPmoDtIniEff(0);
        ws.getParamMovi().setPmoDtEndEff(0);
        ws.getParamMovi().setPmoCodCompAnia(0);
        ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(0);
        ws.getParamMovi().getPmoFrqMovi().setPmoFrqMovi(0);
        ws.getParamMovi().getPmoDurAa().setPmoDurAa(0);
        ws.getParamMovi().getPmoDurMm().setPmoDurMm(0);
        ws.getParamMovi().getPmoDurGg().setPmoDurGg(0);
        ws.getParamMovi().getPmoDtRicorPrec().setPmoDtRicorPrec(0);
        ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSucc(0);
        ws.getParamMovi().getPmoPcIntrFraz().setPmoPcIntrFraz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpBnsDaSco().setPmoImpBnsDaSco(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcAnticBns().setPmoPcAnticBns(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoTpRinnColl("");
        ws.getParamMovi().setPmoTpRivalPre("");
        ws.getParamMovi().setPmoTpRivalPrstz("");
        ws.getParamMovi().setPmoFlEvidRival(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoUltPcPerd().setPmoUltPcPerd(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoTotAaGiaPror().setPmoTotAaGiaPror(0);
        ws.getParamMovi().setPmoTpOpz("");
        ws.getParamMovi().getPmoAaRenCer().setPmoAaRenCer(0);
        ws.getParamMovi().getPmoPcRevrsb().setPmoPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpLrdDiRat().setPmoImpLrdDiRat(new AfDecimal(0, 15, 3));
        ws.getParamMovi().setPmoIbOgg("");
        ws.getParamMovi().getPmoCosOner().setPmoCosOner(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoSpePc().setPmoSpePc(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoFlAttivGar(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCambioVerProd(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoMmDiff().setPmoMmDiff(((short)0));
        ws.getParamMovi().getPmoImpRatManfee().setPmoImpRatManfee(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoDtUltErogManfee().setPmoDtUltErogManfee(0);
        ws.getParamMovi().setPmoTpOggRival("");
        ws.getParamMovi().getPmoSomAsstaGarac().setPmoSomAsstaGarac(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcApplzOpz().setPmoPcApplzOpz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoIdAdes().setPmoIdAdes(0);
        ws.getParamMovi().setPmoIdPoli(0);
        ws.getParamMovi().setPmoTpFrmAssva("");
        ws.getParamMovi().setPmoDsRiga(0);
        ws.getParamMovi().setPmoDsOperSql(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoDsVer(0);
        ws.getParamMovi().setPmoDsTsIniCptz(0);
        ws.getParamMovi().setPmoDsTsEndCptz(0);
        ws.getParamMovi().setPmoDsUtente("");
        ws.getParamMovi().setPmoDsStatoElab(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoTpEstrCnt("");
        ws.getParamMovi().setPmoCodRamo("");
        ws.getParamMovi().setPmoGenDaSin(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCodTari("");
        ws.getParamMovi().getPmoNumRatPagPre().setPmoNumRatPagPre(0);
        ws.getParamMovi().getPmoPcServVal().setPmoPcServVal(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficr(((short)0));
    }

    public void initStrPerformanceDbg() {
        ws.getIdsv8888().getStrPerformanceDbg().setFase("");
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setTimestampFld("");
        ws.getIdsv8888().getStrPerformanceDbg().setStato("");
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getIdsv8888().getStrPerformanceDbg().setUserName("");
    }
}
