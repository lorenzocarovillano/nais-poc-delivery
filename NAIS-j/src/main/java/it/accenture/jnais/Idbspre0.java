package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.PrestDao;
import it.accenture.jnais.commons.data.to.IPrest;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbspre0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.PrestIdbspre0;
import it.accenture.jnais.ws.redefines.PreDtConcsPrest;
import it.accenture.jnais.ws.redefines.PreDtDecorPrest;
import it.accenture.jnais.ws.redefines.PreDtRimb;
import it.accenture.jnais.ws.redefines.PreFrazPagIntr;
import it.accenture.jnais.ws.redefines.PreIdMoviChiu;
import it.accenture.jnais.ws.redefines.PreImpPrest;
import it.accenture.jnais.ws.redefines.PreImpPrestLiqto;
import it.accenture.jnais.ws.redefines.PreImpRimb;
import it.accenture.jnais.ws.redefines.PreIntrPrest;
import it.accenture.jnais.ws.redefines.PrePrestResEff;
import it.accenture.jnais.ws.redefines.PreRimbEff;
import it.accenture.jnais.ws.redefines.PreSdoIntr;
import it.accenture.jnais.ws.redefines.PreSpePrest;

/**Original name: IDBSPRE0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  04 SET 2008.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbspre0 extends Program implements IPrest {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private PrestDao prestDao = new PrestDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbspre0Data ws = new Idbspre0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: PREST
    private PrestIdbspre0 prest;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSPRE0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, PrestIdbspre0 prest) {
        this.idsv0003 = idsv0003;
        this.prest = prest;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbspre0 getInstance() {
        return ((Idbspre0)Programs.getInstance(Idbspre0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSPRE0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSPRE0");
        // COB_CODE: MOVE 'PREST' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("PREST");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PREST
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_CONCS_PREST
        //                ,DT_DECOR_PREST
        //                ,IMP_PREST
        //                ,INTR_PREST
        //                ,TP_PREST
        //                ,FRAZ_PAG_INTR
        //                ,DT_RIMB
        //                ,IMP_RIMB
        //                ,COD_DVS
        //                ,DT_RICH_PREST
        //                ,MOD_INTR_PREST
        //                ,SPE_PREST
        //                ,IMP_PREST_LIQTO
        //                ,SDO_INTR
        //                ,RIMB_EFF
        //                ,PREST_RES_EFF
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :PRE-ID-PREST
        //               ,:PRE-ID-OGG
        //               ,:PRE-TP-OGG
        //               ,:PRE-ID-MOVI-CRZ
        //               ,:PRE-ID-MOVI-CHIU
        //                :IND-PRE-ID-MOVI-CHIU
        //               ,:PRE-DT-INI-EFF-DB
        //               ,:PRE-DT-END-EFF-DB
        //               ,:PRE-COD-COMP-ANIA
        //               ,:PRE-DT-CONCS-PREST-DB
        //                :IND-PRE-DT-CONCS-PREST
        //               ,:PRE-DT-DECOR-PREST-DB
        //                :IND-PRE-DT-DECOR-PREST
        //               ,:PRE-IMP-PREST
        //                :IND-PRE-IMP-PREST
        //               ,:PRE-INTR-PREST
        //                :IND-PRE-INTR-PREST
        //               ,:PRE-TP-PREST
        //                :IND-PRE-TP-PREST
        //               ,:PRE-FRAZ-PAG-INTR
        //                :IND-PRE-FRAZ-PAG-INTR
        //               ,:PRE-DT-RIMB-DB
        //                :IND-PRE-DT-RIMB
        //               ,:PRE-IMP-RIMB
        //                :IND-PRE-IMP-RIMB
        //               ,:PRE-COD-DVS
        //                :IND-PRE-COD-DVS
        //               ,:PRE-DT-RICH-PREST-DB
        //               ,:PRE-MOD-INTR-PREST
        //                :IND-PRE-MOD-INTR-PREST
        //               ,:PRE-SPE-PREST
        //                :IND-PRE-SPE-PREST
        //               ,:PRE-IMP-PREST-LIQTO
        //                :IND-PRE-IMP-PREST-LIQTO
        //               ,:PRE-SDO-INTR
        //                :IND-PRE-SDO-INTR
        //               ,:PRE-RIMB-EFF
        //                :IND-PRE-RIMB-EFF
        //               ,:PRE-PREST-RES-EFF
        //                :IND-PRE-PREST-RES-EFF
        //               ,:PRE-DS-RIGA
        //               ,:PRE-DS-OPER-SQL
        //               ,:PRE-DS-VER
        //               ,:PRE-DS-TS-INI-CPTZ
        //               ,:PRE-DS-TS-END-CPTZ
        //               ,:PRE-DS-UTENTE
        //               ,:PRE-DS-STATO-ELAB
        //             FROM PREST
        //             WHERE     DS_RIGA = :PRE-DS-RIGA
        //           END-EXEC.
        prestDao.selectByPreDsRiga(prest.getPreDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO PREST
            //                  (
            //                     ID_PREST
            //                    ,ID_OGG
            //                    ,TP_OGG
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,DT_CONCS_PREST
            //                    ,DT_DECOR_PREST
            //                    ,IMP_PREST
            //                    ,INTR_PREST
            //                    ,TP_PREST
            //                    ,FRAZ_PAG_INTR
            //                    ,DT_RIMB
            //                    ,IMP_RIMB
            //                    ,COD_DVS
            //                    ,DT_RICH_PREST
            //                    ,MOD_INTR_PREST
            //                    ,SPE_PREST
            //                    ,IMP_PREST_LIQTO
            //                    ,SDO_INTR
            //                    ,RIMB_EFF
            //                    ,PREST_RES_EFF
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                  )
            //              VALUES
            //                  (
            //                    :PRE-ID-PREST
            //                    ,:PRE-ID-OGG
            //                    ,:PRE-TP-OGG
            //                    ,:PRE-ID-MOVI-CRZ
            //                    ,:PRE-ID-MOVI-CHIU
            //                     :IND-PRE-ID-MOVI-CHIU
            //                    ,:PRE-DT-INI-EFF-DB
            //                    ,:PRE-DT-END-EFF-DB
            //                    ,:PRE-COD-COMP-ANIA
            //                    ,:PRE-DT-CONCS-PREST-DB
            //                     :IND-PRE-DT-CONCS-PREST
            //                    ,:PRE-DT-DECOR-PREST-DB
            //                     :IND-PRE-DT-DECOR-PREST
            //                    ,:PRE-IMP-PREST
            //                     :IND-PRE-IMP-PREST
            //                    ,:PRE-INTR-PREST
            //                     :IND-PRE-INTR-PREST
            //                    ,:PRE-TP-PREST
            //                     :IND-PRE-TP-PREST
            //                    ,:PRE-FRAZ-PAG-INTR
            //                     :IND-PRE-FRAZ-PAG-INTR
            //                    ,:PRE-DT-RIMB-DB
            //                     :IND-PRE-DT-RIMB
            //                    ,:PRE-IMP-RIMB
            //                     :IND-PRE-IMP-RIMB
            //                    ,:PRE-COD-DVS
            //                     :IND-PRE-COD-DVS
            //                    ,:PRE-DT-RICH-PREST-DB
            //                    ,:PRE-MOD-INTR-PREST
            //                     :IND-PRE-MOD-INTR-PREST
            //                    ,:PRE-SPE-PREST
            //                     :IND-PRE-SPE-PREST
            //                    ,:PRE-IMP-PREST-LIQTO
            //                     :IND-PRE-IMP-PREST-LIQTO
            //                    ,:PRE-SDO-INTR
            //                     :IND-PRE-SDO-INTR
            //                    ,:PRE-RIMB-EFF
            //                     :IND-PRE-RIMB-EFF
            //                    ,:PRE-PREST-RES-EFF
            //                     :IND-PRE-PREST-RES-EFF
            //                    ,:PRE-DS-RIGA
            //                    ,:PRE-DS-OPER-SQL
            //                    ,:PRE-DS-VER
            //                    ,:PRE-DS-TS-INI-CPTZ
            //                    ,:PRE-DS-TS-END-CPTZ
            //                    ,:PRE-DS-UTENTE
            //                    ,:PRE-DS-STATO-ELAB
            //                  )
            //           END-EXEC
            prestDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE PREST SET
        //                   ID_PREST               =
        //                :PRE-ID-PREST
        //                  ,ID_OGG                 =
        //                :PRE-ID-OGG
        //                  ,TP_OGG                 =
        //                :PRE-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :PRE-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :PRE-ID-MOVI-CHIU
        //                                       :IND-PRE-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :PRE-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :PRE-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :PRE-COD-COMP-ANIA
        //                  ,DT_CONCS_PREST         =
        //           :PRE-DT-CONCS-PREST-DB
        //                                       :IND-PRE-DT-CONCS-PREST
        //                  ,DT_DECOR_PREST         =
        //           :PRE-DT-DECOR-PREST-DB
        //                                       :IND-PRE-DT-DECOR-PREST
        //                  ,IMP_PREST              =
        //                :PRE-IMP-PREST
        //                                       :IND-PRE-IMP-PREST
        //                  ,INTR_PREST             =
        //                :PRE-INTR-PREST
        //                                       :IND-PRE-INTR-PREST
        //                  ,TP_PREST               =
        //                :PRE-TP-PREST
        //                                       :IND-PRE-TP-PREST
        //                  ,FRAZ_PAG_INTR          =
        //                :PRE-FRAZ-PAG-INTR
        //                                       :IND-PRE-FRAZ-PAG-INTR
        //                  ,DT_RIMB                =
        //           :PRE-DT-RIMB-DB
        //                                       :IND-PRE-DT-RIMB
        //                  ,IMP_RIMB               =
        //                :PRE-IMP-RIMB
        //                                       :IND-PRE-IMP-RIMB
        //                  ,COD_DVS                =
        //                :PRE-COD-DVS
        //                                       :IND-PRE-COD-DVS
        //                  ,DT_RICH_PREST          =
        //           :PRE-DT-RICH-PREST-DB
        //                  ,MOD_INTR_PREST         =
        //                :PRE-MOD-INTR-PREST
        //                                       :IND-PRE-MOD-INTR-PREST
        //                  ,SPE_PREST              =
        //                :PRE-SPE-PREST
        //                                       :IND-PRE-SPE-PREST
        //                  ,IMP_PREST_LIQTO        =
        //                :PRE-IMP-PREST-LIQTO
        //                                       :IND-PRE-IMP-PREST-LIQTO
        //                  ,SDO_INTR               =
        //                :PRE-SDO-INTR
        //                                       :IND-PRE-SDO-INTR
        //                  ,RIMB_EFF               =
        //                :PRE-RIMB-EFF
        //                                       :IND-PRE-RIMB-EFF
        //                  ,PREST_RES_EFF          =
        //                :PRE-PREST-RES-EFF
        //                                       :IND-PRE-PREST-RES-EFF
        //                  ,DS_RIGA                =
        //                :PRE-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :PRE-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :PRE-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :PRE-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :PRE-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :PRE-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :PRE-DS-STATO-ELAB
        //                WHERE     DS_RIGA = :PRE-DS-RIGA
        //           END-EXEC.
        prestDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM PREST
        //                WHERE     DS_RIGA = :PRE-DS-RIGA
        //           END-EXEC.
        prestDao.deleteByPreDsRiga(prest.getPreDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-PRE CURSOR FOR
        //              SELECT
        //                     ID_PREST
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_CONCS_PREST
        //                    ,DT_DECOR_PREST
        //                    ,IMP_PREST
        //                    ,INTR_PREST
        //                    ,TP_PREST
        //                    ,FRAZ_PAG_INTR
        //                    ,DT_RIMB
        //                    ,IMP_RIMB
        //                    ,COD_DVS
        //                    ,DT_RICH_PREST
        //                    ,MOD_INTR_PREST
        //                    ,SPE_PREST
        //                    ,IMP_PREST_LIQTO
        //                    ,SDO_INTR
        //                    ,RIMB_EFF
        //                    ,PREST_RES_EFF
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM PREST
        //              WHERE     ID_PREST = :PRE-ID-PREST
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PREST
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_CONCS_PREST
        //                ,DT_DECOR_PREST
        //                ,IMP_PREST
        //                ,INTR_PREST
        //                ,TP_PREST
        //                ,FRAZ_PAG_INTR
        //                ,DT_RIMB
        //                ,IMP_RIMB
        //                ,COD_DVS
        //                ,DT_RICH_PREST
        //                ,MOD_INTR_PREST
        //                ,SPE_PREST
        //                ,IMP_PREST_LIQTO
        //                ,SDO_INTR
        //                ,RIMB_EFF
        //                ,PREST_RES_EFF
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :PRE-ID-PREST
        //               ,:PRE-ID-OGG
        //               ,:PRE-TP-OGG
        //               ,:PRE-ID-MOVI-CRZ
        //               ,:PRE-ID-MOVI-CHIU
        //                :IND-PRE-ID-MOVI-CHIU
        //               ,:PRE-DT-INI-EFF-DB
        //               ,:PRE-DT-END-EFF-DB
        //               ,:PRE-COD-COMP-ANIA
        //               ,:PRE-DT-CONCS-PREST-DB
        //                :IND-PRE-DT-CONCS-PREST
        //               ,:PRE-DT-DECOR-PREST-DB
        //                :IND-PRE-DT-DECOR-PREST
        //               ,:PRE-IMP-PREST
        //                :IND-PRE-IMP-PREST
        //               ,:PRE-INTR-PREST
        //                :IND-PRE-INTR-PREST
        //               ,:PRE-TP-PREST
        //                :IND-PRE-TP-PREST
        //               ,:PRE-FRAZ-PAG-INTR
        //                :IND-PRE-FRAZ-PAG-INTR
        //               ,:PRE-DT-RIMB-DB
        //                :IND-PRE-DT-RIMB
        //               ,:PRE-IMP-RIMB
        //                :IND-PRE-IMP-RIMB
        //               ,:PRE-COD-DVS
        //                :IND-PRE-COD-DVS
        //               ,:PRE-DT-RICH-PREST-DB
        //               ,:PRE-MOD-INTR-PREST
        //                :IND-PRE-MOD-INTR-PREST
        //               ,:PRE-SPE-PREST
        //                :IND-PRE-SPE-PREST
        //               ,:PRE-IMP-PREST-LIQTO
        //                :IND-PRE-IMP-PREST-LIQTO
        //               ,:PRE-SDO-INTR
        //                :IND-PRE-SDO-INTR
        //               ,:PRE-RIMB-EFF
        //                :IND-PRE-RIMB-EFF
        //               ,:PRE-PREST-RES-EFF
        //                :IND-PRE-PREST-RES-EFF
        //               ,:PRE-DS-RIGA
        //               ,:PRE-DS-OPER-SQL
        //               ,:PRE-DS-VER
        //               ,:PRE-DS-TS-INI-CPTZ
        //               ,:PRE-DS-TS-END-CPTZ
        //               ,:PRE-DS-UTENTE
        //               ,:PRE-DS-STATO-ELAB
        //             FROM PREST
        //             WHERE     ID_PREST = :PRE-ID-PREST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        prestDao.selectRec(prest.getPreIdPrest(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE PREST SET
        //                   ID_PREST               =
        //                :PRE-ID-PREST
        //                  ,ID_OGG                 =
        //                :PRE-ID-OGG
        //                  ,TP_OGG                 =
        //                :PRE-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :PRE-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :PRE-ID-MOVI-CHIU
        //                                       :IND-PRE-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :PRE-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :PRE-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :PRE-COD-COMP-ANIA
        //                  ,DT_CONCS_PREST         =
        //           :PRE-DT-CONCS-PREST-DB
        //                                       :IND-PRE-DT-CONCS-PREST
        //                  ,DT_DECOR_PREST         =
        //           :PRE-DT-DECOR-PREST-DB
        //                                       :IND-PRE-DT-DECOR-PREST
        //                  ,IMP_PREST              =
        //                :PRE-IMP-PREST
        //                                       :IND-PRE-IMP-PREST
        //                  ,INTR_PREST             =
        //                :PRE-INTR-PREST
        //                                       :IND-PRE-INTR-PREST
        //                  ,TP_PREST               =
        //                :PRE-TP-PREST
        //                                       :IND-PRE-TP-PREST
        //                  ,FRAZ_PAG_INTR          =
        //                :PRE-FRAZ-PAG-INTR
        //                                       :IND-PRE-FRAZ-PAG-INTR
        //                  ,DT_RIMB                =
        //           :PRE-DT-RIMB-DB
        //                                       :IND-PRE-DT-RIMB
        //                  ,IMP_RIMB               =
        //                :PRE-IMP-RIMB
        //                                       :IND-PRE-IMP-RIMB
        //                  ,COD_DVS                =
        //                :PRE-COD-DVS
        //                                       :IND-PRE-COD-DVS
        //                  ,DT_RICH_PREST          =
        //           :PRE-DT-RICH-PREST-DB
        //                  ,MOD_INTR_PREST         =
        //                :PRE-MOD-INTR-PREST
        //                                       :IND-PRE-MOD-INTR-PREST
        //                  ,SPE_PREST              =
        //                :PRE-SPE-PREST
        //                                       :IND-PRE-SPE-PREST
        //                  ,IMP_PREST_LIQTO        =
        //                :PRE-IMP-PREST-LIQTO
        //                                       :IND-PRE-IMP-PREST-LIQTO
        //                  ,SDO_INTR               =
        //                :PRE-SDO-INTR
        //                                       :IND-PRE-SDO-INTR
        //                  ,RIMB_EFF               =
        //                :PRE-RIMB-EFF
        //                                       :IND-PRE-RIMB-EFF
        //                  ,PREST_RES_EFF          =
        //                :PRE-PREST-RES-EFF
        //                                       :IND-PRE-PREST-RES-EFF
        //                  ,DS_RIGA                =
        //                :PRE-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :PRE-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :PRE-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :PRE-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :PRE-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :PRE-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :PRE-DS-STATO-ELAB
        //                WHERE     DS_RIGA = :PRE-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        prestDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-PRE
        //           END-EXEC.
        prestDao.openCIdUpdEffPre(prest.getPreIdPrest(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-PRE
        //           END-EXEC.
        prestDao.closeCIdUpdEffPre();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-PRE
        //           INTO
        //                :PRE-ID-PREST
        //               ,:PRE-ID-OGG
        //               ,:PRE-TP-OGG
        //               ,:PRE-ID-MOVI-CRZ
        //               ,:PRE-ID-MOVI-CHIU
        //                :IND-PRE-ID-MOVI-CHIU
        //               ,:PRE-DT-INI-EFF-DB
        //               ,:PRE-DT-END-EFF-DB
        //               ,:PRE-COD-COMP-ANIA
        //               ,:PRE-DT-CONCS-PREST-DB
        //                :IND-PRE-DT-CONCS-PREST
        //               ,:PRE-DT-DECOR-PREST-DB
        //                :IND-PRE-DT-DECOR-PREST
        //               ,:PRE-IMP-PREST
        //                :IND-PRE-IMP-PREST
        //               ,:PRE-INTR-PREST
        //                :IND-PRE-INTR-PREST
        //               ,:PRE-TP-PREST
        //                :IND-PRE-TP-PREST
        //               ,:PRE-FRAZ-PAG-INTR
        //                :IND-PRE-FRAZ-PAG-INTR
        //               ,:PRE-DT-RIMB-DB
        //                :IND-PRE-DT-RIMB
        //               ,:PRE-IMP-RIMB
        //                :IND-PRE-IMP-RIMB
        //               ,:PRE-COD-DVS
        //                :IND-PRE-COD-DVS
        //               ,:PRE-DT-RICH-PREST-DB
        //               ,:PRE-MOD-INTR-PREST
        //                :IND-PRE-MOD-INTR-PREST
        //               ,:PRE-SPE-PREST
        //                :IND-PRE-SPE-PREST
        //               ,:PRE-IMP-PREST-LIQTO
        //                :IND-PRE-IMP-PREST-LIQTO
        //               ,:PRE-SDO-INTR
        //                :IND-PRE-SDO-INTR
        //               ,:PRE-RIMB-EFF
        //                :IND-PRE-RIMB-EFF
        //               ,:PRE-PREST-RES-EFF
        //                :IND-PRE-PREST-RES-EFF
        //               ,:PRE-DS-RIGA
        //               ,:PRE-DS-OPER-SQL
        //               ,:PRE-DS-VER
        //               ,:PRE-DS-TS-INI-CPTZ
        //               ,:PRE-DS-TS-END-CPTZ
        //               ,:PRE-DS-UTENTE
        //               ,:PRE-DS-STATO-ELAB
        //           END-EXEC.
        prestDao.fetchCIdUpdEffPre(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-EFF-PRE CURSOR FOR
        //              SELECT
        //                     ID_PREST
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_CONCS_PREST
        //                    ,DT_DECOR_PREST
        //                    ,IMP_PREST
        //                    ,INTR_PREST
        //                    ,TP_PREST
        //                    ,FRAZ_PAG_INTR
        //                    ,DT_RIMB
        //                    ,IMP_RIMB
        //                    ,COD_DVS
        //                    ,DT_RICH_PREST
        //                    ,MOD_INTR_PREST
        //                    ,SPE_PREST
        //                    ,IMP_PREST_LIQTO
        //                    ,SDO_INTR
        //                    ,RIMB_EFF
        //                    ,PREST_RES_EFF
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM PREST
        //              WHERE     ID_OGG = :PRE-ID-OGG
        //                    AND TP_OGG = :PRE-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_PREST ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PREST
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_CONCS_PREST
        //                ,DT_DECOR_PREST
        //                ,IMP_PREST
        //                ,INTR_PREST
        //                ,TP_PREST
        //                ,FRAZ_PAG_INTR
        //                ,DT_RIMB
        //                ,IMP_RIMB
        //                ,COD_DVS
        //                ,DT_RICH_PREST
        //                ,MOD_INTR_PREST
        //                ,SPE_PREST
        //                ,IMP_PREST_LIQTO
        //                ,SDO_INTR
        //                ,RIMB_EFF
        //                ,PREST_RES_EFF
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :PRE-ID-PREST
        //               ,:PRE-ID-OGG
        //               ,:PRE-TP-OGG
        //               ,:PRE-ID-MOVI-CRZ
        //               ,:PRE-ID-MOVI-CHIU
        //                :IND-PRE-ID-MOVI-CHIU
        //               ,:PRE-DT-INI-EFF-DB
        //               ,:PRE-DT-END-EFF-DB
        //               ,:PRE-COD-COMP-ANIA
        //               ,:PRE-DT-CONCS-PREST-DB
        //                :IND-PRE-DT-CONCS-PREST
        //               ,:PRE-DT-DECOR-PREST-DB
        //                :IND-PRE-DT-DECOR-PREST
        //               ,:PRE-IMP-PREST
        //                :IND-PRE-IMP-PREST
        //               ,:PRE-INTR-PREST
        //                :IND-PRE-INTR-PREST
        //               ,:PRE-TP-PREST
        //                :IND-PRE-TP-PREST
        //               ,:PRE-FRAZ-PAG-INTR
        //                :IND-PRE-FRAZ-PAG-INTR
        //               ,:PRE-DT-RIMB-DB
        //                :IND-PRE-DT-RIMB
        //               ,:PRE-IMP-RIMB
        //                :IND-PRE-IMP-RIMB
        //               ,:PRE-COD-DVS
        //                :IND-PRE-COD-DVS
        //               ,:PRE-DT-RICH-PREST-DB
        //               ,:PRE-MOD-INTR-PREST
        //                :IND-PRE-MOD-INTR-PREST
        //               ,:PRE-SPE-PREST
        //                :IND-PRE-SPE-PREST
        //               ,:PRE-IMP-PREST-LIQTO
        //                :IND-PRE-IMP-PREST-LIQTO
        //               ,:PRE-SDO-INTR
        //                :IND-PRE-SDO-INTR
        //               ,:PRE-RIMB-EFF
        //                :IND-PRE-RIMB-EFF
        //               ,:PRE-PREST-RES-EFF
        //                :IND-PRE-PREST-RES-EFF
        //               ,:PRE-DS-RIGA
        //               ,:PRE-DS-OPER-SQL
        //               ,:PRE-DS-VER
        //               ,:PRE-DS-TS-INI-CPTZ
        //               ,:PRE-DS-TS-END-CPTZ
        //               ,:PRE-DS-UTENTE
        //               ,:PRE-DS-STATO-ELAB
        //             FROM PREST
        //             WHERE     ID_OGG = :PRE-ID-OGG
        //                    AND TP_OGG = :PRE-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        prestDao.selectRec1(prest.getPreIdOgg(), prest.getPreTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-EFF-PRE
        //           END-EXEC.
        prestDao.openCIdoEffPre(prest.getPreIdOgg(), prest.getPreTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-EFF-PRE
        //           END-EXEC.
        prestDao.closeCIdoEffPre();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-EFF-PRE
        //           INTO
        //                :PRE-ID-PREST
        //               ,:PRE-ID-OGG
        //               ,:PRE-TP-OGG
        //               ,:PRE-ID-MOVI-CRZ
        //               ,:PRE-ID-MOVI-CHIU
        //                :IND-PRE-ID-MOVI-CHIU
        //               ,:PRE-DT-INI-EFF-DB
        //               ,:PRE-DT-END-EFF-DB
        //               ,:PRE-COD-COMP-ANIA
        //               ,:PRE-DT-CONCS-PREST-DB
        //                :IND-PRE-DT-CONCS-PREST
        //               ,:PRE-DT-DECOR-PREST-DB
        //                :IND-PRE-DT-DECOR-PREST
        //               ,:PRE-IMP-PREST
        //                :IND-PRE-IMP-PREST
        //               ,:PRE-INTR-PREST
        //                :IND-PRE-INTR-PREST
        //               ,:PRE-TP-PREST
        //                :IND-PRE-TP-PREST
        //               ,:PRE-FRAZ-PAG-INTR
        //                :IND-PRE-FRAZ-PAG-INTR
        //               ,:PRE-DT-RIMB-DB
        //                :IND-PRE-DT-RIMB
        //               ,:PRE-IMP-RIMB
        //                :IND-PRE-IMP-RIMB
        //               ,:PRE-COD-DVS
        //                :IND-PRE-COD-DVS
        //               ,:PRE-DT-RICH-PREST-DB
        //               ,:PRE-MOD-INTR-PREST
        //                :IND-PRE-MOD-INTR-PREST
        //               ,:PRE-SPE-PREST
        //                :IND-PRE-SPE-PREST
        //               ,:PRE-IMP-PREST-LIQTO
        //                :IND-PRE-IMP-PREST-LIQTO
        //               ,:PRE-SDO-INTR
        //                :IND-PRE-SDO-INTR
        //               ,:PRE-RIMB-EFF
        //                :IND-PRE-RIMB-EFF
        //               ,:PRE-PREST-RES-EFF
        //                :IND-PRE-PREST-RES-EFF
        //               ,:PRE-DS-RIGA
        //               ,:PRE-DS-OPER-SQL
        //               ,:PRE-DS-VER
        //               ,:PRE-DS-TS-INI-CPTZ
        //               ,:PRE-DS-TS-END-CPTZ
        //               ,:PRE-DS-UTENTE
        //               ,:PRE-DS-STATO-ELAB
        //           END-EXEC.
        prestDao.fetchCIdoEffPre(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX
            a770CloseCursorIdo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PREST
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_CONCS_PREST
        //                ,DT_DECOR_PREST
        //                ,IMP_PREST
        //                ,INTR_PREST
        //                ,TP_PREST
        //                ,FRAZ_PAG_INTR
        //                ,DT_RIMB
        //                ,IMP_RIMB
        //                ,COD_DVS
        //                ,DT_RICH_PREST
        //                ,MOD_INTR_PREST
        //                ,SPE_PREST
        //                ,IMP_PREST_LIQTO
        //                ,SDO_INTR
        //                ,RIMB_EFF
        //                ,PREST_RES_EFF
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :PRE-ID-PREST
        //               ,:PRE-ID-OGG
        //               ,:PRE-TP-OGG
        //               ,:PRE-ID-MOVI-CRZ
        //               ,:PRE-ID-MOVI-CHIU
        //                :IND-PRE-ID-MOVI-CHIU
        //               ,:PRE-DT-INI-EFF-DB
        //               ,:PRE-DT-END-EFF-DB
        //               ,:PRE-COD-COMP-ANIA
        //               ,:PRE-DT-CONCS-PREST-DB
        //                :IND-PRE-DT-CONCS-PREST
        //               ,:PRE-DT-DECOR-PREST-DB
        //                :IND-PRE-DT-DECOR-PREST
        //               ,:PRE-IMP-PREST
        //                :IND-PRE-IMP-PREST
        //               ,:PRE-INTR-PREST
        //                :IND-PRE-INTR-PREST
        //               ,:PRE-TP-PREST
        //                :IND-PRE-TP-PREST
        //               ,:PRE-FRAZ-PAG-INTR
        //                :IND-PRE-FRAZ-PAG-INTR
        //               ,:PRE-DT-RIMB-DB
        //                :IND-PRE-DT-RIMB
        //               ,:PRE-IMP-RIMB
        //                :IND-PRE-IMP-RIMB
        //               ,:PRE-COD-DVS
        //                :IND-PRE-COD-DVS
        //               ,:PRE-DT-RICH-PREST-DB
        //               ,:PRE-MOD-INTR-PREST
        //                :IND-PRE-MOD-INTR-PREST
        //               ,:PRE-SPE-PREST
        //                :IND-PRE-SPE-PREST
        //               ,:PRE-IMP-PREST-LIQTO
        //                :IND-PRE-IMP-PREST-LIQTO
        //               ,:PRE-SDO-INTR
        //                :IND-PRE-SDO-INTR
        //               ,:PRE-RIMB-EFF
        //                :IND-PRE-RIMB-EFF
        //               ,:PRE-PREST-RES-EFF
        //                :IND-PRE-PREST-RES-EFF
        //               ,:PRE-DS-RIGA
        //               ,:PRE-DS-OPER-SQL
        //               ,:PRE-DS-VER
        //               ,:PRE-DS-TS-INI-CPTZ
        //               ,:PRE-DS-TS-END-CPTZ
        //               ,:PRE-DS-UTENTE
        //               ,:PRE-DS-STATO-ELAB
        //             FROM PREST
        //             WHERE     ID_PREST = :PRE-ID-PREST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        prestDao.selectRec2(prest.getPreIdPrest(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-CPZ-PRE CURSOR FOR
        //              SELECT
        //                     ID_PREST
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_CONCS_PREST
        //                    ,DT_DECOR_PREST
        //                    ,IMP_PREST
        //                    ,INTR_PREST
        //                    ,TP_PREST
        //                    ,FRAZ_PAG_INTR
        //                    ,DT_RIMB
        //                    ,IMP_RIMB
        //                    ,COD_DVS
        //                    ,DT_RICH_PREST
        //                    ,MOD_INTR_PREST
        //                    ,SPE_PREST
        //                    ,IMP_PREST_LIQTO
        //                    ,SDO_INTR
        //                    ,RIMB_EFF
        //                    ,PREST_RES_EFF
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM PREST
        //              WHERE     ID_OGG = :PRE-ID-OGG
        //           AND TP_OGG = :PRE-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_PREST ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PREST
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_CONCS_PREST
        //                ,DT_DECOR_PREST
        //                ,IMP_PREST
        //                ,INTR_PREST
        //                ,TP_PREST
        //                ,FRAZ_PAG_INTR
        //                ,DT_RIMB
        //                ,IMP_RIMB
        //                ,COD_DVS
        //                ,DT_RICH_PREST
        //                ,MOD_INTR_PREST
        //                ,SPE_PREST
        //                ,IMP_PREST_LIQTO
        //                ,SDO_INTR
        //                ,RIMB_EFF
        //                ,PREST_RES_EFF
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :PRE-ID-PREST
        //               ,:PRE-ID-OGG
        //               ,:PRE-TP-OGG
        //               ,:PRE-ID-MOVI-CRZ
        //               ,:PRE-ID-MOVI-CHIU
        //                :IND-PRE-ID-MOVI-CHIU
        //               ,:PRE-DT-INI-EFF-DB
        //               ,:PRE-DT-END-EFF-DB
        //               ,:PRE-COD-COMP-ANIA
        //               ,:PRE-DT-CONCS-PREST-DB
        //                :IND-PRE-DT-CONCS-PREST
        //               ,:PRE-DT-DECOR-PREST-DB
        //                :IND-PRE-DT-DECOR-PREST
        //               ,:PRE-IMP-PREST
        //                :IND-PRE-IMP-PREST
        //               ,:PRE-INTR-PREST
        //                :IND-PRE-INTR-PREST
        //               ,:PRE-TP-PREST
        //                :IND-PRE-TP-PREST
        //               ,:PRE-FRAZ-PAG-INTR
        //                :IND-PRE-FRAZ-PAG-INTR
        //               ,:PRE-DT-RIMB-DB
        //                :IND-PRE-DT-RIMB
        //               ,:PRE-IMP-RIMB
        //                :IND-PRE-IMP-RIMB
        //               ,:PRE-COD-DVS
        //                :IND-PRE-COD-DVS
        //               ,:PRE-DT-RICH-PREST-DB
        //               ,:PRE-MOD-INTR-PREST
        //                :IND-PRE-MOD-INTR-PREST
        //               ,:PRE-SPE-PREST
        //                :IND-PRE-SPE-PREST
        //               ,:PRE-IMP-PREST-LIQTO
        //                :IND-PRE-IMP-PREST-LIQTO
        //               ,:PRE-SDO-INTR
        //                :IND-PRE-SDO-INTR
        //               ,:PRE-RIMB-EFF
        //                :IND-PRE-RIMB-EFF
        //               ,:PRE-PREST-RES-EFF
        //                :IND-PRE-PREST-RES-EFF
        //               ,:PRE-DS-RIGA
        //               ,:PRE-DS-OPER-SQL
        //               ,:PRE-DS-VER
        //               ,:PRE-DS-TS-INI-CPTZ
        //               ,:PRE-DS-TS-END-CPTZ
        //               ,:PRE-DS-UTENTE
        //               ,:PRE-DS-STATO-ELAB
        //             FROM PREST
        //             WHERE     ID_OGG = :PRE-ID-OGG
        //                    AND TP_OGG = :PRE-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        prestDao.selectRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-CPZ-PRE
        //           END-EXEC.
        prestDao.openCIdoCpzPre(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-CPZ-PRE
        //           END-EXEC.
        prestDao.closeCIdoCpzPre();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-CPZ-PRE
        //           INTO
        //                :PRE-ID-PREST
        //               ,:PRE-ID-OGG
        //               ,:PRE-TP-OGG
        //               ,:PRE-ID-MOVI-CRZ
        //               ,:PRE-ID-MOVI-CHIU
        //                :IND-PRE-ID-MOVI-CHIU
        //               ,:PRE-DT-INI-EFF-DB
        //               ,:PRE-DT-END-EFF-DB
        //               ,:PRE-COD-COMP-ANIA
        //               ,:PRE-DT-CONCS-PREST-DB
        //                :IND-PRE-DT-CONCS-PREST
        //               ,:PRE-DT-DECOR-PREST-DB
        //                :IND-PRE-DT-DECOR-PREST
        //               ,:PRE-IMP-PREST
        //                :IND-PRE-IMP-PREST
        //               ,:PRE-INTR-PREST
        //                :IND-PRE-INTR-PREST
        //               ,:PRE-TP-PREST
        //                :IND-PRE-TP-PREST
        //               ,:PRE-FRAZ-PAG-INTR
        //                :IND-PRE-FRAZ-PAG-INTR
        //               ,:PRE-DT-RIMB-DB
        //                :IND-PRE-DT-RIMB
        //               ,:PRE-IMP-RIMB
        //                :IND-PRE-IMP-RIMB
        //               ,:PRE-COD-DVS
        //                :IND-PRE-COD-DVS
        //               ,:PRE-DT-RICH-PREST-DB
        //               ,:PRE-MOD-INTR-PREST
        //                :IND-PRE-MOD-INTR-PREST
        //               ,:PRE-SPE-PREST
        //                :IND-PRE-SPE-PREST
        //               ,:PRE-IMP-PREST-LIQTO
        //                :IND-PRE-IMP-PREST-LIQTO
        //               ,:PRE-SDO-INTR
        //                :IND-PRE-SDO-INTR
        //               ,:PRE-RIMB-EFF
        //                :IND-PRE-RIMB-EFF
        //               ,:PRE-PREST-RES-EFF
        //                :IND-PRE-PREST-RES-EFF
        //               ,:PRE-DS-RIGA
        //               ,:PRE-DS-OPER-SQL
        //               ,:PRE-DS-VER
        //               ,:PRE-DS-TS-INI-CPTZ
        //               ,:PRE-DS-TS-END-CPTZ
        //               ,:PRE-DS-UTENTE
        //               ,:PRE-DS-STATO-ELAB
        //           END-EXEC.
        prestDao.fetchCIdoCpzPre(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX
            b770CloseCursorIdoCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-PRE-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO PRE-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndPrest().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-ID-MOVI-CHIU-NULL
            prest.getPreIdMoviChiu().setPreIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreIdMoviChiu.Len.PRE_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-PRE-DT-CONCS-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-DT-CONCS-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getDtConcsPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-DT-CONCS-PREST-NULL
            prest.getPreDtConcsPrest().setPreDtConcsPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreDtConcsPrest.Len.PRE_DT_CONCS_PREST_NULL));
        }
        // COB_CODE: IF IND-PRE-DT-DECOR-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-DT-DECOR-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getDtDecorPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-DT-DECOR-PREST-NULL
            prest.getPreDtDecorPrest().setPreDtDecorPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreDtDecorPrest.Len.PRE_DT_DECOR_PREST_NULL));
        }
        // COB_CODE: IF IND-PRE-IMP-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-IMP-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getImpPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-IMP-PREST-NULL
            prest.getPreImpPrest().setPreImpPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreImpPrest.Len.PRE_IMP_PREST_NULL));
        }
        // COB_CODE: IF IND-PRE-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-INTR-PREST-NULL
            prest.getPreIntrPrest().setPreIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreIntrPrest.Len.PRE_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-PRE-TP-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-TP-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getTpPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-TP-PREST-NULL
            prest.setPreTpPrest(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PrestIdbspre0.Len.PRE_TP_PREST));
        }
        // COB_CODE: IF IND-PRE-FRAZ-PAG-INTR = -1
        //              MOVE HIGH-VALUES TO PRE-FRAZ-PAG-INTR-NULL
        //           END-IF
        if (ws.getIndPrest().getFrazPagIntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-FRAZ-PAG-INTR-NULL
            prest.getPreFrazPagIntr().setPreFrazPagIntrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreFrazPagIntr.Len.PRE_FRAZ_PAG_INTR_NULL));
        }
        // COB_CODE: IF IND-PRE-DT-RIMB = -1
        //              MOVE HIGH-VALUES TO PRE-DT-RIMB-NULL
        //           END-IF
        if (ws.getIndPrest().getDtRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-DT-RIMB-NULL
            prest.getPreDtRimb().setPreDtRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreDtRimb.Len.PRE_DT_RIMB_NULL));
        }
        // COB_CODE: IF IND-PRE-IMP-RIMB = -1
        //              MOVE HIGH-VALUES TO PRE-IMP-RIMB-NULL
        //           END-IF
        if (ws.getIndPrest().getImpRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-IMP-RIMB-NULL
            prest.getPreImpRimb().setPreImpRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreImpRimb.Len.PRE_IMP_RIMB_NULL));
        }
        // COB_CODE: IF IND-PRE-COD-DVS = -1
        //              MOVE HIGH-VALUES TO PRE-COD-DVS-NULL
        //           END-IF
        if (ws.getIndPrest().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-COD-DVS-NULL
            prest.setPreCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PrestIdbspre0.Len.PRE_COD_DVS));
        }
        // COB_CODE: IF IND-PRE-MOD-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-MOD-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getModIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-MOD-INTR-PREST-NULL
            prest.setPreModIntrPrest(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PrestIdbspre0.Len.PRE_MOD_INTR_PREST));
        }
        // COB_CODE: IF IND-PRE-SPE-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-SPE-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getSpePrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-SPE-PREST-NULL
            prest.getPreSpePrest().setPreSpePrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreSpePrest.Len.PRE_SPE_PREST_NULL));
        }
        // COB_CODE: IF IND-PRE-IMP-PREST-LIQTO = -1
        //              MOVE HIGH-VALUES TO PRE-IMP-PREST-LIQTO-NULL
        //           END-IF
        if (ws.getIndPrest().getImpPrestLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-IMP-PREST-LIQTO-NULL
            prest.getPreImpPrestLiqto().setPreImpPrestLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreImpPrestLiqto.Len.PRE_IMP_PREST_LIQTO_NULL));
        }
        // COB_CODE: IF IND-PRE-SDO-INTR = -1
        //              MOVE HIGH-VALUES TO PRE-SDO-INTR-NULL
        //           END-IF
        if (ws.getIndPrest().getSdoIntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-SDO-INTR-NULL
            prest.getPreSdoIntr().setPreSdoIntrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreSdoIntr.Len.PRE_SDO_INTR_NULL));
        }
        // COB_CODE: IF IND-PRE-RIMB-EFF = -1
        //              MOVE HIGH-VALUES TO PRE-RIMB-EFF-NULL
        //           END-IF
        if (ws.getIndPrest().getRimbEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-RIMB-EFF-NULL
            prest.getPreRimbEff().setPreRimbEffNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreRimbEff.Len.PRE_RIMB_EFF_NULL));
        }
        // COB_CODE: IF IND-PRE-PREST-RES-EFF = -1
        //              MOVE HIGH-VALUES TO PRE-PREST-RES-EFF-NULL
        //           END-IF.
        if (ws.getIndPrest().getPrestResEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-PREST-RES-EFF-NULL
            prest.getPrePrestResEff().setPrePrestResEffNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PrePrestResEff.Len.PRE_PREST_RES_EFF_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO PRE-DS-OPER-SQL
        prest.setPreDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO PRE-DS-VER
        prest.setPreDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO PRE-DS-UTENTE
        prest.setPreDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO PRE-DS-STATO-ELAB.
        prest.setPreDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO PRE-DS-OPER-SQL
        prest.setPreDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO PRE-DS-UTENTE.
        prest.setPreDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF PRE-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-PRE-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreIdMoviChiu().getPreIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-ID-MOVI-CHIU
            ws.getIndPrest().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-ID-MOVI-CHIU
            ws.getIndPrest().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF PRE-DT-CONCS-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-DT-CONCS-PREST
        //           ELSE
        //              MOVE 0 TO IND-PRE-DT-CONCS-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreDtConcsPrest().getPreDtConcsPrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-DT-CONCS-PREST
            ws.getIndPrest().setDtConcsPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-DT-CONCS-PREST
            ws.getIndPrest().setDtConcsPrest(((short)0));
        }
        // COB_CODE: IF PRE-DT-DECOR-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-DT-DECOR-PREST
        //           ELSE
        //              MOVE 0 TO IND-PRE-DT-DECOR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreDtDecorPrest().getPreDtDecorPrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-DT-DECOR-PREST
            ws.getIndPrest().setDtDecorPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-DT-DECOR-PREST
            ws.getIndPrest().setDtDecorPrest(((short)0));
        }
        // COB_CODE: IF PRE-IMP-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-IMP-PREST
        //           ELSE
        //              MOVE 0 TO IND-PRE-IMP-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreImpPrest().getPreImpPrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-IMP-PREST
            ws.getIndPrest().setImpPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-IMP-PREST
            ws.getIndPrest().setImpPrest(((short)0));
        }
        // COB_CODE: IF PRE-INTR-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-INTR-PREST
        //           ELSE
        //              MOVE 0 TO IND-PRE-INTR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreIntrPrest().getPreIntrPrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-INTR-PREST
            ws.getIndPrest().setIntrPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-INTR-PREST
            ws.getIndPrest().setIntrPrest(((short)0));
        }
        // COB_CODE: IF PRE-TP-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-TP-PREST
        //           ELSE
        //              MOVE 0 TO IND-PRE-TP-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreTpPrestFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-TP-PREST
            ws.getIndPrest().setTpPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-TP-PREST
            ws.getIndPrest().setTpPrest(((short)0));
        }
        // COB_CODE: IF PRE-FRAZ-PAG-INTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-FRAZ-PAG-INTR
        //           ELSE
        //              MOVE 0 TO IND-PRE-FRAZ-PAG-INTR
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreFrazPagIntr().getPreFrazPagIntrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-FRAZ-PAG-INTR
            ws.getIndPrest().setFrazPagIntr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-FRAZ-PAG-INTR
            ws.getIndPrest().setFrazPagIntr(((short)0));
        }
        // COB_CODE: IF PRE-DT-RIMB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-DT-RIMB
        //           ELSE
        //              MOVE 0 TO IND-PRE-DT-RIMB
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreDtRimb().getPreDtRimbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-DT-RIMB
            ws.getIndPrest().setDtRimb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-DT-RIMB
            ws.getIndPrest().setDtRimb(((short)0));
        }
        // COB_CODE: IF PRE-IMP-RIMB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-IMP-RIMB
        //           ELSE
        //              MOVE 0 TO IND-PRE-IMP-RIMB
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreImpRimb().getPreImpRimbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-IMP-RIMB
            ws.getIndPrest().setImpRimb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-IMP-RIMB
            ws.getIndPrest().setImpRimb(((short)0));
        }
        // COB_CODE: IF PRE-COD-DVS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-COD-DVS
        //           ELSE
        //              MOVE 0 TO IND-PRE-COD-DVS
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreCodDvsFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-COD-DVS
            ws.getIndPrest().setCodDvs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-COD-DVS
            ws.getIndPrest().setCodDvs(((short)0));
        }
        // COB_CODE: IF PRE-MOD-INTR-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-MOD-INTR-PREST
        //           ELSE
        //              MOVE 0 TO IND-PRE-MOD-INTR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreModIntrPrestFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-MOD-INTR-PREST
            ws.getIndPrest().setModIntrPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-MOD-INTR-PREST
            ws.getIndPrest().setModIntrPrest(((short)0));
        }
        // COB_CODE: IF PRE-SPE-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-SPE-PREST
        //           ELSE
        //              MOVE 0 TO IND-PRE-SPE-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreSpePrest().getPreSpePrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-SPE-PREST
            ws.getIndPrest().setSpePrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-SPE-PREST
            ws.getIndPrest().setSpePrest(((short)0));
        }
        // COB_CODE: IF PRE-IMP-PREST-LIQTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-IMP-PREST-LIQTO
        //           ELSE
        //              MOVE 0 TO IND-PRE-IMP-PREST-LIQTO
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreImpPrestLiqto().getPreImpPrestLiqtoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-IMP-PREST-LIQTO
            ws.getIndPrest().setImpPrestLiqto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-IMP-PREST-LIQTO
            ws.getIndPrest().setImpPrestLiqto(((short)0));
        }
        // COB_CODE: IF PRE-SDO-INTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-SDO-INTR
        //           ELSE
        //              MOVE 0 TO IND-PRE-SDO-INTR
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreSdoIntr().getPreSdoIntrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-SDO-INTR
            ws.getIndPrest().setSdoIntr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-SDO-INTR
            ws.getIndPrest().setSdoIntr(((short)0));
        }
        // COB_CODE: IF PRE-RIMB-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-RIMB-EFF
        //           ELSE
        //              MOVE 0 TO IND-PRE-RIMB-EFF
        //           END-IF
        if (Characters.EQ_HIGH.test(prest.getPreRimbEff().getPreRimbEffNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-RIMB-EFF
            ws.getIndPrest().setRimbEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-RIMB-EFF
            ws.getIndPrest().setRimbEff(((short)0));
        }
        // COB_CODE: IF PRE-PREST-RES-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PRE-PREST-RES-EFF
        //           ELSE
        //              MOVE 0 TO IND-PRE-PREST-RES-EFF
        //           END-IF.
        if (Characters.EQ_HIGH.test(prest.getPrePrestResEff().getPrePrestResEffNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PRE-PREST-RES-EFF
            ws.getIndPrest().setPrestResEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PRE-PREST-RES-EFF
            ws.getIndPrest().setPrestResEff(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : PRE-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE PREST TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(prest.getPrestFormatted());
        // COB_CODE: MOVE PRE-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(prest.getPreIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO PRE-ID-MOVI-CHIU
                prest.getPreIdMoviChiu().setPreIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO PRE-DS-TS-END-CPTZ
                prest.setPreDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO PRE-ID-MOVI-CRZ
                    prest.setPreIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO PRE-ID-MOVI-CHIU-NULL
                    prest.getPreIdMoviChiu().setPreIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreIdMoviChiu.Len.PRE_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO PRE-DT-END-EFF
                    prest.setPreDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO PRE-DS-TS-INI-CPTZ
                    prest.setPreDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO PRE-DS-TS-END-CPTZ
                    prest.setPreDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO PREST.
        prest.setPrestFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO PRE-ID-MOVI-CRZ.
        prest.setPreIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO PRE-ID-MOVI-CHIU-NULL.
        prest.getPreIdMoviChiu().setPreIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreIdMoviChiu.Len.PRE_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO PRE-DT-INI-EFF.
        prest.setPreDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO PRE-DT-END-EFF.
        prest.setPreDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO PRE-DS-TS-INI-CPTZ.
        prest.setPreDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO PRE-DS-TS-END-CPTZ.
        prest.setPreDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO PRE-COD-COMP-ANIA.
        prest.setPreCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE PRE-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(prest.getPreDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO PRE-DT-INI-EFF-DB
        ws.getPrestDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE PRE-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(prest.getPreDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO PRE-DT-END-EFF-DB
        ws.getPrestDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-PRE-DT-CONCS-PREST = 0
        //               MOVE WS-DATE-X      TO PRE-DT-CONCS-PREST-DB
        //           END-IF
        if (ws.getIndPrest().getDtConcsPrest() == 0) {
            // COB_CODE: MOVE PRE-DT-CONCS-PREST TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(prest.getPreDtConcsPrest().getPreDtConcsPrest(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PRE-DT-CONCS-PREST-DB
            ws.getPrestDb().setConcsPrestDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PRE-DT-DECOR-PREST = 0
        //               MOVE WS-DATE-X      TO PRE-DT-DECOR-PREST-DB
        //           END-IF
        if (ws.getIndPrest().getDtDecorPrest() == 0) {
            // COB_CODE: MOVE PRE-DT-DECOR-PREST TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(prest.getPreDtDecorPrest().getPreDtDecorPrest(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PRE-DT-DECOR-PREST-DB
            ws.getPrestDb().setDecorPrestDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PRE-DT-RIMB = 0
        //               MOVE WS-DATE-X      TO PRE-DT-RIMB-DB
        //           END-IF
        if (ws.getIndPrest().getDtRimb() == 0) {
            // COB_CODE: MOVE PRE-DT-RIMB TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(prest.getPreDtRimb().getPreDtRimb(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PRE-DT-RIMB-DB
            ws.getPrestDb().setRimbDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: MOVE PRE-DT-RICH-PREST TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(prest.getPreDtRichPrest(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO PRE-DT-RICH-PREST-DB.
        ws.getPrestDb().setRichPrestDb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE PRE-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPrestDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-INI-EFF
        prest.setPreDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE PRE-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPrestDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-END-EFF
        prest.setPreDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-PRE-DT-CONCS-PREST = 0
        //               MOVE WS-DATE-N      TO PRE-DT-CONCS-PREST
        //           END-IF
        if (ws.getIndPrest().getDtConcsPrest() == 0) {
            // COB_CODE: MOVE PRE-DT-CONCS-PREST-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPrestDb().getConcsPrestDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-CONCS-PREST
            prest.getPreDtConcsPrest().setPreDtConcsPrest(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PRE-DT-DECOR-PREST = 0
        //               MOVE WS-DATE-N      TO PRE-DT-DECOR-PREST
        //           END-IF
        if (ws.getIndPrest().getDtDecorPrest() == 0) {
            // COB_CODE: MOVE PRE-DT-DECOR-PREST-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPrestDb().getDecorPrestDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-DECOR-PREST
            prest.getPreDtDecorPrest().setPreDtDecorPrest(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PRE-DT-RIMB = 0
        //               MOVE WS-DATE-N      TO PRE-DT-RIMB
        //           END-IF
        if (ws.getIndPrest().getDtRimb() == 0) {
            // COB_CODE: MOVE PRE-DT-RIMB-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPrestDb().getRimbDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-RIMB
            prest.getPreDtRimb().setPreDtRimb(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE PRE-DT-RICH-PREST-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPrestDb().getRichPrestDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-RICH-PREST.
        prest.setPreDtRichPrest(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return prest.getPreCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.prest.setPreCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return prest.getPreCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.prest.setPreCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndPrest().getCodDvs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndPrest().setCodDvs(((short)0));
        }
        else {
            ws.getIndPrest().setCodDvs(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return prest.getPreDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.prest.setPreDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return prest.getPreDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.prest.setPreDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return prest.getPreDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.prest.setPreDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return prest.getPreDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.prest.setPreDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return prest.getPreDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.prest.setPreDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return prest.getPreDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.prest.setPreDsVer(dsVer);
    }

    @Override
    public String getDtConcsPrestDb() {
        return ws.getPrestDb().getConcsPrestDb();
    }

    @Override
    public void setDtConcsPrestDb(String dtConcsPrestDb) {
        this.ws.getPrestDb().setConcsPrestDb(dtConcsPrestDb);
    }

    @Override
    public String getDtConcsPrestDbObj() {
        if (ws.getIndPrest().getDtConcsPrest() >= 0) {
            return getDtConcsPrestDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtConcsPrestDbObj(String dtConcsPrestDbObj) {
        if (dtConcsPrestDbObj != null) {
            setDtConcsPrestDb(dtConcsPrestDbObj);
            ws.getIndPrest().setDtConcsPrest(((short)0));
        }
        else {
            ws.getIndPrest().setDtConcsPrest(((short)-1));
        }
    }

    @Override
    public String getDtDecorPrestDb() {
        return ws.getPrestDb().getDecorPrestDb();
    }

    @Override
    public void setDtDecorPrestDb(String dtDecorPrestDb) {
        this.ws.getPrestDb().setDecorPrestDb(dtDecorPrestDb);
    }

    @Override
    public String getDtDecorPrestDbObj() {
        if (ws.getIndPrest().getDtDecorPrest() >= 0) {
            return getDtDecorPrestDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorPrestDbObj(String dtDecorPrestDbObj) {
        if (dtDecorPrestDbObj != null) {
            setDtDecorPrestDb(dtDecorPrestDbObj);
            ws.getIndPrest().setDtDecorPrest(((short)0));
        }
        else {
            ws.getIndPrest().setDtDecorPrest(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getPrestDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getPrestDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getPrestDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getPrestDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtRichPrestDb() {
        return ws.getPrestDb().getRichPrestDb();
    }

    @Override
    public void setDtRichPrestDb(String dtRichPrestDb) {
        this.ws.getPrestDb().setRichPrestDb(dtRichPrestDb);
    }

    @Override
    public String getDtRimbDb() {
        return ws.getPrestDb().getRimbDb();
    }

    @Override
    public void setDtRimbDb(String dtRimbDb) {
        this.ws.getPrestDb().setRimbDb(dtRimbDb);
    }

    @Override
    public String getDtRimbDbObj() {
        if (ws.getIndPrest().getDtRimb() >= 0) {
            return getDtRimbDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRimbDbObj(String dtRimbDbObj) {
        if (dtRimbDbObj != null) {
            setDtRimbDb(dtRimbDbObj);
            ws.getIndPrest().setDtRimb(((short)0));
        }
        else {
            ws.getIndPrest().setDtRimb(((short)-1));
        }
    }

    @Override
    public int getFrazPagIntr() {
        return prest.getPreFrazPagIntr().getPreFrazPagIntr();
    }

    @Override
    public void setFrazPagIntr(int frazPagIntr) {
        this.prest.getPreFrazPagIntr().setPreFrazPagIntr(frazPagIntr);
    }

    @Override
    public Integer getFrazPagIntrObj() {
        if (ws.getIndPrest().getFrazPagIntr() >= 0) {
            return ((Integer)getFrazPagIntr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazPagIntrObj(Integer frazPagIntrObj) {
        if (frazPagIntrObj != null) {
            setFrazPagIntr(((int)frazPagIntrObj));
            ws.getIndPrest().setFrazPagIntr(((short)0));
        }
        else {
            ws.getIndPrest().setFrazPagIntr(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return prest.getPreIdMoviChiu().getPreIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.prest.getPreIdMoviChiu().setPreIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndPrest().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndPrest().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndPrest().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return prest.getPreIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.prest.setPreIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPrest() {
        return prest.getPreIdPrest();
    }

    @Override
    public void setIdPrest(int idPrest) {
        this.prest.setPreIdPrest(idPrest);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpPrest() {
        return prest.getPreImpPrest().getPreImpPrest();
    }

    @Override
    public void setImpPrest(AfDecimal impPrest) {
        this.prest.getPreImpPrest().setPreImpPrest(impPrest.copy());
    }

    @Override
    public AfDecimal getImpPrestLiqto() {
        return prest.getPreImpPrestLiqto().getPreImpPrestLiqto();
    }

    @Override
    public void setImpPrestLiqto(AfDecimal impPrestLiqto) {
        this.prest.getPreImpPrestLiqto().setPreImpPrestLiqto(impPrestLiqto.copy());
    }

    @Override
    public AfDecimal getImpPrestLiqtoObj() {
        if (ws.getIndPrest().getImpPrestLiqto() >= 0) {
            return getImpPrestLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPrestLiqtoObj(AfDecimal impPrestLiqtoObj) {
        if (impPrestLiqtoObj != null) {
            setImpPrestLiqto(new AfDecimal(impPrestLiqtoObj, 15, 3));
            ws.getIndPrest().setImpPrestLiqto(((short)0));
        }
        else {
            ws.getIndPrest().setImpPrestLiqto(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPrestObj() {
        if (ws.getIndPrest().getImpPrest() >= 0) {
            return getImpPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPrestObj(AfDecimal impPrestObj) {
        if (impPrestObj != null) {
            setImpPrest(new AfDecimal(impPrestObj, 15, 3));
            ws.getIndPrest().setImpPrest(((short)0));
        }
        else {
            ws.getIndPrest().setImpPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRimb() {
        return prest.getPreImpRimb().getPreImpRimb();
    }

    @Override
    public void setImpRimb(AfDecimal impRimb) {
        this.prest.getPreImpRimb().setPreImpRimb(impRimb.copy());
    }

    @Override
    public AfDecimal getImpRimbObj() {
        if (ws.getIndPrest().getImpRimb() >= 0) {
            return getImpRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRimbObj(AfDecimal impRimbObj) {
        if (impRimbObj != null) {
            setImpRimb(new AfDecimal(impRimbObj, 15, 3));
            ws.getIndPrest().setImpRimb(((short)0));
        }
        else {
            ws.getIndPrest().setImpRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrPrest() {
        return prest.getPreIntrPrest().getPreIntrPrest();
    }

    @Override
    public void setIntrPrest(AfDecimal intrPrest) {
        this.prest.getPreIntrPrest().setPreIntrPrest(intrPrest.copy());
    }

    @Override
    public AfDecimal getIntrPrestObj() {
        if (ws.getIndPrest().getIntrPrest() >= 0) {
            return getIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrPrestObj(AfDecimal intrPrestObj) {
        if (intrPrestObj != null) {
            setIntrPrest(new AfDecimal(intrPrestObj, 6, 3));
            ws.getIndPrest().setIntrPrest(((short)0));
        }
        else {
            ws.getIndPrest().setIntrPrest(((short)-1));
        }
    }

    @Override
    public int getLdbv2821IdOgg() {
        throw new FieldNotMappedException("ldbv2821IdOgg");
    }

    @Override
    public void setLdbv2821IdOgg(int ldbv2821IdOgg) {
        throw new FieldNotMappedException("ldbv2821IdOgg");
    }

    @Override
    public AfDecimal getLdbv2821ImpPrest() {
        throw new FieldNotMappedException("ldbv2821ImpPrest");
    }

    @Override
    public void setLdbv2821ImpPrest(AfDecimal ldbv2821ImpPrest) {
        throw new FieldNotMappedException("ldbv2821ImpPrest");
    }

    @Override
    public AfDecimal getLdbv2821ImpPrestObj() {
        return getLdbv2821ImpPrest();
    }

    @Override
    public void setLdbv2821ImpPrestObj(AfDecimal ldbv2821ImpPrestObj) {
        setLdbv2821ImpPrest(new AfDecimal(ldbv2821ImpPrestObj, 15, 3));
    }

    @Override
    public String getLdbv2821TpOgg() {
        throw new FieldNotMappedException("ldbv2821TpOgg");
    }

    @Override
    public void setLdbv2821TpOgg(String ldbv2821TpOgg) {
        throw new FieldNotMappedException("ldbv2821TpOgg");
    }

    @Override
    public String getModIntrPrest() {
        return prest.getPreModIntrPrest();
    }

    @Override
    public void setModIntrPrest(String modIntrPrest) {
        this.prest.setPreModIntrPrest(modIntrPrest);
    }

    @Override
    public String getModIntrPrestObj() {
        if (ws.getIndPrest().getModIntrPrest() >= 0) {
            return getModIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModIntrPrestObj(String modIntrPrestObj) {
        if (modIntrPrestObj != null) {
            setModIntrPrest(modIntrPrestObj);
            ws.getIndPrest().setModIntrPrest(((short)0));
        }
        else {
            ws.getIndPrest().setModIntrPrest(((short)-1));
        }
    }

    @Override
    public long getPreDsRiga() {
        return prest.getPreDsRiga();
    }

    @Override
    public void setPreDsRiga(long preDsRiga) {
        this.prest.setPreDsRiga(preDsRiga);
    }

    @Override
    public int getPreIdOgg() {
        return prest.getPreIdOgg();
    }

    @Override
    public void setPreIdOgg(int preIdOgg) {
        this.prest.setPreIdOgg(preIdOgg);
    }

    @Override
    public String getPreTpOgg() {
        return prest.getPreTpOgg();
    }

    @Override
    public void setPreTpOgg(String preTpOgg) {
        this.prest.setPreTpOgg(preTpOgg);
    }

    @Override
    public AfDecimal getPrestResEff() {
        return prest.getPrePrestResEff().getPrePrestResEff();
    }

    @Override
    public void setPrestResEff(AfDecimal prestResEff) {
        this.prest.getPrePrestResEff().setPrePrestResEff(prestResEff.copy());
    }

    @Override
    public AfDecimal getPrestResEffObj() {
        if (ws.getIndPrest().getPrestResEff() >= 0) {
            return getPrestResEff();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrestResEffObj(AfDecimal prestResEffObj) {
        if (prestResEffObj != null) {
            setPrestResEff(new AfDecimal(prestResEffObj, 15, 3));
            ws.getIndPrest().setPrestResEff(((short)0));
        }
        else {
            ws.getIndPrest().setPrestResEff(((short)-1));
        }
    }

    @Override
    public AfDecimal getRimbEff() {
        return prest.getPreRimbEff().getPreRimbEff();
    }

    @Override
    public void setRimbEff(AfDecimal rimbEff) {
        this.prest.getPreRimbEff().setPreRimbEff(rimbEff.copy());
    }

    @Override
    public AfDecimal getRimbEffObj() {
        if (ws.getIndPrest().getRimbEff() >= 0) {
            return getRimbEff();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRimbEffObj(AfDecimal rimbEffObj) {
        if (rimbEffObj != null) {
            setRimbEff(new AfDecimal(rimbEffObj, 15, 3));
            ws.getIndPrest().setRimbEff(((short)0));
        }
        else {
            ws.getIndPrest().setRimbEff(((short)-1));
        }
    }

    @Override
    public AfDecimal getSdoIntr() {
        return prest.getPreSdoIntr().getPreSdoIntr();
    }

    @Override
    public void setSdoIntr(AfDecimal sdoIntr) {
        this.prest.getPreSdoIntr().setPreSdoIntr(sdoIntr.copy());
    }

    @Override
    public AfDecimal getSdoIntrObj() {
        if (ws.getIndPrest().getSdoIntr() >= 0) {
            return getSdoIntr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSdoIntrObj(AfDecimal sdoIntrObj) {
        if (sdoIntrObj != null) {
            setSdoIntr(new AfDecimal(sdoIntrObj, 15, 3));
            ws.getIndPrest().setSdoIntr(((short)0));
        }
        else {
            ws.getIndPrest().setSdoIntr(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpePrest() {
        return prest.getPreSpePrest().getPreSpePrest();
    }

    @Override
    public void setSpePrest(AfDecimal spePrest) {
        this.prest.getPreSpePrest().setPreSpePrest(spePrest.copy());
    }

    @Override
    public AfDecimal getSpePrestObj() {
        if (ws.getIndPrest().getSpePrest() >= 0) {
            return getSpePrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpePrestObj(AfDecimal spePrestObj) {
        if (spePrestObj != null) {
            setSpePrest(new AfDecimal(spePrestObj, 15, 3));
            ws.getIndPrest().setSpePrest(((short)0));
        }
        else {
            ws.getIndPrest().setSpePrest(((short)-1));
        }
    }

    @Override
    public String getTpPrest() {
        return prest.getPreTpPrest();
    }

    @Override
    public void setTpPrest(String tpPrest) {
        this.prest.setPreTpPrest(tpPrest);
    }

    @Override
    public String getTpPrestObj() {
        if (ws.getIndPrest().getTpPrest() >= 0) {
            return getTpPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPrestObj(String tpPrestObj) {
        if (tpPrestObj != null) {
            setTpPrest(tpPrestObj);
            ws.getIndPrest().setTpPrest(((short)0));
        }
        else {
            ws.getIndPrest().setTpPrest(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
