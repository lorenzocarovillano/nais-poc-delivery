package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaIoLccs0024;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Lccs0024Data;
import it.accenture.jnais.ws.redefines.L11IdRich;

/**Original name: LCCS0024<br>
 * <pre>****************************************************************
 * *                                                              *
 * *    PORTAFOGLIO VITA ITALIA VER. 1.0                          *
 * *                                                              *
 * ****************************************************************
 * AUTHOR.             ATS/NA.
 * DATE-WRITTEN.       2008.
 * DATE-COMPILED.
 * *--------------------------------------------------------------**
 *     PROGRAMMA ..... LCCS0024
 *     TIPOLOGIA...... Componenti Comuni
 *     PROCESSO.......
 *     FUNZIONE....... CREAZIONE OGGETTO BLOCCO
 *     DESCRIZIONE....
 * *--------------------------------------------------------------**</pre>*/
public class Lccs0024 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0024Data ws = new Lccs0024Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: AREA-IO-LCCS0024
    private AreaIoLccs0024 areaIoLccs0024;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0024_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, AreaIoLccs0024 areaIoLccs0024) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.areaIoLccs0024 = areaIoLccs0024;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU S0000-EX.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lccs0024 getInstance() {
        return ((Lccs0024)Programs.getInstance(Lccs0024.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------
	 *     OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE LCCC0024-TP-OGGETTO           TO WS-TP-OGG
        ws.getWsTpOgg().setWsTpOgg(areaIoLccs0024.getDatiInput().getLccc0024TpOggetto());
        // COB_CODE: PERFORM S0005-CTRL-DATI-INPUT
        //              THRU S0005-EX.
        s0005CtrlDatiInput();
    }

    /**Original name: S0005-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLI SUI DATI DI INPUT DEL SERVIZIO
	 * ----------------------------------------------------------------*
	 * --  il campo ID-OGGETTO deve essere un numerico</pre>*/
    private void s0005CtrlDatiInput() {
        // COB_CODE: IF LCCC0024-ID-OGGETTO NOT NUMERIC
        //                 THRU EX-S0300
        //           END-IF
        if (!Functions.isNumber(areaIoLccs0024.getDatiInput().getLccc0024IdOggetto())) {
            // COB_CODE: MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT' TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
            // COB_CODE: MOVE '005076'                TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005076");
            // COB_CODE: MOVE 'LCCC0024-ID-OGGETTO'   TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("LCCC0024-ID-OGGETTO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *--     il campo ID-OGGETTO deve essere un numerico > zero
        //                   END-IF
        //                END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //--     il campo ID-OGGETTO deve essere un numerico > zero
            // COB_CODE: IF LCCC0024-ID-OGGETTO NOT > ZERO
            //                 THRU EX-S0300
            //           END-IF
            if (areaIoLccs0024.getDatiInput().getLccc0024IdOggetto() <= 0) {
                // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                //                                      TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                // COB_CODE: MOVE '005018'              TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005018");
                // COB_CODE: MOVE 'LCCC0024-ID-OGGETTO' TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("LCCC0024-ID-OGGETTO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        //--  il campo TP-OGGETTO deve essere diverso da spaces e deve
        //--  essere un valore compreso nei valori di dominio
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF LCCC0024-TP-OGGETTO = SPACES
            //                                OR HIGH-VALUE OR LOW-VALUE
            //                 THRU EX-S0300
            //           END-IF
            if (Characters.EQ_SPACE.test(areaIoLccs0024.getDatiInput().getLccc0024TpOggetto()) || Characters.EQ_HIGH.test(areaIoLccs0024.getDatiInput().getLccc0024TpOggettoFormatted()) || Characters.EQ_LOW.test(areaIoLccs0024.getDatiInput().getLccc0024TpOggettoFormatted())) {
                // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                //                                      TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                // COB_CODE: MOVE '005007'              TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005007");
                // COB_CODE: MOVE 'LCCC0024-TP-OGGETTO' TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("LCCC0024-TP-OGGETTO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF  (NOT POLIZZA)
            //           AND (NOT ADESIONE)
            //                 THRU EX-S0300
            //           END-IF
            if (!ws.getWsTpOgg().isPolizza() && !ws.getWsTpOgg().isAdesione()) {
                // COB_CODE: MOVE WK-PGM  TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0020-CTRL-DATI-INPUT' TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0020-CTRL-DATI-INPUT");
                // COB_CODE: MOVE '005018'                TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005018");
                // COB_CODE: MOVE 'TIPO COMUNICAZIONE'    TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("TIPO COMUNICAZIONE");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        // COB_CODE: IF  IDSV0001-ESITO-OK
        //           AND ADESIONE
        //               END-IF
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getWsTpOgg().isAdesione()) {
            // COB_CODE: IF LCCC0024-ID-OGG-PRIM NOT NUMERIC
            //                 THRU EX-S0300
            //           END-IF
            if (!Functions.isNumber(areaIoLccs0024.getDatiInput().getLccc0024IdOggPrim())) {
                // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                //                                      TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                // COB_CODE: MOVE '005076'              TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005076");
                // COB_CODE: MOVE 'LCCC0024-ID-OGG-PRIM'
                //                                      TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("LCCC0024-ID-OGG-PRIM");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
            //--      il campo ID-OGGETTO PRIMARIO deve essere
            //--      un numerico > zero
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: IF LCCC0024-ID-OGG-PRIM NOT > ZERO
                //                 THRU EX-S0300
                //           END-IF
                if (areaIoLccs0024.getDatiInput().getLccc0024IdOggPrim() <= 0) {
                    // COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                    //                                   TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                    // COB_CODE: MOVE '005018'           TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005018");
                    // COB_CODE: MOVE 'LCCC0024-ID-OGG-PRIM'
                    //                                   TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("LCCC0024-ID-OGG-PRIM");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
        }
        //--  il campo COD-BLOCCO deve essere diverso da spaces e da null
        //--  e deve essere un valore compreso nei valori di dominio
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF LCCC0024-COD-BLOCCO = SPACES
            //                                OR HIGH-VALUE OR LOW-VALUE
            //                 THRU EX-S0300
            //           END-IF
            if (Characters.EQ_SPACE.test(areaIoLccs0024.getDatiInput().getLccc0024CodBlocco()) || Characters.EQ_HIGH.test(areaIoLccs0024.getDatiInput().getLccc0024CodBloccoFormatted()) || Characters.EQ_LOW.test(areaIoLccs0024.getDatiInput().getLccc0024CodBloccoFormatted())) {
                // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                //                                      TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                // COB_CODE: MOVE '005007'              TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005007");
                // COB_CODE: MOVE 'LCCC0024-COD-BLOCCO' TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("LCCC0024-COD-BLOCCO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU S0006-EX
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM S0006-CTRL-COD-BLOCCO
                //              THRU S0006-EX
                s0006CtrlCodBlocco();
            }
        }
    }

    /**Original name: S0006-CTRL-COD-BLOCCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA ESISTENZA BLOCCO SULLA TABELLA ANAG_BLOCCO
	 * ----------------------------------------------------------------*</pre>*/
    private void s0006CtrlCodBlocco() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'ANA-BLOCCO'           TO WK-TABELLA.
        ws.setWkTabella("ANA-BLOCCO");
        //--> VALORIZZAZIONE DELLA CHIAVE DI LETTURA
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA    TO XAB-COD-COMP-ANIA
        ws.getAnaBlocco().setCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE LCCC0024-COD-BLOCCO            TO XAB-COD-BLOCCO
        ws.getAnaBlocco().setCodBlocco(areaIoLccs0024.getDatiInput().getLccc0024CodBlocco());
        //--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
        // COB_CODE: MOVE ZERO                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                          IDSI0011-DATA-FINE-EFFETTO
        //                                          IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR  TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE WK-TABELLA             TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE ANA-BLOCCO             TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAnaBlocco().getAnaBloccoFormatted());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-SELECT         TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-PRIMARY-KEY    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *-->    GESTIRE ERRORE DB
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIRE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->    GESTIRE ERRORE DB
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-NOT-FOUND
            //                     THRU EX-S0300
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                  MOVE IDSO0011-BUFFER-DATI TO ANA-BLOCCO
            //               WHEN OTHER
            //                     THRU EX-S0300
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S0006-CTRL-COD-BLOCCO'
                    //                              TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S0006-CTRL-COD-BLOCCO");
                    // COB_CODE: MOVE '005201'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005201");
                    // COB_CODE: STRING 'CODICE BLOCCO = '
                    //                  LCCC0024-COD-BLOCCO
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "CODICE BLOCCO = ", areaIoLccs0024.getDatiInput().getLccc0024CodBloccoFormatted());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI TO ANA-BLOCCO
                    ws.getAnaBlocco().setAnaBloccoFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default:// COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S0006-CTRL-COD-BLOCCO'
                    //                              TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S0006-CTRL-COD-BLOCCO");
                    // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S0006-CTRL-COD-BLOCCO' TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S0006-CTRL-COD-BLOCCO");
            // COB_CODE: MOVE '005016'                TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                    DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 *     PERFORM S1005-VERIF-PRESENZA-BLOCCO
	 *        THRU S1005-EX</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *       IF FL-BLOCCO-PTF-NO
        //                         THRU S1010-EX
        //           *       END-IF
        //                END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //       IF FL-BLOCCO-PTF-NO
            // COB_CODE: PERFORM S1010-CENSIMENTO-BLOCCO
            //              THRU S1010-EX
            s1010CensimentoBlocco();
            //       END-IF
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF LCCC0024-COD-BLOCCO = 'DISPO'
            //                 THRU S1015-EX
            //           END-IF
            if (Conditions.eq(areaIoLccs0024.getDatiInput().getLccc0024CodBlocco(), "DISPO")) {
                // COB_CODE: PERFORM S1015-GEST-MOVI-SOSPESO
                //              THRU S1015-EX
                s1015GestMoviSospeso();
            }
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU S1020-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1020-VALORIZZA-OUTPUT
            //              THRU S1020-EX
            s1020ValorizzaOutput();
        }
    }

    /**Original name: S1010-CENSIMENTO-BLOCCO<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1010CensimentoBlocco() {
        // COB_CODE: PERFORM S1011-VAL-OGG-BLOCCO
        //              THRU S1011-EX
        s1011ValOggBlocco();
        // COB_CODE: PERFORM S1012-INSERT-OGG-BLOCCO
        //              THRU S1012-EX.
        s1012InsertOggBlocco();
    }

    /**Original name: S1011-VAL-OGG-BLOCCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE AREA OGGETTO BLOCCO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1011ValOggBlocco() {
        // COB_CODE: MOVE 'OGG-BLOCCO'           TO WK-TABELLA
        ws.setWkTabella("OGG-BLOCCO");
        // COB_CODE: PERFORM ESTR-SEQUENCE
        //              THRU ESTR-SEQUENCE-EX
        estrSequence();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              MOVE IDSV0001-DATA-EFFETTO       TO L11-DT-EFF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE S090-SEQ-TABELLA            TO L11-ID-OGG-BLOCCO
            ws.getOggBlocco().setL11IdOggBlocco(ws.getAreaIoLccs0090().getSeqTabella());
            // COB_CODE: MOVE LCCC0024-ID-OGG-PRIM        TO L11-ID-OGG-1RIO
            ws.getOggBlocco().getL11IdOgg1rio().setL11IdOgg1rio(areaIoLccs0024.getDatiInput().getLccc0024IdOggPrim());
            // COB_CODE: MOVE LCCC0024-TP-OGG-PRIM        TO L11-TP-OGG-1RIO
            ws.getOggBlocco().setL11TpOgg1rio(areaIoLccs0024.getDatiInput().getLccc0024TpOggPrim());
            // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO L11-COD-COMP-ANIA
            ws.getOggBlocco().setL11CodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
            // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO     TO L11-TP-MOVI
            ws.getOggBlocco().setL11TpMovi(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
            // COB_CODE: MOVE LCCC0024-ID-OGGETTO         TO L11-ID-OGG
            ws.getOggBlocco().setL11IdOgg(areaIoLccs0024.getDatiInput().getLccc0024IdOggetto());
            // COB_CODE: MOVE LCCC0024-TP-OGGETTO         TO L11-TP-OGG
            ws.getOggBlocco().setL11TpOgg(areaIoLccs0024.getDatiInput().getLccc0024TpOggetto());
            // COB_CODE: MOVE LCCC0024-COD-BLOCCO         TO L11-COD-BLOCCO
            ws.getOggBlocco().setL11CodBlocco(areaIoLccs0024.getDatiInput().getLccc0024CodBlocco());
            // COB_CODE: IF LCCC0024-ID-RICH IS NOT NUMERIC OR
            //              LCCC0024-ID-RICH = ZERO
            //              MOVE HIGH-VALUE               TO L11-ID-RICH-NULL
            //           ELSE
            //              MOVE LCCC0024-ID-RICH         TO L11-ID-RICH
            //           END-IF
            if (!Functions.isNumber(areaIoLccs0024.getDatiInput().getLccc0024IdRich()) || areaIoLccs0024.getDatiInput().getLccc0024IdRich() == 0) {
                // COB_CODE: MOVE HIGH-VALUE               TO L11-ID-RICH-NULL
                ws.getOggBlocco().getL11IdRich().setL11IdRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, L11IdRich.Len.L11_ID_RICH_NULL));
            }
            else {
                // COB_CODE: MOVE LCCC0024-ID-RICH         TO L11-ID-RICH
                ws.getOggBlocco().getL11IdRich().setL11IdRich(areaIoLccs0024.getDatiInput().getLccc0024IdRich());
            }
            // COB_CODE: SET ST-BLOCCO-ATTIVO             TO TRUE
            ws.getWsTpStatBlocco().setAttivo();
            // COB_CODE: MOVE WS-TP-STAT-BLOCCO           TO L11-TP-STAT-BLOCCO
            ws.getOggBlocco().setL11TpStatBlocco(ws.getWsTpStatBlocco().getWsTpStatBlocco());
            // COB_CODE: MOVE IDSV0001-DATA-EFFETTO       TO L11-DT-EFF
            ws.getOggBlocco().setL11DtEff(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
        }
    }

    /**Original name: S1012-INSERT-OGG-BLOCCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     INSERT OGGETTO BLOCCO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1012InsertOggBlocco() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'OGG-BLOCCO'           TO WK-TABELLA.
        ws.setWkTabella("OGG-BLOCCO");
        //--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
        // COB_CODE: MOVE ZERO                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                          IDSI0011-DATA-FINE-EFFETTO
        //                                          IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR  TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE WK-TABELLA             TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE OGG-BLOCCO             TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getOggBlocco().getOggBloccoFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-PRIMARY-KEY    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-INSERT         TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setInsert();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                  CONTINUE
            //               WHEN OTHER
            //                     THRU EX-S0300
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                    break;

                default:// COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1012-INSERT-OGG-BLOCCO'
                    //                              TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1012-INSERT-OGG-BLOCCO");
                    // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1012-INSERT-OGG-BLOCCO' TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1012-INSERT-OGG-BLOCCO");
            // COB_CODE: MOVE '005016'                  TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                    DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1015-GEST-MOVI-SOSPESO<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1015GestMoviSospeso() {
        // COB_CODE: PERFORM VARYING IX-MOV-SOSP FROM 1 BY 1
        //             UNTIL IX-MOV-SOSP > LCCC0024-ELE-MOV-SOS-MAX
        //                THRU S1017-EX
        //           END-PERFORM.
        ws.setIxMovSosp(((short)1));
        while (!(ws.getIxMovSosp() > areaIoLccs0024.getDatiInput().getLccc0024EleMovSosMax())) {
            // COB_CODE: PERFORM S1016-VAL-MOVI-SOSPESO
            //              THRU S1016-EX
            s1016ValMoviSospeso();
            // COB_CODE: PERFORM S1017-INSERT-MOVI-SOSPESO
            //              THRU S1017-EX
            s1017InsertMoviSospeso();
            ws.setIxMovSosp(Trunc.toShort(ws.getIxMovSosp() + 1, 4));
        }
    }

    /**Original name: S1016-VAL-MOVI-SOSPESO<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1016ValMoviSospeso() {
        // COB_CODE: MOVE 'MOVI-BATCH-SOSP'              TO WK-TABELLA
        ws.setWkTabella("MOVI-BATCH-SOSP");
        // COB_CODE: PERFORM ESTR-SEQUENCE
        //              THRU ESTR-SEQUENCE-EX
        estrSequence();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                                               TO MBS-D-INPUT-MOVI-SOSP
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE S090-SEQ-TABELLA            TO MBS-ID-MOVI-BATCH-SOSP
            ws.getMoviBatchSosp().setMbsIdMoviBatchSosp(ws.getAreaIoLccs0090().getSeqTabella());
            // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO MBS-COD-COMP-ANIA
            ws.getMoviBatchSosp().setMbsCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
            // COB_CODE: MOVE LCCC0024-ID-OGG-SOSP(IX-MOV-SOSP)
            //                                            TO MBS-ID-OGG
            ws.getMoviBatchSosp().setMbsIdOgg(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024IdOggSosp());
            // COB_CODE: MOVE LCCC0024-TP-OGG-SOSP(IX-MOV-SOSP)
            //                                            TO MBS-TP-OGG
            ws.getMoviBatchSosp().setMbsTpOgg(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024TpOggSosp());
            // COB_CODE: MOVE LCCC0024-ID-MOVI-SOSP(IX-MOV-SOSP)
            //                                            TO MBS-ID-MOVI
            ws.getMoviBatchSosp().setMbsIdMovi(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024IdMoviSosp());
            // COB_CODE: IF LCCC0024-TP-MOVI-SOSP-NULL(IX-MOV-SOSP) = HIGH-VALUE
            //              MOVE IDSV0001-TIPO-MOVIMENTO  TO MBS-TP-MOVI
            //           ELSE
            //                                            TO MBS-TP-MOVI
            //           END-IF
            if (Characters.EQ_HIGH.test(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024TpMoviSosp().getLccc0024TpMoviSospNullFormatted())) {
                // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO  TO MBS-TP-MOVI
                ws.getMoviBatchSosp().setMbsTpMovi(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
            }
            else {
                // COB_CODE: MOVE LCCC0024-TP-MOVI-SOSP(IX-MOV-SOSP)
                //                                         TO MBS-TP-MOVI
                ws.getMoviBatchSosp().setMbsTpMovi(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024TpMoviSosp().getLccc0024TpMoviSosp());
            }
            // COB_CODE: MOVE IDSV0001-DATA-EFFETTO       TO MBS-DT-EFF
            ws.getMoviBatchSosp().setMbsDtEff(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            // COB_CODE: MOVE L11-ID-OGG-BLOCCO           TO MBS-ID-OGG-BLOCCO
            ws.getMoviBatchSosp().setMbsIdOggBlocco(ws.getOggBlocco().getL11IdOggBlocco());
            // COB_CODE: MOVE LCCC0024-TP-FRM-ASSVA(IX-MOV-SOSP)
            //                                            TO MBS-TP-FRM-ASSVA
            ws.getMoviBatchSosp().setMbsTpFrmAssva(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024TpFrmAssva());
            // COB_CODE: IF LCCC0024-ID-BATCH-NULL(IX-MOV-SOSP) = HIGH-VALUE
            //                                            TO MBS-ID-BATCH-NULL
            //           ELSE
            //                                            TO MBS-ID-BATCH
            //           END-IF
            if (Characters.EQ_HIGH.test(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024IdBatch().getLccc0024IdBatchNullFormatted())) {
                // COB_CODE: MOVE LCCC0024-ID-BATCH-NULL(IX-MOV-SOSP)
                //                                         TO MBS-ID-BATCH-NULL
                ws.getMoviBatchSosp().getMbsIdBatch().setMbsIdBatchNull(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024IdBatch().getLccc0024IdBatchNull());
            }
            else {
                // COB_CODE: MOVE LCCC0024-ID-BATCH(IX-MOV-SOSP)
                //                                         TO MBS-ID-BATCH
                ws.getMoviBatchSosp().getMbsIdBatch().setMbsIdBatch(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024IdBatch().getLccc0024IdBatch());
            }
            // COB_CODE: IF LCCC0024-ID-JOB-NULL(IX-MOV-SOSP) = HIGH-VALUE
            //                                            TO MBS-ID-JOB-NULL
            //           ELSE
            //                                            TO MBS-ID-JOB
            //           END-IF
            if (Characters.EQ_HIGH.test(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024IdJob().getLccc0024IdJobNullFormatted())) {
                // COB_CODE: MOVE LCCC0024-ID-JOB-NULL(IX-MOV-SOSP)
                //                                         TO MBS-ID-JOB-NULL
                ws.getMoviBatchSosp().getMbsIdJob().setMbsIdJobNull(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024IdJob().getLccc0024IdJobNull());
            }
            else {
                // COB_CODE: MOVE LCCC0024-ID-JOB(IX-MOV-SOSP)
                //                                         TO MBS-ID-JOB
                ws.getMoviBatchSosp().getMbsIdJob().setMbsIdJob(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024IdJob().getLccc0024IdJob());
            }
            // COB_CODE: MOVE LCCC0024-STEP-ELAB(IX-MOV-SOSP)
            //                                            TO MBS-STEP-ELAB
            ws.getMoviBatchSosp().setMbsStepElab(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024StepElab());
            // COB_CODE: MOVE 'S'                         TO MBS-FL-MOVI-SOSP
            ws.getMoviBatchSosp().setMbsFlMoviSospFormatted("S");
            // COB_CODE: MOVE LCCC0024-D-INPUT-MOVI-SOSP(IX-MOV-SOSP)
            //                                            TO MBS-D-INPUT-MOVI-SOSP
            ws.getMoviBatchSosp().setMbsDInputMoviSosp(areaIoLccs0024.getDatiInput().getLccc0024DatiMovSospesi(ws.getIxMovSosp()).getLccc0024DInputMoviSosp());
        }
    }

    /**Original name: S1017-INSERT-MOVI-SOSPESO<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1017InsertMoviSospeso() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE ZERO                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                          IDSI0011-DATA-FINE-EFFETTO
        //                                          IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR  TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: MOVE WK-TABELLA             TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: MOVE MOVI-BATCH-SOSP        TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getMoviBatchSosp().getMoviBatchSospFormatted());
        // COB_CODE: SET IDSI0011-PRIMARY-KEY    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSI0011-INSERT         TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setInsert();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                  CONTINUE
            //               WHEN OTHER
            //                     THRU EX-S0300
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                    break;

                default:// COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1017-INSERT-MOVI-SOSPESO'
                    //                              TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1017-INSERT-MOVI-SOSPESO");
                    // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1017-INSERT-MOVI-SOSPESO'
            //                                          TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1017-INSERT-MOVI-SOSPESO");
            // COB_CODE: MOVE '005016'                  TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                    DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1020-VALORIZZA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     OUTPUT DEL SERVIZIO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1020ValorizzaOutput() {
        // COB_CODE: MOVE L11-ID-OGG-BLOCCO TO LCCC0024-ID-OGG-BLOCCO.
        areaIoLccs0024.setIdOggBlocco(ws.getOggBlocco().getL11IdOggBlocco());
        //    IF FL-BLOCCO-PTF-SI
        //       SET LCCC0024-BLC-PRESENTE-PTF TO TRUE
        //    END-IF
        //    IF FL-BLOCCO-PTF-NO
        //       SET LCCC0024-BLC-CENSITO-PTF TO TRUE
        //    END-IF.
        // COB_CODE: SET LCCC0024-BLC-CENSITO-PTF TO TRUE.
        areaIoLccs0024.getVerificaBlocco().setCensitoPtf();
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES GESTIONE ERRORI
	 * ----------------------------------------------------------------*
	 * MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>**-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: ESTR-SEQUENCE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     COPY      ..... LCCP0002
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*</pre>*/
    private void estrSequence() {
        Lccs0090 lccs0090 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA TO S090-NOME-TABELLA.
        ws.getAreaIoLccs0090().setNomeTabella(ws.getWkTabella());
        // COB_CODE: CALL LCCS0090 USING AREA-IO-LCCS0090
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0090 = Lccs0090.getInstance();
            lccs0090.run(ws.getAreaIoLccs0090());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO ESTRAZIONE SEQUENCE'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO ESTRAZIONE SEQUENCE");
            // COB_CODE: MOVE 'ESTR-SEQUENCE'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("ESTR-SEQUENCE");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-EVALUATE
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         EVALUATE S090-RETURN-CODE
            //                       WHEN '00'
            //                            CONTINUE
            //                       WHEN 'S1'
            //           *-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
            //                             THRU EX-S0300
            //                       WHEN 'D3'
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getAreaIoLccs0090().getReturnCode().getReturnCode()) {

                case "00":// COB_CODE: CONTINUE
                //continue
                    break;

                case "S1"://-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005015'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case "D3"://-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005016'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default:break;
            }
        }
    }
}
