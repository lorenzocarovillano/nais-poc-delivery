package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.LiqDao;
import it.accenture.jnais.commons.data.to.ILiq;
import it.accenture.jnais.copy.Liq;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.AreaLdbv2271;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs2270Data;
import it.accenture.jnais.ws.redefines.LquAddizComun;
import it.accenture.jnais.ws.redefines.LquAddizRegion;
import it.accenture.jnais.ws.redefines.LquBnsNonGoduto;
import it.accenture.jnais.ws.redefines.LquCnbtInpstfm;
import it.accenture.jnais.ws.redefines.LquComponTaxRimb;
import it.accenture.jnais.ws.redefines.LquDtDen;
import it.accenture.jnais.ws.redefines.LquDtEndIstr;
import it.accenture.jnais.ws.redefines.LquDtLiq;
import it.accenture.jnais.ws.redefines.LquDtMor;
import it.accenture.jnais.ws.redefines.LquDtPervDen;
import it.accenture.jnais.ws.redefines.LquDtRich;
import it.accenture.jnais.ws.redefines.LquDtVlt;
import it.accenture.jnais.ws.redefines.LquIdMoviChiu;
import it.accenture.jnais.ws.redefines.LquImpbAddizComun;
import it.accenture.jnais.ws.redefines.LquImpbAddizRegion;
import it.accenture.jnais.ws.redefines.LquImpbBolloDettC;
import it.accenture.jnais.ws.redefines.LquImpbCnbtInpstfm;
import it.accenture.jnais.ws.redefines.LquImpbImpst252;
import it.accenture.jnais.ws.redefines.LquImpbImpstPrvr;
import it.accenture.jnais.ws.redefines.LquImpbIntrSuPrest;
import it.accenture.jnais.ws.redefines.LquImpbIrpef;
import it.accenture.jnais.ws.redefines.LquImpbIs;
import it.accenture.jnais.ws.redefines.LquImpbIs1382011;
import it.accenture.jnais.ws.redefines.LquImpbIs662014;
import it.accenture.jnais.ws.redefines.LquImpbTaxSep;
import it.accenture.jnais.ws.redefines.LquImpbVis1382011;
import it.accenture.jnais.ws.redefines.LquImpbVis662014;
import it.accenture.jnais.ws.redefines.LquImpDirDaRimb;
import it.accenture.jnais.ws.redefines.LquImpDirLiq;
import it.accenture.jnais.ws.redefines.LquImpExcontr;
import it.accenture.jnais.ws.redefines.LquImpIntrRitPag;
import it.accenture.jnais.ws.redefines.LquImpLrdDaRimb;
import it.accenture.jnais.ws.redefines.LquImpLrdLiqtoRilt;
import it.accenture.jnais.ws.redefines.LquImpOnerLiq;
import it.accenture.jnais.ws.redefines.LquImpPnl;
import it.accenture.jnais.ws.redefines.LquImpRenK1;
import it.accenture.jnais.ws.redefines.LquImpRenK2;
import it.accenture.jnais.ws.redefines.LquImpRenK3;
import it.accenture.jnais.ws.redefines.LquImpst252;
import it.accenture.jnais.ws.redefines.LquImpstApplRilt;
import it.accenture.jnais.ws.redefines.LquImpstBolloDettC;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotAa;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotSw;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotV;
import it.accenture.jnais.ws.redefines.LquImpstDaRimb;
import it.accenture.jnais.ws.redefines.LquImpstIrpef;
import it.accenture.jnais.ws.redefines.LquImpstPrvr;
import it.accenture.jnais.ws.redefines.LquImpstSost1382011;
import it.accenture.jnais.ws.redefines.LquImpstSost662014;
import it.accenture.jnais.ws.redefines.LquImpstVis1382011;
import it.accenture.jnais.ws.redefines.LquImpstVis662014;
import it.accenture.jnais.ws.redefines.LquMontDal2007;
import it.accenture.jnais.ws.redefines.LquMontEnd2000;
import it.accenture.jnais.ws.redefines.LquMontEnd2006;
import it.accenture.jnais.ws.redefines.LquPcAbbTitStat;
import it.accenture.jnais.ws.redefines.LquPcAbbTs662014;
import it.accenture.jnais.ws.redefines.LquPcRen;
import it.accenture.jnais.ws.redefines.LquPcRenK1;
import it.accenture.jnais.ws.redefines.LquPcRenK2;
import it.accenture.jnais.ws.redefines.LquPcRenK3;
import it.accenture.jnais.ws.redefines.LquPcRiscParz;
import it.accenture.jnais.ws.redefines.LquRisMat;
import it.accenture.jnais.ws.redefines.LquRisSpe;
import it.accenture.jnais.ws.redefines.LquSpeRcs;
import it.accenture.jnais.ws.redefines.LquTaxSep;
import it.accenture.jnais.ws.redefines.LquTotIasMggSin;
import it.accenture.jnais.ws.redefines.LquTotIasOnerPrvnt;
import it.accenture.jnais.ws.redefines.LquTotIasPnl;
import it.accenture.jnais.ws.redefines.LquTotIasRstDpst;
import it.accenture.jnais.ws.redefines.LquTotImpbAcc;
import it.accenture.jnais.ws.redefines.LquTotImpbTfr;
import it.accenture.jnais.ws.redefines.LquTotImpbVis;
import it.accenture.jnais.ws.redefines.LquTotImpIntrPrest;
import it.accenture.jnais.ws.redefines.LquTotImpIs;
import it.accenture.jnais.ws.redefines.LquTotImpLrdLiqto;
import it.accenture.jnais.ws.redefines.LquTotImpNetLiqto;
import it.accenture.jnais.ws.redefines.LquTotImpPrest;
import it.accenture.jnais.ws.redefines.LquTotImpRimb;
import it.accenture.jnais.ws.redefines.LquTotImpRitAcc;
import it.accenture.jnais.ws.redefines.LquTotImpRitTfr;
import it.accenture.jnais.ws.redefines.LquTotImpRitVis;
import it.accenture.jnais.ws.redefines.LquTotImpUti;
import it.accenture.jnais.ws.redefines.LquTpMetRisc;

/**Original name: LDBS2270<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  02 LUG 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs2270 extends Program implements ILiq {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private LiqDao liqDao = new LiqDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs2270Data ws = new Ldbs2270Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: AREA-LDBV2271
    private AreaLdbv2271 areaLdbv2271;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS2270_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, AreaLdbv2271 areaLdbv2271) {
        this.idsv0003 = idsv0003;
        this.areaLdbv2271 = areaLdbv2271;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs2270 getInstance() {
        return ((Ldbs2270)Programs.getInstance(Ldbs2270.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS2270'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS2270");
        // COB_CODE: MOVE ' AREA-LDBV2271' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella(" AREA-LDBV2271");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS2270.cbl:line=201, because the code is unreachable.
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS2270.cbl:line=212, because the code is unreachable.
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT SUM (TOT_IMP_LRD_LIQTO)
        //            INTO
        //               :LDBV2271-IMP-LRD-LIQTO
        //               :IND-LQU-TOT-IMP-LRD-LIQTO
        //             FROM LIQ
        //            WHERE ID_OGG   = :LDBV2271-ID-OGG
        //              AND TP_OGG   = :LDBV2271-TP-OGG
        //              AND TP_LIQ   = :LDBV2271-TP-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        liqDao.selectRec8(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS2270.cbl:line=241, because the code is unreachable.
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS2270.cbl:line=279, because the code is unreachable.
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS2270.cbl:line=289, because the code is unreachable.
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT SUM (TOT_IMP_LRD_LIQTO)
        //            INTO
        //               :LDBV2271-IMP-LRD-LIQTO
        //               :IND-LQU-TOT-IMP-LRD-LIQTO
        //             FROM LIQ
        //            WHERE ID_OGG   = :LDBV2271-ID-OGG
        //              AND TP_OGG   = :LDBV2271-TP-OGG
        //              AND TP_LIQ   = :LDBV2271-TP-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        liqDao.selectRec9(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS2270.cbl:line=321, because the code is unreachable.
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS2270.cbl:line=359, because the code is unreachable.
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS2270.cbl:line=369, because the code is unreachable.
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-LQU-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO LQU-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndLiq().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-ID-MOVI-CHIU-NULL
            ws.getLiq().getLquIdMoviChiu().setLquIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquIdMoviChiu.Len.LQU_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-LQU-IB-OGG = -1
        //              MOVE HIGH-VALUES TO LQU-IB-OGG-NULL
        //           END-IF
        if (ws.getIndLiq().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IB-OGG-NULL
            ws.getLiq().setLquIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Liq.Len.LQU_IB_OGG));
        }
        // COB_CODE: IF IND-LQU-DESC-CAU-EVE-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-DESC-CAU-EVE-SIN
        //           END-IF
        if (ws.getIndLiq().getDescCauEveSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DESC-CAU-EVE-SIN
            ws.getLiq().setLquDescCauEveSin(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Liq.Len.LQU_DESC_CAU_EVE_SIN));
        }
        // COB_CODE: IF IND-LQU-COD-CAU-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-COD-CAU-SIN-NULL
        //           END-IF
        if (ws.getIndLiq().getCodCauSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COD-CAU-SIN-NULL
            ws.getLiq().setLquCodCauSin(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Liq.Len.LQU_COD_CAU_SIN));
        }
        // COB_CODE: IF IND-LQU-COD-SIN-CATSTRF = -1
        //              MOVE HIGH-VALUES TO LQU-COD-SIN-CATSTRF-NULL
        //           END-IF
        if (ws.getIndLiq().getCodSinCatstrf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COD-SIN-CATSTRF-NULL
            ws.getLiq().setLquCodSinCatstrf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Liq.Len.LQU_COD_SIN_CATSTRF));
        }
        // COB_CODE: IF IND-LQU-DT-MOR = -1
        //              MOVE HIGH-VALUES TO LQU-DT-MOR-NULL
        //           END-IF
        if (ws.getIndLiq().getDtMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-MOR-NULL
            ws.getLiq().getLquDtMor().setLquDtMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtMor.Len.LQU_DT_MOR_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-DEN = -1
        //              MOVE HIGH-VALUES TO LQU-DT-DEN-NULL
        //           END-IF
        if (ws.getIndLiq().getDtDen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-DEN-NULL
            ws.getLiq().getLquDtDen().setLquDtDenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtDen.Len.LQU_DT_DEN_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-PERV-DEN = -1
        //              MOVE HIGH-VALUES TO LQU-DT-PERV-DEN-NULL
        //           END-IF
        if (ws.getIndLiq().getDtPervDen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-PERV-DEN-NULL
            ws.getLiq().getLquDtPervDen().setLquDtPervDenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtPervDen.Len.LQU_DT_PERV_DEN_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-RICH = -1
        //              MOVE HIGH-VALUES TO LQU-DT-RICH-NULL
        //           END-IF
        if (ws.getIndLiq().getDtRich() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-RICH-NULL
            ws.getLiq().getLquDtRich().setLquDtRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtRich.Len.LQU_DT_RICH_NULL));
        }
        // COB_CODE: IF IND-LQU-TP-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-TP-SIN-NULL
        //           END-IF
        if (ws.getIndLiq().getTpSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-SIN-NULL
            ws.getLiq().setLquTpSin(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Liq.Len.LQU_TP_SIN));
        }
        // COB_CODE: IF IND-LQU-TP-RISC = -1
        //              MOVE HIGH-VALUES TO LQU-TP-RISC-NULL
        //           END-IF
        if (ws.getIndLiq().getTpRisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-RISC-NULL
            ws.getLiq().setLquTpRisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Liq.Len.LQU_TP_RISC));
        }
        // COB_CODE: IF IND-LQU-TP-MET-RISC = -1
        //              MOVE HIGH-VALUES TO LQU-TP-MET-RISC-NULL
        //           END-IF
        if (ws.getIndLiq().getTpMetRisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-MET-RISC-NULL
            ws.getLiq().getLquTpMetRisc().setLquTpMetRiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTpMetRisc.Len.LQU_TP_MET_RISC_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-DT-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getDtLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-LIQ-NULL
            ws.getLiq().getLquDtLiq().setLquDtLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtLiq.Len.LQU_DT_LIQ_NULL));
        }
        // COB_CODE: IF IND-LQU-COD-DVS = -1
        //              MOVE HIGH-VALUES TO LQU-COD-DVS-NULL
        //           END-IF
        if (ws.getIndLiq().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COD-DVS-NULL
            ws.getLiq().setLquCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Liq.Len.LQU_COD_DVS));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-LRD-LIQTO = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-LRD-LIQTO-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpLrdLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-LRD-LIQTO-NULL
            ws.getLiq().getLquTotImpLrdLiqto().setLquTotImpLrdLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpLrdLiqto.Len.LQU_TOT_IMP_LRD_LIQTO_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-PREST = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-PREST-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-PREST-NULL
            ws.getLiq().getLquTotImpPrest().setLquTotImpPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpPrest.Len.LQU_TOT_IMP_PREST_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-INTR-PREST-NULL
            ws.getLiq().getLquTotImpIntrPrest().setLquTotImpIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpIntrPrest.Len.LQU_TOT_IMP_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-UTI = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-UTI-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpUti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-UTI-NULL
            ws.getLiq().getLquTotImpUti().setLquTotImpUtiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpUti.Len.LQU_TOT_IMP_UTI_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIT-TFR = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-TFR-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRitTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-TFR-NULL
            ws.getLiq().getLquTotImpRitTfr().setLquTotImpRitTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRitTfr.Len.LQU_TOT_IMP_RIT_TFR_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIT-ACC = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-ACC-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRitAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-ACC-NULL
            ws.getLiq().getLquTotImpRitAcc().setLquTotImpRitAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRitAcc.Len.LQU_TOT_IMP_RIT_ACC_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIT-VIS = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-VIS-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRitVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-VIS-NULL
            ws.getLiq().getLquTotImpRitVis().setLquTotImpRitVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRitVis.Len.LQU_TOT_IMP_RIT_VIS_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMPB-TFR = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMPB-TFR-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpbTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMPB-TFR-NULL
            ws.getLiq().getLquTotImpbTfr().setLquTotImpbTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpbTfr.Len.LQU_TOT_IMPB_TFR_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMPB-ACC = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMPB-ACC-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpbAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMPB-ACC-NULL
            ws.getLiq().getLquTotImpbAcc().setLquTotImpbAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpbAcc.Len.LQU_TOT_IMPB_ACC_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMPB-VIS = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMPB-VIS-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpbVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMPB-VIS-NULL
            ws.getLiq().getLquTotImpbVis().setLquTotImpbVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpbVis.Len.LQU_TOT_IMPB_VIS_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIMB-NULL
            ws.getLiq().getLquTotImpRimb().setLquTotImpRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRimb.Len.LQU_TOT_IMP_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IMPST-PRVR = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IMPST-PRVR-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbImpstPrvr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IMPST-PRVR-NULL
            ws.getLiq().getLquImpbImpstPrvr().setLquImpbImpstPrvrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbImpstPrvr.Len.LQU_IMPB_IMPST_PRVR_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-PRVR = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-PRVR-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstPrvr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-PRVR-NULL
            ws.getLiq().getLquImpstPrvr().setLquImpstPrvrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstPrvr.Len.LQU_IMPST_PRVR_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IMPST-252 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IMPST-252-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbImpst252() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IMPST-252-NULL
            ws.getLiq().getLquImpbImpst252().setLquImpbImpst252Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbImpst252.Len.LQU_IMPB_IMPST252_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-252 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-252-NULL
        //           END-IF
        if (ws.getIndLiq().getImpst252() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-252-NULL
            ws.getLiq().getLquImpst252().setLquImpst252Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpst252.Len.LQU_IMPST252_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-IS = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-IS-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-IS-NULL
            ws.getLiq().getLquTotImpIs().setLquTotImpIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpIs.Len.LQU_TOT_IMP_IS_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-DIR-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-DIR-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getImpDirLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-DIR-LIQ-NULL
            ws.getLiq().getLquImpDirLiq().setLquImpDirLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpDirLiq.Len.LQU_IMP_DIR_LIQ_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-NET-LIQTO = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-NET-LIQTO-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpNetLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-NET-LIQTO-NULL
            ws.getLiq().getLquTotImpNetLiqto().setLquTotImpNetLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpNetLiqto.Len.LQU_TOT_IMP_NET_LIQTO_NULL));
        }
        // COB_CODE: IF IND-LQU-MONT-END2000 = -1
        //              MOVE HIGH-VALUES TO LQU-MONT-END2000-NULL
        //           END-IF
        if (ws.getIndLiq().getMontEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-MONT-END2000-NULL
            ws.getLiq().getLquMontEnd2000().setLquMontEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquMontEnd2000.Len.LQU_MONT_END2000_NULL));
        }
        // COB_CODE: IF IND-LQU-MONT-END2006 = -1
        //              MOVE HIGH-VALUES TO LQU-MONT-END2006-NULL
        //           END-IF
        if (ws.getIndLiq().getMontEnd2006() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-MONT-END2006-NULL
            ws.getLiq().getLquMontEnd2006().setLquMontEnd2006Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquMontEnd2006.Len.LQU_MONT_END2006_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-NULL
            ws.getLiq().getLquPcRen().setLquPcRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRen.Len.LQU_PC_REN_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-PNL = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-PNL-NULL
        //           END-IF
        if (ws.getIndLiq().getImpPnl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-PNL-NULL
            ws.getLiq().getLquImpPnl().setLquImpPnlNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpPnl.Len.LQU_IMP_PNL_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IRPEF = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IRPEF-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIrpef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IRPEF-NULL
            ws.getLiq().getLquImpbIrpef().setLquImpbIrpefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIrpef.Len.LQU_IMPB_IRPEF_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-IRPEF = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-IRPEF-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstIrpef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-IRPEF-NULL
            ws.getLiq().getLquImpstIrpef().setLquImpstIrpefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstIrpef.Len.LQU_IMPST_IRPEF_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-VLT = -1
        //              MOVE HIGH-VALUES TO LQU-DT-VLT-NULL
        //           END-IF
        if (ws.getIndLiq().getDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-VLT-NULL
            ws.getLiq().getLquDtVlt().setLquDtVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtVlt.Len.LQU_DT_VLT_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-END-ISTR = -1
        //              MOVE HIGH-VALUES TO LQU-DT-END-ISTR-NULL
        //           END-IF
        if (ws.getIndLiq().getDtEndIstr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-END-ISTR-NULL
            ws.getLiq().getLquDtEndIstr().setLquDtEndIstrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtEndIstr.Len.LQU_DT_END_ISTR_NULL));
        }
        // COB_CODE: IF IND-LQU-SPE-RCS = -1
        //              MOVE HIGH-VALUES TO LQU-SPE-RCS-NULL
        //           END-IF
        if (ws.getIndLiq().getSpeRcs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-SPE-RCS-NULL
            ws.getLiq().getLquSpeRcs().setLquSpeRcsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquSpeRcs.Len.LQU_SPE_RCS_NULL));
        }
        // COB_CODE: IF IND-LQU-IB-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-IB-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getIbLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IB-LIQ-NULL
            ws.getLiq().setLquIbLiq(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Liq.Len.LQU_IB_LIQ));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-ONER-PRVNT = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-ONER-PRVNT-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasOnerPrvnt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-ONER-PRVNT-NULL
            ws.getLiq().getLquTotIasOnerPrvnt().setLquTotIasOnerPrvntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasOnerPrvnt.Len.LQU_TOT_IAS_ONER_PRVNT_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-MGG-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-MGG-SIN-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasMggSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-MGG-SIN-NULL
            ws.getLiq().getLquTotIasMggSin().setLquTotIasMggSinNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasMggSin.Len.LQU_TOT_IAS_MGG_SIN_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-RST-DPST = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-RST-DPST-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasRstDpst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-RST-DPST-NULL
            ws.getLiq().getLquTotIasRstDpst().setLquTotIasRstDpstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasRstDpst.Len.LQU_TOT_IAS_RST_DPST_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-ONER-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-ONER-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getImpOnerLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-ONER-LIQ-NULL
            ws.getLiq().getLquImpOnerLiq().setLquImpOnerLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpOnerLiq.Len.LQU_IMP_ONER_LIQ_NULL));
        }
        // COB_CODE: IF IND-LQU-COMPON-TAX-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-COMPON-TAX-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getComponTaxRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COMPON-TAX-RIMB-NULL
            ws.getLiq().getLquComponTaxRimb().setLquComponTaxRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquComponTaxRimb.Len.LQU_COMPON_TAX_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-TP-MEZ-PAG = -1
        //              MOVE HIGH-VALUES TO LQU-TP-MEZ-PAG-NULL
        //           END-IF
        if (ws.getIndLiq().getTpMezPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-MEZ-PAG-NULL
            ws.getLiq().setLquTpMezPag(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Liq.Len.LQU_TP_MEZ_PAG));
        }
        // COB_CODE: IF IND-LQU-IMP-EXCONTR = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-EXCONTR-NULL
        //           END-IF
        if (ws.getIndLiq().getImpExcontr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-EXCONTR-NULL
            ws.getLiq().getLquImpExcontr().setLquImpExcontrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpExcontr.Len.LQU_IMP_EXCONTR_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-INTR-RIT-PAG = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-INTR-RIT-PAG-NULL
        //           END-IF
        if (ws.getIndLiq().getImpIntrRitPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-INTR-RIT-PAG-NULL
            ws.getLiq().getLquImpIntrRitPag().setLquImpIntrRitPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpIntrRitPag.Len.LQU_IMP_INTR_RIT_PAG_NULL));
        }
        // COB_CODE: IF IND-LQU-BNS-NON-GODUTO = -1
        //              MOVE HIGH-VALUES TO LQU-BNS-NON-GODUTO-NULL
        //           END-IF
        if (ws.getIndLiq().getBnsNonGoduto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-BNS-NON-GODUTO-NULL
            ws.getLiq().getLquBnsNonGoduto().setLquBnsNonGodutoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquBnsNonGoduto.Len.LQU_BNS_NON_GODUTO_NULL));
        }
        // COB_CODE: IF IND-LQU-CNBT-INPSTFM = -1
        //              MOVE HIGH-VALUES TO LQU-CNBT-INPSTFM-NULL
        //           END-IF
        if (ws.getIndLiq().getCnbtInpstfm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-CNBT-INPSTFM-NULL
            ws.getLiq().getLquCnbtInpstfm().setLquCnbtInpstfmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquCnbtInpstfm.Len.LQU_CNBT_INPSTFM_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-DA-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-DA-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstDaRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-DA-RIMB-NULL
            ws.getLiq().getLquImpstDaRimb().setLquImpstDaRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstDaRimb.Len.LQU_IMPST_DA_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IS = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IS-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IS-NULL
            ws.getLiq().getLquImpbIs().setLquImpbIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIs.Len.LQU_IMPB_IS_NULL));
        }
        // COB_CODE: IF IND-LQU-TAX-SEP = -1
        //              MOVE HIGH-VALUES TO LQU-TAX-SEP-NULL
        //           END-IF
        if (ws.getIndLiq().getTaxSep() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TAX-SEP-NULL
            ws.getLiq().getLquTaxSep().setLquTaxSepNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTaxSep.Len.LQU_TAX_SEP_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-TAX-SEP = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-TAX-SEP-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbTaxSep() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-TAX-SEP-NULL
            ws.getLiq().getLquImpbTaxSep().setLquImpbTaxSepNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbTaxSep.Len.LQU_IMPB_TAX_SEP_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-INTR-SU-PREST = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-INTR-SU-PREST-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIntrSuPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-INTR-SU-PREST-NULL
            ws.getLiq().getLquImpbIntrSuPrest().setLquImpbIntrSuPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIntrSuPrest.Len.LQU_IMPB_INTR_SU_PREST_NULL));
        }
        // COB_CODE: IF IND-LQU-ADDIZ-COMUN = -1
        //              MOVE HIGH-VALUES TO LQU-ADDIZ-COMUN-NULL
        //           END-IF
        if (ws.getIndLiq().getAddizComun() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-ADDIZ-COMUN-NULL
            ws.getLiq().getLquAddizComun().setLquAddizComunNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquAddizComun.Len.LQU_ADDIZ_COMUN_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-ADDIZ-COMUN = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-COMUN-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbAddizComun() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-COMUN-NULL
            ws.getLiq().getLquImpbAddizComun().setLquImpbAddizComunNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbAddizComun.Len.LQU_IMPB_ADDIZ_COMUN_NULL));
        }
        // COB_CODE: IF IND-LQU-ADDIZ-REGION = -1
        //              MOVE HIGH-VALUES TO LQU-ADDIZ-REGION-NULL
        //           END-IF
        if (ws.getIndLiq().getAddizRegion() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-ADDIZ-REGION-NULL
            ws.getLiq().getLquAddizRegion().setLquAddizRegionNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquAddizRegion.Len.LQU_ADDIZ_REGION_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-ADDIZ-REGION = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-REGION-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbAddizRegion() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-REGION-NULL
            ws.getLiq().getLquImpbAddizRegion().setLquImpbAddizRegionNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbAddizRegion.Len.LQU_IMPB_ADDIZ_REGION_NULL));
        }
        // COB_CODE: IF IND-LQU-MONT-DAL2007 = -1
        //              MOVE HIGH-VALUES TO LQU-MONT-DAL2007-NULL
        //           END-IF
        if (ws.getIndLiq().getMontDal2007() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-MONT-DAL2007-NULL
            ws.getLiq().getLquMontDal2007().setLquMontDal2007Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquMontDal2007.Len.LQU_MONT_DAL2007_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-CNBT-INPSTFM = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-CNBT-INPSTFM-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbCnbtInpstfm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-CNBT-INPSTFM-NULL
            ws.getLiq().getLquImpbCnbtInpstfm().setLquImpbCnbtInpstfmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbCnbtInpstfm.Len.LQU_IMPB_CNBT_INPSTFM_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-LRD-DA-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-LRD-DA-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getImpLrdDaRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-LRD-DA-RIMB-NULL
            ws.getLiq().getLquImpLrdDaRimb().setLquImpLrdDaRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpLrdDaRimb.Len.LQU_IMP_LRD_DA_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-DIR-DA-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-DIR-DA-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getImpDirDaRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-DIR-DA-RIMB-NULL
            ws.getLiq().getLquImpDirDaRimb().setLquImpDirDaRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpDirDaRimb.Len.LQU_IMP_DIR_DA_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-RIS-MAT = -1
        //              MOVE HIGH-VALUES TO LQU-RIS-MAT-NULL
        //           END-IF
        if (ws.getIndLiq().getRisMat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-RIS-MAT-NULL
            ws.getLiq().getLquRisMat().setLquRisMatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquRisMat.Len.LQU_RIS_MAT_NULL));
        }
        // COB_CODE: IF IND-LQU-RIS-SPE = -1
        //              MOVE HIGH-VALUES TO LQU-RIS-SPE-NULL
        //           END-IF
        if (ws.getIndLiq().getRisSpe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-RIS-SPE-NULL
            ws.getLiq().getLquRisSpe().setLquRisSpeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquRisSpe.Len.LQU_RIS_SPE_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-PNL = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-PNL-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasPnl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-PNL-NULL
            ws.getLiq().getLquTotIasPnl().setLquTotIasPnlNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasPnl.Len.LQU_TOT_IAS_PNL_NULL));
        }
        // COB_CODE: IF IND-LQU-FL-EVE-GARTO = -1
        //              MOVE HIGH-VALUES TO LQU-FL-EVE-GARTO-NULL
        //           END-IF
        if (ws.getIndLiq().getFlEveGarto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-FL-EVE-GARTO-NULL
            ws.getLiq().setLquFlEveGarto(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-LQU-IMP-REN-K1 = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-REN-K1-NULL
        //           END-IF
        if (ws.getIndLiq().getImpRenK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-REN-K1-NULL
            ws.getLiq().getLquImpRenK1().setLquImpRenK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpRenK1.Len.LQU_IMP_REN_K1_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-REN-K2 = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-REN-K2-NULL
        //           END-IF
        if (ws.getIndLiq().getImpRenK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-REN-K2-NULL
            ws.getLiq().getLquImpRenK2().setLquImpRenK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpRenK2.Len.LQU_IMP_REN_K2_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-REN-K3 = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-REN-K3-NULL
        //           END-IF
        if (ws.getIndLiq().getImpRenK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-REN-K3-NULL
            ws.getLiq().getLquImpRenK3().setLquImpRenK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpRenK3.Len.LQU_IMP_REN_K3_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN-K1 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-K1-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRenK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-K1-NULL
            ws.getLiq().getLquPcRenK1().setLquPcRenK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRenK1.Len.LQU_PC_REN_K1_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN-K2 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-K2-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRenK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-K2-NULL
            ws.getLiq().getLquPcRenK2().setLquPcRenK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRenK2.Len.LQU_PC_REN_K2_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN-K3 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-K3-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRenK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-K3-NULL
            ws.getLiq().getLquPcRenK3().setLquPcRenK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRenK3.Len.LQU_PC_REN_K3_NULL));
        }
        // COB_CODE: IF IND-LQU-TP-CAUS-ANTIC = -1
        //              MOVE HIGH-VALUES TO LQU-TP-CAUS-ANTIC-NULL
        //           END-IF
        if (ws.getIndLiq().getTpCausAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-CAUS-ANTIC-NULL
            ws.getLiq().setLquTpCausAntic(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Liq.Len.LQU_TP_CAUS_ANTIC));
        }
        // COB_CODE: IF IND-LQU-IMP-LRD-LIQTO-RILT = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-LRD-LIQTO-RILT-NULL
        //           END-IF
        if (ws.getIndLiq().getImpLrdLiqtoRilt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-LRD-LIQTO-RILT-NULL
            ws.getLiq().getLquImpLrdLiqtoRilt().setLquImpLrdLiqtoRiltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpLrdLiqtoRilt.Len.LQU_IMP_LRD_LIQTO_RILT_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-APPL-RILT = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-APPL-RILT-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstApplRilt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-APPL-RILT-NULL
            ws.getLiq().getLquImpstApplRilt().setLquImpstApplRiltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstApplRilt.Len.LQU_IMPST_APPL_RILT_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-RISC-PARZ = -1
        //              MOVE HIGH-VALUES TO LQU-PC-RISC-PARZ-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRiscParz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-RISC-PARZ-NULL
            ws.getLiq().getLquPcRiscParz().setLquPcRiscParzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRiscParz.Len.LQU_PC_RISC_PARZ_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-TOT-V = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-V-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloTotV() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-V-NULL
            ws.getLiq().getLquImpstBolloTotV().setLquImpstBolloTotVNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloTotV.Len.LQU_IMPST_BOLLO_TOT_V_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-DETT-C = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-DETT-C-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloDettC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-DETT-C-NULL
            ws.getLiq().getLquImpstBolloDettC().setLquImpstBolloDettCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloDettC.Len.LQU_IMPST_BOLLO_DETT_C_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-TOT-SW = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-SW-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloTotSw() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-SW-NULL
            ws.getLiq().getLquImpstBolloTotSw().setLquImpstBolloTotSwNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloTotSw.Len.LQU_IMPST_BOLLO_TOT_SW_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-TOT-AA = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-AA-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloTotAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-AA-NULL
            ws.getLiq().getLquImpstBolloTotAa().setLquImpstBolloTotAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloTotAa.Len.LQU_IMPST_BOLLO_TOT_AA_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-VIS-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-VIS-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbVis1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-VIS-1382011-NULL
            ws.getLiq().getLquImpbVis1382011().setLquImpbVis1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbVis1382011.Len.LQU_IMPB_VIS1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-VIS-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-VIS-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstVis1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-VIS-1382011-NULL
            ws.getLiq().getLquImpstVis1382011().setLquImpstVis1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstVis1382011.Len.LQU_IMPST_VIS1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IS-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IS-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIs1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IS-1382011-NULL
            ws.getLiq().getLquImpbIs1382011().setLquImpbIs1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIs1382011.Len.LQU_IMPB_IS1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-SOST-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-SOST-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstSost1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-SOST-1382011-NULL
            ws.getLiq().getLquImpstSost1382011().setLquImpstSost1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstSost1382011.Len.LQU_IMPST_SOST1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-ABB-TIT-STAT = -1
        //              MOVE HIGH-VALUES TO LQU-PC-ABB-TIT-STAT-NULL
        //           END-IF
        if (ws.getIndLiq().getPcAbbTitStat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-ABB-TIT-STAT-NULL
            ws.getLiq().getLquPcAbbTitStat().setLquPcAbbTitStatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcAbbTitStat.Len.LQU_PC_ABB_TIT_STAT_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-BOLLO-DETT-C = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-BOLLO-DETT-C-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbBolloDettC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-BOLLO-DETT-C-NULL
            ws.getLiq().getLquImpbBolloDettC().setLquImpbBolloDettCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbBolloDettC.Len.LQU_IMPB_BOLLO_DETT_C_NULL));
        }
        // COB_CODE: IF IND-LQU-FL-PRE-COMP = -1
        //              MOVE HIGH-VALUES TO LQU-FL-PRE-COMP-NULL
        //           END-IF
        if (ws.getIndLiq().getFlPreComp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-FL-PRE-COMP-NULL
            ws.getLiq().setLquFlPreComp(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-LQU-IMPB-VIS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-VIS-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbVis662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-VIS-662014-NULL
            ws.getLiq().getLquImpbVis662014().setLquImpbVis662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbVis662014.Len.LQU_IMPB_VIS662014_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-VIS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-VIS-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstVis662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-VIS-662014-NULL
            ws.getLiq().getLquImpstVis662014().setLquImpstVis662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstVis662014.Len.LQU_IMPST_VIS662014_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IS-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIs662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IS-662014-NULL
            ws.getLiq().getLquImpbIs662014().setLquImpbIs662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIs662014.Len.LQU_IMPB_IS662014_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-SOST-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-SOST-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstSost662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-SOST-662014-NULL
            ws.getLiq().getLquImpstSost662014().setLquImpstSost662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstSost662014.Len.LQU_IMPST_SOST662014_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-ABB-TS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-ABB-TS-662014-NULL
        //           END-IF.
        if (ws.getIndLiq().getPcAbbTs662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-ABB-TS-662014-NULL
            ws.getLiq().getLquPcAbbTs662014().setLquPcAbbTs662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcAbbTs662014.Len.LQU_PC_ABB_TS662014_NULL));
        }
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    @Override
    public AfDecimal getAddizComun() {
        throw new FieldNotMappedException("addizComun");
    }

    @Override
    public void setAddizComun(AfDecimal addizComun) {
        throw new FieldNotMappedException("addizComun");
    }

    @Override
    public AfDecimal getAddizComunObj() {
        return getAddizComun();
    }

    @Override
    public void setAddizComunObj(AfDecimal addizComunObj) {
        setAddizComun(new AfDecimal(addizComunObj, 15, 3));
    }

    @Override
    public AfDecimal getAddizRegion() {
        throw new FieldNotMappedException("addizRegion");
    }

    @Override
    public void setAddizRegion(AfDecimal addizRegion) {
        throw new FieldNotMappedException("addizRegion");
    }

    @Override
    public AfDecimal getAddizRegionObj() {
        return getAddizRegion();
    }

    @Override
    public void setAddizRegionObj(AfDecimal addizRegionObj) {
        setAddizRegion(new AfDecimal(addizRegionObj, 15, 3));
    }

    @Override
    public AfDecimal getBnsNonGoduto() {
        throw new FieldNotMappedException("bnsNonGoduto");
    }

    @Override
    public void setBnsNonGoduto(AfDecimal bnsNonGoduto) {
        throw new FieldNotMappedException("bnsNonGoduto");
    }

    @Override
    public AfDecimal getBnsNonGodutoObj() {
        return getBnsNonGoduto();
    }

    @Override
    public void setBnsNonGodutoObj(AfDecimal bnsNonGodutoObj) {
        setBnsNonGoduto(new AfDecimal(bnsNonGodutoObj, 15, 3));
    }

    @Override
    public AfDecimal getCnbtInpstfm() {
        throw new FieldNotMappedException("cnbtInpstfm");
    }

    @Override
    public void setCnbtInpstfm(AfDecimal cnbtInpstfm) {
        throw new FieldNotMappedException("cnbtInpstfm");
    }

    @Override
    public AfDecimal getCnbtInpstfmObj() {
        return getCnbtInpstfm();
    }

    @Override
    public void setCnbtInpstfmObj(AfDecimal cnbtInpstfmObj) {
        setCnbtInpstfm(new AfDecimal(cnbtInpstfmObj, 15, 3));
    }

    @Override
    public String getCodCauSin() {
        throw new FieldNotMappedException("codCauSin");
    }

    @Override
    public void setCodCauSin(String codCauSin) {
        throw new FieldNotMappedException("codCauSin");
    }

    @Override
    public String getCodCauSinObj() {
        return getCodCauSin();
    }

    @Override
    public void setCodCauSinObj(String codCauSinObj) {
        setCodCauSin(codCauSinObj);
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public String getCodDvs() {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public void setCodDvs(String codDvs) {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public String getCodDvsObj() {
        return getCodDvs();
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        setCodDvs(codDvsObj);
    }

    @Override
    public String getCodSinCatstrf() {
        throw new FieldNotMappedException("codSinCatstrf");
    }

    @Override
    public void setCodSinCatstrf(String codSinCatstrf) {
        throw new FieldNotMappedException("codSinCatstrf");
    }

    @Override
    public String getCodSinCatstrfObj() {
        return getCodSinCatstrf();
    }

    @Override
    public void setCodSinCatstrfObj(String codSinCatstrfObj) {
        setCodSinCatstrf(codSinCatstrfObj);
    }

    @Override
    public AfDecimal getComponTaxRimb() {
        throw new FieldNotMappedException("componTaxRimb");
    }

    @Override
    public void setComponTaxRimb(AfDecimal componTaxRimb) {
        throw new FieldNotMappedException("componTaxRimb");
    }

    @Override
    public AfDecimal getComponTaxRimbObj() {
        return getComponTaxRimb();
    }

    @Override
    public void setComponTaxRimbObj(AfDecimal componTaxRimbObj) {
        setComponTaxRimb(new AfDecimal(componTaxRimbObj, 15, 3));
    }

    @Override
    public AfDecimal getCosTunnelUscita() {
        throw new FieldNotMappedException("cosTunnelUscita");
    }

    @Override
    public void setCosTunnelUscita(AfDecimal cosTunnelUscita) {
        throw new FieldNotMappedException("cosTunnelUscita");
    }

    @Override
    public AfDecimal getCosTunnelUscitaObj() {
        return getCosTunnelUscita();
    }

    @Override
    public void setCosTunnelUscitaObj(AfDecimal cosTunnelUscitaObj) {
        setCosTunnelUscita(new AfDecimal(cosTunnelUscitaObj, 15, 3));
    }

    @Override
    public String getDescCauEveSinVchar() {
        throw new FieldNotMappedException("descCauEveSinVchar");
    }

    @Override
    public void setDescCauEveSinVchar(String descCauEveSinVchar) {
        throw new FieldNotMappedException("descCauEveSinVchar");
    }

    @Override
    public String getDescCauEveSinVcharObj() {
        return getDescCauEveSinVchar();
    }

    @Override
    public void setDescCauEveSinVcharObj(String descCauEveSinVcharObj) {
        setDescCauEveSinVchar(descCauEveSinVcharObj);
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsEndCptz() {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public long getDsTsIniCptz() {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtDenDb() {
        throw new FieldNotMappedException("dtDenDb");
    }

    @Override
    public void setDtDenDb(String dtDenDb) {
        throw new FieldNotMappedException("dtDenDb");
    }

    @Override
    public String getDtDenDbObj() {
        return getDtDenDb();
    }

    @Override
    public void setDtDenDbObj(String dtDenDbObj) {
        setDtDenDb(dtDenDbObj);
    }

    @Override
    public String getDtEndEffDb() {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public String getDtEndIstrDb() {
        throw new FieldNotMappedException("dtEndIstrDb");
    }

    @Override
    public void setDtEndIstrDb(String dtEndIstrDb) {
        throw new FieldNotMappedException("dtEndIstrDb");
    }

    @Override
    public String getDtEndIstrDbObj() {
        return getDtEndIstrDb();
    }

    @Override
    public void setDtEndIstrDbObj(String dtEndIstrDbObj) {
        setDtEndIstrDb(dtEndIstrDbObj);
    }

    @Override
    public String getDtIniEffDb() {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public String getDtLiqDb() {
        throw new FieldNotMappedException("dtLiqDb");
    }

    @Override
    public void setDtLiqDb(String dtLiqDb) {
        throw new FieldNotMappedException("dtLiqDb");
    }

    @Override
    public String getDtLiqDbObj() {
        return getDtLiqDb();
    }

    @Override
    public void setDtLiqDbObj(String dtLiqDbObj) {
        setDtLiqDb(dtLiqDbObj);
    }

    @Override
    public String getDtMorDb() {
        throw new FieldNotMappedException("dtMorDb");
    }

    @Override
    public void setDtMorDb(String dtMorDb) {
        throw new FieldNotMappedException("dtMorDb");
    }

    @Override
    public String getDtMorDbObj() {
        return getDtMorDb();
    }

    @Override
    public void setDtMorDbObj(String dtMorDbObj) {
        setDtMorDb(dtMorDbObj);
    }

    @Override
    public String getDtPervDenDb() {
        throw new FieldNotMappedException("dtPervDenDb");
    }

    @Override
    public void setDtPervDenDb(String dtPervDenDb) {
        throw new FieldNotMappedException("dtPervDenDb");
    }

    @Override
    public String getDtPervDenDbObj() {
        return getDtPervDenDb();
    }

    @Override
    public void setDtPervDenDbObj(String dtPervDenDbObj) {
        setDtPervDenDb(dtPervDenDbObj);
    }

    @Override
    public String getDtRichDb() {
        throw new FieldNotMappedException("dtRichDb");
    }

    @Override
    public void setDtRichDb(String dtRichDb) {
        throw new FieldNotMappedException("dtRichDb");
    }

    @Override
    public String getDtRichDbObj() {
        return getDtRichDb();
    }

    @Override
    public void setDtRichDbObj(String dtRichDbObj) {
        setDtRichDb(dtRichDbObj);
    }

    @Override
    public String getDtVltDb() {
        throw new FieldNotMappedException("dtVltDb");
    }

    @Override
    public void setDtVltDb(String dtVltDb) {
        throw new FieldNotMappedException("dtVltDb");
    }

    @Override
    public String getDtVltDbObj() {
        return getDtVltDb();
    }

    @Override
    public void setDtVltDbObj(String dtVltDbObj) {
        setDtVltDb(dtVltDbObj);
    }

    @Override
    public char getFlEveGarto() {
        throw new FieldNotMappedException("flEveGarto");
    }

    @Override
    public void setFlEveGarto(char flEveGarto) {
        throw new FieldNotMappedException("flEveGarto");
    }

    @Override
    public Character getFlEveGartoObj() {
        return ((Character)getFlEveGarto());
    }

    @Override
    public void setFlEveGartoObj(Character flEveGartoObj) {
        setFlEveGarto(((char)flEveGartoObj));
    }

    @Override
    public char getFlPreComp() {
        throw new FieldNotMappedException("flPreComp");
    }

    @Override
    public void setFlPreComp(char flPreComp) {
        throw new FieldNotMappedException("flPreComp");
    }

    @Override
    public Character getFlPreCompObj() {
        return ((Character)getFlPreComp());
    }

    @Override
    public void setFlPreCompObj(Character flPreCompObj) {
        setFlPreComp(((char)flPreCompObj));
    }

    @Override
    public String getIbLiq() {
        throw new FieldNotMappedException("ibLiq");
    }

    @Override
    public void setIbLiq(String ibLiq) {
        throw new FieldNotMappedException("ibLiq");
    }

    @Override
    public String getIbLiqObj() {
        return getIbLiq();
    }

    @Override
    public void setIbLiqObj(String ibLiqObj) {
        setIbLiq(ibLiqObj);
    }

    @Override
    public String getIbOgg() {
        throw new FieldNotMappedException("ibOgg");
    }

    @Override
    public void setIbOgg(String ibOgg) {
        throw new FieldNotMappedException("ibOgg");
    }

    @Override
    public String getIbOggObj() {
        return getIbOgg();
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        setIbOgg(ibOggObj);
    }

    @Override
    public int getIdLiq() {
        throw new FieldNotMappedException("idLiq");
    }

    @Override
    public void setIdLiq(int idLiq) {
        throw new FieldNotMappedException("idLiq");
    }

    @Override
    public int getIdMoviChiu() {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public Integer getIdMoviChiuObj() {
        return ((Integer)getIdMoviChiu());
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        setIdMoviChiu(((int)idMoviChiuObj));
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpDirDaRimb() {
        throw new FieldNotMappedException("impDirDaRimb");
    }

    @Override
    public void setImpDirDaRimb(AfDecimal impDirDaRimb) {
        throw new FieldNotMappedException("impDirDaRimb");
    }

    @Override
    public AfDecimal getImpDirDaRimbObj() {
        return getImpDirDaRimb();
    }

    @Override
    public void setImpDirDaRimbObj(AfDecimal impDirDaRimbObj) {
        setImpDirDaRimb(new AfDecimal(impDirDaRimbObj, 15, 3));
    }

    @Override
    public AfDecimal getImpDirLiq() {
        throw new FieldNotMappedException("impDirLiq");
    }

    @Override
    public void setImpDirLiq(AfDecimal impDirLiq) {
        throw new FieldNotMappedException("impDirLiq");
    }

    @Override
    public AfDecimal getImpDirLiqObj() {
        return getImpDirLiq();
    }

    @Override
    public void setImpDirLiqObj(AfDecimal impDirLiqObj) {
        setImpDirLiq(new AfDecimal(impDirLiqObj, 15, 3));
    }

    @Override
    public AfDecimal getImpExcontr() {
        throw new FieldNotMappedException("impExcontr");
    }

    @Override
    public void setImpExcontr(AfDecimal impExcontr) {
        throw new FieldNotMappedException("impExcontr");
    }

    @Override
    public AfDecimal getImpExcontrObj() {
        return getImpExcontr();
    }

    @Override
    public void setImpExcontrObj(AfDecimal impExcontrObj) {
        setImpExcontr(new AfDecimal(impExcontrObj, 15, 3));
    }

    @Override
    public AfDecimal getImpIntrRitPag() {
        throw new FieldNotMappedException("impIntrRitPag");
    }

    @Override
    public void setImpIntrRitPag(AfDecimal impIntrRitPag) {
        throw new FieldNotMappedException("impIntrRitPag");
    }

    @Override
    public AfDecimal getImpIntrRitPagObj() {
        return getImpIntrRitPag();
    }

    @Override
    public void setImpIntrRitPagObj(AfDecimal impIntrRitPagObj) {
        setImpIntrRitPag(new AfDecimal(impIntrRitPagObj, 15, 3));
    }

    @Override
    public AfDecimal getImpLrdCalcCp() {
        throw new FieldNotMappedException("impLrdCalcCp");
    }

    @Override
    public void setImpLrdCalcCp(AfDecimal impLrdCalcCp) {
        throw new FieldNotMappedException("impLrdCalcCp");
    }

    @Override
    public AfDecimal getImpLrdCalcCpObj() {
        return getImpLrdCalcCp();
    }

    @Override
    public void setImpLrdCalcCpObj(AfDecimal impLrdCalcCpObj) {
        setImpLrdCalcCp(new AfDecimal(impLrdCalcCpObj, 15, 3));
    }

    @Override
    public AfDecimal getImpLrdDaRimb() {
        throw new FieldNotMappedException("impLrdDaRimb");
    }

    @Override
    public void setImpLrdDaRimb(AfDecimal impLrdDaRimb) {
        throw new FieldNotMappedException("impLrdDaRimb");
    }

    @Override
    public AfDecimal getImpLrdDaRimbObj() {
        return getImpLrdDaRimb();
    }

    @Override
    public void setImpLrdDaRimbObj(AfDecimal impLrdDaRimbObj) {
        setImpLrdDaRimb(new AfDecimal(impLrdDaRimbObj, 15, 3));
    }

    @Override
    public AfDecimal getImpLrdLiqtoRilt() {
        throw new FieldNotMappedException("impLrdLiqtoRilt");
    }

    @Override
    public void setImpLrdLiqtoRilt(AfDecimal impLrdLiqtoRilt) {
        throw new FieldNotMappedException("impLrdLiqtoRilt");
    }

    @Override
    public AfDecimal getImpLrdLiqtoRiltObj() {
        return getImpLrdLiqtoRilt();
    }

    @Override
    public void setImpLrdLiqtoRiltObj(AfDecimal impLrdLiqtoRiltObj) {
        setImpLrdLiqtoRilt(new AfDecimal(impLrdLiqtoRiltObj, 15, 3));
    }

    @Override
    public AfDecimal getImpOnerLiq() {
        throw new FieldNotMappedException("impOnerLiq");
    }

    @Override
    public void setImpOnerLiq(AfDecimal impOnerLiq) {
        throw new FieldNotMappedException("impOnerLiq");
    }

    @Override
    public AfDecimal getImpOnerLiqObj() {
        return getImpOnerLiq();
    }

    @Override
    public void setImpOnerLiqObj(AfDecimal impOnerLiqObj) {
        setImpOnerLiq(new AfDecimal(impOnerLiqObj, 15, 3));
    }

    @Override
    public AfDecimal getImpPnl() {
        throw new FieldNotMappedException("impPnl");
    }

    @Override
    public void setImpPnl(AfDecimal impPnl) {
        throw new FieldNotMappedException("impPnl");
    }

    @Override
    public AfDecimal getImpPnlObj() {
        return getImpPnl();
    }

    @Override
    public void setImpPnlObj(AfDecimal impPnlObj) {
        setImpPnl(new AfDecimal(impPnlObj, 15, 3));
    }

    @Override
    public AfDecimal getImpRenK1() {
        throw new FieldNotMappedException("impRenK1");
    }

    @Override
    public void setImpRenK1(AfDecimal impRenK1) {
        throw new FieldNotMappedException("impRenK1");
    }

    @Override
    public AfDecimal getImpRenK1Obj() {
        return getImpRenK1();
    }

    @Override
    public void setImpRenK1Obj(AfDecimal impRenK1Obj) {
        setImpRenK1(new AfDecimal(impRenK1Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpRenK2() {
        throw new FieldNotMappedException("impRenK2");
    }

    @Override
    public void setImpRenK2(AfDecimal impRenK2) {
        throw new FieldNotMappedException("impRenK2");
    }

    @Override
    public AfDecimal getImpRenK2Obj() {
        return getImpRenK2();
    }

    @Override
    public void setImpRenK2Obj(AfDecimal impRenK2Obj) {
        setImpRenK2(new AfDecimal(impRenK2Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpRenK3() {
        throw new FieldNotMappedException("impRenK3");
    }

    @Override
    public void setImpRenK3(AfDecimal impRenK3) {
        throw new FieldNotMappedException("impRenK3");
    }

    @Override
    public AfDecimal getImpRenK3Obj() {
        return getImpRenK3();
    }

    @Override
    public void setImpRenK3Obj(AfDecimal impRenK3Obj) {
        setImpRenK3(new AfDecimal(impRenK3Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpbAddizComun() {
        throw new FieldNotMappedException("impbAddizComun");
    }

    @Override
    public void setImpbAddizComun(AfDecimal impbAddizComun) {
        throw new FieldNotMappedException("impbAddizComun");
    }

    @Override
    public AfDecimal getImpbAddizComunObj() {
        return getImpbAddizComun();
    }

    @Override
    public void setImpbAddizComunObj(AfDecimal impbAddizComunObj) {
        setImpbAddizComun(new AfDecimal(impbAddizComunObj, 15, 3));
    }

    @Override
    public AfDecimal getImpbAddizRegion() {
        throw new FieldNotMappedException("impbAddizRegion");
    }

    @Override
    public void setImpbAddizRegion(AfDecimal impbAddizRegion) {
        throw new FieldNotMappedException("impbAddizRegion");
    }

    @Override
    public AfDecimal getImpbAddizRegionObj() {
        return getImpbAddizRegion();
    }

    @Override
    public void setImpbAddizRegionObj(AfDecimal impbAddizRegionObj) {
        setImpbAddizRegion(new AfDecimal(impbAddizRegionObj, 15, 3));
    }

    @Override
    public AfDecimal getImpbBolloDettC() {
        throw new FieldNotMappedException("impbBolloDettC");
    }

    @Override
    public void setImpbBolloDettC(AfDecimal impbBolloDettC) {
        throw new FieldNotMappedException("impbBolloDettC");
    }

    @Override
    public AfDecimal getImpbBolloDettCObj() {
        return getImpbBolloDettC();
    }

    @Override
    public void setImpbBolloDettCObj(AfDecimal impbBolloDettCObj) {
        setImpbBolloDettC(new AfDecimal(impbBolloDettCObj, 15, 3));
    }

    @Override
    public AfDecimal getImpbCnbtInpstfm() {
        throw new FieldNotMappedException("impbCnbtInpstfm");
    }

    @Override
    public void setImpbCnbtInpstfm(AfDecimal impbCnbtInpstfm) {
        throw new FieldNotMappedException("impbCnbtInpstfm");
    }

    @Override
    public AfDecimal getImpbCnbtInpstfmObj() {
        return getImpbCnbtInpstfm();
    }

    @Override
    public void setImpbCnbtInpstfmObj(AfDecimal impbCnbtInpstfmObj) {
        setImpbCnbtInpstfm(new AfDecimal(impbCnbtInpstfmObj, 15, 3));
    }

    @Override
    public AfDecimal getImpbImpst252() {
        throw new FieldNotMappedException("impbImpst252");
    }

    @Override
    public void setImpbImpst252(AfDecimal impbImpst252) {
        throw new FieldNotMappedException("impbImpst252");
    }

    @Override
    public AfDecimal getImpbImpst252Obj() {
        return getImpbImpst252();
    }

    @Override
    public void setImpbImpst252Obj(AfDecimal impbImpst252Obj) {
        setImpbImpst252(new AfDecimal(impbImpst252Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpbImpstPrvr() {
        throw new FieldNotMappedException("impbImpstPrvr");
    }

    @Override
    public void setImpbImpstPrvr(AfDecimal impbImpstPrvr) {
        throw new FieldNotMappedException("impbImpstPrvr");
    }

    @Override
    public AfDecimal getImpbImpstPrvrObj() {
        return getImpbImpstPrvr();
    }

    @Override
    public void setImpbImpstPrvrObj(AfDecimal impbImpstPrvrObj) {
        setImpbImpstPrvr(new AfDecimal(impbImpstPrvrObj, 15, 3));
    }

    @Override
    public AfDecimal getImpbIntrSuPrest() {
        throw new FieldNotMappedException("impbIntrSuPrest");
    }

    @Override
    public void setImpbIntrSuPrest(AfDecimal impbIntrSuPrest) {
        throw new FieldNotMappedException("impbIntrSuPrest");
    }

    @Override
    public AfDecimal getImpbIntrSuPrestObj() {
        return getImpbIntrSuPrest();
    }

    @Override
    public void setImpbIntrSuPrestObj(AfDecimal impbIntrSuPrestObj) {
        setImpbIntrSuPrest(new AfDecimal(impbIntrSuPrestObj, 15, 3));
    }

    @Override
    public AfDecimal getImpbIrpef() {
        throw new FieldNotMappedException("impbIrpef");
    }

    @Override
    public void setImpbIrpef(AfDecimal impbIrpef) {
        throw new FieldNotMappedException("impbIrpef");
    }

    @Override
    public AfDecimal getImpbIrpefObj() {
        return getImpbIrpef();
    }

    @Override
    public void setImpbIrpefObj(AfDecimal impbIrpefObj) {
        setImpbIrpef(new AfDecimal(impbIrpefObj, 15, 3));
    }

    @Override
    public AfDecimal getImpbIs1382011() {
        throw new FieldNotMappedException("impbIs1382011");
    }

    @Override
    public void setImpbIs1382011(AfDecimal impbIs1382011) {
        throw new FieldNotMappedException("impbIs1382011");
    }

    @Override
    public AfDecimal getImpbIs1382011Obj() {
        return getImpbIs1382011();
    }

    @Override
    public void setImpbIs1382011Obj(AfDecimal impbIs1382011Obj) {
        setImpbIs1382011(new AfDecimal(impbIs1382011Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpbIs662014() {
        throw new FieldNotMappedException("impbIs662014");
    }

    @Override
    public void setImpbIs662014(AfDecimal impbIs662014) {
        throw new FieldNotMappedException("impbIs662014");
    }

    @Override
    public AfDecimal getImpbIs662014Obj() {
        return getImpbIs662014();
    }

    @Override
    public void setImpbIs662014Obj(AfDecimal impbIs662014Obj) {
        setImpbIs662014(new AfDecimal(impbIs662014Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpbIs() {
        throw new FieldNotMappedException("impbIs");
    }

    @Override
    public void setImpbIs(AfDecimal impbIs) {
        throw new FieldNotMappedException("impbIs");
    }

    @Override
    public AfDecimal getImpbIsObj() {
        return getImpbIs();
    }

    @Override
    public void setImpbIsObj(AfDecimal impbIsObj) {
        setImpbIs(new AfDecimal(impbIsObj, 15, 3));
    }

    @Override
    public AfDecimal getImpbTaxSep() {
        throw new FieldNotMappedException("impbTaxSep");
    }

    @Override
    public void setImpbTaxSep(AfDecimal impbTaxSep) {
        throw new FieldNotMappedException("impbTaxSep");
    }

    @Override
    public AfDecimal getImpbTaxSepObj() {
        return getImpbTaxSep();
    }

    @Override
    public void setImpbTaxSepObj(AfDecimal impbTaxSepObj) {
        setImpbTaxSep(new AfDecimal(impbTaxSepObj, 15, 3));
    }

    @Override
    public AfDecimal getImpbVis1382011() {
        throw new FieldNotMappedException("impbVis1382011");
    }

    @Override
    public void setImpbVis1382011(AfDecimal impbVis1382011) {
        throw new FieldNotMappedException("impbVis1382011");
    }

    @Override
    public AfDecimal getImpbVis1382011Obj() {
        return getImpbVis1382011();
    }

    @Override
    public void setImpbVis1382011Obj(AfDecimal impbVis1382011Obj) {
        setImpbVis1382011(new AfDecimal(impbVis1382011Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpbVis662014() {
        throw new FieldNotMappedException("impbVis662014");
    }

    @Override
    public void setImpbVis662014(AfDecimal impbVis662014) {
        throw new FieldNotMappedException("impbVis662014");
    }

    @Override
    public AfDecimal getImpbVis662014Obj() {
        return getImpbVis662014();
    }

    @Override
    public void setImpbVis662014Obj(AfDecimal impbVis662014Obj) {
        setImpbVis662014(new AfDecimal(impbVis662014Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpst252() {
        throw new FieldNotMappedException("impst252");
    }

    @Override
    public void setImpst252(AfDecimal impst252) {
        throw new FieldNotMappedException("impst252");
    }

    @Override
    public AfDecimal getImpst252Obj() {
        return getImpst252();
    }

    @Override
    public void setImpst252Obj(AfDecimal impst252Obj) {
        setImpst252(new AfDecimal(impst252Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpstApplRilt() {
        throw new FieldNotMappedException("impstApplRilt");
    }

    @Override
    public void setImpstApplRilt(AfDecimal impstApplRilt) {
        throw new FieldNotMappedException("impstApplRilt");
    }

    @Override
    public AfDecimal getImpstApplRiltObj() {
        return getImpstApplRilt();
    }

    @Override
    public void setImpstApplRiltObj(AfDecimal impstApplRiltObj) {
        setImpstApplRilt(new AfDecimal(impstApplRiltObj, 15, 3));
    }

    @Override
    public AfDecimal getImpstBolloDettC() {
        throw new FieldNotMappedException("impstBolloDettC");
    }

    @Override
    public void setImpstBolloDettC(AfDecimal impstBolloDettC) {
        throw new FieldNotMappedException("impstBolloDettC");
    }

    @Override
    public AfDecimal getImpstBolloDettCObj() {
        return getImpstBolloDettC();
    }

    @Override
    public void setImpstBolloDettCObj(AfDecimal impstBolloDettCObj) {
        setImpstBolloDettC(new AfDecimal(impstBolloDettCObj, 15, 3));
    }

    @Override
    public AfDecimal getImpstBolloTotAa() {
        throw new FieldNotMappedException("impstBolloTotAa");
    }

    @Override
    public void setImpstBolloTotAa(AfDecimal impstBolloTotAa) {
        throw new FieldNotMappedException("impstBolloTotAa");
    }

    @Override
    public AfDecimal getImpstBolloTotAaObj() {
        return getImpstBolloTotAa();
    }

    @Override
    public void setImpstBolloTotAaObj(AfDecimal impstBolloTotAaObj) {
        setImpstBolloTotAa(new AfDecimal(impstBolloTotAaObj, 15, 3));
    }

    @Override
    public AfDecimal getImpstBolloTotSw() {
        throw new FieldNotMappedException("impstBolloTotSw");
    }

    @Override
    public void setImpstBolloTotSw(AfDecimal impstBolloTotSw) {
        throw new FieldNotMappedException("impstBolloTotSw");
    }

    @Override
    public AfDecimal getImpstBolloTotSwObj() {
        return getImpstBolloTotSw();
    }

    @Override
    public void setImpstBolloTotSwObj(AfDecimal impstBolloTotSwObj) {
        setImpstBolloTotSw(new AfDecimal(impstBolloTotSwObj, 15, 3));
    }

    @Override
    public AfDecimal getImpstBolloTotV() {
        throw new FieldNotMappedException("impstBolloTotV");
    }

    @Override
    public void setImpstBolloTotV(AfDecimal impstBolloTotV) {
        throw new FieldNotMappedException("impstBolloTotV");
    }

    @Override
    public AfDecimal getImpstBolloTotVObj() {
        return getImpstBolloTotV();
    }

    @Override
    public void setImpstBolloTotVObj(AfDecimal impstBolloTotVObj) {
        setImpstBolloTotV(new AfDecimal(impstBolloTotVObj, 15, 3));
    }

    @Override
    public AfDecimal getImpstDaRimb() {
        throw new FieldNotMappedException("impstDaRimb");
    }

    @Override
    public void setImpstDaRimb(AfDecimal impstDaRimb) {
        throw new FieldNotMappedException("impstDaRimb");
    }

    @Override
    public AfDecimal getImpstDaRimbObj() {
        return getImpstDaRimb();
    }

    @Override
    public void setImpstDaRimbObj(AfDecimal impstDaRimbObj) {
        setImpstDaRimb(new AfDecimal(impstDaRimbObj, 15, 3));
    }

    @Override
    public AfDecimal getImpstIrpef() {
        throw new FieldNotMappedException("impstIrpef");
    }

    @Override
    public void setImpstIrpef(AfDecimal impstIrpef) {
        throw new FieldNotMappedException("impstIrpef");
    }

    @Override
    public AfDecimal getImpstIrpefObj() {
        return getImpstIrpef();
    }

    @Override
    public void setImpstIrpefObj(AfDecimal impstIrpefObj) {
        setImpstIrpef(new AfDecimal(impstIrpefObj, 15, 3));
    }

    @Override
    public AfDecimal getImpstPrvr() {
        throw new FieldNotMappedException("impstPrvr");
    }

    @Override
    public void setImpstPrvr(AfDecimal impstPrvr) {
        throw new FieldNotMappedException("impstPrvr");
    }

    @Override
    public AfDecimal getImpstPrvrObj() {
        return getImpstPrvr();
    }

    @Override
    public void setImpstPrvrObj(AfDecimal impstPrvrObj) {
        setImpstPrvr(new AfDecimal(impstPrvrObj, 15, 3));
    }

    @Override
    public AfDecimal getImpstSost1382011() {
        throw new FieldNotMappedException("impstSost1382011");
    }

    @Override
    public void setImpstSost1382011(AfDecimal impstSost1382011) {
        throw new FieldNotMappedException("impstSost1382011");
    }

    @Override
    public AfDecimal getImpstSost1382011Obj() {
        return getImpstSost1382011();
    }

    @Override
    public void setImpstSost1382011Obj(AfDecimal impstSost1382011Obj) {
        setImpstSost1382011(new AfDecimal(impstSost1382011Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpstSost662014() {
        throw new FieldNotMappedException("impstSost662014");
    }

    @Override
    public void setImpstSost662014(AfDecimal impstSost662014) {
        throw new FieldNotMappedException("impstSost662014");
    }

    @Override
    public AfDecimal getImpstSost662014Obj() {
        return getImpstSost662014();
    }

    @Override
    public void setImpstSost662014Obj(AfDecimal impstSost662014Obj) {
        setImpstSost662014(new AfDecimal(impstSost662014Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpstVis1382011() {
        throw new FieldNotMappedException("impstVis1382011");
    }

    @Override
    public void setImpstVis1382011(AfDecimal impstVis1382011) {
        throw new FieldNotMappedException("impstVis1382011");
    }

    @Override
    public AfDecimal getImpstVis1382011Obj() {
        return getImpstVis1382011();
    }

    @Override
    public void setImpstVis1382011Obj(AfDecimal impstVis1382011Obj) {
        setImpstVis1382011(new AfDecimal(impstVis1382011Obj, 15, 3));
    }

    @Override
    public AfDecimal getImpstVis662014() {
        throw new FieldNotMappedException("impstVis662014");
    }

    @Override
    public void setImpstVis662014(AfDecimal impstVis662014) {
        throw new FieldNotMappedException("impstVis662014");
    }

    @Override
    public AfDecimal getImpstVis662014Obj() {
        return getImpstVis662014();
    }

    @Override
    public void setImpstVis662014Obj(AfDecimal impstVis662014Obj) {
        setImpstVis662014(new AfDecimal(impstVis662014Obj, 15, 3));
    }

    @Override
    public int getLdbv2271IdOgg() {
        return areaLdbv2271.getLdbv2271IdOgg();
    }

    @Override
    public void setLdbv2271IdOgg(int ldbv2271IdOgg) {
        this.areaLdbv2271.setLdbv2271IdOgg(ldbv2271IdOgg);
    }

    @Override
    public AfDecimal getLdbv2271ImpLrdLiqto() {
        return areaLdbv2271.getLdbv2271ImpLrdLiqto();
    }

    @Override
    public void setLdbv2271ImpLrdLiqto(AfDecimal ldbv2271ImpLrdLiqto) {
        this.areaLdbv2271.setLdbv2271ImpLrdLiqto(ldbv2271ImpLrdLiqto.copy());
    }

    @Override
    public AfDecimal getLdbv2271ImpLrdLiqtoObj() {
        if (ws.getIndLiq().getTotImpLrdLiqto() >= 0) {
            return getLdbv2271ImpLrdLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setLdbv2271ImpLrdLiqtoObj(AfDecimal ldbv2271ImpLrdLiqtoObj) {
        if (ldbv2271ImpLrdLiqtoObj != null) {
            setLdbv2271ImpLrdLiqto(new AfDecimal(ldbv2271ImpLrdLiqtoObj, 15, 3));
            ws.getIndLiq().setTotImpLrdLiqto(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpLrdLiqto(((short)-1));
        }
    }

    @Override
    public String getLdbv2271TpLiq() {
        return areaLdbv2271.getLdbv2271TpLiq();
    }

    @Override
    public void setLdbv2271TpLiq(String ldbv2271TpLiq) {
        this.areaLdbv2271.setLdbv2271TpLiq(ldbv2271TpLiq);
    }

    @Override
    public String getLdbv2271TpOgg() {
        return areaLdbv2271.getLdbv2271TpOgg();
    }

    @Override
    public void setLdbv2271TpOgg(String ldbv2271TpOgg) {
        this.areaLdbv2271.setLdbv2271TpOgg(ldbv2271TpOgg);
    }

    @Override
    public long getLquDsRiga() {
        throw new FieldNotMappedException("lquDsRiga");
    }

    @Override
    public void setLquDsRiga(long lquDsRiga) {
        throw new FieldNotMappedException("lquDsRiga");
    }

    @Override
    public int getLquIdMoviCrz() {
        throw new FieldNotMappedException("lquIdMoviCrz");
    }

    @Override
    public void setLquIdMoviCrz(int lquIdMoviCrz) {
        throw new FieldNotMappedException("lquIdMoviCrz");
    }

    @Override
    public int getLquIdOgg() {
        throw new FieldNotMappedException("lquIdOgg");
    }

    @Override
    public void setLquIdOgg(int lquIdOgg) {
        throw new FieldNotMappedException("lquIdOgg");
    }

    @Override
    public String getLquTpOgg() {
        throw new FieldNotMappedException("lquTpOgg");
    }

    @Override
    public void setLquTpOgg(String lquTpOgg) {
        throw new FieldNotMappedException("lquTpOgg");
    }

    @Override
    public AfDecimal getMontDal2007() {
        throw new FieldNotMappedException("montDal2007");
    }

    @Override
    public void setMontDal2007(AfDecimal montDal2007) {
        throw new FieldNotMappedException("montDal2007");
    }

    @Override
    public AfDecimal getMontDal2007Obj() {
        return getMontDal2007();
    }

    @Override
    public void setMontDal2007Obj(AfDecimal montDal2007Obj) {
        setMontDal2007(new AfDecimal(montDal2007Obj, 15, 3));
    }

    @Override
    public AfDecimal getMontEnd2000() {
        throw new FieldNotMappedException("montEnd2000");
    }

    @Override
    public void setMontEnd2000(AfDecimal montEnd2000) {
        throw new FieldNotMappedException("montEnd2000");
    }

    @Override
    public AfDecimal getMontEnd2000Obj() {
        return getMontEnd2000();
    }

    @Override
    public void setMontEnd2000Obj(AfDecimal montEnd2000Obj) {
        setMontEnd2000(new AfDecimal(montEnd2000Obj, 15, 3));
    }

    @Override
    public AfDecimal getMontEnd2006() {
        throw new FieldNotMappedException("montEnd2006");
    }

    @Override
    public void setMontEnd2006(AfDecimal montEnd2006) {
        throw new FieldNotMappedException("montEnd2006");
    }

    @Override
    public AfDecimal getMontEnd2006Obj() {
        return getMontEnd2006();
    }

    @Override
    public void setMontEnd2006Obj(AfDecimal montEnd2006Obj) {
        setMontEnd2006(new AfDecimal(montEnd2006Obj, 15, 3));
    }

    @Override
    public AfDecimal getPcAbbTitStat() {
        throw new FieldNotMappedException("pcAbbTitStat");
    }

    @Override
    public void setPcAbbTitStat(AfDecimal pcAbbTitStat) {
        throw new FieldNotMappedException("pcAbbTitStat");
    }

    @Override
    public AfDecimal getPcAbbTitStatObj() {
        return getPcAbbTitStat();
    }

    @Override
    public void setPcAbbTitStatObj(AfDecimal pcAbbTitStatObj) {
        setPcAbbTitStat(new AfDecimal(pcAbbTitStatObj, 6, 3));
    }

    @Override
    public AfDecimal getPcAbbTs662014() {
        throw new FieldNotMappedException("pcAbbTs662014");
    }

    @Override
    public void setPcAbbTs662014(AfDecimal pcAbbTs662014) {
        throw new FieldNotMappedException("pcAbbTs662014");
    }

    @Override
    public AfDecimal getPcAbbTs662014Obj() {
        return getPcAbbTs662014();
    }

    @Override
    public void setPcAbbTs662014Obj(AfDecimal pcAbbTs662014Obj) {
        setPcAbbTs662014(new AfDecimal(pcAbbTs662014Obj, 6, 3));
    }

    @Override
    public AfDecimal getPcRen() {
        throw new FieldNotMappedException("pcRen");
    }

    @Override
    public void setPcRen(AfDecimal pcRen) {
        throw new FieldNotMappedException("pcRen");
    }

    @Override
    public AfDecimal getPcRenK1() {
        throw new FieldNotMappedException("pcRenK1");
    }

    @Override
    public void setPcRenK1(AfDecimal pcRenK1) {
        throw new FieldNotMappedException("pcRenK1");
    }

    @Override
    public AfDecimal getPcRenK1Obj() {
        return getPcRenK1();
    }

    @Override
    public void setPcRenK1Obj(AfDecimal pcRenK1Obj) {
        setPcRenK1(new AfDecimal(pcRenK1Obj, 6, 3));
    }

    @Override
    public AfDecimal getPcRenK2() {
        throw new FieldNotMappedException("pcRenK2");
    }

    @Override
    public void setPcRenK2(AfDecimal pcRenK2) {
        throw new FieldNotMappedException("pcRenK2");
    }

    @Override
    public AfDecimal getPcRenK2Obj() {
        return getPcRenK2();
    }

    @Override
    public void setPcRenK2Obj(AfDecimal pcRenK2Obj) {
        setPcRenK2(new AfDecimal(pcRenK2Obj, 6, 3));
    }

    @Override
    public AfDecimal getPcRenK3() {
        throw new FieldNotMappedException("pcRenK3");
    }

    @Override
    public void setPcRenK3(AfDecimal pcRenK3) {
        throw new FieldNotMappedException("pcRenK3");
    }

    @Override
    public AfDecimal getPcRenK3Obj() {
        return getPcRenK3();
    }

    @Override
    public void setPcRenK3Obj(AfDecimal pcRenK3Obj) {
        setPcRenK3(new AfDecimal(pcRenK3Obj, 6, 3));
    }

    @Override
    public AfDecimal getPcRenObj() {
        return getPcRen();
    }

    @Override
    public void setPcRenObj(AfDecimal pcRenObj) {
        setPcRen(new AfDecimal(pcRenObj, 6, 3));
    }

    @Override
    public AfDecimal getPcRiscParz() {
        throw new FieldNotMappedException("pcRiscParz");
    }

    @Override
    public void setPcRiscParz(AfDecimal pcRiscParz) {
        throw new FieldNotMappedException("pcRiscParz");
    }

    @Override
    public AfDecimal getPcRiscParzObj() {
        return getPcRiscParz();
    }

    @Override
    public void setPcRiscParzObj(AfDecimal pcRiscParzObj) {
        setPcRiscParz(new AfDecimal(pcRiscParzObj, 12, 5));
    }

    @Override
    public AfDecimal getRisMat() {
        throw new FieldNotMappedException("risMat");
    }

    @Override
    public void setRisMat(AfDecimal risMat) {
        throw new FieldNotMappedException("risMat");
    }

    @Override
    public AfDecimal getRisMatObj() {
        return getRisMat();
    }

    @Override
    public void setRisMatObj(AfDecimal risMatObj) {
        setRisMat(new AfDecimal(risMatObj, 15, 3));
    }

    @Override
    public AfDecimal getRisSpe() {
        throw new FieldNotMappedException("risSpe");
    }

    @Override
    public void setRisSpe(AfDecimal risSpe) {
        throw new FieldNotMappedException("risSpe");
    }

    @Override
    public AfDecimal getRisSpeObj() {
        return getRisSpe();
    }

    @Override
    public void setRisSpeObj(AfDecimal risSpeObj) {
        setRisSpe(new AfDecimal(risSpeObj, 15, 3));
    }

    @Override
    public AfDecimal getSpeRcs() {
        throw new FieldNotMappedException("speRcs");
    }

    @Override
    public void setSpeRcs(AfDecimal speRcs) {
        throw new FieldNotMappedException("speRcs");
    }

    @Override
    public AfDecimal getSpeRcsObj() {
        return getSpeRcs();
    }

    @Override
    public void setSpeRcsObj(AfDecimal speRcsObj) {
        setSpeRcs(new AfDecimal(speRcsObj, 15, 3));
    }

    @Override
    public AfDecimal getTaxSep() {
        throw new FieldNotMappedException("taxSep");
    }

    @Override
    public void setTaxSep(AfDecimal taxSep) {
        throw new FieldNotMappedException("taxSep");
    }

    @Override
    public AfDecimal getTaxSepObj() {
        return getTaxSep();
    }

    @Override
    public void setTaxSepObj(AfDecimal taxSepObj) {
        setTaxSep(new AfDecimal(taxSepObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIasMggSin() {
        throw new FieldNotMappedException("totIasMggSin");
    }

    @Override
    public void setTotIasMggSin(AfDecimal totIasMggSin) {
        throw new FieldNotMappedException("totIasMggSin");
    }

    @Override
    public AfDecimal getTotIasMggSinObj() {
        return getTotIasMggSin();
    }

    @Override
    public void setTotIasMggSinObj(AfDecimal totIasMggSinObj) {
        setTotIasMggSin(new AfDecimal(totIasMggSinObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIasOnerPrvnt() {
        throw new FieldNotMappedException("totIasOnerPrvnt");
    }

    @Override
    public void setTotIasOnerPrvnt(AfDecimal totIasOnerPrvnt) {
        throw new FieldNotMappedException("totIasOnerPrvnt");
    }

    @Override
    public AfDecimal getTotIasOnerPrvntObj() {
        return getTotIasOnerPrvnt();
    }

    @Override
    public void setTotIasOnerPrvntObj(AfDecimal totIasOnerPrvntObj) {
        setTotIasOnerPrvnt(new AfDecimal(totIasOnerPrvntObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIasPnl() {
        throw new FieldNotMappedException("totIasPnl");
    }

    @Override
    public void setTotIasPnl(AfDecimal totIasPnl) {
        throw new FieldNotMappedException("totIasPnl");
    }

    @Override
    public AfDecimal getTotIasPnlObj() {
        return getTotIasPnl();
    }

    @Override
    public void setTotIasPnlObj(AfDecimal totIasPnlObj) {
        setTotIasPnl(new AfDecimal(totIasPnlObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIasRstDpst() {
        throw new FieldNotMappedException("totIasRstDpst");
    }

    @Override
    public void setTotIasRstDpst(AfDecimal totIasRstDpst) {
        throw new FieldNotMappedException("totIasRstDpst");
    }

    @Override
    public AfDecimal getTotIasRstDpstObj() {
        return getTotIasRstDpst();
    }

    @Override
    public void setTotIasRstDpstObj(AfDecimal totIasRstDpstObj) {
        setTotIasRstDpst(new AfDecimal(totIasRstDpstObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpIntrPrest() {
        throw new FieldNotMappedException("totImpIntrPrest");
    }

    @Override
    public void setTotImpIntrPrest(AfDecimal totImpIntrPrest) {
        throw new FieldNotMappedException("totImpIntrPrest");
    }

    @Override
    public AfDecimal getTotImpIntrPrestObj() {
        return getTotImpIntrPrest();
    }

    @Override
    public void setTotImpIntrPrestObj(AfDecimal totImpIntrPrestObj) {
        setTotImpIntrPrest(new AfDecimal(totImpIntrPrestObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpIs() {
        throw new FieldNotMappedException("totImpIs");
    }

    @Override
    public void setTotImpIs(AfDecimal totImpIs) {
        throw new FieldNotMappedException("totImpIs");
    }

    @Override
    public AfDecimal getTotImpIsObj() {
        return getTotImpIs();
    }

    @Override
    public void setTotImpIsObj(AfDecimal totImpIsObj) {
        setTotImpIs(new AfDecimal(totImpIsObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpLrdLiqto() {
        throw new FieldNotMappedException("totImpLrdLiqto");
    }

    @Override
    public void setTotImpLrdLiqto(AfDecimal totImpLrdLiqto) {
        throw new FieldNotMappedException("totImpLrdLiqto");
    }

    @Override
    public AfDecimal getTotImpLrdLiqtoObj() {
        return getTotImpLrdLiqto();
    }

    @Override
    public void setTotImpLrdLiqtoObj(AfDecimal totImpLrdLiqtoObj) {
        setTotImpLrdLiqto(new AfDecimal(totImpLrdLiqtoObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpNetLiqto() {
        throw new FieldNotMappedException("totImpNetLiqto");
    }

    @Override
    public void setTotImpNetLiqto(AfDecimal totImpNetLiqto) {
        throw new FieldNotMappedException("totImpNetLiqto");
    }

    @Override
    public AfDecimal getTotImpNetLiqtoObj() {
        return getTotImpNetLiqto();
    }

    @Override
    public void setTotImpNetLiqtoObj(AfDecimal totImpNetLiqtoObj) {
        setTotImpNetLiqto(new AfDecimal(totImpNetLiqtoObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpPrest() {
        throw new FieldNotMappedException("totImpPrest");
    }

    @Override
    public void setTotImpPrest(AfDecimal totImpPrest) {
        throw new FieldNotMappedException("totImpPrest");
    }

    @Override
    public AfDecimal getTotImpPrestObj() {
        return getTotImpPrest();
    }

    @Override
    public void setTotImpPrestObj(AfDecimal totImpPrestObj) {
        setTotImpPrest(new AfDecimal(totImpPrestObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpRimb() {
        throw new FieldNotMappedException("totImpRimb");
    }

    @Override
    public void setTotImpRimb(AfDecimal totImpRimb) {
        throw new FieldNotMappedException("totImpRimb");
    }

    @Override
    public AfDecimal getTotImpRimbObj() {
        return getTotImpRimb();
    }

    @Override
    public void setTotImpRimbObj(AfDecimal totImpRimbObj) {
        setTotImpRimb(new AfDecimal(totImpRimbObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpRitAcc() {
        throw new FieldNotMappedException("totImpRitAcc");
    }

    @Override
    public void setTotImpRitAcc(AfDecimal totImpRitAcc) {
        throw new FieldNotMappedException("totImpRitAcc");
    }

    @Override
    public AfDecimal getTotImpRitAccObj() {
        return getTotImpRitAcc();
    }

    @Override
    public void setTotImpRitAccObj(AfDecimal totImpRitAccObj) {
        setTotImpRitAcc(new AfDecimal(totImpRitAccObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpRitTfr() {
        throw new FieldNotMappedException("totImpRitTfr");
    }

    @Override
    public void setTotImpRitTfr(AfDecimal totImpRitTfr) {
        throw new FieldNotMappedException("totImpRitTfr");
    }

    @Override
    public AfDecimal getTotImpRitTfrObj() {
        return getTotImpRitTfr();
    }

    @Override
    public void setTotImpRitTfrObj(AfDecimal totImpRitTfrObj) {
        setTotImpRitTfr(new AfDecimal(totImpRitTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpRitVis() {
        throw new FieldNotMappedException("totImpRitVis");
    }

    @Override
    public void setTotImpRitVis(AfDecimal totImpRitVis) {
        throw new FieldNotMappedException("totImpRitVis");
    }

    @Override
    public AfDecimal getTotImpRitVisObj() {
        return getTotImpRitVis();
    }

    @Override
    public void setTotImpRitVisObj(AfDecimal totImpRitVisObj) {
        setTotImpRitVis(new AfDecimal(totImpRitVisObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpUti() {
        throw new FieldNotMappedException("totImpUti");
    }

    @Override
    public void setTotImpUti(AfDecimal totImpUti) {
        throw new FieldNotMappedException("totImpUti");
    }

    @Override
    public AfDecimal getTotImpUtiObj() {
        return getTotImpUti();
    }

    @Override
    public void setTotImpUtiObj(AfDecimal totImpUtiObj) {
        setTotImpUti(new AfDecimal(totImpUtiObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpbAcc() {
        throw new FieldNotMappedException("totImpbAcc");
    }

    @Override
    public void setTotImpbAcc(AfDecimal totImpbAcc) {
        throw new FieldNotMappedException("totImpbAcc");
    }

    @Override
    public AfDecimal getTotImpbAccObj() {
        return getTotImpbAcc();
    }

    @Override
    public void setTotImpbAccObj(AfDecimal totImpbAccObj) {
        setTotImpbAcc(new AfDecimal(totImpbAccObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpbTfr() {
        throw new FieldNotMappedException("totImpbTfr");
    }

    @Override
    public void setTotImpbTfr(AfDecimal totImpbTfr) {
        throw new FieldNotMappedException("totImpbTfr");
    }

    @Override
    public AfDecimal getTotImpbTfrObj() {
        return getTotImpbTfr();
    }

    @Override
    public void setTotImpbTfrObj(AfDecimal totImpbTfrObj) {
        setTotImpbTfr(new AfDecimal(totImpbTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getTotImpbVis() {
        throw new FieldNotMappedException("totImpbVis");
    }

    @Override
    public void setTotImpbVis(AfDecimal totImpbVis) {
        throw new FieldNotMappedException("totImpbVis");
    }

    @Override
    public AfDecimal getTotImpbVisObj() {
        return getTotImpbVis();
    }

    @Override
    public void setTotImpbVisObj(AfDecimal totImpbVisObj) {
        setTotImpbVis(new AfDecimal(totImpbVisObj, 15, 3));
    }

    @Override
    public String getTpCausAntic() {
        throw new FieldNotMappedException("tpCausAntic");
    }

    @Override
    public void setTpCausAntic(String tpCausAntic) {
        throw new FieldNotMappedException("tpCausAntic");
    }

    @Override
    public String getTpCausAnticObj() {
        return getTpCausAntic();
    }

    @Override
    public void setTpCausAnticObj(String tpCausAnticObj) {
        setTpCausAntic(tpCausAnticObj);
    }

    @Override
    public String getTpLiq() {
        throw new FieldNotMappedException("tpLiq");
    }

    @Override
    public void setTpLiq(String tpLiq) {
        throw new FieldNotMappedException("tpLiq");
    }

    @Override
    public short getTpMetRisc() {
        throw new FieldNotMappedException("tpMetRisc");
    }

    @Override
    public void setTpMetRisc(short tpMetRisc) {
        throw new FieldNotMappedException("tpMetRisc");
    }

    @Override
    public Short getTpMetRiscObj() {
        return ((Short)getTpMetRisc());
    }

    @Override
    public void setTpMetRiscObj(Short tpMetRiscObj) {
        setTpMetRisc(((short)tpMetRiscObj));
    }

    @Override
    public String getTpMezPag() {
        throw new FieldNotMappedException("tpMezPag");
    }

    @Override
    public void setTpMezPag(String tpMezPag) {
        throw new FieldNotMappedException("tpMezPag");
    }

    @Override
    public String getTpMezPagObj() {
        return getTpMezPag();
    }

    @Override
    public void setTpMezPagObj(String tpMezPagObj) {
        setTpMezPag(tpMezPagObj);
    }

    @Override
    public String getTpRimb() {
        throw new FieldNotMappedException("tpRimb");
    }

    @Override
    public void setTpRimb(String tpRimb) {
        throw new FieldNotMappedException("tpRimb");
    }

    @Override
    public String getTpRisc() {
        throw new FieldNotMappedException("tpRisc");
    }

    @Override
    public void setTpRisc(String tpRisc) {
        throw new FieldNotMappedException("tpRisc");
    }

    @Override
    public String getTpRiscObj() {
        return getTpRisc();
    }

    @Override
    public void setTpRiscObj(String tpRiscObj) {
        setTpRisc(tpRiscObj);
    }

    @Override
    public String getTpSin() {
        throw new FieldNotMappedException("tpSin");
    }

    @Override
    public void setTpSin(String tpSin) {
        throw new FieldNotMappedException("tpSin");
    }

    @Override
    public String getTpSinObj() {
        return getTpSin();
    }

    @Override
    public void setTpSinObj(String tpSinObj) {
        setTpSin(tpSinObj);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
