package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.WpcoDati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaLoas0350;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimentoLoas0350;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Loas0350Data;
import it.accenture.jnais.ws.redefines.PcoDtCont;
import it.accenture.jnais.ws.redefines.PcoDtEstrAssMag70a;
import it.accenture.jnais.ws.redefines.PcoDtEstrAssMin70a;
import it.accenture.jnais.ws.redefines.PcoDtRiatRiassComm;
import it.accenture.jnais.ws.redefines.PcoDtRiatRiassRsh;
import it.accenture.jnais.ws.redefines.PcoDtRiclRiriasCom;
import it.accenture.jnais.ws.redefines.PcoDtUltAggErogRe;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCoriC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCoriI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCotrC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCotrI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollEmes;
import it.accenture.jnais.ws.redefines.PcoDtUltBollEmesI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollLiq;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPerfC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPerfI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPreC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPreI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollQuieC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollQuieI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRiat;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRiatI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRpCl;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRpIn;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRspCl;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRspIn;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSdI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSdnlI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSnden;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSndnlq;
import it.accenture.jnais.ws.redefines.PcoDtUltBollStor;
import it.accenture.jnais.ws.redefines.PcoDtUltBollStorI;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsfdtCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsfdtIn;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsricCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsricIn;
import it.accenture.jnais.ws.redefines.PcoDtUltcIsCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcIsIn;
import it.accenture.jnais.ws.redefines.PcoDtUltcMarsol;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiAaC;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiAaI;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiMmC;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiMmI;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiTrI;
import it.accenture.jnais.ws.redefines.PcoDtUltcRbCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcRbIn;
import it.accenture.jnais.ws.redefines.PcoDtUltEcIlColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcIlInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcMrmColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcMrmInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcRivColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcRivInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcTcmColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcTcmInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcUlColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcUlInd;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt92C;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt92I;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt93C;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt93I;
import it.accenture.jnais.ws.redefines.PcoDtUltElabCommef;
import it.accenture.jnais.ws.redefines.PcoDtUltElabCosAt;
import it.accenture.jnais.ws.redefines.PcoDtUltElabCosSt;
import it.accenture.jnais.ws.redefines.PcoDtUltElabLiqmef;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPaspas;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPrAut;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPrCon;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPrlcos;
import it.accenture.jnais.ws.redefines.PcoDtUltElabRedpro;
import it.accenture.jnais.ws.redefines.PcoDtUltElabSpeIn;
import it.accenture.jnais.ws.redefines.PcoDtUltElabTakeP;
import it.accenture.jnais.ws.redefines.PcoDtUltelriscparPr;
import it.accenture.jnais.ws.redefines.PcoDtUltEstrazFug;
import it.accenture.jnais.ws.redefines.PcoDtUltEstrDecCo;
import it.accenture.jnais.ws.redefines.PcoDtUltgzCed;
import it.accenture.jnais.ws.redefines.PcoDtUltgzCedColl;
import it.accenture.jnais.ws.redefines.PcoDtUltgzTrchECl;
import it.accenture.jnais.ws.redefines.PcoDtUltgzTrchEIn;
import it.accenture.jnais.ws.redefines.PcoDtUltQtzoCl;
import it.accenture.jnais.ws.redefines.PcoDtUltQtzoIn;
import it.accenture.jnais.ws.redefines.PcoDtUltRiclPre;
import it.accenture.jnais.ws.redefines.PcoDtUltRiclRiass;
import it.accenture.jnais.ws.redefines.PcoDtUltRinnColl;
import it.accenture.jnais.ws.redefines.PcoDtUltRinnGarac;
import it.accenture.jnais.ws.redefines.PcoDtUltRinnTac;
import it.accenture.jnais.ws.redefines.PcoDtUltRivalCl;
import it.accenture.jnais.ws.redefines.PcoDtUltRivalIn;
import it.accenture.jnais.ws.redefines.PcoDtUltscElabCl;
import it.accenture.jnais.ws.redefines.PcoDtUltscElabIn;
import it.accenture.jnais.ws.redefines.PcoDtUltscOpzCl;
import it.accenture.jnais.ws.redefines.PcoDtUltscOpzIn;
import it.accenture.jnais.ws.redefines.PcoDtUltTabulRiass;
import it.accenture.jnais.ws.redefines.PcoStstXRegione;
import it.accenture.jnais.ws.redefines.WpcoAaUti;
import it.accenture.jnais.ws.redefines.WpcoArrotPre;
import it.accenture.jnais.ws.redefines.WpcoDtCont;
import it.accenture.jnais.ws.redefines.WpcoDtEstrAssMag70a;
import it.accenture.jnais.ws.redefines.WpcoDtEstrAssMin70a;
import it.accenture.jnais.ws.redefines.WpcoDtRiatRiassComm;
import it.accenture.jnais.ws.redefines.WpcoDtRiatRiassRsh;
import it.accenture.jnais.ws.redefines.WpcoDtRiclRiriasCom;
import it.accenture.jnais.ws.redefines.WpcoDtUltAggErogRe;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollCoriC;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollCoriI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollCotrC;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollCotrI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollEmes;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollEmesI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollLiq;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollPerfC;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollPerfI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollPreC;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollPreI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollQuieC;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollQuieI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRiat;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRiatI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRpCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRpIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRspCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRspIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollSdI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollSdnlI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollSnden;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollSndnlq;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollStor;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollStorI;
import it.accenture.jnais.ws.redefines.WpcoDtUltcBnsfdtCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltcBnsfdtIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltcBnsricCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltcBnsricIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltcIsCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltcIsIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltcMarsol;
import it.accenture.jnais.ws.redefines.WpcoDtUltcPildiAaC;
import it.accenture.jnais.ws.redefines.WpcoDtUltcPildiAaI;
import it.accenture.jnais.ws.redefines.WpcoDtUltcPildiMmC;
import it.accenture.jnais.ws.redefines.WpcoDtUltcPildiMmI;
import it.accenture.jnais.ws.redefines.WpcoDtUltcPildiTrI;
import it.accenture.jnais.ws.redefines.WpcoDtUltcRbCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltcRbIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcIlColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcIlInd;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcMrmColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcMrmInd;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcRivColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcRivInd;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcTcmColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcTcmInd;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcUlColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcUlInd;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabAt92C;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabAt92I;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabAt93C;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabAt93I;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabCommef;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabCosAt;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabCosSt;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabLiqmef;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabPaspas;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabPrAut;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabPrCon;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabPrlcos;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabRedpro;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabSpeIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabTakeP;
import it.accenture.jnais.ws.redefines.WpcoDtUltelriscparPr;
import it.accenture.jnais.ws.redefines.WpcoDtUltEstrazFug;
import it.accenture.jnais.ws.redefines.WpcoDtUltEstrDecCo;
import it.accenture.jnais.ws.redefines.WpcoDtUltgzCed;
import it.accenture.jnais.ws.redefines.WpcoDtUltgzCedColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltgzTrchECl;
import it.accenture.jnais.ws.redefines.WpcoDtUltgzTrchEIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltQtzoCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltQtzoIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltRiclPre;
import it.accenture.jnais.ws.redefines.WpcoDtUltRiclRiass;
import it.accenture.jnais.ws.redefines.WpcoDtUltRinnColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltRinnGarac;
import it.accenture.jnais.ws.redefines.WpcoDtUltRinnTac;
import it.accenture.jnais.ws.redefines.WpcoDtUltRivalCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltRivalIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltscElabCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltscElabIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltscOpzCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltscOpzIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltTabulRiass;
import it.accenture.jnais.ws.redefines.WpcoFrqCostiAtt;
import it.accenture.jnais.ws.redefines.WpcoFrqCostiStornati;
import it.accenture.jnais.ws.redefines.WpcoGgIntrRitPag;
import it.accenture.jnais.ws.redefines.WpcoGgMaxRecProv;
import it.accenture.jnais.ws.redefines.WpcoImpAssSociale;
import it.accenture.jnais.ws.redefines.WpcoLimVltr;
import it.accenture.jnais.ws.redefines.WpcoLmCSubrshConIn;
import it.accenture.jnais.ws.redefines.WpcoLmRisConInt;
import it.accenture.jnais.ws.redefines.WpcoNumGgArrIntrPr;
import it.accenture.jnais.ws.redefines.WpcoNumMmCalcMora;
import it.accenture.jnais.ws.redefines.WpcoPcCSubrshMarsol;
import it.accenture.jnais.ws.redefines.WpcoPcGarNoriskMars;
import it.accenture.jnais.ws.redefines.WpcoPcProv1aaAcq;
import it.accenture.jnais.ws.redefines.WpcoPcRidImp1382011;
import it.accenture.jnais.ws.redefines.WpcoPcRidImp662014;
import it.accenture.jnais.ws.redefines.WpcoPcRmMarsol;
import it.accenture.jnais.ws.redefines.WpcoSoglAmlPrePer;
import it.accenture.jnais.ws.redefines.WpcoSoglAmlPreSavR;
import it.accenture.jnais.ws.redefines.WpcoSoglAmlPreUni;
import it.accenture.jnais.ws.redefines.WpcoStstXRegione;

/**Original name: LOAS0350<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             AISS.
 * DATE-WRITTEN.       01/2008.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *     PROGRAMMA ..... LOAS0350
 *     TIPOLOGIA...... GESTIONE PARAMETRO COMPAGNIA
 *     PROCESSO....... OPERAZIONI AUTOMATICA
 *     FUNZIONE....... TRASVERSALI
 *     DESCRIZIONE.... AGGIORNAMENTO DATA ELABORAZIONE
 * **------------------------------------------------------------***</pre>*/
public class Loas0350 extends BatchProgram {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Loas0350Data ws = new Loas0350Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: AREA-LOAS0350
    private AreaLoas0350 areaLoas0350;

    //==== METHODS ====
    /**Original name: PROGRAM_LOAS0350_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, AreaLoas0350 areaLoas0350) {
        this.areaIdsv0001 = areaIdsv0001;
        this.areaLoas0350 = areaLoas0350;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Loas0350 getInstance() {
        return ((Loas0350)Programs.getInstance(Loas0350.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI                                         *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE WK-VARIABILI.
        initWkVariabili();
        // COB_CODE: PERFORM INIZIA-TOT-PCO
        //              THRU INIZIA-TOT-PCO-EX.
        iniziaTotPco();
        // COB_CODE: EVALUATE S350-FORMA-ASS
        //             WHEN 'IN'
        //                MOVE 'IN'           TO WS-FORMA2
        //             WHEN 'CO'
        //                MOVE 'CO'           TO WS-FORMA2
        //             WHEN OTHER
        //                MOVE 'CO'           TO WS-FORMA2
        //           END-EVALUATE.
        switch (areaLoas0350.getFormaAss()) {

            case "IN":// COB_CODE: MOVE 'IN'           TO WS-FORMA1
                ws.setWsForma1("IN");
                // COB_CODE: MOVE 'IN'           TO WS-FORMA2
                ws.setWsForma2("IN");
                break;

            case "CO":// COB_CODE: MOVE 'CO'           TO WS-FORMA1
                ws.setWsForma1("CO");
                // COB_CODE: MOVE 'CO'           TO WS-FORMA2
                ws.setWsForma2("CO");
                break;

            default:// COB_CODE: MOVE 'IN'           TO WS-FORMA1
                ws.setWsForma1("IN");
                // COB_CODE: MOVE 'CO'           TO WS-FORMA2
                ws.setWsForma2("CO");
                break;
        }
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO   TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S2100-LEGGI-PAR-COMP
        //              THRU EX-S2100
        s2100LeggiParComp();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //           END-EVALUATE.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //-->      RINNOVO TARIFFE ACCESSORIE 6001
            //-->      CALCOLO BONUS RICORRENTE 6016
            // COB_CODE:         EVALUATE TRUE
            //           *-->      RINNOVO TARIFFE ACCESSORIE 6001
            //                     WHEN RITAR-ACCES
            //                           THRU EX-S1100
            //           *-->      GENERAZIONE RISCATTI PARZIALI PROGRAMMATI
            //                     WHEN GENER-RISPAR
            //                          THRU  EX-S1200
            //           *-->      ADEGUAMENTO PREMIO PRESTAZIONE 6006
            //                     WHEN ADPRE-PRESTA
            //                           THRU EX-S1300
            //           *-->      GENERAZIONE CEDOLA 6007
            //                     WHEN GENER-CEDOLA
            //                           THRU EX-S1400
            //           *-->      RINNOVO CONTRATTO COLLETTIVO 6008
            //                     WHEN RICON-COLLET
            //                           THRU EX-S1500
            //           *-->      SCADENZA - DIFFERIMENTO - PROROGA 6009
            //                     WHEN VARIA-OPZION
            //                           THRU EX-S1600
            //           *-->      CALCOLO IMPOSTA SOSTITUTIVA 6010
            //                     WHEN CALC-IMPOSTA-SOSTIT
            //                           THRU EX-S1700
            //           *-->      CALCOLO BONUS FEDELTA' 6013
            //                     WHEN BONUS-FEDE
            //           *-->      CALCOLO BONUS RICORRENTE 6016
            //                     WHEN BONUS-RICOR
            //                           THRU EX-S1800
            //           *-->      SIGNIFICATIVITA DEL RISCHIO 6014
            //                     WHEN SIGNI-RISCH
            //                           THRU EX-S1900
            //           *-->      ATTUAZIONE STRATEGIA DI INVESTIMENTO 6015
            //                     WHEN ATTUA-STINV
            //                           THRU EX-S2000
            //           *-->      PRELIEVO COSTI GARANZIA ACCESSORIA 6017
            //                     WHEN DETERM-BONUS
            //                           THRU EX-S2200
            //           *-->      QUIETANZAMENTO 6101 6102
            //                     WHEN MOVIM-QUIETA
            //                     WHEN MOVIM-QUIETA-INT-PREST
            //                           THRU EX-S2300
            //           *-->      GENERAZIONE TRANCHE STANDARD 6002
            //                     WHEN GENER-TRANCH
            //                           THRU EX-S2400
            //           *-->      RINNOVO TACITO 6050
            //                     WHEN RINN-TAC-KM2009
            //                           THRU EX-S2500
            //           *-->      PASSO DOPO PASSO  2321
            //                     WHEN SW-PASSO-PASSO
            //                           THRU EX-S2600
            //           *-->      PASSO REDDITO PROGRAMMATO 2316
            //                     WHEN RPP-REDDITO-PROGRAMMATO
            //                           THRU EX-S2700
            //           *-->      PASSO REDDITO PROGRAMMATO 2318
            //                     WHEN RPP-TAKE-PROFIT
            //                           THRU EX-S2800
            //           *-->      ESTRAZIONE-DECESSI-COMUNICATI 6063
            //                     WHEN ESTRAZIONE-DECESSI-COMUNICATI
            //                           THRU EX-S2900
            //           *-->      ESTRAZIONE-CALCOLO-COSTI 6064
            //                     WHEN ESTRAZIONE-CALCOLO-COSTI
            //                           THRU EX-S3000
            //                     WHEN OTHER
            //                        CONTINUE
            //                END-EVALUATE.
            switch (ws.getWsMovimento().getWsMovimentoFormatted()) {

                case WsMovimentoLoas0350.RITAR_ACCES:// COB_CODE: PERFORM S1100-GEST-6001
                    //              THRU EX-S1100
                    s1100Gest6001();
                    //-->      GENERAZIONE RISCATTI PARZIALI PROGRAMMATI
                    break;

                case WsMovimentoLoas0350.GENER_RISPAR:// COB_CODE: PERFORM S1200-GEST-6005
                    //             THRU  EX-S1200
                    s1200Gest6005();
                    //-->      ADEGUAMENTO PREMIO PRESTAZIONE 6006
                    break;

                case WsMovimentoLoas0350.ADPRE_PRESTA:// COB_CODE: PERFORM S1300-GEST-6006
                    //              THRU EX-S1300
                    s1300Gest6006();
                    //-->      GENERAZIONE CEDOLA 6007
                    break;

                case WsMovimentoLoas0350.GENER_CEDOLA:// COB_CODE: PERFORM S1400-GEST-6007
                    //              THRU EX-S1400
                    s1400Gest6007();
                    //-->      RINNOVO CONTRATTO COLLETTIVO 6008
                    break;

                case WsMovimentoLoas0350.RICON_COLLET:// COB_CODE: PERFORM S1500-GEST-6008
                    //              THRU EX-S1500
                    s1500Gest6008();
                    //-->      SCADENZA - DIFFERIMENTO - PROROGA 6009
                    break;

                case WsMovimentoLoas0350.VARIA_OPZION:// COB_CODE: PERFORM S1600-GEST-6009
                    //              THRU EX-S1600
                    s1600Gest6009();
                    //-->      CALCOLO IMPOSTA SOSTITUTIVA 6010
                    break;

                case WsMovimentoLoas0350.CALC_IMPOSTA_SOSTIT:// COB_CODE: PERFORM S1700-GEST-6010
                    //              THRU EX-S1700
                    s1700Gest6010();
                    //-->      CALCOLO BONUS FEDELTA' 6013
                    break;

                case WsMovimentoLoas0350.BONUS_FEDE:
                case WsMovimentoLoas0350.BONUS_RICOR:// COB_CODE: PERFORM S1800-GEST-BONUS
                    //              THRU EX-S1800
                    s1800GestBonus();
                    //-->      SIGNIFICATIVITA DEL RISCHIO 6014
                    break;

                case WsMovimentoLoas0350.SIGNI_RISCH:// COB_CODE: PERFORM S1900-GEST-6014
                    //              THRU EX-S1900
                    s1900Gest6014();
                    //-->      ATTUAZIONE STRATEGIA DI INVESTIMENTO 6015
                    break;

                case WsMovimentoLoas0350.ATTUA_STINV:// COB_CODE: PERFORM S2000-GEST-6015
                    //              THRU EX-S2000
                    s2000Gest6015();
                    //-->      PRELIEVO COSTI GARANZIA ACCESSORIA 6017
                    break;

                case WsMovimentoLoas0350.DETERM_BONUS:// COB_CODE: PERFORM S2200-GEST-6017
                    //              THRU EX-S2200
                    s2200Gest6017();
                    //-->      QUIETANZAMENTO 6101 6102
                    break;

                case WsMovimentoLoas0350.MOVIM_QUIETA:
                case WsMovimentoLoas0350.MOVIM_QUIETA_INT_PREST:// COB_CODE: PERFORM S2300-GEST-6101
                    //              THRU EX-S2300
                    s2300Gest6101();
                    //-->      GENERAZIONE TRANCHE STANDARD 6002
                    break;

                case WsMovimentoLoas0350.GENER_TRANCH:// COB_CODE: PERFORM S2400-GEST-6002
                    //              THRU EX-S2400
                    s2400Gest6002();
                    //-->      RINNOVO TACITO 6050
                    break;

                case WsMovimentoLoas0350.RINN_TAC_KM2009:// COB_CODE: PERFORM S2500-GEST-6050
                    //              THRU EX-S2500
                    s2500Gest6050();
                    //-->      PASSO DOPO PASSO  2321
                    break;

                case WsMovimentoLoas0350.SW_PASSO_PASSO:// COB_CODE: PERFORM S2600-GEST-2321
                    //              THRU EX-S2600
                    s2600Gest2321();
                    //-->      PASSO REDDITO PROGRAMMATO 2316
                    break;

                case WsMovimentoLoas0350.RPP_REDDITO_PROGRAMMATO:// COB_CODE: PERFORM S2700-GEST-2316
                    //              THRU EX-S2700
                    s2700Gest2316();
                    //-->      PASSO REDDITO PROGRAMMATO 2318
                    break;

                case WsMovimentoLoas0350.RPP_TAKE_PROFIT:// COB_CODE: PERFORM S2800-GEST-2318
                    //              THRU EX-S2800
                    s2800Gest2318();
                    //-->      ESTRAZIONE-DECESSI-COMUNICATI 6063
                    break;

                case WsMovimentoLoas0350.ESTRAZIONE_DECESSI_COMUNICATI:// COB_CODE: PERFORM S2900-GEST-6063
                    //              THRU EX-S2900
                    s2900Gest6063();
                    //-->      ESTRAZIONE-CALCOLO-COSTI 6064
                    break;

                case WsMovimentoLoas0350.ESTRAZIONE_CALCOLO_COSTI:// COB_CODE: PERFORM S3000-GEST-6064
                    //              THRU EX-S3000
                    s3000Gest6064();
                    break;

                default:// COB_CODE: CONTINUE
                //continue
                    break;
            }
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU AGGIORNA-PARAM-COMP-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM AGGIORNA-PARAM-COMP
            //              THRU AGGIORNA-PARAM-COMP-EX
            aggiornaParamComp();
        }
    }

    /**Original name: S2100-LEGGI-PAR-COMP<br>
	 * <pre>----------------------------------------------------------------
	 *      LETTURA PARAMETRO COMPAGNIA
	 * ----------------------------------------------------------------</pre>*/
    private void s2100LeggiParComp() {
        // COB_CODE: MOVE HIGH-VALUE               TO PARAM-COMP.
        ws.getParamComp().initParamCompHighValues();
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE 'PARAM-COMP'                TO WK-TABELLA.
        ws.setWkTabella("PARAM-COMP");
        // COB_CODE: SET  IDSV0001-ESITO-OK           TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: SET  IDSI0011-SELECT             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET  IDSI0011-PRIMARY-KEY        TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET  IDSI0011-TRATT-SENZA-STOR   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: MOVE WK-TABELLA                  TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO PCO-COD-COMP-ANIA.
        ws.getParamComp().setPcoCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE PARAM-COMP                  TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getParamComp().getParamCompFormatted());
        // COB_CODE: PERFORM LETTURA-PCO              THRU LETTURA-PCO-EX.
        letturaPco();
    }

    /**Original name: S1100-GEST-6001<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6001
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100Gest6001() {
        // COB_CODE: IF WS-FORMA1 = 'IN'
        //              SET  WPCO-ST-MOD               TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma1(), "IN")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-RINN-GARAC
            ws.getLccvpco1().getDati().getWpcoDtUltRinnGarac().setWpcoDtUltRinnGarac(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD               TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
    }

    /**Original name: S1200-GEST-6005<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6005
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200Gest6005() {
        // COB_CODE: IF WS-FORMA1 = 'IN'
        //              SET  WPCO-ST-MOD              TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma1(), "IN")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTELRISCPAR-PR
            ws.getLccvpco1().getDati().getWpcoDtUltelriscparPr().setWpcoDtUltelriscparPr(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
    }

    /**Original name: S1300-GEST-6006<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6006
	 * ----------------------------------------------------------------*</pre>*/
    private void s1300Gest6006() {
        // COB_CODE: IF WS-FORMA1 = 'IN'
        //              SET  WPCO-ST-MOD              TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma1(), "IN")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-RIVAL-IN
            ws.getLccvpco1().getDati().getWpcoDtUltRivalIn().setWpcoDtUltRivalIn(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
        // COB_CODE: IF WS-FORMA2 = 'CO'
        //             SET  WPCO-ST-MOD               TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma2(), "CO")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-RIVAL-CL
            ws.getLccvpco1().getDati().getWpcoDtUltRivalCl().setWpcoDtUltRivalCl(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD               TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
    }

    /**Original name: S1400-GEST-6007<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6007
	 * ----------------------------------------------------------------*</pre>*/
    private void s1400Gest6007() {
        // COB_CODE: IF WS-FORMA1 = 'IN'
        //              SET  WPCO-ST-MOD              TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma1(), "IN")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTGZ-CED
            ws.getLccvpco1().getDati().getWpcoDtUltgzCed().setWpcoDtUltgzCed(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
        // COB_CODE: IF WS-FORMA2 = 'CO'
        //              SET  WPCO-ST-MOD              TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma2(), "CO")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTGZ-CED-COLL
            ws.getLccvpco1().getDati().getWpcoDtUltgzCedColl().setWpcoDtUltgzCedColl(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
    }

    /**Original name: S1500-GEST-6008<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6008
	 * ----------------------------------------------------------------*</pre>*/
    private void s1500Gest6008() {
        // COB_CODE: IF WS-FORMA2 = 'CO'
        //               SET  WPCO-ST-MOD              TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma2(), "CO")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-RINN-COLL
            ws.getLccvpco1().getDati().getWpcoDtUltRinnColl().setWpcoDtUltRinnColl(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
    }

    /**Original name: S1600-GEST-6009<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6009
	 * ----------------------------------------------------------------*</pre>*/
    private void s1600Gest6009() {
        // COB_CODE: EVALUATE S350-FL-TP-OPZ-SCAD
        //              WHEN 'N'
        //                END-IF
        //              WHEN 'S'
        //                END-IF
        //              WHEN OTHER
        //                CONTINUE
        //           END-EVALUATE.
        switch (areaLoas0350.getFlTpOpzScad()) {

            case 'N':// COB_CODE: IF WS-FORMA1 = 'IN'
                //              SET  WPCO-ST-MOD     TO TRUE
                //           END-IF
                if (Conditions.eq(ws.getWsForma1(), "IN")) {
                    // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE
                    //             TO WPCO-DT-ULTSC-ELAB-IN
                    ws.getLccvpco1().getDati().getWpcoDtUltscElabIn().setWpcoDtUltscElabIn(areaLoas0350.getDtUltElaborazione());
                    // COB_CODE: SET  WPCO-ST-MOD     TO TRUE
                    ws.getLccvpco1().getStatus().setMod();
                }
                // COB_CODE: IF WS-FORMA2 = 'CO'
                //              SET  WPCO-ST-MOD     TO TRUE
                //           END-IF
                if (Conditions.eq(ws.getWsForma2(), "CO")) {
                    // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE
                    //             TO WPCO-DT-ULTSC-ELAB-CL
                    ws.getLccvpco1().getDati().getWpcoDtUltscElabCl().setWpcoDtUltscElabCl(areaLoas0350.getDtUltElaborazione());
                    // COB_CODE: SET  WPCO-ST-MOD     TO TRUE
                    ws.getLccvpco1().getStatus().setMod();
                }
                break;

            case 'S':// COB_CODE: IF WS-FORMA1 = 'IN'
                //             SET  WPCO-ST-MOD     TO TRUE
                //           END-IF
                if (Conditions.eq(ws.getWsForma1(), "IN")) {
                    // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE
                    //             TO WPCO-DT-ULTSC-OPZ-IN
                    ws.getLccvpco1().getDati().getWpcoDtUltscOpzIn().setWpcoDtUltscOpzIn(areaLoas0350.getDtUltElaborazione());
                    // COB_CODE: SET  WPCO-ST-MOD     TO TRUE
                    ws.getLccvpco1().getStatus().setMod();
                }
                // COB_CODE: IF WS-FORMA2 = 'CO'
                //             SET  WPCO-ST-MOD     TO TRUE
                //           END-IF
                if (Conditions.eq(ws.getWsForma2(), "CO")) {
                    // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE
                    //             TO WPCO-DT-ULTSC-OPZ-CL
                    ws.getLccvpco1().getDati().getWpcoDtUltscOpzCl().setWpcoDtUltscOpzCl(areaLoas0350.getDtUltElaborazione());
                    // COB_CODE: SET  WPCO-ST-MOD     TO TRUE
                    ws.getLccvpco1().getStatus().setMod();
                }
                break;

            default:// COB_CODE: CONTINUE
            //continue
                break;
        }
    }

    /**Original name: S1700-GEST-6010<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6010
	 * ----------------------------------------------------------------*</pre>*/
    private void s1700Gest6010() {
        // COB_CODE: IF WS-FORMA1 = 'IN'
        //              SET  WPCO-ST-MOD              TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma1(), "IN")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-IS-IN
            ws.getLccvpco1().getDati().getWpcoDtUltcIsIn().setWpcoDtUltcIsIn(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
        // COB_CODE: IF WS-FORMA2 = 'CO'
        //              SET  WPCO-ST-MOD              TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma2(), "CO")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-IS-CL
            ws.getLccvpco1().getDati().getWpcoDtUltcIsCl().setWpcoDtUltcIsCl(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
    }

    /**Original name: S1800-GEST-BONUS<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6013
	 * ----------------------------------------------------------------*</pre>*/
    private void s1800GestBonus() {
        // COB_CODE: IF S350-FL-BONUS-FEDELTA  = 'S'
        //              END-IF
        //           END-IF.
        if (areaLoas0350.getFlBonusFedelta() == 'S') {
            // COB_CODE: IF WS-FORMA1 = 'IN'
            //            SET  WPCO-ST-MOD              TO TRUE
            //           END-IF
            if (Conditions.eq(ws.getWsForma1(), "IN")) {
                // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-BNSFDT-IN
                ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtIn().setWpcoDtUltcBnsfdtIn(areaLoas0350.getDtUltElaborazione());
                // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
                ws.getLccvpco1().getStatus().setMod();
            }
            // COB_CODE: IF WS-FORMA2 = 'CO'
            //            SET  WPCO-ST-MOD              TO TRUE
            //           END-IF
            if (Conditions.eq(ws.getWsForma2(), "CO")) {
                // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-BNSFDT-CL
                ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtCl().setWpcoDtUltcBnsfdtCl(areaLoas0350.getDtUltElaborazione());
                // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
                ws.getLccvpco1().getStatus().setMod();
            }
        }
        // COB_CODE: IF S350-FL-BONUS-RICORRENTE  = 'S'
        //              END-IF
        //           END-IF.
        if (areaLoas0350.getFlBonusRicorrente() == 'S') {
            // COB_CODE: IF WS-FORMA1 = 'IN'
            //            SET  WPCO-ST-MOD              TO TRUE
            //           END-IF
            if (Conditions.eq(ws.getWsForma1(), "IN")) {
                // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-BNSRIC-IN
                ws.getLccvpco1().getDati().getWpcoDtUltcBnsricIn().setWpcoDtUltcBnsricIn(areaLoas0350.getDtUltElaborazione());
                // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
                ws.getLccvpco1().getStatus().setMod();
            }
            // COB_CODE: IF WS-FORMA2 = 'CO'
            //            SET  WPCO-ST-MOD              TO TRUE
            //           END-IF
            if (Conditions.eq(ws.getWsForma2(), "CO")) {
                // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTC-BNSRIC-CL
                ws.getLccvpco1().getDati().getWpcoDtUltcBnsricCl().setWpcoDtUltcBnsricCl(areaLoas0350.getDtUltElaborazione());
                // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
                ws.getLccvpco1().getStatus().setMod();
            }
        }
    }

    /**Original name: S1900-GEST-6014<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6014
	 * ----------------------------------------------------------------*</pre>*/
    private void s1900Gest6014() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: S2000-GEST-6015<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6015
	 * ----------------------------------------------------------------*</pre>*/
    private void s2000Gest6015() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: S2200-GEST-6017<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6017
	 * ----------------------------------------------------------------*</pre>*/
    private void s2200Gest6017() {
        // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE
        //             TO WPCO-DT-ULT-ELAB-PRLCOS
        ws.getLccvpco1().getDati().getWpcoDtUltElabPrlcos().setWpcoDtUltElabPrlcos(areaLoas0350.getDtUltElaborazione());
        // COB_CODE: SET  WPCO-ST-MOD TO TRUE.
        ws.getLccvpco1().getStatus().setMod();
    }

    /**Original name: S2300-GEST-6101<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6101
	 * ----------------------------------------------------------------*</pre>*/
    private void s2300Gest6101() {
        // COB_CODE: IF WS-FORMA1 = 'IN'
        //              SET  WPCO-ST-MOD              TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma1(), "IN")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-QTZO-IN
            ws.getLccvpco1().getDati().getWpcoDtUltQtzoIn().setWpcoDtUltQtzoIn(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
        // COB_CODE: IF WS-FORMA2 = 'CO'
        //              SET  WPCO-ST-MOD              TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma2(), "CO")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-QTZO-CL
            ws.getLccvpco1().getDati().getWpcoDtUltQtzoCl().setWpcoDtUltQtzoCl(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
    }

    /**Original name: S2400-GEST-6002<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6002
	 * ----------------------------------------------------------------*</pre>*/
    private void s2400Gest6002() {
        // COB_CODE: IF WS-FORMA1 = 'IN'
        //              SET  WPCO-ST-MOD              TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma1(), "IN")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTGZ-TRCH-E-IN
            ws.getLccvpco1().getDati().getWpcoDtUltgzTrchEIn().setWpcoDtUltgzTrchEIn(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
        // COB_CODE: IF WS-FORMA2 = 'CO'
        //              SET  WPCO-ST-MOD              TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getWsForma2(), "CO")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULTGZ-TRCH-E-CL
            ws.getLccvpco1().getDati().getWpcoDtUltgzTrchECl().setWpcoDtUltgzTrchECl(areaLoas0350.getDtUltElaborazione());
            // COB_CODE: SET  WPCO-ST-MOD              TO TRUE
            ws.getLccvpco1().getStatus().setMod();
        }
    }

    /**Original name: S2500-GEST-6050<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6050
	 * ----------------------------------------------------------------*</pre>*/
    private void s2500Gest6050() {
        // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-RINN-TAC
        ws.getLccvpco1().getDati().getWpcoDtUltRinnTac().setWpcoDtUltRinnTac(areaLoas0350.getDtUltElaborazione());
        // COB_CODE: SET  WPCO-ST-MOD              TO TRUE.
        ws.getLccvpco1().getStatus().setMod();
    }

    /**Original name: S2600-GEST-2321<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 2321
	 * ----------------------------------------------------------------*</pre>*/
    private void s2600Gest2321() {
        // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-ELAB-PASPAS
        ws.getLccvpco1().getDati().getWpcoDtUltElabPaspas().setWpcoDtUltElabPaspas(areaLoas0350.getDtUltElaborazione());
        // COB_CODE: SET  WPCO-ST-MOD              TO TRUE.
        ws.getLccvpco1().getStatus().setMod();
    }

    /**Original name: S2700-GEST-2316<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 2316
	 * ----------------------------------------------------------------*</pre>*/
    private void s2700Gest2316() {
        // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-ELAB-REDPRO
        ws.getLccvpco1().getDati().getWpcoDtUltElabRedpro().setWpcoDtUltElabRedpro(areaLoas0350.getDtUltElaborazione());
        // COB_CODE: SET  WPCO-ST-MOD              TO TRUE.
        ws.getLccvpco1().getStatus().setMod();
    }

    /**Original name: S2800-GEST-2318<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 2318
	 * ----------------------------------------------------------------*</pre>*/
    private void s2800Gest2318() {
        // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE TO WPCO-DT-ULT-ELAB-TAKE-P.
        ws.getLccvpco1().getDati().getWpcoDtUltElabTakeP().setWpcoDtUltElabTakeP(areaLoas0350.getDtUltElaborazione());
        // COB_CODE: SET  WPCO-ST-MOD              TO TRUE.
        ws.getLccvpco1().getStatus().setMod();
    }

    /**Original name: S2900-GEST-6063<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6063
	 * ----------------------------------------------------------------*</pre>*/
    private void s2900Gest6063() {
        // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-ESTR-DEC-CO
        ws.getLccvpco1().getDati().getWpcoDtUltEstrDecCo().setWpcoDtUltEstrDecCo(areaLoas0350.getDtUltElaborazione());
        // COB_CODE: SET  WPCO-ST-MOD               TO TRUE.
        ws.getLccvpco1().getStatus().setMod();
    }

    /**Original name: S3000-GEST-6064<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE MOVIMENTO 6064
	 * ----------------------------------------------------------------*</pre>*/
    private void s3000Gest6064() {
        // COB_CODE: IF S350-TP-ELAB-COSTI = 'AT'
        //              MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-ELAB-COS-AT
        //           ELSE
        //              MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-ELAB-COS-ST
        //           END-IF.
        if (Conditions.eq(areaLoas0350.getTpElabCosti(), "AT")) {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-ELAB-COS-AT
            ws.getLccvpco1().getDati().getWpcoDtUltElabCosAt().setWpcoDtUltElabCosAt(areaLoas0350.getDtUltElaborazione());
        }
        else {
            // COB_CODE: MOVE S350-DT-ULT-ELABORAZIONE  TO WPCO-DT-ULT-ELAB-COS-ST
            ws.getLccvpco1().getDati().getWpcoDtUltElabCosSt().setWpcoDtUltElabCosSt(areaLoas0350.getDtUltElaborazione());
        }
        //
        // COB_CODE: SET  WPCO-ST-MOD               TO TRUE.
        ws.getLccvpco1().getStatus().setMod();
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: AGGIORNA-TABELLA<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PROCEDURE (COMPONENTI COMUNI)
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     COPY      ..... LCCP0001
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void aggiornaTabella() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            // COB_CODE:         EVALUATE TRUE
            //           *-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            //                     WHEN IDSO0011-SUCCESSFUL-SQL
            //                        CONTINUE
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                     WHEN OTHER
            //                           THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    break;

                default:// COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'AGGIORNA-TABELLA'
                    //                                 TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
                    // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA            ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE   INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'AGGIORNA-TABELLA'
            //                                      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA            ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE        INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: LETTURA-PCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PROCEDURE LETTURA PORTAFOGLIO
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA                          *
	 *    CHIAMATA AL DISPATCHER PARAM-COMP                            *
	 *    ULTIMO AGG.  01 MARZO 2007                                   *
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *                GESTIONE LETTURA (SELECT/FETCH)                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaPco() {
        // COB_CODE: IF IDSI0011-SELECT
        //                 THRU SELECT-PCO-EX
        //           ELSE
        //                 THRU FETCH-PCO-EX
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect()) {
            // COB_CODE: PERFORM SELECT-PCO
            //              THRU SELECT-PCO-EX
            selectPco();
        }
        else {
            // COB_CODE: PERFORM FETCH-PCO
            //              THRU FETCH-PCO-EX
            fetchPco();
        }
        // COB_CODE: MOVE IX-TAB-PCO
        //             TO (SF)-ELE-PAR-COMP-MAX.
        ws.setWpcoEleParCompMax(TruncAbs.toInt(ws.getIxTabPco(), 4));
    }

    /**Original name: SELECT-PCO<br>
	 * <pre>----------------------------------------------------------------*
	 *                          SELECT                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void selectPco() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //               THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:         IF IDSO0011-SUCCESSFUL-RC
        //                      END-EVALUATE
        //                   ELSE
        //           *-->       GESTIRE ERRORE DISPATCHER
        //                         THRU EX-S0300
        //                   END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:            EVALUATE TRUE
            //                          WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                                THRU VALORIZZA-OUTPUT-PCO-EX
            //                          WHEN IDSO0011-NOT-FOUND
            //           *--->          CHIAVE NON TROVATA
            //                                  THRU EX-S0300
            //                          WHEN OTHER
            //           *--->          ERRORE DI ACCESSO AL DB
            //                                  THRU EX-S0300
            //                      END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO PARAM-COMP
                    ws.getParamComp().setParamCompFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE 1               TO IX-TAB-PCO
                    ws.setIxTabPco(((short)1));
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-PCO
                    //              THRU VALORIZZA-OUTPUT-PCO-EX
                    valorizzaOutputPco();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->          CHIAVE NON TROVATA
                    // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'SELECT-PCO'  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("SELECT-PCO");
                    // COB_CODE: MOVE '005017'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005017");
                    // COB_CODE: MOVE SPACES        TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default://--->          ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'SELECT-PCO'  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("SELECT-PCO");
                    // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'PARAM-COMP'         ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "PARAM-COMP", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->       GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SELECT-PCO'  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("SELECT-PCO");
            // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'PARAM-COMP'          ';'
            //                   IDSO0011-RETURN-CODE ';'
            //                   IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "PARAM-COMP", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: FETCH-PCO<br>
	 * <pre>----------------------------------------------------------------*
	 *                          FETCH                                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void fetchPco() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET  WCOM-OVERFLOW-NO                  TO TRUE.
        ws.getWcomFlagOverflow().setNo();
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR NOT IDSO0011-SUCCESSFUL-SQL
        //                      OR WCOM-OVERFLOW-YES
        //               END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || !ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().isSuccessfulSql() || ws.getWcomFlagOverflow().isYes())) {
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:          IF IDSO0011-SUCCESSFUL-RC
            //                       END-EVALUATE
            //                    ELSE
            //           *-->        ERRORE DISPATCHER
            //                          THRU EX-S0300
            //                    END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:             EVALUATE TRUE
                //                           WHEN IDSO0011-NOT-FOUND
                //           *-->            CHIAVE NON TROVATA
                //                                END-IF
                //                           WHEN IDSO0011-SUCCESSFUL-SQL
                //           *-->            OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                 END-IF
                //                           WHEN OTHER
                //           *-->            ERRORE DI ACCESSO AL DB
                //                                   THRU EX-S0300
                //                       END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.NOT_FOUND://-->            CHIAVE NON TROVATA
                        // COB_CODE: IF IDSI0011-FETCH-FIRST
                        //                 THRU EX-S0300
                        //           END-IF
                        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                            // COB_CODE: MOVE WK-PGM
                            //             TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                            // COB_CODE: MOVE 'FETCH-PCO'
                            //             TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr("FETCH-PCO");
                            // COB_CODE: MOVE '005017'
                            //             TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("005017");
                            // COB_CODE: MOVE SPACES
                            //             TO IEAI9901-PARAMETRI-ERR
                            ws.getIeai9901Area().setParametriErr("");
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //              THRU EX-S0300
                            s0300RicercaGravitaErrore();
                        }
                        break;

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->            OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: MOVE IDSO0011-BUFFER-DATI  TO PARAM-COMP
                        ws.getParamComp().setParamCompFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        // COB_CODE: ADD  1                     TO IX-TAB-PCO
                        ws.setIxTabPco(Trunc.toShort(1 + ws.getIxTabPco(), 4));
                        // COB_CODE: IF IX-TAB-PCO > (SF)-ELE-PAR-COMP-MAX
                        //              SET WCOM-OVERFLOW-YES   TO TRUE
                        //           ELSE
                        //              SET IDSI0011-FETCH-NEXT TO TRUE
                        //           END-IF
                        if (ws.getIxTabPco() > ws.getWpcoEleParCompMax()) {
                            // COB_CODE: SET WCOM-OVERFLOW-YES   TO TRUE
                            ws.getWcomFlagOverflow().setYes();
                        }
                        else {
                            // COB_CODE: PERFORM VALORIZZA-OUTPUT-PCO
                            //              THRU VALORIZZA-OUTPUT-PCO-EX
                            valorizzaOutputPco();
                            // COB_CODE: SET IDSI0011-FETCH-NEXT TO TRUE
                            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                        }
                        break;

                    default://-->            ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'FETCH-PCO'  TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("FETCH-PCO");
                        // COB_CODE: MOVE '005016'     TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005016");
                        // COB_CODE: STRING 'PARAM-COMP'         ';'
                        //                  IDSO0011-RETURN-CODE ';'
                        //                  IDSO0011-SQLCODE
                        //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "PARAM-COMP", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //-->        ERRORE DISPATCHER
                // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'FETCH-PCO'   TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("FETCH-PCO");
                // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING 'PARAM-COMP'         ';'
                //                   IDSO0011-RETURN-CODE ';'
                //                   IDSO0011-SQLCODE
                //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "PARAM-COMP", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: VALORIZZA-OUTPUT-PCO<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVPCO3
	 *    ULTIMO AGG. 13 NOV 2018
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputPco() {
        // COB_CODE: MOVE PCO-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA
        ws.getLccvpco1().getDati().setWpcoCodCompAnia(ws.getParamComp().getPcoCodCompAnia());
        // COB_CODE: IF PCO-COD-TRAT-CIRT-NULL = HIGH-VALUES
        //                TO (SF)-COD-TRAT-CIRT-NULL
        //           ELSE
        //                TO (SF)-COD-TRAT-CIRT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoCodTratCirtFormatted())) {
            // COB_CODE: MOVE PCO-COD-TRAT-CIRT-NULL
            //             TO (SF)-COD-TRAT-CIRT-NULL
            ws.getLccvpco1().getDati().setWpcoCodTratCirt(ws.getParamComp().getPcoCodTratCirt());
        }
        else {
            // COB_CODE: MOVE PCO-COD-TRAT-CIRT
            //             TO (SF)-COD-TRAT-CIRT
            ws.getLccvpco1().getDati().setWpcoCodTratCirt(ws.getParamComp().getPcoCodTratCirt());
        }
        // COB_CODE: IF PCO-LIM-VLTR-NULL = HIGH-VALUES
        //                TO (SF)-LIM-VLTR-NULL
        //           ELSE
        //                TO (SF)-LIM-VLTR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoLimVltr().getPcoLimVltrNullFormatted())) {
            // COB_CODE: MOVE PCO-LIM-VLTR-NULL
            //             TO (SF)-LIM-VLTR-NULL
            ws.getLccvpco1().getDati().getWpcoLimVltr().setWpcoLimVltrNull(ws.getParamComp().getPcoLimVltr().getPcoLimVltrNull());
        }
        else {
            // COB_CODE: MOVE PCO-LIM-VLTR
            //             TO (SF)-LIM-VLTR
            ws.getLccvpco1().getDati().getWpcoLimVltr().setWpcoLimVltr(Trunc.toDecimal(ws.getParamComp().getPcoLimVltr().getPcoLimVltr(), 15, 3));
        }
        // COB_CODE: IF PCO-TP-RAT-PERF-NULL = HIGH-VALUES
        //                TO (SF)-TP-RAT-PERF-NULL
        //           ELSE
        //                TO (SF)-TP-RAT-PERF
        //           END-IF
        if (Conditions.eq(ws.getParamComp().getPcoTpRatPerf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PCO-TP-RAT-PERF-NULL
            //             TO (SF)-TP-RAT-PERF-NULL
            ws.getLccvpco1().getDati().setWpcoTpRatPerf(ws.getParamComp().getPcoTpRatPerf());
        }
        else {
            // COB_CODE: MOVE PCO-TP-RAT-PERF
            //             TO (SF)-TP-RAT-PERF
            ws.getLccvpco1().getDati().setWpcoTpRatPerf(ws.getParamComp().getPcoTpRatPerf());
        }
        // COB_CODE: MOVE PCO-TP-LIV-GENZ-TIT
        //             TO (SF)-TP-LIV-GENZ-TIT
        ws.getLccvpco1().getDati().setWpcoTpLivGenzTit(ws.getParamComp().getPcoTpLivGenzTit());
        // COB_CODE: IF PCO-ARROT-PRE-NULL = HIGH-VALUES
        //                TO (SF)-ARROT-PRE-NULL
        //           ELSE
        //                TO (SF)-ARROT-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoArrotPre().getPcoArrotPreNullFormatted())) {
            // COB_CODE: MOVE PCO-ARROT-PRE-NULL
            //             TO (SF)-ARROT-PRE-NULL
            ws.getLccvpco1().getDati().getWpcoArrotPre().setWpcoArrotPreNull(ws.getParamComp().getPcoArrotPre().getPcoArrotPreNull());
        }
        else {
            // COB_CODE: MOVE PCO-ARROT-PRE
            //             TO (SF)-ARROT-PRE
            ws.getLccvpco1().getDati().getWpcoArrotPre().setWpcoArrotPre(Trunc.toDecimal(ws.getParamComp().getPcoArrotPre().getPcoArrotPre(), 15, 3));
        }
        // COB_CODE: IF PCO-DT-CONT-NULL = HIGH-VALUES
        //                TO (SF)-DT-CONT-NULL
        //           ELSE
        //                TO (SF)-DT-CONT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtCont().getPcoDtContNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-CONT-NULL
            //             TO (SF)-DT-CONT-NULL
            ws.getLccvpco1().getDati().getWpcoDtCont().setWpcoDtContNull(ws.getParamComp().getPcoDtCont().getPcoDtContNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-CONT
            //             TO (SF)-DT-CONT
            ws.getLccvpco1().getDati().getWpcoDtCont().setWpcoDtCont(ws.getParamComp().getPcoDtCont().getPcoDtCont());
        }
        // COB_CODE: IF PCO-DT-ULT-RIVAL-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-RIVAL-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-RIVAL-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltRivalIn().getPcoDtUltRivalInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-RIVAL-IN-NULL
            //             TO (SF)-DT-ULT-RIVAL-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltRivalIn().setWpcoDtUltRivalInNull(ws.getParamComp().getPcoDtUltRivalIn().getPcoDtUltRivalInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-RIVAL-IN
            //             TO (SF)-DT-ULT-RIVAL-IN
            ws.getLccvpco1().getDati().getWpcoDtUltRivalIn().setWpcoDtUltRivalIn(ws.getParamComp().getPcoDtUltRivalIn().getPcoDtUltRivalIn());
        }
        // COB_CODE: IF PCO-DT-ULT-QTZO-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-QTZO-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-QTZO-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltQtzoIn().getPcoDtUltQtzoInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-QTZO-IN-NULL
            //             TO (SF)-DT-ULT-QTZO-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltQtzoIn().setWpcoDtUltQtzoInNull(ws.getParamComp().getPcoDtUltQtzoIn().getPcoDtUltQtzoInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-QTZO-IN
            //             TO (SF)-DT-ULT-QTZO-IN
            ws.getLccvpco1().getDati().getWpcoDtUltQtzoIn().setWpcoDtUltQtzoIn(ws.getParamComp().getPcoDtUltQtzoIn().getPcoDtUltQtzoIn());
        }
        // COB_CODE: IF PCO-DT-ULT-RICL-RIASS-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-RICL-RIASS-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-RICL-RIASS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltRiclRiass().getPcoDtUltRiclRiassNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-RICL-RIASS-NULL
            //             TO (SF)-DT-ULT-RICL-RIASS-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltRiclRiass().setWpcoDtUltRiclRiassNull(ws.getParamComp().getPcoDtUltRiclRiass().getPcoDtUltRiclRiassNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-RICL-RIASS
            //             TO (SF)-DT-ULT-RICL-RIASS
            ws.getLccvpco1().getDati().getWpcoDtUltRiclRiass().setWpcoDtUltRiclRiass(ws.getParamComp().getPcoDtUltRiclRiass().getPcoDtUltRiclRiass());
        }
        // COB_CODE: IF PCO-DT-ULT-TABUL-RIASS-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-TABUL-RIASS-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-TABUL-RIASS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltTabulRiass().getPcoDtUltTabulRiassNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-TABUL-RIASS-NULL
            //             TO (SF)-DT-ULT-TABUL-RIASS-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltTabulRiass().setWpcoDtUltTabulRiassNull(ws.getParamComp().getPcoDtUltTabulRiass().getPcoDtUltTabulRiassNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-TABUL-RIASS
            //             TO (SF)-DT-ULT-TABUL-RIASS
            ws.getLccvpco1().getDati().getWpcoDtUltTabulRiass().setWpcoDtUltTabulRiass(ws.getParamComp().getPcoDtUltTabulRiass().getPcoDtUltTabulRiass());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-EMES-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-EMES-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-EMES
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollEmes().getPcoDtUltBollEmesNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-EMES-NULL
            //             TO (SF)-DT-ULT-BOLL-EMES-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollEmes().setWpcoDtUltBollEmesNull(ws.getParamComp().getPcoDtUltBollEmes().getPcoDtUltBollEmesNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-EMES
            //             TO (SF)-DT-ULT-BOLL-EMES
            ws.getLccvpco1().getDati().getWpcoDtUltBollEmes().setWpcoDtUltBollEmes(ws.getParamComp().getPcoDtUltBollEmes().getPcoDtUltBollEmes());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-STOR-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-STOR-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-STOR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollStor().getPcoDtUltBollStorNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-STOR-NULL
            //             TO (SF)-DT-ULT-BOLL-STOR-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollStor().setWpcoDtUltBollStorNull(ws.getParamComp().getPcoDtUltBollStor().getPcoDtUltBollStorNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-STOR
            //             TO (SF)-DT-ULT-BOLL-STOR
            ws.getLccvpco1().getDati().getWpcoDtUltBollStor().setWpcoDtUltBollStor(ws.getParamComp().getPcoDtUltBollStor().getPcoDtUltBollStor());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-LIQ-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-LIQ-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-LIQ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollLiq().getPcoDtUltBollLiqNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-LIQ-NULL
            //             TO (SF)-DT-ULT-BOLL-LIQ-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollLiq().setWpcoDtUltBollLiqNull(ws.getParamComp().getPcoDtUltBollLiq().getPcoDtUltBollLiqNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-LIQ
            //             TO (SF)-DT-ULT-BOLL-LIQ
            ws.getLccvpco1().getDati().getWpcoDtUltBollLiq().setWpcoDtUltBollLiq(ws.getParamComp().getPcoDtUltBollLiq().getPcoDtUltBollLiq());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RIAT-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-RIAT-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-RIAT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollRiat().getPcoDtUltBollRiatNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RIAT-NULL
            //             TO (SF)-DT-ULT-BOLL-RIAT-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollRiat().setWpcoDtUltBollRiatNull(ws.getParamComp().getPcoDtUltBollRiat().getPcoDtUltBollRiatNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RIAT
            //             TO (SF)-DT-ULT-BOLL-RIAT
            ws.getLccvpco1().getDati().getWpcoDtUltBollRiat().setWpcoDtUltBollRiat(ws.getParamComp().getPcoDtUltBollRiat().getPcoDtUltBollRiat());
        }
        // COB_CODE: IF PCO-DT-ULTELRISCPAR-PR-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTELRISCPAR-PR-NULL
        //           ELSE
        //                TO (SF)-DT-ULTELRISCPAR-PR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltelriscparPr().getPcoDtUltelriscparPrNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTELRISCPAR-PR-NULL
            //             TO (SF)-DT-ULTELRISCPAR-PR-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltelriscparPr().setWpcoDtUltelriscparPrNull(ws.getParamComp().getPcoDtUltelriscparPr().getPcoDtUltelriscparPrNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTELRISCPAR-PR
            //             TO (SF)-DT-ULTELRISCPAR-PR
            ws.getLccvpco1().getDati().getWpcoDtUltelriscparPr().setWpcoDtUltelriscparPr(ws.getParamComp().getPcoDtUltelriscparPr().getPcoDtUltelriscparPr());
        }
        // COB_CODE: IF PCO-DT-ULTC-IS-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-IS-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-IS-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcIsIn().getPcoDtUltcIsInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-IS-IN-NULL
            //             TO (SF)-DT-ULTC-IS-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcIsIn().setWpcoDtUltcIsInNull(ws.getParamComp().getPcoDtUltcIsIn().getPcoDtUltcIsInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-IS-IN
            //             TO (SF)-DT-ULTC-IS-IN
            ws.getLccvpco1().getDati().getWpcoDtUltcIsIn().setWpcoDtUltcIsIn(ws.getParamComp().getPcoDtUltcIsIn().getPcoDtUltcIsIn());
        }
        // COB_CODE: IF PCO-DT-ULT-RICL-PRE-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-RICL-PRE-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-RICL-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltRiclPre().getPcoDtUltRiclPreNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-RICL-PRE-NULL
            //             TO (SF)-DT-ULT-RICL-PRE-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltRiclPre().setWpcoDtUltRiclPreNull(ws.getParamComp().getPcoDtUltRiclPre().getPcoDtUltRiclPreNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-RICL-PRE
            //             TO (SF)-DT-ULT-RICL-PRE
            ws.getLccvpco1().getDati().getWpcoDtUltRiclPre().setWpcoDtUltRiclPre(ws.getParamComp().getPcoDtUltRiclPre().getPcoDtUltRiclPre());
        }
        // COB_CODE: IF PCO-DT-ULTC-MARSOL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-MARSOL-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-MARSOL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcMarsol().getPcoDtUltcMarsolNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-MARSOL-NULL
            //             TO (SF)-DT-ULTC-MARSOL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcMarsol().setWpcoDtUltcMarsolNull(ws.getParamComp().getPcoDtUltcMarsol().getPcoDtUltcMarsolNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-MARSOL
            //             TO (SF)-DT-ULTC-MARSOL
            ws.getLccvpco1().getDati().getWpcoDtUltcMarsol().setWpcoDtUltcMarsol(ws.getParamComp().getPcoDtUltcMarsol().getPcoDtUltcMarsol());
        }
        // COB_CODE: IF PCO-DT-ULTC-RB-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-RB-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-RB-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcRbIn().getPcoDtUltcRbInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-RB-IN-NULL
            //             TO (SF)-DT-ULTC-RB-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcRbIn().setWpcoDtUltcRbInNull(ws.getParamComp().getPcoDtUltcRbIn().getPcoDtUltcRbInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-RB-IN
            //             TO (SF)-DT-ULTC-RB-IN
            ws.getLccvpco1().getDati().getWpcoDtUltcRbIn().setWpcoDtUltcRbIn(ws.getParamComp().getPcoDtUltcRbIn().getPcoDtUltcRbIn());
        }
        // COB_CODE: IF PCO-PC-PROV-1AA-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-PC-PROV-1AA-ACQ-NULL
        //           ELSE
        //                TO (SF)-PC-PROV-1AA-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoPcProv1aaAcq().getPcoPcProv1aaAcqNullFormatted())) {
            // COB_CODE: MOVE PCO-PC-PROV-1AA-ACQ-NULL
            //             TO (SF)-PC-PROV-1AA-ACQ-NULL
            ws.getLccvpco1().getDati().getWpcoPcProv1aaAcq().setWpcoPcProv1aaAcqNull(ws.getParamComp().getPcoPcProv1aaAcq().getPcoPcProv1aaAcqNull());
        }
        else {
            // COB_CODE: MOVE PCO-PC-PROV-1AA-ACQ
            //             TO (SF)-PC-PROV-1AA-ACQ
            ws.getLccvpco1().getDati().getWpcoPcProv1aaAcq().setWpcoPcProv1aaAcq(Trunc.toDecimal(ws.getParamComp().getPcoPcProv1aaAcq().getPcoPcProv1aaAcq(), 6, 3));
        }
        // COB_CODE: IF PCO-MOD-INTR-PREST-NULL = HIGH-VALUES
        //                TO (SF)-MOD-INTR-PREST-NULL
        //           ELSE
        //                TO (SF)-MOD-INTR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoModIntrPrestFormatted())) {
            // COB_CODE: MOVE PCO-MOD-INTR-PREST-NULL
            //             TO (SF)-MOD-INTR-PREST-NULL
            ws.getLccvpco1().getDati().setWpcoModIntrPrest(ws.getParamComp().getPcoModIntrPrest());
        }
        else {
            // COB_CODE: MOVE PCO-MOD-INTR-PREST
            //             TO (SF)-MOD-INTR-PREST
            ws.getLccvpco1().getDati().setWpcoModIntrPrest(ws.getParamComp().getPcoModIntrPrest());
        }
        // COB_CODE: IF PCO-GG-MAX-REC-PROV-NULL = HIGH-VALUES
        //                TO (SF)-GG-MAX-REC-PROV-NULL
        //           ELSE
        //                TO (SF)-GG-MAX-REC-PROV
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoGgMaxRecProv().getPcoGgMaxRecProvNullFormatted())) {
            // COB_CODE: MOVE PCO-GG-MAX-REC-PROV-NULL
            //             TO (SF)-GG-MAX-REC-PROV-NULL
            ws.getLccvpco1().getDati().getWpcoGgMaxRecProv().setWpcoGgMaxRecProvNull(ws.getParamComp().getPcoGgMaxRecProv().getPcoGgMaxRecProvNull());
        }
        else {
            // COB_CODE: MOVE PCO-GG-MAX-REC-PROV
            //             TO (SF)-GG-MAX-REC-PROV
            ws.getLccvpco1().getDati().getWpcoGgMaxRecProv().setWpcoGgMaxRecProv(ws.getParamComp().getPcoGgMaxRecProv().getPcoGgMaxRecProv());
        }
        // COB_CODE: IF PCO-DT-ULTGZ-TRCH-E-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTGZ-TRCH-E-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULTGZ-TRCH-E-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltgzTrchEIn().getPcoDtUltgzTrchEInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-TRCH-E-IN-NULL
            //             TO (SF)-DT-ULTGZ-TRCH-E-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltgzTrchEIn().setWpcoDtUltgzTrchEInNull(ws.getParamComp().getPcoDtUltgzTrchEIn().getPcoDtUltgzTrchEInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTGZ-TRCH-E-IN
            //             TO (SF)-DT-ULTGZ-TRCH-E-IN
            ws.getLccvpco1().getDati().getWpcoDtUltgzTrchEIn().setWpcoDtUltgzTrchEIn(ws.getParamComp().getPcoDtUltgzTrchEIn().getPcoDtUltgzTrchEIn());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-SNDEN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-SNDEN-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-SNDEN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollSnden().getPcoDtUltBollSndenNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SNDEN-NULL
            //             TO (SF)-DT-ULT-BOLL-SNDEN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollSnden().setWpcoDtUltBollSndenNull(ws.getParamComp().getPcoDtUltBollSnden().getPcoDtUltBollSndenNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SNDEN
            //             TO (SF)-DT-ULT-BOLL-SNDEN
            ws.getLccvpco1().getDati().getWpcoDtUltBollSnden().setWpcoDtUltBollSnden(ws.getParamComp().getPcoDtUltBollSnden().getPcoDtUltBollSnden());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-SNDNLQ-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-SNDNLQ-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-SNDNLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollSndnlq().getPcoDtUltBollSndnlqNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SNDNLQ-NULL
            //             TO (SF)-DT-ULT-BOLL-SNDNLQ-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollSndnlq().setWpcoDtUltBollSndnlqNull(ws.getParamComp().getPcoDtUltBollSndnlq().getPcoDtUltBollSndnlqNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SNDNLQ
            //             TO (SF)-DT-ULT-BOLL-SNDNLQ
            ws.getLccvpco1().getDati().getWpcoDtUltBollSndnlq().setWpcoDtUltBollSndnlq(ws.getParamComp().getPcoDtUltBollSndnlq().getPcoDtUltBollSndnlq());
        }
        // COB_CODE: IF PCO-DT-ULTSC-ELAB-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTSC-ELAB-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULTSC-ELAB-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltscElabIn().getPcoDtUltscElabInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTSC-ELAB-IN-NULL
            //             TO (SF)-DT-ULTSC-ELAB-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltscElabIn().setWpcoDtUltscElabInNull(ws.getParamComp().getPcoDtUltscElabIn().getPcoDtUltscElabInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTSC-ELAB-IN
            //             TO (SF)-DT-ULTSC-ELAB-IN
            ws.getLccvpco1().getDati().getWpcoDtUltscElabIn().setWpcoDtUltscElabIn(ws.getParamComp().getPcoDtUltscElabIn().getPcoDtUltscElabIn());
        }
        // COB_CODE: IF PCO-DT-ULTSC-OPZ-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTSC-OPZ-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULTSC-OPZ-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltscOpzIn().getPcoDtUltscOpzInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTSC-OPZ-IN-NULL
            //             TO (SF)-DT-ULTSC-OPZ-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltscOpzIn().setWpcoDtUltscOpzInNull(ws.getParamComp().getPcoDtUltscOpzIn().getPcoDtUltscOpzInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTSC-OPZ-IN
            //             TO (SF)-DT-ULTSC-OPZ-IN
            ws.getLccvpco1().getDati().getWpcoDtUltscOpzIn().setWpcoDtUltscOpzIn(ws.getParamComp().getPcoDtUltscOpzIn().getPcoDtUltscOpzIn());
        }
        // COB_CODE: IF PCO-DT-ULTC-BNSRIC-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-BNSRIC-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-BNSRIC-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcBnsricIn().getPcoDtUltcBnsricInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSRIC-IN-NULL
            //             TO (SF)-DT-ULTC-BNSRIC-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcBnsricIn().setWpcoDtUltcBnsricInNull(ws.getParamComp().getPcoDtUltcBnsricIn().getPcoDtUltcBnsricInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSRIC-IN
            //             TO (SF)-DT-ULTC-BNSRIC-IN
            ws.getLccvpco1().getDati().getWpcoDtUltcBnsricIn().setWpcoDtUltcBnsricIn(ws.getParamComp().getPcoDtUltcBnsricIn().getPcoDtUltcBnsricIn());
        }
        // COB_CODE: IF PCO-DT-ULTC-BNSFDT-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-BNSFDT-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-BNSFDT-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcBnsfdtIn().getPcoDtUltcBnsfdtInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSFDT-IN-NULL
            //             TO (SF)-DT-ULTC-BNSFDT-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtIn().setWpcoDtUltcBnsfdtInNull(ws.getParamComp().getPcoDtUltcBnsfdtIn().getPcoDtUltcBnsfdtInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSFDT-IN
            //             TO (SF)-DT-ULTC-BNSFDT-IN
            ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtIn().setWpcoDtUltcBnsfdtIn(ws.getParamComp().getPcoDtUltcBnsfdtIn().getPcoDtUltcBnsfdtIn());
        }
        // COB_CODE: IF PCO-DT-ULT-RINN-GARAC-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-RINN-GARAC-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-RINN-GARAC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltRinnGarac().getPcoDtUltRinnGaracNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-GARAC-NULL
            //             TO (SF)-DT-ULT-RINN-GARAC-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltRinnGarac().setWpcoDtUltRinnGaracNull(ws.getParamComp().getPcoDtUltRinnGarac().getPcoDtUltRinnGaracNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-GARAC
            //             TO (SF)-DT-ULT-RINN-GARAC
            ws.getLccvpco1().getDati().getWpcoDtUltRinnGarac().setWpcoDtUltRinnGarac(ws.getParamComp().getPcoDtUltRinnGarac().getPcoDtUltRinnGarac());
        }
        // COB_CODE: IF PCO-DT-ULTGZ-CED-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTGZ-CED-NULL
        //           ELSE
        //                TO (SF)-DT-ULTGZ-CED
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltgzCed().getPcoDtUltgzCedNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-CED-NULL
            //             TO (SF)-DT-ULTGZ-CED-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltgzCed().setWpcoDtUltgzCedNull(ws.getParamComp().getPcoDtUltgzCed().getPcoDtUltgzCedNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTGZ-CED
            //             TO (SF)-DT-ULTGZ-CED
            ws.getLccvpco1().getDati().getWpcoDtUltgzCed().setWpcoDtUltgzCed(ws.getParamComp().getPcoDtUltgzCed().getPcoDtUltgzCed());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-PRLCOS-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-PRLCOS-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-PRLCOS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabPrlcos().getPcoDtUltElabPrlcosNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PRLCOS-NULL
            //             TO (SF)-DT-ULT-ELAB-PRLCOS-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabPrlcos().setWpcoDtUltElabPrlcosNull(ws.getParamComp().getPcoDtUltElabPrlcos().getPcoDtUltElabPrlcosNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PRLCOS
            //             TO (SF)-DT-ULT-ELAB-PRLCOS
            ws.getLccvpco1().getDati().getWpcoDtUltElabPrlcos().setWpcoDtUltElabPrlcos(ws.getParamComp().getPcoDtUltElabPrlcos().getPcoDtUltElabPrlcos());
        }
        // COB_CODE: IF PCO-DT-ULT-RINN-COLL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-RINN-COLL-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-RINN-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltRinnColl().getPcoDtUltRinnCollNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-COLL-NULL
            //             TO (SF)-DT-ULT-RINN-COLL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltRinnColl().setWpcoDtUltRinnCollNull(ws.getParamComp().getPcoDtUltRinnColl().getPcoDtUltRinnCollNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-COLL
            //             TO (SF)-DT-ULT-RINN-COLL
            ws.getLccvpco1().getDati().getWpcoDtUltRinnColl().setWpcoDtUltRinnColl(ws.getParamComp().getPcoDtUltRinnColl().getPcoDtUltRinnColl());
        }
        // COB_CODE: IF PCO-FL-RVC-PERF-NULL = HIGH-VALUES
        //                TO (SF)-FL-RVC-PERF-NULL
        //           ELSE
        //                TO (SF)-FL-RVC-PERF
        //           END-IF
        if (Conditions.eq(ws.getParamComp().getPcoFlRvcPerf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PCO-FL-RVC-PERF-NULL
            //             TO (SF)-FL-RVC-PERF-NULL
            ws.getLccvpco1().getDati().setWpcoFlRvcPerf(ws.getParamComp().getPcoFlRvcPerf());
        }
        else {
            // COB_CODE: MOVE PCO-FL-RVC-PERF
            //             TO (SF)-FL-RVC-PERF
            ws.getLccvpco1().getDati().setWpcoFlRvcPerf(ws.getParamComp().getPcoFlRvcPerf());
        }
        // COB_CODE: IF PCO-FL-RCS-POLI-NOPERF-NULL = HIGH-VALUES
        //                TO (SF)-FL-RCS-POLI-NOPERF-NULL
        //           ELSE
        //                TO (SF)-FL-RCS-POLI-NOPERF
        //           END-IF
        if (Conditions.eq(ws.getParamComp().getPcoFlRcsPoliNoperf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PCO-FL-RCS-POLI-NOPERF-NULL
            //             TO (SF)-FL-RCS-POLI-NOPERF-NULL
            ws.getLccvpco1().getDati().setWpcoFlRcsPoliNoperf(ws.getParamComp().getPcoFlRcsPoliNoperf());
        }
        else {
            // COB_CODE: MOVE PCO-FL-RCS-POLI-NOPERF
            //             TO (SF)-FL-RCS-POLI-NOPERF
            ws.getLccvpco1().getDati().setWpcoFlRcsPoliNoperf(ws.getParamComp().getPcoFlRcsPoliNoperf());
        }
        // COB_CODE: IF PCO-FL-GEST-PLUSV-NULL = HIGH-VALUES
        //                TO (SF)-FL-GEST-PLUSV-NULL
        //           ELSE
        //                TO (SF)-FL-GEST-PLUSV
        //           END-IF
        if (Conditions.eq(ws.getParamComp().getPcoFlGestPlusv(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PCO-FL-GEST-PLUSV-NULL
            //             TO (SF)-FL-GEST-PLUSV-NULL
            ws.getLccvpco1().getDati().setWpcoFlGestPlusv(ws.getParamComp().getPcoFlGestPlusv());
        }
        else {
            // COB_CODE: MOVE PCO-FL-GEST-PLUSV
            //             TO (SF)-FL-GEST-PLUSV
            ws.getLccvpco1().getDati().setWpcoFlGestPlusv(ws.getParamComp().getPcoFlGestPlusv());
        }
        // COB_CODE: IF PCO-DT-ULT-RIVAL-CL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-RIVAL-CL-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-RIVAL-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltRivalCl().getPcoDtUltRivalClNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-RIVAL-CL-NULL
            //             TO (SF)-DT-ULT-RIVAL-CL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltRivalCl().setWpcoDtUltRivalClNull(ws.getParamComp().getPcoDtUltRivalCl().getPcoDtUltRivalClNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-RIVAL-CL
            //             TO (SF)-DT-ULT-RIVAL-CL
            ws.getLccvpco1().getDati().getWpcoDtUltRivalCl().setWpcoDtUltRivalCl(ws.getParamComp().getPcoDtUltRivalCl().getPcoDtUltRivalCl());
        }
        // COB_CODE: IF PCO-DT-ULT-QTZO-CL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-QTZO-CL-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-QTZO-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltQtzoCl().getPcoDtUltQtzoClNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-QTZO-CL-NULL
            //             TO (SF)-DT-ULT-QTZO-CL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltQtzoCl().setWpcoDtUltQtzoClNull(ws.getParamComp().getPcoDtUltQtzoCl().getPcoDtUltQtzoClNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-QTZO-CL
            //             TO (SF)-DT-ULT-QTZO-CL
            ws.getLccvpco1().getDati().getWpcoDtUltQtzoCl().setWpcoDtUltQtzoCl(ws.getParamComp().getPcoDtUltQtzoCl().getPcoDtUltQtzoCl());
        }
        // COB_CODE: IF PCO-DT-ULTC-BNSRIC-CL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-BNSRIC-CL-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-BNSRIC-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcBnsricCl().getPcoDtUltcBnsricClNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSRIC-CL-NULL
            //             TO (SF)-DT-ULTC-BNSRIC-CL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcBnsricCl().setWpcoDtUltcBnsricClNull(ws.getParamComp().getPcoDtUltcBnsricCl().getPcoDtUltcBnsricClNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSRIC-CL
            //             TO (SF)-DT-ULTC-BNSRIC-CL
            ws.getLccvpco1().getDati().getWpcoDtUltcBnsricCl().setWpcoDtUltcBnsricCl(ws.getParamComp().getPcoDtUltcBnsricCl().getPcoDtUltcBnsricCl());
        }
        // COB_CODE: IF PCO-DT-ULTC-BNSFDT-CL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-BNSFDT-CL-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-BNSFDT-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcBnsfdtCl().getPcoDtUltcBnsfdtClNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSFDT-CL-NULL
            //             TO (SF)-DT-ULTC-BNSFDT-CL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtCl().setWpcoDtUltcBnsfdtClNull(ws.getParamComp().getPcoDtUltcBnsfdtCl().getPcoDtUltcBnsfdtClNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSFDT-CL
            //             TO (SF)-DT-ULTC-BNSFDT-CL
            ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtCl().setWpcoDtUltcBnsfdtCl(ws.getParamComp().getPcoDtUltcBnsfdtCl().getPcoDtUltcBnsfdtCl());
        }
        // COB_CODE: IF PCO-DT-ULTC-IS-CL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-IS-CL-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-IS-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcIsCl().getPcoDtUltcIsClNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-IS-CL-NULL
            //             TO (SF)-DT-ULTC-IS-CL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcIsCl().setWpcoDtUltcIsClNull(ws.getParamComp().getPcoDtUltcIsCl().getPcoDtUltcIsClNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-IS-CL
            //             TO (SF)-DT-ULTC-IS-CL
            ws.getLccvpco1().getDati().getWpcoDtUltcIsCl().setWpcoDtUltcIsCl(ws.getParamComp().getPcoDtUltcIsCl().getPcoDtUltcIsCl());
        }
        // COB_CODE: IF PCO-DT-ULTC-RB-CL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-RB-CL-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-RB-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcRbCl().getPcoDtUltcRbClNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-RB-CL-NULL
            //             TO (SF)-DT-ULTC-RB-CL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcRbCl().setWpcoDtUltcRbClNull(ws.getParamComp().getPcoDtUltcRbCl().getPcoDtUltcRbClNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-RB-CL
            //             TO (SF)-DT-ULTC-RB-CL
            ws.getLccvpco1().getDati().getWpcoDtUltcRbCl().setWpcoDtUltcRbCl(ws.getParamComp().getPcoDtUltcRbCl().getPcoDtUltcRbCl());
        }
        // COB_CODE: IF PCO-DT-ULTGZ-TRCH-E-CL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTGZ-TRCH-E-CL-NULL
        //           ELSE
        //                TO (SF)-DT-ULTGZ-TRCH-E-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltgzTrchECl().getPcoDtUltgzTrchEClNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-TRCH-E-CL-NULL
            //             TO (SF)-DT-ULTGZ-TRCH-E-CL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltgzTrchECl().setWpcoDtUltgzTrchEClNull(ws.getParamComp().getPcoDtUltgzTrchECl().getPcoDtUltgzTrchEClNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTGZ-TRCH-E-CL
            //             TO (SF)-DT-ULTGZ-TRCH-E-CL
            ws.getLccvpco1().getDati().getWpcoDtUltgzTrchECl().setWpcoDtUltgzTrchECl(ws.getParamComp().getPcoDtUltgzTrchECl().getPcoDtUltgzTrchECl());
        }
        // COB_CODE: IF PCO-DT-ULTSC-ELAB-CL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTSC-ELAB-CL-NULL
        //           ELSE
        //                TO (SF)-DT-ULTSC-ELAB-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltscElabCl().getPcoDtUltscElabClNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTSC-ELAB-CL-NULL
            //             TO (SF)-DT-ULTSC-ELAB-CL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltscElabCl().setWpcoDtUltscElabClNull(ws.getParamComp().getPcoDtUltscElabCl().getPcoDtUltscElabClNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTSC-ELAB-CL
            //             TO (SF)-DT-ULTSC-ELAB-CL
            ws.getLccvpco1().getDati().getWpcoDtUltscElabCl().setWpcoDtUltscElabCl(ws.getParamComp().getPcoDtUltscElabCl().getPcoDtUltscElabCl());
        }
        // COB_CODE: IF PCO-DT-ULTSC-OPZ-CL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTSC-OPZ-CL-NULL
        //           ELSE
        //                TO (SF)-DT-ULTSC-OPZ-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltscOpzCl().getPcoDtUltscOpzClNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTSC-OPZ-CL-NULL
            //             TO (SF)-DT-ULTSC-OPZ-CL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltscOpzCl().setWpcoDtUltscOpzClNull(ws.getParamComp().getPcoDtUltscOpzCl().getPcoDtUltscOpzClNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTSC-OPZ-CL
            //             TO (SF)-DT-ULTSC-OPZ-CL
            ws.getLccvpco1().getDati().getWpcoDtUltscOpzCl().setWpcoDtUltscOpzCl(ws.getParamComp().getPcoDtUltscOpzCl().getPcoDtUltscOpzCl());
        }
        // COB_CODE: IF PCO-STST-X-REGIONE-NULL = HIGH-VALUES
        //                TO (SF)-STST-X-REGIONE-NULL
        //           ELSE
        //                TO (SF)-STST-X-REGIONE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoStstXRegione().getPcoStstXRegioneNullFormatted())) {
            // COB_CODE: MOVE PCO-STST-X-REGIONE-NULL
            //             TO (SF)-STST-X-REGIONE-NULL
            ws.getLccvpco1().getDati().getWpcoStstXRegione().setWpcoStstXRegioneNull(ws.getParamComp().getPcoStstXRegione().getPcoStstXRegioneNull());
        }
        else {
            // COB_CODE: MOVE PCO-STST-X-REGIONE
            //             TO (SF)-STST-X-REGIONE
            ws.getLccvpco1().getDati().getWpcoStstXRegione().setWpcoStstXRegione(ws.getParamComp().getPcoStstXRegione().getPcoStstXRegione());
        }
        // COB_CODE: IF PCO-DT-ULTGZ-CED-COLL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTGZ-CED-COLL-NULL
        //           ELSE
        //                TO (SF)-DT-ULTGZ-CED-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltgzCedColl().getPcoDtUltgzCedCollNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-CED-COLL-NULL
            //             TO (SF)-DT-ULTGZ-CED-COLL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltgzCedColl().setWpcoDtUltgzCedCollNull(ws.getParamComp().getPcoDtUltgzCedColl().getPcoDtUltgzCedCollNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTGZ-CED-COLL
            //             TO (SF)-DT-ULTGZ-CED-COLL
            ws.getLccvpco1().getDati().getWpcoDtUltgzCedColl().setWpcoDtUltgzCedColl(ws.getParamComp().getPcoDtUltgzCedColl().getPcoDtUltgzCedColl());
        }
        // COB_CODE: MOVE PCO-TP-MOD-RIVAL
        //             TO (SF)-TP-MOD-RIVAL
        ws.getLccvpco1().getDati().setWpcoTpModRival(ws.getParamComp().getPcoTpModRival());
        // COB_CODE: IF PCO-NUM-MM-CALC-MORA-NULL = HIGH-VALUES
        //                TO (SF)-NUM-MM-CALC-MORA-NULL
        //           ELSE
        //                TO (SF)-NUM-MM-CALC-MORA
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoNumMmCalcMora().getPcoNumMmCalcMoraNullFormatted())) {
            // COB_CODE: MOVE PCO-NUM-MM-CALC-MORA-NULL
            //             TO (SF)-NUM-MM-CALC-MORA-NULL
            ws.getLccvpco1().getDati().getWpcoNumMmCalcMora().setWpcoNumMmCalcMoraNull(ws.getParamComp().getPcoNumMmCalcMora().getPcoNumMmCalcMoraNull());
        }
        else {
            // COB_CODE: MOVE PCO-NUM-MM-CALC-MORA
            //             TO (SF)-NUM-MM-CALC-MORA
            ws.getLccvpco1().getDati().getWpcoNumMmCalcMora().setWpcoNumMmCalcMora(ws.getParamComp().getPcoNumMmCalcMora().getPcoNumMmCalcMora());
        }
        // COB_CODE: IF PCO-DT-ULT-EC-RIV-COLL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EC-RIV-COLL-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-EC-RIV-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEcRivColl().getPcoDtUltEcRivCollNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-RIV-COLL-NULL
            //             TO (SF)-DT-ULT-EC-RIV-COLL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEcRivColl().setWpcoDtUltEcRivCollNull(ws.getParamComp().getPcoDtUltEcRivColl().getPcoDtUltEcRivCollNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-EC-RIV-COLL
            //             TO (SF)-DT-ULT-EC-RIV-COLL
            ws.getLccvpco1().getDati().getWpcoDtUltEcRivColl().setWpcoDtUltEcRivColl(ws.getParamComp().getPcoDtUltEcRivColl().getPcoDtUltEcRivColl());
        }
        // COB_CODE: IF PCO-DT-ULT-EC-RIV-IND-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EC-RIV-IND-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-EC-RIV-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEcRivInd().getPcoDtUltEcRivIndNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-RIV-IND-NULL
            //             TO (SF)-DT-ULT-EC-RIV-IND-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEcRivInd().setWpcoDtUltEcRivIndNull(ws.getParamComp().getPcoDtUltEcRivInd().getPcoDtUltEcRivIndNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-EC-RIV-IND
            //             TO (SF)-DT-ULT-EC-RIV-IND
            ws.getLccvpco1().getDati().getWpcoDtUltEcRivInd().setWpcoDtUltEcRivInd(ws.getParamComp().getPcoDtUltEcRivInd().getPcoDtUltEcRivInd());
        }
        // COB_CODE: IF PCO-DT-ULT-EC-IL-COLL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EC-IL-COLL-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-EC-IL-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEcIlColl().getPcoDtUltEcIlCollNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-IL-COLL-NULL
            //             TO (SF)-DT-ULT-EC-IL-COLL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEcIlColl().setWpcoDtUltEcIlCollNull(ws.getParamComp().getPcoDtUltEcIlColl().getPcoDtUltEcIlCollNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-EC-IL-COLL
            //             TO (SF)-DT-ULT-EC-IL-COLL
            ws.getLccvpco1().getDati().getWpcoDtUltEcIlColl().setWpcoDtUltEcIlColl(ws.getParamComp().getPcoDtUltEcIlColl().getPcoDtUltEcIlColl());
        }
        // COB_CODE: IF PCO-DT-ULT-EC-IL-IND-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EC-IL-IND-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-EC-IL-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEcIlInd().getPcoDtUltEcIlIndNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-IL-IND-NULL
            //             TO (SF)-DT-ULT-EC-IL-IND-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEcIlInd().setWpcoDtUltEcIlIndNull(ws.getParamComp().getPcoDtUltEcIlInd().getPcoDtUltEcIlIndNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-EC-IL-IND
            //             TO (SF)-DT-ULT-EC-IL-IND
            ws.getLccvpco1().getDati().getWpcoDtUltEcIlInd().setWpcoDtUltEcIlInd(ws.getParamComp().getPcoDtUltEcIlInd().getPcoDtUltEcIlInd());
        }
        // COB_CODE: IF PCO-DT-ULT-EC-UL-COLL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EC-UL-COLL-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-EC-UL-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEcUlColl().getPcoDtUltEcUlCollNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-UL-COLL-NULL
            //             TO (SF)-DT-ULT-EC-UL-COLL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEcUlColl().setWpcoDtUltEcUlCollNull(ws.getParamComp().getPcoDtUltEcUlColl().getPcoDtUltEcUlCollNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-EC-UL-COLL
            //             TO (SF)-DT-ULT-EC-UL-COLL
            ws.getLccvpco1().getDati().getWpcoDtUltEcUlColl().setWpcoDtUltEcUlColl(ws.getParamComp().getPcoDtUltEcUlColl().getPcoDtUltEcUlColl());
        }
        // COB_CODE: IF PCO-DT-ULT-EC-UL-IND-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EC-UL-IND-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-EC-UL-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEcUlInd().getPcoDtUltEcUlIndNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-UL-IND-NULL
            //             TO (SF)-DT-ULT-EC-UL-IND-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEcUlInd().setWpcoDtUltEcUlIndNull(ws.getParamComp().getPcoDtUltEcUlInd().getPcoDtUltEcUlIndNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-EC-UL-IND
            //             TO (SF)-DT-ULT-EC-UL-IND
            ws.getLccvpco1().getDati().getWpcoDtUltEcUlInd().setWpcoDtUltEcUlInd(ws.getParamComp().getPcoDtUltEcUlInd().getPcoDtUltEcUlInd());
        }
        // COB_CODE: IF PCO-AA-UTI-NULL = HIGH-VALUES
        //                TO (SF)-AA-UTI-NULL
        //           ELSE
        //                TO (SF)-AA-UTI
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoAaUti().getPcoAaUtiNullFormatted())) {
            // COB_CODE: MOVE PCO-AA-UTI-NULL
            //             TO (SF)-AA-UTI-NULL
            ws.getLccvpco1().getDati().getWpcoAaUti().setWpcoAaUtiNull(ws.getParamComp().getPcoAaUti().getPcoAaUtiNull());
        }
        else {
            // COB_CODE: MOVE PCO-AA-UTI
            //             TO (SF)-AA-UTI
            ws.getLccvpco1().getDati().getWpcoAaUti().setWpcoAaUti(ws.getParamComp().getPcoAaUti().getPcoAaUti());
        }
        // COB_CODE: MOVE PCO-CALC-RSH-COMUN
        //             TO (SF)-CALC-RSH-COMUN
        ws.getLccvpco1().getDati().setWpcoCalcRshComun(ws.getParamComp().getPcoCalcRshComun());
        // COB_CODE: MOVE PCO-FL-LIV-DEBUG
        //             TO (SF)-FL-LIV-DEBUG
        ws.getLccvpco1().getDati().setWpcoFlLivDebug(ws.getParamComp().getPcoFlLivDebug());
        // COB_CODE: IF PCO-DT-ULT-BOLL-PERF-C-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-PERF-C-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-PERF-C
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollPerfC().getPcoDtUltBollPerfCNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PERF-C-NULL
            //             TO (SF)-DT-ULT-BOLL-PERF-C-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollPerfC().setWpcoDtUltBollPerfCNull(ws.getParamComp().getPcoDtUltBollPerfC().getPcoDtUltBollPerfCNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PERF-C
            //             TO (SF)-DT-ULT-BOLL-PERF-C
            ws.getLccvpco1().getDati().getWpcoDtUltBollPerfC().setWpcoDtUltBollPerfC(ws.getParamComp().getPcoDtUltBollPerfC().getPcoDtUltBollPerfC());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RSP-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-RSP-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-RSP-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollRspIn().getPcoDtUltBollRspInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RSP-IN-NULL
            //             TO (SF)-DT-ULT-BOLL-RSP-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollRspIn().setWpcoDtUltBollRspInNull(ws.getParamComp().getPcoDtUltBollRspIn().getPcoDtUltBollRspInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RSP-IN
            //             TO (SF)-DT-ULT-BOLL-RSP-IN
            ws.getLccvpco1().getDati().getWpcoDtUltBollRspIn().setWpcoDtUltBollRspIn(ws.getParamComp().getPcoDtUltBollRspIn().getPcoDtUltBollRspIn());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RSP-CL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-RSP-CL-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-RSP-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollRspCl().getPcoDtUltBollRspClNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RSP-CL-NULL
            //             TO (SF)-DT-ULT-BOLL-RSP-CL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollRspCl().setWpcoDtUltBollRspClNull(ws.getParamComp().getPcoDtUltBollRspCl().getPcoDtUltBollRspClNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RSP-CL
            //             TO (SF)-DT-ULT-BOLL-RSP-CL
            ws.getLccvpco1().getDati().getWpcoDtUltBollRspCl().setWpcoDtUltBollRspCl(ws.getParamComp().getPcoDtUltBollRspCl().getPcoDtUltBollRspCl());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-EMES-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-EMES-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-EMES-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollEmesI().getPcoDtUltBollEmesINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-EMES-I-NULL
            //             TO (SF)-DT-ULT-BOLL-EMES-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollEmesI().setWpcoDtUltBollEmesINull(ws.getParamComp().getPcoDtUltBollEmesI().getPcoDtUltBollEmesINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-EMES-I
            //             TO (SF)-DT-ULT-BOLL-EMES-I
            ws.getLccvpco1().getDati().getWpcoDtUltBollEmesI().setWpcoDtUltBollEmesI(ws.getParamComp().getPcoDtUltBollEmesI().getPcoDtUltBollEmesI());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-STOR-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-STOR-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-STOR-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollStorI().getPcoDtUltBollStorINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-STOR-I-NULL
            //             TO (SF)-DT-ULT-BOLL-STOR-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollStorI().setWpcoDtUltBollStorINull(ws.getParamComp().getPcoDtUltBollStorI().getPcoDtUltBollStorINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-STOR-I
            //             TO (SF)-DT-ULT-BOLL-STOR-I
            ws.getLccvpco1().getDati().getWpcoDtUltBollStorI().setWpcoDtUltBollStorI(ws.getParamComp().getPcoDtUltBollStorI().getPcoDtUltBollStorI());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RIAT-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-RIAT-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-RIAT-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollRiatI().getPcoDtUltBollRiatINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RIAT-I-NULL
            //             TO (SF)-DT-ULT-BOLL-RIAT-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollRiatI().setWpcoDtUltBollRiatINull(ws.getParamComp().getPcoDtUltBollRiatI().getPcoDtUltBollRiatINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RIAT-I
            //             TO (SF)-DT-ULT-BOLL-RIAT-I
            ws.getLccvpco1().getDati().getWpcoDtUltBollRiatI().setWpcoDtUltBollRiatI(ws.getParamComp().getPcoDtUltBollRiatI().getPcoDtUltBollRiatI());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-SD-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-SD-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-SD-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollSdI().getPcoDtUltBollSdINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SD-I-NULL
            //             TO (SF)-DT-ULT-BOLL-SD-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollSdI().setWpcoDtUltBollSdINull(ws.getParamComp().getPcoDtUltBollSdI().getPcoDtUltBollSdINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SD-I
            //             TO (SF)-DT-ULT-BOLL-SD-I
            ws.getLccvpco1().getDati().getWpcoDtUltBollSdI().setWpcoDtUltBollSdI(ws.getParamComp().getPcoDtUltBollSdI().getPcoDtUltBollSdI());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-SDNL-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-SDNL-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-SDNL-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollSdnlI().getPcoDtUltBollSdnlINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SDNL-I-NULL
            //             TO (SF)-DT-ULT-BOLL-SDNL-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollSdnlI().setWpcoDtUltBollSdnlINull(ws.getParamComp().getPcoDtUltBollSdnlI().getPcoDtUltBollSdnlINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SDNL-I
            //             TO (SF)-DT-ULT-BOLL-SDNL-I
            ws.getLccvpco1().getDati().getWpcoDtUltBollSdnlI().setWpcoDtUltBollSdnlI(ws.getParamComp().getPcoDtUltBollSdnlI().getPcoDtUltBollSdnlI());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-PERF-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-PERF-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-PERF-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollPerfI().getPcoDtUltBollPerfINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PERF-I-NULL
            //             TO (SF)-DT-ULT-BOLL-PERF-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollPerfI().setWpcoDtUltBollPerfINull(ws.getParamComp().getPcoDtUltBollPerfI().getPcoDtUltBollPerfINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PERF-I
            //             TO (SF)-DT-ULT-BOLL-PERF-I
            ws.getLccvpco1().getDati().getWpcoDtUltBollPerfI().setWpcoDtUltBollPerfI(ws.getParamComp().getPcoDtUltBollPerfI().getPcoDtUltBollPerfI());
        }
        // COB_CODE: IF PCO-DT-RICL-RIRIAS-COM-NULL = HIGH-VALUES
        //                TO (SF)-DT-RICL-RIRIAS-COM-NULL
        //           ELSE
        //                TO (SF)-DT-RICL-RIRIAS-COM
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtRiclRiriasCom().getPcoDtRiclRiriasComNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-RICL-RIRIAS-COM-NULL
            //             TO (SF)-DT-RICL-RIRIAS-COM-NULL
            ws.getLccvpco1().getDati().getWpcoDtRiclRiriasCom().setWpcoDtRiclRiriasComNull(ws.getParamComp().getPcoDtRiclRiriasCom().getPcoDtRiclRiriasComNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-RICL-RIRIAS-COM
            //             TO (SF)-DT-RICL-RIRIAS-COM
            ws.getLccvpco1().getDati().getWpcoDtRiclRiriasCom().setWpcoDtRiclRiriasCom(ws.getParamComp().getPcoDtRiclRiriasCom().getPcoDtRiclRiriasCom());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-AT92-C-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-AT92-C-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-AT92-C
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabAt92C().getPcoDtUltElabAt92CNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT92-C-NULL
            //             TO (SF)-DT-ULT-ELAB-AT92-C-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabAt92C().setWpcoDtUltElabAt92CNull(ws.getParamComp().getPcoDtUltElabAt92C().getPcoDtUltElabAt92CNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT92-C
            //             TO (SF)-DT-ULT-ELAB-AT92-C
            ws.getLccvpco1().getDati().getWpcoDtUltElabAt92C().setWpcoDtUltElabAt92C(ws.getParamComp().getPcoDtUltElabAt92C().getPcoDtUltElabAt92C());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-AT92-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-AT92-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-AT92-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabAt92I().getPcoDtUltElabAt92INullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT92-I-NULL
            //             TO (SF)-DT-ULT-ELAB-AT92-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabAt92I().setWpcoDtUltElabAt92INull(ws.getParamComp().getPcoDtUltElabAt92I().getPcoDtUltElabAt92INull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT92-I
            //             TO (SF)-DT-ULT-ELAB-AT92-I
            ws.getLccvpco1().getDati().getWpcoDtUltElabAt92I().setWpcoDtUltElabAt92I(ws.getParamComp().getPcoDtUltElabAt92I().getPcoDtUltElabAt92I());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-AT93-C-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-AT93-C-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-AT93-C
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabAt93C().getPcoDtUltElabAt93CNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT93-C-NULL
            //             TO (SF)-DT-ULT-ELAB-AT93-C-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabAt93C().setWpcoDtUltElabAt93CNull(ws.getParamComp().getPcoDtUltElabAt93C().getPcoDtUltElabAt93CNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT93-C
            //             TO (SF)-DT-ULT-ELAB-AT93-C
            ws.getLccvpco1().getDati().getWpcoDtUltElabAt93C().setWpcoDtUltElabAt93C(ws.getParamComp().getPcoDtUltElabAt93C().getPcoDtUltElabAt93C());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-AT93-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-AT93-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-AT93-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabAt93I().getPcoDtUltElabAt93INullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT93-I-NULL
            //             TO (SF)-DT-ULT-ELAB-AT93-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabAt93I().setWpcoDtUltElabAt93INull(ws.getParamComp().getPcoDtUltElabAt93I().getPcoDtUltElabAt93INull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT93-I
            //             TO (SF)-DT-ULT-ELAB-AT93-I
            ws.getLccvpco1().getDati().getWpcoDtUltElabAt93I().setWpcoDtUltElabAt93I(ws.getParamComp().getPcoDtUltElabAt93I().getPcoDtUltElabAt93I());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-SPE-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-SPE-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-SPE-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabSpeIn().getPcoDtUltElabSpeInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-SPE-IN-NULL
            //             TO (SF)-DT-ULT-ELAB-SPE-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabSpeIn().setWpcoDtUltElabSpeInNull(ws.getParamComp().getPcoDtUltElabSpeIn().getPcoDtUltElabSpeInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-SPE-IN
            //             TO (SF)-DT-ULT-ELAB-SPE-IN
            ws.getLccvpco1().getDati().getWpcoDtUltElabSpeIn().setWpcoDtUltElabSpeIn(ws.getParamComp().getPcoDtUltElabSpeIn().getPcoDtUltElabSpeIn());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-PR-CON-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-PR-CON-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-PR-CON
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabPrCon().getPcoDtUltElabPrConNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PR-CON-NULL
            //             TO (SF)-DT-ULT-ELAB-PR-CON-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabPrCon().setWpcoDtUltElabPrConNull(ws.getParamComp().getPcoDtUltElabPrCon().getPcoDtUltElabPrConNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PR-CON
            //             TO (SF)-DT-ULT-ELAB-PR-CON
            ws.getLccvpco1().getDati().getWpcoDtUltElabPrCon().setWpcoDtUltElabPrCon(ws.getParamComp().getPcoDtUltElabPrCon().getPcoDtUltElabPrCon());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RP-CL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-RP-CL-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-RP-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollRpCl().getPcoDtUltBollRpClNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RP-CL-NULL
            //             TO (SF)-DT-ULT-BOLL-RP-CL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollRpCl().setWpcoDtUltBollRpClNull(ws.getParamComp().getPcoDtUltBollRpCl().getPcoDtUltBollRpClNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RP-CL
            //             TO (SF)-DT-ULT-BOLL-RP-CL
            ws.getLccvpco1().getDati().getWpcoDtUltBollRpCl().setWpcoDtUltBollRpCl(ws.getParamComp().getPcoDtUltBollRpCl().getPcoDtUltBollRpCl());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RP-IN-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-RP-IN-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-RP-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollRpIn().getPcoDtUltBollRpInNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RP-IN-NULL
            //             TO (SF)-DT-ULT-BOLL-RP-IN-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollRpIn().setWpcoDtUltBollRpInNull(ws.getParamComp().getPcoDtUltBollRpIn().getPcoDtUltBollRpInNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RP-IN
            //             TO (SF)-DT-ULT-BOLL-RP-IN
            ws.getLccvpco1().getDati().getWpcoDtUltBollRpIn().setWpcoDtUltBollRpIn(ws.getParamComp().getPcoDtUltBollRpIn().getPcoDtUltBollRpIn());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-PRE-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-PRE-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-PRE-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollPreI().getPcoDtUltBollPreINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PRE-I-NULL
            //             TO (SF)-DT-ULT-BOLL-PRE-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollPreI().setWpcoDtUltBollPreINull(ws.getParamComp().getPcoDtUltBollPreI().getPcoDtUltBollPreINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PRE-I
            //             TO (SF)-DT-ULT-BOLL-PRE-I
            ws.getLccvpco1().getDati().getWpcoDtUltBollPreI().setWpcoDtUltBollPreI(ws.getParamComp().getPcoDtUltBollPreI().getPcoDtUltBollPreI());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-PRE-C-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-PRE-C-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-PRE-C
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollPreC().getPcoDtUltBollPreCNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PRE-C-NULL
            //             TO (SF)-DT-ULT-BOLL-PRE-C-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollPreC().setWpcoDtUltBollPreCNull(ws.getParamComp().getPcoDtUltBollPreC().getPcoDtUltBollPreCNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PRE-C
            //             TO (SF)-DT-ULT-BOLL-PRE-C
            ws.getLccvpco1().getDati().getWpcoDtUltBollPreC().setWpcoDtUltBollPreC(ws.getParamComp().getPcoDtUltBollPreC().getPcoDtUltBollPreC());
        }
        // COB_CODE: IF PCO-DT-ULTC-PILDI-MM-C-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-PILDI-MM-C-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-PILDI-MM-C
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcPildiMmC().getPcoDtUltcPildiMmCNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-MM-C-NULL
            //             TO (SF)-DT-ULTC-PILDI-MM-C-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmC().setWpcoDtUltcPildiMmCNull(ws.getParamComp().getPcoDtUltcPildiMmC().getPcoDtUltcPildiMmCNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-MM-C
            //             TO (SF)-DT-ULTC-PILDI-MM-C
            ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmC().setWpcoDtUltcPildiMmC(ws.getParamComp().getPcoDtUltcPildiMmC().getPcoDtUltcPildiMmC());
        }
        // COB_CODE: IF PCO-DT-ULTC-PILDI-AA-C-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-PILDI-AA-C-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-PILDI-AA-C
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcPildiAaC().getPcoDtUltcPildiAaCNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-AA-C-NULL
            //             TO (SF)-DT-ULTC-PILDI-AA-C-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaC().setWpcoDtUltcPildiAaCNull(ws.getParamComp().getPcoDtUltcPildiAaC().getPcoDtUltcPildiAaCNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-AA-C
            //             TO (SF)-DT-ULTC-PILDI-AA-C
            ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaC().setWpcoDtUltcPildiAaC(ws.getParamComp().getPcoDtUltcPildiAaC().getPcoDtUltcPildiAaC());
        }
        // COB_CODE: IF PCO-DT-ULTC-PILDI-MM-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-PILDI-MM-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-PILDI-MM-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcPildiMmI().getPcoDtUltcPildiMmINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-MM-I-NULL
            //             TO (SF)-DT-ULTC-PILDI-MM-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmI().setWpcoDtUltcPildiMmINull(ws.getParamComp().getPcoDtUltcPildiMmI().getPcoDtUltcPildiMmINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-MM-I
            //             TO (SF)-DT-ULTC-PILDI-MM-I
            ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmI().setWpcoDtUltcPildiMmI(ws.getParamComp().getPcoDtUltcPildiMmI().getPcoDtUltcPildiMmI());
        }
        // COB_CODE: IF PCO-DT-ULTC-PILDI-TR-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-PILDI-TR-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-PILDI-TR-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcPildiTrI().getPcoDtUltcPildiTrINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-TR-I-NULL
            //             TO (SF)-DT-ULTC-PILDI-TR-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcPildiTrI().setWpcoDtUltcPildiTrINull(ws.getParamComp().getPcoDtUltcPildiTrI().getPcoDtUltcPildiTrINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-TR-I
            //             TO (SF)-DT-ULTC-PILDI-TR-I
            ws.getLccvpco1().getDati().getWpcoDtUltcPildiTrI().setWpcoDtUltcPildiTrI(ws.getParamComp().getPcoDtUltcPildiTrI().getPcoDtUltcPildiTrI());
        }
        // COB_CODE: IF PCO-DT-ULTC-PILDI-AA-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULTC-PILDI-AA-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULTC-PILDI-AA-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltcPildiAaI().getPcoDtUltcPildiAaINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-AA-I-NULL
            //             TO (SF)-DT-ULTC-PILDI-AA-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaI().setWpcoDtUltcPildiAaINull(ws.getParamComp().getPcoDtUltcPildiAaI().getPcoDtUltcPildiAaINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-AA-I
            //             TO (SF)-DT-ULTC-PILDI-AA-I
            ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaI().setWpcoDtUltcPildiAaI(ws.getParamComp().getPcoDtUltcPildiAaI().getPcoDtUltcPildiAaI());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-QUIE-C-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-QUIE-C-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-QUIE-C
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollQuieC().getPcoDtUltBollQuieCNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-QUIE-C-NULL
            //             TO (SF)-DT-ULT-BOLL-QUIE-C-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollQuieC().setWpcoDtUltBollQuieCNull(ws.getParamComp().getPcoDtUltBollQuieC().getPcoDtUltBollQuieCNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-QUIE-C
            //             TO (SF)-DT-ULT-BOLL-QUIE-C
            ws.getLccvpco1().getDati().getWpcoDtUltBollQuieC().setWpcoDtUltBollQuieC(ws.getParamComp().getPcoDtUltBollQuieC().getPcoDtUltBollQuieC());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-QUIE-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-QUIE-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-QUIE-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollQuieI().getPcoDtUltBollQuieINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-QUIE-I-NULL
            //             TO (SF)-DT-ULT-BOLL-QUIE-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollQuieI().setWpcoDtUltBollQuieINull(ws.getParamComp().getPcoDtUltBollQuieI().getPcoDtUltBollQuieINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-QUIE-I
            //             TO (SF)-DT-ULT-BOLL-QUIE-I
            ws.getLccvpco1().getDati().getWpcoDtUltBollQuieI().setWpcoDtUltBollQuieI(ws.getParamComp().getPcoDtUltBollQuieI().getPcoDtUltBollQuieI());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-COTR-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-COTR-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-COTR-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollCotrI().getPcoDtUltBollCotrINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-COTR-I-NULL
            //             TO (SF)-DT-ULT-BOLL-COTR-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollCotrI().setWpcoDtUltBollCotrINull(ws.getParamComp().getPcoDtUltBollCotrI().getPcoDtUltBollCotrINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-COTR-I
            //             TO (SF)-DT-ULT-BOLL-COTR-I
            ws.getLccvpco1().getDati().getWpcoDtUltBollCotrI().setWpcoDtUltBollCotrI(ws.getParamComp().getPcoDtUltBollCotrI().getPcoDtUltBollCotrI());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-COTR-C-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-COTR-C-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-COTR-C
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollCotrC().getPcoDtUltBollCotrCNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-COTR-C-NULL
            //             TO (SF)-DT-ULT-BOLL-COTR-C-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollCotrC().setWpcoDtUltBollCotrCNull(ws.getParamComp().getPcoDtUltBollCotrC().getPcoDtUltBollCotrCNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-COTR-C
            //             TO (SF)-DT-ULT-BOLL-COTR-C
            ws.getLccvpco1().getDati().getWpcoDtUltBollCotrC().setWpcoDtUltBollCotrC(ws.getParamComp().getPcoDtUltBollCotrC().getPcoDtUltBollCotrC());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-CORI-C-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-CORI-C-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-CORI-C
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollCoriC().getPcoDtUltBollCoriCNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-CORI-C-NULL
            //             TO (SF)-DT-ULT-BOLL-CORI-C-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollCoriC().setWpcoDtUltBollCoriCNull(ws.getParamComp().getPcoDtUltBollCoriC().getPcoDtUltBollCoriCNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-CORI-C
            //             TO (SF)-DT-ULT-BOLL-CORI-C
            ws.getLccvpco1().getDati().getWpcoDtUltBollCoriC().setWpcoDtUltBollCoriC(ws.getParamComp().getPcoDtUltBollCoriC().getPcoDtUltBollCoriC());
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-CORI-I-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-BOLL-CORI-I-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-BOLL-CORI-I
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltBollCoriI().getPcoDtUltBollCoriINullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-CORI-I-NULL
            //             TO (SF)-DT-ULT-BOLL-CORI-I-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltBollCoriI().setWpcoDtUltBollCoriINull(ws.getParamComp().getPcoDtUltBollCoriI().getPcoDtUltBollCoriINull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-CORI-I
            //             TO (SF)-DT-ULT-BOLL-CORI-I
            ws.getLccvpco1().getDati().getWpcoDtUltBollCoriI().setWpcoDtUltBollCoriI(ws.getParamComp().getPcoDtUltBollCoriI().getPcoDtUltBollCoriI());
        }
        // COB_CODE: MOVE PCO-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL
        ws.getLccvpco1().getDati().setWpcoDsOperSql(ws.getParamComp().getPcoDsOperSql());
        // COB_CODE: MOVE PCO-DS-VER
        //             TO (SF)-DS-VER
        ws.getLccvpco1().getDati().setWpcoDsVer(ws.getParamComp().getPcoDsVer());
        // COB_CODE: MOVE PCO-DS-TS-CPTZ
        //             TO (SF)-DS-TS-CPTZ
        ws.getLccvpco1().getDati().setWpcoDsTsCptz(ws.getParamComp().getPcoDsTsCptz());
        // COB_CODE: MOVE PCO-DS-UTENTE
        //             TO (SF)-DS-UTENTE
        ws.getLccvpco1().getDati().setWpcoDsUtente(ws.getParamComp().getPcoDsUtente());
        // COB_CODE: MOVE PCO-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB
        ws.getLccvpco1().getDati().setWpcoDsStatoElab(ws.getParamComp().getPcoDsStatoElab());
        // COB_CODE: IF PCO-TP-VALZZ-DT-VLT-NULL = HIGH-VALUES
        //                TO (SF)-TP-VALZZ-DT-VLT-NULL
        //           ELSE
        //                TO (SF)-TP-VALZZ-DT-VLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoTpValzzDtVltFormatted())) {
            // COB_CODE: MOVE PCO-TP-VALZZ-DT-VLT-NULL
            //             TO (SF)-TP-VALZZ-DT-VLT-NULL
            ws.getLccvpco1().getDati().setWpcoTpValzzDtVlt(ws.getParamComp().getPcoTpValzzDtVlt());
        }
        else {
            // COB_CODE: MOVE PCO-TP-VALZZ-DT-VLT
            //             TO (SF)-TP-VALZZ-DT-VLT
            ws.getLccvpco1().getDati().setWpcoTpValzzDtVlt(ws.getParamComp().getPcoTpValzzDtVlt());
        }
        // COB_CODE: IF PCO-FL-FRAZ-PROV-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-FL-FRAZ-PROV-ACQ-NULL
        //           ELSE
        //                TO (SF)-FL-FRAZ-PROV-ACQ
        //           END-IF
        if (Conditions.eq(ws.getParamComp().getPcoFlFrazProvAcq(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PCO-FL-FRAZ-PROV-ACQ-NULL
            //             TO (SF)-FL-FRAZ-PROV-ACQ-NULL
            ws.getLccvpco1().getDati().setWpcoFlFrazProvAcq(ws.getParamComp().getPcoFlFrazProvAcq());
        }
        else {
            // COB_CODE: MOVE PCO-FL-FRAZ-PROV-ACQ
            //             TO (SF)-FL-FRAZ-PROV-ACQ
            ws.getLccvpco1().getDati().setWpcoFlFrazProvAcq(ws.getParamComp().getPcoFlFrazProvAcq());
        }
        // COB_CODE: IF PCO-DT-ULT-AGG-EROG-RE-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-AGG-EROG-RE-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-AGG-EROG-RE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltAggErogRe().getPcoDtUltAggErogReNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-AGG-EROG-RE-NULL
            //             TO (SF)-DT-ULT-AGG-EROG-RE-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltAggErogRe().setWpcoDtUltAggErogReNull(ws.getParamComp().getPcoDtUltAggErogRe().getPcoDtUltAggErogReNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-AGG-EROG-RE
            //             TO (SF)-DT-ULT-AGG-EROG-RE
            ws.getLccvpco1().getDati().getWpcoDtUltAggErogRe().setWpcoDtUltAggErogRe(ws.getParamComp().getPcoDtUltAggErogRe().getPcoDtUltAggErogRe());
        }
        // COB_CODE: IF PCO-PC-RM-MARSOL-NULL = HIGH-VALUES
        //                TO (SF)-PC-RM-MARSOL-NULL
        //           ELSE
        //                TO (SF)-PC-RM-MARSOL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoPcRmMarsol().getPcoPcRmMarsolNullFormatted())) {
            // COB_CODE: MOVE PCO-PC-RM-MARSOL-NULL
            //             TO (SF)-PC-RM-MARSOL-NULL
            ws.getLccvpco1().getDati().getWpcoPcRmMarsol().setWpcoPcRmMarsolNull(ws.getParamComp().getPcoPcRmMarsol().getPcoPcRmMarsolNull());
        }
        else {
            // COB_CODE: MOVE PCO-PC-RM-MARSOL
            //             TO (SF)-PC-RM-MARSOL
            ws.getLccvpco1().getDati().getWpcoPcRmMarsol().setWpcoPcRmMarsol(Trunc.toDecimal(ws.getParamComp().getPcoPcRmMarsol().getPcoPcRmMarsol(), 6, 3));
        }
        // COB_CODE: IF PCO-PC-C-SUBRSH-MARSOL-NULL = HIGH-VALUES
        //                TO (SF)-PC-C-SUBRSH-MARSOL-NULL
        //           ELSE
        //                TO (SF)-PC-C-SUBRSH-MARSOL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoPcCSubrshMarsol().getPcoPcCSubrshMarsolNullFormatted())) {
            // COB_CODE: MOVE PCO-PC-C-SUBRSH-MARSOL-NULL
            //             TO (SF)-PC-C-SUBRSH-MARSOL-NULL
            ws.getLccvpco1().getDati().getWpcoPcCSubrshMarsol().setWpcoPcCSubrshMarsolNull(ws.getParamComp().getPcoPcCSubrshMarsol().getPcoPcCSubrshMarsolNull());
        }
        else {
            // COB_CODE: MOVE PCO-PC-C-SUBRSH-MARSOL
            //             TO (SF)-PC-C-SUBRSH-MARSOL
            ws.getLccvpco1().getDati().getWpcoPcCSubrshMarsol().setWpcoPcCSubrshMarsol(Trunc.toDecimal(ws.getParamComp().getPcoPcCSubrshMarsol().getPcoPcCSubrshMarsol(), 6, 3));
        }
        // COB_CODE: IF PCO-COD-COMP-ISVAP-NULL = HIGH-VALUES
        //                TO (SF)-COD-COMP-ISVAP-NULL
        //           ELSE
        //                TO (SF)-COD-COMP-ISVAP
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoCodCompIsvapFormatted())) {
            // COB_CODE: MOVE PCO-COD-COMP-ISVAP-NULL
            //             TO (SF)-COD-COMP-ISVAP-NULL
            ws.getLccvpco1().getDati().setWpcoCodCompIsvap(ws.getParamComp().getPcoCodCompIsvap());
        }
        else {
            // COB_CODE: MOVE PCO-COD-COMP-ISVAP
            //             TO (SF)-COD-COMP-ISVAP
            ws.getLccvpco1().getDati().setWpcoCodCompIsvap(ws.getParamComp().getPcoCodCompIsvap());
        }
        // COB_CODE: IF PCO-LM-RIS-CON-INT-NULL = HIGH-VALUES
        //                TO (SF)-LM-RIS-CON-INT-NULL
        //           ELSE
        //                TO (SF)-LM-RIS-CON-INT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoLmRisConInt().getPcoLmRisConIntNullFormatted())) {
            // COB_CODE: MOVE PCO-LM-RIS-CON-INT-NULL
            //             TO (SF)-LM-RIS-CON-INT-NULL
            ws.getLccvpco1().getDati().getWpcoLmRisConInt().setWpcoLmRisConIntNull(ws.getParamComp().getPcoLmRisConInt().getPcoLmRisConIntNull());
        }
        else {
            // COB_CODE: MOVE PCO-LM-RIS-CON-INT
            //             TO (SF)-LM-RIS-CON-INT
            ws.getLccvpco1().getDati().getWpcoLmRisConInt().setWpcoLmRisConInt(Trunc.toDecimal(ws.getParamComp().getPcoLmRisConInt().getPcoLmRisConInt(), 6, 3));
        }
        // COB_CODE: IF PCO-LM-C-SUBRSH-CON-IN-NULL = HIGH-VALUES
        //                TO (SF)-LM-C-SUBRSH-CON-IN-NULL
        //           ELSE
        //                TO (SF)-LM-C-SUBRSH-CON-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoLmCSubrshConIn().getPcoLmCSubrshConInNullFormatted())) {
            // COB_CODE: MOVE PCO-LM-C-SUBRSH-CON-IN-NULL
            //             TO (SF)-LM-C-SUBRSH-CON-IN-NULL
            ws.getLccvpco1().getDati().getWpcoLmCSubrshConIn().setWpcoLmCSubrshConInNull(ws.getParamComp().getPcoLmCSubrshConIn().getPcoLmCSubrshConInNull());
        }
        else {
            // COB_CODE: MOVE PCO-LM-C-SUBRSH-CON-IN
            //             TO (SF)-LM-C-SUBRSH-CON-IN
            ws.getLccvpco1().getDati().getWpcoLmCSubrshConIn().setWpcoLmCSubrshConIn(Trunc.toDecimal(ws.getParamComp().getPcoLmCSubrshConIn().getPcoLmCSubrshConIn(), 6, 3));
        }
        // COB_CODE: IF PCO-PC-GAR-NORISK-MARS-NULL = HIGH-VALUES
        //                TO (SF)-PC-GAR-NORISK-MARS-NULL
        //           ELSE
        //                TO (SF)-PC-GAR-NORISK-MARS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoPcGarNoriskMars().getPcoPcGarNoriskMarsNullFormatted())) {
            // COB_CODE: MOVE PCO-PC-GAR-NORISK-MARS-NULL
            //             TO (SF)-PC-GAR-NORISK-MARS-NULL
            ws.getLccvpco1().getDati().getWpcoPcGarNoriskMars().setWpcoPcGarNoriskMarsNull(ws.getParamComp().getPcoPcGarNoriskMars().getPcoPcGarNoriskMarsNull());
        }
        else {
            // COB_CODE: MOVE PCO-PC-GAR-NORISK-MARS
            //             TO (SF)-PC-GAR-NORISK-MARS
            ws.getLccvpco1().getDati().getWpcoPcGarNoriskMars().setWpcoPcGarNoriskMars(Trunc.toDecimal(ws.getParamComp().getPcoPcGarNoriskMars().getPcoPcGarNoriskMars(), 6, 3));
        }
        // COB_CODE: IF PCO-CRZ-1A-RAT-INTR-PR-NULL = HIGH-VALUES
        //                TO (SF)-CRZ-1A-RAT-INTR-PR-NULL
        //           ELSE
        //                TO (SF)-CRZ-1A-RAT-INTR-PR
        //           END-IF
        if (Conditions.eq(ws.getParamComp().getPcoCrz1aRatIntrPr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PCO-CRZ-1A-RAT-INTR-PR-NULL
            //             TO (SF)-CRZ-1A-RAT-INTR-PR-NULL
            ws.getLccvpco1().getDati().setWpcoCrz1aRatIntrPr(ws.getParamComp().getPcoCrz1aRatIntrPr());
        }
        else {
            // COB_CODE: MOVE PCO-CRZ-1A-RAT-INTR-PR
            //             TO (SF)-CRZ-1A-RAT-INTR-PR
            ws.getLccvpco1().getDati().setWpcoCrz1aRatIntrPr(ws.getParamComp().getPcoCrz1aRatIntrPr());
        }
        // COB_CODE: IF PCO-NUM-GG-ARR-INTR-PR-NULL = HIGH-VALUES
        //                TO (SF)-NUM-GG-ARR-INTR-PR-NULL
        //           ELSE
        //                TO (SF)-NUM-GG-ARR-INTR-PR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoNumGgArrIntrPr().getPcoNumGgArrIntrPrNullFormatted())) {
            // COB_CODE: MOVE PCO-NUM-GG-ARR-INTR-PR-NULL
            //             TO (SF)-NUM-GG-ARR-INTR-PR-NULL
            ws.getLccvpco1().getDati().getWpcoNumGgArrIntrPr().setWpcoNumGgArrIntrPrNull(ws.getParamComp().getPcoNumGgArrIntrPr().getPcoNumGgArrIntrPrNull());
        }
        else {
            // COB_CODE: MOVE PCO-NUM-GG-ARR-INTR-PR
            //             TO (SF)-NUM-GG-ARR-INTR-PR
            ws.getLccvpco1().getDati().getWpcoNumGgArrIntrPr().setWpcoNumGgArrIntrPr(ws.getParamComp().getPcoNumGgArrIntrPr().getPcoNumGgArrIntrPr());
        }
        // COB_CODE: IF PCO-FL-VISUAL-VINPG-NULL = HIGH-VALUES
        //                TO (SF)-FL-VISUAL-VINPG-NULL
        //           ELSE
        //                TO (SF)-FL-VISUAL-VINPG
        //           END-IF
        if (Conditions.eq(ws.getParamComp().getPcoFlVisualVinpg(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PCO-FL-VISUAL-VINPG-NULL
            //             TO (SF)-FL-VISUAL-VINPG-NULL
            ws.getLccvpco1().getDati().setWpcoFlVisualVinpg(ws.getParamComp().getPcoFlVisualVinpg());
        }
        else {
            // COB_CODE: MOVE PCO-FL-VISUAL-VINPG
            //             TO (SF)-FL-VISUAL-VINPG
            ws.getLccvpco1().getDati().setWpcoFlVisualVinpg(ws.getParamComp().getPcoFlVisualVinpg());
        }
        // COB_CODE: IF PCO-DT-ULT-ESTRAZ-FUG-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ESTRAZ-FUG-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ESTRAZ-FUG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEstrazFug().getPcoDtUltEstrazFugNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ESTRAZ-FUG-NULL
            //             TO (SF)-DT-ULT-ESTRAZ-FUG-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEstrazFug().setWpcoDtUltEstrazFugNull(ws.getParamComp().getPcoDtUltEstrazFug().getPcoDtUltEstrazFugNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ESTRAZ-FUG
            //             TO (SF)-DT-ULT-ESTRAZ-FUG
            ws.getLccvpco1().getDati().getWpcoDtUltEstrazFug().setWpcoDtUltEstrazFug(ws.getParamComp().getPcoDtUltEstrazFug().getPcoDtUltEstrazFug());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-PR-AUT-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-PR-AUT-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-PR-AUT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabPrAut().getPcoDtUltElabPrAutNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PR-AUT-NULL
            //             TO (SF)-DT-ULT-ELAB-PR-AUT-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabPrAut().setWpcoDtUltElabPrAutNull(ws.getParamComp().getPcoDtUltElabPrAut().getPcoDtUltElabPrAutNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PR-AUT
            //             TO (SF)-DT-ULT-ELAB-PR-AUT
            ws.getLccvpco1().getDati().getWpcoDtUltElabPrAut().setWpcoDtUltElabPrAut(ws.getParamComp().getPcoDtUltElabPrAut().getPcoDtUltElabPrAut());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-COMMEF-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-COMMEF-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-COMMEF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabCommef().getPcoDtUltElabCommefNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COMMEF-NULL
            //             TO (SF)-DT-ULT-ELAB-COMMEF-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabCommef().setWpcoDtUltElabCommefNull(ws.getParamComp().getPcoDtUltElabCommef().getPcoDtUltElabCommefNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COMMEF
            //             TO (SF)-DT-ULT-ELAB-COMMEF
            ws.getLccvpco1().getDati().getWpcoDtUltElabCommef().setWpcoDtUltElabCommef(ws.getParamComp().getPcoDtUltElabCommef().getPcoDtUltElabCommef());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-LIQMEF-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-LIQMEF-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-LIQMEF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabLiqmef().getPcoDtUltElabLiqmefNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-LIQMEF-NULL
            //             TO (SF)-DT-ULT-ELAB-LIQMEF-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabLiqmef().setWpcoDtUltElabLiqmefNull(ws.getParamComp().getPcoDtUltElabLiqmef().getPcoDtUltElabLiqmefNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-LIQMEF
            //             TO (SF)-DT-ULT-ELAB-LIQMEF
            ws.getLccvpco1().getDati().getWpcoDtUltElabLiqmef().setWpcoDtUltElabLiqmef(ws.getParamComp().getPcoDtUltElabLiqmef().getPcoDtUltElabLiqmef());
        }
        // COB_CODE: IF PCO-COD-FISC-MEF-NULL = HIGH-VALUES
        //                TO (SF)-COD-FISC-MEF-NULL
        //           ELSE
        //                TO (SF)-COD-FISC-MEF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoCodFiscMefFormatted())) {
            // COB_CODE: MOVE PCO-COD-FISC-MEF-NULL
            //             TO (SF)-COD-FISC-MEF-NULL
            ws.getLccvpco1().getDati().setWpcoCodFiscMef(ws.getParamComp().getPcoCodFiscMef());
        }
        else {
            // COB_CODE: MOVE PCO-COD-FISC-MEF
            //             TO (SF)-COD-FISC-MEF
            ws.getLccvpco1().getDati().setWpcoCodFiscMef(ws.getParamComp().getPcoCodFiscMef());
        }
        // COB_CODE: IF PCO-IMP-ASS-SOCIALE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ASS-SOCIALE-NULL
        //           ELSE
        //                TO (SF)-IMP-ASS-SOCIALE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoImpAssSociale().getPcoImpAssSocialeNullFormatted())) {
            // COB_CODE: MOVE PCO-IMP-ASS-SOCIALE-NULL
            //             TO (SF)-IMP-ASS-SOCIALE-NULL
            ws.getLccvpco1().getDati().getWpcoImpAssSociale().setWpcoImpAssSocialeNull(ws.getParamComp().getPcoImpAssSociale().getPcoImpAssSocialeNull());
        }
        else {
            // COB_CODE: MOVE PCO-IMP-ASS-SOCIALE
            //             TO (SF)-IMP-ASS-SOCIALE
            ws.getLccvpco1().getDati().getWpcoImpAssSociale().setWpcoImpAssSociale(Trunc.toDecimal(ws.getParamComp().getPcoImpAssSociale().getPcoImpAssSociale(), 15, 3));
        }
        // COB_CODE: IF PCO-MOD-COMNZ-INVST-SW-NULL = HIGH-VALUES
        //                TO (SF)-MOD-COMNZ-INVST-SW-NULL
        //           ELSE
        //                TO (SF)-MOD-COMNZ-INVST-SW
        //           END-IF
        if (Conditions.eq(ws.getParamComp().getPcoModComnzInvstSw(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE PCO-MOD-COMNZ-INVST-SW-NULL
            //             TO (SF)-MOD-COMNZ-INVST-SW-NULL
            ws.getLccvpco1().getDati().setWpcoModComnzInvstSw(ws.getParamComp().getPcoModComnzInvstSw());
        }
        else {
            // COB_CODE: MOVE PCO-MOD-COMNZ-INVST-SW
            //             TO (SF)-MOD-COMNZ-INVST-SW
            ws.getLccvpco1().getDati().setWpcoModComnzInvstSw(ws.getParamComp().getPcoModComnzInvstSw());
        }
        // COB_CODE: IF PCO-DT-RIAT-RIASS-RSH-NULL = HIGH-VALUES
        //                TO (SF)-DT-RIAT-RIASS-RSH-NULL
        //           ELSE
        //                TO (SF)-DT-RIAT-RIASS-RSH
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtRiatRiassRsh().getPcoDtRiatRiassRshNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-RIAT-RIASS-RSH-NULL
            //             TO (SF)-DT-RIAT-RIASS-RSH-NULL
            ws.getLccvpco1().getDati().getWpcoDtRiatRiassRsh().setWpcoDtRiatRiassRshNull(ws.getParamComp().getPcoDtRiatRiassRsh().getPcoDtRiatRiassRshNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-RIAT-RIASS-RSH
            //             TO (SF)-DT-RIAT-RIASS-RSH
            ws.getLccvpco1().getDati().getWpcoDtRiatRiassRsh().setWpcoDtRiatRiassRsh(ws.getParamComp().getPcoDtRiatRiassRsh().getPcoDtRiatRiassRsh());
        }
        // COB_CODE: IF PCO-DT-RIAT-RIASS-COMM-NULL = HIGH-VALUES
        //                TO (SF)-DT-RIAT-RIASS-COMM-NULL
        //           ELSE
        //                TO (SF)-DT-RIAT-RIASS-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtRiatRiassComm().getPcoDtRiatRiassCommNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-RIAT-RIASS-COMM-NULL
            //             TO (SF)-DT-RIAT-RIASS-COMM-NULL
            ws.getLccvpco1().getDati().getWpcoDtRiatRiassComm().setWpcoDtRiatRiassCommNull(ws.getParamComp().getPcoDtRiatRiassComm().getPcoDtRiatRiassCommNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-RIAT-RIASS-COMM
            //             TO (SF)-DT-RIAT-RIASS-COMM
            ws.getLccvpco1().getDati().getWpcoDtRiatRiassComm().setWpcoDtRiatRiassComm(ws.getParamComp().getPcoDtRiatRiassComm().getPcoDtRiatRiassComm());
        }
        // COB_CODE: IF PCO-GG-INTR-RIT-PAG-NULL = HIGH-VALUES
        //                TO (SF)-GG-INTR-RIT-PAG-NULL
        //           ELSE
        //                TO (SF)-GG-INTR-RIT-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoGgIntrRitPag().getPcoGgIntrRitPagNullFormatted())) {
            // COB_CODE: MOVE PCO-GG-INTR-RIT-PAG-NULL
            //             TO (SF)-GG-INTR-RIT-PAG-NULL
            ws.getLccvpco1().getDati().getWpcoGgIntrRitPag().setWpcoGgIntrRitPagNull(ws.getParamComp().getPcoGgIntrRitPag().getPcoGgIntrRitPagNull());
        }
        else {
            // COB_CODE: MOVE PCO-GG-INTR-RIT-PAG
            //             TO (SF)-GG-INTR-RIT-PAG
            ws.getLccvpco1().getDati().getWpcoGgIntrRitPag().setWpcoGgIntrRitPag(ws.getParamComp().getPcoGgIntrRitPag().getPcoGgIntrRitPag());
        }
        // COB_CODE: IF PCO-DT-ULT-RINN-TAC-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-RINN-TAC-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-RINN-TAC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltRinnTac().getPcoDtUltRinnTacNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-TAC-NULL
            //             TO (SF)-DT-ULT-RINN-TAC-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltRinnTac().setWpcoDtUltRinnTacNull(ws.getParamComp().getPcoDtUltRinnTac().getPcoDtUltRinnTacNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-TAC
            //             TO (SF)-DT-ULT-RINN-TAC
            ws.getLccvpco1().getDati().getWpcoDtUltRinnTac().setWpcoDtUltRinnTac(ws.getParamComp().getPcoDtUltRinnTac().getPcoDtUltRinnTac());
        }
        // COB_CODE: MOVE PCO-DESC-COMP-VCHAR
        //             TO (SF)-DESC-COMP-VCHAR
        ws.getLccvpco1().getDati().setWpcoDescCompVcharBytes(ws.getParamComp().getPcoDescCompVcharBytes());
        // COB_CODE: IF PCO-DT-ULT-EC-TCM-IND-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EC-TCM-IND-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-EC-TCM-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEcTcmInd().getPcoDtUltEcTcmIndNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-TCM-IND-NULL
            //             TO (SF)-DT-ULT-EC-TCM-IND-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEcTcmInd().setWpcoDtUltEcTcmIndNull(ws.getParamComp().getPcoDtUltEcTcmInd().getPcoDtUltEcTcmIndNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-EC-TCM-IND
            //             TO (SF)-DT-ULT-EC-TCM-IND
            ws.getLccvpco1().getDati().getWpcoDtUltEcTcmInd().setWpcoDtUltEcTcmInd(ws.getParamComp().getPcoDtUltEcTcmInd().getPcoDtUltEcTcmInd());
        }
        // COB_CODE: IF PCO-DT-ULT-EC-TCM-COLL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EC-TCM-COLL-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-EC-TCM-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEcTcmColl().getPcoDtUltEcTcmCollNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-TCM-COLL-NULL
            //             TO (SF)-DT-ULT-EC-TCM-COLL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEcTcmColl().setWpcoDtUltEcTcmCollNull(ws.getParamComp().getPcoDtUltEcTcmColl().getPcoDtUltEcTcmCollNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-EC-TCM-COLL
            //             TO (SF)-DT-ULT-EC-TCM-COLL
            ws.getLccvpco1().getDati().getWpcoDtUltEcTcmColl().setWpcoDtUltEcTcmColl(ws.getParamComp().getPcoDtUltEcTcmColl().getPcoDtUltEcTcmColl());
        }
        // COB_CODE: IF PCO-DT-ULT-EC-MRM-IND-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EC-MRM-IND-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-EC-MRM-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEcMrmInd().getPcoDtUltEcMrmIndNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-MRM-IND-NULL
            //             TO (SF)-DT-ULT-EC-MRM-IND-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEcMrmInd().setWpcoDtUltEcMrmIndNull(ws.getParamComp().getPcoDtUltEcMrmInd().getPcoDtUltEcMrmIndNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-EC-MRM-IND
            //             TO (SF)-DT-ULT-EC-MRM-IND
            ws.getLccvpco1().getDati().getWpcoDtUltEcMrmInd().setWpcoDtUltEcMrmInd(ws.getParamComp().getPcoDtUltEcMrmInd().getPcoDtUltEcMrmInd());
        }
        // COB_CODE: IF PCO-DT-ULT-EC-MRM-COLL-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-EC-MRM-COLL-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-EC-MRM-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEcMrmColl().getPcoDtUltEcMrmCollNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-MRM-COLL-NULL
            //             TO (SF)-DT-ULT-EC-MRM-COLL-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEcMrmColl().setWpcoDtUltEcMrmCollNull(ws.getParamComp().getPcoDtUltEcMrmColl().getPcoDtUltEcMrmCollNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-EC-MRM-COLL
            //             TO (SF)-DT-ULT-EC-MRM-COLL
            ws.getLccvpco1().getDati().getWpcoDtUltEcMrmColl().setWpcoDtUltEcMrmColl(ws.getParamComp().getPcoDtUltEcMrmColl().getPcoDtUltEcMrmColl());
        }
        // COB_CODE: IF PCO-COD-COMP-LDAP-NULL = HIGH-VALUES
        //                TO (SF)-COD-COMP-LDAP-NULL
        //           ELSE
        //                TO (SF)-COD-COMP-LDAP
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoCodCompLdapFormatted())) {
            // COB_CODE: MOVE PCO-COD-COMP-LDAP-NULL
            //             TO (SF)-COD-COMP-LDAP-NULL
            ws.getLccvpco1().getDati().setWpcoCodCompLdap(ws.getParamComp().getPcoCodCompLdap());
        }
        else {
            // COB_CODE: MOVE PCO-COD-COMP-LDAP
            //             TO (SF)-COD-COMP-LDAP
            ws.getLccvpco1().getDati().setWpcoCodCompLdap(ws.getParamComp().getPcoCodCompLdap());
        }
        // COB_CODE: IF PCO-PC-RID-IMP-1382011-NULL = HIGH-VALUES
        //                TO (SF)-PC-RID-IMP-1382011-NULL
        //           ELSE
        //                TO (SF)-PC-RID-IMP-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoPcRidImp1382011().getPcoPcRidImp1382011NullFormatted())) {
            // COB_CODE: MOVE PCO-PC-RID-IMP-1382011-NULL
            //             TO (SF)-PC-RID-IMP-1382011-NULL
            ws.getLccvpco1().getDati().getWpcoPcRidImp1382011().setWpcoPcRidImp1382011Null(ws.getParamComp().getPcoPcRidImp1382011().getPcoPcRidImp1382011Null());
        }
        else {
            // COB_CODE: MOVE PCO-PC-RID-IMP-1382011
            //             TO (SF)-PC-RID-IMP-1382011
            ws.getLccvpco1().getDati().getWpcoPcRidImp1382011().setWpcoPcRidImp1382011(Trunc.toDecimal(ws.getParamComp().getPcoPcRidImp1382011().getPcoPcRidImp1382011(), 6, 3));
        }
        // COB_CODE: IF PCO-PC-RID-IMP-662014-NULL = HIGH-VALUES
        //                TO (SF)-PC-RID-IMP-662014-NULL
        //           ELSE
        //                TO (SF)-PC-RID-IMP-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoPcRidImp662014().getPcoPcRidImp662014NullFormatted())) {
            // COB_CODE: MOVE PCO-PC-RID-IMP-662014-NULL
            //             TO (SF)-PC-RID-IMP-662014-NULL
            ws.getLccvpco1().getDati().getWpcoPcRidImp662014().setWpcoPcRidImp662014Null(ws.getParamComp().getPcoPcRidImp662014().getPcoPcRidImp662014Null());
        }
        else {
            // COB_CODE: MOVE PCO-PC-RID-IMP-662014
            //             TO (SF)-PC-RID-IMP-662014
            ws.getLccvpco1().getDati().getWpcoPcRidImp662014().setWpcoPcRidImp662014(Trunc.toDecimal(ws.getParamComp().getPcoPcRidImp662014().getPcoPcRidImp662014(), 6, 3));
        }
        // COB_CODE: IF PCO-SOGL-AML-PRE-UNI-NULL = HIGH-VALUES
        //                TO (SF)-SOGL-AML-PRE-UNI-NULL
        //           ELSE
        //                TO (SF)-SOGL-AML-PRE-UNI
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoSoglAmlPreUni().getPcoSoglAmlPreUniNullFormatted())) {
            // COB_CODE: MOVE PCO-SOGL-AML-PRE-UNI-NULL
            //             TO (SF)-SOGL-AML-PRE-UNI-NULL
            ws.getLccvpco1().getDati().getWpcoSoglAmlPreUni().setWpcoSoglAmlPreUniNull(ws.getParamComp().getPcoSoglAmlPreUni().getPcoSoglAmlPreUniNull());
        }
        else {
            // COB_CODE: MOVE PCO-SOGL-AML-PRE-UNI
            //             TO (SF)-SOGL-AML-PRE-UNI
            ws.getLccvpco1().getDati().getWpcoSoglAmlPreUni().setWpcoSoglAmlPreUni(Trunc.toDecimal(ws.getParamComp().getPcoSoglAmlPreUni().getPcoSoglAmlPreUni(), 15, 3));
        }
        // COB_CODE: IF PCO-SOGL-AML-PRE-PER-NULL = HIGH-VALUES
        //                TO (SF)-SOGL-AML-PRE-PER-NULL
        //           ELSE
        //                TO (SF)-SOGL-AML-PRE-PER
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoSoglAmlPrePer().getPcoSoglAmlPrePerNullFormatted())) {
            // COB_CODE: MOVE PCO-SOGL-AML-PRE-PER-NULL
            //             TO (SF)-SOGL-AML-PRE-PER-NULL
            ws.getLccvpco1().getDati().getWpcoSoglAmlPrePer().setWpcoSoglAmlPrePerNull(ws.getParamComp().getPcoSoglAmlPrePer().getPcoSoglAmlPrePerNull());
        }
        else {
            // COB_CODE: MOVE PCO-SOGL-AML-PRE-PER
            //             TO (SF)-SOGL-AML-PRE-PER
            ws.getLccvpco1().getDati().getWpcoSoglAmlPrePer().setWpcoSoglAmlPrePer(Trunc.toDecimal(ws.getParamComp().getPcoSoglAmlPrePer().getPcoSoglAmlPrePer(), 15, 3));
        }
        // COB_CODE: IF PCO-COD-SOGG-FTZ-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-COD-SOGG-FTZ-ASSTO-NULL
        //           ELSE
        //                TO (SF)-COD-SOGG-FTZ-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoCodSoggFtzAsstoFormatted())) {
            // COB_CODE: MOVE PCO-COD-SOGG-FTZ-ASSTO-NULL
            //             TO (SF)-COD-SOGG-FTZ-ASSTO-NULL
            ws.getLccvpco1().getDati().setWpcoCodSoggFtzAssto(ws.getParamComp().getPcoCodSoggFtzAssto());
        }
        else {
            // COB_CODE: MOVE PCO-COD-SOGG-FTZ-ASSTO
            //             TO (SF)-COD-SOGG-FTZ-ASSTO
            ws.getLccvpco1().getDati().setWpcoCodSoggFtzAssto(ws.getParamComp().getPcoCodSoggFtzAssto());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-REDPRO-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-REDPRO-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-REDPRO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabRedpro().getPcoDtUltElabRedproNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-REDPRO-NULL
            //             TO (SF)-DT-ULT-ELAB-REDPRO-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabRedpro().setWpcoDtUltElabRedproNull(ws.getParamComp().getPcoDtUltElabRedpro().getPcoDtUltElabRedproNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-REDPRO
            //             TO (SF)-DT-ULT-ELAB-REDPRO
            ws.getLccvpco1().getDati().getWpcoDtUltElabRedpro().setWpcoDtUltElabRedpro(ws.getParamComp().getPcoDtUltElabRedpro().getPcoDtUltElabRedpro());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-TAKE-P-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-TAKE-P-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-TAKE-P
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabTakeP().getPcoDtUltElabTakePNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-TAKE-P-NULL
            //             TO (SF)-DT-ULT-ELAB-TAKE-P-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabTakeP().setWpcoDtUltElabTakePNull(ws.getParamComp().getPcoDtUltElabTakeP().getPcoDtUltElabTakePNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-TAKE-P
            //             TO (SF)-DT-ULT-ELAB-TAKE-P
            ws.getLccvpco1().getDati().getWpcoDtUltElabTakeP().setWpcoDtUltElabTakeP(ws.getParamComp().getPcoDtUltElabTakeP().getPcoDtUltElabTakeP());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-PASPAS-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-PASPAS-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-PASPAS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabPaspas().getPcoDtUltElabPaspasNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PASPAS-NULL
            //             TO (SF)-DT-ULT-ELAB-PASPAS-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabPaspas().setWpcoDtUltElabPaspasNull(ws.getParamComp().getPcoDtUltElabPaspas().getPcoDtUltElabPaspasNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PASPAS
            //             TO (SF)-DT-ULT-ELAB-PASPAS
            ws.getLccvpco1().getDati().getWpcoDtUltElabPaspas().setWpcoDtUltElabPaspas(ws.getParamComp().getPcoDtUltElabPaspas().getPcoDtUltElabPaspas());
        }
        // COB_CODE: IF PCO-SOGL-AML-PRE-SAV-R-NULL = HIGH-VALUES
        //                TO (SF)-SOGL-AML-PRE-SAV-R-NULL
        //           ELSE
        //                TO (SF)-SOGL-AML-PRE-SAV-R
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoSoglAmlPreSavR().getPcoSoglAmlPreSavRNullFormatted())) {
            // COB_CODE: MOVE PCO-SOGL-AML-PRE-SAV-R-NULL
            //             TO (SF)-SOGL-AML-PRE-SAV-R-NULL
            ws.getLccvpco1().getDati().getWpcoSoglAmlPreSavR().setWpcoSoglAmlPreSavRNull(ws.getParamComp().getPcoSoglAmlPreSavR().getPcoSoglAmlPreSavRNull());
        }
        else {
            // COB_CODE: MOVE PCO-SOGL-AML-PRE-SAV-R
            //             TO (SF)-SOGL-AML-PRE-SAV-R
            ws.getLccvpco1().getDati().getWpcoSoglAmlPreSavR().setWpcoSoglAmlPreSavR(Trunc.toDecimal(ws.getParamComp().getPcoSoglAmlPreSavR().getPcoSoglAmlPreSavR(), 15, 3));
        }
        // COB_CODE: IF PCO-DT-ULT-ESTR-DEC-CO-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ESTR-DEC-CO-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ESTR-DEC-CO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltEstrDecCo().getPcoDtUltEstrDecCoNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ESTR-DEC-CO-NULL
            //             TO (SF)-DT-ULT-ESTR-DEC-CO-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltEstrDecCo().setWpcoDtUltEstrDecCoNull(ws.getParamComp().getPcoDtUltEstrDecCo().getPcoDtUltEstrDecCoNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ESTR-DEC-CO
            //             TO (SF)-DT-ULT-ESTR-DEC-CO
            ws.getLccvpco1().getDati().getWpcoDtUltEstrDecCo().setWpcoDtUltEstrDecCo(ws.getParamComp().getPcoDtUltEstrDecCo().getPcoDtUltEstrDecCo());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-COS-AT-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-COS-AT-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-COS-AT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabCosAt().getPcoDtUltElabCosAtNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COS-AT-NULL
            //             TO (SF)-DT-ULT-ELAB-COS-AT-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabCosAt().setWpcoDtUltElabCosAtNull(ws.getParamComp().getPcoDtUltElabCosAt().getPcoDtUltElabCosAtNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COS-AT
            //             TO (SF)-DT-ULT-ELAB-COS-AT
            ws.getLccvpco1().getDati().getWpcoDtUltElabCosAt().setWpcoDtUltElabCosAt(ws.getParamComp().getPcoDtUltElabCosAt().getPcoDtUltElabCosAt());
        }
        // COB_CODE: IF PCO-FRQ-COSTI-ATT-NULL = HIGH-VALUES
        //                TO (SF)-FRQ-COSTI-ATT-NULL
        //           ELSE
        //                TO (SF)-FRQ-COSTI-ATT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoFrqCostiAtt().getPcoFrqCostiAttNullFormatted())) {
            // COB_CODE: MOVE PCO-FRQ-COSTI-ATT-NULL
            //             TO (SF)-FRQ-COSTI-ATT-NULL
            ws.getLccvpco1().getDati().getWpcoFrqCostiAtt().setWpcoFrqCostiAttNull(ws.getParamComp().getPcoFrqCostiAtt().getPcoFrqCostiAttNull());
        }
        else {
            // COB_CODE: MOVE PCO-FRQ-COSTI-ATT
            //             TO (SF)-FRQ-COSTI-ATT
            ws.getLccvpco1().getDati().getWpcoFrqCostiAtt().setWpcoFrqCostiAtt(ws.getParamComp().getPcoFrqCostiAtt().getPcoFrqCostiAtt());
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-COS-ST-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ELAB-COS-ST-NULL
        //           ELSE
        //                TO (SF)-DT-ULT-ELAB-COS-ST
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtUltElabCosSt().getPcoDtUltElabCosStNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COS-ST-NULL
            //             TO (SF)-DT-ULT-ELAB-COS-ST-NULL
            ws.getLccvpco1().getDati().getWpcoDtUltElabCosSt().setWpcoDtUltElabCosStNull(ws.getParamComp().getPcoDtUltElabCosSt().getPcoDtUltElabCosStNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COS-ST
            //             TO (SF)-DT-ULT-ELAB-COS-ST
            ws.getLccvpco1().getDati().getWpcoDtUltElabCosSt().setWpcoDtUltElabCosSt(ws.getParamComp().getPcoDtUltElabCosSt().getPcoDtUltElabCosSt());
        }
        // COB_CODE: IF PCO-FRQ-COSTI-STORNATI-NULL = HIGH-VALUES
        //                TO (SF)-FRQ-COSTI-STORNATI-NULL
        //           ELSE
        //                TO (SF)-FRQ-COSTI-STORNATI
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoFrqCostiStornati().getPcoFrqCostiStornatiNullFormatted())) {
            // COB_CODE: MOVE PCO-FRQ-COSTI-STORNATI-NULL
            //             TO (SF)-FRQ-COSTI-STORNATI-NULL
            ws.getLccvpco1().getDati().getWpcoFrqCostiStornati().setWpcoFrqCostiStornatiNull(ws.getParamComp().getPcoFrqCostiStornati().getPcoFrqCostiStornatiNull());
        }
        else {
            // COB_CODE: MOVE PCO-FRQ-COSTI-STORNATI
            //             TO (SF)-FRQ-COSTI-STORNATI
            ws.getLccvpco1().getDati().getWpcoFrqCostiStornati().setWpcoFrqCostiStornati(ws.getParamComp().getPcoFrqCostiStornati().getPcoFrqCostiStornati());
        }
        // COB_CODE: IF PCO-DT-ESTR-ASS-MIN70A-NULL = HIGH-VALUES
        //                TO (SF)-DT-ESTR-ASS-MIN70A-NULL
        //           ELSE
        //                TO (SF)-DT-ESTR-ASS-MIN70A
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtEstrAssMin70a().getPcoDtEstrAssMin70aNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ESTR-ASS-MIN70A-NULL
            //             TO (SF)-DT-ESTR-ASS-MIN70A-NULL
            ws.getLccvpco1().getDati().getWpcoDtEstrAssMin70a().setWpcoDtEstrAssMin70aNull(ws.getParamComp().getPcoDtEstrAssMin70a().getPcoDtEstrAssMin70aNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ESTR-ASS-MIN70A
            //             TO (SF)-DT-ESTR-ASS-MIN70A
            ws.getLccvpco1().getDati().getWpcoDtEstrAssMin70a().setWpcoDtEstrAssMin70a(ws.getParamComp().getPcoDtEstrAssMin70a().getPcoDtEstrAssMin70a());
        }
        // COB_CODE: IF PCO-DT-ESTR-ASS-MAG70A-NULL = HIGH-VALUES
        //                TO (SF)-DT-ESTR-ASS-MAG70A-NULL
        //           ELSE
        //                TO (SF)-DT-ESTR-ASS-MAG70A
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtEstrAssMag70a().getPcoDtEstrAssMag70aNullFormatted())) {
            // COB_CODE: MOVE PCO-DT-ESTR-ASS-MAG70A-NULL
            //             TO (SF)-DT-ESTR-ASS-MAG70A-NULL
            ws.getLccvpco1().getDati().getWpcoDtEstrAssMag70a().setWpcoDtEstrAssMag70aNull(ws.getParamComp().getPcoDtEstrAssMag70a().getPcoDtEstrAssMag70aNull());
        }
        else {
            // COB_CODE: MOVE PCO-DT-ESTR-ASS-MAG70A
            //             TO (SF)-DT-ESTR-ASS-MAG70A
            ws.getLccvpco1().getDati().getWpcoDtEstrAssMag70a().setWpcoDtEstrAssMag70a(ws.getParamComp().getPcoDtEstrAssMag70a().getPcoDtEstrAssMag70a());
        }
    }

    /**Original name: INIZIA-TOT-PCO<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVPCO4
	 *    ULTIMO AGG. 13 NOV 2018
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotPco() {
        // COB_CODE: PERFORM INIZIA-ZEROES-PCO THRU INIZIA-ZEROES-PCO-EX
        iniziaZeroesPco();
        // COB_CODE: PERFORM INIZIA-SPACES-PCO THRU INIZIA-SPACES-PCO-EX
        iniziaSpacesPco();
        // COB_CODE: PERFORM INIZIA-NULL-PCO THRU INIZIA-NULL-PCO-EX.
        iniziaNullPco();
    }

    /**Original name: INIZIA-NULL-PCO<br>*/
    private void iniziaNullPco() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-TRAT-CIRT-NULL
        ws.getLccvpco1().getDati().setWpcoCodTratCirt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDati.Len.WPCO_COD_TRAT_CIRT));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-LIM-VLTR-NULL
        ws.getLccvpco1().getDati().getWpcoLimVltr().setWpcoLimVltrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoLimVltr.Len.WPCO_LIM_VLTR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-RAT-PERF-NULL
        ws.getLccvpco1().getDati().setWpcoTpRatPerf(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ARROT-PRE-NULL
        ws.getLccvpco1().getDati().getWpcoArrotPre().setWpcoArrotPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoArrotPre.Len.WPCO_ARROT_PRE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-CONT-NULL
        ws.getLccvpco1().getDati().getWpcoDtCont().setWpcoDtContNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtCont.Len.WPCO_DT_CONT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-RIVAL-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltRivalIn().setWpcoDtUltRivalInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltRivalIn.Len.WPCO_DT_ULT_RIVAL_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-QTZO-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltQtzoIn().setWpcoDtUltQtzoInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltQtzoIn.Len.WPCO_DT_ULT_QTZO_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-RICL-RIASS-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltRiclRiass().setWpcoDtUltRiclRiassNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltRiclRiass.Len.WPCO_DT_ULT_RICL_RIASS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-TABUL-RIASS-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltTabulRiass().setWpcoDtUltTabulRiassNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltTabulRiass.Len.WPCO_DT_ULT_TABUL_RIASS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-EMES-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollEmes().setWpcoDtUltBollEmesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollEmes.Len.WPCO_DT_ULT_BOLL_EMES_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-STOR-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollStor().setWpcoDtUltBollStorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollStor.Len.WPCO_DT_ULT_BOLL_STOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-LIQ-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollLiq().setWpcoDtUltBollLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollLiq.Len.WPCO_DT_ULT_BOLL_LIQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-RIAT-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollRiat().setWpcoDtUltBollRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollRiat.Len.WPCO_DT_ULT_BOLL_RIAT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTELRISCPAR-PR-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltelriscparPr().setWpcoDtUltelriscparPrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltelriscparPr.Len.WPCO_DT_ULTELRISCPAR_PR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-IS-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcIsIn().setWpcoDtUltcIsInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcIsIn.Len.WPCO_DT_ULTC_IS_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-RICL-PRE-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltRiclPre().setWpcoDtUltRiclPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltRiclPre.Len.WPCO_DT_ULT_RICL_PRE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-MARSOL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcMarsol().setWpcoDtUltcMarsolNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcMarsol.Len.WPCO_DT_ULTC_MARSOL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-RB-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcRbIn().setWpcoDtUltcRbInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcRbIn.Len.WPCO_DT_ULTC_RB_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-PROV-1AA-ACQ-NULL
        ws.getLccvpco1().getDati().getWpcoPcProv1aaAcq().setWpcoPcProv1aaAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoPcProv1aaAcq.Len.WPCO_PC_PROV1AA_ACQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MOD-INTR-PREST-NULL
        ws.getLccvpco1().getDati().setWpcoModIntrPrest(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDati.Len.WPCO_MOD_INTR_PREST));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-GG-MAX-REC-PROV-NULL
        ws.getLccvpco1().getDati().getWpcoGgMaxRecProv().setWpcoGgMaxRecProvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoGgMaxRecProv.Len.WPCO_GG_MAX_REC_PROV_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTGZ-TRCH-E-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltgzTrchEIn().setWpcoDtUltgzTrchEInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltgzTrchEIn.Len.WPCO_DT_ULTGZ_TRCH_E_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-SNDEN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollSnden().setWpcoDtUltBollSndenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollSnden.Len.WPCO_DT_ULT_BOLL_SNDEN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-SNDNLQ-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollSndnlq().setWpcoDtUltBollSndnlqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollSndnlq.Len.WPCO_DT_ULT_BOLL_SNDNLQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTSC-ELAB-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltscElabIn().setWpcoDtUltscElabInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltscElabIn.Len.WPCO_DT_ULTSC_ELAB_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTSC-OPZ-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltscOpzIn().setWpcoDtUltscOpzInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltscOpzIn.Len.WPCO_DT_ULTSC_OPZ_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-BNSRIC-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcBnsricIn().setWpcoDtUltcBnsricInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcBnsricIn.Len.WPCO_DT_ULTC_BNSRIC_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-BNSFDT-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtIn().setWpcoDtUltcBnsfdtInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcBnsfdtIn.Len.WPCO_DT_ULTC_BNSFDT_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-RINN-GARAC-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltRinnGarac().setWpcoDtUltRinnGaracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltRinnGarac.Len.WPCO_DT_ULT_RINN_GARAC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTGZ-CED-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltgzCed().setWpcoDtUltgzCedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltgzCed.Len.WPCO_DT_ULTGZ_CED_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-PRLCOS-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabPrlcos().setWpcoDtUltElabPrlcosNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabPrlcos.Len.WPCO_DT_ULT_ELAB_PRLCOS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-RINN-COLL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltRinnColl().setWpcoDtUltRinnCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltRinnColl.Len.WPCO_DT_ULT_RINN_COLL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-RVC-PERF-NULL
        ws.getLccvpco1().getDati().setWpcoFlRvcPerf(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-RCS-POLI-NOPERF-NULL
        ws.getLccvpco1().getDati().setWpcoFlRcsPoliNoperf(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-GEST-PLUSV-NULL
        ws.getLccvpco1().getDati().setWpcoFlGestPlusv(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-RIVAL-CL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltRivalCl().setWpcoDtUltRivalClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltRivalCl.Len.WPCO_DT_ULT_RIVAL_CL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-QTZO-CL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltQtzoCl().setWpcoDtUltQtzoClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltQtzoCl.Len.WPCO_DT_ULT_QTZO_CL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-BNSRIC-CL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcBnsricCl().setWpcoDtUltcBnsricClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcBnsricCl.Len.WPCO_DT_ULTC_BNSRIC_CL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-BNSFDT-CL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtCl().setWpcoDtUltcBnsfdtClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcBnsfdtCl.Len.WPCO_DT_ULTC_BNSFDT_CL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-IS-CL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcIsCl().setWpcoDtUltcIsClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcIsCl.Len.WPCO_DT_ULTC_IS_CL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-RB-CL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcRbCl().setWpcoDtUltcRbClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcRbCl.Len.WPCO_DT_ULTC_RB_CL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTGZ-TRCH-E-CL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltgzTrchECl().setWpcoDtUltgzTrchEClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltgzTrchECl.Len.WPCO_DT_ULTGZ_TRCH_E_CL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTSC-ELAB-CL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltscElabCl().setWpcoDtUltscElabClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltscElabCl.Len.WPCO_DT_ULTSC_ELAB_CL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTSC-OPZ-CL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltscOpzCl().setWpcoDtUltscOpzClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltscOpzCl.Len.WPCO_DT_ULTSC_OPZ_CL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-STST-X-REGIONE-NULL
        ws.getLccvpco1().getDati().getWpcoStstXRegione().setWpcoStstXRegioneNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoStstXRegione.Len.WPCO_STST_X_REGIONE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTGZ-CED-COLL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltgzCedColl().setWpcoDtUltgzCedCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltgzCedColl.Len.WPCO_DT_ULTGZ_CED_COLL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-NUM-MM-CALC-MORA-NULL
        ws.getLccvpco1().getDati().getWpcoNumMmCalcMora().setWpcoNumMmCalcMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoNumMmCalcMora.Len.WPCO_NUM_MM_CALC_MORA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-EC-RIV-COLL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEcRivColl().setWpcoDtUltEcRivCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEcRivColl.Len.WPCO_DT_ULT_EC_RIV_COLL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-EC-RIV-IND-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEcRivInd().setWpcoDtUltEcRivIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEcRivInd.Len.WPCO_DT_ULT_EC_RIV_IND_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-EC-IL-COLL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEcIlColl().setWpcoDtUltEcIlCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEcIlColl.Len.WPCO_DT_ULT_EC_IL_COLL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-EC-IL-IND-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEcIlInd().setWpcoDtUltEcIlIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEcIlInd.Len.WPCO_DT_ULT_EC_IL_IND_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-EC-UL-COLL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEcUlColl().setWpcoDtUltEcUlCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEcUlColl.Len.WPCO_DT_ULT_EC_UL_COLL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-EC-UL-IND-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEcUlInd().setWpcoDtUltEcUlIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEcUlInd.Len.WPCO_DT_ULT_EC_UL_IND_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-AA-UTI-NULL
        ws.getLccvpco1().getDati().getWpcoAaUti().setWpcoAaUtiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoAaUti.Len.WPCO_AA_UTI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-PERF-C-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollPerfC().setWpcoDtUltBollPerfCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollPerfC.Len.WPCO_DT_ULT_BOLL_PERF_C_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-RSP-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollRspIn().setWpcoDtUltBollRspInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollRspIn.Len.WPCO_DT_ULT_BOLL_RSP_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-RSP-CL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollRspCl().setWpcoDtUltBollRspClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollRspCl.Len.WPCO_DT_ULT_BOLL_RSP_CL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-EMES-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollEmesI().setWpcoDtUltBollEmesINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollEmesI.Len.WPCO_DT_ULT_BOLL_EMES_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-STOR-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollStorI().setWpcoDtUltBollStorINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollStorI.Len.WPCO_DT_ULT_BOLL_STOR_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-RIAT-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollRiatI().setWpcoDtUltBollRiatINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollRiatI.Len.WPCO_DT_ULT_BOLL_RIAT_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-SD-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollSdI().setWpcoDtUltBollSdINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollSdI.Len.WPCO_DT_ULT_BOLL_SD_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-SDNL-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollSdnlI().setWpcoDtUltBollSdnlINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollSdnlI.Len.WPCO_DT_ULT_BOLL_SDNL_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-PERF-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollPerfI().setWpcoDtUltBollPerfINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollPerfI.Len.WPCO_DT_ULT_BOLL_PERF_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-RICL-RIRIAS-COM-NULL
        ws.getLccvpco1().getDati().getWpcoDtRiclRiriasCom().setWpcoDtRiclRiriasComNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtRiclRiriasCom.Len.WPCO_DT_RICL_RIRIAS_COM_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-AT92-C-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabAt92C().setWpcoDtUltElabAt92CNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabAt92C.Len.WPCO_DT_ULT_ELAB_AT92_C_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-AT92-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabAt92I().setWpcoDtUltElabAt92INull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabAt92I.Len.WPCO_DT_ULT_ELAB_AT92_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-AT93-C-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabAt93C().setWpcoDtUltElabAt93CNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabAt93C.Len.WPCO_DT_ULT_ELAB_AT93_C_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-AT93-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabAt93I().setWpcoDtUltElabAt93INull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabAt93I.Len.WPCO_DT_ULT_ELAB_AT93_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-SPE-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabSpeIn().setWpcoDtUltElabSpeInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabSpeIn.Len.WPCO_DT_ULT_ELAB_SPE_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-PR-CON-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabPrCon().setWpcoDtUltElabPrConNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabPrCon.Len.WPCO_DT_ULT_ELAB_PR_CON_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-RP-CL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollRpCl().setWpcoDtUltBollRpClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollRpCl.Len.WPCO_DT_ULT_BOLL_RP_CL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-RP-IN-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollRpIn().setWpcoDtUltBollRpInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollRpIn.Len.WPCO_DT_ULT_BOLL_RP_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-PRE-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollPreI().setWpcoDtUltBollPreINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollPreI.Len.WPCO_DT_ULT_BOLL_PRE_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-PRE-C-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollPreC().setWpcoDtUltBollPreCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollPreC.Len.WPCO_DT_ULT_BOLL_PRE_C_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-PILDI-MM-C-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmC().setWpcoDtUltcPildiMmCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcPildiMmC.Len.WPCO_DT_ULTC_PILDI_MM_C_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-PILDI-AA-C-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaC().setWpcoDtUltcPildiAaCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcPildiAaC.Len.WPCO_DT_ULTC_PILDI_AA_C_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-PILDI-MM-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmI().setWpcoDtUltcPildiMmINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcPildiMmI.Len.WPCO_DT_ULTC_PILDI_MM_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-PILDI-TR-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcPildiTrI().setWpcoDtUltcPildiTrINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcPildiTrI.Len.WPCO_DT_ULTC_PILDI_TR_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULTC-PILDI-AA-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaI().setWpcoDtUltcPildiAaINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltcPildiAaI.Len.WPCO_DT_ULTC_PILDI_AA_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-QUIE-C-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollQuieC().setWpcoDtUltBollQuieCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollQuieC.Len.WPCO_DT_ULT_BOLL_QUIE_C_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-QUIE-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollQuieI().setWpcoDtUltBollQuieINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollQuieI.Len.WPCO_DT_ULT_BOLL_QUIE_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-COTR-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollCotrI().setWpcoDtUltBollCotrINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollCotrI.Len.WPCO_DT_ULT_BOLL_COTR_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-COTR-C-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollCotrC().setWpcoDtUltBollCotrCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollCotrC.Len.WPCO_DT_ULT_BOLL_COTR_C_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-CORI-C-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollCoriC().setWpcoDtUltBollCoriCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollCoriC.Len.WPCO_DT_ULT_BOLL_CORI_C_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-BOLL-CORI-I-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltBollCoriI().setWpcoDtUltBollCoriINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltBollCoriI.Len.WPCO_DT_ULT_BOLL_CORI_I_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-VALZZ-DT-VLT-NULL
        ws.getLccvpco1().getDati().setWpcoTpValzzDtVlt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDati.Len.WPCO_TP_VALZZ_DT_VLT));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-FRAZ-PROV-ACQ-NULL
        ws.getLccvpco1().getDati().setWpcoFlFrazProvAcq(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-AGG-EROG-RE-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltAggErogRe().setWpcoDtUltAggErogReNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltAggErogRe.Len.WPCO_DT_ULT_AGG_EROG_RE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-RM-MARSOL-NULL
        ws.getLccvpco1().getDati().getWpcoPcRmMarsol().setWpcoPcRmMarsolNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoPcRmMarsol.Len.WPCO_PC_RM_MARSOL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-C-SUBRSH-MARSOL-NULL
        ws.getLccvpco1().getDati().getWpcoPcCSubrshMarsol().setWpcoPcCSubrshMarsolNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoPcCSubrshMarsol.Len.WPCO_PC_C_SUBRSH_MARSOL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-COMP-ISVAP-NULL
        ws.getLccvpco1().getDati().setWpcoCodCompIsvap(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDati.Len.WPCO_COD_COMP_ISVAP));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-LM-RIS-CON-INT-NULL
        ws.getLccvpco1().getDati().getWpcoLmRisConInt().setWpcoLmRisConIntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoLmRisConInt.Len.WPCO_LM_RIS_CON_INT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-LM-C-SUBRSH-CON-IN-NULL
        ws.getLccvpco1().getDati().getWpcoLmCSubrshConIn().setWpcoLmCSubrshConInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoLmCSubrshConIn.Len.WPCO_LM_C_SUBRSH_CON_IN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-GAR-NORISK-MARS-NULL
        ws.getLccvpco1().getDati().getWpcoPcGarNoriskMars().setWpcoPcGarNoriskMarsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoPcGarNoriskMars.Len.WPCO_PC_GAR_NORISK_MARS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CRZ-1A-RAT-INTR-PR-NULL
        ws.getLccvpco1().getDati().setWpcoCrz1aRatIntrPr(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-NUM-GG-ARR-INTR-PR-NULL
        ws.getLccvpco1().getDati().getWpcoNumGgArrIntrPr().setWpcoNumGgArrIntrPrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoNumGgArrIntrPr.Len.WPCO_NUM_GG_ARR_INTR_PR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-VISUAL-VINPG-NULL
        ws.getLccvpco1().getDati().setWpcoFlVisualVinpg(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ESTRAZ-FUG-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEstrazFug().setWpcoDtUltEstrazFugNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEstrazFug.Len.WPCO_DT_ULT_ESTRAZ_FUG_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-PR-AUT-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabPrAut().setWpcoDtUltElabPrAutNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabPrAut.Len.WPCO_DT_ULT_ELAB_PR_AUT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-COMMEF-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabCommef().setWpcoDtUltElabCommefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabCommef.Len.WPCO_DT_ULT_ELAB_COMMEF_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-LIQMEF-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabLiqmef().setWpcoDtUltElabLiqmefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabLiqmef.Len.WPCO_DT_ULT_ELAB_LIQMEF_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-FISC-MEF-NULL
        ws.getLccvpco1().getDati().setWpcoCodFiscMef(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDati.Len.WPCO_COD_FISC_MEF));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-ASS-SOCIALE-NULL
        ws.getLccvpco1().getDati().getWpcoImpAssSociale().setWpcoImpAssSocialeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoImpAssSociale.Len.WPCO_IMP_ASS_SOCIALE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MOD-COMNZ-INVST-SW-NULL
        ws.getLccvpco1().getDati().setWpcoModComnzInvstSw(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-RIAT-RIASS-RSH-NULL
        ws.getLccvpco1().getDati().getWpcoDtRiatRiassRsh().setWpcoDtRiatRiassRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtRiatRiassRsh.Len.WPCO_DT_RIAT_RIASS_RSH_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-RIAT-RIASS-COMM-NULL
        ws.getLccvpco1().getDati().getWpcoDtRiatRiassComm().setWpcoDtRiatRiassCommNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtRiatRiassComm.Len.WPCO_DT_RIAT_RIASS_COMM_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-GG-INTR-RIT-PAG-NULL
        ws.getLccvpco1().getDati().getWpcoGgIntrRitPag().setWpcoGgIntrRitPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoGgIntrRitPag.Len.WPCO_GG_INTR_RIT_PAG_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-RINN-TAC-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltRinnTac().setWpcoDtUltRinnTacNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltRinnTac.Len.WPCO_DT_ULT_RINN_TAC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DESC-COMP
        ws.getLccvpco1().getDati().setWpcoDescComp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDati.Len.WPCO_DESC_COMP));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-EC-TCM-IND-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEcTcmInd().setWpcoDtUltEcTcmIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEcTcmInd.Len.WPCO_DT_ULT_EC_TCM_IND_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-EC-TCM-COLL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEcTcmColl().setWpcoDtUltEcTcmCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEcTcmColl.Len.WPCO_DT_ULT_EC_TCM_COLL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-EC-MRM-IND-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEcMrmInd().setWpcoDtUltEcMrmIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEcMrmInd.Len.WPCO_DT_ULT_EC_MRM_IND_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-EC-MRM-COLL-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEcMrmColl().setWpcoDtUltEcMrmCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEcMrmColl.Len.WPCO_DT_ULT_EC_MRM_COLL_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-COMP-LDAP-NULL
        ws.getLccvpco1().getDati().setWpcoCodCompLdap(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDati.Len.WPCO_COD_COMP_LDAP));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-RID-IMP-1382011-NULL
        ws.getLccvpco1().getDati().getWpcoPcRidImp1382011().setWpcoPcRidImp1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoPcRidImp1382011.Len.WPCO_PC_RID_IMP1382011_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-RID-IMP-662014-NULL
        ws.getLccvpco1().getDati().getWpcoPcRidImp662014().setWpcoPcRidImp662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoPcRidImp662014.Len.WPCO_PC_RID_IMP662014_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SOGL-AML-PRE-UNI-NULL
        ws.getLccvpco1().getDati().getWpcoSoglAmlPreUni().setWpcoSoglAmlPreUniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoSoglAmlPreUni.Len.WPCO_SOGL_AML_PRE_UNI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SOGL-AML-PRE-PER-NULL
        ws.getLccvpco1().getDati().getWpcoSoglAmlPrePer().setWpcoSoglAmlPrePerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoSoglAmlPrePer.Len.WPCO_SOGL_AML_PRE_PER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-SOGG-FTZ-ASSTO-NULL
        ws.getLccvpco1().getDati().setWpcoCodSoggFtzAssto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDati.Len.WPCO_COD_SOGG_FTZ_ASSTO));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-REDPRO-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabRedpro().setWpcoDtUltElabRedproNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabRedpro.Len.WPCO_DT_ULT_ELAB_REDPRO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-TAKE-P-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabTakeP().setWpcoDtUltElabTakePNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabTakeP.Len.WPCO_DT_ULT_ELAB_TAKE_P_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-PASPAS-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabPaspas().setWpcoDtUltElabPaspasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabPaspas.Len.WPCO_DT_ULT_ELAB_PASPAS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SOGL-AML-PRE-SAV-R-NULL
        ws.getLccvpco1().getDati().getWpcoSoglAmlPreSavR().setWpcoSoglAmlPreSavRNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoSoglAmlPreSavR.Len.WPCO_SOGL_AML_PRE_SAV_R_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ESTR-DEC-CO-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltEstrDecCo().setWpcoDtUltEstrDecCoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltEstrDecCo.Len.WPCO_DT_ULT_ESTR_DEC_CO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-COS-AT-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabCosAt().setWpcoDtUltElabCosAtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabCosAt.Len.WPCO_DT_ULT_ELAB_COS_AT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FRQ-COSTI-ATT-NULL
        ws.getLccvpco1().getDati().getWpcoFrqCostiAtt().setWpcoFrqCostiAttNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoFrqCostiAtt.Len.WPCO_FRQ_COSTI_ATT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-ELAB-COS-ST-NULL
        ws.getLccvpco1().getDati().getWpcoDtUltElabCosSt().setWpcoDtUltElabCosStNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtUltElabCosSt.Len.WPCO_DT_ULT_ELAB_COS_ST_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FRQ-COSTI-STORNATI-NULL
        ws.getLccvpco1().getDati().getWpcoFrqCostiStornati().setWpcoFrqCostiStornatiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoFrqCostiStornati.Len.WPCO_FRQ_COSTI_STORNATI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ESTR-ASS-MIN70A-NULL
        ws.getLccvpco1().getDati().getWpcoDtEstrAssMin70a().setWpcoDtEstrAssMin70aNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtEstrAssMin70a.Len.WPCO_DT_ESTR_ASS_MIN70A_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ESTR-ASS-MAG70A-NULL.
        ws.getLccvpco1().getDati().getWpcoDtEstrAssMag70a().setWpcoDtEstrAssMag70aNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpcoDtEstrAssMag70a.Len.WPCO_DT_ESTR_ASS_MAG70A_NULL));
    }

    /**Original name: INIZIA-ZEROES-PCO<br>*/
    private void iniziaZeroesPco() {
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA
        ws.getLccvpco1().getDati().setWpcoCodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-FL-LIV-DEBUG
        ws.getLccvpco1().getDati().setWpcoFlLivDebug(((short)0));
        // COB_CODE: MOVE 0 TO (SF)-DS-VER
        ws.getLccvpco1().getDati().setWpcoDsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-CPTZ.
        ws.getLccvpco1().getDati().setWpcoDsTsCptz(0);
    }

    /**Original name: INIZIA-SPACES-PCO<br>*/
    private void iniziaSpacesPco() {
        // COB_CODE: MOVE SPACES TO (SF)-TP-LIV-GENZ-TIT
        ws.getLccvpco1().getDati().setWpcoTpLivGenzTit("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-MOD-RIVAL
        ws.getLccvpco1().getDati().setWpcoTpModRival("");
        // COB_CODE: MOVE SPACES TO (SF)-CALC-RSH-COMUN
        ws.getLccvpco1().getDati().setWpcoCalcRshComun(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-OPER-SQL
        ws.getLccvpco1().getDati().setWpcoDsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-UTENTE
        ws.getLccvpco1().getDati().setWpcoDsUtente("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-STATO-ELAB.
        ws.getLccvpco1().getDati().setWpcoDsStatoElab(Types.SPACE_CHAR);
    }

    /**Original name: VAL-DCLGEN-PCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PROCEDURE AGGIORNAMENTO PORTAFOGLIO
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVPCO5
	 *    ULTIMO AGG. 13 NOV 2018
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenPco() {
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA
        //              TO PCO-COD-COMP-ANIA
        ws.getParamComp().setPcoCodCompAnia(ws.getLccvpco1().getDati().getWpcoCodCompAnia());
        // COB_CODE: IF (SF)-COD-TRAT-CIRT-NULL = HIGH-VALUES
        //              TO PCO-COD-TRAT-CIRT-NULL
        //           ELSE
        //              TO PCO-COD-TRAT-CIRT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoCodTratCirtFormatted())) {
            // COB_CODE: MOVE (SF)-COD-TRAT-CIRT-NULL
            //           TO PCO-COD-TRAT-CIRT-NULL
            ws.getParamComp().setPcoCodTratCirt(ws.getLccvpco1().getDati().getWpcoCodTratCirt());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-TRAT-CIRT
            //           TO PCO-COD-TRAT-CIRT
            ws.getParamComp().setPcoCodTratCirt(ws.getLccvpco1().getDati().getWpcoCodTratCirt());
        }
        // COB_CODE: IF (SF)-LIM-VLTR-NULL = HIGH-VALUES
        //              TO PCO-LIM-VLTR-NULL
        //           ELSE
        //              TO PCO-LIM-VLTR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoLimVltr().getWpcoLimVltrNullFormatted())) {
            // COB_CODE: MOVE (SF)-LIM-VLTR-NULL
            //           TO PCO-LIM-VLTR-NULL
            ws.getParamComp().getPcoLimVltr().setPcoLimVltrNull(ws.getLccvpco1().getDati().getWpcoLimVltr().getWpcoLimVltrNull());
        }
        else {
            // COB_CODE: MOVE (SF)-LIM-VLTR
            //           TO PCO-LIM-VLTR
            ws.getParamComp().getPcoLimVltr().setPcoLimVltr(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoLimVltr().getWpcoLimVltr(), 15, 3));
        }
        // COB_CODE: IF (SF)-TP-RAT-PERF-NULL = HIGH-VALUES
        //              TO PCO-TP-RAT-PERF-NULL
        //           ELSE
        //              TO PCO-TP-RAT-PERF
        //           END-IF
        if (Conditions.eq(ws.getLccvpco1().getDati().getWpcoTpRatPerf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-TP-RAT-PERF-NULL
            //           TO PCO-TP-RAT-PERF-NULL
            ws.getParamComp().setPcoTpRatPerf(ws.getLccvpco1().getDati().getWpcoTpRatPerf());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-RAT-PERF
            //           TO PCO-TP-RAT-PERF
            ws.getParamComp().setPcoTpRatPerf(ws.getLccvpco1().getDati().getWpcoTpRatPerf());
        }
        // COB_CODE: MOVE (SF)-TP-LIV-GENZ-TIT
        //              TO PCO-TP-LIV-GENZ-TIT
        ws.getParamComp().setPcoTpLivGenzTit(ws.getLccvpco1().getDati().getWpcoTpLivGenzTit());
        // COB_CODE: IF (SF)-ARROT-PRE-NULL = HIGH-VALUES
        //              TO PCO-ARROT-PRE-NULL
        //           ELSE
        //              TO PCO-ARROT-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoArrotPre().getWpcoArrotPreNullFormatted())) {
            // COB_CODE: MOVE (SF)-ARROT-PRE-NULL
            //           TO PCO-ARROT-PRE-NULL
            ws.getParamComp().getPcoArrotPre().setPcoArrotPreNull(ws.getLccvpco1().getDati().getWpcoArrotPre().getWpcoArrotPreNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ARROT-PRE
            //           TO PCO-ARROT-PRE
            ws.getParamComp().getPcoArrotPre().setPcoArrotPre(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoArrotPre().getWpcoArrotPre(), 15, 3));
        }
        // COB_CODE: IF (SF)-DT-CONT-NULL = HIGH-VALUES
        //              TO PCO-DT-CONT-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtCont().getWpcoDtContNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-CONT-NULL
            //           TO PCO-DT-CONT-NULL
            ws.getParamComp().getPcoDtCont().setPcoDtContNull(ws.getLccvpco1().getDati().getWpcoDtCont().getWpcoDtContNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtCont().getWpcoDtCont() == 0) {
            // COB_CODE: IF (SF)-DT-CONT = ZERO
            //              TO PCO-DT-CONT-NULL
            //           ELSE
            //            TO PCO-DT-CONT
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-CONT-NULL
            ws.getParamComp().getPcoDtCont().setPcoDtContNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtCont.Len.PCO_DT_CONT_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-CONT
            //           TO PCO-DT-CONT
            ws.getParamComp().getPcoDtCont().setPcoDtCont(ws.getLccvpco1().getDati().getWpcoDtCont().getWpcoDtCont());
        }
        // COB_CODE: IF (SF)-DT-ULT-RIVAL-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-RIVAL-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltRivalIn().getWpcoDtUltRivalInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-RIVAL-IN-NULL
            //           TO PCO-DT-ULT-RIVAL-IN-NULL
            ws.getParamComp().getPcoDtUltRivalIn().setPcoDtUltRivalInNull(ws.getLccvpco1().getDati().getWpcoDtUltRivalIn().getWpcoDtUltRivalInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltRivalIn().getWpcoDtUltRivalIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-RIVAL-IN = ZERO
            //              TO PCO-DT-ULT-RIVAL-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULT-RIVAL-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-RIVAL-IN-NULL
            ws.getParamComp().getPcoDtUltRivalIn().setPcoDtUltRivalInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRivalIn.Len.PCO_DT_ULT_RIVAL_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-RIVAL-IN
            //           TO PCO-DT-ULT-RIVAL-IN
            ws.getParamComp().getPcoDtUltRivalIn().setPcoDtUltRivalIn(ws.getLccvpco1().getDati().getWpcoDtUltRivalIn().getWpcoDtUltRivalIn());
        }
        // COB_CODE: IF (SF)-DT-ULT-QTZO-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-QTZO-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltQtzoIn().getWpcoDtUltQtzoInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-QTZO-IN-NULL
            //           TO PCO-DT-ULT-QTZO-IN-NULL
            ws.getParamComp().getPcoDtUltQtzoIn().setPcoDtUltQtzoInNull(ws.getLccvpco1().getDati().getWpcoDtUltQtzoIn().getWpcoDtUltQtzoInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltQtzoIn().getWpcoDtUltQtzoIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-QTZO-IN = ZERO
            //              TO PCO-DT-ULT-QTZO-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULT-QTZO-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-QTZO-IN-NULL
            ws.getParamComp().getPcoDtUltQtzoIn().setPcoDtUltQtzoInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltQtzoIn.Len.PCO_DT_ULT_QTZO_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-QTZO-IN
            //           TO PCO-DT-ULT-QTZO-IN
            ws.getParamComp().getPcoDtUltQtzoIn().setPcoDtUltQtzoIn(ws.getLccvpco1().getDati().getWpcoDtUltQtzoIn().getWpcoDtUltQtzoIn());
        }
        // COB_CODE: IF (SF)-DT-ULT-RICL-RIASS-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-RICL-RIASS-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltRiclRiass().getWpcoDtUltRiclRiassNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-RICL-RIASS-NULL
            //           TO PCO-DT-ULT-RICL-RIASS-NULL
            ws.getParamComp().getPcoDtUltRiclRiass().setPcoDtUltRiclRiassNull(ws.getLccvpco1().getDati().getWpcoDtUltRiclRiass().getWpcoDtUltRiclRiassNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltRiclRiass().getWpcoDtUltRiclRiass() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-RICL-RIASS = ZERO
            //              TO PCO-DT-ULT-RICL-RIASS-NULL
            //           ELSE
            //            TO PCO-DT-ULT-RICL-RIASS
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-RICL-RIASS-NULL
            ws.getParamComp().getPcoDtUltRiclRiass().setPcoDtUltRiclRiassNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRiclRiass.Len.PCO_DT_ULT_RICL_RIASS_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-RICL-RIASS
            //           TO PCO-DT-ULT-RICL-RIASS
            ws.getParamComp().getPcoDtUltRiclRiass().setPcoDtUltRiclRiass(ws.getLccvpco1().getDati().getWpcoDtUltRiclRiass().getWpcoDtUltRiclRiass());
        }
        // COB_CODE: IF (SF)-DT-ULT-TABUL-RIASS-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-TABUL-RIASS-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltTabulRiass().getWpcoDtUltTabulRiassNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-TABUL-RIASS-NULL
            //           TO PCO-DT-ULT-TABUL-RIASS-NULL
            ws.getParamComp().getPcoDtUltTabulRiass().setPcoDtUltTabulRiassNull(ws.getLccvpco1().getDati().getWpcoDtUltTabulRiass().getWpcoDtUltTabulRiassNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltTabulRiass().getWpcoDtUltTabulRiass() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-TABUL-RIASS = ZERO
            //              TO PCO-DT-ULT-TABUL-RIASS-NULL
            //           ELSE
            //            TO PCO-DT-ULT-TABUL-RIASS
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-TABUL-RIASS-NULL
            ws.getParamComp().getPcoDtUltTabulRiass().setPcoDtUltTabulRiassNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltTabulRiass.Len.PCO_DT_ULT_TABUL_RIASS_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-TABUL-RIASS
            //           TO PCO-DT-ULT-TABUL-RIASS
            ws.getParamComp().getPcoDtUltTabulRiass().setPcoDtUltTabulRiass(ws.getLccvpco1().getDati().getWpcoDtUltTabulRiass().getWpcoDtUltTabulRiass());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-EMES-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-EMES-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollEmes().getWpcoDtUltBollEmesNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-EMES-NULL
            //           TO PCO-DT-ULT-BOLL-EMES-NULL
            ws.getParamComp().getPcoDtUltBollEmes().setPcoDtUltBollEmesNull(ws.getLccvpco1().getDati().getWpcoDtUltBollEmes().getWpcoDtUltBollEmesNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollEmes().getWpcoDtUltBollEmes() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-EMES = ZERO
            //              TO PCO-DT-ULT-BOLL-EMES-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-EMES
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-EMES-NULL
            ws.getParamComp().getPcoDtUltBollEmes().setPcoDtUltBollEmesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollEmes.Len.PCO_DT_ULT_BOLL_EMES_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-EMES
            //           TO PCO-DT-ULT-BOLL-EMES
            ws.getParamComp().getPcoDtUltBollEmes().setPcoDtUltBollEmes(ws.getLccvpco1().getDati().getWpcoDtUltBollEmes().getWpcoDtUltBollEmes());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-STOR-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-STOR-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollStor().getWpcoDtUltBollStorNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-STOR-NULL
            //           TO PCO-DT-ULT-BOLL-STOR-NULL
            ws.getParamComp().getPcoDtUltBollStor().setPcoDtUltBollStorNull(ws.getLccvpco1().getDati().getWpcoDtUltBollStor().getWpcoDtUltBollStorNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollStor().getWpcoDtUltBollStor() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-STOR = ZERO
            //              TO PCO-DT-ULT-BOLL-STOR-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-STOR
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-STOR-NULL
            ws.getParamComp().getPcoDtUltBollStor().setPcoDtUltBollStorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollStor.Len.PCO_DT_ULT_BOLL_STOR_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-STOR
            //           TO PCO-DT-ULT-BOLL-STOR
            ws.getParamComp().getPcoDtUltBollStor().setPcoDtUltBollStor(ws.getLccvpco1().getDati().getWpcoDtUltBollStor().getWpcoDtUltBollStor());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-LIQ-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-LIQ-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollLiq().getWpcoDtUltBollLiqNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-LIQ-NULL
            //           TO PCO-DT-ULT-BOLL-LIQ-NULL
            ws.getParamComp().getPcoDtUltBollLiq().setPcoDtUltBollLiqNull(ws.getLccvpco1().getDati().getWpcoDtUltBollLiq().getWpcoDtUltBollLiqNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollLiq().getWpcoDtUltBollLiq() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-LIQ = ZERO
            //              TO PCO-DT-ULT-BOLL-LIQ-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-LIQ
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-LIQ-NULL
            ws.getParamComp().getPcoDtUltBollLiq().setPcoDtUltBollLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollLiq.Len.PCO_DT_ULT_BOLL_LIQ_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-LIQ
            //           TO PCO-DT-ULT-BOLL-LIQ
            ws.getParamComp().getPcoDtUltBollLiq().setPcoDtUltBollLiq(ws.getLccvpco1().getDati().getWpcoDtUltBollLiq().getWpcoDtUltBollLiq());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-RIAT-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-RIAT-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollRiat().getWpcoDtUltBollRiatNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RIAT-NULL
            //           TO PCO-DT-ULT-BOLL-RIAT-NULL
            ws.getParamComp().getPcoDtUltBollRiat().setPcoDtUltBollRiatNull(ws.getLccvpco1().getDati().getWpcoDtUltBollRiat().getWpcoDtUltBollRiatNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollRiat().getWpcoDtUltBollRiat() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-RIAT = ZERO
            //              TO PCO-DT-ULT-BOLL-RIAT-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-RIAT
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-RIAT-NULL
            ws.getParamComp().getPcoDtUltBollRiat().setPcoDtUltBollRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRiat.Len.PCO_DT_ULT_BOLL_RIAT_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RIAT
            //           TO PCO-DT-ULT-BOLL-RIAT
            ws.getParamComp().getPcoDtUltBollRiat().setPcoDtUltBollRiat(ws.getLccvpco1().getDati().getWpcoDtUltBollRiat().getWpcoDtUltBollRiat());
        }
        // COB_CODE: IF (SF)-DT-ULTELRISCPAR-PR-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTELRISCPAR-PR-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltelriscparPr().getWpcoDtUltelriscparPrNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTELRISCPAR-PR-NULL
            //           TO PCO-DT-ULTELRISCPAR-PR-NULL
            ws.getParamComp().getPcoDtUltelriscparPr().setPcoDtUltelriscparPrNull(ws.getLccvpco1().getDati().getWpcoDtUltelriscparPr().getWpcoDtUltelriscparPrNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltelriscparPr().getWpcoDtUltelriscparPr() == 0) {
            // COB_CODE: IF (SF)-DT-ULTELRISCPAR-PR = ZERO
            //              TO PCO-DT-ULTELRISCPAR-PR-NULL
            //           ELSE
            //            TO PCO-DT-ULTELRISCPAR-PR
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTELRISCPAR-PR-NULL
            ws.getParamComp().getPcoDtUltelriscparPr().setPcoDtUltelriscparPrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltelriscparPr.Len.PCO_DT_ULTELRISCPAR_PR_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTELRISCPAR-PR
            //           TO PCO-DT-ULTELRISCPAR-PR
            ws.getParamComp().getPcoDtUltelriscparPr().setPcoDtUltelriscparPr(ws.getLccvpco1().getDati().getWpcoDtUltelriscparPr().getWpcoDtUltelriscparPr());
        }
        // COB_CODE: IF (SF)-DT-ULTC-IS-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-IS-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcIsIn().getWpcoDtUltcIsInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-IS-IN-NULL
            //           TO PCO-DT-ULTC-IS-IN-NULL
            ws.getParamComp().getPcoDtUltcIsIn().setPcoDtUltcIsInNull(ws.getLccvpco1().getDati().getWpcoDtUltcIsIn().getWpcoDtUltcIsInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcIsIn().getWpcoDtUltcIsIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-IS-IN = ZERO
            //              TO PCO-DT-ULTC-IS-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-IS-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-IS-IN-NULL
            ws.getParamComp().getPcoDtUltcIsIn().setPcoDtUltcIsInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcIsIn.Len.PCO_DT_ULTC_IS_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-IS-IN
            //           TO PCO-DT-ULTC-IS-IN
            ws.getParamComp().getPcoDtUltcIsIn().setPcoDtUltcIsIn(ws.getLccvpco1().getDati().getWpcoDtUltcIsIn().getWpcoDtUltcIsIn());
        }
        // COB_CODE: IF (SF)-DT-ULT-RICL-PRE-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-RICL-PRE-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltRiclPre().getWpcoDtUltRiclPreNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-RICL-PRE-NULL
            //           TO PCO-DT-ULT-RICL-PRE-NULL
            ws.getParamComp().getPcoDtUltRiclPre().setPcoDtUltRiclPreNull(ws.getLccvpco1().getDati().getWpcoDtUltRiclPre().getWpcoDtUltRiclPreNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltRiclPre().getWpcoDtUltRiclPre() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-RICL-PRE = ZERO
            //              TO PCO-DT-ULT-RICL-PRE-NULL
            //           ELSE
            //            TO PCO-DT-ULT-RICL-PRE
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-RICL-PRE-NULL
            ws.getParamComp().getPcoDtUltRiclPre().setPcoDtUltRiclPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRiclPre.Len.PCO_DT_ULT_RICL_PRE_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-RICL-PRE
            //           TO PCO-DT-ULT-RICL-PRE
            ws.getParamComp().getPcoDtUltRiclPre().setPcoDtUltRiclPre(ws.getLccvpco1().getDati().getWpcoDtUltRiclPre().getWpcoDtUltRiclPre());
        }
        // COB_CODE: IF (SF)-DT-ULTC-MARSOL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-MARSOL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcMarsol().getWpcoDtUltcMarsolNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-MARSOL-NULL
            //           TO PCO-DT-ULTC-MARSOL-NULL
            ws.getParamComp().getPcoDtUltcMarsol().setPcoDtUltcMarsolNull(ws.getLccvpco1().getDati().getWpcoDtUltcMarsol().getWpcoDtUltcMarsolNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcMarsol().getWpcoDtUltcMarsol() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-MARSOL = ZERO
            //              TO PCO-DT-ULTC-MARSOL-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-MARSOL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-MARSOL-NULL
            ws.getParamComp().getPcoDtUltcMarsol().setPcoDtUltcMarsolNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcMarsol.Len.PCO_DT_ULTC_MARSOL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-MARSOL
            //           TO PCO-DT-ULTC-MARSOL
            ws.getParamComp().getPcoDtUltcMarsol().setPcoDtUltcMarsol(ws.getLccvpco1().getDati().getWpcoDtUltcMarsol().getWpcoDtUltcMarsol());
        }
        // COB_CODE: IF (SF)-DT-ULTC-RB-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-RB-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcRbIn().getWpcoDtUltcRbInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-RB-IN-NULL
            //           TO PCO-DT-ULTC-RB-IN-NULL
            ws.getParamComp().getPcoDtUltcRbIn().setPcoDtUltcRbInNull(ws.getLccvpco1().getDati().getWpcoDtUltcRbIn().getWpcoDtUltcRbInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcRbIn().getWpcoDtUltcRbIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-RB-IN = ZERO
            //              TO PCO-DT-ULTC-RB-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-RB-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-RB-IN-NULL
            ws.getParamComp().getPcoDtUltcRbIn().setPcoDtUltcRbInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcRbIn.Len.PCO_DT_ULTC_RB_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-RB-IN
            //           TO PCO-DT-ULTC-RB-IN
            ws.getParamComp().getPcoDtUltcRbIn().setPcoDtUltcRbIn(ws.getLccvpco1().getDati().getWpcoDtUltcRbIn().getWpcoDtUltcRbIn());
        }
        // COB_CODE: IF (SF)-PC-PROV-1AA-ACQ-NULL = HIGH-VALUES
        //              TO PCO-PC-PROV-1AA-ACQ-NULL
        //           ELSE
        //              TO PCO-PC-PROV-1AA-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoPcProv1aaAcq().getWpcoPcProv1aaAcqNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-PROV-1AA-ACQ-NULL
            //           TO PCO-PC-PROV-1AA-ACQ-NULL
            ws.getParamComp().getPcoPcProv1aaAcq().setPcoPcProv1aaAcqNull(ws.getLccvpco1().getDati().getWpcoPcProv1aaAcq().getWpcoPcProv1aaAcqNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-PROV-1AA-ACQ
            //           TO PCO-PC-PROV-1AA-ACQ
            ws.getParamComp().getPcoPcProv1aaAcq().setPcoPcProv1aaAcq(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoPcProv1aaAcq().getWpcoPcProv1aaAcq(), 6, 3));
        }
        // COB_CODE: IF (SF)-MOD-INTR-PREST-NULL = HIGH-VALUES
        //              TO PCO-MOD-INTR-PREST-NULL
        //           ELSE
        //              TO PCO-MOD-INTR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoModIntrPrestFormatted())) {
            // COB_CODE: MOVE (SF)-MOD-INTR-PREST-NULL
            //           TO PCO-MOD-INTR-PREST-NULL
            ws.getParamComp().setPcoModIntrPrest(ws.getLccvpco1().getDati().getWpcoModIntrPrest());
        }
        else {
            // COB_CODE: MOVE (SF)-MOD-INTR-PREST
            //           TO PCO-MOD-INTR-PREST
            ws.getParamComp().setPcoModIntrPrest(ws.getLccvpco1().getDati().getWpcoModIntrPrest());
        }
        // COB_CODE: IF (SF)-GG-MAX-REC-PROV-NULL = HIGH-VALUES
        //              TO PCO-GG-MAX-REC-PROV-NULL
        //           ELSE
        //              TO PCO-GG-MAX-REC-PROV
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoGgMaxRecProv().getWpcoGgMaxRecProvNullFormatted())) {
            // COB_CODE: MOVE (SF)-GG-MAX-REC-PROV-NULL
            //           TO PCO-GG-MAX-REC-PROV-NULL
            ws.getParamComp().getPcoGgMaxRecProv().setPcoGgMaxRecProvNull(ws.getLccvpco1().getDati().getWpcoGgMaxRecProv().getWpcoGgMaxRecProvNull());
        }
        else {
            // COB_CODE: MOVE (SF)-GG-MAX-REC-PROV
            //           TO PCO-GG-MAX-REC-PROV
            ws.getParamComp().getPcoGgMaxRecProv().setPcoGgMaxRecProv(ws.getLccvpco1().getDati().getWpcoGgMaxRecProv().getWpcoGgMaxRecProv());
        }
        // COB_CODE: IF (SF)-DT-ULTGZ-TRCH-E-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTGZ-TRCH-E-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltgzTrchEIn().getWpcoDtUltgzTrchEInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTGZ-TRCH-E-IN-NULL
            //           TO PCO-DT-ULTGZ-TRCH-E-IN-NULL
            ws.getParamComp().getPcoDtUltgzTrchEIn().setPcoDtUltgzTrchEInNull(ws.getLccvpco1().getDati().getWpcoDtUltgzTrchEIn().getWpcoDtUltgzTrchEInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltgzTrchEIn().getWpcoDtUltgzTrchEIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULTGZ-TRCH-E-IN = ZERO
            //              TO PCO-DT-ULTGZ-TRCH-E-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULTGZ-TRCH-E-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTGZ-TRCH-E-IN-NULL
            ws.getParamComp().getPcoDtUltgzTrchEIn().setPcoDtUltgzTrchEInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltgzTrchEIn.Len.PCO_DT_ULTGZ_TRCH_E_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTGZ-TRCH-E-IN
            //           TO PCO-DT-ULTGZ-TRCH-E-IN
            ws.getParamComp().getPcoDtUltgzTrchEIn().setPcoDtUltgzTrchEIn(ws.getLccvpco1().getDati().getWpcoDtUltgzTrchEIn().getWpcoDtUltgzTrchEIn());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-SNDEN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-SNDEN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollSnden().getWpcoDtUltBollSndenNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-SNDEN-NULL
            //           TO PCO-DT-ULT-BOLL-SNDEN-NULL
            ws.getParamComp().getPcoDtUltBollSnden().setPcoDtUltBollSndenNull(ws.getLccvpco1().getDati().getWpcoDtUltBollSnden().getWpcoDtUltBollSndenNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollSnden().getWpcoDtUltBollSnden() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-SNDEN = ZERO
            //              TO PCO-DT-ULT-BOLL-SNDEN-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-SNDEN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-SNDEN-NULL
            ws.getParamComp().getPcoDtUltBollSnden().setPcoDtUltBollSndenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollSnden.Len.PCO_DT_ULT_BOLL_SNDEN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-SNDEN
            //           TO PCO-DT-ULT-BOLL-SNDEN
            ws.getParamComp().getPcoDtUltBollSnden().setPcoDtUltBollSnden(ws.getLccvpco1().getDati().getWpcoDtUltBollSnden().getWpcoDtUltBollSnden());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-SNDNLQ-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-SNDNLQ-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollSndnlq().getWpcoDtUltBollSndnlqNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-SNDNLQ-NULL
            //           TO PCO-DT-ULT-BOLL-SNDNLQ-NULL
            ws.getParamComp().getPcoDtUltBollSndnlq().setPcoDtUltBollSndnlqNull(ws.getLccvpco1().getDati().getWpcoDtUltBollSndnlq().getWpcoDtUltBollSndnlqNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollSndnlq().getWpcoDtUltBollSndnlq() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-SNDNLQ = ZERO
            //              TO PCO-DT-ULT-BOLL-SNDNLQ-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-SNDNLQ
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-SNDNLQ-NULL
            ws.getParamComp().getPcoDtUltBollSndnlq().setPcoDtUltBollSndnlqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollSndnlq.Len.PCO_DT_ULT_BOLL_SNDNLQ_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-SNDNLQ
            //           TO PCO-DT-ULT-BOLL-SNDNLQ
            ws.getParamComp().getPcoDtUltBollSndnlq().setPcoDtUltBollSndnlq(ws.getLccvpco1().getDati().getWpcoDtUltBollSndnlq().getWpcoDtUltBollSndnlq());
        }
        // COB_CODE: IF (SF)-DT-ULTSC-ELAB-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTSC-ELAB-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltscElabIn().getWpcoDtUltscElabInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTSC-ELAB-IN-NULL
            //           TO PCO-DT-ULTSC-ELAB-IN-NULL
            ws.getParamComp().getPcoDtUltscElabIn().setPcoDtUltscElabInNull(ws.getLccvpco1().getDati().getWpcoDtUltscElabIn().getWpcoDtUltscElabInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltscElabIn().getWpcoDtUltscElabIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULTSC-ELAB-IN = ZERO
            //              TO PCO-DT-ULTSC-ELAB-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULTSC-ELAB-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTSC-ELAB-IN-NULL
            ws.getParamComp().getPcoDtUltscElabIn().setPcoDtUltscElabInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltscElabIn.Len.PCO_DT_ULTSC_ELAB_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTSC-ELAB-IN
            //           TO PCO-DT-ULTSC-ELAB-IN
            ws.getParamComp().getPcoDtUltscElabIn().setPcoDtUltscElabIn(ws.getLccvpco1().getDati().getWpcoDtUltscElabIn().getWpcoDtUltscElabIn());
        }
        // COB_CODE: IF (SF)-DT-ULTSC-OPZ-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTSC-OPZ-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltscOpzIn().getWpcoDtUltscOpzInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTSC-OPZ-IN-NULL
            //           TO PCO-DT-ULTSC-OPZ-IN-NULL
            ws.getParamComp().getPcoDtUltscOpzIn().setPcoDtUltscOpzInNull(ws.getLccvpco1().getDati().getWpcoDtUltscOpzIn().getWpcoDtUltscOpzInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltscOpzIn().getWpcoDtUltscOpzIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULTSC-OPZ-IN = ZERO
            //              TO PCO-DT-ULTSC-OPZ-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULTSC-OPZ-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTSC-OPZ-IN-NULL
            ws.getParamComp().getPcoDtUltscOpzIn().setPcoDtUltscOpzInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltscOpzIn.Len.PCO_DT_ULTSC_OPZ_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTSC-OPZ-IN
            //           TO PCO-DT-ULTSC-OPZ-IN
            ws.getParamComp().getPcoDtUltscOpzIn().setPcoDtUltscOpzIn(ws.getLccvpco1().getDati().getWpcoDtUltscOpzIn().getWpcoDtUltscOpzIn());
        }
        // COB_CODE: IF (SF)-DT-ULTC-BNSRIC-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-BNSRIC-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcBnsricIn().getWpcoDtUltcBnsricInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-BNSRIC-IN-NULL
            //           TO PCO-DT-ULTC-BNSRIC-IN-NULL
            ws.getParamComp().getPcoDtUltcBnsricIn().setPcoDtUltcBnsricInNull(ws.getLccvpco1().getDati().getWpcoDtUltcBnsricIn().getWpcoDtUltcBnsricInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcBnsricIn().getWpcoDtUltcBnsricIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-BNSRIC-IN = ZERO
            //              TO PCO-DT-ULTC-BNSRIC-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-BNSRIC-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-BNSRIC-IN-NULL
            ws.getParamComp().getPcoDtUltcBnsricIn().setPcoDtUltcBnsricInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcBnsricIn.Len.PCO_DT_ULTC_BNSRIC_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-BNSRIC-IN
            //           TO PCO-DT-ULTC-BNSRIC-IN
            ws.getParamComp().getPcoDtUltcBnsricIn().setPcoDtUltcBnsricIn(ws.getLccvpco1().getDati().getWpcoDtUltcBnsricIn().getWpcoDtUltcBnsricIn());
        }
        // COB_CODE: IF (SF)-DT-ULTC-BNSFDT-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-BNSFDT-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtIn().getWpcoDtUltcBnsfdtInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-BNSFDT-IN-NULL
            //           TO PCO-DT-ULTC-BNSFDT-IN-NULL
            ws.getParamComp().getPcoDtUltcBnsfdtIn().setPcoDtUltcBnsfdtInNull(ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtIn().getWpcoDtUltcBnsfdtInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtIn().getWpcoDtUltcBnsfdtIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-BNSFDT-IN = ZERO
            //              TO PCO-DT-ULTC-BNSFDT-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-BNSFDT-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-BNSFDT-IN-NULL
            ws.getParamComp().getPcoDtUltcBnsfdtIn().setPcoDtUltcBnsfdtInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcBnsfdtIn.Len.PCO_DT_ULTC_BNSFDT_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-BNSFDT-IN
            //           TO PCO-DT-ULTC-BNSFDT-IN
            ws.getParamComp().getPcoDtUltcBnsfdtIn().setPcoDtUltcBnsfdtIn(ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtIn().getWpcoDtUltcBnsfdtIn());
        }
        // COB_CODE: IF (SF)-DT-ULT-RINN-GARAC-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-RINN-GARAC-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltRinnGarac().getWpcoDtUltRinnGaracNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-RINN-GARAC-NULL
            //           TO PCO-DT-ULT-RINN-GARAC-NULL
            ws.getParamComp().getPcoDtUltRinnGarac().setPcoDtUltRinnGaracNull(ws.getLccvpco1().getDati().getWpcoDtUltRinnGarac().getWpcoDtUltRinnGaracNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltRinnGarac().getWpcoDtUltRinnGarac() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-RINN-GARAC = ZERO
            //              TO PCO-DT-ULT-RINN-GARAC-NULL
            //           ELSE
            //            TO PCO-DT-ULT-RINN-GARAC
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-RINN-GARAC-NULL
            ws.getParamComp().getPcoDtUltRinnGarac().setPcoDtUltRinnGaracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRinnGarac.Len.PCO_DT_ULT_RINN_GARAC_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-RINN-GARAC
            //           TO PCO-DT-ULT-RINN-GARAC
            ws.getParamComp().getPcoDtUltRinnGarac().setPcoDtUltRinnGarac(ws.getLccvpco1().getDati().getWpcoDtUltRinnGarac().getWpcoDtUltRinnGarac());
        }
        // COB_CODE: IF (SF)-DT-ULTGZ-CED-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTGZ-CED-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltgzCed().getWpcoDtUltgzCedNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTGZ-CED-NULL
            //           TO PCO-DT-ULTGZ-CED-NULL
            ws.getParamComp().getPcoDtUltgzCed().setPcoDtUltgzCedNull(ws.getLccvpco1().getDati().getWpcoDtUltgzCed().getWpcoDtUltgzCedNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltgzCed().getWpcoDtUltgzCed() == 0) {
            // COB_CODE: IF (SF)-DT-ULTGZ-CED = ZERO
            //              TO PCO-DT-ULTGZ-CED-NULL
            //           ELSE
            //            TO PCO-DT-ULTGZ-CED
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTGZ-CED-NULL
            ws.getParamComp().getPcoDtUltgzCed().setPcoDtUltgzCedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltgzCed.Len.PCO_DT_ULTGZ_CED_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTGZ-CED
            //           TO PCO-DT-ULTGZ-CED
            ws.getParamComp().getPcoDtUltgzCed().setPcoDtUltgzCed(ws.getLccvpco1().getDati().getWpcoDtUltgzCed().getWpcoDtUltgzCed());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-PRLCOS-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-PRLCOS-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabPrlcos().getWpcoDtUltElabPrlcosNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-PRLCOS-NULL
            //           TO PCO-DT-ULT-ELAB-PRLCOS-NULL
            ws.getParamComp().getPcoDtUltElabPrlcos().setPcoDtUltElabPrlcosNull(ws.getLccvpco1().getDati().getWpcoDtUltElabPrlcos().getWpcoDtUltElabPrlcosNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabPrlcos().getWpcoDtUltElabPrlcos() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-PRLCOS = ZERO
            //              TO PCO-DT-ULT-ELAB-PRLCOS-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-PRLCOS
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-PRLCOS-NULL
            ws.getParamComp().getPcoDtUltElabPrlcos().setPcoDtUltElabPrlcosNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabPrlcos.Len.PCO_DT_ULT_ELAB_PRLCOS_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-PRLCOS
            //           TO PCO-DT-ULT-ELAB-PRLCOS
            ws.getParamComp().getPcoDtUltElabPrlcos().setPcoDtUltElabPrlcos(ws.getLccvpco1().getDati().getWpcoDtUltElabPrlcos().getWpcoDtUltElabPrlcos());
        }
        // COB_CODE: IF (SF)-DT-ULT-RINN-COLL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-RINN-COLL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltRinnColl().getWpcoDtUltRinnCollNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-RINN-COLL-NULL
            //           TO PCO-DT-ULT-RINN-COLL-NULL
            ws.getParamComp().getPcoDtUltRinnColl().setPcoDtUltRinnCollNull(ws.getLccvpco1().getDati().getWpcoDtUltRinnColl().getWpcoDtUltRinnCollNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltRinnColl().getWpcoDtUltRinnColl() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-RINN-COLL = ZERO
            //              TO PCO-DT-ULT-RINN-COLL-NULL
            //           ELSE
            //            TO PCO-DT-ULT-RINN-COLL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-RINN-COLL-NULL
            ws.getParamComp().getPcoDtUltRinnColl().setPcoDtUltRinnCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRinnColl.Len.PCO_DT_ULT_RINN_COLL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-RINN-COLL
            //           TO PCO-DT-ULT-RINN-COLL
            ws.getParamComp().getPcoDtUltRinnColl().setPcoDtUltRinnColl(ws.getLccvpco1().getDati().getWpcoDtUltRinnColl().getWpcoDtUltRinnColl());
        }
        // COB_CODE: IF (SF)-FL-RVC-PERF-NULL = HIGH-VALUES
        //              TO PCO-FL-RVC-PERF-NULL
        //           ELSE
        //              TO PCO-FL-RVC-PERF
        //           END-IF
        if (Conditions.eq(ws.getLccvpco1().getDati().getWpcoFlRvcPerf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-RVC-PERF-NULL
            //           TO PCO-FL-RVC-PERF-NULL
            ws.getParamComp().setPcoFlRvcPerf(ws.getLccvpco1().getDati().getWpcoFlRvcPerf());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-RVC-PERF
            //           TO PCO-FL-RVC-PERF
            ws.getParamComp().setPcoFlRvcPerf(ws.getLccvpco1().getDati().getWpcoFlRvcPerf());
        }
        // COB_CODE: IF (SF)-FL-RCS-POLI-NOPERF-NULL = HIGH-VALUES
        //              TO PCO-FL-RCS-POLI-NOPERF-NULL
        //           ELSE
        //              TO PCO-FL-RCS-POLI-NOPERF
        //           END-IF
        if (Conditions.eq(ws.getLccvpco1().getDati().getWpcoFlRcsPoliNoperf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-RCS-POLI-NOPERF-NULL
            //           TO PCO-FL-RCS-POLI-NOPERF-NULL
            ws.getParamComp().setPcoFlRcsPoliNoperf(ws.getLccvpco1().getDati().getWpcoFlRcsPoliNoperf());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-RCS-POLI-NOPERF
            //           TO PCO-FL-RCS-POLI-NOPERF
            ws.getParamComp().setPcoFlRcsPoliNoperf(ws.getLccvpco1().getDati().getWpcoFlRcsPoliNoperf());
        }
        // COB_CODE: IF (SF)-FL-GEST-PLUSV-NULL = HIGH-VALUES
        //              TO PCO-FL-GEST-PLUSV-NULL
        //           ELSE
        //              TO PCO-FL-GEST-PLUSV
        //           END-IF
        if (Conditions.eq(ws.getLccvpco1().getDati().getWpcoFlGestPlusv(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-GEST-PLUSV-NULL
            //           TO PCO-FL-GEST-PLUSV-NULL
            ws.getParamComp().setPcoFlGestPlusv(ws.getLccvpco1().getDati().getWpcoFlGestPlusv());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-GEST-PLUSV
            //           TO PCO-FL-GEST-PLUSV
            ws.getParamComp().setPcoFlGestPlusv(ws.getLccvpco1().getDati().getWpcoFlGestPlusv());
        }
        // COB_CODE: IF (SF)-DT-ULT-RIVAL-CL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-RIVAL-CL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltRivalCl().getWpcoDtUltRivalClNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-RIVAL-CL-NULL
            //           TO PCO-DT-ULT-RIVAL-CL-NULL
            ws.getParamComp().getPcoDtUltRivalCl().setPcoDtUltRivalClNull(ws.getLccvpco1().getDati().getWpcoDtUltRivalCl().getWpcoDtUltRivalClNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltRivalCl().getWpcoDtUltRivalCl() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-RIVAL-CL = ZERO
            //              TO PCO-DT-ULT-RIVAL-CL-NULL
            //           ELSE
            //            TO PCO-DT-ULT-RIVAL-CL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-RIVAL-CL-NULL
            ws.getParamComp().getPcoDtUltRivalCl().setPcoDtUltRivalClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRivalCl.Len.PCO_DT_ULT_RIVAL_CL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-RIVAL-CL
            //           TO PCO-DT-ULT-RIVAL-CL
            ws.getParamComp().getPcoDtUltRivalCl().setPcoDtUltRivalCl(ws.getLccvpco1().getDati().getWpcoDtUltRivalCl().getWpcoDtUltRivalCl());
        }
        // COB_CODE: IF (SF)-DT-ULT-QTZO-CL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-QTZO-CL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltQtzoCl().getWpcoDtUltQtzoClNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-QTZO-CL-NULL
            //           TO PCO-DT-ULT-QTZO-CL-NULL
            ws.getParamComp().getPcoDtUltQtzoCl().setPcoDtUltQtzoClNull(ws.getLccvpco1().getDati().getWpcoDtUltQtzoCl().getWpcoDtUltQtzoClNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltQtzoCl().getWpcoDtUltQtzoCl() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-QTZO-CL = ZERO
            //              TO PCO-DT-ULT-QTZO-CL-NULL
            //           ELSE
            //            TO PCO-DT-ULT-QTZO-CL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-QTZO-CL-NULL
            ws.getParamComp().getPcoDtUltQtzoCl().setPcoDtUltQtzoClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltQtzoCl.Len.PCO_DT_ULT_QTZO_CL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-QTZO-CL
            //           TO PCO-DT-ULT-QTZO-CL
            ws.getParamComp().getPcoDtUltQtzoCl().setPcoDtUltQtzoCl(ws.getLccvpco1().getDati().getWpcoDtUltQtzoCl().getWpcoDtUltQtzoCl());
        }
        // COB_CODE: IF (SF)-DT-ULTC-BNSRIC-CL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-BNSRIC-CL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcBnsricCl().getWpcoDtUltcBnsricClNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-BNSRIC-CL-NULL
            //           TO PCO-DT-ULTC-BNSRIC-CL-NULL
            ws.getParamComp().getPcoDtUltcBnsricCl().setPcoDtUltcBnsricClNull(ws.getLccvpco1().getDati().getWpcoDtUltcBnsricCl().getWpcoDtUltcBnsricClNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcBnsricCl().getWpcoDtUltcBnsricCl() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-BNSRIC-CL = ZERO
            //              TO PCO-DT-ULTC-BNSRIC-CL-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-BNSRIC-CL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-BNSRIC-CL-NULL
            ws.getParamComp().getPcoDtUltcBnsricCl().setPcoDtUltcBnsricClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcBnsricCl.Len.PCO_DT_ULTC_BNSRIC_CL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-BNSRIC-CL
            //           TO PCO-DT-ULTC-BNSRIC-CL
            ws.getParamComp().getPcoDtUltcBnsricCl().setPcoDtUltcBnsricCl(ws.getLccvpco1().getDati().getWpcoDtUltcBnsricCl().getWpcoDtUltcBnsricCl());
        }
        // COB_CODE: IF (SF)-DT-ULTC-BNSFDT-CL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-BNSFDT-CL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtCl().getWpcoDtUltcBnsfdtClNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-BNSFDT-CL-NULL
            //           TO PCO-DT-ULTC-BNSFDT-CL-NULL
            ws.getParamComp().getPcoDtUltcBnsfdtCl().setPcoDtUltcBnsfdtClNull(ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtCl().getWpcoDtUltcBnsfdtClNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtCl().getWpcoDtUltcBnsfdtCl() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-BNSFDT-CL = ZERO
            //              TO PCO-DT-ULTC-BNSFDT-CL-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-BNSFDT-CL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-BNSFDT-CL-NULL
            ws.getParamComp().getPcoDtUltcBnsfdtCl().setPcoDtUltcBnsfdtClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcBnsfdtCl.Len.PCO_DT_ULTC_BNSFDT_CL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-BNSFDT-CL
            //           TO PCO-DT-ULTC-BNSFDT-CL
            ws.getParamComp().getPcoDtUltcBnsfdtCl().setPcoDtUltcBnsfdtCl(ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtCl().getWpcoDtUltcBnsfdtCl());
        }
        // COB_CODE: IF (SF)-DT-ULTC-IS-CL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-IS-CL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcIsCl().getWpcoDtUltcIsClNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-IS-CL-NULL
            //           TO PCO-DT-ULTC-IS-CL-NULL
            ws.getParamComp().getPcoDtUltcIsCl().setPcoDtUltcIsClNull(ws.getLccvpco1().getDati().getWpcoDtUltcIsCl().getWpcoDtUltcIsClNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcIsCl().getWpcoDtUltcIsCl() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-IS-CL = ZERO
            //              TO PCO-DT-ULTC-IS-CL-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-IS-CL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-IS-CL-NULL
            ws.getParamComp().getPcoDtUltcIsCl().setPcoDtUltcIsClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcIsCl.Len.PCO_DT_ULTC_IS_CL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-IS-CL
            //           TO PCO-DT-ULTC-IS-CL
            ws.getParamComp().getPcoDtUltcIsCl().setPcoDtUltcIsCl(ws.getLccvpco1().getDati().getWpcoDtUltcIsCl().getWpcoDtUltcIsCl());
        }
        // COB_CODE: IF (SF)-DT-ULTC-RB-CL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-RB-CL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcRbCl().getWpcoDtUltcRbClNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-RB-CL-NULL
            //           TO PCO-DT-ULTC-RB-CL-NULL
            ws.getParamComp().getPcoDtUltcRbCl().setPcoDtUltcRbClNull(ws.getLccvpco1().getDati().getWpcoDtUltcRbCl().getWpcoDtUltcRbClNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcRbCl().getWpcoDtUltcRbCl() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-RB-CL = ZERO
            //              TO PCO-DT-ULTC-RB-CL-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-RB-CL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-RB-CL-NULL
            ws.getParamComp().getPcoDtUltcRbCl().setPcoDtUltcRbClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcRbCl.Len.PCO_DT_ULTC_RB_CL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-RB-CL
            //           TO PCO-DT-ULTC-RB-CL
            ws.getParamComp().getPcoDtUltcRbCl().setPcoDtUltcRbCl(ws.getLccvpco1().getDati().getWpcoDtUltcRbCl().getWpcoDtUltcRbCl());
        }
        // COB_CODE: IF (SF)-DT-ULTGZ-TRCH-E-CL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTGZ-TRCH-E-CL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltgzTrchECl().getWpcoDtUltgzTrchEClNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTGZ-TRCH-E-CL-NULL
            //           TO PCO-DT-ULTGZ-TRCH-E-CL-NULL
            ws.getParamComp().getPcoDtUltgzTrchECl().setPcoDtUltgzTrchEClNull(ws.getLccvpco1().getDati().getWpcoDtUltgzTrchECl().getWpcoDtUltgzTrchEClNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltgzTrchECl().getWpcoDtUltgzTrchECl() == 0) {
            // COB_CODE: IF (SF)-DT-ULTGZ-TRCH-E-CL = ZERO
            //              TO PCO-DT-ULTGZ-TRCH-E-CL-NULL
            //           ELSE
            //            TO PCO-DT-ULTGZ-TRCH-E-CL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTGZ-TRCH-E-CL-NULL
            ws.getParamComp().getPcoDtUltgzTrchECl().setPcoDtUltgzTrchEClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltgzTrchECl.Len.PCO_DT_ULTGZ_TRCH_E_CL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTGZ-TRCH-E-CL
            //           TO PCO-DT-ULTGZ-TRCH-E-CL
            ws.getParamComp().getPcoDtUltgzTrchECl().setPcoDtUltgzTrchECl(ws.getLccvpco1().getDati().getWpcoDtUltgzTrchECl().getWpcoDtUltgzTrchECl());
        }
        // COB_CODE: IF (SF)-DT-ULTSC-ELAB-CL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTSC-ELAB-CL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltscElabCl().getWpcoDtUltscElabClNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTSC-ELAB-CL-NULL
            //           TO PCO-DT-ULTSC-ELAB-CL-NULL
            ws.getParamComp().getPcoDtUltscElabCl().setPcoDtUltscElabClNull(ws.getLccvpco1().getDati().getWpcoDtUltscElabCl().getWpcoDtUltscElabClNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltscElabCl().getWpcoDtUltscElabCl() == 0) {
            // COB_CODE: IF (SF)-DT-ULTSC-ELAB-CL = ZERO
            //              TO PCO-DT-ULTSC-ELAB-CL-NULL
            //           ELSE
            //            TO PCO-DT-ULTSC-ELAB-CL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTSC-ELAB-CL-NULL
            ws.getParamComp().getPcoDtUltscElabCl().setPcoDtUltscElabClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltscElabCl.Len.PCO_DT_ULTSC_ELAB_CL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTSC-ELAB-CL
            //           TO PCO-DT-ULTSC-ELAB-CL
            ws.getParamComp().getPcoDtUltscElabCl().setPcoDtUltscElabCl(ws.getLccvpco1().getDati().getWpcoDtUltscElabCl().getWpcoDtUltscElabCl());
        }
        // COB_CODE: IF (SF)-DT-ULTSC-OPZ-CL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTSC-OPZ-CL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltscOpzCl().getWpcoDtUltscOpzClNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTSC-OPZ-CL-NULL
            //           TO PCO-DT-ULTSC-OPZ-CL-NULL
            ws.getParamComp().getPcoDtUltscOpzCl().setPcoDtUltscOpzClNull(ws.getLccvpco1().getDati().getWpcoDtUltscOpzCl().getWpcoDtUltscOpzClNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltscOpzCl().getWpcoDtUltscOpzCl() == 0) {
            // COB_CODE: IF (SF)-DT-ULTSC-OPZ-CL = ZERO
            //              TO PCO-DT-ULTSC-OPZ-CL-NULL
            //           ELSE
            //            TO PCO-DT-ULTSC-OPZ-CL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTSC-OPZ-CL-NULL
            ws.getParamComp().getPcoDtUltscOpzCl().setPcoDtUltscOpzClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltscOpzCl.Len.PCO_DT_ULTSC_OPZ_CL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTSC-OPZ-CL
            //           TO PCO-DT-ULTSC-OPZ-CL
            ws.getParamComp().getPcoDtUltscOpzCl().setPcoDtUltscOpzCl(ws.getLccvpco1().getDati().getWpcoDtUltscOpzCl().getWpcoDtUltscOpzCl());
        }
        // COB_CODE: IF (SF)-STST-X-REGIONE-NULL = HIGH-VALUES
        //              TO PCO-STST-X-REGIONE-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoStstXRegione().getWpcoStstXRegioneNullFormatted())) {
            // COB_CODE: MOVE (SF)-STST-X-REGIONE-NULL
            //           TO PCO-STST-X-REGIONE-NULL
            ws.getParamComp().getPcoStstXRegione().setPcoStstXRegioneNull(ws.getLccvpco1().getDati().getWpcoStstXRegione().getWpcoStstXRegioneNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoStstXRegione().getWpcoStstXRegione() == 0) {
            // COB_CODE: IF (SF)-STST-X-REGIONE = ZERO
            //              TO PCO-STST-X-REGIONE-NULL
            //           ELSE
            //            TO PCO-STST-X-REGIONE
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-STST-X-REGIONE-NULL
            ws.getParamComp().getPcoStstXRegione().setPcoStstXRegioneNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoStstXRegione.Len.PCO_STST_X_REGIONE_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-STST-X-REGIONE
            //           TO PCO-STST-X-REGIONE
            ws.getParamComp().getPcoStstXRegione().setPcoStstXRegione(ws.getLccvpco1().getDati().getWpcoStstXRegione().getWpcoStstXRegione());
        }
        // COB_CODE: IF (SF)-DT-ULTGZ-CED-COLL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTGZ-CED-COLL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltgzCedColl().getWpcoDtUltgzCedCollNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTGZ-CED-COLL-NULL
            //           TO PCO-DT-ULTGZ-CED-COLL-NULL
            ws.getParamComp().getPcoDtUltgzCedColl().setPcoDtUltgzCedCollNull(ws.getLccvpco1().getDati().getWpcoDtUltgzCedColl().getWpcoDtUltgzCedCollNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltgzCedColl().getWpcoDtUltgzCedColl() == 0) {
            // COB_CODE: IF (SF)-DT-ULTGZ-CED-COLL = ZERO
            //              TO PCO-DT-ULTGZ-CED-COLL-NULL
            //           ELSE
            //            TO PCO-DT-ULTGZ-CED-COLL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTGZ-CED-COLL-NULL
            ws.getParamComp().getPcoDtUltgzCedColl().setPcoDtUltgzCedCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltgzCedColl.Len.PCO_DT_ULTGZ_CED_COLL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTGZ-CED-COLL
            //           TO PCO-DT-ULTGZ-CED-COLL
            ws.getParamComp().getPcoDtUltgzCedColl().setPcoDtUltgzCedColl(ws.getLccvpco1().getDati().getWpcoDtUltgzCedColl().getWpcoDtUltgzCedColl());
        }
        // COB_CODE: MOVE (SF)-TP-MOD-RIVAL
        //              TO PCO-TP-MOD-RIVAL
        ws.getParamComp().setPcoTpModRival(ws.getLccvpco1().getDati().getWpcoTpModRival());
        // COB_CODE: IF (SF)-NUM-MM-CALC-MORA-NULL = HIGH-VALUES
        //              TO PCO-NUM-MM-CALC-MORA-NULL
        //           ELSE
        //              TO PCO-NUM-MM-CALC-MORA
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoNumMmCalcMora().getWpcoNumMmCalcMoraNullFormatted())) {
            // COB_CODE: MOVE (SF)-NUM-MM-CALC-MORA-NULL
            //           TO PCO-NUM-MM-CALC-MORA-NULL
            ws.getParamComp().getPcoNumMmCalcMora().setPcoNumMmCalcMoraNull(ws.getLccvpco1().getDati().getWpcoNumMmCalcMora().getWpcoNumMmCalcMoraNull());
        }
        else {
            // COB_CODE: MOVE (SF)-NUM-MM-CALC-MORA
            //           TO PCO-NUM-MM-CALC-MORA
            ws.getParamComp().getPcoNumMmCalcMora().setPcoNumMmCalcMora(ws.getLccvpco1().getDati().getWpcoNumMmCalcMora().getWpcoNumMmCalcMora());
        }
        // COB_CODE: IF (SF)-DT-ULT-EC-RIV-COLL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-EC-RIV-COLL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEcRivColl().getWpcoDtUltEcRivCollNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-RIV-COLL-NULL
            //           TO PCO-DT-ULT-EC-RIV-COLL-NULL
            ws.getParamComp().getPcoDtUltEcRivColl().setPcoDtUltEcRivCollNull(ws.getLccvpco1().getDati().getWpcoDtUltEcRivColl().getWpcoDtUltEcRivCollNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEcRivColl().getWpcoDtUltEcRivColl() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-EC-RIV-COLL = ZERO
            //              TO PCO-DT-ULT-EC-RIV-COLL-NULL
            //           ELSE
            //            TO PCO-DT-ULT-EC-RIV-COLL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-EC-RIV-COLL-NULL
            ws.getParamComp().getPcoDtUltEcRivColl().setPcoDtUltEcRivCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcRivColl.Len.PCO_DT_ULT_EC_RIV_COLL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-RIV-COLL
            //           TO PCO-DT-ULT-EC-RIV-COLL
            ws.getParamComp().getPcoDtUltEcRivColl().setPcoDtUltEcRivColl(ws.getLccvpco1().getDati().getWpcoDtUltEcRivColl().getWpcoDtUltEcRivColl());
        }
        // COB_CODE: IF (SF)-DT-ULT-EC-RIV-IND-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-EC-RIV-IND-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEcRivInd().getWpcoDtUltEcRivIndNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-RIV-IND-NULL
            //           TO PCO-DT-ULT-EC-RIV-IND-NULL
            ws.getParamComp().getPcoDtUltEcRivInd().setPcoDtUltEcRivIndNull(ws.getLccvpco1().getDati().getWpcoDtUltEcRivInd().getWpcoDtUltEcRivIndNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEcRivInd().getWpcoDtUltEcRivInd() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-EC-RIV-IND = ZERO
            //              TO PCO-DT-ULT-EC-RIV-IND-NULL
            //           ELSE
            //            TO PCO-DT-ULT-EC-RIV-IND
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-EC-RIV-IND-NULL
            ws.getParamComp().getPcoDtUltEcRivInd().setPcoDtUltEcRivIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcRivInd.Len.PCO_DT_ULT_EC_RIV_IND_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-RIV-IND
            //           TO PCO-DT-ULT-EC-RIV-IND
            ws.getParamComp().getPcoDtUltEcRivInd().setPcoDtUltEcRivInd(ws.getLccvpco1().getDati().getWpcoDtUltEcRivInd().getWpcoDtUltEcRivInd());
        }
        // COB_CODE: IF (SF)-DT-ULT-EC-IL-COLL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-EC-IL-COLL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEcIlColl().getWpcoDtUltEcIlCollNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-IL-COLL-NULL
            //           TO PCO-DT-ULT-EC-IL-COLL-NULL
            ws.getParamComp().getPcoDtUltEcIlColl().setPcoDtUltEcIlCollNull(ws.getLccvpco1().getDati().getWpcoDtUltEcIlColl().getWpcoDtUltEcIlCollNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEcIlColl().getWpcoDtUltEcIlColl() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-EC-IL-COLL = ZERO
            //              TO PCO-DT-ULT-EC-IL-COLL-NULL
            //           ELSE
            //            TO PCO-DT-ULT-EC-IL-COLL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-EC-IL-COLL-NULL
            ws.getParamComp().getPcoDtUltEcIlColl().setPcoDtUltEcIlCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcIlColl.Len.PCO_DT_ULT_EC_IL_COLL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-IL-COLL
            //           TO PCO-DT-ULT-EC-IL-COLL
            ws.getParamComp().getPcoDtUltEcIlColl().setPcoDtUltEcIlColl(ws.getLccvpco1().getDati().getWpcoDtUltEcIlColl().getWpcoDtUltEcIlColl());
        }
        // COB_CODE: IF (SF)-DT-ULT-EC-IL-IND-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-EC-IL-IND-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEcIlInd().getWpcoDtUltEcIlIndNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-IL-IND-NULL
            //           TO PCO-DT-ULT-EC-IL-IND-NULL
            ws.getParamComp().getPcoDtUltEcIlInd().setPcoDtUltEcIlIndNull(ws.getLccvpco1().getDati().getWpcoDtUltEcIlInd().getWpcoDtUltEcIlIndNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEcIlInd().getWpcoDtUltEcIlInd() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-EC-IL-IND = ZERO
            //              TO PCO-DT-ULT-EC-IL-IND-NULL
            //           ELSE
            //            TO PCO-DT-ULT-EC-IL-IND
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-EC-IL-IND-NULL
            ws.getParamComp().getPcoDtUltEcIlInd().setPcoDtUltEcIlIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcIlInd.Len.PCO_DT_ULT_EC_IL_IND_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-IL-IND
            //           TO PCO-DT-ULT-EC-IL-IND
            ws.getParamComp().getPcoDtUltEcIlInd().setPcoDtUltEcIlInd(ws.getLccvpco1().getDati().getWpcoDtUltEcIlInd().getWpcoDtUltEcIlInd());
        }
        // COB_CODE: IF (SF)-DT-ULT-EC-UL-COLL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-EC-UL-COLL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEcUlColl().getWpcoDtUltEcUlCollNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-UL-COLL-NULL
            //           TO PCO-DT-ULT-EC-UL-COLL-NULL
            ws.getParamComp().getPcoDtUltEcUlColl().setPcoDtUltEcUlCollNull(ws.getLccvpco1().getDati().getWpcoDtUltEcUlColl().getWpcoDtUltEcUlCollNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEcUlColl().getWpcoDtUltEcUlColl() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-EC-UL-COLL = ZERO
            //              TO PCO-DT-ULT-EC-UL-COLL-NULL
            //           ELSE
            //            TO PCO-DT-ULT-EC-UL-COLL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-EC-UL-COLL-NULL
            ws.getParamComp().getPcoDtUltEcUlColl().setPcoDtUltEcUlCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcUlColl.Len.PCO_DT_ULT_EC_UL_COLL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-UL-COLL
            //           TO PCO-DT-ULT-EC-UL-COLL
            ws.getParamComp().getPcoDtUltEcUlColl().setPcoDtUltEcUlColl(ws.getLccvpco1().getDati().getWpcoDtUltEcUlColl().getWpcoDtUltEcUlColl());
        }
        // COB_CODE: IF (SF)-DT-ULT-EC-UL-IND-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-EC-UL-IND-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEcUlInd().getWpcoDtUltEcUlIndNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-UL-IND-NULL
            //           TO PCO-DT-ULT-EC-UL-IND-NULL
            ws.getParamComp().getPcoDtUltEcUlInd().setPcoDtUltEcUlIndNull(ws.getLccvpco1().getDati().getWpcoDtUltEcUlInd().getWpcoDtUltEcUlIndNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEcUlInd().getWpcoDtUltEcUlInd() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-EC-UL-IND = ZERO
            //              TO PCO-DT-ULT-EC-UL-IND-NULL
            //           ELSE
            //            TO PCO-DT-ULT-EC-UL-IND
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-EC-UL-IND-NULL
            ws.getParamComp().getPcoDtUltEcUlInd().setPcoDtUltEcUlIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcUlInd.Len.PCO_DT_ULT_EC_UL_IND_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-UL-IND
            //           TO PCO-DT-ULT-EC-UL-IND
            ws.getParamComp().getPcoDtUltEcUlInd().setPcoDtUltEcUlInd(ws.getLccvpco1().getDati().getWpcoDtUltEcUlInd().getWpcoDtUltEcUlInd());
        }
        // COB_CODE: IF (SF)-AA-UTI-NULL = HIGH-VALUES
        //              TO PCO-AA-UTI-NULL
        //           ELSE
        //              TO PCO-AA-UTI
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoAaUti().getWpcoAaUtiNullFormatted())) {
            // COB_CODE: MOVE (SF)-AA-UTI-NULL
            //           TO PCO-AA-UTI-NULL
            ws.getParamComp().getPcoAaUti().setPcoAaUtiNull(ws.getLccvpco1().getDati().getWpcoAaUti().getWpcoAaUtiNull());
        }
        else {
            // COB_CODE: MOVE (SF)-AA-UTI
            //           TO PCO-AA-UTI
            ws.getParamComp().getPcoAaUti().setPcoAaUti(ws.getLccvpco1().getDati().getWpcoAaUti().getWpcoAaUti());
        }
        // COB_CODE: MOVE (SF)-CALC-RSH-COMUN
        //              TO PCO-CALC-RSH-COMUN
        ws.getParamComp().setPcoCalcRshComun(ws.getLccvpco1().getDati().getWpcoCalcRshComun());
        // COB_CODE: MOVE (SF)-FL-LIV-DEBUG
        //              TO PCO-FL-LIV-DEBUG
        ws.getParamComp().setPcoFlLivDebug(ws.getLccvpco1().getDati().getWpcoFlLivDebug());
        // COB_CODE: IF (SF)-DT-ULT-BOLL-PERF-C-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-PERF-C-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollPerfC().getWpcoDtUltBollPerfCNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-PERF-C-NULL
            //           TO PCO-DT-ULT-BOLL-PERF-C-NULL
            ws.getParamComp().getPcoDtUltBollPerfC().setPcoDtUltBollPerfCNull(ws.getLccvpco1().getDati().getWpcoDtUltBollPerfC().getWpcoDtUltBollPerfCNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollPerfC().getWpcoDtUltBollPerfC() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-PERF-C = ZERO
            //              TO PCO-DT-ULT-BOLL-PERF-C-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-PERF-C
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-PERF-C-NULL
            ws.getParamComp().getPcoDtUltBollPerfC().setPcoDtUltBollPerfCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollPerfC.Len.PCO_DT_ULT_BOLL_PERF_C_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-PERF-C
            //           TO PCO-DT-ULT-BOLL-PERF-C
            ws.getParamComp().getPcoDtUltBollPerfC().setPcoDtUltBollPerfC(ws.getLccvpco1().getDati().getWpcoDtUltBollPerfC().getWpcoDtUltBollPerfC());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-RSP-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-RSP-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollRspIn().getWpcoDtUltBollRspInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RSP-IN-NULL
            //           TO PCO-DT-ULT-BOLL-RSP-IN-NULL
            ws.getParamComp().getPcoDtUltBollRspIn().setPcoDtUltBollRspInNull(ws.getLccvpco1().getDati().getWpcoDtUltBollRspIn().getWpcoDtUltBollRspInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollRspIn().getWpcoDtUltBollRspIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-RSP-IN = ZERO
            //              TO PCO-DT-ULT-BOLL-RSP-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-RSP-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-RSP-IN-NULL
            ws.getParamComp().getPcoDtUltBollRspIn().setPcoDtUltBollRspInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRspIn.Len.PCO_DT_ULT_BOLL_RSP_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RSP-IN
            //           TO PCO-DT-ULT-BOLL-RSP-IN
            ws.getParamComp().getPcoDtUltBollRspIn().setPcoDtUltBollRspIn(ws.getLccvpco1().getDati().getWpcoDtUltBollRspIn().getWpcoDtUltBollRspIn());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-RSP-CL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-RSP-CL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollRspCl().getWpcoDtUltBollRspClNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RSP-CL-NULL
            //           TO PCO-DT-ULT-BOLL-RSP-CL-NULL
            ws.getParamComp().getPcoDtUltBollRspCl().setPcoDtUltBollRspClNull(ws.getLccvpco1().getDati().getWpcoDtUltBollRspCl().getWpcoDtUltBollRspClNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollRspCl().getWpcoDtUltBollRspCl() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-RSP-CL = ZERO
            //              TO PCO-DT-ULT-BOLL-RSP-CL-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-RSP-CL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-RSP-CL-NULL
            ws.getParamComp().getPcoDtUltBollRspCl().setPcoDtUltBollRspClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRspCl.Len.PCO_DT_ULT_BOLL_RSP_CL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RSP-CL
            //           TO PCO-DT-ULT-BOLL-RSP-CL
            ws.getParamComp().getPcoDtUltBollRspCl().setPcoDtUltBollRspCl(ws.getLccvpco1().getDati().getWpcoDtUltBollRspCl().getWpcoDtUltBollRspCl());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-EMES-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-EMES-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollEmesI().getWpcoDtUltBollEmesINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-EMES-I-NULL
            //           TO PCO-DT-ULT-BOLL-EMES-I-NULL
            ws.getParamComp().getPcoDtUltBollEmesI().setPcoDtUltBollEmesINull(ws.getLccvpco1().getDati().getWpcoDtUltBollEmesI().getWpcoDtUltBollEmesINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollEmesI().getWpcoDtUltBollEmesI() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-EMES-I = ZERO
            //              TO PCO-DT-ULT-BOLL-EMES-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-EMES-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-EMES-I-NULL
            ws.getParamComp().getPcoDtUltBollEmesI().setPcoDtUltBollEmesINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollEmesI.Len.PCO_DT_ULT_BOLL_EMES_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-EMES-I
            //           TO PCO-DT-ULT-BOLL-EMES-I
            ws.getParamComp().getPcoDtUltBollEmesI().setPcoDtUltBollEmesI(ws.getLccvpco1().getDati().getWpcoDtUltBollEmesI().getWpcoDtUltBollEmesI());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-STOR-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-STOR-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollStorI().getWpcoDtUltBollStorINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-STOR-I-NULL
            //           TO PCO-DT-ULT-BOLL-STOR-I-NULL
            ws.getParamComp().getPcoDtUltBollStorI().setPcoDtUltBollStorINull(ws.getLccvpco1().getDati().getWpcoDtUltBollStorI().getWpcoDtUltBollStorINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollStorI().getWpcoDtUltBollStorI() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-STOR-I = ZERO
            //              TO PCO-DT-ULT-BOLL-STOR-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-STOR-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-STOR-I-NULL
            ws.getParamComp().getPcoDtUltBollStorI().setPcoDtUltBollStorINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollStorI.Len.PCO_DT_ULT_BOLL_STOR_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-STOR-I
            //           TO PCO-DT-ULT-BOLL-STOR-I
            ws.getParamComp().getPcoDtUltBollStorI().setPcoDtUltBollStorI(ws.getLccvpco1().getDati().getWpcoDtUltBollStorI().getWpcoDtUltBollStorI());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-RIAT-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-RIAT-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollRiatI().getWpcoDtUltBollRiatINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RIAT-I-NULL
            //           TO PCO-DT-ULT-BOLL-RIAT-I-NULL
            ws.getParamComp().getPcoDtUltBollRiatI().setPcoDtUltBollRiatINull(ws.getLccvpco1().getDati().getWpcoDtUltBollRiatI().getWpcoDtUltBollRiatINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollRiatI().getWpcoDtUltBollRiatI() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-RIAT-I = ZERO
            //              TO PCO-DT-ULT-BOLL-RIAT-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-RIAT-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-RIAT-I-NULL
            ws.getParamComp().getPcoDtUltBollRiatI().setPcoDtUltBollRiatINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRiatI.Len.PCO_DT_ULT_BOLL_RIAT_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RIAT-I
            //           TO PCO-DT-ULT-BOLL-RIAT-I
            ws.getParamComp().getPcoDtUltBollRiatI().setPcoDtUltBollRiatI(ws.getLccvpco1().getDati().getWpcoDtUltBollRiatI().getWpcoDtUltBollRiatI());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-SD-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-SD-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollSdI().getWpcoDtUltBollSdINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-SD-I-NULL
            //           TO PCO-DT-ULT-BOLL-SD-I-NULL
            ws.getParamComp().getPcoDtUltBollSdI().setPcoDtUltBollSdINull(ws.getLccvpco1().getDati().getWpcoDtUltBollSdI().getWpcoDtUltBollSdINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollSdI().getWpcoDtUltBollSdI() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-SD-I = ZERO
            //              TO PCO-DT-ULT-BOLL-SD-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-SD-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-SD-I-NULL
            ws.getParamComp().getPcoDtUltBollSdI().setPcoDtUltBollSdINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollSdI.Len.PCO_DT_ULT_BOLL_SD_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-SD-I
            //           TO PCO-DT-ULT-BOLL-SD-I
            ws.getParamComp().getPcoDtUltBollSdI().setPcoDtUltBollSdI(ws.getLccvpco1().getDati().getWpcoDtUltBollSdI().getWpcoDtUltBollSdI());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-SDNL-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-SDNL-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollSdnlI().getWpcoDtUltBollSdnlINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-SDNL-I-NULL
            //           TO PCO-DT-ULT-BOLL-SDNL-I-NULL
            ws.getParamComp().getPcoDtUltBollSdnlI().setPcoDtUltBollSdnlINull(ws.getLccvpco1().getDati().getWpcoDtUltBollSdnlI().getWpcoDtUltBollSdnlINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollSdnlI().getWpcoDtUltBollSdnlI() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-SDNL-I = ZERO
            //              TO PCO-DT-ULT-BOLL-SDNL-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-SDNL-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-SDNL-I-NULL
            ws.getParamComp().getPcoDtUltBollSdnlI().setPcoDtUltBollSdnlINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollSdnlI.Len.PCO_DT_ULT_BOLL_SDNL_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-SDNL-I
            //           TO PCO-DT-ULT-BOLL-SDNL-I
            ws.getParamComp().getPcoDtUltBollSdnlI().setPcoDtUltBollSdnlI(ws.getLccvpco1().getDati().getWpcoDtUltBollSdnlI().getWpcoDtUltBollSdnlI());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-PERF-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-PERF-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollPerfI().getWpcoDtUltBollPerfINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-PERF-I-NULL
            //           TO PCO-DT-ULT-BOLL-PERF-I-NULL
            ws.getParamComp().getPcoDtUltBollPerfI().setPcoDtUltBollPerfINull(ws.getLccvpco1().getDati().getWpcoDtUltBollPerfI().getWpcoDtUltBollPerfINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollPerfI().getWpcoDtUltBollPerfI() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-PERF-I = ZERO
            //              TO PCO-DT-ULT-BOLL-PERF-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-PERF-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-PERF-I-NULL
            ws.getParamComp().getPcoDtUltBollPerfI().setPcoDtUltBollPerfINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollPerfI.Len.PCO_DT_ULT_BOLL_PERF_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-PERF-I
            //           TO PCO-DT-ULT-BOLL-PERF-I
            ws.getParamComp().getPcoDtUltBollPerfI().setPcoDtUltBollPerfI(ws.getLccvpco1().getDati().getWpcoDtUltBollPerfI().getWpcoDtUltBollPerfI());
        }
        // COB_CODE: IF (SF)-DT-RICL-RIRIAS-COM-NULL = HIGH-VALUES
        //              TO PCO-DT-RICL-RIRIAS-COM-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtRiclRiriasCom().getWpcoDtRiclRiriasComNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-RICL-RIRIAS-COM-NULL
            //           TO PCO-DT-RICL-RIRIAS-COM-NULL
            ws.getParamComp().getPcoDtRiclRiriasCom().setPcoDtRiclRiriasComNull(ws.getLccvpco1().getDati().getWpcoDtRiclRiriasCom().getWpcoDtRiclRiriasComNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtRiclRiriasCom().getWpcoDtRiclRiriasCom() == 0) {
            // COB_CODE: IF (SF)-DT-RICL-RIRIAS-COM = ZERO
            //              TO PCO-DT-RICL-RIRIAS-COM-NULL
            //           ELSE
            //            TO PCO-DT-RICL-RIRIAS-COM
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-RICL-RIRIAS-COM-NULL
            ws.getParamComp().getPcoDtRiclRiriasCom().setPcoDtRiclRiriasComNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtRiclRiriasCom.Len.PCO_DT_RICL_RIRIAS_COM_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-RICL-RIRIAS-COM
            //           TO PCO-DT-RICL-RIRIAS-COM
            ws.getParamComp().getPcoDtRiclRiriasCom().setPcoDtRiclRiriasCom(ws.getLccvpco1().getDati().getWpcoDtRiclRiriasCom().getWpcoDtRiclRiriasCom());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-AT92-C-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-AT92-C-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabAt92C().getWpcoDtUltElabAt92CNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-AT92-C-NULL
            //           TO PCO-DT-ULT-ELAB-AT92-C-NULL
            ws.getParamComp().getPcoDtUltElabAt92C().setPcoDtUltElabAt92CNull(ws.getLccvpco1().getDati().getWpcoDtUltElabAt92C().getWpcoDtUltElabAt92CNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabAt92C().getWpcoDtUltElabAt92C() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-AT92-C = ZERO
            //              TO PCO-DT-ULT-ELAB-AT92-C-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-AT92-C
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-AT92-C-NULL
            ws.getParamComp().getPcoDtUltElabAt92C().setPcoDtUltElabAt92CNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabAt92C.Len.PCO_DT_ULT_ELAB_AT92_C_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-AT92-C
            //           TO PCO-DT-ULT-ELAB-AT92-C
            ws.getParamComp().getPcoDtUltElabAt92C().setPcoDtUltElabAt92C(ws.getLccvpco1().getDati().getWpcoDtUltElabAt92C().getWpcoDtUltElabAt92C());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-AT92-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-AT92-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabAt92I().getWpcoDtUltElabAt92INullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-AT92-I-NULL
            //           TO PCO-DT-ULT-ELAB-AT92-I-NULL
            ws.getParamComp().getPcoDtUltElabAt92I().setPcoDtUltElabAt92INull(ws.getLccvpco1().getDati().getWpcoDtUltElabAt92I().getWpcoDtUltElabAt92INull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabAt92I().getWpcoDtUltElabAt92I() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-AT92-I = ZERO
            //              TO PCO-DT-ULT-ELAB-AT92-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-AT92-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-AT92-I-NULL
            ws.getParamComp().getPcoDtUltElabAt92I().setPcoDtUltElabAt92INull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabAt92I.Len.PCO_DT_ULT_ELAB_AT92_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-AT92-I
            //           TO PCO-DT-ULT-ELAB-AT92-I
            ws.getParamComp().getPcoDtUltElabAt92I().setPcoDtUltElabAt92I(ws.getLccvpco1().getDati().getWpcoDtUltElabAt92I().getWpcoDtUltElabAt92I());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-AT93-C-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-AT93-C-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabAt93C().getWpcoDtUltElabAt93CNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-AT93-C-NULL
            //           TO PCO-DT-ULT-ELAB-AT93-C-NULL
            ws.getParamComp().getPcoDtUltElabAt93C().setPcoDtUltElabAt93CNull(ws.getLccvpco1().getDati().getWpcoDtUltElabAt93C().getWpcoDtUltElabAt93CNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabAt93C().getWpcoDtUltElabAt93C() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-AT93-C = ZERO
            //              TO PCO-DT-ULT-ELAB-AT93-C-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-AT93-C
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-AT93-C-NULL
            ws.getParamComp().getPcoDtUltElabAt93C().setPcoDtUltElabAt93CNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabAt93C.Len.PCO_DT_ULT_ELAB_AT93_C_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-AT93-C
            //           TO PCO-DT-ULT-ELAB-AT93-C
            ws.getParamComp().getPcoDtUltElabAt93C().setPcoDtUltElabAt93C(ws.getLccvpco1().getDati().getWpcoDtUltElabAt93C().getWpcoDtUltElabAt93C());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-AT93-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-AT93-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabAt93I().getWpcoDtUltElabAt93INullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-AT93-I-NULL
            //           TO PCO-DT-ULT-ELAB-AT93-I-NULL
            ws.getParamComp().getPcoDtUltElabAt93I().setPcoDtUltElabAt93INull(ws.getLccvpco1().getDati().getWpcoDtUltElabAt93I().getWpcoDtUltElabAt93INull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabAt93I().getWpcoDtUltElabAt93I() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-AT93-I = ZERO
            //              TO PCO-DT-ULT-ELAB-AT93-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-AT93-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-AT93-I-NULL
            ws.getParamComp().getPcoDtUltElabAt93I().setPcoDtUltElabAt93INull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabAt93I.Len.PCO_DT_ULT_ELAB_AT93_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-AT93-I
            //           TO PCO-DT-ULT-ELAB-AT93-I
            ws.getParamComp().getPcoDtUltElabAt93I().setPcoDtUltElabAt93I(ws.getLccvpco1().getDati().getWpcoDtUltElabAt93I().getWpcoDtUltElabAt93I());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-SPE-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-SPE-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabSpeIn().getWpcoDtUltElabSpeInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-SPE-IN-NULL
            //           TO PCO-DT-ULT-ELAB-SPE-IN-NULL
            ws.getParamComp().getPcoDtUltElabSpeIn().setPcoDtUltElabSpeInNull(ws.getLccvpco1().getDati().getWpcoDtUltElabSpeIn().getWpcoDtUltElabSpeInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabSpeIn().getWpcoDtUltElabSpeIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-SPE-IN = ZERO
            //              TO PCO-DT-ULT-ELAB-SPE-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-SPE-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-SPE-IN-NULL
            ws.getParamComp().getPcoDtUltElabSpeIn().setPcoDtUltElabSpeInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabSpeIn.Len.PCO_DT_ULT_ELAB_SPE_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-SPE-IN
            //           TO PCO-DT-ULT-ELAB-SPE-IN
            ws.getParamComp().getPcoDtUltElabSpeIn().setPcoDtUltElabSpeIn(ws.getLccvpco1().getDati().getWpcoDtUltElabSpeIn().getWpcoDtUltElabSpeIn());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-PR-CON-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-PR-CON-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabPrCon().getWpcoDtUltElabPrConNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-PR-CON-NULL
            //           TO PCO-DT-ULT-ELAB-PR-CON-NULL
            ws.getParamComp().getPcoDtUltElabPrCon().setPcoDtUltElabPrConNull(ws.getLccvpco1().getDati().getWpcoDtUltElabPrCon().getWpcoDtUltElabPrConNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabPrCon().getWpcoDtUltElabPrCon() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-PR-CON = ZERO
            //              TO PCO-DT-ULT-ELAB-PR-CON-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-PR-CON
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-PR-CON-NULL
            ws.getParamComp().getPcoDtUltElabPrCon().setPcoDtUltElabPrConNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabPrCon.Len.PCO_DT_ULT_ELAB_PR_CON_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-PR-CON
            //           TO PCO-DT-ULT-ELAB-PR-CON
            ws.getParamComp().getPcoDtUltElabPrCon().setPcoDtUltElabPrCon(ws.getLccvpco1().getDati().getWpcoDtUltElabPrCon().getWpcoDtUltElabPrCon());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-RP-CL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-RP-CL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollRpCl().getWpcoDtUltBollRpClNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RP-CL-NULL
            //           TO PCO-DT-ULT-BOLL-RP-CL-NULL
            ws.getParamComp().getPcoDtUltBollRpCl().setPcoDtUltBollRpClNull(ws.getLccvpco1().getDati().getWpcoDtUltBollRpCl().getWpcoDtUltBollRpClNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollRpCl().getWpcoDtUltBollRpCl() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-RP-CL = ZERO
            //              TO PCO-DT-ULT-BOLL-RP-CL-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-RP-CL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-RP-CL-NULL
            ws.getParamComp().getPcoDtUltBollRpCl().setPcoDtUltBollRpClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRpCl.Len.PCO_DT_ULT_BOLL_RP_CL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RP-CL
            //           TO PCO-DT-ULT-BOLL-RP-CL
            ws.getParamComp().getPcoDtUltBollRpCl().setPcoDtUltBollRpCl(ws.getLccvpco1().getDati().getWpcoDtUltBollRpCl().getWpcoDtUltBollRpCl());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-RP-IN-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-RP-IN-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollRpIn().getWpcoDtUltBollRpInNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RP-IN-NULL
            //           TO PCO-DT-ULT-BOLL-RP-IN-NULL
            ws.getParamComp().getPcoDtUltBollRpIn().setPcoDtUltBollRpInNull(ws.getLccvpco1().getDati().getWpcoDtUltBollRpIn().getWpcoDtUltBollRpInNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollRpIn().getWpcoDtUltBollRpIn() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-RP-IN = ZERO
            //              TO PCO-DT-ULT-BOLL-RP-IN-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-RP-IN
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-RP-IN-NULL
            ws.getParamComp().getPcoDtUltBollRpIn().setPcoDtUltBollRpInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRpIn.Len.PCO_DT_ULT_BOLL_RP_IN_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-RP-IN
            //           TO PCO-DT-ULT-BOLL-RP-IN
            ws.getParamComp().getPcoDtUltBollRpIn().setPcoDtUltBollRpIn(ws.getLccvpco1().getDati().getWpcoDtUltBollRpIn().getWpcoDtUltBollRpIn());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-PRE-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-PRE-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollPreI().getWpcoDtUltBollPreINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-PRE-I-NULL
            //           TO PCO-DT-ULT-BOLL-PRE-I-NULL
            ws.getParamComp().getPcoDtUltBollPreI().setPcoDtUltBollPreINull(ws.getLccvpco1().getDati().getWpcoDtUltBollPreI().getWpcoDtUltBollPreINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollPreI().getWpcoDtUltBollPreI() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-PRE-I = ZERO
            //              TO PCO-DT-ULT-BOLL-PRE-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-PRE-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-PRE-I-NULL
            ws.getParamComp().getPcoDtUltBollPreI().setPcoDtUltBollPreINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollPreI.Len.PCO_DT_ULT_BOLL_PRE_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-PRE-I
            //           TO PCO-DT-ULT-BOLL-PRE-I
            ws.getParamComp().getPcoDtUltBollPreI().setPcoDtUltBollPreI(ws.getLccvpco1().getDati().getWpcoDtUltBollPreI().getWpcoDtUltBollPreI());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-PRE-C-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-PRE-C-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollPreC().getWpcoDtUltBollPreCNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-PRE-C-NULL
            //           TO PCO-DT-ULT-BOLL-PRE-C-NULL
            ws.getParamComp().getPcoDtUltBollPreC().setPcoDtUltBollPreCNull(ws.getLccvpco1().getDati().getWpcoDtUltBollPreC().getWpcoDtUltBollPreCNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollPreC().getWpcoDtUltBollPreC() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-PRE-C = ZERO
            //              TO PCO-DT-ULT-BOLL-PRE-C-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-PRE-C
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-PRE-C-NULL
            ws.getParamComp().getPcoDtUltBollPreC().setPcoDtUltBollPreCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollPreC.Len.PCO_DT_ULT_BOLL_PRE_C_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-PRE-C
            //           TO PCO-DT-ULT-BOLL-PRE-C
            ws.getParamComp().getPcoDtUltBollPreC().setPcoDtUltBollPreC(ws.getLccvpco1().getDati().getWpcoDtUltBollPreC().getWpcoDtUltBollPreC());
        }
        // COB_CODE: IF (SF)-DT-ULTC-PILDI-MM-C-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-PILDI-MM-C-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmC().getWpcoDtUltcPildiMmCNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-PILDI-MM-C-NULL
            //           TO PCO-DT-ULTC-PILDI-MM-C-NULL
            ws.getParamComp().getPcoDtUltcPildiMmC().setPcoDtUltcPildiMmCNull(ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmC().getWpcoDtUltcPildiMmCNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmC().getWpcoDtUltcPildiMmC() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-PILDI-MM-C = ZERO
            //              TO PCO-DT-ULTC-PILDI-MM-C-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-PILDI-MM-C
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-PILDI-MM-C-NULL
            ws.getParamComp().getPcoDtUltcPildiMmC().setPcoDtUltcPildiMmCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcPildiMmC.Len.PCO_DT_ULTC_PILDI_MM_C_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-PILDI-MM-C
            //           TO PCO-DT-ULTC-PILDI-MM-C
            ws.getParamComp().getPcoDtUltcPildiMmC().setPcoDtUltcPildiMmC(ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmC().getWpcoDtUltcPildiMmC());
        }
        // COB_CODE: IF (SF)-DT-ULTC-PILDI-AA-C-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-PILDI-AA-C-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaC().getWpcoDtUltcPildiAaCNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-PILDI-AA-C-NULL
            //           TO PCO-DT-ULTC-PILDI-AA-C-NULL
            ws.getParamComp().getPcoDtUltcPildiAaC().setPcoDtUltcPildiAaCNull(ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaC().getWpcoDtUltcPildiAaCNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaC().getWpcoDtUltcPildiAaC() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-PILDI-AA-C = ZERO
            //              TO PCO-DT-ULTC-PILDI-AA-C-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-PILDI-AA-C
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-PILDI-AA-C-NULL
            ws.getParamComp().getPcoDtUltcPildiAaC().setPcoDtUltcPildiAaCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcPildiAaC.Len.PCO_DT_ULTC_PILDI_AA_C_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-PILDI-AA-C
            //           TO PCO-DT-ULTC-PILDI-AA-C
            ws.getParamComp().getPcoDtUltcPildiAaC().setPcoDtUltcPildiAaC(ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaC().getWpcoDtUltcPildiAaC());
        }
        // COB_CODE: IF (SF)-DT-ULTC-PILDI-MM-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-PILDI-MM-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmI().getWpcoDtUltcPildiMmINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-PILDI-MM-I-NULL
            //           TO PCO-DT-ULTC-PILDI-MM-I-NULL
            ws.getParamComp().getPcoDtUltcPildiMmI().setPcoDtUltcPildiMmINull(ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmI().getWpcoDtUltcPildiMmINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmI().getWpcoDtUltcPildiMmI() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-PILDI-MM-I = ZERO
            //              TO PCO-DT-ULTC-PILDI-MM-I-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-PILDI-MM-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-PILDI-MM-I-NULL
            ws.getParamComp().getPcoDtUltcPildiMmI().setPcoDtUltcPildiMmINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcPildiMmI.Len.PCO_DT_ULTC_PILDI_MM_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-PILDI-MM-I
            //           TO PCO-DT-ULTC-PILDI-MM-I
            ws.getParamComp().getPcoDtUltcPildiMmI().setPcoDtUltcPildiMmI(ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmI().getWpcoDtUltcPildiMmI());
        }
        // COB_CODE: IF (SF)-DT-ULTC-PILDI-TR-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-PILDI-TR-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcPildiTrI().getWpcoDtUltcPildiTrINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-PILDI-TR-I-NULL
            //           TO PCO-DT-ULTC-PILDI-TR-I-NULL
            ws.getParamComp().getPcoDtUltcPildiTrI().setPcoDtUltcPildiTrINull(ws.getLccvpco1().getDati().getWpcoDtUltcPildiTrI().getWpcoDtUltcPildiTrINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcPildiTrI().getWpcoDtUltcPildiTrI() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-PILDI-TR-I = ZERO
            //              TO PCO-DT-ULTC-PILDI-TR-I-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-PILDI-TR-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-PILDI-TR-I-NULL
            ws.getParamComp().getPcoDtUltcPildiTrI().setPcoDtUltcPildiTrINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcPildiTrI.Len.PCO_DT_ULTC_PILDI_TR_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-PILDI-TR-I
            //           TO PCO-DT-ULTC-PILDI-TR-I
            ws.getParamComp().getPcoDtUltcPildiTrI().setPcoDtUltcPildiTrI(ws.getLccvpco1().getDati().getWpcoDtUltcPildiTrI().getWpcoDtUltcPildiTrI());
        }
        // COB_CODE: IF (SF)-DT-ULTC-PILDI-AA-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULTC-PILDI-AA-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaI().getWpcoDtUltcPildiAaINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULTC-PILDI-AA-I-NULL
            //           TO PCO-DT-ULTC-PILDI-AA-I-NULL
            ws.getParamComp().getPcoDtUltcPildiAaI().setPcoDtUltcPildiAaINull(ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaI().getWpcoDtUltcPildiAaINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaI().getWpcoDtUltcPildiAaI() == 0) {
            // COB_CODE: IF (SF)-DT-ULTC-PILDI-AA-I = ZERO
            //              TO PCO-DT-ULTC-PILDI-AA-I-NULL
            //           ELSE
            //            TO PCO-DT-ULTC-PILDI-AA-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULTC-PILDI-AA-I-NULL
            ws.getParamComp().getPcoDtUltcPildiAaI().setPcoDtUltcPildiAaINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcPildiAaI.Len.PCO_DT_ULTC_PILDI_AA_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULTC-PILDI-AA-I
            //           TO PCO-DT-ULTC-PILDI-AA-I
            ws.getParamComp().getPcoDtUltcPildiAaI().setPcoDtUltcPildiAaI(ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaI().getWpcoDtUltcPildiAaI());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-QUIE-C-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-QUIE-C-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollQuieC().getWpcoDtUltBollQuieCNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-QUIE-C-NULL
            //           TO PCO-DT-ULT-BOLL-QUIE-C-NULL
            ws.getParamComp().getPcoDtUltBollQuieC().setPcoDtUltBollQuieCNull(ws.getLccvpco1().getDati().getWpcoDtUltBollQuieC().getWpcoDtUltBollQuieCNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollQuieC().getWpcoDtUltBollQuieC() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-QUIE-C = ZERO
            //              TO PCO-DT-ULT-BOLL-QUIE-C-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-QUIE-C
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-QUIE-C-NULL
            ws.getParamComp().getPcoDtUltBollQuieC().setPcoDtUltBollQuieCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollQuieC.Len.PCO_DT_ULT_BOLL_QUIE_C_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-QUIE-C
            //           TO PCO-DT-ULT-BOLL-QUIE-C
            ws.getParamComp().getPcoDtUltBollQuieC().setPcoDtUltBollQuieC(ws.getLccvpco1().getDati().getWpcoDtUltBollQuieC().getWpcoDtUltBollQuieC());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-QUIE-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-QUIE-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollQuieI().getWpcoDtUltBollQuieINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-QUIE-I-NULL
            //           TO PCO-DT-ULT-BOLL-QUIE-I-NULL
            ws.getParamComp().getPcoDtUltBollQuieI().setPcoDtUltBollQuieINull(ws.getLccvpco1().getDati().getWpcoDtUltBollQuieI().getWpcoDtUltBollQuieINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollQuieI().getWpcoDtUltBollQuieI() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-QUIE-I = ZERO
            //              TO PCO-DT-ULT-BOLL-QUIE-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-QUIE-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-QUIE-I-NULL
            ws.getParamComp().getPcoDtUltBollQuieI().setPcoDtUltBollQuieINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollQuieI.Len.PCO_DT_ULT_BOLL_QUIE_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-QUIE-I
            //           TO PCO-DT-ULT-BOLL-QUIE-I
            ws.getParamComp().getPcoDtUltBollQuieI().setPcoDtUltBollQuieI(ws.getLccvpco1().getDati().getWpcoDtUltBollQuieI().getWpcoDtUltBollQuieI());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-COTR-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-COTR-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollCotrI().getWpcoDtUltBollCotrINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-COTR-I-NULL
            //           TO PCO-DT-ULT-BOLL-COTR-I-NULL
            ws.getParamComp().getPcoDtUltBollCotrI().setPcoDtUltBollCotrINull(ws.getLccvpco1().getDati().getWpcoDtUltBollCotrI().getWpcoDtUltBollCotrINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollCotrI().getWpcoDtUltBollCotrI() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-COTR-I = ZERO
            //              TO PCO-DT-ULT-BOLL-COTR-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-COTR-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-COTR-I-NULL
            ws.getParamComp().getPcoDtUltBollCotrI().setPcoDtUltBollCotrINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollCotrI.Len.PCO_DT_ULT_BOLL_COTR_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-COTR-I
            //           TO PCO-DT-ULT-BOLL-COTR-I
            ws.getParamComp().getPcoDtUltBollCotrI().setPcoDtUltBollCotrI(ws.getLccvpco1().getDati().getWpcoDtUltBollCotrI().getWpcoDtUltBollCotrI());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-COTR-C-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-COTR-C-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollCotrC().getWpcoDtUltBollCotrCNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-COTR-C-NULL
            //           TO PCO-DT-ULT-BOLL-COTR-C-NULL
            ws.getParamComp().getPcoDtUltBollCotrC().setPcoDtUltBollCotrCNull(ws.getLccvpco1().getDati().getWpcoDtUltBollCotrC().getWpcoDtUltBollCotrCNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollCotrC().getWpcoDtUltBollCotrC() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-COTR-C = ZERO
            //              TO PCO-DT-ULT-BOLL-COTR-C-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-COTR-C
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-COTR-C-NULL
            ws.getParamComp().getPcoDtUltBollCotrC().setPcoDtUltBollCotrCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollCotrC.Len.PCO_DT_ULT_BOLL_COTR_C_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-COTR-C
            //           TO PCO-DT-ULT-BOLL-COTR-C
            ws.getParamComp().getPcoDtUltBollCotrC().setPcoDtUltBollCotrC(ws.getLccvpco1().getDati().getWpcoDtUltBollCotrC().getWpcoDtUltBollCotrC());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-CORI-C-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-CORI-C-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollCoriC().getWpcoDtUltBollCoriCNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-CORI-C-NULL
            //           TO PCO-DT-ULT-BOLL-CORI-C-NULL
            ws.getParamComp().getPcoDtUltBollCoriC().setPcoDtUltBollCoriCNull(ws.getLccvpco1().getDati().getWpcoDtUltBollCoriC().getWpcoDtUltBollCoriCNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollCoriC().getWpcoDtUltBollCoriC() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-CORI-C = ZERO
            //              TO PCO-DT-ULT-BOLL-CORI-C-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-CORI-C
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-CORI-C-NULL
            ws.getParamComp().getPcoDtUltBollCoriC().setPcoDtUltBollCoriCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollCoriC.Len.PCO_DT_ULT_BOLL_CORI_C_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-CORI-C
            //           TO PCO-DT-ULT-BOLL-CORI-C
            ws.getParamComp().getPcoDtUltBollCoriC().setPcoDtUltBollCoriC(ws.getLccvpco1().getDati().getWpcoDtUltBollCoriC().getWpcoDtUltBollCoriC());
        }
        // COB_CODE: IF (SF)-DT-ULT-BOLL-CORI-I-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-BOLL-CORI-I-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltBollCoriI().getWpcoDtUltBollCoriINullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-CORI-I-NULL
            //           TO PCO-DT-ULT-BOLL-CORI-I-NULL
            ws.getParamComp().getPcoDtUltBollCoriI().setPcoDtUltBollCoriINull(ws.getLccvpco1().getDati().getWpcoDtUltBollCoriI().getWpcoDtUltBollCoriINull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltBollCoriI().getWpcoDtUltBollCoriI() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-BOLL-CORI-I = ZERO
            //              TO PCO-DT-ULT-BOLL-CORI-I-NULL
            //           ELSE
            //            TO PCO-DT-ULT-BOLL-CORI-I
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-BOLL-CORI-I-NULL
            ws.getParamComp().getPcoDtUltBollCoriI().setPcoDtUltBollCoriINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollCoriI.Len.PCO_DT_ULT_BOLL_CORI_I_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-BOLL-CORI-I
            //           TO PCO-DT-ULT-BOLL-CORI-I
            ws.getParamComp().getPcoDtUltBollCoriI().setPcoDtUltBollCoriI(ws.getLccvpco1().getDati().getWpcoDtUltBollCoriI().getWpcoDtUltBollCoriI());
        }
        // COB_CODE: MOVE (SF)-DS-OPER-SQL
        //              TO PCO-DS-OPER-SQL
        ws.getParamComp().setPcoDsOperSql(ws.getLccvpco1().getDati().getWpcoDsOperSql());
        // COB_CODE: IF (SF)-DS-VER NOT NUMERIC
        //              MOVE 0 TO PCO-DS-VER
        //           ELSE
        //              TO PCO-DS-VER
        //           END-IF
        if (!Functions.isNumber(ws.getLccvpco1().getDati().getWpcoDsVer())) {
            // COB_CODE: MOVE 0 TO PCO-DS-VER
            ws.getParamComp().setPcoDsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER
            //           TO PCO-DS-VER
            ws.getParamComp().setPcoDsVer(ws.getLccvpco1().getDati().getWpcoDsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-CPTZ NOT NUMERIC
        //              MOVE 0 TO PCO-DS-TS-CPTZ
        //           ELSE
        //              TO PCO-DS-TS-CPTZ
        //           END-IF
        if (!Functions.isNumber(ws.getLccvpco1().getDati().getWpcoDsTsCptz())) {
            // COB_CODE: MOVE 0 TO PCO-DS-TS-CPTZ
            ws.getParamComp().setPcoDsTsCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-CPTZ
            //           TO PCO-DS-TS-CPTZ
            ws.getParamComp().setPcoDsTsCptz(ws.getLccvpco1().getDati().getWpcoDsTsCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE
        //              TO PCO-DS-UTENTE
        ws.getParamComp().setPcoDsUtente(ws.getLccvpco1().getDati().getWpcoDsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB
        //              TO PCO-DS-STATO-ELAB
        ws.getParamComp().setPcoDsStatoElab(ws.getLccvpco1().getDati().getWpcoDsStatoElab());
        // COB_CODE: IF (SF)-TP-VALZZ-DT-VLT-NULL = HIGH-VALUES
        //              TO PCO-TP-VALZZ-DT-VLT-NULL
        //           ELSE
        //              TO PCO-TP-VALZZ-DT-VLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoTpValzzDtVltFormatted())) {
            // COB_CODE: MOVE (SF)-TP-VALZZ-DT-VLT-NULL
            //           TO PCO-TP-VALZZ-DT-VLT-NULL
            ws.getParamComp().setPcoTpValzzDtVlt(ws.getLccvpco1().getDati().getWpcoTpValzzDtVlt());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-VALZZ-DT-VLT
            //           TO PCO-TP-VALZZ-DT-VLT
            ws.getParamComp().setPcoTpValzzDtVlt(ws.getLccvpco1().getDati().getWpcoTpValzzDtVlt());
        }
        // COB_CODE: IF (SF)-FL-FRAZ-PROV-ACQ-NULL = HIGH-VALUES
        //              TO PCO-FL-FRAZ-PROV-ACQ-NULL
        //           ELSE
        //              TO PCO-FL-FRAZ-PROV-ACQ
        //           END-IF
        if (Conditions.eq(ws.getLccvpco1().getDati().getWpcoFlFrazProvAcq(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-FRAZ-PROV-ACQ-NULL
            //           TO PCO-FL-FRAZ-PROV-ACQ-NULL
            ws.getParamComp().setPcoFlFrazProvAcq(ws.getLccvpco1().getDati().getWpcoFlFrazProvAcq());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-FRAZ-PROV-ACQ
            //           TO PCO-FL-FRAZ-PROV-ACQ
            ws.getParamComp().setPcoFlFrazProvAcq(ws.getLccvpco1().getDati().getWpcoFlFrazProvAcq());
        }
        // COB_CODE: IF (SF)-DT-ULT-AGG-EROG-RE-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-AGG-EROG-RE-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltAggErogRe().getWpcoDtUltAggErogReNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-AGG-EROG-RE-NULL
            //           TO PCO-DT-ULT-AGG-EROG-RE-NULL
            ws.getParamComp().getPcoDtUltAggErogRe().setPcoDtUltAggErogReNull(ws.getLccvpco1().getDati().getWpcoDtUltAggErogRe().getWpcoDtUltAggErogReNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltAggErogRe().getWpcoDtUltAggErogRe() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-AGG-EROG-RE = ZERO
            //              TO PCO-DT-ULT-AGG-EROG-RE-NULL
            //           ELSE
            //            TO PCO-DT-ULT-AGG-EROG-RE
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-AGG-EROG-RE-NULL
            ws.getParamComp().getPcoDtUltAggErogRe().setPcoDtUltAggErogReNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltAggErogRe.Len.PCO_DT_ULT_AGG_EROG_RE_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-AGG-EROG-RE
            //           TO PCO-DT-ULT-AGG-EROG-RE
            ws.getParamComp().getPcoDtUltAggErogRe().setPcoDtUltAggErogRe(ws.getLccvpco1().getDati().getWpcoDtUltAggErogRe().getWpcoDtUltAggErogRe());
        }
        // COB_CODE: IF (SF)-PC-RM-MARSOL-NULL = HIGH-VALUES
        //              TO PCO-PC-RM-MARSOL-NULL
        //           ELSE
        //              TO PCO-PC-RM-MARSOL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoPcRmMarsol().getWpcoPcRmMarsolNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-RM-MARSOL-NULL
            //           TO PCO-PC-RM-MARSOL-NULL
            ws.getParamComp().getPcoPcRmMarsol().setPcoPcRmMarsolNull(ws.getLccvpco1().getDati().getWpcoPcRmMarsol().getWpcoPcRmMarsolNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-RM-MARSOL
            //           TO PCO-PC-RM-MARSOL
            ws.getParamComp().getPcoPcRmMarsol().setPcoPcRmMarsol(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoPcRmMarsol().getWpcoPcRmMarsol(), 6, 3));
        }
        // COB_CODE: IF (SF)-PC-C-SUBRSH-MARSOL-NULL = HIGH-VALUES
        //              TO PCO-PC-C-SUBRSH-MARSOL-NULL
        //           ELSE
        //              TO PCO-PC-C-SUBRSH-MARSOL
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoPcCSubrshMarsol().getWpcoPcCSubrshMarsolNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-C-SUBRSH-MARSOL-NULL
            //           TO PCO-PC-C-SUBRSH-MARSOL-NULL
            ws.getParamComp().getPcoPcCSubrshMarsol().setPcoPcCSubrshMarsolNull(ws.getLccvpco1().getDati().getWpcoPcCSubrshMarsol().getWpcoPcCSubrshMarsolNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-C-SUBRSH-MARSOL
            //           TO PCO-PC-C-SUBRSH-MARSOL
            ws.getParamComp().getPcoPcCSubrshMarsol().setPcoPcCSubrshMarsol(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoPcCSubrshMarsol().getWpcoPcCSubrshMarsol(), 6, 3));
        }
        // COB_CODE: IF (SF)-COD-COMP-ISVAP-NULL = HIGH-VALUES
        //              TO PCO-COD-COMP-ISVAP-NULL
        //           ELSE
        //              TO PCO-COD-COMP-ISVAP
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoCodCompIsvapFormatted())) {
            // COB_CODE: MOVE (SF)-COD-COMP-ISVAP-NULL
            //           TO PCO-COD-COMP-ISVAP-NULL
            ws.getParamComp().setPcoCodCompIsvap(ws.getLccvpco1().getDati().getWpcoCodCompIsvap());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-COMP-ISVAP
            //           TO PCO-COD-COMP-ISVAP
            ws.getParamComp().setPcoCodCompIsvap(ws.getLccvpco1().getDati().getWpcoCodCompIsvap());
        }
        // COB_CODE: IF (SF)-LM-RIS-CON-INT-NULL = HIGH-VALUES
        //              TO PCO-LM-RIS-CON-INT-NULL
        //           ELSE
        //              TO PCO-LM-RIS-CON-INT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoLmRisConInt().getWpcoLmRisConIntNullFormatted())) {
            // COB_CODE: MOVE (SF)-LM-RIS-CON-INT-NULL
            //           TO PCO-LM-RIS-CON-INT-NULL
            ws.getParamComp().getPcoLmRisConInt().setPcoLmRisConIntNull(ws.getLccvpco1().getDati().getWpcoLmRisConInt().getWpcoLmRisConIntNull());
        }
        else {
            // COB_CODE: MOVE (SF)-LM-RIS-CON-INT
            //           TO PCO-LM-RIS-CON-INT
            ws.getParamComp().getPcoLmRisConInt().setPcoLmRisConInt(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoLmRisConInt().getWpcoLmRisConInt(), 6, 3));
        }
        // COB_CODE: IF (SF)-LM-C-SUBRSH-CON-IN-NULL = HIGH-VALUES
        //              TO PCO-LM-C-SUBRSH-CON-IN-NULL
        //           ELSE
        //              TO PCO-LM-C-SUBRSH-CON-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoLmCSubrshConIn().getWpcoLmCSubrshConInNullFormatted())) {
            // COB_CODE: MOVE (SF)-LM-C-SUBRSH-CON-IN-NULL
            //           TO PCO-LM-C-SUBRSH-CON-IN-NULL
            ws.getParamComp().getPcoLmCSubrshConIn().setPcoLmCSubrshConInNull(ws.getLccvpco1().getDati().getWpcoLmCSubrshConIn().getWpcoLmCSubrshConInNull());
        }
        else {
            // COB_CODE: MOVE (SF)-LM-C-SUBRSH-CON-IN
            //           TO PCO-LM-C-SUBRSH-CON-IN
            ws.getParamComp().getPcoLmCSubrshConIn().setPcoLmCSubrshConIn(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoLmCSubrshConIn().getWpcoLmCSubrshConIn(), 6, 3));
        }
        // COB_CODE: IF (SF)-PC-GAR-NORISK-MARS-NULL = HIGH-VALUES
        //              TO PCO-PC-GAR-NORISK-MARS-NULL
        //           ELSE
        //              TO PCO-PC-GAR-NORISK-MARS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoPcGarNoriskMars().getWpcoPcGarNoriskMarsNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-GAR-NORISK-MARS-NULL
            //           TO PCO-PC-GAR-NORISK-MARS-NULL
            ws.getParamComp().getPcoPcGarNoriskMars().setPcoPcGarNoriskMarsNull(ws.getLccvpco1().getDati().getWpcoPcGarNoriskMars().getWpcoPcGarNoriskMarsNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-GAR-NORISK-MARS
            //           TO PCO-PC-GAR-NORISK-MARS
            ws.getParamComp().getPcoPcGarNoriskMars().setPcoPcGarNoriskMars(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoPcGarNoriskMars().getWpcoPcGarNoriskMars(), 6, 3));
        }
        // COB_CODE: IF (SF)-CRZ-1A-RAT-INTR-PR-NULL = HIGH-VALUES
        //              TO PCO-CRZ-1A-RAT-INTR-PR-NULL
        //           ELSE
        //              TO PCO-CRZ-1A-RAT-INTR-PR
        //           END-IF
        if (Conditions.eq(ws.getLccvpco1().getDati().getWpcoCrz1aRatIntrPr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-CRZ-1A-RAT-INTR-PR-NULL
            //           TO PCO-CRZ-1A-RAT-INTR-PR-NULL
            ws.getParamComp().setPcoCrz1aRatIntrPr(ws.getLccvpco1().getDati().getWpcoCrz1aRatIntrPr());
        }
        else {
            // COB_CODE: MOVE (SF)-CRZ-1A-RAT-INTR-PR
            //           TO PCO-CRZ-1A-RAT-INTR-PR
            ws.getParamComp().setPcoCrz1aRatIntrPr(ws.getLccvpco1().getDati().getWpcoCrz1aRatIntrPr());
        }
        // COB_CODE: IF (SF)-NUM-GG-ARR-INTR-PR-NULL = HIGH-VALUES
        //              TO PCO-NUM-GG-ARR-INTR-PR-NULL
        //           ELSE
        //              TO PCO-NUM-GG-ARR-INTR-PR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoNumGgArrIntrPr().getWpcoNumGgArrIntrPrNullFormatted())) {
            // COB_CODE: MOVE (SF)-NUM-GG-ARR-INTR-PR-NULL
            //           TO PCO-NUM-GG-ARR-INTR-PR-NULL
            ws.getParamComp().getPcoNumGgArrIntrPr().setPcoNumGgArrIntrPrNull(ws.getLccvpco1().getDati().getWpcoNumGgArrIntrPr().getWpcoNumGgArrIntrPrNull());
        }
        else {
            // COB_CODE: MOVE (SF)-NUM-GG-ARR-INTR-PR
            //           TO PCO-NUM-GG-ARR-INTR-PR
            ws.getParamComp().getPcoNumGgArrIntrPr().setPcoNumGgArrIntrPr(ws.getLccvpco1().getDati().getWpcoNumGgArrIntrPr().getWpcoNumGgArrIntrPr());
        }
        // COB_CODE: IF (SF)-FL-VISUAL-VINPG-NULL = HIGH-VALUES
        //              TO PCO-FL-VISUAL-VINPG-NULL
        //           ELSE
        //              TO PCO-FL-VISUAL-VINPG
        //           END-IF
        if (Conditions.eq(ws.getLccvpco1().getDati().getWpcoFlVisualVinpg(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-VISUAL-VINPG-NULL
            //           TO PCO-FL-VISUAL-VINPG-NULL
            ws.getParamComp().setPcoFlVisualVinpg(ws.getLccvpco1().getDati().getWpcoFlVisualVinpg());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-VISUAL-VINPG
            //           TO PCO-FL-VISUAL-VINPG
            ws.getParamComp().setPcoFlVisualVinpg(ws.getLccvpco1().getDati().getWpcoFlVisualVinpg());
        }
        // COB_CODE: IF (SF)-DT-ULT-ESTRAZ-FUG-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ESTRAZ-FUG-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEstrazFug().getWpcoDtUltEstrazFugNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ESTRAZ-FUG-NULL
            //           TO PCO-DT-ULT-ESTRAZ-FUG-NULL
            ws.getParamComp().getPcoDtUltEstrazFug().setPcoDtUltEstrazFugNull(ws.getLccvpco1().getDati().getWpcoDtUltEstrazFug().getWpcoDtUltEstrazFugNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEstrazFug().getWpcoDtUltEstrazFug() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ESTRAZ-FUG = ZERO
            //              TO PCO-DT-ULT-ESTRAZ-FUG-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ESTRAZ-FUG
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ESTRAZ-FUG-NULL
            ws.getParamComp().getPcoDtUltEstrazFug().setPcoDtUltEstrazFugNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEstrazFug.Len.PCO_DT_ULT_ESTRAZ_FUG_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ESTRAZ-FUG
            //           TO PCO-DT-ULT-ESTRAZ-FUG
            ws.getParamComp().getPcoDtUltEstrazFug().setPcoDtUltEstrazFug(ws.getLccvpco1().getDati().getWpcoDtUltEstrazFug().getWpcoDtUltEstrazFug());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-PR-AUT-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-PR-AUT-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabPrAut().getWpcoDtUltElabPrAutNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-PR-AUT-NULL
            //           TO PCO-DT-ULT-ELAB-PR-AUT-NULL
            ws.getParamComp().getPcoDtUltElabPrAut().setPcoDtUltElabPrAutNull(ws.getLccvpco1().getDati().getWpcoDtUltElabPrAut().getWpcoDtUltElabPrAutNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabPrAut().getWpcoDtUltElabPrAut() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-PR-AUT = ZERO
            //              TO PCO-DT-ULT-ELAB-PR-AUT-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-PR-AUT
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-PR-AUT-NULL
            ws.getParamComp().getPcoDtUltElabPrAut().setPcoDtUltElabPrAutNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabPrAut.Len.PCO_DT_ULT_ELAB_PR_AUT_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-PR-AUT
            //           TO PCO-DT-ULT-ELAB-PR-AUT
            ws.getParamComp().getPcoDtUltElabPrAut().setPcoDtUltElabPrAut(ws.getLccvpco1().getDati().getWpcoDtUltElabPrAut().getWpcoDtUltElabPrAut());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-COMMEF-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-COMMEF-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabCommef().getWpcoDtUltElabCommefNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-COMMEF-NULL
            //           TO PCO-DT-ULT-ELAB-COMMEF-NULL
            ws.getParamComp().getPcoDtUltElabCommef().setPcoDtUltElabCommefNull(ws.getLccvpco1().getDati().getWpcoDtUltElabCommef().getWpcoDtUltElabCommefNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabCommef().getWpcoDtUltElabCommef() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-COMMEF = ZERO
            //              TO PCO-DT-ULT-ELAB-COMMEF-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-COMMEF
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-COMMEF-NULL
            ws.getParamComp().getPcoDtUltElabCommef().setPcoDtUltElabCommefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabCommef.Len.PCO_DT_ULT_ELAB_COMMEF_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-COMMEF
            //           TO PCO-DT-ULT-ELAB-COMMEF
            ws.getParamComp().getPcoDtUltElabCommef().setPcoDtUltElabCommef(ws.getLccvpco1().getDati().getWpcoDtUltElabCommef().getWpcoDtUltElabCommef());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-LIQMEF-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-LIQMEF-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabLiqmef().getWpcoDtUltElabLiqmefNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-LIQMEF-NULL
            //           TO PCO-DT-ULT-ELAB-LIQMEF-NULL
            ws.getParamComp().getPcoDtUltElabLiqmef().setPcoDtUltElabLiqmefNull(ws.getLccvpco1().getDati().getWpcoDtUltElabLiqmef().getWpcoDtUltElabLiqmefNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabLiqmef().getWpcoDtUltElabLiqmef() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-LIQMEF = ZERO
            //              TO PCO-DT-ULT-ELAB-LIQMEF-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-LIQMEF
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-LIQMEF-NULL
            ws.getParamComp().getPcoDtUltElabLiqmef().setPcoDtUltElabLiqmefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabLiqmef.Len.PCO_DT_ULT_ELAB_LIQMEF_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-LIQMEF
            //           TO PCO-DT-ULT-ELAB-LIQMEF
            ws.getParamComp().getPcoDtUltElabLiqmef().setPcoDtUltElabLiqmef(ws.getLccvpco1().getDati().getWpcoDtUltElabLiqmef().getWpcoDtUltElabLiqmef());
        }
        // COB_CODE: IF (SF)-COD-FISC-MEF-NULL = HIGH-VALUES
        //              TO PCO-COD-FISC-MEF-NULL
        //           ELSE
        //              TO PCO-COD-FISC-MEF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoCodFiscMefFormatted())) {
            // COB_CODE: MOVE (SF)-COD-FISC-MEF-NULL
            //           TO PCO-COD-FISC-MEF-NULL
            ws.getParamComp().setPcoCodFiscMef(ws.getLccvpco1().getDati().getWpcoCodFiscMef());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-FISC-MEF
            //           TO PCO-COD-FISC-MEF
            ws.getParamComp().setPcoCodFiscMef(ws.getLccvpco1().getDati().getWpcoCodFiscMef());
        }
        // COB_CODE: IF (SF)-IMP-ASS-SOCIALE-NULL = HIGH-VALUES
        //              TO PCO-IMP-ASS-SOCIALE-NULL
        //           ELSE
        //              TO PCO-IMP-ASS-SOCIALE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoImpAssSociale().getWpcoImpAssSocialeNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-ASS-SOCIALE-NULL
            //           TO PCO-IMP-ASS-SOCIALE-NULL
            ws.getParamComp().getPcoImpAssSociale().setPcoImpAssSocialeNull(ws.getLccvpco1().getDati().getWpcoImpAssSociale().getWpcoImpAssSocialeNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-ASS-SOCIALE
            //           TO PCO-IMP-ASS-SOCIALE
            ws.getParamComp().getPcoImpAssSociale().setPcoImpAssSociale(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoImpAssSociale().getWpcoImpAssSociale(), 15, 3));
        }
        // COB_CODE: IF (SF)-MOD-COMNZ-INVST-SW-NULL = HIGH-VALUES
        //              TO PCO-MOD-COMNZ-INVST-SW-NULL
        //           ELSE
        //              TO PCO-MOD-COMNZ-INVST-SW
        //           END-IF
        if (Conditions.eq(ws.getLccvpco1().getDati().getWpcoModComnzInvstSw(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-MOD-COMNZ-INVST-SW-NULL
            //           TO PCO-MOD-COMNZ-INVST-SW-NULL
            ws.getParamComp().setPcoModComnzInvstSw(ws.getLccvpco1().getDati().getWpcoModComnzInvstSw());
        }
        else {
            // COB_CODE: MOVE (SF)-MOD-COMNZ-INVST-SW
            //           TO PCO-MOD-COMNZ-INVST-SW
            ws.getParamComp().setPcoModComnzInvstSw(ws.getLccvpco1().getDati().getWpcoModComnzInvstSw());
        }
        // COB_CODE: IF (SF)-DT-RIAT-RIASS-RSH-NULL = HIGH-VALUES
        //              TO PCO-DT-RIAT-RIASS-RSH-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtRiatRiassRsh().getWpcoDtRiatRiassRshNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-RIAT-RIASS-RSH-NULL
            //           TO PCO-DT-RIAT-RIASS-RSH-NULL
            ws.getParamComp().getPcoDtRiatRiassRsh().setPcoDtRiatRiassRshNull(ws.getLccvpco1().getDati().getWpcoDtRiatRiassRsh().getWpcoDtRiatRiassRshNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtRiatRiassRsh().getWpcoDtRiatRiassRsh() == 0) {
            // COB_CODE: IF (SF)-DT-RIAT-RIASS-RSH = ZERO
            //              TO PCO-DT-RIAT-RIASS-RSH-NULL
            //           ELSE
            //            TO PCO-DT-RIAT-RIASS-RSH
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-RIAT-RIASS-RSH-NULL
            ws.getParamComp().getPcoDtRiatRiassRsh().setPcoDtRiatRiassRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtRiatRiassRsh.Len.PCO_DT_RIAT_RIASS_RSH_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-RIAT-RIASS-RSH
            //           TO PCO-DT-RIAT-RIASS-RSH
            ws.getParamComp().getPcoDtRiatRiassRsh().setPcoDtRiatRiassRsh(ws.getLccvpco1().getDati().getWpcoDtRiatRiassRsh().getWpcoDtRiatRiassRsh());
        }
        // COB_CODE: IF (SF)-DT-RIAT-RIASS-COMM-NULL = HIGH-VALUES
        //              TO PCO-DT-RIAT-RIASS-COMM-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtRiatRiassComm().getWpcoDtRiatRiassCommNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-RIAT-RIASS-COMM-NULL
            //           TO PCO-DT-RIAT-RIASS-COMM-NULL
            ws.getParamComp().getPcoDtRiatRiassComm().setPcoDtRiatRiassCommNull(ws.getLccvpco1().getDati().getWpcoDtRiatRiassComm().getWpcoDtRiatRiassCommNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtRiatRiassComm().getWpcoDtRiatRiassComm() == 0) {
            // COB_CODE: IF (SF)-DT-RIAT-RIASS-COMM = ZERO
            //              TO PCO-DT-RIAT-RIASS-COMM-NULL
            //           ELSE
            //            TO PCO-DT-RIAT-RIASS-COMM
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-RIAT-RIASS-COMM-NULL
            ws.getParamComp().getPcoDtRiatRiassComm().setPcoDtRiatRiassCommNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtRiatRiassComm.Len.PCO_DT_RIAT_RIASS_COMM_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-RIAT-RIASS-COMM
            //           TO PCO-DT-RIAT-RIASS-COMM
            ws.getParamComp().getPcoDtRiatRiassComm().setPcoDtRiatRiassComm(ws.getLccvpco1().getDati().getWpcoDtRiatRiassComm().getWpcoDtRiatRiassComm());
        }
        // COB_CODE: IF (SF)-GG-INTR-RIT-PAG-NULL = HIGH-VALUES
        //              TO PCO-GG-INTR-RIT-PAG-NULL
        //           ELSE
        //              TO PCO-GG-INTR-RIT-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoGgIntrRitPag().getWpcoGgIntrRitPagNullFormatted())) {
            // COB_CODE: MOVE (SF)-GG-INTR-RIT-PAG-NULL
            //           TO PCO-GG-INTR-RIT-PAG-NULL
            ws.getParamComp().getPcoGgIntrRitPag().setPcoGgIntrRitPagNull(ws.getLccvpco1().getDati().getWpcoGgIntrRitPag().getWpcoGgIntrRitPagNull());
        }
        else {
            // COB_CODE: MOVE (SF)-GG-INTR-RIT-PAG
            //           TO PCO-GG-INTR-RIT-PAG
            ws.getParamComp().getPcoGgIntrRitPag().setPcoGgIntrRitPag(ws.getLccvpco1().getDati().getWpcoGgIntrRitPag().getWpcoGgIntrRitPag());
        }
        // COB_CODE: IF (SF)-DT-ULT-RINN-TAC-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-RINN-TAC-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltRinnTac().getWpcoDtUltRinnTacNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-RINN-TAC-NULL
            //           TO PCO-DT-ULT-RINN-TAC-NULL
            ws.getParamComp().getPcoDtUltRinnTac().setPcoDtUltRinnTacNull(ws.getLccvpco1().getDati().getWpcoDtUltRinnTac().getWpcoDtUltRinnTacNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltRinnTac().getWpcoDtUltRinnTac() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-RINN-TAC = ZERO
            //              TO PCO-DT-ULT-RINN-TAC-NULL
            //           ELSE
            //            TO PCO-DT-ULT-RINN-TAC
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-RINN-TAC-NULL
            ws.getParamComp().getPcoDtUltRinnTac().setPcoDtUltRinnTacNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRinnTac.Len.PCO_DT_ULT_RINN_TAC_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-RINN-TAC
            //           TO PCO-DT-ULT-RINN-TAC
            ws.getParamComp().getPcoDtUltRinnTac().setPcoDtUltRinnTac(ws.getLccvpco1().getDati().getWpcoDtUltRinnTac().getWpcoDtUltRinnTac());
        }
        // COB_CODE: MOVE (SF)-DESC-COMP-VCHAR
        //             TO PCO-DESC-COMP-VCHAR
        ws.getParamComp().setPcoDescCompVcharBytes(ws.getLccvpco1().getDati().getWpcoDescCompVcharBytes());
        // COB_CODE: IF (SF)-DT-ULT-EC-TCM-IND-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-EC-TCM-IND-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEcTcmInd().getWpcoDtUltEcTcmIndNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-TCM-IND-NULL
            //           TO PCO-DT-ULT-EC-TCM-IND-NULL
            ws.getParamComp().getPcoDtUltEcTcmInd().setPcoDtUltEcTcmIndNull(ws.getLccvpco1().getDati().getWpcoDtUltEcTcmInd().getWpcoDtUltEcTcmIndNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEcTcmInd().getWpcoDtUltEcTcmInd() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-EC-TCM-IND = ZERO
            //              TO PCO-DT-ULT-EC-TCM-IND-NULL
            //           ELSE
            //            TO PCO-DT-ULT-EC-TCM-IND
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-EC-TCM-IND-NULL
            ws.getParamComp().getPcoDtUltEcTcmInd().setPcoDtUltEcTcmIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcTcmInd.Len.PCO_DT_ULT_EC_TCM_IND_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-TCM-IND
            //           TO PCO-DT-ULT-EC-TCM-IND
            ws.getParamComp().getPcoDtUltEcTcmInd().setPcoDtUltEcTcmInd(ws.getLccvpco1().getDati().getWpcoDtUltEcTcmInd().getWpcoDtUltEcTcmInd());
        }
        // COB_CODE: IF (SF)-DT-ULT-EC-TCM-COLL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-EC-TCM-COLL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEcTcmColl().getWpcoDtUltEcTcmCollNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-TCM-COLL-NULL
            //           TO PCO-DT-ULT-EC-TCM-COLL-NULL
            ws.getParamComp().getPcoDtUltEcTcmColl().setPcoDtUltEcTcmCollNull(ws.getLccvpco1().getDati().getWpcoDtUltEcTcmColl().getWpcoDtUltEcTcmCollNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEcTcmColl().getWpcoDtUltEcTcmColl() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-EC-TCM-COLL = ZERO
            //              TO PCO-DT-ULT-EC-TCM-COLL-NULL
            //           ELSE
            //            TO PCO-DT-ULT-EC-TCM-COLL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-EC-TCM-COLL-NULL
            ws.getParamComp().getPcoDtUltEcTcmColl().setPcoDtUltEcTcmCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcTcmColl.Len.PCO_DT_ULT_EC_TCM_COLL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-TCM-COLL
            //           TO PCO-DT-ULT-EC-TCM-COLL
            ws.getParamComp().getPcoDtUltEcTcmColl().setPcoDtUltEcTcmColl(ws.getLccvpco1().getDati().getWpcoDtUltEcTcmColl().getWpcoDtUltEcTcmColl());
        }
        // COB_CODE: IF (SF)-DT-ULT-EC-MRM-IND-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-EC-MRM-IND-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEcMrmInd().getWpcoDtUltEcMrmIndNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-MRM-IND-NULL
            //           TO PCO-DT-ULT-EC-MRM-IND-NULL
            ws.getParamComp().getPcoDtUltEcMrmInd().setPcoDtUltEcMrmIndNull(ws.getLccvpco1().getDati().getWpcoDtUltEcMrmInd().getWpcoDtUltEcMrmIndNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEcMrmInd().getWpcoDtUltEcMrmInd() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-EC-MRM-IND = ZERO
            //              TO PCO-DT-ULT-EC-MRM-IND-NULL
            //           ELSE
            //            TO PCO-DT-ULT-EC-MRM-IND
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-EC-MRM-IND-NULL
            ws.getParamComp().getPcoDtUltEcMrmInd().setPcoDtUltEcMrmIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcMrmInd.Len.PCO_DT_ULT_EC_MRM_IND_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-MRM-IND
            //           TO PCO-DT-ULT-EC-MRM-IND
            ws.getParamComp().getPcoDtUltEcMrmInd().setPcoDtUltEcMrmInd(ws.getLccvpco1().getDati().getWpcoDtUltEcMrmInd().getWpcoDtUltEcMrmInd());
        }
        // COB_CODE: IF (SF)-DT-ULT-EC-MRM-COLL-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-EC-MRM-COLL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEcMrmColl().getWpcoDtUltEcMrmCollNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-MRM-COLL-NULL
            //           TO PCO-DT-ULT-EC-MRM-COLL-NULL
            ws.getParamComp().getPcoDtUltEcMrmColl().setPcoDtUltEcMrmCollNull(ws.getLccvpco1().getDati().getWpcoDtUltEcMrmColl().getWpcoDtUltEcMrmCollNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEcMrmColl().getWpcoDtUltEcMrmColl() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-EC-MRM-COLL = ZERO
            //              TO PCO-DT-ULT-EC-MRM-COLL-NULL
            //           ELSE
            //            TO PCO-DT-ULT-EC-MRM-COLL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-EC-MRM-COLL-NULL
            ws.getParamComp().getPcoDtUltEcMrmColl().setPcoDtUltEcMrmCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcMrmColl.Len.PCO_DT_ULT_EC_MRM_COLL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-EC-MRM-COLL
            //           TO PCO-DT-ULT-EC-MRM-COLL
            ws.getParamComp().getPcoDtUltEcMrmColl().setPcoDtUltEcMrmColl(ws.getLccvpco1().getDati().getWpcoDtUltEcMrmColl().getWpcoDtUltEcMrmColl());
        }
        // COB_CODE: IF (SF)-COD-COMP-LDAP-NULL = HIGH-VALUES
        //              TO PCO-COD-COMP-LDAP-NULL
        //           ELSE
        //              TO PCO-COD-COMP-LDAP
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoCodCompLdapFormatted())) {
            // COB_CODE: MOVE (SF)-COD-COMP-LDAP-NULL
            //           TO PCO-COD-COMP-LDAP-NULL
            ws.getParamComp().setPcoCodCompLdap(ws.getLccvpco1().getDati().getWpcoCodCompLdap());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-COMP-LDAP
            //           TO PCO-COD-COMP-LDAP
            ws.getParamComp().setPcoCodCompLdap(ws.getLccvpco1().getDati().getWpcoCodCompLdap());
        }
        // COB_CODE: IF (SF)-PC-RID-IMP-1382011-NULL = HIGH-VALUES
        //              TO PCO-PC-RID-IMP-1382011-NULL
        //           ELSE
        //              TO PCO-PC-RID-IMP-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoPcRidImp1382011().getWpcoPcRidImp1382011NullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-RID-IMP-1382011-NULL
            //           TO PCO-PC-RID-IMP-1382011-NULL
            ws.getParamComp().getPcoPcRidImp1382011().setPcoPcRidImp1382011Null(ws.getLccvpco1().getDati().getWpcoPcRidImp1382011().getWpcoPcRidImp1382011Null());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-RID-IMP-1382011
            //           TO PCO-PC-RID-IMP-1382011
            ws.getParamComp().getPcoPcRidImp1382011().setPcoPcRidImp1382011(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoPcRidImp1382011().getWpcoPcRidImp1382011(), 6, 3));
        }
        // COB_CODE: IF (SF)-PC-RID-IMP-662014-NULL = HIGH-VALUES
        //              TO PCO-PC-RID-IMP-662014-NULL
        //           ELSE
        //              TO PCO-PC-RID-IMP-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoPcRidImp662014().getWpcoPcRidImp662014NullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-RID-IMP-662014-NULL
            //           TO PCO-PC-RID-IMP-662014-NULL
            ws.getParamComp().getPcoPcRidImp662014().setPcoPcRidImp662014Null(ws.getLccvpco1().getDati().getWpcoPcRidImp662014().getWpcoPcRidImp662014Null());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-RID-IMP-662014
            //           TO PCO-PC-RID-IMP-662014
            ws.getParamComp().getPcoPcRidImp662014().setPcoPcRidImp662014(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoPcRidImp662014().getWpcoPcRidImp662014(), 6, 3));
        }
        // COB_CODE: IF (SF)-SOGL-AML-PRE-UNI-NULL = HIGH-VALUES
        //              TO PCO-SOGL-AML-PRE-UNI-NULL
        //           ELSE
        //              TO PCO-SOGL-AML-PRE-UNI
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoSoglAmlPreUni().getWpcoSoglAmlPreUniNullFormatted())) {
            // COB_CODE: MOVE (SF)-SOGL-AML-PRE-UNI-NULL
            //           TO PCO-SOGL-AML-PRE-UNI-NULL
            ws.getParamComp().getPcoSoglAmlPreUni().setPcoSoglAmlPreUniNull(ws.getLccvpco1().getDati().getWpcoSoglAmlPreUni().getWpcoSoglAmlPreUniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SOGL-AML-PRE-UNI
            //           TO PCO-SOGL-AML-PRE-UNI
            ws.getParamComp().getPcoSoglAmlPreUni().setPcoSoglAmlPreUni(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoSoglAmlPreUni().getWpcoSoglAmlPreUni(), 15, 3));
        }
        // COB_CODE: IF (SF)-SOGL-AML-PRE-PER-NULL = HIGH-VALUES
        //              TO PCO-SOGL-AML-PRE-PER-NULL
        //           ELSE
        //              TO PCO-SOGL-AML-PRE-PER
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoSoglAmlPrePer().getWpcoSoglAmlPrePerNullFormatted())) {
            // COB_CODE: MOVE (SF)-SOGL-AML-PRE-PER-NULL
            //           TO PCO-SOGL-AML-PRE-PER-NULL
            ws.getParamComp().getPcoSoglAmlPrePer().setPcoSoglAmlPrePerNull(ws.getLccvpco1().getDati().getWpcoSoglAmlPrePer().getWpcoSoglAmlPrePerNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SOGL-AML-PRE-PER
            //           TO PCO-SOGL-AML-PRE-PER
            ws.getParamComp().getPcoSoglAmlPrePer().setPcoSoglAmlPrePer(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoSoglAmlPrePer().getWpcoSoglAmlPrePer(), 15, 3));
        }
        // COB_CODE: IF (SF)-COD-SOGG-FTZ-ASSTO-NULL = HIGH-VALUES
        //              TO PCO-COD-SOGG-FTZ-ASSTO-NULL
        //           ELSE
        //              TO PCO-COD-SOGG-FTZ-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoCodSoggFtzAsstoFormatted())) {
            // COB_CODE: MOVE (SF)-COD-SOGG-FTZ-ASSTO-NULL
            //           TO PCO-COD-SOGG-FTZ-ASSTO-NULL
            ws.getParamComp().setPcoCodSoggFtzAssto(ws.getLccvpco1().getDati().getWpcoCodSoggFtzAssto());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-SOGG-FTZ-ASSTO
            //           TO PCO-COD-SOGG-FTZ-ASSTO
            ws.getParamComp().setPcoCodSoggFtzAssto(ws.getLccvpco1().getDati().getWpcoCodSoggFtzAssto());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-REDPRO-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-REDPRO-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabRedpro().getWpcoDtUltElabRedproNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-REDPRO-NULL
            //           TO PCO-DT-ULT-ELAB-REDPRO-NULL
            ws.getParamComp().getPcoDtUltElabRedpro().setPcoDtUltElabRedproNull(ws.getLccvpco1().getDati().getWpcoDtUltElabRedpro().getWpcoDtUltElabRedproNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabRedpro().getWpcoDtUltElabRedpro() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-REDPRO = ZERO
            //              TO PCO-DT-ULT-ELAB-REDPRO-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-REDPRO
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-REDPRO-NULL
            ws.getParamComp().getPcoDtUltElabRedpro().setPcoDtUltElabRedproNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabRedpro.Len.PCO_DT_ULT_ELAB_REDPRO_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-REDPRO
            //           TO PCO-DT-ULT-ELAB-REDPRO
            ws.getParamComp().getPcoDtUltElabRedpro().setPcoDtUltElabRedpro(ws.getLccvpco1().getDati().getWpcoDtUltElabRedpro().getWpcoDtUltElabRedpro());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-TAKE-P-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-TAKE-P-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabTakeP().getWpcoDtUltElabTakePNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-TAKE-P-NULL
            //           TO PCO-DT-ULT-ELAB-TAKE-P-NULL
            ws.getParamComp().getPcoDtUltElabTakeP().setPcoDtUltElabTakePNull(ws.getLccvpco1().getDati().getWpcoDtUltElabTakeP().getWpcoDtUltElabTakePNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabTakeP().getWpcoDtUltElabTakeP() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-TAKE-P = ZERO
            //              TO PCO-DT-ULT-ELAB-TAKE-P-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-TAKE-P
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-TAKE-P-NULL
            ws.getParamComp().getPcoDtUltElabTakeP().setPcoDtUltElabTakePNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabTakeP.Len.PCO_DT_ULT_ELAB_TAKE_P_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-TAKE-P
            //           TO PCO-DT-ULT-ELAB-TAKE-P
            ws.getParamComp().getPcoDtUltElabTakeP().setPcoDtUltElabTakeP(ws.getLccvpco1().getDati().getWpcoDtUltElabTakeP().getWpcoDtUltElabTakeP());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-PASPAS-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-PASPAS-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabPaspas().getWpcoDtUltElabPaspasNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-PASPAS-NULL
            //           TO PCO-DT-ULT-ELAB-PASPAS-NULL
            ws.getParamComp().getPcoDtUltElabPaspas().setPcoDtUltElabPaspasNull(ws.getLccvpco1().getDati().getWpcoDtUltElabPaspas().getWpcoDtUltElabPaspasNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabPaspas().getWpcoDtUltElabPaspas() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-PASPAS = ZERO
            //              TO PCO-DT-ULT-ELAB-PASPAS-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-PASPAS
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-PASPAS-NULL
            ws.getParamComp().getPcoDtUltElabPaspas().setPcoDtUltElabPaspasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabPaspas.Len.PCO_DT_ULT_ELAB_PASPAS_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-PASPAS
            //           TO PCO-DT-ULT-ELAB-PASPAS
            ws.getParamComp().getPcoDtUltElabPaspas().setPcoDtUltElabPaspas(ws.getLccvpco1().getDati().getWpcoDtUltElabPaspas().getWpcoDtUltElabPaspas());
        }
        // COB_CODE: IF (SF)-SOGL-AML-PRE-SAV-R-NULL = HIGH-VALUES
        //              TO PCO-SOGL-AML-PRE-SAV-R-NULL
        //           ELSE
        //              TO PCO-SOGL-AML-PRE-SAV-R
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoSoglAmlPreSavR().getWpcoSoglAmlPreSavRNullFormatted())) {
            // COB_CODE: MOVE (SF)-SOGL-AML-PRE-SAV-R-NULL
            //           TO PCO-SOGL-AML-PRE-SAV-R-NULL
            ws.getParamComp().getPcoSoglAmlPreSavR().setPcoSoglAmlPreSavRNull(ws.getLccvpco1().getDati().getWpcoSoglAmlPreSavR().getWpcoSoglAmlPreSavRNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SOGL-AML-PRE-SAV-R
            //           TO PCO-SOGL-AML-PRE-SAV-R
            ws.getParamComp().getPcoSoglAmlPreSavR().setPcoSoglAmlPreSavR(Trunc.toDecimal(ws.getLccvpco1().getDati().getWpcoSoglAmlPreSavR().getWpcoSoglAmlPreSavR(), 15, 3));
        }
        // COB_CODE: IF (SF)-DT-ULT-ESTR-DEC-CO-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ESTR-DEC-CO-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltEstrDecCo().getWpcoDtUltEstrDecCoNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ESTR-DEC-CO-NULL
            //           TO PCO-DT-ULT-ESTR-DEC-CO-NULL
            ws.getParamComp().getPcoDtUltEstrDecCo().setPcoDtUltEstrDecCoNull(ws.getLccvpco1().getDati().getWpcoDtUltEstrDecCo().getWpcoDtUltEstrDecCoNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltEstrDecCo().getWpcoDtUltEstrDecCo() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ESTR-DEC-CO = ZERO
            //              TO PCO-DT-ULT-ESTR-DEC-CO-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ESTR-DEC-CO
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ESTR-DEC-CO-NULL
            ws.getParamComp().getPcoDtUltEstrDecCo().setPcoDtUltEstrDecCoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEstrDecCo.Len.PCO_DT_ULT_ESTR_DEC_CO_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ESTR-DEC-CO
            //           TO PCO-DT-ULT-ESTR-DEC-CO
            ws.getParamComp().getPcoDtUltEstrDecCo().setPcoDtUltEstrDecCo(ws.getLccvpco1().getDati().getWpcoDtUltEstrDecCo().getWpcoDtUltEstrDecCo());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-COS-AT-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-COS-AT-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabCosAt().getWpcoDtUltElabCosAtNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-COS-AT-NULL
            //           TO PCO-DT-ULT-ELAB-COS-AT-NULL
            ws.getParamComp().getPcoDtUltElabCosAt().setPcoDtUltElabCosAtNull(ws.getLccvpco1().getDati().getWpcoDtUltElabCosAt().getWpcoDtUltElabCosAtNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabCosAt().getWpcoDtUltElabCosAt() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-COS-AT = ZERO
            //              TO PCO-DT-ULT-ELAB-COS-AT-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-COS-AT
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-COS-AT-NULL
            ws.getParamComp().getPcoDtUltElabCosAt().setPcoDtUltElabCosAtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabCosAt.Len.PCO_DT_ULT_ELAB_COS_AT_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-COS-AT
            //           TO PCO-DT-ULT-ELAB-COS-AT
            ws.getParamComp().getPcoDtUltElabCosAt().setPcoDtUltElabCosAt(ws.getLccvpco1().getDati().getWpcoDtUltElabCosAt().getWpcoDtUltElabCosAt());
        }
        // COB_CODE: IF (SF)-FRQ-COSTI-ATT-NULL = HIGH-VALUES
        //              TO PCO-FRQ-COSTI-ATT-NULL
        //           ELSE
        //              TO PCO-FRQ-COSTI-ATT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoFrqCostiAtt().getWpcoFrqCostiAttNullFormatted())) {
            // COB_CODE: MOVE (SF)-FRQ-COSTI-ATT-NULL
            //           TO PCO-FRQ-COSTI-ATT-NULL
            ws.getParamComp().getPcoFrqCostiAtt().setPcoFrqCostiAttNull(ws.getLccvpco1().getDati().getWpcoFrqCostiAtt().getWpcoFrqCostiAttNull());
        }
        else {
            // COB_CODE: MOVE (SF)-FRQ-COSTI-ATT
            //           TO PCO-FRQ-COSTI-ATT
            ws.getParamComp().getPcoFrqCostiAtt().setPcoFrqCostiAtt(ws.getLccvpco1().getDati().getWpcoFrqCostiAtt().getWpcoFrqCostiAtt());
        }
        // COB_CODE: IF (SF)-DT-ULT-ELAB-COS-ST-NULL = HIGH-VALUES
        //              TO PCO-DT-ULT-ELAB-COS-ST-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtUltElabCosSt().getWpcoDtUltElabCosStNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-COS-ST-NULL
            //           TO PCO-DT-ULT-ELAB-COS-ST-NULL
            ws.getParamComp().getPcoDtUltElabCosSt().setPcoDtUltElabCosStNull(ws.getLccvpco1().getDati().getWpcoDtUltElabCosSt().getWpcoDtUltElabCosStNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtUltElabCosSt().getWpcoDtUltElabCosSt() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ELAB-COS-ST = ZERO
            //              TO PCO-DT-ULT-ELAB-COS-ST-NULL
            //           ELSE
            //            TO PCO-DT-ULT-ELAB-COS-ST
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ULT-ELAB-COS-ST-NULL
            ws.getParamComp().getPcoDtUltElabCosSt().setPcoDtUltElabCosStNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabCosSt.Len.PCO_DT_ULT_ELAB_COS_ST_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ELAB-COS-ST
            //           TO PCO-DT-ULT-ELAB-COS-ST
            ws.getParamComp().getPcoDtUltElabCosSt().setPcoDtUltElabCosSt(ws.getLccvpco1().getDati().getWpcoDtUltElabCosSt().getWpcoDtUltElabCosSt());
        }
        // COB_CODE: IF (SF)-FRQ-COSTI-STORNATI-NULL = HIGH-VALUES
        //              TO PCO-FRQ-COSTI-STORNATI-NULL
        //           ELSE
        //              TO PCO-FRQ-COSTI-STORNATI
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoFrqCostiStornati().getWpcoFrqCostiStornatiNullFormatted())) {
            // COB_CODE: MOVE (SF)-FRQ-COSTI-STORNATI-NULL
            //           TO PCO-FRQ-COSTI-STORNATI-NULL
            ws.getParamComp().getPcoFrqCostiStornati().setPcoFrqCostiStornatiNull(ws.getLccvpco1().getDati().getWpcoFrqCostiStornati().getWpcoFrqCostiStornatiNull());
        }
        else {
            // COB_CODE: MOVE (SF)-FRQ-COSTI-STORNATI
            //           TO PCO-FRQ-COSTI-STORNATI
            ws.getParamComp().getPcoFrqCostiStornati().setPcoFrqCostiStornati(ws.getLccvpco1().getDati().getWpcoFrqCostiStornati().getWpcoFrqCostiStornati());
        }
        // COB_CODE: IF (SF)-DT-ESTR-ASS-MIN70A-NULL = HIGH-VALUES
        //              TO PCO-DT-ESTR-ASS-MIN70A-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtEstrAssMin70a().getWpcoDtEstrAssMin70aNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ESTR-ASS-MIN70A-NULL
            //           TO PCO-DT-ESTR-ASS-MIN70A-NULL
            ws.getParamComp().getPcoDtEstrAssMin70a().setPcoDtEstrAssMin70aNull(ws.getLccvpco1().getDati().getWpcoDtEstrAssMin70a().getWpcoDtEstrAssMin70aNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtEstrAssMin70a().getWpcoDtEstrAssMin70a() == 0) {
            // COB_CODE: IF (SF)-DT-ESTR-ASS-MIN70A = ZERO
            //              TO PCO-DT-ESTR-ASS-MIN70A-NULL
            //           ELSE
            //            TO PCO-DT-ESTR-ASS-MIN70A
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ESTR-ASS-MIN70A-NULL
            ws.getParamComp().getPcoDtEstrAssMin70a().setPcoDtEstrAssMin70aNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtEstrAssMin70a.Len.PCO_DT_ESTR_ASS_MIN70A_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ESTR-ASS-MIN70A
            //           TO PCO-DT-ESTR-ASS-MIN70A
            ws.getParamComp().getPcoDtEstrAssMin70a().setPcoDtEstrAssMin70a(ws.getLccvpco1().getDati().getWpcoDtEstrAssMin70a().getWpcoDtEstrAssMin70a());
        }
        // COB_CODE: IF (SF)-DT-ESTR-ASS-MAG70A-NULL = HIGH-VALUES
        //              TO PCO-DT-ESTR-ASS-MAG70A-NULL
        //           ELSE
        //             END-IF
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getLccvpco1().getDati().getWpcoDtEstrAssMag70a().getWpcoDtEstrAssMag70aNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ESTR-ASS-MAG70A-NULL
            //           TO PCO-DT-ESTR-ASS-MAG70A-NULL
            ws.getParamComp().getPcoDtEstrAssMag70a().setPcoDtEstrAssMag70aNull(ws.getLccvpco1().getDati().getWpcoDtEstrAssMag70a().getWpcoDtEstrAssMag70aNull());
        }
        else if (ws.getLccvpco1().getDati().getWpcoDtEstrAssMag70a().getWpcoDtEstrAssMag70a() == 0) {
            // COB_CODE: IF (SF)-DT-ESTR-ASS-MAG70A = ZERO
            //              TO PCO-DT-ESTR-ASS-MAG70A-NULL
            //           ELSE
            //            TO PCO-DT-ESTR-ASS-MAG70A
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO PCO-DT-ESTR-ASS-MAG70A-NULL
            ws.getParamComp().getPcoDtEstrAssMag70a().setPcoDtEstrAssMag70aNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtEstrAssMag70a.Len.PCO_DT_ESTR_ASS_MAG70A_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ESTR-ASS-MAG70A
            //           TO PCO-DT-ESTR-ASS-MAG70A
            ws.getParamComp().getPcoDtEstrAssMag70a().setPcoDtEstrAssMag70a(ws.getLccvpco1().getDati().getWpcoDtEstrAssMag70a().getWpcoDtEstrAssMag70a());
        }
    }

    /**Original name: AGGIORNA-PARAM-COMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCVPCO6
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO PARAMETRO COMPAGNIA
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN (LCCV*1)
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void aggiornaParamComp() {
        // COB_CODE: INITIALIZE PARAM-COMP.
        initParamComp();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'PARAM-COMP'                   TO WK-TABELLA.
        ws.setWkTabella("PARAM-COMP");
        //--> SE LO STATUS NON E' "INVARIATO"
        // COB_CODE: IF NOT WPCO-ST-INV
        //              AND WPCO-ELE-PAR-COMP-MAX NOT = 0
        //              END-IF
        //           END-IF.
        if (!ws.getLccvpco1().getStatus().isInv() && ws.getWpcoEleParCompMax() != 0) {
            //-->        INSERIMENTO
            //-->        CANCELLAZIONE
            // COB_CODE:         EVALUATE TRUE
            //           *-->        INSERIMENTO
            //                       WHEN WPCO-ST-ADD
            //           *-->        CANCELLAZIONE
            //                       WHEN WPCO-ST-DEL
            //           *-->        MODIFICA
            //                                  THRU EX-S0300
            //                       WHEN WPCO-ST-MOD
            //                          SET  IDSI0011-UPDATE   TO TRUE
            //                   END-EVALUATE
            switch (ws.getLccvpco1().getStatus().getStatus()) {

                case WpolStatus.ADD:
                case WpolStatus.DEL://-->        MODIFICA
                    // COB_CODE: SET IDSV0001-ESITO-KO TO TRUE
                    areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                    // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'AGGIORNA-PARAM-COMP'
                    //                              TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("AGGIORNA-PARAM-COMP");
                    // COB_CODE: MOVE '005017'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005017");
                    // COB_CODE: MOVE SPACES        TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //                   THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case WpolStatus.MOD:// COB_CODE: MOVE WPCO-COD-COMP-ANIA  TO PCO-COD-COMP-ANIA
                    ws.getParamComp().setPcoCodCompAnia(ws.getLccvpco1().getDati().getWpcoCodCompAnia());
                    //-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-UPDATE   TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Update();
                    break;

                default:break;
            }
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       VALORIZZA DCLGEN NOTE OGGETTO
            //                         THRU AGGIORNA-TABELLA-EX
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->       VALORIZZA DCLGEN NOTE OGGETTO
                // COB_CODE: PERFORM VAL-DCLGEN-PCO
                //              THRU VAL-DCLGEN-PCO-EX
                valDclgenPco();
                //-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                // COB_CODE: PERFORM VALORIZZA-AREA-DSH-NOT
                //              THRU VALORIZZA-AREA-DSH-NOT-EX
                valorizzaAreaDshNot();
                //-->       CALL DISPATCHER PER AGGIORNAMENTO
                // COB_CODE: PERFORM AGGIORNA-TABELLA
                //              THRU AGGIORNA-TABELLA-EX
                aggiornaTabella();
            }
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-NOT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> DCLGEN TABELLA</pre>*/
    private void valorizzaAreaDshNot() {
        // COB_CODE: MOVE PARAM-COMP                     TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getParamComp().getParamCompFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-PRIMARY-KEY           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET  IDSI0011-TRATT-SENZA-STOR      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    public void initWkVariabili() {
        ws.setWkTabella("");
    }

    public void initParamComp() {
        ws.getParamComp().setPcoCodCompAnia(0);
        ws.getParamComp().setPcoCodTratCirt("");
        ws.getParamComp().getPcoLimVltr().setPcoLimVltr(new AfDecimal(0, 15, 3));
        ws.getParamComp().setPcoTpRatPerf(Types.SPACE_CHAR);
        ws.getParamComp().setPcoTpLivGenzTit("");
        ws.getParamComp().getPcoArrotPre().setPcoArrotPre(new AfDecimal(0, 15, 3));
        ws.getParamComp().getPcoDtCont().setPcoDtCont(0);
        ws.getParamComp().getPcoDtUltRivalIn().setPcoDtUltRivalIn(0);
        ws.getParamComp().getPcoDtUltQtzoIn().setPcoDtUltQtzoIn(0);
        ws.getParamComp().getPcoDtUltRiclRiass().setPcoDtUltRiclRiass(0);
        ws.getParamComp().getPcoDtUltTabulRiass().setPcoDtUltTabulRiass(0);
        ws.getParamComp().getPcoDtUltBollEmes().setPcoDtUltBollEmes(0);
        ws.getParamComp().getPcoDtUltBollStor().setPcoDtUltBollStor(0);
        ws.getParamComp().getPcoDtUltBollLiq().setPcoDtUltBollLiq(0);
        ws.getParamComp().getPcoDtUltBollRiat().setPcoDtUltBollRiat(0);
        ws.getParamComp().getPcoDtUltelriscparPr().setPcoDtUltelriscparPr(0);
        ws.getParamComp().getPcoDtUltcIsIn().setPcoDtUltcIsIn(0);
        ws.getParamComp().getPcoDtUltRiclPre().setPcoDtUltRiclPre(0);
        ws.getParamComp().getPcoDtUltcMarsol().setPcoDtUltcMarsol(0);
        ws.getParamComp().getPcoDtUltcRbIn().setPcoDtUltcRbIn(0);
        ws.getParamComp().getPcoPcProv1aaAcq().setPcoPcProv1aaAcq(new AfDecimal(0, 6, 3));
        ws.getParamComp().setPcoModIntrPrest("");
        ws.getParamComp().getPcoGgMaxRecProv().setPcoGgMaxRecProv(((short)0));
        ws.getParamComp().getPcoDtUltgzTrchEIn().setPcoDtUltgzTrchEIn(0);
        ws.getParamComp().getPcoDtUltBollSnden().setPcoDtUltBollSnden(0);
        ws.getParamComp().getPcoDtUltBollSndnlq().setPcoDtUltBollSndnlq(0);
        ws.getParamComp().getPcoDtUltscElabIn().setPcoDtUltscElabIn(0);
        ws.getParamComp().getPcoDtUltscOpzIn().setPcoDtUltscOpzIn(0);
        ws.getParamComp().getPcoDtUltcBnsricIn().setPcoDtUltcBnsricIn(0);
        ws.getParamComp().getPcoDtUltcBnsfdtIn().setPcoDtUltcBnsfdtIn(0);
        ws.getParamComp().getPcoDtUltRinnGarac().setPcoDtUltRinnGarac(0);
        ws.getParamComp().getPcoDtUltgzCed().setPcoDtUltgzCed(0);
        ws.getParamComp().getPcoDtUltElabPrlcos().setPcoDtUltElabPrlcos(0);
        ws.getParamComp().getPcoDtUltRinnColl().setPcoDtUltRinnColl(0);
        ws.getParamComp().setPcoFlRvcPerf(Types.SPACE_CHAR);
        ws.getParamComp().setPcoFlRcsPoliNoperf(Types.SPACE_CHAR);
        ws.getParamComp().setPcoFlGestPlusv(Types.SPACE_CHAR);
        ws.getParamComp().getPcoDtUltRivalCl().setPcoDtUltRivalCl(0);
        ws.getParamComp().getPcoDtUltQtzoCl().setPcoDtUltQtzoCl(0);
        ws.getParamComp().getPcoDtUltcBnsricCl().setPcoDtUltcBnsricCl(0);
        ws.getParamComp().getPcoDtUltcBnsfdtCl().setPcoDtUltcBnsfdtCl(0);
        ws.getParamComp().getPcoDtUltcIsCl().setPcoDtUltcIsCl(0);
        ws.getParamComp().getPcoDtUltcRbCl().setPcoDtUltcRbCl(0);
        ws.getParamComp().getPcoDtUltgzTrchECl().setPcoDtUltgzTrchECl(0);
        ws.getParamComp().getPcoDtUltscElabCl().setPcoDtUltscElabCl(0);
        ws.getParamComp().getPcoDtUltscOpzCl().setPcoDtUltscOpzCl(0);
        ws.getParamComp().getPcoStstXRegione().setPcoStstXRegione(0);
        ws.getParamComp().getPcoDtUltgzCedColl().setPcoDtUltgzCedColl(0);
        ws.getParamComp().setPcoTpModRival("");
        ws.getParamComp().getPcoNumMmCalcMora().setPcoNumMmCalcMora(0);
        ws.getParamComp().getPcoDtUltEcRivColl().setPcoDtUltEcRivColl(0);
        ws.getParamComp().getPcoDtUltEcRivInd().setPcoDtUltEcRivInd(0);
        ws.getParamComp().getPcoDtUltEcIlColl().setPcoDtUltEcIlColl(0);
        ws.getParamComp().getPcoDtUltEcIlInd().setPcoDtUltEcIlInd(0);
        ws.getParamComp().getPcoDtUltEcUlColl().setPcoDtUltEcUlColl(0);
        ws.getParamComp().getPcoDtUltEcUlInd().setPcoDtUltEcUlInd(0);
        ws.getParamComp().getPcoAaUti().setPcoAaUti(((short)0));
        ws.getParamComp().setPcoCalcRshComun(Types.SPACE_CHAR);
        ws.getParamComp().setPcoFlLivDebug(((short)0));
        ws.getParamComp().getPcoDtUltBollPerfC().setPcoDtUltBollPerfC(0);
        ws.getParamComp().getPcoDtUltBollRspIn().setPcoDtUltBollRspIn(0);
        ws.getParamComp().getPcoDtUltBollRspCl().setPcoDtUltBollRspCl(0);
        ws.getParamComp().getPcoDtUltBollEmesI().setPcoDtUltBollEmesI(0);
        ws.getParamComp().getPcoDtUltBollStorI().setPcoDtUltBollStorI(0);
        ws.getParamComp().getPcoDtUltBollRiatI().setPcoDtUltBollRiatI(0);
        ws.getParamComp().getPcoDtUltBollSdI().setPcoDtUltBollSdI(0);
        ws.getParamComp().getPcoDtUltBollSdnlI().setPcoDtUltBollSdnlI(0);
        ws.getParamComp().getPcoDtUltBollPerfI().setPcoDtUltBollPerfI(0);
        ws.getParamComp().getPcoDtRiclRiriasCom().setPcoDtRiclRiriasCom(0);
        ws.getParamComp().getPcoDtUltElabAt92C().setPcoDtUltElabAt92C(0);
        ws.getParamComp().getPcoDtUltElabAt92I().setPcoDtUltElabAt92I(0);
        ws.getParamComp().getPcoDtUltElabAt93C().setPcoDtUltElabAt93C(0);
        ws.getParamComp().getPcoDtUltElabAt93I().setPcoDtUltElabAt93I(0);
        ws.getParamComp().getPcoDtUltElabSpeIn().setPcoDtUltElabSpeIn(0);
        ws.getParamComp().getPcoDtUltElabPrCon().setPcoDtUltElabPrCon(0);
        ws.getParamComp().getPcoDtUltBollRpCl().setPcoDtUltBollRpCl(0);
        ws.getParamComp().getPcoDtUltBollRpIn().setPcoDtUltBollRpIn(0);
        ws.getParamComp().getPcoDtUltBollPreI().setPcoDtUltBollPreI(0);
        ws.getParamComp().getPcoDtUltBollPreC().setPcoDtUltBollPreC(0);
        ws.getParamComp().getPcoDtUltcPildiMmC().setPcoDtUltcPildiMmC(0);
        ws.getParamComp().getPcoDtUltcPildiAaC().setPcoDtUltcPildiAaC(0);
        ws.getParamComp().getPcoDtUltcPildiMmI().setPcoDtUltcPildiMmI(0);
        ws.getParamComp().getPcoDtUltcPildiTrI().setPcoDtUltcPildiTrI(0);
        ws.getParamComp().getPcoDtUltcPildiAaI().setPcoDtUltcPildiAaI(0);
        ws.getParamComp().getPcoDtUltBollQuieC().setPcoDtUltBollQuieC(0);
        ws.getParamComp().getPcoDtUltBollQuieI().setPcoDtUltBollQuieI(0);
        ws.getParamComp().getPcoDtUltBollCotrI().setPcoDtUltBollCotrI(0);
        ws.getParamComp().getPcoDtUltBollCotrC().setPcoDtUltBollCotrC(0);
        ws.getParamComp().getPcoDtUltBollCoriC().setPcoDtUltBollCoriC(0);
        ws.getParamComp().getPcoDtUltBollCoriI().setPcoDtUltBollCoriI(0);
        ws.getParamComp().setPcoDsOperSql(Types.SPACE_CHAR);
        ws.getParamComp().setPcoDsVer(0);
        ws.getParamComp().setPcoDsTsCptz(0);
        ws.getParamComp().setPcoDsUtente("");
        ws.getParamComp().setPcoDsStatoElab(Types.SPACE_CHAR);
        ws.getParamComp().setPcoTpValzzDtVlt("");
        ws.getParamComp().setPcoFlFrazProvAcq(Types.SPACE_CHAR);
        ws.getParamComp().getPcoDtUltAggErogRe().setPcoDtUltAggErogRe(0);
        ws.getParamComp().getPcoPcRmMarsol().setPcoPcRmMarsol(new AfDecimal(0, 6, 3));
        ws.getParamComp().getPcoPcCSubrshMarsol().setPcoPcCSubrshMarsol(new AfDecimal(0, 6, 3));
        ws.getParamComp().setPcoCodCompIsvap("");
        ws.getParamComp().getPcoLmRisConInt().setPcoLmRisConInt(new AfDecimal(0, 6, 3));
        ws.getParamComp().getPcoLmCSubrshConIn().setPcoLmCSubrshConIn(new AfDecimal(0, 6, 3));
        ws.getParamComp().getPcoPcGarNoriskMars().setPcoPcGarNoriskMars(new AfDecimal(0, 6, 3));
        ws.getParamComp().setPcoCrz1aRatIntrPr(Types.SPACE_CHAR);
        ws.getParamComp().getPcoNumGgArrIntrPr().setPcoNumGgArrIntrPr(0);
        ws.getParamComp().setPcoFlVisualVinpg(Types.SPACE_CHAR);
        ws.getParamComp().getPcoDtUltEstrazFug().setPcoDtUltEstrazFug(0);
        ws.getParamComp().getPcoDtUltElabPrAut().setPcoDtUltElabPrAut(0);
        ws.getParamComp().getPcoDtUltElabCommef().setPcoDtUltElabCommef(0);
        ws.getParamComp().getPcoDtUltElabLiqmef().setPcoDtUltElabLiqmef(0);
        ws.getParamComp().setPcoCodFiscMef("");
        ws.getParamComp().getPcoImpAssSociale().setPcoImpAssSociale(new AfDecimal(0, 15, 3));
        ws.getParamComp().setPcoModComnzInvstSw(Types.SPACE_CHAR);
        ws.getParamComp().getPcoDtRiatRiassRsh().setPcoDtRiatRiassRsh(0);
        ws.getParamComp().getPcoDtRiatRiassComm().setPcoDtRiatRiassComm(0);
        ws.getParamComp().getPcoGgIntrRitPag().setPcoGgIntrRitPag(0);
        ws.getParamComp().getPcoDtUltRinnTac().setPcoDtUltRinnTac(0);
        ws.getParamComp().setPcoDescCompLen(((short)0));
        ws.getParamComp().setPcoDescComp("");
        ws.getParamComp().getPcoDtUltEcTcmInd().setPcoDtUltEcTcmInd(0);
        ws.getParamComp().getPcoDtUltEcTcmColl().setPcoDtUltEcTcmColl(0);
        ws.getParamComp().getPcoDtUltEcMrmInd().setPcoDtUltEcMrmInd(0);
        ws.getParamComp().getPcoDtUltEcMrmColl().setPcoDtUltEcMrmColl(0);
        ws.getParamComp().setPcoCodCompLdap("");
        ws.getParamComp().getPcoPcRidImp1382011().setPcoPcRidImp1382011(new AfDecimal(0, 6, 3));
        ws.getParamComp().getPcoPcRidImp662014().setPcoPcRidImp662014(new AfDecimal(0, 6, 3));
        ws.getParamComp().getPcoSoglAmlPreUni().setPcoSoglAmlPreUni(new AfDecimal(0, 15, 3));
        ws.getParamComp().getPcoSoglAmlPrePer().setPcoSoglAmlPrePer(new AfDecimal(0, 15, 3));
        ws.getParamComp().setPcoCodSoggFtzAssto("");
        ws.getParamComp().getPcoDtUltElabRedpro().setPcoDtUltElabRedpro(0);
        ws.getParamComp().getPcoDtUltElabTakeP().setPcoDtUltElabTakeP(0);
        ws.getParamComp().getPcoDtUltElabPaspas().setPcoDtUltElabPaspas(0);
        ws.getParamComp().getPcoSoglAmlPreSavR().setPcoSoglAmlPreSavR(new AfDecimal(0, 15, 3));
        ws.getParamComp().getPcoDtUltEstrDecCo().setPcoDtUltEstrDecCo(0);
        ws.getParamComp().getPcoDtUltElabCosAt().setPcoDtUltElabCosAt(0);
        ws.getParamComp().getPcoFrqCostiAtt().setPcoFrqCostiAtt(0);
        ws.getParamComp().getPcoDtUltElabCosSt().setPcoDtUltElabCosSt(0);
        ws.getParamComp().getPcoFrqCostiStornati().setPcoFrqCostiStornati(0);
        ws.getParamComp().getPcoDtEstrAssMin70a().setPcoDtEstrAssMin70a(0);
        ws.getParamComp().getPcoDtEstrAssMag70a().setPcoDtEstrAssMag70a(0);
    }
}
