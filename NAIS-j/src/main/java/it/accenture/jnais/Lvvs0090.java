package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0090Data;

/**Original name: LVVS0090<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0090
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA FINE PERIODO
 *                               IMPOSTA SOSTITUTIVA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0090 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0090Data ws = new Lvvs0090Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0090
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0090_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0090 getInstance() {
        return ((Lvvs0090)Programs.getInstance(Lvvs0090.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-ISO
        //                      WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        initAreaIoIso();
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
        //--> DCLGEN DI WORKING
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-CONTROLLO-DATI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //           *--> CALL MODULO PER RECUPERO DATA
        //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
        //                    END-IF
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            //--> CALL MODULO PER RECUPERO DATA
            //-->          OPERAZIONE ESEGUITA CORRETTAMENTE
            // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O =
            //                   DISO-IMPB-IS(IVVC0213-IX-TABB) -
            //                   DISO-IMPST-SOST(IVVC0213-IX-TABB)
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getDisoTabIso(ivvc0213.getIxTabb()).getLccviso1().getDati().getWisoImpbIs().getWisoImpbIs().subtract(ws.getDisoTabIso(ivvc0213.getIxTabb()).getLccviso1().getDati().getWisoImpstSost().getWisoImpstSost()), 18, 7));
            // COB_CODE: IF IVVC0213-VAL-IMP-O < 0
            //                 TO IVVC0213-VAL-IMP-O
            //           END-IF
            if (ivvc0213.getTabOutput().getValImpO().compareTo(0) < 0) {
                // COB_CODE: MOVE ZERO
                //              TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-IMPOSTA-SOST
        //                TO DISO-AREA-ISO
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasImpostaSost())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DISO-AREA-ISO
            ws.setDisoAreaIsoFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF DISO-IMPB-IS(IVVC0213-IX-TABB) NOT NUMERIC
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           ELSE
        //              END-IF
        //           END-IF.
        if (!Functions.isNumber(ws.getDisoTabIso(ivvc0213.getIxTabb()).getLccviso1().getDati().getWisoImpbIs().getWisoImpbIs())) {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'DATA-FINE-PERIODO NON NUMERICA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("DATA-FINE-PERIODO NON NUMERICA");
        }
        else if (ws.getDisoTabIso(ivvc0213.getIxTabb()).getLccviso1().getDati().getWisoImpbIs().getWisoImpbIs().compareTo(0) == 0) {
            // COB_CODE: IF DISO-IMPB-IS(IVVC0213-IX-TABB) = 0
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'DATA-FINE-PERIODO NON VALORIZZATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("DATA-FINE-PERIODO NON VALORIZZATA");
        }
        // COB_CODE: IF DISO-IMPST-SOST(IVVC0213-IX-TABB) NOT NUMERIC
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           END-IF.
        if (!Functions.isNumber(ws.getDisoTabIso(ivvc0213.getIxTabb()).getLccviso1().getDati().getWisoImpstSost().getWisoImpstSost())) {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'DATA-FINE-PERIODO NON NUMERICA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("DATA-FINE-PERIODO NON NUMERICA");
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        //
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabIso(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoIso() {
        ws.setDisoEleIsoMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0090Data.DISO_TAB_ISO_MAXOCCURS; idx0++) {
            ws.getDisoTabIso(idx0).getLccviso1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDisoTabIso(idx0).getLccviso1().setIdPtf(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoIdImpstSost(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoIdOgg().setWisoIdOgg(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoTpOgg("");
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoIdMoviCrz(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoIdMoviChiu().setWisoIdMoviChiu(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDtIniEff(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDtEndEff(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoDtIniPer().setWisoDtIniPer(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoDtEndPer().setWisoDtEndPer(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoCodCompAnia(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoImpstSost().setWisoImpstSost(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoImpbIs().setWisoImpbIs(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoAlqIs().setWisoAlqIs(new AfDecimal(0, 6, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoCodTrb("");
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoPrstzLrdAnteIs().setWisoPrstzLrdAnteIs(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoRisMatNetPrec().setWisoRisMatNetPrec(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoRisMatAnteTax().setWisoRisMatAnteTax(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoRisMatPostTax().setWisoRisMatPostTax(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoPrstzNet().setWisoPrstzNet(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoPrstzPrec().setWisoPrstzPrec(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoCumPreVers().setWisoCumPreVers(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoTpCalcImpst("");
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoImpGiaTassato(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsRiga(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsOperSql(Types.SPACE_CHAR);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsVer(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsTsIniCptz(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsTsEndCptz(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsUtente("");
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsStatoElab(Types.SPACE_CHAR);
        }
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }
}
