package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.PersDao;
import it.accenture.jnais.commons.data.to.IPers;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs1300Data;
import it.accenture.jnais.ws.Pers;
import it.accenture.jnais.ws.redefines.A25CodPersSecond;
import it.accenture.jnais.ws.redefines.A25Dt1aAtvt;
import it.accenture.jnais.ws.redefines.A25DtAcqsPers;
import it.accenture.jnais.ws.redefines.A25DtBlocCli;
import it.accenture.jnais.ws.redefines.A25DtDeadPers;
import it.accenture.jnais.ws.redefines.A25DtEndVldtPers;
import it.accenture.jnais.ws.redefines.A25DtNascCli;
import it.accenture.jnais.ws.redefines.A25DtSegnalPartner;
import it.accenture.jnais.ws.redefines.A25IdSegmentazCli;
import it.accenture.jnais.ws.redefines.A25TstamAggmRiga;
import it.accenture.jnais.ws.redefines.A25TstamEndVldt;

/**Original name: LDBS1300<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  30 OTT 2007.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs1300 extends Program implements IPers {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private PersDao persDao = new PersDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs1300Data ws = new Ldbs1300Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: PERS
    private Pers pers;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS1300_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Pers pers) {
        this.idsv0003 = idsv0003;
        this.pers = pers;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND        TO LDBV1301.
        ws.getLdbv1301().setLdbv1301Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN IDSV0003-PRIMARY-KEY
            //                 PERFORM A200-ELABORA-PK          THRU A200-EX
            //              WHEN OTHER
            //                 SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
            //           END-EVALUATE
            switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                    a200ElaboraPk();
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
                    this.idsv0003.getReturnCode().setInvalidLevelOper();
                    break;
            }
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            this.idsv0003.getReturnCode().setInvalidLevelOper();
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs1300 getInstance() {
        return ((Ldbs1300)Programs.getInstance(Ldbs1300.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS1300'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS1300");
        // COB_CODE: MOVE 'PERS'       TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("PERS");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS1300.cbl:line=140, because the code is unreachable.
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PERS
        //                ,TSTAM_INI_VLDT
        //                ,TSTAM_END_VLDT
        //                ,COD_PERS
        //                ,RIFTO_RETE
        //                ,COD_PRT_IVA
        //                ,IND_PVCY_PRSNL
        //                ,IND_PVCY_CMMRC
        //                ,IND_PVCY_INDST
        //                ,DT_NASC_CLI
        //                ,DT_ACQS_PERS
        //                ,IND_CLI
        //                ,COD_CMN
        //                ,COD_FRM_GIURD
        //                ,COD_ENTE_PUBB
        //                ,DEN_RGN_SOC
        //                ,DEN_SIG_RGN_SOC
        //                ,IND_ESE_FISC
        //                ,COD_STAT_CVL
        //                ,DEN_NOME
        //                ,DEN_COGN
        //                ,COD_FISC
        //                ,IND_SEX
        //                ,IND_CPCT_GIURD
        //                ,IND_PORT_HDCP
        //                ,COD_USER_INS
        //                ,TSTAM_INS_RIGA
        //                ,COD_USER_AGGM
        //                ,TSTAM_AGGM_RIGA
        //                ,DEN_CMN_NASC_STRN
        //                ,COD_RAMO_STGR
        //                ,COD_STGR_ATVT_UIC
        //                ,COD_RAMO_ATVT_UIC
        //                ,DT_END_VLDT_PERS
        //                ,DT_DEAD_PERS
        //                ,TP_STAT_CLI
        //                ,DT_BLOC_CLI
        //                ,COD_PERS_SECOND
        //                ,ID_SEGMENTAZ_CLI
        //                ,DT_1A_ATVT
        //             INTO
        //                :A25-ID-PERS
        //               ,:A25-TSTAM-INI-VLDT
        //               ,:A25-TSTAM-END-VLDT
        //                :IND-A25-TSTAM-END-VLDT
        //               ,:A25-COD-PERS
        //               ,:A25-RIFTO-RETE
        //                :IND-A25-RIFTO-RETE
        //               ,:A25-COD-PRT-IVA
        //                :IND-A25-COD-PRT-IVA
        //               ,:A25-IND-PVCY-PRSNL
        //                :IND-A25-IND-PVCY-PRSNL
        //               ,:A25-IND-PVCY-CMMRC
        //                :IND-A25-IND-PVCY-CMMRC
        //               ,:A25-IND-PVCY-INDST
        //                :IND-A25-IND-PVCY-INDST
        //               ,:A25-DT-NASC-CLI-DB
        //                :IND-A25-DT-NASC-CLI
        //               ,:A25-DT-ACQS-PERS-DB
        //                :IND-A25-DT-ACQS-PERS
        //               ,:A25-IND-CLI
        //                :IND-A25-IND-CLI
        //               ,:A25-COD-CMN
        //                :IND-A25-COD-CMN
        //               ,:A25-COD-FRM-GIURD
        //                :IND-A25-COD-FRM-GIURD
        //               ,:A25-COD-ENTE-PUBB
        //                :IND-A25-COD-ENTE-PUBB
        //               ,:A25-DEN-RGN-SOC-VCHAR
        //                :IND-A25-DEN-RGN-SOC
        //               ,:A25-DEN-SIG-RGN-SOC-VCHAR
        //                :IND-A25-DEN-SIG-RGN-SOC
        //               ,:A25-IND-ESE-FISC
        //                :IND-A25-IND-ESE-FISC
        //               ,:A25-COD-STAT-CVL
        //                :IND-A25-COD-STAT-CVL
        //               ,:A25-DEN-NOME-VCHAR
        //                :IND-A25-DEN-NOME
        //               ,:A25-DEN-COGN-VCHAR
        //                :IND-A25-DEN-COGN
        //               ,:A25-COD-FISC
        //                :IND-A25-COD-FISC
        //               ,:A25-IND-SEX
        //                :IND-A25-IND-SEX
        //               ,:A25-IND-CPCT-GIURD
        //                :IND-A25-IND-CPCT-GIURD
        //               ,:A25-IND-PORT-HDCP
        //                :IND-A25-IND-PORT-HDCP
        //               ,:A25-COD-USER-INS
        //               ,:A25-TSTAM-INS-RIGA
        //               ,:A25-COD-USER-AGGM
        //                :IND-A25-COD-USER-AGGM
        //               ,:A25-TSTAM-AGGM-RIGA
        //                :IND-A25-TSTAM-AGGM-RIGA
        //               ,:A25-DEN-CMN-NASC-STRN-VCHAR
        //                :IND-A25-DEN-CMN-NASC-STRN
        //               ,:A25-COD-RAMO-STGR
        //                :IND-A25-COD-RAMO-STGR
        //               ,:A25-COD-STGR-ATVT-UIC
        //                :IND-A25-COD-STGR-ATVT-UIC
        //               ,:A25-COD-RAMO-ATVT-UIC
        //                :IND-A25-COD-RAMO-ATVT-UIC
        //               ,:A25-DT-END-VLDT-PERS-DB
        //                :IND-A25-DT-END-VLDT-PERS
        //               ,:A25-DT-DEAD-PERS-DB
        //                :IND-A25-DT-DEAD-PERS
        //               ,:A25-TP-STAT-CLI
        //                :IND-A25-TP-STAT-CLI
        //               ,:A25-DT-BLOC-CLI-DB
        //                :IND-A25-DT-BLOC-CLI
        //               ,:A25-COD-PERS-SECOND
        //                :IND-A25-COD-PERS-SECOND
        //               ,:A25-ID-SEGMENTAZ-CLI
        //                :IND-A25-ID-SEGMENTAZ-CLI
        //               ,:A25-DT-1A-ATVT-DB
        //                :IND-A25-DT-1A-ATVT
        //               ,:A25-DT-SEGNAL-PARTNER-DB
        //                :IND-A25-DT-SEGNAL-PARTNER
        //             FROM PERS
        //             WHERE ID_PERS          = :LDBV1301-ID-TAB
        //               AND TSTAM_INI_VLDT  <= :LDBV1301-TIMESTAMP
        //               AND TSTAM_END_VLDT  >  :LDBV1301-TIMESTAMP
        //           END-EXEC.
        persDao.selectRec1(ws.getLdbv1301().getLdbv1301IdTab(), ws.getLdbv1301().getLdbv1301Timestamp(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-A25-TSTAM-END-VLDT = -1
        //              MOVE HIGH-VALUES TO A25-TSTAM-END-VLDT-NULL
        //           END-IF
        if (ws.getIndPers().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-TSTAM-END-VLDT-NULL
            pers.getA25TstamEndVldt().setA25TstamEndVldtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25TstamEndVldt.Len.A25_TSTAM_END_VLDT_NULL));
        }
        // COB_CODE: IF IND-A25-RIFTO-RETE = -1
        //              MOVE HIGH-VALUES TO A25-RIFTO-RETE-NULL
        //           END-IF
        if (ws.getIndPers().getTpCalcRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-RIFTO-RETE-NULL
            pers.setA25RiftoRete(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_RIFTO_RETE));
        }
        // COB_CODE: IF IND-A25-COD-PRT-IVA = -1
        //              MOVE HIGH-VALUES TO A25-COD-PRT-IVA-NULL
        //           END-IF
        if (ws.getIndPers().getUltRm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-PRT-IVA-NULL
            pers.setA25CodPrtIva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_PRT_IVA));
        }
        // COB_CODE: IF IND-A25-IND-PVCY-PRSNL = -1
        //              MOVE HIGH-VALUES TO A25-IND-PVCY-PRSNL-NULL
        //           END-IF
        if (ws.getIndPers().getDtCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-PVCY-PRSNL-NULL
            pers.setA25IndPvcyPrsnl(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-IND-PVCY-CMMRC = -1
        //              MOVE HIGH-VALUES TO A25-IND-PVCY-CMMRC-NULL
        //           END-IF
        if (ws.getIndPers().getDtElab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-PVCY-CMMRC-NULL
            pers.setA25IndPvcyCmmrc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-IND-PVCY-INDST = -1
        //              MOVE HIGH-VALUES TO A25-IND-PVCY-INDST-NULL
        //           END-IF
        if (ws.getIndPers().getRisBila() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-PVCY-INDST-NULL
            pers.setA25IndPvcyIndst(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-DT-NASC-CLI = -1
        //              MOVE HIGH-VALUES TO A25-DT-NASC-CLI-NULL
        //           END-IF
        if (ws.getIndPers().getRisMat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-NASC-CLI-NULL
            pers.getA25DtNascCli().setA25DtNascCliNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtNascCli.Len.A25_DT_NASC_CLI_NULL));
        }
        // COB_CODE: IF IND-A25-DT-ACQS-PERS = -1
        //              MOVE HIGH-VALUES TO A25-DT-ACQS-PERS-NULL
        //           END-IF
        if (ws.getIndPers().getIncrXRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-ACQS-PERS-NULL
            pers.getA25DtAcqsPers().setA25DtAcqsPersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtAcqsPers.Len.A25_DT_ACQS_PERS_NULL));
        }
        // COB_CODE: IF IND-A25-IND-CLI = -1
        //              MOVE HIGH-VALUES TO A25-IND-CLI-NULL
        //           END-IF
        if (ws.getIndPers().getRptoPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-CLI-NULL
            pers.setA25IndCli(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-COD-CMN = -1
        //              MOVE HIGH-VALUES TO A25-COD-CMN-NULL
        //           END-IF
        if (ws.getIndPers().getFrazPrePp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-CMN-NULL
            pers.setA25CodCmn(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_CMN));
        }
        // COB_CODE: IF IND-A25-COD-FRM-GIURD = -1
        //              MOVE HIGH-VALUES TO A25-COD-FRM-GIURD-NULL
        //           END-IF
        if (ws.getIndPers().getRisTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-FRM-GIURD-NULL
            pers.setA25CodFrmGiurd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_FRM_GIURD));
        }
        // COB_CODE: IF IND-A25-COD-ENTE-PUBB = -1
        //              MOVE HIGH-VALUES TO A25-COD-ENTE-PUBB-NULL
        //           END-IF
        if (ws.getIndPers().getRisSpe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-ENTE-PUBB-NULL
            pers.setA25CodEntePubb(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_ENTE_PUBB));
        }
        // COB_CODE: IF IND-A25-DEN-RGN-SOC = -1
        //              MOVE HIGH-VALUES TO A25-DEN-RGN-SOC
        //           END-IF
        if (ws.getIndPers().getRisAbb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DEN-RGN-SOC
            pers.setA25DenRgnSoc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_DEN_RGN_SOC));
        }
        // COB_CODE: IF IND-A25-DEN-SIG-RGN-SOC = -1
        //              MOVE HIGH-VALUES TO A25-DEN-SIG-RGN-SOC
        //           END-IF
        if (ws.getIndPers().getRisBnsfdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DEN-SIG-RGN-SOC
            pers.setA25DenSigRgnSoc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_DEN_SIG_RGN_SOC));
        }
        // COB_CODE: IF IND-A25-IND-ESE-FISC = -1
        //              MOVE HIGH-VALUES TO A25-IND-ESE-FISC-NULL
        //           END-IF
        if (ws.getIndPers().getRisSopr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-ESE-FISC-NULL
            pers.setA25IndEseFisc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-COD-STAT-CVL = -1
        //              MOVE HIGH-VALUES TO A25-COD-STAT-CVL-NULL
        //           END-IF
        if (ws.getIndPers().getRisIntegBasTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-STAT-CVL-NULL
            pers.setA25CodStatCvl(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_STAT_CVL));
        }
        // COB_CODE: IF IND-A25-DEN-NOME = -1
        //              MOVE HIGH-VALUES TO A25-DEN-NOME
        //           END-IF
        if (ws.getIndPers().getRisIntegDecrTs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DEN-NOME
            pers.setA25DenNome(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_DEN_NOME));
        }
        // COB_CODE: IF IND-A25-DEN-COGN = -1
        //              MOVE HIGH-VALUES TO A25-DEN-COGN
        //           END-IF
        if (ws.getIndPers().getRisGarCasoMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DEN-COGN
            pers.setA25DenCogn(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_DEN_COGN));
        }
        // COB_CODE: IF IND-A25-COD-FISC = -1
        //              MOVE HIGH-VALUES TO A25-COD-FISC-NULL
        //           END-IF
        if (ws.getIndPers().getRisZil() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-FISC-NULL
            pers.setA25CodFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_FISC));
        }
        // COB_CODE: IF IND-A25-IND-SEX = -1
        //              MOVE HIGH-VALUES TO A25-IND-SEX-NULL
        //           END-IF
        if (ws.getIndPers().getRisFaivl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-SEX-NULL
            pers.setA25IndSex(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-IND-CPCT-GIURD = -1
        //              MOVE HIGH-VALUES TO A25-IND-CPCT-GIURD-NULL
        //           END-IF
        if (ws.getIndPers().getRisCosAmmtz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-CPCT-GIURD-NULL
            pers.setA25IndCpctGiurd(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-IND-PORT-HDCP = -1
        //              MOVE HIGH-VALUES TO A25-IND-PORT-HDCP-NULL
        //           END-IF
        if (ws.getIndPers().getRisSpeFaivl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-PORT-HDCP-NULL
            pers.setA25IndPortHdcp(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-COD-USER-AGGM = -1
        //              MOVE HIGH-VALUES TO A25-COD-USER-AGGM-NULL
        //           END-IF
        if (ws.getIndPers().getRisPrestFaivl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-USER-AGGM-NULL
            pers.setA25CodUserAggm(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_USER_AGGM));
        }
        // COB_CODE: IF IND-A25-TSTAM-AGGM-RIGA = -1
        //              MOVE HIGH-VALUES TO A25-TSTAM-AGGM-RIGA-NULL
        //           END-IF
        if (ws.getIndPers().getRisComponAssva() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-TSTAM-AGGM-RIGA-NULL
            pers.getA25TstamAggmRiga().setA25TstamAggmRigaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25TstamAggmRiga.Len.A25_TSTAM_AGGM_RIGA_NULL));
        }
        // COB_CODE: IF IND-A25-DEN-CMN-NASC-STRN = -1
        //              MOVE HIGH-VALUES TO A25-DEN-CMN-NASC-STRN
        //           END-IF
        if (ws.getIndPers().getUltCoeffRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DEN-CMN-NASC-STRN
            pers.setA25DenCmnNascStrn(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_DEN_CMN_NASC_STRN));
        }
        // COB_CODE: IF IND-A25-COD-RAMO-STGR = -1
        //              MOVE HIGH-VALUES TO A25-COD-RAMO-STGR-NULL
        //           END-IF
        if (ws.getIndPers().getUltCoeffAggRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-RAMO-STGR-NULL
            pers.setA25CodRamoStgr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_RAMO_STGR));
        }
        // COB_CODE: IF IND-A25-COD-STGR-ATVT-UIC = -1
        //              MOVE HIGH-VALUES TO A25-COD-STGR-ATVT-UIC-NULL
        //           END-IF
        if (ws.getIndPers().getRisAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-STGR-ATVT-UIC-NULL
            pers.setA25CodStgrAtvtUic(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_STGR_ATVT_UIC));
        }
        // COB_CODE: IF IND-A25-COD-RAMO-ATVT-UIC = -1
        //              MOVE HIGH-VALUES TO A25-COD-RAMO-ATVT-UIC-NULL
        //           END-IF
        if (ws.getIndPers().getRisUti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-RAMO-ATVT-UIC-NULL
            pers.setA25CodRamoAtvtUic(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_RAMO_ATVT_UIC));
        }
        // COB_CODE: IF IND-A25-DT-END-VLDT-PERS = -1
        //              MOVE HIGH-VALUES TO A25-DT-END-VLDT-PERS-NULL
        //           END-IF
        if (ws.getIndPers().getRisMatEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-END-VLDT-PERS-NULL
            pers.getA25DtEndVldtPers().setA25DtEndVldtPersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtEndVldtPers.Len.A25_DT_END_VLDT_PERS_NULL));
        }
        // COB_CODE: IF IND-A25-DT-DEAD-PERS = -1
        //              MOVE HIGH-VALUES TO A25-DT-DEAD-PERS-NULL
        //           END-IF
        if (ws.getIndPers().getRisRistorniCap() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-DEAD-PERS-NULL
            pers.getA25DtDeadPers().setA25DtDeadPersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtDeadPers.Len.A25_DT_DEAD_PERS_NULL));
        }
        // COB_CODE: IF IND-A25-TP-STAT-CLI = -1
        //              MOVE HIGH-VALUES TO A25-TP-STAT-CLI-NULL
        //           END-IF
        if (ws.getIndPers().getRisTrmBns() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-TP-STAT-CLI-NULL
            pers.setA25TpStatCli(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_TP_STAT_CLI));
        }
        // COB_CODE: IF IND-A25-DT-BLOC-CLI = -1
        //              MOVE HIGH-VALUES TO A25-DT-BLOC-CLI-NULL
        //           END-IF.
        if (ws.getIndPers().getRisBnsric() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-BLOC-CLI-NULL
            pers.getA25DtBlocCli().setA25DtBlocCliNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtBlocCli.Len.A25_DT_BLOC_CLI_NULL));
        }
        // COB_CODE: IF IND-A25-COD-PERS-SECOND = -1
        //              MOVE HIGH-VALUES TO A25-COD-PERS-SECOND-NULL
        //           END-IF
        if (ws.getIndPers().getCodFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-PERS-SECOND-NULL
            pers.getA25CodPersSecond().setA25CodPersSecondNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25CodPersSecond.Len.A25_COD_PERS_SECOND_NULL));
        }
        // COB_CODE: IF IND-A25-ID-SEGMENTAZ-CLI = -1
        //              MOVE HIGH-VALUES TO A25-ID-SEGMENTAZ-CLI-NULL
        //           END-IF.
        if (ws.getIndPers().getRisMinGarto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-ID-SEGMENTAZ-CLI-NULL
            pers.getA25IdSegmentazCli().setA25IdSegmentazCliNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25IdSegmentazCli.Len.A25_ID_SEGMENTAZ_CLI_NULL));
        }
        // COB_CODE: IF IND-A25-DT-1A-ATVT = -1
        //              MOVE HIGH-VALUES TO A25-DT-1A-ATVT-NULL
        //           END-IF.
        if (ws.getIndPers().getRisRshDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-1A-ATVT-NULL
            pers.getA25Dt1aAtvt().setA25Dt1aAtvtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25Dt1aAtvt.Len.A25_DT1A_ATVT_NULL));
        }
        // COB_CODE: IF IND-A25-DT-SEGNAL-PARTNER = -1
        //              MOVE HIGH-VALUES TO A25-DT-SEGNAL-PARTNER-NULL
        //           END-IF.
        if (ws.getIndPers().getRisMoviNonInves() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-SEGNAL-PARTNER-NULL
            pers.getA25DtSegnalPartner().setA25DtSegnalPartnerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtSegnalPartner.Len.A25_DT_SEGNAL_PARTNER_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: IF IND-A25-DT-NASC-CLI = 0
        //               MOVE WS-DATE-N      TO A25-DT-NASC-CLI
        //           END-IF
        if (ws.getIndPers().getRisMat() == 0) {
            // COB_CODE: MOVE A25-DT-NASC-CLI-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getNascCliDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-NASC-CLI
            pers.getA25DtNascCli().setA25DtNascCli(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-ACQS-PERS = 0
        //               MOVE WS-DATE-N      TO A25-DT-ACQS-PERS
        //           END-IF
        if (ws.getIndPers().getIncrXRival() == 0) {
            // COB_CODE: MOVE A25-DT-ACQS-PERS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getAcqsPersDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-ACQS-PERS
            pers.getA25DtAcqsPers().setA25DtAcqsPers(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-END-VLDT-PERS = 0
        //               MOVE WS-DATE-N      TO A25-DT-END-VLDT-PERS
        //           END-IF
        if (ws.getIndPers().getRisMatEff() == 0) {
            // COB_CODE: MOVE A25-DT-END-VLDT-PERS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getEndVldtPersDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-END-VLDT-PERS
            pers.getA25DtEndVldtPers().setA25DtEndVldtPers(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-DEAD-PERS = 0
        //               MOVE WS-DATE-N      TO A25-DT-DEAD-PERS
        //           END-IF
        if (ws.getIndPers().getRisRistorniCap() == 0) {
            // COB_CODE: MOVE A25-DT-DEAD-PERS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getDeadPersDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-DEAD-PERS
            pers.getA25DtDeadPers().setA25DtDeadPers(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-BLOC-CLI = 0
        //               MOVE WS-DATE-N      TO A25-DT-BLOC-CLI
        //           END-IF.
        if (ws.getIndPers().getRisBnsric() == 0) {
            // COB_CODE: MOVE A25-DT-BLOC-CLI-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getBlocCliDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-BLOC-CLI
            pers.getA25DtBlocCli().setA25DtBlocCli(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-1A-ATVT = 0
        //               MOVE WS-DATE-N      TO A25-DT-1A-ATVT
        //           END-IF.
        if (ws.getIndPers().getRisRshDflt() == 0) {
            // COB_CODE: MOVE A25-DT-1A-ATVT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getDt1aAtvtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-1A-ATVT
            pers.getA25Dt1aAtvt().setA25Dt1aAtvt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-SEGNAL-PARTNER = 0
        //               MOVE WS-DATE-N      TO A25-DT-SEGNAL-PARTNER
        //           END-IF.
        if (ws.getIndPers().getRisMoviNonInves() == 0) {
            // COB_CODE: MOVE A25-DT-SEGNAL-PARTNER-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getSegnalPartnerDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-SEGNAL-PARTNER
            pers.getA25DtSegnalPartner().setA25DtSegnalPartner(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public String getCodCmn() {
        return pers.getA25CodCmn();
    }

    @Override
    public void setCodCmn(String codCmn) {
        this.pers.setA25CodCmn(codCmn);
    }

    @Override
    public String getCodCmnObj() {
        if (ws.getIndPers().getFrazPrePp() >= 0) {
            return getCodCmn();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodCmnObj(String codCmnObj) {
        if (codCmnObj != null) {
            setCodCmn(codCmnObj);
            ws.getIndPers().setFrazPrePp(((short)0));
        }
        else {
            ws.getIndPers().setFrazPrePp(((short)-1));
        }
    }

    @Override
    public String getCodEntePubb() {
        return pers.getA25CodEntePubb();
    }

    @Override
    public void setCodEntePubb(String codEntePubb) {
        this.pers.setA25CodEntePubb(codEntePubb);
    }

    @Override
    public String getCodEntePubbObj() {
        if (ws.getIndPers().getRisSpe() >= 0) {
            return getCodEntePubb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodEntePubbObj(String codEntePubbObj) {
        if (codEntePubbObj != null) {
            setCodEntePubb(codEntePubbObj);
            ws.getIndPers().setRisSpe(((short)0));
        }
        else {
            ws.getIndPers().setRisSpe(((short)-1));
        }
    }

    @Override
    public String getCodFisc() {
        return pers.getA25CodFisc();
    }

    @Override
    public void setCodFisc(String codFisc) {
        this.pers.setA25CodFisc(codFisc);
    }

    @Override
    public String getCodFiscObj() {
        if (ws.getIndPers().getRisZil() >= 0) {
            return getCodFisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFiscObj(String codFiscObj) {
        if (codFiscObj != null) {
            setCodFisc(codFiscObj);
            ws.getIndPers().setRisZil(((short)0));
        }
        else {
            ws.getIndPers().setRisZil(((short)-1));
        }
    }

    @Override
    public String getCodFrmGiurd() {
        return pers.getA25CodFrmGiurd();
    }

    @Override
    public void setCodFrmGiurd(String codFrmGiurd) {
        this.pers.setA25CodFrmGiurd(codFrmGiurd);
    }

    @Override
    public String getCodFrmGiurdObj() {
        if (ws.getIndPers().getRisTot() >= 0) {
            return getCodFrmGiurd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFrmGiurdObj(String codFrmGiurdObj) {
        if (codFrmGiurdObj != null) {
            setCodFrmGiurd(codFrmGiurdObj);
            ws.getIndPers().setRisTot(((short)0));
        }
        else {
            ws.getIndPers().setRisTot(((short)-1));
        }
    }

    @Override
    public long getCodPers() {
        return pers.getA25CodPers();
    }

    @Override
    public void setCodPers(long codPers) {
        this.pers.setA25CodPers(codPers);
    }

    @Override
    public long getCodPersSecond() {
        return pers.getA25CodPersSecond().getA25CodPersSecond();
    }

    @Override
    public void setCodPersSecond(long codPersSecond) {
        this.pers.getA25CodPersSecond().setA25CodPersSecond(codPersSecond);
    }

    @Override
    public Long getCodPersSecondObj() {
        if (ws.getIndPers().getCodFnd() >= 0) {
            return ((Long)getCodPersSecond());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPersSecondObj(Long codPersSecondObj) {
        if (codPersSecondObj != null) {
            setCodPersSecond(((long)codPersSecondObj));
            ws.getIndPers().setCodFnd(((short)0));
        }
        else {
            ws.getIndPers().setCodFnd(((short)-1));
        }
    }

    @Override
    public String getCodPrtIva() {
        return pers.getA25CodPrtIva();
    }

    @Override
    public void setCodPrtIva(String codPrtIva) {
        this.pers.setA25CodPrtIva(codPrtIva);
    }

    @Override
    public String getCodPrtIvaObj() {
        if (ws.getIndPers().getUltRm() >= 0) {
            return getCodPrtIva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPrtIvaObj(String codPrtIvaObj) {
        if (codPrtIvaObj != null) {
            setCodPrtIva(codPrtIvaObj);
            ws.getIndPers().setUltRm(((short)0));
        }
        else {
            ws.getIndPers().setUltRm(((short)-1));
        }
    }

    @Override
    public String getCodRamoAtvtUic() {
        return pers.getA25CodRamoAtvtUic();
    }

    @Override
    public void setCodRamoAtvtUic(String codRamoAtvtUic) {
        this.pers.setA25CodRamoAtvtUic(codRamoAtvtUic);
    }

    @Override
    public String getCodRamoAtvtUicObj() {
        if (ws.getIndPers().getRisUti() >= 0) {
            return getCodRamoAtvtUic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodRamoAtvtUicObj(String codRamoAtvtUicObj) {
        if (codRamoAtvtUicObj != null) {
            setCodRamoAtvtUic(codRamoAtvtUicObj);
            ws.getIndPers().setRisUti(((short)0));
        }
        else {
            ws.getIndPers().setRisUti(((short)-1));
        }
    }

    @Override
    public String getCodRamoStgr() {
        return pers.getA25CodRamoStgr();
    }

    @Override
    public void setCodRamoStgr(String codRamoStgr) {
        this.pers.setA25CodRamoStgr(codRamoStgr);
    }

    @Override
    public String getCodRamoStgrObj() {
        if (ws.getIndPers().getUltCoeffAggRis() >= 0) {
            return getCodRamoStgr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodRamoStgrObj(String codRamoStgrObj) {
        if (codRamoStgrObj != null) {
            setCodRamoStgr(codRamoStgrObj);
            ws.getIndPers().setUltCoeffAggRis(((short)0));
        }
        else {
            ws.getIndPers().setUltCoeffAggRis(((short)-1));
        }
    }

    @Override
    public String getCodStatCvl() {
        return pers.getA25CodStatCvl();
    }

    @Override
    public void setCodStatCvl(String codStatCvl) {
        this.pers.setA25CodStatCvl(codStatCvl);
    }

    @Override
    public String getCodStatCvlObj() {
        if (ws.getIndPers().getRisIntegBasTec() >= 0) {
            return getCodStatCvl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodStatCvlObj(String codStatCvlObj) {
        if (codStatCvlObj != null) {
            setCodStatCvl(codStatCvlObj);
            ws.getIndPers().setRisIntegBasTec(((short)0));
        }
        else {
            ws.getIndPers().setRisIntegBasTec(((short)-1));
        }
    }

    @Override
    public String getCodStgrAtvtUic() {
        return pers.getA25CodStgrAtvtUic();
    }

    @Override
    public void setCodStgrAtvtUic(String codStgrAtvtUic) {
        this.pers.setA25CodStgrAtvtUic(codStgrAtvtUic);
    }

    @Override
    public String getCodStgrAtvtUicObj() {
        if (ws.getIndPers().getRisAcq() >= 0) {
            return getCodStgrAtvtUic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodStgrAtvtUicObj(String codStgrAtvtUicObj) {
        if (codStgrAtvtUicObj != null) {
            setCodStgrAtvtUic(codStgrAtvtUicObj);
            ws.getIndPers().setRisAcq(((short)0));
        }
        else {
            ws.getIndPers().setRisAcq(((short)-1));
        }
    }

    @Override
    public String getCodUserAggm() {
        return pers.getA25CodUserAggm();
    }

    @Override
    public void setCodUserAggm(String codUserAggm) {
        this.pers.setA25CodUserAggm(codUserAggm);
    }

    @Override
    public String getCodUserAggmObj() {
        if (ws.getIndPers().getRisPrestFaivl() >= 0) {
            return getCodUserAggm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodUserAggmObj(String codUserAggmObj) {
        if (codUserAggmObj != null) {
            setCodUserAggm(codUserAggmObj);
            ws.getIndPers().setRisPrestFaivl(((short)0));
        }
        else {
            ws.getIndPers().setRisPrestFaivl(((short)-1));
        }
    }

    @Override
    public String getCodUserIns() {
        return pers.getA25CodUserIns();
    }

    @Override
    public void setCodUserIns(String codUserIns) {
        this.pers.setA25CodUserIns(codUserIns);
    }

    @Override
    public String getDenCmnNascStrnVchar() {
        return pers.getA25DenCmnNascStrnVcharFormatted();
    }

    @Override
    public void setDenCmnNascStrnVchar(String denCmnNascStrnVchar) {
        this.pers.setA25DenCmnNascStrnVcharFormatted(denCmnNascStrnVchar);
    }

    @Override
    public String getDenCmnNascStrnVcharObj() {
        if (ws.getIndPers().getUltCoeffRis() >= 0) {
            return getDenCmnNascStrnVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDenCmnNascStrnVcharObj(String denCmnNascStrnVcharObj) {
        if (denCmnNascStrnVcharObj != null) {
            setDenCmnNascStrnVchar(denCmnNascStrnVcharObj);
            ws.getIndPers().setUltCoeffRis(((short)0));
        }
        else {
            ws.getIndPers().setUltCoeffRis(((short)-1));
        }
    }

    @Override
    public String getDenCognVchar() {
        return pers.getA25DenCognVcharFormatted();
    }

    @Override
    public void setDenCognVchar(String denCognVchar) {
        this.pers.setA25DenCognVcharFormatted(denCognVchar);
    }

    @Override
    public String getDenCognVcharObj() {
        if (ws.getIndPers().getRisGarCasoMor() >= 0) {
            return getDenCognVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDenCognVcharObj(String denCognVcharObj) {
        if (denCognVcharObj != null) {
            setDenCognVchar(denCognVcharObj);
            ws.getIndPers().setRisGarCasoMor(((short)0));
        }
        else {
            ws.getIndPers().setRisGarCasoMor(((short)-1));
        }
    }

    @Override
    public String getDenNomeVchar() {
        return pers.getA25DenNomeVcharFormatted();
    }

    @Override
    public void setDenNomeVchar(String denNomeVchar) {
        this.pers.setA25DenNomeVcharFormatted(denNomeVchar);
    }

    @Override
    public String getDenNomeVcharObj() {
        if (ws.getIndPers().getRisIntegDecrTs() >= 0) {
            return getDenNomeVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDenNomeVcharObj(String denNomeVcharObj) {
        if (denNomeVcharObj != null) {
            setDenNomeVchar(denNomeVcharObj);
            ws.getIndPers().setRisIntegDecrTs(((short)0));
        }
        else {
            ws.getIndPers().setRisIntegDecrTs(((short)-1));
        }
    }

    @Override
    public String getDenRgnSocVchar() {
        return pers.getA25DenRgnSocVcharFormatted();
    }

    @Override
    public void setDenRgnSocVchar(String denRgnSocVchar) {
        this.pers.setA25DenRgnSocVcharFormatted(denRgnSocVchar);
    }

    @Override
    public String getDenRgnSocVcharObj() {
        if (ws.getIndPers().getRisAbb() >= 0) {
            return getDenRgnSocVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDenRgnSocVcharObj(String denRgnSocVcharObj) {
        if (denRgnSocVcharObj != null) {
            setDenRgnSocVchar(denRgnSocVcharObj);
            ws.getIndPers().setRisAbb(((short)0));
        }
        else {
            ws.getIndPers().setRisAbb(((short)-1));
        }
    }

    @Override
    public String getDenSigRgnSocVchar() {
        return pers.getA25DenSigRgnSocVcharFormatted();
    }

    @Override
    public void setDenSigRgnSocVchar(String denSigRgnSocVchar) {
        this.pers.setA25DenSigRgnSocVcharFormatted(denSigRgnSocVchar);
    }

    @Override
    public String getDenSigRgnSocVcharObj() {
        if (ws.getIndPers().getRisBnsfdt() >= 0) {
            return getDenSigRgnSocVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDenSigRgnSocVcharObj(String denSigRgnSocVcharObj) {
        if (denSigRgnSocVcharObj != null) {
            setDenSigRgnSocVchar(denSigRgnSocVcharObj);
            ws.getIndPers().setRisBnsfdt(((short)0));
        }
        else {
            ws.getIndPers().setRisBnsfdt(((short)-1));
        }
    }

    @Override
    public String getDt1aAtvtDb() {
        return ws.getPersDb().getDt1aAtvtDb();
    }

    @Override
    public void setDt1aAtvtDb(String dt1aAtvtDb) {
        this.ws.getPersDb().setDt1aAtvtDb(dt1aAtvtDb);
    }

    @Override
    public String getDt1aAtvtDbObj() {
        if (ws.getIndPers().getRisRshDflt() >= 0) {
            return getDt1aAtvtDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDt1aAtvtDbObj(String dt1aAtvtDbObj) {
        if (dt1aAtvtDbObj != null) {
            setDt1aAtvtDb(dt1aAtvtDbObj);
            ws.getIndPers().setRisRshDflt(((short)0));
        }
        else {
            ws.getIndPers().setRisRshDflt(((short)-1));
        }
    }

    @Override
    public String getDtAcqsPersDb() {
        return ws.getPersDb().getAcqsPersDb();
    }

    @Override
    public void setDtAcqsPersDb(String dtAcqsPersDb) {
        this.ws.getPersDb().setAcqsPersDb(dtAcqsPersDb);
    }

    @Override
    public String getDtAcqsPersDbObj() {
        if (ws.getIndPers().getIncrXRival() >= 0) {
            return getDtAcqsPersDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtAcqsPersDbObj(String dtAcqsPersDbObj) {
        if (dtAcqsPersDbObj != null) {
            setDtAcqsPersDb(dtAcqsPersDbObj);
            ws.getIndPers().setIncrXRival(((short)0));
        }
        else {
            ws.getIndPers().setIncrXRival(((short)-1));
        }
    }

    @Override
    public String getDtBlocCliDb() {
        return ws.getPersDb().getBlocCliDb();
    }

    @Override
    public void setDtBlocCliDb(String dtBlocCliDb) {
        this.ws.getPersDb().setBlocCliDb(dtBlocCliDb);
    }

    @Override
    public String getDtBlocCliDbObj() {
        if (ws.getIndPers().getRisBnsric() >= 0) {
            return getDtBlocCliDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtBlocCliDbObj(String dtBlocCliDbObj) {
        if (dtBlocCliDbObj != null) {
            setDtBlocCliDb(dtBlocCliDbObj);
            ws.getIndPers().setRisBnsric(((short)0));
        }
        else {
            ws.getIndPers().setRisBnsric(((short)-1));
        }
    }

    @Override
    public String getDtDeadPersDb() {
        return ws.getPersDb().getDeadPersDb();
    }

    @Override
    public void setDtDeadPersDb(String dtDeadPersDb) {
        this.ws.getPersDb().setDeadPersDb(dtDeadPersDb);
    }

    @Override
    public String getDtDeadPersDbObj() {
        if (ws.getIndPers().getRisRistorniCap() >= 0) {
            return getDtDeadPersDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDeadPersDbObj(String dtDeadPersDbObj) {
        if (dtDeadPersDbObj != null) {
            setDtDeadPersDb(dtDeadPersDbObj);
            ws.getIndPers().setRisRistorniCap(((short)0));
        }
        else {
            ws.getIndPers().setRisRistorniCap(((short)-1));
        }
    }

    @Override
    public String getDtEndVldtPersDb() {
        return ws.getPersDb().getEndVldtPersDb();
    }

    @Override
    public void setDtEndVldtPersDb(String dtEndVldtPersDb) {
        this.ws.getPersDb().setEndVldtPersDb(dtEndVldtPersDb);
    }

    @Override
    public String getDtEndVldtPersDbObj() {
        if (ws.getIndPers().getRisMatEff() >= 0) {
            return getDtEndVldtPersDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndVldtPersDbObj(String dtEndVldtPersDbObj) {
        if (dtEndVldtPersDbObj != null) {
            setDtEndVldtPersDb(dtEndVldtPersDbObj);
            ws.getIndPers().setRisMatEff(((short)0));
        }
        else {
            ws.getIndPers().setRisMatEff(((short)-1));
        }
    }

    @Override
    public String getDtNascCliDb() {
        return ws.getPersDb().getNascCliDb();
    }

    @Override
    public void setDtNascCliDb(String dtNascCliDb) {
        this.ws.getPersDb().setNascCliDb(dtNascCliDb);
    }

    @Override
    public String getDtNascCliDbObj() {
        if (ws.getIndPers().getRisMat() >= 0) {
            return getDtNascCliDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtNascCliDbObj(String dtNascCliDbObj) {
        if (dtNascCliDbObj != null) {
            setDtNascCliDb(dtNascCliDbObj);
            ws.getIndPers().setRisMat(((short)0));
        }
        else {
            ws.getIndPers().setRisMat(((short)-1));
        }
    }

    @Override
    public String getDtSegnalPartnerDb() {
        return ws.getPersDb().getSegnalPartnerDb();
    }

    @Override
    public void setDtSegnalPartnerDb(String dtSegnalPartnerDb) {
        this.ws.getPersDb().setSegnalPartnerDb(dtSegnalPartnerDb);
    }

    @Override
    public String getDtSegnalPartnerDbObj() {
        if (ws.getIndPers().getRisMoviNonInves() >= 0) {
            return getDtSegnalPartnerDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtSegnalPartnerDbObj(String dtSegnalPartnerDbObj) {
        if (dtSegnalPartnerDbObj != null) {
            setDtSegnalPartnerDb(dtSegnalPartnerDbObj);
            ws.getIndPers().setRisMoviNonInves(((short)0));
        }
        else {
            ws.getIndPers().setRisMoviNonInves(((short)-1));
        }
    }

    @Override
    public int getIdPers() {
        return pers.getA25IdPers();
    }

    @Override
    public void setIdPers(int idPers) {
        this.pers.setA25IdPers(idPers);
    }

    @Override
    public int getIdSegmentazCli() {
        return pers.getA25IdSegmentazCli().getA25IdSegmentazCli();
    }

    @Override
    public void setIdSegmentazCli(int idSegmentazCli) {
        this.pers.getA25IdSegmentazCli().setA25IdSegmentazCli(idSegmentazCli);
    }

    @Override
    public Integer getIdSegmentazCliObj() {
        if (ws.getIndPers().getRisMinGarto() >= 0) {
            return ((Integer)getIdSegmentazCli());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdSegmentazCliObj(Integer idSegmentazCliObj) {
        if (idSegmentazCliObj != null) {
            setIdSegmentazCli(((int)idSegmentazCliObj));
            ws.getIndPers().setRisMinGarto(((short)0));
        }
        else {
            ws.getIndPers().setRisMinGarto(((short)-1));
        }
    }

    @Override
    public char getIndCli() {
        return pers.getA25IndCli();
    }

    @Override
    public void setIndCli(char indCli) {
        this.pers.setA25IndCli(indCli);
    }

    @Override
    public Character getIndCliObj() {
        if (ws.getIndPers().getRptoPre() >= 0) {
            return ((Character)getIndCli());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndCliObj(Character indCliObj) {
        if (indCliObj != null) {
            setIndCli(((char)indCliObj));
            ws.getIndPers().setRptoPre(((short)0));
        }
        else {
            ws.getIndPers().setRptoPre(((short)-1));
        }
    }

    @Override
    public char getIndCpctGiurd() {
        return pers.getA25IndCpctGiurd();
    }

    @Override
    public void setIndCpctGiurd(char indCpctGiurd) {
        this.pers.setA25IndCpctGiurd(indCpctGiurd);
    }

    @Override
    public Character getIndCpctGiurdObj() {
        if (ws.getIndPers().getRisCosAmmtz() >= 0) {
            return ((Character)getIndCpctGiurd());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndCpctGiurdObj(Character indCpctGiurdObj) {
        if (indCpctGiurdObj != null) {
            setIndCpctGiurd(((char)indCpctGiurdObj));
            ws.getIndPers().setRisCosAmmtz(((short)0));
        }
        else {
            ws.getIndPers().setRisCosAmmtz(((short)-1));
        }
    }

    @Override
    public char getIndEseFisc() {
        return pers.getA25IndEseFisc();
    }

    @Override
    public void setIndEseFisc(char indEseFisc) {
        this.pers.setA25IndEseFisc(indEseFisc);
    }

    @Override
    public Character getIndEseFiscObj() {
        if (ws.getIndPers().getRisSopr() >= 0) {
            return ((Character)getIndEseFisc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndEseFiscObj(Character indEseFiscObj) {
        if (indEseFiscObj != null) {
            setIndEseFisc(((char)indEseFiscObj));
            ws.getIndPers().setRisSopr(((short)0));
        }
        else {
            ws.getIndPers().setRisSopr(((short)-1));
        }
    }

    @Override
    public char getIndPortHdcp() {
        return pers.getA25IndPortHdcp();
    }

    @Override
    public void setIndPortHdcp(char indPortHdcp) {
        this.pers.setA25IndPortHdcp(indPortHdcp);
    }

    @Override
    public Character getIndPortHdcpObj() {
        if (ws.getIndPers().getRisSpeFaivl() >= 0) {
            return ((Character)getIndPortHdcp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPortHdcpObj(Character indPortHdcpObj) {
        if (indPortHdcpObj != null) {
            setIndPortHdcp(((char)indPortHdcpObj));
            ws.getIndPers().setRisSpeFaivl(((short)0));
        }
        else {
            ws.getIndPers().setRisSpeFaivl(((short)-1));
        }
    }

    @Override
    public char getIndPvcyCmmrc() {
        return pers.getA25IndPvcyCmmrc();
    }

    @Override
    public void setIndPvcyCmmrc(char indPvcyCmmrc) {
        this.pers.setA25IndPvcyCmmrc(indPvcyCmmrc);
    }

    @Override
    public Character getIndPvcyCmmrcObj() {
        if (ws.getIndPers().getDtElab() >= 0) {
            return ((Character)getIndPvcyCmmrc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPvcyCmmrcObj(Character indPvcyCmmrcObj) {
        if (indPvcyCmmrcObj != null) {
            setIndPvcyCmmrc(((char)indPvcyCmmrcObj));
            ws.getIndPers().setDtElab(((short)0));
        }
        else {
            ws.getIndPers().setDtElab(((short)-1));
        }
    }

    @Override
    public char getIndPvcyIndst() {
        return pers.getA25IndPvcyIndst();
    }

    @Override
    public void setIndPvcyIndst(char indPvcyIndst) {
        this.pers.setA25IndPvcyIndst(indPvcyIndst);
    }

    @Override
    public Character getIndPvcyIndstObj() {
        if (ws.getIndPers().getRisBila() >= 0) {
            return ((Character)getIndPvcyIndst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPvcyIndstObj(Character indPvcyIndstObj) {
        if (indPvcyIndstObj != null) {
            setIndPvcyIndst(((char)indPvcyIndstObj));
            ws.getIndPers().setRisBila(((short)0));
        }
        else {
            ws.getIndPers().setRisBila(((short)-1));
        }
    }

    @Override
    public char getIndPvcyPrsnl() {
        return pers.getA25IndPvcyPrsnl();
    }

    @Override
    public void setIndPvcyPrsnl(char indPvcyPrsnl) {
        this.pers.setA25IndPvcyPrsnl(indPvcyPrsnl);
    }

    @Override
    public Character getIndPvcyPrsnlObj() {
        if (ws.getIndPers().getDtCalc() >= 0) {
            return ((Character)getIndPvcyPrsnl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPvcyPrsnlObj(Character indPvcyPrsnlObj) {
        if (indPvcyPrsnlObj != null) {
            setIndPvcyPrsnl(((char)indPvcyPrsnlObj));
            ws.getIndPers().setDtCalc(((short)0));
        }
        else {
            ws.getIndPers().setDtCalc(((short)-1));
        }
    }

    @Override
    public char getIndSex() {
        return pers.getA25IndSex();
    }

    @Override
    public void setIndSex(char indSex) {
        this.pers.setA25IndSex(indSex);
    }

    @Override
    public Character getIndSexObj() {
        if (ws.getIndPers().getRisFaivl() >= 0) {
            return ((Character)getIndSex());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndSexObj(Character indSexObj) {
        if (indSexObj != null) {
            setIndSex(((char)indSexObj));
            ws.getIndPers().setRisFaivl(((short)0));
        }
        else {
            ws.getIndPers().setRisFaivl(((short)-1));
        }
    }

    @Override
    public String getRiftoRete() {
        return pers.getA25RiftoRete();
    }

    @Override
    public void setRiftoRete(String riftoRete) {
        this.pers.setA25RiftoRete(riftoRete);
    }

    @Override
    public String getRiftoReteObj() {
        if (ws.getIndPers().getTpCalcRis() >= 0) {
            return getRiftoRete();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRiftoReteObj(String riftoReteObj) {
        if (riftoReteObj != null) {
            setRiftoRete(riftoReteObj);
            ws.getIndPers().setTpCalcRis(((short)0));
        }
        else {
            ws.getIndPers().setTpCalcRis(((short)-1));
        }
    }

    @Override
    public String getTpStatCli() {
        return pers.getA25TpStatCli();
    }

    @Override
    public void setTpStatCli(String tpStatCli) {
        this.pers.setA25TpStatCli(tpStatCli);
    }

    @Override
    public String getTpStatCliObj() {
        if (ws.getIndPers().getRisTrmBns() >= 0) {
            return getTpStatCli();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStatCliObj(String tpStatCliObj) {
        if (tpStatCliObj != null) {
            setTpStatCli(tpStatCliObj);
            ws.getIndPers().setRisTrmBns(((short)0));
        }
        else {
            ws.getIndPers().setRisTrmBns(((short)-1));
        }
    }

    @Override
    public long getTstamAggmRiga() {
        return pers.getA25TstamAggmRiga().getA25TstamAggmRiga();
    }

    @Override
    public void setTstamAggmRiga(long tstamAggmRiga) {
        this.pers.getA25TstamAggmRiga().setA25TstamAggmRiga(tstamAggmRiga);
    }

    @Override
    public Long getTstamAggmRigaObj() {
        if (ws.getIndPers().getRisComponAssva() >= 0) {
            return ((Long)getTstamAggmRiga());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTstamAggmRigaObj(Long tstamAggmRigaObj) {
        if (tstamAggmRigaObj != null) {
            setTstamAggmRiga(((long)tstamAggmRigaObj));
            ws.getIndPers().setRisComponAssva(((short)0));
        }
        else {
            ws.getIndPers().setRisComponAssva(((short)-1));
        }
    }

    @Override
    public long getTstamEndVldt() {
        return pers.getA25TstamEndVldt().getA25TstamEndVldt();
    }

    @Override
    public void setTstamEndVldt(long tstamEndVldt) {
        this.pers.getA25TstamEndVldt().setA25TstamEndVldt(tstamEndVldt);
    }

    @Override
    public Long getTstamEndVldtObj() {
        if (ws.getIndPers().getIdMoviChiu() >= 0) {
            return ((Long)getTstamEndVldt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTstamEndVldtObj(Long tstamEndVldtObj) {
        if (tstamEndVldtObj != null) {
            setTstamEndVldt(((long)tstamEndVldtObj));
            ws.getIndPers().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndPers().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public long getTstamIniVldt() {
        return pers.getA25TstamIniVldt();
    }

    @Override
    public void setTstamIniVldt(long tstamIniVldt) {
        this.pers.setA25TstamIniVldt(tstamIniVldt);
    }

    @Override
    public long getTstamInsRiga() {
        return pers.getA25TstamInsRiga();
    }

    @Override
    public void setTstamInsRiga(long tstamInsRiga) {
        this.pers.setA25TstamInsRiga(tstamInsRiga);
    }
}
