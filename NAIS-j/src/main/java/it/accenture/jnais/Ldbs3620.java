package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.TrchLiqDao;
import it.accenture.jnais.commons.data.to.ITrchLiq;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs3620Data;
import it.accenture.jnais.ws.redefines.TliComponTaxRimb;
import it.accenture.jnais.ws.redefines.TliIasMggSin;
import it.accenture.jnais.ws.redefines.TliIasOnerPrvntFin;
import it.accenture.jnais.ws.redefines.TliIasPnl;
import it.accenture.jnais.ws.redefines.TliIasRstDpst;
import it.accenture.jnais.ws.redefines.TliIdGarLiq;
import it.accenture.jnais.ws.redefines.TliIdMoviChiu;
import it.accenture.jnais.ws.redefines.TliImpLrdDfz;
import it.accenture.jnais.ws.redefines.TliImpNetDfz;
import it.accenture.jnais.ws.redefines.TliImpRimb;
import it.accenture.jnais.ws.redefines.TliImpUti;
import it.accenture.jnais.ws.redefines.TliRisMat;
import it.accenture.jnais.ws.redefines.TliRisSpe;
import it.accenture.jnais.ws.TrchLiqIdbstli0;

/**Original name: LDBS3620<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs3620 extends Program implements ITrchLiq {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private TrchLiqDao trchLiqDao = new TrchLiqDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs3620Data ws = new Ldbs3620Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: TRCH-LIQ
    private TrchLiqIdbstli0 trchLiq;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS3620_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, TrchLiqIdbstli0 trchLiq) {
        this.idsv0003 = idsv0003;
        this.trchLiq = trchLiq;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV3621.
        ws.setLdbv3621Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: MOVE LDBV3621 TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getLdbv3621Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs3620 getInstance() {
        return ((Ldbs3620)Programs.getInstance(Ldbs3620.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS3620'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS3620");
        // COB_CODE: MOVE 'TRCH-LIQ' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("TRCH-LIQ");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_TRCH_LIQ
        //                    ,ID_GAR_LIQ
        //                    ,ID_LIQ
        //                    ,ID_TRCH_DI_GAR
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IMP_LRD_CALC
        //                    ,IMP_LRD_DFZ
        //                    ,IMP_LRD_EFFLQ
        //                    ,IMP_NET_CALC
        //                    ,IMP_NET_DFZ
        //                    ,IMP_NET_EFFLQ
        //                    ,IMP_UTI
        //                    ,COD_TARI
        //                    ,COD_DVS
        //                    ,IAS_ONER_PRVNT_FIN
        //                    ,IAS_MGG_SIN
        //                    ,IAS_RST_DPST
        //                    ,IMP_RIMB
        //                    ,COMPON_TAX_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IAS_PNL
        //              FROM TRCH_LIQ
        //              WHERE  ID_MOVI_CRZ    =    :LDBV3621-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_TRCH_DI_GAR
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_TRCH_LIQ
        //                    ,ID_GAR_LIQ
        //                    ,ID_LIQ
        //                    ,ID_TRCH_DI_GAR
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IMP_LRD_CALC
        //                    ,IMP_LRD_DFZ
        //                    ,IMP_LRD_EFFLQ
        //                    ,IMP_NET_CALC
        //                    ,IMP_NET_DFZ
        //                    ,IMP_NET_EFFLQ
        //                    ,IMP_UTI
        //                    ,COD_TARI
        //                    ,COD_DVS
        //                    ,IAS_ONER_PRVNT_FIN
        //                    ,IAS_MGG_SIN
        //                    ,IAS_RST_DPST
        //                    ,IMP_RIMB
        //                    ,COMPON_TAX_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IAS_PNL
        //             INTO
        //                :TLI-ID-TRCH-LIQ
        //               ,:TLI-ID-GAR-LIQ
        //                :IND-TLI-ID-GAR-LIQ
        //               ,:TLI-ID-LIQ
        //               ,:TLI-ID-TRCH-DI-GAR
        //               ,:TLI-ID-MOVI-CRZ
        //               ,:TLI-ID-MOVI-CHIU
        //                :IND-TLI-ID-MOVI-CHIU
        //               ,:TLI-DT-INI-EFF-DB
        //               ,:TLI-DT-END-EFF-DB
        //               ,:TLI-COD-COMP-ANIA
        //               ,:TLI-IMP-LRD-CALC
        //               ,:TLI-IMP-LRD-DFZ
        //                :IND-TLI-IMP-LRD-DFZ
        //               ,:TLI-IMP-LRD-EFFLQ
        //               ,:TLI-IMP-NET-CALC
        //               ,:TLI-IMP-NET-DFZ
        //                :IND-TLI-IMP-NET-DFZ
        //               ,:TLI-IMP-NET-EFFLQ
        //               ,:TLI-IMP-UTI
        //                :IND-TLI-IMP-UTI
        //               ,:TLI-COD-TARI
        //                :IND-TLI-COD-TARI
        //               ,:TLI-COD-DVS
        //                :IND-TLI-COD-DVS
        //               ,:TLI-IAS-ONER-PRVNT-FIN
        //                :IND-TLI-IAS-ONER-PRVNT-FIN
        //               ,:TLI-IAS-MGG-SIN
        //                :IND-TLI-IAS-MGG-SIN
        //               ,:TLI-IAS-RST-DPST
        //                :IND-TLI-IAS-RST-DPST
        //               ,:TLI-IMP-RIMB
        //                :IND-TLI-IMP-RIMB
        //               ,:TLI-COMPON-TAX-RIMB
        //                :IND-TLI-COMPON-TAX-RIMB
        //               ,:TLI-RIS-MAT
        //                :IND-TLI-RIS-MAT
        //               ,:TLI-RIS-SPE
        //                :IND-TLI-RIS-SPE
        //               ,:TLI-DS-RIGA
        //               ,:TLI-DS-OPER-SQL
        //               ,:TLI-DS-VER
        //               ,:TLI-DS-TS-INI-CPTZ
        //               ,:TLI-DS-TS-END-CPTZ
        //               ,:TLI-DS-UTENTE
        //               ,:TLI-DS-STATO-ELAB
        //               ,:TLI-IAS-PNL
        //                :IND-TLI-IAS-PNL
        //             FROM TRCH_LIQ
        //             WHERE  ID_MOVI_CRZ    =    :LDBV3621-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_TRCH_DI_GAR
        //           END-EXEC.
        trchLiqDao.selectRec2(ws.getLdbv3621IdMoviCrz(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        trchLiqDao.openCEff22(ws.getLdbv3621IdMoviCrz(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        trchLiqDao.closeCEff22();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :TLI-ID-TRCH-LIQ
        //               ,:TLI-ID-GAR-LIQ
        //                :IND-TLI-ID-GAR-LIQ
        //               ,:TLI-ID-LIQ
        //               ,:TLI-ID-TRCH-DI-GAR
        //               ,:TLI-ID-MOVI-CRZ
        //               ,:TLI-ID-MOVI-CHIU
        //                :IND-TLI-ID-MOVI-CHIU
        //               ,:TLI-DT-INI-EFF-DB
        //               ,:TLI-DT-END-EFF-DB
        //               ,:TLI-COD-COMP-ANIA
        //               ,:TLI-IMP-LRD-CALC
        //               ,:TLI-IMP-LRD-DFZ
        //                :IND-TLI-IMP-LRD-DFZ
        //               ,:TLI-IMP-LRD-EFFLQ
        //               ,:TLI-IMP-NET-CALC
        //               ,:TLI-IMP-NET-DFZ
        //                :IND-TLI-IMP-NET-DFZ
        //               ,:TLI-IMP-NET-EFFLQ
        //               ,:TLI-IMP-UTI
        //                :IND-TLI-IMP-UTI
        //               ,:TLI-COD-TARI
        //                :IND-TLI-COD-TARI
        //               ,:TLI-COD-DVS
        //                :IND-TLI-COD-DVS
        //               ,:TLI-IAS-ONER-PRVNT-FIN
        //                :IND-TLI-IAS-ONER-PRVNT-FIN
        //               ,:TLI-IAS-MGG-SIN
        //                :IND-TLI-IAS-MGG-SIN
        //               ,:TLI-IAS-RST-DPST
        //                :IND-TLI-IAS-RST-DPST
        //               ,:TLI-IMP-RIMB
        //                :IND-TLI-IMP-RIMB
        //               ,:TLI-COMPON-TAX-RIMB
        //                :IND-TLI-COMPON-TAX-RIMB
        //               ,:TLI-RIS-MAT
        //                :IND-TLI-RIS-MAT
        //               ,:TLI-RIS-SPE
        //                :IND-TLI-RIS-SPE
        //               ,:TLI-DS-RIGA
        //               ,:TLI-DS-OPER-SQL
        //               ,:TLI-DS-VER
        //               ,:TLI-DS-TS-INI-CPTZ
        //               ,:TLI-DS-TS-END-CPTZ
        //               ,:TLI-DS-UTENTE
        //               ,:TLI-DS-STATO-ELAB
        //               ,:TLI-IAS-PNL
        //                :IND-TLI-IAS-PNL
        //           END-EXEC.
        trchLiqDao.fetchCEff22(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_TRCH_LIQ
        //                    ,ID_GAR_LIQ
        //                    ,ID_LIQ
        //                    ,ID_TRCH_DI_GAR
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IMP_LRD_CALC
        //                    ,IMP_LRD_DFZ
        //                    ,IMP_LRD_EFFLQ
        //                    ,IMP_NET_CALC
        //                    ,IMP_NET_DFZ
        //                    ,IMP_NET_EFFLQ
        //                    ,IMP_UTI
        //                    ,COD_TARI
        //                    ,COD_DVS
        //                    ,IAS_ONER_PRVNT_FIN
        //                    ,IAS_MGG_SIN
        //                    ,IAS_RST_DPST
        //                    ,IMP_RIMB
        //                    ,COMPON_TAX_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IAS_PNL
        //              FROM TRCH_LIQ
        //              WHERE  ID_MOVI_CRZ    =    :LDBV3621-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_TRCH_DI_GAR
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_TRCH_LIQ
        //                    ,ID_GAR_LIQ
        //                    ,ID_LIQ
        //                    ,ID_TRCH_DI_GAR
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IMP_LRD_CALC
        //                    ,IMP_LRD_DFZ
        //                    ,IMP_LRD_EFFLQ
        //                    ,IMP_NET_CALC
        //                    ,IMP_NET_DFZ
        //                    ,IMP_NET_EFFLQ
        //                    ,IMP_UTI
        //                    ,COD_TARI
        //                    ,COD_DVS
        //                    ,IAS_ONER_PRVNT_FIN
        //                    ,IAS_MGG_SIN
        //                    ,IAS_RST_DPST
        //                    ,IMP_RIMB
        //                    ,COMPON_TAX_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IAS_PNL
        //             INTO
        //                :TLI-ID-TRCH-LIQ
        //               ,:TLI-ID-GAR-LIQ
        //                :IND-TLI-ID-GAR-LIQ
        //               ,:TLI-ID-LIQ
        //               ,:TLI-ID-TRCH-DI-GAR
        //               ,:TLI-ID-MOVI-CRZ
        //               ,:TLI-ID-MOVI-CHIU
        //                :IND-TLI-ID-MOVI-CHIU
        //               ,:TLI-DT-INI-EFF-DB
        //               ,:TLI-DT-END-EFF-DB
        //               ,:TLI-COD-COMP-ANIA
        //               ,:TLI-IMP-LRD-CALC
        //               ,:TLI-IMP-LRD-DFZ
        //                :IND-TLI-IMP-LRD-DFZ
        //               ,:TLI-IMP-LRD-EFFLQ
        //               ,:TLI-IMP-NET-CALC
        //               ,:TLI-IMP-NET-DFZ
        //                :IND-TLI-IMP-NET-DFZ
        //               ,:TLI-IMP-NET-EFFLQ
        //               ,:TLI-IMP-UTI
        //                :IND-TLI-IMP-UTI
        //               ,:TLI-COD-TARI
        //                :IND-TLI-COD-TARI
        //               ,:TLI-COD-DVS
        //                :IND-TLI-COD-DVS
        //               ,:TLI-IAS-ONER-PRVNT-FIN
        //                :IND-TLI-IAS-ONER-PRVNT-FIN
        //               ,:TLI-IAS-MGG-SIN
        //                :IND-TLI-IAS-MGG-SIN
        //               ,:TLI-IAS-RST-DPST
        //                :IND-TLI-IAS-RST-DPST
        //               ,:TLI-IMP-RIMB
        //                :IND-TLI-IMP-RIMB
        //               ,:TLI-COMPON-TAX-RIMB
        //                :IND-TLI-COMPON-TAX-RIMB
        //               ,:TLI-RIS-MAT
        //                :IND-TLI-RIS-MAT
        //               ,:TLI-RIS-SPE
        //                :IND-TLI-RIS-SPE
        //               ,:TLI-DS-RIGA
        //               ,:TLI-DS-OPER-SQL
        //               ,:TLI-DS-VER
        //               ,:TLI-DS-TS-INI-CPTZ
        //               ,:TLI-DS-TS-END-CPTZ
        //               ,:TLI-DS-UTENTE
        //               ,:TLI-DS-STATO-ELAB
        //               ,:TLI-IAS-PNL
        //                :IND-TLI-IAS-PNL
        //             FROM TRCH_LIQ
        //             WHERE  ID_MOVI_CRZ    =    :LDBV3621-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_TRCH_DI_GAR
        //           END-EXEC.
        trchLiqDao.selectRec3(ws.getLdbv3621IdMoviCrz(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        trchLiqDao.openCCpz22(ws.getLdbv3621IdMoviCrz(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        trchLiqDao.closeCCpz22();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :TLI-ID-TRCH-LIQ
        //               ,:TLI-ID-GAR-LIQ
        //                :IND-TLI-ID-GAR-LIQ
        //               ,:TLI-ID-LIQ
        //               ,:TLI-ID-TRCH-DI-GAR
        //               ,:TLI-ID-MOVI-CRZ
        //               ,:TLI-ID-MOVI-CHIU
        //                :IND-TLI-ID-MOVI-CHIU
        //               ,:TLI-DT-INI-EFF-DB
        //               ,:TLI-DT-END-EFF-DB
        //               ,:TLI-COD-COMP-ANIA
        //               ,:TLI-IMP-LRD-CALC
        //               ,:TLI-IMP-LRD-DFZ
        //                :IND-TLI-IMP-LRD-DFZ
        //               ,:TLI-IMP-LRD-EFFLQ
        //               ,:TLI-IMP-NET-CALC
        //               ,:TLI-IMP-NET-DFZ
        //                :IND-TLI-IMP-NET-DFZ
        //               ,:TLI-IMP-NET-EFFLQ
        //               ,:TLI-IMP-UTI
        //                :IND-TLI-IMP-UTI
        //               ,:TLI-COD-TARI
        //                :IND-TLI-COD-TARI
        //               ,:TLI-COD-DVS
        //                :IND-TLI-COD-DVS
        //               ,:TLI-IAS-ONER-PRVNT-FIN
        //                :IND-TLI-IAS-ONER-PRVNT-FIN
        //               ,:TLI-IAS-MGG-SIN
        //                :IND-TLI-IAS-MGG-SIN
        //               ,:TLI-IAS-RST-DPST
        //                :IND-TLI-IAS-RST-DPST
        //               ,:TLI-IMP-RIMB
        //                :IND-TLI-IMP-RIMB
        //               ,:TLI-COMPON-TAX-RIMB
        //                :IND-TLI-COMPON-TAX-RIMB
        //               ,:TLI-RIS-MAT
        //                :IND-TLI-RIS-MAT
        //               ,:TLI-RIS-SPE
        //                :IND-TLI-RIS-SPE
        //               ,:TLI-DS-RIGA
        //               ,:TLI-DS-OPER-SQL
        //               ,:TLI-DS-VER
        //               ,:TLI-DS-TS-INI-CPTZ
        //               ,:TLI-DS-TS-END-CPTZ
        //               ,:TLI-DS-UTENTE
        //               ,:TLI-DS-STATO-ELAB
        //               ,:TLI-IAS-PNL
        //                :IND-TLI-IAS-PNL
        //           END-EXEC.
        trchLiqDao.fetchCCpz22(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-TLI-ID-GAR-LIQ = -1
        //              MOVE HIGH-VALUES TO TLI-ID-GAR-LIQ-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getIdOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-ID-GAR-LIQ-NULL
            trchLiq.getTliIdGarLiq().setTliIdGarLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliIdGarLiq.Len.TLI_ID_GAR_LIQ_NULL));
        }
        // COB_CODE: IF IND-TLI-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO TLI-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-ID-MOVI-CHIU-NULL
            trchLiq.getTliIdMoviChiu().setTliIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliIdMoviChiu.Len.TLI_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-TLI-IMP-LRD-DFZ = -1
        //              MOVE HIGH-VALUES TO TLI-IMP-LRD-DFZ-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getDtIniPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-IMP-LRD-DFZ-NULL
            trchLiq.getTliImpLrdDfz().setTliImpLrdDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliImpLrdDfz.Len.TLI_IMP_LRD_DFZ_NULL));
        }
        // COB_CODE: IF IND-TLI-IMP-NET-DFZ = -1
        //              MOVE HIGH-VALUES TO TLI-IMP-NET-DFZ-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getDtEndPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-IMP-NET-DFZ-NULL
            trchLiq.getTliImpNetDfz().setTliImpNetDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliImpNetDfz.Len.TLI_IMP_NET_DFZ_NULL));
        }
        // COB_CODE: IF IND-TLI-IMP-UTI = -1
        //              MOVE HIGH-VALUES TO TLI-IMP-UTI-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getImpstSost() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-IMP-UTI-NULL
            trchLiq.getTliImpUti().setTliImpUtiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliImpUti.Len.TLI_IMP_UTI_NULL));
        }
        // COB_CODE: IF IND-TLI-COD-TARI = -1
        //              MOVE HIGH-VALUES TO TLI-COD-TARI-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getImpbIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-COD-TARI-NULL
            trchLiq.setTliCodTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchLiqIdbstli0.Len.TLI_COD_TARI));
        }
        // COB_CODE: IF IND-TLI-COD-DVS = -1
        //              MOVE HIGH-VALUES TO TLI-COD-DVS-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getAlqIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-COD-DVS-NULL
            trchLiq.setTliCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchLiqIdbstli0.Len.TLI_COD_DVS));
        }
        // COB_CODE: IF IND-TLI-IAS-ONER-PRVNT-FIN = -1
        //              MOVE HIGH-VALUES TO TLI-IAS-ONER-PRVNT-FIN-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getCodTrb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-IAS-ONER-PRVNT-FIN-NULL
            trchLiq.getTliIasOnerPrvntFin().setTliIasOnerPrvntFinNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliIasOnerPrvntFin.Len.TLI_IAS_ONER_PRVNT_FIN_NULL));
        }
        // COB_CODE: IF IND-TLI-IAS-MGG-SIN = -1
        //              MOVE HIGH-VALUES TO TLI-IAS-MGG-SIN-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getPrstzLrdAnteIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-IAS-MGG-SIN-NULL
            trchLiq.getTliIasMggSin().setTliIasMggSinNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliIasMggSin.Len.TLI_IAS_MGG_SIN_NULL));
        }
        // COB_CODE: IF IND-TLI-IAS-RST-DPST = -1
        //              MOVE HIGH-VALUES TO TLI-IAS-RST-DPST-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getRisMatNetPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-IAS-RST-DPST-NULL
            trchLiq.getTliIasRstDpst().setTliIasRstDpstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliIasRstDpst.Len.TLI_IAS_RST_DPST_NULL));
        }
        // COB_CODE: IF IND-TLI-IMP-RIMB = -1
        //              MOVE HIGH-VALUES TO TLI-IMP-RIMB-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getRisMatAnteTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-IMP-RIMB-NULL
            trchLiq.getTliImpRimb().setTliImpRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliImpRimb.Len.TLI_IMP_RIMB_NULL));
        }
        // COB_CODE: IF IND-TLI-COMPON-TAX-RIMB = -1
        //              MOVE HIGH-VALUES TO TLI-COMPON-TAX-RIMB-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getRisMatPostTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-COMPON-TAX-RIMB-NULL
            trchLiq.getTliComponTaxRimb().setTliComponTaxRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliComponTaxRimb.Len.TLI_COMPON_TAX_RIMB_NULL));
        }
        // COB_CODE: IF IND-TLI-RIS-MAT = -1
        //              MOVE HIGH-VALUES TO TLI-RIS-MAT-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getPrstzNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-RIS-MAT-NULL
            trchLiq.getTliRisMat().setTliRisMatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliRisMat.Len.TLI_RIS_MAT_NULL));
        }
        // COB_CODE: IF IND-TLI-RIS-SPE = -1
        //              MOVE HIGH-VALUES TO TLI-RIS-SPE-NULL
        //           END-IF
        if (ws.getIndTrchLiq().getPrstzPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-RIS-SPE-NULL
            trchLiq.getTliRisSpe().setTliRisSpeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliRisSpe.Len.TLI_RIS_SPE_NULL));
        }
        // COB_CODE: IF IND-TLI-IAS-PNL = -1
        //              MOVE HIGH-VALUES TO TLI-IAS-PNL-NULL
        //           END-IF.
        if (ws.getIndTrchLiq().getCumPreVers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TLI-IAS-PNL-NULL
            trchLiq.getTliIasPnl().setTliIasPnlNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TliIasPnl.Len.TLI_IAS_PNL_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE TLI-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvtli3().getTliDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TLI-DT-INI-EFF
        trchLiq.setTliDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE TLI-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvtli3().getTliDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TLI-DT-END-EFF.
        trchLiq.setTliDtEndEff(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return trchLiq.getTliCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.trchLiq.setTliCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return trchLiq.getTliCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.trchLiq.setTliCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndTrchLiq().getAlqIs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndTrchLiq().setAlqIs(((short)0));
        }
        else {
            ws.getIndTrchLiq().setAlqIs(((short)-1));
        }
    }

    @Override
    public String getCodTari() {
        return trchLiq.getTliCodTari();
    }

    @Override
    public void setCodTari(String codTari) {
        this.trchLiq.setTliCodTari(codTari);
    }

    @Override
    public String getCodTariObj() {
        if (ws.getIndTrchLiq().getImpbIs() >= 0) {
            return getCodTari();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTariObj(String codTariObj) {
        if (codTariObj != null) {
            setCodTari(codTariObj);
            ws.getIndTrchLiq().setImpbIs(((short)0));
        }
        else {
            ws.getIndTrchLiq().setImpbIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getComponTaxRimb() {
        return trchLiq.getTliComponTaxRimb().getTliComponTaxRimb();
    }

    @Override
    public void setComponTaxRimb(AfDecimal componTaxRimb) {
        this.trchLiq.getTliComponTaxRimb().setTliComponTaxRimb(componTaxRimb.copy());
    }

    @Override
    public AfDecimal getComponTaxRimbObj() {
        if (ws.getIndTrchLiq().getRisMatPostTax() >= 0) {
            return getComponTaxRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setComponTaxRimbObj(AfDecimal componTaxRimbObj) {
        if (componTaxRimbObj != null) {
            setComponTaxRimb(new AfDecimal(componTaxRimbObj, 15, 3));
            ws.getIndTrchLiq().setRisMatPostTax(((short)0));
        }
        else {
            ws.getIndTrchLiq().setRisMatPostTax(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return trchLiq.getTliDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.trchLiq.setTliDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return trchLiq.getTliDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.trchLiq.setTliDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return trchLiq.getTliDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.trchLiq.setTliDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return trchLiq.getTliDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.trchLiq.setTliDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return trchLiq.getTliDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.trchLiq.setTliDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return trchLiq.getTliDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.trchLiq.setTliDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getIdbvtli3().getTliDtEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getIdbvtli3().setTliDtEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getIdbvtli3().getTliDtIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getIdbvtli3().setTliDtIniEffDb(dtIniEffDb);
    }

    @Override
    public AfDecimal getIasMggSin() {
        return trchLiq.getTliIasMggSin().getTliIasMggSin();
    }

    @Override
    public void setIasMggSin(AfDecimal iasMggSin) {
        this.trchLiq.getTliIasMggSin().setTliIasMggSin(iasMggSin.copy());
    }

    @Override
    public AfDecimal getIasMggSinObj() {
        if (ws.getIndTrchLiq().getPrstzLrdAnteIs() >= 0) {
            return getIasMggSin();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIasMggSinObj(AfDecimal iasMggSinObj) {
        if (iasMggSinObj != null) {
            setIasMggSin(new AfDecimal(iasMggSinObj, 15, 3));
            ws.getIndTrchLiq().setPrstzLrdAnteIs(((short)0));
        }
        else {
            ws.getIndTrchLiq().setPrstzLrdAnteIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getIasOnerPrvntFin() {
        return trchLiq.getTliIasOnerPrvntFin().getTliIasOnerPrvntFin();
    }

    @Override
    public void setIasOnerPrvntFin(AfDecimal iasOnerPrvntFin) {
        this.trchLiq.getTliIasOnerPrvntFin().setTliIasOnerPrvntFin(iasOnerPrvntFin.copy());
    }

    @Override
    public AfDecimal getIasOnerPrvntFinObj() {
        if (ws.getIndTrchLiq().getCodTrb() >= 0) {
            return getIasOnerPrvntFin();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIasOnerPrvntFinObj(AfDecimal iasOnerPrvntFinObj) {
        if (iasOnerPrvntFinObj != null) {
            setIasOnerPrvntFin(new AfDecimal(iasOnerPrvntFinObj, 15, 3));
            ws.getIndTrchLiq().setCodTrb(((short)0));
        }
        else {
            ws.getIndTrchLiq().setCodTrb(((short)-1));
        }
    }

    @Override
    public AfDecimal getIasPnl() {
        return trchLiq.getTliIasPnl().getTliIasPnl();
    }

    @Override
    public void setIasPnl(AfDecimal iasPnl) {
        this.trchLiq.getTliIasPnl().setTliIasPnl(iasPnl.copy());
    }

    @Override
    public AfDecimal getIasPnlObj() {
        if (ws.getIndTrchLiq().getCumPreVers() >= 0) {
            return getIasPnl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIasPnlObj(AfDecimal iasPnlObj) {
        if (iasPnlObj != null) {
            setIasPnl(new AfDecimal(iasPnlObj, 15, 3));
            ws.getIndTrchLiq().setCumPreVers(((short)0));
        }
        else {
            ws.getIndTrchLiq().setCumPreVers(((short)-1));
        }
    }

    @Override
    public AfDecimal getIasRstDpst() {
        return trchLiq.getTliIasRstDpst().getTliIasRstDpst();
    }

    @Override
    public void setIasRstDpst(AfDecimal iasRstDpst) {
        this.trchLiq.getTliIasRstDpst().setTliIasRstDpst(iasRstDpst.copy());
    }

    @Override
    public AfDecimal getIasRstDpstObj() {
        if (ws.getIndTrchLiq().getRisMatNetPrec() >= 0) {
            return getIasRstDpst();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIasRstDpstObj(AfDecimal iasRstDpstObj) {
        if (iasRstDpstObj != null) {
            setIasRstDpst(new AfDecimal(iasRstDpstObj, 15, 3));
            ws.getIndTrchLiq().setRisMatNetPrec(((short)0));
        }
        else {
            ws.getIndTrchLiq().setRisMatNetPrec(((short)-1));
        }
    }

    @Override
    public int getIdGarLiq() {
        return trchLiq.getTliIdGarLiq().getTliIdGarLiq();
    }

    @Override
    public void setIdGarLiq(int idGarLiq) {
        this.trchLiq.getTliIdGarLiq().setTliIdGarLiq(idGarLiq);
    }

    @Override
    public Integer getIdGarLiqObj() {
        if (ws.getIndTrchLiq().getIdOgg() >= 0) {
            return ((Integer)getIdGarLiq());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdGarLiqObj(Integer idGarLiqObj) {
        if (idGarLiqObj != null) {
            setIdGarLiq(((int)idGarLiqObj));
            ws.getIndTrchLiq().setIdOgg(((short)0));
        }
        else {
            ws.getIndTrchLiq().setIdOgg(((short)-1));
        }
    }

    @Override
    public int getIdLiq() {
        return trchLiq.getTliIdLiq();
    }

    @Override
    public void setIdLiq(int idLiq) {
        this.trchLiq.setTliIdLiq(idLiq);
    }

    @Override
    public int getIdMoviChiu() {
        return trchLiq.getTliIdMoviChiu().getTliIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.trchLiq.getTliIdMoviChiu().setTliIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndTrchLiq().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndTrchLiq().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndTrchLiq().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return trchLiq.getTliIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.trchLiq.setTliIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdTrchDiGar() {
        return trchLiq.getTliIdTrchDiGar();
    }

    @Override
    public void setIdTrchDiGar(int idTrchDiGar) {
        this.trchLiq.setTliIdTrchDiGar(idTrchDiGar);
    }

    @Override
    public int getIdTrchLiq() {
        return trchLiq.getTliIdTrchLiq();
    }

    @Override
    public void setIdTrchLiq(int idTrchLiq) {
        this.trchLiq.setTliIdTrchLiq(idTrchLiq);
    }

    @Override
    public AfDecimal getImpLrdCalc() {
        return trchLiq.getTliImpLrdCalc();
    }

    @Override
    public void setImpLrdCalc(AfDecimal impLrdCalc) {
        this.trchLiq.setTliImpLrdCalc(impLrdCalc.copy());
    }

    @Override
    public AfDecimal getImpLrdDfz() {
        return trchLiq.getTliImpLrdDfz().getTliImpLrdDfz();
    }

    @Override
    public void setImpLrdDfz(AfDecimal impLrdDfz) {
        this.trchLiq.getTliImpLrdDfz().setTliImpLrdDfz(impLrdDfz.copy());
    }

    @Override
    public AfDecimal getImpLrdDfzObj() {
        if (ws.getIndTrchLiq().getDtIniPer() >= 0) {
            return getImpLrdDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdDfzObj(AfDecimal impLrdDfzObj) {
        if (impLrdDfzObj != null) {
            setImpLrdDfz(new AfDecimal(impLrdDfzObj, 15, 3));
            ws.getIndTrchLiq().setDtIniPer(((short)0));
        }
        else {
            ws.getIndTrchLiq().setDtIniPer(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdEfflq() {
        return trchLiq.getTliImpLrdEfflq();
    }

    @Override
    public void setImpLrdEfflq(AfDecimal impLrdEfflq) {
        this.trchLiq.setTliImpLrdEfflq(impLrdEfflq.copy());
    }

    @Override
    public AfDecimal getImpNetCalc() {
        return trchLiq.getTliImpNetCalc();
    }

    @Override
    public void setImpNetCalc(AfDecimal impNetCalc) {
        this.trchLiq.setTliImpNetCalc(impNetCalc.copy());
    }

    @Override
    public AfDecimal getImpNetDfz() {
        return trchLiq.getTliImpNetDfz().getTliImpNetDfz();
    }

    @Override
    public void setImpNetDfz(AfDecimal impNetDfz) {
        this.trchLiq.getTliImpNetDfz().setTliImpNetDfz(impNetDfz.copy());
    }

    @Override
    public AfDecimal getImpNetDfzObj() {
        if (ws.getIndTrchLiq().getDtEndPer() >= 0) {
            return getImpNetDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpNetDfzObj(AfDecimal impNetDfzObj) {
        if (impNetDfzObj != null) {
            setImpNetDfz(new AfDecimal(impNetDfzObj, 15, 3));
            ws.getIndTrchLiq().setDtEndPer(((short)0));
        }
        else {
            ws.getIndTrchLiq().setDtEndPer(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpNetEfflq() {
        return trchLiq.getTliImpNetEfflq();
    }

    @Override
    public void setImpNetEfflq(AfDecimal impNetEfflq) {
        this.trchLiq.setTliImpNetEfflq(impNetEfflq.copy());
    }

    @Override
    public AfDecimal getImpRimb() {
        return trchLiq.getTliImpRimb().getTliImpRimb();
    }

    @Override
    public void setImpRimb(AfDecimal impRimb) {
        this.trchLiq.getTliImpRimb().setTliImpRimb(impRimb.copy());
    }

    @Override
    public AfDecimal getImpRimbObj() {
        if (ws.getIndTrchLiq().getRisMatAnteTax() >= 0) {
            return getImpRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRimbObj(AfDecimal impRimbObj) {
        if (impRimbObj != null) {
            setImpRimb(new AfDecimal(impRimbObj, 15, 3));
            ws.getIndTrchLiq().setRisMatAnteTax(((short)0));
        }
        else {
            ws.getIndTrchLiq().setRisMatAnteTax(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpUti() {
        return trchLiq.getTliImpUti().getTliImpUti();
    }

    @Override
    public void setImpUti(AfDecimal impUti) {
        this.trchLiq.getTliImpUti().setTliImpUti(impUti.copy());
    }

    @Override
    public AfDecimal getImpUtiObj() {
        if (ws.getIndTrchLiq().getImpstSost() >= 0) {
            return getImpUti();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpUtiObj(AfDecimal impUtiObj) {
        if (impUtiObj != null) {
            setImpUti(new AfDecimal(impUtiObj, 15, 3));
            ws.getIndTrchLiq().setImpstSost(((short)0));
        }
        else {
            ws.getIndTrchLiq().setImpstSost(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMat() {
        return trchLiq.getTliRisMat().getTliRisMat();
    }

    @Override
    public void setRisMat(AfDecimal risMat) {
        this.trchLiq.getTliRisMat().setTliRisMat(risMat.copy());
    }

    @Override
    public AfDecimal getRisMatObj() {
        if (ws.getIndTrchLiq().getPrstzNet() >= 0) {
            return getRisMat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatObj(AfDecimal risMatObj) {
        if (risMatObj != null) {
            setRisMat(new AfDecimal(risMatObj, 15, 3));
            ws.getIndTrchLiq().setPrstzNet(((short)0));
        }
        else {
            ws.getIndTrchLiq().setPrstzNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisSpe() {
        return trchLiq.getTliRisSpe().getTliRisSpe();
    }

    @Override
    public void setRisSpe(AfDecimal risSpe) {
        this.trchLiq.getTliRisSpe().setTliRisSpe(risSpe.copy());
    }

    @Override
    public AfDecimal getRisSpeObj() {
        if (ws.getIndTrchLiq().getPrstzPrec() >= 0) {
            return getRisSpe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisSpeObj(AfDecimal risSpeObj) {
        if (risSpeObj != null) {
            setRisSpe(new AfDecimal(risSpeObj, 15, 3));
            ws.getIndTrchLiq().setPrstzPrec(((short)0));
        }
        else {
            ws.getIndTrchLiq().setPrstzPrec(((short)-1));
        }
    }

    @Override
    public long getTliDsRiga() {
        return trchLiq.getTliDsRiga();
    }

    @Override
    public void setTliDsRiga(long tliDsRiga) {
        this.trchLiq.setTliDsRiga(tliDsRiga);
    }
}
