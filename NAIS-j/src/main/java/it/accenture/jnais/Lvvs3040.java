package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs3040Data;

/**Original name: LVVS3040<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2014.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS3040                                      *
 *   TIPOLOGIA...... SERVIZIO                                      *
 *   PROCESSO....... XXX                                           *
 *   FUNZIONE....... XXX                                           *
 *   DESCRIZIONE.... MODULO CALCOLO NUOVA VARIABILE CUMPREVERS     *
 * **------------------------------------------------------------***</pre>*/
public class Lvvs3040 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs3040Data ws = new Lvvs3040Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS3040
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS3040_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs3040 getInstance() {
        return ((Lvvs3040)Programs.getInstance(Lvvs3040.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE LDBVF111.
        initLdbvf111();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE                                                *
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU A000-EX
        //           END-IF
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A000-PREPARA-CALL-LDBSF110
            //              THRU A000-EX
            a000PreparaCallLdbsf110();
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU A010-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A010-CALL-LDBSF110
            //              THRU A010-EX
            a010CallLdbsf110();
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE            *
	 *     RISPETTIVE AREE DCLGEN IN WORKING                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-TRCH-GAR
        //                TO DTGA-AREA-TRANCHE
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTrchGar())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DTGA-AREA-TRANCHE
            ws.setDtgaAreaTrancheFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: A000-PREPARA-CALL-LDBSF110<br>
	 * <pre>----------------------------------------------------------------*
	 *   PREPARA CHIAMATA AL MODULO LDBSF110                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void a000PreparaCallLdbsf110() {
        // COB_CODE: MOVE ZEROES                     TO IVVC0213-VAL-IMP-O.
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        //
        // COB_CODE: INITIALIZE LDBVF111.
        initLdbvf111();
        //
        // COB_CODE: SET IDSV0003-SELECT             TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: MOVE DTGA-ID-POLI               TO LDBVF111-ID-OGG.
        ws.getLdbvf111().getInput().setIdOgg(ws.getLccvtga1().getDati().getWtgaIdPoli());
        // COB_CODE: MOVE 'EM'                       TO LDBVF111-TP-STAT-TIT1.
        ws.getLdbvf111().getInput().setTpStatTit1("EM");
        // COB_CODE: MOVE 'IN'                       TO LDBVF111-TP-STAT-TIT2.
        ws.getLdbvf111().getInput().setTpStatTit2("IN");
        // COB_CODE: MOVE SPACES                     TO LDBVF111-TP-STAT-TIT3
        //                                              LDBVF111-TP-STAT-TIT4
        //                                              LDBVF111-TP-STAT-TIT5.
        ws.getLdbvf111().getInput().setTpStatTit3("");
        ws.getLdbvf111().getInput().setTpStatTit4("");
        ws.getLdbvf111().getInput().setTpStatTit5("");
        // COB_CODE: MOVE DTGA-DT-DECOR              TO LDBVF111-DT-DECOR-TRCH.
        ws.getLdbvf111().getInput().setDtDecorTrch(ws.getLccvtga1().getDati().getWtgaDtDecor());
        // COB_CODE: MOVE LDBVF111
        //             TO IDSV0003-BUFFER-WHERE-COND.
        idsv0003.setBufferWhereCond(ws.getLdbvf111().getLdbvf111Formatted());
    }

    /**Original name: A010-CALL-LDBSF110<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLO NUOVA VARIABILE CUMPREVERS                            *
	 *   === SOMMA TOT-PRE-TOT CON DATA INIZIO COPERTURA MINORE ===    *
	 *   === DT-DECOR-TRCH E TITOLO IN STATO EMESSO O INCASSATO ===    *
	 * ----------------------------------------------------------------*</pre>*/
    private void a010CallLdbsf110() {
        Ldbsf110 ldbsf110 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: CALL WK-PGM-CALLED  USING  IDSV0003 LDBVF111
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            ldbsf110 = Ldbsf110.getInstance();
            ldbsf110.run(idsv0003, ws.getLdbvf111());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM-CALLED
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCalled());
            // COB_CODE: MOVE 'CALL LDBSF110 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL LDBSF110 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE LDBVF111-CUM-PRE-VERS  TO IVVC0213-VAL-IMP-O
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LDBVF111-CUM-PRE-VERS  TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLdbvf111().getCumPreVers(), 18, 7));
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           ELSE
            //              END-STRING
            //           END-IF
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM-CALLED       TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCalled());
            // COB_CODE: STRING 'CHIAMATA LDBSF110 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBSF110 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbvf111() {
        ws.getLdbvf111().getInput().setIdOgg(0);
        ws.getLdbvf111().getInput().setTpStatTit1("");
        ws.getLdbvf111().getInput().setTpStatTit2("");
        ws.getLdbvf111().getInput().setTpStatTit3("");
        ws.getLdbvf111().getInput().setTpStatTit4("");
        ws.getLdbvf111().getInput().setTpStatTit5("");
        ws.getLdbvf111().getInput().setDtDecorTrch(0);
        ws.getLdbvf111().setCumPreVers(new AfDecimal(0, 15, 3));
    }
}
