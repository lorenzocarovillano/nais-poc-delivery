package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.AmmbFunzDao;
import it.accenture.jnais.commons.data.to.IAmmbFunz;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.AmmbFunzFunz;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs2440Data;

/**Original name: LDBS2440<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  07 GIU 2018.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs2440 extends Program implements IAmmbFunz {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private AmmbFunzDao ammbFunzDao = new AmmbFunzDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs2440Data ws = new Ldbs2440Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: AMMB-FUNZ-FUNZ
    private AmmbFunzFunz ammbFunzFunz;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS2440_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, AmmbFunzFunz ammbFunzFunz) {
        this.idsv0003 = idsv0003;
        this.ammbFunzFunz = ammbFunzFunz;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV2441.
        ws.getLdbv2441().setLdbv2441Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: MOVE LDBV2441 TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getLdbv2441().getLdbv2441Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs2440 getInstance() {
        return ((Ldbs2440)Programs.getInstance(Ldbs2440.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS2440'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS2440");
        // COB_CODE: MOVE 'AMMB-FUNZ-FUNZ' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("AMMB-FUNZ-FUNZ");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-NST CURSOR FOR
        //              SELECT
        //                     COD_COMP_ANIA
        //                    ,TP_MOVI_ESEC
        //                    ,TP_MOVI_RIFTO
        //                    ,GRAV_FUNZ_FUNZ
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,COD_BLOCCO
        //                    ,SRVZ_VER_ANN
        //                    ,WHERE_CONDITION
        //                    ,FL_POLI_IFP
        //              FROM AMMB_FUNZ_FUNZ
        //              WHERE  TP_MOVI_ESEC = :L05-TP-MOVI-ESEC
        //                        AND TP_MOVI_RIFTO = :L05-TP-MOVI-RIFTO
        //                        AND GRAV_FUNZ_FUNZ IN
        //                        (:LDBV2441-GRAV-FUNZ-FUNZ-1,
        //                         :LDBV2441-GRAV-FUNZ-FUNZ-2,
        //                         :LDBV2441-GRAV-FUNZ-FUNZ-3,
        //                         :LDBV2441-GRAV-FUNZ-FUNZ-4,
        //                         :LDBV2441-GRAV-FUNZ-FUNZ-5)
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     COD_COMP_ANIA
        //                    ,TP_MOVI_ESEC
        //                    ,TP_MOVI_RIFTO
        //                    ,GRAV_FUNZ_FUNZ
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,COD_BLOCCO
        //                    ,SRVZ_VER_ANN
        //                    ,WHERE_CONDITION
        //                    ,FL_POLI_IFP
        //             INTO
        //                :L05-COD-COMP-ANIA
        //               ,:L05-TP-MOVI-ESEC
        //               ,:L05-TP-MOVI-RIFTO
        //               ,:L05-GRAV-FUNZ-FUNZ
        //               ,:L05-DS-OPER-SQL
        //               ,:L05-DS-VER
        //               ,:L05-DS-TS-CPTZ
        //               ,:L05-DS-UTENTE
        //               ,:L05-DS-STATO-ELAB
        //               ,:L05-COD-BLOCCO
        //                :IND-L05-COD-BLOCCO
        //               ,:L05-SRVZ-VER-ANN
        //                :IND-L05-SRVZ-VER-ANN
        //               ,:L05-WHERE-CONDITION-VCHAR
        //                :IND-L05-WHERE-CONDITION
        //               ,:L05-FL-POLI-IFP
        //                :IND-L05-FL-POLI-IFP
        //             FROM AMMB_FUNZ_FUNZ
        //             WHERE  TP_MOVI_ESEC = :L05-TP-MOVI-ESEC
        //                    AND TP_MOVI_RIFTO = :L05-TP-MOVI-RIFTO
        //                    AND GRAV_FUNZ_FUNZ IN
        //                    (:LDBV2441-GRAV-FUNZ-FUNZ-1,
        //                     :LDBV2441-GRAV-FUNZ-FUNZ-2,
        //                     :LDBV2441-GRAV-FUNZ-FUNZ-3,
        //                     :LDBV2441-GRAV-FUNZ-FUNZ-4,
        //                     :LDBV2441-GRAV-FUNZ-FUNZ-5)
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        ammbFunzDao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: EXEC SQL
        //                OPEN C-NST
        //           END-EXEC.
        ammbFunzDao.openCNst6(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-NST
        //           END-EXEC.
        ammbFunzDao.closeCNst6();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: EXEC SQL
        //                FETCH C-NST
        //           INTO
        //                :L05-COD-COMP-ANIA
        //               ,:L05-TP-MOVI-ESEC
        //               ,:L05-TP-MOVI-RIFTO
        //               ,:L05-GRAV-FUNZ-FUNZ
        //               ,:L05-DS-OPER-SQL
        //               ,:L05-DS-VER
        //               ,:L05-DS-TS-CPTZ
        //               ,:L05-DS-UTENTE
        //               ,:L05-DS-STATO-ELAB
        //               ,:L05-COD-BLOCCO
        //                :IND-L05-COD-BLOCCO
        //               ,:L05-SRVZ-VER-ANN
        //                :IND-L05-SRVZ-VER-ANN
        //               ,:L05-WHERE-CONDITION-VCHAR
        //                :IND-L05-WHERE-CONDITION
        //               ,:L05-FL-POLI-IFP
        //                :IND-L05-FL-POLI-IFP
        //           END-EXEC.
        ammbFunzDao.fetchCNst6(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST THRU C270-EX
            c270CloseCursorWcNst();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-L05-COD-BLOCCO = -1
        //              MOVE HIGH-VALUES TO L05-COD-BLOCCO-NULL
        //           END-IF
        if (ws.getIndAmmbFunzFunz().getCodBlocco() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L05-COD-BLOCCO-NULL
            ammbFunzFunz.setCodBlocco(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AmmbFunzFunz.Len.COD_BLOCCO));
        }
        // COB_CODE: IF IND-L05-SRVZ-VER-ANN = -1
        //              MOVE HIGH-VALUES TO L05-SRVZ-VER-ANN-NULL
        //           END-IF
        if (ws.getIndAmmbFunzFunz().getSrvzVerAnn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L05-SRVZ-VER-ANN-NULL
            ammbFunzFunz.setSrvzVerAnn(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AmmbFunzFunz.Len.SRVZ_VER_ANN));
        }
        // COB_CODE: IF IND-L05-WHERE-CONDITION = -1
        //              MOVE HIGH-VALUES TO L05-WHERE-CONDITION
        //           END-IF
        if (ws.getIndAmmbFunzFunz().getWhereCondition() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L05-WHERE-CONDITION
            ammbFunzFunz.setWhereCondition(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AmmbFunzFunz.Len.WHERE_CONDITION));
        }
        // COB_CODE: IF IND-L05-FL-POLI-IFP = -1
        //              MOVE HIGH-VALUES TO L05-FL-POLI-IFP-NULL
        //           END-IF.
        if (ws.getIndAmmbFunzFunz().getFlPoliIfp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L05-FL-POLI-IFP-NULL
            ammbFunzFunz.setFlPoliIfp(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF L05-WHERE-CONDITION
        //                       TO L05-WHERE-CONDITION-LEN.
        ammbFunzFunz.setWhereConditionLen(((short)AmmbFunzFunz.Len.WHERE_CONDITION));
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    @Override
    public String getCodBlocco() {
        return ammbFunzFunz.getCodBlocco();
    }

    @Override
    public void setCodBlocco(String codBlocco) {
        this.ammbFunzFunz.setCodBlocco(codBlocco);
    }

    @Override
    public String getCodBloccoObj() {
        if (ws.getIndAmmbFunzFunz().getCodBlocco() >= 0) {
            return getCodBlocco();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodBloccoObj(String codBloccoObj) {
        if (codBloccoObj != null) {
            setCodBlocco(codBloccoObj);
            ws.getIndAmmbFunzFunz().setCodBlocco(((short)0));
        }
        else {
            ws.getIndAmmbFunzFunz().setCodBlocco(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return ammbFunzFunz.getCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.ammbFunzFunz.setCodCompAnia(codCompAnia);
    }

    @Override
    public char getDsOperSql() {
        return ammbFunzFunz.getDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.ammbFunzFunz.setDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return ammbFunzFunz.getDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.ammbFunzFunz.setDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return ammbFunzFunz.getDsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.ammbFunzFunz.setDsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return ammbFunzFunz.getDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.ammbFunzFunz.setDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return ammbFunzFunz.getDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.ammbFunzFunz.setDsVer(dsVer);
    }

    @Override
    public char getFlPoliIfp() {
        return ammbFunzFunz.getFlPoliIfp();
    }

    @Override
    public void setFlPoliIfp(char flPoliIfp) {
        this.ammbFunzFunz.setFlPoliIfp(flPoliIfp);
    }

    @Override
    public Character getFlPoliIfpObj() {
        if (ws.getIndAmmbFunzFunz().getFlPoliIfp() >= 0) {
            return ((Character)getFlPoliIfp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPoliIfpObj(Character flPoliIfpObj) {
        if (flPoliIfpObj != null) {
            setFlPoliIfp(((char)flPoliIfpObj));
            ws.getIndAmmbFunzFunz().setFlPoliIfp(((short)0));
        }
        else {
            ws.getIndAmmbFunzFunz().setFlPoliIfp(((short)-1));
        }
    }

    @Override
    public String getGravFunzFunz() {
        return ammbFunzFunz.getGravFunzFunz();
    }

    @Override
    public void setGravFunzFunz(String gravFunzFunz) {
        this.ammbFunzFunz.setGravFunzFunz(gravFunzFunz);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public int getL05TpMoviEsec() {
        return ammbFunzFunz.getTpMoviEsec();
    }

    @Override
    public void setL05TpMoviEsec(int l05TpMoviEsec) {
        this.ammbFunzFunz.setTpMoviEsec(l05TpMoviEsec);
    }

    @Override
    public int getL05TpMoviRifto() {
        return ammbFunzFunz.getTpMoviRifto();
    }

    @Override
    public void setL05TpMoviRifto(int l05TpMoviRifto) {
        this.ammbFunzFunz.setTpMoviRifto(l05TpMoviRifto);
    }

    @Override
    public String getLdbv2441GravFunzFunz1() {
        return ws.getLdbv2441().getFunz1();
    }

    @Override
    public void setLdbv2441GravFunzFunz1(String ldbv2441GravFunzFunz1) {
        this.ws.getLdbv2441().setFunz1(ldbv2441GravFunzFunz1);
    }

    @Override
    public String getLdbv2441GravFunzFunz2() {
        return ws.getLdbv2441().getFunz2();
    }

    @Override
    public void setLdbv2441GravFunzFunz2(String ldbv2441GravFunzFunz2) {
        this.ws.getLdbv2441().setFunz2(ldbv2441GravFunzFunz2);
    }

    @Override
    public String getLdbv2441GravFunzFunz3() {
        return ws.getLdbv2441().getFunz3();
    }

    @Override
    public void setLdbv2441GravFunzFunz3(String ldbv2441GravFunzFunz3) {
        this.ws.getLdbv2441().setFunz3(ldbv2441GravFunzFunz3);
    }

    @Override
    public String getLdbv2441GravFunzFunz4() {
        return ws.getLdbv2441().getFunz4();
    }

    @Override
    public void setLdbv2441GravFunzFunz4(String ldbv2441GravFunzFunz4) {
        this.ws.getLdbv2441().setFunz4(ldbv2441GravFunzFunz4);
    }

    @Override
    public String getLdbv2441GravFunzFunz5() {
        return ws.getLdbv2441().getFunz5();
    }

    @Override
    public void setLdbv2441GravFunzFunz5(String ldbv2441GravFunzFunz5) {
        this.ws.getLdbv2441().setFunz5(ldbv2441GravFunzFunz5);
    }

    @Override
    public char getLdbvg781FlPoliIfp() {
        throw new FieldNotMappedException("ldbvg781FlPoliIfp");
    }

    @Override
    public void setLdbvg781FlPoliIfp(char ldbvg781FlPoliIfp) {
        throw new FieldNotMappedException("ldbvg781FlPoliIfp");
    }

    @Override
    public String getLdbvg781GravFunzFunz1() {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz1");
    }

    @Override
    public void setLdbvg781GravFunzFunz1(String ldbvg781GravFunzFunz1) {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz1");
    }

    @Override
    public String getLdbvg781GravFunzFunz2() {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz2");
    }

    @Override
    public void setLdbvg781GravFunzFunz2(String ldbvg781GravFunzFunz2) {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz2");
    }

    @Override
    public String getLdbvg781GravFunzFunz3() {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz3");
    }

    @Override
    public void setLdbvg781GravFunzFunz3(String ldbvg781GravFunzFunz3) {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz3");
    }

    @Override
    public String getLdbvg781GravFunzFunz4() {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz4");
    }

    @Override
    public void setLdbvg781GravFunzFunz4(String ldbvg781GravFunzFunz4) {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz4");
    }

    @Override
    public String getLdbvg781GravFunzFunz5() {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz5");
    }

    @Override
    public void setLdbvg781GravFunzFunz5(String ldbvg781GravFunzFunz5) {
        throw new FieldNotMappedException("ldbvg781GravFunzFunz5");
    }

    @Override
    public String getSrvzVerAnn() {
        return ammbFunzFunz.getSrvzVerAnn();
    }

    @Override
    public void setSrvzVerAnn(String srvzVerAnn) {
        this.ammbFunzFunz.setSrvzVerAnn(srvzVerAnn);
    }

    @Override
    public String getSrvzVerAnnObj() {
        if (ws.getIndAmmbFunzFunz().getSrvzVerAnn() >= 0) {
            return getSrvzVerAnn();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSrvzVerAnnObj(String srvzVerAnnObj) {
        if (srvzVerAnnObj != null) {
            setSrvzVerAnn(srvzVerAnnObj);
            ws.getIndAmmbFunzFunz().setSrvzVerAnn(((short)0));
        }
        else {
            ws.getIndAmmbFunzFunz().setSrvzVerAnn(((short)-1));
        }
    }

    @Override
    public String getWhereConditionVchar() {
        return ammbFunzFunz.getWhereConditionVcharFormatted();
    }

    @Override
    public void setWhereConditionVchar(String whereConditionVchar) {
        this.ammbFunzFunz.setWhereConditionVcharFormatted(whereConditionVchar);
    }

    @Override
    public String getWhereConditionVcharObj() {
        if (ws.getIndAmmbFunzFunz().getWhereCondition() >= 0) {
            return getWhereConditionVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setWhereConditionVcharObj(String whereConditionVcharObj) {
        if (whereConditionVcharObj != null) {
            setWhereConditionVchar(whereConditionVcharObj);
            ws.getIndAmmbFunzFunz().setWhereCondition(((short)0));
        }
        else {
            ws.getIndAmmbFunzFunz().setWhereCondition(((short)-1));
        }
    }
}
