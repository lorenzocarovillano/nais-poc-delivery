package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ParamMoviDao;
import it.accenture.jnais.commons.data.to.IParamMovi;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbspmo0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.ParamMoviLdbs1470;
import it.accenture.jnais.ws.redefines.PmoAaRenCer;
import it.accenture.jnais.ws.redefines.PmoCosOner;
import it.accenture.jnais.ws.redefines.PmoDtRicorPrec;
import it.accenture.jnais.ws.redefines.PmoDtRicorSucc;
import it.accenture.jnais.ws.redefines.PmoDtUltErogManfee;
import it.accenture.jnais.ws.redefines.PmoDurAa;
import it.accenture.jnais.ws.redefines.PmoDurGg;
import it.accenture.jnais.ws.redefines.PmoDurMm;
import it.accenture.jnais.ws.redefines.PmoEtaAaSoglBnficr;
import it.accenture.jnais.ws.redefines.PmoFrqMovi;
import it.accenture.jnais.ws.redefines.PmoIdAdes;
import it.accenture.jnais.ws.redefines.PmoIdMoviChiu;
import it.accenture.jnais.ws.redefines.PmoImpBnsDaSco;
import it.accenture.jnais.ws.redefines.PmoImpBnsDaScoTot;
import it.accenture.jnais.ws.redefines.PmoImpLrdDiRat;
import it.accenture.jnais.ws.redefines.PmoImpRatManfee;
import it.accenture.jnais.ws.redefines.PmoImpRiscParzPrgt;
import it.accenture.jnais.ws.redefines.PmoMmDiff;
import it.accenture.jnais.ws.redefines.PmoNumRatPagPre;
import it.accenture.jnais.ws.redefines.PmoPcAnticBns;
import it.accenture.jnais.ws.redefines.PmoPcApplzOpz;
import it.accenture.jnais.ws.redefines.PmoPcIntrFraz;
import it.accenture.jnais.ws.redefines.PmoPcRevrsb;
import it.accenture.jnais.ws.redefines.PmoPcServVal;
import it.accenture.jnais.ws.redefines.PmoSomAsstaGarac;
import it.accenture.jnais.ws.redefines.PmoSpePc;
import it.accenture.jnais.ws.redefines.PmoTotAaGiaPror;
import it.accenture.jnais.ws.redefines.PmoTpMovi;
import it.accenture.jnais.ws.redefines.PmoUltPcPerd;

/**Original name: IDBSPMO0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  17 FEB 2015.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbspmo0 extends Program implements IParamMovi {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ParamMoviDao paramMoviDao = new ParamMoviDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbspmo0Data ws = new Idbspmo0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: PARAM-MOVI
    private ParamMoviLdbs1470 paramMovi;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSPMO0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, ParamMoviLdbs1470 paramMovi) {
        this.idsv0003 = idsv0003;
        this.paramMovi = paramMovi;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbspmo0 getInstance() {
        return ((Idbspmo0)Programs.getInstance(Idbspmo0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSPMO0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSPMO0");
        // COB_CODE: MOVE 'PARAM_MOVI' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("PARAM_MOVI");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PARAM_MOVI
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI
        //                ,FRQ_MOVI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,DT_RICOR_PREC
        //                ,DT_RICOR_SUCC
        //                ,PC_INTR_FRAZ
        //                ,IMP_BNS_DA_SCO_TOT
        //                ,IMP_BNS_DA_SCO
        //                ,PC_ANTIC_BNS
        //                ,TP_RINN_COLL
        //                ,TP_RIVAL_PRE
        //                ,TP_RIVAL_PRSTZ
        //                ,FL_EVID_RIVAL
        //                ,ULT_PC_PERD
        //                ,TOT_AA_GIA_PROR
        //                ,TP_OPZ
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,IMP_RISC_PARZ_PRGT
        //                ,IMP_LRD_DI_RAT
        //                ,IB_OGG
        //                ,COS_ONER
        //                ,SPE_PC
        //                ,FL_ATTIV_GAR
        //                ,CAMBIO_VER_PROD
        //                ,MM_DIFF
        //                ,IMP_RAT_MANFEE
        //                ,DT_ULT_EROG_MANFEE
        //                ,TP_OGG_RIVAL
        //                ,SOM_ASSTA_GARAC
        //                ,PC_APPLZ_OPZ
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,TP_FRM_ASSVA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_ESTR_CNT
        //                ,COD_RAMO
        //                ,GEN_DA_SIN
        //                ,COD_TARI
        //                ,NUM_RAT_PAG_PRE
        //                ,PC_SERV_VAL
        //                ,ETA_AA_SOGL_BNFICR
        //             INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //             FROM PARAM_MOVI
        //             WHERE     DS_RIGA = :PMO-DS-RIGA
        //           END-EXEC.
        paramMoviDao.selectByPmoDsRiga(paramMovi.getPmoDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO PARAM_MOVI
            //                  (
            //                     ID_PARAM_MOVI
            //                    ,ID_OGG
            //                    ,TP_OGG
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,TP_MOVI
            //                    ,FRQ_MOVI
            //                    ,DUR_AA
            //                    ,DUR_MM
            //                    ,DUR_GG
            //                    ,DT_RICOR_PREC
            //                    ,DT_RICOR_SUCC
            //                    ,PC_INTR_FRAZ
            //                    ,IMP_BNS_DA_SCO_TOT
            //                    ,IMP_BNS_DA_SCO
            //                    ,PC_ANTIC_BNS
            //                    ,TP_RINN_COLL
            //                    ,TP_RIVAL_PRE
            //                    ,TP_RIVAL_PRSTZ
            //                    ,FL_EVID_RIVAL
            //                    ,ULT_PC_PERD
            //                    ,TOT_AA_GIA_PROR
            //                    ,TP_OPZ
            //                    ,AA_REN_CER
            //                    ,PC_REVRSB
            //                    ,IMP_RISC_PARZ_PRGT
            //                    ,IMP_LRD_DI_RAT
            //                    ,IB_OGG
            //                    ,COS_ONER
            //                    ,SPE_PC
            //                    ,FL_ATTIV_GAR
            //                    ,CAMBIO_VER_PROD
            //                    ,MM_DIFF
            //                    ,IMP_RAT_MANFEE
            //                    ,DT_ULT_EROG_MANFEE
            //                    ,TP_OGG_RIVAL
            //                    ,SOM_ASSTA_GARAC
            //                    ,PC_APPLZ_OPZ
            //                    ,ID_ADES
            //                    ,ID_POLI
            //                    ,TP_FRM_ASSVA
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,TP_ESTR_CNT
            //                    ,COD_RAMO
            //                    ,GEN_DA_SIN
            //                    ,COD_TARI
            //                    ,NUM_RAT_PAG_PRE
            //                    ,PC_SERV_VAL
            //                    ,ETA_AA_SOGL_BNFICR
            //                  )
            //              VALUES
            //                  (
            //                    :PMO-ID-PARAM-MOVI
            //                    ,:PMO-ID-OGG
            //                    ,:PMO-TP-OGG
            //                    ,:PMO-ID-MOVI-CRZ
            //                    ,:PMO-ID-MOVI-CHIU
            //                     :IND-PMO-ID-MOVI-CHIU
            //                    ,:PMO-DT-INI-EFF-DB
            //                    ,:PMO-DT-END-EFF-DB
            //                    ,:PMO-COD-COMP-ANIA
            //                    ,:PMO-TP-MOVI
            //                     :IND-PMO-TP-MOVI
            //                    ,:PMO-FRQ-MOVI
            //                     :IND-PMO-FRQ-MOVI
            //                    ,:PMO-DUR-AA
            //                     :IND-PMO-DUR-AA
            //                    ,:PMO-DUR-MM
            //                     :IND-PMO-DUR-MM
            //                    ,:PMO-DUR-GG
            //                     :IND-PMO-DUR-GG
            //                    ,:PMO-DT-RICOR-PREC-DB
            //                     :IND-PMO-DT-RICOR-PREC
            //                    ,:PMO-DT-RICOR-SUCC-DB
            //                     :IND-PMO-DT-RICOR-SUCC
            //                    ,:PMO-PC-INTR-FRAZ
            //                     :IND-PMO-PC-INTR-FRAZ
            //                    ,:PMO-IMP-BNS-DA-SCO-TOT
            //                     :IND-PMO-IMP-BNS-DA-SCO-TOT
            //                    ,:PMO-IMP-BNS-DA-SCO
            //                     :IND-PMO-IMP-BNS-DA-SCO
            //                    ,:PMO-PC-ANTIC-BNS
            //                     :IND-PMO-PC-ANTIC-BNS
            //                    ,:PMO-TP-RINN-COLL
            //                     :IND-PMO-TP-RINN-COLL
            //                    ,:PMO-TP-RIVAL-PRE
            //                     :IND-PMO-TP-RIVAL-PRE
            //                    ,:PMO-TP-RIVAL-PRSTZ
            //                     :IND-PMO-TP-RIVAL-PRSTZ
            //                    ,:PMO-FL-EVID-RIVAL
            //                     :IND-PMO-FL-EVID-RIVAL
            //                    ,:PMO-ULT-PC-PERD
            //                     :IND-PMO-ULT-PC-PERD
            //                    ,:PMO-TOT-AA-GIA-PROR
            //                     :IND-PMO-TOT-AA-GIA-PROR
            //                    ,:PMO-TP-OPZ
            //                     :IND-PMO-TP-OPZ
            //                    ,:PMO-AA-REN-CER
            //                     :IND-PMO-AA-REN-CER
            //                    ,:PMO-PC-REVRSB
            //                     :IND-PMO-PC-REVRSB
            //                    ,:PMO-IMP-RISC-PARZ-PRGT
            //                     :IND-PMO-IMP-RISC-PARZ-PRGT
            //                    ,:PMO-IMP-LRD-DI-RAT
            //                     :IND-PMO-IMP-LRD-DI-RAT
            //                    ,:PMO-IB-OGG
            //                     :IND-PMO-IB-OGG
            //                    ,:PMO-COS-ONER
            //                     :IND-PMO-COS-ONER
            //                    ,:PMO-SPE-PC
            //                     :IND-PMO-SPE-PC
            //                    ,:PMO-FL-ATTIV-GAR
            //                     :IND-PMO-FL-ATTIV-GAR
            //                    ,:PMO-CAMBIO-VER-PROD
            //                     :IND-PMO-CAMBIO-VER-PROD
            //                    ,:PMO-MM-DIFF
            //                     :IND-PMO-MM-DIFF
            //                    ,:PMO-IMP-RAT-MANFEE
            //                     :IND-PMO-IMP-RAT-MANFEE
            //                    ,:PMO-DT-ULT-EROG-MANFEE-DB
            //                     :IND-PMO-DT-ULT-EROG-MANFEE
            //                    ,:PMO-TP-OGG-RIVAL
            //                     :IND-PMO-TP-OGG-RIVAL
            //                    ,:PMO-SOM-ASSTA-GARAC
            //                     :IND-PMO-SOM-ASSTA-GARAC
            //                    ,:PMO-PC-APPLZ-OPZ
            //                     :IND-PMO-PC-APPLZ-OPZ
            //                    ,:PMO-ID-ADES
            //                     :IND-PMO-ID-ADES
            //                    ,:PMO-ID-POLI
            //                    ,:PMO-TP-FRM-ASSVA
            //                    ,:PMO-DS-RIGA
            //                    ,:PMO-DS-OPER-SQL
            //                    ,:PMO-DS-VER
            //                    ,:PMO-DS-TS-INI-CPTZ
            //                    ,:PMO-DS-TS-END-CPTZ
            //                    ,:PMO-DS-UTENTE
            //                    ,:PMO-DS-STATO-ELAB
            //                    ,:PMO-TP-ESTR-CNT
            //                     :IND-PMO-TP-ESTR-CNT
            //                    ,:PMO-COD-RAMO
            //                     :IND-PMO-COD-RAMO
            //                    ,:PMO-GEN-DA-SIN
            //                     :IND-PMO-GEN-DA-SIN
            //                    ,:PMO-COD-TARI
            //                     :IND-PMO-COD-TARI
            //                    ,:PMO-NUM-RAT-PAG-PRE
            //                     :IND-PMO-NUM-RAT-PAG-PRE
            //                    ,:PMO-PC-SERV-VAL
            //                     :IND-PMO-PC-SERV-VAL
            //                    ,:PMO-ETA-AA-SOGL-BNFICR
            //                     :IND-PMO-ETA-AA-SOGL-BNFICR
            //                  )
            //           END-EXEC
            paramMoviDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE PARAM_MOVI SET
        //                   ID_PARAM_MOVI          =
        //                :PMO-ID-PARAM-MOVI
        //                  ,ID_OGG                 =
        //                :PMO-ID-OGG
        //                  ,TP_OGG                 =
        //                :PMO-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :PMO-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :PMO-ID-MOVI-CHIU
        //                                       :IND-PMO-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :PMO-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :PMO-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :PMO-COD-COMP-ANIA
        //                  ,TP_MOVI                =
        //                :PMO-TP-MOVI
        //                                       :IND-PMO-TP-MOVI
        //                  ,FRQ_MOVI               =
        //                :PMO-FRQ-MOVI
        //                                       :IND-PMO-FRQ-MOVI
        //                  ,DUR_AA                 =
        //                :PMO-DUR-AA
        //                                       :IND-PMO-DUR-AA
        //                  ,DUR_MM                 =
        //                :PMO-DUR-MM
        //                                       :IND-PMO-DUR-MM
        //                  ,DUR_GG                 =
        //                :PMO-DUR-GG
        //                                       :IND-PMO-DUR-GG
        //                  ,DT_RICOR_PREC          =
        //           :PMO-DT-RICOR-PREC-DB
        //                                       :IND-PMO-DT-RICOR-PREC
        //                  ,DT_RICOR_SUCC          =
        //           :PMO-DT-RICOR-SUCC-DB
        //                                       :IND-PMO-DT-RICOR-SUCC
        //                  ,PC_INTR_FRAZ           =
        //                :PMO-PC-INTR-FRAZ
        //                                       :IND-PMO-PC-INTR-FRAZ
        //                  ,IMP_BNS_DA_SCO_TOT     =
        //                :PMO-IMP-BNS-DA-SCO-TOT
        //                                       :IND-PMO-IMP-BNS-DA-SCO-TOT
        //                  ,IMP_BNS_DA_SCO         =
        //                :PMO-IMP-BNS-DA-SCO
        //                                       :IND-PMO-IMP-BNS-DA-SCO
        //                  ,PC_ANTIC_BNS           =
        //                :PMO-PC-ANTIC-BNS
        //                                       :IND-PMO-PC-ANTIC-BNS
        //                  ,TP_RINN_COLL           =
        //                :PMO-TP-RINN-COLL
        //                                       :IND-PMO-TP-RINN-COLL
        //                  ,TP_RIVAL_PRE           =
        //                :PMO-TP-RIVAL-PRE
        //                                       :IND-PMO-TP-RIVAL-PRE
        //                  ,TP_RIVAL_PRSTZ         =
        //                :PMO-TP-RIVAL-PRSTZ
        //                                       :IND-PMO-TP-RIVAL-PRSTZ
        //                  ,FL_EVID_RIVAL          =
        //                :PMO-FL-EVID-RIVAL
        //                                       :IND-PMO-FL-EVID-RIVAL
        //                  ,ULT_PC_PERD            =
        //                :PMO-ULT-PC-PERD
        //                                       :IND-PMO-ULT-PC-PERD
        //                  ,TOT_AA_GIA_PROR        =
        //                :PMO-TOT-AA-GIA-PROR
        //                                       :IND-PMO-TOT-AA-GIA-PROR
        //                  ,TP_OPZ                 =
        //                :PMO-TP-OPZ
        //                                       :IND-PMO-TP-OPZ
        //                  ,AA_REN_CER             =
        //                :PMO-AA-REN-CER
        //                                       :IND-PMO-AA-REN-CER
        //                  ,PC_REVRSB              =
        //                :PMO-PC-REVRSB
        //                                       :IND-PMO-PC-REVRSB
        //                  ,IMP_RISC_PARZ_PRGT     =
        //                :PMO-IMP-RISC-PARZ-PRGT
        //                                       :IND-PMO-IMP-RISC-PARZ-PRGT
        //                  ,IMP_LRD_DI_RAT         =
        //                :PMO-IMP-LRD-DI-RAT
        //                                       :IND-PMO-IMP-LRD-DI-RAT
        //                  ,IB_OGG                 =
        //                :PMO-IB-OGG
        //                                       :IND-PMO-IB-OGG
        //                  ,COS_ONER               =
        //                :PMO-COS-ONER
        //                                       :IND-PMO-COS-ONER
        //                  ,SPE_PC                 =
        //                :PMO-SPE-PC
        //                                       :IND-PMO-SPE-PC
        //                  ,FL_ATTIV_GAR           =
        //                :PMO-FL-ATTIV-GAR
        //                                       :IND-PMO-FL-ATTIV-GAR
        //                  ,CAMBIO_VER_PROD        =
        //                :PMO-CAMBIO-VER-PROD
        //                                       :IND-PMO-CAMBIO-VER-PROD
        //                  ,MM_DIFF                =
        //                :PMO-MM-DIFF
        //                                       :IND-PMO-MM-DIFF
        //                  ,IMP_RAT_MANFEE         =
        //                :PMO-IMP-RAT-MANFEE
        //                                       :IND-PMO-IMP-RAT-MANFEE
        //                  ,DT_ULT_EROG_MANFEE     =
        //           :PMO-DT-ULT-EROG-MANFEE-DB
        //                                       :IND-PMO-DT-ULT-EROG-MANFEE
        //                  ,TP_OGG_RIVAL           =
        //                :PMO-TP-OGG-RIVAL
        //                                       :IND-PMO-TP-OGG-RIVAL
        //                  ,SOM_ASSTA_GARAC        =
        //                :PMO-SOM-ASSTA-GARAC
        //                                       :IND-PMO-SOM-ASSTA-GARAC
        //                  ,PC_APPLZ_OPZ           =
        //                :PMO-PC-APPLZ-OPZ
        //                                       :IND-PMO-PC-APPLZ-OPZ
        //                  ,ID_ADES                =
        //                :PMO-ID-ADES
        //                                       :IND-PMO-ID-ADES
        //                  ,ID_POLI                =
        //                :PMO-ID-POLI
        //                  ,TP_FRM_ASSVA           =
        //                :PMO-TP-FRM-ASSVA
        //                  ,DS_RIGA                =
        //                :PMO-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :PMO-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :PMO-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :PMO-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :PMO-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :PMO-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :PMO-DS-STATO-ELAB
        //                  ,TP_ESTR_CNT            =
        //                :PMO-TP-ESTR-CNT
        //                                       :IND-PMO-TP-ESTR-CNT
        //                  ,COD_RAMO               =
        //                :PMO-COD-RAMO
        //                                       :IND-PMO-COD-RAMO
        //                  ,GEN_DA_SIN             =
        //                :PMO-GEN-DA-SIN
        //                                       :IND-PMO-GEN-DA-SIN
        //                  ,COD_TARI               =
        //                :PMO-COD-TARI
        //                                       :IND-PMO-COD-TARI
        //                  ,NUM_RAT_PAG_PRE        =
        //                :PMO-NUM-RAT-PAG-PRE
        //                                       :IND-PMO-NUM-RAT-PAG-PRE
        //                  ,PC_SERV_VAL            =
        //                :PMO-PC-SERV-VAL
        //                                       :IND-PMO-PC-SERV-VAL
        //                  ,ETA_AA_SOGL_BNFICR     =
        //                :PMO-ETA-AA-SOGL-BNFICR
        //                                       :IND-PMO-ETA-AA-SOGL-BNFICR
        //                WHERE     DS_RIGA = :PMO-DS-RIGA
        //           END-EXEC.
        paramMoviDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM PARAM_MOVI
        //                WHERE     DS_RIGA = :PMO-DS-RIGA
        //           END-EXEC.
        paramMoviDao.deleteByPmoDsRiga(paramMovi.getPmoDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        //       ORDER BY DT_INI_EFF ASC
        // COB_CODE:      EXEC SQL
        //                     DECLARE C-ID-UPD-EFF-PMO CURSOR FOR
        //                   SELECT
        //                          ID_PARAM_MOVI
        //                         ,ID_OGG
        //                         ,TP_OGG
        //                         ,ID_MOVI_CRZ
        //                         ,ID_MOVI_CHIU
        //                         ,DT_INI_EFF
        //                         ,DT_END_EFF
        //                         ,COD_COMP_ANIA
        //                         ,TP_MOVI
        //                         ,FRQ_MOVI
        //                         ,DUR_AA
        //                         ,DUR_MM
        //                         ,DUR_GG
        //                         ,DT_RICOR_PREC
        //                         ,DT_RICOR_SUCC
        //                         ,PC_INTR_FRAZ
        //                         ,IMP_BNS_DA_SCO_TOT
        //                         ,IMP_BNS_DA_SCO
        //                         ,PC_ANTIC_BNS
        //                         ,TP_RINN_COLL
        //                         ,TP_RIVAL_PRE
        //                         ,TP_RIVAL_PRSTZ
        //                         ,FL_EVID_RIVAL
        //                         ,ULT_PC_PERD
        //                         ,TOT_AA_GIA_PROR
        //                         ,TP_OPZ
        //                         ,AA_REN_CER
        //                         ,PC_REVRSB
        //                         ,IMP_RISC_PARZ_PRGT
        //                         ,IMP_LRD_DI_RAT
        //                         ,IB_OGG
        //                         ,COS_ONER
        //                         ,SPE_PC
        //                         ,FL_ATTIV_GAR
        //                         ,CAMBIO_VER_PROD
        //                         ,MM_DIFF
        //                         ,IMP_RAT_MANFEE
        //                         ,DT_ULT_EROG_MANFEE
        //                         ,TP_OGG_RIVAL
        //                         ,SOM_ASSTA_GARAC
        //                         ,PC_APPLZ_OPZ
        //                         ,ID_ADES
        //                         ,ID_POLI
        //                         ,TP_FRM_ASSVA
        //                         ,DS_RIGA
        //                         ,DS_OPER_SQL
        //                         ,DS_VER
        //                         ,DS_TS_INI_CPTZ
        //                         ,DS_TS_END_CPTZ
        //                         ,DS_UTENTE
        //                         ,DS_STATO_ELAB
        //                         ,TP_ESTR_CNT
        //                         ,COD_RAMO
        //                         ,GEN_DA_SIN
        //                         ,COD_TARI
        //                         ,NUM_RAT_PAG_PRE
        //                         ,PC_SERV_VAL
        //                         ,ETA_AA_SOGL_BNFICR
        //                   FROM PARAM_MOVI
        //                   WHERE     ID_PARAM_MOVI = :PMO-ID-PARAM-MOVI
        //                         AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                         AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                         AND COD_COMP_ANIA =
        //                             :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                   ORDER BY COD_COMP_ANIA, ID_PARAM_MOVI,
        //                             DS_TS_END_CPTZ, DT_INI_EFF
        //           *       ORDER BY DT_INI_EFF ASC
        //                END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PARAM_MOVI
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI
        //                ,FRQ_MOVI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,DT_RICOR_PREC
        //                ,DT_RICOR_SUCC
        //                ,PC_INTR_FRAZ
        //                ,IMP_BNS_DA_SCO_TOT
        //                ,IMP_BNS_DA_SCO
        //                ,PC_ANTIC_BNS
        //                ,TP_RINN_COLL
        //                ,TP_RIVAL_PRE
        //                ,TP_RIVAL_PRSTZ
        //                ,FL_EVID_RIVAL
        //                ,ULT_PC_PERD
        //                ,TOT_AA_GIA_PROR
        //                ,TP_OPZ
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,IMP_RISC_PARZ_PRGT
        //                ,IMP_LRD_DI_RAT
        //                ,IB_OGG
        //                ,COS_ONER
        //                ,SPE_PC
        //                ,FL_ATTIV_GAR
        //                ,CAMBIO_VER_PROD
        //                ,MM_DIFF
        //                ,IMP_RAT_MANFEE
        //                ,DT_ULT_EROG_MANFEE
        //                ,TP_OGG_RIVAL
        //                ,SOM_ASSTA_GARAC
        //                ,PC_APPLZ_OPZ
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,TP_FRM_ASSVA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_ESTR_CNT
        //                ,COD_RAMO
        //                ,GEN_DA_SIN
        //                ,COD_TARI
        //                ,NUM_RAT_PAG_PRE
        //                ,PC_SERV_VAL
        //                ,ETA_AA_SOGL_BNFICR
        //             INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //             FROM PARAM_MOVI
        //             WHERE     ID_PARAM_MOVI = :PMO-ID-PARAM-MOVI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        paramMoviDao.selectRec(paramMovi.getPmoIdParamMovi(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE PARAM_MOVI SET
        //                   ID_PARAM_MOVI          =
        //                :PMO-ID-PARAM-MOVI
        //                  ,ID_OGG                 =
        //                :PMO-ID-OGG
        //                  ,TP_OGG                 =
        //                :PMO-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :PMO-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :PMO-ID-MOVI-CHIU
        //                                       :IND-PMO-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :PMO-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :PMO-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :PMO-COD-COMP-ANIA
        //                  ,TP_MOVI                =
        //                :PMO-TP-MOVI
        //                                       :IND-PMO-TP-MOVI
        //                  ,FRQ_MOVI               =
        //                :PMO-FRQ-MOVI
        //                                       :IND-PMO-FRQ-MOVI
        //                  ,DUR_AA                 =
        //                :PMO-DUR-AA
        //                                       :IND-PMO-DUR-AA
        //                  ,DUR_MM                 =
        //                :PMO-DUR-MM
        //                                       :IND-PMO-DUR-MM
        //                  ,DUR_GG                 =
        //                :PMO-DUR-GG
        //                                       :IND-PMO-DUR-GG
        //                  ,DT_RICOR_PREC          =
        //           :PMO-DT-RICOR-PREC-DB
        //                                       :IND-PMO-DT-RICOR-PREC
        //                  ,DT_RICOR_SUCC          =
        //           :PMO-DT-RICOR-SUCC-DB
        //                                       :IND-PMO-DT-RICOR-SUCC
        //                  ,PC_INTR_FRAZ           =
        //                :PMO-PC-INTR-FRAZ
        //                                       :IND-PMO-PC-INTR-FRAZ
        //                  ,IMP_BNS_DA_SCO_TOT     =
        //                :PMO-IMP-BNS-DA-SCO-TOT
        //                                       :IND-PMO-IMP-BNS-DA-SCO-TOT
        //                  ,IMP_BNS_DA_SCO         =
        //                :PMO-IMP-BNS-DA-SCO
        //                                       :IND-PMO-IMP-BNS-DA-SCO
        //                  ,PC_ANTIC_BNS           =
        //                :PMO-PC-ANTIC-BNS
        //                                       :IND-PMO-PC-ANTIC-BNS
        //                  ,TP_RINN_COLL           =
        //                :PMO-TP-RINN-COLL
        //                                       :IND-PMO-TP-RINN-COLL
        //                  ,TP_RIVAL_PRE           =
        //                :PMO-TP-RIVAL-PRE
        //                                       :IND-PMO-TP-RIVAL-PRE
        //                  ,TP_RIVAL_PRSTZ         =
        //                :PMO-TP-RIVAL-PRSTZ
        //                                       :IND-PMO-TP-RIVAL-PRSTZ
        //                  ,FL_EVID_RIVAL          =
        //                :PMO-FL-EVID-RIVAL
        //                                       :IND-PMO-FL-EVID-RIVAL
        //                  ,ULT_PC_PERD            =
        //                :PMO-ULT-PC-PERD
        //                                       :IND-PMO-ULT-PC-PERD
        //                  ,TOT_AA_GIA_PROR        =
        //                :PMO-TOT-AA-GIA-PROR
        //                                       :IND-PMO-TOT-AA-GIA-PROR
        //                  ,TP_OPZ                 =
        //                :PMO-TP-OPZ
        //                                       :IND-PMO-TP-OPZ
        //                  ,AA_REN_CER             =
        //                :PMO-AA-REN-CER
        //                                       :IND-PMO-AA-REN-CER
        //                  ,PC_REVRSB              =
        //                :PMO-PC-REVRSB
        //                                       :IND-PMO-PC-REVRSB
        //                  ,IMP_RISC_PARZ_PRGT     =
        //                :PMO-IMP-RISC-PARZ-PRGT
        //                                       :IND-PMO-IMP-RISC-PARZ-PRGT
        //                  ,IMP_LRD_DI_RAT         =
        //                :PMO-IMP-LRD-DI-RAT
        //                                       :IND-PMO-IMP-LRD-DI-RAT
        //                  ,IB_OGG                 =
        //                :PMO-IB-OGG
        //                                       :IND-PMO-IB-OGG
        //                  ,COS_ONER               =
        //                :PMO-COS-ONER
        //                                       :IND-PMO-COS-ONER
        //                  ,SPE_PC                 =
        //                :PMO-SPE-PC
        //                                       :IND-PMO-SPE-PC
        //                  ,FL_ATTIV_GAR           =
        //                :PMO-FL-ATTIV-GAR
        //                                       :IND-PMO-FL-ATTIV-GAR
        //                  ,CAMBIO_VER_PROD        =
        //                :PMO-CAMBIO-VER-PROD
        //                                       :IND-PMO-CAMBIO-VER-PROD
        //                  ,MM_DIFF                =
        //                :PMO-MM-DIFF
        //                                       :IND-PMO-MM-DIFF
        //                  ,IMP_RAT_MANFEE         =
        //                :PMO-IMP-RAT-MANFEE
        //                                       :IND-PMO-IMP-RAT-MANFEE
        //                  ,DT_ULT_EROG_MANFEE     =
        //           :PMO-DT-ULT-EROG-MANFEE-DB
        //                                       :IND-PMO-DT-ULT-EROG-MANFEE
        //                  ,TP_OGG_RIVAL           =
        //                :PMO-TP-OGG-RIVAL
        //                                       :IND-PMO-TP-OGG-RIVAL
        //                  ,SOM_ASSTA_GARAC        =
        //                :PMO-SOM-ASSTA-GARAC
        //                                       :IND-PMO-SOM-ASSTA-GARAC
        //                  ,PC_APPLZ_OPZ           =
        //                :PMO-PC-APPLZ-OPZ
        //                                       :IND-PMO-PC-APPLZ-OPZ
        //                  ,ID_ADES                =
        //                :PMO-ID-ADES
        //                                       :IND-PMO-ID-ADES
        //                  ,ID_POLI                =
        //                :PMO-ID-POLI
        //                  ,TP_FRM_ASSVA           =
        //                :PMO-TP-FRM-ASSVA
        //                  ,DS_RIGA                =
        //                :PMO-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :PMO-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :PMO-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :PMO-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :PMO-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :PMO-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :PMO-DS-STATO-ELAB
        //                  ,TP_ESTR_CNT            =
        //                :PMO-TP-ESTR-CNT
        //                                       :IND-PMO-TP-ESTR-CNT
        //                  ,COD_RAMO               =
        //                :PMO-COD-RAMO
        //                                       :IND-PMO-COD-RAMO
        //                  ,GEN_DA_SIN             =
        //                :PMO-GEN-DA-SIN
        //                                       :IND-PMO-GEN-DA-SIN
        //                  ,COD_TARI               =
        //                :PMO-COD-TARI
        //                                       :IND-PMO-COD-TARI
        //                  ,NUM_RAT_PAG_PRE        =
        //                :PMO-NUM-RAT-PAG-PRE
        //                                       :IND-PMO-NUM-RAT-PAG-PRE
        //                  ,PC_SERV_VAL            =
        //                :PMO-PC-SERV-VAL
        //                                       :IND-PMO-PC-SERV-VAL
        //                  ,ETA_AA_SOGL_BNFICR     =
        //                :PMO-ETA-AA-SOGL-BNFICR
        //                                       :IND-PMO-ETA-AA-SOGL-BNFICR
        //                WHERE     DS_RIGA = :PMO-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        paramMoviDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-PMO
        //           END-EXEC.
        paramMoviDao.openCIdUpdEffPmo(paramMovi.getPmoIdParamMovi(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-PMO
        //           END-EXEC.
        paramMoviDao.closeCIdUpdEffPmo();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-PMO
        //           INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //           END-EXEC.
        paramMoviDao.fetchCIdUpdEffPmo(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-EFF-PMO CURSOR FOR
        //              SELECT
        //                     ID_PARAM_MOVI
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_MOVI
        //                    ,FRQ_MOVI
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,DT_RICOR_PREC
        //                    ,DT_RICOR_SUCC
        //                    ,PC_INTR_FRAZ
        //                    ,IMP_BNS_DA_SCO_TOT
        //                    ,IMP_BNS_DA_SCO
        //                    ,PC_ANTIC_BNS
        //                    ,TP_RINN_COLL
        //                    ,TP_RIVAL_PRE
        //                    ,TP_RIVAL_PRSTZ
        //                    ,FL_EVID_RIVAL
        //                    ,ULT_PC_PERD
        //                    ,TOT_AA_GIA_PROR
        //                    ,TP_OPZ
        //                    ,AA_REN_CER
        //                    ,PC_REVRSB
        //                    ,IMP_RISC_PARZ_PRGT
        //                    ,IMP_LRD_DI_RAT
        //                    ,IB_OGG
        //                    ,COS_ONER
        //                    ,SPE_PC
        //                    ,FL_ATTIV_GAR
        //                    ,CAMBIO_VER_PROD
        //                    ,MM_DIFF
        //                    ,IMP_RAT_MANFEE
        //                    ,DT_ULT_EROG_MANFEE
        //                    ,TP_OGG_RIVAL
        //                    ,SOM_ASSTA_GARAC
        //                    ,PC_APPLZ_OPZ
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,TP_FRM_ASSVA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_ESTR_CNT
        //                    ,COD_RAMO
        //                    ,GEN_DA_SIN
        //                    ,COD_TARI
        //                    ,NUM_RAT_PAG_PRE
        //                    ,PC_SERV_VAL
        //                    ,ETA_AA_SOGL_BNFICR
        //              FROM PARAM_MOVI
        //              WHERE     IB_OGG = :PMO-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_PARAM_MOVI ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PARAM_MOVI
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI
        //                ,FRQ_MOVI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,DT_RICOR_PREC
        //                ,DT_RICOR_SUCC
        //                ,PC_INTR_FRAZ
        //                ,IMP_BNS_DA_SCO_TOT
        //                ,IMP_BNS_DA_SCO
        //                ,PC_ANTIC_BNS
        //                ,TP_RINN_COLL
        //                ,TP_RIVAL_PRE
        //                ,TP_RIVAL_PRSTZ
        //                ,FL_EVID_RIVAL
        //                ,ULT_PC_PERD
        //                ,TOT_AA_GIA_PROR
        //                ,TP_OPZ
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,IMP_RISC_PARZ_PRGT
        //                ,IMP_LRD_DI_RAT
        //                ,IB_OGG
        //                ,COS_ONER
        //                ,SPE_PC
        //                ,FL_ATTIV_GAR
        //                ,CAMBIO_VER_PROD
        //                ,MM_DIFF
        //                ,IMP_RAT_MANFEE
        //                ,DT_ULT_EROG_MANFEE
        //                ,TP_OGG_RIVAL
        //                ,SOM_ASSTA_GARAC
        //                ,PC_APPLZ_OPZ
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,TP_FRM_ASSVA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_ESTR_CNT
        //                ,COD_RAMO
        //                ,GEN_DA_SIN
        //                ,COD_TARI
        //                ,NUM_RAT_PAG_PRE
        //                ,PC_SERV_VAL
        //                ,ETA_AA_SOGL_BNFICR
        //             INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //             FROM PARAM_MOVI
        //             WHERE     IB_OGG = :PMO-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        paramMoviDao.selectRec1(paramMovi.getPmoIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-EFF-PMO
        //           END-EXEC.
        paramMoviDao.openCIboEffPmo(paramMovi.getPmoIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-EFF-PMO
        //           END-EXEC.
        paramMoviDao.closeCIboEffPmo();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-EFF-PMO
        //           INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //           END-EXEC.
        paramMoviDao.fetchCIboEffPmo(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX
            a570CloseCursorIbo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-EFF-PMO CURSOR FOR
        //              SELECT
        //                     ID_PARAM_MOVI
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_MOVI
        //                    ,FRQ_MOVI
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,DT_RICOR_PREC
        //                    ,DT_RICOR_SUCC
        //                    ,PC_INTR_FRAZ
        //                    ,IMP_BNS_DA_SCO_TOT
        //                    ,IMP_BNS_DA_SCO
        //                    ,PC_ANTIC_BNS
        //                    ,TP_RINN_COLL
        //                    ,TP_RIVAL_PRE
        //                    ,TP_RIVAL_PRSTZ
        //                    ,FL_EVID_RIVAL
        //                    ,ULT_PC_PERD
        //                    ,TOT_AA_GIA_PROR
        //                    ,TP_OPZ
        //                    ,AA_REN_CER
        //                    ,PC_REVRSB
        //                    ,IMP_RISC_PARZ_PRGT
        //                    ,IMP_LRD_DI_RAT
        //                    ,IB_OGG
        //                    ,COS_ONER
        //                    ,SPE_PC
        //                    ,FL_ATTIV_GAR
        //                    ,CAMBIO_VER_PROD
        //                    ,MM_DIFF
        //                    ,IMP_RAT_MANFEE
        //                    ,DT_ULT_EROG_MANFEE
        //                    ,TP_OGG_RIVAL
        //                    ,SOM_ASSTA_GARAC
        //                    ,PC_APPLZ_OPZ
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,TP_FRM_ASSVA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_ESTR_CNT
        //                    ,COD_RAMO
        //                    ,GEN_DA_SIN
        //                    ,COD_TARI
        //                    ,NUM_RAT_PAG_PRE
        //                    ,PC_SERV_VAL
        //                    ,ETA_AA_SOGL_BNFICR
        //              FROM PARAM_MOVI
        //              WHERE     ID_OGG = :PMO-ID-OGG
        //                    AND TP_OGG = :PMO-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_PARAM_MOVI ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PARAM_MOVI
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI
        //                ,FRQ_MOVI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,DT_RICOR_PREC
        //                ,DT_RICOR_SUCC
        //                ,PC_INTR_FRAZ
        //                ,IMP_BNS_DA_SCO_TOT
        //                ,IMP_BNS_DA_SCO
        //                ,PC_ANTIC_BNS
        //                ,TP_RINN_COLL
        //                ,TP_RIVAL_PRE
        //                ,TP_RIVAL_PRSTZ
        //                ,FL_EVID_RIVAL
        //                ,ULT_PC_PERD
        //                ,TOT_AA_GIA_PROR
        //                ,TP_OPZ
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,IMP_RISC_PARZ_PRGT
        //                ,IMP_LRD_DI_RAT
        //                ,IB_OGG
        //                ,COS_ONER
        //                ,SPE_PC
        //                ,FL_ATTIV_GAR
        //                ,CAMBIO_VER_PROD
        //                ,MM_DIFF
        //                ,IMP_RAT_MANFEE
        //                ,DT_ULT_EROG_MANFEE
        //                ,TP_OGG_RIVAL
        //                ,SOM_ASSTA_GARAC
        //                ,PC_APPLZ_OPZ
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,TP_FRM_ASSVA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_ESTR_CNT
        //                ,COD_RAMO
        //                ,GEN_DA_SIN
        //                ,COD_TARI
        //                ,NUM_RAT_PAG_PRE
        //                ,PC_SERV_VAL
        //                ,ETA_AA_SOGL_BNFICR
        //             INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //             FROM PARAM_MOVI
        //             WHERE     ID_OGG = :PMO-ID-OGG
        //                    AND TP_OGG = :PMO-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        paramMoviDao.selectRec2(paramMovi.getPmoIdOgg(), paramMovi.getPmoTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-EFF-PMO
        //           END-EXEC.
        paramMoviDao.openCIdoEffPmo(paramMovi.getPmoIdOgg(), paramMovi.getPmoTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-EFF-PMO
        //           END-EXEC.
        paramMoviDao.closeCIdoEffPmo();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-EFF-PMO
        //           INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //           END-EXEC.
        paramMoviDao.fetchCIdoEffPmo(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX
            a770CloseCursorIdo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PARAM_MOVI
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI
        //                ,FRQ_MOVI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,DT_RICOR_PREC
        //                ,DT_RICOR_SUCC
        //                ,PC_INTR_FRAZ
        //                ,IMP_BNS_DA_SCO_TOT
        //                ,IMP_BNS_DA_SCO
        //                ,PC_ANTIC_BNS
        //                ,TP_RINN_COLL
        //                ,TP_RIVAL_PRE
        //                ,TP_RIVAL_PRSTZ
        //                ,FL_EVID_RIVAL
        //                ,ULT_PC_PERD
        //                ,TOT_AA_GIA_PROR
        //                ,TP_OPZ
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,IMP_RISC_PARZ_PRGT
        //                ,IMP_LRD_DI_RAT
        //                ,IB_OGG
        //                ,COS_ONER
        //                ,SPE_PC
        //                ,FL_ATTIV_GAR
        //                ,CAMBIO_VER_PROD
        //                ,MM_DIFF
        //                ,IMP_RAT_MANFEE
        //                ,DT_ULT_EROG_MANFEE
        //                ,TP_OGG_RIVAL
        //                ,SOM_ASSTA_GARAC
        //                ,PC_APPLZ_OPZ
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,TP_FRM_ASSVA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_ESTR_CNT
        //                ,COD_RAMO
        //                ,GEN_DA_SIN
        //                ,COD_TARI
        //                ,NUM_RAT_PAG_PRE
        //                ,PC_SERV_VAL
        //                ,ETA_AA_SOGL_BNFICR
        //             INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //             FROM PARAM_MOVI
        //             WHERE     ID_PARAM_MOVI = :PMO-ID-PARAM-MOVI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        paramMoviDao.selectRec3(paramMovi.getPmoIdParamMovi(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-CPZ-PMO CURSOR FOR
        //              SELECT
        //                     ID_PARAM_MOVI
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_MOVI
        //                    ,FRQ_MOVI
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,DT_RICOR_PREC
        //                    ,DT_RICOR_SUCC
        //                    ,PC_INTR_FRAZ
        //                    ,IMP_BNS_DA_SCO_TOT
        //                    ,IMP_BNS_DA_SCO
        //                    ,PC_ANTIC_BNS
        //                    ,TP_RINN_COLL
        //                    ,TP_RIVAL_PRE
        //                    ,TP_RIVAL_PRSTZ
        //                    ,FL_EVID_RIVAL
        //                    ,ULT_PC_PERD
        //                    ,TOT_AA_GIA_PROR
        //                    ,TP_OPZ
        //                    ,AA_REN_CER
        //                    ,PC_REVRSB
        //                    ,IMP_RISC_PARZ_PRGT
        //                    ,IMP_LRD_DI_RAT
        //                    ,IB_OGG
        //                    ,COS_ONER
        //                    ,SPE_PC
        //                    ,FL_ATTIV_GAR
        //                    ,CAMBIO_VER_PROD
        //                    ,MM_DIFF
        //                    ,IMP_RAT_MANFEE
        //                    ,DT_ULT_EROG_MANFEE
        //                    ,TP_OGG_RIVAL
        //                    ,SOM_ASSTA_GARAC
        //                    ,PC_APPLZ_OPZ
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,TP_FRM_ASSVA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_ESTR_CNT
        //                    ,COD_RAMO
        //                    ,GEN_DA_SIN
        //                    ,COD_TARI
        //                    ,NUM_RAT_PAG_PRE
        //                    ,PC_SERV_VAL
        //                    ,ETA_AA_SOGL_BNFICR
        //              FROM PARAM_MOVI
        //              WHERE     IB_OGG = :PMO-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_PARAM_MOVI ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PARAM_MOVI
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI
        //                ,FRQ_MOVI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,DT_RICOR_PREC
        //                ,DT_RICOR_SUCC
        //                ,PC_INTR_FRAZ
        //                ,IMP_BNS_DA_SCO_TOT
        //                ,IMP_BNS_DA_SCO
        //                ,PC_ANTIC_BNS
        //                ,TP_RINN_COLL
        //                ,TP_RIVAL_PRE
        //                ,TP_RIVAL_PRSTZ
        //                ,FL_EVID_RIVAL
        //                ,ULT_PC_PERD
        //                ,TOT_AA_GIA_PROR
        //                ,TP_OPZ
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,IMP_RISC_PARZ_PRGT
        //                ,IMP_LRD_DI_RAT
        //                ,IB_OGG
        //                ,COS_ONER
        //                ,SPE_PC
        //                ,FL_ATTIV_GAR
        //                ,CAMBIO_VER_PROD
        //                ,MM_DIFF
        //                ,IMP_RAT_MANFEE
        //                ,DT_ULT_EROG_MANFEE
        //                ,TP_OGG_RIVAL
        //                ,SOM_ASSTA_GARAC
        //                ,PC_APPLZ_OPZ
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,TP_FRM_ASSVA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_ESTR_CNT
        //                ,COD_RAMO
        //                ,GEN_DA_SIN
        //                ,COD_TARI
        //                ,NUM_RAT_PAG_PRE
        //                ,PC_SERV_VAL
        //                ,ETA_AA_SOGL_BNFICR
        //             INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //             FROM PARAM_MOVI
        //             WHERE     IB_OGG = :PMO-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        paramMoviDao.selectRec4(paramMovi.getPmoIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-CPZ-PMO
        //           END-EXEC.
        paramMoviDao.openCIboCpzPmo(paramMovi.getPmoIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-CPZ-PMO
        //           END-EXEC.
        paramMoviDao.closeCIboCpzPmo();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-CPZ-PMO
        //           INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //           END-EXEC.
        paramMoviDao.fetchCIboCpzPmo(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX
            b570CloseCursorIboCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-CPZ-PMO CURSOR FOR
        //              SELECT
        //                     ID_PARAM_MOVI
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_MOVI
        //                    ,FRQ_MOVI
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,DT_RICOR_PREC
        //                    ,DT_RICOR_SUCC
        //                    ,PC_INTR_FRAZ
        //                    ,IMP_BNS_DA_SCO_TOT
        //                    ,IMP_BNS_DA_SCO
        //                    ,PC_ANTIC_BNS
        //                    ,TP_RINN_COLL
        //                    ,TP_RIVAL_PRE
        //                    ,TP_RIVAL_PRSTZ
        //                    ,FL_EVID_RIVAL
        //                    ,ULT_PC_PERD
        //                    ,TOT_AA_GIA_PROR
        //                    ,TP_OPZ
        //                    ,AA_REN_CER
        //                    ,PC_REVRSB
        //                    ,IMP_RISC_PARZ_PRGT
        //                    ,IMP_LRD_DI_RAT
        //                    ,IB_OGG
        //                    ,COS_ONER
        //                    ,SPE_PC
        //                    ,FL_ATTIV_GAR
        //                    ,CAMBIO_VER_PROD
        //                    ,MM_DIFF
        //                    ,IMP_RAT_MANFEE
        //                    ,DT_ULT_EROG_MANFEE
        //                    ,TP_OGG_RIVAL
        //                    ,SOM_ASSTA_GARAC
        //                    ,PC_APPLZ_OPZ
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,TP_FRM_ASSVA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_ESTR_CNT
        //                    ,COD_RAMO
        //                    ,GEN_DA_SIN
        //                    ,COD_TARI
        //                    ,NUM_RAT_PAG_PRE
        //                    ,PC_SERV_VAL
        //                    ,ETA_AA_SOGL_BNFICR
        //              FROM PARAM_MOVI
        //              WHERE     ID_OGG = :PMO-ID-OGG
        //           AND TP_OGG = :PMO-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_PARAM_MOVI ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PARAM_MOVI
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_MOVI
        //                ,FRQ_MOVI
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,DT_RICOR_PREC
        //                ,DT_RICOR_SUCC
        //                ,PC_INTR_FRAZ
        //                ,IMP_BNS_DA_SCO_TOT
        //                ,IMP_BNS_DA_SCO
        //                ,PC_ANTIC_BNS
        //                ,TP_RINN_COLL
        //                ,TP_RIVAL_PRE
        //                ,TP_RIVAL_PRSTZ
        //                ,FL_EVID_RIVAL
        //                ,ULT_PC_PERD
        //                ,TOT_AA_GIA_PROR
        //                ,TP_OPZ
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,IMP_RISC_PARZ_PRGT
        //                ,IMP_LRD_DI_RAT
        //                ,IB_OGG
        //                ,COS_ONER
        //                ,SPE_PC
        //                ,FL_ATTIV_GAR
        //                ,CAMBIO_VER_PROD
        //                ,MM_DIFF
        //                ,IMP_RAT_MANFEE
        //                ,DT_ULT_EROG_MANFEE
        //                ,TP_OGG_RIVAL
        //                ,SOM_ASSTA_GARAC
        //                ,PC_APPLZ_OPZ
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,TP_FRM_ASSVA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_ESTR_CNT
        //                ,COD_RAMO
        //                ,GEN_DA_SIN
        //                ,COD_TARI
        //                ,NUM_RAT_PAG_PRE
        //                ,PC_SERV_VAL
        //                ,ETA_AA_SOGL_BNFICR
        //             INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //             FROM PARAM_MOVI
        //             WHERE     ID_OGG = :PMO-ID-OGG
        //                    AND TP_OGG = :PMO-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        paramMoviDao.selectRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-CPZ-PMO
        //           END-EXEC.
        paramMoviDao.openCIdoCpzPmo(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-CPZ-PMO
        //           END-EXEC.
        paramMoviDao.closeCIdoCpzPmo();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-CPZ-PMO
        //           INTO
        //                :PMO-ID-PARAM-MOVI
        //               ,:PMO-ID-OGG
        //               ,:PMO-TP-OGG
        //               ,:PMO-ID-MOVI-CRZ
        //               ,:PMO-ID-MOVI-CHIU
        //                :IND-PMO-ID-MOVI-CHIU
        //               ,:PMO-DT-INI-EFF-DB
        //               ,:PMO-DT-END-EFF-DB
        //               ,:PMO-COD-COMP-ANIA
        //               ,:PMO-TP-MOVI
        //                :IND-PMO-TP-MOVI
        //               ,:PMO-FRQ-MOVI
        //                :IND-PMO-FRQ-MOVI
        //               ,:PMO-DUR-AA
        //                :IND-PMO-DUR-AA
        //               ,:PMO-DUR-MM
        //                :IND-PMO-DUR-MM
        //               ,:PMO-DUR-GG
        //                :IND-PMO-DUR-GG
        //               ,:PMO-DT-RICOR-PREC-DB
        //                :IND-PMO-DT-RICOR-PREC
        //               ,:PMO-DT-RICOR-SUCC-DB
        //                :IND-PMO-DT-RICOR-SUCC
        //               ,:PMO-PC-INTR-FRAZ
        //                :IND-PMO-PC-INTR-FRAZ
        //               ,:PMO-IMP-BNS-DA-SCO-TOT
        //                :IND-PMO-IMP-BNS-DA-SCO-TOT
        //               ,:PMO-IMP-BNS-DA-SCO
        //                :IND-PMO-IMP-BNS-DA-SCO
        //               ,:PMO-PC-ANTIC-BNS
        //                :IND-PMO-PC-ANTIC-BNS
        //               ,:PMO-TP-RINN-COLL
        //                :IND-PMO-TP-RINN-COLL
        //               ,:PMO-TP-RIVAL-PRE
        //                :IND-PMO-TP-RIVAL-PRE
        //               ,:PMO-TP-RIVAL-PRSTZ
        //                :IND-PMO-TP-RIVAL-PRSTZ
        //               ,:PMO-FL-EVID-RIVAL
        //                :IND-PMO-FL-EVID-RIVAL
        //               ,:PMO-ULT-PC-PERD
        //                :IND-PMO-ULT-PC-PERD
        //               ,:PMO-TOT-AA-GIA-PROR
        //                :IND-PMO-TOT-AA-GIA-PROR
        //               ,:PMO-TP-OPZ
        //                :IND-PMO-TP-OPZ
        //               ,:PMO-AA-REN-CER
        //                :IND-PMO-AA-REN-CER
        //               ,:PMO-PC-REVRSB
        //                :IND-PMO-PC-REVRSB
        //               ,:PMO-IMP-RISC-PARZ-PRGT
        //                :IND-PMO-IMP-RISC-PARZ-PRGT
        //               ,:PMO-IMP-LRD-DI-RAT
        //                :IND-PMO-IMP-LRD-DI-RAT
        //               ,:PMO-IB-OGG
        //                :IND-PMO-IB-OGG
        //               ,:PMO-COS-ONER
        //                :IND-PMO-COS-ONER
        //               ,:PMO-SPE-PC
        //                :IND-PMO-SPE-PC
        //               ,:PMO-FL-ATTIV-GAR
        //                :IND-PMO-FL-ATTIV-GAR
        //               ,:PMO-CAMBIO-VER-PROD
        //                :IND-PMO-CAMBIO-VER-PROD
        //               ,:PMO-MM-DIFF
        //                :IND-PMO-MM-DIFF
        //               ,:PMO-IMP-RAT-MANFEE
        //                :IND-PMO-IMP-RAT-MANFEE
        //               ,:PMO-DT-ULT-EROG-MANFEE-DB
        //                :IND-PMO-DT-ULT-EROG-MANFEE
        //               ,:PMO-TP-OGG-RIVAL
        //                :IND-PMO-TP-OGG-RIVAL
        //               ,:PMO-SOM-ASSTA-GARAC
        //                :IND-PMO-SOM-ASSTA-GARAC
        //               ,:PMO-PC-APPLZ-OPZ
        //                :IND-PMO-PC-APPLZ-OPZ
        //               ,:PMO-ID-ADES
        //                :IND-PMO-ID-ADES
        //               ,:PMO-ID-POLI
        //               ,:PMO-TP-FRM-ASSVA
        //               ,:PMO-DS-RIGA
        //               ,:PMO-DS-OPER-SQL
        //               ,:PMO-DS-VER
        //               ,:PMO-DS-TS-INI-CPTZ
        //               ,:PMO-DS-TS-END-CPTZ
        //               ,:PMO-DS-UTENTE
        //               ,:PMO-DS-STATO-ELAB
        //               ,:PMO-TP-ESTR-CNT
        //                :IND-PMO-TP-ESTR-CNT
        //               ,:PMO-COD-RAMO
        //                :IND-PMO-COD-RAMO
        //               ,:PMO-GEN-DA-SIN
        //                :IND-PMO-GEN-DA-SIN
        //               ,:PMO-COD-TARI
        //                :IND-PMO-COD-TARI
        //               ,:PMO-NUM-RAT-PAG-PRE
        //                :IND-PMO-NUM-RAT-PAG-PRE
        //               ,:PMO-PC-SERV-VAL
        //                :IND-PMO-PC-SERV-VAL
        //               ,:PMO-ETA-AA-SOGL-BNFICR
        //                :IND-PMO-ETA-AA-SOGL-BNFICR
        //           END-EXEC.
        paramMoviDao.fetchCIdoCpzPmo(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX
            b770CloseCursorIdoCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-PMO-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO PMO-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndParamMovi().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-ID-MOVI-CHIU-NULL
            paramMovi.getPmoIdMoviChiu().setPmoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoIdMoviChiu.Len.PMO_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-PMO-TP-MOVI = -1
        //              MOVE HIGH-VALUES TO PMO-TP-MOVI-NULL
        //           END-IF
        if (ws.getIndParamMovi().getTpMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-TP-MOVI-NULL
            paramMovi.getPmoTpMovi().setPmoTpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoTpMovi.Len.PMO_TP_MOVI_NULL));
        }
        // COB_CODE: IF IND-PMO-FRQ-MOVI = -1
        //              MOVE HIGH-VALUES TO PMO-FRQ-MOVI-NULL
        //           END-IF
        if (ws.getIndParamMovi().getFrqMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-FRQ-MOVI-NULL
            paramMovi.getPmoFrqMovi().setPmoFrqMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoFrqMovi.Len.PMO_FRQ_MOVI_NULL));
        }
        // COB_CODE: IF IND-PMO-DUR-AA = -1
        //              MOVE HIGH-VALUES TO PMO-DUR-AA-NULL
        //           END-IF
        if (ws.getIndParamMovi().getDurAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-DUR-AA-NULL
            paramMovi.getPmoDurAa().setPmoDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoDurAa.Len.PMO_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-PMO-DUR-MM = -1
        //              MOVE HIGH-VALUES TO PMO-DUR-MM-NULL
        //           END-IF
        if (ws.getIndParamMovi().getDurMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-DUR-MM-NULL
            paramMovi.getPmoDurMm().setPmoDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoDurMm.Len.PMO_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-PMO-DUR-GG = -1
        //              MOVE HIGH-VALUES TO PMO-DUR-GG-NULL
        //           END-IF
        if (ws.getIndParamMovi().getDurGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-DUR-GG-NULL
            paramMovi.getPmoDurGg().setPmoDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoDurGg.Len.PMO_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-PMO-DT-RICOR-PREC = -1
        //              MOVE HIGH-VALUES TO PMO-DT-RICOR-PREC-NULL
        //           END-IF
        if (ws.getIndParamMovi().getDtRicorPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-DT-RICOR-PREC-NULL
            paramMovi.getPmoDtRicorPrec().setPmoDtRicorPrecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoDtRicorPrec.Len.PMO_DT_RICOR_PREC_NULL));
        }
        // COB_CODE: IF IND-PMO-DT-RICOR-SUCC = -1
        //              MOVE HIGH-VALUES TO PMO-DT-RICOR-SUCC-NULL
        //           END-IF
        if (ws.getIndParamMovi().getDtRicorSucc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-DT-RICOR-SUCC-NULL
            paramMovi.getPmoDtRicorSucc().setPmoDtRicorSuccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoDtRicorSucc.Len.PMO_DT_RICOR_SUCC_NULL));
        }
        // COB_CODE: IF IND-PMO-PC-INTR-FRAZ = -1
        //              MOVE HIGH-VALUES TO PMO-PC-INTR-FRAZ-NULL
        //           END-IF
        if (ws.getIndParamMovi().getPcIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-PC-INTR-FRAZ-NULL
            paramMovi.getPmoPcIntrFraz().setPmoPcIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoPcIntrFraz.Len.PMO_PC_INTR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-PMO-IMP-BNS-DA-SCO-TOT = -1
        //              MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-TOT-NULL
        //           END-IF
        if (ws.getIndParamMovi().getImpBnsDaScoTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-TOT-NULL
            paramMovi.getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoImpBnsDaScoTot.Len.PMO_IMP_BNS_DA_SCO_TOT_NULL));
        }
        // COB_CODE: IF IND-PMO-IMP-BNS-DA-SCO = -1
        //              MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-NULL
        //           END-IF
        if (ws.getIndParamMovi().getImpBnsDaSco() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-NULL
            paramMovi.getPmoImpBnsDaSco().setPmoImpBnsDaScoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoImpBnsDaSco.Len.PMO_IMP_BNS_DA_SCO_NULL));
        }
        // COB_CODE: IF IND-PMO-PC-ANTIC-BNS = -1
        //              MOVE HIGH-VALUES TO PMO-PC-ANTIC-BNS-NULL
        //           END-IF
        if (ws.getIndParamMovi().getPcAnticBns() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-PC-ANTIC-BNS-NULL
            paramMovi.getPmoPcAnticBns().setPmoPcAnticBnsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoPcAnticBns.Len.PMO_PC_ANTIC_BNS_NULL));
        }
        // COB_CODE: IF IND-PMO-TP-RINN-COLL = -1
        //              MOVE HIGH-VALUES TO PMO-TP-RINN-COLL-NULL
        //           END-IF
        if (ws.getIndParamMovi().getTpRinnColl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-TP-RINN-COLL-NULL
            paramMovi.setPmoTpRinnColl(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamMoviLdbs1470.Len.PMO_TP_RINN_COLL));
        }
        // COB_CODE: IF IND-PMO-TP-RIVAL-PRE = -1
        //              MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRE-NULL
        //           END-IF
        if (ws.getIndParamMovi().getTpRivalPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRE-NULL
            paramMovi.setPmoTpRivalPre(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamMoviLdbs1470.Len.PMO_TP_RIVAL_PRE));
        }
        // COB_CODE: IF IND-PMO-TP-RIVAL-PRSTZ = -1
        //              MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRSTZ-NULL
        //           END-IF
        if (ws.getIndParamMovi().getTpRivalPrstz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRSTZ-NULL
            paramMovi.setPmoTpRivalPrstz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamMoviLdbs1470.Len.PMO_TP_RIVAL_PRSTZ));
        }
        // COB_CODE: IF IND-PMO-FL-EVID-RIVAL = -1
        //              MOVE HIGH-VALUES TO PMO-FL-EVID-RIVAL-NULL
        //           END-IF
        if (ws.getIndParamMovi().getFlEvidRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-FL-EVID-RIVAL-NULL
            paramMovi.setPmoFlEvidRival(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PMO-ULT-PC-PERD = -1
        //              MOVE HIGH-VALUES TO PMO-ULT-PC-PERD-NULL
        //           END-IF
        if (ws.getIndParamMovi().getUltPcPerd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-ULT-PC-PERD-NULL
            paramMovi.getPmoUltPcPerd().setPmoUltPcPerdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoUltPcPerd.Len.PMO_ULT_PC_PERD_NULL));
        }
        // COB_CODE: IF IND-PMO-TOT-AA-GIA-PROR = -1
        //              MOVE HIGH-VALUES TO PMO-TOT-AA-GIA-PROR-NULL
        //           END-IF
        if (ws.getIndParamMovi().getTotAaGiaPror() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-TOT-AA-GIA-PROR-NULL
            paramMovi.getPmoTotAaGiaPror().setPmoTotAaGiaProrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoTotAaGiaPror.Len.PMO_TOT_AA_GIA_PROR_NULL));
        }
        // COB_CODE: IF IND-PMO-TP-OPZ = -1
        //              MOVE HIGH-VALUES TO PMO-TP-OPZ-NULL
        //           END-IF
        if (ws.getIndParamMovi().getTpOpz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-TP-OPZ-NULL
            paramMovi.setPmoTpOpz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamMoviLdbs1470.Len.PMO_TP_OPZ));
        }
        // COB_CODE: IF IND-PMO-AA-REN-CER = -1
        //              MOVE HIGH-VALUES TO PMO-AA-REN-CER-NULL
        //           END-IF
        if (ws.getIndParamMovi().getAaRenCer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-AA-REN-CER-NULL
            paramMovi.getPmoAaRenCer().setPmoAaRenCerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoAaRenCer.Len.PMO_AA_REN_CER_NULL));
        }
        // COB_CODE: IF IND-PMO-PC-REVRSB = -1
        //              MOVE HIGH-VALUES TO PMO-PC-REVRSB-NULL
        //           END-IF
        if (ws.getIndParamMovi().getPcRevrsb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-PC-REVRSB-NULL
            paramMovi.getPmoPcRevrsb().setPmoPcRevrsbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoPcRevrsb.Len.PMO_PC_REVRSB_NULL));
        }
        // COB_CODE: IF IND-PMO-IMP-RISC-PARZ-PRGT = -1
        //              MOVE HIGH-VALUES TO PMO-IMP-RISC-PARZ-PRGT-NULL
        //           END-IF
        if (ws.getIndParamMovi().getImpRiscParzPrgt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-IMP-RISC-PARZ-PRGT-NULL
            paramMovi.getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoImpRiscParzPrgt.Len.PMO_IMP_RISC_PARZ_PRGT_NULL));
        }
        // COB_CODE: IF IND-PMO-IMP-LRD-DI-RAT = -1
        //              MOVE HIGH-VALUES TO PMO-IMP-LRD-DI-RAT-NULL
        //           END-IF
        if (ws.getIndParamMovi().getImpLrdDiRat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-IMP-LRD-DI-RAT-NULL
            paramMovi.getPmoImpLrdDiRat().setPmoImpLrdDiRatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoImpLrdDiRat.Len.PMO_IMP_LRD_DI_RAT_NULL));
        }
        // COB_CODE: IF IND-PMO-IB-OGG = -1
        //              MOVE HIGH-VALUES TO PMO-IB-OGG-NULL
        //           END-IF
        if (ws.getIndParamMovi().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-IB-OGG-NULL
            paramMovi.setPmoIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamMoviLdbs1470.Len.PMO_IB_OGG));
        }
        // COB_CODE: IF IND-PMO-COS-ONER = -1
        //              MOVE HIGH-VALUES TO PMO-COS-ONER-NULL
        //           END-IF
        if (ws.getIndParamMovi().getCosOner() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-COS-ONER-NULL
            paramMovi.getPmoCosOner().setPmoCosOnerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoCosOner.Len.PMO_COS_ONER_NULL));
        }
        // COB_CODE: IF IND-PMO-SPE-PC = -1
        //              MOVE HIGH-VALUES TO PMO-SPE-PC-NULL
        //           END-IF
        if (ws.getIndParamMovi().getSpePc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-SPE-PC-NULL
            paramMovi.getPmoSpePc().setPmoSpePcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoSpePc.Len.PMO_SPE_PC_NULL));
        }
        // COB_CODE: IF IND-PMO-FL-ATTIV-GAR = -1
        //              MOVE HIGH-VALUES TO PMO-FL-ATTIV-GAR-NULL
        //           END-IF
        if (ws.getIndParamMovi().getFlAttivGar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-FL-ATTIV-GAR-NULL
            paramMovi.setPmoFlAttivGar(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PMO-CAMBIO-VER-PROD = -1
        //              MOVE HIGH-VALUES TO PMO-CAMBIO-VER-PROD-NULL
        //           END-IF
        if (ws.getIndParamMovi().getCambioVerProd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-CAMBIO-VER-PROD-NULL
            paramMovi.setPmoCambioVerProd(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PMO-MM-DIFF = -1
        //              MOVE HIGH-VALUES TO PMO-MM-DIFF-NULL
        //           END-IF
        if (ws.getIndParamMovi().getMmDiff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-MM-DIFF-NULL
            paramMovi.getPmoMmDiff().setPmoMmDiffNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoMmDiff.Len.PMO_MM_DIFF_NULL));
        }
        // COB_CODE: IF IND-PMO-IMP-RAT-MANFEE = -1
        //              MOVE HIGH-VALUES TO PMO-IMP-RAT-MANFEE-NULL
        //           END-IF
        if (ws.getIndParamMovi().getImpRatManfee() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-IMP-RAT-MANFEE-NULL
            paramMovi.getPmoImpRatManfee().setPmoImpRatManfeeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoImpRatManfee.Len.PMO_IMP_RAT_MANFEE_NULL));
        }
        // COB_CODE: IF IND-PMO-DT-ULT-EROG-MANFEE = -1
        //              MOVE HIGH-VALUES TO PMO-DT-ULT-EROG-MANFEE-NULL
        //           END-IF
        if (ws.getIndParamMovi().getDtUltErogManfee() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-DT-ULT-EROG-MANFEE-NULL
            paramMovi.getPmoDtUltErogManfee().setPmoDtUltErogManfeeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoDtUltErogManfee.Len.PMO_DT_ULT_EROG_MANFEE_NULL));
        }
        // COB_CODE: IF IND-PMO-TP-OGG-RIVAL = -1
        //              MOVE HIGH-VALUES TO PMO-TP-OGG-RIVAL-NULL
        //           END-IF
        if (ws.getIndParamMovi().getTpOggRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-TP-OGG-RIVAL-NULL
            paramMovi.setPmoTpOggRival(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamMoviLdbs1470.Len.PMO_TP_OGG_RIVAL));
        }
        // COB_CODE: IF IND-PMO-SOM-ASSTA-GARAC = -1
        //              MOVE HIGH-VALUES TO PMO-SOM-ASSTA-GARAC-NULL
        //           END-IF
        if (ws.getIndParamMovi().getSomAsstaGarac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-SOM-ASSTA-GARAC-NULL
            paramMovi.getPmoSomAsstaGarac().setPmoSomAsstaGaracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoSomAsstaGarac.Len.PMO_SOM_ASSTA_GARAC_NULL));
        }
        // COB_CODE: IF IND-PMO-PC-APPLZ-OPZ = -1
        //              MOVE HIGH-VALUES TO PMO-PC-APPLZ-OPZ-NULL
        //           END-IF
        if (ws.getIndParamMovi().getPcApplzOpz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-PC-APPLZ-OPZ-NULL
            paramMovi.getPmoPcApplzOpz().setPmoPcApplzOpzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoPcApplzOpz.Len.PMO_PC_APPLZ_OPZ_NULL));
        }
        // COB_CODE: IF IND-PMO-ID-ADES = -1
        //              MOVE HIGH-VALUES TO PMO-ID-ADES-NULL
        //           END-IF
        if (ws.getIndParamMovi().getIdAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-ID-ADES-NULL
            paramMovi.getPmoIdAdes().setPmoIdAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoIdAdes.Len.PMO_ID_ADES_NULL));
        }
        // COB_CODE: IF IND-PMO-TP-ESTR-CNT = -1
        //              MOVE HIGH-VALUES TO PMO-TP-ESTR-CNT-NULL
        //           END-IF
        if (ws.getIndParamMovi().getTpEstrCnt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-TP-ESTR-CNT-NULL
            paramMovi.setPmoTpEstrCnt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamMoviLdbs1470.Len.PMO_TP_ESTR_CNT));
        }
        // COB_CODE: IF IND-PMO-COD-RAMO = -1
        //              MOVE HIGH-VALUES TO PMO-COD-RAMO-NULL
        //           END-IF
        if (ws.getIndParamMovi().getCodRamo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-COD-RAMO-NULL
            paramMovi.setPmoCodRamo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamMoviLdbs1470.Len.PMO_COD_RAMO));
        }
        // COB_CODE: IF IND-PMO-GEN-DA-SIN = -1
        //              MOVE HIGH-VALUES TO PMO-GEN-DA-SIN-NULL
        //           END-IF
        if (ws.getIndParamMovi().getGenDaSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-GEN-DA-SIN-NULL
            paramMovi.setPmoGenDaSin(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PMO-COD-TARI = -1
        //              MOVE HIGH-VALUES TO PMO-COD-TARI-NULL
        //           END-IF
        if (ws.getIndParamMovi().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-COD-TARI-NULL
            paramMovi.setPmoCodTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamMoviLdbs1470.Len.PMO_COD_TARI));
        }
        // COB_CODE: IF IND-PMO-NUM-RAT-PAG-PRE = -1
        //              MOVE HIGH-VALUES TO PMO-NUM-RAT-PAG-PRE-NULL
        //           END-IF
        if (ws.getIndParamMovi().getNumRatPagPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-NUM-RAT-PAG-PRE-NULL
            paramMovi.getPmoNumRatPagPre().setPmoNumRatPagPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoNumRatPagPre.Len.PMO_NUM_RAT_PAG_PRE_NULL));
        }
        // COB_CODE: IF IND-PMO-PC-SERV-VAL = -1
        //              MOVE HIGH-VALUES TO PMO-PC-SERV-VAL-NULL
        //           END-IF
        if (ws.getIndParamMovi().getPcServVal() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-PC-SERV-VAL-NULL
            paramMovi.getPmoPcServVal().setPmoPcServValNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoPcServVal.Len.PMO_PC_SERV_VAL_NULL));
        }
        // COB_CODE: IF IND-PMO-ETA-AA-SOGL-BNFICR = -1
        //              MOVE HIGH-VALUES TO PMO-ETA-AA-SOGL-BNFICR-NULL
        //           END-IF.
        if (ws.getIndParamMovi().getEtaAaSoglBnficr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PMO-ETA-AA-SOGL-BNFICR-NULL
            paramMovi.getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoEtaAaSoglBnficr.Len.PMO_ETA_AA_SOGL_BNFICR_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO PMO-DS-OPER-SQL
        paramMovi.setPmoDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO PMO-DS-VER
        paramMovi.setPmoDsVer(0);
        // COB_CODE: INITIALIZE WK-ID-OGG
        ws.setWkIdOggFormatted("000000000");
        // COB_CODE: MOVE PMO-ID-POLI
        //             TO WK-ID-OGG
        ws.setWkIdOgg(TruncAbs.toInt(paramMovi.getPmoIdPoli(), 9));
        // COB_CODE: MOVE WK-ID-OGG(8:2)
        //             TO PMO-DS-VER
        paramMovi.setPmoDsVerFormatted(ws.getWkIdOggFormatted().substring((8) - 1, 9));
        // COB_CODE: IF PMO-DS-VER = 00
        //                TO PMO-DS-VER
        //           END-IF
        if (paramMovi.getPmoDsVer() == 0) {
            // COB_CODE: MOVE 100
            //             TO PMO-DS-VER
            paramMovi.setPmoDsVer(100);
        }
        // COB_CODE: MOVE IDSV0003-USER-NAME TO PMO-DS-UTENTE
        paramMovi.setPmoDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO PMO-DS-STATO-ELAB.
        paramMovi.setPmoDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO PMO-DS-OPER-SQL
        paramMovi.setPmoDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO PMO-DS-UTENTE.
        paramMovi.setPmoDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF PMO-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-PMO-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoIdMoviChiu().getPmoIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-ID-MOVI-CHIU
            ws.getIndParamMovi().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-ID-MOVI-CHIU
            ws.getIndParamMovi().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF PMO-TP-MOVI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-TP-MOVI
        //           ELSE
        //              MOVE 0 TO IND-PMO-TP-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoTpMovi().getPmoTpMoviNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-TP-MOVI
            ws.getIndParamMovi().setTpMovi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-TP-MOVI
            ws.getIndParamMovi().setTpMovi(((short)0));
        }
        // COB_CODE: IF PMO-FRQ-MOVI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-FRQ-MOVI
        //           ELSE
        //              MOVE 0 TO IND-PMO-FRQ-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoFrqMovi().getPmoFrqMoviNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-FRQ-MOVI
            ws.getIndParamMovi().setFrqMovi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-FRQ-MOVI
            ws.getIndParamMovi().setFrqMovi(((short)0));
        }
        // COB_CODE: IF PMO-DUR-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-DUR-AA
        //           ELSE
        //              MOVE 0 TO IND-PMO-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoDurAa().getPmoDurAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-DUR-AA
            ws.getIndParamMovi().setDurAa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-DUR-AA
            ws.getIndParamMovi().setDurAa(((short)0));
        }
        // COB_CODE: IF PMO-DUR-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-DUR-MM
        //           ELSE
        //              MOVE 0 TO IND-PMO-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoDurMm().getPmoDurMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-DUR-MM
            ws.getIndParamMovi().setDurMm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-DUR-MM
            ws.getIndParamMovi().setDurMm(((short)0));
        }
        // COB_CODE: IF PMO-DUR-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-DUR-GG
        //           ELSE
        //              MOVE 0 TO IND-PMO-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoDurGg().getPmoDurGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-DUR-GG
            ws.getIndParamMovi().setDurGg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-DUR-GG
            ws.getIndParamMovi().setDurGg(((short)0));
        }
        // COB_CODE: IF PMO-DT-RICOR-PREC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-DT-RICOR-PREC
        //           ELSE
        //              MOVE 0 TO IND-PMO-DT-RICOR-PREC
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoDtRicorPrec().getPmoDtRicorPrecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-DT-RICOR-PREC
            ws.getIndParamMovi().setDtRicorPrec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-DT-RICOR-PREC
            ws.getIndParamMovi().setDtRicorPrec(((short)0));
        }
        // COB_CODE: IF PMO-DT-RICOR-SUCC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-DT-RICOR-SUCC
        //           ELSE
        //              MOVE 0 TO IND-PMO-DT-RICOR-SUCC
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoDtRicorSucc().getPmoDtRicorSuccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-DT-RICOR-SUCC
            ws.getIndParamMovi().setDtRicorSucc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-DT-RICOR-SUCC
            ws.getIndParamMovi().setDtRicorSucc(((short)0));
        }
        // COB_CODE: IF PMO-PC-INTR-FRAZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-PC-INTR-FRAZ
        //           ELSE
        //              MOVE 0 TO IND-PMO-PC-INTR-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoPcIntrFraz().getPmoPcIntrFrazNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-PC-INTR-FRAZ
            ws.getIndParamMovi().setPcIntrFraz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-PC-INTR-FRAZ
            ws.getIndParamMovi().setPcIntrFraz(((short)0));
        }
        // COB_CODE: IF PMO-IMP-BNS-DA-SCO-TOT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-IMP-BNS-DA-SCO-TOT
        //           ELSE
        //              MOVE 0 TO IND-PMO-IMP-BNS-DA-SCO-TOT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoImpBnsDaScoTot().getPmoImpBnsDaScoTotNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-IMP-BNS-DA-SCO-TOT
            ws.getIndParamMovi().setImpBnsDaScoTot(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-IMP-BNS-DA-SCO-TOT
            ws.getIndParamMovi().setImpBnsDaScoTot(((short)0));
        }
        // COB_CODE: IF PMO-IMP-BNS-DA-SCO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-IMP-BNS-DA-SCO
        //           ELSE
        //              MOVE 0 TO IND-PMO-IMP-BNS-DA-SCO
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoImpBnsDaSco().getPmoImpBnsDaScoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-IMP-BNS-DA-SCO
            ws.getIndParamMovi().setImpBnsDaSco(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-IMP-BNS-DA-SCO
            ws.getIndParamMovi().setImpBnsDaSco(((short)0));
        }
        // COB_CODE: IF PMO-PC-ANTIC-BNS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-PC-ANTIC-BNS
        //           ELSE
        //              MOVE 0 TO IND-PMO-PC-ANTIC-BNS
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoPcAnticBns().getPmoPcAnticBnsNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-PC-ANTIC-BNS
            ws.getIndParamMovi().setPcAnticBns(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-PC-ANTIC-BNS
            ws.getIndParamMovi().setPcAnticBns(((short)0));
        }
        // COB_CODE: IF PMO-TP-RINN-COLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-TP-RINN-COLL
        //           ELSE
        //              MOVE 0 TO IND-PMO-TP-RINN-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoTpRinnCollFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-TP-RINN-COLL
            ws.getIndParamMovi().setTpRinnColl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-TP-RINN-COLL
            ws.getIndParamMovi().setTpRinnColl(((short)0));
        }
        // COB_CODE: IF PMO-TP-RIVAL-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-TP-RIVAL-PRE
        //           ELSE
        //              MOVE 0 TO IND-PMO-TP-RIVAL-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoTpRivalPreFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-TP-RIVAL-PRE
            ws.getIndParamMovi().setTpRivalPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-TP-RIVAL-PRE
            ws.getIndParamMovi().setTpRivalPre(((short)0));
        }
        // COB_CODE: IF PMO-TP-RIVAL-PRSTZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-TP-RIVAL-PRSTZ
        //           ELSE
        //              MOVE 0 TO IND-PMO-TP-RIVAL-PRSTZ
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoTpRivalPrstzFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-TP-RIVAL-PRSTZ
            ws.getIndParamMovi().setTpRivalPrstz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-TP-RIVAL-PRSTZ
            ws.getIndParamMovi().setTpRivalPrstz(((short)0));
        }
        // COB_CODE: IF PMO-FL-EVID-RIVAL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-FL-EVID-RIVAL
        //           ELSE
        //              MOVE 0 TO IND-PMO-FL-EVID-RIVAL
        //           END-IF
        if (Conditions.eq(paramMovi.getPmoFlEvidRival(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PMO-FL-EVID-RIVAL
            ws.getIndParamMovi().setFlEvidRival(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-FL-EVID-RIVAL
            ws.getIndParamMovi().setFlEvidRival(((short)0));
        }
        // COB_CODE: IF PMO-ULT-PC-PERD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-ULT-PC-PERD
        //           ELSE
        //              MOVE 0 TO IND-PMO-ULT-PC-PERD
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoUltPcPerd().getPmoUltPcPerdNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-ULT-PC-PERD
            ws.getIndParamMovi().setUltPcPerd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-ULT-PC-PERD
            ws.getIndParamMovi().setUltPcPerd(((short)0));
        }
        // COB_CODE: IF PMO-TOT-AA-GIA-PROR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-TOT-AA-GIA-PROR
        //           ELSE
        //              MOVE 0 TO IND-PMO-TOT-AA-GIA-PROR
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoTotAaGiaPror().getPmoTotAaGiaProrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-TOT-AA-GIA-PROR
            ws.getIndParamMovi().setTotAaGiaPror(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-TOT-AA-GIA-PROR
            ws.getIndParamMovi().setTotAaGiaPror(((short)0));
        }
        // COB_CODE: IF PMO-TP-OPZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-TP-OPZ
        //           ELSE
        //              MOVE 0 TO IND-PMO-TP-OPZ
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoTpOpzFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-TP-OPZ
            ws.getIndParamMovi().setTpOpz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-TP-OPZ
            ws.getIndParamMovi().setTpOpz(((short)0));
        }
        // COB_CODE: IF PMO-AA-REN-CER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-AA-REN-CER
        //           ELSE
        //              MOVE 0 TO IND-PMO-AA-REN-CER
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoAaRenCer().getPmoAaRenCerNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-AA-REN-CER
            ws.getIndParamMovi().setAaRenCer(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-AA-REN-CER
            ws.getIndParamMovi().setAaRenCer(((short)0));
        }
        // COB_CODE: IF PMO-PC-REVRSB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-PC-REVRSB
        //           ELSE
        //              MOVE 0 TO IND-PMO-PC-REVRSB
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoPcRevrsb().getPmoPcRevrsbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-PC-REVRSB
            ws.getIndParamMovi().setPcRevrsb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-PC-REVRSB
            ws.getIndParamMovi().setPcRevrsb(((short)0));
        }
        // COB_CODE: IF PMO-IMP-RISC-PARZ-PRGT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-IMP-RISC-PARZ-PRGT
        //           ELSE
        //              MOVE 0 TO IND-PMO-IMP-RISC-PARZ-PRGT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoImpRiscParzPrgt().getPmoImpRiscParzPrgtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-IMP-RISC-PARZ-PRGT
            ws.getIndParamMovi().setImpRiscParzPrgt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-IMP-RISC-PARZ-PRGT
            ws.getIndParamMovi().setImpRiscParzPrgt(((short)0));
        }
        // COB_CODE: IF PMO-IMP-LRD-DI-RAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-IMP-LRD-DI-RAT
        //           ELSE
        //              MOVE 0 TO IND-PMO-IMP-LRD-DI-RAT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoImpLrdDiRat().getPmoImpLrdDiRatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-IMP-LRD-DI-RAT
            ws.getIndParamMovi().setImpLrdDiRat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-IMP-LRD-DI-RAT
            ws.getIndParamMovi().setImpLrdDiRat(((short)0));
        }
        // COB_CODE: IF PMO-IB-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-IB-OGG
        //           ELSE
        //              MOVE 0 TO IND-PMO-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoIbOgg(), ParamMoviLdbs1470.Len.PMO_IB_OGG)) {
            // COB_CODE: MOVE -1 TO IND-PMO-IB-OGG
            ws.getIndParamMovi().setIbOgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-IB-OGG
            ws.getIndParamMovi().setIbOgg(((short)0));
        }
        // COB_CODE: IF PMO-COS-ONER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-COS-ONER
        //           ELSE
        //              MOVE 0 TO IND-PMO-COS-ONER
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoCosOner().getPmoCosOnerNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-COS-ONER
            ws.getIndParamMovi().setCosOner(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-COS-ONER
            ws.getIndParamMovi().setCosOner(((short)0));
        }
        // COB_CODE: IF PMO-SPE-PC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-SPE-PC
        //           ELSE
        //              MOVE 0 TO IND-PMO-SPE-PC
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoSpePc().getPmoSpePcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-SPE-PC
            ws.getIndParamMovi().setSpePc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-SPE-PC
            ws.getIndParamMovi().setSpePc(((short)0));
        }
        // COB_CODE: IF PMO-FL-ATTIV-GAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-FL-ATTIV-GAR
        //           ELSE
        //              MOVE 0 TO IND-PMO-FL-ATTIV-GAR
        //           END-IF
        if (Conditions.eq(paramMovi.getPmoFlAttivGar(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PMO-FL-ATTIV-GAR
            ws.getIndParamMovi().setFlAttivGar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-FL-ATTIV-GAR
            ws.getIndParamMovi().setFlAttivGar(((short)0));
        }
        // COB_CODE: IF PMO-CAMBIO-VER-PROD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-CAMBIO-VER-PROD
        //           ELSE
        //              MOVE 0 TO IND-PMO-CAMBIO-VER-PROD
        //           END-IF
        if (Conditions.eq(paramMovi.getPmoCambioVerProd(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PMO-CAMBIO-VER-PROD
            ws.getIndParamMovi().setCambioVerProd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-CAMBIO-VER-PROD
            ws.getIndParamMovi().setCambioVerProd(((short)0));
        }
        // COB_CODE: IF PMO-MM-DIFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-MM-DIFF
        //           ELSE
        //              MOVE 0 TO IND-PMO-MM-DIFF
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoMmDiff().getPmoMmDiffNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-MM-DIFF
            ws.getIndParamMovi().setMmDiff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-MM-DIFF
            ws.getIndParamMovi().setMmDiff(((short)0));
        }
        // COB_CODE: IF PMO-IMP-RAT-MANFEE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-IMP-RAT-MANFEE
        //           ELSE
        //              MOVE 0 TO IND-PMO-IMP-RAT-MANFEE
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoImpRatManfee().getPmoImpRatManfeeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-IMP-RAT-MANFEE
            ws.getIndParamMovi().setImpRatManfee(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-IMP-RAT-MANFEE
            ws.getIndParamMovi().setImpRatManfee(((short)0));
        }
        // COB_CODE: IF PMO-DT-ULT-EROG-MANFEE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-DT-ULT-EROG-MANFEE
        //           ELSE
        //              MOVE 0 TO IND-PMO-DT-ULT-EROG-MANFEE
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoDtUltErogManfee().getPmoDtUltErogManfeeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-DT-ULT-EROG-MANFEE
            ws.getIndParamMovi().setDtUltErogManfee(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-DT-ULT-EROG-MANFEE
            ws.getIndParamMovi().setDtUltErogManfee(((short)0));
        }
        // COB_CODE: IF PMO-TP-OGG-RIVAL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-TP-OGG-RIVAL
        //           ELSE
        //              MOVE 0 TO IND-PMO-TP-OGG-RIVAL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoTpOggRivalFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-TP-OGG-RIVAL
            ws.getIndParamMovi().setTpOggRival(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-TP-OGG-RIVAL
            ws.getIndParamMovi().setTpOggRival(((short)0));
        }
        // COB_CODE: IF PMO-SOM-ASSTA-GARAC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-SOM-ASSTA-GARAC
        //           ELSE
        //              MOVE 0 TO IND-PMO-SOM-ASSTA-GARAC
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoSomAsstaGarac().getPmoSomAsstaGaracNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-SOM-ASSTA-GARAC
            ws.getIndParamMovi().setSomAsstaGarac(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-SOM-ASSTA-GARAC
            ws.getIndParamMovi().setSomAsstaGarac(((short)0));
        }
        // COB_CODE: IF PMO-PC-APPLZ-OPZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-PC-APPLZ-OPZ
        //           ELSE
        //              MOVE 0 TO IND-PMO-PC-APPLZ-OPZ
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoPcApplzOpz().getPmoPcApplzOpzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-PC-APPLZ-OPZ
            ws.getIndParamMovi().setPcApplzOpz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-PC-APPLZ-OPZ
            ws.getIndParamMovi().setPcApplzOpz(((short)0));
        }
        // COB_CODE: IF PMO-ID-ADES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-ID-ADES
        //           ELSE
        //              MOVE 0 TO IND-PMO-ID-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoIdAdes().getPmoIdAdesNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-ID-ADES
            ws.getIndParamMovi().setIdAdes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-ID-ADES
            ws.getIndParamMovi().setIdAdes(((short)0));
        }
        // COB_CODE: IF PMO-TP-ESTR-CNT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-TP-ESTR-CNT
        //           ELSE
        //              MOVE 0 TO IND-PMO-TP-ESTR-CNT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoTpEstrCntFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-TP-ESTR-CNT
            ws.getIndParamMovi().setTpEstrCnt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-TP-ESTR-CNT
            ws.getIndParamMovi().setTpEstrCnt(((short)0));
        }
        // COB_CODE: IF PMO-COD-RAMO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-COD-RAMO
        //           ELSE
        //              MOVE 0 TO IND-PMO-COD-RAMO
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoCodRamoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-COD-RAMO
            ws.getIndParamMovi().setCodRamo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-COD-RAMO
            ws.getIndParamMovi().setCodRamo(((short)0));
        }
        // COB_CODE: IF PMO-GEN-DA-SIN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-GEN-DA-SIN
        //           ELSE
        //              MOVE 0 TO IND-PMO-GEN-DA-SIN
        //           END-IF
        if (Conditions.eq(paramMovi.getPmoGenDaSin(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PMO-GEN-DA-SIN
            ws.getIndParamMovi().setGenDaSin(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-GEN-DA-SIN
            ws.getIndParamMovi().setGenDaSin(((short)0));
        }
        // COB_CODE: IF PMO-COD-TARI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-COD-TARI
        //           ELSE
        //              MOVE 0 TO IND-PMO-COD-TARI
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoCodTariFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-COD-TARI
            ws.getIndParamMovi().setCodTari(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-COD-TARI
            ws.getIndParamMovi().setCodTari(((short)0));
        }
        // COB_CODE: IF PMO-NUM-RAT-PAG-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-NUM-RAT-PAG-PRE
        //           ELSE
        //              MOVE 0 TO IND-PMO-NUM-RAT-PAG-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoNumRatPagPre().getPmoNumRatPagPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-NUM-RAT-PAG-PRE
            ws.getIndParamMovi().setNumRatPagPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-NUM-RAT-PAG-PRE
            ws.getIndParamMovi().setNumRatPagPre(((short)0));
        }
        // COB_CODE: IF PMO-PC-SERV-VAL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-PC-SERV-VAL
        //           ELSE
        //              MOVE 0 TO IND-PMO-PC-SERV-VAL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramMovi.getPmoPcServVal().getPmoPcServValNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-PC-SERV-VAL
            ws.getIndParamMovi().setPcServVal(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-PC-SERV-VAL
            ws.getIndParamMovi().setPcServVal(((short)0));
        }
        // COB_CODE: IF PMO-ETA-AA-SOGL-BNFICR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PMO-ETA-AA-SOGL-BNFICR
        //           ELSE
        //              MOVE 0 TO IND-PMO-ETA-AA-SOGL-BNFICR
        //           END-IF.
        if (Characters.EQ_HIGH.test(paramMovi.getPmoEtaAaSoglBnficr().getPmoEtaAaSoglBnficrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PMO-ETA-AA-SOGL-BNFICR
            ws.getIndParamMovi().setEtaAaSoglBnficr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PMO-ETA-AA-SOGL-BNFICR
            ws.getIndParamMovi().setEtaAaSoglBnficr(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : PMO-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE PARAM-MOVI TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(paramMovi.getParamMoviFormatted());
        // COB_CODE: MOVE PMO-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(paramMovi.getPmoIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO PMO-ID-MOVI-CHIU
                paramMovi.getPmoIdMoviChiu().setPmoIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO PMO-DS-TS-END-CPTZ
                paramMovi.setPmoDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO PMO-ID-MOVI-CRZ
                    paramMovi.setPmoIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO PMO-ID-MOVI-CHIU-NULL
                    paramMovi.getPmoIdMoviChiu().setPmoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoIdMoviChiu.Len.PMO_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO PMO-DT-END-EFF
                    paramMovi.setPmoDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO PMO-DS-TS-INI-CPTZ
                    paramMovi.setPmoDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO PMO-DS-TS-END-CPTZ
                    paramMovi.setPmoDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE PARAM-MOVI TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(paramMovi.getParamMoviFormatted());
        // COB_CODE: MOVE PMO-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(paramMovi.getPmoIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO PARAM-MOVI.
        paramMovi.setParamMoviFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO PMO-ID-MOVI-CRZ.
        paramMovi.setPmoIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO PMO-ID-MOVI-CHIU-NULL.
        paramMovi.getPmoIdMoviChiu().setPmoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PmoIdMoviChiu.Len.PMO_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO PMO-DT-INI-EFF.
        paramMovi.setPmoDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO PMO-DT-END-EFF.
        paramMovi.setPmoDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO PMO-DS-TS-INI-CPTZ.
        paramMovi.setPmoDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO PMO-DS-TS-END-CPTZ.
        paramMovi.setPmoDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO PMO-COD-COMP-ANIA.
        paramMovi.setPmoCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE PMO-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramMovi.getPmoDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO PMO-DT-INI-EFF-DB
        ws.getParamMoviDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE PMO-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramMovi.getPmoDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO PMO-DT-END-EFF-DB
        ws.getParamMoviDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-PMO-DT-RICOR-PREC = 0
        //               MOVE WS-DATE-X      TO PMO-DT-RICOR-PREC-DB
        //           END-IF
        if (ws.getIndParamMovi().getDtRicorPrec() == 0) {
            // COB_CODE: MOVE PMO-DT-RICOR-PREC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramMovi.getPmoDtRicorPrec().getPmoDtRicorPrec(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PMO-DT-RICOR-PREC-DB
            ws.getParamMoviDb().setIniCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PMO-DT-RICOR-SUCC = 0
        //               MOVE WS-DATE-X      TO PMO-DT-RICOR-SUCC-DB
        //           END-IF
        if (ws.getIndParamMovi().getDtRicorSucc() == 0) {
            // COB_CODE: MOVE PMO-DT-RICOR-SUCC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramMovi.getPmoDtRicorSucc().getPmoDtRicorSucc(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PMO-DT-RICOR-SUCC-DB
            ws.getParamMoviDb().setEndCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PMO-DT-ULT-EROG-MANFEE = 0
        //               MOVE WS-DATE-X      TO PMO-DT-ULT-EROG-MANFEE-DB
        //           END-IF.
        if (ws.getIndParamMovi().getDtUltErogManfee() == 0) {
            // COB_CODE: MOVE PMO-DT-ULT-EROG-MANFEE TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramMovi.getPmoDtUltErogManfee().getPmoDtUltErogManfee(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PMO-DT-ULT-EROG-MANFEE-DB
            ws.getParamMoviDb().setEsiTitDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE PMO-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getParamMoviDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO PMO-DT-INI-EFF
        paramMovi.setPmoDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE PMO-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getParamMoviDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO PMO-DT-END-EFF
        paramMovi.setPmoDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-PMO-DT-RICOR-PREC = 0
        //               MOVE WS-DATE-N      TO PMO-DT-RICOR-PREC
        //           END-IF
        if (ws.getIndParamMovi().getDtRicorPrec() == 0) {
            // COB_CODE: MOVE PMO-DT-RICOR-PREC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamMoviDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PMO-DT-RICOR-PREC
            paramMovi.getPmoDtRicorPrec().setPmoDtRicorPrec(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PMO-DT-RICOR-SUCC = 0
        //               MOVE WS-DATE-N      TO PMO-DT-RICOR-SUCC
        //           END-IF
        if (ws.getIndParamMovi().getDtRicorSucc() == 0) {
            // COB_CODE: MOVE PMO-DT-RICOR-SUCC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamMoviDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PMO-DT-RICOR-SUCC
            paramMovi.getPmoDtRicorSucc().setPmoDtRicorSucc(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PMO-DT-ULT-EROG-MANFEE = 0
        //               MOVE WS-DATE-N      TO PMO-DT-ULT-EROG-MANFEE
        //           END-IF.
        if (ws.getIndParamMovi().getDtUltErogManfee() == 0) {
            // COB_CODE: MOVE PMO-DT-ULT-EROG-MANFEE-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamMoviDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PMO-DT-ULT-EROG-MANFEE
            paramMovi.getPmoDtUltErogManfee().setPmoDtUltErogManfee(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getAaRenCer() {
        return paramMovi.getPmoAaRenCer().getPmoAaRenCer();
    }

    @Override
    public void setAaRenCer(int aaRenCer) {
        this.paramMovi.getPmoAaRenCer().setPmoAaRenCer(aaRenCer);
    }

    @Override
    public Integer getAaRenCerObj() {
        if (ws.getIndParamMovi().getAaRenCer() >= 0) {
            return ((Integer)getAaRenCer());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaRenCerObj(Integer aaRenCerObj) {
        if (aaRenCerObj != null) {
            setAaRenCer(((int)aaRenCerObj));
            ws.getIndParamMovi().setAaRenCer(((short)0));
        }
        else {
            ws.getIndParamMovi().setAaRenCer(((short)-1));
        }
    }

    @Override
    public char getCambioVerProd() {
        return paramMovi.getPmoCambioVerProd();
    }

    @Override
    public void setCambioVerProd(char cambioVerProd) {
        this.paramMovi.setPmoCambioVerProd(cambioVerProd);
    }

    @Override
    public Character getCambioVerProdObj() {
        if (ws.getIndParamMovi().getCambioVerProd() >= 0) {
            return ((Character)getCambioVerProd());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCambioVerProdObj(Character cambioVerProdObj) {
        if (cambioVerProdObj != null) {
            setCambioVerProd(((char)cambioVerProdObj));
            ws.getIndParamMovi().setCambioVerProd(((short)0));
        }
        else {
            ws.getIndParamMovi().setCambioVerProd(((short)-1));
        }
    }

    @Override
    public String getCodRamo() {
        return paramMovi.getPmoCodRamo();
    }

    @Override
    public void setCodRamo(String codRamo) {
        this.paramMovi.setPmoCodRamo(codRamo);
    }

    @Override
    public String getCodRamoObj() {
        if (ws.getIndParamMovi().getCodRamo() >= 0) {
            return getCodRamo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodRamoObj(String codRamoObj) {
        if (codRamoObj != null) {
            setCodRamo(codRamoObj);
            ws.getIndParamMovi().setCodRamo(((short)0));
        }
        else {
            ws.getIndParamMovi().setCodRamo(((short)-1));
        }
    }

    @Override
    public String getCodTari() {
        return paramMovi.getPmoCodTari();
    }

    @Override
    public void setCodTari(String codTari) {
        this.paramMovi.setPmoCodTari(codTari);
    }

    @Override
    public String getCodTariObj() {
        if (ws.getIndParamMovi().getCodTari() >= 0) {
            return getCodTari();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTariObj(String codTariObj) {
        if (codTariObj != null) {
            setCodTari(codTariObj);
            ws.getIndParamMovi().setCodTari(((short)0));
        }
        else {
            ws.getIndParamMovi().setCodTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getCosOner() {
        return paramMovi.getPmoCosOner().getPmoCosOner();
    }

    @Override
    public void setCosOner(AfDecimal cosOner) {
        this.paramMovi.getPmoCosOner().setPmoCosOner(cosOner.copy());
    }

    @Override
    public AfDecimal getCosOnerObj() {
        if (ws.getIndParamMovi().getCosOner() >= 0) {
            return getCosOner();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCosOnerObj(AfDecimal cosOnerObj) {
        if (cosOnerObj != null) {
            setCosOner(new AfDecimal(cosOnerObj, 15, 3));
            ws.getIndParamMovi().setCosOner(((short)0));
        }
        else {
            ws.getIndParamMovi().setCosOner(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return paramMovi.getPmoDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.paramMovi.setPmoDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return paramMovi.getPmoDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.paramMovi.setPmoDsStatoElab(dsStatoElab);
    }

    @Override
    public String getDsUtente() {
        return paramMovi.getPmoDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.paramMovi.setPmoDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return paramMovi.getPmoDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.paramMovi.setPmoDsVer(dsVer);
    }

    @Override
    public String getDtRicorPrecDb() {
        return ws.getParamMoviDb().getIniCopDb();
    }

    @Override
    public void setDtRicorPrecDb(String dtRicorPrecDb) {
        this.ws.getParamMoviDb().setIniCopDb(dtRicorPrecDb);
    }

    @Override
    public String getDtRicorPrecDbObj() {
        if (ws.getIndParamMovi().getDtRicorPrec() >= 0) {
            return getDtRicorPrecDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRicorPrecDbObj(String dtRicorPrecDbObj) {
        if (dtRicorPrecDbObj != null) {
            setDtRicorPrecDb(dtRicorPrecDbObj);
            ws.getIndParamMovi().setDtRicorPrec(((short)0));
        }
        else {
            ws.getIndParamMovi().setDtRicorPrec(((short)-1));
        }
    }

    @Override
    public String getDtUltErogManfeeDb() {
        return ws.getParamMoviDb().getEsiTitDb();
    }

    @Override
    public void setDtUltErogManfeeDb(String dtUltErogManfeeDb) {
        this.ws.getParamMoviDb().setEsiTitDb(dtUltErogManfeeDb);
    }

    @Override
    public String getDtUltErogManfeeDbObj() {
        if (ws.getIndParamMovi().getDtUltErogManfee() >= 0) {
            return getDtUltErogManfeeDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltErogManfeeDbObj(String dtUltErogManfeeDbObj) {
        if (dtUltErogManfeeDbObj != null) {
            setDtUltErogManfeeDb(dtUltErogManfeeDbObj);
            ws.getIndParamMovi().setDtUltErogManfee(((short)0));
        }
        else {
            ws.getIndParamMovi().setDtUltErogManfee(((short)-1));
        }
    }

    @Override
    public int getDurAa() {
        return paramMovi.getPmoDurAa().getPmoDurAa();
    }

    @Override
    public void setDurAa(int durAa) {
        this.paramMovi.getPmoDurAa().setPmoDurAa(durAa);
    }

    @Override
    public Integer getDurAaObj() {
        if (ws.getIndParamMovi().getDurAa() >= 0) {
            return ((Integer)getDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        if (durAaObj != null) {
            setDurAa(((int)durAaObj));
            ws.getIndParamMovi().setDurAa(((short)0));
        }
        else {
            ws.getIndParamMovi().setDurAa(((short)-1));
        }
    }

    @Override
    public int getDurGg() {
        return paramMovi.getPmoDurGg().getPmoDurGg();
    }

    @Override
    public void setDurGg(int durGg) {
        this.paramMovi.getPmoDurGg().setPmoDurGg(durGg);
    }

    @Override
    public Integer getDurGgObj() {
        if (ws.getIndParamMovi().getDurGg() >= 0) {
            return ((Integer)getDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        if (durGgObj != null) {
            setDurGg(((int)durGgObj));
            ws.getIndParamMovi().setDurGg(((short)0));
        }
        else {
            ws.getIndParamMovi().setDurGg(((short)-1));
        }
    }

    @Override
    public int getDurMm() {
        return paramMovi.getPmoDurMm().getPmoDurMm();
    }

    @Override
    public void setDurMm(int durMm) {
        this.paramMovi.getPmoDurMm().setPmoDurMm(durMm);
    }

    @Override
    public Integer getDurMmObj() {
        if (ws.getIndParamMovi().getDurMm() >= 0) {
            return ((Integer)getDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        if (durMmObj != null) {
            setDurMm(((int)durMmObj));
            ws.getIndParamMovi().setDurMm(((short)0));
        }
        else {
            ws.getIndParamMovi().setDurMm(((short)-1));
        }
    }

    @Override
    public short getEtaAaSoglBnficr() {
        return paramMovi.getPmoEtaAaSoglBnficr().getPmoEtaAaSoglBnficr();
    }

    @Override
    public void setEtaAaSoglBnficr(short etaAaSoglBnficr) {
        this.paramMovi.getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficr(etaAaSoglBnficr);
    }

    @Override
    public Short getEtaAaSoglBnficrObj() {
        if (ws.getIndParamMovi().getEtaAaSoglBnficr() >= 0) {
            return ((Short)getEtaAaSoglBnficr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAaSoglBnficrObj(Short etaAaSoglBnficrObj) {
        if (etaAaSoglBnficrObj != null) {
            setEtaAaSoglBnficr(((short)etaAaSoglBnficrObj));
            ws.getIndParamMovi().setEtaAaSoglBnficr(((short)0));
        }
        else {
            ws.getIndParamMovi().setEtaAaSoglBnficr(((short)-1));
        }
    }

    @Override
    public char getFlAttivGar() {
        return paramMovi.getPmoFlAttivGar();
    }

    @Override
    public void setFlAttivGar(char flAttivGar) {
        this.paramMovi.setPmoFlAttivGar(flAttivGar);
    }

    @Override
    public Character getFlAttivGarObj() {
        if (ws.getIndParamMovi().getFlAttivGar() >= 0) {
            return ((Character)getFlAttivGar());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlAttivGarObj(Character flAttivGarObj) {
        if (flAttivGarObj != null) {
            setFlAttivGar(((char)flAttivGarObj));
            ws.getIndParamMovi().setFlAttivGar(((short)0));
        }
        else {
            ws.getIndParamMovi().setFlAttivGar(((short)-1));
        }
    }

    @Override
    public char getFlEvidRival() {
        return paramMovi.getPmoFlEvidRival();
    }

    @Override
    public void setFlEvidRival(char flEvidRival) {
        this.paramMovi.setPmoFlEvidRival(flEvidRival);
    }

    @Override
    public Character getFlEvidRivalObj() {
        if (ws.getIndParamMovi().getFlEvidRival() >= 0) {
            return ((Character)getFlEvidRival());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlEvidRivalObj(Character flEvidRivalObj) {
        if (flEvidRivalObj != null) {
            setFlEvidRival(((char)flEvidRivalObj));
            ws.getIndParamMovi().setFlEvidRival(((short)0));
        }
        else {
            ws.getIndParamMovi().setFlEvidRival(((short)-1));
        }
    }

    @Override
    public int getFrqMovi() {
        return paramMovi.getPmoFrqMovi().getPmoFrqMovi();
    }

    @Override
    public void setFrqMovi(int frqMovi) {
        this.paramMovi.getPmoFrqMovi().setPmoFrqMovi(frqMovi);
    }

    @Override
    public Integer getFrqMoviObj() {
        if (ws.getIndParamMovi().getFrqMovi() >= 0) {
            return ((Integer)getFrqMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrqMoviObj(Integer frqMoviObj) {
        if (frqMoviObj != null) {
            setFrqMovi(((int)frqMoviObj));
            ws.getIndParamMovi().setFrqMovi(((short)0));
        }
        else {
            ws.getIndParamMovi().setFrqMovi(((short)-1));
        }
    }

    @Override
    public char getGenDaSin() {
        return paramMovi.getPmoGenDaSin();
    }

    @Override
    public void setGenDaSin(char genDaSin) {
        this.paramMovi.setPmoGenDaSin(genDaSin);
    }

    @Override
    public Character getGenDaSinObj() {
        if (ws.getIndParamMovi().getGenDaSin() >= 0) {
            return ((Character)getGenDaSin());
        }
        else {
            return null;
        }
    }

    @Override
    public void setGenDaSinObj(Character genDaSinObj) {
        if (genDaSinObj != null) {
            setGenDaSin(((char)genDaSinObj));
            ws.getIndParamMovi().setGenDaSin(((short)0));
        }
        else {
            ws.getIndParamMovi().setGenDaSin(((short)-1));
        }
    }

    @Override
    public char getIabv0002State01() {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public char getIabv0002State02() {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public char getIabv0002State03() {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public char getIabv0002State04() {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public char getIabv0002State05() {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public char getIabv0002State06() {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public char getIabv0002State07() {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public char getIabv0002State08() {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public char getIabv0002State09() {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public char getIabv0002State10() {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public int getIabv0009IdOggA() {
        throw new FieldNotMappedException("iabv0009IdOggA");
    }

    @Override
    public void setIabv0009IdOggA(int iabv0009IdOggA) {
        throw new FieldNotMappedException("iabv0009IdOggA");
    }

    @Override
    public int getIabv0009IdOggDa() {
        throw new FieldNotMappedException("iabv0009IdOggDa");
    }

    @Override
    public void setIabv0009IdOggDa(int iabv0009IdOggDa) {
        throw new FieldNotMappedException("iabv0009IdOggDa");
    }

    @Override
    public String getIbOgg() {
        return paramMovi.getPmoIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.paramMovi.setPmoIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndParamMovi().getIbOgg() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndParamMovi().setIbOgg(((short)0));
        }
        else {
            ws.getIndParamMovi().setIbOgg(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return paramMovi.getPmoIdMoviChiu().getPmoIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.paramMovi.getPmoIdMoviChiu().setPmoIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndParamMovi().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndParamMovi().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndParamMovi().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return paramMovi.getPmoIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.paramMovi.setPmoIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdParamMovi() {
        return paramMovi.getPmoIdParamMovi();
    }

    @Override
    public void setIdParamMovi(int idParamMovi) {
        this.paramMovi.setPmoIdParamMovi(idParamMovi);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpBnsDaSco() {
        return paramMovi.getPmoImpBnsDaSco().getPmoImpBnsDaSco();
    }

    @Override
    public void setImpBnsDaSco(AfDecimal impBnsDaSco) {
        this.paramMovi.getPmoImpBnsDaSco().setPmoImpBnsDaSco(impBnsDaSco.copy());
    }

    @Override
    public AfDecimal getImpBnsDaScoObj() {
        if (ws.getIndParamMovi().getImpBnsDaSco() >= 0) {
            return getImpBnsDaSco();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpBnsDaScoObj(AfDecimal impBnsDaScoObj) {
        if (impBnsDaScoObj != null) {
            setImpBnsDaSco(new AfDecimal(impBnsDaScoObj, 15, 3));
            ws.getIndParamMovi().setImpBnsDaSco(((short)0));
        }
        else {
            ws.getIndParamMovi().setImpBnsDaSco(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpBnsDaScoTot() {
        return paramMovi.getPmoImpBnsDaScoTot().getPmoImpBnsDaScoTot();
    }

    @Override
    public void setImpBnsDaScoTot(AfDecimal impBnsDaScoTot) {
        this.paramMovi.getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTot(impBnsDaScoTot.copy());
    }

    @Override
    public AfDecimal getImpBnsDaScoTotObj() {
        if (ws.getIndParamMovi().getImpBnsDaScoTot() >= 0) {
            return getImpBnsDaScoTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpBnsDaScoTotObj(AfDecimal impBnsDaScoTotObj) {
        if (impBnsDaScoTotObj != null) {
            setImpBnsDaScoTot(new AfDecimal(impBnsDaScoTotObj, 15, 3));
            ws.getIndParamMovi().setImpBnsDaScoTot(((short)0));
        }
        else {
            ws.getIndParamMovi().setImpBnsDaScoTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdDiRat() {
        return paramMovi.getPmoImpLrdDiRat().getPmoImpLrdDiRat();
    }

    @Override
    public void setImpLrdDiRat(AfDecimal impLrdDiRat) {
        this.paramMovi.getPmoImpLrdDiRat().setPmoImpLrdDiRat(impLrdDiRat.copy());
    }

    @Override
    public AfDecimal getImpLrdDiRatObj() {
        if (ws.getIndParamMovi().getImpLrdDiRat() >= 0) {
            return getImpLrdDiRat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdDiRatObj(AfDecimal impLrdDiRatObj) {
        if (impLrdDiRatObj != null) {
            setImpLrdDiRat(new AfDecimal(impLrdDiRatObj, 15, 3));
            ws.getIndParamMovi().setImpLrdDiRat(((short)0));
        }
        else {
            ws.getIndParamMovi().setImpLrdDiRat(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRatManfee() {
        return paramMovi.getPmoImpRatManfee().getPmoImpRatManfee();
    }

    @Override
    public void setImpRatManfee(AfDecimal impRatManfee) {
        this.paramMovi.getPmoImpRatManfee().setPmoImpRatManfee(impRatManfee.copy());
    }

    @Override
    public AfDecimal getImpRatManfeeObj() {
        if (ws.getIndParamMovi().getImpRatManfee() >= 0) {
            return getImpRatManfee();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRatManfeeObj(AfDecimal impRatManfeeObj) {
        if (impRatManfeeObj != null) {
            setImpRatManfee(new AfDecimal(impRatManfeeObj, 15, 3));
            ws.getIndParamMovi().setImpRatManfee(((short)0));
        }
        else {
            ws.getIndParamMovi().setImpRatManfee(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRiscParzPrgt() {
        return paramMovi.getPmoImpRiscParzPrgt().getPmoImpRiscParzPrgt();
    }

    @Override
    public void setImpRiscParzPrgt(AfDecimal impRiscParzPrgt) {
        this.paramMovi.getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgt(impRiscParzPrgt.copy());
    }

    @Override
    public AfDecimal getImpRiscParzPrgtObj() {
        if (ws.getIndParamMovi().getImpRiscParzPrgt() >= 0) {
            return getImpRiscParzPrgt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRiscParzPrgtObj(AfDecimal impRiscParzPrgtObj) {
        if (impRiscParzPrgtObj != null) {
            setImpRiscParzPrgt(new AfDecimal(impRiscParzPrgtObj, 15, 3));
            ws.getIndParamMovi().setImpRiscParzPrgt(((short)0));
        }
        else {
            ws.getIndParamMovi().setImpRiscParzPrgt(((short)-1));
        }
    }

    @Override
    public int getLdbv0641IdPoli() {
        throw new FieldNotMappedException("ldbv0641IdPoli");
    }

    @Override
    public void setLdbv0641IdPoli(int ldbv0641IdPoli) {
        throw new FieldNotMappedException("ldbv0641IdPoli");
    }

    @Override
    public int getLdbv0641TpMovi10() {
        throw new FieldNotMappedException("ldbv0641TpMovi10");
    }

    @Override
    public void setLdbv0641TpMovi10(int ldbv0641TpMovi10) {
        throw new FieldNotMappedException("ldbv0641TpMovi10");
    }

    @Override
    public int getLdbv0641TpMovi11() {
        throw new FieldNotMappedException("ldbv0641TpMovi11");
    }

    @Override
    public void setLdbv0641TpMovi11(int ldbv0641TpMovi11) {
        throw new FieldNotMappedException("ldbv0641TpMovi11");
    }

    @Override
    public int getLdbv0641TpMovi12() {
        throw new FieldNotMappedException("ldbv0641TpMovi12");
    }

    @Override
    public void setLdbv0641TpMovi12(int ldbv0641TpMovi12) {
        throw new FieldNotMappedException("ldbv0641TpMovi12");
    }

    @Override
    public int getLdbv0641TpMovi13() {
        throw new FieldNotMappedException("ldbv0641TpMovi13");
    }

    @Override
    public void setLdbv0641TpMovi13(int ldbv0641TpMovi13) {
        throw new FieldNotMappedException("ldbv0641TpMovi13");
    }

    @Override
    public int getLdbv0641TpMovi14() {
        throw new FieldNotMappedException("ldbv0641TpMovi14");
    }

    @Override
    public void setLdbv0641TpMovi14(int ldbv0641TpMovi14) {
        throw new FieldNotMappedException("ldbv0641TpMovi14");
    }

    @Override
    public int getLdbv0641TpMovi15() {
        throw new FieldNotMappedException("ldbv0641TpMovi15");
    }

    @Override
    public void setLdbv0641TpMovi15(int ldbv0641TpMovi15) {
        throw new FieldNotMappedException("ldbv0641TpMovi15");
    }

    @Override
    public int getLdbv0641TpMovi1() {
        throw new FieldNotMappedException("ldbv0641TpMovi1");
    }

    @Override
    public void setLdbv0641TpMovi1(int ldbv0641TpMovi1) {
        throw new FieldNotMappedException("ldbv0641TpMovi1");
    }

    @Override
    public int getLdbv0641TpMovi2() {
        throw new FieldNotMappedException("ldbv0641TpMovi2");
    }

    @Override
    public void setLdbv0641TpMovi2(int ldbv0641TpMovi2) {
        throw new FieldNotMappedException("ldbv0641TpMovi2");
    }

    @Override
    public int getLdbv0641TpMovi3() {
        throw new FieldNotMappedException("ldbv0641TpMovi3");
    }

    @Override
    public void setLdbv0641TpMovi3(int ldbv0641TpMovi3) {
        throw new FieldNotMappedException("ldbv0641TpMovi3");
    }

    @Override
    public int getLdbv0641TpMovi4() {
        throw new FieldNotMappedException("ldbv0641TpMovi4");
    }

    @Override
    public void setLdbv0641TpMovi4(int ldbv0641TpMovi4) {
        throw new FieldNotMappedException("ldbv0641TpMovi4");
    }

    @Override
    public int getLdbv0641TpMovi5() {
        throw new FieldNotMappedException("ldbv0641TpMovi5");
    }

    @Override
    public void setLdbv0641TpMovi5(int ldbv0641TpMovi5) {
        throw new FieldNotMappedException("ldbv0641TpMovi5");
    }

    @Override
    public int getLdbv0641TpMovi6() {
        throw new FieldNotMappedException("ldbv0641TpMovi6");
    }

    @Override
    public void setLdbv0641TpMovi6(int ldbv0641TpMovi6) {
        throw new FieldNotMappedException("ldbv0641TpMovi6");
    }

    @Override
    public int getLdbv0641TpMovi7() {
        throw new FieldNotMappedException("ldbv0641TpMovi7");
    }

    @Override
    public void setLdbv0641TpMovi7(int ldbv0641TpMovi7) {
        throw new FieldNotMappedException("ldbv0641TpMovi7");
    }

    @Override
    public int getLdbv0641TpMovi8() {
        throw new FieldNotMappedException("ldbv0641TpMovi8");
    }

    @Override
    public void setLdbv0641TpMovi8(int ldbv0641TpMovi8) {
        throw new FieldNotMappedException("ldbv0641TpMovi8");
    }

    @Override
    public int getLdbv0641TpMovi9() {
        throw new FieldNotMappedException("ldbv0641TpMovi9");
    }

    @Override
    public void setLdbv0641TpMovi9(int ldbv0641TpMovi9) {
        throw new FieldNotMappedException("ldbv0641TpMovi9");
    }

    @Override
    public int getLdbv1471TpMovi01() {
        throw new FieldNotMappedException("ldbv1471TpMovi01");
    }

    @Override
    public void setLdbv1471TpMovi01(int ldbv1471TpMovi01) {
        throw new FieldNotMappedException("ldbv1471TpMovi01");
    }

    @Override
    public int getLdbv1471TpMovi02() {
        throw new FieldNotMappedException("ldbv1471TpMovi02");
    }

    @Override
    public void setLdbv1471TpMovi02(int ldbv1471TpMovi02) {
        throw new FieldNotMappedException("ldbv1471TpMovi02");
    }

    @Override
    public int getLdbv1471TpMovi03() {
        throw new FieldNotMappedException("ldbv1471TpMovi03");
    }

    @Override
    public void setLdbv1471TpMovi03(int ldbv1471TpMovi03) {
        throw new FieldNotMappedException("ldbv1471TpMovi03");
    }

    @Override
    public int getLdbv1471TpMovi04() {
        throw new FieldNotMappedException("ldbv1471TpMovi04");
    }

    @Override
    public void setLdbv1471TpMovi04(int ldbv1471TpMovi04) {
        throw new FieldNotMappedException("ldbv1471TpMovi04");
    }

    @Override
    public int getLdbv1471TpMovi05() {
        throw new FieldNotMappedException("ldbv1471TpMovi05");
    }

    @Override
    public void setLdbv1471TpMovi05(int ldbv1471TpMovi05) {
        throw new FieldNotMappedException("ldbv1471TpMovi05");
    }

    @Override
    public int getLdbv1471TpMovi06() {
        throw new FieldNotMappedException("ldbv1471TpMovi06");
    }

    @Override
    public void setLdbv1471TpMovi06(int ldbv1471TpMovi06) {
        throw new FieldNotMappedException("ldbv1471TpMovi06");
    }

    @Override
    public int getLdbv1471TpMovi07() {
        throw new FieldNotMappedException("ldbv1471TpMovi07");
    }

    @Override
    public void setLdbv1471TpMovi07(int ldbv1471TpMovi07) {
        throw new FieldNotMappedException("ldbv1471TpMovi07");
    }

    @Override
    public int getLdbv1471TpMovi08() {
        throw new FieldNotMappedException("ldbv1471TpMovi08");
    }

    @Override
    public void setLdbv1471TpMovi08(int ldbv1471TpMovi08) {
        throw new FieldNotMappedException("ldbv1471TpMovi08");
    }

    @Override
    public int getLdbv1471TpMovi09() {
        throw new FieldNotMappedException("ldbv1471TpMovi09");
    }

    @Override
    public void setLdbv1471TpMovi09(int ldbv1471TpMovi09) {
        throw new FieldNotMappedException("ldbv1471TpMovi09");
    }

    @Override
    public int getLdbv1471TpMovi10() {
        throw new FieldNotMappedException("ldbv1471TpMovi10");
    }

    @Override
    public void setLdbv1471TpMovi10(int ldbv1471TpMovi10) {
        throw new FieldNotMappedException("ldbv1471TpMovi10");
    }

    @Override
    public int getLdbv2681IdOgg() {
        throw new FieldNotMappedException("ldbv2681IdOgg");
    }

    @Override
    public void setLdbv2681IdOgg(int ldbv2681IdOgg) {
        throw new FieldNotMappedException("ldbv2681IdOgg");
    }

    @Override
    public int getLdbv2681TpMovi01() {
        throw new FieldNotMappedException("ldbv2681TpMovi01");
    }

    @Override
    public void setLdbv2681TpMovi01(int ldbv2681TpMovi01) {
        throw new FieldNotMappedException("ldbv2681TpMovi01");
    }

    @Override
    public int getLdbv2681TpMovi02() {
        throw new FieldNotMappedException("ldbv2681TpMovi02");
    }

    @Override
    public void setLdbv2681TpMovi02(int ldbv2681TpMovi02) {
        throw new FieldNotMappedException("ldbv2681TpMovi02");
    }

    @Override
    public int getLdbv2681TpMovi03() {
        throw new FieldNotMappedException("ldbv2681TpMovi03");
    }

    @Override
    public void setLdbv2681TpMovi03(int ldbv2681TpMovi03) {
        throw new FieldNotMappedException("ldbv2681TpMovi03");
    }

    @Override
    public int getLdbv2681TpMovi04() {
        throw new FieldNotMappedException("ldbv2681TpMovi04");
    }

    @Override
    public void setLdbv2681TpMovi04(int ldbv2681TpMovi04) {
        throw new FieldNotMappedException("ldbv2681TpMovi04");
    }

    @Override
    public int getLdbv2681TpMovi05() {
        throw new FieldNotMappedException("ldbv2681TpMovi05");
    }

    @Override
    public void setLdbv2681TpMovi05(int ldbv2681TpMovi05) {
        throw new FieldNotMappedException("ldbv2681TpMovi05");
    }

    @Override
    public int getLdbv2681TpMovi06() {
        throw new FieldNotMappedException("ldbv2681TpMovi06");
    }

    @Override
    public void setLdbv2681TpMovi06(int ldbv2681TpMovi06) {
        throw new FieldNotMappedException("ldbv2681TpMovi06");
    }

    @Override
    public String getLdbv2681TpOgg() {
        throw new FieldNotMappedException("ldbv2681TpOgg");
    }

    @Override
    public void setLdbv2681TpOgg(String ldbv2681TpOgg) {
        throw new FieldNotMappedException("ldbv2681TpOgg");
    }

    @Override
    public int getLdbv5061IdPoli() {
        throw new FieldNotMappedException("ldbv5061IdPoli");
    }

    @Override
    public void setLdbv5061IdPoli(int ldbv5061IdPoli) {
        throw new FieldNotMappedException("ldbv5061IdPoli");
    }

    @Override
    public int getLdbv5061TpMovi01() {
        throw new FieldNotMappedException("ldbv5061TpMovi01");
    }

    @Override
    public void setLdbv5061TpMovi01(int ldbv5061TpMovi01) {
        throw new FieldNotMappedException("ldbv5061TpMovi01");
    }

    @Override
    public int getLdbv5061TpMovi02() {
        throw new FieldNotMappedException("ldbv5061TpMovi02");
    }

    @Override
    public void setLdbv5061TpMovi02(int ldbv5061TpMovi02) {
        throw new FieldNotMappedException("ldbv5061TpMovi02");
    }

    @Override
    public int getLdbv5061TpMovi03() {
        throw new FieldNotMappedException("ldbv5061TpMovi03");
    }

    @Override
    public void setLdbv5061TpMovi03(int ldbv5061TpMovi03) {
        throw new FieldNotMappedException("ldbv5061TpMovi03");
    }

    @Override
    public int getLdbv5061TpMovi04() {
        throw new FieldNotMappedException("ldbv5061TpMovi04");
    }

    @Override
    public void setLdbv5061TpMovi04(int ldbv5061TpMovi04) {
        throw new FieldNotMappedException("ldbv5061TpMovi04");
    }

    @Override
    public int getLdbv5061TpMovi05() {
        throw new FieldNotMappedException("ldbv5061TpMovi05");
    }

    @Override
    public void setLdbv5061TpMovi05(int ldbv5061TpMovi05) {
        throw new FieldNotMappedException("ldbv5061TpMovi05");
    }

    @Override
    public int getLdbv5061TpMovi06() {
        throw new FieldNotMappedException("ldbv5061TpMovi06");
    }

    @Override
    public void setLdbv5061TpMovi06(int ldbv5061TpMovi06) {
        throw new FieldNotMappedException("ldbv5061TpMovi06");
    }

    @Override
    public int getLdbv5061TpMovi07() {
        throw new FieldNotMappedException("ldbv5061TpMovi07");
    }

    @Override
    public void setLdbv5061TpMovi07(int ldbv5061TpMovi07) {
        throw new FieldNotMappedException("ldbv5061TpMovi07");
    }

    @Override
    public int getLdbv5061TpMovi08() {
        throw new FieldNotMappedException("ldbv5061TpMovi08");
    }

    @Override
    public void setLdbv5061TpMovi08(int ldbv5061TpMovi08) {
        throw new FieldNotMappedException("ldbv5061TpMovi08");
    }

    @Override
    public int getLdbv5061TpMovi09() {
        throw new FieldNotMappedException("ldbv5061TpMovi09");
    }

    @Override
    public void setLdbv5061TpMovi09(int ldbv5061TpMovi09) {
        throw new FieldNotMappedException("ldbv5061TpMovi09");
    }

    @Override
    public int getLdbv5061TpMovi10() {
        throw new FieldNotMappedException("ldbv5061TpMovi10");
    }

    @Override
    public void setLdbv5061TpMovi10(int ldbv5061TpMovi10) {
        throw new FieldNotMappedException("ldbv5061TpMovi10");
    }

    @Override
    public int getLdbv7851IdOgg() {
        throw new FieldNotMappedException("ldbv7851IdOgg");
    }

    @Override
    public void setLdbv7851IdOgg(int ldbv7851IdOgg) {
        throw new FieldNotMappedException("ldbv7851IdOgg");
    }

    @Override
    public int getLdbv7851TpMovi01() {
        throw new FieldNotMappedException("ldbv7851TpMovi01");
    }

    @Override
    public void setLdbv7851TpMovi01(int ldbv7851TpMovi01) {
        throw new FieldNotMappedException("ldbv7851TpMovi01");
    }

    @Override
    public int getLdbv7851TpMovi02() {
        throw new FieldNotMappedException("ldbv7851TpMovi02");
    }

    @Override
    public void setLdbv7851TpMovi02(int ldbv7851TpMovi02) {
        throw new FieldNotMappedException("ldbv7851TpMovi02");
    }

    @Override
    public int getLdbv7851TpMovi03() {
        throw new FieldNotMappedException("ldbv7851TpMovi03");
    }

    @Override
    public void setLdbv7851TpMovi03(int ldbv7851TpMovi03) {
        throw new FieldNotMappedException("ldbv7851TpMovi03");
    }

    @Override
    public String getLdbv7851TpOgg() {
        throw new FieldNotMappedException("ldbv7851TpOgg");
    }

    @Override
    public void setLdbv7851TpOgg(String ldbv7851TpOgg) {
        throw new FieldNotMappedException("ldbv7851TpOgg");
    }

    @Override
    public int getLdbvd961TpMovi01() {
        throw new FieldNotMappedException("ldbvd961TpMovi01");
    }

    @Override
    public void setLdbvd961TpMovi01(int ldbvd961TpMovi01) {
        throw new FieldNotMappedException("ldbvd961TpMovi01");
    }

    @Override
    public int getLdbvd961TpMovi02() {
        throw new FieldNotMappedException("ldbvd961TpMovi02");
    }

    @Override
    public void setLdbvd961TpMovi02(int ldbvd961TpMovi02) {
        throw new FieldNotMappedException("ldbvd961TpMovi02");
    }

    @Override
    public int getLdbvd961TpMovi03() {
        throw new FieldNotMappedException("ldbvd961TpMovi03");
    }

    @Override
    public void setLdbvd961TpMovi03(int ldbvd961TpMovi03) {
        throw new FieldNotMappedException("ldbvd961TpMovi03");
    }

    @Override
    public int getLdbvd961TpMovi04() {
        throw new FieldNotMappedException("ldbvd961TpMovi04");
    }

    @Override
    public void setLdbvd961TpMovi04(int ldbvd961TpMovi04) {
        throw new FieldNotMappedException("ldbvd961TpMovi04");
    }

    @Override
    public int getLdbvd961TpMovi05() {
        throw new FieldNotMappedException("ldbvd961TpMovi05");
    }

    @Override
    public void setLdbvd961TpMovi05(int ldbvd961TpMovi05) {
        throw new FieldNotMappedException("ldbvd961TpMovi05");
    }

    @Override
    public int getLdbvd961TpMovi06() {
        throw new FieldNotMappedException("ldbvd961TpMovi06");
    }

    @Override
    public void setLdbvd961TpMovi06(int ldbvd961TpMovi06) {
        throw new FieldNotMappedException("ldbvd961TpMovi06");
    }

    @Override
    public int getLdbvd961TpMovi07() {
        throw new FieldNotMappedException("ldbvd961TpMovi07");
    }

    @Override
    public void setLdbvd961TpMovi07(int ldbvd961TpMovi07) {
        throw new FieldNotMappedException("ldbvd961TpMovi07");
    }

    @Override
    public int getLdbvd961TpMovi08() {
        throw new FieldNotMappedException("ldbvd961TpMovi08");
    }

    @Override
    public void setLdbvd961TpMovi08(int ldbvd961TpMovi08) {
        throw new FieldNotMappedException("ldbvd961TpMovi08");
    }

    @Override
    public int getLdbvd961TpMovi09() {
        throw new FieldNotMappedException("ldbvd961TpMovi09");
    }

    @Override
    public void setLdbvd961TpMovi09(int ldbvd961TpMovi09) {
        throw new FieldNotMappedException("ldbvd961TpMovi09");
    }

    @Override
    public int getLdbvd961TpMovi10() {
        throw new FieldNotMappedException("ldbvd961TpMovi10");
    }

    @Override
    public void setLdbvd961TpMovi10(int ldbvd961TpMovi10) {
        throw new FieldNotMappedException("ldbvd961TpMovi10");
    }

    @Override
    public int getLdbvf971TpMovi1() {
        throw new FieldNotMappedException("ldbvf971TpMovi1");
    }

    @Override
    public void setLdbvf971TpMovi1(int ldbvf971TpMovi1) {
        throw new FieldNotMappedException("ldbvf971TpMovi1");
    }

    @Override
    public int getLdbvf971TpMovi2() {
        throw new FieldNotMappedException("ldbvf971TpMovi2");
    }

    @Override
    public void setLdbvf971TpMovi2(int ldbvf971TpMovi2) {
        throw new FieldNotMappedException("ldbvf971TpMovi2");
    }

    @Override
    public int getLdbvf971TpMovi3() {
        throw new FieldNotMappedException("ldbvf971TpMovi3");
    }

    @Override
    public void setLdbvf971TpMovi3(int ldbvf971TpMovi3) {
        throw new FieldNotMappedException("ldbvf971TpMovi3");
    }

    @Override
    public int getLdbvg351IdOgg() {
        throw new FieldNotMappedException("ldbvg351IdOgg");
    }

    @Override
    public void setLdbvg351IdOgg(int ldbvg351IdOgg) {
        throw new FieldNotMappedException("ldbvg351IdOgg");
    }

    @Override
    public int getLdbvg351TpMovi() {
        throw new FieldNotMappedException("ldbvg351TpMovi");
    }

    @Override
    public void setLdbvg351TpMovi(int ldbvg351TpMovi) {
        throw new FieldNotMappedException("ldbvg351TpMovi");
    }

    @Override
    public String getLdbvg351TpOgg() {
        throw new FieldNotMappedException("ldbvg351TpOgg");
    }

    @Override
    public void setLdbvg351TpOgg(String ldbvg351TpOgg) {
        throw new FieldNotMappedException("ldbvg351TpOgg");
    }

    @Override
    public int getLdbvh601IdAdes() {
        throw new FieldNotMappedException("ldbvh601IdAdes");
    }

    @Override
    public void setLdbvh601IdAdes(int ldbvh601IdAdes) {
        throw new FieldNotMappedException("ldbvh601IdAdes");
    }

    @Override
    public int getLdbvh601TpMovi10() {
        throw new FieldNotMappedException("ldbvh601TpMovi10");
    }

    @Override
    public void setLdbvh601TpMovi10(int ldbvh601TpMovi10) {
        throw new FieldNotMappedException("ldbvh601TpMovi10");
    }

    @Override
    public int getLdbvh601TpMovi11() {
        throw new FieldNotMappedException("ldbvh601TpMovi11");
    }

    @Override
    public void setLdbvh601TpMovi11(int ldbvh601TpMovi11) {
        throw new FieldNotMappedException("ldbvh601TpMovi11");
    }

    @Override
    public int getLdbvh601TpMovi12() {
        throw new FieldNotMappedException("ldbvh601TpMovi12");
    }

    @Override
    public void setLdbvh601TpMovi12(int ldbvh601TpMovi12) {
        throw new FieldNotMappedException("ldbvh601TpMovi12");
    }

    @Override
    public int getLdbvh601TpMovi13() {
        throw new FieldNotMappedException("ldbvh601TpMovi13");
    }

    @Override
    public void setLdbvh601TpMovi13(int ldbvh601TpMovi13) {
        throw new FieldNotMappedException("ldbvh601TpMovi13");
    }

    @Override
    public int getLdbvh601TpMovi14() {
        throw new FieldNotMappedException("ldbvh601TpMovi14");
    }

    @Override
    public void setLdbvh601TpMovi14(int ldbvh601TpMovi14) {
        throw new FieldNotMappedException("ldbvh601TpMovi14");
    }

    @Override
    public int getLdbvh601TpMovi15() {
        throw new FieldNotMappedException("ldbvh601TpMovi15");
    }

    @Override
    public void setLdbvh601TpMovi15(int ldbvh601TpMovi15) {
        throw new FieldNotMappedException("ldbvh601TpMovi15");
    }

    @Override
    public int getLdbvh601TpMovi1() {
        throw new FieldNotMappedException("ldbvh601TpMovi1");
    }

    @Override
    public void setLdbvh601TpMovi1(int ldbvh601TpMovi1) {
        throw new FieldNotMappedException("ldbvh601TpMovi1");
    }

    @Override
    public int getLdbvh601TpMovi2() {
        throw new FieldNotMappedException("ldbvh601TpMovi2");
    }

    @Override
    public void setLdbvh601TpMovi2(int ldbvh601TpMovi2) {
        throw new FieldNotMappedException("ldbvh601TpMovi2");
    }

    @Override
    public int getLdbvh601TpMovi3() {
        throw new FieldNotMappedException("ldbvh601TpMovi3");
    }

    @Override
    public void setLdbvh601TpMovi3(int ldbvh601TpMovi3) {
        throw new FieldNotMappedException("ldbvh601TpMovi3");
    }

    @Override
    public int getLdbvh601TpMovi4() {
        throw new FieldNotMappedException("ldbvh601TpMovi4");
    }

    @Override
    public void setLdbvh601TpMovi4(int ldbvh601TpMovi4) {
        throw new FieldNotMappedException("ldbvh601TpMovi4");
    }

    @Override
    public int getLdbvh601TpMovi5() {
        throw new FieldNotMappedException("ldbvh601TpMovi5");
    }

    @Override
    public void setLdbvh601TpMovi5(int ldbvh601TpMovi5) {
        throw new FieldNotMappedException("ldbvh601TpMovi5");
    }

    @Override
    public int getLdbvh601TpMovi6() {
        throw new FieldNotMappedException("ldbvh601TpMovi6");
    }

    @Override
    public void setLdbvh601TpMovi6(int ldbvh601TpMovi6) {
        throw new FieldNotMappedException("ldbvh601TpMovi6");
    }

    @Override
    public int getLdbvh601TpMovi7() {
        throw new FieldNotMappedException("ldbvh601TpMovi7");
    }

    @Override
    public void setLdbvh601TpMovi7(int ldbvh601TpMovi7) {
        throw new FieldNotMappedException("ldbvh601TpMovi7");
    }

    @Override
    public int getLdbvh601TpMovi8() {
        throw new FieldNotMappedException("ldbvh601TpMovi8");
    }

    @Override
    public void setLdbvh601TpMovi8(int ldbvh601TpMovi8) {
        throw new FieldNotMappedException("ldbvh601TpMovi8");
    }

    @Override
    public int getLdbvh601TpMovi9() {
        throw new FieldNotMappedException("ldbvh601TpMovi9");
    }

    @Override
    public void setLdbvh601TpMovi9(int ldbvh601TpMovi9) {
        throw new FieldNotMappedException("ldbvh601TpMovi9");
    }

    @Override
    public short getMmDiff() {
        return paramMovi.getPmoMmDiff().getPmoMmDiff();
    }

    @Override
    public void setMmDiff(short mmDiff) {
        this.paramMovi.getPmoMmDiff().setPmoMmDiff(mmDiff);
    }

    @Override
    public Short getMmDiffObj() {
        if (ws.getIndParamMovi().getMmDiff() >= 0) {
            return ((Short)getMmDiff());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMmDiffObj(Short mmDiffObj) {
        if (mmDiffObj != null) {
            setMmDiff(((short)mmDiffObj));
            ws.getIndParamMovi().setMmDiff(((short)0));
        }
        else {
            ws.getIndParamMovi().setMmDiff(((short)-1));
        }
    }

    @Override
    public int getNumRatPagPre() {
        return paramMovi.getPmoNumRatPagPre().getPmoNumRatPagPre();
    }

    @Override
    public void setNumRatPagPre(int numRatPagPre) {
        this.paramMovi.getPmoNumRatPagPre().setPmoNumRatPagPre(numRatPagPre);
    }

    @Override
    public Integer getNumRatPagPreObj() {
        if (ws.getIndParamMovi().getNumRatPagPre() >= 0) {
            return ((Integer)getNumRatPagPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumRatPagPreObj(Integer numRatPagPreObj) {
        if (numRatPagPreObj != null) {
            setNumRatPagPre(((int)numRatPagPreObj));
            ws.getIndParamMovi().setNumRatPagPre(((short)0));
        }
        else {
            ws.getIndParamMovi().setNumRatPagPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcAnticBns() {
        return paramMovi.getPmoPcAnticBns().getPmoPcAnticBns();
    }

    @Override
    public void setPcAnticBns(AfDecimal pcAnticBns) {
        this.paramMovi.getPmoPcAnticBns().setPmoPcAnticBns(pcAnticBns.copy());
    }

    @Override
    public AfDecimal getPcAnticBnsObj() {
        if (ws.getIndParamMovi().getPcAnticBns() >= 0) {
            return getPcAnticBns();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcAnticBnsObj(AfDecimal pcAnticBnsObj) {
        if (pcAnticBnsObj != null) {
            setPcAnticBns(new AfDecimal(pcAnticBnsObj, 6, 3));
            ws.getIndParamMovi().setPcAnticBns(((short)0));
        }
        else {
            ws.getIndParamMovi().setPcAnticBns(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcApplzOpz() {
        return paramMovi.getPmoPcApplzOpz().getPmoPcApplzOpz();
    }

    @Override
    public void setPcApplzOpz(AfDecimal pcApplzOpz) {
        this.paramMovi.getPmoPcApplzOpz().setPmoPcApplzOpz(pcApplzOpz.copy());
    }

    @Override
    public AfDecimal getPcApplzOpzObj() {
        if (ws.getIndParamMovi().getPcApplzOpz() >= 0) {
            return getPcApplzOpz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcApplzOpzObj(AfDecimal pcApplzOpzObj) {
        if (pcApplzOpzObj != null) {
            setPcApplzOpz(new AfDecimal(pcApplzOpzObj, 6, 3));
            ws.getIndParamMovi().setPcApplzOpz(((short)0));
        }
        else {
            ws.getIndParamMovi().setPcApplzOpz(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcIntrFraz() {
        return paramMovi.getPmoPcIntrFraz().getPmoPcIntrFraz();
    }

    @Override
    public void setPcIntrFraz(AfDecimal pcIntrFraz) {
        this.paramMovi.getPmoPcIntrFraz().setPmoPcIntrFraz(pcIntrFraz.copy());
    }

    @Override
    public AfDecimal getPcIntrFrazObj() {
        if (ws.getIndParamMovi().getPcIntrFraz() >= 0) {
            return getPcIntrFraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcIntrFrazObj(AfDecimal pcIntrFrazObj) {
        if (pcIntrFrazObj != null) {
            setPcIntrFraz(new AfDecimal(pcIntrFrazObj, 6, 3));
            ws.getIndParamMovi().setPcIntrFraz(((short)0));
        }
        else {
            ws.getIndParamMovi().setPcIntrFraz(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRevrsb() {
        return paramMovi.getPmoPcRevrsb().getPmoPcRevrsb();
    }

    @Override
    public void setPcRevrsb(AfDecimal pcRevrsb) {
        this.paramMovi.getPmoPcRevrsb().setPmoPcRevrsb(pcRevrsb.copy());
    }

    @Override
    public AfDecimal getPcRevrsbObj() {
        if (ws.getIndParamMovi().getPcRevrsb() >= 0) {
            return getPcRevrsb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRevrsbObj(AfDecimal pcRevrsbObj) {
        if (pcRevrsbObj != null) {
            setPcRevrsb(new AfDecimal(pcRevrsbObj, 6, 3));
            ws.getIndParamMovi().setPcRevrsb(((short)0));
        }
        else {
            ws.getIndParamMovi().setPcRevrsb(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcServVal() {
        return paramMovi.getPmoPcServVal().getPmoPcServVal();
    }

    @Override
    public void setPcServVal(AfDecimal pcServVal) {
        this.paramMovi.getPmoPcServVal().setPmoPcServVal(pcServVal.copy());
    }

    @Override
    public AfDecimal getPcServValObj() {
        if (ws.getIndParamMovi().getPcServVal() >= 0) {
            return getPcServVal();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcServValObj(AfDecimal pcServValObj) {
        if (pcServValObj != null) {
            setPcServVal(new AfDecimal(pcServValObj, 6, 3));
            ws.getIndParamMovi().setPcServVal(((short)0));
        }
        else {
            ws.getIndParamMovi().setPcServVal(((short)-1));
        }
    }

    @Override
    public int getPmoCodCompAnia() {
        return paramMovi.getPmoCodCompAnia();
    }

    @Override
    public void setPmoCodCompAnia(int pmoCodCompAnia) {
        this.paramMovi.setPmoCodCompAnia(pmoCodCompAnia);
    }

    @Override
    public long getPmoDsRiga() {
        return paramMovi.getPmoDsRiga();
    }

    @Override
    public void setPmoDsRiga(long pmoDsRiga) {
        this.paramMovi.setPmoDsRiga(pmoDsRiga);
    }

    @Override
    public long getPmoDsTsEndCptz() {
        return paramMovi.getPmoDsTsEndCptz();
    }

    @Override
    public void setPmoDsTsEndCptz(long pmoDsTsEndCptz) {
        this.paramMovi.setPmoDsTsEndCptz(pmoDsTsEndCptz);
    }

    @Override
    public long getPmoDsTsIniCptz() {
        return paramMovi.getPmoDsTsIniCptz();
    }

    @Override
    public void setPmoDsTsIniCptz(long pmoDsTsIniCptz) {
        this.paramMovi.setPmoDsTsIniCptz(pmoDsTsIniCptz);
    }

    @Override
    public String getPmoDtEndEffDb() {
        return ws.getParamMoviDb().getEndEffDb();
    }

    @Override
    public void setPmoDtEndEffDb(String pmoDtEndEffDb) {
        this.ws.getParamMoviDb().setEndEffDb(pmoDtEndEffDb);
    }

    @Override
    public String getPmoDtIniEffDb() {
        return ws.getParamMoviDb().getIniEffDb();
    }

    @Override
    public void setPmoDtIniEffDb(String pmoDtIniEffDb) {
        this.ws.getParamMoviDb().setIniEffDb(pmoDtIniEffDb);
    }

    @Override
    public String getPmoDtRicorSuccDb() {
        return ws.getParamMoviDb().getEndCopDb();
    }

    @Override
    public void setPmoDtRicorSuccDb(String pmoDtRicorSuccDb) {
        this.ws.getParamMoviDb().setEndCopDb(pmoDtRicorSuccDb);
    }

    @Override
    public String getPmoDtRicorSuccDbObj() {
        if (ws.getIndParamMovi().getDtRicorSucc() >= 0) {
            return getPmoDtRicorSuccDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPmoDtRicorSuccDbObj(String pmoDtRicorSuccDbObj) {
        if (pmoDtRicorSuccDbObj != null) {
            setPmoDtRicorSuccDb(pmoDtRicorSuccDbObj);
            ws.getIndParamMovi().setDtRicorSucc(((short)0));
        }
        else {
            ws.getIndParamMovi().setDtRicorSucc(((short)-1));
        }
    }

    @Override
    public int getPmoIdAdes() {
        return paramMovi.getPmoIdAdes().getPmoIdAdes();
    }

    @Override
    public void setPmoIdAdes(int pmoIdAdes) {
        this.paramMovi.getPmoIdAdes().setPmoIdAdes(pmoIdAdes);
    }

    @Override
    public Integer getPmoIdAdesObj() {
        if (ws.getIndParamMovi().getIdAdes() >= 0) {
            return ((Integer)getPmoIdAdes());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPmoIdAdesObj(Integer pmoIdAdesObj) {
        if (pmoIdAdesObj != null) {
            setPmoIdAdes(((int)pmoIdAdesObj));
            ws.getIndParamMovi().setIdAdes(((short)0));
        }
        else {
            ws.getIndParamMovi().setIdAdes(((short)-1));
        }
    }

    @Override
    public int getPmoIdOgg() {
        return paramMovi.getPmoIdOgg();
    }

    @Override
    public void setPmoIdOgg(int pmoIdOgg) {
        this.paramMovi.setPmoIdOgg(pmoIdOgg);
    }

    @Override
    public int getPmoIdPoli() {
        return paramMovi.getPmoIdPoli();
    }

    @Override
    public void setPmoIdPoli(int pmoIdPoli) {
        this.paramMovi.setPmoIdPoli(pmoIdPoli);
    }

    @Override
    public String getPmoTpFrmAssva() {
        return paramMovi.getPmoTpFrmAssva();
    }

    @Override
    public void setPmoTpFrmAssva(String pmoTpFrmAssva) {
        this.paramMovi.setPmoTpFrmAssva(pmoTpFrmAssva);
    }

    @Override
    public int getPmoTpMovi() {
        return paramMovi.getPmoTpMovi().getPmoTpMovi();
    }

    @Override
    public void setPmoTpMovi(int pmoTpMovi) {
        this.paramMovi.getPmoTpMovi().setPmoTpMovi(pmoTpMovi);
    }

    @Override
    public Integer getPmoTpMoviObj() {
        if (ws.getIndParamMovi().getTpMovi() >= 0) {
            return ((Integer)getPmoTpMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPmoTpMoviObj(Integer pmoTpMoviObj) {
        if (pmoTpMoviObj != null) {
            setPmoTpMovi(((int)pmoTpMoviObj));
            ws.getIndParamMovi().setTpMovi(((short)0));
        }
        else {
            ws.getIndParamMovi().setTpMovi(((short)-1));
        }
    }

    @Override
    public String getPmoTpOgg() {
        return paramMovi.getPmoTpOgg();
    }

    @Override
    public void setPmoTpOgg(String pmoTpOgg) {
        this.paramMovi.setPmoTpOgg(pmoTpOgg);
    }

    @Override
    public AfDecimal getSomAsstaGarac() {
        return paramMovi.getPmoSomAsstaGarac().getPmoSomAsstaGarac();
    }

    @Override
    public void setSomAsstaGarac(AfDecimal somAsstaGarac) {
        this.paramMovi.getPmoSomAsstaGarac().setPmoSomAsstaGarac(somAsstaGarac.copy());
    }

    @Override
    public AfDecimal getSomAsstaGaracObj() {
        if (ws.getIndParamMovi().getSomAsstaGarac() >= 0) {
            return getSomAsstaGarac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSomAsstaGaracObj(AfDecimal somAsstaGaracObj) {
        if (somAsstaGaracObj != null) {
            setSomAsstaGarac(new AfDecimal(somAsstaGaracObj, 15, 3));
            ws.getIndParamMovi().setSomAsstaGarac(((short)0));
        }
        else {
            ws.getIndParamMovi().setSomAsstaGarac(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpePc() {
        return paramMovi.getPmoSpePc().getPmoSpePc();
    }

    @Override
    public void setSpePc(AfDecimal spePc) {
        this.paramMovi.getPmoSpePc().setPmoSpePc(spePc.copy());
    }

    @Override
    public AfDecimal getSpePcObj() {
        if (ws.getIndParamMovi().getSpePc() >= 0) {
            return getSpePc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpePcObj(AfDecimal spePcObj) {
        if (spePcObj != null) {
            setSpePc(new AfDecimal(spePcObj, 6, 3));
            ws.getIndParamMovi().setSpePc(((short)0));
        }
        else {
            ws.getIndParamMovi().setSpePc(((short)-1));
        }
    }

    @Override
    public int getTotAaGiaPror() {
        return paramMovi.getPmoTotAaGiaPror().getPmoTotAaGiaPror();
    }

    @Override
    public void setTotAaGiaPror(int totAaGiaPror) {
        this.paramMovi.getPmoTotAaGiaPror().setPmoTotAaGiaPror(totAaGiaPror);
    }

    @Override
    public Integer getTotAaGiaProrObj() {
        if (ws.getIndParamMovi().getTotAaGiaPror() >= 0) {
            return ((Integer)getTotAaGiaPror());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotAaGiaProrObj(Integer totAaGiaProrObj) {
        if (totAaGiaProrObj != null) {
            setTotAaGiaPror(((int)totAaGiaProrObj));
            ws.getIndParamMovi().setTotAaGiaPror(((short)0));
        }
        else {
            ws.getIndParamMovi().setTotAaGiaPror(((short)-1));
        }
    }

    @Override
    public String getTpEstrCnt() {
        return paramMovi.getPmoTpEstrCnt();
    }

    @Override
    public void setTpEstrCnt(String tpEstrCnt) {
        this.paramMovi.setPmoTpEstrCnt(tpEstrCnt);
    }

    @Override
    public String getTpEstrCntObj() {
        if (ws.getIndParamMovi().getTpEstrCnt() >= 0) {
            return getTpEstrCnt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpEstrCntObj(String tpEstrCntObj) {
        if (tpEstrCntObj != null) {
            setTpEstrCnt(tpEstrCntObj);
            ws.getIndParamMovi().setTpEstrCnt(((short)0));
        }
        else {
            ws.getIndParamMovi().setTpEstrCnt(((short)-1));
        }
    }

    @Override
    public String getTpOggRival() {
        return paramMovi.getPmoTpOggRival();
    }

    @Override
    public void setTpOggRival(String tpOggRival) {
        this.paramMovi.setPmoTpOggRival(tpOggRival);
    }

    @Override
    public String getTpOggRivalObj() {
        if (ws.getIndParamMovi().getTpOggRival() >= 0) {
            return getTpOggRival();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpOggRivalObj(String tpOggRivalObj) {
        if (tpOggRivalObj != null) {
            setTpOggRival(tpOggRivalObj);
            ws.getIndParamMovi().setTpOggRival(((short)0));
        }
        else {
            ws.getIndParamMovi().setTpOggRival(((short)-1));
        }
    }

    @Override
    public String getTpOpz() {
        return paramMovi.getPmoTpOpz();
    }

    @Override
    public void setTpOpz(String tpOpz) {
        this.paramMovi.setPmoTpOpz(tpOpz);
    }

    @Override
    public String getTpOpzObj() {
        if (ws.getIndParamMovi().getTpOpz() >= 0) {
            return getTpOpz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpOpzObj(String tpOpzObj) {
        if (tpOpzObj != null) {
            setTpOpz(tpOpzObj);
            ws.getIndParamMovi().setTpOpz(((short)0));
        }
        else {
            ws.getIndParamMovi().setTpOpz(((short)-1));
        }
    }

    @Override
    public String getTpRinnColl() {
        return paramMovi.getPmoTpRinnColl();
    }

    @Override
    public void setTpRinnColl(String tpRinnColl) {
        this.paramMovi.setPmoTpRinnColl(tpRinnColl);
    }

    @Override
    public String getTpRinnCollObj() {
        if (ws.getIndParamMovi().getTpRinnColl() >= 0) {
            return getTpRinnColl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRinnCollObj(String tpRinnCollObj) {
        if (tpRinnCollObj != null) {
            setTpRinnColl(tpRinnCollObj);
            ws.getIndParamMovi().setTpRinnColl(((short)0));
        }
        else {
            ws.getIndParamMovi().setTpRinnColl(((short)-1));
        }
    }

    @Override
    public String getTpRivalPre() {
        return paramMovi.getPmoTpRivalPre();
    }

    @Override
    public void setTpRivalPre(String tpRivalPre) {
        this.paramMovi.setPmoTpRivalPre(tpRivalPre);
    }

    @Override
    public String getTpRivalPreObj() {
        if (ws.getIndParamMovi().getTpRivalPre() >= 0) {
            return getTpRivalPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRivalPreObj(String tpRivalPreObj) {
        if (tpRivalPreObj != null) {
            setTpRivalPre(tpRivalPreObj);
            ws.getIndParamMovi().setTpRivalPre(((short)0));
        }
        else {
            ws.getIndParamMovi().setTpRivalPre(((short)-1));
        }
    }

    @Override
    public String getTpRivalPrstz() {
        return paramMovi.getPmoTpRivalPrstz();
    }

    @Override
    public void setTpRivalPrstz(String tpRivalPrstz) {
        this.paramMovi.setPmoTpRivalPrstz(tpRivalPrstz);
    }

    @Override
    public String getTpRivalPrstzObj() {
        if (ws.getIndParamMovi().getTpRivalPrstz() >= 0) {
            return getTpRivalPrstz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRivalPrstzObj(String tpRivalPrstzObj) {
        if (tpRivalPrstzObj != null) {
            setTpRivalPrstz(tpRivalPrstzObj);
            ws.getIndParamMovi().setTpRivalPrstz(((short)0));
        }
        else {
            ws.getIndParamMovi().setTpRivalPrstz(((short)-1));
        }
    }

    @Override
    public AfDecimal getUltPcPerd() {
        return paramMovi.getPmoUltPcPerd().getPmoUltPcPerd();
    }

    @Override
    public void setUltPcPerd(AfDecimal ultPcPerd) {
        this.paramMovi.getPmoUltPcPerd().setPmoUltPcPerd(ultPcPerd.copy());
    }

    @Override
    public AfDecimal getUltPcPerdObj() {
        if (ws.getIndParamMovi().getUltPcPerd() >= 0) {
            return getUltPcPerd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setUltPcPerdObj(AfDecimal ultPcPerdObj) {
        if (ultPcPerdObj != null) {
            setUltPcPerd(new AfDecimal(ultPcPerdObj, 6, 3));
            ws.getIndParamMovi().setUltPcPerd(((short)0));
        }
        else {
            ws.getIndParamMovi().setUltPcPerd(((short)-1));
        }
    }

    @Override
    public String getWsCodRamo() {
        throw new FieldNotMappedException("wsCodRamo");
    }

    @Override
    public void setWsCodRamo(String wsCodRamo) {
        throw new FieldNotMappedException("wsCodRamo");
    }

    @Override
    public String getWsDataEff() {
        throw new FieldNotMappedException("wsDataEff");
    }

    @Override
    public void setWsDataEff(String wsDataEff) {
        throw new FieldNotMappedException("wsDataEff");
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public String getWsDtElabADb() {
        throw new FieldNotMappedException("wsDtElabADb");
    }

    @Override
    public void setWsDtElabADb(String wsDtElabADb) {
        throw new FieldNotMappedException("wsDtElabADb");
    }

    @Override
    public String getWsDtElabDaDb() {
        throw new FieldNotMappedException("wsDtElabDaDb");
    }

    @Override
    public void setWsDtElabDaDb(String wsDtElabDaDb) {
        throw new FieldNotMappedException("wsDtElabDaDb");
    }

    @Override
    public String getWsForma1() {
        throw new FieldNotMappedException("wsForma1");
    }

    @Override
    public void setWsForma1(String wsForma1) {
        throw new FieldNotMappedException("wsForma1");
    }

    @Override
    public String getWsForma2() {
        throw new FieldNotMappedException("wsForma2");
    }

    @Override
    public void setWsForma2(String wsForma2) {
        throw new FieldNotMappedException("wsForma2");
    }

    @Override
    public String getWsGaranzia() {
        throw new FieldNotMappedException("wsGaranzia");
    }

    @Override
    public void setWsGaranzia(String wsGaranzia) {
        throw new FieldNotMappedException("wsGaranzia");
    }

    @Override
    public int getWsIdAdes() {
        throw new FieldNotMappedException("wsIdAdes");
    }

    @Override
    public void setWsIdAdes(int wsIdAdes) {
        throw new FieldNotMappedException("wsIdAdes");
    }

    @Override
    public int getWsIdPoli() {
        throw new FieldNotMappedException("wsIdPoli");
    }

    @Override
    public void setWsIdPoli(int wsIdPoli) {
        throw new FieldNotMappedException("wsIdPoli");
    }

    @Override
    public int getWsTpMovi() {
        throw new FieldNotMappedException("wsTpMovi");
    }

    @Override
    public void setWsTpMovi(int wsTpMovi) {
        throw new FieldNotMappedException("wsTpMovi");
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }

    @Override
    public long getWsTsInfinito1() {
        throw new FieldNotMappedException("wsTsInfinito1");
    }

    @Override
    public void setWsTsInfinito1(long wsTsInfinito1) {
        throw new FieldNotMappedException("wsTsInfinito1");
    }
}
