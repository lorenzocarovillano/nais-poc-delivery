package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-MOVI-FINRIO<br>
 * Variable: IND-MOVI-FINRIO from copybook IDBVMFZ2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndMoviFinrio {

    //==== PROPERTIES ====
    //Original name: IND-MFZ-ID-ADES
    private short idAdes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MFZ-ID-LIQ
    private short idLiq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MFZ-ID-TIT-CONT
    private short idTitCont = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MFZ-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MFZ-DT-EFF-MOVI-FINRIO
    private short dtEffMoviFinrio = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MFZ-DT-ELAB
    private short dtElab = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MFZ-DT-RICH-MOVI
    private short dtRichMovi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MFZ-COS-OPRZ
    private short cosOprz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MFZ-TP-CAUS-RETTIFICA
    private short tpCausRettifica = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdAdes(short idAdes) {
        this.idAdes = idAdes;
    }

    public short getIdAdes() {
        return this.idAdes;
    }

    public void setIdLiq(short idLiq) {
        this.idLiq = idLiq;
    }

    public short getIdLiq() {
        return this.idLiq;
    }

    public void setIdTitCont(short idTitCont) {
        this.idTitCont = idTitCont;
    }

    public short getIdTitCont() {
        return this.idTitCont;
    }

    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtEffMoviFinrio(short dtEffMoviFinrio) {
        this.dtEffMoviFinrio = dtEffMoviFinrio;
    }

    public short getDtEffMoviFinrio() {
        return this.dtEffMoviFinrio;
    }

    public void setDtElab(short dtElab) {
        this.dtElab = dtElab;
    }

    public short getDtElab() {
        return this.dtElab;
    }

    public void setDtRichMovi(short dtRichMovi) {
        this.dtRichMovi = dtRichMovi;
    }

    public short getDtRichMovi() {
        return this.dtRichMovi;
    }

    public void setCosOprz(short cosOprz) {
        this.cosOprz = cosOprz;
    }

    public short getCosOprz() {
        return this.cosOprz;
    }

    public void setTpCausRettifica(short tpCausRettifica) {
        this.tpCausRettifica = tpCausRettifica;
    }

    public short getTpCausRettifica() {
        return this.tpCausRettifica;
    }
}
