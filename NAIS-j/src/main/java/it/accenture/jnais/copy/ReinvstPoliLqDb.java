package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: REINVST-POLI-LQ-DB<br>
 * Variable: REINVST-POLI-LQ-DB from copybook IDBVL303<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ReinvstPoliLqDb {

    //==== PROPERTIES ====
    //Original name: L30-DT-INI-EFF-DB
    private String iniEffDb = DefaultValues.stringVal(Len.INI_EFF_DB);
    //Original name: L30-DT-END-EFF-DB
    private String endEffDb = DefaultValues.stringVal(Len.END_EFF_DB);
    //Original name: L30-DT-DECOR-POLI-LQ-DB
    private String decorPoliLqDb = DefaultValues.stringVal(Len.DECOR_POLI_LQ_DB);

    //==== METHODS ====
    public void setIniEffDb(String iniEffDb) {
        this.iniEffDb = Functions.subString(iniEffDb, Len.INI_EFF_DB);
    }

    public String getIniEffDb() {
        return this.iniEffDb;
    }

    public void setEndEffDb(String endEffDb) {
        this.endEffDb = Functions.subString(endEffDb, Len.END_EFF_DB);
    }

    public String getEndEffDb() {
        return this.endEffDb;
    }

    public void setDecorPoliLqDb(String decorPoliLqDb) {
        this.decorPoliLqDb = Functions.subString(decorPoliLqDb, Len.DECOR_POLI_LQ_DB);
    }

    public String getDecorPoliLqDb() {
        return this.decorPoliLqDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INI_EFF_DB = 10;
        public static final int END_EFF_DB = 10;
        public static final int DECOR_POLI_LQ_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
