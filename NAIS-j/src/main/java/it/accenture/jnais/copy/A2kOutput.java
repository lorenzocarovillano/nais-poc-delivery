package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.A2kOuamgX;
import it.accenture.jnais.ws.A2kOugl07X;
import it.accenture.jnais.ws.A2kOugmaX;
import it.accenture.jnais.ws.A2kOujulX;

/**Original name: A2K-OUTPUT<br>
 * Variable: A2K-OUTPUT from copybook LCCC0003<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class A2kOutput {

    //==== PROPERTIES ====
    //Original name: A2K-OUGMA-X
    private A2kOugmaX a2kOugmaX = new A2kOugmaX();
    //Original name: A2K-OUGBMBA
    private A2kOugbmba a2kOugbmba = new A2kOugbmba();
    //Original name: A2K-OUAMG-X
    private A2kOuamgX a2kOuamgX = new A2kOuamgX();
    /**Original name: A2K-OUAMGP<br>
	 * <pre>                                            formato 0AAAAMMGGs
	 *                                                         packed</pre>*/
    private int a2kOuamgp = DefaultValues.INT_VAL;
    /**Original name: A2K-OUAMG0P<br>
	 * <pre>                                            formato AAAAMMGG0s
	 *                                                         packed</pre>*/
    private int a2kOuamg0p = DefaultValues.INT_VAL;
    /**Original name: A2K-OUPROG9<br>
	 * <pre>                                            formato GGGGGs packed
	 *                                              progressivo dal 1900</pre>*/
    private int a2kOuprog9 = DefaultValues.INT_VAL;
    /**Original name: A2K-OUPROGC<br>
	 * <pre>                                            formato GGGGGs packed
	 *                                             progr. comm. dal 1900</pre>*/
    private int a2kOuprogc = DefaultValues.INT_VAL;
    /**Original name: A2K-OUPROG<br>
	 * <pre>                                            formato GGGGGGGs pack
	 *                                                         progressi</pre>*/
    private int a2kOuprog = DefaultValues.INT_VAL;
    /**Original name: A2K-OUPROCO<br>
	 * <pre>                                            formato GGGGGGGs pack
	 *                                             progressivo commercia</pre>*/
    private int a2kOuproco = DefaultValues.INT_VAL;
    //Original name: A2K-OUJUL-X
    private A2kOujulX a2kOujulX = new A2kOujulX();
    //Original name: A2K-OUSTA
    private A2kOusta a2kOusta = new A2kOusta();
    //Original name: A2K-OURID
    private A2kOurid a2kOurid = new A2kOurid();
    /**Original name: A2K-OUGG06<br>
	 * <pre>                                            descrizione giorno</pre>*/
    private String a2kOugg06 = DefaultValues.stringVal(Len.A2K_OUGG06);
    /**Original name: A2K-OUNG06<br>
	 * <pre>                                            Nr. giorno settimana
	 *                                             0 = domenica
	 *                                             1 = lunedi
	 *                                             2 = martedi
	 *                                             3 = mercoledi
	 *                                             4 = giovedi
	 *                                             5 = venerdi
	 *                                             6 = sabato</pre>*/
    private String a2kOung06 = DefaultValues.stringVal(Len.A2K_OUNG06);
    /**Original name: A2K-OUTG06<br>
	 * <pre>                                            festa
	 *                                             F = festivo (anche sa
	 *                                             N = non festivo</pre>*/
    private char a2kOutg06 = DefaultValues.CHAR_VAL;
    /**Original name: A2K-OUGG07<br>
	 * <pre>                                            Nr. giorni del mese
	 *                                             giorni lav. del mese</pre>*/
    private String a2kOugg07 = DefaultValues.stringVal(Len.A2K_OUGG07);
    //Original name: A2K-OUGL07-X
    private A2kOugl07X a2kOugl07X = new A2kOugl07X();
    /**Original name: A2K-OUGG08<br>
	 * <pre>                                            Nr. trimestre</pre>*/
    private String a2kOugg08 = DefaultValues.stringVal(Len.A2K_OUGG08);
    /**Original name: A2K-OUGG09<br>
	 * <pre>                                            tipo giorno
	 *                                             spaces oppure
	 *                                             D = fine decade
	 *                                             Q = fine quindicina
	 *                                             M = fine mese</pre>*/
    private char a2kOugg09 = DefaultValues.CHAR_VAL;
    /**Original name: A2K-OUGG10<br>
	 * <pre>                                            spaces oppure
	 *                                             P = pasqua o pasquett
	 *                                             F = festivita' fissa</pre>*/
    private char a2kOugg10 = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        a2kOugmaX.setA2kOugmaXBytes(buffer, position);
        position += A2kOugmaX.Len.A2K_OUGMA_X;
        a2kOugbmba.setA2kOugbmbaBytes(buffer, position);
        position += A2kOugbmba.Len.A2K_OUGBMBA;
        a2kOuamgX.setA2kOuamgXBytes(buffer, position);
        position += A2kOuamgX.Len.A2K_OUAMG_X;
        a2kOuamgp = MarshalByte.readPackedAsInt(buffer, position, Len.Int.A2K_OUAMGP, 0, SignType.NO_SIGN);
        position += Len.A2K_OUAMGP;
        a2kOuamg0p = MarshalByte.readPackedAsInt(buffer, position, Len.Int.A2K_OUAMG0P, 0, SignType.NO_SIGN);
        position += Len.A2K_OUAMG0P;
        a2kOuprog9 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.A2K_OUPROG9, 0, SignType.NO_SIGN);
        position += Len.A2K_OUPROG9;
        a2kOuprogc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.A2K_OUPROGC, 0, SignType.NO_SIGN);
        position += Len.A2K_OUPROGC;
        a2kOuprog = MarshalByte.readPackedAsInt(buffer, position, Len.Int.A2K_OUPROG, 0, SignType.NO_SIGN);
        position += Len.A2K_OUPROG;
        a2kOuproco = MarshalByte.readPackedAsInt(buffer, position, Len.Int.A2K_OUPROCO, 0, SignType.NO_SIGN);
        position += Len.A2K_OUPROCO;
        a2kOujulX.setA2kOujulXBytes(buffer, position);
        position += A2kOujulX.Len.A2K_OUJUL_X;
        a2kOusta.setA2kOustaBytes(buffer, position);
        position += A2kOusta.Len.A2K_OUSTA;
        a2kOurid.setA2kOuridBytes(buffer, position);
        position += A2kOurid.Len.A2K_OURID;
        a2kOugg06 = MarshalByte.readString(buffer, position, Len.A2K_OUGG06);
        position += Len.A2K_OUGG06;
        a2kOung06 = MarshalByte.readFixedString(buffer, position, Len.A2K_OUNG06);
        position += Len.A2K_OUNG06;
        a2kOutg06 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a2kOugg07 = MarshalByte.readFixedString(buffer, position, Len.A2K_OUGG07);
        position += Len.A2K_OUGG07;
        a2kOugl07X.setA2kOugl07XBytes(buffer, position);
        position += A2kOugl07X.Len.A2K_OUGL07_X;
        a2kOugg08 = MarshalByte.readFixedString(buffer, position, Len.A2K_OUGG08);
        position += Len.A2K_OUGG08;
        a2kOugg09 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a2kOugg10 = MarshalByte.readChar(buffer, position);
    }

    public byte[] getOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        a2kOugmaX.getA2kOugmaXBytes(buffer, position);
        position += A2kOugmaX.Len.A2K_OUGMA_X;
        a2kOugbmba.getA2kOugbmbaBytes(buffer, position);
        position += A2kOugbmba.Len.A2K_OUGBMBA;
        a2kOuamgX.getA2kOuamgXBytes(buffer, position);
        position += A2kOuamgX.Len.A2K_OUAMG_X;
        MarshalByte.writeIntAsPacked(buffer, position, a2kOuamgp, Len.Int.A2K_OUAMGP, 0, SignType.NO_SIGN);
        position += Len.A2K_OUAMGP;
        MarshalByte.writeIntAsPacked(buffer, position, a2kOuamg0p, Len.Int.A2K_OUAMG0P, 0, SignType.NO_SIGN);
        position += Len.A2K_OUAMG0P;
        MarshalByte.writeIntAsPacked(buffer, position, a2kOuprog9, Len.Int.A2K_OUPROG9, 0, SignType.NO_SIGN);
        position += Len.A2K_OUPROG9;
        MarshalByte.writeIntAsPacked(buffer, position, a2kOuprogc, Len.Int.A2K_OUPROGC, 0, SignType.NO_SIGN);
        position += Len.A2K_OUPROGC;
        MarshalByte.writeIntAsPacked(buffer, position, a2kOuprog, Len.Int.A2K_OUPROG, 0, SignType.NO_SIGN);
        position += Len.A2K_OUPROG;
        MarshalByte.writeIntAsPacked(buffer, position, a2kOuproco, Len.Int.A2K_OUPROCO, 0, SignType.NO_SIGN);
        position += Len.A2K_OUPROCO;
        a2kOujulX.getA2kOujulXBytes(buffer, position);
        position += A2kOujulX.Len.A2K_OUJUL_X;
        a2kOusta.getA2kOustaBytes(buffer, position);
        position += A2kOusta.Len.A2K_OUSTA;
        a2kOurid.getA2kOuridBytes(buffer, position);
        position += A2kOurid.Len.A2K_OURID;
        MarshalByte.writeString(buffer, position, a2kOugg06, Len.A2K_OUGG06);
        position += Len.A2K_OUGG06;
        MarshalByte.writeString(buffer, position, a2kOung06, Len.A2K_OUNG06);
        position += Len.A2K_OUNG06;
        MarshalByte.writeChar(buffer, position, a2kOutg06);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, a2kOugg07, Len.A2K_OUGG07);
        position += Len.A2K_OUGG07;
        a2kOugl07X.getA2kOugl07XBytes(buffer, position);
        position += A2kOugl07X.Len.A2K_OUGL07_X;
        MarshalByte.writeString(buffer, position, a2kOugg08, Len.A2K_OUGG08);
        position += Len.A2K_OUGG08;
        MarshalByte.writeChar(buffer, position, a2kOugg09);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, a2kOugg10);
        return buffer;
    }

    public void initOutputSpaces() {
        a2kOugmaX.initA2kOugmaXSpaces();
        a2kOugbmba.initA2kOugbmbaSpaces();
        a2kOuamgX.initA2kOuamgXSpaces();
        a2kOuamgp = Types.INVALID_INT_VAL;
        a2kOuamg0p = Types.INVALID_INT_VAL;
        a2kOuprog9 = Types.INVALID_INT_VAL;
        a2kOuprogc = Types.INVALID_INT_VAL;
        a2kOuprog = Types.INVALID_INT_VAL;
        a2kOuproco = Types.INVALID_INT_VAL;
        a2kOujulX.initA2kOujulXSpaces();
        a2kOusta.initA2kOustaSpaces();
        a2kOurid.initA2kOuridSpaces();
        a2kOugg06 = "";
        a2kOung06 = "";
        a2kOutg06 = Types.SPACE_CHAR;
        a2kOugg07 = "";
        a2kOugl07X.initA2kOugl07XSpaces();
        a2kOugg08 = "";
        a2kOugg09 = Types.SPACE_CHAR;
        a2kOugg10 = Types.SPACE_CHAR;
    }

    public void setA2kOuamgp(int a2kOuamgp) {
        this.a2kOuamgp = a2kOuamgp;
    }

    public int getA2kOuamgp() {
        return this.a2kOuamgp;
    }

    public void setA2kOuamg0p(int a2kOuamg0p) {
        this.a2kOuamg0p = a2kOuamg0p;
    }

    public int getA2kOuamg0p() {
        return this.a2kOuamg0p;
    }

    public void setA2kOuprog9(int a2kOuprog9) {
        this.a2kOuprog9 = a2kOuprog9;
    }

    public int getA2kOuprog9() {
        return this.a2kOuprog9;
    }

    public void setA2kOuprogc(int a2kOuprogc) {
        this.a2kOuprogc = a2kOuprogc;
    }

    public int getA2kOuprogc() {
        return this.a2kOuprogc;
    }

    public void setA2kOuprog(int a2kOuprog) {
        this.a2kOuprog = a2kOuprog;
    }

    public int getA2kOuprog() {
        return this.a2kOuprog;
    }

    public void setA2kOuproco(int a2kOuproco) {
        this.a2kOuproco = a2kOuproco;
    }

    public int getA2kOuproco() {
        return this.a2kOuproco;
    }

    public void setA2kOugg06(String a2kOugg06) {
        this.a2kOugg06 = Functions.subString(a2kOugg06, Len.A2K_OUGG06);
    }

    public String getA2kOugg06() {
        return this.a2kOugg06;
    }

    public void setA2kOung06(short a2kOung06) {
        this.a2kOung06 = NumericDisplay.asString(a2kOung06, Len.A2K_OUNG06);
    }

    public void setA2kOung06Formatted(String a2kOung06) {
        this.a2kOung06 = Trunc.toUnsignedNumeric(a2kOung06, Len.A2K_OUNG06);
    }

    public short getA2kOung06() {
        return NumericDisplay.asShort(this.a2kOung06);
    }

    public void setA2kOutg06(char a2kOutg06) {
        this.a2kOutg06 = a2kOutg06;
    }

    public void setA2kOutg06Formatted(String a2kOutg06) {
        setA2kOutg06(Functions.charAt(a2kOutg06, Types.CHAR_SIZE));
    }

    public char getA2kOutg06() {
        return this.a2kOutg06;
    }

    public void setA2kOugg07Formatted(String a2kOugg07) {
        this.a2kOugg07 = Trunc.toUnsignedNumeric(a2kOugg07, Len.A2K_OUGG07);
    }

    public short getA2kOugg07() {
        return NumericDisplay.asShort(this.a2kOugg07);
    }

    public void setA2kOugg08(short a2kOugg08) {
        this.a2kOugg08 = NumericDisplay.asString(a2kOugg08, Len.A2K_OUGG08);
    }

    public void setA2kOugg08Formatted(String a2kOugg08) {
        this.a2kOugg08 = Trunc.toUnsignedNumeric(a2kOugg08, Len.A2K_OUGG08);
    }

    public short getA2kOugg08() {
        return NumericDisplay.asShort(this.a2kOugg08);
    }

    public void setA2kOugg09(char a2kOugg09) {
        this.a2kOugg09 = a2kOugg09;
    }

    public void setA2kOugg09Formatted(String a2kOugg09) {
        setA2kOugg09(Functions.charAt(a2kOugg09, Types.CHAR_SIZE));
    }

    public char getA2kOugg09() {
        return this.a2kOugg09;
    }

    public void setA2kOugg10(char a2kOugg10) {
        this.a2kOugg10 = a2kOugg10;
    }

    public void setA2kOugg10Formatted(String a2kOugg10) {
        setA2kOugg10(Functions.charAt(a2kOugg10, Types.CHAR_SIZE));
    }

    public char getA2kOugg10() {
        return this.a2kOugg10;
    }

    public A2kOuamgX getA2kOuamgX() {
        return a2kOuamgX;
    }

    public A2kOugbmba getA2kOugbmba() {
        return a2kOugbmba;
    }

    public A2kOugl07X getA2kOugl07X() {
        return a2kOugl07X;
    }

    public A2kOugmaX getA2kOugmaX() {
        return a2kOugmaX;
    }

    public A2kOujulX getA2kOujulX() {
        return a2kOujulX;
    }

    public A2kOurid getA2kOurid() {
        return a2kOurid;
    }

    public A2kOusta getA2kOusta() {
        return a2kOusta;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int A2K_OUAMGP = 5;
        public static final int A2K_OUAMG0P = 5;
        public static final int A2K_OUPROG9 = 3;
        public static final int A2K_OUPROGC = 3;
        public static final int A2K_OUPROG = 4;
        public static final int A2K_OUPROCO = 4;
        public static final int A2K_OUGG06 = 9;
        public static final int A2K_OUNG06 = 1;
        public static final int A2K_OUTG06 = 1;
        public static final int A2K_OUGG07 = 2;
        public static final int A2K_OUGG08 = 1;
        public static final int A2K_OUGG09 = 1;
        public static final int A2K_OUGG10 = 1;
        public static final int OUTPUT = A2kOugmaX.Len.A2K_OUGMA_X + A2kOugbmba.Len.A2K_OUGBMBA + A2kOuamgX.Len.A2K_OUAMG_X + A2K_OUAMGP + A2K_OUAMG0P + A2K_OUPROG9 + A2K_OUPROGC + A2K_OUPROG + A2K_OUPROCO + A2kOujulX.Len.A2K_OUJUL_X + A2kOusta.Len.A2K_OUSTA + A2kOurid.Len.A2K_OURID + A2K_OUGG06 + A2K_OUNG06 + A2K_OUTG06 + A2K_OUGG07 + A2kOugl07X.Len.A2K_OUGL07_X + A2K_OUGG08 + A2K_OUGG09 + A2K_OUGG10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A2K_OUAMGP = 9;
            public static final int A2K_OUAMG0P = 9;
            public static final int A2K_OUPROG9 = 5;
            public static final int A2K_OUPROGC = 5;
            public static final int A2K_OUPROG = 7;
            public static final int A2K_OUPROCO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
