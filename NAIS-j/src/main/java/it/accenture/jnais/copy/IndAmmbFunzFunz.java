package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-AMMB-FUNZ-FUNZ<br>
 * Variable: IND-AMMB-FUNZ-FUNZ from copybook IDBVL052<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndAmmbFunzFunz {

    //==== PROPERTIES ====
    //Original name: IND-L05-COD-BLOCCO
    private short codBlocco = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L05-SRVZ-VER-ANN
    private short srvzVerAnn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L05-WHERE-CONDITION
    private short whereCondition = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L05-FL-POLI-IFP
    private short flPoliIfp = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setCodBlocco(short codBlocco) {
        this.codBlocco = codBlocco;
    }

    public short getCodBlocco() {
        return this.codBlocco;
    }

    public void setSrvzVerAnn(short srvzVerAnn) {
        this.srvzVerAnn = srvzVerAnn;
    }

    public short getSrvzVerAnn() {
        return this.srvzVerAnn;
    }

    public void setWhereCondition(short whereCondition) {
        this.whereCondition = whereCondition;
    }

    public short getWhereCondition() {
        return this.whereCondition;
    }

    public void setFlPoliIfp(short flPoliIfp) {
        this.flPoliIfp = flPoliIfp;
    }

    public short getFlPoliIfp() {
        return this.flPoliIfp;
    }
}
