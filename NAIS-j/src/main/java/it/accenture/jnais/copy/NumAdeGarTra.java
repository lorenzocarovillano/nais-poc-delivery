package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.NumUltNumLin;

/**Original name: NUM-ADE-GAR-TRA<br>
 * Variable: NUM-ADE-GAR-TRA from copybook IDBVNUM1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class NumAdeGarTra {

    //==== PROPERTIES ====
    //Original name: NUM-COD-COMP-ANIA
    private int numCodCompAnia = DefaultValues.INT_VAL;
    //Original name: NUM-ID-OGG
    private int numIdOgg = DefaultValues.INT_VAL;
    //Original name: NUM-TP-OGG
    private String numTpOgg = DefaultValues.stringVal(Len.NUM_TP_OGG);
    //Original name: NUM-ULT-NUM-LIN
    private NumUltNumLin numUltNumLin = new NumUltNumLin();
    //Original name: NUM-DS-OPER-SQL
    private char numDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: NUM-DS-VER
    private int numDsVer = DefaultValues.INT_VAL;
    //Original name: NUM-DS-TS-CPTZ
    private long numDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: NUM-DS-UTENTE
    private String numDsUtente = DefaultValues.stringVal(Len.NUM_DS_UTENTE);
    //Original name: NUM-DS-STATO-ELAB
    private char numDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setNumAdeGarTraFormatted(String data) {
        byte[] buffer = new byte[Len.NUM_ADE_GAR_TRA];
        MarshalByte.writeString(buffer, 1, data, Len.NUM_ADE_GAR_TRA);
        setNumAdeGarTraBytes(buffer, 1);
    }

    public String getNumAdeGarTraFormatted() {
        return MarshalByteExt.bufferToStr(getNumAdeGarTraBytes());
    }

    public byte[] getNumAdeGarTraBytes() {
        byte[] buffer = new byte[Len.NUM_ADE_GAR_TRA];
        return getNumAdeGarTraBytes(buffer, 1);
    }

    public void setNumAdeGarTraBytes(byte[] buffer, int offset) {
        int position = offset;
        numCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_COD_COMP_ANIA, 0);
        position += Len.NUM_COD_COMP_ANIA;
        numIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_ID_OGG, 0);
        position += Len.NUM_ID_OGG;
        numTpOgg = MarshalByte.readString(buffer, position, Len.NUM_TP_OGG);
        position += Len.NUM_TP_OGG;
        numUltNumLin.setNumUltNumLinFromBuffer(buffer, position);
        position += NumUltNumLin.Len.NUM_ULT_NUM_LIN;
        numDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        numDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_DS_VER, 0);
        position += Len.NUM_DS_VER;
        numDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.NUM_DS_TS_CPTZ, 0);
        position += Len.NUM_DS_TS_CPTZ;
        numDsUtente = MarshalByte.readString(buffer, position, Len.NUM_DS_UTENTE);
        position += Len.NUM_DS_UTENTE;
        numDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getNumAdeGarTraBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, numCodCompAnia, Len.Int.NUM_COD_COMP_ANIA, 0);
        position += Len.NUM_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, numIdOgg, Len.Int.NUM_ID_OGG, 0);
        position += Len.NUM_ID_OGG;
        MarshalByte.writeString(buffer, position, numTpOgg, Len.NUM_TP_OGG);
        position += Len.NUM_TP_OGG;
        numUltNumLin.getNumUltNumLinAsBuffer(buffer, position);
        position += NumUltNumLin.Len.NUM_ULT_NUM_LIN;
        MarshalByte.writeChar(buffer, position, numDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, numDsVer, Len.Int.NUM_DS_VER, 0);
        position += Len.NUM_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, numDsTsCptz, Len.Int.NUM_DS_TS_CPTZ, 0);
        position += Len.NUM_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, numDsUtente, Len.NUM_DS_UTENTE);
        position += Len.NUM_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, numDsStatoElab);
        return buffer;
    }

    public void setNumCodCompAnia(int numCodCompAnia) {
        this.numCodCompAnia = numCodCompAnia;
    }

    public int getNumCodCompAnia() {
        return this.numCodCompAnia;
    }

    public void setNumIdOgg(int numIdOgg) {
        this.numIdOgg = numIdOgg;
    }

    public int getNumIdOgg() {
        return this.numIdOgg;
    }

    public void setNumTpOgg(String numTpOgg) {
        this.numTpOgg = Functions.subString(numTpOgg, Len.NUM_TP_OGG);
    }

    public String getNumTpOgg() {
        return this.numTpOgg;
    }

    public void setNumDsOperSql(char numDsOperSql) {
        this.numDsOperSql = numDsOperSql;
    }

    public char getNumDsOperSql() {
        return this.numDsOperSql;
    }

    public void setNumDsVer(int numDsVer) {
        this.numDsVer = numDsVer;
    }

    public int getNumDsVer() {
        return this.numDsVer;
    }

    public void setNumDsTsCptz(long numDsTsCptz) {
        this.numDsTsCptz = numDsTsCptz;
    }

    public long getNumDsTsCptz() {
        return this.numDsTsCptz;
    }

    public void setNumDsUtente(String numDsUtente) {
        this.numDsUtente = Functions.subString(numDsUtente, Len.NUM_DS_UTENTE);
    }

    public String getNumDsUtente() {
        return this.numDsUtente;
    }

    public void setNumDsStatoElab(char numDsStatoElab) {
        this.numDsStatoElab = numDsStatoElab;
    }

    public char getNumDsStatoElab() {
        return this.numDsStatoElab;
    }

    public NumUltNumLin getNumUltNumLin() {
        return numUltNumLin;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NUM_TP_OGG = 2;
        public static final int NUM_DS_UTENTE = 20;
        public static final int NUM_COD_COMP_ANIA = 3;
        public static final int NUM_ID_OGG = 5;
        public static final int NUM_DS_OPER_SQL = 1;
        public static final int NUM_DS_VER = 5;
        public static final int NUM_DS_TS_CPTZ = 10;
        public static final int NUM_DS_STATO_ELAB = 1;
        public static final int NUM_ADE_GAR_TRA = NUM_COD_COMP_ANIA + NUM_ID_OGG + NUM_TP_OGG + NumUltNumLin.Len.NUM_ULT_NUM_LIN + NUM_DS_OPER_SQL + NUM_DS_VER + NUM_DS_TS_CPTZ + NUM_DS_UTENTE + NUM_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int NUM_COD_COMP_ANIA = 5;
            public static final int NUM_ID_OGG = 9;
            public static final int NUM_DS_VER = 9;
            public static final int NUM_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
