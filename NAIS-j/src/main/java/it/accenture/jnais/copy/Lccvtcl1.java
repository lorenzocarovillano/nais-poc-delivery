package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVTCL1<br>
 * Variable: LCCVTCL1 from copybook LCCVTCL1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvtcl1 {

    //==== PROPERTIES ====
    /**Original name: WTCL-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA TCONT_LIQ
	 *    ALIAS TCL
	 *    ULTIMO AGG. 02 LUG 2014
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WTCL-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WTCL-DATI
    private WtclDati dati = new WtclDati();

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WtclDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
