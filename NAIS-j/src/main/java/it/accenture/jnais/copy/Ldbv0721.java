package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV0721<br>
 * Copybook: LDBV0721 from copybook LDBV0721<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Ldbv0721 {

    //==== PROPERTIES ====
    //Original name: LDBV0721-ID-OGG-COLLG
    private int ldbv0721IdOggCollg = DefaultValues.INT_VAL;
    //Original name: LDBV0721-TP-OGG-COLLG
    private String ldbv0721TpOggCollg = DefaultValues.stringVal(Len.LDBV0721_TP_OGG_COLLG);

    //==== METHODS ====
    public void setLdbv0721Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV0721];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV0721);
        setLdbv0721Bytes(buffer, 1);
    }

    public String getLdbv0721Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv0721Bytes());
    }

    /**Original name: LDBV0721<br>*/
    public byte[] getLdbv0721Bytes() {
        byte[] buffer = new byte[Len.LDBV0721];
        return getLdbv0721Bytes(buffer, 1);
    }

    public void setLdbv0721Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv0721IdOggCollg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV0721_ID_OGG_COLLG, 0);
        position += Len.LDBV0721_ID_OGG_COLLG;
        ldbv0721TpOggCollg = MarshalByte.readString(buffer, position, Len.LDBV0721_TP_OGG_COLLG);
    }

    public byte[] getLdbv0721Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv0721IdOggCollg, Len.Int.LDBV0721_ID_OGG_COLLG, 0);
        position += Len.LDBV0721_ID_OGG_COLLG;
        MarshalByte.writeString(buffer, position, ldbv0721TpOggCollg, Len.LDBV0721_TP_OGG_COLLG);
        return buffer;
    }

    public void setLdbv0721IdOggCollg(int ldbv0721IdOggCollg) {
        this.ldbv0721IdOggCollg = ldbv0721IdOggCollg;
    }

    public int getLdbv0721IdOggCollg() {
        return this.ldbv0721IdOggCollg;
    }

    public void setLdbv0721TpOggCollg(String ldbv0721TpOggCollg) {
        this.ldbv0721TpOggCollg = Functions.subString(ldbv0721TpOggCollg, Len.LDBV0721_TP_OGG_COLLG);
    }

    public String getLdbv0721TpOggCollg() {
        return this.ldbv0721TpOggCollg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV0721_TP_OGG_COLLG = 2;
        public static final int LDBV0721_ID_OGG_COLLG = 5;
        public static final int LDBV0721 = LDBV0721_ID_OGG_COLLG + LDBV0721_TP_OGG_COLLG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV0721_ID_OGG_COLLG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
