package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-DFLT-ADES<br>
 * Variable: IND-DFLT-ADES from copybook IDBVDAD2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndDfltAdes {

    //==== PROPERTIES ====
    //Original name: IND-DAD-IB-POLI
    private short ibPoli = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-IB-DFLT
    private short ibDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-COD-GAR-1
    private short codGar1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-COD-GAR-2
    private short codGar2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-COD-GAR-3
    private short codGar3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-COD-GAR-4
    private short codGar4 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-COD-GAR-5
    private short codGar5 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-COD-GAR-6
    private short codGar6 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-DT-DECOR-DFLT
    private short dtDecorDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-ETA-SCAD-MASC-DFLT
    private short etaScadMascDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-ETA-SCAD-FEMM-DFLT
    private short etaScadFemmDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-DUR-AA-ADES-DFLT
    private short durAaAdesDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-DUR-MM-ADES-DFLT
    private short durMmAdesDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-DUR-GG-ADES-DFLT
    private short durGgAdesDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-DT-SCAD-ADES-DFLT
    private short dtScadAdesDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-FRAZ-DFLT
    private short frazDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-PC-PROV-INC-DFLT
    private short pcProvIncDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-IMP-PROV-INC-DFLT
    private short impProvIncDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-IMP-AZ
    private short impAz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-IMP-ADER
    private short impAder = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-IMP-TFR
    private short impTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-IMP-VOLO
    private short impVolo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-PC-AZ
    private short pcAz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-PC-ADER
    private short pcAder = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-PC-TFR
    private short pcTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-PC-VOLO
    private short pcVolo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-TP-FNT-CNBTVA
    private short tpFntCnbtva = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-IMP-PRE-DFLT
    private short impPreDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DAD-TP-PRE
    private short tpPre = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIbPoli(short ibPoli) {
        this.ibPoli = ibPoli;
    }

    public short getIbPoli() {
        return this.ibPoli;
    }

    public void setIbDflt(short ibDflt) {
        this.ibDflt = ibDflt;
    }

    public short getIbDflt() {
        return this.ibDflt;
    }

    public void setCodGar1(short codGar1) {
        this.codGar1 = codGar1;
    }

    public short getCodGar1() {
        return this.codGar1;
    }

    public void setCodGar2(short codGar2) {
        this.codGar2 = codGar2;
    }

    public short getCodGar2() {
        return this.codGar2;
    }

    public void setCodGar3(short codGar3) {
        this.codGar3 = codGar3;
    }

    public short getCodGar3() {
        return this.codGar3;
    }

    public void setCodGar4(short codGar4) {
        this.codGar4 = codGar4;
    }

    public short getCodGar4() {
        return this.codGar4;
    }

    public void setCodGar5(short codGar5) {
        this.codGar5 = codGar5;
    }

    public short getCodGar5() {
        return this.codGar5;
    }

    public void setCodGar6(short codGar6) {
        this.codGar6 = codGar6;
    }

    public short getCodGar6() {
        return this.codGar6;
    }

    public void setDtDecorDflt(short dtDecorDflt) {
        this.dtDecorDflt = dtDecorDflt;
    }

    public short getDtDecorDflt() {
        return this.dtDecorDflt;
    }

    public void setEtaScadMascDflt(short etaScadMascDflt) {
        this.etaScadMascDflt = etaScadMascDflt;
    }

    public short getEtaScadMascDflt() {
        return this.etaScadMascDflt;
    }

    public void setEtaScadFemmDflt(short etaScadFemmDflt) {
        this.etaScadFemmDflt = etaScadFemmDflt;
    }

    public short getEtaScadFemmDflt() {
        return this.etaScadFemmDflt;
    }

    public void setDurAaAdesDflt(short durAaAdesDflt) {
        this.durAaAdesDflt = durAaAdesDflt;
    }

    public short getDurAaAdesDflt() {
        return this.durAaAdesDflt;
    }

    public void setDurMmAdesDflt(short durMmAdesDflt) {
        this.durMmAdesDflt = durMmAdesDflt;
    }

    public short getDurMmAdesDflt() {
        return this.durMmAdesDflt;
    }

    public void setDurGgAdesDflt(short durGgAdesDflt) {
        this.durGgAdesDflt = durGgAdesDflt;
    }

    public short getDurGgAdesDflt() {
        return this.durGgAdesDflt;
    }

    public void setDtScadAdesDflt(short dtScadAdesDflt) {
        this.dtScadAdesDflt = dtScadAdesDflt;
    }

    public short getDtScadAdesDflt() {
        return this.dtScadAdesDflt;
    }

    public void setFrazDflt(short frazDflt) {
        this.frazDflt = frazDflt;
    }

    public short getFrazDflt() {
        return this.frazDflt;
    }

    public void setPcProvIncDflt(short pcProvIncDflt) {
        this.pcProvIncDflt = pcProvIncDflt;
    }

    public short getPcProvIncDflt() {
        return this.pcProvIncDflt;
    }

    public void setImpProvIncDflt(short impProvIncDflt) {
        this.impProvIncDflt = impProvIncDflt;
    }

    public short getImpProvIncDflt() {
        return this.impProvIncDflt;
    }

    public void setImpAz(short impAz) {
        this.impAz = impAz;
    }

    public short getImpAz() {
        return this.impAz;
    }

    public void setImpAder(short impAder) {
        this.impAder = impAder;
    }

    public short getImpAder() {
        return this.impAder;
    }

    public void setImpTfr(short impTfr) {
        this.impTfr = impTfr;
    }

    public short getImpTfr() {
        return this.impTfr;
    }

    public void setImpVolo(short impVolo) {
        this.impVolo = impVolo;
    }

    public short getImpVolo() {
        return this.impVolo;
    }

    public void setPcAz(short pcAz) {
        this.pcAz = pcAz;
    }

    public short getPcAz() {
        return this.pcAz;
    }

    public void setPcAder(short pcAder) {
        this.pcAder = pcAder;
    }

    public short getPcAder() {
        return this.pcAder;
    }

    public void setPcTfr(short pcTfr) {
        this.pcTfr = pcTfr;
    }

    public short getPcTfr() {
        return this.pcTfr;
    }

    public void setPcVolo(short pcVolo) {
        this.pcVolo = pcVolo;
    }

    public short getPcVolo() {
        return this.pcVolo;
    }

    public void setTpFntCnbtva(short tpFntCnbtva) {
        this.tpFntCnbtva = tpFntCnbtva;
    }

    public short getTpFntCnbtva() {
        return this.tpFntCnbtva;
    }

    public void setImpPreDflt(short impPreDflt) {
        this.impPreDflt = impPreDflt;
    }

    public short getImpPreDflt() {
        return this.impPreDflt;
    }

    public void setTpPre(short tpPre) {
        this.tpPre = tpPre;
    }

    public short getTpPre() {
        return this.tpPre;
    }
}
