package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.GrlDtSin1oAssto;
import it.accenture.jnais.ws.redefines.GrlDtSin2oAssto;
import it.accenture.jnais.ws.redefines.GrlDtSin3oAssto;
import it.accenture.jnais.ws.redefines.GrlIdMoviChiu;

/**Original name: GAR-LIQ<br>
 * Variable: GAR-LIQ from copybook IDBVGRL1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class GarLiq {

    //==== PROPERTIES ====
    //Original name: GRL-ID-GAR-LIQ
    private int grlIdGarLiq = DefaultValues.INT_VAL;
    //Original name: GRL-ID-LIQ
    private int grlIdLiq = DefaultValues.INT_VAL;
    //Original name: GRL-ID-GAR
    private int grlIdGar = DefaultValues.INT_VAL;
    //Original name: GRL-ID-MOVI-CRZ
    private int grlIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: GRL-ID-MOVI-CHIU
    private GrlIdMoviChiu grlIdMoviChiu = new GrlIdMoviChiu();
    //Original name: GRL-DT-INI-EFF
    private int grlDtIniEff = DefaultValues.INT_VAL;
    //Original name: GRL-DT-END-EFF
    private int grlDtEndEff = DefaultValues.INT_VAL;
    //Original name: GRL-COD-COMP-ANIA
    private int grlCodCompAnia = DefaultValues.INT_VAL;
    //Original name: GRL-DT-SIN-1O-ASSTO
    private GrlDtSin1oAssto grlDtSin1oAssto = new GrlDtSin1oAssto();
    //Original name: GRL-CAU-SIN-1O-ASSTO
    private String grlCauSin1oAssto = DefaultValues.stringVal(Len.GRL_CAU_SIN1O_ASSTO);
    //Original name: GRL-TP-SIN-1O-ASSTO
    private String grlTpSin1oAssto = DefaultValues.stringVal(Len.GRL_TP_SIN1O_ASSTO);
    //Original name: GRL-DT-SIN-2O-ASSTO
    private GrlDtSin2oAssto grlDtSin2oAssto = new GrlDtSin2oAssto();
    //Original name: GRL-CAU-SIN-2O-ASSTO
    private String grlCauSin2oAssto = DefaultValues.stringVal(Len.GRL_CAU_SIN2O_ASSTO);
    //Original name: GRL-TP-SIN-2O-ASSTO
    private String grlTpSin2oAssto = DefaultValues.stringVal(Len.GRL_TP_SIN2O_ASSTO);
    //Original name: GRL-DT-SIN-3O-ASSTO
    private GrlDtSin3oAssto grlDtSin3oAssto = new GrlDtSin3oAssto();
    //Original name: GRL-CAU-SIN-3O-ASSTO
    private String grlCauSin3oAssto = DefaultValues.stringVal(Len.GRL_CAU_SIN3O_ASSTO);
    //Original name: GRL-TP-SIN-3O-ASSTO
    private String grlTpSin3oAssto = DefaultValues.stringVal(Len.GRL_TP_SIN3O_ASSTO);
    //Original name: GRL-DS-RIGA
    private long grlDsRiga = DefaultValues.LONG_VAL;
    //Original name: GRL-DS-OPER-SQL
    private char grlDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: GRL-DS-VER
    private int grlDsVer = DefaultValues.INT_VAL;
    //Original name: GRL-DS-TS-INI-CPTZ
    private long grlDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: GRL-DS-TS-END-CPTZ
    private long grlDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: GRL-DS-UTENTE
    private String grlDsUtente = DefaultValues.stringVal(Len.GRL_DS_UTENTE);
    //Original name: GRL-DS-STATO-ELAB
    private char grlDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setGarLiqFormatted(String data) {
        byte[] buffer = new byte[Len.GAR_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.GAR_LIQ);
        setGarLiqBytes(buffer, 1);
    }

    public void setGarLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        grlIdGarLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_ID_GAR_LIQ, 0);
        position += Len.GRL_ID_GAR_LIQ;
        grlIdLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_ID_LIQ, 0);
        position += Len.GRL_ID_LIQ;
        grlIdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_ID_GAR, 0);
        position += Len.GRL_ID_GAR;
        grlIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_ID_MOVI_CRZ, 0);
        position += Len.GRL_ID_MOVI_CRZ;
        grlIdMoviChiu.setGrlIdMoviChiuFromBuffer(buffer, position);
        position += GrlIdMoviChiu.Len.GRL_ID_MOVI_CHIU;
        grlDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_DT_INI_EFF, 0);
        position += Len.GRL_DT_INI_EFF;
        grlDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_DT_END_EFF, 0);
        position += Len.GRL_DT_END_EFF;
        grlCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_COD_COMP_ANIA, 0);
        position += Len.GRL_COD_COMP_ANIA;
        grlDtSin1oAssto.setGrlDtSin1oAsstoFromBuffer(buffer, position);
        position += GrlDtSin1oAssto.Len.GRL_DT_SIN1O_ASSTO;
        grlCauSin1oAssto = MarshalByte.readString(buffer, position, Len.GRL_CAU_SIN1O_ASSTO);
        position += Len.GRL_CAU_SIN1O_ASSTO;
        grlTpSin1oAssto = MarshalByte.readString(buffer, position, Len.GRL_TP_SIN1O_ASSTO);
        position += Len.GRL_TP_SIN1O_ASSTO;
        grlDtSin2oAssto.setGrlDtSin2oAsstoFromBuffer(buffer, position);
        position += GrlDtSin2oAssto.Len.GRL_DT_SIN2O_ASSTO;
        grlCauSin2oAssto = MarshalByte.readString(buffer, position, Len.GRL_CAU_SIN2O_ASSTO);
        position += Len.GRL_CAU_SIN2O_ASSTO;
        grlTpSin2oAssto = MarshalByte.readString(buffer, position, Len.GRL_TP_SIN2O_ASSTO);
        position += Len.GRL_TP_SIN2O_ASSTO;
        grlDtSin3oAssto.setGrlDtSin3oAsstoFromBuffer(buffer, position);
        position += GrlDtSin3oAssto.Len.GRL_DT_SIN3O_ASSTO;
        grlCauSin3oAssto = MarshalByte.readString(buffer, position, Len.GRL_CAU_SIN3O_ASSTO);
        position += Len.GRL_CAU_SIN3O_ASSTO;
        grlTpSin3oAssto = MarshalByte.readString(buffer, position, Len.GRL_TP_SIN3O_ASSTO);
        position += Len.GRL_TP_SIN3O_ASSTO;
        grlDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.GRL_DS_RIGA, 0);
        position += Len.GRL_DS_RIGA;
        grlDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        grlDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRL_DS_VER, 0);
        position += Len.GRL_DS_VER;
        grlDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.GRL_DS_TS_INI_CPTZ, 0);
        position += Len.GRL_DS_TS_INI_CPTZ;
        grlDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.GRL_DS_TS_END_CPTZ, 0);
        position += Len.GRL_DS_TS_END_CPTZ;
        grlDsUtente = MarshalByte.readString(buffer, position, Len.GRL_DS_UTENTE);
        position += Len.GRL_DS_UTENTE;
        grlDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public void setGrlIdGarLiq(int grlIdGarLiq) {
        this.grlIdGarLiq = grlIdGarLiq;
    }

    public int getGrlIdGarLiq() {
        return this.grlIdGarLiq;
    }

    public void setGrlIdLiq(int grlIdLiq) {
        this.grlIdLiq = grlIdLiq;
    }

    public int getGrlIdLiq() {
        return this.grlIdLiq;
    }

    public void setGrlIdGar(int grlIdGar) {
        this.grlIdGar = grlIdGar;
    }

    public int getGrlIdGar() {
        return this.grlIdGar;
    }

    public void setGrlIdMoviCrz(int grlIdMoviCrz) {
        this.grlIdMoviCrz = grlIdMoviCrz;
    }

    public int getGrlIdMoviCrz() {
        return this.grlIdMoviCrz;
    }

    public void setGrlDtIniEff(int grlDtIniEff) {
        this.grlDtIniEff = grlDtIniEff;
    }

    public int getGrlDtIniEff() {
        return this.grlDtIniEff;
    }

    public void setGrlDtEndEff(int grlDtEndEff) {
        this.grlDtEndEff = grlDtEndEff;
    }

    public int getGrlDtEndEff() {
        return this.grlDtEndEff;
    }

    public void setGrlCodCompAnia(int grlCodCompAnia) {
        this.grlCodCompAnia = grlCodCompAnia;
    }

    public int getGrlCodCompAnia() {
        return this.grlCodCompAnia;
    }

    public void setGrlCauSin1oAssto(String grlCauSin1oAssto) {
        this.grlCauSin1oAssto = Functions.subString(grlCauSin1oAssto, Len.GRL_CAU_SIN1O_ASSTO);
    }

    public String getGrlCauSin1oAssto() {
        return this.grlCauSin1oAssto;
    }

    public String getGrlCauSin1oAsstoFormatted() {
        return Functions.padBlanks(getGrlCauSin1oAssto(), Len.GRL_CAU_SIN1O_ASSTO);
    }

    public void setGrlTpSin1oAssto(String grlTpSin1oAssto) {
        this.grlTpSin1oAssto = Functions.subString(grlTpSin1oAssto, Len.GRL_TP_SIN1O_ASSTO);
    }

    public String getGrlTpSin1oAssto() {
        return this.grlTpSin1oAssto;
    }

    public String getGrlTpSin1oAsstoFormatted() {
        return Functions.padBlanks(getGrlTpSin1oAssto(), Len.GRL_TP_SIN1O_ASSTO);
    }

    public void setGrlCauSin2oAssto(String grlCauSin2oAssto) {
        this.grlCauSin2oAssto = Functions.subString(grlCauSin2oAssto, Len.GRL_CAU_SIN2O_ASSTO);
    }

    public String getGrlCauSin2oAssto() {
        return this.grlCauSin2oAssto;
    }

    public String getGrlCauSin2oAsstoFormatted() {
        return Functions.padBlanks(getGrlCauSin2oAssto(), Len.GRL_CAU_SIN2O_ASSTO);
    }

    public void setGrlTpSin2oAssto(String grlTpSin2oAssto) {
        this.grlTpSin2oAssto = Functions.subString(grlTpSin2oAssto, Len.GRL_TP_SIN2O_ASSTO);
    }

    public String getGrlTpSin2oAssto() {
        return this.grlTpSin2oAssto;
    }

    public String getGrlTpSin2oAsstoFormatted() {
        return Functions.padBlanks(getGrlTpSin2oAssto(), Len.GRL_TP_SIN2O_ASSTO);
    }

    public void setGrlCauSin3oAssto(String grlCauSin3oAssto) {
        this.grlCauSin3oAssto = Functions.subString(grlCauSin3oAssto, Len.GRL_CAU_SIN3O_ASSTO);
    }

    public String getGrlCauSin3oAssto() {
        return this.grlCauSin3oAssto;
    }

    public String getGrlCauSin3oAsstoFormatted() {
        return Functions.padBlanks(getGrlCauSin3oAssto(), Len.GRL_CAU_SIN3O_ASSTO);
    }

    public void setGrlTpSin3oAssto(String grlTpSin3oAssto) {
        this.grlTpSin3oAssto = Functions.subString(grlTpSin3oAssto, Len.GRL_TP_SIN3O_ASSTO);
    }

    public String getGrlTpSin3oAssto() {
        return this.grlTpSin3oAssto;
    }

    public String getGrlTpSin3oAsstoFormatted() {
        return Functions.padBlanks(getGrlTpSin3oAssto(), Len.GRL_TP_SIN3O_ASSTO);
    }

    public void setGrlDsRiga(long grlDsRiga) {
        this.grlDsRiga = grlDsRiga;
    }

    public long getGrlDsRiga() {
        return this.grlDsRiga;
    }

    public void setGrlDsOperSql(char grlDsOperSql) {
        this.grlDsOperSql = grlDsOperSql;
    }

    public char getGrlDsOperSql() {
        return this.grlDsOperSql;
    }

    public void setGrlDsVer(int grlDsVer) {
        this.grlDsVer = grlDsVer;
    }

    public int getGrlDsVer() {
        return this.grlDsVer;
    }

    public void setGrlDsTsIniCptz(long grlDsTsIniCptz) {
        this.grlDsTsIniCptz = grlDsTsIniCptz;
    }

    public long getGrlDsTsIniCptz() {
        return this.grlDsTsIniCptz;
    }

    public void setGrlDsTsEndCptz(long grlDsTsEndCptz) {
        this.grlDsTsEndCptz = grlDsTsEndCptz;
    }

    public long getGrlDsTsEndCptz() {
        return this.grlDsTsEndCptz;
    }

    public void setGrlDsUtente(String grlDsUtente) {
        this.grlDsUtente = Functions.subString(grlDsUtente, Len.GRL_DS_UTENTE);
    }

    public String getGrlDsUtente() {
        return this.grlDsUtente;
    }

    public void setGrlDsStatoElab(char grlDsStatoElab) {
        this.grlDsStatoElab = grlDsStatoElab;
    }

    public char getGrlDsStatoElab() {
        return this.grlDsStatoElab;
    }

    public GrlDtSin1oAssto getGrlDtSin1oAssto() {
        return grlDtSin1oAssto;
    }

    public GrlDtSin2oAssto getGrlDtSin2oAssto() {
        return grlDtSin2oAssto;
    }

    public GrlDtSin3oAssto getGrlDtSin3oAssto() {
        return grlDtSin3oAssto;
    }

    public GrlIdMoviChiu getGrlIdMoviChiu() {
        return grlIdMoviChiu;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GRL_CAU_SIN1O_ASSTO = 2;
        public static final int GRL_TP_SIN1O_ASSTO = 2;
        public static final int GRL_CAU_SIN2O_ASSTO = 2;
        public static final int GRL_TP_SIN2O_ASSTO = 2;
        public static final int GRL_CAU_SIN3O_ASSTO = 2;
        public static final int GRL_TP_SIN3O_ASSTO = 2;
        public static final int GRL_DS_UTENTE = 20;
        public static final int GRL_ID_GAR_LIQ = 5;
        public static final int GRL_ID_LIQ = 5;
        public static final int GRL_ID_GAR = 5;
        public static final int GRL_ID_MOVI_CRZ = 5;
        public static final int GRL_DT_INI_EFF = 5;
        public static final int GRL_DT_END_EFF = 5;
        public static final int GRL_COD_COMP_ANIA = 3;
        public static final int GRL_DS_RIGA = 6;
        public static final int GRL_DS_OPER_SQL = 1;
        public static final int GRL_DS_VER = 5;
        public static final int GRL_DS_TS_INI_CPTZ = 10;
        public static final int GRL_DS_TS_END_CPTZ = 10;
        public static final int GRL_DS_STATO_ELAB = 1;
        public static final int GAR_LIQ = GRL_ID_GAR_LIQ + GRL_ID_LIQ + GRL_ID_GAR + GRL_ID_MOVI_CRZ + GrlIdMoviChiu.Len.GRL_ID_MOVI_CHIU + GRL_DT_INI_EFF + GRL_DT_END_EFF + GRL_COD_COMP_ANIA + GrlDtSin1oAssto.Len.GRL_DT_SIN1O_ASSTO + GRL_CAU_SIN1O_ASSTO + GRL_TP_SIN1O_ASSTO + GrlDtSin2oAssto.Len.GRL_DT_SIN2O_ASSTO + GRL_CAU_SIN2O_ASSTO + GRL_TP_SIN2O_ASSTO + GrlDtSin3oAssto.Len.GRL_DT_SIN3O_ASSTO + GRL_CAU_SIN3O_ASSTO + GRL_TP_SIN3O_ASSTO + GRL_DS_RIGA + GRL_DS_OPER_SQL + GRL_DS_VER + GRL_DS_TS_INI_CPTZ + GRL_DS_TS_END_CPTZ + GRL_DS_UTENTE + GRL_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRL_ID_GAR_LIQ = 9;
            public static final int GRL_ID_LIQ = 9;
            public static final int GRL_ID_GAR = 9;
            public static final int GRL_ID_MOVI_CRZ = 9;
            public static final int GRL_DT_INI_EFF = 8;
            public static final int GRL_DT_END_EFF = 8;
            public static final int GRL_COD_COMP_ANIA = 5;
            public static final int GRL_DS_RIGA = 10;
            public static final int GRL_DS_VER = 9;
            public static final int GRL_DS_TS_INI_CPTZ = 18;
            public static final int GRL_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
