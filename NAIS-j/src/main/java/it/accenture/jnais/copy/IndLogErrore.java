package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-LOG-ERRORE<br>
 * Variable: IND-LOG-ERRORE from copybook IDBVLOR2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndLogErrore {

    //==== PROPERTIES ====
    //Original name: IND-LOR-LABEL-ERR
    private short labelErr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LOR-OPER-TABELLA
    private short operTabella = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LOR-NOME-TABELLA
    private short nomeTabella = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LOR-STATUS-TABELLA
    private short statusTabella = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LOR-KEY-TABELLA
    private short keyTabella = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LOR-TIPO-OGGETTO
    private short tipoOggetto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LOR-IB-OGGETTO
    private short ibOggetto = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setLabelErr(short labelErr) {
        this.labelErr = labelErr;
    }

    public short getLabelErr() {
        return this.labelErr;
    }

    public void setOperTabella(short operTabella) {
        this.operTabella = operTabella;
    }

    public short getOperTabella() {
        return this.operTabella;
    }

    public void setNomeTabella(short nomeTabella) {
        this.nomeTabella = nomeTabella;
    }

    public short getNomeTabella() {
        return this.nomeTabella;
    }

    public void setStatusTabella(short statusTabella) {
        this.statusTabella = statusTabella;
    }

    public short getStatusTabella() {
        return this.statusTabella;
    }

    public void setKeyTabella(short keyTabella) {
        this.keyTabella = keyTabella;
    }

    public short getKeyTabella() {
        return this.keyTabella;
    }

    public void setTipoOggetto(short tipoOggetto) {
        this.tipoOggetto = tipoOggetto;
    }

    public short getTipoOggetto() {
        return this.tipoOggetto;
    }

    public void setIbOggetto(short ibOggetto) {
        this.ibOggetto = ibOggetto;
    }

    public short getIbOggetto() {
        return this.ibOggetto;
    }
}
