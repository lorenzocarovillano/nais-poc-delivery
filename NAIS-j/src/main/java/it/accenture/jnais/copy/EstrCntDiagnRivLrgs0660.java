package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.P85CommisGest;
import it.accenture.jnais.ws.redefines.P85MinGarto;
import it.accenture.jnais.ws.redefines.P85MinTrnut;
import it.accenture.jnais.ws.redefines.P85PcRetr;
import it.accenture.jnais.ws.redefines.P85RendtoLrd;
import it.accenture.jnais.ws.redefines.P85RendtoRetr;
import it.accenture.jnais.ws.redefines.P85TsRivalNet;

/**Original name: ESTR-CNT-DIAGN-RIV<br>
 * Variable: ESTR-CNT-DIAGN-RIV from copybook IDBVP851<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class EstrCntDiagnRivLrgs0660 {

    //==== PROPERTIES ====
    //Original name: P85-COD-COMP-ANIA
    private int p85CodCompAnia = DefaultValues.INT_VAL;
    //Original name: P85-ID-POLI
    private int p85IdPoli = DefaultValues.INT_VAL;
    //Original name: P85-DT-RIVAL
    private int p85DtRival = DefaultValues.INT_VAL;
    //Original name: P85-COD-TARI
    private String p85CodTari = DefaultValues.stringVal(Len.P85_COD_TARI);
    //Original name: P85-RENDTO-LRD
    private P85RendtoLrd p85RendtoLrd = new P85RendtoLrd();
    //Original name: P85-PC-RETR
    private P85PcRetr p85PcRetr = new P85PcRetr();
    //Original name: P85-RENDTO-RETR
    private P85RendtoRetr p85RendtoRetr = new P85RendtoRetr();
    //Original name: P85-COMMIS-GEST
    private P85CommisGest p85CommisGest = new P85CommisGest();
    //Original name: P85-TS-RIVAL-NET
    private P85TsRivalNet p85TsRivalNet = new P85TsRivalNet();
    //Original name: P85-MIN-GARTO
    private P85MinGarto p85MinGarto = new P85MinGarto();
    //Original name: P85-MIN-TRNUT
    private P85MinTrnut p85MinTrnut = new P85MinTrnut();
    //Original name: P85-IB-POLI
    private String p85IbPoli = DefaultValues.stringVal(Len.P85_IB_POLI);
    //Original name: P85-DS-OPER-SQL
    private char p85DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P85-DS-VER
    private int p85DsVer = DefaultValues.INT_VAL;
    //Original name: P85-DS-TS-CPTZ
    private long p85DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: P85-DS-UTENTE
    private String p85DsUtente = DefaultValues.stringVal(Len.P85_DS_UTENTE);
    //Original name: P85-DS-STATO-ELAB
    private char p85DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setEstrCntDiagnRivFormatted(String data) {
        byte[] buffer = new byte[Len.ESTR_CNT_DIAGN_RIV];
        MarshalByte.writeString(buffer, 1, data, Len.ESTR_CNT_DIAGN_RIV);
        setEstrCntDiagnRivBytes(buffer, 1);
    }

    public void setEstrCntDiagnRivBytes(byte[] buffer, int offset) {
        int position = offset;
        p85CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P85_COD_COMP_ANIA, 0);
        position += Len.P85_COD_COMP_ANIA;
        p85IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P85_ID_POLI, 0);
        position += Len.P85_ID_POLI;
        p85DtRival = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P85_DT_RIVAL, 0);
        position += Len.P85_DT_RIVAL;
        p85CodTari = MarshalByte.readString(buffer, position, Len.P85_COD_TARI);
        position += Len.P85_COD_TARI;
        p85RendtoLrd.setP85RendtoLrdFromBuffer(buffer, position);
        position += P85RendtoLrd.Len.P85_RENDTO_LRD;
        p85PcRetr.setP85PcRetrFromBuffer(buffer, position);
        position += P85PcRetr.Len.P85_PC_RETR;
        p85RendtoRetr.setP85RendtoRetrFromBuffer(buffer, position);
        position += P85RendtoRetr.Len.P85_RENDTO_RETR;
        p85CommisGest.setP85CommisGestFromBuffer(buffer, position);
        position += P85CommisGest.Len.P85_COMMIS_GEST;
        p85TsRivalNet.setP85TsRivalNetFromBuffer(buffer, position);
        position += P85TsRivalNet.Len.P85_TS_RIVAL_NET;
        p85MinGarto.setP85MinGartoFromBuffer(buffer, position);
        position += P85MinGarto.Len.P85_MIN_GARTO;
        p85MinTrnut.setP85MinTrnutFromBuffer(buffer, position);
        position += P85MinTrnut.Len.P85_MIN_TRNUT;
        p85IbPoli = MarshalByte.readString(buffer, position, Len.P85_IB_POLI);
        position += Len.P85_IB_POLI;
        p85DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p85DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P85_DS_VER, 0);
        position += Len.P85_DS_VER;
        p85DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P85_DS_TS_CPTZ, 0);
        position += Len.P85_DS_TS_CPTZ;
        p85DsUtente = MarshalByte.readString(buffer, position, Len.P85_DS_UTENTE);
        position += Len.P85_DS_UTENTE;
        p85DsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public void setP85CodCompAnia(int p85CodCompAnia) {
        this.p85CodCompAnia = p85CodCompAnia;
    }

    public int getP85CodCompAnia() {
        return this.p85CodCompAnia;
    }

    public void setP85IdPoli(int p85IdPoli) {
        this.p85IdPoli = p85IdPoli;
    }

    public int getP85IdPoli() {
        return this.p85IdPoli;
    }

    public void setP85DtRival(int p85DtRival) {
        this.p85DtRival = p85DtRival;
    }

    public int getP85DtRival() {
        return this.p85DtRival;
    }

    public void setP85CodTari(String p85CodTari) {
        this.p85CodTari = Functions.subString(p85CodTari, Len.P85_COD_TARI);
    }

    public String getP85CodTari() {
        return this.p85CodTari;
    }

    public void setP85IbPoli(String p85IbPoli) {
        this.p85IbPoli = Functions.subString(p85IbPoli, Len.P85_IB_POLI);
    }

    public String getP85IbPoli() {
        return this.p85IbPoli;
    }

    public void setP85DsOperSql(char p85DsOperSql) {
        this.p85DsOperSql = p85DsOperSql;
    }

    public char getP85DsOperSql() {
        return this.p85DsOperSql;
    }

    public void setP85DsVer(int p85DsVer) {
        this.p85DsVer = p85DsVer;
    }

    public int getP85DsVer() {
        return this.p85DsVer;
    }

    public void setP85DsTsCptz(long p85DsTsCptz) {
        this.p85DsTsCptz = p85DsTsCptz;
    }

    public long getP85DsTsCptz() {
        return this.p85DsTsCptz;
    }

    public void setP85DsUtente(String p85DsUtente) {
        this.p85DsUtente = Functions.subString(p85DsUtente, Len.P85_DS_UTENTE);
    }

    public String getP85DsUtente() {
        return this.p85DsUtente;
    }

    public void setP85DsStatoElab(char p85DsStatoElab) {
        this.p85DsStatoElab = p85DsStatoElab;
    }

    public char getP85DsStatoElab() {
        return this.p85DsStatoElab;
    }

    public P85CommisGest getP85CommisGest() {
        return p85CommisGest;
    }

    public P85MinGarto getP85MinGarto() {
        return p85MinGarto;
    }

    public P85MinTrnut getP85MinTrnut() {
        return p85MinTrnut;
    }

    public P85PcRetr getP85PcRetr() {
        return p85PcRetr;
    }

    public P85RendtoLrd getP85RendtoLrd() {
        return p85RendtoLrd;
    }

    public P85RendtoRetr getP85RendtoRetr() {
        return p85RendtoRetr;
    }

    public P85TsRivalNet getP85TsRivalNet() {
        return p85TsRivalNet;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P85_COD_TARI = 12;
        public static final int P85_IB_POLI = 40;
        public static final int P85_DS_UTENTE = 20;
        public static final int P85_COD_COMP_ANIA = 3;
        public static final int P85_ID_POLI = 5;
        public static final int P85_DT_RIVAL = 5;
        public static final int P85_DS_OPER_SQL = 1;
        public static final int P85_DS_VER = 5;
        public static final int P85_DS_TS_CPTZ = 10;
        public static final int P85_DS_STATO_ELAB = 1;
        public static final int ESTR_CNT_DIAGN_RIV = P85_COD_COMP_ANIA + P85_ID_POLI + P85_DT_RIVAL + P85_COD_TARI + P85RendtoLrd.Len.P85_RENDTO_LRD + P85PcRetr.Len.P85_PC_RETR + P85RendtoRetr.Len.P85_RENDTO_RETR + P85CommisGest.Len.P85_COMMIS_GEST + P85TsRivalNet.Len.P85_TS_RIVAL_NET + P85MinGarto.Len.P85_MIN_GARTO + P85MinTrnut.Len.P85_MIN_TRNUT + P85_IB_POLI + P85_DS_OPER_SQL + P85_DS_VER + P85_DS_TS_CPTZ + P85_DS_UTENTE + P85_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P85_COD_COMP_ANIA = 5;
            public static final int P85_ID_POLI = 9;
            public static final int P85_DT_RIVAL = 8;
            public static final int P85_DS_VER = 9;
            public static final int P85_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
