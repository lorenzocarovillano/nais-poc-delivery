package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WbepCodBnfic;
import it.accenture.jnais.ws.redefines.WbepIdBnficr;
import it.accenture.jnais.ws.redefines.WbepIdMoviChiu;
import it.accenture.jnais.ws.redefines.WbepPcDelBnficr;

/**Original name: WBEP-DATI<br>
 * Variable: WBEP-DATI from copybook LCCVBEP1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WbepDati {

    //==== PROPERTIES ====
    //Original name: WBEP-ID-BNFIC
    private int wbepIdBnfic = DefaultValues.INT_VAL;
    //Original name: WBEP-ID-RAPP-ANA
    private int wbepIdRappAna = DefaultValues.INT_VAL;
    //Original name: WBEP-ID-BNFICR
    private WbepIdBnficr wbepIdBnficr = new WbepIdBnficr();
    //Original name: WBEP-ID-MOVI-CRZ
    private int wbepIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WBEP-ID-MOVI-CHIU
    private WbepIdMoviChiu wbepIdMoviChiu = new WbepIdMoviChiu();
    //Original name: WBEP-DT-INI-EFF
    private int wbepDtIniEff = DefaultValues.INT_VAL;
    //Original name: WBEP-DT-END-EFF
    private int wbepDtEndEff = DefaultValues.INT_VAL;
    //Original name: WBEP-COD-COMP-ANIA
    private int wbepCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WBEP-COD-BNFIC
    private WbepCodBnfic wbepCodBnfic = new WbepCodBnfic();
    //Original name: WBEP-TP-IND-BNFICR
    private String wbepTpIndBnficr = DefaultValues.stringVal(Len.WBEP_TP_IND_BNFICR);
    //Original name: WBEP-COD-BNFICR
    private String wbepCodBnficr = DefaultValues.stringVal(Len.WBEP_COD_BNFICR);
    //Original name: WBEP-DESC-BNFICR
    private String wbepDescBnficr = DefaultValues.stringVal(Len.WBEP_DESC_BNFICR);
    //Original name: WBEP-PC-DEL-BNFICR
    private WbepPcDelBnficr wbepPcDelBnficr = new WbepPcDelBnficr();
    //Original name: WBEP-FL-ESE
    private char wbepFlEse = DefaultValues.CHAR_VAL;
    //Original name: WBEP-FL-IRREV
    private char wbepFlIrrev = DefaultValues.CHAR_VAL;
    //Original name: WBEP-FL-DFLT
    private char wbepFlDflt = DefaultValues.CHAR_VAL;
    //Original name: WBEP-ESRCN-ATTVT-IMPRS
    private char wbepEsrcnAttvtImprs = DefaultValues.CHAR_VAL;
    //Original name: WBEP-FL-BNFICR-COLL
    private char wbepFlBnficrColl = DefaultValues.CHAR_VAL;
    //Original name: WBEP-DS-RIGA
    private long wbepDsRiga = DefaultValues.LONG_VAL;
    //Original name: WBEP-DS-OPER-SQL
    private char wbepDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WBEP-DS-VER
    private int wbepDsVer = DefaultValues.INT_VAL;
    //Original name: WBEP-DS-TS-INI-CPTZ
    private long wbepDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WBEP-DS-TS-END-CPTZ
    private long wbepDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WBEP-DS-UTENTE
    private String wbepDsUtente = DefaultValues.stringVal(Len.WBEP_DS_UTENTE);
    //Original name: WBEP-DS-STATO-ELAB
    private char wbepDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WBEP-TP-NORMAL-BNFIC
    private String wbepTpNormalBnfic = DefaultValues.stringVal(Len.WBEP_TP_NORMAL_BNFIC);

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wbepIdBnfic = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEP_ID_BNFIC, 0);
        position += Len.WBEP_ID_BNFIC;
        wbepIdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEP_ID_RAPP_ANA, 0);
        position += Len.WBEP_ID_RAPP_ANA;
        wbepIdBnficr.setWbepIdBnficrFromBuffer(buffer, position);
        position += WbepIdBnficr.Len.WBEP_ID_BNFICR;
        wbepIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEP_ID_MOVI_CRZ, 0);
        position += Len.WBEP_ID_MOVI_CRZ;
        wbepIdMoviChiu.setWbepIdMoviChiuFromBuffer(buffer, position);
        position += WbepIdMoviChiu.Len.WBEP_ID_MOVI_CHIU;
        wbepDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEP_DT_INI_EFF, 0);
        position += Len.WBEP_DT_INI_EFF;
        wbepDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEP_DT_END_EFF, 0);
        position += Len.WBEP_DT_END_EFF;
        wbepCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEP_COD_COMP_ANIA, 0);
        position += Len.WBEP_COD_COMP_ANIA;
        wbepCodBnfic.setWbepCodBnficFromBuffer(buffer, position);
        position += WbepCodBnfic.Len.WBEP_COD_BNFIC;
        wbepTpIndBnficr = MarshalByte.readString(buffer, position, Len.WBEP_TP_IND_BNFICR);
        position += Len.WBEP_TP_IND_BNFICR;
        wbepCodBnficr = MarshalByte.readString(buffer, position, Len.WBEP_COD_BNFICR);
        position += Len.WBEP_COD_BNFICR;
        wbepDescBnficr = MarshalByte.readString(buffer, position, Len.WBEP_DESC_BNFICR);
        position += Len.WBEP_DESC_BNFICR;
        wbepPcDelBnficr.setWbepPcDelBnficrFromBuffer(buffer, position);
        position += WbepPcDelBnficr.Len.WBEP_PC_DEL_BNFICR;
        wbepFlEse = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbepFlIrrev = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbepFlDflt = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbepEsrcnAttvtImprs = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbepFlBnficrColl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbepDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WBEP_DS_RIGA, 0);
        position += Len.WBEP_DS_RIGA;
        wbepDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbepDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEP_DS_VER, 0);
        position += Len.WBEP_DS_VER;
        wbepDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WBEP_DS_TS_INI_CPTZ, 0);
        position += Len.WBEP_DS_TS_INI_CPTZ;
        wbepDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WBEP_DS_TS_END_CPTZ, 0);
        position += Len.WBEP_DS_TS_END_CPTZ;
        wbepDsUtente = MarshalByte.readString(buffer, position, Len.WBEP_DS_UTENTE);
        position += Len.WBEP_DS_UTENTE;
        wbepDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbepTpNormalBnfic = MarshalByte.readString(buffer, position, Len.WBEP_TP_NORMAL_BNFIC);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wbepIdBnfic, Len.Int.WBEP_ID_BNFIC, 0);
        position += Len.WBEP_ID_BNFIC;
        MarshalByte.writeIntAsPacked(buffer, position, wbepIdRappAna, Len.Int.WBEP_ID_RAPP_ANA, 0);
        position += Len.WBEP_ID_RAPP_ANA;
        wbepIdBnficr.getWbepIdBnficrAsBuffer(buffer, position);
        position += WbepIdBnficr.Len.WBEP_ID_BNFICR;
        MarshalByte.writeIntAsPacked(buffer, position, wbepIdMoviCrz, Len.Int.WBEP_ID_MOVI_CRZ, 0);
        position += Len.WBEP_ID_MOVI_CRZ;
        wbepIdMoviChiu.getWbepIdMoviChiuAsBuffer(buffer, position);
        position += WbepIdMoviChiu.Len.WBEP_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wbepDtIniEff, Len.Int.WBEP_DT_INI_EFF, 0);
        position += Len.WBEP_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wbepDtEndEff, Len.Int.WBEP_DT_END_EFF, 0);
        position += Len.WBEP_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wbepCodCompAnia, Len.Int.WBEP_COD_COMP_ANIA, 0);
        position += Len.WBEP_COD_COMP_ANIA;
        wbepCodBnfic.getWbepCodBnficAsBuffer(buffer, position);
        position += WbepCodBnfic.Len.WBEP_COD_BNFIC;
        MarshalByte.writeString(buffer, position, wbepTpIndBnficr, Len.WBEP_TP_IND_BNFICR);
        position += Len.WBEP_TP_IND_BNFICR;
        MarshalByte.writeString(buffer, position, wbepCodBnficr, Len.WBEP_COD_BNFICR);
        position += Len.WBEP_COD_BNFICR;
        MarshalByte.writeString(buffer, position, wbepDescBnficr, Len.WBEP_DESC_BNFICR);
        position += Len.WBEP_DESC_BNFICR;
        wbepPcDelBnficr.getWbepPcDelBnficrAsBuffer(buffer, position);
        position += WbepPcDelBnficr.Len.WBEP_PC_DEL_BNFICR;
        MarshalByte.writeChar(buffer, position, wbepFlEse);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wbepFlIrrev);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wbepFlDflt);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wbepEsrcnAttvtImprs);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wbepFlBnficrColl);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, wbepDsRiga, Len.Int.WBEP_DS_RIGA, 0);
        position += Len.WBEP_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wbepDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wbepDsVer, Len.Int.WBEP_DS_VER, 0);
        position += Len.WBEP_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wbepDsTsIniCptz, Len.Int.WBEP_DS_TS_INI_CPTZ, 0);
        position += Len.WBEP_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wbepDsTsEndCptz, Len.Int.WBEP_DS_TS_END_CPTZ, 0);
        position += Len.WBEP_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wbepDsUtente, Len.WBEP_DS_UTENTE);
        position += Len.WBEP_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wbepDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wbepTpNormalBnfic, Len.WBEP_TP_NORMAL_BNFIC);
        return buffer;
    }

    public void initDatiSpaces() {
        wbepIdBnfic = Types.INVALID_INT_VAL;
        wbepIdRappAna = Types.INVALID_INT_VAL;
        wbepIdBnficr.initWbepIdBnficrSpaces();
        wbepIdMoviCrz = Types.INVALID_INT_VAL;
        wbepIdMoviChiu.initWbepIdMoviChiuSpaces();
        wbepDtIniEff = Types.INVALID_INT_VAL;
        wbepDtEndEff = Types.INVALID_INT_VAL;
        wbepCodCompAnia = Types.INVALID_INT_VAL;
        wbepCodBnfic.initWbepCodBnficSpaces();
        wbepTpIndBnficr = "";
        wbepCodBnficr = "";
        wbepDescBnficr = "";
        wbepPcDelBnficr.initWbepPcDelBnficrSpaces();
        wbepFlEse = Types.SPACE_CHAR;
        wbepFlIrrev = Types.SPACE_CHAR;
        wbepFlDflt = Types.SPACE_CHAR;
        wbepEsrcnAttvtImprs = Types.SPACE_CHAR;
        wbepFlBnficrColl = Types.SPACE_CHAR;
        wbepDsRiga = Types.INVALID_LONG_VAL;
        wbepDsOperSql = Types.SPACE_CHAR;
        wbepDsVer = Types.INVALID_INT_VAL;
        wbepDsTsIniCptz = Types.INVALID_LONG_VAL;
        wbepDsTsEndCptz = Types.INVALID_LONG_VAL;
        wbepDsUtente = "";
        wbepDsStatoElab = Types.SPACE_CHAR;
        wbepTpNormalBnfic = "";
    }

    public void setWbepIdBnfic(int wbepIdBnfic) {
        this.wbepIdBnfic = wbepIdBnfic;
    }

    public int getWbepIdBnfic() {
        return this.wbepIdBnfic;
    }

    public void setWbepIdRappAna(int wbepIdRappAna) {
        this.wbepIdRappAna = wbepIdRappAna;
    }

    public int getWbepIdRappAna() {
        return this.wbepIdRappAna;
    }

    public void setWbepIdMoviCrz(int wbepIdMoviCrz) {
        this.wbepIdMoviCrz = wbepIdMoviCrz;
    }

    public int getWbepIdMoviCrz() {
        return this.wbepIdMoviCrz;
    }

    public void setWbepDtIniEff(int wbepDtIniEff) {
        this.wbepDtIniEff = wbepDtIniEff;
    }

    public int getWbepDtIniEff() {
        return this.wbepDtIniEff;
    }

    public void setWbepDtEndEff(int wbepDtEndEff) {
        this.wbepDtEndEff = wbepDtEndEff;
    }

    public int getWbepDtEndEff() {
        return this.wbepDtEndEff;
    }

    public void setWbepCodCompAnia(int wbepCodCompAnia) {
        this.wbepCodCompAnia = wbepCodCompAnia;
    }

    public int getWbepCodCompAnia() {
        return this.wbepCodCompAnia;
    }

    public void setWbepTpIndBnficr(String wbepTpIndBnficr) {
        this.wbepTpIndBnficr = Functions.subString(wbepTpIndBnficr, Len.WBEP_TP_IND_BNFICR);
    }

    public String getWbepTpIndBnficr() {
        return this.wbepTpIndBnficr;
    }

    public void setWbepCodBnficr(String wbepCodBnficr) {
        this.wbepCodBnficr = Functions.subString(wbepCodBnficr, Len.WBEP_COD_BNFICR);
    }

    public String getWbepCodBnficr() {
        return this.wbepCodBnficr;
    }

    public void setWbepDescBnficr(String wbepDescBnficr) {
        this.wbepDescBnficr = Functions.subString(wbepDescBnficr, Len.WBEP_DESC_BNFICR);
    }

    public String getWbepDescBnficr() {
        return this.wbepDescBnficr;
    }

    public void setWbepFlEse(char wbepFlEse) {
        this.wbepFlEse = wbepFlEse;
    }

    public char getWbepFlEse() {
        return this.wbepFlEse;
    }

    public void setWbepFlIrrev(char wbepFlIrrev) {
        this.wbepFlIrrev = wbepFlIrrev;
    }

    public char getWbepFlIrrev() {
        return this.wbepFlIrrev;
    }

    public void setWbepFlDflt(char wbepFlDflt) {
        this.wbepFlDflt = wbepFlDflt;
    }

    public char getWbepFlDflt() {
        return this.wbepFlDflt;
    }

    public void setWbepEsrcnAttvtImprs(char wbepEsrcnAttvtImprs) {
        this.wbepEsrcnAttvtImprs = wbepEsrcnAttvtImprs;
    }

    public char getWbepEsrcnAttvtImprs() {
        return this.wbepEsrcnAttvtImprs;
    }

    public void setWbepFlBnficrColl(char wbepFlBnficrColl) {
        this.wbepFlBnficrColl = wbepFlBnficrColl;
    }

    public char getWbepFlBnficrColl() {
        return this.wbepFlBnficrColl;
    }

    public void setWbepDsRiga(long wbepDsRiga) {
        this.wbepDsRiga = wbepDsRiga;
    }

    public long getWbepDsRiga() {
        return this.wbepDsRiga;
    }

    public void setWbepDsOperSql(char wbepDsOperSql) {
        this.wbepDsOperSql = wbepDsOperSql;
    }

    public char getWbepDsOperSql() {
        return this.wbepDsOperSql;
    }

    public void setWbepDsVer(int wbepDsVer) {
        this.wbepDsVer = wbepDsVer;
    }

    public int getWbepDsVer() {
        return this.wbepDsVer;
    }

    public void setWbepDsTsIniCptz(long wbepDsTsIniCptz) {
        this.wbepDsTsIniCptz = wbepDsTsIniCptz;
    }

    public long getWbepDsTsIniCptz() {
        return this.wbepDsTsIniCptz;
    }

    public void setWbepDsTsEndCptz(long wbepDsTsEndCptz) {
        this.wbepDsTsEndCptz = wbepDsTsEndCptz;
    }

    public long getWbepDsTsEndCptz() {
        return this.wbepDsTsEndCptz;
    }

    public void setWbepDsUtente(String wbepDsUtente) {
        this.wbepDsUtente = Functions.subString(wbepDsUtente, Len.WBEP_DS_UTENTE);
    }

    public String getWbepDsUtente() {
        return this.wbepDsUtente;
    }

    public void setWbepDsStatoElab(char wbepDsStatoElab) {
        this.wbepDsStatoElab = wbepDsStatoElab;
    }

    public char getWbepDsStatoElab() {
        return this.wbepDsStatoElab;
    }

    public void setWbepTpNormalBnfic(String wbepTpNormalBnfic) {
        this.wbepTpNormalBnfic = Functions.subString(wbepTpNormalBnfic, Len.WBEP_TP_NORMAL_BNFIC);
    }

    public String getWbepTpNormalBnfic() {
        return this.wbepTpNormalBnfic;
    }

    public WbepCodBnfic getWbepCodBnfic() {
        return wbepCodBnfic;
    }

    public WbepIdBnficr getWbepIdBnficr() {
        return wbepIdBnficr;
    }

    public WbepIdMoviChiu getWbepIdMoviChiu() {
        return wbepIdMoviChiu;
    }

    public WbepPcDelBnficr getWbepPcDelBnficr() {
        return wbepPcDelBnficr;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEP_ID_BNFIC = 5;
        public static final int WBEP_ID_RAPP_ANA = 5;
        public static final int WBEP_ID_MOVI_CRZ = 5;
        public static final int WBEP_DT_INI_EFF = 5;
        public static final int WBEP_DT_END_EFF = 5;
        public static final int WBEP_COD_COMP_ANIA = 3;
        public static final int WBEP_TP_IND_BNFICR = 2;
        public static final int WBEP_COD_BNFICR = 20;
        public static final int WBEP_DESC_BNFICR = 250;
        public static final int WBEP_FL_ESE = 1;
        public static final int WBEP_FL_IRREV = 1;
        public static final int WBEP_FL_DFLT = 1;
        public static final int WBEP_ESRCN_ATTVT_IMPRS = 1;
        public static final int WBEP_FL_BNFICR_COLL = 1;
        public static final int WBEP_DS_RIGA = 6;
        public static final int WBEP_DS_OPER_SQL = 1;
        public static final int WBEP_DS_VER = 5;
        public static final int WBEP_DS_TS_INI_CPTZ = 10;
        public static final int WBEP_DS_TS_END_CPTZ = 10;
        public static final int WBEP_DS_UTENTE = 20;
        public static final int WBEP_DS_STATO_ELAB = 1;
        public static final int WBEP_TP_NORMAL_BNFIC = 2;
        public static final int DATI = WBEP_ID_BNFIC + WBEP_ID_RAPP_ANA + WbepIdBnficr.Len.WBEP_ID_BNFICR + WBEP_ID_MOVI_CRZ + WbepIdMoviChiu.Len.WBEP_ID_MOVI_CHIU + WBEP_DT_INI_EFF + WBEP_DT_END_EFF + WBEP_COD_COMP_ANIA + WbepCodBnfic.Len.WBEP_COD_BNFIC + WBEP_TP_IND_BNFICR + WBEP_COD_BNFICR + WBEP_DESC_BNFICR + WbepPcDelBnficr.Len.WBEP_PC_DEL_BNFICR + WBEP_FL_ESE + WBEP_FL_IRREV + WBEP_FL_DFLT + WBEP_ESRCN_ATTVT_IMPRS + WBEP_FL_BNFICR_COLL + WBEP_DS_RIGA + WBEP_DS_OPER_SQL + WBEP_DS_VER + WBEP_DS_TS_INI_CPTZ + WBEP_DS_TS_END_CPTZ + WBEP_DS_UTENTE + WBEP_DS_STATO_ELAB + WBEP_TP_NORMAL_BNFIC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEP_ID_BNFIC = 9;
            public static final int WBEP_ID_RAPP_ANA = 9;
            public static final int WBEP_ID_MOVI_CRZ = 9;
            public static final int WBEP_DT_INI_EFF = 8;
            public static final int WBEP_DT_END_EFF = 8;
            public static final int WBEP_COD_COMP_ANIA = 5;
            public static final int WBEP_DS_RIGA = 10;
            public static final int WBEP_DS_VER = 9;
            public static final int WBEP_DS_TS_INI_CPTZ = 18;
            public static final int WBEP_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
