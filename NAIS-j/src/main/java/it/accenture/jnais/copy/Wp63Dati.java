package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP63-DATI<br>
 * Variable: WP63-DATI from copybook LCCVP631<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wp63Dati {

    //==== PROPERTIES ====
    //Original name: WP63-ID-ACC-COMM
    private int idAccComm = DefaultValues.INT_VAL;
    //Original name: WP63-TP-ACC-COMM
    private String tpAccComm = DefaultValues.stringVal(Len.TP_ACC_COMM);
    //Original name: WP63-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: WP63-COD-PARTNER
    private long codPartner = DefaultValues.LONG_VAL;
    //Original name: WP63-IB-ACC-COMM
    private String ibAccComm = DefaultValues.stringVal(Len.IB_ACC_COMM);
    //Original name: WP63-IB-ACC-COMM-MASTER
    private String ibAccCommMaster = DefaultValues.stringVal(Len.IB_ACC_COMM_MASTER);
    //Original name: WP63-DESC-ACC-COMM
    private String descAccComm = DefaultValues.stringVal(Len.DESC_ACC_COMM);
    //Original name: WP63-DT-FIRMA
    private int dtFirma = DefaultValues.INT_VAL;
    //Original name: WP63-DT-INI-VLDT
    private int dtIniVldt = DefaultValues.INT_VAL;
    //Original name: WP63-DT-END-VLDT
    private int dtEndVldt = DefaultValues.INT_VAL;
    //Original name: WP63-FL-INVIO-CONFERME
    private char flInvioConferme = DefaultValues.CHAR_VAL;
    //Original name: WP63-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WP63-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: WP63-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WP63-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: WP63-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WP63-DESC-ACC-COMM-MAST
    private String descAccCommMast = DefaultValues.stringVal(Len.DESC_ACC_COMM_MAST);

    //==== METHODS ====
    public void setIdAccComm(int idAccComm) {
        this.idAccComm = idAccComm;
    }

    public int getIdAccComm() {
        return this.idAccComm;
    }

    public void setTpAccComm(String tpAccComm) {
        this.tpAccComm = Functions.subString(tpAccComm, Len.TP_ACC_COMM);
    }

    public String getTpAccComm() {
        return this.tpAccComm;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setCodPartner(long codPartner) {
        this.codPartner = codPartner;
    }

    public long getCodPartner() {
        return this.codPartner;
    }

    public void setIbAccComm(String ibAccComm) {
        this.ibAccComm = Functions.subString(ibAccComm, Len.IB_ACC_COMM);
    }

    public String getIbAccComm() {
        return this.ibAccComm;
    }

    public void setIbAccCommMaster(String ibAccCommMaster) {
        this.ibAccCommMaster = Functions.subString(ibAccCommMaster, Len.IB_ACC_COMM_MASTER);
    }

    public String getIbAccCommMaster() {
        return this.ibAccCommMaster;
    }

    public void setDescAccComm(String descAccComm) {
        this.descAccComm = Functions.subString(descAccComm, Len.DESC_ACC_COMM);
    }

    public String getDescAccComm() {
        return this.descAccComm;
    }

    public void setDtFirma(int dtFirma) {
        this.dtFirma = dtFirma;
    }

    public int getDtFirma() {
        return this.dtFirma;
    }

    public void setDtIniVldt(int dtIniVldt) {
        this.dtIniVldt = dtIniVldt;
    }

    public int getDtIniVldt() {
        return this.dtIniVldt;
    }

    public void setDtEndVldt(int dtEndVldt) {
        this.dtEndVldt = dtEndVldt;
    }

    public int getDtEndVldt() {
        return this.dtEndVldt;
    }

    public void setFlInvioConferme(char flInvioConferme) {
        this.flInvioConferme = flInvioConferme;
    }

    public char getFlInvioConferme() {
        return this.flInvioConferme;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setDescAccCommMast(String descAccCommMast) {
        this.descAccCommMast = Functions.subString(descAccCommMast, Len.DESC_ACC_COMM_MAST);
    }

    public String getDescAccCommMast() {
        return this.descAccCommMast;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_ACC_COMM = 2;
        public static final int IB_ACC_COMM = 40;
        public static final int IB_ACC_COMM_MASTER = 40;
        public static final int DESC_ACC_COMM = 100;
        public static final int DS_UTENTE = 20;
        public static final int DESC_ACC_COMM_MAST = 100;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
