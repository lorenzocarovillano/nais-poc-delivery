package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.S0024ModificaDtdecor;
import it.accenture.jnais.ws.enums.S0024TpVisualizPag;
import it.accenture.jnais.ws.enums.WcomNavigabilita;
import it.accenture.jnais.ws.enums.WcomTipoOperazione;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.occurs.S0024TabMovAnnull;
import it.accenture.jnais.ws.occurs.WcomTabPlatfond;

/**Original name: LCCC0001<br>
 * Variable: LCCC0001 from copybook LCCC0001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccc0001Loas0110 {

    //==== PROPERTIES ====
    public static final int TAB_MOV_ANNULL_MAXOCCURS = 10;
    public static final int TAB_PLATFOND_MAXOCCURS = 3;
    /**Original name: S0024-TIPO-OPERAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *      COPY COMUNE DI INTERFACCIA  BACK-END --> FRONT-END
	 *      LUNGHEZZA COMPLESSIVA : 5000 BYTES
	 * ----------------------------------------------------------------*</pre>*/
    private WcomTipoOperazione tipoOperazione = new WcomTipoOperazione();
    //Original name: S0024-NAVIGABILITA
    private WcomNavigabilita navigabilita = new WcomNavigabilita();
    //Original name: S0024-TASTI-DA-ABILITARE
    private WcomTastiDaAbilitare tastiDaAbilitare = new WcomTastiDaAbilitare();
    //Original name: S0024-DT-ULT-VERS-PROD
    private String dtUltVersProd = DefaultValues.stringVal(Len.DT_ULT_VERS_PROD);
    //Original name: S0024-STATI
    private WcomStati stati = new WcomStati();
    //Original name: S0024-DATI-DEROGHE
    private WcomDatiDeroghe datiDeroghe = new WcomDatiDeroghe();
    //Original name: S0024-ELE-MOV-ANNULL-MAX
    private short eleMovAnnullMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: S0024-TAB-MOV-ANNULL
    private S0024TabMovAnnull[] tabMovAnnull = new S0024TabMovAnnull[TAB_MOV_ANNULL_MAXOCCURS];
    //Original name: S0024-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: S0024-FLAG-TARIFFA-RISCHIO
    private char flagTariffaRischio = DefaultValues.CHAR_VAL;
    //Original name: S0024-ELE-MAX-PLATFOND
    private short eleMaxPlatfond = DefaultValues.BIN_SHORT_VAL;
    //Original name: S0024-TAB-PLATFOND
    private WcomTabPlatfond[] tabPlatfond = new WcomTabPlatfond[TAB_PLATFOND_MAXOCCURS];
    //Original name: S0024-MODIFICA-DTDECOR
    private S0024ModificaDtdecor modificaDtdecor = new S0024ModificaDtdecor();
    //Original name: S0024-STATUS-DER
    private WpolStatus statusDer = new WpolStatus();
    //Original name: S0024-ID-RICH-EST
    private int idRichEst = DefaultValues.INT_VAL;
    //Original name: S0024-CODICE-INIZIATIVA
    private String codiceIniziativa = DefaultValues.stringVal(Len.CODICE_INIZIATIVA);
    //Original name: S0024-TP-VISUALIZ-PAG
    private S0024TpVisualizPag tpVisualizPag = new S0024TpVisualizPag();
    //Original name: FILLER-S0024-AREA-STATI
    private String flr1 = DefaultValues.stringVal(Len.FLR1);

    //==== CONSTRUCTORS ====
    public Lccc0001Loas0110() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabMovAnnullIdx = 1; tabMovAnnullIdx <= TAB_MOV_ANNULL_MAXOCCURS; tabMovAnnullIdx++) {
            tabMovAnnull[tabMovAnnullIdx - 1] = new S0024TabMovAnnull();
        }
        for (int tabPlatfondIdx = 1; tabPlatfondIdx <= TAB_PLATFOND_MAXOCCURS; tabPlatfondIdx++) {
            tabPlatfond[tabPlatfondIdx - 1] = new WcomTabPlatfond();
        }
    }

    public void setDatiActuatorBytes(byte[] buffer, int offset) {
        int position = offset;
        dtUltVersProd = MarshalByte.readFixedString(buffer, position, Len.DT_ULT_VERS_PROD);
    }

    public byte[] getDatiActuatorBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, dtUltVersProd, Len.DT_ULT_VERS_PROD);
        return buffer;
    }

    public void setMovimentiAnnullBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMovAnnullMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_MOV_ANNULL_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabMovAnnull[idx - 1].setTabMovAnnullBytes(buffer, position);
                position += S0024TabMovAnnull.Len.TAB_MOV_ANNULL;
            }
            else {
                tabMovAnnull[idx - 1].initTabMovAnnullSpaces();
                position += S0024TabMovAnnull.Len.TAB_MOV_ANNULL;
            }
        }
    }

    public byte[] getMovimentiAnnullBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleMovAnnullMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_MOV_ANNULL_MAXOCCURS; idx++) {
            tabMovAnnull[idx - 1].getTabMovAnnullBytes(buffer, position);
            position += S0024TabMovAnnull.Len.TAB_MOV_ANNULL;
        }
        return buffer;
    }

    public void setEleMovAnnullMax(short eleMovAnnullMax) {
        this.eleMovAnnullMax = eleMovAnnullMax;
    }

    public short getEleMovAnnullMax() {
        return this.eleMovAnnullMax;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setFlagTariffaRischio(char flagTariffaRischio) {
        this.flagTariffaRischio = flagTariffaRischio;
    }

    public char getFlagTariffaRischio() {
        return this.flagTariffaRischio;
    }

    public void setAreaPlatfondBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxPlatfond = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_PLATFOND_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabPlatfond[idx - 1].setTabPlatfondBytes(buffer, position);
                position += WcomTabPlatfond.Len.TAB_PLATFOND;
            }
            else {
                tabPlatfond[idx - 1].initTabPlatfondSpaces();
                position += WcomTabPlatfond.Len.TAB_PLATFOND;
            }
        }
    }

    public byte[] getAreaPlatfondBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleMaxPlatfond);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_PLATFOND_MAXOCCURS; idx++) {
            tabPlatfond[idx - 1].getTabPlatfondBytes(buffer, position);
            position += WcomTabPlatfond.Len.TAB_PLATFOND;
        }
        return buffer;
    }

    public void setEleMaxPlatfond(short eleMaxPlatfond) {
        this.eleMaxPlatfond = eleMaxPlatfond;
    }

    public short getEleMaxPlatfond() {
        return this.eleMaxPlatfond;
    }

    public void setIdRichEst(int idRichEst) {
        this.idRichEst = idRichEst;
    }

    public int getIdRichEst() {
        return this.idRichEst;
    }

    public void setCodiceIniziativa(String codiceIniziativa) {
        this.codiceIniziativa = Functions.subString(codiceIniziativa, Len.CODICE_INIZIATIVA);
    }

    public String getCodiceIniziativa() {
        return this.codiceIniziativa;
    }

    public void setFlr1(String flr1) {
        this.flr1 = Functions.subString(flr1, Len.FLR1);
    }

    public String getFlr1() {
        return this.flr1;
    }

    public WcomDatiDeroghe getDatiDeroghe() {
        return datiDeroghe;
    }

    public S0024ModificaDtdecor getModificaDtdecor() {
        return modificaDtdecor;
    }

    public WcomNavigabilita getNavigabilita() {
        return navigabilita;
    }

    public WcomStati getStati() {
        return stati;
    }

    public WpolStatus getStatusDer() {
        return statusDer;
    }

    public WcomTastiDaAbilitare getTastiDaAbilitare() {
        return tastiDaAbilitare;
    }

    public WcomTipoOperazione getTipoOperazione() {
        return tipoOperazione;
    }

    public S0024TpVisualizPag getTpVisualizPag() {
        return tpVisualizPag;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_ULT_VERS_PROD = 8;
        public static final int DATI_ACTUATOR = DT_ULT_VERS_PROD;
        public static final int ELE_MOV_ANNULL_MAX = 2;
        public static final int MOVIMENTI_ANNULL = ELE_MOV_ANNULL_MAX + Lccc0001Loas0110.TAB_MOV_ANNULL_MAXOCCURS * S0024TabMovAnnull.Len.TAB_MOV_ANNULL;
        public static final int ID_MOVI_CRZ = 5;
        public static final int FLAG_TARIFFA_RISCHIO = 1;
        public static final int ELE_MAX_PLATFOND = 2;
        public static final int AREA_PLATFOND = ELE_MAX_PLATFOND + Lccc0001Loas0110.TAB_PLATFOND_MAXOCCURS * WcomTabPlatfond.Len.TAB_PLATFOND;
        public static final int ID_RICH_EST = 5;
        public static final int CODICE_INIZIATIVA = 12;
        public static final int FLR1 = 135;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_RICH_EST = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
