package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.WcomTabMotDeroga;
import it.accenture.jnais.ws.occurs.WcomVariabiliCondWf;

/**Original name: WCOM-DATI-DEROGHE<br>
 * Variable: WCOM-DATI-DEROGHE from copybook LCCC0001<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WcomDatiDeroghe {

    //==== PROPERTIES ====
    public static final int VARIABILI_COND_WF_MAXOCCURS = 20;
    public static final int TAB_MOT_DEROGA_MAXOCCURS = 20;
    //Original name: WCOM-KEY-AUT-OPER1
    private String keyAutOper1 = DefaultValues.stringVal(Len.KEY_AUT_OPER1);
    //Original name: WCOM-KEY-AUT-OPER2
    private String keyAutOper2 = DefaultValues.stringVal(Len.KEY_AUT_OPER2);
    //Original name: WCOM-KEY-AUT-OPER3
    private String keyAutOper3 = DefaultValues.stringVal(Len.KEY_AUT_OPER3);
    //Original name: WCOM-KEY-AUT-OPER4
    private String keyAutOper4 = DefaultValues.stringVal(Len.KEY_AUT_OPER4);
    //Original name: WCOM-KEY-AUT-OPER5
    private String keyAutOper5 = DefaultValues.stringVal(Len.KEY_AUT_OPER5);
    //Original name: WCOM-COD-PROCESSO-WF
    private String codProcessoWf = DefaultValues.stringVal(Len.COD_PROCESSO_WF);
    //Original name: WCOM-COD-ATTIVITA-WF
    private String codAttivitaWf = DefaultValues.stringVal(Len.COD_ATTIVITA_WF);
    //Original name: WCOM-NUM-ELE-VARIABILI-COND
    private String numEleVariabiliCond = DefaultValues.stringVal(Len.NUM_ELE_VARIABILI_COND);
    //Original name: WCOM-VARIABILI-COND-WF
    private WcomVariabiliCondWf[] variabiliCondWf = new WcomVariabiliCondWf[VARIABILI_COND_WF_MAXOCCURS];
    //Original name: WCOM-COD-LIV-AUT-PROFIL
    private int codLivAutProfil = DefaultValues.INT_VAL;
    //Original name: WCOM-COD-GRU-AUT-PROFIL
    private long codGruAutProfil = DefaultValues.LONG_VAL;
    //Original name: WCOM-CANALE-VENDITA
    private int canaleVendita = DefaultValues.INT_VAL;
    //Original name: WCOM-PUNTO-VENDITA
    private String puntoVendita = DefaultValues.stringVal(Len.PUNTO_VENDITA);
    //Original name: WCOM-OGGETTO-DEROGA
    private WcomOggettoDeroga oggettoDeroga = new WcomOggettoDeroga();
    //Original name: WCOM-NUM-ELE-MOT-DEROGA
    private short numEleMotDeroga = DefaultValues.SHORT_VAL;
    //Original name: WCOM-TAB-MOT-DEROGA
    private WcomTabMotDeroga[] tabMotDeroga = new WcomTabMotDeroga[TAB_MOT_DEROGA_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WcomDatiDeroghe() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int variabiliCondWfIdx = 1; variabiliCondWfIdx <= VARIABILI_COND_WF_MAXOCCURS; variabiliCondWfIdx++) {
            variabiliCondWf[variabiliCondWfIdx - 1] = new WcomVariabiliCondWf();
        }
        for (int tabMotDerogaIdx = 1; tabMotDerogaIdx <= TAB_MOT_DEROGA_MAXOCCURS; tabMotDerogaIdx++) {
            tabMotDeroga[tabMotDerogaIdx - 1] = new WcomTabMotDeroga();
        }
    }

    public void setDatiDerogheBytes(byte[] buffer, int offset) {
        int position = offset;
        keyAutOper1 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER1);
        position += Len.KEY_AUT_OPER1;
        keyAutOper2 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER2);
        position += Len.KEY_AUT_OPER2;
        keyAutOper3 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER3);
        position += Len.KEY_AUT_OPER3;
        keyAutOper4 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER4);
        position += Len.KEY_AUT_OPER4;
        keyAutOper5 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER5);
        position += Len.KEY_AUT_OPER5;
        codProcessoWf = MarshalByte.readString(buffer, position, Len.COD_PROCESSO_WF);
        position += Len.COD_PROCESSO_WF;
        codAttivitaWf = MarshalByte.readString(buffer, position, Len.COD_ATTIVITA_WF);
        position += Len.COD_ATTIVITA_WF;
        numEleVariabiliCond = MarshalByte.readFixedString(buffer, position, Len.NUM_ELE_VARIABILI_COND);
        position += Len.NUM_ELE_VARIABILI_COND;
        for (int idx = 1; idx <= VARIABILI_COND_WF_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                variabiliCondWf[idx - 1].setVariabiliCondWfBytes(buffer, position);
                position += WcomVariabiliCondWf.Len.VARIABILI_COND_WF;
            }
            else {
                variabiliCondWf[idx - 1].initVariabiliCondWfSpaces();
                position += WcomVariabiliCondWf.Len.VARIABILI_COND_WF;
            }
        }
        codLivAutProfil = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_LIV_AUT_PROFIL, 0);
        position += Len.COD_LIV_AUT_PROFIL;
        codGruAutProfil = MarshalByte.readPackedAsLong(buffer, position, Len.Int.COD_GRU_AUT_PROFIL, 0);
        position += Len.COD_GRU_AUT_PROFIL;
        canaleVendita = MarshalByte.readPackedAsInt(buffer, position, Len.Int.CANALE_VENDITA, 0);
        position += Len.CANALE_VENDITA;
        puntoVendita = MarshalByte.readString(buffer, position, Len.PUNTO_VENDITA);
        position += Len.PUNTO_VENDITA;
        oggettoDeroga.setOggettoDerogaBytes(buffer, position);
        position += WcomOggettoDeroga.Len.OGGETTO_DEROGA;
        setMotiviDerogaBytes(buffer, position);
    }

    public byte[] getDatiDerogheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, keyAutOper1, Len.KEY_AUT_OPER1);
        position += Len.KEY_AUT_OPER1;
        MarshalByte.writeString(buffer, position, keyAutOper2, Len.KEY_AUT_OPER2);
        position += Len.KEY_AUT_OPER2;
        MarshalByte.writeString(buffer, position, keyAutOper3, Len.KEY_AUT_OPER3);
        position += Len.KEY_AUT_OPER3;
        MarshalByte.writeString(buffer, position, keyAutOper4, Len.KEY_AUT_OPER4);
        position += Len.KEY_AUT_OPER4;
        MarshalByte.writeString(buffer, position, keyAutOper5, Len.KEY_AUT_OPER5);
        position += Len.KEY_AUT_OPER5;
        MarshalByte.writeString(buffer, position, codProcessoWf, Len.COD_PROCESSO_WF);
        position += Len.COD_PROCESSO_WF;
        MarshalByte.writeString(buffer, position, codAttivitaWf, Len.COD_ATTIVITA_WF);
        position += Len.COD_ATTIVITA_WF;
        MarshalByte.writeString(buffer, position, numEleVariabiliCond, Len.NUM_ELE_VARIABILI_COND);
        position += Len.NUM_ELE_VARIABILI_COND;
        for (int idx = 1; idx <= VARIABILI_COND_WF_MAXOCCURS; idx++) {
            variabiliCondWf[idx - 1].getVariabiliCondWfBytes(buffer, position);
            position += WcomVariabiliCondWf.Len.VARIABILI_COND_WF;
        }
        MarshalByte.writeIntAsPacked(buffer, position, codLivAutProfil, Len.Int.COD_LIV_AUT_PROFIL, 0);
        position += Len.COD_LIV_AUT_PROFIL;
        MarshalByte.writeLongAsPacked(buffer, position, codGruAutProfil, Len.Int.COD_GRU_AUT_PROFIL, 0);
        position += Len.COD_GRU_AUT_PROFIL;
        MarshalByte.writeIntAsPacked(buffer, position, canaleVendita, Len.Int.CANALE_VENDITA, 0);
        position += Len.CANALE_VENDITA;
        MarshalByte.writeString(buffer, position, puntoVendita, Len.PUNTO_VENDITA);
        position += Len.PUNTO_VENDITA;
        oggettoDeroga.getOggettoDerogaBytes(buffer, position);
        position += WcomOggettoDeroga.Len.OGGETTO_DEROGA;
        getMotiviDerogaBytes(buffer, position);
        return buffer;
    }

    public void setKeyAutOper1(String keyAutOper1) {
        this.keyAutOper1 = Functions.subString(keyAutOper1, Len.KEY_AUT_OPER1);
    }

    public String getKeyAutOper1() {
        return this.keyAutOper1;
    }

    public void setKeyAutOper2(String keyAutOper2) {
        this.keyAutOper2 = Functions.subString(keyAutOper2, Len.KEY_AUT_OPER2);
    }

    public String getKeyAutOper2() {
        return this.keyAutOper2;
    }

    public void setKeyAutOper3(String keyAutOper3) {
        this.keyAutOper3 = Functions.subString(keyAutOper3, Len.KEY_AUT_OPER3);
    }

    public String getKeyAutOper3() {
        return this.keyAutOper3;
    }

    public void setKeyAutOper4(String keyAutOper4) {
        this.keyAutOper4 = Functions.subString(keyAutOper4, Len.KEY_AUT_OPER4);
    }

    public String getKeyAutOper4() {
        return this.keyAutOper4;
    }

    public void setKeyAutOper5(String keyAutOper5) {
        this.keyAutOper5 = Functions.subString(keyAutOper5, Len.KEY_AUT_OPER5);
    }

    public String getKeyAutOper5() {
        return this.keyAutOper5;
    }

    public void setCodProcessoWf(String codProcessoWf) {
        this.codProcessoWf = Functions.subString(codProcessoWf, Len.COD_PROCESSO_WF);
    }

    public String getCodProcessoWf() {
        return this.codProcessoWf;
    }

    public void setCodAttivitaWf(String codAttivitaWf) {
        this.codAttivitaWf = Functions.subString(codAttivitaWf, Len.COD_ATTIVITA_WF);
    }

    public String getCodAttivitaWf() {
        return this.codAttivitaWf;
    }

    public void setWcomNumEleVariabiliCondFormatted(String wcomNumEleVariabiliCond) {
        this.numEleVariabiliCond = Trunc.toUnsignedNumeric(wcomNumEleVariabiliCond, Len.NUM_ELE_VARIABILI_COND);
    }

    public short getWcomNumEleVariabiliCond() {
        return NumericDisplay.asShort(this.numEleVariabiliCond);
    }

    public void setCodLivAutProfil(int codLivAutProfil) {
        this.codLivAutProfil = codLivAutProfil;
    }

    public int getCodLivAutProfil() {
        return this.codLivAutProfil;
    }

    public void setCodGruAutProfil(long codGruAutProfil) {
        this.codGruAutProfil = codGruAutProfil;
    }

    public long getCodGruAutProfil() {
        return this.codGruAutProfil;
    }

    public void setCanaleVendita(int canaleVendita) {
        this.canaleVendita = canaleVendita;
    }

    public int getCanaleVendita() {
        return this.canaleVendita;
    }

    public void setPuntoVendita(String puntoVendita) {
        this.puntoVendita = Functions.subString(puntoVendita, Len.PUNTO_VENDITA);
    }

    public String getPuntoVendita() {
        return this.puntoVendita;
    }

    public void setMotiviDerogaBytes(byte[] buffer, int offset) {
        int position = offset;
        numEleMotDeroga = MarshalByte.readShort(buffer, position, Len.NUM_ELE_MOT_DEROGA, SignType.NO_SIGN);
        position += Len.NUM_ELE_MOT_DEROGA;
        for (int idx = 1; idx <= TAB_MOT_DEROGA_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabMotDeroga[idx - 1].setTabMotDerogaBytes(buffer, position);
                position += WcomTabMotDeroga.Len.TAB_MOT_DEROGA;
            }
            else {
                tabMotDeroga[idx - 1].initTabMotDerogaSpaces();
                position += WcomTabMotDeroga.Len.TAB_MOT_DEROGA;
            }
        }
    }

    public byte[] getMotiviDerogaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeShort(buffer, position, numEleMotDeroga, Len.NUM_ELE_MOT_DEROGA, SignType.NO_SIGN);
        position += Len.NUM_ELE_MOT_DEROGA;
        for (int idx = 1; idx <= TAB_MOT_DEROGA_MAXOCCURS; idx++) {
            tabMotDeroga[idx - 1].getTabMotDerogaBytes(buffer, position);
            position += WcomTabMotDeroga.Len.TAB_MOT_DEROGA;
        }
        return buffer;
    }

    public void setNumEleMotDeroga(short numEleMotDeroga) {
        this.numEleMotDeroga = numEleMotDeroga;
    }

    public short getNumEleMotDeroga() {
        return this.numEleMotDeroga;
    }

    public WcomOggettoDeroga getOggettoDeroga() {
        return oggettoDeroga;
    }

    public WcomTabMotDeroga getTabMotDeroga(int idx) {
        return tabMotDeroga[idx - 1];
    }

    public WcomVariabiliCondWf getVariabiliCondWf(int idx) {
        return variabiliCondWf[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int KEY_AUT_OPER1 = 20;
        public static final int KEY_AUT_OPER2 = 20;
        public static final int KEY_AUT_OPER3 = 20;
        public static final int KEY_AUT_OPER4 = 20;
        public static final int KEY_AUT_OPER5 = 20;
        public static final int COD_PROCESSO_WF = 3;
        public static final int COD_ATTIVITA_WF = 10;
        public static final int NUM_ELE_VARIABILI_COND = 3;
        public static final int COD_LIV_AUT_PROFIL = 3;
        public static final int COD_GRU_AUT_PROFIL = 6;
        public static final int CANALE_VENDITA = 3;
        public static final int PUNTO_VENDITA = 6;
        public static final int NUM_ELE_MOT_DEROGA = 3;
        public static final int MOTIVI_DEROGA = NUM_ELE_MOT_DEROGA + WcomDatiDeroghe.TAB_MOT_DEROGA_MAXOCCURS * WcomTabMotDeroga.Len.TAB_MOT_DEROGA;
        public static final int DATI_DEROGHE = KEY_AUT_OPER1 + KEY_AUT_OPER2 + KEY_AUT_OPER3 + KEY_AUT_OPER4 + KEY_AUT_OPER5 + COD_PROCESSO_WF + COD_ATTIVITA_WF + NUM_ELE_VARIABILI_COND + WcomDatiDeroghe.VARIABILI_COND_WF_MAXOCCURS * WcomVariabiliCondWf.Len.VARIABILI_COND_WF + COD_LIV_AUT_PROFIL + COD_GRU_AUT_PROFIL + CANALE_VENDITA + PUNTO_VENDITA + WcomOggettoDeroga.Len.OGGETTO_DEROGA + MOTIVI_DEROGA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_LIV_AUT_PROFIL = 5;
            public static final int COD_GRU_AUT_PROFIL = 10;
            public static final int CANALE_VENDITA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
