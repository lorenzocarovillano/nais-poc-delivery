package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-BILA-TRCH-ESTR<br>
 * Variable: IND-BILA-TRCH-ESTR from copybook IDBVB032<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndBilaTrchEstr {

    //==== PROPERTIES ====
    //Original name: IND-B03-ID-RICH-ESTRAZ-AGG
    private short idRichEstrazAgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-FL-SIMULAZIONE
    private short flSimulazione = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-INI-VAL-TAR
    private short dtIniValTar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-PROD
    private short codProd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-TARI-ORGN
    private short codTariOrgn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-MIN-GARTO-T
    private short minGartoT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-TARI
    private short tpTari = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-PRE
    private short tpPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-ADEG-PRE
    private short tpAdegPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-RIVAL
    private short tpRival = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-FL-DA-TRASF
    private short flDaTrasf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-FL-CAR-CONT
    private short flCarCont = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-FL-PRE-DA-RIS
    private short flPreDaRis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-FL-PRE-AGG
    private short flPreAgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-TRCH
    private short tpTrch = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-TST
    private short tpTst = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-CONV
    private short codConv = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-DECOR-ADES
    private short dtDecorAdes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-EMIS-TRCH
    private short dtEmisTrch = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-SCAD-TRCH
    private short dtScadTrch = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-SCAD-INTMD
    private short dtScadIntmd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-SCAD-PAG-PRE
    private short dtScadPagPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-ULT-PRE-PAG
    private short dtUltPrePag = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-NASC-1O-ASSTO
    private short dtNasc1oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-SEX-1O-ASSTO
    private short sex1oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ETA-AA-1O-ASSTO
    private short etaAa1oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ETA-MM-1O-ASSTO
    private short etaMm1oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ETA-RAGGN-DT-CALC
    private short etaRaggnDtCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-AA
    private short durAa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-MM
    private short durMm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-GG
    private short durGg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-1O-PER-AA
    private short dur1oPerAa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-1O-PER-MM
    private short dur1oPerMm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-1O-PER-GG
    private short dur1oPerGg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ANTIDUR-RICOR-PREC
    private short antidurRicorPrec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ANTIDUR-DT-CALC
    private short antidurDtCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-RES-DT-CALC
    private short durResDtCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-EFF-CAMB-STAT
    private short dtEffCambStat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-EMIS-CAMB-STAT
    private short dtEmisCambStat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-EFF-STAB
    private short dtEffStab = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CPT-DT-STAB
    private short cptDtStab = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-EFF-RIDZ
    private short dtEffRidz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-EMIS-RIDZ
    private short dtEmisRidz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CPT-DT-RIDZ
    private short cptDtRidz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-FRAZ
    private short fraz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-PAG-PRE
    private short durPagPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-NUM-PRE-PATT
    private short numPrePatt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-FRAZ-INI-EROG-REN
    private short frazIniErogRen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-AA-REN-CER
    private short aaRenCer = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-RAT-REN
    private short ratRen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-DIV
    private short codDiv = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-RISCPAR
    private short riscpar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CUM-RISCPAR
    private short cumRiscpar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ULT-RM
    private short ultRm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TS-RENDTO-T
    private short tsRendtoT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ALQ-RETR-T
    private short alqRetrT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-MIN-TRNUT-T
    private short minTrnutT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TS-NET-T
    private short tsNetT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-ULT-RIVAL
    private short dtUltRival = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRSTZ-INI
    private short prstzIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRSTZ-AGG-INI
    private short prstzAggIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRSTZ-AGG-ULT
    private short prstzAggUlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-RAPPEL
    private short rappel = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRE-PATTUITO-INI
    private short prePattuitoIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRE-DOV-INI
    private short preDovIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRE-DOV-RIVTO-T
    private short preDovRivtoT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRE-ANNUALIZ-RICOR
    private short preAnnualizRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRE-CONT
    private short preCont = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRE-PP-INI
    private short prePpIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-RIS-PURA-T
    private short risPuraT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PROV-ACQ
    private short provAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PROV-ACQ-RICOR
    private short provAcqRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PROV-INC
    private short provInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CAR-ACQ-NON-SCON
    private short carAcqNonScon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-OVER-COMM
    private short overComm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CAR-ACQ-PRECONTATO
    private short carAcqPrecontato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-RIS-ACQ-T
    private short risAcqT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-RIS-ZIL-T
    private short risZilT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CAR-GEST-NON-SCON
    private short carGestNonScon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CAR-GEST
    private short carGest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-RIS-SPE-T
    private short risSpeT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CAR-INC-NON-SCON
    private short carIncNonScon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CAR-INC
    private short carInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-RIS-RISTORNI-CAP
    private short risRistorniCap = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-INTR-TECN
    private short intrTecn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CPT-RSH-MOR
    private short cptRshMor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-C-SUBRSH-T
    private short cSubrshT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRE-RSH-T
    private short preRshT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ALQ-MARG-RIS
    private short alqMargRis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ALQ-MARG-C-SUBRSH
    private short alqMargCSubrsh = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TS-RENDTO-SPPR
    private short tsRendtoSppr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-IAS
    private short tpIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-NS-QUO
    private short nsQuo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TS-MEDIO
    private short tsMedio = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CPT-RIASTO
    private short cptRiasto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRE-RIASTO
    private short preRiasto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-RIS-RIASTA
    private short risRiasta = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CPT-RIASTO-ECC
    private short cptRiastoEcc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRE-RIASTO-ECC
    private short preRiastoEcc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-RIS-RIASTA-ECC
    private short risRiastaEcc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-AGE
    private short codAge = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-SUBAGE
    private short codSubage = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-CAN
    private short codCan = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-IB-POLI
    private short ibPoli = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-IB-ADES
    private short ibAdes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-IB-TRCH-DI-GAR
    private short ibTrchDiGar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-PRSTZ
    private short tpPrstz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-TRASF
    private short tpTrasf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PP-INVRIO-TARI
    private short ppInvrioTari = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COEFF-OPZ-REN
    private short coeffOpzRen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COEFF-OPZ-CPT
    private short coeffOpzCpt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-PAG-REN
    private short durPagRen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-VLT
    private short vlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-RIS-MAT-CHIU-PREC
    private short risMatChiuPrec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-FND
    private short codFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRSTZ-T
    private short prstzT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TS-TARI-DOV
    private short tsTariDov = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TS-TARI-SCON
    private short tsTariScon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TS-PP
    private short tsPp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COEFF-RIS-1-T
    private short coeffRis1T = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COEFF-RIS-2-T
    private short coeffRis2T = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ABB
    private short abb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-COASS
    private short tpCoass = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TRAT-RIASS
    private short tratRiass = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TRAT-RIASS-ECC
    private short tratRiassEcc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-RGM-FISC
    private short tpRgmFisc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-GAR-AA
    private short durGarAa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-GAR-MM
    private short durGarMm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DUR-GAR-GG
    private short durGarGg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ANTIDUR-CALC-365
    private short antidurCalc365 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-FISC-CNTR
    private short codFiscCntr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-FISC-ASSTO1
    private short codFiscAssto1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-FISC-ASSTO2
    private short codFiscAssto2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-FISC-ASSTO3
    private short codFiscAssto3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CAUS-SCON
    private short causScon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-EMIT-TIT-OPZ
    private short emitTitOpz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-QTZ-SP-Z-COUP-EMIS
    private short qtzSpZCoupEmis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-QTZ-SP-Z-OPZ-EMIS
    private short qtzSpZOpzEmis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-QTZ-SP-Z-COUP-DT-C
    private short qtzSpZCoupDtC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-QTZ-SP-Z-OPZ-DT-CA
    private short qtzSpZOpzDtCa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-QTZ-TOT-EMIS
    private short qtzTotEmis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-QTZ-TOT-DT-CALC
    private short qtzTotDtCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-QTZ-TOT-DT-ULT-BIL
    private short qtzTotDtUltBil = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-QTZ-EMIS
    private short dtQtzEmis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PC-CAR-GEST
    private short pcCarGest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PC-CAR-ACQ
    private short pcCarAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-IMP-CAR-CASO-MOR
    private short impCarCasoMor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PC-CAR-MOR
    private short pcCarMor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-VERS
    private short tpVers = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-FL-SWITCH
    private short flSwitch = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-FL-IAS
    private short flIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DIR
    private short dir = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-COP-CASO-MOR
    private short tpCopCasoMor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-MET-RISC-SPCL
    private short metRiscSpcl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-STAT-INVST
    private short tpStatInvst = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COD-PRDT
    private short codPrdt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-STAT-ASSTO-1
    private short statAssto1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-STAT-ASSTO-2
    private short statAssto2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-STAT-ASSTO-3
    private short statAssto3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CPT-ASSTO-INI-MOR
    private short cptAsstoIniMor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TS-STAB-PRE
    private short tsStabPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DIR-EMIS
    private short dirEmis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-DT-INC-ULT-PRE
    private short dtIncUltPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-STAT-TBGC-ASSTO-1
    private short statTbgcAssto1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-STAT-TBGC-ASSTO-2
    private short statTbgcAssto2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-STAT-TBGC-ASSTO-3
    private short statTbgcAssto3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-FRAZ-DECR-CPT
    private short frazDecrCpt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-PRE-PP-ULT
    private short prePpUlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-ACQ-EXP
    private short acqExp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-REMUN-ASS
    private short remunAss = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-COMMIS-INTER
    private short commisInter = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-NUM-FINANZ
    private short numFinanz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-TP-ACC-COMM
    private short tpAccComm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-IB-ACC-COMM
    private short ibAccComm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-B03-CARZ
    private short carz = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdRichEstrazAgg(short idRichEstrazAgg) {
        this.idRichEstrazAgg = idRichEstrazAgg;
    }

    public short getIdRichEstrazAgg() {
        return this.idRichEstrazAgg;
    }

    public void setFlSimulazione(short flSimulazione) {
        this.flSimulazione = flSimulazione;
    }

    public short getFlSimulazione() {
        return this.flSimulazione;
    }

    public void setDtIniValTar(short dtIniValTar) {
        this.dtIniValTar = dtIniValTar;
    }

    public short getDtIniValTar() {
        return this.dtIniValTar;
    }

    public void setCodProd(short codProd) {
        this.codProd = codProd;
    }

    public short getCodProd() {
        return this.codProd;
    }

    public void setCodTariOrgn(short codTariOrgn) {
        this.codTariOrgn = codTariOrgn;
    }

    public short getCodTariOrgn() {
        return this.codTariOrgn;
    }

    public void setMinGartoT(short minGartoT) {
        this.minGartoT = minGartoT;
    }

    public short getMinGartoT() {
        return this.minGartoT;
    }

    public void setTpTari(short tpTari) {
        this.tpTari = tpTari;
    }

    public short getTpTari() {
        return this.tpTari;
    }

    public void setTpPre(short tpPre) {
        this.tpPre = tpPre;
    }

    public short getTpPre() {
        return this.tpPre;
    }

    public void setTpAdegPre(short tpAdegPre) {
        this.tpAdegPre = tpAdegPre;
    }

    public short getTpAdegPre() {
        return this.tpAdegPre;
    }

    public void setTpRival(short tpRival) {
        this.tpRival = tpRival;
    }

    public short getTpRival() {
        return this.tpRival;
    }

    public void setFlDaTrasf(short flDaTrasf) {
        this.flDaTrasf = flDaTrasf;
    }

    public short getFlDaTrasf() {
        return this.flDaTrasf;
    }

    public void setFlCarCont(short flCarCont) {
        this.flCarCont = flCarCont;
    }

    public short getFlCarCont() {
        return this.flCarCont;
    }

    public void setFlPreDaRis(short flPreDaRis) {
        this.flPreDaRis = flPreDaRis;
    }

    public short getFlPreDaRis() {
        return this.flPreDaRis;
    }

    public void setFlPreAgg(short flPreAgg) {
        this.flPreAgg = flPreAgg;
    }

    public short getFlPreAgg() {
        return this.flPreAgg;
    }

    public void setTpTrch(short tpTrch) {
        this.tpTrch = tpTrch;
    }

    public short getTpTrch() {
        return this.tpTrch;
    }

    public void setTpTst(short tpTst) {
        this.tpTst = tpTst;
    }

    public short getTpTst() {
        return this.tpTst;
    }

    public void setCodConv(short codConv) {
        this.codConv = codConv;
    }

    public short getCodConv() {
        return this.codConv;
    }

    public void setDtDecorAdes(short dtDecorAdes) {
        this.dtDecorAdes = dtDecorAdes;
    }

    public short getDtDecorAdes() {
        return this.dtDecorAdes;
    }

    public void setDtEmisTrch(short dtEmisTrch) {
        this.dtEmisTrch = dtEmisTrch;
    }

    public short getDtEmisTrch() {
        return this.dtEmisTrch;
    }

    public void setDtScadTrch(short dtScadTrch) {
        this.dtScadTrch = dtScadTrch;
    }

    public short getDtScadTrch() {
        return this.dtScadTrch;
    }

    public void setDtScadIntmd(short dtScadIntmd) {
        this.dtScadIntmd = dtScadIntmd;
    }

    public short getDtScadIntmd() {
        return this.dtScadIntmd;
    }

    public void setDtScadPagPre(short dtScadPagPre) {
        this.dtScadPagPre = dtScadPagPre;
    }

    public short getDtScadPagPre() {
        return this.dtScadPagPre;
    }

    public void setDtUltPrePag(short dtUltPrePag) {
        this.dtUltPrePag = dtUltPrePag;
    }

    public short getDtUltPrePag() {
        return this.dtUltPrePag;
    }

    public void setDtNasc1oAssto(short dtNasc1oAssto) {
        this.dtNasc1oAssto = dtNasc1oAssto;
    }

    public short getDtNasc1oAssto() {
        return this.dtNasc1oAssto;
    }

    public void setSex1oAssto(short sex1oAssto) {
        this.sex1oAssto = sex1oAssto;
    }

    public short getSex1oAssto() {
        return this.sex1oAssto;
    }

    public void setEtaAa1oAssto(short etaAa1oAssto) {
        this.etaAa1oAssto = etaAa1oAssto;
    }

    public short getEtaAa1oAssto() {
        return this.etaAa1oAssto;
    }

    public void setEtaMm1oAssto(short etaMm1oAssto) {
        this.etaMm1oAssto = etaMm1oAssto;
    }

    public short getEtaMm1oAssto() {
        return this.etaMm1oAssto;
    }

    public void setEtaRaggnDtCalc(short etaRaggnDtCalc) {
        this.etaRaggnDtCalc = etaRaggnDtCalc;
    }

    public short getEtaRaggnDtCalc() {
        return this.etaRaggnDtCalc;
    }

    public void setDurAa(short durAa) {
        this.durAa = durAa;
    }

    public short getDurAa() {
        return this.durAa;
    }

    public void setDurMm(short durMm) {
        this.durMm = durMm;
    }

    public short getDurMm() {
        return this.durMm;
    }

    public void setDurGg(short durGg) {
        this.durGg = durGg;
    }

    public short getDurGg() {
        return this.durGg;
    }

    public void setDur1oPerAa(short dur1oPerAa) {
        this.dur1oPerAa = dur1oPerAa;
    }

    public short getDur1oPerAa() {
        return this.dur1oPerAa;
    }

    public void setDur1oPerMm(short dur1oPerMm) {
        this.dur1oPerMm = dur1oPerMm;
    }

    public short getDur1oPerMm() {
        return this.dur1oPerMm;
    }

    public void setDur1oPerGg(short dur1oPerGg) {
        this.dur1oPerGg = dur1oPerGg;
    }

    public short getDur1oPerGg() {
        return this.dur1oPerGg;
    }

    public void setAntidurRicorPrec(short antidurRicorPrec) {
        this.antidurRicorPrec = antidurRicorPrec;
    }

    public short getAntidurRicorPrec() {
        return this.antidurRicorPrec;
    }

    public void setAntidurDtCalc(short antidurDtCalc) {
        this.antidurDtCalc = antidurDtCalc;
    }

    public short getAntidurDtCalc() {
        return this.antidurDtCalc;
    }

    public void setDurResDtCalc(short durResDtCalc) {
        this.durResDtCalc = durResDtCalc;
    }

    public short getDurResDtCalc() {
        return this.durResDtCalc;
    }

    public void setDtEffCambStat(short dtEffCambStat) {
        this.dtEffCambStat = dtEffCambStat;
    }

    public short getDtEffCambStat() {
        return this.dtEffCambStat;
    }

    public void setDtEmisCambStat(short dtEmisCambStat) {
        this.dtEmisCambStat = dtEmisCambStat;
    }

    public short getDtEmisCambStat() {
        return this.dtEmisCambStat;
    }

    public void setDtEffStab(short dtEffStab) {
        this.dtEffStab = dtEffStab;
    }

    public short getDtEffStab() {
        return this.dtEffStab;
    }

    public void setCptDtStab(short cptDtStab) {
        this.cptDtStab = cptDtStab;
    }

    public short getCptDtStab() {
        return this.cptDtStab;
    }

    public void setDtEffRidz(short dtEffRidz) {
        this.dtEffRidz = dtEffRidz;
    }

    public short getDtEffRidz() {
        return this.dtEffRidz;
    }

    public void setDtEmisRidz(short dtEmisRidz) {
        this.dtEmisRidz = dtEmisRidz;
    }

    public short getDtEmisRidz() {
        return this.dtEmisRidz;
    }

    public void setCptDtRidz(short cptDtRidz) {
        this.cptDtRidz = cptDtRidz;
    }

    public short getCptDtRidz() {
        return this.cptDtRidz;
    }

    public void setFraz(short fraz) {
        this.fraz = fraz;
    }

    public short getFraz() {
        return this.fraz;
    }

    public void setDurPagPre(short durPagPre) {
        this.durPagPre = durPagPre;
    }

    public short getDurPagPre() {
        return this.durPagPre;
    }

    public void setNumPrePatt(short numPrePatt) {
        this.numPrePatt = numPrePatt;
    }

    public short getNumPrePatt() {
        return this.numPrePatt;
    }

    public void setFrazIniErogRen(short frazIniErogRen) {
        this.frazIniErogRen = frazIniErogRen;
    }

    public short getFrazIniErogRen() {
        return this.frazIniErogRen;
    }

    public void setAaRenCer(short aaRenCer) {
        this.aaRenCer = aaRenCer;
    }

    public short getAaRenCer() {
        return this.aaRenCer;
    }

    public void setRatRen(short ratRen) {
        this.ratRen = ratRen;
    }

    public short getRatRen() {
        return this.ratRen;
    }

    public void setCodDiv(short codDiv) {
        this.codDiv = codDiv;
    }

    public short getCodDiv() {
        return this.codDiv;
    }

    public void setRiscpar(short riscpar) {
        this.riscpar = riscpar;
    }

    public short getRiscpar() {
        return this.riscpar;
    }

    public void setCumRiscpar(short cumRiscpar) {
        this.cumRiscpar = cumRiscpar;
    }

    public short getCumRiscpar() {
        return this.cumRiscpar;
    }

    public void setUltRm(short ultRm) {
        this.ultRm = ultRm;
    }

    public short getUltRm() {
        return this.ultRm;
    }

    public void setTsRendtoT(short tsRendtoT) {
        this.tsRendtoT = tsRendtoT;
    }

    public short getTsRendtoT() {
        return this.tsRendtoT;
    }

    public void setAlqRetrT(short alqRetrT) {
        this.alqRetrT = alqRetrT;
    }

    public short getAlqRetrT() {
        return this.alqRetrT;
    }

    public void setMinTrnutT(short minTrnutT) {
        this.minTrnutT = minTrnutT;
    }

    public short getMinTrnutT() {
        return this.minTrnutT;
    }

    public void setTsNetT(short tsNetT) {
        this.tsNetT = tsNetT;
    }

    public short getTsNetT() {
        return this.tsNetT;
    }

    public void setDtUltRival(short dtUltRival) {
        this.dtUltRival = dtUltRival;
    }

    public short getDtUltRival() {
        return this.dtUltRival;
    }

    public void setPrstzIni(short prstzIni) {
        this.prstzIni = prstzIni;
    }

    public short getPrstzIni() {
        return this.prstzIni;
    }

    public void setPrstzAggIni(short prstzAggIni) {
        this.prstzAggIni = prstzAggIni;
    }

    public short getPrstzAggIni() {
        return this.prstzAggIni;
    }

    public void setPrstzAggUlt(short prstzAggUlt) {
        this.prstzAggUlt = prstzAggUlt;
    }

    public short getPrstzAggUlt() {
        return this.prstzAggUlt;
    }

    public void setRappel(short rappel) {
        this.rappel = rappel;
    }

    public short getRappel() {
        return this.rappel;
    }

    public void setPrePattuitoIni(short prePattuitoIni) {
        this.prePattuitoIni = prePattuitoIni;
    }

    public short getPrePattuitoIni() {
        return this.prePattuitoIni;
    }

    public void setPreDovIni(short preDovIni) {
        this.preDovIni = preDovIni;
    }

    public short getPreDovIni() {
        return this.preDovIni;
    }

    public void setPreDovRivtoT(short preDovRivtoT) {
        this.preDovRivtoT = preDovRivtoT;
    }

    public short getPreDovRivtoT() {
        return this.preDovRivtoT;
    }

    public void setPreAnnualizRicor(short preAnnualizRicor) {
        this.preAnnualizRicor = preAnnualizRicor;
    }

    public short getPreAnnualizRicor() {
        return this.preAnnualizRicor;
    }

    public void setPreCont(short preCont) {
        this.preCont = preCont;
    }

    public short getPreCont() {
        return this.preCont;
    }

    public void setPrePpIni(short prePpIni) {
        this.prePpIni = prePpIni;
    }

    public short getPrePpIni() {
        return this.prePpIni;
    }

    public void setRisPuraT(short risPuraT) {
        this.risPuraT = risPuraT;
    }

    public short getRisPuraT() {
        return this.risPuraT;
    }

    public void setProvAcq(short provAcq) {
        this.provAcq = provAcq;
    }

    public short getProvAcq() {
        return this.provAcq;
    }

    public void setProvAcqRicor(short provAcqRicor) {
        this.provAcqRicor = provAcqRicor;
    }

    public short getProvAcqRicor() {
        return this.provAcqRicor;
    }

    public void setProvInc(short provInc) {
        this.provInc = provInc;
    }

    public short getProvInc() {
        return this.provInc;
    }

    public void setCarAcqNonScon(short carAcqNonScon) {
        this.carAcqNonScon = carAcqNonScon;
    }

    public short getCarAcqNonScon() {
        return this.carAcqNonScon;
    }

    public void setOverComm(short overComm) {
        this.overComm = overComm;
    }

    public short getOverComm() {
        return this.overComm;
    }

    public void setCarAcqPrecontato(short carAcqPrecontato) {
        this.carAcqPrecontato = carAcqPrecontato;
    }

    public short getCarAcqPrecontato() {
        return this.carAcqPrecontato;
    }

    public void setRisAcqT(short risAcqT) {
        this.risAcqT = risAcqT;
    }

    public short getRisAcqT() {
        return this.risAcqT;
    }

    public void setRisZilT(short risZilT) {
        this.risZilT = risZilT;
    }

    public short getRisZilT() {
        return this.risZilT;
    }

    public void setCarGestNonScon(short carGestNonScon) {
        this.carGestNonScon = carGestNonScon;
    }

    public short getCarGestNonScon() {
        return this.carGestNonScon;
    }

    public void setCarGest(short carGest) {
        this.carGest = carGest;
    }

    public short getCarGest() {
        return this.carGest;
    }

    public void setRisSpeT(short risSpeT) {
        this.risSpeT = risSpeT;
    }

    public short getRisSpeT() {
        return this.risSpeT;
    }

    public void setCarIncNonScon(short carIncNonScon) {
        this.carIncNonScon = carIncNonScon;
    }

    public short getCarIncNonScon() {
        return this.carIncNonScon;
    }

    public void setCarInc(short carInc) {
        this.carInc = carInc;
    }

    public short getCarInc() {
        return this.carInc;
    }

    public void setRisRistorniCap(short risRistorniCap) {
        this.risRistorniCap = risRistorniCap;
    }

    public short getRisRistorniCap() {
        return this.risRistorniCap;
    }

    public void setIntrTecn(short intrTecn) {
        this.intrTecn = intrTecn;
    }

    public short getIntrTecn() {
        return this.intrTecn;
    }

    public void setCptRshMor(short cptRshMor) {
        this.cptRshMor = cptRshMor;
    }

    public short getCptRshMor() {
        return this.cptRshMor;
    }

    public void setcSubrshT(short cSubrshT) {
        this.cSubrshT = cSubrshT;
    }

    public short getcSubrshT() {
        return this.cSubrshT;
    }

    public void setPreRshT(short preRshT) {
        this.preRshT = preRshT;
    }

    public short getPreRshT() {
        return this.preRshT;
    }

    public void setAlqMargRis(short alqMargRis) {
        this.alqMargRis = alqMargRis;
    }

    public short getAlqMargRis() {
        return this.alqMargRis;
    }

    public void setAlqMargCSubrsh(short alqMargCSubrsh) {
        this.alqMargCSubrsh = alqMargCSubrsh;
    }

    public short getAlqMargCSubrsh() {
        return this.alqMargCSubrsh;
    }

    public void setTsRendtoSppr(short tsRendtoSppr) {
        this.tsRendtoSppr = tsRendtoSppr;
    }

    public short getTsRendtoSppr() {
        return this.tsRendtoSppr;
    }

    public void setTpIas(short tpIas) {
        this.tpIas = tpIas;
    }

    public short getTpIas() {
        return this.tpIas;
    }

    public void setNsQuo(short nsQuo) {
        this.nsQuo = nsQuo;
    }

    public short getNsQuo() {
        return this.nsQuo;
    }

    public void setTsMedio(short tsMedio) {
        this.tsMedio = tsMedio;
    }

    public short getTsMedio() {
        return this.tsMedio;
    }

    public void setCptRiasto(short cptRiasto) {
        this.cptRiasto = cptRiasto;
    }

    public short getCptRiasto() {
        return this.cptRiasto;
    }

    public void setPreRiasto(short preRiasto) {
        this.preRiasto = preRiasto;
    }

    public short getPreRiasto() {
        return this.preRiasto;
    }

    public void setRisRiasta(short risRiasta) {
        this.risRiasta = risRiasta;
    }

    public short getRisRiasta() {
        return this.risRiasta;
    }

    public void setCptRiastoEcc(short cptRiastoEcc) {
        this.cptRiastoEcc = cptRiastoEcc;
    }

    public short getCptRiastoEcc() {
        return this.cptRiastoEcc;
    }

    public void setPreRiastoEcc(short preRiastoEcc) {
        this.preRiastoEcc = preRiastoEcc;
    }

    public short getPreRiastoEcc() {
        return this.preRiastoEcc;
    }

    public void setRisRiastaEcc(short risRiastaEcc) {
        this.risRiastaEcc = risRiastaEcc;
    }

    public short getRisRiastaEcc() {
        return this.risRiastaEcc;
    }

    public void setCodAge(short codAge) {
        this.codAge = codAge;
    }

    public short getCodAge() {
        return this.codAge;
    }

    public void setCodSubage(short codSubage) {
        this.codSubage = codSubage;
    }

    public short getCodSubage() {
        return this.codSubage;
    }

    public void setCodCan(short codCan) {
        this.codCan = codCan;
    }

    public short getCodCan() {
        return this.codCan;
    }

    public void setIbPoli(short ibPoli) {
        this.ibPoli = ibPoli;
    }

    public short getIbPoli() {
        return this.ibPoli;
    }

    public void setIbAdes(short ibAdes) {
        this.ibAdes = ibAdes;
    }

    public short getIbAdes() {
        return this.ibAdes;
    }

    public void setIbTrchDiGar(short ibTrchDiGar) {
        this.ibTrchDiGar = ibTrchDiGar;
    }

    public short getIbTrchDiGar() {
        return this.ibTrchDiGar;
    }

    public void setTpPrstz(short tpPrstz) {
        this.tpPrstz = tpPrstz;
    }

    public short getTpPrstz() {
        return this.tpPrstz;
    }

    public void setTpTrasf(short tpTrasf) {
        this.tpTrasf = tpTrasf;
    }

    public short getTpTrasf() {
        return this.tpTrasf;
    }

    public void setPpInvrioTari(short ppInvrioTari) {
        this.ppInvrioTari = ppInvrioTari;
    }

    public short getPpInvrioTari() {
        return this.ppInvrioTari;
    }

    public void setCoeffOpzRen(short coeffOpzRen) {
        this.coeffOpzRen = coeffOpzRen;
    }

    public short getCoeffOpzRen() {
        return this.coeffOpzRen;
    }

    public void setCoeffOpzCpt(short coeffOpzCpt) {
        this.coeffOpzCpt = coeffOpzCpt;
    }

    public short getCoeffOpzCpt() {
        return this.coeffOpzCpt;
    }

    public void setDurPagRen(short durPagRen) {
        this.durPagRen = durPagRen;
    }

    public short getDurPagRen() {
        return this.durPagRen;
    }

    public void setVlt(short vlt) {
        this.vlt = vlt;
    }

    public short getVlt() {
        return this.vlt;
    }

    public void setRisMatChiuPrec(short risMatChiuPrec) {
        this.risMatChiuPrec = risMatChiuPrec;
    }

    public short getRisMatChiuPrec() {
        return this.risMatChiuPrec;
    }

    public void setCodFnd(short codFnd) {
        this.codFnd = codFnd;
    }

    public short getCodFnd() {
        return this.codFnd;
    }

    public void setPrstzT(short prstzT) {
        this.prstzT = prstzT;
    }

    public short getPrstzT() {
        return this.prstzT;
    }

    public void setTsTariDov(short tsTariDov) {
        this.tsTariDov = tsTariDov;
    }

    public short getTsTariDov() {
        return this.tsTariDov;
    }

    public void setTsTariScon(short tsTariScon) {
        this.tsTariScon = tsTariScon;
    }

    public short getTsTariScon() {
        return this.tsTariScon;
    }

    public void setTsPp(short tsPp) {
        this.tsPp = tsPp;
    }

    public short getTsPp() {
        return this.tsPp;
    }

    public void setCoeffRis1T(short coeffRis1T) {
        this.coeffRis1T = coeffRis1T;
    }

    public short getCoeffRis1T() {
        return this.coeffRis1T;
    }

    public void setCoeffRis2T(short coeffRis2T) {
        this.coeffRis2T = coeffRis2T;
    }

    public short getCoeffRis2T() {
        return this.coeffRis2T;
    }

    public void setAbb(short abb) {
        this.abb = abb;
    }

    public short getAbb() {
        return this.abb;
    }

    public void setTpCoass(short tpCoass) {
        this.tpCoass = tpCoass;
    }

    public short getTpCoass() {
        return this.tpCoass;
    }

    public void setTratRiass(short tratRiass) {
        this.tratRiass = tratRiass;
    }

    public short getTratRiass() {
        return this.tratRiass;
    }

    public void setTratRiassEcc(short tratRiassEcc) {
        this.tratRiassEcc = tratRiassEcc;
    }

    public short getTratRiassEcc() {
        return this.tratRiassEcc;
    }

    public void setTpRgmFisc(short tpRgmFisc) {
        this.tpRgmFisc = tpRgmFisc;
    }

    public short getTpRgmFisc() {
        return this.tpRgmFisc;
    }

    public void setDurGarAa(short durGarAa) {
        this.durGarAa = durGarAa;
    }

    public short getDurGarAa() {
        return this.durGarAa;
    }

    public void setDurGarMm(short durGarMm) {
        this.durGarMm = durGarMm;
    }

    public short getDurGarMm() {
        return this.durGarMm;
    }

    public void setDurGarGg(short durGarGg) {
        this.durGarGg = durGarGg;
    }

    public short getDurGarGg() {
        return this.durGarGg;
    }

    public void setAntidurCalc365(short antidurCalc365) {
        this.antidurCalc365 = antidurCalc365;
    }

    public short getAntidurCalc365() {
        return this.antidurCalc365;
    }

    public void setCodFiscCntr(short codFiscCntr) {
        this.codFiscCntr = codFiscCntr;
    }

    public short getCodFiscCntr() {
        return this.codFiscCntr;
    }

    public void setCodFiscAssto1(short codFiscAssto1) {
        this.codFiscAssto1 = codFiscAssto1;
    }

    public short getCodFiscAssto1() {
        return this.codFiscAssto1;
    }

    public void setCodFiscAssto2(short codFiscAssto2) {
        this.codFiscAssto2 = codFiscAssto2;
    }

    public short getCodFiscAssto2() {
        return this.codFiscAssto2;
    }

    public void setCodFiscAssto3(short codFiscAssto3) {
        this.codFiscAssto3 = codFiscAssto3;
    }

    public short getCodFiscAssto3() {
        return this.codFiscAssto3;
    }

    public void setCausScon(short causScon) {
        this.causScon = causScon;
    }

    public short getCausScon() {
        return this.causScon;
    }

    public void setEmitTitOpz(short emitTitOpz) {
        this.emitTitOpz = emitTitOpz;
    }

    public short getEmitTitOpz() {
        return this.emitTitOpz;
    }

    public void setQtzSpZCoupEmis(short qtzSpZCoupEmis) {
        this.qtzSpZCoupEmis = qtzSpZCoupEmis;
    }

    public short getQtzSpZCoupEmis() {
        return this.qtzSpZCoupEmis;
    }

    public void setQtzSpZOpzEmis(short qtzSpZOpzEmis) {
        this.qtzSpZOpzEmis = qtzSpZOpzEmis;
    }

    public short getQtzSpZOpzEmis() {
        return this.qtzSpZOpzEmis;
    }

    public void setQtzSpZCoupDtC(short qtzSpZCoupDtC) {
        this.qtzSpZCoupDtC = qtzSpZCoupDtC;
    }

    public short getQtzSpZCoupDtC() {
        return this.qtzSpZCoupDtC;
    }

    public void setQtzSpZOpzDtCa(short qtzSpZOpzDtCa) {
        this.qtzSpZOpzDtCa = qtzSpZOpzDtCa;
    }

    public short getQtzSpZOpzDtCa() {
        return this.qtzSpZOpzDtCa;
    }

    public void setQtzTotEmis(short qtzTotEmis) {
        this.qtzTotEmis = qtzTotEmis;
    }

    public short getQtzTotEmis() {
        return this.qtzTotEmis;
    }

    public void setQtzTotDtCalc(short qtzTotDtCalc) {
        this.qtzTotDtCalc = qtzTotDtCalc;
    }

    public short getQtzTotDtCalc() {
        return this.qtzTotDtCalc;
    }

    public void setQtzTotDtUltBil(short qtzTotDtUltBil) {
        this.qtzTotDtUltBil = qtzTotDtUltBil;
    }

    public short getQtzTotDtUltBil() {
        return this.qtzTotDtUltBil;
    }

    public void setDtQtzEmis(short dtQtzEmis) {
        this.dtQtzEmis = dtQtzEmis;
    }

    public short getDtQtzEmis() {
        return this.dtQtzEmis;
    }

    public void setPcCarGest(short pcCarGest) {
        this.pcCarGest = pcCarGest;
    }

    public short getPcCarGest() {
        return this.pcCarGest;
    }

    public void setPcCarAcq(short pcCarAcq) {
        this.pcCarAcq = pcCarAcq;
    }

    public short getPcCarAcq() {
        return this.pcCarAcq;
    }

    public void setImpCarCasoMor(short impCarCasoMor) {
        this.impCarCasoMor = impCarCasoMor;
    }

    public short getImpCarCasoMor() {
        return this.impCarCasoMor;
    }

    public void setPcCarMor(short pcCarMor) {
        this.pcCarMor = pcCarMor;
    }

    public short getPcCarMor() {
        return this.pcCarMor;
    }

    public void setTpVers(short tpVers) {
        this.tpVers = tpVers;
    }

    public short getTpVers() {
        return this.tpVers;
    }

    public void setFlSwitch(short flSwitch) {
        this.flSwitch = flSwitch;
    }

    public short getFlSwitch() {
        return this.flSwitch;
    }

    public void setFlIas(short flIas) {
        this.flIas = flIas;
    }

    public short getFlIas() {
        return this.flIas;
    }

    public void setDir(short dir) {
        this.dir = dir;
    }

    public short getDir() {
        return this.dir;
    }

    public void setTpCopCasoMor(short tpCopCasoMor) {
        this.tpCopCasoMor = tpCopCasoMor;
    }

    public short getTpCopCasoMor() {
        return this.tpCopCasoMor;
    }

    public void setMetRiscSpcl(short metRiscSpcl) {
        this.metRiscSpcl = metRiscSpcl;
    }

    public short getMetRiscSpcl() {
        return this.metRiscSpcl;
    }

    public void setTpStatInvst(short tpStatInvst) {
        this.tpStatInvst = tpStatInvst;
    }

    public short getTpStatInvst() {
        return this.tpStatInvst;
    }

    public void setCodPrdt(short codPrdt) {
        this.codPrdt = codPrdt;
    }

    public short getCodPrdt() {
        return this.codPrdt;
    }

    public void setStatAssto1(short statAssto1) {
        this.statAssto1 = statAssto1;
    }

    public short getStatAssto1() {
        return this.statAssto1;
    }

    public void setStatAssto2(short statAssto2) {
        this.statAssto2 = statAssto2;
    }

    public short getStatAssto2() {
        return this.statAssto2;
    }

    public void setStatAssto3(short statAssto3) {
        this.statAssto3 = statAssto3;
    }

    public short getStatAssto3() {
        return this.statAssto3;
    }

    public void setCptAsstoIniMor(short cptAsstoIniMor) {
        this.cptAsstoIniMor = cptAsstoIniMor;
    }

    public short getCptAsstoIniMor() {
        return this.cptAsstoIniMor;
    }

    public void setTsStabPre(short tsStabPre) {
        this.tsStabPre = tsStabPre;
    }

    public short getTsStabPre() {
        return this.tsStabPre;
    }

    public void setDirEmis(short dirEmis) {
        this.dirEmis = dirEmis;
    }

    public short getDirEmis() {
        return this.dirEmis;
    }

    public void setDtIncUltPre(short dtIncUltPre) {
        this.dtIncUltPre = dtIncUltPre;
    }

    public short getDtIncUltPre() {
        return this.dtIncUltPre;
    }

    public void setStatTbgcAssto1(short statTbgcAssto1) {
        this.statTbgcAssto1 = statTbgcAssto1;
    }

    public short getStatTbgcAssto1() {
        return this.statTbgcAssto1;
    }

    public void setStatTbgcAssto2(short statTbgcAssto2) {
        this.statTbgcAssto2 = statTbgcAssto2;
    }

    public short getStatTbgcAssto2() {
        return this.statTbgcAssto2;
    }

    public void setStatTbgcAssto3(short statTbgcAssto3) {
        this.statTbgcAssto3 = statTbgcAssto3;
    }

    public short getStatTbgcAssto3() {
        return this.statTbgcAssto3;
    }

    public void setFrazDecrCpt(short frazDecrCpt) {
        this.frazDecrCpt = frazDecrCpt;
    }

    public short getFrazDecrCpt() {
        return this.frazDecrCpt;
    }

    public void setPrePpUlt(short prePpUlt) {
        this.prePpUlt = prePpUlt;
    }

    public short getPrePpUlt() {
        return this.prePpUlt;
    }

    public void setAcqExp(short acqExp) {
        this.acqExp = acqExp;
    }

    public short getAcqExp() {
        return this.acqExp;
    }

    public void setRemunAss(short remunAss) {
        this.remunAss = remunAss;
    }

    public short getRemunAss() {
        return this.remunAss;
    }

    public void setCommisInter(short commisInter) {
        this.commisInter = commisInter;
    }

    public short getCommisInter() {
        return this.commisInter;
    }

    public void setNumFinanz(short numFinanz) {
        this.numFinanz = numFinanz;
    }

    public short getNumFinanz() {
        return this.numFinanz;
    }

    public void setTpAccComm(short tpAccComm) {
        this.tpAccComm = tpAccComm;
    }

    public short getTpAccComm() {
        return this.tpAccComm;
    }

    public void setIbAccComm(short ibAccComm) {
        this.ibAccComm = ibAccComm;
    }

    public short getIbAccComm() {
        return this.ibAccComm;
    }

    public void setCarz(short carz) {
        this.carz = carz;
    }

    public short getCarz() {
        return this.carz;
    }
}
