package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-DETT-TIT-CONT<br>
 * Variable: IND-DETT-TIT-CONT from copybook IDBVDTC2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndDettTitCont {

    //==== PROPERTIES ====
    //Original name: IND-DTC-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-DT-INI-COP
    private short dtIniCop = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-DT-END-COP
    private short dtEndCop = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-PRE-NET
    private short preNet = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-INTR-FRAZ
    private short intrFraz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-INTR-MORA
    private short intrMora = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-INTR-RETDT
    private short intrRetdt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-INTR-RIAT
    private short intrRiat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-DIR
    private short dir = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-SPE-MED
    private short speMed = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-TAX
    private short tax = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-SOPR-SAN
    private short soprSan = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-SOPR-SPO
    private short soprSpo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-SOPR-TEC
    private short soprTec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-SOPR-PROF
    private short soprProf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-SOPR-ALT
    private short soprAlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-PRE-TOT
    private short preTot = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-PRE-PP-IAS
    private short prePpIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-PRE-SOLO-RSH
    private short preSoloRsh = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-CAR-ACQ
    private short carAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-CAR-GEST
    private short carGest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-CAR-INC
    private short carInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-PROV-ACQ-1AA
    private short provAcq1aa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-PROV-ACQ-2AA
    private short provAcq2aa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-PROV-RICOR
    private short provRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-PROV-INC
    private short provInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-PROV-DA-REC
    private short provDaRec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-COD-DVS
    private short codDvs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-FRQ-MOVI
    private short frqMovi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-COD-TARI
    private short codTari = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-IMP-AZ
    private short impAz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-IMP-ADER
    private short impAder = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-IMP-TFR
    private short impTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-IMP-VOLO
    private short impVolo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-MANFEE-ANTIC
    private short manfeeAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-MANFEE-RICOR
    private short manfeeRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-MANFEE-REC
    private short manfeeRec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-DT-ESI-TIT
    private short dtEsiTit = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-SPE-AGE
    private short speAge = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-CAR-IAS
    private short carIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-TOT-INTR-PREST
    private short totIntrPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-IMP-TRASFE
    private short impTrasfe = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-IMP-TFR-STRC
    private short impTfrStrc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-NUM-GG-RITARDO-PAG
    private short numGgRitardoPag = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-NUM-GG-RIVAL
    private short numGgRival = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-ACQ-EXP
    private short acqExp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-REMUN-ASS
    private short remunAss = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-COMMIS-INTER
    private short commisInter = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DTC-CNBT-ANTIRAC
    private short cnbtAntirac = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtIniCop(short dtIniCop) {
        this.dtIniCop = dtIniCop;
    }

    public short getDtIniCop() {
        return this.dtIniCop;
    }

    public void setDtEndCop(short dtEndCop) {
        this.dtEndCop = dtEndCop;
    }

    public short getDtEndCop() {
        return this.dtEndCop;
    }

    public void setPreNet(short preNet) {
        this.preNet = preNet;
    }

    public short getPreNet() {
        return this.preNet;
    }

    public void setIntrFraz(short intrFraz) {
        this.intrFraz = intrFraz;
    }

    public short getIntrFraz() {
        return this.intrFraz;
    }

    public void setIntrMora(short intrMora) {
        this.intrMora = intrMora;
    }

    public short getIntrMora() {
        return this.intrMora;
    }

    public void setIntrRetdt(short intrRetdt) {
        this.intrRetdt = intrRetdt;
    }

    public short getIntrRetdt() {
        return this.intrRetdt;
    }

    public void setIntrRiat(short intrRiat) {
        this.intrRiat = intrRiat;
    }

    public short getIntrRiat() {
        return this.intrRiat;
    }

    public void setDir(short dir) {
        this.dir = dir;
    }

    public short getDir() {
        return this.dir;
    }

    public void setSpeMed(short speMed) {
        this.speMed = speMed;
    }

    public short getSpeMed() {
        return this.speMed;
    }

    public void setTax(short tax) {
        this.tax = tax;
    }

    public short getTax() {
        return this.tax;
    }

    public void setSoprSan(short soprSan) {
        this.soprSan = soprSan;
    }

    public short getSoprSan() {
        return this.soprSan;
    }

    public void setSoprSpo(short soprSpo) {
        this.soprSpo = soprSpo;
    }

    public short getSoprSpo() {
        return this.soprSpo;
    }

    public void setSoprTec(short soprTec) {
        this.soprTec = soprTec;
    }

    public short getSoprTec() {
        return this.soprTec;
    }

    public void setSoprProf(short soprProf) {
        this.soprProf = soprProf;
    }

    public short getSoprProf() {
        return this.soprProf;
    }

    public void setSoprAlt(short soprAlt) {
        this.soprAlt = soprAlt;
    }

    public short getSoprAlt() {
        return this.soprAlt;
    }

    public void setPreTot(short preTot) {
        this.preTot = preTot;
    }

    public short getPreTot() {
        return this.preTot;
    }

    public void setPrePpIas(short prePpIas) {
        this.prePpIas = prePpIas;
    }

    public short getPrePpIas() {
        return this.prePpIas;
    }

    public void setPreSoloRsh(short preSoloRsh) {
        this.preSoloRsh = preSoloRsh;
    }

    public short getPreSoloRsh() {
        return this.preSoloRsh;
    }

    public void setCarAcq(short carAcq) {
        this.carAcq = carAcq;
    }

    public short getCarAcq() {
        return this.carAcq;
    }

    public void setCarGest(short carGest) {
        this.carGest = carGest;
    }

    public short getCarGest() {
        return this.carGest;
    }

    public void setCarInc(short carInc) {
        this.carInc = carInc;
    }

    public short getCarInc() {
        return this.carInc;
    }

    public void setProvAcq1aa(short provAcq1aa) {
        this.provAcq1aa = provAcq1aa;
    }

    public short getProvAcq1aa() {
        return this.provAcq1aa;
    }

    public void setProvAcq2aa(short provAcq2aa) {
        this.provAcq2aa = provAcq2aa;
    }

    public short getProvAcq2aa() {
        return this.provAcq2aa;
    }

    public void setProvRicor(short provRicor) {
        this.provRicor = provRicor;
    }

    public short getProvRicor() {
        return this.provRicor;
    }

    public void setProvInc(short provInc) {
        this.provInc = provInc;
    }

    public short getProvInc() {
        return this.provInc;
    }

    public void setProvDaRec(short provDaRec) {
        this.provDaRec = provDaRec;
    }

    public short getProvDaRec() {
        return this.provDaRec;
    }

    public void setCodDvs(short codDvs) {
        this.codDvs = codDvs;
    }

    public short getCodDvs() {
        return this.codDvs;
    }

    public void setFrqMovi(short frqMovi) {
        this.frqMovi = frqMovi;
    }

    public short getFrqMovi() {
        return this.frqMovi;
    }

    public void setCodTari(short codTari) {
        this.codTari = codTari;
    }

    public short getCodTari() {
        return this.codTari;
    }

    public void setImpAz(short impAz) {
        this.impAz = impAz;
    }

    public short getImpAz() {
        return this.impAz;
    }

    public void setImpAder(short impAder) {
        this.impAder = impAder;
    }

    public short getImpAder() {
        return this.impAder;
    }

    public void setImpTfr(short impTfr) {
        this.impTfr = impTfr;
    }

    public short getImpTfr() {
        return this.impTfr;
    }

    public void setImpVolo(short impVolo) {
        this.impVolo = impVolo;
    }

    public short getImpVolo() {
        return this.impVolo;
    }

    public void setManfeeAntic(short manfeeAntic) {
        this.manfeeAntic = manfeeAntic;
    }

    public short getManfeeAntic() {
        return this.manfeeAntic;
    }

    public void setManfeeRicor(short manfeeRicor) {
        this.manfeeRicor = manfeeRicor;
    }

    public short getManfeeRicor() {
        return this.manfeeRicor;
    }

    public void setManfeeRec(short manfeeRec) {
        this.manfeeRec = manfeeRec;
    }

    public short getManfeeRec() {
        return this.manfeeRec;
    }

    public void setDtEsiTit(short dtEsiTit) {
        this.dtEsiTit = dtEsiTit;
    }

    public short getDtEsiTit() {
        return this.dtEsiTit;
    }

    public void setSpeAge(short speAge) {
        this.speAge = speAge;
    }

    public short getSpeAge() {
        return this.speAge;
    }

    public void setCarIas(short carIas) {
        this.carIas = carIas;
    }

    public short getCarIas() {
        return this.carIas;
    }

    public void setTotIntrPrest(short totIntrPrest) {
        this.totIntrPrest = totIntrPrest;
    }

    public short getTotIntrPrest() {
        return this.totIntrPrest;
    }

    public void setImpTrasfe(short impTrasfe) {
        this.impTrasfe = impTrasfe;
    }

    public short getImpTrasfe() {
        return this.impTrasfe;
    }

    public void setImpTfrStrc(short impTfrStrc) {
        this.impTfrStrc = impTfrStrc;
    }

    public short getImpTfrStrc() {
        return this.impTfrStrc;
    }

    public void setNumGgRitardoPag(short numGgRitardoPag) {
        this.numGgRitardoPag = numGgRitardoPag;
    }

    public short getNumGgRitardoPag() {
        return this.numGgRitardoPag;
    }

    public void setNumGgRival(short numGgRival) {
        this.numGgRival = numGgRival;
    }

    public short getNumGgRival() {
        return this.numGgRival;
    }

    public void setAcqExp(short acqExp) {
        this.acqExp = acqExp;
    }

    public short getAcqExp() {
        return this.acqExp;
    }

    public void setRemunAss(short remunAss) {
        this.remunAss = remunAss;
    }

    public short getRemunAss() {
        return this.remunAss;
    }

    public void setCommisInter(short commisInter) {
        this.commisInter = commisInter;
    }

    public short getCommisInter() {
        return this.commisInter;
    }

    public void setCnbtAntirac(short cnbtAntirac) {
        this.cnbtAntirac = cnbtAntirac;
    }

    public short getCnbtAntirac() {
        return this.cnbtAntirac;
    }
}
