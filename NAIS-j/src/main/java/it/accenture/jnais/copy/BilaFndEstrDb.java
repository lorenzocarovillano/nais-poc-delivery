package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: BILA-FND-ESTR-DB<br>
 * Variable: BILA-FND-ESTR-DB from copybook IDBVB013<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class BilaFndEstrDb {

    //==== PROPERTIES ====
    //Original name: B01-DT-RIS-DB
    private String risDb = DefaultValues.stringVal(Len.RIS_DB);
    //Original name: B01-DT-QTZ-INI-DB
    private String qtzIniDb = DefaultValues.stringVal(Len.QTZ_INI_DB);
    //Original name: B01-DT-VALZZ-QUO-DT-CA-DB
    private String valzzQuoDtCaDb = DefaultValues.stringVal(Len.VALZZ_QUO_DT_CA_DB);

    //==== METHODS ====
    public void setRisDb(String risDb) {
        this.risDb = Functions.subString(risDb, Len.RIS_DB);
    }

    public String getRisDb() {
        return this.risDb;
    }

    public void setQtzIniDb(String qtzIniDb) {
        this.qtzIniDb = Functions.subString(qtzIniDb, Len.QTZ_INI_DB);
    }

    public String getQtzIniDb() {
        return this.qtzIniDb;
    }

    public void setValzzQuoDtCaDb(String valzzQuoDtCaDb) {
        this.valzzQuoDtCaDb = Functions.subString(valzzQuoDtCaDb, Len.VALZZ_QUO_DT_CA_DB);
    }

    public String getValzzQuoDtCaDb() {
        return this.valzzQuoDtCaDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RIS_DB = 10;
        public static final int QTZ_INI_DB = 10;
        public static final int VALZZ_QUO_DT_CA_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
