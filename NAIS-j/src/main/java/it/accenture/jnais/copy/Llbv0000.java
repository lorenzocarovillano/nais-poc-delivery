package it.accenture.jnais.copy;

import it.accenture.jnais.ws.enums.WsStatiPrenotazione;
import it.accenture.jnais.ws.enums.WsTpRich;

/**Original name: LLBV0000<br>
 * Variable: LLBV0000 from copybook LLBV0000<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Llbv0000 {

    //==== PROPERTIES ====
    /**Original name: WS-TP-RICH<br>
	 * <pre>----------------------------------------------------------------*
	 *     TIPO RICHIESTA
	 * ----------------------------------------------------------------*
	 *     B1 => Estrazione Dati Massiva
	 *     B2 => Aggiornamento Dati Estratti
	 *     B3 => Calcolo Riserve
	 *     B4 => Consolidamento Riserve Calcolate
	 *     B5 => Estrazione Dati Massiva e Calcolo Riserve
	 *     B6 => Aggiornamento Dati Estratti e Calcolo Riserve
	 *     B7 => Verifica Corrispondenza Adesioni Estratte/Calcolate
	 *     B8 => Certificazione Dati Estratti
	 *     B9 => Certificazione Dati Calcolati
	 *     BA => Aggiornamento Calcolo Riserve
	 *     BB => Svecchiamento Prenotazioni Annullate
	 *     BC => Annullamento Estrazione Dati Massiva
	 *     BD => Riserve Somme da Pagare
	 *     BE => Aggiornamento Riserve Somme da Pagare
	 *     BF => Certificazione Riserve Somme da Pagare
	 *     BG => Consolidamento Riserve Somme da Pagare</pre>*/
    private WsTpRich tpRich = new WsTpRich();
    /**Original name: WS-STATI-PRENOTAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     STATI PRENOTAZIONE
	 * ----------------------------------------------------------------*
	 *     'P' => PRENOTATA
	 *     'E' => ESEGUITA
	 *     'A' => ANNULLATA
	 *     'C' => CERTIFICATA
	 *     'S' => CONSOLIDATA
	 *     'K' => ESEGUITA KO</pre>*/
    private WsStatiPrenotazione statiPrenotazione = new WsStatiPrenotazione();

    //==== METHODS ====
    public WsStatiPrenotazione getStatiPrenotazione() {
        return statiPrenotazione;
    }

    public WsTpRich getTpRich() {
        return tpRich;
    }
}
