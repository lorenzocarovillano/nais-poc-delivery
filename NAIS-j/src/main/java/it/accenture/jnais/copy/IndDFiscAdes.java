package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-D-FISC-ADES<br>
 * Variable: IND-D-FISC-ADES from copybook IDBVDFA2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndDFiscAdes {

    //==== PROPERTIES ====
    //Original name: IND-DFA-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-TP-ISC-FND
    private short tpIscFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-DT-ACCNS-RAPP-FND
    private short dtAccnsRappFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-AZ-K1
    private short impCnbtAzK1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-ISC-K1
    private short impCnbtIscK1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-TFR-K1
    private short impCnbtTfrK1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-VOL-K1
    private short impCnbtVolK1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-AZ-K2
    private short impCnbtAzK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-ISC-K2
    private short impCnbtIscK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-TFR-K2
    private short impCnbtTfrK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-VOL-K2
    private short impCnbtVolK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-MATU-K1
    private short matuK1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-MATU-RES-K1
    private short matuResK1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-MATU-K2
    private short matuK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPB-VIS
    private short impbVis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPST-VIS
    private short impstVis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPB-IS-K2
    private short impbIsK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPST-SOST-K2
    private short impstSostK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-RIDZ-TFR
    private short ridzTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-PC-TFR
    private short pcTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-ALQ-TFR
    private short alqTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-TOT-ANTIC
    private short totAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPB-TFR-ANTIC
    private short impbTfrAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPST-TFR-ANTIC
    private short impstTfrAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-RIDZ-TFR-SU-ANTIC
    private short ridzTfrSuAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPB-VIS-ANTIC
    private short impbVisAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPST-VIS-ANTIC
    private short impstVisAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPB-IS-K2-ANTIC
    private short impbIsK2Antic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPST-SOST-K2-ANTI
    private short impstSostK2Anti = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-ULT-COMMIS-TRASFE
    private short ultCommisTrasfe = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-COD-DVS
    private short codDvs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-ALQ-PRVR
    private short alqPrvr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-PC-ESE-IMPST-TFR
    private short pcEseImpstTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-ESE-IMPST-TFR
    private short impEseImpstTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-ANZ-CNBTVA-CARASS
    private short anzCnbtvaCarass = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-ANZ-CNBTVA-CARAZI
    private short anzCnbtvaCarazi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-ANZ-SRVZ
    private short anzSrvz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-NDED-K1
    private short impCnbtNdedK1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-NDED-K2
    private short impCnbtNdedK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-NDED-K3
    private short impCnbtNdedK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-AZ-K3
    private short impCnbtAzK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-ISC-K3
    private short impCnbtIscK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-TFR-K3
    private short impCnbtTfrK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-CNBT-VOL-K3
    private short impCnbtVolK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-MATU-K3
    private short matuK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPB-252-ANTIC
    private short impb252Antic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPST-252-ANTIC
    private short impst252Antic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-DT-1A-CNBZ
    private short dt1aCnbz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-COMMIS-DI-TRASFE
    private short commisDiTrasfe = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-AA-CNBZ-K1
    private short aaCnbzK1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-AA-CNBZ-K2
    private short aaCnbzK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-AA-CNBZ-K3
    private short aaCnbzK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-MM-CNBZ-K1
    private short mmCnbzK1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-MM-CNBZ-K2
    private short mmCnbzK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-MM-CNBZ-K3
    private short mmCnbzK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-FL-APPLZ-NEWFIS
    private short flApplzNewfis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-REDT-TASS-ABBAT-K3
    private short redtTassAbbatK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-CNBT-ECC-4X100-K1
    private short cnbtEcc4x100K1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-CREDITO-IS
    private short creditoIs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-REDT-TASS-ABBAT-K2
    private short redtTassAbbatK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPB-IS-K3
    private short impbIsK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPST-SOST-K3
    private short impstSostK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPB-252-K3
    private short impb252K3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPST-252-K3
    private short impst252K3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPB-IS-K3-ANTIC
    private short impbIsK3Antic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPST-SOST-K3-ANTI
    private short impstSostK3Anti = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPB-IRPEF-K1-ANTI
    private short impbIrpefK1Anti = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPST-IRPEF-K1-ANT
    private short impstIrpefK1Ant = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPB-IRPEF-K2-ANTI
    private short impbIrpefK2Anti = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMPST-IRPEF-K2-ANT
    private short impstIrpefK2Ant = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-DT-CESSAZIONE
    private short dtCessazione = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-TOT-IMPST
    private short totImpst = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-ONER-TRASFE
    private short onerTrasfe = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFA-IMP-NET-TRASFERITO
    private short impNetTrasferito = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setTpIscFnd(short tpIscFnd) {
        this.tpIscFnd = tpIscFnd;
    }

    public short getTpIscFnd() {
        return this.tpIscFnd;
    }

    public void setDtAccnsRappFnd(short dtAccnsRappFnd) {
        this.dtAccnsRappFnd = dtAccnsRappFnd;
    }

    public short getDtAccnsRappFnd() {
        return this.dtAccnsRappFnd;
    }

    public void setImpCnbtAzK1(short impCnbtAzK1) {
        this.impCnbtAzK1 = impCnbtAzK1;
    }

    public short getImpCnbtAzK1() {
        return this.impCnbtAzK1;
    }

    public void setImpCnbtIscK1(short impCnbtIscK1) {
        this.impCnbtIscK1 = impCnbtIscK1;
    }

    public short getImpCnbtIscK1() {
        return this.impCnbtIscK1;
    }

    public void setImpCnbtTfrK1(short impCnbtTfrK1) {
        this.impCnbtTfrK1 = impCnbtTfrK1;
    }

    public short getImpCnbtTfrK1() {
        return this.impCnbtTfrK1;
    }

    public void setImpCnbtVolK1(short impCnbtVolK1) {
        this.impCnbtVolK1 = impCnbtVolK1;
    }

    public short getImpCnbtVolK1() {
        return this.impCnbtVolK1;
    }

    public void setImpCnbtAzK2(short impCnbtAzK2) {
        this.impCnbtAzK2 = impCnbtAzK2;
    }

    public short getImpCnbtAzK2() {
        return this.impCnbtAzK2;
    }

    public void setImpCnbtIscK2(short impCnbtIscK2) {
        this.impCnbtIscK2 = impCnbtIscK2;
    }

    public short getImpCnbtIscK2() {
        return this.impCnbtIscK2;
    }

    public void setImpCnbtTfrK2(short impCnbtTfrK2) {
        this.impCnbtTfrK2 = impCnbtTfrK2;
    }

    public short getImpCnbtTfrK2() {
        return this.impCnbtTfrK2;
    }

    public void setImpCnbtVolK2(short impCnbtVolK2) {
        this.impCnbtVolK2 = impCnbtVolK2;
    }

    public short getImpCnbtVolK2() {
        return this.impCnbtVolK2;
    }

    public void setMatuK1(short matuK1) {
        this.matuK1 = matuK1;
    }

    public short getMatuK1() {
        return this.matuK1;
    }

    public void setMatuResK1(short matuResK1) {
        this.matuResK1 = matuResK1;
    }

    public short getMatuResK1() {
        return this.matuResK1;
    }

    public void setMatuK2(short matuK2) {
        this.matuK2 = matuK2;
    }

    public short getMatuK2() {
        return this.matuK2;
    }

    public void setImpbVis(short impbVis) {
        this.impbVis = impbVis;
    }

    public short getImpbVis() {
        return this.impbVis;
    }

    public void setImpstVis(short impstVis) {
        this.impstVis = impstVis;
    }

    public short getImpstVis() {
        return this.impstVis;
    }

    public void setImpbIsK2(short impbIsK2) {
        this.impbIsK2 = impbIsK2;
    }

    public short getImpbIsK2() {
        return this.impbIsK2;
    }

    public void setImpstSostK2(short impstSostK2) {
        this.impstSostK2 = impstSostK2;
    }

    public short getImpstSostK2() {
        return this.impstSostK2;
    }

    public void setRidzTfr(short ridzTfr) {
        this.ridzTfr = ridzTfr;
    }

    public short getRidzTfr() {
        return this.ridzTfr;
    }

    public void setPcTfr(short pcTfr) {
        this.pcTfr = pcTfr;
    }

    public short getPcTfr() {
        return this.pcTfr;
    }

    public void setAlqTfr(short alqTfr) {
        this.alqTfr = alqTfr;
    }

    public short getAlqTfr() {
        return this.alqTfr;
    }

    public void setTotAntic(short totAntic) {
        this.totAntic = totAntic;
    }

    public short getTotAntic() {
        return this.totAntic;
    }

    public void setImpbTfrAntic(short impbTfrAntic) {
        this.impbTfrAntic = impbTfrAntic;
    }

    public short getImpbTfrAntic() {
        return this.impbTfrAntic;
    }

    public void setImpstTfrAntic(short impstTfrAntic) {
        this.impstTfrAntic = impstTfrAntic;
    }

    public short getImpstTfrAntic() {
        return this.impstTfrAntic;
    }

    public void setRidzTfrSuAntic(short ridzTfrSuAntic) {
        this.ridzTfrSuAntic = ridzTfrSuAntic;
    }

    public short getRidzTfrSuAntic() {
        return this.ridzTfrSuAntic;
    }

    public void setImpbVisAntic(short impbVisAntic) {
        this.impbVisAntic = impbVisAntic;
    }

    public short getImpbVisAntic() {
        return this.impbVisAntic;
    }

    public void setImpstVisAntic(short impstVisAntic) {
        this.impstVisAntic = impstVisAntic;
    }

    public short getImpstVisAntic() {
        return this.impstVisAntic;
    }

    public void setImpbIsK2Antic(short impbIsK2Antic) {
        this.impbIsK2Antic = impbIsK2Antic;
    }

    public short getImpbIsK2Antic() {
        return this.impbIsK2Antic;
    }

    public void setImpstSostK2Anti(short impstSostK2Anti) {
        this.impstSostK2Anti = impstSostK2Anti;
    }

    public short getImpstSostK2Anti() {
        return this.impstSostK2Anti;
    }

    public void setUltCommisTrasfe(short ultCommisTrasfe) {
        this.ultCommisTrasfe = ultCommisTrasfe;
    }

    public short getUltCommisTrasfe() {
        return this.ultCommisTrasfe;
    }

    public void setCodDvs(short codDvs) {
        this.codDvs = codDvs;
    }

    public short getCodDvs() {
        return this.codDvs;
    }

    public void setAlqPrvr(short alqPrvr) {
        this.alqPrvr = alqPrvr;
    }

    public short getAlqPrvr() {
        return this.alqPrvr;
    }

    public void setPcEseImpstTfr(short pcEseImpstTfr) {
        this.pcEseImpstTfr = pcEseImpstTfr;
    }

    public short getPcEseImpstTfr() {
        return this.pcEseImpstTfr;
    }

    public void setImpEseImpstTfr(short impEseImpstTfr) {
        this.impEseImpstTfr = impEseImpstTfr;
    }

    public short getImpEseImpstTfr() {
        return this.impEseImpstTfr;
    }

    public void setAnzCnbtvaCarass(short anzCnbtvaCarass) {
        this.anzCnbtvaCarass = anzCnbtvaCarass;
    }

    public short getAnzCnbtvaCarass() {
        return this.anzCnbtvaCarass;
    }

    public void setAnzCnbtvaCarazi(short anzCnbtvaCarazi) {
        this.anzCnbtvaCarazi = anzCnbtvaCarazi;
    }

    public short getAnzCnbtvaCarazi() {
        return this.anzCnbtvaCarazi;
    }

    public void setAnzSrvz(short anzSrvz) {
        this.anzSrvz = anzSrvz;
    }

    public short getAnzSrvz() {
        return this.anzSrvz;
    }

    public void setImpCnbtNdedK1(short impCnbtNdedK1) {
        this.impCnbtNdedK1 = impCnbtNdedK1;
    }

    public short getImpCnbtNdedK1() {
        return this.impCnbtNdedK1;
    }

    public void setImpCnbtNdedK2(short impCnbtNdedK2) {
        this.impCnbtNdedK2 = impCnbtNdedK2;
    }

    public short getImpCnbtNdedK2() {
        return this.impCnbtNdedK2;
    }

    public void setImpCnbtNdedK3(short impCnbtNdedK3) {
        this.impCnbtNdedK3 = impCnbtNdedK3;
    }

    public short getImpCnbtNdedK3() {
        return this.impCnbtNdedK3;
    }

    public void setImpCnbtAzK3(short impCnbtAzK3) {
        this.impCnbtAzK3 = impCnbtAzK3;
    }

    public short getImpCnbtAzK3() {
        return this.impCnbtAzK3;
    }

    public void setImpCnbtIscK3(short impCnbtIscK3) {
        this.impCnbtIscK3 = impCnbtIscK3;
    }

    public short getImpCnbtIscK3() {
        return this.impCnbtIscK3;
    }

    public void setImpCnbtTfrK3(short impCnbtTfrK3) {
        this.impCnbtTfrK3 = impCnbtTfrK3;
    }

    public short getImpCnbtTfrK3() {
        return this.impCnbtTfrK3;
    }

    public void setImpCnbtVolK3(short impCnbtVolK3) {
        this.impCnbtVolK3 = impCnbtVolK3;
    }

    public short getImpCnbtVolK3() {
        return this.impCnbtVolK3;
    }

    public void setMatuK3(short matuK3) {
        this.matuK3 = matuK3;
    }

    public short getMatuK3() {
        return this.matuK3;
    }

    public void setImpb252Antic(short impb252Antic) {
        this.impb252Antic = impb252Antic;
    }

    public short getImpb252Antic() {
        return this.impb252Antic;
    }

    public void setImpst252Antic(short impst252Antic) {
        this.impst252Antic = impst252Antic;
    }

    public short getImpst252Antic() {
        return this.impst252Antic;
    }

    public void setDt1aCnbz(short dt1aCnbz) {
        this.dt1aCnbz = dt1aCnbz;
    }

    public short getDt1aCnbz() {
        return this.dt1aCnbz;
    }

    public void setCommisDiTrasfe(short commisDiTrasfe) {
        this.commisDiTrasfe = commisDiTrasfe;
    }

    public short getCommisDiTrasfe() {
        return this.commisDiTrasfe;
    }

    public void setAaCnbzK1(short aaCnbzK1) {
        this.aaCnbzK1 = aaCnbzK1;
    }

    public short getAaCnbzK1() {
        return this.aaCnbzK1;
    }

    public void setAaCnbzK2(short aaCnbzK2) {
        this.aaCnbzK2 = aaCnbzK2;
    }

    public short getAaCnbzK2() {
        return this.aaCnbzK2;
    }

    public void setAaCnbzK3(short aaCnbzK3) {
        this.aaCnbzK3 = aaCnbzK3;
    }

    public short getAaCnbzK3() {
        return this.aaCnbzK3;
    }

    public void setMmCnbzK1(short mmCnbzK1) {
        this.mmCnbzK1 = mmCnbzK1;
    }

    public short getMmCnbzK1() {
        return this.mmCnbzK1;
    }

    public void setMmCnbzK2(short mmCnbzK2) {
        this.mmCnbzK2 = mmCnbzK2;
    }

    public short getMmCnbzK2() {
        return this.mmCnbzK2;
    }

    public void setMmCnbzK3(short mmCnbzK3) {
        this.mmCnbzK3 = mmCnbzK3;
    }

    public short getMmCnbzK3() {
        return this.mmCnbzK3;
    }

    public void setFlApplzNewfis(short flApplzNewfis) {
        this.flApplzNewfis = flApplzNewfis;
    }

    public short getFlApplzNewfis() {
        return this.flApplzNewfis;
    }

    public void setRedtTassAbbatK3(short redtTassAbbatK3) {
        this.redtTassAbbatK3 = redtTassAbbatK3;
    }

    public short getRedtTassAbbatK3() {
        return this.redtTassAbbatK3;
    }

    public void setCnbtEcc4x100K1(short cnbtEcc4x100K1) {
        this.cnbtEcc4x100K1 = cnbtEcc4x100K1;
    }

    public short getCnbtEcc4x100K1() {
        return this.cnbtEcc4x100K1;
    }

    public void setCreditoIs(short creditoIs) {
        this.creditoIs = creditoIs;
    }

    public short getCreditoIs() {
        return this.creditoIs;
    }

    public void setRedtTassAbbatK2(short redtTassAbbatK2) {
        this.redtTassAbbatK2 = redtTassAbbatK2;
    }

    public short getRedtTassAbbatK2() {
        return this.redtTassAbbatK2;
    }

    public void setImpbIsK3(short impbIsK3) {
        this.impbIsK3 = impbIsK3;
    }

    public short getImpbIsK3() {
        return this.impbIsK3;
    }

    public void setImpstSostK3(short impstSostK3) {
        this.impstSostK3 = impstSostK3;
    }

    public short getImpstSostK3() {
        return this.impstSostK3;
    }

    public void setImpb252K3(short impb252K3) {
        this.impb252K3 = impb252K3;
    }

    public short getImpb252K3() {
        return this.impb252K3;
    }

    public void setImpst252K3(short impst252K3) {
        this.impst252K3 = impst252K3;
    }

    public short getImpst252K3() {
        return this.impst252K3;
    }

    public void setImpbIsK3Antic(short impbIsK3Antic) {
        this.impbIsK3Antic = impbIsK3Antic;
    }

    public short getImpbIsK3Antic() {
        return this.impbIsK3Antic;
    }

    public void setImpstSostK3Anti(short impstSostK3Anti) {
        this.impstSostK3Anti = impstSostK3Anti;
    }

    public short getImpstSostK3Anti() {
        return this.impstSostK3Anti;
    }

    public void setImpbIrpefK1Anti(short impbIrpefK1Anti) {
        this.impbIrpefK1Anti = impbIrpefK1Anti;
    }

    public short getImpbIrpefK1Anti() {
        return this.impbIrpefK1Anti;
    }

    public void setImpstIrpefK1Ant(short impstIrpefK1Ant) {
        this.impstIrpefK1Ant = impstIrpefK1Ant;
    }

    public short getImpstIrpefK1Ant() {
        return this.impstIrpefK1Ant;
    }

    public void setImpbIrpefK2Anti(short impbIrpefK2Anti) {
        this.impbIrpefK2Anti = impbIrpefK2Anti;
    }

    public short getImpbIrpefK2Anti() {
        return this.impbIrpefK2Anti;
    }

    public void setImpstIrpefK2Ant(short impstIrpefK2Ant) {
        this.impstIrpefK2Ant = impstIrpefK2Ant;
    }

    public short getImpstIrpefK2Ant() {
        return this.impstIrpefK2Ant;
    }

    public void setDtCessazione(short dtCessazione) {
        this.dtCessazione = dtCessazione;
    }

    public short getDtCessazione() {
        return this.dtCessazione;
    }

    public void setTotImpst(short totImpst) {
        this.totImpst = totImpst;
    }

    public short getTotImpst() {
        return this.totImpst;
    }

    public void setOnerTrasfe(short onerTrasfe) {
        this.onerTrasfe = onerTrasfe;
    }

    public short getOnerTrasfe() {
        return this.onerTrasfe;
    }

    public void setImpNetTrasferito(short impNetTrasferito) {
        this.impNetTrasferito = impNetTrasferito;
    }

    public short getImpNetTrasferito() {
        return this.impNetTrasferito;
    }
}
