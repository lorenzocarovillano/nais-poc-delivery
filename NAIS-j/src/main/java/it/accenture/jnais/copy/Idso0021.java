package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.Idso0021StrutturaDato;

/**Original name: IDSO0021<br>
 * Variable: IDSO0021 from copybook IDSO0021<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idso0021 {

    //==== PROPERTIES ====
    //Original name: IDSO0021-LIMITE-STR-MAX
    private String idso0021LimiteStrMax = "00250";
    //Original name: IDSO0021-NUM-ELEM
    private String idso0021NumElem = "00000";
    //Original name: IDSO0021-STRUTTURA-DATO
    private Idso0021StrutturaDato idso0021StrutturaDato = new Idso0021StrutturaDato();
    //Original name: IDSO0021-AREA-ERRORI
    private Idso0021AreaErrori idso0021AreaErrori = new Idso0021AreaErrori();

    //==== METHODS ====
    public void initIdso0021HighValues() {
        initIdso0021AreaHighValues();
    }

    public void initIdso0021Spaces() {
        initIdso0021AreaSpaces();
    }

    public void setIdso0021AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        idso0021LimiteStrMax = MarshalByte.readFixedString(buffer, position, Len.IDSO0021_LIMITE_STR_MAX);
        position += Len.IDSO0021_LIMITE_STR_MAX;
        idso0021NumElem = MarshalByte.readFixedString(buffer, position, Len.IDSO0021_NUM_ELEM);
        position += Len.IDSO0021_NUM_ELEM;
        setIdso0021OutputBytes(buffer, position);
    }

    public byte[] getIdso0021AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, idso0021LimiteStrMax, Len.IDSO0021_LIMITE_STR_MAX);
        position += Len.IDSO0021_LIMITE_STR_MAX;
        MarshalByte.writeString(buffer, position, idso0021NumElem, Len.IDSO0021_NUM_ELEM);
        position += Len.IDSO0021_NUM_ELEM;
        getIdso0021OutputBytes(buffer, position);
        return buffer;
    }

    public void initIdso0021AreaHighValues() {
        idso0021LimiteStrMax = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.IDSO0021_LIMITE_STR_MAX);
        idso0021NumElem = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.IDSO0021_NUM_ELEM);
        initIdso0021OutputHighValues();
    }

    public void initIdso0021AreaSpaces() {
        idso0021LimiteStrMax = "";
        idso0021NumElem = "";
        initIdso0021OutputSpaces();
    }

    public void setIdso0021LimiteStrMax(int idso0021LimiteStrMax) {
        this.idso0021LimiteStrMax = NumericDisplay.asString(idso0021LimiteStrMax, Len.IDSO0021_LIMITE_STR_MAX);
    }

    public void setIdso0021LimiteStrMaxFormatted(String idso0021LimiteStrMax) {
        this.idso0021LimiteStrMax = Trunc.toUnsignedNumeric(idso0021LimiteStrMax, Len.IDSO0021_LIMITE_STR_MAX);
    }

    public int getIdso0021LimiteStrMax() {
        return NumericDisplay.asInt(this.idso0021LimiteStrMax);
    }

    public void setIdso0021NumElem(int idso0021NumElem) {
        this.idso0021NumElem = NumericDisplay.asString(idso0021NumElem, Len.IDSO0021_NUM_ELEM);
    }

    public void setIdso0021NumElemFormatted(String idso0021NumElem) {
        this.idso0021NumElem = Trunc.toUnsignedNumeric(idso0021NumElem, Len.IDSO0021_NUM_ELEM);
    }

    public int getIdso0021NumElem() {
        return NumericDisplay.asInt(this.idso0021NumElem);
    }

    public void setIdso0021OutputBytes(byte[] buffer, int offset) {
        int position = offset;
        idso0021StrutturaDato.setIdso0021StrutturaDatoBytes(buffer, position);
        position += Idso0021StrutturaDato.Len.IDSO0021_STRUTTURA_DATO;
        idso0021AreaErrori.setIdso0021AreaErroriBytes(buffer, position);
    }

    public byte[] getIdso0021OutputBytes(byte[] buffer, int offset) {
        int position = offset;
        idso0021StrutturaDato.getIdso0021StrutturaDatoBytes(buffer, position);
        position += Idso0021StrutturaDato.Len.IDSO0021_STRUTTURA_DATO;
        idso0021AreaErrori.getIdso0021AreaErroriBytes(buffer, position);
        return buffer;
    }

    public void initIdso0021OutputHighValues() {
        idso0021StrutturaDato.initIdso0021StrutturaDatoHighValues();
        idso0021AreaErrori.initIdso0021AreaErroriHighValues();
    }

    public void initIdso0021OutputSpaces() {
        idso0021StrutturaDato.initIdso0021StrutturaDatoSpaces();
        idso0021AreaErrori.initIdso0021AreaErroriSpaces();
    }

    public Idso0021AreaErrori getIdso0021AreaErrori() {
        return idso0021AreaErrori;
    }

    public Idso0021StrutturaDato getIdso0021StrutturaDato() {
        return idso0021StrutturaDato;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSO0021_LIMITE_STR_MAX = 5;
        public static final int IDSO0021_NUM_ELEM = 5;
        public static final int IDSO0021_OUTPUT = Idso0021StrutturaDato.Len.IDSO0021_STRUTTURA_DATO + Idso0021AreaErrori.Len.IDSO0021_AREA_ERRORI;
        public static final int IDSO0021_AREA = IDSO0021_LIMITE_STR_MAX + IDSO0021_NUM_ELEM + IDSO0021_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
