package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WspgIdGar;
import it.accenture.jnais.ws.redefines.WspgIdMoviChiu;
import it.accenture.jnais.ws.redefines.WspgPcSopram;
import it.accenture.jnais.ws.redefines.WspgValImp;
import it.accenture.jnais.ws.redefines.WspgValPc;

/**Original name: WSPG-DATI<br>
 * Variable: WSPG-DATI from copybook LCCVSPG1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WspgDati {

    //==== PROPERTIES ====
    //Original name: WSPG-ID-SOPR-DI-GAR
    private int wspgIdSoprDiGar = DefaultValues.INT_VAL;
    //Original name: WSPG-ID-GAR
    private WspgIdGar wspgIdGar = new WspgIdGar();
    //Original name: WSPG-ID-MOVI-CRZ
    private int wspgIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WSPG-ID-MOVI-CHIU
    private WspgIdMoviChiu wspgIdMoviChiu = new WspgIdMoviChiu();
    //Original name: WSPG-DT-INI-EFF
    private int wspgDtIniEff = DefaultValues.INT_VAL;
    //Original name: WSPG-DT-END-EFF
    private int wspgDtEndEff = DefaultValues.INT_VAL;
    //Original name: WSPG-COD-COMP-ANIA
    private int wspgCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WSPG-COD-SOPR
    private String wspgCodSopr = DefaultValues.stringVal(Len.WSPG_COD_SOPR);
    //Original name: WSPG-TP-D
    private String wspgTpD = DefaultValues.stringVal(Len.WSPG_TP_D);
    //Original name: WSPG-VAL-PC
    private WspgValPc wspgValPc = new WspgValPc();
    //Original name: WSPG-VAL-IMP
    private WspgValImp wspgValImp = new WspgValImp();
    //Original name: WSPG-PC-SOPRAM
    private WspgPcSopram wspgPcSopram = new WspgPcSopram();
    //Original name: WSPG-FL-ESCL-SOPR
    private char wspgFlEsclSopr = DefaultValues.CHAR_VAL;
    //Original name: WSPG-DESC-ESCL
    private String wspgDescEscl = DefaultValues.stringVal(Len.WSPG_DESC_ESCL);
    //Original name: WSPG-DS-RIGA
    private long wspgDsRiga = DefaultValues.LONG_VAL;
    //Original name: WSPG-DS-OPER-SQL
    private char wspgDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WSPG-DS-VER
    private int wspgDsVer = DefaultValues.INT_VAL;
    //Original name: WSPG-DS-TS-INI-CPTZ
    private long wspgDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WSPG-DS-TS-END-CPTZ
    private long wspgDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WSPG-DS-UTENTE
    private String wspgDsUtente = DefaultValues.stringVal(Len.WSPG_DS_UTENTE);
    //Original name: WSPG-DS-STATO-ELAB
    private char wspgDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wspgIdSoprDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSPG_ID_SOPR_DI_GAR, 0);
        position += Len.WSPG_ID_SOPR_DI_GAR;
        wspgIdGar.setWspgIdGarFromBuffer(buffer, position);
        position += WspgIdGar.Len.WSPG_ID_GAR;
        wspgIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSPG_ID_MOVI_CRZ, 0);
        position += Len.WSPG_ID_MOVI_CRZ;
        wspgIdMoviChiu.setWspgIdMoviChiuFromBuffer(buffer, position);
        position += WspgIdMoviChiu.Len.WSPG_ID_MOVI_CHIU;
        wspgDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSPG_DT_INI_EFF, 0);
        position += Len.WSPG_DT_INI_EFF;
        wspgDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSPG_DT_END_EFF, 0);
        position += Len.WSPG_DT_END_EFF;
        wspgCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSPG_COD_COMP_ANIA, 0);
        position += Len.WSPG_COD_COMP_ANIA;
        wspgCodSopr = MarshalByte.readString(buffer, position, Len.WSPG_COD_SOPR);
        position += Len.WSPG_COD_SOPR;
        wspgTpD = MarshalByte.readString(buffer, position, Len.WSPG_TP_D);
        position += Len.WSPG_TP_D;
        wspgValPc.setWspgValPcFromBuffer(buffer, position);
        position += WspgValPc.Len.WSPG_VAL_PC;
        wspgValImp.setWspgValImpFromBuffer(buffer, position);
        position += WspgValImp.Len.WSPG_VAL_IMP;
        wspgPcSopram.setWspgPcSopramFromBuffer(buffer, position);
        position += WspgPcSopram.Len.WSPG_PC_SOPRAM;
        wspgFlEsclSopr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wspgDescEscl = MarshalByte.readString(buffer, position, Len.WSPG_DESC_ESCL);
        position += Len.WSPG_DESC_ESCL;
        wspgDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WSPG_DS_RIGA, 0);
        position += Len.WSPG_DS_RIGA;
        wspgDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wspgDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSPG_DS_VER, 0);
        position += Len.WSPG_DS_VER;
        wspgDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WSPG_DS_TS_INI_CPTZ, 0);
        position += Len.WSPG_DS_TS_INI_CPTZ;
        wspgDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WSPG_DS_TS_END_CPTZ, 0);
        position += Len.WSPG_DS_TS_END_CPTZ;
        wspgDsUtente = MarshalByte.readString(buffer, position, Len.WSPG_DS_UTENTE);
        position += Len.WSPG_DS_UTENTE;
        wspgDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wspgIdSoprDiGar, Len.Int.WSPG_ID_SOPR_DI_GAR, 0);
        position += Len.WSPG_ID_SOPR_DI_GAR;
        wspgIdGar.getWspgIdGarAsBuffer(buffer, position);
        position += WspgIdGar.Len.WSPG_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, wspgIdMoviCrz, Len.Int.WSPG_ID_MOVI_CRZ, 0);
        position += Len.WSPG_ID_MOVI_CRZ;
        wspgIdMoviChiu.getWspgIdMoviChiuAsBuffer(buffer, position);
        position += WspgIdMoviChiu.Len.WSPG_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wspgDtIniEff, Len.Int.WSPG_DT_INI_EFF, 0);
        position += Len.WSPG_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wspgDtEndEff, Len.Int.WSPG_DT_END_EFF, 0);
        position += Len.WSPG_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wspgCodCompAnia, Len.Int.WSPG_COD_COMP_ANIA, 0);
        position += Len.WSPG_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wspgCodSopr, Len.WSPG_COD_SOPR);
        position += Len.WSPG_COD_SOPR;
        MarshalByte.writeString(buffer, position, wspgTpD, Len.WSPG_TP_D);
        position += Len.WSPG_TP_D;
        wspgValPc.getWspgValPcAsBuffer(buffer, position);
        position += WspgValPc.Len.WSPG_VAL_PC;
        wspgValImp.getWspgValImpAsBuffer(buffer, position);
        position += WspgValImp.Len.WSPG_VAL_IMP;
        wspgPcSopram.getWspgPcSopramAsBuffer(buffer, position);
        position += WspgPcSopram.Len.WSPG_PC_SOPRAM;
        MarshalByte.writeChar(buffer, position, wspgFlEsclSopr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wspgDescEscl, Len.WSPG_DESC_ESCL);
        position += Len.WSPG_DESC_ESCL;
        MarshalByte.writeLongAsPacked(buffer, position, wspgDsRiga, Len.Int.WSPG_DS_RIGA, 0);
        position += Len.WSPG_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wspgDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wspgDsVer, Len.Int.WSPG_DS_VER, 0);
        position += Len.WSPG_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wspgDsTsIniCptz, Len.Int.WSPG_DS_TS_INI_CPTZ, 0);
        position += Len.WSPG_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wspgDsTsEndCptz, Len.Int.WSPG_DS_TS_END_CPTZ, 0);
        position += Len.WSPG_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wspgDsUtente, Len.WSPG_DS_UTENTE);
        position += Len.WSPG_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wspgDsStatoElab);
        return buffer;
    }

    public void initDatiSpaces() {
        wspgIdSoprDiGar = Types.INVALID_INT_VAL;
        wspgIdGar.initWspgIdGarSpaces();
        wspgIdMoviCrz = Types.INVALID_INT_VAL;
        wspgIdMoviChiu.initWspgIdMoviChiuSpaces();
        wspgDtIniEff = Types.INVALID_INT_VAL;
        wspgDtEndEff = Types.INVALID_INT_VAL;
        wspgCodCompAnia = Types.INVALID_INT_VAL;
        wspgCodSopr = "";
        wspgTpD = "";
        wspgValPc.initWspgValPcSpaces();
        wspgValImp.initWspgValImpSpaces();
        wspgPcSopram.initWspgPcSopramSpaces();
        wspgFlEsclSopr = Types.SPACE_CHAR;
        wspgDescEscl = "";
        wspgDsRiga = Types.INVALID_LONG_VAL;
        wspgDsOperSql = Types.SPACE_CHAR;
        wspgDsVer = Types.INVALID_INT_VAL;
        wspgDsTsIniCptz = Types.INVALID_LONG_VAL;
        wspgDsTsEndCptz = Types.INVALID_LONG_VAL;
        wspgDsUtente = "";
        wspgDsStatoElab = Types.SPACE_CHAR;
    }

    public void setWspgIdSoprDiGar(int wspgIdSoprDiGar) {
        this.wspgIdSoprDiGar = wspgIdSoprDiGar;
    }

    public int getWspgIdSoprDiGar() {
        return this.wspgIdSoprDiGar;
    }

    public void setWspgIdMoviCrz(int wspgIdMoviCrz) {
        this.wspgIdMoviCrz = wspgIdMoviCrz;
    }

    public int getWspgIdMoviCrz() {
        return this.wspgIdMoviCrz;
    }

    public void setWspgDtIniEff(int wspgDtIniEff) {
        this.wspgDtIniEff = wspgDtIniEff;
    }

    public int getWspgDtIniEff() {
        return this.wspgDtIniEff;
    }

    public void setWspgDtEndEff(int wspgDtEndEff) {
        this.wspgDtEndEff = wspgDtEndEff;
    }

    public int getWspgDtEndEff() {
        return this.wspgDtEndEff;
    }

    public void setWspgCodCompAnia(int wspgCodCompAnia) {
        this.wspgCodCompAnia = wspgCodCompAnia;
    }

    public int getWspgCodCompAnia() {
        return this.wspgCodCompAnia;
    }

    public void setWspgCodSopr(String wspgCodSopr) {
        this.wspgCodSopr = Functions.subString(wspgCodSopr, Len.WSPG_COD_SOPR);
    }

    public String getWspgCodSopr() {
        return this.wspgCodSopr;
    }

    public void setWspgTpD(String wspgTpD) {
        this.wspgTpD = Functions.subString(wspgTpD, Len.WSPG_TP_D);
    }

    public String getWspgTpD() {
        return this.wspgTpD;
    }

    public void setWspgFlEsclSopr(char wspgFlEsclSopr) {
        this.wspgFlEsclSopr = wspgFlEsclSopr;
    }

    public char getWspgFlEsclSopr() {
        return this.wspgFlEsclSopr;
    }

    public void setWspgDescEscl(String wspgDescEscl) {
        this.wspgDescEscl = Functions.subString(wspgDescEscl, Len.WSPG_DESC_ESCL);
    }

    public String getWspgDescEscl() {
        return this.wspgDescEscl;
    }

    public void setWspgDsRiga(long wspgDsRiga) {
        this.wspgDsRiga = wspgDsRiga;
    }

    public long getWspgDsRiga() {
        return this.wspgDsRiga;
    }

    public void setWspgDsOperSql(char wspgDsOperSql) {
        this.wspgDsOperSql = wspgDsOperSql;
    }

    public char getWspgDsOperSql() {
        return this.wspgDsOperSql;
    }

    public void setWspgDsVer(int wspgDsVer) {
        this.wspgDsVer = wspgDsVer;
    }

    public int getWspgDsVer() {
        return this.wspgDsVer;
    }

    public void setWspgDsTsIniCptz(long wspgDsTsIniCptz) {
        this.wspgDsTsIniCptz = wspgDsTsIniCptz;
    }

    public long getWspgDsTsIniCptz() {
        return this.wspgDsTsIniCptz;
    }

    public void setWspgDsTsEndCptz(long wspgDsTsEndCptz) {
        this.wspgDsTsEndCptz = wspgDsTsEndCptz;
    }

    public long getWspgDsTsEndCptz() {
        return this.wspgDsTsEndCptz;
    }

    public void setWspgDsUtente(String wspgDsUtente) {
        this.wspgDsUtente = Functions.subString(wspgDsUtente, Len.WSPG_DS_UTENTE);
    }

    public String getWspgDsUtente() {
        return this.wspgDsUtente;
    }

    public void setWspgDsStatoElab(char wspgDsStatoElab) {
        this.wspgDsStatoElab = wspgDsStatoElab;
    }

    public char getWspgDsStatoElab() {
        return this.wspgDsStatoElab;
    }

    public WspgIdGar getWspgIdGar() {
        return wspgIdGar;
    }

    public WspgIdMoviChiu getWspgIdMoviChiu() {
        return wspgIdMoviChiu;
    }

    public WspgPcSopram getWspgPcSopram() {
        return wspgPcSopram;
    }

    public WspgValImp getWspgValImp() {
        return wspgValImp;
    }

    public WspgValPc getWspgValPc() {
        return wspgValPc;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WSPG_ID_SOPR_DI_GAR = 5;
        public static final int WSPG_ID_MOVI_CRZ = 5;
        public static final int WSPG_DT_INI_EFF = 5;
        public static final int WSPG_DT_END_EFF = 5;
        public static final int WSPG_COD_COMP_ANIA = 3;
        public static final int WSPG_COD_SOPR = 12;
        public static final int WSPG_TP_D = 2;
        public static final int WSPG_FL_ESCL_SOPR = 1;
        public static final int WSPG_DESC_ESCL = 100;
        public static final int WSPG_DS_RIGA = 6;
        public static final int WSPG_DS_OPER_SQL = 1;
        public static final int WSPG_DS_VER = 5;
        public static final int WSPG_DS_TS_INI_CPTZ = 10;
        public static final int WSPG_DS_TS_END_CPTZ = 10;
        public static final int WSPG_DS_UTENTE = 20;
        public static final int WSPG_DS_STATO_ELAB = 1;
        public static final int DATI = WSPG_ID_SOPR_DI_GAR + WspgIdGar.Len.WSPG_ID_GAR + WSPG_ID_MOVI_CRZ + WspgIdMoviChiu.Len.WSPG_ID_MOVI_CHIU + WSPG_DT_INI_EFF + WSPG_DT_END_EFF + WSPG_COD_COMP_ANIA + WSPG_COD_SOPR + WSPG_TP_D + WspgValPc.Len.WSPG_VAL_PC + WspgValImp.Len.WSPG_VAL_IMP + WspgPcSopram.Len.WSPG_PC_SOPRAM + WSPG_FL_ESCL_SOPR + WSPG_DESC_ESCL + WSPG_DS_RIGA + WSPG_DS_OPER_SQL + WSPG_DS_VER + WSPG_DS_TS_INI_CPTZ + WSPG_DS_TS_END_CPTZ + WSPG_DS_UTENTE + WSPG_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WSPG_ID_SOPR_DI_GAR = 9;
            public static final int WSPG_ID_MOVI_CRZ = 9;
            public static final int WSPG_DT_INI_EFF = 8;
            public static final int WSPG_DT_END_EFF = 8;
            public static final int WSPG_COD_COMP_ANIA = 5;
            public static final int WSPG_DS_RIGA = 10;
            public static final int WSPG_DS_VER = 9;
            public static final int WSPG_DS_TS_INI_CPTZ = 18;
            public static final int WSPG_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
