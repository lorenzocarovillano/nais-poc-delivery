package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.commons.data.to.ILinguaErrore;

/**Original name: LINGUA-ERRORE<br>
 * Variable: LINGUA-ERRORE from copybook IDBVLER1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class LinguaErrore implements ILinguaErrore {

    //==== PROPERTIES ====
    //Original name: LER-LINGUA
    private String lingua = DefaultValues.stringVal(Len.LINGUA);
    //Original name: LER-PAESE
    private String paese = DefaultValues.stringVal(Len.PAESE);
    //Original name: LER-DESC-ERRORE-BREVE-LEN
    private short descErroreBreveLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: LER-DESC-ERRORE-BREVE
    private String descErroreBreve = DefaultValues.stringVal(Len.DESC_ERRORE_BREVE);
    //Original name: LER-DESC-ERRORE-ESTESA-LEN
    private short descErroreEstesaLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: LER-DESC-ERRORE-ESTESA
    private String descErroreEstesa = DefaultValues.stringVal(Len.DESC_ERRORE_ESTESA);

    //==== METHODS ====
    public void setLingua(String lingua) {
        this.lingua = Functions.subString(lingua, Len.LINGUA);
    }

    public String getLingua() {
        return this.lingua;
    }

    public void setPaese(String paese) {
        this.paese = Functions.subString(paese, Len.PAESE);
    }

    public String getPaese() {
        return this.paese;
    }

    public void setDescErroreBreveVcharFormatted(String data) {
        byte[] buffer = new byte[Len.DESC_ERRORE_BREVE_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.DESC_ERRORE_BREVE_VCHAR);
        setDescErroreBreveVcharBytes(buffer, 1);
    }

    public String getDescErroreBreveVcharFormatted() {
        return MarshalByteExt.bufferToStr(getDescErroreBreveVcharBytes());
    }

    /**Original name: LER-DESC-ERRORE-BREVE-VCHAR<br>*/
    public byte[] getDescErroreBreveVcharBytes() {
        byte[] buffer = new byte[Len.DESC_ERRORE_BREVE_VCHAR];
        return getDescErroreBreveVcharBytes(buffer, 1);
    }

    public void setDescErroreBreveVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        descErroreBreveLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        descErroreBreve = MarshalByte.readString(buffer, position, Len.DESC_ERRORE_BREVE);
    }

    public byte[] getDescErroreBreveVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, descErroreBreveLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, descErroreBreve, Len.DESC_ERRORE_BREVE);
        return buffer;
    }

    public void setDescErroreBreveLen(short descErroreBreveLen) {
        this.descErroreBreveLen = descErroreBreveLen;
    }

    public short getDescErroreBreveLen() {
        return this.descErroreBreveLen;
    }

    public void setDescErroreBreve(String descErroreBreve) {
        this.descErroreBreve = Functions.subString(descErroreBreve, Len.DESC_ERRORE_BREVE);
    }

    public String getDescErroreBreve() {
        return this.descErroreBreve;
    }

    public void setDescErroreEstesaVcharFormatted(String data) {
        byte[] buffer = new byte[Len.DESC_ERRORE_ESTESA_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.DESC_ERRORE_ESTESA_VCHAR);
        setDescErroreEstesaVcharBytes(buffer, 1);
    }

    public String getDescErroreEstesaVcharFormatted() {
        return MarshalByteExt.bufferToStr(getDescErroreEstesaVcharBytes());
    }

    /**Original name: LER-DESC-ERRORE-ESTESA-VCHAR<br>*/
    public byte[] getDescErroreEstesaVcharBytes() {
        byte[] buffer = new byte[Len.DESC_ERRORE_ESTESA_VCHAR];
        return getDescErroreEstesaVcharBytes(buffer, 1);
    }

    public void setDescErroreEstesaVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        descErroreEstesaLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        descErroreEstesa = MarshalByte.readString(buffer, position, Len.DESC_ERRORE_ESTESA);
    }

    public byte[] getDescErroreEstesaVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, descErroreEstesaLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, descErroreEstesa, Len.DESC_ERRORE_ESTESA);
        return buffer;
    }

    public void setDescErroreEstesaLen(short descErroreEstesaLen) {
        this.descErroreEstesaLen = descErroreEstesaLen;
    }

    public short getDescErroreEstesaLen() {
        return this.descErroreEstesaLen;
    }

    public void setDescErroreEstesa(String descErroreEstesa) {
        this.descErroreEstesa = Functions.subString(descErroreEstesa, Len.DESC_ERRORE_ESTESA);
    }

    public String getDescErroreEstesa() {
        return this.descErroreEstesa;
    }

    @Override
    public String getBreveVchar() {
        return getDescErroreBreveVcharFormatted();
    }

    @Override
    public void setBreveVchar(String breveVchar) {
        this.setDescErroreBreveVcharFormatted(breveVchar);
    }

    @Override
    public String getEstesaVchar() {
        return getDescErroreEstesaVcharFormatted();
    }

    @Override
    public void setEstesaVchar(String estesaVchar) {
        this.setDescErroreEstesaVcharFormatted(estesaVchar);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESC_ERRORE_BREVE_LEN = 2;
        public static final int DESC_ERRORE_BREVE = 100;
        public static final int DESC_ERRORE_BREVE_VCHAR = DESC_ERRORE_BREVE_LEN + DESC_ERRORE_BREVE;
        public static final int DESC_ERRORE_ESTESA_LEN = 2;
        public static final int DESC_ERRORE_ESTESA = 200;
        public static final int DESC_ERRORE_ESTESA_VCHAR = DESC_ERRORE_ESTESA_LEN + DESC_ERRORE_ESTESA;
        public static final int LINGUA = 2;
        public static final int PAESE = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
