package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-PARAM-COMP<br>
 * Variable: IND-PARAM-COMP from copybook IDBVPCO2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndParamComp {

    //==== PROPERTIES ====
    //Original name: IND-PCO-COD-TRAT-CIRT
    private short codTratCirt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-LIM-VLTR
    private short limVltr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-TP-RAT-PERF
    private short tpRatPerf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-ARROT-PRE
    private short arrotPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-CONT
    private short dtCont = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-RIVAL-IN
    private short dtUltRivalIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-QTZO-IN
    private short dtUltQtzoIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-RICL-RIASS
    private short dtUltRiclRiass = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-TABUL-RIASS
    private short dtUltTabulRiass = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-EMES
    private short dtUltBollEmes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-STOR
    private short dtUltBollStor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-LIQ
    private short dtUltBollLiq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-RIAT
    private short dtUltBollRiat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTELRISCPAR-PR
    private short dtUltelriscparPr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-IS-IN
    private short dtUltcIsIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-RICL-PRE
    private short dtUltRiclPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-MARSOL
    private short dtUltcMarsol = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-RB-IN
    private short dtUltcRbIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-PC-PROV-1AA-ACQ
    private short pcProv1aaAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-MOD-INTR-PREST
    private short modIntrPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-GG-MAX-REC-PROV
    private short ggMaxRecProv = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTGZ-TRCH-E-IN
    private short dtUltgzTrchEIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-SNDEN
    private short dtUltBollSnden = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-SNDNLQ
    private short dtUltBollSndnlq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTSC-ELAB-IN
    private short dtUltscElabIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTSC-OPZ-IN
    private short dtUltscOpzIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-BNSRIC-IN
    private short dtUltcBnsricIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-BNSFDT-IN
    private short dtUltcBnsfdtIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-RINN-GARAC
    private short dtUltRinnGarac = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTGZ-CED
    private short dtUltgzCed = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-PRLCOS
    private short dtUltElabPrlcos = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-RINN-COLL
    private short dtUltRinnColl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-FL-RVC-PERF
    private short flRvcPerf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-FL-RCS-POLI-NOPERF
    private short flRcsPoliNoperf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-FL-GEST-PLUSV
    private short flGestPlusv = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-RIVAL-CL
    private short dtUltRivalCl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-QTZO-CL
    private short dtUltQtzoCl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-BNSRIC-CL
    private short dtUltcBnsricCl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-BNSFDT-CL
    private short dtUltcBnsfdtCl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-IS-CL
    private short dtUltcIsCl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-RB-CL
    private short dtUltcRbCl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTGZ-TRCH-E-CL
    private short dtUltgzTrchECl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTSC-ELAB-CL
    private short dtUltscElabCl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTSC-OPZ-CL
    private short dtUltscOpzCl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-STST-X-REGIONE
    private short ststXRegione = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTGZ-CED-COLL
    private short dtUltgzCedColl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-NUM-MM-CALC-MORA
    private short numMmCalcMora = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-EC-RIV-COLL
    private short dtUltEcRivColl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-EC-RIV-IND
    private short dtUltEcRivInd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-EC-IL-COLL
    private short dtUltEcIlColl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-EC-IL-IND
    private short dtUltEcIlInd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-EC-UL-COLL
    private short dtUltEcUlColl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-EC-UL-IND
    private short dtUltEcUlInd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-AA-UTI
    private short aaUti = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-PERF-C
    private short dtUltBollPerfC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-RSP-IN
    private short dtUltBollRspIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-RSP-CL
    private short dtUltBollRspCl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-EMES-I
    private short dtUltBollEmesI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-STOR-I
    private short dtUltBollStorI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-RIAT-I
    private short dtUltBollRiatI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-SD-I
    private short dtUltBollSdI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-SDNL-I
    private short dtUltBollSdnlI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-PERF-I
    private short dtUltBollPerfI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-RICL-RIRIAS-COM
    private short dtRiclRiriasCom = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-AT92-C
    private short dtUltElabAt92C = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-AT92-I
    private short dtUltElabAt92I = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-AT93-C
    private short dtUltElabAt93C = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-AT93-I
    private short dtUltElabAt93I = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-SPE-IN
    private short dtUltElabSpeIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-PR-CON
    private short dtUltElabPrCon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-RP-CL
    private short dtUltBollRpCl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-RP-IN
    private short dtUltBollRpIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-PRE-I
    private short dtUltBollPreI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-PRE-C
    private short dtUltBollPreC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-PILDI-MM-C
    private short dtUltcPildiMmC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-PILDI-AA-C
    private short dtUltcPildiAaC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-PILDI-MM-I
    private short dtUltcPildiMmI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-PILDI-TR-I
    private short dtUltcPildiTrI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULTC-PILDI-AA-I
    private short dtUltcPildiAaI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-QUIE-C
    private short dtUltBollQuieC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-QUIE-I
    private short dtUltBollQuieI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-COTR-I
    private short dtUltBollCotrI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-COTR-C
    private short dtUltBollCotrC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-CORI-C
    private short dtUltBollCoriC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-BOLL-CORI-I
    private short dtUltBollCoriI = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-TP-VALZZ-DT-VLT
    private short tpValzzDtVlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-FL-FRAZ-PROV-ACQ
    private short flFrazProvAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-AGG-EROG-RE
    private short dtUltAggErogRe = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-PC-RM-MARSOL
    private short pcRmMarsol = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-PC-C-SUBRSH-MARSOL
    private short pcCSubrshMarsol = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-COD-COMP-ISVAP
    private short codCompIsvap = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-LM-RIS-CON-INT
    private short lmRisConInt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-LM-C-SUBRSH-CON-IN
    private short lmCSubrshConIn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-PC-GAR-NORISK-MARS
    private short pcGarNoriskMars = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-CRZ-1A-RAT-INTR-PR
    private short crz1aRatIntrPr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-NUM-GG-ARR-INTR-PR
    private short numGgArrIntrPr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-FL-VISUAL-VINPG
    private short flVisualVinpg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ESTRAZ-FUG
    private short dtUltEstrazFug = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-PR-AUT
    private short dtUltElabPrAut = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-COMMEF
    private short dtUltElabCommef = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-LIQMEF
    private short dtUltElabLiqmef = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-COD-FISC-MEF
    private short codFiscMef = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-IMP-ASS-SOCIALE
    private short impAssSociale = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-MOD-COMNZ-INVST-SW
    private short modComnzInvstSw = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-RIAT-RIASS-RSH
    private short dtRiatRiassRsh = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-RIAT-RIASS-COMM
    private short dtRiatRiassComm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-GG-INTR-RIT-PAG
    private short ggIntrRitPag = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-RINN-TAC
    private short dtUltRinnTac = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DESC-COMP
    private short descComp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-EC-TCM-IND
    private short dtUltEcTcmInd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-EC-TCM-COLL
    private short dtUltEcTcmColl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-EC-MRM-IND
    private short dtUltEcMrmInd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-EC-MRM-COLL
    private short dtUltEcMrmColl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-COD-COMP-LDAP
    private short codCompLdap = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-PC-RID-IMP-1382011
    private short pcRidImp1382011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-PC-RID-IMP-662014
    private short pcRidImp662014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-SOGL-AML-PRE-UNI
    private short soglAmlPreUni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-SOGL-AML-PRE-PER
    private short soglAmlPrePer = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-COD-SOGG-FTZ-ASSTO
    private short codSoggFtzAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-REDPRO
    private short dtUltElabRedpro = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-TAKE-P
    private short dtUltElabTakeP = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-PASPAS
    private short dtUltElabPaspas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-SOGL-AML-PRE-SAV-R
    private short soglAmlPreSavR = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ESTR-DEC-CO
    private short dtUltEstrDecCo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-COS-AT
    private short dtUltElabCosAt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-FRQ-COSTI-ATT
    private short frqCostiAtt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ULT-ELAB-COS-ST
    private short dtUltElabCosSt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-FRQ-COSTI-STORNATI
    private short frqCostiStornati = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ESTR-ASS-MIN70A
    private short dtEstrAssMin70a = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PCO-DT-ESTR-ASS-MAG70A
    private short dtEstrAssMag70a = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setCodTratCirt(short codTratCirt) {
        this.codTratCirt = codTratCirt;
    }

    public short getCodTratCirt() {
        return this.codTratCirt;
    }

    public void setLimVltr(short limVltr) {
        this.limVltr = limVltr;
    }

    public short getLimVltr() {
        return this.limVltr;
    }

    public void setTpRatPerf(short tpRatPerf) {
        this.tpRatPerf = tpRatPerf;
    }

    public short getTpRatPerf() {
        return this.tpRatPerf;
    }

    public void setArrotPre(short arrotPre) {
        this.arrotPre = arrotPre;
    }

    public short getArrotPre() {
        return this.arrotPre;
    }

    public void setDtCont(short dtCont) {
        this.dtCont = dtCont;
    }

    public short getDtCont() {
        return this.dtCont;
    }

    public void setDtUltRivalIn(short dtUltRivalIn) {
        this.dtUltRivalIn = dtUltRivalIn;
    }

    public short getDtUltRivalIn() {
        return this.dtUltRivalIn;
    }

    public void setDtUltQtzoIn(short dtUltQtzoIn) {
        this.dtUltQtzoIn = dtUltQtzoIn;
    }

    public short getDtUltQtzoIn() {
        return this.dtUltQtzoIn;
    }

    public void setDtUltRiclRiass(short dtUltRiclRiass) {
        this.dtUltRiclRiass = dtUltRiclRiass;
    }

    public short getDtUltRiclRiass() {
        return this.dtUltRiclRiass;
    }

    public void setDtUltTabulRiass(short dtUltTabulRiass) {
        this.dtUltTabulRiass = dtUltTabulRiass;
    }

    public short getDtUltTabulRiass() {
        return this.dtUltTabulRiass;
    }

    public void setDtUltBollEmes(short dtUltBollEmes) {
        this.dtUltBollEmes = dtUltBollEmes;
    }

    public short getDtUltBollEmes() {
        return this.dtUltBollEmes;
    }

    public void setDtUltBollStor(short dtUltBollStor) {
        this.dtUltBollStor = dtUltBollStor;
    }

    public short getDtUltBollStor() {
        return this.dtUltBollStor;
    }

    public void setDtUltBollLiq(short dtUltBollLiq) {
        this.dtUltBollLiq = dtUltBollLiq;
    }

    public short getDtUltBollLiq() {
        return this.dtUltBollLiq;
    }

    public void setDtUltBollRiat(short dtUltBollRiat) {
        this.dtUltBollRiat = dtUltBollRiat;
    }

    public short getDtUltBollRiat() {
        return this.dtUltBollRiat;
    }

    public void setDtUltelriscparPr(short dtUltelriscparPr) {
        this.dtUltelriscparPr = dtUltelriscparPr;
    }

    public short getDtUltelriscparPr() {
        return this.dtUltelriscparPr;
    }

    public void setDtUltcIsIn(short dtUltcIsIn) {
        this.dtUltcIsIn = dtUltcIsIn;
    }

    public short getDtUltcIsIn() {
        return this.dtUltcIsIn;
    }

    public void setDtUltRiclPre(short dtUltRiclPre) {
        this.dtUltRiclPre = dtUltRiclPre;
    }

    public short getDtUltRiclPre() {
        return this.dtUltRiclPre;
    }

    public void setDtUltcMarsol(short dtUltcMarsol) {
        this.dtUltcMarsol = dtUltcMarsol;
    }

    public short getDtUltcMarsol() {
        return this.dtUltcMarsol;
    }

    public void setDtUltcRbIn(short dtUltcRbIn) {
        this.dtUltcRbIn = dtUltcRbIn;
    }

    public short getDtUltcRbIn() {
        return this.dtUltcRbIn;
    }

    public void setPcProv1aaAcq(short pcProv1aaAcq) {
        this.pcProv1aaAcq = pcProv1aaAcq;
    }

    public short getPcProv1aaAcq() {
        return this.pcProv1aaAcq;
    }

    public void setModIntrPrest(short modIntrPrest) {
        this.modIntrPrest = modIntrPrest;
    }

    public short getModIntrPrest() {
        return this.modIntrPrest;
    }

    public void setGgMaxRecProv(short ggMaxRecProv) {
        this.ggMaxRecProv = ggMaxRecProv;
    }

    public short getGgMaxRecProv() {
        return this.ggMaxRecProv;
    }

    public void setDtUltgzTrchEIn(short dtUltgzTrchEIn) {
        this.dtUltgzTrchEIn = dtUltgzTrchEIn;
    }

    public short getDtUltgzTrchEIn() {
        return this.dtUltgzTrchEIn;
    }

    public void setDtUltBollSnden(short dtUltBollSnden) {
        this.dtUltBollSnden = dtUltBollSnden;
    }

    public short getDtUltBollSnden() {
        return this.dtUltBollSnden;
    }

    public void setDtUltBollSndnlq(short dtUltBollSndnlq) {
        this.dtUltBollSndnlq = dtUltBollSndnlq;
    }

    public short getDtUltBollSndnlq() {
        return this.dtUltBollSndnlq;
    }

    public void setDtUltscElabIn(short dtUltscElabIn) {
        this.dtUltscElabIn = dtUltscElabIn;
    }

    public short getDtUltscElabIn() {
        return this.dtUltscElabIn;
    }

    public void setDtUltscOpzIn(short dtUltscOpzIn) {
        this.dtUltscOpzIn = dtUltscOpzIn;
    }

    public short getDtUltscOpzIn() {
        return this.dtUltscOpzIn;
    }

    public void setDtUltcBnsricIn(short dtUltcBnsricIn) {
        this.dtUltcBnsricIn = dtUltcBnsricIn;
    }

    public short getDtUltcBnsricIn() {
        return this.dtUltcBnsricIn;
    }

    public void setDtUltcBnsfdtIn(short dtUltcBnsfdtIn) {
        this.dtUltcBnsfdtIn = dtUltcBnsfdtIn;
    }

    public short getDtUltcBnsfdtIn() {
        return this.dtUltcBnsfdtIn;
    }

    public void setDtUltRinnGarac(short dtUltRinnGarac) {
        this.dtUltRinnGarac = dtUltRinnGarac;
    }

    public short getDtUltRinnGarac() {
        return this.dtUltRinnGarac;
    }

    public void setDtUltgzCed(short dtUltgzCed) {
        this.dtUltgzCed = dtUltgzCed;
    }

    public short getDtUltgzCed() {
        return this.dtUltgzCed;
    }

    public void setDtUltElabPrlcos(short dtUltElabPrlcos) {
        this.dtUltElabPrlcos = dtUltElabPrlcos;
    }

    public short getDtUltElabPrlcos() {
        return this.dtUltElabPrlcos;
    }

    public void setDtUltRinnColl(short dtUltRinnColl) {
        this.dtUltRinnColl = dtUltRinnColl;
    }

    public short getDtUltRinnColl() {
        return this.dtUltRinnColl;
    }

    public void setFlRvcPerf(short flRvcPerf) {
        this.flRvcPerf = flRvcPerf;
    }

    public short getFlRvcPerf() {
        return this.flRvcPerf;
    }

    public void setFlRcsPoliNoperf(short flRcsPoliNoperf) {
        this.flRcsPoliNoperf = flRcsPoliNoperf;
    }

    public short getFlRcsPoliNoperf() {
        return this.flRcsPoliNoperf;
    }

    public void setFlGestPlusv(short flGestPlusv) {
        this.flGestPlusv = flGestPlusv;
    }

    public short getFlGestPlusv() {
        return this.flGestPlusv;
    }

    public void setDtUltRivalCl(short dtUltRivalCl) {
        this.dtUltRivalCl = dtUltRivalCl;
    }

    public short getDtUltRivalCl() {
        return this.dtUltRivalCl;
    }

    public void setDtUltQtzoCl(short dtUltQtzoCl) {
        this.dtUltQtzoCl = dtUltQtzoCl;
    }

    public short getDtUltQtzoCl() {
        return this.dtUltQtzoCl;
    }

    public void setDtUltcBnsricCl(short dtUltcBnsricCl) {
        this.dtUltcBnsricCl = dtUltcBnsricCl;
    }

    public short getDtUltcBnsricCl() {
        return this.dtUltcBnsricCl;
    }

    public void setDtUltcBnsfdtCl(short dtUltcBnsfdtCl) {
        this.dtUltcBnsfdtCl = dtUltcBnsfdtCl;
    }

    public short getDtUltcBnsfdtCl() {
        return this.dtUltcBnsfdtCl;
    }

    public void setDtUltcIsCl(short dtUltcIsCl) {
        this.dtUltcIsCl = dtUltcIsCl;
    }

    public short getDtUltcIsCl() {
        return this.dtUltcIsCl;
    }

    public void setDtUltcRbCl(short dtUltcRbCl) {
        this.dtUltcRbCl = dtUltcRbCl;
    }

    public short getDtUltcRbCl() {
        return this.dtUltcRbCl;
    }

    public void setDtUltgzTrchECl(short dtUltgzTrchECl) {
        this.dtUltgzTrchECl = dtUltgzTrchECl;
    }

    public short getDtUltgzTrchECl() {
        return this.dtUltgzTrchECl;
    }

    public void setDtUltscElabCl(short dtUltscElabCl) {
        this.dtUltscElabCl = dtUltscElabCl;
    }

    public short getDtUltscElabCl() {
        return this.dtUltscElabCl;
    }

    public void setDtUltscOpzCl(short dtUltscOpzCl) {
        this.dtUltscOpzCl = dtUltscOpzCl;
    }

    public short getDtUltscOpzCl() {
        return this.dtUltscOpzCl;
    }

    public void setStstXRegione(short ststXRegione) {
        this.ststXRegione = ststXRegione;
    }

    public short getStstXRegione() {
        return this.ststXRegione;
    }

    public void setDtUltgzCedColl(short dtUltgzCedColl) {
        this.dtUltgzCedColl = dtUltgzCedColl;
    }

    public short getDtUltgzCedColl() {
        return this.dtUltgzCedColl;
    }

    public void setNumMmCalcMora(short numMmCalcMora) {
        this.numMmCalcMora = numMmCalcMora;
    }

    public short getNumMmCalcMora() {
        return this.numMmCalcMora;
    }

    public void setDtUltEcRivColl(short dtUltEcRivColl) {
        this.dtUltEcRivColl = dtUltEcRivColl;
    }

    public short getDtUltEcRivColl() {
        return this.dtUltEcRivColl;
    }

    public void setDtUltEcRivInd(short dtUltEcRivInd) {
        this.dtUltEcRivInd = dtUltEcRivInd;
    }

    public short getDtUltEcRivInd() {
        return this.dtUltEcRivInd;
    }

    public void setDtUltEcIlColl(short dtUltEcIlColl) {
        this.dtUltEcIlColl = dtUltEcIlColl;
    }

    public short getDtUltEcIlColl() {
        return this.dtUltEcIlColl;
    }

    public void setDtUltEcIlInd(short dtUltEcIlInd) {
        this.dtUltEcIlInd = dtUltEcIlInd;
    }

    public short getDtUltEcIlInd() {
        return this.dtUltEcIlInd;
    }

    public void setDtUltEcUlColl(short dtUltEcUlColl) {
        this.dtUltEcUlColl = dtUltEcUlColl;
    }

    public short getDtUltEcUlColl() {
        return this.dtUltEcUlColl;
    }

    public void setDtUltEcUlInd(short dtUltEcUlInd) {
        this.dtUltEcUlInd = dtUltEcUlInd;
    }

    public short getDtUltEcUlInd() {
        return this.dtUltEcUlInd;
    }

    public void setAaUti(short aaUti) {
        this.aaUti = aaUti;
    }

    public short getAaUti() {
        return this.aaUti;
    }

    public void setDtUltBollPerfC(short dtUltBollPerfC) {
        this.dtUltBollPerfC = dtUltBollPerfC;
    }

    public short getDtUltBollPerfC() {
        return this.dtUltBollPerfC;
    }

    public void setDtUltBollRspIn(short dtUltBollRspIn) {
        this.dtUltBollRspIn = dtUltBollRspIn;
    }

    public short getDtUltBollRspIn() {
        return this.dtUltBollRspIn;
    }

    public void setDtUltBollRspCl(short dtUltBollRspCl) {
        this.dtUltBollRspCl = dtUltBollRspCl;
    }

    public short getDtUltBollRspCl() {
        return this.dtUltBollRspCl;
    }

    public void setDtUltBollEmesI(short dtUltBollEmesI) {
        this.dtUltBollEmesI = dtUltBollEmesI;
    }

    public short getDtUltBollEmesI() {
        return this.dtUltBollEmesI;
    }

    public void setDtUltBollStorI(short dtUltBollStorI) {
        this.dtUltBollStorI = dtUltBollStorI;
    }

    public short getDtUltBollStorI() {
        return this.dtUltBollStorI;
    }

    public void setDtUltBollRiatI(short dtUltBollRiatI) {
        this.dtUltBollRiatI = dtUltBollRiatI;
    }

    public short getDtUltBollRiatI() {
        return this.dtUltBollRiatI;
    }

    public void setDtUltBollSdI(short dtUltBollSdI) {
        this.dtUltBollSdI = dtUltBollSdI;
    }

    public short getDtUltBollSdI() {
        return this.dtUltBollSdI;
    }

    public void setDtUltBollSdnlI(short dtUltBollSdnlI) {
        this.dtUltBollSdnlI = dtUltBollSdnlI;
    }

    public short getDtUltBollSdnlI() {
        return this.dtUltBollSdnlI;
    }

    public void setDtUltBollPerfI(short dtUltBollPerfI) {
        this.dtUltBollPerfI = dtUltBollPerfI;
    }

    public short getDtUltBollPerfI() {
        return this.dtUltBollPerfI;
    }

    public void setDtRiclRiriasCom(short dtRiclRiriasCom) {
        this.dtRiclRiriasCom = dtRiclRiriasCom;
    }

    public short getDtRiclRiriasCom() {
        return this.dtRiclRiriasCom;
    }

    public void setDtUltElabAt92C(short dtUltElabAt92C) {
        this.dtUltElabAt92C = dtUltElabAt92C;
    }

    public short getDtUltElabAt92C() {
        return this.dtUltElabAt92C;
    }

    public void setDtUltElabAt92I(short dtUltElabAt92I) {
        this.dtUltElabAt92I = dtUltElabAt92I;
    }

    public short getDtUltElabAt92I() {
        return this.dtUltElabAt92I;
    }

    public void setDtUltElabAt93C(short dtUltElabAt93C) {
        this.dtUltElabAt93C = dtUltElabAt93C;
    }

    public short getDtUltElabAt93C() {
        return this.dtUltElabAt93C;
    }

    public void setDtUltElabAt93I(short dtUltElabAt93I) {
        this.dtUltElabAt93I = dtUltElabAt93I;
    }

    public short getDtUltElabAt93I() {
        return this.dtUltElabAt93I;
    }

    public void setDtUltElabSpeIn(short dtUltElabSpeIn) {
        this.dtUltElabSpeIn = dtUltElabSpeIn;
    }

    public short getDtUltElabSpeIn() {
        return this.dtUltElabSpeIn;
    }

    public void setDtUltElabPrCon(short dtUltElabPrCon) {
        this.dtUltElabPrCon = dtUltElabPrCon;
    }

    public short getDtUltElabPrCon() {
        return this.dtUltElabPrCon;
    }

    public void setDtUltBollRpCl(short dtUltBollRpCl) {
        this.dtUltBollRpCl = dtUltBollRpCl;
    }

    public short getDtUltBollRpCl() {
        return this.dtUltBollRpCl;
    }

    public void setDtUltBollRpIn(short dtUltBollRpIn) {
        this.dtUltBollRpIn = dtUltBollRpIn;
    }

    public short getDtUltBollRpIn() {
        return this.dtUltBollRpIn;
    }

    public void setDtUltBollPreI(short dtUltBollPreI) {
        this.dtUltBollPreI = dtUltBollPreI;
    }

    public short getDtUltBollPreI() {
        return this.dtUltBollPreI;
    }

    public void setDtUltBollPreC(short dtUltBollPreC) {
        this.dtUltBollPreC = dtUltBollPreC;
    }

    public short getDtUltBollPreC() {
        return this.dtUltBollPreC;
    }

    public void setDtUltcPildiMmC(short dtUltcPildiMmC) {
        this.dtUltcPildiMmC = dtUltcPildiMmC;
    }

    public short getDtUltcPildiMmC() {
        return this.dtUltcPildiMmC;
    }

    public void setDtUltcPildiAaC(short dtUltcPildiAaC) {
        this.dtUltcPildiAaC = dtUltcPildiAaC;
    }

    public short getDtUltcPildiAaC() {
        return this.dtUltcPildiAaC;
    }

    public void setDtUltcPildiMmI(short dtUltcPildiMmI) {
        this.dtUltcPildiMmI = dtUltcPildiMmI;
    }

    public short getDtUltcPildiMmI() {
        return this.dtUltcPildiMmI;
    }

    public void setDtUltcPildiTrI(short dtUltcPildiTrI) {
        this.dtUltcPildiTrI = dtUltcPildiTrI;
    }

    public short getDtUltcPildiTrI() {
        return this.dtUltcPildiTrI;
    }

    public void setDtUltcPildiAaI(short dtUltcPildiAaI) {
        this.dtUltcPildiAaI = dtUltcPildiAaI;
    }

    public short getDtUltcPildiAaI() {
        return this.dtUltcPildiAaI;
    }

    public void setDtUltBollQuieC(short dtUltBollQuieC) {
        this.dtUltBollQuieC = dtUltBollQuieC;
    }

    public short getDtUltBollQuieC() {
        return this.dtUltBollQuieC;
    }

    public void setDtUltBollQuieI(short dtUltBollQuieI) {
        this.dtUltBollQuieI = dtUltBollQuieI;
    }

    public short getDtUltBollQuieI() {
        return this.dtUltBollQuieI;
    }

    public void setDtUltBollCotrI(short dtUltBollCotrI) {
        this.dtUltBollCotrI = dtUltBollCotrI;
    }

    public short getDtUltBollCotrI() {
        return this.dtUltBollCotrI;
    }

    public void setDtUltBollCotrC(short dtUltBollCotrC) {
        this.dtUltBollCotrC = dtUltBollCotrC;
    }

    public short getDtUltBollCotrC() {
        return this.dtUltBollCotrC;
    }

    public void setDtUltBollCoriC(short dtUltBollCoriC) {
        this.dtUltBollCoriC = dtUltBollCoriC;
    }

    public short getDtUltBollCoriC() {
        return this.dtUltBollCoriC;
    }

    public void setDtUltBollCoriI(short dtUltBollCoriI) {
        this.dtUltBollCoriI = dtUltBollCoriI;
    }

    public short getDtUltBollCoriI() {
        return this.dtUltBollCoriI;
    }

    public void setTpValzzDtVlt(short tpValzzDtVlt) {
        this.tpValzzDtVlt = tpValzzDtVlt;
    }

    public short getTpValzzDtVlt() {
        return this.tpValzzDtVlt;
    }

    public void setFlFrazProvAcq(short flFrazProvAcq) {
        this.flFrazProvAcq = flFrazProvAcq;
    }

    public short getFlFrazProvAcq() {
        return this.flFrazProvAcq;
    }

    public void setDtUltAggErogRe(short dtUltAggErogRe) {
        this.dtUltAggErogRe = dtUltAggErogRe;
    }

    public short getDtUltAggErogRe() {
        return this.dtUltAggErogRe;
    }

    public void setPcRmMarsol(short pcRmMarsol) {
        this.pcRmMarsol = pcRmMarsol;
    }

    public short getPcRmMarsol() {
        return this.pcRmMarsol;
    }

    public void setPcCSubrshMarsol(short pcCSubrshMarsol) {
        this.pcCSubrshMarsol = pcCSubrshMarsol;
    }

    public short getPcCSubrshMarsol() {
        return this.pcCSubrshMarsol;
    }

    public void setCodCompIsvap(short codCompIsvap) {
        this.codCompIsvap = codCompIsvap;
    }

    public short getCodCompIsvap() {
        return this.codCompIsvap;
    }

    public void setLmRisConInt(short lmRisConInt) {
        this.lmRisConInt = lmRisConInt;
    }

    public short getLmRisConInt() {
        return this.lmRisConInt;
    }

    public void setLmCSubrshConIn(short lmCSubrshConIn) {
        this.lmCSubrshConIn = lmCSubrshConIn;
    }

    public short getLmCSubrshConIn() {
        return this.lmCSubrshConIn;
    }

    public void setPcGarNoriskMars(short pcGarNoriskMars) {
        this.pcGarNoriskMars = pcGarNoriskMars;
    }

    public short getPcGarNoriskMars() {
        return this.pcGarNoriskMars;
    }

    public void setCrz1aRatIntrPr(short crz1aRatIntrPr) {
        this.crz1aRatIntrPr = crz1aRatIntrPr;
    }

    public short getCrz1aRatIntrPr() {
        return this.crz1aRatIntrPr;
    }

    public void setNumGgArrIntrPr(short numGgArrIntrPr) {
        this.numGgArrIntrPr = numGgArrIntrPr;
    }

    public short getNumGgArrIntrPr() {
        return this.numGgArrIntrPr;
    }

    public void setFlVisualVinpg(short flVisualVinpg) {
        this.flVisualVinpg = flVisualVinpg;
    }

    public short getFlVisualVinpg() {
        return this.flVisualVinpg;
    }

    public void setDtUltEstrazFug(short dtUltEstrazFug) {
        this.dtUltEstrazFug = dtUltEstrazFug;
    }

    public short getDtUltEstrazFug() {
        return this.dtUltEstrazFug;
    }

    public void setDtUltElabPrAut(short dtUltElabPrAut) {
        this.dtUltElabPrAut = dtUltElabPrAut;
    }

    public short getDtUltElabPrAut() {
        return this.dtUltElabPrAut;
    }

    public void setDtUltElabCommef(short dtUltElabCommef) {
        this.dtUltElabCommef = dtUltElabCommef;
    }

    public short getDtUltElabCommef() {
        return this.dtUltElabCommef;
    }

    public void setDtUltElabLiqmef(short dtUltElabLiqmef) {
        this.dtUltElabLiqmef = dtUltElabLiqmef;
    }

    public short getDtUltElabLiqmef() {
        return this.dtUltElabLiqmef;
    }

    public void setCodFiscMef(short codFiscMef) {
        this.codFiscMef = codFiscMef;
    }

    public short getCodFiscMef() {
        return this.codFiscMef;
    }

    public void setImpAssSociale(short impAssSociale) {
        this.impAssSociale = impAssSociale;
    }

    public short getImpAssSociale() {
        return this.impAssSociale;
    }

    public void setModComnzInvstSw(short modComnzInvstSw) {
        this.modComnzInvstSw = modComnzInvstSw;
    }

    public short getModComnzInvstSw() {
        return this.modComnzInvstSw;
    }

    public void setDtRiatRiassRsh(short dtRiatRiassRsh) {
        this.dtRiatRiassRsh = dtRiatRiassRsh;
    }

    public short getDtRiatRiassRsh() {
        return this.dtRiatRiassRsh;
    }

    public void setDtRiatRiassComm(short dtRiatRiassComm) {
        this.dtRiatRiassComm = dtRiatRiassComm;
    }

    public short getDtRiatRiassComm() {
        return this.dtRiatRiassComm;
    }

    public void setGgIntrRitPag(short ggIntrRitPag) {
        this.ggIntrRitPag = ggIntrRitPag;
    }

    public short getGgIntrRitPag() {
        return this.ggIntrRitPag;
    }

    public void setDtUltRinnTac(short dtUltRinnTac) {
        this.dtUltRinnTac = dtUltRinnTac;
    }

    public short getDtUltRinnTac() {
        return this.dtUltRinnTac;
    }

    public void setDescComp(short descComp) {
        this.descComp = descComp;
    }

    public short getDescComp() {
        return this.descComp;
    }

    public void setDtUltEcTcmInd(short dtUltEcTcmInd) {
        this.dtUltEcTcmInd = dtUltEcTcmInd;
    }

    public short getDtUltEcTcmInd() {
        return this.dtUltEcTcmInd;
    }

    public void setDtUltEcTcmColl(short dtUltEcTcmColl) {
        this.dtUltEcTcmColl = dtUltEcTcmColl;
    }

    public short getDtUltEcTcmColl() {
        return this.dtUltEcTcmColl;
    }

    public void setDtUltEcMrmInd(short dtUltEcMrmInd) {
        this.dtUltEcMrmInd = dtUltEcMrmInd;
    }

    public short getDtUltEcMrmInd() {
        return this.dtUltEcMrmInd;
    }

    public void setDtUltEcMrmColl(short dtUltEcMrmColl) {
        this.dtUltEcMrmColl = dtUltEcMrmColl;
    }

    public short getDtUltEcMrmColl() {
        return this.dtUltEcMrmColl;
    }

    public void setCodCompLdap(short codCompLdap) {
        this.codCompLdap = codCompLdap;
    }

    public short getCodCompLdap() {
        return this.codCompLdap;
    }

    public void setPcRidImp1382011(short pcRidImp1382011) {
        this.pcRidImp1382011 = pcRidImp1382011;
    }

    public short getPcRidImp1382011() {
        return this.pcRidImp1382011;
    }

    public void setPcRidImp662014(short pcRidImp662014) {
        this.pcRidImp662014 = pcRidImp662014;
    }

    public short getPcRidImp662014() {
        return this.pcRidImp662014;
    }

    public void setSoglAmlPreUni(short soglAmlPreUni) {
        this.soglAmlPreUni = soglAmlPreUni;
    }

    public short getSoglAmlPreUni() {
        return this.soglAmlPreUni;
    }

    public void setSoglAmlPrePer(short soglAmlPrePer) {
        this.soglAmlPrePer = soglAmlPrePer;
    }

    public short getSoglAmlPrePer() {
        return this.soglAmlPrePer;
    }

    public void setCodSoggFtzAssto(short codSoggFtzAssto) {
        this.codSoggFtzAssto = codSoggFtzAssto;
    }

    public short getCodSoggFtzAssto() {
        return this.codSoggFtzAssto;
    }

    public void setDtUltElabRedpro(short dtUltElabRedpro) {
        this.dtUltElabRedpro = dtUltElabRedpro;
    }

    public short getDtUltElabRedpro() {
        return this.dtUltElabRedpro;
    }

    public void setDtUltElabTakeP(short dtUltElabTakeP) {
        this.dtUltElabTakeP = dtUltElabTakeP;
    }

    public short getDtUltElabTakeP() {
        return this.dtUltElabTakeP;
    }

    public void setDtUltElabPaspas(short dtUltElabPaspas) {
        this.dtUltElabPaspas = dtUltElabPaspas;
    }

    public short getDtUltElabPaspas() {
        return this.dtUltElabPaspas;
    }

    public void setSoglAmlPreSavR(short soglAmlPreSavR) {
        this.soglAmlPreSavR = soglAmlPreSavR;
    }

    public short getSoglAmlPreSavR() {
        return this.soglAmlPreSavR;
    }

    public void setDtUltEstrDecCo(short dtUltEstrDecCo) {
        this.dtUltEstrDecCo = dtUltEstrDecCo;
    }

    public short getDtUltEstrDecCo() {
        return this.dtUltEstrDecCo;
    }

    public void setDtUltElabCosAt(short dtUltElabCosAt) {
        this.dtUltElabCosAt = dtUltElabCosAt;
    }

    public short getDtUltElabCosAt() {
        return this.dtUltElabCosAt;
    }

    public void setFrqCostiAtt(short frqCostiAtt) {
        this.frqCostiAtt = frqCostiAtt;
    }

    public short getFrqCostiAtt() {
        return this.frqCostiAtt;
    }

    public void setDtUltElabCosSt(short dtUltElabCosSt) {
        this.dtUltElabCosSt = dtUltElabCosSt;
    }

    public short getDtUltElabCosSt() {
        return this.dtUltElabCosSt;
    }

    public void setFrqCostiStornati(short frqCostiStornati) {
        this.frqCostiStornati = frqCostiStornati;
    }

    public short getFrqCostiStornati() {
        return this.frqCostiStornati;
    }

    public void setDtEstrAssMin70a(short dtEstrAssMin70a) {
        this.dtEstrAssMin70a = dtEstrAssMin70a;
    }

    public short getDtEstrAssMin70a() {
        return this.dtEstrAssMin70a;
    }

    public void setDtEstrAssMag70a(short dtEstrAssMag70a) {
        this.dtEstrAssMag70a = dtEstrAssMag70a;
    }

    public short getDtEstrAssMag70a() {
        return this.dtEstrAssMag70a;
    }
}
