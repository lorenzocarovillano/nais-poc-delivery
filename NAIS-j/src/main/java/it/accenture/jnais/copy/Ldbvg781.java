package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVG781<br>
 * Variable: LDBVG781 from copybook LDBVG781<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbvg781 {

    //==== PROPERTIES ====
    //Original name: LDBVG781-GRAV-FUNZ-FUNZ-1
    private String gravFunzFunz1 = DefaultValues.stringVal(Len.GRAV_FUNZ_FUNZ1);
    //Original name: LDBVG781-GRAV-FUNZ-FUNZ-2
    private String gravFunzFunz2 = DefaultValues.stringVal(Len.GRAV_FUNZ_FUNZ2);
    //Original name: LDBVG781-GRAV-FUNZ-FUNZ-3
    private String gravFunzFunz3 = DefaultValues.stringVal(Len.GRAV_FUNZ_FUNZ3);
    //Original name: LDBVG781-GRAV-FUNZ-FUNZ-4
    private String gravFunzFunz4 = DefaultValues.stringVal(Len.GRAV_FUNZ_FUNZ4);
    //Original name: LDBVG781-GRAV-FUNZ-FUNZ-5
    private String gravFunzFunz5 = DefaultValues.stringVal(Len.GRAV_FUNZ_FUNZ5);
    //Original name: LDBVG781-FL-POLI-IFP
    private char flPoliIfp = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setLdbvg781Formatted(String data) {
        byte[] buffer = new byte[Len.LDBVG781];
        MarshalByte.writeString(buffer, 1, data, Len.LDBVG781);
        setLdbvg781Bytes(buffer, 1);
    }

    public String getLdbvg781Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvg781Bytes());
    }

    public byte[] getLdbvg781Bytes() {
        byte[] buffer = new byte[Len.LDBVG781];
        return getLdbvg781Bytes(buffer, 1);
    }

    public void setLdbvg781Bytes(byte[] buffer, int offset) {
        int position = offset;
        gravFunzFunz1 = MarshalByte.readString(buffer, position, Len.GRAV_FUNZ_FUNZ1);
        position += Len.GRAV_FUNZ_FUNZ1;
        gravFunzFunz2 = MarshalByte.readString(buffer, position, Len.GRAV_FUNZ_FUNZ2);
        position += Len.GRAV_FUNZ_FUNZ2;
        gravFunzFunz3 = MarshalByte.readString(buffer, position, Len.GRAV_FUNZ_FUNZ3);
        position += Len.GRAV_FUNZ_FUNZ3;
        gravFunzFunz4 = MarshalByte.readString(buffer, position, Len.GRAV_FUNZ_FUNZ4);
        position += Len.GRAV_FUNZ_FUNZ4;
        gravFunzFunz5 = MarshalByte.readString(buffer, position, Len.GRAV_FUNZ_FUNZ5);
        position += Len.GRAV_FUNZ_FUNZ5;
        flPoliIfp = MarshalByte.readChar(buffer, position);
    }

    public byte[] getLdbvg781Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, gravFunzFunz1, Len.GRAV_FUNZ_FUNZ1);
        position += Len.GRAV_FUNZ_FUNZ1;
        MarshalByte.writeString(buffer, position, gravFunzFunz2, Len.GRAV_FUNZ_FUNZ2);
        position += Len.GRAV_FUNZ_FUNZ2;
        MarshalByte.writeString(buffer, position, gravFunzFunz3, Len.GRAV_FUNZ_FUNZ3);
        position += Len.GRAV_FUNZ_FUNZ3;
        MarshalByte.writeString(buffer, position, gravFunzFunz4, Len.GRAV_FUNZ_FUNZ4);
        position += Len.GRAV_FUNZ_FUNZ4;
        MarshalByte.writeString(buffer, position, gravFunzFunz5, Len.GRAV_FUNZ_FUNZ5);
        position += Len.GRAV_FUNZ_FUNZ5;
        MarshalByte.writeChar(buffer, position, flPoliIfp);
        return buffer;
    }

    public void setGravFunzFunz1(String gravFunzFunz1) {
        this.gravFunzFunz1 = Functions.subString(gravFunzFunz1, Len.GRAV_FUNZ_FUNZ1);
    }

    public String getGravFunzFunz1() {
        return this.gravFunzFunz1;
    }

    public void setGravFunzFunz2(String gravFunzFunz2) {
        this.gravFunzFunz2 = Functions.subString(gravFunzFunz2, Len.GRAV_FUNZ_FUNZ2);
    }

    public String getGravFunzFunz2() {
        return this.gravFunzFunz2;
    }

    public void setGravFunzFunz3(String gravFunzFunz3) {
        this.gravFunzFunz3 = Functions.subString(gravFunzFunz3, Len.GRAV_FUNZ_FUNZ3);
    }

    public String getGravFunzFunz3() {
        return this.gravFunzFunz3;
    }

    public void setGravFunzFunz4(String gravFunzFunz4) {
        this.gravFunzFunz4 = Functions.subString(gravFunzFunz4, Len.GRAV_FUNZ_FUNZ4);
    }

    public String getGravFunzFunz4() {
        return this.gravFunzFunz4;
    }

    public void setGravFunzFunz5(String gravFunzFunz5) {
        this.gravFunzFunz5 = Functions.subString(gravFunzFunz5, Len.GRAV_FUNZ_FUNZ5);
    }

    public String getGravFunzFunz5() {
        return this.gravFunzFunz5;
    }

    public void setFlPoliIfp(char flPoliIfp) {
        this.flPoliIfp = flPoliIfp;
    }

    public void setFlPoliIfpFormatted(String flPoliIfp) {
        setFlPoliIfp(Functions.charAt(flPoliIfp, Types.CHAR_SIZE));
    }

    public char getFlPoliIfp() {
        return this.flPoliIfp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GRAV_FUNZ_FUNZ1 = 2;
        public static final int GRAV_FUNZ_FUNZ2 = 2;
        public static final int GRAV_FUNZ_FUNZ3 = 2;
        public static final int GRAV_FUNZ_FUNZ4 = 2;
        public static final int GRAV_FUNZ_FUNZ5 = 2;
        public static final int FL_POLI_IFP = 1;
        public static final int LDBVG781 = GRAV_FUNZ_FUNZ1 + GRAV_FUNZ_FUNZ2 + GRAV_FUNZ_FUNZ3 + GRAV_FUNZ_FUNZ4 + GRAV_FUNZ_FUNZ5 + FL_POLI_IFP;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
