package it.accenture.jnais.copy;

import it.accenture.jnais.ws.enums.EofFilesqs1;
import it.accenture.jnais.ws.enums.EofFilesqs2;
import it.accenture.jnais.ws.enums.EofFilesqs3;
import it.accenture.jnais.ws.enums.EofFilesqs4;
import it.accenture.jnais.ws.enums.EofFilesqs5;
import it.accenture.jnais.ws.enums.EofFilesqs6;
import it.accenture.jnais.ws.enums.FlagCloseFilesqs1;
import it.accenture.jnais.ws.enums.FlagCloseFilesqs2;
import it.accenture.jnais.ws.enums.FlagCloseFilesqs3;
import it.accenture.jnais.ws.enums.FlagCloseFilesqs4;
import it.accenture.jnais.ws.enums.FlagCloseFilesqs5;
import it.accenture.jnais.ws.enums.FlagCloseFilesqs6;
import it.accenture.jnais.ws.enums.FlagCloseFilesqsAll;
import it.accenture.jnais.ws.enums.FlagOpenFilesqs1;
import it.accenture.jnais.ws.enums.FlagOpenFilesqs2;
import it.accenture.jnais.ws.enums.FlagOpenFilesqs3;
import it.accenture.jnais.ws.enums.FlagOpenFilesqs4;
import it.accenture.jnais.ws.enums.FlagOpenFilesqs5;
import it.accenture.jnais.ws.enums.FlagOpenFilesqs6;
import it.accenture.jnais.ws.enums.FlagOpenFilesqsAll;
import it.accenture.jnais.ws.enums.FlagReadFilesqs1;
import it.accenture.jnais.ws.enums.FlagReadFilesqs2;
import it.accenture.jnais.ws.enums.FlagReadFilesqs3;
import it.accenture.jnais.ws.enums.FlagReadFilesqs4;
import it.accenture.jnais.ws.enums.FlagReadFilesqs5;
import it.accenture.jnais.ws.enums.FlagReadFilesqs6;
import it.accenture.jnais.ws.enums.FlagReadFilesqsAll;
import it.accenture.jnais.ws.enums.FlagRewriteFilesqs1;
import it.accenture.jnais.ws.enums.FlagRewriteFilesqs2;
import it.accenture.jnais.ws.enums.FlagRewriteFilesqs3;
import it.accenture.jnais.ws.enums.FlagRewriteFilesqs4;
import it.accenture.jnais.ws.enums.FlagRewriteFilesqs5;
import it.accenture.jnais.ws.enums.FlagRewriteFilesqs6;
import it.accenture.jnais.ws.enums.FlagRewriteFilesqsAll;
import it.accenture.jnais.ws.enums.FlagWriteFilesqs1;
import it.accenture.jnais.ws.enums.FlagWriteFilesqs2;
import it.accenture.jnais.ws.enums.FlagWriteFilesqs3;
import it.accenture.jnais.ws.enums.FlagWriteFilesqs4;
import it.accenture.jnais.ws.enums.FlagWriteFilesqs5;
import it.accenture.jnais.ws.enums.FlagWriteFilesqs6;
import it.accenture.jnais.ws.enums.FlagWriteFilesqsAll;
import it.accenture.jnais.ws.enums.OpenTypeFilesqs1;
import it.accenture.jnais.ws.enums.OperazioneFilesqs;
import it.accenture.jnais.ws.enums.StatoFilesqs1;
import it.accenture.jnais.ws.enums.StatoFilesqs2;
import it.accenture.jnais.ws.enums.StatoFilesqs3;
import it.accenture.jnais.ws.enums.StatoFilesqs4;
import it.accenture.jnais.ws.enums.StatoFilesqs5;
import it.accenture.jnais.ws.enums.StatoFilesqs6;

/**Original name: IABVSQS1<br>
 * Variable: IABVSQS1 from copybook IABVSQS1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Iabvsqs1 {

    //==== PROPERTIES ====
    /**Original name: OPERAZIONE-FILESQS<br>
	 * <pre>*****************************************************************
	 *  OPERAZIONE SU FILE SEQUENZIALE
	 * *****************************************************************</pre>*/
    private OperazioneFilesqs operazioneFilesqs = new OperazioneFilesqs();
    /**Original name: STATO-FILESQS1<br>
	 * <pre>*****************************************************************
	 *  FLAG DI CONTROLLO x FILES SEQUENZIALI
	 * *****************************************************************</pre>*/
    private StatoFilesqs1 statoFilesqs1 = new StatoFilesqs1();
    //Original name: STATO-FILESQS2
    private StatoFilesqs2 statoFilesqs2 = new StatoFilesqs2();
    //Original name: STATO-FILESQS3
    private StatoFilesqs3 statoFilesqs3 = new StatoFilesqs3();
    //Original name: STATO-FILESQS4
    private StatoFilesqs4 statoFilesqs4 = new StatoFilesqs4();
    //Original name: STATO-FILESQS5
    private StatoFilesqs5 statoFilesqs5 = new StatoFilesqs5();
    //Original name: STATO-FILESQS6
    private StatoFilesqs6 statoFilesqs6 = new StatoFilesqs6();
    /**Original name: EOF-FILESQS1<br>
	 * <pre>*****************************************************************
	 *  FLAG DI CONTROLLO x END-OF-FILE
	 * *****************************************************************</pre>*/
    private EofFilesqs1 eofFilesqs1 = new EofFilesqs1();
    //Original name: EOF-FILESQS2
    private EofFilesqs2 eofFilesqs2 = new EofFilesqs2();
    //Original name: EOF-FILESQS3
    private EofFilesqs3 eofFilesqs3 = new EofFilesqs3();
    //Original name: EOF-FILESQS4
    private EofFilesqs4 eofFilesqs4 = new EofFilesqs4();
    //Original name: EOF-FILESQS5
    private EofFilesqs5 eofFilesqs5 = new EofFilesqs5();
    //Original name: EOF-FILESQS6
    private EofFilesqs6 eofFilesqs6 = new EofFilesqs6();
    /**Original name: FLAG-OPEN-FILESQS1<br>
	 * <pre>*****************************************************************
	 *  FLAG X OPEN FILES SEQUENZIALI
	 * *****************************************************************</pre>*/
    private FlagOpenFilesqs1 flagOpenFilesqs1 = new FlagOpenFilesqs1();
    //Original name: FLAG-OPEN-FILESQS2
    private FlagOpenFilesqs2 flagOpenFilesqs2 = new FlagOpenFilesqs2();
    //Original name: FLAG-OPEN-FILESQS3
    private FlagOpenFilesqs3 flagOpenFilesqs3 = new FlagOpenFilesqs3();
    //Original name: FLAG-OPEN-FILESQS4
    private FlagOpenFilesqs4 flagOpenFilesqs4 = new FlagOpenFilesqs4();
    //Original name: FLAG-OPEN-FILESQS5
    private FlagOpenFilesqs5 flagOpenFilesqs5 = new FlagOpenFilesqs5();
    //Original name: FLAG-OPEN-FILESQS6
    private FlagOpenFilesqs6 flagOpenFilesqs6 = new FlagOpenFilesqs6();
    //Original name: FLAG-OPEN-FILESQS-ALL
    private FlagOpenFilesqsAll flagOpenFilesqsAll = new FlagOpenFilesqsAll();
    /**Original name: FLAG-READ-FILESQS1<br>
	 * <pre>*****************************************************************
	 *  FLAG X READ FILES SEQUENZIALI
	 * *****************************************************************</pre>*/
    private FlagReadFilesqs1 flagReadFilesqs1 = new FlagReadFilesqs1();
    //Original name: FLAG-READ-FILESQS2
    private FlagReadFilesqs2 flagReadFilesqs2 = new FlagReadFilesqs2();
    //Original name: FLAG-READ-FILESQS3
    private FlagReadFilesqs3 flagReadFilesqs3 = new FlagReadFilesqs3();
    //Original name: FLAG-READ-FILESQS4
    private FlagReadFilesqs4 flagReadFilesqs4 = new FlagReadFilesqs4();
    //Original name: FLAG-READ-FILESQS5
    private FlagReadFilesqs5 flagReadFilesqs5 = new FlagReadFilesqs5();
    //Original name: FLAG-READ-FILESQS6
    private FlagReadFilesqs6 flagReadFilesqs6 = new FlagReadFilesqs6();
    //Original name: FLAG-READ-FILESQS-ALL
    private FlagReadFilesqsAll flagReadFilesqsAll = new FlagReadFilesqsAll();
    /**Original name: FLAG-WRITE-FILESQS1<br>
	 * <pre>*****************************************************************
	 *  FLAG X WRITE FILES SEQUENZIALI
	 * *****************************************************************</pre>*/
    private FlagWriteFilesqs1 flagWriteFilesqs1 = new FlagWriteFilesqs1();
    //Original name: FLAG-WRITE-FILESQS2
    private FlagWriteFilesqs2 flagWriteFilesqs2 = new FlagWriteFilesqs2();
    //Original name: FLAG-WRITE-FILESQS3
    private FlagWriteFilesqs3 flagWriteFilesqs3 = new FlagWriteFilesqs3();
    //Original name: FLAG-WRITE-FILESQS4
    private FlagWriteFilesqs4 flagWriteFilesqs4 = new FlagWriteFilesqs4();
    //Original name: FLAG-WRITE-FILESQS5
    private FlagWriteFilesqs5 flagWriteFilesqs5 = new FlagWriteFilesqs5();
    //Original name: FLAG-WRITE-FILESQS6
    private FlagWriteFilesqs6 flagWriteFilesqs6 = new FlagWriteFilesqs6();
    //Original name: FLAG-WRITE-FILESQS-ALL
    private FlagWriteFilesqsAll flagWriteFilesqsAll = new FlagWriteFilesqsAll();
    /**Original name: FLAG-REWRITE-FILESQS1<br>
	 * <pre>*****************************************************************
	 *  FLAG X WRITE FILES SEQUENZIALI
	 * *****************************************************************</pre>*/
    private FlagRewriteFilesqs1 flagRewriteFilesqs1 = new FlagRewriteFilesqs1();
    //Original name: FLAG-REWRITE-FILESQS2
    private FlagRewriteFilesqs2 flagRewriteFilesqs2 = new FlagRewriteFilesqs2();
    //Original name: FLAG-REWRITE-FILESQS3
    private FlagRewriteFilesqs3 flagRewriteFilesqs3 = new FlagRewriteFilesqs3();
    //Original name: FLAG-REWRITE-FILESQS4
    private FlagRewriteFilesqs4 flagRewriteFilesqs4 = new FlagRewriteFilesqs4();
    //Original name: FLAG-REWRITE-FILESQS5
    private FlagRewriteFilesqs5 flagRewriteFilesqs5 = new FlagRewriteFilesqs5();
    //Original name: FLAG-REWRITE-FILESQS6
    private FlagRewriteFilesqs6 flagRewriteFilesqs6 = new FlagRewriteFilesqs6();
    //Original name: FLAG-REWRITE-FILESQS-ALL
    private FlagRewriteFilesqsAll flagRewriteFilesqsAll = new FlagRewriteFilesqsAll();
    /**Original name: FLAG-CLOSE-FILESQS1<br>
	 * <pre>*****************************************************************
	 *  FLAG X CLOSE FILES SEQUENZIALI
	 * *****************************************************************</pre>*/
    private FlagCloseFilesqs1 flagCloseFilesqs1 = new FlagCloseFilesqs1();
    //Original name: FLAG-CLOSE-FILESQS2
    private FlagCloseFilesqs2 flagCloseFilesqs2 = new FlagCloseFilesqs2();
    //Original name: FLAG-CLOSE-FILESQS3
    private FlagCloseFilesqs3 flagCloseFilesqs3 = new FlagCloseFilesqs3();
    //Original name: FLAG-CLOSE-FILESQS4
    private FlagCloseFilesqs4 flagCloseFilesqs4 = new FlagCloseFilesqs4();
    //Original name: FLAG-CLOSE-FILESQS5
    private FlagCloseFilesqs5 flagCloseFilesqs5 = new FlagCloseFilesqs5();
    //Original name: FLAG-CLOSE-FILESQS6
    private FlagCloseFilesqs6 flagCloseFilesqs6 = new FlagCloseFilesqs6();
    //Original name: FLAG-CLOSE-FILESQS-ALL
    private FlagCloseFilesqsAll flagCloseFilesqsAll = new FlagCloseFilesqsAll();
    /**Original name: OPEN-TYPE-FILESQS1<br>
	 * <pre>*****************************************************************
	 *  FLAG X TIPO OPEN FILES SEQUENZIALI
	 * *****************************************************************</pre>*/
    private OpenTypeFilesqs1 openTypeFilesqs1 = new OpenTypeFilesqs1();
    //Original name: OPEN-TYPE-FILESQS2
    private OpenTypeFilesqs1 openTypeFilesqs2 = new OpenTypeFilesqs1();
    //Original name: OPEN-TYPE-FILESQS3
    private OpenTypeFilesqs1 openTypeFilesqs3 = new OpenTypeFilesqs1();
    //Original name: OPEN-TYPE-FILESQS4
    private OpenTypeFilesqs1 openTypeFilesqs4 = new OpenTypeFilesqs1();
    //Original name: OPEN-TYPE-FILESQS5
    private OpenTypeFilesqs1 openTypeFilesqs5 = new OpenTypeFilesqs1();
    //Original name: OPEN-TYPE-FILESQS6
    private OpenTypeFilesqs1 openTypeFilesqs6 = new OpenTypeFilesqs1();
    //Original name: OPEN-TYPE-FILESQS-ALL
    private OpenTypeFilesqs1 openTypeFilesqsAll = new OpenTypeFilesqs1();

    //==== METHODS ====
    public EofFilesqs1 getEofFilesqs1() {
        return eofFilesqs1;
    }

    public EofFilesqs2 getEofFilesqs2() {
        return eofFilesqs2;
    }

    public EofFilesqs3 getEofFilesqs3() {
        return eofFilesqs3;
    }

    public EofFilesqs4 getEofFilesqs4() {
        return eofFilesqs4;
    }

    public EofFilesqs5 getEofFilesqs5() {
        return eofFilesqs5;
    }

    public EofFilesqs6 getEofFilesqs6() {
        return eofFilesqs6;
    }

    public FlagCloseFilesqs1 getFlagCloseFilesqs1() {
        return flagCloseFilesqs1;
    }

    public FlagCloseFilesqs2 getFlagCloseFilesqs2() {
        return flagCloseFilesqs2;
    }

    public FlagCloseFilesqs3 getFlagCloseFilesqs3() {
        return flagCloseFilesqs3;
    }

    public FlagCloseFilesqs4 getFlagCloseFilesqs4() {
        return flagCloseFilesqs4;
    }

    public FlagCloseFilesqs5 getFlagCloseFilesqs5() {
        return flagCloseFilesqs5;
    }

    public FlagCloseFilesqs6 getFlagCloseFilesqs6() {
        return flagCloseFilesqs6;
    }

    public FlagCloseFilesqsAll getFlagCloseFilesqsAll() {
        return flagCloseFilesqsAll;
    }

    public FlagOpenFilesqs1 getFlagOpenFilesqs1() {
        return flagOpenFilesqs1;
    }

    public FlagOpenFilesqs2 getFlagOpenFilesqs2() {
        return flagOpenFilesqs2;
    }

    public FlagOpenFilesqs3 getFlagOpenFilesqs3() {
        return flagOpenFilesqs3;
    }

    public FlagOpenFilesqs4 getFlagOpenFilesqs4() {
        return flagOpenFilesqs4;
    }

    public FlagOpenFilesqs5 getFlagOpenFilesqs5() {
        return flagOpenFilesqs5;
    }

    public FlagOpenFilesqs6 getFlagOpenFilesqs6() {
        return flagOpenFilesqs6;
    }

    public FlagOpenFilesqsAll getFlagOpenFilesqsAll() {
        return flagOpenFilesqsAll;
    }

    public FlagReadFilesqs1 getFlagReadFilesqs1() {
        return flagReadFilesqs1;
    }

    public FlagReadFilesqs2 getFlagReadFilesqs2() {
        return flagReadFilesqs2;
    }

    public FlagReadFilesqs3 getFlagReadFilesqs3() {
        return flagReadFilesqs3;
    }

    public FlagReadFilesqs4 getFlagReadFilesqs4() {
        return flagReadFilesqs4;
    }

    public FlagReadFilesqs5 getFlagReadFilesqs5() {
        return flagReadFilesqs5;
    }

    public FlagReadFilesqs6 getFlagReadFilesqs6() {
        return flagReadFilesqs6;
    }

    public FlagReadFilesqsAll getFlagReadFilesqsAll() {
        return flagReadFilesqsAll;
    }

    public FlagRewriteFilesqs1 getFlagRewriteFilesqs1() {
        return flagRewriteFilesqs1;
    }

    public FlagRewriteFilesqs2 getFlagRewriteFilesqs2() {
        return flagRewriteFilesqs2;
    }

    public FlagRewriteFilesqs3 getFlagRewriteFilesqs3() {
        return flagRewriteFilesqs3;
    }

    public FlagRewriteFilesqs4 getFlagRewriteFilesqs4() {
        return flagRewriteFilesqs4;
    }

    public FlagRewriteFilesqs5 getFlagRewriteFilesqs5() {
        return flagRewriteFilesqs5;
    }

    public FlagRewriteFilesqs6 getFlagRewriteFilesqs6() {
        return flagRewriteFilesqs6;
    }

    public FlagRewriteFilesqsAll getFlagRewriteFilesqsAll() {
        return flagRewriteFilesqsAll;
    }

    public FlagWriteFilesqs1 getFlagWriteFilesqs1() {
        return flagWriteFilesqs1;
    }

    public FlagWriteFilesqs2 getFlagWriteFilesqs2() {
        return flagWriteFilesqs2;
    }

    public FlagWriteFilesqs3 getFlagWriteFilesqs3() {
        return flagWriteFilesqs3;
    }

    public FlagWriteFilesqs4 getFlagWriteFilesqs4() {
        return flagWriteFilesqs4;
    }

    public FlagWriteFilesqs5 getFlagWriteFilesqs5() {
        return flagWriteFilesqs5;
    }

    public FlagWriteFilesqs6 getFlagWriteFilesqs6() {
        return flagWriteFilesqs6;
    }

    public FlagWriteFilesqsAll getFlagWriteFilesqsAll() {
        return flagWriteFilesqsAll;
    }

    public OpenTypeFilesqs1 getOpenTypeFilesqs1() {
        return openTypeFilesqs1;
    }

    public OpenTypeFilesqs1 getOpenTypeFilesqs2() {
        return openTypeFilesqs2;
    }

    public OpenTypeFilesqs1 getOpenTypeFilesqs3() {
        return openTypeFilesqs3;
    }

    public OpenTypeFilesqs1 getOpenTypeFilesqs4() {
        return openTypeFilesqs4;
    }

    public OpenTypeFilesqs1 getOpenTypeFilesqs5() {
        return openTypeFilesqs5;
    }

    public OpenTypeFilesqs1 getOpenTypeFilesqs6() {
        return openTypeFilesqs6;
    }

    public OpenTypeFilesqs1 getOpenTypeFilesqsAll() {
        return openTypeFilesqsAll;
    }

    public OperazioneFilesqs getOperazioneFilesqs() {
        return operazioneFilesqs;
    }

    public StatoFilesqs1 getStatoFilesqs1() {
        return statoFilesqs1;
    }

    public StatoFilesqs2 getStatoFilesqs2() {
        return statoFilesqs2;
    }

    public StatoFilesqs3 getStatoFilesqs3() {
        return statoFilesqs3;
    }

    public StatoFilesqs4 getStatoFilesqs4() {
        return statoFilesqs4;
    }

    public StatoFilesqs5 getStatoFilesqs5() {
        return statoFilesqs5;
    }

    public StatoFilesqs6 getStatoFilesqs6() {
        return statoFilesqs6;
    }
}
