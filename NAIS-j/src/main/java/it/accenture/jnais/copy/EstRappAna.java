package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.E15IdMoviChiu;
import it.accenture.jnais.ws.redefines.E15IdRappAnaCollg;
import it.accenture.jnais.ws.redefines.E15IdSegmentazCli;

/**Original name: EST-RAPP-ANA<br>
 * Variable: EST-RAPP-ANA from copybook IDBVE151<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class EstRappAna {

    //==== PROPERTIES ====
    //Original name: E15-ID-EST-RAPP-ANA
    private int e15IdEstRappAna = DefaultValues.INT_VAL;
    //Original name: E15-ID-RAPP-ANA
    private int e15IdRappAna = DefaultValues.INT_VAL;
    //Original name: E15-ID-RAPP-ANA-COLLG
    private E15IdRappAnaCollg e15IdRappAnaCollg = new E15IdRappAnaCollg();
    //Original name: E15-ID-OGG
    private int e15IdOgg = DefaultValues.INT_VAL;
    //Original name: E15-TP-OGG
    private String e15TpOgg = DefaultValues.stringVal(Len.E15_TP_OGG);
    //Original name: E15-ID-MOVI-CRZ
    private int e15IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: E15-ID-MOVI-CHIU
    private E15IdMoviChiu e15IdMoviChiu = new E15IdMoviChiu();
    //Original name: E15-DT-INI-EFF
    private int e15DtIniEff = DefaultValues.INT_VAL;
    //Original name: E15-DT-END-EFF
    private int e15DtEndEff = DefaultValues.INT_VAL;
    //Original name: E15-COD-COMP-ANIA
    private int e15CodCompAnia = DefaultValues.INT_VAL;
    //Original name: E15-COD-SOGG
    private String e15CodSogg = DefaultValues.stringVal(Len.E15_COD_SOGG);
    //Original name: E15-TP-RAPP-ANA
    private String e15TpRappAna = DefaultValues.stringVal(Len.E15_TP_RAPP_ANA);
    //Original name: E15-DS-RIGA
    private long e15DsRiga = DefaultValues.LONG_VAL;
    //Original name: E15-DS-OPER-SQL
    private char e15DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: E15-DS-VER
    private int e15DsVer = DefaultValues.INT_VAL;
    //Original name: E15-DS-TS-INI-CPTZ
    private long e15DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: E15-DS-TS-END-CPTZ
    private long e15DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: E15-DS-UTENTE
    private String e15DsUtente = DefaultValues.stringVal(Len.E15_DS_UTENTE);
    //Original name: E15-DS-STATO-ELAB
    private char e15DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: E15-ID-SEGMENTAZ-CLI
    private E15IdSegmentazCli e15IdSegmentazCli = new E15IdSegmentazCli();
    //Original name: E15-FL-COINC-TIT-EFF
    private char e15FlCoincTitEff = DefaultValues.CHAR_VAL;
    //Original name: E15-FL-PERS-ESP-POL
    private char e15FlPersEspPol = DefaultValues.CHAR_VAL;
    //Original name: E15-DESC-PERS-ESP-POL-LEN
    private short e15DescPersEspPolLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: E15-DESC-PERS-ESP-POL
    private String e15DescPersEspPol = DefaultValues.stringVal(Len.E15_DESC_PERS_ESP_POL);
    //Original name: E15-TP-LEG-CNTR
    private String e15TpLegCntr = DefaultValues.stringVal(Len.E15_TP_LEG_CNTR);
    //Original name: E15-DESC-LEG-CNTR-LEN
    private short e15DescLegCntrLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: E15-DESC-LEG-CNTR
    private String e15DescLegCntr = DefaultValues.stringVal(Len.E15_DESC_LEG_CNTR);
    //Original name: E15-TP-LEG-PERC-BNFICR
    private String e15TpLegPercBnficr = DefaultValues.stringVal(Len.E15_TP_LEG_PERC_BNFICR);
    //Original name: E15-D-LEG-PERC-BNFICR-LEN
    private short e15DLegPercBnficrLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: E15-D-LEG-PERC-BNFICR
    private String e15DLegPercBnficr = DefaultValues.stringVal(Len.E15_D_LEG_PERC_BNFICR);
    //Original name: E15-TP-CNT-CORR
    private String e15TpCntCorr = DefaultValues.stringVal(Len.E15_TP_CNT_CORR);
    //Original name: E15-TP-COINC-PIC-PAC
    private String e15TpCoincPicPac = DefaultValues.stringVal(Len.E15_TP_COINC_PIC_PAC);

    //==== METHODS ====
    public void setEstRappAnaFormatted(String data) {
        byte[] buffer = new byte[Len.EST_RAPP_ANA];
        MarshalByte.writeString(buffer, 1, data, Len.EST_RAPP_ANA);
        setEstRappAnaBytes(buffer, 1);
    }

    public String getEstRappAnaFormatted() {
        return MarshalByteExt.bufferToStr(getEstRappAnaBytes());
    }

    public byte[] getEstRappAnaBytes() {
        byte[] buffer = new byte[Len.EST_RAPP_ANA];
        return getEstRappAnaBytes(buffer, 1);
    }

    public void setEstRappAnaBytes(byte[] buffer, int offset) {
        int position = offset;
        e15IdEstRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E15_ID_EST_RAPP_ANA, 0);
        position += Len.E15_ID_EST_RAPP_ANA;
        e15IdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E15_ID_RAPP_ANA, 0);
        position += Len.E15_ID_RAPP_ANA;
        e15IdRappAnaCollg.setE15IdRappAnaCollgFromBuffer(buffer, position);
        position += E15IdRappAnaCollg.Len.E15_ID_RAPP_ANA_COLLG;
        e15IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E15_ID_OGG, 0);
        position += Len.E15_ID_OGG;
        e15TpOgg = MarshalByte.readString(buffer, position, Len.E15_TP_OGG);
        position += Len.E15_TP_OGG;
        e15IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E15_ID_MOVI_CRZ, 0);
        position += Len.E15_ID_MOVI_CRZ;
        e15IdMoviChiu.setE15IdMoviChiuFromBuffer(buffer, position);
        position += E15IdMoviChiu.Len.E15_ID_MOVI_CHIU;
        e15DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E15_DT_INI_EFF, 0);
        position += Len.E15_DT_INI_EFF;
        e15DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E15_DT_END_EFF, 0);
        position += Len.E15_DT_END_EFF;
        e15CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E15_COD_COMP_ANIA, 0);
        position += Len.E15_COD_COMP_ANIA;
        e15CodSogg = MarshalByte.readString(buffer, position, Len.E15_COD_SOGG);
        position += Len.E15_COD_SOGG;
        e15TpRappAna = MarshalByte.readString(buffer, position, Len.E15_TP_RAPP_ANA);
        position += Len.E15_TP_RAPP_ANA;
        e15DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.E15_DS_RIGA, 0);
        position += Len.E15_DS_RIGA;
        e15DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        e15DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E15_DS_VER, 0);
        position += Len.E15_DS_VER;
        e15DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.E15_DS_TS_INI_CPTZ, 0);
        position += Len.E15_DS_TS_INI_CPTZ;
        e15DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.E15_DS_TS_END_CPTZ, 0);
        position += Len.E15_DS_TS_END_CPTZ;
        e15DsUtente = MarshalByte.readString(buffer, position, Len.E15_DS_UTENTE);
        position += Len.E15_DS_UTENTE;
        e15DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        e15IdSegmentazCli.setE15IdSegmentazCliFromBuffer(buffer, position);
        position += E15IdSegmentazCli.Len.E15_ID_SEGMENTAZ_CLI;
        e15FlCoincTitEff = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        e15FlPersEspPol = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setE15DescPersEspPolVcharBytes(buffer, position);
        position += Len.E15_DESC_PERS_ESP_POL_VCHAR;
        e15TpLegCntr = MarshalByte.readString(buffer, position, Len.E15_TP_LEG_CNTR);
        position += Len.E15_TP_LEG_CNTR;
        setE15DescLegCntrVcharBytes(buffer, position);
        position += Len.E15_DESC_LEG_CNTR_VCHAR;
        e15TpLegPercBnficr = MarshalByte.readString(buffer, position, Len.E15_TP_LEG_PERC_BNFICR);
        position += Len.E15_TP_LEG_PERC_BNFICR;
        setE15DLegPercBnficrVcharBytes(buffer, position);
        position += Len.E15_D_LEG_PERC_BNFICR_VCHAR;
        e15TpCntCorr = MarshalByte.readString(buffer, position, Len.E15_TP_CNT_CORR);
        position += Len.E15_TP_CNT_CORR;
        e15TpCoincPicPac = MarshalByte.readString(buffer, position, Len.E15_TP_COINC_PIC_PAC);
    }

    public byte[] getEstRappAnaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, e15IdEstRappAna, Len.Int.E15_ID_EST_RAPP_ANA, 0);
        position += Len.E15_ID_EST_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, e15IdRappAna, Len.Int.E15_ID_RAPP_ANA, 0);
        position += Len.E15_ID_RAPP_ANA;
        e15IdRappAnaCollg.getE15IdRappAnaCollgAsBuffer(buffer, position);
        position += E15IdRappAnaCollg.Len.E15_ID_RAPP_ANA_COLLG;
        MarshalByte.writeIntAsPacked(buffer, position, e15IdOgg, Len.Int.E15_ID_OGG, 0);
        position += Len.E15_ID_OGG;
        MarshalByte.writeString(buffer, position, e15TpOgg, Len.E15_TP_OGG);
        position += Len.E15_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, e15IdMoviCrz, Len.Int.E15_ID_MOVI_CRZ, 0);
        position += Len.E15_ID_MOVI_CRZ;
        e15IdMoviChiu.getE15IdMoviChiuAsBuffer(buffer, position);
        position += E15IdMoviChiu.Len.E15_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, e15DtIniEff, Len.Int.E15_DT_INI_EFF, 0);
        position += Len.E15_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, e15DtEndEff, Len.Int.E15_DT_END_EFF, 0);
        position += Len.E15_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, e15CodCompAnia, Len.Int.E15_COD_COMP_ANIA, 0);
        position += Len.E15_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, e15CodSogg, Len.E15_COD_SOGG);
        position += Len.E15_COD_SOGG;
        MarshalByte.writeString(buffer, position, e15TpRappAna, Len.E15_TP_RAPP_ANA);
        position += Len.E15_TP_RAPP_ANA;
        MarshalByte.writeLongAsPacked(buffer, position, e15DsRiga, Len.Int.E15_DS_RIGA, 0);
        position += Len.E15_DS_RIGA;
        MarshalByte.writeChar(buffer, position, e15DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, e15DsVer, Len.Int.E15_DS_VER, 0);
        position += Len.E15_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, e15DsTsIniCptz, Len.Int.E15_DS_TS_INI_CPTZ, 0);
        position += Len.E15_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, e15DsTsEndCptz, Len.Int.E15_DS_TS_END_CPTZ, 0);
        position += Len.E15_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, e15DsUtente, Len.E15_DS_UTENTE);
        position += Len.E15_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, e15DsStatoElab);
        position += Types.CHAR_SIZE;
        e15IdSegmentazCli.getE15IdSegmentazCliAsBuffer(buffer, position);
        position += E15IdSegmentazCli.Len.E15_ID_SEGMENTAZ_CLI;
        MarshalByte.writeChar(buffer, position, e15FlCoincTitEff);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, e15FlPersEspPol);
        position += Types.CHAR_SIZE;
        getE15DescPersEspPolVcharBytes(buffer, position);
        position += Len.E15_DESC_PERS_ESP_POL_VCHAR;
        MarshalByte.writeString(buffer, position, e15TpLegCntr, Len.E15_TP_LEG_CNTR);
        position += Len.E15_TP_LEG_CNTR;
        getE15DescLegCntrVcharBytes(buffer, position);
        position += Len.E15_DESC_LEG_CNTR_VCHAR;
        MarshalByte.writeString(buffer, position, e15TpLegPercBnficr, Len.E15_TP_LEG_PERC_BNFICR);
        position += Len.E15_TP_LEG_PERC_BNFICR;
        getE15DLegPercBnficrVcharBytes(buffer, position);
        position += Len.E15_D_LEG_PERC_BNFICR_VCHAR;
        MarshalByte.writeString(buffer, position, e15TpCntCorr, Len.E15_TP_CNT_CORR);
        position += Len.E15_TP_CNT_CORR;
        MarshalByte.writeString(buffer, position, e15TpCoincPicPac, Len.E15_TP_COINC_PIC_PAC);
        return buffer;
    }

    public void setE15IdEstRappAna(int e15IdEstRappAna) {
        this.e15IdEstRappAna = e15IdEstRappAna;
    }

    public int getE15IdEstRappAna() {
        return this.e15IdEstRappAna;
    }

    public void setE15IdRappAna(int e15IdRappAna) {
        this.e15IdRappAna = e15IdRappAna;
    }

    public int getE15IdRappAna() {
        return this.e15IdRappAna;
    }

    public void setE15IdOgg(int e15IdOgg) {
        this.e15IdOgg = e15IdOgg;
    }

    public int getE15IdOgg() {
        return this.e15IdOgg;
    }

    public void setE15TpOgg(String e15TpOgg) {
        this.e15TpOgg = Functions.subString(e15TpOgg, Len.E15_TP_OGG);
    }

    public String getE15TpOgg() {
        return this.e15TpOgg;
    }

    public void setE15IdMoviCrz(int e15IdMoviCrz) {
        this.e15IdMoviCrz = e15IdMoviCrz;
    }

    public int getE15IdMoviCrz() {
        return this.e15IdMoviCrz;
    }

    public void setE15DtIniEff(int e15DtIniEff) {
        this.e15DtIniEff = e15DtIniEff;
    }

    public int getE15DtIniEff() {
        return this.e15DtIniEff;
    }

    public void setE15DtEndEff(int e15DtEndEff) {
        this.e15DtEndEff = e15DtEndEff;
    }

    public int getE15DtEndEff() {
        return this.e15DtEndEff;
    }

    public void setE15CodCompAnia(int e15CodCompAnia) {
        this.e15CodCompAnia = e15CodCompAnia;
    }

    public int getE15CodCompAnia() {
        return this.e15CodCompAnia;
    }

    public void setE15CodSogg(String e15CodSogg) {
        this.e15CodSogg = Functions.subString(e15CodSogg, Len.E15_COD_SOGG);
    }

    public String getE15CodSogg() {
        return this.e15CodSogg;
    }

    public String getE15CodSoggFormatted() {
        return Functions.padBlanks(getE15CodSogg(), Len.E15_COD_SOGG);
    }

    public void setE15TpRappAna(String e15TpRappAna) {
        this.e15TpRappAna = Functions.subString(e15TpRappAna, Len.E15_TP_RAPP_ANA);
    }

    public String getE15TpRappAna() {
        return this.e15TpRappAna;
    }

    public void setE15DsRiga(long e15DsRiga) {
        this.e15DsRiga = e15DsRiga;
    }

    public long getE15DsRiga() {
        return this.e15DsRiga;
    }

    public void setE15DsOperSql(char e15DsOperSql) {
        this.e15DsOperSql = e15DsOperSql;
    }

    public char getE15DsOperSql() {
        return this.e15DsOperSql;
    }

    public void setE15DsVer(int e15DsVer) {
        this.e15DsVer = e15DsVer;
    }

    public int getE15DsVer() {
        return this.e15DsVer;
    }

    public void setE15DsTsIniCptz(long e15DsTsIniCptz) {
        this.e15DsTsIniCptz = e15DsTsIniCptz;
    }

    public long getE15DsTsIniCptz() {
        return this.e15DsTsIniCptz;
    }

    public void setE15DsTsEndCptz(long e15DsTsEndCptz) {
        this.e15DsTsEndCptz = e15DsTsEndCptz;
    }

    public long getE15DsTsEndCptz() {
        return this.e15DsTsEndCptz;
    }

    public void setE15DsUtente(String e15DsUtente) {
        this.e15DsUtente = Functions.subString(e15DsUtente, Len.E15_DS_UTENTE);
    }

    public String getE15DsUtente() {
        return this.e15DsUtente;
    }

    public void setE15DsStatoElab(char e15DsStatoElab) {
        this.e15DsStatoElab = e15DsStatoElab;
    }

    public char getE15DsStatoElab() {
        return this.e15DsStatoElab;
    }

    public void setE15FlCoincTitEff(char e15FlCoincTitEff) {
        this.e15FlCoincTitEff = e15FlCoincTitEff;
    }

    public char getE15FlCoincTitEff() {
        return this.e15FlCoincTitEff;
    }

    public void setE15FlPersEspPol(char e15FlPersEspPol) {
        this.e15FlPersEspPol = e15FlPersEspPol;
    }

    public char getE15FlPersEspPol() {
        return this.e15FlPersEspPol;
    }

    public void setE15DescPersEspPolVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        e15DescPersEspPolLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        e15DescPersEspPol = MarshalByte.readString(buffer, position, Len.E15_DESC_PERS_ESP_POL);
    }

    public byte[] getE15DescPersEspPolVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, e15DescPersEspPolLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, e15DescPersEspPol, Len.E15_DESC_PERS_ESP_POL);
        return buffer;
    }

    public void setE15DescPersEspPolLen(short e15DescPersEspPolLen) {
        this.e15DescPersEspPolLen = e15DescPersEspPolLen;
    }

    public short getE15DescPersEspPolLen() {
        return this.e15DescPersEspPolLen;
    }

    public void setE15DescPersEspPol(String e15DescPersEspPol) {
        this.e15DescPersEspPol = Functions.subString(e15DescPersEspPol, Len.E15_DESC_PERS_ESP_POL);
    }

    public String getE15DescPersEspPol() {
        return this.e15DescPersEspPol;
    }

    public void setE15TpLegCntr(String e15TpLegCntr) {
        this.e15TpLegCntr = Functions.subString(e15TpLegCntr, Len.E15_TP_LEG_CNTR);
    }

    public String getE15TpLegCntr() {
        return this.e15TpLegCntr;
    }

    public String getE15TpLegCntrFormatted() {
        return Functions.padBlanks(getE15TpLegCntr(), Len.E15_TP_LEG_CNTR);
    }

    public void setE15DescLegCntrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        e15DescLegCntrLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        e15DescLegCntr = MarshalByte.readString(buffer, position, Len.E15_DESC_LEG_CNTR);
    }

    public byte[] getE15DescLegCntrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, e15DescLegCntrLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, e15DescLegCntr, Len.E15_DESC_LEG_CNTR);
        return buffer;
    }

    public void setE15DescLegCntrLen(short e15DescLegCntrLen) {
        this.e15DescLegCntrLen = e15DescLegCntrLen;
    }

    public short getE15DescLegCntrLen() {
        return this.e15DescLegCntrLen;
    }

    public void setE15DescLegCntr(String e15DescLegCntr) {
        this.e15DescLegCntr = Functions.subString(e15DescLegCntr, Len.E15_DESC_LEG_CNTR);
    }

    public String getE15DescLegCntr() {
        return this.e15DescLegCntr;
    }

    public void setE15TpLegPercBnficr(String e15TpLegPercBnficr) {
        this.e15TpLegPercBnficr = Functions.subString(e15TpLegPercBnficr, Len.E15_TP_LEG_PERC_BNFICR);
    }

    public String getE15TpLegPercBnficr() {
        return this.e15TpLegPercBnficr;
    }

    public String getE15TpLegPercBnficrFormatted() {
        return Functions.padBlanks(getE15TpLegPercBnficr(), Len.E15_TP_LEG_PERC_BNFICR);
    }

    public void setE15DLegPercBnficrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        e15DLegPercBnficrLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        e15DLegPercBnficr = MarshalByte.readString(buffer, position, Len.E15_D_LEG_PERC_BNFICR);
    }

    public byte[] getE15DLegPercBnficrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, e15DLegPercBnficrLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, e15DLegPercBnficr, Len.E15_D_LEG_PERC_BNFICR);
        return buffer;
    }

    public void setE15DLegPercBnficrLen(short e15DLegPercBnficrLen) {
        this.e15DLegPercBnficrLen = e15DLegPercBnficrLen;
    }

    public short getE15DLegPercBnficrLen() {
        return this.e15DLegPercBnficrLen;
    }

    public void setE15DLegPercBnficr(String e15DLegPercBnficr) {
        this.e15DLegPercBnficr = Functions.subString(e15DLegPercBnficr, Len.E15_D_LEG_PERC_BNFICR);
    }

    public String getE15DLegPercBnficr() {
        return this.e15DLegPercBnficr;
    }

    public void setE15TpCntCorr(String e15TpCntCorr) {
        this.e15TpCntCorr = Functions.subString(e15TpCntCorr, Len.E15_TP_CNT_CORR);
    }

    public String getE15TpCntCorr() {
        return this.e15TpCntCorr;
    }

    public String getE15TpCntCorrFormatted() {
        return Functions.padBlanks(getE15TpCntCorr(), Len.E15_TP_CNT_CORR);
    }

    public void setE15TpCoincPicPac(String e15TpCoincPicPac) {
        this.e15TpCoincPicPac = Functions.subString(e15TpCoincPicPac, Len.E15_TP_COINC_PIC_PAC);
    }

    public String getE15TpCoincPicPac() {
        return this.e15TpCoincPicPac;
    }

    public String getE15TpCoincPicPacFormatted() {
        return Functions.padBlanks(getE15TpCoincPicPac(), Len.E15_TP_COINC_PIC_PAC);
    }

    public E15IdMoviChiu getE15IdMoviChiu() {
        return e15IdMoviChiu;
    }

    public E15IdRappAnaCollg getE15IdRappAnaCollg() {
        return e15IdRappAnaCollg;
    }

    public E15IdSegmentazCli getE15IdSegmentazCli() {
        return e15IdSegmentazCli;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int E15_TP_OGG = 2;
        public static final int E15_COD_SOGG = 20;
        public static final int E15_TP_RAPP_ANA = 2;
        public static final int E15_DS_UTENTE = 20;
        public static final int E15_DESC_PERS_ESP_POL = 250;
        public static final int E15_TP_LEG_CNTR = 2;
        public static final int E15_DESC_LEG_CNTR = 250;
        public static final int E15_TP_LEG_PERC_BNFICR = 2;
        public static final int E15_D_LEG_PERC_BNFICR = 250;
        public static final int E15_TP_CNT_CORR = 2;
        public static final int E15_TP_COINC_PIC_PAC = 2;
        public static final int E15_ID_EST_RAPP_ANA = 5;
        public static final int E15_ID_RAPP_ANA = 5;
        public static final int E15_ID_OGG = 5;
        public static final int E15_ID_MOVI_CRZ = 5;
        public static final int E15_DT_INI_EFF = 5;
        public static final int E15_DT_END_EFF = 5;
        public static final int E15_COD_COMP_ANIA = 3;
        public static final int E15_DS_RIGA = 6;
        public static final int E15_DS_OPER_SQL = 1;
        public static final int E15_DS_VER = 5;
        public static final int E15_DS_TS_INI_CPTZ = 10;
        public static final int E15_DS_TS_END_CPTZ = 10;
        public static final int E15_DS_STATO_ELAB = 1;
        public static final int E15_FL_COINC_TIT_EFF = 1;
        public static final int E15_FL_PERS_ESP_POL = 1;
        public static final int E15_DESC_PERS_ESP_POL_LEN = 2;
        public static final int E15_DESC_PERS_ESP_POL_VCHAR = E15_DESC_PERS_ESP_POL_LEN + E15_DESC_PERS_ESP_POL;
        public static final int E15_DESC_LEG_CNTR_LEN = 2;
        public static final int E15_DESC_LEG_CNTR_VCHAR = E15_DESC_LEG_CNTR_LEN + E15_DESC_LEG_CNTR;
        public static final int E15_D_LEG_PERC_BNFICR_LEN = 2;
        public static final int E15_D_LEG_PERC_BNFICR_VCHAR = E15_D_LEG_PERC_BNFICR_LEN + E15_D_LEG_PERC_BNFICR;
        public static final int EST_RAPP_ANA = E15_ID_EST_RAPP_ANA + E15_ID_RAPP_ANA + E15IdRappAnaCollg.Len.E15_ID_RAPP_ANA_COLLG + E15_ID_OGG + E15_TP_OGG + E15_ID_MOVI_CRZ + E15IdMoviChiu.Len.E15_ID_MOVI_CHIU + E15_DT_INI_EFF + E15_DT_END_EFF + E15_COD_COMP_ANIA + E15_COD_SOGG + E15_TP_RAPP_ANA + E15_DS_RIGA + E15_DS_OPER_SQL + E15_DS_VER + E15_DS_TS_INI_CPTZ + E15_DS_TS_END_CPTZ + E15_DS_UTENTE + E15_DS_STATO_ELAB + E15IdSegmentazCli.Len.E15_ID_SEGMENTAZ_CLI + E15_FL_COINC_TIT_EFF + E15_FL_PERS_ESP_POL + E15_DESC_PERS_ESP_POL_VCHAR + E15_TP_LEG_CNTR + E15_DESC_LEG_CNTR_VCHAR + E15_TP_LEG_PERC_BNFICR + E15_D_LEG_PERC_BNFICR_VCHAR + E15_TP_CNT_CORR + E15_TP_COINC_PIC_PAC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E15_ID_EST_RAPP_ANA = 9;
            public static final int E15_ID_RAPP_ANA = 9;
            public static final int E15_ID_OGG = 9;
            public static final int E15_ID_MOVI_CRZ = 9;
            public static final int E15_DT_INI_EFF = 8;
            public static final int E15_DT_END_EFF = 8;
            public static final int E15_COD_COMP_ANIA = 5;
            public static final int E15_DS_RIGA = 10;
            public static final int E15_DS_VER = 9;
            public static final int E15_DS_TS_INI_CPTZ = 18;
            public static final int E15_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
