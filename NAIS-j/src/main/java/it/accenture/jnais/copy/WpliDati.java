package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpliDtVlt;
import it.accenture.jnais.ws.redefines.WpliIdMoviChiu;
import it.accenture.jnais.ws.redefines.WpliIdRappAna;
import it.accenture.jnais.ws.redefines.WpliImpLiq;
import it.accenture.jnais.ws.redefines.WpliPcLiq;

/**Original name: WPLI-DATI<br>
 * Variable: WPLI-DATI from copybook LCCVPLI1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WpliDati {

    //==== PROPERTIES ====
    //Original name: WPLI-ID-PERC-LIQ
    private int wpliIdPercLiq = DefaultValues.INT_VAL;
    //Original name: WPLI-ID-BNFICR-LIQ
    private int wpliIdBnficrLiq = DefaultValues.INT_VAL;
    //Original name: WPLI-ID-MOVI-CRZ
    private int wpliIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WPLI-ID-MOVI-CHIU
    private WpliIdMoviChiu wpliIdMoviChiu = new WpliIdMoviChiu();
    //Original name: WPLI-ID-RAPP-ANA
    private WpliIdRappAna wpliIdRappAna = new WpliIdRappAna();
    //Original name: WPLI-DT-INI-EFF
    private int wpliDtIniEff = DefaultValues.INT_VAL;
    //Original name: WPLI-DT-END-EFF
    private int wpliDtEndEff = DefaultValues.INT_VAL;
    //Original name: WPLI-COD-COMP-ANIA
    private int wpliCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WPLI-PC-LIQ
    private WpliPcLiq wpliPcLiq = new WpliPcLiq();
    //Original name: WPLI-IMP-LIQ
    private WpliImpLiq wpliImpLiq = new WpliImpLiq();
    //Original name: WPLI-TP-MEZ-PAG
    private String wpliTpMezPag = DefaultValues.stringVal(Len.WPLI_TP_MEZ_PAG);
    //Original name: WPLI-ITER-PAG-AVV
    private char wpliIterPagAvv = DefaultValues.CHAR_VAL;
    //Original name: WPLI-DS-RIGA
    private long wpliDsRiga = DefaultValues.LONG_VAL;
    //Original name: WPLI-DS-OPER-SQL
    private char wpliDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WPLI-DS-VER
    private int wpliDsVer = DefaultValues.INT_VAL;
    //Original name: WPLI-DS-TS-INI-CPTZ
    private long wpliDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WPLI-DS-TS-END-CPTZ
    private long wpliDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WPLI-DS-UTENTE
    private String wpliDsUtente = DefaultValues.stringVal(Len.WPLI_DS_UTENTE);
    //Original name: WPLI-DS-STATO-ELAB
    private char wpliDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WPLI-DT-VLT
    private WpliDtVlt wpliDtVlt = new WpliDtVlt();
    //Original name: WPLI-INT-CNT-CORR-ACCR
    private String wpliIntCntCorrAccr = DefaultValues.stringVal(Len.WPLI_INT_CNT_CORR_ACCR);
    //Original name: WPLI-COD-IBAN-RIT-CON
    private String wpliCodIbanRitCon = DefaultValues.stringVal(Len.WPLI_COD_IBAN_RIT_CON);

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wpliIdPercLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPLI_ID_PERC_LIQ, 0);
        position += Len.WPLI_ID_PERC_LIQ;
        wpliIdBnficrLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPLI_ID_BNFICR_LIQ, 0);
        position += Len.WPLI_ID_BNFICR_LIQ;
        wpliIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPLI_ID_MOVI_CRZ, 0);
        position += Len.WPLI_ID_MOVI_CRZ;
        wpliIdMoviChiu.setWpliIdMoviChiuFromBuffer(buffer, position);
        position += WpliIdMoviChiu.Len.WPLI_ID_MOVI_CHIU;
        wpliIdRappAna.setWpliIdRappAnaFromBuffer(buffer, position);
        position += WpliIdRappAna.Len.WPLI_ID_RAPP_ANA;
        wpliDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPLI_DT_INI_EFF, 0);
        position += Len.WPLI_DT_INI_EFF;
        wpliDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPLI_DT_END_EFF, 0);
        position += Len.WPLI_DT_END_EFF;
        wpliCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPLI_COD_COMP_ANIA, 0);
        position += Len.WPLI_COD_COMP_ANIA;
        wpliPcLiq.setWpliPcLiqFromBuffer(buffer, position);
        position += WpliPcLiq.Len.WPLI_PC_LIQ;
        wpliImpLiq.setWpliImpLiqFromBuffer(buffer, position);
        position += WpliImpLiq.Len.WPLI_IMP_LIQ;
        wpliTpMezPag = MarshalByte.readString(buffer, position, Len.WPLI_TP_MEZ_PAG);
        position += Len.WPLI_TP_MEZ_PAG;
        wpliIterPagAvv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpliDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPLI_DS_RIGA, 0);
        position += Len.WPLI_DS_RIGA;
        wpliDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpliDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPLI_DS_VER, 0);
        position += Len.WPLI_DS_VER;
        wpliDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPLI_DS_TS_INI_CPTZ, 0);
        position += Len.WPLI_DS_TS_INI_CPTZ;
        wpliDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPLI_DS_TS_END_CPTZ, 0);
        position += Len.WPLI_DS_TS_END_CPTZ;
        wpliDsUtente = MarshalByte.readString(buffer, position, Len.WPLI_DS_UTENTE);
        position += Len.WPLI_DS_UTENTE;
        wpliDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpliDtVlt.setWpliDtVltFromBuffer(buffer, position);
        position += WpliDtVlt.Len.WPLI_DT_VLT;
        wpliIntCntCorrAccr = MarshalByte.readString(buffer, position, Len.WPLI_INT_CNT_CORR_ACCR);
        position += Len.WPLI_INT_CNT_CORR_ACCR;
        wpliCodIbanRitCon = MarshalByte.readString(buffer, position, Len.WPLI_COD_IBAN_RIT_CON);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wpliIdPercLiq, Len.Int.WPLI_ID_PERC_LIQ, 0);
        position += Len.WPLI_ID_PERC_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, wpliIdBnficrLiq, Len.Int.WPLI_ID_BNFICR_LIQ, 0);
        position += Len.WPLI_ID_BNFICR_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, wpliIdMoviCrz, Len.Int.WPLI_ID_MOVI_CRZ, 0);
        position += Len.WPLI_ID_MOVI_CRZ;
        wpliIdMoviChiu.getWpliIdMoviChiuAsBuffer(buffer, position);
        position += WpliIdMoviChiu.Len.WPLI_ID_MOVI_CHIU;
        wpliIdRappAna.getWpliIdRappAnaAsBuffer(buffer, position);
        position += WpliIdRappAna.Len.WPLI_ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, wpliDtIniEff, Len.Int.WPLI_DT_INI_EFF, 0);
        position += Len.WPLI_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpliDtEndEff, Len.Int.WPLI_DT_END_EFF, 0);
        position += Len.WPLI_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpliCodCompAnia, Len.Int.WPLI_COD_COMP_ANIA, 0);
        position += Len.WPLI_COD_COMP_ANIA;
        wpliPcLiq.getWpliPcLiqAsBuffer(buffer, position);
        position += WpliPcLiq.Len.WPLI_PC_LIQ;
        wpliImpLiq.getWpliImpLiqAsBuffer(buffer, position);
        position += WpliImpLiq.Len.WPLI_IMP_LIQ;
        MarshalByte.writeString(buffer, position, wpliTpMezPag, Len.WPLI_TP_MEZ_PAG);
        position += Len.WPLI_TP_MEZ_PAG;
        MarshalByte.writeChar(buffer, position, wpliIterPagAvv);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, wpliDsRiga, Len.Int.WPLI_DS_RIGA, 0);
        position += Len.WPLI_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wpliDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wpliDsVer, Len.Int.WPLI_DS_VER, 0);
        position += Len.WPLI_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wpliDsTsIniCptz, Len.Int.WPLI_DS_TS_INI_CPTZ, 0);
        position += Len.WPLI_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wpliDsTsEndCptz, Len.Int.WPLI_DS_TS_END_CPTZ, 0);
        position += Len.WPLI_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wpliDsUtente, Len.WPLI_DS_UTENTE);
        position += Len.WPLI_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wpliDsStatoElab);
        position += Types.CHAR_SIZE;
        wpliDtVlt.getWpliDtVltAsBuffer(buffer, position);
        position += WpliDtVlt.Len.WPLI_DT_VLT;
        MarshalByte.writeString(buffer, position, wpliIntCntCorrAccr, Len.WPLI_INT_CNT_CORR_ACCR);
        position += Len.WPLI_INT_CNT_CORR_ACCR;
        MarshalByte.writeString(buffer, position, wpliCodIbanRitCon, Len.WPLI_COD_IBAN_RIT_CON);
        return buffer;
    }

    public void initDatiSpaces() {
        wpliIdPercLiq = Types.INVALID_INT_VAL;
        wpliIdBnficrLiq = Types.INVALID_INT_VAL;
        wpliIdMoviCrz = Types.INVALID_INT_VAL;
        wpliIdMoviChiu.initWpliIdMoviChiuSpaces();
        wpliIdRappAna.initWpliIdRappAnaSpaces();
        wpliDtIniEff = Types.INVALID_INT_VAL;
        wpliDtEndEff = Types.INVALID_INT_VAL;
        wpliCodCompAnia = Types.INVALID_INT_VAL;
        wpliPcLiq.initWpliPcLiqSpaces();
        wpliImpLiq.initWpliImpLiqSpaces();
        wpliTpMezPag = "";
        wpliIterPagAvv = Types.SPACE_CHAR;
        wpliDsRiga = Types.INVALID_LONG_VAL;
        wpliDsOperSql = Types.SPACE_CHAR;
        wpliDsVer = Types.INVALID_INT_VAL;
        wpliDsTsIniCptz = Types.INVALID_LONG_VAL;
        wpliDsTsEndCptz = Types.INVALID_LONG_VAL;
        wpliDsUtente = "";
        wpliDsStatoElab = Types.SPACE_CHAR;
        wpliDtVlt.initWpliDtVltSpaces();
        wpliIntCntCorrAccr = "";
        wpliCodIbanRitCon = "";
    }

    public void setWpliIdPercLiq(int wpliIdPercLiq) {
        this.wpliIdPercLiq = wpliIdPercLiq;
    }

    public int getWpliIdPercLiq() {
        return this.wpliIdPercLiq;
    }

    public void setWpliIdBnficrLiq(int wpliIdBnficrLiq) {
        this.wpliIdBnficrLiq = wpliIdBnficrLiq;
    }

    public int getWpliIdBnficrLiq() {
        return this.wpliIdBnficrLiq;
    }

    public void setWpliIdMoviCrz(int wpliIdMoviCrz) {
        this.wpliIdMoviCrz = wpliIdMoviCrz;
    }

    public int getWpliIdMoviCrz() {
        return this.wpliIdMoviCrz;
    }

    public void setWpliDtIniEff(int wpliDtIniEff) {
        this.wpliDtIniEff = wpliDtIniEff;
    }

    public int getWpliDtIniEff() {
        return this.wpliDtIniEff;
    }

    public void setWpliDtEndEff(int wpliDtEndEff) {
        this.wpliDtEndEff = wpliDtEndEff;
    }

    public int getWpliDtEndEff() {
        return this.wpliDtEndEff;
    }

    public void setWpliCodCompAnia(int wpliCodCompAnia) {
        this.wpliCodCompAnia = wpliCodCompAnia;
    }

    public int getWpliCodCompAnia() {
        return this.wpliCodCompAnia;
    }

    public void setWpliTpMezPag(String wpliTpMezPag) {
        this.wpliTpMezPag = Functions.subString(wpliTpMezPag, Len.WPLI_TP_MEZ_PAG);
    }

    public String getWpliTpMezPag() {
        return this.wpliTpMezPag;
    }

    public void setWpliIterPagAvv(char wpliIterPagAvv) {
        this.wpliIterPagAvv = wpliIterPagAvv;
    }

    public char getWpliIterPagAvv() {
        return this.wpliIterPagAvv;
    }

    public void setWpliDsRiga(long wpliDsRiga) {
        this.wpliDsRiga = wpliDsRiga;
    }

    public long getWpliDsRiga() {
        return this.wpliDsRiga;
    }

    public void setWpliDsOperSql(char wpliDsOperSql) {
        this.wpliDsOperSql = wpliDsOperSql;
    }

    public char getWpliDsOperSql() {
        return this.wpliDsOperSql;
    }

    public void setWpliDsVer(int wpliDsVer) {
        this.wpliDsVer = wpliDsVer;
    }

    public int getWpliDsVer() {
        return this.wpliDsVer;
    }

    public void setWpliDsTsIniCptz(long wpliDsTsIniCptz) {
        this.wpliDsTsIniCptz = wpliDsTsIniCptz;
    }

    public long getWpliDsTsIniCptz() {
        return this.wpliDsTsIniCptz;
    }

    public void setWpliDsTsEndCptz(long wpliDsTsEndCptz) {
        this.wpliDsTsEndCptz = wpliDsTsEndCptz;
    }

    public long getWpliDsTsEndCptz() {
        return this.wpliDsTsEndCptz;
    }

    public void setWpliDsUtente(String wpliDsUtente) {
        this.wpliDsUtente = Functions.subString(wpliDsUtente, Len.WPLI_DS_UTENTE);
    }

    public String getWpliDsUtente() {
        return this.wpliDsUtente;
    }

    public void setWpliDsStatoElab(char wpliDsStatoElab) {
        this.wpliDsStatoElab = wpliDsStatoElab;
    }

    public char getWpliDsStatoElab() {
        return this.wpliDsStatoElab;
    }

    public void setWpliIntCntCorrAccr(String wpliIntCntCorrAccr) {
        this.wpliIntCntCorrAccr = Functions.subString(wpliIntCntCorrAccr, Len.WPLI_INT_CNT_CORR_ACCR);
    }

    public String getWpliIntCntCorrAccr() {
        return this.wpliIntCntCorrAccr;
    }

    public void setWpliCodIbanRitCon(String wpliCodIbanRitCon) {
        this.wpliCodIbanRitCon = Functions.subString(wpliCodIbanRitCon, Len.WPLI_COD_IBAN_RIT_CON);
    }

    public String getWpliCodIbanRitCon() {
        return this.wpliCodIbanRitCon;
    }

    public WpliDtVlt getWpliDtVlt() {
        return wpliDtVlt;
    }

    public WpliIdMoviChiu getWpliIdMoviChiu() {
        return wpliIdMoviChiu;
    }

    public WpliIdRappAna getWpliIdRappAna() {
        return wpliIdRappAna;
    }

    public WpliImpLiq getWpliImpLiq() {
        return wpliImpLiq;
    }

    public WpliPcLiq getWpliPcLiq() {
        return wpliPcLiq;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPLI_ID_PERC_LIQ = 5;
        public static final int WPLI_ID_BNFICR_LIQ = 5;
        public static final int WPLI_ID_MOVI_CRZ = 5;
        public static final int WPLI_DT_INI_EFF = 5;
        public static final int WPLI_DT_END_EFF = 5;
        public static final int WPLI_COD_COMP_ANIA = 3;
        public static final int WPLI_TP_MEZ_PAG = 2;
        public static final int WPLI_ITER_PAG_AVV = 1;
        public static final int WPLI_DS_RIGA = 6;
        public static final int WPLI_DS_OPER_SQL = 1;
        public static final int WPLI_DS_VER = 5;
        public static final int WPLI_DS_TS_INI_CPTZ = 10;
        public static final int WPLI_DS_TS_END_CPTZ = 10;
        public static final int WPLI_DS_UTENTE = 20;
        public static final int WPLI_DS_STATO_ELAB = 1;
        public static final int WPLI_INT_CNT_CORR_ACCR = 100;
        public static final int WPLI_COD_IBAN_RIT_CON = 34;
        public static final int DATI = WPLI_ID_PERC_LIQ + WPLI_ID_BNFICR_LIQ + WPLI_ID_MOVI_CRZ + WpliIdMoviChiu.Len.WPLI_ID_MOVI_CHIU + WpliIdRappAna.Len.WPLI_ID_RAPP_ANA + WPLI_DT_INI_EFF + WPLI_DT_END_EFF + WPLI_COD_COMP_ANIA + WpliPcLiq.Len.WPLI_PC_LIQ + WpliImpLiq.Len.WPLI_IMP_LIQ + WPLI_TP_MEZ_PAG + WPLI_ITER_PAG_AVV + WPLI_DS_RIGA + WPLI_DS_OPER_SQL + WPLI_DS_VER + WPLI_DS_TS_INI_CPTZ + WPLI_DS_TS_END_CPTZ + WPLI_DS_UTENTE + WPLI_DS_STATO_ELAB + WpliDtVlt.Len.WPLI_DT_VLT + WPLI_INT_CNT_CORR_ACCR + WPLI_COD_IBAN_RIT_CON;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPLI_ID_PERC_LIQ = 9;
            public static final int WPLI_ID_BNFICR_LIQ = 9;
            public static final int WPLI_ID_MOVI_CRZ = 9;
            public static final int WPLI_DT_INI_EFF = 8;
            public static final int WPLI_DT_END_EFF = 8;
            public static final int WPLI_COD_COMP_ANIA = 5;
            public static final int WPLI_DS_RIGA = 10;
            public static final int WPLI_DS_VER = 9;
            public static final int WPLI_DS_TS_INI_CPTZ = 18;
            public static final int WPLI_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
