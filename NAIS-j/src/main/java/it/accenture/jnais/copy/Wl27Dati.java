package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WL27-DATI<br>
 * Variable: WL27-DATI from copybook LCCVL271<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wl27Dati {

    //==== PROPERTIES ====
    //Original name: WL27-ID-PREV
    private int idPrev = DefaultValues.INT_VAL;
    //Original name: WL27-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: WL27-STAT-PREV
    private String statPrev = DefaultValues.stringVal(Len.STAT_PREV);
    //Original name: WL27-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: WL27-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: WL27-IB-OGG
    private String ibOgg = DefaultValues.stringVal(Len.IB_OGG);
    //Original name: WL27-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WL27-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: WL27-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WL27-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: WL27-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setIdPrev(int idPrev) {
        this.idPrev = idPrev;
    }

    public int getIdPrev() {
        return this.idPrev;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setStatPrev(String statPrev) {
        this.statPrev = Functions.subString(statPrev, Len.STAT_PREV);
    }

    public String getStatPrev() {
        return this.statPrev;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setIbOgg(String ibOgg) {
        this.ibOgg = Functions.subString(ibOgg, Len.IB_OGG);
    }

    public String getIbOgg() {
        return this.ibOgg;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int STAT_PREV = 2;
        public static final int TP_OGG = 2;
        public static final int IB_OGG = 40;
        public static final int DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
