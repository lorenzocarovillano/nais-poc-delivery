package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WbelDtDormienza;
import it.accenture.jnais.ws.redefines.WbelDtRiserveSomP;
import it.accenture.jnais.ws.redefines.WbelDtUltDocto;
import it.accenture.jnais.ws.redefines.WbelDtVlt;
import it.accenture.jnais.ws.redefines.WbelIdAssto;
import it.accenture.jnais.ws.redefines.WbelIdMoviChiu;
import it.accenture.jnais.ws.redefines.WbelIdRappAna;
import it.accenture.jnais.ws.redefines.WbelImpIntrRitPag;
import it.accenture.jnais.ws.redefines.WbelImpLrdLiqto;
import it.accenture.jnais.ws.redefines.WbelImpNetLiqto;
import it.accenture.jnais.ws.redefines.WbelImpst252Ipt;
import it.accenture.jnais.ws.redefines.WbelImpstBolloTotV;
import it.accenture.jnais.ws.redefines.WbelImpstIpt;
import it.accenture.jnais.ws.redefines.WbelImpstIrpefIpt;
import it.accenture.jnais.ws.redefines.WbelImpstPrvrIpt;
import it.accenture.jnais.ws.redefines.WbelImpstSost1382011;
import it.accenture.jnais.ws.redefines.WbelImpstSost662014;
import it.accenture.jnais.ws.redefines.WbelImpstSostIpt;
import it.accenture.jnais.ws.redefines.WbelImpstVis1382011;
import it.accenture.jnais.ws.redefines.WbelImpstVis662014;
import it.accenture.jnais.ws.redefines.WbelPcLiq;
import it.accenture.jnais.ws.redefines.WbelRitAccIpt;
import it.accenture.jnais.ws.redefines.WbelRitTfrIpt;
import it.accenture.jnais.ws.redefines.WbelRitVisIpt;
import it.accenture.jnais.ws.redefines.WbelTaxSep;

/**Original name: WBEL-DATI<br>
 * Variable: WBEL-DATI from copybook LCCVBEL1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WbelDati {

    //==== PROPERTIES ====
    //Original name: WBEL-ID-BNFICR-LIQ
    private int wbelIdBnficrLiq = DefaultValues.INT_VAL;
    //Original name: WBEL-ID-LIQ
    private int wbelIdLiq = DefaultValues.INT_VAL;
    //Original name: WBEL-ID-MOVI-CRZ
    private int wbelIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WBEL-ID-MOVI-CHIU
    private WbelIdMoviChiu wbelIdMoviChiu = new WbelIdMoviChiu();
    //Original name: WBEL-COD-COMP-ANIA
    private int wbelCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WBEL-DT-INI-EFF
    private int wbelDtIniEff = DefaultValues.INT_VAL;
    //Original name: WBEL-DT-END-EFF
    private int wbelDtEndEff = DefaultValues.INT_VAL;
    //Original name: WBEL-ID-RAPP-ANA
    private WbelIdRappAna wbelIdRappAna = new WbelIdRappAna();
    //Original name: WBEL-COD-BNFICR
    private String wbelCodBnficr = DefaultValues.stringVal(Len.WBEL_COD_BNFICR);
    //Original name: WBEL-DESC-BNFICR
    private String wbelDescBnficr = DefaultValues.stringVal(Len.WBEL_DESC_BNFICR);
    //Original name: WBEL-PC-LIQ
    private WbelPcLiq wbelPcLiq = new WbelPcLiq();
    //Original name: WBEL-ESRCN-ATTVT-IMPRS
    private char wbelEsrcnAttvtImprs = DefaultValues.CHAR_VAL;
    //Original name: WBEL-TP-IND-BNFICR
    private String wbelTpIndBnficr = DefaultValues.stringVal(Len.WBEL_TP_IND_BNFICR);
    //Original name: WBEL-FL-ESE
    private char wbelFlEse = DefaultValues.CHAR_VAL;
    //Original name: WBEL-FL-IRREV
    private char wbelFlIrrev = DefaultValues.CHAR_VAL;
    //Original name: WBEL-IMP-LRD-LIQTO
    private WbelImpLrdLiqto wbelImpLrdLiqto = new WbelImpLrdLiqto();
    //Original name: WBEL-IMPST-IPT
    private WbelImpstIpt wbelImpstIpt = new WbelImpstIpt();
    //Original name: WBEL-IMP-NET-LIQTO
    private WbelImpNetLiqto wbelImpNetLiqto = new WbelImpNetLiqto();
    //Original name: WBEL-RIT-ACC-IPT
    private WbelRitAccIpt wbelRitAccIpt = new WbelRitAccIpt();
    //Original name: WBEL-RIT-VIS-IPT
    private WbelRitVisIpt wbelRitVisIpt = new WbelRitVisIpt();
    //Original name: WBEL-RIT-TFR-IPT
    private WbelRitTfrIpt wbelRitTfrIpt = new WbelRitTfrIpt();
    //Original name: WBEL-IMPST-IRPEF-IPT
    private WbelImpstIrpefIpt wbelImpstIrpefIpt = new WbelImpstIrpefIpt();
    //Original name: WBEL-IMPST-SOST-IPT
    private WbelImpstSostIpt wbelImpstSostIpt = new WbelImpstSostIpt();
    //Original name: WBEL-IMPST-PRVR-IPT
    private WbelImpstPrvrIpt wbelImpstPrvrIpt = new WbelImpstPrvrIpt();
    //Original name: WBEL-IMPST-252-IPT
    private WbelImpst252Ipt wbelImpst252Ipt = new WbelImpst252Ipt();
    //Original name: WBEL-ID-ASSTO
    private WbelIdAssto wbelIdAssto = new WbelIdAssto();
    //Original name: WBEL-TAX-SEP
    private WbelTaxSep wbelTaxSep = new WbelTaxSep();
    //Original name: WBEL-DT-RISERVE-SOM-P
    private WbelDtRiserveSomP wbelDtRiserveSomP = new WbelDtRiserveSomP();
    //Original name: WBEL-DT-VLT
    private WbelDtVlt wbelDtVlt = new WbelDtVlt();
    //Original name: WBEL-TP-STAT-LIQ-BNFICR
    private String wbelTpStatLiqBnficr = DefaultValues.stringVal(Len.WBEL_TP_STAT_LIQ_BNFICR);
    //Original name: WBEL-DS-RIGA
    private long wbelDsRiga = DefaultValues.LONG_VAL;
    //Original name: WBEL-DS-OPER-SQL
    private char wbelDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WBEL-DS-VER
    private int wbelDsVer = DefaultValues.INT_VAL;
    //Original name: WBEL-DS-TS-INI-CPTZ
    private long wbelDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WBEL-DS-TS-END-CPTZ
    private long wbelDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WBEL-DS-UTENTE
    private String wbelDsUtente = DefaultValues.stringVal(Len.WBEL_DS_UTENTE);
    //Original name: WBEL-DS-STATO-ELAB
    private char wbelDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WBEL-RICH-CALC-CNBT-INP
    private char wbelRichCalcCnbtInp = DefaultValues.CHAR_VAL;
    //Original name: WBEL-IMP-INTR-RIT-PAG
    private WbelImpIntrRitPag wbelImpIntrRitPag = new WbelImpIntrRitPag();
    //Original name: WBEL-DT-ULT-DOCTO
    private WbelDtUltDocto wbelDtUltDocto = new WbelDtUltDocto();
    //Original name: WBEL-DT-DORMIENZA
    private WbelDtDormienza wbelDtDormienza = new WbelDtDormienza();
    //Original name: WBEL-IMPST-BOLLO-TOT-V
    private WbelImpstBolloTotV wbelImpstBolloTotV = new WbelImpstBolloTotV();
    //Original name: WBEL-IMPST-VIS-1382011
    private WbelImpstVis1382011 wbelImpstVis1382011 = new WbelImpstVis1382011();
    //Original name: WBEL-IMPST-SOST-1382011
    private WbelImpstSost1382011 wbelImpstSost1382011 = new WbelImpstSost1382011();
    //Original name: WBEL-IMPST-VIS-662014
    private WbelImpstVis662014 wbelImpstVis662014 = new WbelImpstVis662014();
    //Original name: WBEL-IMPST-SOST-662014
    private WbelImpstSost662014 wbelImpstSost662014 = new WbelImpstSost662014();

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wbelIdBnficrLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEL_ID_BNFICR_LIQ, 0);
        position += Len.WBEL_ID_BNFICR_LIQ;
        wbelIdLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEL_ID_LIQ, 0);
        position += Len.WBEL_ID_LIQ;
        wbelIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEL_ID_MOVI_CRZ, 0);
        position += Len.WBEL_ID_MOVI_CRZ;
        wbelIdMoviChiu.setWbelIdMoviChiuFromBuffer(buffer, position);
        position += WbelIdMoviChiu.Len.WBEL_ID_MOVI_CHIU;
        wbelCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEL_COD_COMP_ANIA, 0);
        position += Len.WBEL_COD_COMP_ANIA;
        wbelDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEL_DT_INI_EFF, 0);
        position += Len.WBEL_DT_INI_EFF;
        wbelDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEL_DT_END_EFF, 0);
        position += Len.WBEL_DT_END_EFF;
        wbelIdRappAna.setWbelIdRappAnaFromBuffer(buffer, position);
        position += WbelIdRappAna.Len.WBEL_ID_RAPP_ANA;
        wbelCodBnficr = MarshalByte.readString(buffer, position, Len.WBEL_COD_BNFICR);
        position += Len.WBEL_COD_BNFICR;
        wbelDescBnficr = MarshalByte.readString(buffer, position, Len.WBEL_DESC_BNFICR);
        position += Len.WBEL_DESC_BNFICR;
        wbelPcLiq.setWbelPcLiqFromBuffer(buffer, position);
        position += WbelPcLiq.Len.WBEL_PC_LIQ;
        wbelEsrcnAttvtImprs = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbelTpIndBnficr = MarshalByte.readString(buffer, position, Len.WBEL_TP_IND_BNFICR);
        position += Len.WBEL_TP_IND_BNFICR;
        wbelFlEse = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbelFlIrrev = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbelImpLrdLiqto.setWbelImpLrdLiqtoFromBuffer(buffer, position);
        position += WbelImpLrdLiqto.Len.WBEL_IMP_LRD_LIQTO;
        wbelImpstIpt.setWbelImpstIptFromBuffer(buffer, position);
        position += WbelImpstIpt.Len.WBEL_IMPST_IPT;
        wbelImpNetLiqto.setWbelImpNetLiqtoFromBuffer(buffer, position);
        position += WbelImpNetLiqto.Len.WBEL_IMP_NET_LIQTO;
        wbelRitAccIpt.setWbelRitAccIptFromBuffer(buffer, position);
        position += WbelRitAccIpt.Len.WBEL_RIT_ACC_IPT;
        wbelRitVisIpt.setWbelRitVisIptFromBuffer(buffer, position);
        position += WbelRitVisIpt.Len.WBEL_RIT_VIS_IPT;
        wbelRitTfrIpt.setWbelRitTfrIptFromBuffer(buffer, position);
        position += WbelRitTfrIpt.Len.WBEL_RIT_TFR_IPT;
        wbelImpstIrpefIpt.setWbelImpstIrpefIptFromBuffer(buffer, position);
        position += WbelImpstIrpefIpt.Len.WBEL_IMPST_IRPEF_IPT;
        wbelImpstSostIpt.setWbelImpstSostIptFromBuffer(buffer, position);
        position += WbelImpstSostIpt.Len.WBEL_IMPST_SOST_IPT;
        wbelImpstPrvrIpt.setWbelImpstPrvrIptFromBuffer(buffer, position);
        position += WbelImpstPrvrIpt.Len.WBEL_IMPST_PRVR_IPT;
        wbelImpst252Ipt.setWbelImpst252IptFromBuffer(buffer, position);
        position += WbelImpst252Ipt.Len.WBEL_IMPST252_IPT;
        wbelIdAssto.setWbelIdAsstoFromBuffer(buffer, position);
        position += WbelIdAssto.Len.WBEL_ID_ASSTO;
        wbelTaxSep.setWbelTaxSepFromBuffer(buffer, position);
        position += WbelTaxSep.Len.WBEL_TAX_SEP;
        wbelDtRiserveSomP.setWbelDtRiserveSomPFromBuffer(buffer, position);
        position += WbelDtRiserveSomP.Len.WBEL_DT_RISERVE_SOM_P;
        wbelDtVlt.setWbelDtVltFromBuffer(buffer, position);
        position += WbelDtVlt.Len.WBEL_DT_VLT;
        wbelTpStatLiqBnficr = MarshalByte.readString(buffer, position, Len.WBEL_TP_STAT_LIQ_BNFICR);
        position += Len.WBEL_TP_STAT_LIQ_BNFICR;
        wbelDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WBEL_DS_RIGA, 0);
        position += Len.WBEL_DS_RIGA;
        wbelDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbelDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WBEL_DS_VER, 0);
        position += Len.WBEL_DS_VER;
        wbelDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WBEL_DS_TS_INI_CPTZ, 0);
        position += Len.WBEL_DS_TS_INI_CPTZ;
        wbelDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WBEL_DS_TS_END_CPTZ, 0);
        position += Len.WBEL_DS_TS_END_CPTZ;
        wbelDsUtente = MarshalByte.readString(buffer, position, Len.WBEL_DS_UTENTE);
        position += Len.WBEL_DS_UTENTE;
        wbelDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbelRichCalcCnbtInp = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wbelImpIntrRitPag.setWbelImpIntrRitPagFromBuffer(buffer, position);
        position += WbelImpIntrRitPag.Len.WBEL_IMP_INTR_RIT_PAG;
        wbelDtUltDocto.setWbelDtUltDoctoFromBuffer(buffer, position);
        position += WbelDtUltDocto.Len.WBEL_DT_ULT_DOCTO;
        wbelDtDormienza.setWbelDtDormienzaFromBuffer(buffer, position);
        position += WbelDtDormienza.Len.WBEL_DT_DORMIENZA;
        wbelImpstBolloTotV.setWbelImpstBolloTotVFromBuffer(buffer, position);
        position += WbelImpstBolloTotV.Len.WBEL_IMPST_BOLLO_TOT_V;
        wbelImpstVis1382011.setWbelImpstVis1382011FromBuffer(buffer, position);
        position += WbelImpstVis1382011.Len.WBEL_IMPST_VIS1382011;
        wbelImpstSost1382011.setWbelImpstSost1382011FromBuffer(buffer, position);
        position += WbelImpstSost1382011.Len.WBEL_IMPST_SOST1382011;
        wbelImpstVis662014.setWbelImpstVis662014FromBuffer(buffer, position);
        position += WbelImpstVis662014.Len.WBEL_IMPST_VIS662014;
        wbelImpstSost662014.setWbelImpstSost662014FromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wbelIdBnficrLiq, Len.Int.WBEL_ID_BNFICR_LIQ, 0);
        position += Len.WBEL_ID_BNFICR_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, wbelIdLiq, Len.Int.WBEL_ID_LIQ, 0);
        position += Len.WBEL_ID_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, wbelIdMoviCrz, Len.Int.WBEL_ID_MOVI_CRZ, 0);
        position += Len.WBEL_ID_MOVI_CRZ;
        wbelIdMoviChiu.getWbelIdMoviChiuAsBuffer(buffer, position);
        position += WbelIdMoviChiu.Len.WBEL_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wbelCodCompAnia, Len.Int.WBEL_COD_COMP_ANIA, 0);
        position += Len.WBEL_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wbelDtIniEff, Len.Int.WBEL_DT_INI_EFF, 0);
        position += Len.WBEL_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wbelDtEndEff, Len.Int.WBEL_DT_END_EFF, 0);
        position += Len.WBEL_DT_END_EFF;
        wbelIdRappAna.getWbelIdRappAnaAsBuffer(buffer, position);
        position += WbelIdRappAna.Len.WBEL_ID_RAPP_ANA;
        MarshalByte.writeString(buffer, position, wbelCodBnficr, Len.WBEL_COD_BNFICR);
        position += Len.WBEL_COD_BNFICR;
        MarshalByte.writeString(buffer, position, wbelDescBnficr, Len.WBEL_DESC_BNFICR);
        position += Len.WBEL_DESC_BNFICR;
        wbelPcLiq.getWbelPcLiqAsBuffer(buffer, position);
        position += WbelPcLiq.Len.WBEL_PC_LIQ;
        MarshalByte.writeChar(buffer, position, wbelEsrcnAttvtImprs);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wbelTpIndBnficr, Len.WBEL_TP_IND_BNFICR);
        position += Len.WBEL_TP_IND_BNFICR;
        MarshalByte.writeChar(buffer, position, wbelFlEse);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wbelFlIrrev);
        position += Types.CHAR_SIZE;
        wbelImpLrdLiqto.getWbelImpLrdLiqtoAsBuffer(buffer, position);
        position += WbelImpLrdLiqto.Len.WBEL_IMP_LRD_LIQTO;
        wbelImpstIpt.getWbelImpstIptAsBuffer(buffer, position);
        position += WbelImpstIpt.Len.WBEL_IMPST_IPT;
        wbelImpNetLiqto.getWbelImpNetLiqtoAsBuffer(buffer, position);
        position += WbelImpNetLiqto.Len.WBEL_IMP_NET_LIQTO;
        wbelRitAccIpt.getWbelRitAccIptAsBuffer(buffer, position);
        position += WbelRitAccIpt.Len.WBEL_RIT_ACC_IPT;
        wbelRitVisIpt.getWbelRitVisIptAsBuffer(buffer, position);
        position += WbelRitVisIpt.Len.WBEL_RIT_VIS_IPT;
        wbelRitTfrIpt.getWbelRitTfrIptAsBuffer(buffer, position);
        position += WbelRitTfrIpt.Len.WBEL_RIT_TFR_IPT;
        wbelImpstIrpefIpt.getWbelImpstIrpefIptAsBuffer(buffer, position);
        position += WbelImpstIrpefIpt.Len.WBEL_IMPST_IRPEF_IPT;
        wbelImpstSostIpt.getWbelImpstSostIptAsBuffer(buffer, position);
        position += WbelImpstSostIpt.Len.WBEL_IMPST_SOST_IPT;
        wbelImpstPrvrIpt.getWbelImpstPrvrIptAsBuffer(buffer, position);
        position += WbelImpstPrvrIpt.Len.WBEL_IMPST_PRVR_IPT;
        wbelImpst252Ipt.getWbelImpst252IptAsBuffer(buffer, position);
        position += WbelImpst252Ipt.Len.WBEL_IMPST252_IPT;
        wbelIdAssto.getWbelIdAsstoAsBuffer(buffer, position);
        position += WbelIdAssto.Len.WBEL_ID_ASSTO;
        wbelTaxSep.getWbelTaxSepAsBuffer(buffer, position);
        position += WbelTaxSep.Len.WBEL_TAX_SEP;
        wbelDtRiserveSomP.getWbelDtRiserveSomPAsBuffer(buffer, position);
        position += WbelDtRiserveSomP.Len.WBEL_DT_RISERVE_SOM_P;
        wbelDtVlt.getWbelDtVltAsBuffer(buffer, position);
        position += WbelDtVlt.Len.WBEL_DT_VLT;
        MarshalByte.writeString(buffer, position, wbelTpStatLiqBnficr, Len.WBEL_TP_STAT_LIQ_BNFICR);
        position += Len.WBEL_TP_STAT_LIQ_BNFICR;
        MarshalByte.writeLongAsPacked(buffer, position, wbelDsRiga, Len.Int.WBEL_DS_RIGA, 0);
        position += Len.WBEL_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wbelDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wbelDsVer, Len.Int.WBEL_DS_VER, 0);
        position += Len.WBEL_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wbelDsTsIniCptz, Len.Int.WBEL_DS_TS_INI_CPTZ, 0);
        position += Len.WBEL_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wbelDsTsEndCptz, Len.Int.WBEL_DS_TS_END_CPTZ, 0);
        position += Len.WBEL_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wbelDsUtente, Len.WBEL_DS_UTENTE);
        position += Len.WBEL_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wbelDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wbelRichCalcCnbtInp);
        position += Types.CHAR_SIZE;
        wbelImpIntrRitPag.getWbelImpIntrRitPagAsBuffer(buffer, position);
        position += WbelImpIntrRitPag.Len.WBEL_IMP_INTR_RIT_PAG;
        wbelDtUltDocto.getWbelDtUltDoctoAsBuffer(buffer, position);
        position += WbelDtUltDocto.Len.WBEL_DT_ULT_DOCTO;
        wbelDtDormienza.getWbelDtDormienzaAsBuffer(buffer, position);
        position += WbelDtDormienza.Len.WBEL_DT_DORMIENZA;
        wbelImpstBolloTotV.getWbelImpstBolloTotVAsBuffer(buffer, position);
        position += WbelImpstBolloTotV.Len.WBEL_IMPST_BOLLO_TOT_V;
        wbelImpstVis1382011.getWbelImpstVis1382011AsBuffer(buffer, position);
        position += WbelImpstVis1382011.Len.WBEL_IMPST_VIS1382011;
        wbelImpstSost1382011.getWbelImpstSost1382011AsBuffer(buffer, position);
        position += WbelImpstSost1382011.Len.WBEL_IMPST_SOST1382011;
        wbelImpstVis662014.getWbelImpstVis662014AsBuffer(buffer, position);
        position += WbelImpstVis662014.Len.WBEL_IMPST_VIS662014;
        wbelImpstSost662014.getWbelImpstSost662014AsBuffer(buffer, position);
        return buffer;
    }

    public void initDatiSpaces() {
        wbelIdBnficrLiq = Types.INVALID_INT_VAL;
        wbelIdLiq = Types.INVALID_INT_VAL;
        wbelIdMoviCrz = Types.INVALID_INT_VAL;
        wbelIdMoviChiu.initWbelIdMoviChiuSpaces();
        wbelCodCompAnia = Types.INVALID_INT_VAL;
        wbelDtIniEff = Types.INVALID_INT_VAL;
        wbelDtEndEff = Types.INVALID_INT_VAL;
        wbelIdRappAna.initWbelIdRappAnaSpaces();
        wbelCodBnficr = "";
        wbelDescBnficr = "";
        wbelPcLiq.initWbelPcLiqSpaces();
        wbelEsrcnAttvtImprs = Types.SPACE_CHAR;
        wbelTpIndBnficr = "";
        wbelFlEse = Types.SPACE_CHAR;
        wbelFlIrrev = Types.SPACE_CHAR;
        wbelImpLrdLiqto.initWbelImpLrdLiqtoSpaces();
        wbelImpstIpt.initWbelImpstIptSpaces();
        wbelImpNetLiqto.initWbelImpNetLiqtoSpaces();
        wbelRitAccIpt.initWbelRitAccIptSpaces();
        wbelRitVisIpt.initWbelRitVisIptSpaces();
        wbelRitTfrIpt.initWbelRitTfrIptSpaces();
        wbelImpstIrpefIpt.initWbelImpstIrpefIptSpaces();
        wbelImpstSostIpt.initWbelImpstSostIptSpaces();
        wbelImpstPrvrIpt.initWbelImpstPrvrIptSpaces();
        wbelImpst252Ipt.initWbelImpst252IptSpaces();
        wbelIdAssto.initWbelIdAsstoSpaces();
        wbelTaxSep.initWbelTaxSepSpaces();
        wbelDtRiserveSomP.initWbelDtRiserveSomPSpaces();
        wbelDtVlt.initWbelDtVltSpaces();
        wbelTpStatLiqBnficr = "";
        wbelDsRiga = Types.INVALID_LONG_VAL;
        wbelDsOperSql = Types.SPACE_CHAR;
        wbelDsVer = Types.INVALID_INT_VAL;
        wbelDsTsIniCptz = Types.INVALID_LONG_VAL;
        wbelDsTsEndCptz = Types.INVALID_LONG_VAL;
        wbelDsUtente = "";
        wbelDsStatoElab = Types.SPACE_CHAR;
        wbelRichCalcCnbtInp = Types.SPACE_CHAR;
        wbelImpIntrRitPag.initWbelImpIntrRitPagSpaces();
        wbelDtUltDocto.initWbelDtUltDoctoSpaces();
        wbelDtDormienza.initWbelDtDormienzaSpaces();
        wbelImpstBolloTotV.initWbelImpstBolloTotVSpaces();
        wbelImpstVis1382011.initWbelImpstVis1382011Spaces();
        wbelImpstSost1382011.initWbelImpstSost1382011Spaces();
        wbelImpstVis662014.initWbelImpstVis662014Spaces();
        wbelImpstSost662014.initWbelImpstSost662014Spaces();
    }

    public void setWbelIdBnficrLiq(int wbelIdBnficrLiq) {
        this.wbelIdBnficrLiq = wbelIdBnficrLiq;
    }

    public int getWbelIdBnficrLiq() {
        return this.wbelIdBnficrLiq;
    }

    public void setWbelIdLiq(int wbelIdLiq) {
        this.wbelIdLiq = wbelIdLiq;
    }

    public int getWbelIdLiq() {
        return this.wbelIdLiq;
    }

    public void setWbelIdMoviCrz(int wbelIdMoviCrz) {
        this.wbelIdMoviCrz = wbelIdMoviCrz;
    }

    public int getWbelIdMoviCrz() {
        return this.wbelIdMoviCrz;
    }

    public void setWbelCodCompAnia(int wbelCodCompAnia) {
        this.wbelCodCompAnia = wbelCodCompAnia;
    }

    public int getWbelCodCompAnia() {
        return this.wbelCodCompAnia;
    }

    public void setWbelDtIniEff(int wbelDtIniEff) {
        this.wbelDtIniEff = wbelDtIniEff;
    }

    public int getWbelDtIniEff() {
        return this.wbelDtIniEff;
    }

    public void setWbelDtEndEff(int wbelDtEndEff) {
        this.wbelDtEndEff = wbelDtEndEff;
    }

    public int getWbelDtEndEff() {
        return this.wbelDtEndEff;
    }

    public void setWbelCodBnficr(String wbelCodBnficr) {
        this.wbelCodBnficr = Functions.subString(wbelCodBnficr, Len.WBEL_COD_BNFICR);
    }

    public String getWbelCodBnficr() {
        return this.wbelCodBnficr;
    }

    public void setWbelDescBnficr(String wbelDescBnficr) {
        this.wbelDescBnficr = Functions.subString(wbelDescBnficr, Len.WBEL_DESC_BNFICR);
    }

    public String getWbelDescBnficr() {
        return this.wbelDescBnficr;
    }

    public void setWbelEsrcnAttvtImprs(char wbelEsrcnAttvtImprs) {
        this.wbelEsrcnAttvtImprs = wbelEsrcnAttvtImprs;
    }

    public char getWbelEsrcnAttvtImprs() {
        return this.wbelEsrcnAttvtImprs;
    }

    public void setWbelTpIndBnficr(String wbelTpIndBnficr) {
        this.wbelTpIndBnficr = Functions.subString(wbelTpIndBnficr, Len.WBEL_TP_IND_BNFICR);
    }

    public String getWbelTpIndBnficr() {
        return this.wbelTpIndBnficr;
    }

    public void setWbelFlEse(char wbelFlEse) {
        this.wbelFlEse = wbelFlEse;
    }

    public char getWbelFlEse() {
        return this.wbelFlEse;
    }

    public void setWbelFlIrrev(char wbelFlIrrev) {
        this.wbelFlIrrev = wbelFlIrrev;
    }

    public char getWbelFlIrrev() {
        return this.wbelFlIrrev;
    }

    public void setWbelTpStatLiqBnficr(String wbelTpStatLiqBnficr) {
        this.wbelTpStatLiqBnficr = Functions.subString(wbelTpStatLiqBnficr, Len.WBEL_TP_STAT_LIQ_BNFICR);
    }

    public String getWbelTpStatLiqBnficr() {
        return this.wbelTpStatLiqBnficr;
    }

    public void setWbelDsRiga(long wbelDsRiga) {
        this.wbelDsRiga = wbelDsRiga;
    }

    public long getWbelDsRiga() {
        return this.wbelDsRiga;
    }

    public void setWbelDsOperSql(char wbelDsOperSql) {
        this.wbelDsOperSql = wbelDsOperSql;
    }

    public char getWbelDsOperSql() {
        return this.wbelDsOperSql;
    }

    public void setWbelDsVer(int wbelDsVer) {
        this.wbelDsVer = wbelDsVer;
    }

    public int getWbelDsVer() {
        return this.wbelDsVer;
    }

    public void setWbelDsTsIniCptz(long wbelDsTsIniCptz) {
        this.wbelDsTsIniCptz = wbelDsTsIniCptz;
    }

    public long getWbelDsTsIniCptz() {
        return this.wbelDsTsIniCptz;
    }

    public void setWbelDsTsEndCptz(long wbelDsTsEndCptz) {
        this.wbelDsTsEndCptz = wbelDsTsEndCptz;
    }

    public long getWbelDsTsEndCptz() {
        return this.wbelDsTsEndCptz;
    }

    public void setWbelDsUtente(String wbelDsUtente) {
        this.wbelDsUtente = Functions.subString(wbelDsUtente, Len.WBEL_DS_UTENTE);
    }

    public String getWbelDsUtente() {
        return this.wbelDsUtente;
    }

    public void setWbelDsStatoElab(char wbelDsStatoElab) {
        this.wbelDsStatoElab = wbelDsStatoElab;
    }

    public char getWbelDsStatoElab() {
        return this.wbelDsStatoElab;
    }

    public void setWbelRichCalcCnbtInp(char wbelRichCalcCnbtInp) {
        this.wbelRichCalcCnbtInp = wbelRichCalcCnbtInp;
    }

    public char getWbelRichCalcCnbtInp() {
        return this.wbelRichCalcCnbtInp;
    }

    public WbelDtDormienza getWbelDtDormienza() {
        return wbelDtDormienza;
    }

    public WbelDtRiserveSomP getWbelDtRiserveSomP() {
        return wbelDtRiserveSomP;
    }

    public WbelDtUltDocto getWbelDtUltDocto() {
        return wbelDtUltDocto;
    }

    public WbelDtVlt getWbelDtVlt() {
        return wbelDtVlt;
    }

    public WbelIdAssto getWbelIdAssto() {
        return wbelIdAssto;
    }

    public WbelIdMoviChiu getWbelIdMoviChiu() {
        return wbelIdMoviChiu;
    }

    public WbelIdRappAna getWbelIdRappAna() {
        return wbelIdRappAna;
    }

    public WbelImpIntrRitPag getWbelImpIntrRitPag() {
        return wbelImpIntrRitPag;
    }

    public WbelImpLrdLiqto getWbelImpLrdLiqto() {
        return wbelImpLrdLiqto;
    }

    public WbelImpNetLiqto getWbelImpNetLiqto() {
        return wbelImpNetLiqto;
    }

    public WbelImpst252Ipt getWbelImpst252Ipt() {
        return wbelImpst252Ipt;
    }

    public WbelImpstBolloTotV getWbelImpstBolloTotV() {
        return wbelImpstBolloTotV;
    }

    public WbelImpstIpt getWbelImpstIpt() {
        return wbelImpstIpt;
    }

    public WbelImpstIrpefIpt getWbelImpstIrpefIpt() {
        return wbelImpstIrpefIpt;
    }

    public WbelImpstPrvrIpt getWbelImpstPrvrIpt() {
        return wbelImpstPrvrIpt;
    }

    public WbelImpstSost1382011 getWbelImpstSost1382011() {
        return wbelImpstSost1382011;
    }

    public WbelImpstSost662014 getWbelImpstSost662014() {
        return wbelImpstSost662014;
    }

    public WbelImpstSostIpt getWbelImpstSostIpt() {
        return wbelImpstSostIpt;
    }

    public WbelImpstVis1382011 getWbelImpstVis1382011() {
        return wbelImpstVis1382011;
    }

    public WbelImpstVis662014 getWbelImpstVis662014() {
        return wbelImpstVis662014;
    }

    public WbelPcLiq getWbelPcLiq() {
        return wbelPcLiq;
    }

    public WbelRitAccIpt getWbelRitAccIpt() {
        return wbelRitAccIpt;
    }

    public WbelRitTfrIpt getWbelRitTfrIpt() {
        return wbelRitTfrIpt;
    }

    public WbelRitVisIpt getWbelRitVisIpt() {
        return wbelRitVisIpt;
    }

    public WbelTaxSep getWbelTaxSep() {
        return wbelTaxSep;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_ID_BNFICR_LIQ = 5;
        public static final int WBEL_ID_LIQ = 5;
        public static final int WBEL_ID_MOVI_CRZ = 5;
        public static final int WBEL_COD_COMP_ANIA = 3;
        public static final int WBEL_DT_INI_EFF = 5;
        public static final int WBEL_DT_END_EFF = 5;
        public static final int WBEL_COD_BNFICR = 20;
        public static final int WBEL_DESC_BNFICR = 100;
        public static final int WBEL_ESRCN_ATTVT_IMPRS = 1;
        public static final int WBEL_TP_IND_BNFICR = 2;
        public static final int WBEL_FL_ESE = 1;
        public static final int WBEL_FL_IRREV = 1;
        public static final int WBEL_TP_STAT_LIQ_BNFICR = 2;
        public static final int WBEL_DS_RIGA = 6;
        public static final int WBEL_DS_OPER_SQL = 1;
        public static final int WBEL_DS_VER = 5;
        public static final int WBEL_DS_TS_INI_CPTZ = 10;
        public static final int WBEL_DS_TS_END_CPTZ = 10;
        public static final int WBEL_DS_UTENTE = 20;
        public static final int WBEL_DS_STATO_ELAB = 1;
        public static final int WBEL_RICH_CALC_CNBT_INP = 1;
        public static final int DATI = WBEL_ID_BNFICR_LIQ + WBEL_ID_LIQ + WBEL_ID_MOVI_CRZ + WbelIdMoviChiu.Len.WBEL_ID_MOVI_CHIU + WBEL_COD_COMP_ANIA + WBEL_DT_INI_EFF + WBEL_DT_END_EFF + WbelIdRappAna.Len.WBEL_ID_RAPP_ANA + WBEL_COD_BNFICR + WBEL_DESC_BNFICR + WbelPcLiq.Len.WBEL_PC_LIQ + WBEL_ESRCN_ATTVT_IMPRS + WBEL_TP_IND_BNFICR + WBEL_FL_ESE + WBEL_FL_IRREV + WbelImpLrdLiqto.Len.WBEL_IMP_LRD_LIQTO + WbelImpstIpt.Len.WBEL_IMPST_IPT + WbelImpNetLiqto.Len.WBEL_IMP_NET_LIQTO + WbelRitAccIpt.Len.WBEL_RIT_ACC_IPT + WbelRitVisIpt.Len.WBEL_RIT_VIS_IPT + WbelRitTfrIpt.Len.WBEL_RIT_TFR_IPT + WbelImpstIrpefIpt.Len.WBEL_IMPST_IRPEF_IPT + WbelImpstSostIpt.Len.WBEL_IMPST_SOST_IPT + WbelImpstPrvrIpt.Len.WBEL_IMPST_PRVR_IPT + WbelImpst252Ipt.Len.WBEL_IMPST252_IPT + WbelIdAssto.Len.WBEL_ID_ASSTO + WbelTaxSep.Len.WBEL_TAX_SEP + WbelDtRiserveSomP.Len.WBEL_DT_RISERVE_SOM_P + WbelDtVlt.Len.WBEL_DT_VLT + WBEL_TP_STAT_LIQ_BNFICR + WBEL_DS_RIGA + WBEL_DS_OPER_SQL + WBEL_DS_VER + WBEL_DS_TS_INI_CPTZ + WBEL_DS_TS_END_CPTZ + WBEL_DS_UTENTE + WBEL_DS_STATO_ELAB + WBEL_RICH_CALC_CNBT_INP + WbelImpIntrRitPag.Len.WBEL_IMP_INTR_RIT_PAG + WbelDtUltDocto.Len.WBEL_DT_ULT_DOCTO + WbelDtDormienza.Len.WBEL_DT_DORMIENZA + WbelImpstBolloTotV.Len.WBEL_IMPST_BOLLO_TOT_V + WbelImpstVis1382011.Len.WBEL_IMPST_VIS1382011 + WbelImpstSost1382011.Len.WBEL_IMPST_SOST1382011 + WbelImpstVis662014.Len.WBEL_IMPST_VIS662014 + WbelImpstSost662014.Len.WBEL_IMPST_SOST662014;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_ID_BNFICR_LIQ = 9;
            public static final int WBEL_ID_LIQ = 9;
            public static final int WBEL_ID_MOVI_CRZ = 9;
            public static final int WBEL_COD_COMP_ANIA = 5;
            public static final int WBEL_DT_INI_EFF = 8;
            public static final int WBEL_DT_END_EFF = 8;
            public static final int WBEL_DS_RIGA = 10;
            public static final int WBEL_DS_VER = 9;
            public static final int WBEL_DS_TS_INI_CPTZ = 18;
            public static final int WBEL_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
