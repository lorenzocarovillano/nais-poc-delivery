package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.DtrAcqExp;
import it.accenture.jnais.ws.redefines.DtrCarAcq;
import it.accenture.jnais.ws.redefines.DtrCarGest;
import it.accenture.jnais.ws.redefines.DtrCarIas;
import it.accenture.jnais.ws.redefines.DtrCarInc;
import it.accenture.jnais.ws.redefines.DtrCnbtAntirac;
import it.accenture.jnais.ws.redefines.DtrCommisInter;
import it.accenture.jnais.ws.redefines.DtrDir;
import it.accenture.jnais.ws.redefines.DtrDtEndCop;
import it.accenture.jnais.ws.redefines.DtrDtIniCop;
import it.accenture.jnais.ws.redefines.DtrFrqMovi;
import it.accenture.jnais.ws.redefines.DtrIdMoviChiu;
import it.accenture.jnais.ws.redefines.DtrImpAder;
import it.accenture.jnais.ws.redefines.DtrImpAz;
import it.accenture.jnais.ws.redefines.DtrImpTfr;
import it.accenture.jnais.ws.redefines.DtrImpTfrStrc;
import it.accenture.jnais.ws.redefines.DtrImpTrasfe;
import it.accenture.jnais.ws.redefines.DtrImpVolo;
import it.accenture.jnais.ws.redefines.DtrIntrFraz;
import it.accenture.jnais.ws.redefines.DtrIntrMora;
import it.accenture.jnais.ws.redefines.DtrIntrRetdt;
import it.accenture.jnais.ws.redefines.DtrIntrRiat;
import it.accenture.jnais.ws.redefines.DtrManfeeAntic;
import it.accenture.jnais.ws.redefines.DtrManfeeRicor;
import it.accenture.jnais.ws.redefines.DtrPreNet;
import it.accenture.jnais.ws.redefines.DtrPrePpIas;
import it.accenture.jnais.ws.redefines.DtrPreSoloRsh;
import it.accenture.jnais.ws.redefines.DtrPreTot;
import it.accenture.jnais.ws.redefines.DtrProvAcq1aa;
import it.accenture.jnais.ws.redefines.DtrProvAcq2aa;
import it.accenture.jnais.ws.redefines.DtrProvDaRec;
import it.accenture.jnais.ws.redefines.DtrProvInc;
import it.accenture.jnais.ws.redefines.DtrProvRicor;
import it.accenture.jnais.ws.redefines.DtrRemunAss;
import it.accenture.jnais.ws.redefines.DtrSoprAlt;
import it.accenture.jnais.ws.redefines.DtrSoprProf;
import it.accenture.jnais.ws.redefines.DtrSoprSan;
import it.accenture.jnais.ws.redefines.DtrSoprSpo;
import it.accenture.jnais.ws.redefines.DtrSoprTec;
import it.accenture.jnais.ws.redefines.DtrSpeAge;
import it.accenture.jnais.ws.redefines.DtrSpeMed;
import it.accenture.jnais.ws.redefines.DtrTax;
import it.accenture.jnais.ws.redefines.DtrTotIntrPrest;

/**Original name: DETT-TIT-DI-RAT<br>
 * Variable: DETT-TIT-DI-RAT from copybook IDBVDTR1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DettTitDiRat {

    //==== PROPERTIES ====
    //Original name: DTR-ID-DETT-TIT-DI-RAT
    private int dtrIdDettTitDiRat = DefaultValues.INT_VAL;
    //Original name: DTR-ID-TIT-RAT
    private int dtrIdTitRat = DefaultValues.INT_VAL;
    //Original name: DTR-ID-OGG
    private int dtrIdOgg = DefaultValues.INT_VAL;
    //Original name: DTR-TP-OGG
    private String dtrTpOgg = DefaultValues.stringVal(Len.DTR_TP_OGG);
    //Original name: DTR-ID-MOVI-CRZ
    private int dtrIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: DTR-ID-MOVI-CHIU
    private DtrIdMoviChiu dtrIdMoviChiu = new DtrIdMoviChiu();
    //Original name: DTR-DT-INI-EFF
    private int dtrDtIniEff = DefaultValues.INT_VAL;
    //Original name: DTR-DT-END-EFF
    private int dtrDtEndEff = DefaultValues.INT_VAL;
    //Original name: DTR-COD-COMP-ANIA
    private int dtrCodCompAnia = DefaultValues.INT_VAL;
    //Original name: DTR-DT-INI-COP
    private DtrDtIniCop dtrDtIniCop = new DtrDtIniCop();
    //Original name: DTR-DT-END-COP
    private DtrDtEndCop dtrDtEndCop = new DtrDtEndCop();
    //Original name: DTR-PRE-NET
    private DtrPreNet dtrPreNet = new DtrPreNet();
    //Original name: DTR-INTR-FRAZ
    private DtrIntrFraz dtrIntrFraz = new DtrIntrFraz();
    //Original name: DTR-INTR-MORA
    private DtrIntrMora dtrIntrMora = new DtrIntrMora();
    //Original name: DTR-INTR-RETDT
    private DtrIntrRetdt dtrIntrRetdt = new DtrIntrRetdt();
    //Original name: DTR-INTR-RIAT
    private DtrIntrRiat dtrIntrRiat = new DtrIntrRiat();
    //Original name: DTR-DIR
    private DtrDir dtrDir = new DtrDir();
    //Original name: DTR-SPE-MED
    private DtrSpeMed dtrSpeMed = new DtrSpeMed();
    //Original name: DTR-SPE-AGE
    private DtrSpeAge dtrSpeAge = new DtrSpeAge();
    //Original name: DTR-TAX
    private DtrTax dtrTax = new DtrTax();
    //Original name: DTR-SOPR-SAN
    private DtrSoprSan dtrSoprSan = new DtrSoprSan();
    //Original name: DTR-SOPR-SPO
    private DtrSoprSpo dtrSoprSpo = new DtrSoprSpo();
    //Original name: DTR-SOPR-TEC
    private DtrSoprTec dtrSoprTec = new DtrSoprTec();
    //Original name: DTR-SOPR-PROF
    private DtrSoprProf dtrSoprProf = new DtrSoprProf();
    //Original name: DTR-SOPR-ALT
    private DtrSoprAlt dtrSoprAlt = new DtrSoprAlt();
    //Original name: DTR-PRE-TOT
    private DtrPreTot dtrPreTot = new DtrPreTot();
    //Original name: DTR-PRE-PP-IAS
    private DtrPrePpIas dtrPrePpIas = new DtrPrePpIas();
    //Original name: DTR-PRE-SOLO-RSH
    private DtrPreSoloRsh dtrPreSoloRsh = new DtrPreSoloRsh();
    //Original name: DTR-CAR-IAS
    private DtrCarIas dtrCarIas = new DtrCarIas();
    //Original name: DTR-PROV-ACQ-1AA
    private DtrProvAcq1aa dtrProvAcq1aa = new DtrProvAcq1aa();
    //Original name: DTR-PROV-ACQ-2AA
    private DtrProvAcq2aa dtrProvAcq2aa = new DtrProvAcq2aa();
    //Original name: DTR-PROV-RICOR
    private DtrProvRicor dtrProvRicor = new DtrProvRicor();
    //Original name: DTR-PROV-INC
    private DtrProvInc dtrProvInc = new DtrProvInc();
    //Original name: DTR-PROV-DA-REC
    private DtrProvDaRec dtrProvDaRec = new DtrProvDaRec();
    //Original name: DTR-COD-DVS
    private String dtrCodDvs = DefaultValues.stringVal(Len.DTR_COD_DVS);
    //Original name: DTR-FRQ-MOVI
    private DtrFrqMovi dtrFrqMovi = new DtrFrqMovi();
    //Original name: DTR-TP-RGM-FISC
    private String dtrTpRgmFisc = DefaultValues.stringVal(Len.DTR_TP_RGM_FISC);
    //Original name: DTR-COD-TARI
    private String dtrCodTari = DefaultValues.stringVal(Len.DTR_COD_TARI);
    //Original name: DTR-TP-STAT-TIT
    private String dtrTpStatTit = DefaultValues.stringVal(Len.DTR_TP_STAT_TIT);
    //Original name: DTR-IMP-AZ
    private DtrImpAz dtrImpAz = new DtrImpAz();
    //Original name: DTR-IMP-ADER
    private DtrImpAder dtrImpAder = new DtrImpAder();
    //Original name: DTR-IMP-TFR
    private DtrImpTfr dtrImpTfr = new DtrImpTfr();
    //Original name: DTR-IMP-VOLO
    private DtrImpVolo dtrImpVolo = new DtrImpVolo();
    //Original name: DTR-FL-VLDT-TIT
    private char dtrFlVldtTit = DefaultValues.CHAR_VAL;
    //Original name: DTR-CAR-ACQ
    private DtrCarAcq dtrCarAcq = new DtrCarAcq();
    //Original name: DTR-CAR-GEST
    private DtrCarGest dtrCarGest = new DtrCarGest();
    //Original name: DTR-CAR-INC
    private DtrCarInc dtrCarInc = new DtrCarInc();
    //Original name: DTR-MANFEE-ANTIC
    private DtrManfeeAntic dtrManfeeAntic = new DtrManfeeAntic();
    //Original name: DTR-MANFEE-RICOR
    private DtrManfeeRicor dtrManfeeRicor = new DtrManfeeRicor();
    //Original name: DTR-MANFEE-REC
    private String dtrManfeeRec = DefaultValues.stringVal(Len.DTR_MANFEE_REC);
    //Original name: DTR-TOT-INTR-PREST
    private DtrTotIntrPrest dtrTotIntrPrest = new DtrTotIntrPrest();
    //Original name: DTR-DS-RIGA
    private long dtrDsRiga = DefaultValues.LONG_VAL;
    //Original name: DTR-DS-OPER-SQL
    private char dtrDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: DTR-DS-VER
    private int dtrDsVer = DefaultValues.INT_VAL;
    //Original name: DTR-DS-TS-INI-CPTZ
    private long dtrDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: DTR-DS-TS-END-CPTZ
    private long dtrDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: DTR-DS-UTENTE
    private String dtrDsUtente = DefaultValues.stringVal(Len.DTR_DS_UTENTE);
    //Original name: DTR-DS-STATO-ELAB
    private char dtrDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: DTR-IMP-TRASFE
    private DtrImpTrasfe dtrImpTrasfe = new DtrImpTrasfe();
    //Original name: DTR-IMP-TFR-STRC
    private DtrImpTfrStrc dtrImpTfrStrc = new DtrImpTfrStrc();
    //Original name: DTR-ACQ-EXP
    private DtrAcqExp dtrAcqExp = new DtrAcqExp();
    //Original name: DTR-REMUN-ASS
    private DtrRemunAss dtrRemunAss = new DtrRemunAss();
    //Original name: DTR-COMMIS-INTER
    private DtrCommisInter dtrCommisInter = new DtrCommisInter();
    //Original name: DTR-CNBT-ANTIRAC
    private DtrCnbtAntirac dtrCnbtAntirac = new DtrCnbtAntirac();

    //==== METHODS ====
    public void setDettTitDiRatFormatted(String data) {
        byte[] buffer = new byte[Len.DETT_TIT_DI_RAT];
        MarshalByte.writeString(buffer, 1, data, Len.DETT_TIT_DI_RAT);
        setDettTitDiRatBytes(buffer, 1);
    }

    public String getDettTitDiRatFormatted() {
        return MarshalByteExt.bufferToStr(getDettTitDiRatBytes());
    }

    public byte[] getDettTitDiRatBytes() {
        byte[] buffer = new byte[Len.DETT_TIT_DI_RAT];
        return getDettTitDiRatBytes(buffer, 1);
    }

    public void setDettTitDiRatBytes(byte[] buffer, int offset) {
        int position = offset;
        dtrIdDettTitDiRat = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTR_ID_DETT_TIT_DI_RAT, 0);
        position += Len.DTR_ID_DETT_TIT_DI_RAT;
        dtrIdTitRat = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTR_ID_TIT_RAT, 0);
        position += Len.DTR_ID_TIT_RAT;
        dtrIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTR_ID_OGG, 0);
        position += Len.DTR_ID_OGG;
        dtrTpOgg = MarshalByte.readString(buffer, position, Len.DTR_TP_OGG);
        position += Len.DTR_TP_OGG;
        dtrIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTR_ID_MOVI_CRZ, 0);
        position += Len.DTR_ID_MOVI_CRZ;
        dtrIdMoviChiu.setDtrIdMoviChiuFromBuffer(buffer, position);
        position += DtrIdMoviChiu.Len.DTR_ID_MOVI_CHIU;
        dtrDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTR_DT_INI_EFF, 0);
        position += Len.DTR_DT_INI_EFF;
        dtrDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTR_DT_END_EFF, 0);
        position += Len.DTR_DT_END_EFF;
        dtrCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTR_COD_COMP_ANIA, 0);
        position += Len.DTR_COD_COMP_ANIA;
        dtrDtIniCop.setDtrDtIniCopFromBuffer(buffer, position);
        position += DtrDtIniCop.Len.DTR_DT_INI_COP;
        dtrDtEndCop.setDtrDtEndCopFromBuffer(buffer, position);
        position += DtrDtEndCop.Len.DTR_DT_END_COP;
        dtrPreNet.setDtrPreNetFromBuffer(buffer, position);
        position += DtrPreNet.Len.DTR_PRE_NET;
        dtrIntrFraz.setDtrIntrFrazFromBuffer(buffer, position);
        position += DtrIntrFraz.Len.DTR_INTR_FRAZ;
        dtrIntrMora.setDtrIntrMoraFromBuffer(buffer, position);
        position += DtrIntrMora.Len.DTR_INTR_MORA;
        dtrIntrRetdt.setDtrIntrRetdtFromBuffer(buffer, position);
        position += DtrIntrRetdt.Len.DTR_INTR_RETDT;
        dtrIntrRiat.setDtrIntrRiatFromBuffer(buffer, position);
        position += DtrIntrRiat.Len.DTR_INTR_RIAT;
        dtrDir.setDtrDirFromBuffer(buffer, position);
        position += DtrDir.Len.DTR_DIR;
        dtrSpeMed.setDtrSpeMedFromBuffer(buffer, position);
        position += DtrSpeMed.Len.DTR_SPE_MED;
        dtrSpeAge.setDtrSpeAgeFromBuffer(buffer, position);
        position += DtrSpeAge.Len.DTR_SPE_AGE;
        dtrTax.setDtrTaxFromBuffer(buffer, position);
        position += DtrTax.Len.DTR_TAX;
        dtrSoprSan.setDtrSoprSanFromBuffer(buffer, position);
        position += DtrSoprSan.Len.DTR_SOPR_SAN;
        dtrSoprSpo.setDtrSoprSpoFromBuffer(buffer, position);
        position += DtrSoprSpo.Len.DTR_SOPR_SPO;
        dtrSoprTec.setDtrSoprTecFromBuffer(buffer, position);
        position += DtrSoprTec.Len.DTR_SOPR_TEC;
        dtrSoprProf.setDtrSoprProfFromBuffer(buffer, position);
        position += DtrSoprProf.Len.DTR_SOPR_PROF;
        dtrSoprAlt.setDtrSoprAltFromBuffer(buffer, position);
        position += DtrSoprAlt.Len.DTR_SOPR_ALT;
        dtrPreTot.setDtrPreTotFromBuffer(buffer, position);
        position += DtrPreTot.Len.DTR_PRE_TOT;
        dtrPrePpIas.setDtrPrePpIasFromBuffer(buffer, position);
        position += DtrPrePpIas.Len.DTR_PRE_PP_IAS;
        dtrPreSoloRsh.setDtrPreSoloRshFromBuffer(buffer, position);
        position += DtrPreSoloRsh.Len.DTR_PRE_SOLO_RSH;
        dtrCarIas.setDtrCarIasFromBuffer(buffer, position);
        position += DtrCarIas.Len.DTR_CAR_IAS;
        dtrProvAcq1aa.setDtrProvAcq1aaFromBuffer(buffer, position);
        position += DtrProvAcq1aa.Len.DTR_PROV_ACQ1AA;
        dtrProvAcq2aa.setDtrProvAcq2aaFromBuffer(buffer, position);
        position += DtrProvAcq2aa.Len.DTR_PROV_ACQ2AA;
        dtrProvRicor.setDtrProvRicorFromBuffer(buffer, position);
        position += DtrProvRicor.Len.DTR_PROV_RICOR;
        dtrProvInc.setDtrProvIncFromBuffer(buffer, position);
        position += DtrProvInc.Len.DTR_PROV_INC;
        dtrProvDaRec.setDtrProvDaRecFromBuffer(buffer, position);
        position += DtrProvDaRec.Len.DTR_PROV_DA_REC;
        dtrCodDvs = MarshalByte.readString(buffer, position, Len.DTR_COD_DVS);
        position += Len.DTR_COD_DVS;
        dtrFrqMovi.setDtrFrqMoviFromBuffer(buffer, position);
        position += DtrFrqMovi.Len.DTR_FRQ_MOVI;
        dtrTpRgmFisc = MarshalByte.readString(buffer, position, Len.DTR_TP_RGM_FISC);
        position += Len.DTR_TP_RGM_FISC;
        dtrCodTari = MarshalByte.readString(buffer, position, Len.DTR_COD_TARI);
        position += Len.DTR_COD_TARI;
        dtrTpStatTit = MarshalByte.readString(buffer, position, Len.DTR_TP_STAT_TIT);
        position += Len.DTR_TP_STAT_TIT;
        dtrImpAz.setDtrImpAzFromBuffer(buffer, position);
        position += DtrImpAz.Len.DTR_IMP_AZ;
        dtrImpAder.setDtrImpAderFromBuffer(buffer, position);
        position += DtrImpAder.Len.DTR_IMP_ADER;
        dtrImpTfr.setDtrImpTfrFromBuffer(buffer, position);
        position += DtrImpTfr.Len.DTR_IMP_TFR;
        dtrImpVolo.setDtrImpVoloFromBuffer(buffer, position);
        position += DtrImpVolo.Len.DTR_IMP_VOLO;
        dtrFlVldtTit = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dtrCarAcq.setDtrCarAcqFromBuffer(buffer, position);
        position += DtrCarAcq.Len.DTR_CAR_ACQ;
        dtrCarGest.setDtrCarGestFromBuffer(buffer, position);
        position += DtrCarGest.Len.DTR_CAR_GEST;
        dtrCarInc.setDtrCarIncFromBuffer(buffer, position);
        position += DtrCarInc.Len.DTR_CAR_INC;
        dtrManfeeAntic.setDtrManfeeAnticFromBuffer(buffer, position);
        position += DtrManfeeAntic.Len.DTR_MANFEE_ANTIC;
        dtrManfeeRicor.setDtrManfeeRicorFromBuffer(buffer, position);
        position += DtrManfeeRicor.Len.DTR_MANFEE_RICOR;
        dtrManfeeRec = MarshalByte.readString(buffer, position, Len.DTR_MANFEE_REC);
        position += Len.DTR_MANFEE_REC;
        dtrTotIntrPrest.setDtrTotIntrPrestFromBuffer(buffer, position);
        position += DtrTotIntrPrest.Len.DTR_TOT_INTR_PREST;
        dtrDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DTR_DS_RIGA, 0);
        position += Len.DTR_DS_RIGA;
        dtrDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dtrDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTR_DS_VER, 0);
        position += Len.DTR_DS_VER;
        dtrDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DTR_DS_TS_INI_CPTZ, 0);
        position += Len.DTR_DS_TS_INI_CPTZ;
        dtrDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DTR_DS_TS_END_CPTZ, 0);
        position += Len.DTR_DS_TS_END_CPTZ;
        dtrDsUtente = MarshalByte.readString(buffer, position, Len.DTR_DS_UTENTE);
        position += Len.DTR_DS_UTENTE;
        dtrDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dtrImpTrasfe.setDtrImpTrasfeFromBuffer(buffer, position);
        position += DtrImpTrasfe.Len.DTR_IMP_TRASFE;
        dtrImpTfrStrc.setDtrImpTfrStrcFromBuffer(buffer, position);
        position += DtrImpTfrStrc.Len.DTR_IMP_TFR_STRC;
        dtrAcqExp.setDtrAcqExpFromBuffer(buffer, position);
        position += DtrAcqExp.Len.DTR_ACQ_EXP;
        dtrRemunAss.setDtrRemunAssFromBuffer(buffer, position);
        position += DtrRemunAss.Len.DTR_REMUN_ASS;
        dtrCommisInter.setDtrCommisInterFromBuffer(buffer, position);
        position += DtrCommisInter.Len.DTR_COMMIS_INTER;
        dtrCnbtAntirac.setDtrCnbtAntiracFromBuffer(buffer, position);
    }

    public byte[] getDettTitDiRatBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, dtrIdDettTitDiRat, Len.Int.DTR_ID_DETT_TIT_DI_RAT, 0);
        position += Len.DTR_ID_DETT_TIT_DI_RAT;
        MarshalByte.writeIntAsPacked(buffer, position, dtrIdTitRat, Len.Int.DTR_ID_TIT_RAT, 0);
        position += Len.DTR_ID_TIT_RAT;
        MarshalByte.writeIntAsPacked(buffer, position, dtrIdOgg, Len.Int.DTR_ID_OGG, 0);
        position += Len.DTR_ID_OGG;
        MarshalByte.writeString(buffer, position, dtrTpOgg, Len.DTR_TP_OGG);
        position += Len.DTR_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, dtrIdMoviCrz, Len.Int.DTR_ID_MOVI_CRZ, 0);
        position += Len.DTR_ID_MOVI_CRZ;
        dtrIdMoviChiu.getDtrIdMoviChiuAsBuffer(buffer, position);
        position += DtrIdMoviChiu.Len.DTR_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dtrDtIniEff, Len.Int.DTR_DT_INI_EFF, 0);
        position += Len.DTR_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtrDtEndEff, Len.Int.DTR_DT_END_EFF, 0);
        position += Len.DTR_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtrCodCompAnia, Len.Int.DTR_COD_COMP_ANIA, 0);
        position += Len.DTR_COD_COMP_ANIA;
        dtrDtIniCop.getDtrDtIniCopAsBuffer(buffer, position);
        position += DtrDtIniCop.Len.DTR_DT_INI_COP;
        dtrDtEndCop.getDtrDtEndCopAsBuffer(buffer, position);
        position += DtrDtEndCop.Len.DTR_DT_END_COP;
        dtrPreNet.getDtrPreNetAsBuffer(buffer, position);
        position += DtrPreNet.Len.DTR_PRE_NET;
        dtrIntrFraz.getDtrIntrFrazAsBuffer(buffer, position);
        position += DtrIntrFraz.Len.DTR_INTR_FRAZ;
        dtrIntrMora.getDtrIntrMoraAsBuffer(buffer, position);
        position += DtrIntrMora.Len.DTR_INTR_MORA;
        dtrIntrRetdt.getDtrIntrRetdtAsBuffer(buffer, position);
        position += DtrIntrRetdt.Len.DTR_INTR_RETDT;
        dtrIntrRiat.getDtrIntrRiatAsBuffer(buffer, position);
        position += DtrIntrRiat.Len.DTR_INTR_RIAT;
        dtrDir.getDtrDirAsBuffer(buffer, position);
        position += DtrDir.Len.DTR_DIR;
        dtrSpeMed.getDtrSpeMedAsBuffer(buffer, position);
        position += DtrSpeMed.Len.DTR_SPE_MED;
        dtrSpeAge.getDtrSpeAgeAsBuffer(buffer, position);
        position += DtrSpeAge.Len.DTR_SPE_AGE;
        dtrTax.getDtrTaxAsBuffer(buffer, position);
        position += DtrTax.Len.DTR_TAX;
        dtrSoprSan.getDtrSoprSanAsBuffer(buffer, position);
        position += DtrSoprSan.Len.DTR_SOPR_SAN;
        dtrSoprSpo.getDtrSoprSpoAsBuffer(buffer, position);
        position += DtrSoprSpo.Len.DTR_SOPR_SPO;
        dtrSoprTec.getDtrSoprTecAsBuffer(buffer, position);
        position += DtrSoprTec.Len.DTR_SOPR_TEC;
        dtrSoprProf.getDtrSoprProfAsBuffer(buffer, position);
        position += DtrSoprProf.Len.DTR_SOPR_PROF;
        dtrSoprAlt.getDtrSoprAltAsBuffer(buffer, position);
        position += DtrSoprAlt.Len.DTR_SOPR_ALT;
        dtrPreTot.getDtrPreTotAsBuffer(buffer, position);
        position += DtrPreTot.Len.DTR_PRE_TOT;
        dtrPrePpIas.getDtrPrePpIasAsBuffer(buffer, position);
        position += DtrPrePpIas.Len.DTR_PRE_PP_IAS;
        dtrPreSoloRsh.getDtrPreSoloRshAsBuffer(buffer, position);
        position += DtrPreSoloRsh.Len.DTR_PRE_SOLO_RSH;
        dtrCarIas.getDtrCarIasAsBuffer(buffer, position);
        position += DtrCarIas.Len.DTR_CAR_IAS;
        dtrProvAcq1aa.getDtrProvAcq1aaAsBuffer(buffer, position);
        position += DtrProvAcq1aa.Len.DTR_PROV_ACQ1AA;
        dtrProvAcq2aa.getDtrProvAcq2aaAsBuffer(buffer, position);
        position += DtrProvAcq2aa.Len.DTR_PROV_ACQ2AA;
        dtrProvRicor.getDtrProvRicorAsBuffer(buffer, position);
        position += DtrProvRicor.Len.DTR_PROV_RICOR;
        dtrProvInc.getDtrProvIncAsBuffer(buffer, position);
        position += DtrProvInc.Len.DTR_PROV_INC;
        dtrProvDaRec.getDtrProvDaRecAsBuffer(buffer, position);
        position += DtrProvDaRec.Len.DTR_PROV_DA_REC;
        MarshalByte.writeString(buffer, position, dtrCodDvs, Len.DTR_COD_DVS);
        position += Len.DTR_COD_DVS;
        dtrFrqMovi.getDtrFrqMoviAsBuffer(buffer, position);
        position += DtrFrqMovi.Len.DTR_FRQ_MOVI;
        MarshalByte.writeString(buffer, position, dtrTpRgmFisc, Len.DTR_TP_RGM_FISC);
        position += Len.DTR_TP_RGM_FISC;
        MarshalByte.writeString(buffer, position, dtrCodTari, Len.DTR_COD_TARI);
        position += Len.DTR_COD_TARI;
        MarshalByte.writeString(buffer, position, dtrTpStatTit, Len.DTR_TP_STAT_TIT);
        position += Len.DTR_TP_STAT_TIT;
        dtrImpAz.getDtrImpAzAsBuffer(buffer, position);
        position += DtrImpAz.Len.DTR_IMP_AZ;
        dtrImpAder.getDtrImpAderAsBuffer(buffer, position);
        position += DtrImpAder.Len.DTR_IMP_ADER;
        dtrImpTfr.getDtrImpTfrAsBuffer(buffer, position);
        position += DtrImpTfr.Len.DTR_IMP_TFR;
        dtrImpVolo.getDtrImpVoloAsBuffer(buffer, position);
        position += DtrImpVolo.Len.DTR_IMP_VOLO;
        MarshalByte.writeChar(buffer, position, dtrFlVldtTit);
        position += Types.CHAR_SIZE;
        dtrCarAcq.getDtrCarAcqAsBuffer(buffer, position);
        position += DtrCarAcq.Len.DTR_CAR_ACQ;
        dtrCarGest.getDtrCarGestAsBuffer(buffer, position);
        position += DtrCarGest.Len.DTR_CAR_GEST;
        dtrCarInc.getDtrCarIncAsBuffer(buffer, position);
        position += DtrCarInc.Len.DTR_CAR_INC;
        dtrManfeeAntic.getDtrManfeeAnticAsBuffer(buffer, position);
        position += DtrManfeeAntic.Len.DTR_MANFEE_ANTIC;
        dtrManfeeRicor.getDtrManfeeRicorAsBuffer(buffer, position);
        position += DtrManfeeRicor.Len.DTR_MANFEE_RICOR;
        MarshalByte.writeString(buffer, position, dtrManfeeRec, Len.DTR_MANFEE_REC);
        position += Len.DTR_MANFEE_REC;
        dtrTotIntrPrest.getDtrTotIntrPrestAsBuffer(buffer, position);
        position += DtrTotIntrPrest.Len.DTR_TOT_INTR_PREST;
        MarshalByte.writeLongAsPacked(buffer, position, dtrDsRiga, Len.Int.DTR_DS_RIGA, 0);
        position += Len.DTR_DS_RIGA;
        MarshalByte.writeChar(buffer, position, dtrDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dtrDsVer, Len.Int.DTR_DS_VER, 0);
        position += Len.DTR_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dtrDsTsIniCptz, Len.Int.DTR_DS_TS_INI_CPTZ, 0);
        position += Len.DTR_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dtrDsTsEndCptz, Len.Int.DTR_DS_TS_END_CPTZ, 0);
        position += Len.DTR_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dtrDsUtente, Len.DTR_DS_UTENTE);
        position += Len.DTR_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dtrDsStatoElab);
        position += Types.CHAR_SIZE;
        dtrImpTrasfe.getDtrImpTrasfeAsBuffer(buffer, position);
        position += DtrImpTrasfe.Len.DTR_IMP_TRASFE;
        dtrImpTfrStrc.getDtrImpTfrStrcAsBuffer(buffer, position);
        position += DtrImpTfrStrc.Len.DTR_IMP_TFR_STRC;
        dtrAcqExp.getDtrAcqExpAsBuffer(buffer, position);
        position += DtrAcqExp.Len.DTR_ACQ_EXP;
        dtrRemunAss.getDtrRemunAssAsBuffer(buffer, position);
        position += DtrRemunAss.Len.DTR_REMUN_ASS;
        dtrCommisInter.getDtrCommisInterAsBuffer(buffer, position);
        position += DtrCommisInter.Len.DTR_COMMIS_INTER;
        dtrCnbtAntirac.getDtrCnbtAntiracAsBuffer(buffer, position);
        return buffer;
    }

    public void setDtrIdDettTitDiRat(int dtrIdDettTitDiRat) {
        this.dtrIdDettTitDiRat = dtrIdDettTitDiRat;
    }

    public int getDtrIdDettTitDiRat() {
        return this.dtrIdDettTitDiRat;
    }

    public void setDtrIdTitRat(int dtrIdTitRat) {
        this.dtrIdTitRat = dtrIdTitRat;
    }

    public int getDtrIdTitRat() {
        return this.dtrIdTitRat;
    }

    public void setDtrIdOgg(int dtrIdOgg) {
        this.dtrIdOgg = dtrIdOgg;
    }

    public int getDtrIdOgg() {
        return this.dtrIdOgg;
    }

    public void setDtrTpOgg(String dtrTpOgg) {
        this.dtrTpOgg = Functions.subString(dtrTpOgg, Len.DTR_TP_OGG);
    }

    public String getDtrTpOgg() {
        return this.dtrTpOgg;
    }

    public void setDtrIdMoviCrz(int dtrIdMoviCrz) {
        this.dtrIdMoviCrz = dtrIdMoviCrz;
    }

    public int getDtrIdMoviCrz() {
        return this.dtrIdMoviCrz;
    }

    public void setDtrDtIniEff(int dtrDtIniEff) {
        this.dtrDtIniEff = dtrDtIniEff;
    }

    public int getDtrDtIniEff() {
        return this.dtrDtIniEff;
    }

    public void setDtrDtEndEff(int dtrDtEndEff) {
        this.dtrDtEndEff = dtrDtEndEff;
    }

    public int getDtrDtEndEff() {
        return this.dtrDtEndEff;
    }

    public void setDtrCodCompAnia(int dtrCodCompAnia) {
        this.dtrCodCompAnia = dtrCodCompAnia;
    }

    public int getDtrCodCompAnia() {
        return this.dtrCodCompAnia;
    }

    public void setDtrCodDvs(String dtrCodDvs) {
        this.dtrCodDvs = Functions.subString(dtrCodDvs, Len.DTR_COD_DVS);
    }

    public String getDtrCodDvs() {
        return this.dtrCodDvs;
    }

    public String getDtrCodDvsFormatted() {
        return Functions.padBlanks(getDtrCodDvs(), Len.DTR_COD_DVS);
    }

    public void setDtrTpRgmFisc(String dtrTpRgmFisc) {
        this.dtrTpRgmFisc = Functions.subString(dtrTpRgmFisc, Len.DTR_TP_RGM_FISC);
    }

    public String getDtrTpRgmFisc() {
        return this.dtrTpRgmFisc;
    }

    public void setDtrCodTari(String dtrCodTari) {
        this.dtrCodTari = Functions.subString(dtrCodTari, Len.DTR_COD_TARI);
    }

    public String getDtrCodTari() {
        return this.dtrCodTari;
    }

    public String getDtrCodTariFormatted() {
        return Functions.padBlanks(getDtrCodTari(), Len.DTR_COD_TARI);
    }

    public void setDtrTpStatTit(String dtrTpStatTit) {
        this.dtrTpStatTit = Functions.subString(dtrTpStatTit, Len.DTR_TP_STAT_TIT);
    }

    public String getDtrTpStatTit() {
        return this.dtrTpStatTit;
    }

    public void setDtrFlVldtTit(char dtrFlVldtTit) {
        this.dtrFlVldtTit = dtrFlVldtTit;
    }

    public void setDtrFlVldtTitFormatted(String dtrFlVldtTit) {
        setDtrFlVldtTit(Functions.charAt(dtrFlVldtTit, Types.CHAR_SIZE));
    }

    public char getDtrFlVldtTit() {
        return this.dtrFlVldtTit;
    }

    public void setDtrManfeeRec(String dtrManfeeRec) {
        this.dtrManfeeRec = Functions.subString(dtrManfeeRec, Len.DTR_MANFEE_REC);
    }

    public String getDtrManfeeRec() {
        return this.dtrManfeeRec;
    }

    public String getDtrManfeeRecFormatted() {
        return Functions.padBlanks(getDtrManfeeRec(), Len.DTR_MANFEE_REC);
    }

    public void setDtrDsRiga(long dtrDsRiga) {
        this.dtrDsRiga = dtrDsRiga;
    }

    public long getDtrDsRiga() {
        return this.dtrDsRiga;
    }

    public void setDtrDsOperSql(char dtrDsOperSql) {
        this.dtrDsOperSql = dtrDsOperSql;
    }

    public char getDtrDsOperSql() {
        return this.dtrDsOperSql;
    }

    public void setDtrDsVer(int dtrDsVer) {
        this.dtrDsVer = dtrDsVer;
    }

    public int getDtrDsVer() {
        return this.dtrDsVer;
    }

    public void setDtrDsTsIniCptz(long dtrDsTsIniCptz) {
        this.dtrDsTsIniCptz = dtrDsTsIniCptz;
    }

    public long getDtrDsTsIniCptz() {
        return this.dtrDsTsIniCptz;
    }

    public void setDtrDsTsEndCptz(long dtrDsTsEndCptz) {
        this.dtrDsTsEndCptz = dtrDsTsEndCptz;
    }

    public long getDtrDsTsEndCptz() {
        return this.dtrDsTsEndCptz;
    }

    public void setDtrDsUtente(String dtrDsUtente) {
        this.dtrDsUtente = Functions.subString(dtrDsUtente, Len.DTR_DS_UTENTE);
    }

    public String getDtrDsUtente() {
        return this.dtrDsUtente;
    }

    public void setDtrDsStatoElab(char dtrDsStatoElab) {
        this.dtrDsStatoElab = dtrDsStatoElab;
    }

    public char getDtrDsStatoElab() {
        return this.dtrDsStatoElab;
    }

    public DtrAcqExp getDtrAcqExp() {
        return dtrAcqExp;
    }

    public DtrCarAcq getDtrCarAcq() {
        return dtrCarAcq;
    }

    public DtrCarGest getDtrCarGest() {
        return dtrCarGest;
    }

    public DtrCarIas getDtrCarIas() {
        return dtrCarIas;
    }

    public DtrCarInc getDtrCarInc() {
        return dtrCarInc;
    }

    public DtrCnbtAntirac getDtrCnbtAntirac() {
        return dtrCnbtAntirac;
    }

    public DtrCommisInter getDtrCommisInter() {
        return dtrCommisInter;
    }

    public DtrDir getDtrDir() {
        return dtrDir;
    }

    public DtrDtEndCop getDtrDtEndCop() {
        return dtrDtEndCop;
    }

    public DtrDtIniCop getDtrDtIniCop() {
        return dtrDtIniCop;
    }

    public DtrFrqMovi getDtrFrqMovi() {
        return dtrFrqMovi;
    }

    public DtrIdMoviChiu getDtrIdMoviChiu() {
        return dtrIdMoviChiu;
    }

    public DtrImpAder getDtrImpAder() {
        return dtrImpAder;
    }

    public DtrImpAz getDtrImpAz() {
        return dtrImpAz;
    }

    public DtrImpTfr getDtrImpTfr() {
        return dtrImpTfr;
    }

    public DtrImpTfrStrc getDtrImpTfrStrc() {
        return dtrImpTfrStrc;
    }

    public DtrImpTrasfe getDtrImpTrasfe() {
        return dtrImpTrasfe;
    }

    public DtrImpVolo getDtrImpVolo() {
        return dtrImpVolo;
    }

    public DtrIntrFraz getDtrIntrFraz() {
        return dtrIntrFraz;
    }

    public DtrIntrMora getDtrIntrMora() {
        return dtrIntrMora;
    }

    public DtrIntrRetdt getDtrIntrRetdt() {
        return dtrIntrRetdt;
    }

    public DtrIntrRiat getDtrIntrRiat() {
        return dtrIntrRiat;
    }

    public DtrManfeeAntic getDtrManfeeAntic() {
        return dtrManfeeAntic;
    }

    public DtrManfeeRicor getDtrManfeeRicor() {
        return dtrManfeeRicor;
    }

    public DtrPreNet getDtrPreNet() {
        return dtrPreNet;
    }

    public DtrPrePpIas getDtrPrePpIas() {
        return dtrPrePpIas;
    }

    public DtrPreSoloRsh getDtrPreSoloRsh() {
        return dtrPreSoloRsh;
    }

    public DtrPreTot getDtrPreTot() {
        return dtrPreTot;
    }

    public DtrProvAcq1aa getDtrProvAcq1aa() {
        return dtrProvAcq1aa;
    }

    public DtrProvAcq2aa getDtrProvAcq2aa() {
        return dtrProvAcq2aa;
    }

    public DtrProvDaRec getDtrProvDaRec() {
        return dtrProvDaRec;
    }

    public DtrProvInc getDtrProvInc() {
        return dtrProvInc;
    }

    public DtrProvRicor getDtrProvRicor() {
        return dtrProvRicor;
    }

    public DtrRemunAss getDtrRemunAss() {
        return dtrRemunAss;
    }

    public DtrSoprAlt getDtrSoprAlt() {
        return dtrSoprAlt;
    }

    public DtrSoprProf getDtrSoprProf() {
        return dtrSoprProf;
    }

    public DtrSoprSan getDtrSoprSan() {
        return dtrSoprSan;
    }

    public DtrSoprSpo getDtrSoprSpo() {
        return dtrSoprSpo;
    }

    public DtrSoprTec getDtrSoprTec() {
        return dtrSoprTec;
    }

    public DtrSpeAge getDtrSpeAge() {
        return dtrSpeAge;
    }

    public DtrSpeMed getDtrSpeMed() {
        return dtrSpeMed;
    }

    public DtrTax getDtrTax() {
        return dtrTax;
    }

    public DtrTotIntrPrest getDtrTotIntrPrest() {
        return dtrTotIntrPrest;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_TP_OGG = 2;
        public static final int DTR_COD_DVS = 20;
        public static final int DTR_TP_RGM_FISC = 2;
        public static final int DTR_COD_TARI = 12;
        public static final int DTR_TP_STAT_TIT = 2;
        public static final int DTR_MANFEE_REC = 18;
        public static final int DTR_DS_UTENTE = 20;
        public static final int DTR_ID_DETT_TIT_DI_RAT = 5;
        public static final int DTR_ID_TIT_RAT = 5;
        public static final int DTR_ID_OGG = 5;
        public static final int DTR_ID_MOVI_CRZ = 5;
        public static final int DTR_DT_INI_EFF = 5;
        public static final int DTR_DT_END_EFF = 5;
        public static final int DTR_COD_COMP_ANIA = 3;
        public static final int DTR_FL_VLDT_TIT = 1;
        public static final int DTR_DS_RIGA = 6;
        public static final int DTR_DS_OPER_SQL = 1;
        public static final int DTR_DS_VER = 5;
        public static final int DTR_DS_TS_INI_CPTZ = 10;
        public static final int DTR_DS_TS_END_CPTZ = 10;
        public static final int DTR_DS_STATO_ELAB = 1;
        public static final int DETT_TIT_DI_RAT = DTR_ID_DETT_TIT_DI_RAT + DTR_ID_TIT_RAT + DTR_ID_OGG + DTR_TP_OGG + DTR_ID_MOVI_CRZ + DtrIdMoviChiu.Len.DTR_ID_MOVI_CHIU + DTR_DT_INI_EFF + DTR_DT_END_EFF + DTR_COD_COMP_ANIA + DtrDtIniCop.Len.DTR_DT_INI_COP + DtrDtEndCop.Len.DTR_DT_END_COP + DtrPreNet.Len.DTR_PRE_NET + DtrIntrFraz.Len.DTR_INTR_FRAZ + DtrIntrMora.Len.DTR_INTR_MORA + DtrIntrRetdt.Len.DTR_INTR_RETDT + DtrIntrRiat.Len.DTR_INTR_RIAT + DtrDir.Len.DTR_DIR + DtrSpeMed.Len.DTR_SPE_MED + DtrSpeAge.Len.DTR_SPE_AGE + DtrTax.Len.DTR_TAX + DtrSoprSan.Len.DTR_SOPR_SAN + DtrSoprSpo.Len.DTR_SOPR_SPO + DtrSoprTec.Len.DTR_SOPR_TEC + DtrSoprProf.Len.DTR_SOPR_PROF + DtrSoprAlt.Len.DTR_SOPR_ALT + DtrPreTot.Len.DTR_PRE_TOT + DtrPrePpIas.Len.DTR_PRE_PP_IAS + DtrPreSoloRsh.Len.DTR_PRE_SOLO_RSH + DtrCarIas.Len.DTR_CAR_IAS + DtrProvAcq1aa.Len.DTR_PROV_ACQ1AA + DtrProvAcq2aa.Len.DTR_PROV_ACQ2AA + DtrProvRicor.Len.DTR_PROV_RICOR + DtrProvInc.Len.DTR_PROV_INC + DtrProvDaRec.Len.DTR_PROV_DA_REC + DTR_COD_DVS + DtrFrqMovi.Len.DTR_FRQ_MOVI + DTR_TP_RGM_FISC + DTR_COD_TARI + DTR_TP_STAT_TIT + DtrImpAz.Len.DTR_IMP_AZ + DtrImpAder.Len.DTR_IMP_ADER + DtrImpTfr.Len.DTR_IMP_TFR + DtrImpVolo.Len.DTR_IMP_VOLO + DTR_FL_VLDT_TIT + DtrCarAcq.Len.DTR_CAR_ACQ + DtrCarGest.Len.DTR_CAR_GEST + DtrCarInc.Len.DTR_CAR_INC + DtrManfeeAntic.Len.DTR_MANFEE_ANTIC + DtrManfeeRicor.Len.DTR_MANFEE_RICOR + DTR_MANFEE_REC + DtrTotIntrPrest.Len.DTR_TOT_INTR_PREST + DTR_DS_RIGA + DTR_DS_OPER_SQL + DTR_DS_VER + DTR_DS_TS_INI_CPTZ + DTR_DS_TS_END_CPTZ + DTR_DS_UTENTE + DTR_DS_STATO_ELAB + DtrImpTrasfe.Len.DTR_IMP_TRASFE + DtrImpTfrStrc.Len.DTR_IMP_TFR_STRC + DtrAcqExp.Len.DTR_ACQ_EXP + DtrRemunAss.Len.DTR_REMUN_ASS + DtrCommisInter.Len.DTR_COMMIS_INTER + DtrCnbtAntirac.Len.DTR_CNBT_ANTIRAC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_ID_DETT_TIT_DI_RAT = 9;
            public static final int DTR_ID_TIT_RAT = 9;
            public static final int DTR_ID_OGG = 9;
            public static final int DTR_ID_MOVI_CRZ = 9;
            public static final int DTR_DT_INI_EFF = 8;
            public static final int DTR_DT_END_EFF = 8;
            public static final int DTR_COD_COMP_ANIA = 5;
            public static final int DTR_DS_RIGA = 10;
            public static final int DTR_DS_VER = 9;
            public static final int DTR_DS_TS_INI_CPTZ = 18;
            public static final int DTR_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
