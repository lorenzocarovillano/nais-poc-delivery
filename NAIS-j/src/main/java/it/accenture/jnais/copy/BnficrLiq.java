package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.BelDtDormienza;
import it.accenture.jnais.ws.redefines.BelDtRiserveSomP;
import it.accenture.jnais.ws.redefines.BelDtUltDocto;
import it.accenture.jnais.ws.redefines.BelDtVlt;
import it.accenture.jnais.ws.redefines.BelIdAssto;
import it.accenture.jnais.ws.redefines.BelIdMoviChiu;
import it.accenture.jnais.ws.redefines.BelIdRappAna;
import it.accenture.jnais.ws.redefines.BelImpIntrRitPag;
import it.accenture.jnais.ws.redefines.BelImpLrdLiqto;
import it.accenture.jnais.ws.redefines.BelImpNetLiqto;
import it.accenture.jnais.ws.redefines.BelImpst252Ipt;
import it.accenture.jnais.ws.redefines.BelImpstBolloTotV;
import it.accenture.jnais.ws.redefines.BelImpstIpt;
import it.accenture.jnais.ws.redefines.BelImpstIrpefIpt;
import it.accenture.jnais.ws.redefines.BelImpstPrvrIpt;
import it.accenture.jnais.ws.redefines.BelImpstSost1382011;
import it.accenture.jnais.ws.redefines.BelImpstSost662014;
import it.accenture.jnais.ws.redefines.BelImpstSostIpt;
import it.accenture.jnais.ws.redefines.BelImpstVis1382011;
import it.accenture.jnais.ws.redefines.BelImpstVis662014;
import it.accenture.jnais.ws.redefines.BelPcLiq;
import it.accenture.jnais.ws.redefines.BelRitAccIpt;
import it.accenture.jnais.ws.redefines.BelRitTfrIpt;
import it.accenture.jnais.ws.redefines.BelRitVisIpt;
import it.accenture.jnais.ws.redefines.BelTaxSep;

/**Original name: BNFICR-LIQ<br>
 * Variable: BNFICR-LIQ from copybook IDBVBEL1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class BnficrLiq {

    //==== PROPERTIES ====
    //Original name: BEL-ID-BNFICR-LIQ
    private int belIdBnficrLiq = DefaultValues.INT_VAL;
    //Original name: BEL-ID-LIQ
    private int belIdLiq = DefaultValues.INT_VAL;
    //Original name: BEL-ID-MOVI-CRZ
    private int belIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: BEL-ID-MOVI-CHIU
    private BelIdMoviChiu belIdMoviChiu = new BelIdMoviChiu();
    //Original name: BEL-COD-COMP-ANIA
    private int belCodCompAnia = DefaultValues.INT_VAL;
    //Original name: BEL-DT-INI-EFF
    private int belDtIniEff = DefaultValues.INT_VAL;
    //Original name: BEL-DT-END-EFF
    private int belDtEndEff = DefaultValues.INT_VAL;
    //Original name: BEL-ID-RAPP-ANA
    private BelIdRappAna belIdRappAna = new BelIdRappAna();
    //Original name: BEL-COD-BNFICR
    private String belCodBnficr = DefaultValues.stringVal(Len.BEL_COD_BNFICR);
    //Original name: BEL-DESC-BNFICR-LEN
    private short belDescBnficrLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: BEL-DESC-BNFICR
    private String belDescBnficr = DefaultValues.stringVal(Len.BEL_DESC_BNFICR);
    //Original name: BEL-PC-LIQ
    private BelPcLiq belPcLiq = new BelPcLiq();
    //Original name: BEL-ESRCN-ATTVT-IMPRS
    private char belEsrcnAttvtImprs = DefaultValues.CHAR_VAL;
    //Original name: BEL-TP-IND-BNFICR
    private String belTpIndBnficr = DefaultValues.stringVal(Len.BEL_TP_IND_BNFICR);
    //Original name: BEL-FL-ESE
    private char belFlEse = DefaultValues.CHAR_VAL;
    //Original name: BEL-FL-IRREV
    private char belFlIrrev = DefaultValues.CHAR_VAL;
    //Original name: BEL-IMP-LRD-LIQTO
    private BelImpLrdLiqto belImpLrdLiqto = new BelImpLrdLiqto();
    //Original name: BEL-IMPST-IPT
    private BelImpstIpt belImpstIpt = new BelImpstIpt();
    //Original name: BEL-IMP-NET-LIQTO
    private BelImpNetLiqto belImpNetLiqto = new BelImpNetLiqto();
    //Original name: BEL-RIT-ACC-IPT
    private BelRitAccIpt belRitAccIpt = new BelRitAccIpt();
    //Original name: BEL-RIT-VIS-IPT
    private BelRitVisIpt belRitVisIpt = new BelRitVisIpt();
    //Original name: BEL-RIT-TFR-IPT
    private BelRitTfrIpt belRitTfrIpt = new BelRitTfrIpt();
    //Original name: BEL-IMPST-IRPEF-IPT
    private BelImpstIrpefIpt belImpstIrpefIpt = new BelImpstIrpefIpt();
    //Original name: BEL-IMPST-SOST-IPT
    private BelImpstSostIpt belImpstSostIpt = new BelImpstSostIpt();
    //Original name: BEL-IMPST-PRVR-IPT
    private BelImpstPrvrIpt belImpstPrvrIpt = new BelImpstPrvrIpt();
    //Original name: BEL-IMPST-252-IPT
    private BelImpst252Ipt belImpst252Ipt = new BelImpst252Ipt();
    //Original name: BEL-ID-ASSTO
    private BelIdAssto belIdAssto = new BelIdAssto();
    //Original name: BEL-TAX-SEP
    private BelTaxSep belTaxSep = new BelTaxSep();
    //Original name: BEL-DT-RISERVE-SOM-P
    private BelDtRiserveSomP belDtRiserveSomP = new BelDtRiserveSomP();
    //Original name: BEL-DT-VLT
    private BelDtVlt belDtVlt = new BelDtVlt();
    //Original name: BEL-TP-STAT-LIQ-BNFICR
    private String belTpStatLiqBnficr = DefaultValues.stringVal(Len.BEL_TP_STAT_LIQ_BNFICR);
    //Original name: BEL-DS-RIGA
    private long belDsRiga = DefaultValues.LONG_VAL;
    //Original name: BEL-DS-OPER-SQL
    private char belDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: BEL-DS-VER
    private int belDsVer = DefaultValues.INT_VAL;
    //Original name: BEL-DS-TS-INI-CPTZ
    private long belDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: BEL-DS-TS-END-CPTZ
    private long belDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: BEL-DS-UTENTE
    private String belDsUtente = DefaultValues.stringVal(Len.BEL_DS_UTENTE);
    //Original name: BEL-DS-STATO-ELAB
    private char belDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: BEL-RICH-CALC-CNBT-INP
    private char belRichCalcCnbtInp = DefaultValues.CHAR_VAL;
    //Original name: BEL-IMP-INTR-RIT-PAG
    private BelImpIntrRitPag belImpIntrRitPag = new BelImpIntrRitPag();
    //Original name: BEL-DT-ULT-DOCTO
    private BelDtUltDocto belDtUltDocto = new BelDtUltDocto();
    //Original name: BEL-DT-DORMIENZA
    private BelDtDormienza belDtDormienza = new BelDtDormienza();
    //Original name: BEL-IMPST-BOLLO-TOT-V
    private BelImpstBolloTotV belImpstBolloTotV = new BelImpstBolloTotV();
    //Original name: BEL-IMPST-VIS-1382011
    private BelImpstVis1382011 belImpstVis1382011 = new BelImpstVis1382011();
    //Original name: BEL-IMPST-SOST-1382011
    private BelImpstSost1382011 belImpstSost1382011 = new BelImpstSost1382011();
    //Original name: BEL-IMPST-VIS-662014
    private BelImpstVis662014 belImpstVis662014 = new BelImpstVis662014();
    //Original name: BEL-IMPST-SOST-662014
    private BelImpstSost662014 belImpstSost662014 = new BelImpstSost662014();

    //==== METHODS ====
    public void setBnficrLiqFormatted(String data) {
        byte[] buffer = new byte[Len.BNFICR_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.BNFICR_LIQ);
        setBnficrLiqBytes(buffer, 1);
    }

    public void setBnficrLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        belIdBnficrLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEL_ID_BNFICR_LIQ, 0);
        position += Len.BEL_ID_BNFICR_LIQ;
        belIdLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEL_ID_LIQ, 0);
        position += Len.BEL_ID_LIQ;
        belIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEL_ID_MOVI_CRZ, 0);
        position += Len.BEL_ID_MOVI_CRZ;
        belIdMoviChiu.setBelIdMoviChiuFromBuffer(buffer, position);
        position += BelIdMoviChiu.Len.BEL_ID_MOVI_CHIU;
        belCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEL_COD_COMP_ANIA, 0);
        position += Len.BEL_COD_COMP_ANIA;
        belDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEL_DT_INI_EFF, 0);
        position += Len.BEL_DT_INI_EFF;
        belDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEL_DT_END_EFF, 0);
        position += Len.BEL_DT_END_EFF;
        belIdRappAna.setBelIdRappAnaFromBuffer(buffer, position);
        position += BelIdRappAna.Len.BEL_ID_RAPP_ANA;
        belCodBnficr = MarshalByte.readString(buffer, position, Len.BEL_COD_BNFICR);
        position += Len.BEL_COD_BNFICR;
        setBelDescBnficrVcharBytes(buffer, position);
        position += Len.BEL_DESC_BNFICR_VCHAR;
        belPcLiq.setBelPcLiqFromBuffer(buffer, position);
        position += BelPcLiq.Len.BEL_PC_LIQ;
        belEsrcnAttvtImprs = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        belTpIndBnficr = MarshalByte.readString(buffer, position, Len.BEL_TP_IND_BNFICR);
        position += Len.BEL_TP_IND_BNFICR;
        belFlEse = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        belFlIrrev = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        belImpLrdLiqto.setBelImpLrdLiqtoFromBuffer(buffer, position);
        position += BelImpLrdLiqto.Len.BEL_IMP_LRD_LIQTO;
        belImpstIpt.setBelImpstIptFromBuffer(buffer, position);
        position += BelImpstIpt.Len.BEL_IMPST_IPT;
        belImpNetLiqto.setBelImpNetLiqtoFromBuffer(buffer, position);
        position += BelImpNetLiqto.Len.BEL_IMP_NET_LIQTO;
        belRitAccIpt.setBelRitAccIptFromBuffer(buffer, position);
        position += BelRitAccIpt.Len.BEL_RIT_ACC_IPT;
        belRitVisIpt.setBelRitVisIptFromBuffer(buffer, position);
        position += BelRitVisIpt.Len.BEL_RIT_VIS_IPT;
        belRitTfrIpt.setBelRitTfrIptFromBuffer(buffer, position);
        position += BelRitTfrIpt.Len.BEL_RIT_TFR_IPT;
        belImpstIrpefIpt.setBelImpstIrpefIptFromBuffer(buffer, position);
        position += BelImpstIrpefIpt.Len.BEL_IMPST_IRPEF_IPT;
        belImpstSostIpt.setBelImpstSostIptFromBuffer(buffer, position);
        position += BelImpstSostIpt.Len.BEL_IMPST_SOST_IPT;
        belImpstPrvrIpt.setBelImpstPrvrIptFromBuffer(buffer, position);
        position += BelImpstPrvrIpt.Len.BEL_IMPST_PRVR_IPT;
        belImpst252Ipt.setBelImpst252IptFromBuffer(buffer, position);
        position += BelImpst252Ipt.Len.BEL_IMPST252_IPT;
        belIdAssto.setBelIdAsstoFromBuffer(buffer, position);
        position += BelIdAssto.Len.BEL_ID_ASSTO;
        belTaxSep.setBelTaxSepFromBuffer(buffer, position);
        position += BelTaxSep.Len.BEL_TAX_SEP;
        belDtRiserveSomP.setBelDtRiserveSomPFromBuffer(buffer, position);
        position += BelDtRiserveSomP.Len.BEL_DT_RISERVE_SOM_P;
        belDtVlt.setBelDtVltFromBuffer(buffer, position);
        position += BelDtVlt.Len.BEL_DT_VLT;
        belTpStatLiqBnficr = MarshalByte.readString(buffer, position, Len.BEL_TP_STAT_LIQ_BNFICR);
        position += Len.BEL_TP_STAT_LIQ_BNFICR;
        belDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.BEL_DS_RIGA, 0);
        position += Len.BEL_DS_RIGA;
        belDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        belDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEL_DS_VER, 0);
        position += Len.BEL_DS_VER;
        belDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.BEL_DS_TS_INI_CPTZ, 0);
        position += Len.BEL_DS_TS_INI_CPTZ;
        belDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.BEL_DS_TS_END_CPTZ, 0);
        position += Len.BEL_DS_TS_END_CPTZ;
        belDsUtente = MarshalByte.readString(buffer, position, Len.BEL_DS_UTENTE);
        position += Len.BEL_DS_UTENTE;
        belDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        belRichCalcCnbtInp = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        belImpIntrRitPag.setBelImpIntrRitPagFromBuffer(buffer, position);
        position += BelImpIntrRitPag.Len.BEL_IMP_INTR_RIT_PAG;
        belDtUltDocto.setBelDtUltDoctoFromBuffer(buffer, position);
        position += BelDtUltDocto.Len.BEL_DT_ULT_DOCTO;
        belDtDormienza.setBelDtDormienzaFromBuffer(buffer, position);
        position += BelDtDormienza.Len.BEL_DT_DORMIENZA;
        belImpstBolloTotV.setBelImpstBolloTotVFromBuffer(buffer, position);
        position += BelImpstBolloTotV.Len.BEL_IMPST_BOLLO_TOT_V;
        belImpstVis1382011.setBelImpstVis1382011FromBuffer(buffer, position);
        position += BelImpstVis1382011.Len.BEL_IMPST_VIS1382011;
        belImpstSost1382011.setBelImpstSost1382011FromBuffer(buffer, position);
        position += BelImpstSost1382011.Len.BEL_IMPST_SOST1382011;
        belImpstVis662014.setBelImpstVis662014FromBuffer(buffer, position);
        position += BelImpstVis662014.Len.BEL_IMPST_VIS662014;
        belImpstSost662014.setBelImpstSost662014FromBuffer(buffer, position);
    }

    public void setBelIdBnficrLiq(int belIdBnficrLiq) {
        this.belIdBnficrLiq = belIdBnficrLiq;
    }

    public int getBelIdBnficrLiq() {
        return this.belIdBnficrLiq;
    }

    public void setBelIdLiq(int belIdLiq) {
        this.belIdLiq = belIdLiq;
    }

    public int getBelIdLiq() {
        return this.belIdLiq;
    }

    public void setBelIdMoviCrz(int belIdMoviCrz) {
        this.belIdMoviCrz = belIdMoviCrz;
    }

    public int getBelIdMoviCrz() {
        return this.belIdMoviCrz;
    }

    public void setBelCodCompAnia(int belCodCompAnia) {
        this.belCodCompAnia = belCodCompAnia;
    }

    public int getBelCodCompAnia() {
        return this.belCodCompAnia;
    }

    public void setBelDtIniEff(int belDtIniEff) {
        this.belDtIniEff = belDtIniEff;
    }

    public int getBelDtIniEff() {
        return this.belDtIniEff;
    }

    public void setBelDtEndEff(int belDtEndEff) {
        this.belDtEndEff = belDtEndEff;
    }

    public int getBelDtEndEff() {
        return this.belDtEndEff;
    }

    public void setBelCodBnficr(String belCodBnficr) {
        this.belCodBnficr = Functions.subString(belCodBnficr, Len.BEL_COD_BNFICR);
    }

    public String getBelCodBnficr() {
        return this.belCodBnficr;
    }

    public String getBelCodBnficrFormatted() {
        return Functions.padBlanks(getBelCodBnficr(), Len.BEL_COD_BNFICR);
    }

    public void setBelDescBnficrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        belDescBnficrLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        belDescBnficr = MarshalByte.readString(buffer, position, Len.BEL_DESC_BNFICR);
    }

    public void setBelDescBnficrLen(short belDescBnficrLen) {
        this.belDescBnficrLen = belDescBnficrLen;
    }

    public short getBelDescBnficrLen() {
        return this.belDescBnficrLen;
    }

    public void setBelDescBnficr(String belDescBnficr) {
        this.belDescBnficr = Functions.subString(belDescBnficr, Len.BEL_DESC_BNFICR);
    }

    public String getBelDescBnficr() {
        return this.belDescBnficr;
    }

    public void setBelEsrcnAttvtImprs(char belEsrcnAttvtImprs) {
        this.belEsrcnAttvtImprs = belEsrcnAttvtImprs;
    }

    public char getBelEsrcnAttvtImprs() {
        return this.belEsrcnAttvtImprs;
    }

    public void setBelTpIndBnficr(String belTpIndBnficr) {
        this.belTpIndBnficr = Functions.subString(belTpIndBnficr, Len.BEL_TP_IND_BNFICR);
    }

    public String getBelTpIndBnficr() {
        return this.belTpIndBnficr;
    }

    public String getBelTpIndBnficrFormatted() {
        return Functions.padBlanks(getBelTpIndBnficr(), Len.BEL_TP_IND_BNFICR);
    }

    public void setBelFlEse(char belFlEse) {
        this.belFlEse = belFlEse;
    }

    public char getBelFlEse() {
        return this.belFlEse;
    }

    public void setBelFlIrrev(char belFlIrrev) {
        this.belFlIrrev = belFlIrrev;
    }

    public char getBelFlIrrev() {
        return this.belFlIrrev;
    }

    public void setBelTpStatLiqBnficr(String belTpStatLiqBnficr) {
        this.belTpStatLiqBnficr = Functions.subString(belTpStatLiqBnficr, Len.BEL_TP_STAT_LIQ_BNFICR);
    }

    public String getBelTpStatLiqBnficr() {
        return this.belTpStatLiqBnficr;
    }

    public String getBelTpStatLiqBnficrFormatted() {
        return Functions.padBlanks(getBelTpStatLiqBnficr(), Len.BEL_TP_STAT_LIQ_BNFICR);
    }

    public void setBelDsRiga(long belDsRiga) {
        this.belDsRiga = belDsRiga;
    }

    public long getBelDsRiga() {
        return this.belDsRiga;
    }

    public void setBelDsOperSql(char belDsOperSql) {
        this.belDsOperSql = belDsOperSql;
    }

    public char getBelDsOperSql() {
        return this.belDsOperSql;
    }

    public void setBelDsVer(int belDsVer) {
        this.belDsVer = belDsVer;
    }

    public int getBelDsVer() {
        return this.belDsVer;
    }

    public void setBelDsTsIniCptz(long belDsTsIniCptz) {
        this.belDsTsIniCptz = belDsTsIniCptz;
    }

    public long getBelDsTsIniCptz() {
        return this.belDsTsIniCptz;
    }

    public void setBelDsTsEndCptz(long belDsTsEndCptz) {
        this.belDsTsEndCptz = belDsTsEndCptz;
    }

    public long getBelDsTsEndCptz() {
        return this.belDsTsEndCptz;
    }

    public void setBelDsUtente(String belDsUtente) {
        this.belDsUtente = Functions.subString(belDsUtente, Len.BEL_DS_UTENTE);
    }

    public String getBelDsUtente() {
        return this.belDsUtente;
    }

    public void setBelDsStatoElab(char belDsStatoElab) {
        this.belDsStatoElab = belDsStatoElab;
    }

    public char getBelDsStatoElab() {
        return this.belDsStatoElab;
    }

    public void setBelRichCalcCnbtInp(char belRichCalcCnbtInp) {
        this.belRichCalcCnbtInp = belRichCalcCnbtInp;
    }

    public char getBelRichCalcCnbtInp() {
        return this.belRichCalcCnbtInp;
    }

    public BelDtDormienza getBelDtDormienza() {
        return belDtDormienza;
    }

    public BelDtRiserveSomP getBelDtRiserveSomP() {
        return belDtRiserveSomP;
    }

    public BelDtUltDocto getBelDtUltDocto() {
        return belDtUltDocto;
    }

    public BelDtVlt getBelDtVlt() {
        return belDtVlt;
    }

    public BelIdAssto getBelIdAssto() {
        return belIdAssto;
    }

    public BelIdMoviChiu getBelIdMoviChiu() {
        return belIdMoviChiu;
    }

    public BelIdRappAna getBelIdRappAna() {
        return belIdRappAna;
    }

    public BelImpIntrRitPag getBelImpIntrRitPag() {
        return belImpIntrRitPag;
    }

    public BelImpLrdLiqto getBelImpLrdLiqto() {
        return belImpLrdLiqto;
    }

    public BelImpNetLiqto getBelImpNetLiqto() {
        return belImpNetLiqto;
    }

    public BelImpst252Ipt getBelImpst252Ipt() {
        return belImpst252Ipt;
    }

    public BelImpstBolloTotV getBelImpstBolloTotV() {
        return belImpstBolloTotV;
    }

    public BelImpstIpt getBelImpstIpt() {
        return belImpstIpt;
    }

    public BelImpstIrpefIpt getBelImpstIrpefIpt() {
        return belImpstIrpefIpt;
    }

    public BelImpstPrvrIpt getBelImpstPrvrIpt() {
        return belImpstPrvrIpt;
    }

    public BelImpstSost1382011 getBelImpstSost1382011() {
        return belImpstSost1382011;
    }

    public BelImpstSost662014 getBelImpstSost662014() {
        return belImpstSost662014;
    }

    public BelImpstSostIpt getBelImpstSostIpt() {
        return belImpstSostIpt;
    }

    public BelImpstVis1382011 getBelImpstVis1382011() {
        return belImpstVis1382011;
    }

    public BelImpstVis662014 getBelImpstVis662014() {
        return belImpstVis662014;
    }

    public BelPcLiq getBelPcLiq() {
        return belPcLiq;
    }

    public BelRitAccIpt getBelRitAccIpt() {
        return belRitAccIpt;
    }

    public BelRitTfrIpt getBelRitTfrIpt() {
        return belRitTfrIpt;
    }

    public BelRitVisIpt getBelRitVisIpt() {
        return belRitVisIpt;
    }

    public BelTaxSep getBelTaxSep() {
        return belTaxSep;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_COD_BNFICR = 20;
        public static final int BEL_DESC_BNFICR = 100;
        public static final int BEL_TP_IND_BNFICR = 2;
        public static final int BEL_TP_STAT_LIQ_BNFICR = 2;
        public static final int BEL_DS_UTENTE = 20;
        public static final int BEL_ID_BNFICR_LIQ = 5;
        public static final int BEL_ID_LIQ = 5;
        public static final int BEL_ID_MOVI_CRZ = 5;
        public static final int BEL_COD_COMP_ANIA = 3;
        public static final int BEL_DT_INI_EFF = 5;
        public static final int BEL_DT_END_EFF = 5;
        public static final int BEL_DESC_BNFICR_LEN = 2;
        public static final int BEL_DESC_BNFICR_VCHAR = BEL_DESC_BNFICR_LEN + BEL_DESC_BNFICR;
        public static final int BEL_ESRCN_ATTVT_IMPRS = 1;
        public static final int BEL_FL_ESE = 1;
        public static final int BEL_FL_IRREV = 1;
        public static final int BEL_DS_RIGA = 6;
        public static final int BEL_DS_OPER_SQL = 1;
        public static final int BEL_DS_VER = 5;
        public static final int BEL_DS_TS_INI_CPTZ = 10;
        public static final int BEL_DS_TS_END_CPTZ = 10;
        public static final int BEL_DS_STATO_ELAB = 1;
        public static final int BEL_RICH_CALC_CNBT_INP = 1;
        public static final int BNFICR_LIQ = BEL_ID_BNFICR_LIQ + BEL_ID_LIQ + BEL_ID_MOVI_CRZ + BelIdMoviChiu.Len.BEL_ID_MOVI_CHIU + BEL_COD_COMP_ANIA + BEL_DT_INI_EFF + BEL_DT_END_EFF + BelIdRappAna.Len.BEL_ID_RAPP_ANA + BEL_COD_BNFICR + BEL_DESC_BNFICR_VCHAR + BelPcLiq.Len.BEL_PC_LIQ + BEL_ESRCN_ATTVT_IMPRS + BEL_TP_IND_BNFICR + BEL_FL_ESE + BEL_FL_IRREV + BelImpLrdLiqto.Len.BEL_IMP_LRD_LIQTO + BelImpstIpt.Len.BEL_IMPST_IPT + BelImpNetLiqto.Len.BEL_IMP_NET_LIQTO + BelRitAccIpt.Len.BEL_RIT_ACC_IPT + BelRitVisIpt.Len.BEL_RIT_VIS_IPT + BelRitTfrIpt.Len.BEL_RIT_TFR_IPT + BelImpstIrpefIpt.Len.BEL_IMPST_IRPEF_IPT + BelImpstSostIpt.Len.BEL_IMPST_SOST_IPT + BelImpstPrvrIpt.Len.BEL_IMPST_PRVR_IPT + BelImpst252Ipt.Len.BEL_IMPST252_IPT + BelIdAssto.Len.BEL_ID_ASSTO + BelTaxSep.Len.BEL_TAX_SEP + BelDtRiserveSomP.Len.BEL_DT_RISERVE_SOM_P + BelDtVlt.Len.BEL_DT_VLT + BEL_TP_STAT_LIQ_BNFICR + BEL_DS_RIGA + BEL_DS_OPER_SQL + BEL_DS_VER + BEL_DS_TS_INI_CPTZ + BEL_DS_TS_END_CPTZ + BEL_DS_UTENTE + BEL_DS_STATO_ELAB + BEL_RICH_CALC_CNBT_INP + BelImpIntrRitPag.Len.BEL_IMP_INTR_RIT_PAG + BelDtUltDocto.Len.BEL_DT_ULT_DOCTO + BelDtDormienza.Len.BEL_DT_DORMIENZA + BelImpstBolloTotV.Len.BEL_IMPST_BOLLO_TOT_V + BelImpstVis1382011.Len.BEL_IMPST_VIS1382011 + BelImpstSost1382011.Len.BEL_IMPST_SOST1382011 + BelImpstVis662014.Len.BEL_IMPST_VIS662014 + BelImpstSost662014.Len.BEL_IMPST_SOST662014;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_ID_BNFICR_LIQ = 9;
            public static final int BEL_ID_LIQ = 9;
            public static final int BEL_ID_MOVI_CRZ = 9;
            public static final int BEL_COD_COMP_ANIA = 5;
            public static final int BEL_DT_INI_EFF = 8;
            public static final int BEL_DT_END_EFF = 8;
            public static final int BEL_DS_RIGA = 10;
            public static final int BEL_DS_VER = 9;
            public static final int BEL_DS_TS_INI_CPTZ = 18;
            public static final int BEL_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
