package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV1421<br>
 * Variable: LDBV1421 from copybook LDBV1421<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv1421 {

    //==== PROPERTIES ====
    //Original name: LDBV1421-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LDBV1421-RAMO-BILA-1
    private String ramoBila1 = DefaultValues.stringVal(Len.RAMO_BILA1);
    //Original name: LDBV1421-RAMO-BILA-2
    private String ramoBila2 = DefaultValues.stringVal(Len.RAMO_BILA2);
    //Original name: LDBV1421-RAMO-BILA-3
    private String ramoBila3 = DefaultValues.stringVal(Len.RAMO_BILA3);
    //Original name: LDBV1421-RAMO-BILA-4
    private String ramoBila4 = DefaultValues.stringVal(Len.RAMO_BILA4);
    //Original name: LDBV1421-RAMO-BILA-5
    private String ramoBila5 = DefaultValues.stringVal(Len.RAMO_BILA5);

    //==== METHODS ====
    public void setLdbv1421Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV1421];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV1421);
        setLdbv1421Bytes(buffer, 1);
    }

    public String getLdbv1421Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv1421Bytes());
    }

    public byte[] getLdbv1421Bytes() {
        byte[] buffer = new byte[Len.LDBV1421];
        return getLdbv1421Bytes(buffer, 1);
    }

    public void setLdbv1421Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        ramoBila1 = MarshalByte.readString(buffer, position, Len.RAMO_BILA1);
        position += Len.RAMO_BILA1;
        ramoBila2 = MarshalByte.readString(buffer, position, Len.RAMO_BILA2);
        position += Len.RAMO_BILA2;
        ramoBila3 = MarshalByte.readString(buffer, position, Len.RAMO_BILA3);
        position += Len.RAMO_BILA3;
        ramoBila4 = MarshalByte.readString(buffer, position, Len.RAMO_BILA4);
        position += Len.RAMO_BILA4;
        ramoBila5 = MarshalByte.readString(buffer, position, Len.RAMO_BILA5);
    }

    public byte[] getLdbv1421Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeString(buffer, position, ramoBila1, Len.RAMO_BILA1);
        position += Len.RAMO_BILA1;
        MarshalByte.writeString(buffer, position, ramoBila2, Len.RAMO_BILA2);
        position += Len.RAMO_BILA2;
        MarshalByte.writeString(buffer, position, ramoBila3, Len.RAMO_BILA3);
        position += Len.RAMO_BILA3;
        MarshalByte.writeString(buffer, position, ramoBila4, Len.RAMO_BILA4);
        position += Len.RAMO_BILA4;
        MarshalByte.writeString(buffer, position, ramoBila5, Len.RAMO_BILA5);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setRamoBila1(String ramoBila1) {
        this.ramoBila1 = Functions.subString(ramoBila1, Len.RAMO_BILA1);
    }

    public String getRamoBila1() {
        return this.ramoBila1;
    }

    public void setRamoBila2(String ramoBila2) {
        this.ramoBila2 = Functions.subString(ramoBila2, Len.RAMO_BILA2);
    }

    public String getRamoBila2() {
        return this.ramoBila2;
    }

    public void setRamoBila3(String ramoBila3) {
        this.ramoBila3 = Functions.subString(ramoBila3, Len.RAMO_BILA3);
    }

    public String getRamoBila3() {
        return this.ramoBila3;
    }

    public void setRamoBila4(String ramoBila4) {
        this.ramoBila4 = Functions.subString(ramoBila4, Len.RAMO_BILA4);
    }

    public String getRamoBila4() {
        return this.ramoBila4;
    }

    public void setRamoBila5(String ramoBila5) {
        this.ramoBila5 = Functions.subString(ramoBila5, Len.RAMO_BILA5);
    }

    public String getRamoBila5() {
        return this.ramoBila5;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RAMO_BILA1 = 12;
        public static final int RAMO_BILA2 = 12;
        public static final int RAMO_BILA3 = 12;
        public static final int RAMO_BILA4 = 12;
        public static final int RAMO_BILA5 = 12;
        public static final int ID_POLI = 5;
        public static final int LDBV1421 = ID_POLI + RAMO_BILA1 + RAMO_BILA2 + RAMO_BILA3 + RAMO_BILA4 + RAMO_BILA5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
