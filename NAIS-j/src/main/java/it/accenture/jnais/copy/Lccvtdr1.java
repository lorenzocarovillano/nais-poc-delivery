package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVTDR1<br>
 * Variable: LCCVTDR1 from copybook LCCVTDR1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvtdr1 {

    //==== PROPERTIES ====
    /**Original name: WTDR-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA TIT_RAT
	 *    ALIAS TDR
	 *    ULTIMO AGG. 05 DIC 2014
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WTDR-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WTDR-DATI
    private WtdrDati dati = new WtdrDati();

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WtdrDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }
}
