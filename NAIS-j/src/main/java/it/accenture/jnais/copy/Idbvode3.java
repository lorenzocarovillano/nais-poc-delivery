package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVODE3<br>
 * Copybook: IDBVODE3 from copybook IDBVODE3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvode3 {

    //==== PROPERTIES ====
    //Original name: ODE-DT-INI-EFF-DB
    private String odeDtIniEffDb = DefaultValues.stringVal(Len.ODE_DT_INI_EFF_DB);
    //Original name: ODE-DT-END-EFF-DB
    private String odeDtEndEffDb = DefaultValues.stringVal(Len.ODE_DT_END_EFF_DB);

    //==== METHODS ====
    public void setOdeDtIniEffDb(String odeDtIniEffDb) {
        this.odeDtIniEffDb = Functions.subString(odeDtIniEffDb, Len.ODE_DT_INI_EFF_DB);
    }

    public String getOdeDtIniEffDb() {
        return this.odeDtIniEffDb;
    }

    public void setOdeDtEndEffDb(String odeDtEndEffDb) {
        this.odeDtEndEffDb = Functions.subString(odeDtEndEffDb, Len.ODE_DT_END_EFF_DB);
    }

    public String getOdeDtEndEffDb() {
        return this.odeDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ODE_DT_INI_EFF_DB = 10;
        public static final int ODE_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
