package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;

/**Original name: C214-TAB-OUTPUT<br>
 * Variable: C214-TAB-OUTPUT from copybook IVVC0213<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class C214TabOutput {

    //==== PROPERTIES ====
    //Original name: C214-COD-VARIABILE-O
    private String codVariabileO = DefaultValues.stringVal(Len.COD_VARIABILE_O);
    //Original name: C214-TP-DATO-O
    private char tpDatoO = DefaultValues.CHAR_VAL;
    //Original name: C214-VAL-IMP-O
    private AfDecimal valImpO = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);
    //Original name: C214-VAL-PERC-O
    private AfDecimal valPercO = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: C214-VAL-STR-O
    private String valStrO = DefaultValues.stringVal(Len.VAL_STR_O);

    //==== METHODS ====
    public void setTabOutputBytes(byte[] buffer) {
        setTabOutputBytes(buffer, 1);
    }

    public void setTabOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        codVariabileO = MarshalByte.readString(buffer, position, Len.COD_VARIABILE_O);
        position += Len.COD_VARIABILE_O;
        tpDatoO = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        valImpO.assign(MarshalByte.readDecimal(buffer, position, Len.Int.VAL_IMP_O, Len.Fract.VAL_IMP_O));
        position += Len.VAL_IMP_O;
        valPercO.assign(MarshalByte.readDecimal(buffer, position, Len.Int.VAL_PERC_O, Len.Fract.VAL_PERC_O, SignType.NO_SIGN));
        position += Len.VAL_PERC_O;
        valStrO = MarshalByte.readString(buffer, position, Len.VAL_STR_O);
    }

    public byte[] getTabOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codVariabileO, Len.COD_VARIABILE_O);
        position += Len.COD_VARIABILE_O;
        MarshalByte.writeChar(buffer, position, tpDatoO);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimal(buffer, position, valImpO.copy());
        position += Len.VAL_IMP_O;
        MarshalByte.writeDecimal(buffer, position, valPercO.copy(), SignType.NO_SIGN);
        position += Len.VAL_PERC_O;
        MarshalByte.writeString(buffer, position, valStrO, Len.VAL_STR_O);
        return buffer;
    }

    public void setCodVariabileO(String codVariabileO) {
        this.codVariabileO = Functions.subString(codVariabileO, Len.COD_VARIABILE_O);
    }

    public String getCodVariabileO() {
        return this.codVariabileO;
    }

    public void setTpDatoO(char tpDatoO) {
        this.tpDatoO = tpDatoO;
    }

    public void setIvvc0213TpDatoOFormatted(String ivvc0213TpDatoO) {
        setTpDatoO(Functions.charAt(ivvc0213TpDatoO, Types.CHAR_SIZE));
    }

    public char getTpDatoO() {
        return this.tpDatoO;
    }

    public void setValImpO(AfDecimal valImpO) {
        this.valImpO.assign(valImpO);
    }

    public void setIvvc0213ValImpOFromBuffer(byte[] buffer) {
        valImpO.assign(MarshalByte.readDecimal(buffer, 1, Len.Int.VAL_IMP_O, Len.Fract.VAL_IMP_O));
    }

    public AfDecimal getValImpO() {
        return this.valImpO.copy();
    }

    public String getIvvc0213ValImpOFormatted() {
        return PicFormatter.display("S9(11)V9(7)").format(getValImpO()).toString();
    }

    public void setValPercO(AfDecimal valPercO) {
        this.valPercO.assign(valPercO);
    }

    public AfDecimal getValPercO() {
        return this.valPercO.copy();
    }

    public void setValStrO(String valStrO) {
        this.valStrO = Functions.subString(valStrO, Len.VAL_STR_O);
    }

    public String getValStrO() {
        return this.valStrO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_VARIABILE_O = 12;
        public static final int TP_DATO_O = 1;
        public static final int VAL_IMP_O = 18;
        public static final int VAL_PERC_O = 14;
        public static final int VAL_STR_O = 12;
        public static final int TAB_OUTPUT = COD_VARIABILE_O + TP_DATO_O + VAL_IMP_O + VAL_PERC_O + VAL_STR_O;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAL_IMP_O = 11;
            public static final int VAL_PERC_O = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAL_IMP_O = 7;
            public static final int VAL_PERC_O = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
