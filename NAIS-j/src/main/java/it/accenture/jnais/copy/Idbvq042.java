package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDBVQ042<br>
 * Copybook: IDBVQ042 from copybook IDBVQ042<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvq042 {

    //==== PROPERTIES ====
    //Original name: IND-Q04-COD-CAN
    private short q04CodCan = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-Q04-OBBL-RISP
    private short q04ObblRisp = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setQ04CodCan(short q04CodCan) {
        this.q04CodCan = q04CodCan;
    }

    public short getQ04CodCan() {
        return this.q04CodCan;
    }

    public void setQ04ObblRisp(short q04ObblRisp) {
        this.q04ObblRisp = q04ObblRisp;
    }

    public short getQ04ObblRisp() {
        return this.q04ObblRisp;
    }
}
