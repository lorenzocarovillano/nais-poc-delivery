package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-BTC-JOB-EXECUTION<br>
 * Variable: IND-BTC-JOB-EXECUTION from copybook IDBVBJE2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndBtcJobExecution {

    //==== PROPERTIES ====
    //Original name: IND-BJE-DATA
    private short data2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BJE-FLAG-WARNINGS
    private short flagWarnings = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BJE-DT-END
    private short dtEnd = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setData2(short data2) {
        this.data2 = data2;
    }

    public short getData2() {
        return this.data2;
    }

    public void setFlagWarnings(short flagWarnings) {
        this.flagWarnings = flagWarnings;
    }

    public short getFlagWarnings() {
        return this.flagWarnings;
    }

    public void setDtEnd(short dtEnd) {
        this.dtEnd = dtEnd;
    }

    public short getDtEnd() {
        return this.dtEnd;
    }
}
