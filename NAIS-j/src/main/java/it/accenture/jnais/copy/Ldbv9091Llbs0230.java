package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV9091<br>
 * Variable: LDBV9091 from copybook LDBV9091<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv9091Llbs0230 {

    //==== PROPERTIES ====
    //Original name: LDBV9091-DATI-INPUT
    private Ldbv9091DatiInput ldbv9091DatiInput = new Ldbv9091DatiInput();
    //Original name: LDBV9091-GAR-OUTPUT
    private Ldbv3401GarOutput ldbv9091GarOutput = new Ldbv3401GarOutput();
    //Original name: L9091-TP-STAT-BUS
    private String l9091TpStatBus = DefaultValues.stringVal(Len.L9091_TP_STAT_BUS);
    //Original name: L9091-TP-CAUS
    private String l9091TpCaus = DefaultValues.stringVal(Len.L9091_TP_CAUS);

    //==== METHODS ====
    public void setLdbv9091Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV9091];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV9091);
        setLdbv9091Bytes(buffer, 1);
    }

    public String getLdbv9091Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv9091Bytes());
    }

    public byte[] getLdbv9091Bytes() {
        byte[] buffer = new byte[Len.LDBV9091];
        return getLdbv9091Bytes(buffer, 1);
    }

    public void setLdbv9091Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv9091DatiInput.setLdbv9091DatiInputBytes(buffer, position);
        position += Ldbv9091DatiInput.Len.LDBV9091_DATI_INPUT;
        ldbv9091GarOutput.setLdbv3401GarOutputBytes(buffer, position);
        position += Ldbv3401GarOutput.Len.LDBV3401_GAR_OUTPUT;
        setLdbv9091StbOutputBytes(buffer, position);
    }

    public byte[] getLdbv9091Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv9091DatiInput.getLdbv9091DatiInputBytes(buffer, position);
        position += Ldbv9091DatiInput.Len.LDBV9091_DATI_INPUT;
        ldbv9091GarOutput.getLdbv3401GarOutputBytes(buffer, position);
        position += Ldbv3401GarOutput.Len.LDBV3401_GAR_OUTPUT;
        getLdbv9091StbOutputBytes(buffer, position);
        return buffer;
    }

    public void setLdbv9091StbOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        l9091TpStatBus = MarshalByte.readString(buffer, position, Len.L9091_TP_STAT_BUS);
        position += Len.L9091_TP_STAT_BUS;
        l9091TpCaus = MarshalByte.readString(buffer, position, Len.L9091_TP_CAUS);
    }

    public byte[] getLdbv9091StbOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, l9091TpStatBus, Len.L9091_TP_STAT_BUS);
        position += Len.L9091_TP_STAT_BUS;
        MarshalByte.writeString(buffer, position, l9091TpCaus, Len.L9091_TP_CAUS);
        return buffer;
    }

    public void setL9091TpStatBus(String l9091TpStatBus) {
        this.l9091TpStatBus = Functions.subString(l9091TpStatBus, Len.L9091_TP_STAT_BUS);
    }

    public String getL9091TpStatBus() {
        return this.l9091TpStatBus;
    }

    public void setL9091TpCaus(String l9091TpCaus) {
        this.l9091TpCaus = Functions.subString(l9091TpCaus, Len.L9091_TP_CAUS);
    }

    public String getL9091TpCaus() {
        return this.l9091TpCaus;
    }

    public Ldbv9091DatiInput getLdbv9091DatiInput() {
        return ldbv9091DatiInput;
    }

    public Ldbv3401GarOutput getLdbv9091GarOutput() {
        return ldbv9091GarOutput;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L9091_TP_STAT_BUS = 2;
        public static final int L9091_TP_CAUS = 2;
        public static final int LDBV9091_STB_OUTPUT = L9091_TP_STAT_BUS + L9091_TP_CAUS;
        public static final int LDBV9091 = Ldbv9091DatiInput.Len.LDBV9091_DATI_INPUT + Ldbv3401GarOutput.Len.LDBV3401_GAR_OUTPUT + LDBV9091_STB_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
