package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVSDI3<br>
 * Copybook: IDBVSDI3 from copybook IDBVSDI3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvsdi3 {

    //==== PROPERTIES ====
    //Original name: SDI-DT-INI-EFF-DB
    private String sdiDtIniEffDb = DefaultValues.stringVal(Len.SDI_DT_INI_EFF_DB);
    //Original name: SDI-DT-END-EFF-DB
    private String sdiDtEndEffDb = DefaultValues.stringVal(Len.SDI_DT_END_EFF_DB);

    //==== METHODS ====
    public void setSdiDtIniEffDb(String sdiDtIniEffDb) {
        this.sdiDtIniEffDb = Functions.subString(sdiDtIniEffDb, Len.SDI_DT_INI_EFF_DB);
    }

    public String getSdiDtIniEffDb() {
        return this.sdiDtIniEffDb;
    }

    public void setSdiDtEndEffDb(String sdiDtEndEffDb) {
        this.sdiDtEndEffDb = Functions.subString(sdiDtEndEffDb, Len.SDI_DT_END_EFF_DB);
    }

    public String getSdiDtEndEffDb() {
        return this.sdiDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int SDI_DT_INI_EFF_DB = 10;
        public static final int SDI_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
