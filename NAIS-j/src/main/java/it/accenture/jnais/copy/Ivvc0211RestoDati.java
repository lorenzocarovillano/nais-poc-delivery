package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Ivvc0211FlAreaVarExtra;
import it.accenture.jnais.ws.enums.Ivvc0211ReturnCode;
import it.accenture.jnais.ws.occurs.Ivvc0211Addresses;
import it.accenture.jnais.ws.occurs.Ivvc0211TabCalcoloKo;
import it.accenture.jnais.ws.occurs.Ivvc0211TabVarNotFound;

/**Original name: IVVC0211-RESTO-DATI<br>
 * Variable: IVVC0211-RESTO-DATI from copybook IVVC0211<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0211RestoDati {

    //==== PROPERTIES ====
    public static final int TAB_VAR_NOT_FOUND_MAXOCCURS = 5;
    public static final int TAB_CALCOLO_KO_MAXOCCURS = 5;
    public static final int ADDRESSES_MAXOCCURS = 5;
    /**Original name: IVVC0211-BUFFER-DATI<br>
	 * <pre>       05 (SF)-BUFFER-DATI                   PIC  X(2000000).
	 *        05 (SF)-BUFFER-DATI                   PIC  X(1700000).</pre>*/
    private String bufferDati = DefaultValues.stringVal(Len.BUFFER_DATI);
    /**Original name: IVVC0211-ID-TAB-TEMP<br>
	 * <pre>--     ID TABELLA TEMPORANEA ALTERNATIVA AL BUFFER DATI</pre>*/
    private int idTabTemp = DefaultValues.INT_VAL;
    /**Original name: IVVC0211-RETURN-CODE<br>
	 * <pre>--    return code</pre>*/
    private Ivvc0211ReturnCode returnCode = new Ivvc0211ReturnCode();
    //Original name: IVVC0211-CAMPI-ESITO
    private Ivvc0211CampiEsito campiEsito = new Ivvc0211CampiEsito();
    //Original name: IVVC0211-ELE-MAX-NOT-FOUND
    private short eleMaxNotFound = DefaultValues.SHORT_VAL;
    //Original name: IVVC0211-TAB-VAR-NOT-FOUND
    private Ivvc0211TabVarNotFound[] tabVarNotFound = new Ivvc0211TabVarNotFound[TAB_VAR_NOT_FOUND_MAXOCCURS];
    //Original name: IVVC0211-ELE-MAX-CALC-KO
    private short eleMaxCalcKo = DefaultValues.SHORT_VAL;
    //Original name: IVVC0211-TAB-CALCOLO-KO
    private Ivvc0211TabCalcoloKo[] tabCalcoloKo = new Ivvc0211TabCalcoloKo[TAB_CALCOLO_KO_MAXOCCURS];
    //Original name: IVVC0211-ADDRESSES
    private Ivvc0211Addresses[] addresses = new Ivvc0211Addresses[ADDRESSES_MAXOCCURS];
    //Original name: IVVC0211-CODICE-INIZIATIVA
    private String codiceIniziativa = DefaultValues.stringVal(Len.CODICE_INIZIATIVA);
    //Original name: IVVC0211-CODICE-TRATTATO
    private String codiceTrattato = DefaultValues.stringVal(Len.CODICE_TRATTATO);
    //Original name: IVVC0211-FL-AREA-VAR-EXTRA
    private Ivvc0211FlAreaVarExtra flAreaVarExtra = new Ivvc0211FlAreaVarExtra();
    /**Original name: FILLER-IVVC0211-RESTO-DATI<br>
	 * <pre>      05 FILLER                              PIC X(071).</pre>*/
    private String flr1 = DefaultValues.stringVal(Len.FLR1);

    //==== CONSTRUCTORS ====
    public Ivvc0211RestoDati() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabVarNotFoundIdx = 1; tabVarNotFoundIdx <= TAB_VAR_NOT_FOUND_MAXOCCURS; tabVarNotFoundIdx++) {
            tabVarNotFound[tabVarNotFoundIdx - 1] = new Ivvc0211TabVarNotFound();
        }
        for (int tabCalcoloKoIdx = 1; tabCalcoloKoIdx <= TAB_CALCOLO_KO_MAXOCCURS; tabCalcoloKoIdx++) {
            tabCalcoloKo[tabCalcoloKoIdx - 1] = new Ivvc0211TabCalcoloKo();
        }
        for (int addressesIdx = 1; addressesIdx <= ADDRESSES_MAXOCCURS; addressesIdx++) {
            addresses[addressesIdx - 1] = new Ivvc0211Addresses();
        }
    }

    public void setIvvc0211RestoDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        bufferDati = MarshalByte.readString(buffer, position, Len.BUFFER_DATI);
        position += Len.BUFFER_DATI;
        idTabTemp = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_TAB_TEMP, 0);
        position += Len.ID_TAB_TEMP;
        returnCode.setReturnCode(MarshalByte.readString(buffer, position, Ivvc0211ReturnCode.Len.RETURN_CODE));
        position += Ivvc0211ReturnCode.Len.RETURN_CODE;
        campiEsito.setCampiEsitoBytes(buffer, position);
        position += Ivvc0211CampiEsito.Len.CAMPI_ESITO;
        setAreaVarKoBytes(buffer, position);
        position += Len.AREA_VAR_KO;
        setAreaAddressesBytes(buffer, position);
        position += Len.AREA_ADDRESSES;
        codiceIniziativa = MarshalByte.readString(buffer, position, Len.CODICE_INIZIATIVA);
        position += Len.CODICE_INIZIATIVA;
        codiceTrattato = MarshalByte.readString(buffer, position, Len.CODICE_TRATTATO);
        position += Len.CODICE_TRATTATO;
        flAreaVarExtra.setFlAreaVarExtra(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
    }

    public byte[] getIvvc0211RestoDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, bufferDati, Len.BUFFER_DATI);
        position += Len.BUFFER_DATI;
        MarshalByte.writeIntAsPacked(buffer, position, idTabTemp, Len.Int.ID_TAB_TEMP, 0);
        position += Len.ID_TAB_TEMP;
        MarshalByte.writeString(buffer, position, returnCode.getReturnCode(), Ivvc0211ReturnCode.Len.RETURN_CODE);
        position += Ivvc0211ReturnCode.Len.RETURN_CODE;
        campiEsito.getCampiEsitoBytes(buffer, position);
        position += Ivvc0211CampiEsito.Len.CAMPI_ESITO;
        getAreaVarKoBytes(buffer, position);
        position += Len.AREA_VAR_KO;
        getAreaAddressesBytes(buffer, position);
        position += Len.AREA_ADDRESSES;
        MarshalByte.writeString(buffer, position, codiceIniziativa, Len.CODICE_INIZIATIVA);
        position += Len.CODICE_INIZIATIVA;
        MarshalByte.writeString(buffer, position, codiceTrattato, Len.CODICE_TRATTATO);
        position += Len.CODICE_TRATTATO;
        MarshalByte.writeChar(buffer, position, flAreaVarExtra.getFlAreaVarExtra());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        return buffer;
    }

    public void setBufferDati(String bufferDati) {
        this.bufferDati = Functions.subString(bufferDati, Len.BUFFER_DATI);
    }

    public void setS211BufferDatiSubstring(String replacement, int start, int length) {
        bufferDati = Functions.setSubstring(bufferDati, replacement, start, length);
    }

    public String getBufferDati() {
        return this.bufferDati;
    }

    public String getBufferDatiFormatted() {
        return Functions.padBlanks(getBufferDati(), Len.BUFFER_DATI);
    }

    public void setIdTabTemp(int idTabTemp) {
        this.idTabTemp = idTabTemp;
    }

    public int getIdTabTemp() {
        return this.idTabTemp;
    }

    public void setAreaVarKoBytes(byte[] buffer) {
        setAreaVarKoBytes(buffer, 1);
    }

    public void setAreaVarKoBytes(byte[] buffer, int offset) {
        int position = offset;
        setNotFoundMvvBytes(buffer, position);
        position += Len.NOT_FOUND_MVV;
        setCalcoloKoBytes(buffer, position);
    }

    public byte[] getAreaVarKoBytes(byte[] buffer, int offset) {
        int position = offset;
        getNotFoundMvvBytes(buffer, position);
        position += Len.NOT_FOUND_MVV;
        getCalcoloKoBytes(buffer, position);
        return buffer;
    }

    public void setNotFoundMvvBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxNotFound = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_MAX_NOT_FOUND, 0);
        position += Len.ELE_MAX_NOT_FOUND;
        setAreaVarNotFoundBytes(buffer, position);
    }

    public byte[] getNotFoundMvvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeShortAsPacked(buffer, position, eleMaxNotFound, Len.Int.ELE_MAX_NOT_FOUND, 0);
        position += Len.ELE_MAX_NOT_FOUND;
        getAreaVarNotFoundBytes(buffer, position);
        return buffer;
    }

    public void setEleMaxNotFound(short eleMaxNotFound) {
        this.eleMaxNotFound = eleMaxNotFound;
    }

    public short getEleMaxNotFound() {
        return this.eleMaxNotFound;
    }

    public String getS211AreaVarNotFoundFormatted() {
        return MarshalByteExt.bufferToStr(getS211AreaVarNotFoundBytes());
    }

    /**Original name: S211-AREA-VAR-NOT-FOUND<br>*/
    public byte[] getS211AreaVarNotFoundBytes() {
        byte[] buffer = new byte[Len.AREA_VAR_NOT_FOUND];
        return getAreaVarNotFoundBytes(buffer, 1);
    }

    public void setAreaVarNotFoundBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_VAR_NOT_FOUND_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabVarNotFound[idx - 1].setTabVarNotFoundBytes(buffer, position);
                position += Ivvc0211TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
            }
            else {
                tabVarNotFound[idx - 1].initTabVarNotFoundSpaces();
                position += Ivvc0211TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
            }
        }
    }

    public byte[] getAreaVarNotFoundBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_VAR_NOT_FOUND_MAXOCCURS; idx++) {
            tabVarNotFound[idx - 1].getTabVarNotFoundBytes(buffer, position);
            position += Ivvc0211TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
        }
        return buffer;
    }

    public void setCalcoloKoBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxCalcKo = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_MAX_CALC_KO, 0);
        position += Len.ELE_MAX_CALC_KO;
        setAreaCalcKoBytes(buffer, position);
    }

    public byte[] getCalcoloKoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeShortAsPacked(buffer, position, eleMaxCalcKo, Len.Int.ELE_MAX_CALC_KO, 0);
        position += Len.ELE_MAX_CALC_KO;
        getAreaCalcKoBytes(buffer, position);
        return buffer;
    }

    public void setEleMaxCalcKo(short eleMaxCalcKo) {
        this.eleMaxCalcKo = eleMaxCalcKo;
    }

    public short getEleMaxCalcKo() {
        return this.eleMaxCalcKo;
    }

    public String getS211AreaCalcKoFormatted() {
        return MarshalByteExt.bufferToStr(getS211AreaCalcKoBytes());
    }

    /**Original name: S211-AREA-CALC-KO<br>*/
    public byte[] getS211AreaCalcKoBytes() {
        byte[] buffer = new byte[Len.AREA_CALC_KO];
        return getAreaCalcKoBytes(buffer, 1);
    }

    public void setAreaCalcKoBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_CALCOLO_KO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabCalcoloKo[idx - 1].setTabCalcoloKoBytes(buffer, position);
                position += Ivvc0211TabCalcoloKo.Len.TAB_CALCOLO_KO;
            }
            else {
                tabCalcoloKo[idx - 1].initTabCalcoloKoSpaces();
                position += Ivvc0211TabCalcoloKo.Len.TAB_CALCOLO_KO;
            }
        }
    }

    public byte[] getAreaCalcKoBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_CALCOLO_KO_MAXOCCURS; idx++) {
            tabCalcoloKo[idx - 1].getTabCalcoloKoBytes(buffer, position);
            position += Ivvc0211TabCalcoloKo.Len.TAB_CALCOLO_KO;
        }
        return buffer;
    }

    public void setS211AreaAddressesBytes(byte[] buffer) {
        setAreaAddressesBytes(buffer, 1);
    }

    /**Original name: IVVC0211-AREA-ADDRESSES<br>*/
    public byte[] getAreaAddressesBytes() {
        byte[] buffer = new byte[Len.AREA_ADDRESSES];
        return getAreaAddressesBytes(buffer, 1);
    }

    public void setAreaAddressesBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ADDRESSES_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                addresses[idx - 1].setAddressesBytes(buffer, position);
                position += Ivvc0211Addresses.Len.ADDRESSES;
            }
            else {
                addresses[idx - 1].initAddressesSpaces();
                position += Ivvc0211Addresses.Len.ADDRESSES;
            }
        }
    }

    public byte[] getAreaAddressesBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ADDRESSES_MAXOCCURS; idx++) {
            addresses[idx - 1].getAddressesBytes(buffer, position);
            position += Ivvc0211Addresses.Len.ADDRESSES;
        }
        return buffer;
    }

    public void setCodiceIniziativa(String codiceIniziativa) {
        this.codiceIniziativa = Functions.subString(codiceIniziativa, Len.CODICE_INIZIATIVA);
    }

    public String getCodiceIniziativa() {
        return this.codiceIniziativa;
    }

    public String getCodiceIniziativaFormatted() {
        return Functions.padBlanks(getCodiceIniziativa(), Len.CODICE_INIZIATIVA);
    }

    public void setCodiceTrattato(String codiceTrattato) {
        this.codiceTrattato = Functions.subString(codiceTrattato, Len.CODICE_TRATTATO);
    }

    public String getCodiceTrattato() {
        return this.codiceTrattato;
    }

    public String getCodiceTrattatoFormatted() {
        return Functions.padBlanks(getCodiceTrattato(), Len.CODICE_TRATTATO);
    }

    public void setFlr1(String flr1) {
        this.flr1 = Functions.subString(flr1, Len.FLR1);
    }

    public String getFlr1() {
        return this.flr1;
    }

    public Ivvc0211Addresses getAddresses(int idx) {
        return addresses[idx - 1];
    }

    public Ivvc0211CampiEsito getCampiEsito() {
        return campiEsito;
    }

    public Ivvc0211FlAreaVarExtra getFlAreaVarExtra() {
        return flAreaVarExtra;
    }

    public Ivvc0211ReturnCode getReturnCode() {
        return returnCode;
    }

    public Ivvc0211TabCalcoloKo getTabCalcoloKo(int idx) {
        return tabCalcoloKo[idx - 1];
    }

    public Ivvc0211TabVarNotFound getTabVarNotFound(int idx) {
        return tabVarNotFound[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BUFFER_DATI = 4500000;
        public static final int ID_TAB_TEMP = 5;
        public static final int ELE_MAX_NOT_FOUND = 3;
        public static final int AREA_VAR_NOT_FOUND = Ivvc0211RestoDati.TAB_VAR_NOT_FOUND_MAXOCCURS * Ivvc0211TabVarNotFound.Len.TAB_VAR_NOT_FOUND;
        public static final int NOT_FOUND_MVV = ELE_MAX_NOT_FOUND + AREA_VAR_NOT_FOUND;
        public static final int ELE_MAX_CALC_KO = 3;
        public static final int AREA_CALC_KO = Ivvc0211RestoDati.TAB_CALCOLO_KO_MAXOCCURS * Ivvc0211TabCalcoloKo.Len.TAB_CALCOLO_KO;
        public static final int CALCOLO_KO = ELE_MAX_CALC_KO + AREA_CALC_KO;
        public static final int AREA_VAR_KO = NOT_FOUND_MVV + CALCOLO_KO;
        public static final int AREA_ADDRESSES = Ivvc0211RestoDati.ADDRESSES_MAXOCCURS * Ivvc0211Addresses.Len.ADDRESSES;
        public static final int CODICE_INIZIATIVA = 12;
        public static final int CODICE_TRATTATO = 12;
        public static final int FLR1 = 70;
        public static final int IVVC0211_RESTO_DATI = BUFFER_DATI + ID_TAB_TEMP + Ivvc0211ReturnCode.Len.RETURN_CODE + Ivvc0211CampiEsito.Len.CAMPI_ESITO + AREA_VAR_KO + AREA_ADDRESSES + CODICE_INIZIATIVA + CODICE_TRATTATO + Ivvc0211FlAreaVarExtra.Len.FL_AREA_VAR_EXTRA + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_TAB_TEMP = 9;
            public static final int ELE_MAX_NOT_FOUND = 4;
            public static final int ELE_MAX_CALC_KO = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
