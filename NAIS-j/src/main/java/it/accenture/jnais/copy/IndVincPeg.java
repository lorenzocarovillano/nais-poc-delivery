package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-VINC-PEG<br>
 * Variable: IND-VINC-PEG from copybook IDBVL232<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndVincPeg {

    //==== PROPERTIES ====
    //Original name: IND-L23-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-TP-VINC
    private short tpVinc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-FL-DELEGA-AL-RISC
    private short flDelegaAlRisc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-DESC
    private short desc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-DT-ATTIV-VINPG
    private short dtAttivVinpg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-CPT-VINCTO-PIGN
    private short cptVinctoPign = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-DT-CHIU-VINPG
    private short dtChiuVinpg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-VAL-RISC-INI-VINPG
    private short valRiscIniVinpg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-VAL-RISC-END-VINPG
    private short valRiscEndVinpg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-SOM-PRE-VINPG
    private short somPreVinpg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-FL-VINPG-INT-PRSTZ
    private short flVinpgIntPrstz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-DESC-AGG-VINC
    private short descAggVinc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-TP-AUT-SEQ
    private short tpAutSeq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-COD-UFF-SEQ
    private short codUffSeq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-NUM-PROVV-SEQ
    private short numProvvSeq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-TP-PROVV-SEQ
    private short tpProvvSeq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-DT-PROVV-SEQ
    private short dtProvvSeq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-DT-NOTIFICA-BLOCCO
    private short dtNotificaBlocco = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L23-NOTA-PROVV
    private short notaProvv = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setTpVinc(short tpVinc) {
        this.tpVinc = tpVinc;
    }

    public short getTpVinc() {
        return this.tpVinc;
    }

    public void setFlDelegaAlRisc(short flDelegaAlRisc) {
        this.flDelegaAlRisc = flDelegaAlRisc;
    }

    public short getFlDelegaAlRisc() {
        return this.flDelegaAlRisc;
    }

    public void setDesc(short desc) {
        this.desc = desc;
    }

    public short getDesc() {
        return this.desc;
    }

    public void setDtAttivVinpg(short dtAttivVinpg) {
        this.dtAttivVinpg = dtAttivVinpg;
    }

    public short getDtAttivVinpg() {
        return this.dtAttivVinpg;
    }

    public void setCptVinctoPign(short cptVinctoPign) {
        this.cptVinctoPign = cptVinctoPign;
    }

    public short getCptVinctoPign() {
        return this.cptVinctoPign;
    }

    public void setDtChiuVinpg(short dtChiuVinpg) {
        this.dtChiuVinpg = dtChiuVinpg;
    }

    public short getDtChiuVinpg() {
        return this.dtChiuVinpg;
    }

    public void setValRiscIniVinpg(short valRiscIniVinpg) {
        this.valRiscIniVinpg = valRiscIniVinpg;
    }

    public short getValRiscIniVinpg() {
        return this.valRiscIniVinpg;
    }

    public void setValRiscEndVinpg(short valRiscEndVinpg) {
        this.valRiscEndVinpg = valRiscEndVinpg;
    }

    public short getValRiscEndVinpg() {
        return this.valRiscEndVinpg;
    }

    public void setSomPreVinpg(short somPreVinpg) {
        this.somPreVinpg = somPreVinpg;
    }

    public short getSomPreVinpg() {
        return this.somPreVinpg;
    }

    public void setFlVinpgIntPrstz(short flVinpgIntPrstz) {
        this.flVinpgIntPrstz = flVinpgIntPrstz;
    }

    public short getFlVinpgIntPrstz() {
        return this.flVinpgIntPrstz;
    }

    public void setDescAggVinc(short descAggVinc) {
        this.descAggVinc = descAggVinc;
    }

    public short getDescAggVinc() {
        return this.descAggVinc;
    }

    public void setTpAutSeq(short tpAutSeq) {
        this.tpAutSeq = tpAutSeq;
    }

    public short getTpAutSeq() {
        return this.tpAutSeq;
    }

    public void setCodUffSeq(short codUffSeq) {
        this.codUffSeq = codUffSeq;
    }

    public short getCodUffSeq() {
        return this.codUffSeq;
    }

    public void setNumProvvSeq(short numProvvSeq) {
        this.numProvvSeq = numProvvSeq;
    }

    public short getNumProvvSeq() {
        return this.numProvvSeq;
    }

    public void setTpProvvSeq(short tpProvvSeq) {
        this.tpProvvSeq = tpProvvSeq;
    }

    public short getTpProvvSeq() {
        return this.tpProvvSeq;
    }

    public void setDtProvvSeq(short dtProvvSeq) {
        this.dtProvvSeq = dtProvvSeq;
    }

    public short getDtProvvSeq() {
        return this.dtProvvSeq;
    }

    public void setDtNotificaBlocco(short dtNotificaBlocco) {
        this.dtNotificaBlocco = dtNotificaBlocco;
    }

    public short getDtNotificaBlocco() {
        return this.dtNotificaBlocco;
    }

    public void setNotaProvv(short notaProvv) {
        this.notaProvv = notaProvv;
    }

    public short getNotaProvv() {
        return this.notaProvv;
    }
}
