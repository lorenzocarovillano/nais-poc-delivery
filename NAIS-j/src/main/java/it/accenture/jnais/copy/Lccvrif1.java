package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVRIF1<br>
 * Variable: LCCVRIF1 from copybook LCCVRIF1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvrif1 implements ICopyable<Lccvrif1> {

    //==== PROPERTIES ====
    /**Original name: WRIF-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA RICH_INVST_FND
	 *    ALIAS RIF
	 *    ULTIMO AGG. 25 NOV 2019
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WRIF-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WRIF-DATI
    private WrifDati dati = new WrifDati();

    //==== CONSTRUCTORS ====
    public Lccvrif1() {
    }

    public Lccvrif1(Lccvrif1 lccvrif1) {
        this();
        this.status.setStatus(lccvrif1.status.getStatus());
        this.idPtf = lccvrif1.idPtf;
        this.dati = lccvrif1.dati.copy();
    }

    //==== METHODS ====
    public void initLccvrif1Spaces() {
        status.setStatus(Types.SPACE_CHAR);
        idPtf = Types.INVALID_INT_VAL;
        dati.initDatiSpaces();
    }

    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WrifDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    public Lccvrif1 copy() {
        return new Lccvrif1(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
