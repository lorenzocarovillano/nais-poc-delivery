package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.enums.WcomTpOggBlocco;
import it.accenture.jnais.ws.enums.WcomVerificaBlocco;

/**Original name: WCOM-DATI-BLOCCO<br>
 * Variable: WCOM-DATI-BLOCCO from copybook LCCC0261<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WcomDatiBlocco {

    //==== PROPERTIES ====
    /**Original name: WCOM-VERIFICA-BLOCCO<br>
	 * <pre> --  ID BLOCCO</pre>*/
    private WcomVerificaBlocco verificaBlocco = new WcomVerificaBlocco();
    //Original name: WCOM-ID-BLOCCO-CRZ
    private int idBloccoCrz = DefaultValues.INT_VAL;
    //Original name: WCOM-TP-OGG-BLOCCO
    private WcomTpOggBlocco tpOggBlocco = new WcomTpOggBlocco();

    //==== METHODS ====
    public void setDatiBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        verificaBlocco.setVerificaBlocco(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        idBloccoCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_BLOCCO_CRZ, 0);
        position += Len.ID_BLOCCO_CRZ;
        tpOggBlocco.setTpOggBlocco(MarshalByte.readString(buffer, position, WcomTpOggBlocco.Len.TP_OGG_BLOCCO));
    }

    public byte[] getDatiBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, verificaBlocco.getVerificaBlocco());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, idBloccoCrz, Len.Int.ID_BLOCCO_CRZ, 0);
        position += Len.ID_BLOCCO_CRZ;
        MarshalByte.writeString(buffer, position, tpOggBlocco.getTpOggBlocco(), WcomTpOggBlocco.Len.TP_OGG_BLOCCO);
        return buffer;
    }

    public void setIdBloccoCrz(int idBloccoCrz) {
        this.idBloccoCrz = idBloccoCrz;
    }

    public int getIdBloccoCrz() {
        return this.idBloccoCrz;
    }

    public WcomTpOggBlocco getTpOggBlocco() {
        return tpOggBlocco;
    }

    public WcomVerificaBlocco getVerificaBlocco() {
        return verificaBlocco;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_BLOCCO_CRZ = 5;
        public static final int DATI_BLOCCO = WcomVerificaBlocco.Len.VERIFICA_BLOCCO + ID_BLOCCO_CRZ + WcomTpOggBlocco.Len.TP_OGG_BLOCCO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_BLOCCO_CRZ = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
