package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVTLI1<br>
 * Variable: LCCVTLI1 from copybook LCCVTLI1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvtli1 implements ICopyable<Lccvtli1> {

    //==== PROPERTIES ====
    /**Original name: WTLI-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA TRCH_LIQ
	 *    ALIAS TLI
	 *    ULTIMO AGG. 09 LUG 2009
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WTLI-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WTLI-DATI
    private WtliDati dati = new WtliDati();

    //==== CONSTRUCTORS ====
    public Lccvtli1() {
    }

    public Lccvtli1(Lccvtli1 lccvtli1) {
        this();
        this.status.setStatus(lccvtli1.status.getStatus());
        this.idPtf = lccvtli1.idPtf;
        this.dati = lccvtli1.dati.copy();
    }

    //==== METHODS ====
    public void initLccvtli1Spaces() {
        status.setStatus(Types.SPACE_CHAR);
        idPtf = Types.INVALID_INT_VAL;
        dati.initDatiSpaces();
    }

    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WtliDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    public Lccvtli1 copy() {
        return new Lccvtli1(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
