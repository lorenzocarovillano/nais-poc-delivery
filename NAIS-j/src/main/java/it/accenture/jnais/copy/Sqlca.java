package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.ISqlCa;
import com.bphx.ctu.af.util.Functions;

/**Original name: SQLCA<br>
 * Variable: SQLCA from copybook SQLCA<br>
 * Generated as a class for rule SQLCA.<br>*/
public class Sqlca implements ISqlCa {

    //==== PROPERTIES ====
    public static final int SQLERRD_MAXOCCURS = 6;
    //Original name: SQLCAID
    private String sqlcaid = DefaultValues.stringVal(Len.SQLCAID);
    //Original name: SQLCODE
    private int sqlcode = DefaultValues.BIN_INT_VAL;
    //Original name: SQLERRML
    private short sqlerrml = DefaultValues.BIN_SHORT_VAL;
    //Original name: SQLERRMC
    private String sqlerrmc = DefaultValues.stringVal(Len.SQLERRMC);
    //Original name: SQLERRP
    private String sqlerrp = DefaultValues.stringVal(Len.SQLERRP);
    //Original name: SQLERRD
    private int[] sqlerrd = new int[SQLERRD_MAXOCCURS];
    //Original name: SQLWARN0
    private char sqlwarn0 = DefaultValues.CHAR_VAL;
    //Original name: SQLWARN1
    private char sqlwarn1 = DefaultValues.CHAR_VAL;
    //Original name: SQLWARN2
    private char sqlwarn2 = DefaultValues.CHAR_VAL;
    //Original name: SQLWARN3
    private char sqlwarn3 = DefaultValues.CHAR_VAL;
    //Original name: SQLWARN4
    private char sqlwarn4 = DefaultValues.CHAR_VAL;
    //Original name: SQLWARN5
    private char sqlwarn5 = DefaultValues.CHAR_VAL;
    //Original name: SQLWARN6
    private char sqlwarn6 = DefaultValues.CHAR_VAL;
    //Original name: SQLWARN7
    private char sqlwarn7 = DefaultValues.CHAR_VAL;
    //Original name: SQLWARN8
    private char sqlwarn8 = DefaultValues.CHAR_VAL;
    //Original name: SQLWARN9
    private char sqlwarn9 = DefaultValues.CHAR_VAL;
    //Original name: SQLWARNA
    private char sqlwarna = DefaultValues.CHAR_VAL;
    //Original name: SQLSTATE
    private String sqlstate = DefaultValues.stringVal(Len.SQLSTATE);

    //==== CONSTRUCTORS ====
    public Sqlca() {
        init();
    }

    //==== METHODS ====
    public void updateSqlcaid(String sqlCaid) {
        sqlcaid = sqlCaid;
    }

    public void updateSqlcode(int sqlCode) {
        sqlcode = sqlCode;
    }

    public void updateSqlerrml(short sqlErrml) {
        sqlerrml = sqlErrml;
    }

    public void updateSqlerrmc(String sqlErrmc) {
        sqlerrmc = sqlErrmc;
    }

    public void updateSqlerrp(String sqlErrp) {
        sqlerrp = sqlErrp;
    }

    public void updateSqlerrd(int index, int value) {
        setSqlerrd(index, value);
    }

    public void updateSqlwarn(int index, char value) {
        switch (index) {

            case 1:sqlwarn0 = value;
                break;

            case 2:sqlwarn1 = value;
                break;

            case 3:sqlwarn2 = value;
                break;

            case 4:sqlwarn3 = value;
                break;

            case 5:sqlwarn4 = value;
                break;

            case 6:sqlwarn5 = value;
                break;

            case 7:sqlwarn6 = value;
                break;

            case 8:sqlwarn7 = value;
                break;

            case 9:sqlwarn8 = value;
                break;

            case 10:sqlwarn9 = value;
                break;

            case 11:sqlwarna = value;
                break;

            default:break;
        }
    }

    public void updateSqlstate(String sqlState) {
        sqlstate = sqlState;
    }

    public void init() {
        for (int sqlerrdIdx = 1; sqlerrdIdx <= SQLERRD_MAXOCCURS; sqlerrdIdx++) {
            setSqlerrd(sqlerrdIdx, DefaultValues.BIN_INT_VAL);
        }
    }

    public void setSqlcaid(String sqlcaid) {
        this.sqlcaid = Functions.subString(sqlcaid, Len.SQLCAID);
    }

    public String getSqlcaid() {
        return this.sqlcaid;
    }

    public void setSqlcode(int sqlcode) {
        this.sqlcode = sqlcode;
    }

    public int getSqlcode() {
        return this.sqlcode;
    }

    public void setSqlerrml(short sqlerrml) {
        this.sqlerrml = sqlerrml;
    }

    public short getSqlerrml() {
        return this.sqlerrml;
    }

    public void setSqlerrmc(String sqlerrmc) {
        this.sqlerrmc = Functions.subString(sqlerrmc, Len.SQLERRMC);
    }

    public String getSqlerrmc() {
        return this.sqlerrmc;
    }

    public void setSqlerrp(String sqlerrp) {
        this.sqlerrp = Functions.subString(sqlerrp, Len.SQLERRP);
    }

    public String getSqlerrp() {
        return this.sqlerrp;
    }

    public void setSqlerrd(int sqlerrdIdx, int sqlerrd) {
        this.sqlerrd[sqlerrdIdx - 1] = sqlerrd;
    }

    public int getSqlerrd(int sqlerrdIdx) {
        return this.sqlerrd[sqlerrdIdx - 1];
    }

    public void setSqlwarn0(char sqlwarn0) {
        this.sqlwarn0 = sqlwarn0;
    }

    public char getSqlwarn0() {
        return this.sqlwarn0;
    }

    public void setSqlwarn1(char sqlwarn1) {
        this.sqlwarn1 = sqlwarn1;
    }

    public char getSqlwarn1() {
        return this.sqlwarn1;
    }

    public void setSqlwarn2(char sqlwarn2) {
        this.sqlwarn2 = sqlwarn2;
    }

    public char getSqlwarn2() {
        return this.sqlwarn2;
    }

    public void setSqlwarn3(char sqlwarn3) {
        this.sqlwarn3 = sqlwarn3;
    }

    public char getSqlwarn3() {
        return this.sqlwarn3;
    }

    public void setSqlwarn4(char sqlwarn4) {
        this.sqlwarn4 = sqlwarn4;
    }

    public char getSqlwarn4() {
        return this.sqlwarn4;
    }

    public void setSqlwarn5(char sqlwarn5) {
        this.sqlwarn5 = sqlwarn5;
    }

    public char getSqlwarn5() {
        return this.sqlwarn5;
    }

    public void setSqlwarn6(char sqlwarn6) {
        this.sqlwarn6 = sqlwarn6;
    }

    public char getSqlwarn6() {
        return this.sqlwarn6;
    }

    public void setSqlwarn7(char sqlwarn7) {
        this.sqlwarn7 = sqlwarn7;
    }

    public char getSqlwarn7() {
        return this.sqlwarn7;
    }

    public void setSqlwarn8(char sqlwarn8) {
        this.sqlwarn8 = sqlwarn8;
    }

    public char getSqlwarn8() {
        return this.sqlwarn8;
    }

    public void setSqlwarn9(char sqlwarn9) {
        this.sqlwarn9 = sqlwarn9;
    }

    public char getSqlwarn9() {
        return this.sqlwarn9;
    }

    public void setSqlwarna(char sqlwarna) {
        this.sqlwarna = sqlwarna;
    }

    public char getSqlwarna() {
        return this.sqlwarna;
    }

    public void setSqlstate(String sqlstate) {
        this.sqlstate = Functions.subString(sqlstate, Len.SQLSTATE);
    }

    public String getSqlstate() {
        return this.sqlstate;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int SQLCAID = 8;
        public static final int SQLERRMC = 70;
        public static final int SQLERRP = 8;
        public static final int SQLSTATE = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
