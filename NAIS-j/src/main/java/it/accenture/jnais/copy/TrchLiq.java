package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.TliComponTaxRimb;
import it.accenture.jnais.ws.redefines.TliIasMggSin;
import it.accenture.jnais.ws.redefines.TliIasOnerPrvntFin;
import it.accenture.jnais.ws.redefines.TliIasPnl;
import it.accenture.jnais.ws.redefines.TliIasRstDpst;
import it.accenture.jnais.ws.redefines.TliIdGarLiq;
import it.accenture.jnais.ws.redefines.TliIdMoviChiu;
import it.accenture.jnais.ws.redefines.TliImpLrdDfz;
import it.accenture.jnais.ws.redefines.TliImpNetDfz;
import it.accenture.jnais.ws.redefines.TliImpRimb;
import it.accenture.jnais.ws.redefines.TliImpUti;
import it.accenture.jnais.ws.redefines.TliRisMat;
import it.accenture.jnais.ws.redefines.TliRisSpe;

/**Original name: TRCH-LIQ<br>
 * Variable: TRCH-LIQ from copybook IDBVTLI1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TrchLiq {

    //==== PROPERTIES ====
    //Original name: TLI-ID-TRCH-LIQ
    private int tliIdTrchLiq = DefaultValues.INT_VAL;
    //Original name: TLI-ID-GAR-LIQ
    private TliIdGarLiq tliIdGarLiq = new TliIdGarLiq();
    //Original name: TLI-ID-LIQ
    private int tliIdLiq = DefaultValues.INT_VAL;
    //Original name: TLI-ID-TRCH-DI-GAR
    private int tliIdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: TLI-ID-MOVI-CRZ
    private int tliIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: TLI-ID-MOVI-CHIU
    private TliIdMoviChiu tliIdMoviChiu = new TliIdMoviChiu();
    //Original name: TLI-DT-INI-EFF
    private int tliDtIniEff = DefaultValues.INT_VAL;
    //Original name: TLI-DT-END-EFF
    private int tliDtEndEff = DefaultValues.INT_VAL;
    //Original name: TLI-COD-COMP-ANIA
    private int tliCodCompAnia = DefaultValues.INT_VAL;
    //Original name: TLI-IMP-LRD-CALC
    private AfDecimal tliImpLrdCalc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: TLI-IMP-LRD-DFZ
    private TliImpLrdDfz tliImpLrdDfz = new TliImpLrdDfz();
    //Original name: TLI-IMP-LRD-EFFLQ
    private AfDecimal tliImpLrdEfflq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: TLI-IMP-NET-CALC
    private AfDecimal tliImpNetCalc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: TLI-IMP-NET-DFZ
    private TliImpNetDfz tliImpNetDfz = new TliImpNetDfz();
    //Original name: TLI-IMP-NET-EFFLQ
    private AfDecimal tliImpNetEfflq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: TLI-IMP-UTI
    private TliImpUti tliImpUti = new TliImpUti();
    //Original name: TLI-COD-TARI
    private String tliCodTari = DefaultValues.stringVal(Len.TLI_COD_TARI);
    //Original name: TLI-COD-DVS
    private String tliCodDvs = DefaultValues.stringVal(Len.TLI_COD_DVS);
    //Original name: TLI-IAS-ONER-PRVNT-FIN
    private TliIasOnerPrvntFin tliIasOnerPrvntFin = new TliIasOnerPrvntFin();
    //Original name: TLI-IAS-MGG-SIN
    private TliIasMggSin tliIasMggSin = new TliIasMggSin();
    //Original name: TLI-IAS-RST-DPST
    private TliIasRstDpst tliIasRstDpst = new TliIasRstDpst();
    //Original name: TLI-IMP-RIMB
    private TliImpRimb tliImpRimb = new TliImpRimb();
    //Original name: TLI-COMPON-TAX-RIMB
    private TliComponTaxRimb tliComponTaxRimb = new TliComponTaxRimb();
    //Original name: TLI-RIS-MAT
    private TliRisMat tliRisMat = new TliRisMat();
    //Original name: TLI-RIS-SPE
    private TliRisSpe tliRisSpe = new TliRisSpe();
    //Original name: TLI-DS-RIGA
    private long tliDsRiga = DefaultValues.LONG_VAL;
    //Original name: TLI-DS-OPER-SQL
    private char tliDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: TLI-DS-VER
    private int tliDsVer = DefaultValues.INT_VAL;
    //Original name: TLI-DS-TS-INI-CPTZ
    private long tliDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: TLI-DS-TS-END-CPTZ
    private long tliDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: TLI-DS-UTENTE
    private String tliDsUtente = DefaultValues.stringVal(Len.TLI_DS_UTENTE);
    //Original name: TLI-DS-STATO-ELAB
    private char tliDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: TLI-IAS-PNL
    private TliIasPnl tliIasPnl = new TliIasPnl();

    //==== METHODS ====
    public void setTrchLiqFormatted(String data) {
        byte[] buffer = new byte[Len.TRCH_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.TRCH_LIQ);
        setTrchLiqBytes(buffer, 1);
    }

    public void setTrchLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        tliIdTrchLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TLI_ID_TRCH_LIQ, 0);
        position += Len.TLI_ID_TRCH_LIQ;
        tliIdGarLiq.setTliIdGarLiqFromBuffer(buffer, position);
        position += TliIdGarLiq.Len.TLI_ID_GAR_LIQ;
        tliIdLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TLI_ID_LIQ, 0);
        position += Len.TLI_ID_LIQ;
        tliIdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TLI_ID_TRCH_DI_GAR, 0);
        position += Len.TLI_ID_TRCH_DI_GAR;
        tliIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TLI_ID_MOVI_CRZ, 0);
        position += Len.TLI_ID_MOVI_CRZ;
        tliIdMoviChiu.setTliIdMoviChiuFromBuffer(buffer, position);
        position += TliIdMoviChiu.Len.TLI_ID_MOVI_CHIU;
        tliDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TLI_DT_INI_EFF, 0);
        position += Len.TLI_DT_INI_EFF;
        tliDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TLI_DT_END_EFF, 0);
        position += Len.TLI_DT_END_EFF;
        tliCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TLI_COD_COMP_ANIA, 0);
        position += Len.TLI_COD_COMP_ANIA;
        tliImpLrdCalc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.TLI_IMP_LRD_CALC, Len.Fract.TLI_IMP_LRD_CALC));
        position += Len.TLI_IMP_LRD_CALC;
        tliImpLrdDfz.setTliImpLrdDfzFromBuffer(buffer, position);
        position += TliImpLrdDfz.Len.TLI_IMP_LRD_DFZ;
        tliImpLrdEfflq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.TLI_IMP_LRD_EFFLQ, Len.Fract.TLI_IMP_LRD_EFFLQ));
        position += Len.TLI_IMP_LRD_EFFLQ;
        tliImpNetCalc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.TLI_IMP_NET_CALC, Len.Fract.TLI_IMP_NET_CALC));
        position += Len.TLI_IMP_NET_CALC;
        tliImpNetDfz.setTliImpNetDfzFromBuffer(buffer, position);
        position += TliImpNetDfz.Len.TLI_IMP_NET_DFZ;
        tliImpNetEfflq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.TLI_IMP_NET_EFFLQ, Len.Fract.TLI_IMP_NET_EFFLQ));
        position += Len.TLI_IMP_NET_EFFLQ;
        tliImpUti.setTliImpUtiFromBuffer(buffer, position);
        position += TliImpUti.Len.TLI_IMP_UTI;
        tliCodTari = MarshalByte.readString(buffer, position, Len.TLI_COD_TARI);
        position += Len.TLI_COD_TARI;
        tliCodDvs = MarshalByte.readString(buffer, position, Len.TLI_COD_DVS);
        position += Len.TLI_COD_DVS;
        tliIasOnerPrvntFin.setTliIasOnerPrvntFinFromBuffer(buffer, position);
        position += TliIasOnerPrvntFin.Len.TLI_IAS_ONER_PRVNT_FIN;
        tliIasMggSin.setTliIasMggSinFromBuffer(buffer, position);
        position += TliIasMggSin.Len.TLI_IAS_MGG_SIN;
        tliIasRstDpst.setTliIasRstDpstFromBuffer(buffer, position);
        position += TliIasRstDpst.Len.TLI_IAS_RST_DPST;
        tliImpRimb.setTliImpRimbFromBuffer(buffer, position);
        position += TliImpRimb.Len.TLI_IMP_RIMB;
        tliComponTaxRimb.setTliComponTaxRimbFromBuffer(buffer, position);
        position += TliComponTaxRimb.Len.TLI_COMPON_TAX_RIMB;
        tliRisMat.setTliRisMatFromBuffer(buffer, position);
        position += TliRisMat.Len.TLI_RIS_MAT;
        tliRisSpe.setTliRisSpeFromBuffer(buffer, position);
        position += TliRisSpe.Len.TLI_RIS_SPE;
        tliDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TLI_DS_RIGA, 0);
        position += Len.TLI_DS_RIGA;
        tliDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tliDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TLI_DS_VER, 0);
        position += Len.TLI_DS_VER;
        tliDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TLI_DS_TS_INI_CPTZ, 0);
        position += Len.TLI_DS_TS_INI_CPTZ;
        tliDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TLI_DS_TS_END_CPTZ, 0);
        position += Len.TLI_DS_TS_END_CPTZ;
        tliDsUtente = MarshalByte.readString(buffer, position, Len.TLI_DS_UTENTE);
        position += Len.TLI_DS_UTENTE;
        tliDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tliIasPnl.setTliIasPnlFromBuffer(buffer, position);
    }

    public void setTliIdTrchLiq(int tliIdTrchLiq) {
        this.tliIdTrchLiq = tliIdTrchLiq;
    }

    public int getTliIdTrchLiq() {
        return this.tliIdTrchLiq;
    }

    public void setTliIdLiq(int tliIdLiq) {
        this.tliIdLiq = tliIdLiq;
    }

    public int getTliIdLiq() {
        return this.tliIdLiq;
    }

    public void setTliIdTrchDiGar(int tliIdTrchDiGar) {
        this.tliIdTrchDiGar = tliIdTrchDiGar;
    }

    public int getTliIdTrchDiGar() {
        return this.tliIdTrchDiGar;
    }

    public void setTliIdMoviCrz(int tliIdMoviCrz) {
        this.tliIdMoviCrz = tliIdMoviCrz;
    }

    public int getTliIdMoviCrz() {
        return this.tliIdMoviCrz;
    }

    public void setTliDtIniEff(int tliDtIniEff) {
        this.tliDtIniEff = tliDtIniEff;
    }

    public int getTliDtIniEff() {
        return this.tliDtIniEff;
    }

    public void setTliDtEndEff(int tliDtEndEff) {
        this.tliDtEndEff = tliDtEndEff;
    }

    public int getTliDtEndEff() {
        return this.tliDtEndEff;
    }

    public void setTliCodCompAnia(int tliCodCompAnia) {
        this.tliCodCompAnia = tliCodCompAnia;
    }

    public int getTliCodCompAnia() {
        return this.tliCodCompAnia;
    }

    public void setTliImpLrdCalc(AfDecimal tliImpLrdCalc) {
        this.tliImpLrdCalc.assign(tliImpLrdCalc);
    }

    public AfDecimal getTliImpLrdCalc() {
        return this.tliImpLrdCalc.copy();
    }

    public void setTliImpLrdEfflq(AfDecimal tliImpLrdEfflq) {
        this.tliImpLrdEfflq.assign(tliImpLrdEfflq);
    }

    public AfDecimal getTliImpLrdEfflq() {
        return this.tliImpLrdEfflq.copy();
    }

    public void setTliImpNetCalc(AfDecimal tliImpNetCalc) {
        this.tliImpNetCalc.assign(tliImpNetCalc);
    }

    public AfDecimal getTliImpNetCalc() {
        return this.tliImpNetCalc.copy();
    }

    public void setTliImpNetEfflq(AfDecimal tliImpNetEfflq) {
        this.tliImpNetEfflq.assign(tliImpNetEfflq);
    }

    public AfDecimal getTliImpNetEfflq() {
        return this.tliImpNetEfflq.copy();
    }

    public void setTliCodTari(String tliCodTari) {
        this.tliCodTari = Functions.subString(tliCodTari, Len.TLI_COD_TARI);
    }

    public String getTliCodTari() {
        return this.tliCodTari;
    }

    public String getTliCodTariFormatted() {
        return Functions.padBlanks(getTliCodTari(), Len.TLI_COD_TARI);
    }

    public void setTliCodDvs(String tliCodDvs) {
        this.tliCodDvs = Functions.subString(tliCodDvs, Len.TLI_COD_DVS);
    }

    public String getTliCodDvs() {
        return this.tliCodDvs;
    }

    public String getTliCodDvsFormatted() {
        return Functions.padBlanks(getTliCodDvs(), Len.TLI_COD_DVS);
    }

    public void setTliDsRiga(long tliDsRiga) {
        this.tliDsRiga = tliDsRiga;
    }

    public long getTliDsRiga() {
        return this.tliDsRiga;
    }

    public void setTliDsOperSql(char tliDsOperSql) {
        this.tliDsOperSql = tliDsOperSql;
    }

    public char getTliDsOperSql() {
        return this.tliDsOperSql;
    }

    public void setTliDsVer(int tliDsVer) {
        this.tliDsVer = tliDsVer;
    }

    public int getTliDsVer() {
        return this.tliDsVer;
    }

    public void setTliDsTsIniCptz(long tliDsTsIniCptz) {
        this.tliDsTsIniCptz = tliDsTsIniCptz;
    }

    public long getTliDsTsIniCptz() {
        return this.tliDsTsIniCptz;
    }

    public void setTliDsTsEndCptz(long tliDsTsEndCptz) {
        this.tliDsTsEndCptz = tliDsTsEndCptz;
    }

    public long getTliDsTsEndCptz() {
        return this.tliDsTsEndCptz;
    }

    public void setTliDsUtente(String tliDsUtente) {
        this.tliDsUtente = Functions.subString(tliDsUtente, Len.TLI_DS_UTENTE);
    }

    public String getTliDsUtente() {
        return this.tliDsUtente;
    }

    public void setTliDsStatoElab(char tliDsStatoElab) {
        this.tliDsStatoElab = tliDsStatoElab;
    }

    public char getTliDsStatoElab() {
        return this.tliDsStatoElab;
    }

    public TliComponTaxRimb getTliComponTaxRimb() {
        return tliComponTaxRimb;
    }

    public TliIasMggSin getTliIasMggSin() {
        return tliIasMggSin;
    }

    public TliIasOnerPrvntFin getTliIasOnerPrvntFin() {
        return tliIasOnerPrvntFin;
    }

    public TliIasPnl getTliIasPnl() {
        return tliIasPnl;
    }

    public TliIasRstDpst getTliIasRstDpst() {
        return tliIasRstDpst;
    }

    public TliIdGarLiq getTliIdGarLiq() {
        return tliIdGarLiq;
    }

    public TliIdMoviChiu getTliIdMoviChiu() {
        return tliIdMoviChiu;
    }

    public TliImpLrdDfz getTliImpLrdDfz() {
        return tliImpLrdDfz;
    }

    public TliImpNetDfz getTliImpNetDfz() {
        return tliImpNetDfz;
    }

    public TliImpRimb getTliImpRimb() {
        return tliImpRimb;
    }

    public TliImpUti getTliImpUti() {
        return tliImpUti;
    }

    public TliRisMat getTliRisMat() {
        return tliRisMat;
    }

    public TliRisSpe getTliRisSpe() {
        return tliRisSpe;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_COD_TARI = 12;
        public static final int TLI_COD_DVS = 20;
        public static final int TLI_DS_UTENTE = 20;
        public static final int TLI_ID_TRCH_LIQ = 5;
        public static final int TLI_ID_LIQ = 5;
        public static final int TLI_ID_TRCH_DI_GAR = 5;
        public static final int TLI_ID_MOVI_CRZ = 5;
        public static final int TLI_DT_INI_EFF = 5;
        public static final int TLI_DT_END_EFF = 5;
        public static final int TLI_COD_COMP_ANIA = 3;
        public static final int TLI_IMP_LRD_CALC = 8;
        public static final int TLI_IMP_LRD_EFFLQ = 8;
        public static final int TLI_IMP_NET_CALC = 8;
        public static final int TLI_IMP_NET_EFFLQ = 8;
        public static final int TLI_DS_RIGA = 6;
        public static final int TLI_DS_OPER_SQL = 1;
        public static final int TLI_DS_VER = 5;
        public static final int TLI_DS_TS_INI_CPTZ = 10;
        public static final int TLI_DS_TS_END_CPTZ = 10;
        public static final int TLI_DS_STATO_ELAB = 1;
        public static final int TRCH_LIQ = TLI_ID_TRCH_LIQ + TliIdGarLiq.Len.TLI_ID_GAR_LIQ + TLI_ID_LIQ + TLI_ID_TRCH_DI_GAR + TLI_ID_MOVI_CRZ + TliIdMoviChiu.Len.TLI_ID_MOVI_CHIU + TLI_DT_INI_EFF + TLI_DT_END_EFF + TLI_COD_COMP_ANIA + TLI_IMP_LRD_CALC + TliImpLrdDfz.Len.TLI_IMP_LRD_DFZ + TLI_IMP_LRD_EFFLQ + TLI_IMP_NET_CALC + TliImpNetDfz.Len.TLI_IMP_NET_DFZ + TLI_IMP_NET_EFFLQ + TliImpUti.Len.TLI_IMP_UTI + TLI_COD_TARI + TLI_COD_DVS + TliIasOnerPrvntFin.Len.TLI_IAS_ONER_PRVNT_FIN + TliIasMggSin.Len.TLI_IAS_MGG_SIN + TliIasRstDpst.Len.TLI_IAS_RST_DPST + TliImpRimb.Len.TLI_IMP_RIMB + TliComponTaxRimb.Len.TLI_COMPON_TAX_RIMB + TliRisMat.Len.TLI_RIS_MAT + TliRisSpe.Len.TLI_RIS_SPE + TLI_DS_RIGA + TLI_DS_OPER_SQL + TLI_DS_VER + TLI_DS_TS_INI_CPTZ + TLI_DS_TS_END_CPTZ + TLI_DS_UTENTE + TLI_DS_STATO_ELAB + TliIasPnl.Len.TLI_IAS_PNL;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_ID_TRCH_LIQ = 9;
            public static final int TLI_ID_LIQ = 9;
            public static final int TLI_ID_TRCH_DI_GAR = 9;
            public static final int TLI_ID_MOVI_CRZ = 9;
            public static final int TLI_DT_INI_EFF = 8;
            public static final int TLI_DT_END_EFF = 8;
            public static final int TLI_COD_COMP_ANIA = 5;
            public static final int TLI_IMP_LRD_CALC = 12;
            public static final int TLI_IMP_LRD_EFFLQ = 12;
            public static final int TLI_IMP_NET_CALC = 12;
            public static final int TLI_IMP_NET_EFFLQ = 12;
            public static final int TLI_DS_RIGA = 10;
            public static final int TLI_DS_VER = 9;
            public static final int TLI_DS_TS_INI_CPTZ = 18;
            public static final int TLI_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_IMP_LRD_CALC = 3;
            public static final int TLI_IMP_LRD_EFFLQ = 3;
            public static final int TLI_IMP_NET_CALC = 3;
            public static final int TLI_IMP_NET_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
