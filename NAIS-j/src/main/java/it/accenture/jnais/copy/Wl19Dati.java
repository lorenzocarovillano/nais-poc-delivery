package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Wl19DtRilevazioneNav;
import it.accenture.jnais.ws.redefines.Wl19ValQuo;
import it.accenture.jnais.ws.redefines.Wl19ValQuoAcq;
import it.accenture.jnais.ws.redefines.Wl19ValQuoManfee;

/**Original name: WL19-DATI<br>
 * Variable: WL19-DATI from copybook LCCVL191<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wl19Dati {

    //==== PROPERTIES ====
    //Original name: WL19-COD-COMP-ANIA
    private int wl19CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WL19-COD-FND
    private String wl19CodFnd = DefaultValues.stringVal(Len.WL19_COD_FND);
    //Original name: WL19-DT-QTZ
    private int wl19DtQtz = DefaultValues.INT_VAL;
    //Original name: WL19-VAL-QUO
    private Wl19ValQuo wl19ValQuo = new Wl19ValQuo();
    //Original name: WL19-VAL-QUO-MANFEE
    private Wl19ValQuoManfee wl19ValQuoManfee = new Wl19ValQuoManfee();
    //Original name: WL19-DS-OPER-SQL
    private char wl19DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WL19-DS-VER
    private int wl19DsVer = DefaultValues.INT_VAL;
    //Original name: WL19-DS-TS-CPTZ
    private long wl19DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WL19-DS-UTENTE
    private String wl19DsUtente = DefaultValues.stringVal(Len.WL19_DS_UTENTE);
    //Original name: WL19-DS-STATO-ELAB
    private char wl19DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WL19-TP-FND
    private char wl19TpFnd = DefaultValues.CHAR_VAL;
    //Original name: WL19-DT-RILEVAZIONE-NAV
    private Wl19DtRilevazioneNav wl19DtRilevazioneNav = new Wl19DtRilevazioneNav();
    //Original name: WL19-VAL-QUO-ACQ
    private Wl19ValQuoAcq wl19ValQuoAcq = new Wl19ValQuoAcq();
    //Original name: WL19-FL-NO-NAV
    private char wl19FlNoNav = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wl19CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL19_COD_COMP_ANIA, 0);
        position += Len.WL19_COD_COMP_ANIA;
        wl19CodFnd = MarshalByte.readString(buffer, position, Len.WL19_COD_FND);
        position += Len.WL19_COD_FND;
        wl19DtQtz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL19_DT_QTZ, 0);
        position += Len.WL19_DT_QTZ;
        wl19ValQuo.setWl19ValQuoFromBuffer(buffer, position);
        position += Wl19ValQuo.Len.WL19_VAL_QUO;
        wl19ValQuoManfee.setWl19ValQuoManfeeFromBuffer(buffer, position);
        position += Wl19ValQuoManfee.Len.WL19_VAL_QUO_MANFEE;
        wl19DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wl19DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL19_DS_VER, 0);
        position += Len.WL19_DS_VER;
        wl19DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WL19_DS_TS_CPTZ, 0);
        position += Len.WL19_DS_TS_CPTZ;
        wl19DsUtente = MarshalByte.readString(buffer, position, Len.WL19_DS_UTENTE);
        position += Len.WL19_DS_UTENTE;
        wl19DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wl19TpFnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wl19DtRilevazioneNav.setWl19DtRilevazioneNavFromBuffer(buffer, position);
        position += Wl19DtRilevazioneNav.Len.WL19_DT_RILEVAZIONE_NAV;
        wl19ValQuoAcq.setWl19ValQuoAcqFromBuffer(buffer, position);
        position += Wl19ValQuoAcq.Len.WL19_VAL_QUO_ACQ;
        wl19FlNoNav = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wl19CodCompAnia, Len.Int.WL19_COD_COMP_ANIA, 0);
        position += Len.WL19_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wl19CodFnd, Len.WL19_COD_FND);
        position += Len.WL19_COD_FND;
        MarshalByte.writeIntAsPacked(buffer, position, wl19DtQtz, Len.Int.WL19_DT_QTZ, 0);
        position += Len.WL19_DT_QTZ;
        wl19ValQuo.getWl19ValQuoAsBuffer(buffer, position);
        position += Wl19ValQuo.Len.WL19_VAL_QUO;
        wl19ValQuoManfee.getWl19ValQuoManfeeAsBuffer(buffer, position);
        position += Wl19ValQuoManfee.Len.WL19_VAL_QUO_MANFEE;
        MarshalByte.writeChar(buffer, position, wl19DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wl19DsVer, Len.Int.WL19_DS_VER, 0);
        position += Len.WL19_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wl19DsTsCptz, Len.Int.WL19_DS_TS_CPTZ, 0);
        position += Len.WL19_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wl19DsUtente, Len.WL19_DS_UTENTE);
        position += Len.WL19_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wl19DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wl19TpFnd);
        position += Types.CHAR_SIZE;
        wl19DtRilevazioneNav.getWl19DtRilevazioneNavAsBuffer(buffer, position);
        position += Wl19DtRilevazioneNav.Len.WL19_DT_RILEVAZIONE_NAV;
        wl19ValQuoAcq.getWl19ValQuoAcqAsBuffer(buffer, position);
        position += Wl19ValQuoAcq.Len.WL19_VAL_QUO_ACQ;
        MarshalByte.writeChar(buffer, position, wl19FlNoNav);
        return buffer;
    }

    public void initDatiSpaces() {
        wl19CodCompAnia = Types.INVALID_INT_VAL;
        wl19CodFnd = "";
        wl19DtQtz = Types.INVALID_INT_VAL;
        wl19ValQuo.initWl19ValQuoSpaces();
        wl19ValQuoManfee.initWl19ValQuoManfeeSpaces();
        wl19DsOperSql = Types.SPACE_CHAR;
        wl19DsVer = Types.INVALID_INT_VAL;
        wl19DsTsCptz = Types.INVALID_LONG_VAL;
        wl19DsUtente = "";
        wl19DsStatoElab = Types.SPACE_CHAR;
        wl19TpFnd = Types.SPACE_CHAR;
        wl19DtRilevazioneNav.initWl19DtRilevazioneNavSpaces();
        wl19ValQuoAcq.initWl19ValQuoAcqSpaces();
        wl19FlNoNav = Types.SPACE_CHAR;
    }

    public void setWl19CodCompAnia(int wl19CodCompAnia) {
        this.wl19CodCompAnia = wl19CodCompAnia;
    }

    public int getWl19CodCompAnia() {
        return this.wl19CodCompAnia;
    }

    public void setWl19CodFnd(String wl19CodFnd) {
        this.wl19CodFnd = Functions.subString(wl19CodFnd, Len.WL19_COD_FND);
    }

    public String getWl19CodFnd() {
        return this.wl19CodFnd;
    }

    public void setWl19DtQtz(int wl19DtQtz) {
        this.wl19DtQtz = wl19DtQtz;
    }

    public int getWl19DtQtz() {
        return this.wl19DtQtz;
    }

    public void setWl19DsOperSql(char wl19DsOperSql) {
        this.wl19DsOperSql = wl19DsOperSql;
    }

    public char getWl19DsOperSql() {
        return this.wl19DsOperSql;
    }

    public void setWl19DsVer(int wl19DsVer) {
        this.wl19DsVer = wl19DsVer;
    }

    public int getWl19DsVer() {
        return this.wl19DsVer;
    }

    public void setWl19DsTsCptz(long wl19DsTsCptz) {
        this.wl19DsTsCptz = wl19DsTsCptz;
    }

    public long getWl19DsTsCptz() {
        return this.wl19DsTsCptz;
    }

    public void setWl19DsUtente(String wl19DsUtente) {
        this.wl19DsUtente = Functions.subString(wl19DsUtente, Len.WL19_DS_UTENTE);
    }

    public String getWl19DsUtente() {
        return this.wl19DsUtente;
    }

    public void setWl19DsStatoElab(char wl19DsStatoElab) {
        this.wl19DsStatoElab = wl19DsStatoElab;
    }

    public char getWl19DsStatoElab() {
        return this.wl19DsStatoElab;
    }

    public void setWl19TpFnd(char wl19TpFnd) {
        this.wl19TpFnd = wl19TpFnd;
    }

    public char getWl19TpFnd() {
        return this.wl19TpFnd;
    }

    public void setWl19FlNoNav(char wl19FlNoNav) {
        this.wl19FlNoNav = wl19FlNoNav;
    }

    public char getWl19FlNoNav() {
        return this.wl19FlNoNav;
    }

    public Wl19DtRilevazioneNav getWl19DtRilevazioneNav() {
        return wl19DtRilevazioneNav;
    }

    public Wl19ValQuo getWl19ValQuo() {
        return wl19ValQuo;
    }

    public Wl19ValQuoAcq getWl19ValQuoAcq() {
        return wl19ValQuoAcq;
    }

    public Wl19ValQuoManfee getWl19ValQuoManfee() {
        return wl19ValQuoManfee;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WL19_COD_COMP_ANIA = 3;
        public static final int WL19_COD_FND = 12;
        public static final int WL19_DT_QTZ = 5;
        public static final int WL19_DS_OPER_SQL = 1;
        public static final int WL19_DS_VER = 5;
        public static final int WL19_DS_TS_CPTZ = 10;
        public static final int WL19_DS_UTENTE = 20;
        public static final int WL19_DS_STATO_ELAB = 1;
        public static final int WL19_TP_FND = 1;
        public static final int WL19_FL_NO_NAV = 1;
        public static final int DATI = WL19_COD_COMP_ANIA + WL19_COD_FND + WL19_DT_QTZ + Wl19ValQuo.Len.WL19_VAL_QUO + Wl19ValQuoManfee.Len.WL19_VAL_QUO_MANFEE + WL19_DS_OPER_SQL + WL19_DS_VER + WL19_DS_TS_CPTZ + WL19_DS_UTENTE + WL19_DS_STATO_ELAB + WL19_TP_FND + Wl19DtRilevazioneNav.Len.WL19_DT_RILEVAZIONE_NAV + Wl19ValQuoAcq.Len.WL19_VAL_QUO_ACQ + WL19_FL_NO_NAV;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL19_COD_COMP_ANIA = 5;
            public static final int WL19_DT_QTZ = 8;
            public static final int WL19_DS_VER = 9;
            public static final int WL19_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
