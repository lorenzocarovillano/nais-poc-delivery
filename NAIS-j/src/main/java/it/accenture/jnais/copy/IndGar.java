package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-GAR<br>
 * Variable: IND-GAR from copybook IDBVGRZ2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndGar {

    //==== PROPERTIES ====
    //Original name: IND-GRZ-ID-ADES
    private short idAdes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-IB-OGG
    private short ibOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-DT-DECOR
    private short dtDecor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-DT-SCAD
    private short dtScad = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-COD-SEZ
    private short codSez = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-RAMO-BILA
    private short ramoBila = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-DT-INI-VAL-TAR
    private short dtIniValTar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-ID-1O-ASSTO
    private short id1oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-ID-2O-ASSTO
    private short id2oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-ID-3O-ASSTO
    private short id3oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-GAR
    private short tpGar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-RSH
    private short tpRsh = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-INVST
    private short tpInvst = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-MOD-PAG-GARCOL
    private short modPagGarcol = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-PER-PRE
    private short tpPerPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-ETA-AA-1O-ASSTO
    private short etaAa1oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-ETA-MM-1O-ASSTO
    private short etaMm1oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-ETA-AA-2O-ASSTO
    private short etaAa2oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-ETA-MM-2O-ASSTO
    private short etaMm2oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-ETA-AA-3O-ASSTO
    private short etaAa3oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-ETA-MM-3O-ASSTO
    private short etaMm3oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-EMIS-PUR
    private short tpEmisPur = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-ETA-A-SCAD
    private short etaAScad = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-CALC-PRE-PRSTZ
    private short tpCalcPrePrstz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-PRE
    private short tpPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-DUR
    private short tpDur = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-DUR-AA
    private short durAa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-DUR-MM
    private short durMm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-DUR-GG
    private short durGg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-NUM-AA-PAG-PRE
    private short numAaPagPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-AA-PAG-PRE-UNI
    private short aaPagPreUni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-MM-PAG-PRE-UNI
    private short mmPagPreUni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-FRAZ-INI-EROG-REN
    private short frazIniErogRen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-MM-1O-RAT
    private short mm1oRat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-PC-1O-RAT
    private short pc1oRat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-PRSTZ-ASSTA
    private short tpPrstzAssta = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-DT-END-CARZ
    private short dtEndCarz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-PC-RIP-PRE
    private short pcRipPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-COD-FND
    private short codFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-AA-REN-CER
    private short aaRenCer = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-PC-REVRSB
    private short pcRevrsb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-PC-RIP
    private short tpPcRip = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-PC-OPZ
    private short pcOpz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-IAS
    private short tpIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-STAB
    private short tpStab = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-ADEG-PRE
    private short tpAdegPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-DT-VARZ-TP-IAS
    private short dtVarzTpIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-FRAZ-DECR-CPT
    private short frazDecrCpt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-COD-TRAT-RIASS
    private short codTratRiass = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-DT-EMIS-RIASS
    private short tpDtEmisRiass = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TP-CESS-RIASS
    private short tpCessRiass = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-AA-STAB
    private short aaStab = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-TS-STAB-LIMITATA
    private short tsStabLimitata = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-DT-PRESC
    private short dtPresc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ-RSH-INVST
    private short rshInvst = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdAdes(short idAdes) {
        this.idAdes = idAdes;
    }

    public short getIdAdes() {
        return this.idAdes;
    }

    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setIbOgg(short ibOgg) {
        this.ibOgg = ibOgg;
    }

    public short getIbOgg() {
        return this.ibOgg;
    }

    public void setDtDecor(short dtDecor) {
        this.dtDecor = dtDecor;
    }

    public short getDtDecor() {
        return this.dtDecor;
    }

    public void setDtScad(short dtScad) {
        this.dtScad = dtScad;
    }

    public short getDtScad() {
        return this.dtScad;
    }

    public void setCodSez(short codSez) {
        this.codSez = codSez;
    }

    public short getCodSez() {
        return this.codSez;
    }

    public void setRamoBila(short ramoBila) {
        this.ramoBila = ramoBila;
    }

    public short getRamoBila() {
        return this.ramoBila;
    }

    public void setDtIniValTar(short dtIniValTar) {
        this.dtIniValTar = dtIniValTar;
    }

    public short getDtIniValTar() {
        return this.dtIniValTar;
    }

    public void setId1oAssto(short id1oAssto) {
        this.id1oAssto = id1oAssto;
    }

    public short getId1oAssto() {
        return this.id1oAssto;
    }

    public void setId2oAssto(short id2oAssto) {
        this.id2oAssto = id2oAssto;
    }

    public short getId2oAssto() {
        return this.id2oAssto;
    }

    public void setId3oAssto(short id3oAssto) {
        this.id3oAssto = id3oAssto;
    }

    public short getId3oAssto() {
        return this.id3oAssto;
    }

    public void setTpGar(short tpGar) {
        this.tpGar = tpGar;
    }

    public short getTpGar() {
        return this.tpGar;
    }

    public void setTpRsh(short tpRsh) {
        this.tpRsh = tpRsh;
    }

    public short getTpRsh() {
        return this.tpRsh;
    }

    public void setTpInvst(short tpInvst) {
        this.tpInvst = tpInvst;
    }

    public short getTpInvst() {
        return this.tpInvst;
    }

    public void setModPagGarcol(short modPagGarcol) {
        this.modPagGarcol = modPagGarcol;
    }

    public short getModPagGarcol() {
        return this.modPagGarcol;
    }

    public void setTpPerPre(short tpPerPre) {
        this.tpPerPre = tpPerPre;
    }

    public short getTpPerPre() {
        return this.tpPerPre;
    }

    public void setEtaAa1oAssto(short etaAa1oAssto) {
        this.etaAa1oAssto = etaAa1oAssto;
    }

    public short getEtaAa1oAssto() {
        return this.etaAa1oAssto;
    }

    public void setEtaMm1oAssto(short etaMm1oAssto) {
        this.etaMm1oAssto = etaMm1oAssto;
    }

    public short getEtaMm1oAssto() {
        return this.etaMm1oAssto;
    }

    public void setEtaAa2oAssto(short etaAa2oAssto) {
        this.etaAa2oAssto = etaAa2oAssto;
    }

    public short getEtaAa2oAssto() {
        return this.etaAa2oAssto;
    }

    public void setEtaMm2oAssto(short etaMm2oAssto) {
        this.etaMm2oAssto = etaMm2oAssto;
    }

    public short getEtaMm2oAssto() {
        return this.etaMm2oAssto;
    }

    public void setEtaAa3oAssto(short etaAa3oAssto) {
        this.etaAa3oAssto = etaAa3oAssto;
    }

    public short getEtaAa3oAssto() {
        return this.etaAa3oAssto;
    }

    public void setEtaMm3oAssto(short etaMm3oAssto) {
        this.etaMm3oAssto = etaMm3oAssto;
    }

    public short getEtaMm3oAssto() {
        return this.etaMm3oAssto;
    }

    public void setTpEmisPur(short tpEmisPur) {
        this.tpEmisPur = tpEmisPur;
    }

    public short getTpEmisPur() {
        return this.tpEmisPur;
    }

    public void setEtaAScad(short etaAScad) {
        this.etaAScad = etaAScad;
    }

    public short getEtaAScad() {
        return this.etaAScad;
    }

    public void setTpCalcPrePrstz(short tpCalcPrePrstz) {
        this.tpCalcPrePrstz = tpCalcPrePrstz;
    }

    public short getTpCalcPrePrstz() {
        return this.tpCalcPrePrstz;
    }

    public void setTpPre(short tpPre) {
        this.tpPre = tpPre;
    }

    public short getTpPre() {
        return this.tpPre;
    }

    public void setTpDur(short tpDur) {
        this.tpDur = tpDur;
    }

    public short getTpDur() {
        return this.tpDur;
    }

    public void setDurAa(short durAa) {
        this.durAa = durAa;
    }

    public short getDurAa() {
        return this.durAa;
    }

    public void setDurMm(short durMm) {
        this.durMm = durMm;
    }

    public short getDurMm() {
        return this.durMm;
    }

    public void setDurGg(short durGg) {
        this.durGg = durGg;
    }

    public short getDurGg() {
        return this.durGg;
    }

    public void setNumAaPagPre(short numAaPagPre) {
        this.numAaPagPre = numAaPagPre;
    }

    public short getNumAaPagPre() {
        return this.numAaPagPre;
    }

    public void setAaPagPreUni(short aaPagPreUni) {
        this.aaPagPreUni = aaPagPreUni;
    }

    public short getAaPagPreUni() {
        return this.aaPagPreUni;
    }

    public void setMmPagPreUni(short mmPagPreUni) {
        this.mmPagPreUni = mmPagPreUni;
    }

    public short getMmPagPreUni() {
        return this.mmPagPreUni;
    }

    public void setFrazIniErogRen(short frazIniErogRen) {
        this.frazIniErogRen = frazIniErogRen;
    }

    public short getFrazIniErogRen() {
        return this.frazIniErogRen;
    }

    public void setMm1oRat(short mm1oRat) {
        this.mm1oRat = mm1oRat;
    }

    public short getMm1oRat() {
        return this.mm1oRat;
    }

    public void setPc1oRat(short pc1oRat) {
        this.pc1oRat = pc1oRat;
    }

    public short getPc1oRat() {
        return this.pc1oRat;
    }

    public void setTpPrstzAssta(short tpPrstzAssta) {
        this.tpPrstzAssta = tpPrstzAssta;
    }

    public short getTpPrstzAssta() {
        return this.tpPrstzAssta;
    }

    public void setDtEndCarz(short dtEndCarz) {
        this.dtEndCarz = dtEndCarz;
    }

    public short getDtEndCarz() {
        return this.dtEndCarz;
    }

    public void setPcRipPre(short pcRipPre) {
        this.pcRipPre = pcRipPre;
    }

    public short getPcRipPre() {
        return this.pcRipPre;
    }

    public void setCodFnd(short codFnd) {
        this.codFnd = codFnd;
    }

    public short getCodFnd() {
        return this.codFnd;
    }

    public void setAaRenCer(short aaRenCer) {
        this.aaRenCer = aaRenCer;
    }

    public short getAaRenCer() {
        return this.aaRenCer;
    }

    public void setPcRevrsb(short pcRevrsb) {
        this.pcRevrsb = pcRevrsb;
    }

    public short getPcRevrsb() {
        return this.pcRevrsb;
    }

    public void setTpPcRip(short tpPcRip) {
        this.tpPcRip = tpPcRip;
    }

    public short getTpPcRip() {
        return this.tpPcRip;
    }

    public void setPcOpz(short pcOpz) {
        this.pcOpz = pcOpz;
    }

    public short getPcOpz() {
        return this.pcOpz;
    }

    public void setTpIas(short tpIas) {
        this.tpIas = tpIas;
    }

    public short getTpIas() {
        return this.tpIas;
    }

    public void setTpStab(short tpStab) {
        this.tpStab = tpStab;
    }

    public short getTpStab() {
        return this.tpStab;
    }

    public void setTpAdegPre(short tpAdegPre) {
        this.tpAdegPre = tpAdegPre;
    }

    public short getTpAdegPre() {
        return this.tpAdegPre;
    }

    public void setDtVarzTpIas(short dtVarzTpIas) {
        this.dtVarzTpIas = dtVarzTpIas;
    }

    public short getDtVarzTpIas() {
        return this.dtVarzTpIas;
    }

    public void setFrazDecrCpt(short frazDecrCpt) {
        this.frazDecrCpt = frazDecrCpt;
    }

    public short getFrazDecrCpt() {
        return this.frazDecrCpt;
    }

    public void setCodTratRiass(short codTratRiass) {
        this.codTratRiass = codTratRiass;
    }

    public short getCodTratRiass() {
        return this.codTratRiass;
    }

    public void setTpDtEmisRiass(short tpDtEmisRiass) {
        this.tpDtEmisRiass = tpDtEmisRiass;
    }

    public short getTpDtEmisRiass() {
        return this.tpDtEmisRiass;
    }

    public void setTpCessRiass(short tpCessRiass) {
        this.tpCessRiass = tpCessRiass;
    }

    public short getTpCessRiass() {
        return this.tpCessRiass;
    }

    public void setAaStab(short aaStab) {
        this.aaStab = aaStab;
    }

    public short getAaStab() {
        return this.aaStab;
    }

    public void setTsStabLimitata(short tsStabLimitata) {
        this.tsStabLimitata = tsStabLimitata;
    }

    public short getTsStabLimitata() {
        return this.tsStabLimitata;
    }

    public void setDtPresc(short dtPresc) {
        this.dtPresc = dtPresc;
    }

    public short getDtPresc() {
        return this.dtPresc;
    }

    public void setRshInvst(short rshInvst) {
        this.rshInvst = rshInvst;
    }

    public short getRshInvst() {
        return this.rshInvst;
    }
}
