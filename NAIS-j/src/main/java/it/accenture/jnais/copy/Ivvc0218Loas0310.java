package it.accenture.jnais.copy;

/**Original name: IVVC0218<br>
 * Variable: IVVC0218 from copybook IVVC0218<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0218Loas0310 {

    //==== PROPERTIES ====
    /**Original name: S211-ALIAS-ADES<br>
	 * <pre>----------------------------------------------------------------*
	 *    ALIAS INPUT VARIABILI
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA
	 *    LUNG.
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 * ASSET
	 * *****************************************************************
	 * --  ALIAS ADESIONE</pre>*/
    private String aliasAdes = "ADE";
    /**Original name: S211-ALIAS-BENEF<br>
	 * <pre>--  ALIAS BENEFICIARI</pre>*/
    private String aliasBenef = "BEP";
    /**Original name: S211-ALIAS-DT-COLL<br>
	 * <pre>--  ALIAS DATI COLLETTIVA</pre>*/
    private String aliasDtColl = "DCO";
    /**Original name: S211-ALIAS-DT-FISC-ADES<br>
	 * <pre>--  ALIAS DATI FISCALE ADESIONE</pre>*/
    private String aliasDtFiscAdes = "DFA";
    /**Original name: S211-ALIAS-DETT-QUEST<br>
	 * <pre>--  ALIAS DETTAGLIO QUESTIONARIO</pre>*/
    private String aliasDettQuest = "DEQ";
    /**Original name: S211-ALIAS-GARANZIA<br>
	 * <pre>--  ALIAS GARANZIA</pre>*/
    private String aliasGaranzia = "GRZ";
    /**Original name: S211-ALIAS-MOVIMENTO<br>
	 * <pre>--  ALIAS MOVIMENTO</pre>*/
    private String aliasMovimento = "MOV";
    /**Original name: S211-ALIAS-PARAM-OGG<br>
	 * <pre>--  ALIAS PARAMETRO OGGETTO</pre>*/
    private String aliasParamOgg = "POG";
    /**Original name: S211-ALIAS-PARAM-MOV<br>
	 * <pre>--  ALIAS PARAMETRO MOVIMENTO</pre>*/
    private String aliasParamMov = "PMO";
    /**Original name: S211-ALIAS-POLI<br>
	 * <pre>--  ALIAS POLIZZA</pre>*/
    private String aliasPoli = "POL";
    /**Original name: S211-ALIAS-QUEST<br>
	 * <pre>--  ALIAS QUESTIONARIO</pre>*/
    private String aliasQuest = "QUE";
    /**Original name: S211-ALIAS-RAPP-ANAG<br>
	 * <pre>--  ALIAS RAPPORTO ANAGRAFICO</pre>*/
    private String aliasRappAnag = "RAN";
    /**Original name: S211-ALIAS-RAPP-RETE<br>
	 * <pre>--  ALIAS RAPPORTO RETE</pre>*/
    private String aliasRappRete = "RRE";
    /**Original name: S211-ALIAS-SOPRAP-GAR<br>
	 * <pre>--  ALIAS SOPRAPREMIO DI GARANZIA</pre>*/
    private String aliasSoprapGar = "SPG";
    /**Original name: S211-ALIAS-STRA-INV<br>
	 * <pre>--  ALIAS STRATEGIA DI INVESTIMENTO</pre>*/
    private String aliasStraInv = "SDI";
    /**Original name: S211-ALIAS-TRCH-GAR<br>
	 * <pre>--  ALIAS TRANCHE DI GARANZIA</pre>*/
    private String aliasTrchGar = "TGA";
    /**Original name: S211-ALIAS-DFLT-ADES<br>
	 * <pre>-- ALIAS DEFAULT ADESIONE</pre>*/
    private String aliasDfltAdes = "DAD";
    /**Original name: S211-ALIAS-OGG-COLL<br>
	 * <pre>-- ALIAS OGGETTO COLLEGATO</pre>*/
    private String aliasOggColl = "OCO";
    /**Original name: S211-ALIAS-PARAM-COMP<br>
	 * <pre>--  ALIAS parametro compagnia</pre>*/
    private String aliasParamComp = "PCO";
    /**Original name: S211-ALIAS-DATI-CONTEST<br>
	 * <pre>--  ALIAS DATI CONTESTUALI</pre>*/
    private String aliasDatiContest = "CNT";
    /**Original name: S211-ALIAS-VINC-PEGN<br>
	 * <pre>--  ALIAS VINCOLI E PEGNI</pre>*/
    private String aliasVincPegn = "L23";
    /**Original name: S211-ALIAS-QOTAZ-FON<br>
	 * <pre>--  ALIAS REINVESTIMENTO POLIZZA</pre>*/
    private String aliasQotazFon = "L19";
    /**Original name: S211-ALIAS-EST-POLI-CPI-PR<br>
	 * <pre>--  ALIAS EST POLI CPI PR</pre>*/
    private String aliasEstPoliCpiPr = "P67";

    //==== METHODS ====
    public String getAliasAdes() {
        return this.aliasAdes;
    }

    public String getAliasBenef() {
        return this.aliasBenef;
    }

    public String getAliasDtColl() {
        return this.aliasDtColl;
    }

    public String getAliasDtFiscAdes() {
        return this.aliasDtFiscAdes;
    }

    public String getAliasDettQuest() {
        return this.aliasDettQuest;
    }

    public String getAliasGaranzia() {
        return this.aliasGaranzia;
    }

    public String getAliasMovimento() {
        return this.aliasMovimento;
    }

    public String getAliasParamOgg() {
        return this.aliasParamOgg;
    }

    public String getAliasParamMov() {
        return this.aliasParamMov;
    }

    public String getAliasPoli() {
        return this.aliasPoli;
    }

    public String getAliasQuest() {
        return this.aliasQuest;
    }

    public String getAliasRappAnag() {
        return this.aliasRappAnag;
    }

    public String getAliasRappRete() {
        return this.aliasRappRete;
    }

    public String getAliasSoprapGar() {
        return this.aliasSoprapGar;
    }

    public String getAliasStraInv() {
        return this.aliasStraInv;
    }

    public String getAliasTrchGar() {
        return this.aliasTrchGar;
    }

    public String getAliasDfltAdes() {
        return this.aliasDfltAdes;
    }

    public String getAliasOggColl() {
        return this.aliasOggColl;
    }

    public String getAliasParamComp() {
        return this.aliasParamComp;
    }

    public String getAliasDatiContest() {
        return this.aliasDatiContest;
    }

    public String getAliasVincPegn() {
        return this.aliasVincPegn;
    }

    public String getAliasQotazFon() {
        return this.aliasQotazFon;
    }

    public String getAliasEstPoliCpiPr() {
        return this.aliasEstPoliCpiPr;
    }
}
