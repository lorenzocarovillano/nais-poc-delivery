package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBO0731<br>
 * Variable: LDBO0731 from copybook LDBO0731<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbo07312 {

    //==== PROPERTIES ====
    //Original name: LDBO0731-ID-PADRE
    private int ldbo0731IdPadre = DefaultValues.INT_VAL;
    //Original name: LDBO0731-STB
    private String ldbo0731Stb = DefaultValues.stringVal(Len.LDBO0731_STB);
    //Original name: LDBO0731-TGA
    private String ldbo0731Tga = DefaultValues.stringVal(Len.LDBO0731_TGA);

    //==== METHODS ====
    public void setAreaLdbo0731Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbo0731IdPadre = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBO0731_ID_PADRE, 0);
        position += Len.LDBO0731_ID_PADRE;
        ldbo0731Stb = MarshalByte.readString(buffer, position, Len.LDBO0731_STB);
        position += Len.LDBO0731_STB;
        ldbo0731Tga = MarshalByte.readString(buffer, position, Len.LDBO0731_TGA);
    }

    public void setLdbo0731IdPadre(int ldbo0731IdPadre) {
        this.ldbo0731IdPadre = ldbo0731IdPadre;
    }

    public int getLdbo0731IdPadre() {
        return this.ldbo0731IdPadre;
    }

    public void setLdbo0731Stb(String ldbo0731Stb) {
        this.ldbo0731Stb = Functions.subString(ldbo0731Stb, Len.LDBO0731_STB);
    }

    public String getLdbo0731Stb() {
        return this.ldbo0731Stb;
    }

    public void setLdbo0731Tga(String ldbo0731Tga) {
        this.ldbo0731Tga = Functions.subString(ldbo0731Tga, Len.LDBO0731_TGA);
    }

    public String getLdbo0731Tga() {
        return this.ldbo0731Tga;
    }

    public String getLdbo0731TgaFormatted() {
        return Functions.padBlanks(getLdbo0731Tga(), Len.LDBO0731_TGA);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBO0731_STB = 2000;
        public static final int LDBO0731_TGA = 2000;
        public static final int LDBO0731_ID_PADRE = 5;
        public static final int AREA_LDBO0731 = LDBO0731_ID_PADRE + LDBO0731_STB + LDBO0731_TGA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBO0731_ID_PADRE = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
