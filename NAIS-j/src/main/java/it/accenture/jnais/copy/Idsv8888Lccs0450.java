package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV8888<br>
 * Variable: IDSV8888 from copybook IDSV8888<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv8888Lccs0450 {

    //==== PROPERTIES ====
    //Original name: IDSV8888-AREA-DISPLAY
    private String areaDisplay = "";

    //==== METHODS ====
    public void setAreaDisplay(String areaDisplay) {
        this.areaDisplay = Functions.subString(areaDisplay, Len.AREA_DISPLAY);
    }

    public String getAreaDisplay() {
        return this.areaDisplay;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 50;
        public static final int AREA_DISPLAY = 125;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
