package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVL271<br>
 * Variable: LCCVL271 from copybook LCCVL271<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvl271 {

    //==== PROPERTIES ====
    /**Original name: WL27-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA PREV
	 *    ALIAS L27
	 *    ULTIMO AGG. 17 NOV 2008
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WL27-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WL27-DATI
    private Wl27Dati dati = new Wl27Dati();

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public Wl27Dati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }
}
