package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-EST-RAPP-ANA<br>
 * Variable: IND-EST-RAPP-ANA from copybook IDBVE152<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndEstRappAna {

    //==== PROPERTIES ====
    //Original name: IND-E15-ID-RAPP-ANA-COLLG
    private short idRappAnaCollg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-COD-SOGG
    private short codSogg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-ID-SEGMENTAZ-CLI
    private short idSegmentazCli = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-FL-COINC-TIT-EFF
    private short flCoincTitEff = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-FL-PERS-ESP-POL
    private short flPersEspPol = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-DESC-PERS-ESP-POL
    private short descPersEspPol = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-TP-LEG-CNTR
    private short tpLegCntr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-DESC-LEG-CNTR
    private short descLegCntr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-TP-LEG-PERC-BNFICR
    private short tpLegPercBnficr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-D-LEG-PERC-BNFICR
    private short dLegPercBnficr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-TP-CNT-CORR
    private short tpCntCorr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-E15-TP-COINC-PIC-PAC
    private short tpCoincPicPac = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdRappAnaCollg(short idRappAnaCollg) {
        this.idRappAnaCollg = idRappAnaCollg;
    }

    public short getIdRappAnaCollg() {
        return this.idRappAnaCollg;
    }

    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setCodSogg(short codSogg) {
        this.codSogg = codSogg;
    }

    public short getCodSogg() {
        return this.codSogg;
    }

    public void setIdSegmentazCli(short idSegmentazCli) {
        this.idSegmentazCli = idSegmentazCli;
    }

    public short getIdSegmentazCli() {
        return this.idSegmentazCli;
    }

    public void setFlCoincTitEff(short flCoincTitEff) {
        this.flCoincTitEff = flCoincTitEff;
    }

    public short getFlCoincTitEff() {
        return this.flCoincTitEff;
    }

    public void setFlPersEspPol(short flPersEspPol) {
        this.flPersEspPol = flPersEspPol;
    }

    public short getFlPersEspPol() {
        return this.flPersEspPol;
    }

    public void setDescPersEspPol(short descPersEspPol) {
        this.descPersEspPol = descPersEspPol;
    }

    public short getDescPersEspPol() {
        return this.descPersEspPol;
    }

    public void setTpLegCntr(short tpLegCntr) {
        this.tpLegCntr = tpLegCntr;
    }

    public short getTpLegCntr() {
        return this.tpLegCntr;
    }

    public void setDescLegCntr(short descLegCntr) {
        this.descLegCntr = descLegCntr;
    }

    public short getDescLegCntr() {
        return this.descLegCntr;
    }

    public void setTpLegPercBnficr(short tpLegPercBnficr) {
        this.tpLegPercBnficr = tpLegPercBnficr;
    }

    public short getTpLegPercBnficr() {
        return this.tpLegPercBnficr;
    }

    public void setdLegPercBnficr(short dLegPercBnficr) {
        this.dLegPercBnficr = dLegPercBnficr;
    }

    public short getdLegPercBnficr() {
        return this.dLegPercBnficr;
    }

    public void setTpCntCorr(short tpCntCorr) {
        this.tpCntCorr = tpCntCorr;
    }

    public short getTpCntCorr() {
        return this.tpCntCorr;
    }

    public void setTpCoincPicPac(short tpCoincPicPac) {
        this.tpCoincPicPac = tpCoincPicPac;
    }

    public short getTpCoincPicPac() {
        return this.tpCoincPicPac;
    }
}
