package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Ivvc0211FlagGarOpzione;
import it.accenture.jnais.ws.enums.Ivvc0211FlgArea;
import it.accenture.jnais.ws.enums.Ivvc0211FormatoDataDb;
import it.accenture.jnais.ws.enums.Ivvc0211ModalitaEsecutiva;
import it.accenture.jnais.ws.enums.Ivvc0211StepElab;

/**Original name: IVVC0211-DATI-INPUT<br>
 * Variable: IVVC0211-DATI-INPUT from copybook IVVC0211<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0211DatiInput {

    //==== PROPERTIES ====
    //Original name: IVVC0211-PGM
    private String pgm = DefaultValues.stringVal(Len.PGM);
    /**Original name: IVVC0211-FLG-AREA<br>
	 * <pre>--     FLAG IDENTIFICATIVO DELLA MODALITA DI CHIAMATA</pre>*/
    private Ivvc0211FlgArea flgArea = new Ivvc0211FlgArea();
    //Original name: IVVC0211-MODALITA-ESECUTIVA
    private Ivvc0211ModalitaEsecutiva modalitaEsecutiva = new Ivvc0211ModalitaEsecutiva();
    //Original name: IVVC0211-COD-COMPAGNIA-ANIA
    private String codCompagniaAnia = DefaultValues.stringVal(Len.COD_COMPAGNIA_ANIA);
    //Original name: IVVC0211-TIPO-MOVIMENTO
    private String tipoMovimento = DefaultValues.stringVal(Len.TIPO_MOVIMENTO);
    //Original name: IVVC0211-TIPO-MOVI-ORIG
    private String tipoMoviOrig = DefaultValues.stringVal(Len.TIPO_MOVI_ORIG);
    //Original name: IVVC0211-COD-MAIN-BATCH
    private String codMainBatch = DefaultValues.stringVal(Len.COD_MAIN_BATCH);
    /**Original name: IVVC0211-DATA-EFFETTO<br>
	 * <pre>        05 (SF)-COD-SERVIZIO-BE               PIC  X(12).</pre>*/
    private int dataEffetto = DefaultValues.INT_VAL;
    //Original name: IVVC0211-DATA-COMPETENZA
    private long dataCompetenza = DefaultValues.LONG_VAL;
    //Original name: IVVC0211-DATA-COMP-AGG-STOR
    private long dataCompAggStor = DefaultValues.LONG_VAL;
    //Original name: IVVC0211-DATA-ULT-VERS-PROD
    private int dataUltVersProd = DefaultValues.INT_VAL;
    //Original name: IVVC0211-DATA-ULT-TIT-INC
    private int dataUltTitInc = DefaultValues.INT_VAL;
    //Original name: IVVC0211-TRATTAMENTO-STORICITA
    private String trattamentoStoricita = DefaultValues.stringVal(Len.TRATTAMENTO_STORICITA);
    //Original name: IVVC0211-FORMATO-DATA-DB
    private Ivvc0211FormatoDataDb formatoDataDb = new Ivvc0211FormatoDataDb();
    /**Original name: IVVC0211-STEP-ELAB<br>
	 * <pre>--     FLAG IDENTIFICATIVO DEL PASSO DI ELABORAZIONE</pre>*/
    private Ivvc0211StepElab stepElab = new Ivvc0211StepElab();
    //Original name: IVVC0211-KEY-AUT-OPER1
    private String keyAutOper1 = DefaultValues.stringVal(Len.KEY_AUT_OPER1);
    //Original name: IVVC0211-KEY-AUT-OPER2
    private String keyAutOper2 = DefaultValues.stringVal(Len.KEY_AUT_OPER2);
    //Original name: IVVC0211-KEY-AUT-OPER3
    private String keyAutOper3 = DefaultValues.stringVal(Len.KEY_AUT_OPER3);
    //Original name: IVVC0211-KEY-AUT-OPER4
    private String keyAutOper4 = DefaultValues.stringVal(Len.KEY_AUT_OPER4);
    //Original name: IVVC0211-KEY-AUT-OPER5
    private String keyAutOper5 = DefaultValues.stringVal(Len.KEY_AUT_OPER5);
    //Original name: IVVC0211-FLAG-GAR-OPZIONE
    private Ivvc0211FlagGarOpzione flagGarOpzione = new Ivvc0211FlagGarOpzione();
    /**Original name: IVVC0211-ELE-INFO-MAX<br>
	 * <pre>--     STRUTTURA DI MAPPING DELLE AREE PASSATE IN INPUT</pre>*/
    private short eleInfoMax = DefaultValues.SHORT_VAL;

    //==== METHODS ====
    public void setIvvc0211DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        pgm = MarshalByte.readString(buffer, position, Len.PGM);
        position += Len.PGM;
        flgArea.setFlgArea(MarshalByte.readString(buffer, position, Ivvc0211FlgArea.Len.FLG_AREA));
        position += Ivvc0211FlgArea.Len.FLG_AREA;
        modalitaEsecutiva.setModalitaEsecutiva(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        codCompagniaAnia = MarshalByte.readFixedString(buffer, position, Len.COD_COMPAGNIA_ANIA);
        position += Len.COD_COMPAGNIA_ANIA;
        tipoMovimento = MarshalByte.readFixedString(buffer, position, Len.TIPO_MOVIMENTO);
        position += Len.TIPO_MOVIMENTO;
        tipoMoviOrig = MarshalByte.readFixedString(buffer, position, Len.TIPO_MOVI_ORIG);
        position += Len.TIPO_MOVI_ORIG;
        codMainBatch = MarshalByte.readString(buffer, position, Len.COD_MAIN_BATCH);
        position += Len.COD_MAIN_BATCH;
        dataEffetto = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DATA_EFFETTO, 0);
        position += Len.DATA_EFFETTO;
        dataCompetenza = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DATA_COMPETENZA, 0);
        position += Len.DATA_COMPETENZA;
        dataCompAggStor = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DATA_COMP_AGG_STOR, 0);
        position += Len.DATA_COMP_AGG_STOR;
        dataUltVersProd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DATA_ULT_VERS_PROD, 0);
        position += Len.DATA_ULT_VERS_PROD;
        dataUltTitInc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DATA_ULT_TIT_INC, 0);
        position += Len.DATA_ULT_TIT_INC;
        trattamentoStoricita = MarshalByte.readString(buffer, position, Len.TRATTAMENTO_STORICITA);
        position += Len.TRATTAMENTO_STORICITA;
        formatoDataDb.setFormatoDataDb(MarshalByte.readString(buffer, position, Ivvc0211FormatoDataDb.Len.FORMATO_DATA_DB));
        position += Ivvc0211FormatoDataDb.Len.FORMATO_DATA_DB;
        stepElab.setStepElab(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        keyAutOper1 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER1);
        position += Len.KEY_AUT_OPER1;
        keyAutOper2 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER2);
        position += Len.KEY_AUT_OPER2;
        keyAutOper3 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER3);
        position += Len.KEY_AUT_OPER3;
        keyAutOper4 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER4);
        position += Len.KEY_AUT_OPER4;
        keyAutOper5 = MarshalByte.readString(buffer, position, Len.KEY_AUT_OPER5);
        position += Len.KEY_AUT_OPER5;
        flagGarOpzione.setFlagGarOpzione(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        eleInfoMax = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_INFO_MAX, 0);
    }

    public byte[] getIvvc0211DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, pgm, Len.PGM);
        position += Len.PGM;
        MarshalByte.writeString(buffer, position, flgArea.getFlgArea(), Ivvc0211FlgArea.Len.FLG_AREA);
        position += Ivvc0211FlgArea.Len.FLG_AREA;
        MarshalByte.writeChar(buffer, position, modalitaEsecutiva.getModalitaEsecutiva());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
        position += Len.COD_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, tipoMovimento, Len.TIPO_MOVIMENTO);
        position += Len.TIPO_MOVIMENTO;
        MarshalByte.writeString(buffer, position, tipoMoviOrig, Len.TIPO_MOVI_ORIG);
        position += Len.TIPO_MOVI_ORIG;
        MarshalByte.writeString(buffer, position, codMainBatch, Len.COD_MAIN_BATCH);
        position += Len.COD_MAIN_BATCH;
        MarshalByte.writeIntAsPacked(buffer, position, dataEffetto, Len.Int.DATA_EFFETTO, 0);
        position += Len.DATA_EFFETTO;
        MarshalByte.writeLongAsPacked(buffer, position, dataCompetenza, Len.Int.DATA_COMPETENZA, 0);
        position += Len.DATA_COMPETENZA;
        MarshalByte.writeLongAsPacked(buffer, position, dataCompAggStor, Len.Int.DATA_COMP_AGG_STOR, 0);
        position += Len.DATA_COMP_AGG_STOR;
        MarshalByte.writeIntAsPacked(buffer, position, dataUltVersProd, Len.Int.DATA_ULT_VERS_PROD, 0);
        position += Len.DATA_ULT_VERS_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, dataUltTitInc, Len.Int.DATA_ULT_TIT_INC, 0);
        position += Len.DATA_ULT_TIT_INC;
        MarshalByte.writeString(buffer, position, trattamentoStoricita, Len.TRATTAMENTO_STORICITA);
        position += Len.TRATTAMENTO_STORICITA;
        MarshalByte.writeString(buffer, position, formatoDataDb.getFormatoDataDb(), Ivvc0211FormatoDataDb.Len.FORMATO_DATA_DB);
        position += Ivvc0211FormatoDataDb.Len.FORMATO_DATA_DB;
        MarshalByte.writeChar(buffer, position, stepElab.getStepElab());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, keyAutOper1, Len.KEY_AUT_OPER1);
        position += Len.KEY_AUT_OPER1;
        MarshalByte.writeString(buffer, position, keyAutOper2, Len.KEY_AUT_OPER2);
        position += Len.KEY_AUT_OPER2;
        MarshalByte.writeString(buffer, position, keyAutOper3, Len.KEY_AUT_OPER3);
        position += Len.KEY_AUT_OPER3;
        MarshalByte.writeString(buffer, position, keyAutOper4, Len.KEY_AUT_OPER4);
        position += Len.KEY_AUT_OPER4;
        MarshalByte.writeString(buffer, position, keyAutOper5, Len.KEY_AUT_OPER5);
        position += Len.KEY_AUT_OPER5;
        MarshalByte.writeChar(buffer, position, flagGarOpzione.getFlagGarOpzione());
        position += Types.CHAR_SIZE;
        MarshalByte.writeShortAsPacked(buffer, position, eleInfoMax, Len.Int.ELE_INFO_MAX, 0);
        return buffer;
    }

    public void setPgm(String pgm) {
        this.pgm = Functions.subString(pgm, Len.PGM);
    }

    public String getPgm() {
        return this.pgm;
    }

    public void setS211CodCompagniaAniaFormatted(String s211CodCompagniaAnia) {
        this.codCompagniaAnia = Trunc.toUnsignedNumeric(s211CodCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
    }

    public int getCodCompagniaAnia() {
        return NumericDisplay.asInt(this.codCompagniaAnia);
    }

    public String getCodCompagniaAniaFormatted() {
        return this.codCompagniaAnia;
    }

    public void setS211TipoMovimento(int s211TipoMovimento) {
        this.tipoMovimento = NumericDisplay.asString(s211TipoMovimento, Len.TIPO_MOVIMENTO);
    }

    public void setS211TipoMovimentoFormatted(String s211TipoMovimento) {
        this.tipoMovimento = Trunc.toUnsignedNumeric(s211TipoMovimento, Len.TIPO_MOVIMENTO);
    }

    public int getTipoMovimento() {
        return NumericDisplay.asInt(this.tipoMovimento);
    }

    public String getTipoMovimentoFormatted() {
        return this.tipoMovimento;
    }

    public void setS211TipoMoviOrigFormatted(String s211TipoMoviOrig) {
        this.tipoMoviOrig = Trunc.toUnsignedNumeric(s211TipoMoviOrig, Len.TIPO_MOVI_ORIG);
    }

    public int getS211TipoMoviOrig() {
        return NumericDisplay.asInt(this.tipoMoviOrig);
    }

    public String getTipoMoviOrigFormatted() {
        return this.tipoMoviOrig;
    }

    public void setCodMainBatch(String codMainBatch) {
        this.codMainBatch = Functions.subString(codMainBatch, Len.COD_MAIN_BATCH);
    }

    public String getCodMainBatch() {
        return this.codMainBatch;
    }

    public void setDataEffetto(int dataEffetto) {
        this.dataEffetto = dataEffetto;
    }

    public int getDataEffetto() {
        return this.dataEffetto;
    }

    public void setDataCompetenza(long dataCompetenza) {
        this.dataCompetenza = dataCompetenza;
    }

    public long getDataCompetenza() {
        return this.dataCompetenza;
    }

    public void setDataCompAggStor(long dataCompAggStor) {
        this.dataCompAggStor = dataCompAggStor;
    }

    public long getDataCompAggStor() {
        return this.dataCompAggStor;
    }

    public void setDataUltVersProd(int dataUltVersProd) {
        this.dataUltVersProd = dataUltVersProd;
    }

    public int getDataUltVersProd() {
        return this.dataUltVersProd;
    }

    public void setDataUltTitInc(int dataUltTitInc) {
        this.dataUltTitInc = dataUltTitInc;
    }

    public int getDataUltTitInc() {
        return this.dataUltTitInc;
    }

    public void setTrattamentoStoricita(String trattamentoStoricita) {
        this.trattamentoStoricita = Functions.subString(trattamentoStoricita, Len.TRATTAMENTO_STORICITA);
    }

    public String getTrattamentoStoricita() {
        return this.trattamentoStoricita;
    }

    public void setKeyAutOper1(String keyAutOper1) {
        this.keyAutOper1 = Functions.subString(keyAutOper1, Len.KEY_AUT_OPER1);
    }

    public String getKeyAutOper1() {
        return this.keyAutOper1;
    }

    public void setKeyAutOper2(String keyAutOper2) {
        this.keyAutOper2 = Functions.subString(keyAutOper2, Len.KEY_AUT_OPER2);
    }

    public String getKeyAutOper2() {
        return this.keyAutOper2;
    }

    public void setKeyAutOper3(String keyAutOper3) {
        this.keyAutOper3 = Functions.subString(keyAutOper3, Len.KEY_AUT_OPER3);
    }

    public String getKeyAutOper3() {
        return this.keyAutOper3;
    }

    public void setKeyAutOper4(String keyAutOper4) {
        this.keyAutOper4 = Functions.subString(keyAutOper4, Len.KEY_AUT_OPER4);
    }

    public String getKeyAutOper4() {
        return this.keyAutOper4;
    }

    public void setKeyAutOper5(String keyAutOper5) {
        this.keyAutOper5 = Functions.subString(keyAutOper5, Len.KEY_AUT_OPER5);
    }

    public String getKeyAutOper5() {
        return this.keyAutOper5;
    }

    public void setEleInfoMax(short eleInfoMax) {
        this.eleInfoMax = eleInfoMax;
    }

    public short getEleInfoMax() {
        return this.eleInfoMax;
    }

    public Ivvc0211FlagGarOpzione getFlagGarOpzione() {
        return flagGarOpzione;
    }

    public Ivvc0211FlgArea getFlgArea() {
        return flgArea;
    }

    public Ivvc0211FormatoDataDb getFormatoDataDb() {
        return formatoDataDb;
    }

    public Ivvc0211ModalitaEsecutiva getModalitaEsecutiva() {
        return modalitaEsecutiva;
    }

    public Ivvc0211StepElab getStepElab() {
        return stepElab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PGM = 8;
        public static final int COD_COMPAGNIA_ANIA = 5;
        public static final int TIPO_MOVIMENTO = 5;
        public static final int TIPO_MOVI_ORIG = 5;
        public static final int COD_MAIN_BATCH = 12;
        public static final int DATA_EFFETTO = 5;
        public static final int DATA_COMPETENZA = 10;
        public static final int DATA_COMP_AGG_STOR = 10;
        public static final int DATA_ULT_VERS_PROD = 5;
        public static final int DATA_ULT_TIT_INC = 5;
        public static final int TRATTAMENTO_STORICITA = 3;
        public static final int KEY_AUT_OPER1 = 20;
        public static final int KEY_AUT_OPER2 = 20;
        public static final int KEY_AUT_OPER3 = 20;
        public static final int KEY_AUT_OPER4 = 20;
        public static final int KEY_AUT_OPER5 = 20;
        public static final int ELE_INFO_MAX = 3;
        public static final int IVVC0211_DATI_INPUT = PGM + Ivvc0211FlgArea.Len.FLG_AREA + Ivvc0211ModalitaEsecutiva.Len.MODALITA_ESECUTIVA + COD_COMPAGNIA_ANIA + TIPO_MOVIMENTO + TIPO_MOVI_ORIG + COD_MAIN_BATCH + DATA_EFFETTO + DATA_COMPETENZA + DATA_COMP_AGG_STOR + DATA_ULT_VERS_PROD + DATA_ULT_TIT_INC + TRATTAMENTO_STORICITA + Ivvc0211FormatoDataDb.Len.FORMATO_DATA_DB + Ivvc0211StepElab.Len.STEP_ELAB + KEY_AUT_OPER1 + KEY_AUT_OPER2 + KEY_AUT_OPER3 + KEY_AUT_OPER4 + KEY_AUT_OPER5 + Ivvc0211FlagGarOpzione.Len.FLAG_GAR_OPZIONE + ELE_INFO_MAX;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DATA_EFFETTO = 8;
            public static final int DATA_COMPETENZA = 18;
            public static final int DATA_COMP_AGG_STOR = 18;
            public static final int DATA_ULT_VERS_PROD = 8;
            public static final int DATA_ULT_TIT_INC = 8;
            public static final int ELE_INFO_MAX = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
