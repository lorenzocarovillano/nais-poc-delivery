package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVG351<br>
 * Variable: LDBVG351 from copybook LDBVG351<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbvg351 {

    //==== PROPERTIES ====
    //Original name: LDBVG351-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBVG351-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBVG351-TP-MOVI
    private int tpMovi = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbvg351Formatted(String data) {
        byte[] buffer = new byte[Len.LDBVG351];
        MarshalByte.writeString(buffer, 1, data, Len.LDBVG351);
        setLdbvg351Bytes(buffer, 1);
    }

    public String getLdbvg351Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvg351Bytes());
    }

    public byte[] getLdbvg351Bytes() {
        byte[] buffer = new byte[Len.LDBVG351];
        return getLdbvg351Bytes(buffer, 1);
    }

    public void setLdbvg351Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI, 0);
    }

    public byte[] getLdbvg351Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi, Len.Int.TP_MOVI, 0);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpMovi(int tpMovi) {
        this.tpMovi = tpMovi;
    }

    public int getTpMovi() {
        return this.tpMovi;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int ID_OGG = 5;
        public static final int TP_MOVI = 3;
        public static final int LDBVG351 = ID_OGG + TP_OGG + TP_MOVI;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int TP_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
