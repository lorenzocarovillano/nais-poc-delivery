package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCLT-DATI<br>
 * Variable: WCLT-DATI from copybook LCCVCLT1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WcltDati {

    //==== PROPERTIES ====
    //Original name: WCLT-ID-CLAU-TXT
    private int idClauTxt = DefaultValues.INT_VAL;
    //Original name: WCLT-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: WCLT-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: WCLT-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: WCLT-ID-MOVI-CHIU
    private int idMoviChiu = DefaultValues.INT_VAL;
    //Original name: WCLT-DT-INI-EFF
    private int dtIniEff = DefaultValues.INT_VAL;
    //Original name: WCLT-DT-END-EFF
    private int dtEndEff = DefaultValues.INT_VAL;
    //Original name: WCLT-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: WCLT-TP-CLAU
    private String tpClau = DefaultValues.stringVal(Len.TP_CLAU);
    //Original name: WCLT-COD-CLAU
    private String codClau = DefaultValues.stringVal(Len.COD_CLAU);
    //Original name: WCLT-DESC-BREVE
    private String descBreve = DefaultValues.stringVal(Len.DESC_BREVE);
    //Original name: WCLT-DESC-LNG
    private String descLng = DefaultValues.stringVal(Len.DESC_LNG);
    //Original name: WCLT-DS-RIGA
    private long dsRiga = DefaultValues.LONG_VAL;
    //Original name: WCLT-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WCLT-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: WCLT-DS-TS-INI-CPTZ
    private long dsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WCLT-DS-TS-END-CPTZ
    private long dsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WCLT-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: WCLT-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        idClauTxt = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_CLAU_TXT, 0);
        position += Len.ID_CLAU_TXT;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        idMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        idMoviChiu = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        dtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        dtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        tpClau = MarshalByte.readString(buffer, position, Len.TP_CLAU);
        position += Len.TP_CLAU;
        codClau = MarshalByte.readString(buffer, position, Len.COD_CLAU);
        position += Len.COD_CLAU;
        descBreve = MarshalByte.readString(buffer, position, Len.DESC_BREVE);
        position += Len.DESC_BREVE;
        descLng = MarshalByte.readString(buffer, position, Len.DESC_LNG);
        position += Len.DESC_LNG;
        dsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        dsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idClauTxt, Len.Int.ID_CLAU_TXT, 0);
        position += Len.ID_CLAU_TXT;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviCrz, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviChiu, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniEff, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtEndEff, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, tpClau, Len.TP_CLAU);
        position += Len.TP_CLAU;
        MarshalByte.writeString(buffer, position, codClau, Len.COD_CLAU);
        position += Len.COD_CLAU;
        MarshalByte.writeString(buffer, position, descBreve, Len.DESC_BREVE);
        position += Len.DESC_BREVE;
        MarshalByte.writeString(buffer, position, descLng, Len.DESC_LNG);
        position += Len.DESC_LNG;
        MarshalByte.writeLongAsPacked(buffer, position, dsRiga, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        return buffer;
    }

    public void initDatiSpaces() {
        idClauTxt = Types.INVALID_INT_VAL;
        idOgg = Types.INVALID_INT_VAL;
        tpOgg = "";
        idMoviCrz = Types.INVALID_INT_VAL;
        idMoviChiu = Types.INVALID_INT_VAL;
        dtIniEff = Types.INVALID_INT_VAL;
        dtEndEff = Types.INVALID_INT_VAL;
        codCompAnia = Types.INVALID_INT_VAL;
        tpClau = "";
        codClau = "";
        descBreve = "";
        descLng = "";
        dsRiga = Types.INVALID_LONG_VAL;
        dsOperSql = Types.SPACE_CHAR;
        dsVer = Types.INVALID_INT_VAL;
        dsTsIniCptz = Types.INVALID_LONG_VAL;
        dsTsEndCptz = Types.INVALID_LONG_VAL;
        dsUtente = "";
        dsStatoElab = Types.SPACE_CHAR;
    }

    public void setIdClauTxt(int idClauTxt) {
        this.idClauTxt = idClauTxt;
    }

    public int getIdClauTxt() {
        return this.idClauTxt;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setIdMoviChiu(int idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public int getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtIniEff(int dtIniEff) {
        this.dtIniEff = dtIniEff;
    }

    public int getDtIniEff() {
        return this.dtIniEff;
    }

    public void setDtEndEff(int dtEndEff) {
        this.dtEndEff = dtEndEff;
    }

    public int getDtEndEff() {
        return this.dtEndEff;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setTpClau(String tpClau) {
        this.tpClau = Functions.subString(tpClau, Len.TP_CLAU);
    }

    public String getTpClau() {
        return this.tpClau;
    }

    public void setCodClau(String codClau) {
        this.codClau = Functions.subString(codClau, Len.COD_CLAU);
    }

    public String getCodClau() {
        return this.codClau;
    }

    public void setDescBreve(String descBreve) {
        this.descBreve = Functions.subString(descBreve, Len.DESC_BREVE);
    }

    public String getDescBreve() {
        return this.descBreve;
    }

    public void setDescLng(String descLng) {
        this.descLng = Functions.subString(descLng, Len.DESC_LNG);
    }

    public String getDescLng() {
        return this.descLng;
    }

    public void setDsRiga(long dsRiga) {
        this.dsRiga = dsRiga;
    }

    public long getDsRiga() {
        return this.dsRiga;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dsTsIniCptz = dsTsIniCptz;
    }

    public long getDsTsIniCptz() {
        return this.dsTsIniCptz;
    }

    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dsTsEndCptz = dsTsEndCptz;
    }

    public long getDsTsEndCptz() {
        return this.dsTsEndCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_CLAU_TXT = 5;
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int TP_CLAU = 3;
        public static final int COD_CLAU = 20;
        public static final int DESC_BREVE = 30;
        public static final int DESC_LNG = 250;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int DATI = ID_CLAU_TXT + ID_OGG + TP_OGG + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + TP_CLAU + COD_CLAU + DESC_BREVE + DESC_LNG + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_CLAU_TXT = 9;
            public static final int ID_OGG = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
