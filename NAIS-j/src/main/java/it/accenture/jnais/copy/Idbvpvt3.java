package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVPVT3<br>
 * Copybook: IDBVPVT3 from copybook IDBVPVT3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvpvt3 {

    //==== PROPERTIES ====
    //Original name: PVT-DT-INI-EFF-DB
    private String pvtDtIniEffDb = DefaultValues.stringVal(Len.PVT_DT_INI_EFF_DB);
    //Original name: PVT-DT-END-EFF-DB
    private String pvtDtEndEffDb = DefaultValues.stringVal(Len.PVT_DT_END_EFF_DB);

    //==== METHODS ====
    public void setPvtDtIniEffDb(String pvtDtIniEffDb) {
        this.pvtDtIniEffDb = Functions.subString(pvtDtIniEffDb, Len.PVT_DT_INI_EFF_DB);
    }

    public String getPvtDtIniEffDb() {
        return this.pvtDtIniEffDb;
    }

    public void setPvtDtEndEffDb(String pvtDtEndEffDb) {
        this.pvtDtEndEffDb = Functions.subString(pvtDtEndEffDb, Len.PVT_DT_END_EFF_DB);
    }

    public String getPvtDtEndEffDb() {
        return this.pvtDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_DT_INI_EFF_DB = 10;
        public static final int PVT_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
