package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaXIabs0140;
import it.accenture.jnais.ws.enums.FlagAllineaStati;
import it.accenture.jnais.ws.enums.FlagBatchExecutorStd;
import it.accenture.jnais.ws.enums.FlagGestioneLanciBus;
import it.accenture.jnais.ws.enums.FlagGestioneParallelismo;
import it.accenture.jnais.ws.enums.FlagRc04;
import it.accenture.jnais.ws.enums.FlagReturnCode;
import it.accenture.jnais.ws.enums.FlagTipoPgm;
import it.accenture.jnais.ws.enums.FlagVerificaFinaleJobs;
import it.accenture.jnais.ws.enums.WkFlagGuideType;
import it.accenture.jnais.ws.enums.WkLimiteArrayBlob;
import it.accenture.jnais.ws.enums.WkSimulazione;
import it.accenture.jnais.ws.enums.WkTipoErrore;
import it.accenture.jnais.ws.Iabv0002;
import it.accenture.jnais.ws.Iabv0003;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Idsv0301Lves0269;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Ieao9901Area;
import it.accenture.jnais.ws.LinkArea;
import it.accenture.jnais.ws.occurs.TabellaStatiBatchElements;
import it.accenture.jnais.ws.occurs.TabellaStatiJobElements;
import it.accenture.jnais.ws.occurs.WkStatiSospesiEle;
import it.accenture.jnais.ws.ParamInfrAppl;
import it.accenture.jnais.ws.ptr.Ijccmq04VarAmbiente;
import it.accenture.jnais.ws.ptr.WAreaPerChiamateWsmq;
import it.accenture.jnais.ws.redefines.WkLungParamX;
import it.accenture.jnais.ws.redefines.WkTimestamp26;
import it.accenture.jnais.ws.WkLivelloGravita;
import it.accenture.jnais.ws.WsBufferWhereCondIabs0130;

/**Original name: IABV0007<br>
 * Variable: IABV0007 from copybook IABV0007<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Iabv0007 {

    //==== PROPERTIES ====
    public static final int TABELLA_STATI_BATCH_ELEMENTS_MAXOCCURS = 10;
    public static final int TABELLA_STATI_JOB_ELEMENTS_MAXOCCURS = 10;
    public static final int WK_STATI_SOSPESI_ELE_MAXOCCURS = 50;
    /**Original name: FS-PARA<br>
	 * <pre>---> FILE STATUS FILES UTILIZZATI</pre>*/
    private String fsPara = "00";
    //Original name: FS-REPORT01
    private String fsReport01 = "00";
    /**Original name: WK-SQLCODE<br>
	 * <pre>-- Inizio Include Batch Executor
	 * ---------------------------------------------------------------*
	 *  COPY INFRASTRUTTURALE PER BATCH EXECUTOR
	 * ---------------------------------------------------------------*</pre>*/
    private String wkSqlcode = DefaultValues.stringVal(Len.WK_SQLCODE);
    //Original name: WK-ERRORE
    private boolean wkErrore = false;
    //Original name: WK-TIPO-ERRORE
    private WkTipoErrore wkTipoErrore = new WkTipoErrore();
    //Original name: WK-PGM-ERRORE
    private String wkPgmErrore = "";
    //Original name: WK-PGM-GUIDA
    private String wkPgmGuida = "";
    //Original name: WK-PGM-BUSINESS
    private String wkPgmBusiness = "";
    //Original name: WK-LABEL
    private String wkLabel = DefaultValues.stringVal(Len.WK_LABEL);
    //Original name: ESSE
    private char esse = 'S';
    //Original name: ENNE
    private char enne = 'N';
    //Original name: WK-VERSIONING
    private int wkVersioning = DefaultValues.INT_VAL;
    //Original name: FLAG-BATCH-EXECUTOR-STD
    private FlagBatchExecutorStd flagBatchExecutorStd = new FlagBatchExecutorStd();
    //Original name: WK-LIVELLO-GRAVITA
    private WkLivelloGravita wkLivelloGravita = new WkLivelloGravita();
    //Original name: WK-PROTOCOL
    private boolean wkProtocol = false;
    //Original name: WK-SIMULAZIONE
    private WkSimulazione wkSimulazione = new WkSimulazione();
    //Original name: WK-PRIMA-BUFFER-SK-TROVATO
    private boolean wkPrimaBufferSkTrovato = false;
    //Original name: WK-PRIMA-VOLTA
    private boolean wkPrimaVolta = true;
    //Original name: WK-PRIMA-VOLTA-SUSPEND
    private boolean wkPrimaVoltaSuspend = true;
    //Original name: WK-POST-GUIDE-X-JOB-NOT-FOUND
    private boolean wkPostGuideXJobNotFound = true;
    //Original name: WK-FLAG-GUIDE-TYPE
    private WkFlagGuideType wkFlagGuideType = new WkFlagGuideType();
    //Original name: WK-BAT-COLLECTION-KEY
    private boolean wkBatCollectionKey = false;
    //Original name: WK-JOB-COLLECTION-KEY
    private boolean wkJobCollectionKey = false;
    //Original name: WK-OPEN-PARAM
    private boolean wkOpenParam = false;
    //Original name: WK-OPEN-REPORT
    private boolean wkOpenReport = false;
    //Original name: WK-EOF-PARAM
    private boolean wkEofParam = false;
    //Original name: FLAG-INIZIO-LETTURA
    private boolean flagInizioLettura = false;
    //Original name: FLAG-FINE-LETTURA
    private boolean flagFineLettura = false;
    //Original name: FLAG-POSIZ-PARAM
    private boolean flagPosizParam = false;
    //Original name: FLAG-VERIFICA-BATCH
    private boolean flagVerificaBatch = false;
    //Original name: FLAG-RISULT-VERIF-ELAB-STATE
    private boolean flagRisultVerifElabState = false;
    //Original name: FLAG-WK-BATCH-STATE-FOUND
    private boolean flagWkBatchStateFound = false;
    //Original name: FLAG-VERIFICA-PRENOTAZIONE
    private boolean flagVerificaPrenotazione = false;
    //Original name: FLAG-ELABORA-BATCH
    private boolean flagElaboraBatch = false;
    //Original name: FLAG-AGGIORNA-PRENOTAZ
    private boolean flagAggiornaPrenotaz = false;
    //Original name: FLAG-CONFERMA-BATCH-ESEGUITO
    private boolean flagConfermaBatchEseguito = false;
    //Original name: FLAG-SKIP-BATCH
    private boolean flagSkipBatch = false;
    //Original name: FLAG-ESTRAI-JOB
    private boolean flagEstraiJob = false;
    //Original name: FLAG-ESTRAI-REC
    private boolean flagEstraiRec = false;
    //Original name: FLAG-FOUND-NOT-FOUND
    private boolean flagFoundNotFound = false;
    //Original name: FLAG-FINE-BTC-BATCH
    private boolean flagFineBtcBatch = false;
    //Original name: FLAG-LANCIA-BUSINESS
    private boolean flagLanciaBusiness = false;
    //Original name: FLAG-LANCIA-ROTTURA
    private boolean flagLanciaRottura = false;
    //Original name: FLAG-FINE-ALIMENTAZIONE
    private boolean flagFineAlimentazione = false;
    //Original name: FLAG-ALLINEA-STATI
    private FlagAllineaStati flagAllineaStati = new FlagAllineaStati();
    //Original name: FLAG-GESTIONE-LANCI-BUS
    private FlagGestioneLanciBus flagGestioneLanciBus = new FlagGestioneLanciBus();
    //Original name: FLAG-VERIFICA-FINALE-JOBS
    private FlagVerificaFinaleJobs flagVerificaFinaleJobs = new FlagVerificaFinaleJobs();
    //Original name: WK-COMMIT-FREQUENCY
    private int wkCommitFrequency = DefaultValues.BIN_INT_VAL;
    public static final int INIT_COMMIT_FREQUENCY = 1;
    //Original name: WK-DES-BATCH-STATE
    private String wkDesBatchState = "";
    //Original name: FLAG-TIPO-PGM
    private FlagTipoPgm flagTipoPgm = new FlagTipoPgm();
    //Original name: FLAG-UPDATE-FOR-RESTORE
    private boolean flagUpdateForRestore = false;
    //Original name: FLAG-RETURN-CODE
    private FlagReturnCode flagReturnCode = new FlagReturnCode();
    //Original name: FLAG-RC-04
    private FlagRc04 flagRc04 = new FlagRc04();
    //Original name: WK-ID-BATCH
    private String wkIdBatch = DefaultValues.stringVal(Len.WK_ID_BATCH);
    //Original name: WK-ID-JOB
    private String wkIdJob = DefaultValues.stringVal(Len.WK_ID_JOB);
    //Original name: WK-ID-JOB-ULT-COMMIT
    private String wkIdJobUltCommit = DefaultValues.stringVal(Len.WK_ID_JOB_ULT_COMMIT);
    //Original name: WK-COD-BATCH-STATE
    private char wkCodBatchState = DefaultValues.CHAR_VAL;
    //Original name: WK-COD-ELAB-STATE
    private char wkCodElabState = DefaultValues.CHAR_VAL;
    //Original name: WK-ID-RICH
    private int wkIdRich = DefaultValues.INT_VAL;
    //Original name: ULTIMO-LANCIO-X-CLOSE-FILE
    private String ultimoLancioXCloseFile = "ULTIMO LANCIO X CLOSE FILE";
    //Original name: WS-LUNG-EFF-BLOB
    private String wsLungEffBlob = "000000000";
    //Original name: FLAG-GESTIONE-PARALLELISMO
    private FlagGestioneParallelismo flagGestioneParallelismo = new FlagGestioneParallelismo();
    //Original name: WS-BUFFER-WHERE-COND-IABS0130
    private WsBufferWhereCondIabs0130 wsBufferWhereCondIabs0130 = new WsBufferWhereCondIabs0130();
    //Original name: AREA-X-IABS0140
    private AreaXIabs0140 areaXIabs0140 = new AreaXIabs0140();
    //Original name: WK-ATTIVO
    private char wkAttivo = 'Y';
    //Original name: IABV0004
    private Iabv0004 iabv0004 = new Iabv0004();
    //Original name: IDSV8888
    private Idsv8888 idsv8888 = new Idsv8888();
    //Original name: WS-DATE-N
    private String wsDateN = DefaultValues.stringVal(Len.WS_DATE_N);
    //Original name: WS-DATE-X
    private String wsDateX = DefaultValues.stringVal(Len.WS_DATE_X);
    //Original name: WS-TIMESTAMP-N
    private String wsTimestampN = DefaultValues.stringVal(Len.WS_TIMESTAMP_N);
    //Original name: WS-TIMESTAMP-X
    private String wsTimestampX = DefaultValues.stringVal(Len.WS_TIMESTAMP_X);
    //Original name: TABELLA-STATI-BATCH-ELEMENTS
    private TabellaStatiBatchElements[] tabellaStatiBatchElements = new TabellaStatiBatchElements[TABELLA_STATI_BATCH_ELEMENTS_MAXOCCURS];
    //Original name: TABELLA-STATI-JOB-ELEMENTS
    private TabellaStatiJobElements[] tabellaStatiJobElements = new TabellaStatiJobElements[TABELLA_STATI_JOB_ELEMENTS_MAXOCCURS];
    //Original name: WK-CURRENT-PK
    private long wkCurrentPk = DefaultValues.LONG_VAL;
    //Original name: WK-STATI-SOSPESI-ELE
    private WkStatiSospesiEle[] wkStatiSospesiEle = new WkStatiSospesiEle[WK_STATI_SOSPESI_ELE_MAXOCCURS];
    //Original name: WK-LOG-ERRORE
    private WkLogErrore wkLogErrore = new WkLogErrore();
    /**Original name: WK-LIMITE-SKEDA-PARAMETRO<br>
	 * <pre>**************************************************************
	 *  CAMPI LIMITE
	 * **************************************************************</pre>*/
    private short wkLimiteSkedaParametro = ((short)79);
    //Original name: WK-LIMITE-STATI-BATCH
    private short wkLimiteStatiBatch = ((short)10);
    //Original name: WK-LIMITE-STATI-JOB
    private short wkLimiteStatiJob = ((short)10);
    //Original name: WK-LIMITE-STATI-SOSP
    private short wkLimiteStatiSosp = ((short)50);
    //Original name: WK-LIMITE-ARRAY-BLOB
    private WkLimiteArrayBlob wkLimiteArrayBlob = new WkLimiteArrayBlob();
    //Original name: IND-MAX-ELE-ERRORI
    private short indMaxEleErrori = ((short)0);
    //Original name: IND-SEARCH
    private short indSearch = ((short)0);
    //Original name: IND-LUNG
    private String indLung = "0";
    //Original name: IND-POSIZ-PARAM
    private short indPosizParam = ((short)1);
    //Original name: IND-STATI-BATCH
    private short indStatiBatch = ((short)0);
    //Original name: IND-STATI-JOB
    private short indStatiJob = ((short)0);
    //Original name: IND-STATI-V0002
    private short indStatiV0002 = ((short)0);
    //Original name: IND-STATI-SOSP
    private short indStatiSosp = ((short)0);
    //Original name: WK-LUNG-PARAM-X
    private WkLungParamX wkLungParamX = new WkLungParamX();
    //Original name: LCCC0090
    private LinkArea lccc0090 = new LinkArea();
    /**Original name: WK-SEQ-SESSIONE<br>
	 * <pre>-- fine COPY LCCC0090</pre>*/
    private String wkSeqSessione = "000000000";
    /**Original name: CNT-NUM-CONTR-LETT<br>
	 * <pre>----------------------------------------------------------------*
	 * ---> CONTATORI DI ELABORAZIONE PER STATISTICHE FINALI
	 * ----------------------------------------------------------------*
	 * --- NUMERO CONTRATTI LETTI FILE INPUT MOVIMENTO</pre>*/
    private String cntNumContrLett = "000000000";
    /**Original name: CNT-NUM-CONTR-ELAB<br>
	 * <pre>--- NUMERO CONTRATTI ELABORATI CON ESITO OK</pre>*/
    private String cntNumContrElab = "000000000";
    /**Original name: CNT-NUM-OPERAZ-LETTE<br>
	 * <pre>--- NUMERO MOVIMENTI LETTI DA INPUT MOVIMENTO</pre>*/
    private String cntNumOperazLette = "000000000";
    /**Original name: WK-TIMESTAMP-26<br>
	 * <pre>----------------------------------------------------------------*
	 *  DEFINIZIONE AREA DI APPOGGIO PER FORMATTAZIONE DATE:
	 * ----------------------------------------------------------------*</pre>*/
    private WkTimestamp26 wkTimestamp26 = new WkTimestamp26();
    //Original name: WK-TIMES-14
    private String wkTimes14 = DefaultValues.stringVal(Len.WK_TIMES14);
    //Original name: WK-TIMES-04
    private String wkTimes04 = "0000";
    //Original name: WK-SESSIONE-N09
    private String wkSessioneN09 = DefaultValues.stringVal(Len.WK_SESSIONE_N09);
    //Original name: WK-SESSIONE-X11
    private String wkSessioneX11 = DefaultValues.stringVal(Len.WK_SESSIONE_X11);
    //Original name: COMODO-COD-COMP-ANIA
    private String comodoCodCompAnia = DefaultValues.stringVal(Len.COMODO_COD_COMP_ANIA);
    //Original name: COMODO-PROG-PROTOCOL
    private String comodoProgProtocol = DefaultValues.stringVal(Len.COMODO_PROG_PROTOCOL);
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001 = new AreaIdsv0001();
    //Original name: IDSV0003
    private Idsv0003 idsv0003 = new Idsv0003();
    //Original name: IABV0003
    private Iabv0003 iabv0003 = new Iabv0003();
    //Original name: IABV0002
    private Iabv0002 iabv0002 = new Iabv0002();
    //Original name: IABI0011-AREA
    private Iabi0011Area iabi0011Area = new Iabi0011Area();
    //Original name: IDSV0301
    private Idsv0301Lves0269 idsv0301 = new Idsv0301Lves0269();
    //Original name: IABV0010
    private Iabv0010 iabv0010 = new Iabv0010();
    //Original name: FLAG-DESC-ERR-MORE-100
    private boolean flagDescErrMore100 = false;
    //Original name: FLAG-STAMPA-TESTATA-JCL
    private boolean flagStampaTestataJcl = false;
    //Original name: FLAG-STAMPA-LOGO-ERRORE
    private boolean flagStampaLogoErrore = false;
    //Original name: FLAG-WARNING-TROVATO
    private boolean flagWarningTrovato = false;
    //Original name: CONT-TAB-GUIDE-LETTE
    private String contTabGuideLette = DefaultValues.stringVal(Len.CONT_TAB_GUIDE_LETTE);
    //Original name: CONT-BUS-SERV-ESEG
    private int contBusServEseg = DefaultValues.INT_VAL;
    //Original name: CONT-BUS-SERV-ESITO-OK
    private int contBusServEsitoOk = DefaultValues.INT_VAL;
    //Original name: CONT-BUS-SERV-WARNING
    private int contBusServWarning = DefaultValues.INT_VAL;
    //Original name: CONT-BUS-SERV-ESITO-KO
    private int contBusServEsitoKo = DefaultValues.INT_VAL;
    //Original name: CONST-SEQUENTIAL-GUIDE
    private String constSequentialGuide = "SEQUENTIAL GUIDE";
    //Original name: PGM-IJCS0060
    private String pgmIjcs0060 = "IJCS0060";
    //Original name: IABC0010
    private Iabc0010 iabc0010 = new Iabc0010();
    //Original name: IX-ADDRESS
    private int ixAddress = DefaultValues.BIN_INT_VAL;
    //Original name: IX-ADDRESS1
    private int ixAddress1 = DefaultValues.BIN_INT_VAL;
    //Original name: IDSV0016
    private Idsv0016 idsv0016 = new Idsv0016();
    //Original name: PARAM-INFR-APPL
    private ParamInfrAppl paramInfrAppl = new ParamInfrAppl();
    //Original name: IJCCMQ04-VAR-AMBIENTE
    private Ijccmq04VarAmbiente ijccmq04VarAmbiente = new Ijccmq04VarAmbiente();
    /**Original name: W-EDIT-REASONS<br>
	 * <pre>----------------------------------------------------------------*
	 * --- AREE PER CHIAMATE A WEBSPHERE MQ
	 * ----------------------------------------------------------------*</pre>*/
    private String wEditReasons = DefaultValues.stringVal(Len.W_EDIT_REASONS);
    //Original name: W-AREA-PER-CHIAMATE-WSMQ
    private WAreaPerChiamateWsmq wAreaPerChiamateWsmq = new WAreaPerChiamateWsmq();

    //==== CONSTRUCTORS ====
    public Iabv0007() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabellaStatiBatchElementsIdx = 1; tabellaStatiBatchElementsIdx <= TABELLA_STATI_BATCH_ELEMENTS_MAXOCCURS; tabellaStatiBatchElementsIdx++) {
            tabellaStatiBatchElements[tabellaStatiBatchElementsIdx - 1] = new TabellaStatiBatchElements();
        }
        for (int tabellaStatiJobElementsIdx = 1; tabellaStatiJobElementsIdx <= TABELLA_STATI_JOB_ELEMENTS_MAXOCCURS; tabellaStatiJobElementsIdx++) {
            tabellaStatiJobElements[tabellaStatiJobElementsIdx - 1] = new TabellaStatiJobElements();
        }
        for (int wkStatiSospesiEleIdx = 1; wkStatiSospesiEleIdx <= WK_STATI_SOSPESI_ELE_MAXOCCURS; wkStatiSospesiEleIdx++) {
            wkStatiSospesiEle[wkStatiSospesiEleIdx - 1] = new WkStatiSospesiEle();
        }
    }

    public void setFsPara(String fsPara) {
        this.fsPara = Functions.subString(fsPara, Len.FS_PARA);
    }

    public String getFsPara() {
        return this.fsPara;
    }

    public String getFsParaFormatted() {
        return Functions.padBlanks(getFsPara(), Len.FS_PARA);
    }

    public void setFsReport01(String fsReport01) {
        this.fsReport01 = Functions.subString(fsReport01, Len.FS_REPORT01);
    }

    public String getFsReport01() {
        return this.fsReport01;
    }

    public String getFsReport01Formatted() {
        return Functions.padBlanks(getFsReport01(), Len.FS_REPORT01);
    }

    public void setWkSqlcode(long wkSqlcode) {
        this.wkSqlcode = PicFormatter.display("--(7)9").format(wkSqlcode).toString();
    }

    public long getWkSqlcode() {
        return PicParser.display("--(7)9").parseLong(this.wkSqlcode);
    }

    public String getWkSqlcodeFormatted() {
        return this.wkSqlcode;
    }

    public String getWkSqlcodeAsString() {
        return getWkSqlcodeFormatted();
    }

    public void setWkErrore(boolean wkErrore) {
        this.wkErrore = wkErrore;
    }

    public boolean isWkErrore() {
        return this.wkErrore;
    }

    public void setWkPgmErrore(String wkPgmErrore) {
        this.wkPgmErrore = Functions.subString(wkPgmErrore, Len.WK_PGM_ERRORE);
    }

    public String getWkPgmErrore() {
        return this.wkPgmErrore;
    }

    public String getWkPgmErroreFormatted() {
        return Functions.padBlanks(getWkPgmErrore(), Len.WK_PGM_ERRORE);
    }

    public void setWkPgmGuida(String wkPgmGuida) {
        this.wkPgmGuida = Functions.subString(wkPgmGuida, Len.WK_PGM_GUIDA);
    }

    public String getWkPgmGuida() {
        return this.wkPgmGuida;
    }

    public String getWkPgmGuidaFormatted() {
        return Functions.padBlanks(getWkPgmGuida(), Len.WK_PGM_GUIDA);
    }

    public void setWkPgmBusiness(String wkPgmBusiness) {
        this.wkPgmBusiness = Functions.subString(wkPgmBusiness, Len.WK_PGM_BUSINESS);
    }

    public String getWkPgmBusiness() {
        return this.wkPgmBusiness;
    }

    public String getWkPgmBusinessFormatted() {
        return Functions.padBlanks(getWkPgmBusiness(), Len.WK_PGM_BUSINESS);
    }

    public void setWkLabel(String wkLabel) {
        this.wkLabel = Functions.subString(wkLabel, Len.WK_LABEL);
    }

    public String getWkLabel() {
        return this.wkLabel;
    }

    public char getEsse() {
        return this.esse;
    }

    public char getEnne() {
        return this.enne;
    }

    public void setWkVersioning(int wkVersioning) {
        this.wkVersioning = wkVersioning;
    }

    public int getWkVersioning() {
        return this.wkVersioning;
    }

    public void setWkProtocol(boolean wkProtocol) {
        this.wkProtocol = wkProtocol;
    }

    public boolean isWkProtocol() {
        return this.wkProtocol;
    }

    public void setWkPrimaBufferSkTrovato(boolean wkPrimaBufferSkTrovato) {
        this.wkPrimaBufferSkTrovato = wkPrimaBufferSkTrovato;
    }

    public boolean isWkPrimaBufferSkTrovato() {
        return this.wkPrimaBufferSkTrovato;
    }

    public void setWkPrimaVolta(boolean wkPrimaVolta) {
        this.wkPrimaVolta = wkPrimaVolta;
    }

    public boolean isWkPrimaVolta() {
        return this.wkPrimaVolta;
    }

    public void setWkPrimaVoltaSuspend(boolean wkPrimaVoltaSuspend) {
        this.wkPrimaVoltaSuspend = wkPrimaVoltaSuspend;
    }

    public boolean isWkPrimaVoltaSuspend() {
        return this.wkPrimaVoltaSuspend;
    }

    public void setWkPostGuideXJobNotFound(boolean wkPostGuideXJobNotFound) {
        this.wkPostGuideXJobNotFound = wkPostGuideXJobNotFound;
    }

    public boolean isWkPostGuideXJobNotFound() {
        return this.wkPostGuideXJobNotFound;
    }

    public void setWkBatCollectionKey(boolean wkBatCollectionKey) {
        this.wkBatCollectionKey = wkBatCollectionKey;
    }

    public boolean isWkBatCollectionKey() {
        return this.wkBatCollectionKey;
    }

    public void setWkJobCollectionKey(boolean wkJobCollectionKey) {
        this.wkJobCollectionKey = wkJobCollectionKey;
    }

    public boolean isWkJobCollectionKey() {
        return this.wkJobCollectionKey;
    }

    public void setWkOpenParam(boolean wkOpenParam) {
        this.wkOpenParam = wkOpenParam;
    }

    public boolean isWkOpenParam() {
        return this.wkOpenParam;
    }

    public void setWkOpenReport(boolean wkOpenReport) {
        this.wkOpenReport = wkOpenReport;
    }

    public boolean isWkOpenReport() {
        return this.wkOpenReport;
    }

    public void setWkEofParam(boolean wkEofParam) {
        this.wkEofParam = wkEofParam;
    }

    public boolean isWkEofParam() {
        return this.wkEofParam;
    }

    public void setFlagInizioLettura(boolean flagInizioLettura) {
        this.flagInizioLettura = flagInizioLettura;
    }

    public boolean isFlagInizioLettura() {
        return this.flagInizioLettura;
    }

    public void setFlagFineLettura(boolean flagFineLettura) {
        this.flagFineLettura = flagFineLettura;
    }

    public boolean isFlagFineLettura() {
        return this.flagFineLettura;
    }

    public void setFlagPosizParam(boolean flagPosizParam) {
        this.flagPosizParam = flagPosizParam;
    }

    public boolean isFlagPosizParam() {
        return this.flagPosizParam;
    }

    public void setFlagVerificaBatch(boolean flagVerificaBatch) {
        this.flagVerificaBatch = flagVerificaBatch;
    }

    public boolean isFlagVerificaBatch() {
        return this.flagVerificaBatch;
    }

    public void setFlagRisultVerifElabState(boolean flagRisultVerifElabState) {
        this.flagRisultVerifElabState = flagRisultVerifElabState;
    }

    public boolean isFlagRisultVerifElabState() {
        return this.flagRisultVerifElabState;
    }

    public void setFlagWkBatchStateFound(boolean flagWkBatchStateFound) {
        this.flagWkBatchStateFound = flagWkBatchStateFound;
    }

    public boolean isFlagWkBatchStateFound() {
        return this.flagWkBatchStateFound;
    }

    public void setFlagVerificaPrenotazione(boolean flagVerificaPrenotazione) {
        this.flagVerificaPrenotazione = flagVerificaPrenotazione;
    }

    public boolean isFlagVerificaPrenotazione() {
        return this.flagVerificaPrenotazione;
    }

    public void setFlagElaboraBatch(boolean flagElaboraBatch) {
        this.flagElaboraBatch = flagElaboraBatch;
    }

    public boolean isFlagElaboraBatch() {
        return this.flagElaboraBatch;
    }

    public void setFlagAggiornaPrenotaz(boolean flagAggiornaPrenotaz) {
        this.flagAggiornaPrenotaz = flagAggiornaPrenotaz;
    }

    public boolean isFlagAggiornaPrenotaz() {
        return this.flagAggiornaPrenotaz;
    }

    public void setFlagConfermaBatchEseguito(boolean flagConfermaBatchEseguito) {
        this.flagConfermaBatchEseguito = flagConfermaBatchEseguito;
    }

    public boolean isFlagConfermaBatchEseguito() {
        return this.flagConfermaBatchEseguito;
    }

    public void setFlagSkipBatch(boolean flagSkipBatch) {
        this.flagSkipBatch = flagSkipBatch;
    }

    public boolean isFlagSkipBatch() {
        return this.flagSkipBatch;
    }

    public void setFlagEstraiJob(boolean flagEstraiJob) {
        this.flagEstraiJob = flagEstraiJob;
    }

    public boolean isFlagEstraiJob() {
        return this.flagEstraiJob;
    }

    public void setFlagEstraiRec(boolean flagEstraiRec) {
        this.flagEstraiRec = flagEstraiRec;
    }

    public boolean isFlagEstraiRec() {
        return this.flagEstraiRec;
    }

    public void setFlagFoundNotFound(boolean flagFoundNotFound) {
        this.flagFoundNotFound = flagFoundNotFound;
    }

    public boolean isFlagFoundNotFound() {
        return this.flagFoundNotFound;
    }

    public void setFlagFineBtcBatch(boolean flagFineBtcBatch) {
        this.flagFineBtcBatch = flagFineBtcBatch;
    }

    public boolean isFlagFineBtcBatch() {
        return this.flagFineBtcBatch;
    }

    public void setFlagLanciaBusiness(boolean flagLanciaBusiness) {
        this.flagLanciaBusiness = flagLanciaBusiness;
    }

    public boolean isFlagLanciaBusiness() {
        return this.flagLanciaBusiness;
    }

    public void setFlagLanciaRottura(boolean flagLanciaRottura) {
        this.flagLanciaRottura = flagLanciaRottura;
    }

    public boolean isFlagLanciaRottura() {
        return this.flagLanciaRottura;
    }

    public void setFlagFineAlimentazione(boolean flagFineAlimentazione) {
        this.flagFineAlimentazione = flagFineAlimentazione;
    }

    public boolean isFlagFineAlimentazione() {
        return this.flagFineAlimentazione;
    }

    public void setWkCommitFrequency(int wkCommitFrequency) {
        this.wkCommitFrequency = wkCommitFrequency;
    }

    public int getWkCommitFrequency() {
        return this.wkCommitFrequency;
    }

    public void setInitCommitFrequency() {
        wkCommitFrequency = INIT_COMMIT_FREQUENCY;
    }

    public void setWkDesBatchState(String wkDesBatchState) {
        this.wkDesBatchState = Functions.subString(wkDesBatchState, Len.WK_DES_BATCH_STATE);
    }

    public String getWkDesBatchState() {
        return this.wkDesBatchState;
    }

    public void setFlagUpdateForRestore(boolean flagUpdateForRestore) {
        this.flagUpdateForRestore = flagUpdateForRestore;
    }

    public boolean isFlagUpdateForRestore() {
        return this.flagUpdateForRestore;
    }

    public void setWkIdBatch(int wkIdBatch) {
        this.wkIdBatch = NumericDisplay.asString(wkIdBatch, Len.WK_ID_BATCH);
    }

    public int getWkIdBatch() {
        return NumericDisplay.asInt(this.wkIdBatch);
    }

    public String getWkIdBatchFormatted() {
        return this.wkIdBatch;
    }

    public String getWkIdBatchAsString() {
        return getWkIdBatchFormatted();
    }

    public void setWkIdJob(int wkIdJob) {
        this.wkIdJob = NumericDisplay.asString(wkIdJob, Len.WK_ID_JOB);
    }

    public int getWkIdJob() {
        return NumericDisplay.asInt(this.wkIdJob);
    }

    public String getWkIdJobFormatted() {
        return this.wkIdJob;
    }

    public String getWkIdJobAsString() {
        return getWkIdJobFormatted();
    }

    public void setWkIdJobUltCommitFormatted(String wkIdJobUltCommit) {
        this.wkIdJobUltCommit = Trunc.toUnsignedNumeric(wkIdJobUltCommit, Len.WK_ID_JOB_ULT_COMMIT);
    }

    public int getWkIdJobUltCommit() {
        return NumericDisplay.asInt(this.wkIdJobUltCommit);
    }

    public String getWkIdJobUltCommitFormatted() {
        return this.wkIdJobUltCommit;
    }

    public String getWkIdJobUltCommitAsString() {
        return getWkIdJobUltCommitFormatted();
    }

    public void setWkCodBatchState(char wkCodBatchState) {
        this.wkCodBatchState = wkCodBatchState;
    }

    public char getWkCodBatchState() {
        return this.wkCodBatchState;
    }

    public void setWkCodElabState(char wkCodElabState) {
        this.wkCodElabState = wkCodElabState;
    }

    public char getWkCodElabState() {
        return this.wkCodElabState;
    }

    public void setWkIdRich(int wkIdRich) {
        this.wkIdRich = wkIdRich;
    }

    public int getWkIdRich() {
        return this.wkIdRich;
    }

    public String getUltimoLancioXCloseFile() {
        return this.ultimoLancioXCloseFile;
    }

    public void setWsLungEffBlob(int wsLungEffBlob) {
        this.wsLungEffBlob = NumericDisplay.asString(wsLungEffBlob, Len.WS_LUNG_EFF_BLOB);
    }

    public int getWsLungEffBlob() {
        return NumericDisplay.asInt(this.wsLungEffBlob);
    }

    public String getWsLungEffBlobFormatted() {
        return this.wsLungEffBlob;
    }

    public char getWkAttivo() {
        return this.wkAttivo;
    }

    public void setWsStrDateNFormatted(String data) {
        byte[] buffer = new byte[Len.WS_STR_DATE_N];
        MarshalByte.writeString(buffer, 1, data, Len.WS_STR_DATE_N);
        setWsStrDateNBytes(buffer, 1);
    }

    public String getWsStrDateNFormatted() {
        return getWsDateNFormatted();
    }

    public void setWsStrDateNBytes(byte[] buffer, int offset) {
        int position = offset;
        wsDateN = MarshalByte.readFixedString(buffer, position, Len.WS_DATE_N);
    }

    public void setWsDateN(int wsDateN) {
        this.wsDateN = NumericDisplay.asString(wsDateN, Len.WS_DATE_N);
    }

    public int getWsDateN() {
        return NumericDisplay.asInt(this.wsDateN);
    }

    public String getWsDateNFormatted() {
        return this.wsDateN;
    }

    public void setWsDateX(String wsDateX) {
        this.wsDateX = Functions.subString(wsDateX, Len.WS_DATE_X);
    }

    public String getWsDateX() {
        return this.wsDateX;
    }

    public String getWsDateXFormatted() {
        return Functions.padBlanks(getWsDateX(), Len.WS_DATE_X);
    }

    public void setWsStrTimestampNFormatted(String data) {
        byte[] buffer = new byte[Len.WS_STR_TIMESTAMP_N];
        MarshalByte.writeString(buffer, 1, data, Len.WS_STR_TIMESTAMP_N);
        setWsStrTimestampNBytes(buffer, 1);
    }

    public String getWsStrTimestampNFormatted() {
        return getWsTimestampNFormatted();
    }

    public void setWsStrTimestampNBytes(byte[] buffer, int offset) {
        int position = offset;
        wsTimestampN = MarshalByte.readFixedString(buffer, position, Len.WS_TIMESTAMP_N);
    }

    public void setWsTimestampN(long wsTimestampN) {
        this.wsTimestampN = NumericDisplay.asString(wsTimestampN, Len.WS_TIMESTAMP_N);
    }

    public long getWsTimestampN() {
        return NumericDisplay.asLong(this.wsTimestampN);
    }

    public String getWsTimestampNFormatted() {
        return this.wsTimestampN;
    }

    public void setWsTimestampX(String wsTimestampX) {
        this.wsTimestampX = Functions.subString(wsTimestampX, Len.WS_TIMESTAMP_X);
    }

    public String getWsTimestampX() {
        return this.wsTimestampX;
    }

    public String getWsTimestampXFormatted() {
        return Functions.padBlanks(getWsTimestampX(), Len.WS_TIMESTAMP_X);
    }

    public void setWkCurrentPk(long wkCurrentPk) {
        this.wkCurrentPk = wkCurrentPk;
    }

    public long getWkCurrentPk() {
        return this.wkCurrentPk;
    }

    public short getWkLimiteSkedaParametro() {
        return this.wkLimiteSkedaParametro;
    }

    public short getWkLimiteStatiBatch() {
        return this.wkLimiteStatiBatch;
    }

    public short getWkLimiteStatiJob() {
        return this.wkLimiteStatiJob;
    }

    public short getWkLimiteStatiSosp() {
        return this.wkLimiteStatiSosp;
    }

    public void setIndMaxEleErrori(short indMaxEleErrori) {
        this.indMaxEleErrori = indMaxEleErrori;
    }

    public short getIndMaxEleErrori() {
        return this.indMaxEleErrori;
    }

    public void setIndSearch(short indSearch) {
        this.indSearch = indSearch;
    }

    public short getIndSearch() {
        return this.indSearch;
    }

    public void setIndLung(short indLung) {
        this.indLung = NumericDisplay.asString(indLung, Len.IND_LUNG);
    }

    public void setIndLungFormatted(String indLung) {
        this.indLung = Trunc.toUnsignedNumeric(indLung, Len.IND_LUNG);
    }

    public short getIndLung() {
        return NumericDisplay.asShort(this.indLung);
    }

    public void setIndPosizParam(short indPosizParam) {
        this.indPosizParam = indPosizParam;
    }

    public short getIndPosizParam() {
        return this.indPosizParam;
    }

    public void setIndStatiBatch(short indStatiBatch) {
        this.indStatiBatch = indStatiBatch;
    }

    public short getIndStatiBatch() {
        return this.indStatiBatch;
    }

    public void setIndStatiJob(short indStatiJob) {
        this.indStatiJob = indStatiJob;
    }

    public short getIndStatiJob() {
        return this.indStatiJob;
    }

    public void setIndStatiV0002(short indStatiV0002) {
        this.indStatiV0002 = indStatiV0002;
    }

    public short getIndStatiV0002() {
        return this.indStatiV0002;
    }

    public void setIndStatiSosp(short indStatiSosp) {
        this.indStatiSosp = indStatiSosp;
    }

    public short getIndStatiSosp() {
        return this.indStatiSosp;
    }

    public void setWkSeqSessione(int wkSeqSessione) {
        this.wkSeqSessione = NumericDisplay.asString(wkSeqSessione, Len.WK_SEQ_SESSIONE);
    }

    public int getWkSeqSessione() {
        return NumericDisplay.asInt(this.wkSeqSessione);
    }

    public String getWkSeqSessioneFormatted() {
        return this.wkSeqSessione;
    }

    public void setCntNumContrLettFormatted(String cntNumContrLett) {
        this.cntNumContrLett = Trunc.toUnsignedNumeric(cntNumContrLett, Len.CNT_NUM_CONTR_LETT);
    }

    public int getCntNumContrLett() {
        return NumericDisplay.asInt(this.cntNumContrLett);
    }

    public void setCntNumContrElabFormatted(String cntNumContrElab) {
        this.cntNumContrElab = Trunc.toUnsignedNumeric(cntNumContrElab, Len.CNT_NUM_CONTR_ELAB);
    }

    public int getCntNumContrElab() {
        return NumericDisplay.asInt(this.cntNumContrElab);
    }

    public void setCntNumOperazLette(int cntNumOperazLette) {
        this.cntNumOperazLette = NumericDisplay.asString(cntNumOperazLette, Len.CNT_NUM_OPERAZ_LETTE);
    }

    public void setCntNumOperazLetteFormatted(String cntNumOperazLette) {
        this.cntNumOperazLette = Trunc.toUnsignedNumeric(cntNumOperazLette, Len.CNT_NUM_OPERAZ_LETTE);
    }

    public int getCntNumOperazLette() {
        return NumericDisplay.asInt(this.cntNumOperazLette);
    }

    public String getCntNumOperazLetteFormatted() {
        return this.cntNumOperazLette;
    }

    public String getCntNumOperazLetteAsString() {
        return getCntNumOperazLetteFormatted();
    }

    public void setWkTimestampFormatted(String data) {
        byte[] buffer = new byte[Len.WK_TIMESTAMP];
        MarshalByte.writeString(buffer, 1, data, Len.WK_TIMESTAMP);
        setWkTimestampBytes(buffer, 1);
    }

    public String getWkTimestampFormatted() {
        return MarshalByteExt.bufferToStr(getWkTimestampBytes());
    }

    /**Original name: WK-TIMESTAMP<br>*/
    public byte[] getWkTimestampBytes() {
        byte[] buffer = new byte[Len.WK_TIMESTAMP];
        return getWkTimestampBytes(buffer, 1);
    }

    public void setWkTimestampBytes(byte[] buffer, int offset) {
        int position = offset;
        wkTimes14 = MarshalByte.readFixedString(buffer, position, Len.WK_TIMES14);
        position += Len.WK_TIMES14;
        wkTimes04 = MarshalByte.readFixedString(buffer, position, Len.WK_TIMES04);
    }

    public byte[] getWkTimestampBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wkTimes14, Len.WK_TIMES14);
        position += Len.WK_TIMES14;
        MarshalByte.writeString(buffer, position, wkTimes04, Len.WK_TIMES04);
        return buffer;
    }

    public void setWkSessioneFormatted(String data) {
        byte[] buffer = new byte[Len.WK_SESSIONE];
        MarshalByte.writeString(buffer, 1, data, Len.WK_SESSIONE);
        setWkSessioneBytes(buffer, 1);
    }

    public void setWkSessioneBytes(byte[] buffer, int offset) {
        int position = offset;
        wkSessioneN09 = MarshalByte.readFixedString(buffer, position, Len.WK_SESSIONE_N09);
        position += Len.WK_SESSIONE_N09;
        wkSessioneX11 = MarshalByte.readString(buffer, position, Len.WK_SESSIONE_X11);
    }

    public int getWkSessioneN09() {
        return NumericDisplay.asInt(this.wkSessioneN09);
    }

    public void setWkSessioneX11(String wkSessioneX11) {
        this.wkSessioneX11 = Functions.subString(wkSessioneX11, Len.WK_SESSIONE_X11);
    }

    public String getWkSessioneX11() {
        return this.wkSessioneX11;
    }

    public void setComodoCodCompAnia(int comodoCodCompAnia) {
        this.comodoCodCompAnia = NumericDisplay.asString(comodoCodCompAnia, Len.COMODO_COD_COMP_ANIA);
    }

    public void setComodoCodCompAniaFormatted(String comodoCodCompAnia) {
        this.comodoCodCompAnia = Trunc.toUnsignedNumeric(comodoCodCompAnia, Len.COMODO_COD_COMP_ANIA);
    }

    public int getComodoCodCompAnia() {
        return NumericDisplay.asInt(this.comodoCodCompAnia);
    }

    public String getComodoCodCompAniaFormatted() {
        return this.comodoCodCompAnia;
    }

    public String getComodoCodCompAniaAsString() {
        return getComodoCodCompAniaFormatted();
    }

    public void setComodoProgProtocol(int comodoProgProtocol) {
        this.comodoProgProtocol = NumericDisplay.asString(comodoProgProtocol, Len.COMODO_PROG_PROTOCOL);
    }

    public void setComodoProgProtocolFormatted(String comodoProgProtocol) {
        this.comodoProgProtocol = Trunc.toUnsignedNumeric(comodoProgProtocol, Len.COMODO_PROG_PROTOCOL);
    }

    public int getComodoProgProtocol() {
        return NumericDisplay.asInt(this.comodoProgProtocol);
    }

    public String getComodoProgProtocolFormatted() {
        return this.comodoProgProtocol;
    }

    public String getComodoProgProtocolAsString() {
        return getComodoProgProtocolFormatted();
    }

    public void setFlagDescErrMore100(boolean flagDescErrMore100) {
        this.flagDescErrMore100 = flagDescErrMore100;
    }

    public boolean isFlagDescErrMore100() {
        return this.flagDescErrMore100;
    }

    public void setFlagStampaTestataJcl(boolean flagStampaTestataJcl) {
        this.flagStampaTestataJcl = flagStampaTestataJcl;
    }

    public boolean isFlagStampaTestataJcl() {
        return this.flagStampaTestataJcl;
    }

    public void setFlagStampaLogoErrore(boolean flagStampaLogoErrore) {
        this.flagStampaLogoErrore = flagStampaLogoErrore;
    }

    public boolean isFlagStampaLogoErrore() {
        return this.flagStampaLogoErrore;
    }

    public void setFlagWarningTrovato(boolean flagWarningTrovato) {
        this.flagWarningTrovato = flagWarningTrovato;
    }

    public boolean isFlagWarningTrovato() {
        return this.flagWarningTrovato;
    }

    public void setContTabGuideLette(int contTabGuideLette) {
        this.contTabGuideLette = NumericDisplay.asString(contTabGuideLette, Len.CONT_TAB_GUIDE_LETTE);
    }

    public int getContTabGuideLette() {
        return NumericDisplay.asInt(this.contTabGuideLette);
    }

    public String getContTabGuideLetteFormatted() {
        return this.contTabGuideLette;
    }

    public String getContTabGuideLetteAsString() {
        return getContTabGuideLetteFormatted();
    }

    public void setContBusServEseg(int contBusServEseg) {
        this.contBusServEseg = contBusServEseg;
    }

    public int getContBusServEseg() {
        return this.contBusServEseg;
    }

    public void setContBusServEsitoOk(int contBusServEsitoOk) {
        this.contBusServEsitoOk = contBusServEsitoOk;
    }

    public int getContBusServEsitoOk() {
        return this.contBusServEsitoOk;
    }

    public void setContBusServWarning(int contBusServWarning) {
        this.contBusServWarning = contBusServWarning;
    }

    public int getContBusServWarning() {
        return this.contBusServWarning;
    }

    public void setContBusServEsitoKo(int contBusServEsitoKo) {
        this.contBusServEsitoKo = contBusServEsitoKo;
    }

    public int getContBusServEsitoKo() {
        return this.contBusServEsitoKo;
    }

    public String getConstSequentialGuide() {
        return this.constSequentialGuide;
    }

    public String getPgmIjcs0060() {
        return this.pgmIjcs0060;
    }

    public void setIxAddress(int ixAddress) {
        this.ixAddress = ixAddress;
    }

    public int getIxAddress() {
        return this.ixAddress;
    }

    public void setIxAddress1(int ixAddress1) {
        this.ixAddress1 = ixAddress1;
    }

    public int getIxAddress1() {
        return this.ixAddress1;
    }

    public void setwEditReasons(long wEditReasons) {
        this.wEditReasons = PicFormatter.display("Z(8)9-").format(wEditReasons).toString();
    }

    public long getwEditReasons() {
        return PicParser.display("Z(8)9-").parseLong(this.wEditReasons);
    }

    public String getwEditReasonsFormatted() {
        return this.wEditReasons;
    }

    public String getwEditReasonsAsString() {
        return getwEditReasonsFormatted();
    }

    public AreaIdsv0001 getAreaIdsv0001() {
        return areaIdsv0001;
    }

    public AreaXIabs0140 getAreaXIabs0140() {
        return areaXIabs0140;
    }

    public FlagAllineaStati getFlagAllineaStati() {
        return flagAllineaStati;
    }

    public FlagBatchExecutorStd getFlagBatchExecutorStd() {
        return flagBatchExecutorStd;
    }

    public FlagGestioneLanciBus getFlagGestioneLanciBus() {
        return flagGestioneLanciBus;
    }

    public FlagGestioneParallelismo getFlagGestioneParallelismo() {
        return flagGestioneParallelismo;
    }

    public FlagRc04 getFlagRc04() {
        return flagRc04;
    }

    public FlagReturnCode getFlagReturnCode() {
        return flagReturnCode;
    }

    public FlagTipoPgm getFlagTipoPgm() {
        return flagTipoPgm;
    }

    public FlagVerificaFinaleJobs getFlagVerificaFinaleJobs() {
        return flagVerificaFinaleJobs;
    }

    public Iabc0010 getIabc0010() {
        return iabc0010;
    }

    public Iabi0011Area getIabi0011Area() {
        return iabi0011Area;
    }

    public Iabv0002 getIabv0002() {
        return iabv0002;
    }

    public Iabv0003 getIabv0003() {
        return iabv0003;
    }

    public Iabv0004 getIabv0004() {
        return iabv0004;
    }

    public Iabv0010 getIabv0010() {
        return iabv0010;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv0003 getIdsv0003() {
        return idsv0003;
    }

    public Idsv0016 getIdsv0016() {
        return idsv0016;
    }

    public Idsv0301Lves0269 getIdsv0301() {
        return idsv0301;
    }

    public Idsv8888 getIdsv8888() {
        return idsv8888;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Ijccmq04VarAmbiente getIjccmq04VarAmbiente() {
        return ijccmq04VarAmbiente;
    }

    public LinkArea getLccc0090() {
        return lccc0090;
    }

    public ParamInfrAppl getParamInfrAppl() {
        return paramInfrAppl;
    }

    public TabellaStatiBatchElements getTabellaStatiBatchElements(int idx) {
        return tabellaStatiBatchElements[idx - 1];
    }

    public TabellaStatiJobElements getTabellaStatiJobElements(int idx) {
        return tabellaStatiJobElements[idx - 1];
    }

    public WkFlagGuideType getWkFlagGuideType() {
        return wkFlagGuideType;
    }

    public WkLimiteArrayBlob getWkLimiteArrayBlob() {
        return wkLimiteArrayBlob;
    }

    public WkLivelloGravita getWkLivelloGravita() {
        return wkLivelloGravita;
    }

    public WkLogErrore getWkLogErrore() {
        return wkLogErrore;
    }

    public WkLungParamX getWkLungParamX() {
        return wkLungParamX;
    }

    public WkSimulazione getWkSimulazione() {
        return wkSimulazione;
    }

    public WkStatiSospesiEle getWkStatiSospesiEle(int idx) {
        return wkStatiSospesiEle[idx - 1];
    }

    public WkTimestamp26 getWkTimestamp26() {
        return wkTimestamp26;
    }

    public WkTipoErrore getWkTipoErrore() {
        return wkTipoErrore;
    }

    public WsBufferWhereCondIabs0130 getWsBufferWhereCondIabs0130() {
        return wsBufferWhereCondIabs0130;
    }

    public WAreaPerChiamateWsmq getwAreaPerChiamateWsmq() {
        return wAreaPerChiamateWsmq;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_SQLCODE = 9;
        public static final int WK_NOME_TABELLA = 15;
        public static final int WK_LABEL = 30;
        public static final int WK_ID_BATCH = 9;
        public static final int WK_ID_JOB = 9;
        public static final int WK_ID_JOB_ULT_COMMIT = 9;
        public static final int WK_ID_RICH = 9;
        public static final int WK_DATABASE_NAME = 10;
        public static final int WK_USER = 8;
        public static final int WK_PASSWORD = 8;
        public static final int WK_IB_OGG_OLD = 100;
        public static final int WS_RISULTATO = 2;
        public static final int WS_DATE_SYSTEM_DB = 10;
        public static final int WS_TS_SYSTEM_DB = 26;
        public static final int WS_TS_SYSTEM = 18;
        public static final int WS_DATA_INIZIO_EFFETTO_DB = 10;
        public static final int WS_DATA_FINE_EFFETTO_DB = 10;
        public static final int WS_TS_DISPLAY = 18;
        public static final int WS_DATE_N = 8;
        public static final int WS_DATE_X = 10;
        public static final int WS_TIMESTAMP_N = 18;
        public static final int WS_TIMESTAMP_X = 26;
        public static final int WK_SEQ_SESSIONE = 9;
        public static final int CNT_NUM_CONTR_LETT = 9;
        public static final int CNT_NUM_CONTR_ELAB = 9;
        public static final int CNT_NUM_OPERAZ_LETTE = 9;
        public static final int WK_TIMES14 = 14;
        public static final int WK_TIMES04 = 4;
        public static final int WK_SESSIONE_N09 = 9;
        public static final int WK_SESSIONE_X11 = 11;
        public static final int COMODO_COD_COMP_ANIA = 9;
        public static final int COMODO_PROG_PROTOCOL = 9;
        public static final int CONT_TAB_GUIDE_LETTE = 9;
        public static final int CONT_BUS_SERV_ESEG = 9;
        public static final int CONT_BUS_SERV_ESITO_OK = 9;
        public static final int CONT_BUS_SERV_WARNING = 9;
        public static final int CONT_BUS_SERV_ESITO_KO = 9;
        public static final int W_EDIT_REASONS = 10;
        public static final int FS_PARA = 2;
        public static final int WK_DES_BATCH_STATE = 50;
        public static final int IND_POSIZ_PARAM = 3;
        public static final int IND_LUNG = 1;
        public static final int WK_PGM_BUSINESS = 8;
        public static final int WK_PGM_GUIDA = 8;
        public static final int WK_PGM_ERRORE = 8;
        public static final int FS_REPORT01 = 2;
        public static final int WK_TIMESTAMP = WK_TIMES14 + WK_TIMES04;
        public static final int WK_SESSIONE = WK_SESSIONE_N09 + WK_SESSIONE_X11;
        public static final int WS_STR_DATE_N = WS_DATE_N;
        public static final int WS_STR_TIMESTAMP_N = WS_TIMESTAMP_N;
        public static final int WS_LUNG_EFF_BLOB = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
