package it.accenture.jnais.copy;

import it.accenture.jnais.ws.redefines.WpagDtProsBnsFed;
import it.accenture.jnais.ws.redefines.WpagDtProsBnsRic;
import it.accenture.jnais.ws.redefines.WpagDtProsCedola;

/**Original name: WPAG-DATI-PAR-MOVIM<br>
 * Variable: WPAG-DATI-PAR-MOVIM from copybook LVEC0268<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WpagDatiParMovim {

    //==== PROPERTIES ====
    //Original name: WPAG-DT-PROS-BNS-FED
    private WpagDtProsBnsFed wpagDtProsBnsFed = new WpagDtProsBnsFed();
    //Original name: WPAG-DT-PROS-BNS-RIC
    private WpagDtProsBnsRic wpagDtProsBnsRic = new WpagDtProsBnsRic();
    //Original name: WPAG-DT-PROS-CEDOLA
    private WpagDtProsCedola wpagDtProsCedola = new WpagDtProsCedola();

    //==== METHODS ====
    public void setWpagDatiParMovimBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagDtProsBnsFed.setWpagDtProsBnsFedFromBuffer(buffer, position);
        position += WpagDtProsBnsFed.Len.WPAG_DT_PROS_BNS_FED;
        wpagDtProsBnsRic.setWpagDtProsBnsRicFromBuffer(buffer, position);
        position += WpagDtProsBnsRic.Len.WPAG_DT_PROS_BNS_RIC;
        wpagDtProsCedola.setWpagDtProsCedolaFromBuffer(buffer, position);
    }

    public byte[] getWpagDatiParMovimBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagDtProsBnsFed.getWpagDtProsBnsFedAsBuffer(buffer, position);
        position += WpagDtProsBnsFed.Len.WPAG_DT_PROS_BNS_FED;
        wpagDtProsBnsRic.getWpagDtProsBnsRicAsBuffer(buffer, position);
        position += WpagDtProsBnsRic.Len.WPAG_DT_PROS_BNS_RIC;
        wpagDtProsCedola.getWpagDtProsCedolaAsBuffer(buffer, position);
        return buffer;
    }

    public void initWpagDatiParMovimSpaces() {
        wpagDtProsBnsFed.initWpagDtProsBnsFedSpaces();
        wpagDtProsBnsRic.initWpagDtProsBnsRicSpaces();
        wpagDtProsCedola.initWpagDtProsCedolaSpaces();
    }

    public WpagDtProsBnsFed getWpagDtProsBnsFed() {
        return wpagDtProsBnsFed;
    }

    public WpagDtProsBnsRic getWpagDtProsBnsRic() {
        return wpagDtProsBnsRic;
    }

    public WpagDtProsCedola getWpagDtProsCedola() {
        return wpagDtProsCedola;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_DATI_PAR_MOVIM = WpagDtProsBnsFed.Len.WPAG_DT_PROS_BNS_FED + WpagDtProsBnsRic.Len.WPAG_DT_PROS_BNS_RIC + WpagDtProsCedola.Len.WPAG_DT_PROS_CEDOLA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
