package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WdtcAcqExp;
import it.accenture.jnais.ws.redefines.WdtcCarAcq;
import it.accenture.jnais.ws.redefines.WdtcCarGest;
import it.accenture.jnais.ws.redefines.WdtcCarIas;
import it.accenture.jnais.ws.redefines.WdtcCarInc;
import it.accenture.jnais.ws.redefines.WdtcCnbtAntirac;
import it.accenture.jnais.ws.redefines.WdtcCommisInter;
import it.accenture.jnais.ws.redefines.WdtcDir;
import it.accenture.jnais.ws.redefines.WdtcDtEndCop;
import it.accenture.jnais.ws.redefines.WdtcDtEsiTit;
import it.accenture.jnais.ws.redefines.WdtcDtIniCop;
import it.accenture.jnais.ws.redefines.WdtcFrqMovi;
import it.accenture.jnais.ws.redefines.WdtcIdMoviChiu;
import it.accenture.jnais.ws.redefines.WdtcImpAder;
import it.accenture.jnais.ws.redefines.WdtcImpAz;
import it.accenture.jnais.ws.redefines.WdtcImpTfr;
import it.accenture.jnais.ws.redefines.WdtcImpTfrStrc;
import it.accenture.jnais.ws.redefines.WdtcImpTrasfe;
import it.accenture.jnais.ws.redefines.WdtcImpVolo;
import it.accenture.jnais.ws.redefines.WdtcIntrFraz;
import it.accenture.jnais.ws.redefines.WdtcIntrMora;
import it.accenture.jnais.ws.redefines.WdtcIntrRetdt;
import it.accenture.jnais.ws.redefines.WdtcIntrRiat;
import it.accenture.jnais.ws.redefines.WdtcManfeeAntic;
import it.accenture.jnais.ws.redefines.WdtcManfeeRec;
import it.accenture.jnais.ws.redefines.WdtcManfeeRicor;
import it.accenture.jnais.ws.redefines.WdtcNumGgRitardoPag;
import it.accenture.jnais.ws.redefines.WdtcNumGgRival;
import it.accenture.jnais.ws.redefines.WdtcPreNet;
import it.accenture.jnais.ws.redefines.WdtcPrePpIas;
import it.accenture.jnais.ws.redefines.WdtcPreSoloRsh;
import it.accenture.jnais.ws.redefines.WdtcPreTot;
import it.accenture.jnais.ws.redefines.WdtcProvAcq1aa;
import it.accenture.jnais.ws.redefines.WdtcProvAcq2aa;
import it.accenture.jnais.ws.redefines.WdtcProvDaRec;
import it.accenture.jnais.ws.redefines.WdtcProvInc;
import it.accenture.jnais.ws.redefines.WdtcProvRicor;
import it.accenture.jnais.ws.redefines.WdtcRemunAss;
import it.accenture.jnais.ws.redefines.WdtcSoprAlt;
import it.accenture.jnais.ws.redefines.WdtcSoprProf;
import it.accenture.jnais.ws.redefines.WdtcSoprSan;
import it.accenture.jnais.ws.redefines.WdtcSoprSpo;
import it.accenture.jnais.ws.redefines.WdtcSoprTec;
import it.accenture.jnais.ws.redefines.WdtcSpeAge;
import it.accenture.jnais.ws.redefines.WdtcSpeMed;
import it.accenture.jnais.ws.redefines.WdtcTax;
import it.accenture.jnais.ws.redefines.WdtcTotIntrPrest;

/**Original name: WDTC-DATI<br>
 * Variable: WDTC-DATI from copybook LCCVDTC1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WdtcDati {

    //==== PROPERTIES ====
    //Original name: WDTC-ID-DETT-TIT-CONT
    private int wdtcIdDettTitCont = DefaultValues.INT_VAL;
    //Original name: WDTC-ID-TIT-CONT
    private int wdtcIdTitCont = DefaultValues.INT_VAL;
    //Original name: WDTC-ID-OGG
    private int wdtcIdOgg = DefaultValues.INT_VAL;
    //Original name: WDTC-TP-OGG
    private String wdtcTpOgg = DefaultValues.stringVal(Len.WDTC_TP_OGG);
    //Original name: WDTC-ID-MOVI-CRZ
    private int wdtcIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WDTC-ID-MOVI-CHIU
    private WdtcIdMoviChiu wdtcIdMoviChiu = new WdtcIdMoviChiu();
    //Original name: WDTC-DT-INI-EFF
    private int wdtcDtIniEff = DefaultValues.INT_VAL;
    //Original name: WDTC-DT-END-EFF
    private int wdtcDtEndEff = DefaultValues.INT_VAL;
    //Original name: WDTC-COD-COMP-ANIA
    private int wdtcCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WDTC-DT-INI-COP
    private WdtcDtIniCop wdtcDtIniCop = new WdtcDtIniCop();
    //Original name: WDTC-DT-END-COP
    private WdtcDtEndCop wdtcDtEndCop = new WdtcDtEndCop();
    //Original name: WDTC-PRE-NET
    private WdtcPreNet wdtcPreNet = new WdtcPreNet();
    //Original name: WDTC-INTR-FRAZ
    private WdtcIntrFraz wdtcIntrFraz = new WdtcIntrFraz();
    //Original name: WDTC-INTR-MORA
    private WdtcIntrMora wdtcIntrMora = new WdtcIntrMora();
    //Original name: WDTC-INTR-RETDT
    private WdtcIntrRetdt wdtcIntrRetdt = new WdtcIntrRetdt();
    //Original name: WDTC-INTR-RIAT
    private WdtcIntrRiat wdtcIntrRiat = new WdtcIntrRiat();
    //Original name: WDTC-DIR
    private WdtcDir wdtcDir = new WdtcDir();
    //Original name: WDTC-SPE-MED
    private WdtcSpeMed wdtcSpeMed = new WdtcSpeMed();
    //Original name: WDTC-TAX
    private WdtcTax wdtcTax = new WdtcTax();
    //Original name: WDTC-SOPR-SAN
    private WdtcSoprSan wdtcSoprSan = new WdtcSoprSan();
    //Original name: WDTC-SOPR-SPO
    private WdtcSoprSpo wdtcSoprSpo = new WdtcSoprSpo();
    //Original name: WDTC-SOPR-TEC
    private WdtcSoprTec wdtcSoprTec = new WdtcSoprTec();
    //Original name: WDTC-SOPR-PROF
    private WdtcSoprProf wdtcSoprProf = new WdtcSoprProf();
    //Original name: WDTC-SOPR-ALT
    private WdtcSoprAlt wdtcSoprAlt = new WdtcSoprAlt();
    //Original name: WDTC-PRE-TOT
    private WdtcPreTot wdtcPreTot = new WdtcPreTot();
    //Original name: WDTC-PRE-PP-IAS
    private WdtcPrePpIas wdtcPrePpIas = new WdtcPrePpIas();
    //Original name: WDTC-PRE-SOLO-RSH
    private WdtcPreSoloRsh wdtcPreSoloRsh = new WdtcPreSoloRsh();
    //Original name: WDTC-CAR-ACQ
    private WdtcCarAcq wdtcCarAcq = new WdtcCarAcq();
    //Original name: WDTC-CAR-GEST
    private WdtcCarGest wdtcCarGest = new WdtcCarGest();
    //Original name: WDTC-CAR-INC
    private WdtcCarInc wdtcCarInc = new WdtcCarInc();
    //Original name: WDTC-PROV-ACQ-1AA
    private WdtcProvAcq1aa wdtcProvAcq1aa = new WdtcProvAcq1aa();
    //Original name: WDTC-PROV-ACQ-2AA
    private WdtcProvAcq2aa wdtcProvAcq2aa = new WdtcProvAcq2aa();
    //Original name: WDTC-PROV-RICOR
    private WdtcProvRicor wdtcProvRicor = new WdtcProvRicor();
    //Original name: WDTC-PROV-INC
    private WdtcProvInc wdtcProvInc = new WdtcProvInc();
    //Original name: WDTC-PROV-DA-REC
    private WdtcProvDaRec wdtcProvDaRec = new WdtcProvDaRec();
    //Original name: WDTC-COD-DVS
    private String wdtcCodDvs = DefaultValues.stringVal(Len.WDTC_COD_DVS);
    //Original name: WDTC-FRQ-MOVI
    private WdtcFrqMovi wdtcFrqMovi = new WdtcFrqMovi();
    //Original name: WDTC-TP-RGM-FISC
    private String wdtcTpRgmFisc = DefaultValues.stringVal(Len.WDTC_TP_RGM_FISC);
    //Original name: WDTC-COD-TARI
    private String wdtcCodTari = DefaultValues.stringVal(Len.WDTC_COD_TARI);
    //Original name: WDTC-TP-STAT-TIT
    private String wdtcTpStatTit = DefaultValues.stringVal(Len.WDTC_TP_STAT_TIT);
    //Original name: WDTC-IMP-AZ
    private WdtcImpAz wdtcImpAz = new WdtcImpAz();
    //Original name: WDTC-IMP-ADER
    private WdtcImpAder wdtcImpAder = new WdtcImpAder();
    //Original name: WDTC-IMP-TFR
    private WdtcImpTfr wdtcImpTfr = new WdtcImpTfr();
    //Original name: WDTC-IMP-VOLO
    private WdtcImpVolo wdtcImpVolo = new WdtcImpVolo();
    //Original name: WDTC-MANFEE-ANTIC
    private WdtcManfeeAntic wdtcManfeeAntic = new WdtcManfeeAntic();
    //Original name: WDTC-MANFEE-RICOR
    private WdtcManfeeRicor wdtcManfeeRicor = new WdtcManfeeRicor();
    //Original name: WDTC-MANFEE-REC
    private WdtcManfeeRec wdtcManfeeRec = new WdtcManfeeRec();
    //Original name: WDTC-DT-ESI-TIT
    private WdtcDtEsiTit wdtcDtEsiTit = new WdtcDtEsiTit();
    //Original name: WDTC-SPE-AGE
    private WdtcSpeAge wdtcSpeAge = new WdtcSpeAge();
    //Original name: WDTC-CAR-IAS
    private WdtcCarIas wdtcCarIas = new WdtcCarIas();
    //Original name: WDTC-TOT-INTR-PREST
    private WdtcTotIntrPrest wdtcTotIntrPrest = new WdtcTotIntrPrest();
    //Original name: WDTC-DS-RIGA
    private long wdtcDsRiga = DefaultValues.LONG_VAL;
    //Original name: WDTC-DS-OPER-SQL
    private char wdtcDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WDTC-DS-VER
    private int wdtcDsVer = DefaultValues.INT_VAL;
    //Original name: WDTC-DS-TS-INI-CPTZ
    private long wdtcDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WDTC-DS-TS-END-CPTZ
    private long wdtcDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WDTC-DS-UTENTE
    private String wdtcDsUtente = DefaultValues.stringVal(Len.WDTC_DS_UTENTE);
    //Original name: WDTC-DS-STATO-ELAB
    private char wdtcDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WDTC-IMP-TRASFE
    private WdtcImpTrasfe wdtcImpTrasfe = new WdtcImpTrasfe();
    //Original name: WDTC-IMP-TFR-STRC
    private WdtcImpTfrStrc wdtcImpTfrStrc = new WdtcImpTfrStrc();
    //Original name: WDTC-NUM-GG-RITARDO-PAG
    private WdtcNumGgRitardoPag wdtcNumGgRitardoPag = new WdtcNumGgRitardoPag();
    //Original name: WDTC-NUM-GG-RIVAL
    private WdtcNumGgRival wdtcNumGgRival = new WdtcNumGgRival();
    //Original name: WDTC-ACQ-EXP
    private WdtcAcqExp wdtcAcqExp = new WdtcAcqExp();
    //Original name: WDTC-REMUN-ASS
    private WdtcRemunAss wdtcRemunAss = new WdtcRemunAss();
    //Original name: WDTC-COMMIS-INTER
    private WdtcCommisInter wdtcCommisInter = new WdtcCommisInter();
    //Original name: WDTC-CNBT-ANTIRAC
    private WdtcCnbtAntirac wdtcCnbtAntirac = new WdtcCnbtAntirac();

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wdtcIdDettTitCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDTC_ID_DETT_TIT_CONT, 0);
        position += Len.WDTC_ID_DETT_TIT_CONT;
        wdtcIdTitCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDTC_ID_TIT_CONT, 0);
        position += Len.WDTC_ID_TIT_CONT;
        wdtcIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDTC_ID_OGG, 0);
        position += Len.WDTC_ID_OGG;
        wdtcTpOgg = MarshalByte.readString(buffer, position, Len.WDTC_TP_OGG);
        position += Len.WDTC_TP_OGG;
        wdtcIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDTC_ID_MOVI_CRZ, 0);
        position += Len.WDTC_ID_MOVI_CRZ;
        wdtcIdMoviChiu.setWdtcIdMoviChiuFromBuffer(buffer, position);
        position += WdtcIdMoviChiu.Len.WDTC_ID_MOVI_CHIU;
        wdtcDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDTC_DT_INI_EFF, 0);
        position += Len.WDTC_DT_INI_EFF;
        wdtcDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDTC_DT_END_EFF, 0);
        position += Len.WDTC_DT_END_EFF;
        wdtcCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDTC_COD_COMP_ANIA, 0);
        position += Len.WDTC_COD_COMP_ANIA;
        wdtcDtIniCop.setWdtcDtIniCopFromBuffer(buffer, position);
        position += WdtcDtIniCop.Len.WDTC_DT_INI_COP;
        wdtcDtEndCop.setWdtcDtEndCopFromBuffer(buffer, position);
        position += WdtcDtEndCop.Len.WDTC_DT_END_COP;
        wdtcPreNet.setWdtcPreNetFromBuffer(buffer, position);
        position += WdtcPreNet.Len.WDTC_PRE_NET;
        wdtcIntrFraz.setWdtcIntrFrazFromBuffer(buffer, position);
        position += WdtcIntrFraz.Len.WDTC_INTR_FRAZ;
        wdtcIntrMora.setWdtcIntrMoraFromBuffer(buffer, position);
        position += WdtcIntrMora.Len.WDTC_INTR_MORA;
        wdtcIntrRetdt.setWdtcIntrRetdtFromBuffer(buffer, position);
        position += WdtcIntrRetdt.Len.WDTC_INTR_RETDT;
        wdtcIntrRiat.setWdtcIntrRiatFromBuffer(buffer, position);
        position += WdtcIntrRiat.Len.WDTC_INTR_RIAT;
        wdtcDir.setWdtcDirFromBuffer(buffer, position);
        position += WdtcDir.Len.WDTC_DIR;
        wdtcSpeMed.setWdtcSpeMedFromBuffer(buffer, position);
        position += WdtcSpeMed.Len.WDTC_SPE_MED;
        wdtcTax.setWdtcTaxFromBuffer(buffer, position);
        position += WdtcTax.Len.WDTC_TAX;
        wdtcSoprSan.setWdtcSoprSanFromBuffer(buffer, position);
        position += WdtcSoprSan.Len.WDTC_SOPR_SAN;
        wdtcSoprSpo.setWdtcSoprSpoFromBuffer(buffer, position);
        position += WdtcSoprSpo.Len.WDTC_SOPR_SPO;
        wdtcSoprTec.setWdtcSoprTecFromBuffer(buffer, position);
        position += WdtcSoprTec.Len.WDTC_SOPR_TEC;
        wdtcSoprProf.setWdtcSoprProfFromBuffer(buffer, position);
        position += WdtcSoprProf.Len.WDTC_SOPR_PROF;
        wdtcSoprAlt.setWdtcSoprAltFromBuffer(buffer, position);
        position += WdtcSoprAlt.Len.WDTC_SOPR_ALT;
        wdtcPreTot.setWdtcPreTotFromBuffer(buffer, position);
        position += WdtcPreTot.Len.WDTC_PRE_TOT;
        wdtcPrePpIas.setWdtcPrePpIasFromBuffer(buffer, position);
        position += WdtcPrePpIas.Len.WDTC_PRE_PP_IAS;
        wdtcPreSoloRsh.setWdtcPreSoloRshFromBuffer(buffer, position);
        position += WdtcPreSoloRsh.Len.WDTC_PRE_SOLO_RSH;
        wdtcCarAcq.setWdtcCarAcqFromBuffer(buffer, position);
        position += WdtcCarAcq.Len.WDTC_CAR_ACQ;
        wdtcCarGest.setWdtcCarGestFromBuffer(buffer, position);
        position += WdtcCarGest.Len.WDTC_CAR_GEST;
        wdtcCarInc.setWdtcCarIncFromBuffer(buffer, position);
        position += WdtcCarInc.Len.WDTC_CAR_INC;
        wdtcProvAcq1aa.setWdtcProvAcq1aaFromBuffer(buffer, position);
        position += WdtcProvAcq1aa.Len.WDTC_PROV_ACQ1AA;
        wdtcProvAcq2aa.setWdtcProvAcq2aaFromBuffer(buffer, position);
        position += WdtcProvAcq2aa.Len.WDTC_PROV_ACQ2AA;
        wdtcProvRicor.setWdtcProvRicorFromBuffer(buffer, position);
        position += WdtcProvRicor.Len.WDTC_PROV_RICOR;
        wdtcProvInc.setWdtcProvIncFromBuffer(buffer, position);
        position += WdtcProvInc.Len.WDTC_PROV_INC;
        wdtcProvDaRec.setWdtcProvDaRecFromBuffer(buffer, position);
        position += WdtcProvDaRec.Len.WDTC_PROV_DA_REC;
        wdtcCodDvs = MarshalByte.readString(buffer, position, Len.WDTC_COD_DVS);
        position += Len.WDTC_COD_DVS;
        wdtcFrqMovi.setWdtcFrqMoviFromBuffer(buffer, position);
        position += WdtcFrqMovi.Len.WDTC_FRQ_MOVI;
        wdtcTpRgmFisc = MarshalByte.readString(buffer, position, Len.WDTC_TP_RGM_FISC);
        position += Len.WDTC_TP_RGM_FISC;
        wdtcCodTari = MarshalByte.readString(buffer, position, Len.WDTC_COD_TARI);
        position += Len.WDTC_COD_TARI;
        wdtcTpStatTit = MarshalByte.readString(buffer, position, Len.WDTC_TP_STAT_TIT);
        position += Len.WDTC_TP_STAT_TIT;
        wdtcImpAz.setWdtcImpAzFromBuffer(buffer, position);
        position += WdtcImpAz.Len.WDTC_IMP_AZ;
        wdtcImpAder.setWdtcImpAderFromBuffer(buffer, position);
        position += WdtcImpAder.Len.WDTC_IMP_ADER;
        wdtcImpTfr.setWdtcImpTfrFromBuffer(buffer, position);
        position += WdtcImpTfr.Len.WDTC_IMP_TFR;
        wdtcImpVolo.setWdtcImpVoloFromBuffer(buffer, position);
        position += WdtcImpVolo.Len.WDTC_IMP_VOLO;
        wdtcManfeeAntic.setWdtcManfeeAnticFromBuffer(buffer, position);
        position += WdtcManfeeAntic.Len.WDTC_MANFEE_ANTIC;
        wdtcManfeeRicor.setWdtcManfeeRicorFromBuffer(buffer, position);
        position += WdtcManfeeRicor.Len.WDTC_MANFEE_RICOR;
        wdtcManfeeRec.setWdtcManfeeRecFromBuffer(buffer, position);
        position += WdtcManfeeRec.Len.WDTC_MANFEE_REC;
        wdtcDtEsiTit.setWdtcDtEsiTitFromBuffer(buffer, position);
        position += WdtcDtEsiTit.Len.WDTC_DT_ESI_TIT;
        wdtcSpeAge.setWdtcSpeAgeFromBuffer(buffer, position);
        position += WdtcSpeAge.Len.WDTC_SPE_AGE;
        wdtcCarIas.setWdtcCarIasFromBuffer(buffer, position);
        position += WdtcCarIas.Len.WDTC_CAR_IAS;
        wdtcTotIntrPrest.setWdtcTotIntrPrestFromBuffer(buffer, position);
        position += WdtcTotIntrPrest.Len.WDTC_TOT_INTR_PREST;
        wdtcDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDTC_DS_RIGA, 0);
        position += Len.WDTC_DS_RIGA;
        wdtcDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdtcDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDTC_DS_VER, 0);
        position += Len.WDTC_DS_VER;
        wdtcDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDTC_DS_TS_INI_CPTZ, 0);
        position += Len.WDTC_DS_TS_INI_CPTZ;
        wdtcDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDTC_DS_TS_END_CPTZ, 0);
        position += Len.WDTC_DS_TS_END_CPTZ;
        wdtcDsUtente = MarshalByte.readString(buffer, position, Len.WDTC_DS_UTENTE);
        position += Len.WDTC_DS_UTENTE;
        wdtcDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdtcImpTrasfe.setWdtcImpTrasfeFromBuffer(buffer, position);
        position += WdtcImpTrasfe.Len.WDTC_IMP_TRASFE;
        wdtcImpTfrStrc.setWdtcImpTfrStrcFromBuffer(buffer, position);
        position += WdtcImpTfrStrc.Len.WDTC_IMP_TFR_STRC;
        wdtcNumGgRitardoPag.setWdtcNumGgRitardoPagFromBuffer(buffer, position);
        position += WdtcNumGgRitardoPag.Len.WDTC_NUM_GG_RITARDO_PAG;
        wdtcNumGgRival.setWdtcNumGgRivalFromBuffer(buffer, position);
        position += WdtcNumGgRival.Len.WDTC_NUM_GG_RIVAL;
        wdtcAcqExp.setWdtcAcqExpFromBuffer(buffer, position);
        position += WdtcAcqExp.Len.WDTC_ACQ_EXP;
        wdtcRemunAss.setWdtcRemunAssFromBuffer(buffer, position);
        position += WdtcRemunAss.Len.WDTC_REMUN_ASS;
        wdtcCommisInter.setWdtcCommisInterFromBuffer(buffer, position);
        position += WdtcCommisInter.Len.WDTC_COMMIS_INTER;
        wdtcCnbtAntirac.setWdtcCnbtAntiracFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wdtcIdDettTitCont, Len.Int.WDTC_ID_DETT_TIT_CONT, 0);
        position += Len.WDTC_ID_DETT_TIT_CONT;
        MarshalByte.writeIntAsPacked(buffer, position, wdtcIdTitCont, Len.Int.WDTC_ID_TIT_CONT, 0);
        position += Len.WDTC_ID_TIT_CONT;
        MarshalByte.writeIntAsPacked(buffer, position, wdtcIdOgg, Len.Int.WDTC_ID_OGG, 0);
        position += Len.WDTC_ID_OGG;
        MarshalByte.writeString(buffer, position, wdtcTpOgg, Len.WDTC_TP_OGG);
        position += Len.WDTC_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, wdtcIdMoviCrz, Len.Int.WDTC_ID_MOVI_CRZ, 0);
        position += Len.WDTC_ID_MOVI_CRZ;
        wdtcIdMoviChiu.getWdtcIdMoviChiuAsBuffer(buffer, position);
        position += WdtcIdMoviChiu.Len.WDTC_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wdtcDtIniEff, Len.Int.WDTC_DT_INI_EFF, 0);
        position += Len.WDTC_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wdtcDtEndEff, Len.Int.WDTC_DT_END_EFF, 0);
        position += Len.WDTC_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wdtcCodCompAnia, Len.Int.WDTC_COD_COMP_ANIA, 0);
        position += Len.WDTC_COD_COMP_ANIA;
        wdtcDtIniCop.getWdtcDtIniCopAsBuffer(buffer, position);
        position += WdtcDtIniCop.Len.WDTC_DT_INI_COP;
        wdtcDtEndCop.getWdtcDtEndCopAsBuffer(buffer, position);
        position += WdtcDtEndCop.Len.WDTC_DT_END_COP;
        wdtcPreNet.getWdtcPreNetAsBuffer(buffer, position);
        position += WdtcPreNet.Len.WDTC_PRE_NET;
        wdtcIntrFraz.getWdtcIntrFrazAsBuffer(buffer, position);
        position += WdtcIntrFraz.Len.WDTC_INTR_FRAZ;
        wdtcIntrMora.getWdtcIntrMoraAsBuffer(buffer, position);
        position += WdtcIntrMora.Len.WDTC_INTR_MORA;
        wdtcIntrRetdt.getWdtcIntrRetdtAsBuffer(buffer, position);
        position += WdtcIntrRetdt.Len.WDTC_INTR_RETDT;
        wdtcIntrRiat.getWdtcIntrRiatAsBuffer(buffer, position);
        position += WdtcIntrRiat.Len.WDTC_INTR_RIAT;
        wdtcDir.getWdtcDirAsBuffer(buffer, position);
        position += WdtcDir.Len.WDTC_DIR;
        wdtcSpeMed.getWdtcSpeMedAsBuffer(buffer, position);
        position += WdtcSpeMed.Len.WDTC_SPE_MED;
        wdtcTax.getWdtcTaxAsBuffer(buffer, position);
        position += WdtcTax.Len.WDTC_TAX;
        wdtcSoprSan.getWdtcSoprSanAsBuffer(buffer, position);
        position += WdtcSoprSan.Len.WDTC_SOPR_SAN;
        wdtcSoprSpo.getWdtcSoprSpoAsBuffer(buffer, position);
        position += WdtcSoprSpo.Len.WDTC_SOPR_SPO;
        wdtcSoprTec.getWdtcSoprTecAsBuffer(buffer, position);
        position += WdtcSoprTec.Len.WDTC_SOPR_TEC;
        wdtcSoprProf.getWdtcSoprProfAsBuffer(buffer, position);
        position += WdtcSoprProf.Len.WDTC_SOPR_PROF;
        wdtcSoprAlt.getWdtcSoprAltAsBuffer(buffer, position);
        position += WdtcSoprAlt.Len.WDTC_SOPR_ALT;
        wdtcPreTot.getWdtcPreTotAsBuffer(buffer, position);
        position += WdtcPreTot.Len.WDTC_PRE_TOT;
        wdtcPrePpIas.getWdtcPrePpIasAsBuffer(buffer, position);
        position += WdtcPrePpIas.Len.WDTC_PRE_PP_IAS;
        wdtcPreSoloRsh.getWdtcPreSoloRshAsBuffer(buffer, position);
        position += WdtcPreSoloRsh.Len.WDTC_PRE_SOLO_RSH;
        wdtcCarAcq.getWdtcCarAcqAsBuffer(buffer, position);
        position += WdtcCarAcq.Len.WDTC_CAR_ACQ;
        wdtcCarGest.getWdtcCarGestAsBuffer(buffer, position);
        position += WdtcCarGest.Len.WDTC_CAR_GEST;
        wdtcCarInc.getWdtcCarIncAsBuffer(buffer, position);
        position += WdtcCarInc.Len.WDTC_CAR_INC;
        wdtcProvAcq1aa.getWdtcProvAcq1aaAsBuffer(buffer, position);
        position += WdtcProvAcq1aa.Len.WDTC_PROV_ACQ1AA;
        wdtcProvAcq2aa.getWdtcProvAcq2aaAsBuffer(buffer, position);
        position += WdtcProvAcq2aa.Len.WDTC_PROV_ACQ2AA;
        wdtcProvRicor.getWdtcProvRicorAsBuffer(buffer, position);
        position += WdtcProvRicor.Len.WDTC_PROV_RICOR;
        wdtcProvInc.getWdtcProvIncAsBuffer(buffer, position);
        position += WdtcProvInc.Len.WDTC_PROV_INC;
        wdtcProvDaRec.getWdtcProvDaRecAsBuffer(buffer, position);
        position += WdtcProvDaRec.Len.WDTC_PROV_DA_REC;
        MarshalByte.writeString(buffer, position, wdtcCodDvs, Len.WDTC_COD_DVS);
        position += Len.WDTC_COD_DVS;
        wdtcFrqMovi.getWdtcFrqMoviAsBuffer(buffer, position);
        position += WdtcFrqMovi.Len.WDTC_FRQ_MOVI;
        MarshalByte.writeString(buffer, position, wdtcTpRgmFisc, Len.WDTC_TP_RGM_FISC);
        position += Len.WDTC_TP_RGM_FISC;
        MarshalByte.writeString(buffer, position, wdtcCodTari, Len.WDTC_COD_TARI);
        position += Len.WDTC_COD_TARI;
        MarshalByte.writeString(buffer, position, wdtcTpStatTit, Len.WDTC_TP_STAT_TIT);
        position += Len.WDTC_TP_STAT_TIT;
        wdtcImpAz.getWdtcImpAzAsBuffer(buffer, position);
        position += WdtcImpAz.Len.WDTC_IMP_AZ;
        wdtcImpAder.getWdtcImpAderAsBuffer(buffer, position);
        position += WdtcImpAder.Len.WDTC_IMP_ADER;
        wdtcImpTfr.getWdtcImpTfrAsBuffer(buffer, position);
        position += WdtcImpTfr.Len.WDTC_IMP_TFR;
        wdtcImpVolo.getWdtcImpVoloAsBuffer(buffer, position);
        position += WdtcImpVolo.Len.WDTC_IMP_VOLO;
        wdtcManfeeAntic.getWdtcManfeeAnticAsBuffer(buffer, position);
        position += WdtcManfeeAntic.Len.WDTC_MANFEE_ANTIC;
        wdtcManfeeRicor.getWdtcManfeeRicorAsBuffer(buffer, position);
        position += WdtcManfeeRicor.Len.WDTC_MANFEE_RICOR;
        wdtcManfeeRec.getWdtcManfeeRecAsBuffer(buffer, position);
        position += WdtcManfeeRec.Len.WDTC_MANFEE_REC;
        wdtcDtEsiTit.getWdtcDtEsiTitAsBuffer(buffer, position);
        position += WdtcDtEsiTit.Len.WDTC_DT_ESI_TIT;
        wdtcSpeAge.getWdtcSpeAgeAsBuffer(buffer, position);
        position += WdtcSpeAge.Len.WDTC_SPE_AGE;
        wdtcCarIas.getWdtcCarIasAsBuffer(buffer, position);
        position += WdtcCarIas.Len.WDTC_CAR_IAS;
        wdtcTotIntrPrest.getWdtcTotIntrPrestAsBuffer(buffer, position);
        position += WdtcTotIntrPrest.Len.WDTC_TOT_INTR_PREST;
        MarshalByte.writeLongAsPacked(buffer, position, wdtcDsRiga, Len.Int.WDTC_DS_RIGA, 0);
        position += Len.WDTC_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wdtcDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wdtcDsVer, Len.Int.WDTC_DS_VER, 0);
        position += Len.WDTC_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wdtcDsTsIniCptz, Len.Int.WDTC_DS_TS_INI_CPTZ, 0);
        position += Len.WDTC_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wdtcDsTsEndCptz, Len.Int.WDTC_DS_TS_END_CPTZ, 0);
        position += Len.WDTC_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wdtcDsUtente, Len.WDTC_DS_UTENTE);
        position += Len.WDTC_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wdtcDsStatoElab);
        position += Types.CHAR_SIZE;
        wdtcImpTrasfe.getWdtcImpTrasfeAsBuffer(buffer, position);
        position += WdtcImpTrasfe.Len.WDTC_IMP_TRASFE;
        wdtcImpTfrStrc.getWdtcImpTfrStrcAsBuffer(buffer, position);
        position += WdtcImpTfrStrc.Len.WDTC_IMP_TFR_STRC;
        wdtcNumGgRitardoPag.getWdtcNumGgRitardoPagAsBuffer(buffer, position);
        position += WdtcNumGgRitardoPag.Len.WDTC_NUM_GG_RITARDO_PAG;
        wdtcNumGgRival.getWdtcNumGgRivalAsBuffer(buffer, position);
        position += WdtcNumGgRival.Len.WDTC_NUM_GG_RIVAL;
        wdtcAcqExp.getWdtcAcqExpAsBuffer(buffer, position);
        position += WdtcAcqExp.Len.WDTC_ACQ_EXP;
        wdtcRemunAss.getWdtcRemunAssAsBuffer(buffer, position);
        position += WdtcRemunAss.Len.WDTC_REMUN_ASS;
        wdtcCommisInter.getWdtcCommisInterAsBuffer(buffer, position);
        position += WdtcCommisInter.Len.WDTC_COMMIS_INTER;
        wdtcCnbtAntirac.getWdtcCnbtAntiracAsBuffer(buffer, position);
        return buffer;
    }

    public void initDatiSpaces() {
        wdtcIdDettTitCont = Types.INVALID_INT_VAL;
        wdtcIdTitCont = Types.INVALID_INT_VAL;
        wdtcIdOgg = Types.INVALID_INT_VAL;
        wdtcTpOgg = "";
        wdtcIdMoviCrz = Types.INVALID_INT_VAL;
        wdtcIdMoviChiu.initWdtcIdMoviChiuSpaces();
        wdtcDtIniEff = Types.INVALID_INT_VAL;
        wdtcDtEndEff = Types.INVALID_INT_VAL;
        wdtcCodCompAnia = Types.INVALID_INT_VAL;
        wdtcDtIniCop.initWdtcDtIniCopSpaces();
        wdtcDtEndCop.initWdtcDtEndCopSpaces();
        wdtcPreNet.initWdtcPreNetSpaces();
        wdtcIntrFraz.initWdtcIntrFrazSpaces();
        wdtcIntrMora.initWdtcIntrMoraSpaces();
        wdtcIntrRetdt.initWdtcIntrRetdtSpaces();
        wdtcIntrRiat.initWdtcIntrRiatSpaces();
        wdtcDir.initWdtcDirSpaces();
        wdtcSpeMed.initWdtcSpeMedSpaces();
        wdtcTax.initWdtcTaxSpaces();
        wdtcSoprSan.initWdtcSoprSanSpaces();
        wdtcSoprSpo.initWdtcSoprSpoSpaces();
        wdtcSoprTec.initWdtcSoprTecSpaces();
        wdtcSoprProf.initWdtcSoprProfSpaces();
        wdtcSoprAlt.initWdtcSoprAltSpaces();
        wdtcPreTot.initWdtcPreTotSpaces();
        wdtcPrePpIas.initWdtcPrePpIasSpaces();
        wdtcPreSoloRsh.initWdtcPreSoloRshSpaces();
        wdtcCarAcq.initWdtcCarAcqSpaces();
        wdtcCarGest.initWdtcCarGestSpaces();
        wdtcCarInc.initWdtcCarIncSpaces();
        wdtcProvAcq1aa.initWdtcProvAcq1aaSpaces();
        wdtcProvAcq2aa.initWdtcProvAcq2aaSpaces();
        wdtcProvRicor.initWdtcProvRicorSpaces();
        wdtcProvInc.initWdtcProvIncSpaces();
        wdtcProvDaRec.initWdtcProvDaRecSpaces();
        wdtcCodDvs = "";
        wdtcFrqMovi.initWdtcFrqMoviSpaces();
        wdtcTpRgmFisc = "";
        wdtcCodTari = "";
        wdtcTpStatTit = "";
        wdtcImpAz.initWdtcImpAzSpaces();
        wdtcImpAder.initWdtcImpAderSpaces();
        wdtcImpTfr.initWdtcImpTfrSpaces();
        wdtcImpVolo.initWdtcImpVoloSpaces();
        wdtcManfeeAntic.initWdtcManfeeAnticSpaces();
        wdtcManfeeRicor.initWdtcManfeeRicorSpaces();
        wdtcManfeeRec.initWdtcManfeeRecSpaces();
        wdtcDtEsiTit.initWdtcDtEsiTitSpaces();
        wdtcSpeAge.initWdtcSpeAgeSpaces();
        wdtcCarIas.initWdtcCarIasSpaces();
        wdtcTotIntrPrest.initWdtcTotIntrPrestSpaces();
        wdtcDsRiga = Types.INVALID_LONG_VAL;
        wdtcDsOperSql = Types.SPACE_CHAR;
        wdtcDsVer = Types.INVALID_INT_VAL;
        wdtcDsTsIniCptz = Types.INVALID_LONG_VAL;
        wdtcDsTsEndCptz = Types.INVALID_LONG_VAL;
        wdtcDsUtente = "";
        wdtcDsStatoElab = Types.SPACE_CHAR;
        wdtcImpTrasfe.initWdtcImpTrasfeSpaces();
        wdtcImpTfrStrc.initWdtcImpTfrStrcSpaces();
        wdtcNumGgRitardoPag.initWdtcNumGgRitardoPagSpaces();
        wdtcNumGgRival.initWdtcNumGgRivalSpaces();
        wdtcAcqExp.initWdtcAcqExpSpaces();
        wdtcRemunAss.initWdtcRemunAssSpaces();
        wdtcCommisInter.initWdtcCommisInterSpaces();
        wdtcCnbtAntirac.initWdtcCnbtAntiracSpaces();
    }

    public void setWdtcIdDettTitCont(int wdtcIdDettTitCont) {
        this.wdtcIdDettTitCont = wdtcIdDettTitCont;
    }

    public int getWdtcIdDettTitCont() {
        return this.wdtcIdDettTitCont;
    }

    public void setWdtcIdTitCont(int wdtcIdTitCont) {
        this.wdtcIdTitCont = wdtcIdTitCont;
    }

    public int getWdtcIdTitCont() {
        return this.wdtcIdTitCont;
    }

    public void setWdtcIdOgg(int wdtcIdOgg) {
        this.wdtcIdOgg = wdtcIdOgg;
    }

    public int getWdtcIdOgg() {
        return this.wdtcIdOgg;
    }

    public void setWdtcTpOgg(String wdtcTpOgg) {
        this.wdtcTpOgg = Functions.subString(wdtcTpOgg, Len.WDTC_TP_OGG);
    }

    public String getWdtcTpOgg() {
        return this.wdtcTpOgg;
    }

    public void setWdtcIdMoviCrz(int wdtcIdMoviCrz) {
        this.wdtcIdMoviCrz = wdtcIdMoviCrz;
    }

    public int getWdtcIdMoviCrz() {
        return this.wdtcIdMoviCrz;
    }

    public void setWdtcDtIniEff(int wdtcDtIniEff) {
        this.wdtcDtIniEff = wdtcDtIniEff;
    }

    public int getWdtcDtIniEff() {
        return this.wdtcDtIniEff;
    }

    public void setWdtcDtEndEff(int wdtcDtEndEff) {
        this.wdtcDtEndEff = wdtcDtEndEff;
    }

    public int getWdtcDtEndEff() {
        return this.wdtcDtEndEff;
    }

    public void setWdtcCodCompAnia(int wdtcCodCompAnia) {
        this.wdtcCodCompAnia = wdtcCodCompAnia;
    }

    public int getWdtcCodCompAnia() {
        return this.wdtcCodCompAnia;
    }

    public void setWdtcCodDvs(String wdtcCodDvs) {
        this.wdtcCodDvs = Functions.subString(wdtcCodDvs, Len.WDTC_COD_DVS);
    }

    public String getWdtcCodDvs() {
        return this.wdtcCodDvs;
    }

    public void setWdtcTpRgmFisc(String wdtcTpRgmFisc) {
        this.wdtcTpRgmFisc = Functions.subString(wdtcTpRgmFisc, Len.WDTC_TP_RGM_FISC);
    }

    public String getWdtcTpRgmFisc() {
        return this.wdtcTpRgmFisc;
    }

    public void setWdtcCodTari(String wdtcCodTari) {
        this.wdtcCodTari = Functions.subString(wdtcCodTari, Len.WDTC_COD_TARI);
    }

    public String getWdtcCodTari() {
        return this.wdtcCodTari;
    }

    public void setWdtcTpStatTit(String wdtcTpStatTit) {
        this.wdtcTpStatTit = Functions.subString(wdtcTpStatTit, Len.WDTC_TP_STAT_TIT);
    }

    public String getWdtcTpStatTit() {
        return this.wdtcTpStatTit;
    }

    public void setWdtcDsRiga(long wdtcDsRiga) {
        this.wdtcDsRiga = wdtcDsRiga;
    }

    public long getWdtcDsRiga() {
        return this.wdtcDsRiga;
    }

    public void setWdtcDsOperSql(char wdtcDsOperSql) {
        this.wdtcDsOperSql = wdtcDsOperSql;
    }

    public char getWdtcDsOperSql() {
        return this.wdtcDsOperSql;
    }

    public void setWdtcDsVer(int wdtcDsVer) {
        this.wdtcDsVer = wdtcDsVer;
    }

    public int getWdtcDsVer() {
        return this.wdtcDsVer;
    }

    public void setWdtcDsTsIniCptz(long wdtcDsTsIniCptz) {
        this.wdtcDsTsIniCptz = wdtcDsTsIniCptz;
    }

    public long getWdtcDsTsIniCptz() {
        return this.wdtcDsTsIniCptz;
    }

    public void setWdtcDsTsEndCptz(long wdtcDsTsEndCptz) {
        this.wdtcDsTsEndCptz = wdtcDsTsEndCptz;
    }

    public long getWdtcDsTsEndCptz() {
        return this.wdtcDsTsEndCptz;
    }

    public void setWdtcDsUtente(String wdtcDsUtente) {
        this.wdtcDsUtente = Functions.subString(wdtcDsUtente, Len.WDTC_DS_UTENTE);
    }

    public String getWdtcDsUtente() {
        return this.wdtcDsUtente;
    }

    public void setWdtcDsStatoElab(char wdtcDsStatoElab) {
        this.wdtcDsStatoElab = wdtcDsStatoElab;
    }

    public char getWdtcDsStatoElab() {
        return this.wdtcDsStatoElab;
    }

    public WdtcAcqExp getWdtcAcqExp() {
        return wdtcAcqExp;
    }

    public WdtcCarAcq getWdtcCarAcq() {
        return wdtcCarAcq;
    }

    public WdtcCarGest getWdtcCarGest() {
        return wdtcCarGest;
    }

    public WdtcCarIas getWdtcCarIas() {
        return wdtcCarIas;
    }

    public WdtcCarInc getWdtcCarInc() {
        return wdtcCarInc;
    }

    public WdtcCnbtAntirac getWdtcCnbtAntirac() {
        return wdtcCnbtAntirac;
    }

    public WdtcCommisInter getWdtcCommisInter() {
        return wdtcCommisInter;
    }

    public WdtcDir getWdtcDir() {
        return wdtcDir;
    }

    public WdtcDtEndCop getWdtcDtEndCop() {
        return wdtcDtEndCop;
    }

    public WdtcDtEsiTit getWdtcDtEsiTit() {
        return wdtcDtEsiTit;
    }

    public WdtcDtIniCop getWdtcDtIniCop() {
        return wdtcDtIniCop;
    }

    public WdtcFrqMovi getWdtcFrqMovi() {
        return wdtcFrqMovi;
    }

    public WdtcIdMoviChiu getWdtcIdMoviChiu() {
        return wdtcIdMoviChiu;
    }

    public WdtcImpAder getWdtcImpAder() {
        return wdtcImpAder;
    }

    public WdtcImpAz getWdtcImpAz() {
        return wdtcImpAz;
    }

    public WdtcImpTfr getWdtcImpTfr() {
        return wdtcImpTfr;
    }

    public WdtcImpTfrStrc getWdtcImpTfrStrc() {
        return wdtcImpTfrStrc;
    }

    public WdtcImpTrasfe getWdtcImpTrasfe() {
        return wdtcImpTrasfe;
    }

    public WdtcImpVolo getWdtcImpVolo() {
        return wdtcImpVolo;
    }

    public WdtcIntrFraz getWdtcIntrFraz() {
        return wdtcIntrFraz;
    }

    public WdtcIntrMora getWdtcIntrMora() {
        return wdtcIntrMora;
    }

    public WdtcIntrRetdt getWdtcIntrRetdt() {
        return wdtcIntrRetdt;
    }

    public WdtcIntrRiat getWdtcIntrRiat() {
        return wdtcIntrRiat;
    }

    public WdtcManfeeAntic getWdtcManfeeAntic() {
        return wdtcManfeeAntic;
    }

    public WdtcManfeeRec getWdtcManfeeRec() {
        return wdtcManfeeRec;
    }

    public WdtcManfeeRicor getWdtcManfeeRicor() {
        return wdtcManfeeRicor;
    }

    public WdtcNumGgRitardoPag getWdtcNumGgRitardoPag() {
        return wdtcNumGgRitardoPag;
    }

    public WdtcNumGgRival getWdtcNumGgRival() {
        return wdtcNumGgRival;
    }

    public WdtcPreNet getWdtcPreNet() {
        return wdtcPreNet;
    }

    public WdtcPrePpIas getWdtcPrePpIas() {
        return wdtcPrePpIas;
    }

    public WdtcPreSoloRsh getWdtcPreSoloRsh() {
        return wdtcPreSoloRsh;
    }

    public WdtcPreTot getWdtcPreTot() {
        return wdtcPreTot;
    }

    public WdtcProvAcq1aa getWdtcProvAcq1aa() {
        return wdtcProvAcq1aa;
    }

    public WdtcProvAcq2aa getWdtcProvAcq2aa() {
        return wdtcProvAcq2aa;
    }

    public WdtcProvDaRec getWdtcProvDaRec() {
        return wdtcProvDaRec;
    }

    public WdtcProvInc getWdtcProvInc() {
        return wdtcProvInc;
    }

    public WdtcProvRicor getWdtcProvRicor() {
        return wdtcProvRicor;
    }

    public WdtcRemunAss getWdtcRemunAss() {
        return wdtcRemunAss;
    }

    public WdtcSoprAlt getWdtcSoprAlt() {
        return wdtcSoprAlt;
    }

    public WdtcSoprProf getWdtcSoprProf() {
        return wdtcSoprProf;
    }

    public WdtcSoprSan getWdtcSoprSan() {
        return wdtcSoprSan;
    }

    public WdtcSoprSpo getWdtcSoprSpo() {
        return wdtcSoprSpo;
    }

    public WdtcSoprTec getWdtcSoprTec() {
        return wdtcSoprTec;
    }

    public WdtcSpeAge getWdtcSpeAge() {
        return wdtcSpeAge;
    }

    public WdtcSpeMed getWdtcSpeMed() {
        return wdtcSpeMed;
    }

    public WdtcTax getWdtcTax() {
        return wdtcTax;
    }

    public WdtcTotIntrPrest getWdtcTotIntrPrest() {
        return wdtcTotIntrPrest;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_ID_DETT_TIT_CONT = 5;
        public static final int WDTC_ID_TIT_CONT = 5;
        public static final int WDTC_ID_OGG = 5;
        public static final int WDTC_TP_OGG = 2;
        public static final int WDTC_ID_MOVI_CRZ = 5;
        public static final int WDTC_DT_INI_EFF = 5;
        public static final int WDTC_DT_END_EFF = 5;
        public static final int WDTC_COD_COMP_ANIA = 3;
        public static final int WDTC_COD_DVS = 20;
        public static final int WDTC_TP_RGM_FISC = 2;
        public static final int WDTC_COD_TARI = 12;
        public static final int WDTC_TP_STAT_TIT = 2;
        public static final int WDTC_DS_RIGA = 6;
        public static final int WDTC_DS_OPER_SQL = 1;
        public static final int WDTC_DS_VER = 5;
        public static final int WDTC_DS_TS_INI_CPTZ = 10;
        public static final int WDTC_DS_TS_END_CPTZ = 10;
        public static final int WDTC_DS_UTENTE = 20;
        public static final int WDTC_DS_STATO_ELAB = 1;
        public static final int DATI = WDTC_ID_DETT_TIT_CONT + WDTC_ID_TIT_CONT + WDTC_ID_OGG + WDTC_TP_OGG + WDTC_ID_MOVI_CRZ + WdtcIdMoviChiu.Len.WDTC_ID_MOVI_CHIU + WDTC_DT_INI_EFF + WDTC_DT_END_EFF + WDTC_COD_COMP_ANIA + WdtcDtIniCop.Len.WDTC_DT_INI_COP + WdtcDtEndCop.Len.WDTC_DT_END_COP + WdtcPreNet.Len.WDTC_PRE_NET + WdtcIntrFraz.Len.WDTC_INTR_FRAZ + WdtcIntrMora.Len.WDTC_INTR_MORA + WdtcIntrRetdt.Len.WDTC_INTR_RETDT + WdtcIntrRiat.Len.WDTC_INTR_RIAT + WdtcDir.Len.WDTC_DIR + WdtcSpeMed.Len.WDTC_SPE_MED + WdtcTax.Len.WDTC_TAX + WdtcSoprSan.Len.WDTC_SOPR_SAN + WdtcSoprSpo.Len.WDTC_SOPR_SPO + WdtcSoprTec.Len.WDTC_SOPR_TEC + WdtcSoprProf.Len.WDTC_SOPR_PROF + WdtcSoprAlt.Len.WDTC_SOPR_ALT + WdtcPreTot.Len.WDTC_PRE_TOT + WdtcPrePpIas.Len.WDTC_PRE_PP_IAS + WdtcPreSoloRsh.Len.WDTC_PRE_SOLO_RSH + WdtcCarAcq.Len.WDTC_CAR_ACQ + WdtcCarGest.Len.WDTC_CAR_GEST + WdtcCarInc.Len.WDTC_CAR_INC + WdtcProvAcq1aa.Len.WDTC_PROV_ACQ1AA + WdtcProvAcq2aa.Len.WDTC_PROV_ACQ2AA + WdtcProvRicor.Len.WDTC_PROV_RICOR + WdtcProvInc.Len.WDTC_PROV_INC + WdtcProvDaRec.Len.WDTC_PROV_DA_REC + WDTC_COD_DVS + WdtcFrqMovi.Len.WDTC_FRQ_MOVI + WDTC_TP_RGM_FISC + WDTC_COD_TARI + WDTC_TP_STAT_TIT + WdtcImpAz.Len.WDTC_IMP_AZ + WdtcImpAder.Len.WDTC_IMP_ADER + WdtcImpTfr.Len.WDTC_IMP_TFR + WdtcImpVolo.Len.WDTC_IMP_VOLO + WdtcManfeeAntic.Len.WDTC_MANFEE_ANTIC + WdtcManfeeRicor.Len.WDTC_MANFEE_RICOR + WdtcManfeeRec.Len.WDTC_MANFEE_REC + WdtcDtEsiTit.Len.WDTC_DT_ESI_TIT + WdtcSpeAge.Len.WDTC_SPE_AGE + WdtcCarIas.Len.WDTC_CAR_IAS + WdtcTotIntrPrest.Len.WDTC_TOT_INTR_PREST + WDTC_DS_RIGA + WDTC_DS_OPER_SQL + WDTC_DS_VER + WDTC_DS_TS_INI_CPTZ + WDTC_DS_TS_END_CPTZ + WDTC_DS_UTENTE + WDTC_DS_STATO_ELAB + WdtcImpTrasfe.Len.WDTC_IMP_TRASFE + WdtcImpTfrStrc.Len.WDTC_IMP_TFR_STRC + WdtcNumGgRitardoPag.Len.WDTC_NUM_GG_RITARDO_PAG + WdtcNumGgRival.Len.WDTC_NUM_GG_RIVAL + WdtcAcqExp.Len.WDTC_ACQ_EXP + WdtcRemunAss.Len.WDTC_REMUN_ASS + WdtcCommisInter.Len.WDTC_COMMIS_INTER + WdtcCnbtAntirac.Len.WDTC_CNBT_ANTIRAC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_ID_DETT_TIT_CONT = 9;
            public static final int WDTC_ID_TIT_CONT = 9;
            public static final int WDTC_ID_OGG = 9;
            public static final int WDTC_ID_MOVI_CRZ = 9;
            public static final int WDTC_DT_INI_EFF = 8;
            public static final int WDTC_DT_END_EFF = 8;
            public static final int WDTC_COD_COMP_ANIA = 5;
            public static final int WDTC_DS_RIGA = 10;
            public static final int WDTC_DS_VER = 9;
            public static final int WDTC_DS_TS_INI_CPTZ = 18;
            public static final int WDTC_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
