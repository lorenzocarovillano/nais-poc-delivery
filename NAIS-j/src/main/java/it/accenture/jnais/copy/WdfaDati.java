package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WdfaAaCnbzK1;
import it.accenture.jnais.ws.redefines.WdfaAaCnbzK2;
import it.accenture.jnais.ws.redefines.WdfaAaCnbzK3;
import it.accenture.jnais.ws.redefines.WdfaAlqPrvr;
import it.accenture.jnais.ws.redefines.WdfaAlqTfr;
import it.accenture.jnais.ws.redefines.WdfaAnzCnbtvaCarass;
import it.accenture.jnais.ws.redefines.WdfaAnzCnbtvaCarazi;
import it.accenture.jnais.ws.redefines.WdfaAnzSrvz;
import it.accenture.jnais.ws.redefines.WdfaCnbtEcc4x100K1;
import it.accenture.jnais.ws.redefines.WdfaCommisDiTrasfe;
import it.accenture.jnais.ws.redefines.WdfaCreditoIs;
import it.accenture.jnais.ws.redefines.WdfaDt1aCnbz;
import it.accenture.jnais.ws.redefines.WdfaDtAccnsRappFnd;
import it.accenture.jnais.ws.redefines.WdfaDtCessazione;
import it.accenture.jnais.ws.redefines.WdfaIdMoviChiu;
import it.accenture.jnais.ws.redefines.WdfaImpb252Antic;
import it.accenture.jnais.ws.redefines.WdfaImpb252K3;
import it.accenture.jnais.ws.redefines.WdfaImpbIrpefK1Anti;
import it.accenture.jnais.ws.redefines.WdfaImpbIrpefK2Anti;
import it.accenture.jnais.ws.redefines.WdfaImpbIsK2;
import it.accenture.jnais.ws.redefines.WdfaImpbIsK2Antic;
import it.accenture.jnais.ws.redefines.WdfaImpbIsK3;
import it.accenture.jnais.ws.redefines.WdfaImpbIsK3Antic;
import it.accenture.jnais.ws.redefines.WdfaImpbTfrAntic;
import it.accenture.jnais.ws.redefines.WdfaImpbVis;
import it.accenture.jnais.ws.redefines.WdfaImpbVisAntic;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtAzK1;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtAzK2;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtAzK3;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtIscK1;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtIscK2;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtIscK3;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtNdedK1;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtNdedK2;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtNdedK3;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtTfrK1;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtTfrK2;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtTfrK3;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtVolK1;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtVolK2;
import it.accenture.jnais.ws.redefines.WdfaImpCnbtVolK3;
import it.accenture.jnais.ws.redefines.WdfaImpEseImpstTfr;
import it.accenture.jnais.ws.redefines.WdfaImpNetTrasferito;
import it.accenture.jnais.ws.redefines.WdfaImpst252Antic;
import it.accenture.jnais.ws.redefines.WdfaImpst252K3;
import it.accenture.jnais.ws.redefines.WdfaImpstIrpefK1Ant;
import it.accenture.jnais.ws.redefines.WdfaImpstIrpefK2Ant;
import it.accenture.jnais.ws.redefines.WdfaImpstSostK2;
import it.accenture.jnais.ws.redefines.WdfaImpstSostK2Anti;
import it.accenture.jnais.ws.redefines.WdfaImpstSostK3;
import it.accenture.jnais.ws.redefines.WdfaImpstSostK3Anti;
import it.accenture.jnais.ws.redefines.WdfaImpstTfrAntic;
import it.accenture.jnais.ws.redefines.WdfaImpstVis;
import it.accenture.jnais.ws.redefines.WdfaImpstVisAntic;
import it.accenture.jnais.ws.redefines.WdfaMatuK1;
import it.accenture.jnais.ws.redefines.WdfaMatuK2;
import it.accenture.jnais.ws.redefines.WdfaMatuK3;
import it.accenture.jnais.ws.redefines.WdfaMatuResK1;
import it.accenture.jnais.ws.redefines.WdfaMmCnbzK1;
import it.accenture.jnais.ws.redefines.WdfaMmCnbzK2;
import it.accenture.jnais.ws.redefines.WdfaMmCnbzK3;
import it.accenture.jnais.ws.redefines.WdfaOnerTrasfe;
import it.accenture.jnais.ws.redefines.WdfaPcEseImpstTfr;
import it.accenture.jnais.ws.redefines.WdfaPcTfr;
import it.accenture.jnais.ws.redefines.WdfaRedtTassAbbatK2;
import it.accenture.jnais.ws.redefines.WdfaRedtTassAbbatK3;
import it.accenture.jnais.ws.redefines.WdfaRidzTfr;
import it.accenture.jnais.ws.redefines.WdfaRidzTfrSuAntic;
import it.accenture.jnais.ws.redefines.WdfaTotAntic;
import it.accenture.jnais.ws.redefines.WdfaTotImpst;
import it.accenture.jnais.ws.redefines.WdfaUltCommisTrasfe;

/**Original name: WDFA-DATI<br>
 * Variable: WDFA-DATI from copybook LCCVDFA1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WdfaDati {

    //==== PROPERTIES ====
    //Original name: WDFA-ID-D-FISC-ADES
    private int wdfaIdDFiscAdes = DefaultValues.INT_VAL;
    //Original name: WDFA-ID-ADES
    private int wdfaIdAdes = DefaultValues.INT_VAL;
    //Original name: WDFA-ID-MOVI-CRZ
    private int wdfaIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WDFA-ID-MOVI-CHIU
    private WdfaIdMoviChiu wdfaIdMoviChiu = new WdfaIdMoviChiu();
    //Original name: WDFA-DT-INI-EFF
    private int wdfaDtIniEff = DefaultValues.INT_VAL;
    //Original name: WDFA-DT-END-EFF
    private int wdfaDtEndEff = DefaultValues.INT_VAL;
    //Original name: WDFA-COD-COMP-ANIA
    private int wdfaCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WDFA-TP-ISC-FND
    private String wdfaTpIscFnd = DefaultValues.stringVal(Len.WDFA_TP_ISC_FND);
    //Original name: WDFA-DT-ACCNS-RAPP-FND
    private WdfaDtAccnsRappFnd wdfaDtAccnsRappFnd = new WdfaDtAccnsRappFnd();
    //Original name: WDFA-IMP-CNBT-AZ-K1
    private WdfaImpCnbtAzK1 wdfaImpCnbtAzK1 = new WdfaImpCnbtAzK1();
    //Original name: WDFA-IMP-CNBT-ISC-K1
    private WdfaImpCnbtIscK1 wdfaImpCnbtIscK1 = new WdfaImpCnbtIscK1();
    //Original name: WDFA-IMP-CNBT-TFR-K1
    private WdfaImpCnbtTfrK1 wdfaImpCnbtTfrK1 = new WdfaImpCnbtTfrK1();
    //Original name: WDFA-IMP-CNBT-VOL-K1
    private WdfaImpCnbtVolK1 wdfaImpCnbtVolK1 = new WdfaImpCnbtVolK1();
    //Original name: WDFA-IMP-CNBT-AZ-K2
    private WdfaImpCnbtAzK2 wdfaImpCnbtAzK2 = new WdfaImpCnbtAzK2();
    //Original name: WDFA-IMP-CNBT-ISC-K2
    private WdfaImpCnbtIscK2 wdfaImpCnbtIscK2 = new WdfaImpCnbtIscK2();
    //Original name: WDFA-IMP-CNBT-TFR-K2
    private WdfaImpCnbtTfrK2 wdfaImpCnbtTfrK2 = new WdfaImpCnbtTfrK2();
    //Original name: WDFA-IMP-CNBT-VOL-K2
    private WdfaImpCnbtVolK2 wdfaImpCnbtVolK2 = new WdfaImpCnbtVolK2();
    //Original name: WDFA-MATU-K1
    private WdfaMatuK1 wdfaMatuK1 = new WdfaMatuK1();
    //Original name: WDFA-MATU-RES-K1
    private WdfaMatuResK1 wdfaMatuResK1 = new WdfaMatuResK1();
    //Original name: WDFA-MATU-K2
    private WdfaMatuK2 wdfaMatuK2 = new WdfaMatuK2();
    //Original name: WDFA-IMPB-VIS
    private WdfaImpbVis wdfaImpbVis = new WdfaImpbVis();
    //Original name: WDFA-IMPST-VIS
    private WdfaImpstVis wdfaImpstVis = new WdfaImpstVis();
    //Original name: WDFA-IMPB-IS-K2
    private WdfaImpbIsK2 wdfaImpbIsK2 = new WdfaImpbIsK2();
    //Original name: WDFA-IMPST-SOST-K2
    private WdfaImpstSostK2 wdfaImpstSostK2 = new WdfaImpstSostK2();
    //Original name: WDFA-RIDZ-TFR
    private WdfaRidzTfr wdfaRidzTfr = new WdfaRidzTfr();
    //Original name: WDFA-PC-TFR
    private WdfaPcTfr wdfaPcTfr = new WdfaPcTfr();
    //Original name: WDFA-ALQ-TFR
    private WdfaAlqTfr wdfaAlqTfr = new WdfaAlqTfr();
    //Original name: WDFA-TOT-ANTIC
    private WdfaTotAntic wdfaTotAntic = new WdfaTotAntic();
    //Original name: WDFA-IMPB-TFR-ANTIC
    private WdfaImpbTfrAntic wdfaImpbTfrAntic = new WdfaImpbTfrAntic();
    //Original name: WDFA-IMPST-TFR-ANTIC
    private WdfaImpstTfrAntic wdfaImpstTfrAntic = new WdfaImpstTfrAntic();
    //Original name: WDFA-RIDZ-TFR-SU-ANTIC
    private WdfaRidzTfrSuAntic wdfaRidzTfrSuAntic = new WdfaRidzTfrSuAntic();
    //Original name: WDFA-IMPB-VIS-ANTIC
    private WdfaImpbVisAntic wdfaImpbVisAntic = new WdfaImpbVisAntic();
    //Original name: WDFA-IMPST-VIS-ANTIC
    private WdfaImpstVisAntic wdfaImpstVisAntic = new WdfaImpstVisAntic();
    //Original name: WDFA-IMPB-IS-K2-ANTIC
    private WdfaImpbIsK2Antic wdfaImpbIsK2Antic = new WdfaImpbIsK2Antic();
    //Original name: WDFA-IMPST-SOST-K2-ANTI
    private WdfaImpstSostK2Anti wdfaImpstSostK2Anti = new WdfaImpstSostK2Anti();
    //Original name: WDFA-ULT-COMMIS-TRASFE
    private WdfaUltCommisTrasfe wdfaUltCommisTrasfe = new WdfaUltCommisTrasfe();
    //Original name: WDFA-COD-DVS
    private String wdfaCodDvs = DefaultValues.stringVal(Len.WDFA_COD_DVS);
    //Original name: WDFA-ALQ-PRVR
    private WdfaAlqPrvr wdfaAlqPrvr = new WdfaAlqPrvr();
    //Original name: WDFA-PC-ESE-IMPST-TFR
    private WdfaPcEseImpstTfr wdfaPcEseImpstTfr = new WdfaPcEseImpstTfr();
    //Original name: WDFA-IMP-ESE-IMPST-TFR
    private WdfaImpEseImpstTfr wdfaImpEseImpstTfr = new WdfaImpEseImpstTfr();
    //Original name: WDFA-ANZ-CNBTVA-CARASS
    private WdfaAnzCnbtvaCarass wdfaAnzCnbtvaCarass = new WdfaAnzCnbtvaCarass();
    //Original name: WDFA-ANZ-CNBTVA-CARAZI
    private WdfaAnzCnbtvaCarazi wdfaAnzCnbtvaCarazi = new WdfaAnzCnbtvaCarazi();
    //Original name: WDFA-ANZ-SRVZ
    private WdfaAnzSrvz wdfaAnzSrvz = new WdfaAnzSrvz();
    //Original name: WDFA-IMP-CNBT-NDED-K1
    private WdfaImpCnbtNdedK1 wdfaImpCnbtNdedK1 = new WdfaImpCnbtNdedK1();
    //Original name: WDFA-IMP-CNBT-NDED-K2
    private WdfaImpCnbtNdedK2 wdfaImpCnbtNdedK2 = new WdfaImpCnbtNdedK2();
    //Original name: WDFA-IMP-CNBT-NDED-K3
    private WdfaImpCnbtNdedK3 wdfaImpCnbtNdedK3 = new WdfaImpCnbtNdedK3();
    //Original name: WDFA-IMP-CNBT-AZ-K3
    private WdfaImpCnbtAzK3 wdfaImpCnbtAzK3 = new WdfaImpCnbtAzK3();
    //Original name: WDFA-IMP-CNBT-ISC-K3
    private WdfaImpCnbtIscK3 wdfaImpCnbtIscK3 = new WdfaImpCnbtIscK3();
    //Original name: WDFA-IMP-CNBT-TFR-K3
    private WdfaImpCnbtTfrK3 wdfaImpCnbtTfrK3 = new WdfaImpCnbtTfrK3();
    //Original name: WDFA-IMP-CNBT-VOL-K3
    private WdfaImpCnbtVolK3 wdfaImpCnbtVolK3 = new WdfaImpCnbtVolK3();
    //Original name: WDFA-MATU-K3
    private WdfaMatuK3 wdfaMatuK3 = new WdfaMatuK3();
    //Original name: WDFA-IMPB-252-ANTIC
    private WdfaImpb252Antic wdfaImpb252Antic = new WdfaImpb252Antic();
    //Original name: WDFA-IMPST-252-ANTIC
    private WdfaImpst252Antic wdfaImpst252Antic = new WdfaImpst252Antic();
    //Original name: WDFA-DT-1A-CNBZ
    private WdfaDt1aCnbz wdfaDt1aCnbz = new WdfaDt1aCnbz();
    //Original name: WDFA-COMMIS-DI-TRASFE
    private WdfaCommisDiTrasfe wdfaCommisDiTrasfe = new WdfaCommisDiTrasfe();
    //Original name: WDFA-AA-CNBZ-K1
    private WdfaAaCnbzK1 wdfaAaCnbzK1 = new WdfaAaCnbzK1();
    //Original name: WDFA-AA-CNBZ-K2
    private WdfaAaCnbzK2 wdfaAaCnbzK2 = new WdfaAaCnbzK2();
    //Original name: WDFA-AA-CNBZ-K3
    private WdfaAaCnbzK3 wdfaAaCnbzK3 = new WdfaAaCnbzK3();
    //Original name: WDFA-MM-CNBZ-K1
    private WdfaMmCnbzK1 wdfaMmCnbzK1 = new WdfaMmCnbzK1();
    //Original name: WDFA-MM-CNBZ-K2
    private WdfaMmCnbzK2 wdfaMmCnbzK2 = new WdfaMmCnbzK2();
    //Original name: WDFA-MM-CNBZ-K3
    private WdfaMmCnbzK3 wdfaMmCnbzK3 = new WdfaMmCnbzK3();
    //Original name: WDFA-FL-APPLZ-NEWFIS
    private char wdfaFlApplzNewfis = DefaultValues.CHAR_VAL;
    //Original name: WDFA-REDT-TASS-ABBAT-K3
    private WdfaRedtTassAbbatK3 wdfaRedtTassAbbatK3 = new WdfaRedtTassAbbatK3();
    //Original name: WDFA-CNBT-ECC-4X100-K1
    private WdfaCnbtEcc4x100K1 wdfaCnbtEcc4x100K1 = new WdfaCnbtEcc4x100K1();
    //Original name: WDFA-DS-RIGA
    private long wdfaDsRiga = DefaultValues.LONG_VAL;
    //Original name: WDFA-DS-OPER-SQL
    private char wdfaDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WDFA-DS-VER
    private int wdfaDsVer = DefaultValues.INT_VAL;
    //Original name: WDFA-DS-TS-INI-CPTZ
    private long wdfaDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WDFA-DS-TS-END-CPTZ
    private long wdfaDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WDFA-DS-UTENTE
    private String wdfaDsUtente = DefaultValues.stringVal(Len.WDFA_DS_UTENTE);
    //Original name: WDFA-DS-STATO-ELAB
    private char wdfaDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WDFA-CREDITO-IS
    private WdfaCreditoIs wdfaCreditoIs = new WdfaCreditoIs();
    //Original name: WDFA-REDT-TASS-ABBAT-K2
    private WdfaRedtTassAbbatK2 wdfaRedtTassAbbatK2 = new WdfaRedtTassAbbatK2();
    //Original name: WDFA-IMPB-IS-K3
    private WdfaImpbIsK3 wdfaImpbIsK3 = new WdfaImpbIsK3();
    //Original name: WDFA-IMPST-SOST-K3
    private WdfaImpstSostK3 wdfaImpstSostK3 = new WdfaImpstSostK3();
    //Original name: WDFA-IMPB-252-K3
    private WdfaImpb252K3 wdfaImpb252K3 = new WdfaImpb252K3();
    //Original name: WDFA-IMPST-252-K3
    private WdfaImpst252K3 wdfaImpst252K3 = new WdfaImpst252K3();
    //Original name: WDFA-IMPB-IS-K3-ANTIC
    private WdfaImpbIsK3Antic wdfaImpbIsK3Antic = new WdfaImpbIsK3Antic();
    //Original name: WDFA-IMPST-SOST-K3-ANTI
    private WdfaImpstSostK3Anti wdfaImpstSostK3Anti = new WdfaImpstSostK3Anti();
    //Original name: WDFA-IMPB-IRPEF-K1-ANTI
    private WdfaImpbIrpefK1Anti wdfaImpbIrpefK1Anti = new WdfaImpbIrpefK1Anti();
    //Original name: WDFA-IMPST-IRPEF-K1-ANT
    private WdfaImpstIrpefK1Ant wdfaImpstIrpefK1Ant = new WdfaImpstIrpefK1Ant();
    //Original name: WDFA-IMPB-IRPEF-K2-ANTI
    private WdfaImpbIrpefK2Anti wdfaImpbIrpefK2Anti = new WdfaImpbIrpefK2Anti();
    //Original name: WDFA-IMPST-IRPEF-K2-ANT
    private WdfaImpstIrpefK2Ant wdfaImpstIrpefK2Ant = new WdfaImpstIrpefK2Ant();
    //Original name: WDFA-DT-CESSAZIONE
    private WdfaDtCessazione wdfaDtCessazione = new WdfaDtCessazione();
    //Original name: WDFA-TOT-IMPST
    private WdfaTotImpst wdfaTotImpst = new WdfaTotImpst();
    //Original name: WDFA-ONER-TRASFE
    private WdfaOnerTrasfe wdfaOnerTrasfe = new WdfaOnerTrasfe();
    //Original name: WDFA-IMP-NET-TRASFERITO
    private WdfaImpNetTrasferito wdfaImpNetTrasferito = new WdfaImpNetTrasferito();

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wdfaIdDFiscAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFA_ID_D_FISC_ADES, 0);
        position += Len.WDFA_ID_D_FISC_ADES;
        wdfaIdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFA_ID_ADES, 0);
        position += Len.WDFA_ID_ADES;
        wdfaIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFA_ID_MOVI_CRZ, 0);
        position += Len.WDFA_ID_MOVI_CRZ;
        wdfaIdMoviChiu.setWdfaIdMoviChiuFromBuffer(buffer, position);
        position += WdfaIdMoviChiu.Len.WDFA_ID_MOVI_CHIU;
        wdfaDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFA_DT_INI_EFF, 0);
        position += Len.WDFA_DT_INI_EFF;
        wdfaDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFA_DT_END_EFF, 0);
        position += Len.WDFA_DT_END_EFF;
        wdfaCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFA_COD_COMP_ANIA, 0);
        position += Len.WDFA_COD_COMP_ANIA;
        wdfaTpIscFnd = MarshalByte.readString(buffer, position, Len.WDFA_TP_ISC_FND);
        position += Len.WDFA_TP_ISC_FND;
        wdfaDtAccnsRappFnd.setWdfaDtAccnsRappFndFromBuffer(buffer, position);
        position += WdfaDtAccnsRappFnd.Len.WDFA_DT_ACCNS_RAPP_FND;
        wdfaImpCnbtAzK1.setWdfaImpCnbtAzK1FromBuffer(buffer, position);
        position += WdfaImpCnbtAzK1.Len.WDFA_IMP_CNBT_AZ_K1;
        wdfaImpCnbtIscK1.setWdfaImpCnbtIscK1FromBuffer(buffer, position);
        position += WdfaImpCnbtIscK1.Len.WDFA_IMP_CNBT_ISC_K1;
        wdfaImpCnbtTfrK1.setWdfaImpCnbtTfrK1FromBuffer(buffer, position);
        position += WdfaImpCnbtTfrK1.Len.WDFA_IMP_CNBT_TFR_K1;
        wdfaImpCnbtVolK1.setWdfaImpCnbtVolK1FromBuffer(buffer, position);
        position += WdfaImpCnbtVolK1.Len.WDFA_IMP_CNBT_VOL_K1;
        wdfaImpCnbtAzK2.setWdfaImpCnbtAzK2FromBuffer(buffer, position);
        position += WdfaImpCnbtAzK2.Len.WDFA_IMP_CNBT_AZ_K2;
        wdfaImpCnbtIscK2.setWdfaImpCnbtIscK2FromBuffer(buffer, position);
        position += WdfaImpCnbtIscK2.Len.WDFA_IMP_CNBT_ISC_K2;
        wdfaImpCnbtTfrK2.setWdfaImpCnbtTfrK2FromBuffer(buffer, position);
        position += WdfaImpCnbtTfrK2.Len.WDFA_IMP_CNBT_TFR_K2;
        wdfaImpCnbtVolK2.setWdfaImpCnbtVolK2FromBuffer(buffer, position);
        position += WdfaImpCnbtVolK2.Len.WDFA_IMP_CNBT_VOL_K2;
        wdfaMatuK1.setWdfaMatuK1FromBuffer(buffer, position);
        position += WdfaMatuK1.Len.WDFA_MATU_K1;
        wdfaMatuResK1.setWdfaMatuResK1FromBuffer(buffer, position);
        position += WdfaMatuResK1.Len.WDFA_MATU_RES_K1;
        wdfaMatuK2.setWdfaMatuK2FromBuffer(buffer, position);
        position += WdfaMatuK2.Len.WDFA_MATU_K2;
        wdfaImpbVis.setWdfaImpbVisFromBuffer(buffer, position);
        position += WdfaImpbVis.Len.WDFA_IMPB_VIS;
        wdfaImpstVis.setWdfaImpstVisFromBuffer(buffer, position);
        position += WdfaImpstVis.Len.WDFA_IMPST_VIS;
        wdfaImpbIsK2.setWdfaImpbIsK2FromBuffer(buffer, position);
        position += WdfaImpbIsK2.Len.WDFA_IMPB_IS_K2;
        wdfaImpstSostK2.setWdfaImpstSostK2FromBuffer(buffer, position);
        position += WdfaImpstSostK2.Len.WDFA_IMPST_SOST_K2;
        wdfaRidzTfr.setWdfaRidzTfrFromBuffer(buffer, position);
        position += WdfaRidzTfr.Len.WDFA_RIDZ_TFR;
        wdfaPcTfr.setWdfaPcTfrFromBuffer(buffer, position);
        position += WdfaPcTfr.Len.WDFA_PC_TFR;
        wdfaAlqTfr.setWdfaAlqTfrFromBuffer(buffer, position);
        position += WdfaAlqTfr.Len.WDFA_ALQ_TFR;
        wdfaTotAntic.setWdfaTotAnticFromBuffer(buffer, position);
        position += WdfaTotAntic.Len.WDFA_TOT_ANTIC;
        wdfaImpbTfrAntic.setWdfaImpbTfrAnticFromBuffer(buffer, position);
        position += WdfaImpbTfrAntic.Len.WDFA_IMPB_TFR_ANTIC;
        wdfaImpstTfrAntic.setWdfaImpstTfrAnticFromBuffer(buffer, position);
        position += WdfaImpstTfrAntic.Len.WDFA_IMPST_TFR_ANTIC;
        wdfaRidzTfrSuAntic.setWdfaRidzTfrSuAnticFromBuffer(buffer, position);
        position += WdfaRidzTfrSuAntic.Len.WDFA_RIDZ_TFR_SU_ANTIC;
        wdfaImpbVisAntic.setWdfaImpbVisAnticFromBuffer(buffer, position);
        position += WdfaImpbVisAntic.Len.WDFA_IMPB_VIS_ANTIC;
        wdfaImpstVisAntic.setWdfaImpstVisAnticFromBuffer(buffer, position);
        position += WdfaImpstVisAntic.Len.WDFA_IMPST_VIS_ANTIC;
        wdfaImpbIsK2Antic.setWdfaImpbIsK2AnticFromBuffer(buffer, position);
        position += WdfaImpbIsK2Antic.Len.WDFA_IMPB_IS_K2_ANTIC;
        wdfaImpstSostK2Anti.setWdfaImpstSostK2AntiFromBuffer(buffer, position);
        position += WdfaImpstSostK2Anti.Len.WDFA_IMPST_SOST_K2_ANTI;
        wdfaUltCommisTrasfe.setWdfaUltCommisTrasfeFromBuffer(buffer, position);
        position += WdfaUltCommisTrasfe.Len.WDFA_ULT_COMMIS_TRASFE;
        wdfaCodDvs = MarshalByte.readString(buffer, position, Len.WDFA_COD_DVS);
        position += Len.WDFA_COD_DVS;
        wdfaAlqPrvr.setWdfaAlqPrvrFromBuffer(buffer, position);
        position += WdfaAlqPrvr.Len.WDFA_ALQ_PRVR;
        wdfaPcEseImpstTfr.setWdfaPcEseImpstTfrFromBuffer(buffer, position);
        position += WdfaPcEseImpstTfr.Len.WDFA_PC_ESE_IMPST_TFR;
        wdfaImpEseImpstTfr.setWdfaImpEseImpstTfrFromBuffer(buffer, position);
        position += WdfaImpEseImpstTfr.Len.WDFA_IMP_ESE_IMPST_TFR;
        wdfaAnzCnbtvaCarass.setWdfaAnzCnbtvaCarassFromBuffer(buffer, position);
        position += WdfaAnzCnbtvaCarass.Len.WDFA_ANZ_CNBTVA_CARASS;
        wdfaAnzCnbtvaCarazi.setWdfaAnzCnbtvaCaraziFromBuffer(buffer, position);
        position += WdfaAnzCnbtvaCarazi.Len.WDFA_ANZ_CNBTVA_CARAZI;
        wdfaAnzSrvz.setWdfaAnzSrvzFromBuffer(buffer, position);
        position += WdfaAnzSrvz.Len.WDFA_ANZ_SRVZ;
        wdfaImpCnbtNdedK1.setWdfaImpCnbtNdedK1FromBuffer(buffer, position);
        position += WdfaImpCnbtNdedK1.Len.WDFA_IMP_CNBT_NDED_K1;
        wdfaImpCnbtNdedK2.setWdfaImpCnbtNdedK2FromBuffer(buffer, position);
        position += WdfaImpCnbtNdedK2.Len.WDFA_IMP_CNBT_NDED_K2;
        wdfaImpCnbtNdedK3.setWdfaImpCnbtNdedK3FromBuffer(buffer, position);
        position += WdfaImpCnbtNdedK3.Len.WDFA_IMP_CNBT_NDED_K3;
        wdfaImpCnbtAzK3.setWdfaImpCnbtAzK3FromBuffer(buffer, position);
        position += WdfaImpCnbtAzK3.Len.WDFA_IMP_CNBT_AZ_K3;
        wdfaImpCnbtIscK3.setWdfaImpCnbtIscK3FromBuffer(buffer, position);
        position += WdfaImpCnbtIscK3.Len.WDFA_IMP_CNBT_ISC_K3;
        wdfaImpCnbtTfrK3.setWdfaImpCnbtTfrK3FromBuffer(buffer, position);
        position += WdfaImpCnbtTfrK3.Len.WDFA_IMP_CNBT_TFR_K3;
        wdfaImpCnbtVolK3.setWdfaImpCnbtVolK3FromBuffer(buffer, position);
        position += WdfaImpCnbtVolK3.Len.WDFA_IMP_CNBT_VOL_K3;
        wdfaMatuK3.setWdfaMatuK3FromBuffer(buffer, position);
        position += WdfaMatuK3.Len.WDFA_MATU_K3;
        wdfaImpb252Antic.setWdfaImpb252AnticFromBuffer(buffer, position);
        position += WdfaImpb252Antic.Len.WDFA_IMPB252_ANTIC;
        wdfaImpst252Antic.setWdfaImpst252AnticFromBuffer(buffer, position);
        position += WdfaImpst252Antic.Len.WDFA_IMPST252_ANTIC;
        wdfaDt1aCnbz.setWdfaDt1aCnbzFromBuffer(buffer, position);
        position += WdfaDt1aCnbz.Len.WDFA_DT1A_CNBZ;
        wdfaCommisDiTrasfe.setWdfaCommisDiTrasfeFromBuffer(buffer, position);
        position += WdfaCommisDiTrasfe.Len.WDFA_COMMIS_DI_TRASFE;
        wdfaAaCnbzK1.setWdfaAaCnbzK1FromBuffer(buffer, position);
        position += WdfaAaCnbzK1.Len.WDFA_AA_CNBZ_K1;
        wdfaAaCnbzK2.setWdfaAaCnbzK2FromBuffer(buffer, position);
        position += WdfaAaCnbzK2.Len.WDFA_AA_CNBZ_K2;
        wdfaAaCnbzK3.setWdfaAaCnbzK3FromBuffer(buffer, position);
        position += WdfaAaCnbzK3.Len.WDFA_AA_CNBZ_K3;
        wdfaMmCnbzK1.setWdfaMmCnbzK1FromBuffer(buffer, position);
        position += WdfaMmCnbzK1.Len.WDFA_MM_CNBZ_K1;
        wdfaMmCnbzK2.setWdfaMmCnbzK2FromBuffer(buffer, position);
        position += WdfaMmCnbzK2.Len.WDFA_MM_CNBZ_K2;
        wdfaMmCnbzK3.setWdfaMmCnbzK3FromBuffer(buffer, position);
        position += WdfaMmCnbzK3.Len.WDFA_MM_CNBZ_K3;
        wdfaFlApplzNewfis = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdfaRedtTassAbbatK3.setWdfaRedtTassAbbatK3FromBuffer(buffer, position);
        position += WdfaRedtTassAbbatK3.Len.WDFA_REDT_TASS_ABBAT_K3;
        wdfaCnbtEcc4x100K1.setWdfaCnbtEcc4x100K1FromBuffer(buffer, position);
        position += WdfaCnbtEcc4x100K1.Len.WDFA_CNBT_ECC4X100_K1;
        wdfaDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDFA_DS_RIGA, 0);
        position += Len.WDFA_DS_RIGA;
        wdfaDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdfaDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFA_DS_VER, 0);
        position += Len.WDFA_DS_VER;
        wdfaDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDFA_DS_TS_INI_CPTZ, 0);
        position += Len.WDFA_DS_TS_INI_CPTZ;
        wdfaDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDFA_DS_TS_END_CPTZ, 0);
        position += Len.WDFA_DS_TS_END_CPTZ;
        wdfaDsUtente = MarshalByte.readString(buffer, position, Len.WDFA_DS_UTENTE);
        position += Len.WDFA_DS_UTENTE;
        wdfaDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdfaCreditoIs.setWdfaCreditoIsFromBuffer(buffer, position);
        position += WdfaCreditoIs.Len.WDFA_CREDITO_IS;
        wdfaRedtTassAbbatK2.setWdfaRedtTassAbbatK2FromBuffer(buffer, position);
        position += WdfaRedtTassAbbatK2.Len.WDFA_REDT_TASS_ABBAT_K2;
        wdfaImpbIsK3.setWdfaImpbIsK3FromBuffer(buffer, position);
        position += WdfaImpbIsK3.Len.WDFA_IMPB_IS_K3;
        wdfaImpstSostK3.setWdfaImpstSostK3FromBuffer(buffer, position);
        position += WdfaImpstSostK3.Len.WDFA_IMPST_SOST_K3;
        wdfaImpb252K3.setWdfaImpb252K3FromBuffer(buffer, position);
        position += WdfaImpb252K3.Len.WDFA_IMPB252_K3;
        wdfaImpst252K3.setWdfaImpst252K3FromBuffer(buffer, position);
        position += WdfaImpst252K3.Len.WDFA_IMPST252_K3;
        wdfaImpbIsK3Antic.setWdfaImpbIsK3AnticFromBuffer(buffer, position);
        position += WdfaImpbIsK3Antic.Len.WDFA_IMPB_IS_K3_ANTIC;
        wdfaImpstSostK3Anti.setWdfaImpstSostK3AntiFromBuffer(buffer, position);
        position += WdfaImpstSostK3Anti.Len.WDFA_IMPST_SOST_K3_ANTI;
        wdfaImpbIrpefK1Anti.setWdfaImpbIrpefK1AntiFromBuffer(buffer, position);
        position += WdfaImpbIrpefK1Anti.Len.WDFA_IMPB_IRPEF_K1_ANTI;
        wdfaImpstIrpefK1Ant.setWdfaImpstIrpefK1AntFromBuffer(buffer, position);
        position += WdfaImpstIrpefK1Ant.Len.WDFA_IMPST_IRPEF_K1_ANT;
        wdfaImpbIrpefK2Anti.setWdfaImpbIrpefK2AntiFromBuffer(buffer, position);
        position += WdfaImpbIrpefK2Anti.Len.WDFA_IMPB_IRPEF_K2_ANTI;
        wdfaImpstIrpefK2Ant.setWdfaImpstIrpefK2AntFromBuffer(buffer, position);
        position += WdfaImpstIrpefK2Ant.Len.WDFA_IMPST_IRPEF_K2_ANT;
        wdfaDtCessazione.setWdfaDtCessazioneFromBuffer(buffer, position);
        position += WdfaDtCessazione.Len.WDFA_DT_CESSAZIONE;
        wdfaTotImpst.setWdfaTotImpstFromBuffer(buffer, position);
        position += WdfaTotImpst.Len.WDFA_TOT_IMPST;
        wdfaOnerTrasfe.setWdfaOnerTrasfeFromBuffer(buffer, position);
        position += WdfaOnerTrasfe.Len.WDFA_ONER_TRASFE;
        wdfaImpNetTrasferito.setWdfaImpNetTrasferitoFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wdfaIdDFiscAdes, Len.Int.WDFA_ID_D_FISC_ADES, 0);
        position += Len.WDFA_ID_D_FISC_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wdfaIdAdes, Len.Int.WDFA_ID_ADES, 0);
        position += Len.WDFA_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wdfaIdMoviCrz, Len.Int.WDFA_ID_MOVI_CRZ, 0);
        position += Len.WDFA_ID_MOVI_CRZ;
        wdfaIdMoviChiu.getWdfaIdMoviChiuAsBuffer(buffer, position);
        position += WdfaIdMoviChiu.Len.WDFA_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wdfaDtIniEff, Len.Int.WDFA_DT_INI_EFF, 0);
        position += Len.WDFA_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wdfaDtEndEff, Len.Int.WDFA_DT_END_EFF, 0);
        position += Len.WDFA_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wdfaCodCompAnia, Len.Int.WDFA_COD_COMP_ANIA, 0);
        position += Len.WDFA_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wdfaTpIscFnd, Len.WDFA_TP_ISC_FND);
        position += Len.WDFA_TP_ISC_FND;
        wdfaDtAccnsRappFnd.getWdfaDtAccnsRappFndAsBuffer(buffer, position);
        position += WdfaDtAccnsRappFnd.Len.WDFA_DT_ACCNS_RAPP_FND;
        wdfaImpCnbtAzK1.getWdfaImpCnbtAzK1AsBuffer(buffer, position);
        position += WdfaImpCnbtAzK1.Len.WDFA_IMP_CNBT_AZ_K1;
        wdfaImpCnbtIscK1.getWdfaImpCnbtIscK1AsBuffer(buffer, position);
        position += WdfaImpCnbtIscK1.Len.WDFA_IMP_CNBT_ISC_K1;
        wdfaImpCnbtTfrK1.getWdfaImpCnbtTfrK1AsBuffer(buffer, position);
        position += WdfaImpCnbtTfrK1.Len.WDFA_IMP_CNBT_TFR_K1;
        wdfaImpCnbtVolK1.getWdfaImpCnbtVolK1AsBuffer(buffer, position);
        position += WdfaImpCnbtVolK1.Len.WDFA_IMP_CNBT_VOL_K1;
        wdfaImpCnbtAzK2.getWdfaImpCnbtAzK2AsBuffer(buffer, position);
        position += WdfaImpCnbtAzK2.Len.WDFA_IMP_CNBT_AZ_K2;
        wdfaImpCnbtIscK2.getWdfaImpCnbtIscK2AsBuffer(buffer, position);
        position += WdfaImpCnbtIscK2.Len.WDFA_IMP_CNBT_ISC_K2;
        wdfaImpCnbtTfrK2.getWdfaImpCnbtTfrK2AsBuffer(buffer, position);
        position += WdfaImpCnbtTfrK2.Len.WDFA_IMP_CNBT_TFR_K2;
        wdfaImpCnbtVolK2.getWdfaImpCnbtVolK2AsBuffer(buffer, position);
        position += WdfaImpCnbtVolK2.Len.WDFA_IMP_CNBT_VOL_K2;
        wdfaMatuK1.getWdfaMatuK1AsBuffer(buffer, position);
        position += WdfaMatuK1.Len.WDFA_MATU_K1;
        wdfaMatuResK1.getWdfaMatuResK1AsBuffer(buffer, position);
        position += WdfaMatuResK1.Len.WDFA_MATU_RES_K1;
        wdfaMatuK2.getWdfaMatuK2AsBuffer(buffer, position);
        position += WdfaMatuK2.Len.WDFA_MATU_K2;
        wdfaImpbVis.getWdfaImpbVisAsBuffer(buffer, position);
        position += WdfaImpbVis.Len.WDFA_IMPB_VIS;
        wdfaImpstVis.getWdfaImpstVisAsBuffer(buffer, position);
        position += WdfaImpstVis.Len.WDFA_IMPST_VIS;
        wdfaImpbIsK2.getWdfaImpbIsK2AsBuffer(buffer, position);
        position += WdfaImpbIsK2.Len.WDFA_IMPB_IS_K2;
        wdfaImpstSostK2.getWdfaImpstSostK2AsBuffer(buffer, position);
        position += WdfaImpstSostK2.Len.WDFA_IMPST_SOST_K2;
        wdfaRidzTfr.getWdfaRidzTfrAsBuffer(buffer, position);
        position += WdfaRidzTfr.Len.WDFA_RIDZ_TFR;
        wdfaPcTfr.getWdfaPcTfrAsBuffer(buffer, position);
        position += WdfaPcTfr.Len.WDFA_PC_TFR;
        wdfaAlqTfr.getWdfaAlqTfrAsBuffer(buffer, position);
        position += WdfaAlqTfr.Len.WDFA_ALQ_TFR;
        wdfaTotAntic.getWdfaTotAnticAsBuffer(buffer, position);
        position += WdfaTotAntic.Len.WDFA_TOT_ANTIC;
        wdfaImpbTfrAntic.getWdfaImpbTfrAnticAsBuffer(buffer, position);
        position += WdfaImpbTfrAntic.Len.WDFA_IMPB_TFR_ANTIC;
        wdfaImpstTfrAntic.getWdfaImpstTfrAnticAsBuffer(buffer, position);
        position += WdfaImpstTfrAntic.Len.WDFA_IMPST_TFR_ANTIC;
        wdfaRidzTfrSuAntic.getWdfaRidzTfrSuAnticAsBuffer(buffer, position);
        position += WdfaRidzTfrSuAntic.Len.WDFA_RIDZ_TFR_SU_ANTIC;
        wdfaImpbVisAntic.getWdfaImpbVisAnticAsBuffer(buffer, position);
        position += WdfaImpbVisAntic.Len.WDFA_IMPB_VIS_ANTIC;
        wdfaImpstVisAntic.getWdfaImpstVisAnticAsBuffer(buffer, position);
        position += WdfaImpstVisAntic.Len.WDFA_IMPST_VIS_ANTIC;
        wdfaImpbIsK2Antic.getWdfaImpbIsK2AnticAsBuffer(buffer, position);
        position += WdfaImpbIsK2Antic.Len.WDFA_IMPB_IS_K2_ANTIC;
        wdfaImpstSostK2Anti.getWdfaImpstSostK2AntiAsBuffer(buffer, position);
        position += WdfaImpstSostK2Anti.Len.WDFA_IMPST_SOST_K2_ANTI;
        wdfaUltCommisTrasfe.getWdfaUltCommisTrasfeAsBuffer(buffer, position);
        position += WdfaUltCommisTrasfe.Len.WDFA_ULT_COMMIS_TRASFE;
        MarshalByte.writeString(buffer, position, wdfaCodDvs, Len.WDFA_COD_DVS);
        position += Len.WDFA_COD_DVS;
        wdfaAlqPrvr.getWdfaAlqPrvrAsBuffer(buffer, position);
        position += WdfaAlqPrvr.Len.WDFA_ALQ_PRVR;
        wdfaPcEseImpstTfr.getWdfaPcEseImpstTfrAsBuffer(buffer, position);
        position += WdfaPcEseImpstTfr.Len.WDFA_PC_ESE_IMPST_TFR;
        wdfaImpEseImpstTfr.getWdfaImpEseImpstTfrAsBuffer(buffer, position);
        position += WdfaImpEseImpstTfr.Len.WDFA_IMP_ESE_IMPST_TFR;
        wdfaAnzCnbtvaCarass.getWdfaAnzCnbtvaCarassAsBuffer(buffer, position);
        position += WdfaAnzCnbtvaCarass.Len.WDFA_ANZ_CNBTVA_CARASS;
        wdfaAnzCnbtvaCarazi.getWdfaAnzCnbtvaCaraziAsBuffer(buffer, position);
        position += WdfaAnzCnbtvaCarazi.Len.WDFA_ANZ_CNBTVA_CARAZI;
        wdfaAnzSrvz.getWdfaAnzSrvzAsBuffer(buffer, position);
        position += WdfaAnzSrvz.Len.WDFA_ANZ_SRVZ;
        wdfaImpCnbtNdedK1.getWdfaImpCnbtNdedK1AsBuffer(buffer, position);
        position += WdfaImpCnbtNdedK1.Len.WDFA_IMP_CNBT_NDED_K1;
        wdfaImpCnbtNdedK2.getWdfaImpCnbtNdedK2AsBuffer(buffer, position);
        position += WdfaImpCnbtNdedK2.Len.WDFA_IMP_CNBT_NDED_K2;
        wdfaImpCnbtNdedK3.getWdfaImpCnbtNdedK3AsBuffer(buffer, position);
        position += WdfaImpCnbtNdedK3.Len.WDFA_IMP_CNBT_NDED_K3;
        wdfaImpCnbtAzK3.getWdfaImpCnbtAzK3AsBuffer(buffer, position);
        position += WdfaImpCnbtAzK3.Len.WDFA_IMP_CNBT_AZ_K3;
        wdfaImpCnbtIscK3.getWdfaImpCnbtIscK3AsBuffer(buffer, position);
        position += WdfaImpCnbtIscK3.Len.WDFA_IMP_CNBT_ISC_K3;
        wdfaImpCnbtTfrK3.getWdfaImpCnbtTfrK3AsBuffer(buffer, position);
        position += WdfaImpCnbtTfrK3.Len.WDFA_IMP_CNBT_TFR_K3;
        wdfaImpCnbtVolK3.getWdfaImpCnbtVolK3AsBuffer(buffer, position);
        position += WdfaImpCnbtVolK3.Len.WDFA_IMP_CNBT_VOL_K3;
        wdfaMatuK3.getWdfaMatuK3AsBuffer(buffer, position);
        position += WdfaMatuK3.Len.WDFA_MATU_K3;
        wdfaImpb252Antic.getWdfaImpb252AnticAsBuffer(buffer, position);
        position += WdfaImpb252Antic.Len.WDFA_IMPB252_ANTIC;
        wdfaImpst252Antic.getWdfaImpst252AnticAsBuffer(buffer, position);
        position += WdfaImpst252Antic.Len.WDFA_IMPST252_ANTIC;
        wdfaDt1aCnbz.getWdfaDt1aCnbzAsBuffer(buffer, position);
        position += WdfaDt1aCnbz.Len.WDFA_DT1A_CNBZ;
        wdfaCommisDiTrasfe.getWdfaCommisDiTrasfeAsBuffer(buffer, position);
        position += WdfaCommisDiTrasfe.Len.WDFA_COMMIS_DI_TRASFE;
        wdfaAaCnbzK1.getWdfaAaCnbzK1AsBuffer(buffer, position);
        position += WdfaAaCnbzK1.Len.WDFA_AA_CNBZ_K1;
        wdfaAaCnbzK2.getWdfaAaCnbzK2AsBuffer(buffer, position);
        position += WdfaAaCnbzK2.Len.WDFA_AA_CNBZ_K2;
        wdfaAaCnbzK3.getWdfaAaCnbzK3AsBuffer(buffer, position);
        position += WdfaAaCnbzK3.Len.WDFA_AA_CNBZ_K3;
        wdfaMmCnbzK1.getWdfaMmCnbzK1AsBuffer(buffer, position);
        position += WdfaMmCnbzK1.Len.WDFA_MM_CNBZ_K1;
        wdfaMmCnbzK2.getWdfaMmCnbzK2AsBuffer(buffer, position);
        position += WdfaMmCnbzK2.Len.WDFA_MM_CNBZ_K2;
        wdfaMmCnbzK3.getWdfaMmCnbzK3AsBuffer(buffer, position);
        position += WdfaMmCnbzK3.Len.WDFA_MM_CNBZ_K3;
        MarshalByte.writeChar(buffer, position, wdfaFlApplzNewfis);
        position += Types.CHAR_SIZE;
        wdfaRedtTassAbbatK3.getWdfaRedtTassAbbatK3AsBuffer(buffer, position);
        position += WdfaRedtTassAbbatK3.Len.WDFA_REDT_TASS_ABBAT_K3;
        wdfaCnbtEcc4x100K1.getWdfaCnbtEcc4x100K1AsBuffer(buffer, position);
        position += WdfaCnbtEcc4x100K1.Len.WDFA_CNBT_ECC4X100_K1;
        MarshalByte.writeLongAsPacked(buffer, position, wdfaDsRiga, Len.Int.WDFA_DS_RIGA, 0);
        position += Len.WDFA_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wdfaDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wdfaDsVer, Len.Int.WDFA_DS_VER, 0);
        position += Len.WDFA_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wdfaDsTsIniCptz, Len.Int.WDFA_DS_TS_INI_CPTZ, 0);
        position += Len.WDFA_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wdfaDsTsEndCptz, Len.Int.WDFA_DS_TS_END_CPTZ, 0);
        position += Len.WDFA_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wdfaDsUtente, Len.WDFA_DS_UTENTE);
        position += Len.WDFA_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wdfaDsStatoElab);
        position += Types.CHAR_SIZE;
        wdfaCreditoIs.getWdfaCreditoIsAsBuffer(buffer, position);
        position += WdfaCreditoIs.Len.WDFA_CREDITO_IS;
        wdfaRedtTassAbbatK2.getWdfaRedtTassAbbatK2AsBuffer(buffer, position);
        position += WdfaRedtTassAbbatK2.Len.WDFA_REDT_TASS_ABBAT_K2;
        wdfaImpbIsK3.getWdfaImpbIsK3AsBuffer(buffer, position);
        position += WdfaImpbIsK3.Len.WDFA_IMPB_IS_K3;
        wdfaImpstSostK3.getWdfaImpstSostK3AsBuffer(buffer, position);
        position += WdfaImpstSostK3.Len.WDFA_IMPST_SOST_K3;
        wdfaImpb252K3.getWdfaImpb252K3AsBuffer(buffer, position);
        position += WdfaImpb252K3.Len.WDFA_IMPB252_K3;
        wdfaImpst252K3.getWdfaImpst252K3AsBuffer(buffer, position);
        position += WdfaImpst252K3.Len.WDFA_IMPST252_K3;
        wdfaImpbIsK3Antic.getWdfaImpbIsK3AnticAsBuffer(buffer, position);
        position += WdfaImpbIsK3Antic.Len.WDFA_IMPB_IS_K3_ANTIC;
        wdfaImpstSostK3Anti.getWdfaImpstSostK3AntiAsBuffer(buffer, position);
        position += WdfaImpstSostK3Anti.Len.WDFA_IMPST_SOST_K3_ANTI;
        wdfaImpbIrpefK1Anti.getWdfaImpbIrpefK1AntiAsBuffer(buffer, position);
        position += WdfaImpbIrpefK1Anti.Len.WDFA_IMPB_IRPEF_K1_ANTI;
        wdfaImpstIrpefK1Ant.getWdfaImpstIrpefK1AntAsBuffer(buffer, position);
        position += WdfaImpstIrpefK1Ant.Len.WDFA_IMPST_IRPEF_K1_ANT;
        wdfaImpbIrpefK2Anti.getWdfaImpbIrpefK2AntiAsBuffer(buffer, position);
        position += WdfaImpbIrpefK2Anti.Len.WDFA_IMPB_IRPEF_K2_ANTI;
        wdfaImpstIrpefK2Ant.getWdfaImpstIrpefK2AntAsBuffer(buffer, position);
        position += WdfaImpstIrpefK2Ant.Len.WDFA_IMPST_IRPEF_K2_ANT;
        wdfaDtCessazione.getWdfaDtCessazioneAsBuffer(buffer, position);
        position += WdfaDtCessazione.Len.WDFA_DT_CESSAZIONE;
        wdfaTotImpst.getWdfaTotImpstAsBuffer(buffer, position);
        position += WdfaTotImpst.Len.WDFA_TOT_IMPST;
        wdfaOnerTrasfe.getWdfaOnerTrasfeAsBuffer(buffer, position);
        position += WdfaOnerTrasfe.Len.WDFA_ONER_TRASFE;
        wdfaImpNetTrasferito.getWdfaImpNetTrasferitoAsBuffer(buffer, position);
        return buffer;
    }

    public void setWdfaIdDFiscAdes(int wdfaIdDFiscAdes) {
        this.wdfaIdDFiscAdes = wdfaIdDFiscAdes;
    }

    public int getWdfaIdDFiscAdes() {
        return this.wdfaIdDFiscAdes;
    }

    public void setWdfaIdAdes(int wdfaIdAdes) {
        this.wdfaIdAdes = wdfaIdAdes;
    }

    public int getWdfaIdAdes() {
        return this.wdfaIdAdes;
    }

    public void setWdfaIdMoviCrz(int wdfaIdMoviCrz) {
        this.wdfaIdMoviCrz = wdfaIdMoviCrz;
    }

    public int getWdfaIdMoviCrz() {
        return this.wdfaIdMoviCrz;
    }

    public void setWdfaDtIniEff(int wdfaDtIniEff) {
        this.wdfaDtIniEff = wdfaDtIniEff;
    }

    public int getWdfaDtIniEff() {
        return this.wdfaDtIniEff;
    }

    public void setWdfaDtEndEff(int wdfaDtEndEff) {
        this.wdfaDtEndEff = wdfaDtEndEff;
    }

    public int getWdfaDtEndEff() {
        return this.wdfaDtEndEff;
    }

    public void setWdfaCodCompAnia(int wdfaCodCompAnia) {
        this.wdfaCodCompAnia = wdfaCodCompAnia;
    }

    public int getWdfaCodCompAnia() {
        return this.wdfaCodCompAnia;
    }

    public void setWdfaTpIscFnd(String wdfaTpIscFnd) {
        this.wdfaTpIscFnd = Functions.subString(wdfaTpIscFnd, Len.WDFA_TP_ISC_FND);
    }

    public String getWdfaTpIscFnd() {
        return this.wdfaTpIscFnd;
    }

    public void setWdfaCodDvs(String wdfaCodDvs) {
        this.wdfaCodDvs = Functions.subString(wdfaCodDvs, Len.WDFA_COD_DVS);
    }

    public String getWdfaCodDvs() {
        return this.wdfaCodDvs;
    }

    public void setWdfaFlApplzNewfis(char wdfaFlApplzNewfis) {
        this.wdfaFlApplzNewfis = wdfaFlApplzNewfis;
    }

    public char getWdfaFlApplzNewfis() {
        return this.wdfaFlApplzNewfis;
    }

    public void setWdfaDsRiga(long wdfaDsRiga) {
        this.wdfaDsRiga = wdfaDsRiga;
    }

    public long getWdfaDsRiga() {
        return this.wdfaDsRiga;
    }

    public void setWdfaDsOperSql(char wdfaDsOperSql) {
        this.wdfaDsOperSql = wdfaDsOperSql;
    }

    public char getWdfaDsOperSql() {
        return this.wdfaDsOperSql;
    }

    public void setWdfaDsVer(int wdfaDsVer) {
        this.wdfaDsVer = wdfaDsVer;
    }

    public int getWdfaDsVer() {
        return this.wdfaDsVer;
    }

    public void setWdfaDsTsIniCptz(long wdfaDsTsIniCptz) {
        this.wdfaDsTsIniCptz = wdfaDsTsIniCptz;
    }

    public long getWdfaDsTsIniCptz() {
        return this.wdfaDsTsIniCptz;
    }

    public void setWdfaDsTsEndCptz(long wdfaDsTsEndCptz) {
        this.wdfaDsTsEndCptz = wdfaDsTsEndCptz;
    }

    public long getWdfaDsTsEndCptz() {
        return this.wdfaDsTsEndCptz;
    }

    public void setWdfaDsUtente(String wdfaDsUtente) {
        this.wdfaDsUtente = Functions.subString(wdfaDsUtente, Len.WDFA_DS_UTENTE);
    }

    public String getWdfaDsUtente() {
        return this.wdfaDsUtente;
    }

    public void setWdfaDsStatoElab(char wdfaDsStatoElab) {
        this.wdfaDsStatoElab = wdfaDsStatoElab;
    }

    public char getWdfaDsStatoElab() {
        return this.wdfaDsStatoElab;
    }

    public WdfaAaCnbzK1 getWdfaAaCnbzK1() {
        return wdfaAaCnbzK1;
    }

    public WdfaAaCnbzK2 getWdfaAaCnbzK2() {
        return wdfaAaCnbzK2;
    }

    public WdfaAaCnbzK3 getWdfaAaCnbzK3() {
        return wdfaAaCnbzK3;
    }

    public WdfaAlqPrvr getWdfaAlqPrvr() {
        return wdfaAlqPrvr;
    }

    public WdfaAlqTfr getWdfaAlqTfr() {
        return wdfaAlqTfr;
    }

    public WdfaAnzCnbtvaCarass getWdfaAnzCnbtvaCarass() {
        return wdfaAnzCnbtvaCarass;
    }

    public WdfaAnzCnbtvaCarazi getWdfaAnzCnbtvaCarazi() {
        return wdfaAnzCnbtvaCarazi;
    }

    public WdfaAnzSrvz getWdfaAnzSrvz() {
        return wdfaAnzSrvz;
    }

    public WdfaCnbtEcc4x100K1 getWdfaCnbtEcc4x100K1() {
        return wdfaCnbtEcc4x100K1;
    }

    public WdfaCommisDiTrasfe getWdfaCommisDiTrasfe() {
        return wdfaCommisDiTrasfe;
    }

    public WdfaCreditoIs getWdfaCreditoIs() {
        return wdfaCreditoIs;
    }

    public WdfaDt1aCnbz getWdfaDt1aCnbz() {
        return wdfaDt1aCnbz;
    }

    public WdfaDtAccnsRappFnd getWdfaDtAccnsRappFnd() {
        return wdfaDtAccnsRappFnd;
    }

    public WdfaDtCessazione getWdfaDtCessazione() {
        return wdfaDtCessazione;
    }

    public WdfaIdMoviChiu getWdfaIdMoviChiu() {
        return wdfaIdMoviChiu;
    }

    public WdfaImpCnbtAzK1 getWdfaImpCnbtAzK1() {
        return wdfaImpCnbtAzK1;
    }

    public WdfaImpCnbtAzK2 getWdfaImpCnbtAzK2() {
        return wdfaImpCnbtAzK2;
    }

    public WdfaImpCnbtAzK3 getWdfaImpCnbtAzK3() {
        return wdfaImpCnbtAzK3;
    }

    public WdfaImpCnbtIscK1 getWdfaImpCnbtIscK1() {
        return wdfaImpCnbtIscK1;
    }

    public WdfaImpCnbtIscK2 getWdfaImpCnbtIscK2() {
        return wdfaImpCnbtIscK2;
    }

    public WdfaImpCnbtIscK3 getWdfaImpCnbtIscK3() {
        return wdfaImpCnbtIscK3;
    }

    public WdfaImpCnbtNdedK1 getWdfaImpCnbtNdedK1() {
        return wdfaImpCnbtNdedK1;
    }

    public WdfaImpCnbtNdedK2 getWdfaImpCnbtNdedK2() {
        return wdfaImpCnbtNdedK2;
    }

    public WdfaImpCnbtNdedK3 getWdfaImpCnbtNdedK3() {
        return wdfaImpCnbtNdedK3;
    }

    public WdfaImpCnbtTfrK1 getWdfaImpCnbtTfrK1() {
        return wdfaImpCnbtTfrK1;
    }

    public WdfaImpCnbtTfrK2 getWdfaImpCnbtTfrK2() {
        return wdfaImpCnbtTfrK2;
    }

    public WdfaImpCnbtTfrK3 getWdfaImpCnbtTfrK3() {
        return wdfaImpCnbtTfrK3;
    }

    public WdfaImpCnbtVolK1 getWdfaImpCnbtVolK1() {
        return wdfaImpCnbtVolK1;
    }

    public WdfaImpCnbtVolK2 getWdfaImpCnbtVolK2() {
        return wdfaImpCnbtVolK2;
    }

    public WdfaImpCnbtVolK3 getWdfaImpCnbtVolK3() {
        return wdfaImpCnbtVolK3;
    }

    public WdfaImpEseImpstTfr getWdfaImpEseImpstTfr() {
        return wdfaImpEseImpstTfr;
    }

    public WdfaImpNetTrasferito getWdfaImpNetTrasferito() {
        return wdfaImpNetTrasferito;
    }

    public WdfaImpb252Antic getWdfaImpb252Antic() {
        return wdfaImpb252Antic;
    }

    public WdfaImpb252K3 getWdfaImpb252K3() {
        return wdfaImpb252K3;
    }

    public WdfaImpbIrpefK1Anti getWdfaImpbIrpefK1Anti() {
        return wdfaImpbIrpefK1Anti;
    }

    public WdfaImpbIrpefK2Anti getWdfaImpbIrpefK2Anti() {
        return wdfaImpbIrpefK2Anti;
    }

    public WdfaImpbIsK2 getWdfaImpbIsK2() {
        return wdfaImpbIsK2;
    }

    public WdfaImpbIsK2Antic getWdfaImpbIsK2Antic() {
        return wdfaImpbIsK2Antic;
    }

    public WdfaImpbIsK3 getWdfaImpbIsK3() {
        return wdfaImpbIsK3;
    }

    public WdfaImpbIsK3Antic getWdfaImpbIsK3Antic() {
        return wdfaImpbIsK3Antic;
    }

    public WdfaImpbTfrAntic getWdfaImpbTfrAntic() {
        return wdfaImpbTfrAntic;
    }

    public WdfaImpbVis getWdfaImpbVis() {
        return wdfaImpbVis;
    }

    public WdfaImpbVisAntic getWdfaImpbVisAntic() {
        return wdfaImpbVisAntic;
    }

    public WdfaImpst252Antic getWdfaImpst252Antic() {
        return wdfaImpst252Antic;
    }

    public WdfaImpst252K3 getWdfaImpst252K3() {
        return wdfaImpst252K3;
    }

    public WdfaImpstIrpefK1Ant getWdfaImpstIrpefK1Ant() {
        return wdfaImpstIrpefK1Ant;
    }

    public WdfaImpstIrpefK2Ant getWdfaImpstIrpefK2Ant() {
        return wdfaImpstIrpefK2Ant;
    }

    public WdfaImpstSostK2 getWdfaImpstSostK2() {
        return wdfaImpstSostK2;
    }

    public WdfaImpstSostK2Anti getWdfaImpstSostK2Anti() {
        return wdfaImpstSostK2Anti;
    }

    public WdfaImpstSostK3 getWdfaImpstSostK3() {
        return wdfaImpstSostK3;
    }

    public WdfaImpstSostK3Anti getWdfaImpstSostK3Anti() {
        return wdfaImpstSostK3Anti;
    }

    public WdfaImpstTfrAntic getWdfaImpstTfrAntic() {
        return wdfaImpstTfrAntic;
    }

    public WdfaImpstVis getWdfaImpstVis() {
        return wdfaImpstVis;
    }

    public WdfaImpstVisAntic getWdfaImpstVisAntic() {
        return wdfaImpstVisAntic;
    }

    public WdfaMatuK1 getWdfaMatuK1() {
        return wdfaMatuK1;
    }

    public WdfaMatuK2 getWdfaMatuK2() {
        return wdfaMatuK2;
    }

    public WdfaMatuK3 getWdfaMatuK3() {
        return wdfaMatuK3;
    }

    public WdfaMatuResK1 getWdfaMatuResK1() {
        return wdfaMatuResK1;
    }

    public WdfaMmCnbzK1 getWdfaMmCnbzK1() {
        return wdfaMmCnbzK1;
    }

    public WdfaMmCnbzK2 getWdfaMmCnbzK2() {
        return wdfaMmCnbzK2;
    }

    public WdfaMmCnbzK3 getWdfaMmCnbzK3() {
        return wdfaMmCnbzK3;
    }

    public WdfaOnerTrasfe getWdfaOnerTrasfe() {
        return wdfaOnerTrasfe;
    }

    public WdfaPcEseImpstTfr getWdfaPcEseImpstTfr() {
        return wdfaPcEseImpstTfr;
    }

    public WdfaPcTfr getWdfaPcTfr() {
        return wdfaPcTfr;
    }

    public WdfaRedtTassAbbatK2 getWdfaRedtTassAbbatK2() {
        return wdfaRedtTassAbbatK2;
    }

    public WdfaRedtTassAbbatK3 getWdfaRedtTassAbbatK3() {
        return wdfaRedtTassAbbatK3;
    }

    public WdfaRidzTfr getWdfaRidzTfr() {
        return wdfaRidzTfr;
    }

    public WdfaRidzTfrSuAntic getWdfaRidzTfrSuAntic() {
        return wdfaRidzTfrSuAntic;
    }

    public WdfaTotAntic getWdfaTotAntic() {
        return wdfaTotAntic;
    }

    public WdfaTotImpst getWdfaTotImpst() {
        return wdfaTotImpst;
    }

    public WdfaUltCommisTrasfe getWdfaUltCommisTrasfe() {
        return wdfaUltCommisTrasfe;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_ID_D_FISC_ADES = 5;
        public static final int WDFA_ID_ADES = 5;
        public static final int WDFA_ID_MOVI_CRZ = 5;
        public static final int WDFA_DT_INI_EFF = 5;
        public static final int WDFA_DT_END_EFF = 5;
        public static final int WDFA_COD_COMP_ANIA = 3;
        public static final int WDFA_TP_ISC_FND = 2;
        public static final int WDFA_COD_DVS = 20;
        public static final int WDFA_FL_APPLZ_NEWFIS = 1;
        public static final int WDFA_DS_RIGA = 6;
        public static final int WDFA_DS_OPER_SQL = 1;
        public static final int WDFA_DS_VER = 5;
        public static final int WDFA_DS_TS_INI_CPTZ = 10;
        public static final int WDFA_DS_TS_END_CPTZ = 10;
        public static final int WDFA_DS_UTENTE = 20;
        public static final int WDFA_DS_STATO_ELAB = 1;
        public static final int DATI = WDFA_ID_D_FISC_ADES + WDFA_ID_ADES + WDFA_ID_MOVI_CRZ + WdfaIdMoviChiu.Len.WDFA_ID_MOVI_CHIU + WDFA_DT_INI_EFF + WDFA_DT_END_EFF + WDFA_COD_COMP_ANIA + WDFA_TP_ISC_FND + WdfaDtAccnsRappFnd.Len.WDFA_DT_ACCNS_RAPP_FND + WdfaImpCnbtAzK1.Len.WDFA_IMP_CNBT_AZ_K1 + WdfaImpCnbtIscK1.Len.WDFA_IMP_CNBT_ISC_K1 + WdfaImpCnbtTfrK1.Len.WDFA_IMP_CNBT_TFR_K1 + WdfaImpCnbtVolK1.Len.WDFA_IMP_CNBT_VOL_K1 + WdfaImpCnbtAzK2.Len.WDFA_IMP_CNBT_AZ_K2 + WdfaImpCnbtIscK2.Len.WDFA_IMP_CNBT_ISC_K2 + WdfaImpCnbtTfrK2.Len.WDFA_IMP_CNBT_TFR_K2 + WdfaImpCnbtVolK2.Len.WDFA_IMP_CNBT_VOL_K2 + WdfaMatuK1.Len.WDFA_MATU_K1 + WdfaMatuResK1.Len.WDFA_MATU_RES_K1 + WdfaMatuK2.Len.WDFA_MATU_K2 + WdfaImpbVis.Len.WDFA_IMPB_VIS + WdfaImpstVis.Len.WDFA_IMPST_VIS + WdfaImpbIsK2.Len.WDFA_IMPB_IS_K2 + WdfaImpstSostK2.Len.WDFA_IMPST_SOST_K2 + WdfaRidzTfr.Len.WDFA_RIDZ_TFR + WdfaPcTfr.Len.WDFA_PC_TFR + WdfaAlqTfr.Len.WDFA_ALQ_TFR + WdfaTotAntic.Len.WDFA_TOT_ANTIC + WdfaImpbTfrAntic.Len.WDFA_IMPB_TFR_ANTIC + WdfaImpstTfrAntic.Len.WDFA_IMPST_TFR_ANTIC + WdfaRidzTfrSuAntic.Len.WDFA_RIDZ_TFR_SU_ANTIC + WdfaImpbVisAntic.Len.WDFA_IMPB_VIS_ANTIC + WdfaImpstVisAntic.Len.WDFA_IMPST_VIS_ANTIC + WdfaImpbIsK2Antic.Len.WDFA_IMPB_IS_K2_ANTIC + WdfaImpstSostK2Anti.Len.WDFA_IMPST_SOST_K2_ANTI + WdfaUltCommisTrasfe.Len.WDFA_ULT_COMMIS_TRASFE + WDFA_COD_DVS + WdfaAlqPrvr.Len.WDFA_ALQ_PRVR + WdfaPcEseImpstTfr.Len.WDFA_PC_ESE_IMPST_TFR + WdfaImpEseImpstTfr.Len.WDFA_IMP_ESE_IMPST_TFR + WdfaAnzCnbtvaCarass.Len.WDFA_ANZ_CNBTVA_CARASS + WdfaAnzCnbtvaCarazi.Len.WDFA_ANZ_CNBTVA_CARAZI + WdfaAnzSrvz.Len.WDFA_ANZ_SRVZ + WdfaImpCnbtNdedK1.Len.WDFA_IMP_CNBT_NDED_K1 + WdfaImpCnbtNdedK2.Len.WDFA_IMP_CNBT_NDED_K2 + WdfaImpCnbtNdedK3.Len.WDFA_IMP_CNBT_NDED_K3 + WdfaImpCnbtAzK3.Len.WDFA_IMP_CNBT_AZ_K3 + WdfaImpCnbtIscK3.Len.WDFA_IMP_CNBT_ISC_K3 + WdfaImpCnbtTfrK3.Len.WDFA_IMP_CNBT_TFR_K3 + WdfaImpCnbtVolK3.Len.WDFA_IMP_CNBT_VOL_K3 + WdfaMatuK3.Len.WDFA_MATU_K3 + WdfaImpb252Antic.Len.WDFA_IMPB252_ANTIC + WdfaImpst252Antic.Len.WDFA_IMPST252_ANTIC + WdfaDt1aCnbz.Len.WDFA_DT1A_CNBZ + WdfaCommisDiTrasfe.Len.WDFA_COMMIS_DI_TRASFE + WdfaAaCnbzK1.Len.WDFA_AA_CNBZ_K1 + WdfaAaCnbzK2.Len.WDFA_AA_CNBZ_K2 + WdfaAaCnbzK3.Len.WDFA_AA_CNBZ_K3 + WdfaMmCnbzK1.Len.WDFA_MM_CNBZ_K1 + WdfaMmCnbzK2.Len.WDFA_MM_CNBZ_K2 + WdfaMmCnbzK3.Len.WDFA_MM_CNBZ_K3 + WDFA_FL_APPLZ_NEWFIS + WdfaRedtTassAbbatK3.Len.WDFA_REDT_TASS_ABBAT_K3 + WdfaCnbtEcc4x100K1.Len.WDFA_CNBT_ECC4X100_K1 + WDFA_DS_RIGA + WDFA_DS_OPER_SQL + WDFA_DS_VER + WDFA_DS_TS_INI_CPTZ + WDFA_DS_TS_END_CPTZ + WDFA_DS_UTENTE + WDFA_DS_STATO_ELAB + WdfaCreditoIs.Len.WDFA_CREDITO_IS + WdfaRedtTassAbbatK2.Len.WDFA_REDT_TASS_ABBAT_K2 + WdfaImpbIsK3.Len.WDFA_IMPB_IS_K3 + WdfaImpstSostK3.Len.WDFA_IMPST_SOST_K3 + WdfaImpb252K3.Len.WDFA_IMPB252_K3 + WdfaImpst252K3.Len.WDFA_IMPST252_K3 + WdfaImpbIsK3Antic.Len.WDFA_IMPB_IS_K3_ANTIC + WdfaImpstSostK3Anti.Len.WDFA_IMPST_SOST_K3_ANTI + WdfaImpbIrpefK1Anti.Len.WDFA_IMPB_IRPEF_K1_ANTI + WdfaImpstIrpefK1Ant.Len.WDFA_IMPST_IRPEF_K1_ANT + WdfaImpbIrpefK2Anti.Len.WDFA_IMPB_IRPEF_K2_ANTI + WdfaImpstIrpefK2Ant.Len.WDFA_IMPST_IRPEF_K2_ANT + WdfaDtCessazione.Len.WDFA_DT_CESSAZIONE + WdfaTotImpst.Len.WDFA_TOT_IMPST + WdfaOnerTrasfe.Len.WDFA_ONER_TRASFE + WdfaImpNetTrasferito.Len.WDFA_IMP_NET_TRASFERITO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_ID_D_FISC_ADES = 9;
            public static final int WDFA_ID_ADES = 9;
            public static final int WDFA_ID_MOVI_CRZ = 9;
            public static final int WDFA_DT_INI_EFF = 8;
            public static final int WDFA_DT_END_EFF = 8;
            public static final int WDFA_COD_COMP_ANIA = 5;
            public static final int WDFA_DS_RIGA = 10;
            public static final int WDFA_DS_VER = 9;
            public static final int WDFA_DS_TS_INI_CPTZ = 18;
            public static final int WDFA_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
