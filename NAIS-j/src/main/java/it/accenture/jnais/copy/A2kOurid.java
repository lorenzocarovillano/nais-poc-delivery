package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: A2K-OURID<br>
 * Variable: A2K-OURID from copybook LCCC0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class A2kOurid {

    //==== PROPERTIES ====
    //Original name: A2K-OUGG05
    private String ougg05 = DefaultValues.stringVal(Len.OUGG05);
    //Original name: FILLER-A2K-OURID
    private char flr1 = DefaultValues.CHAR_VAL;
    //Original name: A2K-OUMM05
    private String oumm05 = DefaultValues.stringVal(Len.OUMM05);
    //Original name: FILLER-A2K-OURID-1
    private char flr2 = DefaultValues.CHAR_VAL;
    //Original name: A2K-OUAA05
    private String ouaa05 = DefaultValues.stringVal(Len.OUAA05);

    //==== METHODS ====
    public void setA2kOuridBytes(byte[] buffer, int offset) {
        int position = offset;
        ougg05 = MarshalByte.readFixedString(buffer, position, Len.OUGG05);
        position += Len.OUGG05;
        flr1 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        oumm05 = MarshalByte.readString(buffer, position, Len.OUMM05);
        position += Len.OUMM05;
        flr2 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ouaa05 = MarshalByte.readFixedString(buffer, position, Len.OUAA05);
    }

    public byte[] getA2kOuridBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ougg05, Len.OUGG05);
        position += Len.OUGG05;
        MarshalByte.writeChar(buffer, position, flr1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, oumm05, Len.OUMM05);
        position += Len.OUMM05;
        MarshalByte.writeChar(buffer, position, flr2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ouaa05, Len.OUAA05);
        return buffer;
    }

    public void initA2kOuridSpaces() {
        ougg05 = "";
        flr1 = Types.SPACE_CHAR;
        oumm05 = "";
        flr2 = Types.SPACE_CHAR;
        ouaa05 = "";
    }

    public void setOugg05Formatted(String ougg05) {
        this.ougg05 = Trunc.toUnsignedNumeric(ougg05, Len.OUGG05);
    }

    public short getOugg05() {
        return NumericDisplay.asShort(this.ougg05);
    }

    public void setFlr1(char flr1) {
        this.flr1 = flr1;
    }

    public char getFlr1() {
        return this.flr1;
    }

    public void setOumm05(String oumm05) {
        this.oumm05 = Functions.subString(oumm05, Len.OUMM05);
    }

    public String getOumm05() {
        return this.oumm05;
    }

    public void setFlr2(char flr2) {
        this.flr2 = flr2;
    }

    public char getFlr2() {
        return this.flr2;
    }

    public void setOuaa05Formatted(String ouaa05) {
        this.ouaa05 = Trunc.toUnsignedNumeric(ouaa05, Len.OUAA05);
    }

    public short getOuaa05() {
        return NumericDisplay.asShort(this.ouaa05);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OUGG05 = 2;
        public static final int FLR1 = 1;
        public static final int OUMM05 = 3;
        public static final int OUAA05 = 2;
        public static final int A2K_OURID = OUGG05 + OUMM05 + OUAA05 + 2 * FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
