package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVC821-POLI<br>
 * Variable: LDBVC821-POLI from copybook LDBVC821<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbvc821Poli {

    //==== PROPERTIES ====
    //Original name: LC821-POL-ID-POLI
    private int polIdPoli = DefaultValues.INT_VAL;
    //Original name: LC821-POL-ID-MOVI-CRZ
    private int polIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: LC821-POL-ID-MOVI-CHIU
    private int polIdMoviChiu = DefaultValues.INT_VAL;
    //Original name: LC821-POL-IB-OGG
    private String polIbOgg = DefaultValues.stringVal(Len.POL_IB_OGG);
    //Original name: LC821-POL-IB-PROP
    private String polIbProp = DefaultValues.stringVal(Len.POL_IB_PROP);
    //Original name: LC821-POL-DT-PROP
    private int polDtProp = DefaultValues.INT_VAL;
    //Original name: LC821-POL-DT-INI-EFF
    private int polDtIniEff = DefaultValues.INT_VAL;
    //Original name: LC821-POL-DT-END-EFF
    private int polDtEndEff = DefaultValues.INT_VAL;
    //Original name: LC821-POL-COD-COMP-ANIA
    private int polCodCompAnia = DefaultValues.INT_VAL;
    //Original name: LC821-POL-DT-DECOR
    private int polDtDecor = DefaultValues.INT_VAL;
    //Original name: LC821-POL-DT-EMIS
    private int polDtEmis = DefaultValues.INT_VAL;
    //Original name: LC821-POL-TP-POLI
    private String polTpPoli = DefaultValues.stringVal(Len.POL_TP_POLI);
    //Original name: LC821-POL-DUR-AA
    private int polDurAa = DefaultValues.INT_VAL;
    //Original name: LC821-POL-DUR-MM
    private int polDurMm = DefaultValues.INT_VAL;
    //Original name: LC821-POL-DT-SCAD
    private int polDtScad = DefaultValues.INT_VAL;
    //Original name: LC821-POL-COD-PROD
    private String polCodProd = DefaultValues.stringVal(Len.POL_COD_PROD);
    //Original name: LC821-POL-DT-INI-VLDT-PROD
    private int polDtIniVldtProd = DefaultValues.INT_VAL;
    //Original name: LC821-POL-COD-CONV
    private String polCodConv = DefaultValues.stringVal(Len.POL_COD_CONV);
    //Original name: LC821-POL-COD-RAMO
    private String polCodRamo = DefaultValues.stringVal(Len.POL_COD_RAMO);
    //Original name: LC821-POL-DT-INI-VLDT-CONV
    private int polDtIniVldtConv = DefaultValues.INT_VAL;
    //Original name: LC821-POL-DT-APPLZ-CONV
    private int polDtApplzConv = DefaultValues.INT_VAL;
    //Original name: LC821-POL-TP-FRM-ASSVA
    private String polTpFrmAssva = DefaultValues.stringVal(Len.POL_TP_FRM_ASSVA);
    //Original name: LC821-POL-TP-RGM-FISC
    private String polTpRgmFisc = DefaultValues.stringVal(Len.POL_TP_RGM_FISC);
    //Original name: LC821-POL-FL-ESTAS
    private char polFlEstas = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-FL-RSH-COMUN
    private char polFlRshComun = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-FL-RSH-COMUN-COND
    private char polFlRshComunCond = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-TP-LIV-GENZ-TIT
    private String polTpLivGenzTit = DefaultValues.stringVal(Len.POL_TP_LIV_GENZ_TIT);
    //Original name: LC821-POL-FL-COP-FINANZ
    private char polFlCopFinanz = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-TP-APPLZ-DIR
    private String polTpApplzDir = DefaultValues.stringVal(Len.POL_TP_APPLZ_DIR);
    //Original name: LC821-POL-SPE-MED
    private AfDecimal polSpeMed = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-POL-DIR-EMIS
    private AfDecimal polDirEmis = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-POL-DIR-1O-VERS
    private AfDecimal polDir1oVers = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-POL-DIR-VERS-AGG
    private AfDecimal polDirVersAgg = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-POL-COD-DVS
    private String polCodDvs = DefaultValues.stringVal(Len.POL_COD_DVS);
    //Original name: LC821-POL-FL-FNT-AZ
    private char polFlFntAz = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-FL-FNT-ADER
    private char polFlFntAder = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-FL-FNT-TFR
    private char polFlFntTfr = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-FL-FNT-VOLO
    private char polFlFntVolo = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-TP-OPZ-A-SCAD
    private String polTpOpzAScad = DefaultValues.stringVal(Len.POL_TP_OPZ_A_SCAD);
    //Original name: LC821-POL-AA-DIFF-PROR-DFLT
    private int polAaDiffProrDflt = DefaultValues.INT_VAL;
    //Original name: LC821-POL-FL-VER-PROD
    private String polFlVerProd = DefaultValues.stringVal(Len.POL_FL_VER_PROD);
    //Original name: LC821-POL-DUR-GG
    private int polDurGg = DefaultValues.INT_VAL;
    //Original name: LC821-POL-DIR-QUIET
    private AfDecimal polDirQuiet = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-POL-TP-PTF-ESTNO
    private String polTpPtfEstno = DefaultValues.stringVal(Len.POL_TP_PTF_ESTNO);
    //Original name: LC821-POL-FL-CUM-PRE-CNTR
    private char polFlCumPreCntr = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-FL-AMMB-MOVI
    private char polFlAmmbMovi = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-CONV-GECO
    private String polConvGeco = DefaultValues.stringVal(Len.POL_CONV_GECO);
    //Original name: LC821-POL-DS-RIGA
    private long polDsRiga = DefaultValues.LONG_VAL;
    //Original name: LC821-POL-DS-OPER-SQL
    private char polDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-DS-VER
    private int polDsVer = DefaultValues.INT_VAL;
    //Original name: LC821-POL-DS-TS-INI-CPTZ
    private long polDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: LC821-POL-DS-TS-END-CPTZ
    private long polDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: LC821-POL-DS-UTENTE
    private String polDsUtente = DefaultValues.stringVal(Len.POL_DS_UTENTE);
    //Original name: LC821-POL-DS-STATO-ELAB
    private char polDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-FL-SCUDO-FISC
    private char polFlScudoFisc = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-FL-TRASFE
    private char polFlTrasfe = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-FL-TFR-STRC
    private char polFlTfrStrc = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-DT-PRESC
    private int polDtPresc = DefaultValues.INT_VAL;
    //Original name: LC821-POL-COD-CONV-AGG
    private String polCodConvAgg = DefaultValues.stringVal(Len.POL_COD_CONV_AGG);
    //Original name: LC821-POL-SUBCAT-PROD
    private String polSubcatProd = DefaultValues.stringVal(Len.POL_SUBCAT_PROD);
    //Original name: LC821-POL-FL-QUEST-ADEGZ-ASS
    private char polFlQuestAdegzAss = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-COD-TPA
    private String polCodTpa = DefaultValues.stringVal(Len.POL_COD_TPA);
    //Original name: LC821-POL-ID-ACC-COMM
    private int polIdAccComm = DefaultValues.INT_VAL;
    //Original name: LC821-POL-FL-POLI-CPI-PR
    private char polFlPoliCpiPr = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-FL-POLI-BUNDLING
    private char polFlPoliBundling = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-IND-POLI-PRIN-COLL
    private char polIndPoliPrinColl = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-FL-VND-BUNDLE
    private char polFlVndBundle = DefaultValues.CHAR_VAL;
    //Original name: LC821-POL-IB-BS
    private String polIbBs = DefaultValues.stringVal(Len.POL_IB_BS);
    //Original name: LC821-FL-POLI-IFP
    private char flPoliIfp = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setLdbvc821PoliBytes(byte[] buffer) {
        setLdbvc821PoliBytes(buffer, 1);
    }

    public byte[] getLdbvc821PoliBytes() {
        byte[] buffer = new byte[Len.LDBVC821_POLI];
        return getLdbvc821PoliBytes(buffer, 1);
    }

    public void setLdbvc821PoliBytes(byte[] buffer, int offset) {
        int position = offset;
        polIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_ID_POLI, 0);
        position += Len.POL_ID_POLI;
        polIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_ID_MOVI_CRZ, 0);
        position += Len.POL_ID_MOVI_CRZ;
        polIdMoviChiu = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_ID_MOVI_CHIU, 0);
        position += Len.POL_ID_MOVI_CHIU;
        polIbOgg = MarshalByte.readString(buffer, position, Len.POL_IB_OGG);
        position += Len.POL_IB_OGG;
        polIbProp = MarshalByte.readString(buffer, position, Len.POL_IB_PROP);
        position += Len.POL_IB_PROP;
        polDtProp = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_PROP, 0);
        position += Len.POL_DT_PROP;
        polDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_INI_EFF, 0);
        position += Len.POL_DT_INI_EFF;
        polDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_END_EFF, 0);
        position += Len.POL_DT_END_EFF;
        polCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_COD_COMP_ANIA, 0);
        position += Len.POL_COD_COMP_ANIA;
        polDtDecor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_DECOR, 0);
        position += Len.POL_DT_DECOR;
        polDtEmis = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_EMIS, 0);
        position += Len.POL_DT_EMIS;
        polTpPoli = MarshalByte.readString(buffer, position, Len.POL_TP_POLI);
        position += Len.POL_TP_POLI;
        polDurAa = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DUR_AA, 0);
        position += Len.POL_DUR_AA;
        polDurMm = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DUR_MM, 0);
        position += Len.POL_DUR_MM;
        polDtScad = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_SCAD, 0);
        position += Len.POL_DT_SCAD;
        polCodProd = MarshalByte.readString(buffer, position, Len.POL_COD_PROD);
        position += Len.POL_COD_PROD;
        polDtIniVldtProd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_INI_VLDT_PROD, 0);
        position += Len.POL_DT_INI_VLDT_PROD;
        polCodConv = MarshalByte.readString(buffer, position, Len.POL_COD_CONV);
        position += Len.POL_COD_CONV;
        polCodRamo = MarshalByte.readString(buffer, position, Len.POL_COD_RAMO);
        position += Len.POL_COD_RAMO;
        polDtIniVldtConv = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_INI_VLDT_CONV, 0);
        position += Len.POL_DT_INI_VLDT_CONV;
        polDtApplzConv = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_APPLZ_CONV, 0);
        position += Len.POL_DT_APPLZ_CONV;
        polTpFrmAssva = MarshalByte.readString(buffer, position, Len.POL_TP_FRM_ASSVA);
        position += Len.POL_TP_FRM_ASSVA;
        polTpRgmFisc = MarshalByte.readString(buffer, position, Len.POL_TP_RGM_FISC);
        position += Len.POL_TP_RGM_FISC;
        polFlEstas = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlRshComun = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlRshComunCond = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polTpLivGenzTit = MarshalByte.readString(buffer, position, Len.POL_TP_LIV_GENZ_TIT);
        position += Len.POL_TP_LIV_GENZ_TIT;
        polFlCopFinanz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polTpApplzDir = MarshalByte.readString(buffer, position, Len.POL_TP_APPLZ_DIR);
        position += Len.POL_TP_APPLZ_DIR;
        polSpeMed.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.POL_SPE_MED, Len.Fract.POL_SPE_MED));
        position += Len.POL_SPE_MED;
        polDirEmis.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.POL_DIR_EMIS, Len.Fract.POL_DIR_EMIS));
        position += Len.POL_DIR_EMIS;
        polDir1oVers.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.POL_DIR1O_VERS, Len.Fract.POL_DIR1O_VERS));
        position += Len.POL_DIR1O_VERS;
        polDirVersAgg.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.POL_DIR_VERS_AGG, Len.Fract.POL_DIR_VERS_AGG));
        position += Len.POL_DIR_VERS_AGG;
        polCodDvs = MarshalByte.readString(buffer, position, Len.POL_COD_DVS);
        position += Len.POL_COD_DVS;
        polFlFntAz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlFntAder = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlFntTfr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlFntVolo = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polTpOpzAScad = MarshalByte.readString(buffer, position, Len.POL_TP_OPZ_A_SCAD);
        position += Len.POL_TP_OPZ_A_SCAD;
        polAaDiffProrDflt = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_AA_DIFF_PROR_DFLT, 0);
        position += Len.POL_AA_DIFF_PROR_DFLT;
        polFlVerProd = MarshalByte.readString(buffer, position, Len.POL_FL_VER_PROD);
        position += Len.POL_FL_VER_PROD;
        polDurGg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DUR_GG, 0);
        position += Len.POL_DUR_GG;
        polDirQuiet.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.POL_DIR_QUIET, Len.Fract.POL_DIR_QUIET));
        position += Len.POL_DIR_QUIET;
        polTpPtfEstno = MarshalByte.readString(buffer, position, Len.POL_TP_PTF_ESTNO);
        position += Len.POL_TP_PTF_ESTNO;
        polFlCumPreCntr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlAmmbMovi = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polConvGeco = MarshalByte.readString(buffer, position, Len.POL_CONV_GECO);
        position += Len.POL_CONV_GECO;
        polDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.POL_DS_RIGA, 0);
        position += Len.POL_DS_RIGA;
        polDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DS_VER, 0);
        position += Len.POL_DS_VER;
        polDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.POL_DS_TS_INI_CPTZ, 0);
        position += Len.POL_DS_TS_INI_CPTZ;
        polDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.POL_DS_TS_END_CPTZ, 0);
        position += Len.POL_DS_TS_END_CPTZ;
        polDsUtente = MarshalByte.readString(buffer, position, Len.POL_DS_UTENTE);
        position += Len.POL_DS_UTENTE;
        polDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlScudoFisc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlTrasfe = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlTfrStrc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polDtPresc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_DT_PRESC, 0);
        position += Len.POL_DT_PRESC;
        polCodConvAgg = MarshalByte.readString(buffer, position, Len.POL_COD_CONV_AGG);
        position += Len.POL_COD_CONV_AGG;
        polSubcatProd = MarshalByte.readString(buffer, position, Len.POL_SUBCAT_PROD);
        position += Len.POL_SUBCAT_PROD;
        polFlQuestAdegzAss = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polCodTpa = MarshalByte.readString(buffer, position, Len.POL_COD_TPA);
        position += Len.POL_COD_TPA;
        polIdAccComm = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POL_ID_ACC_COMM, 0);
        position += Len.POL_ID_ACC_COMM;
        polFlPoliCpiPr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlPoliBundling = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polIndPoliPrinColl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polFlVndBundle = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        polIbBs = MarshalByte.readString(buffer, position, Len.POL_IB_BS);
        position += Len.POL_IB_BS;
        flPoliIfp = MarshalByte.readChar(buffer, position);
    }

    public byte[] getLdbvc821PoliBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, polIdPoli, Len.Int.POL_ID_POLI, 0);
        position += Len.POL_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, polIdMoviCrz, Len.Int.POL_ID_MOVI_CRZ, 0);
        position += Len.POL_ID_MOVI_CRZ;
        MarshalByte.writeIntAsPacked(buffer, position, polIdMoviChiu, Len.Int.POL_ID_MOVI_CHIU, 0);
        position += Len.POL_ID_MOVI_CHIU;
        MarshalByte.writeString(buffer, position, polIbOgg, Len.POL_IB_OGG);
        position += Len.POL_IB_OGG;
        MarshalByte.writeString(buffer, position, polIbProp, Len.POL_IB_PROP);
        position += Len.POL_IB_PROP;
        MarshalByte.writeIntAsPacked(buffer, position, polDtProp, Len.Int.POL_DT_PROP, 0);
        position += Len.POL_DT_PROP;
        MarshalByte.writeIntAsPacked(buffer, position, polDtIniEff, Len.Int.POL_DT_INI_EFF, 0);
        position += Len.POL_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, polDtEndEff, Len.Int.POL_DT_END_EFF, 0);
        position += Len.POL_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, polCodCompAnia, Len.Int.POL_COD_COMP_ANIA, 0);
        position += Len.POL_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, polDtDecor, Len.Int.POL_DT_DECOR, 0);
        position += Len.POL_DT_DECOR;
        MarshalByte.writeIntAsPacked(buffer, position, polDtEmis, Len.Int.POL_DT_EMIS, 0);
        position += Len.POL_DT_EMIS;
        MarshalByte.writeString(buffer, position, polTpPoli, Len.POL_TP_POLI);
        position += Len.POL_TP_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, polDurAa, Len.Int.POL_DUR_AA, 0);
        position += Len.POL_DUR_AA;
        MarshalByte.writeIntAsPacked(buffer, position, polDurMm, Len.Int.POL_DUR_MM, 0);
        position += Len.POL_DUR_MM;
        MarshalByte.writeIntAsPacked(buffer, position, polDtScad, Len.Int.POL_DT_SCAD, 0);
        position += Len.POL_DT_SCAD;
        MarshalByte.writeString(buffer, position, polCodProd, Len.POL_COD_PROD);
        position += Len.POL_COD_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, polDtIniVldtProd, Len.Int.POL_DT_INI_VLDT_PROD, 0);
        position += Len.POL_DT_INI_VLDT_PROD;
        MarshalByte.writeString(buffer, position, polCodConv, Len.POL_COD_CONV);
        position += Len.POL_COD_CONV;
        MarshalByte.writeString(buffer, position, polCodRamo, Len.POL_COD_RAMO);
        position += Len.POL_COD_RAMO;
        MarshalByte.writeIntAsPacked(buffer, position, polDtIniVldtConv, Len.Int.POL_DT_INI_VLDT_CONV, 0);
        position += Len.POL_DT_INI_VLDT_CONV;
        MarshalByte.writeIntAsPacked(buffer, position, polDtApplzConv, Len.Int.POL_DT_APPLZ_CONV, 0);
        position += Len.POL_DT_APPLZ_CONV;
        MarshalByte.writeString(buffer, position, polTpFrmAssva, Len.POL_TP_FRM_ASSVA);
        position += Len.POL_TP_FRM_ASSVA;
        MarshalByte.writeString(buffer, position, polTpRgmFisc, Len.POL_TP_RGM_FISC);
        position += Len.POL_TP_RGM_FISC;
        MarshalByte.writeChar(buffer, position, polFlEstas);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlRshComun);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlRshComunCond);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polTpLivGenzTit, Len.POL_TP_LIV_GENZ_TIT);
        position += Len.POL_TP_LIV_GENZ_TIT;
        MarshalByte.writeChar(buffer, position, polFlCopFinanz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polTpApplzDir, Len.POL_TP_APPLZ_DIR);
        position += Len.POL_TP_APPLZ_DIR;
        MarshalByte.writeDecimalAsPacked(buffer, position, polSpeMed.copy());
        position += Len.POL_SPE_MED;
        MarshalByte.writeDecimalAsPacked(buffer, position, polDirEmis.copy());
        position += Len.POL_DIR_EMIS;
        MarshalByte.writeDecimalAsPacked(buffer, position, polDir1oVers.copy());
        position += Len.POL_DIR1O_VERS;
        MarshalByte.writeDecimalAsPacked(buffer, position, polDirVersAgg.copy());
        position += Len.POL_DIR_VERS_AGG;
        MarshalByte.writeString(buffer, position, polCodDvs, Len.POL_COD_DVS);
        position += Len.POL_COD_DVS;
        MarshalByte.writeChar(buffer, position, polFlFntAz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlFntAder);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlFntTfr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlFntVolo);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polTpOpzAScad, Len.POL_TP_OPZ_A_SCAD);
        position += Len.POL_TP_OPZ_A_SCAD;
        MarshalByte.writeIntAsPacked(buffer, position, polAaDiffProrDflt, Len.Int.POL_AA_DIFF_PROR_DFLT, 0);
        position += Len.POL_AA_DIFF_PROR_DFLT;
        MarshalByte.writeString(buffer, position, polFlVerProd, Len.POL_FL_VER_PROD);
        position += Len.POL_FL_VER_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, polDurGg, Len.Int.POL_DUR_GG, 0);
        position += Len.POL_DUR_GG;
        MarshalByte.writeDecimalAsPacked(buffer, position, polDirQuiet.copy());
        position += Len.POL_DIR_QUIET;
        MarshalByte.writeString(buffer, position, polTpPtfEstno, Len.POL_TP_PTF_ESTNO);
        position += Len.POL_TP_PTF_ESTNO;
        MarshalByte.writeChar(buffer, position, polFlCumPreCntr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlAmmbMovi);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polConvGeco, Len.POL_CONV_GECO);
        position += Len.POL_CONV_GECO;
        MarshalByte.writeLongAsPacked(buffer, position, polDsRiga, Len.Int.POL_DS_RIGA, 0);
        position += Len.POL_DS_RIGA;
        MarshalByte.writeChar(buffer, position, polDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, polDsVer, Len.Int.POL_DS_VER, 0);
        position += Len.POL_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, polDsTsIniCptz, Len.Int.POL_DS_TS_INI_CPTZ, 0);
        position += Len.POL_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, polDsTsEndCptz, Len.Int.POL_DS_TS_END_CPTZ, 0);
        position += Len.POL_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, polDsUtente, Len.POL_DS_UTENTE);
        position += Len.POL_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, polDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlScudoFisc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlTrasfe);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlTfrStrc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, polDtPresc, Len.Int.POL_DT_PRESC, 0);
        position += Len.POL_DT_PRESC;
        MarshalByte.writeString(buffer, position, polCodConvAgg, Len.POL_COD_CONV_AGG);
        position += Len.POL_COD_CONV_AGG;
        MarshalByte.writeString(buffer, position, polSubcatProd, Len.POL_SUBCAT_PROD);
        position += Len.POL_SUBCAT_PROD;
        MarshalByte.writeChar(buffer, position, polFlQuestAdegzAss);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polCodTpa, Len.POL_COD_TPA);
        position += Len.POL_COD_TPA;
        MarshalByte.writeIntAsPacked(buffer, position, polIdAccComm, Len.Int.POL_ID_ACC_COMM, 0);
        position += Len.POL_ID_ACC_COMM;
        MarshalByte.writeChar(buffer, position, polFlPoliCpiPr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlPoliBundling);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polIndPoliPrinColl);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, polFlVndBundle);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, polIbBs, Len.POL_IB_BS);
        position += Len.POL_IB_BS;
        MarshalByte.writeChar(buffer, position, flPoliIfp);
        return buffer;
    }

    public void setPolIdPoli(int polIdPoli) {
        this.polIdPoli = polIdPoli;
    }

    public int getPolIdPoli() {
        return this.polIdPoli;
    }

    public void setPolIdMoviCrz(int polIdMoviCrz) {
        this.polIdMoviCrz = polIdMoviCrz;
    }

    public int getPolIdMoviCrz() {
        return this.polIdMoviCrz;
    }

    public void setPolIdMoviChiu(int polIdMoviChiu) {
        this.polIdMoviChiu = polIdMoviChiu;
    }

    public int getPolIdMoviChiu() {
        return this.polIdMoviChiu;
    }

    public void setPolIbOgg(String polIbOgg) {
        this.polIbOgg = Functions.subString(polIbOgg, Len.POL_IB_OGG);
    }

    public String getPolIbOgg() {
        return this.polIbOgg;
    }

    public void setPolIbProp(String polIbProp) {
        this.polIbProp = Functions.subString(polIbProp, Len.POL_IB_PROP);
    }

    public String getPolIbProp() {
        return this.polIbProp;
    }

    public void setPolDtProp(int polDtProp) {
        this.polDtProp = polDtProp;
    }

    public int getPolDtProp() {
        return this.polDtProp;
    }

    public void setPolDtIniEff(int polDtIniEff) {
        this.polDtIniEff = polDtIniEff;
    }

    public int getPolDtIniEff() {
        return this.polDtIniEff;
    }

    public void setPolDtEndEff(int polDtEndEff) {
        this.polDtEndEff = polDtEndEff;
    }

    public int getPolDtEndEff() {
        return this.polDtEndEff;
    }

    public void setPolCodCompAnia(int polCodCompAnia) {
        this.polCodCompAnia = polCodCompAnia;
    }

    public int getPolCodCompAnia() {
        return this.polCodCompAnia;
    }

    public void setPolDtDecor(int polDtDecor) {
        this.polDtDecor = polDtDecor;
    }

    public int getPolDtDecor() {
        return this.polDtDecor;
    }

    public void setPolDtEmis(int polDtEmis) {
        this.polDtEmis = polDtEmis;
    }

    public int getPolDtEmis() {
        return this.polDtEmis;
    }

    public void setPolTpPoli(String polTpPoli) {
        this.polTpPoli = Functions.subString(polTpPoli, Len.POL_TP_POLI);
    }

    public String getPolTpPoli() {
        return this.polTpPoli;
    }

    public void setPolDurAa(int polDurAa) {
        this.polDurAa = polDurAa;
    }

    public int getPolDurAa() {
        return this.polDurAa;
    }

    public void setPolDurMm(int polDurMm) {
        this.polDurMm = polDurMm;
    }

    public int getPolDurMm() {
        return this.polDurMm;
    }

    public void setPolDtScad(int polDtScad) {
        this.polDtScad = polDtScad;
    }

    public int getPolDtScad() {
        return this.polDtScad;
    }

    public void setPolCodProd(String polCodProd) {
        this.polCodProd = Functions.subString(polCodProd, Len.POL_COD_PROD);
    }

    public String getPolCodProd() {
        return this.polCodProd;
    }

    public void setPolDtIniVldtProd(int polDtIniVldtProd) {
        this.polDtIniVldtProd = polDtIniVldtProd;
    }

    public int getPolDtIniVldtProd() {
        return this.polDtIniVldtProd;
    }

    public void setPolCodConv(String polCodConv) {
        this.polCodConv = Functions.subString(polCodConv, Len.POL_COD_CONV);
    }

    public String getPolCodConv() {
        return this.polCodConv;
    }

    public void setPolCodRamo(String polCodRamo) {
        this.polCodRamo = Functions.subString(polCodRamo, Len.POL_COD_RAMO);
    }

    public String getPolCodRamo() {
        return this.polCodRamo;
    }

    public void setPolDtIniVldtConv(int polDtIniVldtConv) {
        this.polDtIniVldtConv = polDtIniVldtConv;
    }

    public int getPolDtIniVldtConv() {
        return this.polDtIniVldtConv;
    }

    public void setPolDtApplzConv(int polDtApplzConv) {
        this.polDtApplzConv = polDtApplzConv;
    }

    public int getPolDtApplzConv() {
        return this.polDtApplzConv;
    }

    public void setPolTpFrmAssva(String polTpFrmAssva) {
        this.polTpFrmAssva = Functions.subString(polTpFrmAssva, Len.POL_TP_FRM_ASSVA);
    }

    public String getPolTpFrmAssva() {
        return this.polTpFrmAssva;
    }

    public void setPolTpRgmFisc(String polTpRgmFisc) {
        this.polTpRgmFisc = Functions.subString(polTpRgmFisc, Len.POL_TP_RGM_FISC);
    }

    public String getPolTpRgmFisc() {
        return this.polTpRgmFisc;
    }

    public void setPolFlEstas(char polFlEstas) {
        this.polFlEstas = polFlEstas;
    }

    public char getPolFlEstas() {
        return this.polFlEstas;
    }

    public void setPolFlRshComun(char polFlRshComun) {
        this.polFlRshComun = polFlRshComun;
    }

    public char getPolFlRshComun() {
        return this.polFlRshComun;
    }

    public void setPolFlRshComunCond(char polFlRshComunCond) {
        this.polFlRshComunCond = polFlRshComunCond;
    }

    public char getPolFlRshComunCond() {
        return this.polFlRshComunCond;
    }

    public void setPolTpLivGenzTit(String polTpLivGenzTit) {
        this.polTpLivGenzTit = Functions.subString(polTpLivGenzTit, Len.POL_TP_LIV_GENZ_TIT);
    }

    public String getPolTpLivGenzTit() {
        return this.polTpLivGenzTit;
    }

    public void setPolFlCopFinanz(char polFlCopFinanz) {
        this.polFlCopFinanz = polFlCopFinanz;
    }

    public char getPolFlCopFinanz() {
        return this.polFlCopFinanz;
    }

    public void setPolTpApplzDir(String polTpApplzDir) {
        this.polTpApplzDir = Functions.subString(polTpApplzDir, Len.POL_TP_APPLZ_DIR);
    }

    public String getPolTpApplzDir() {
        return this.polTpApplzDir;
    }

    public void setPolSpeMed(AfDecimal polSpeMed) {
        this.polSpeMed.assign(polSpeMed);
    }

    public AfDecimal getPolSpeMed() {
        return this.polSpeMed.copy();
    }

    public void setPolDirEmis(AfDecimal polDirEmis) {
        this.polDirEmis.assign(polDirEmis);
    }

    public AfDecimal getPolDirEmis() {
        return this.polDirEmis.copy();
    }

    public void setPolDir1oVers(AfDecimal polDir1oVers) {
        this.polDir1oVers.assign(polDir1oVers);
    }

    public AfDecimal getPolDir1oVers() {
        return this.polDir1oVers.copy();
    }

    public void setPolDirVersAgg(AfDecimal polDirVersAgg) {
        this.polDirVersAgg.assign(polDirVersAgg);
    }

    public AfDecimal getPolDirVersAgg() {
        return this.polDirVersAgg.copy();
    }

    public void setPolCodDvs(String polCodDvs) {
        this.polCodDvs = Functions.subString(polCodDvs, Len.POL_COD_DVS);
    }

    public String getPolCodDvs() {
        return this.polCodDvs;
    }

    public void setPolFlFntAz(char polFlFntAz) {
        this.polFlFntAz = polFlFntAz;
    }

    public char getPolFlFntAz() {
        return this.polFlFntAz;
    }

    public void setPolFlFntAder(char polFlFntAder) {
        this.polFlFntAder = polFlFntAder;
    }

    public char getPolFlFntAder() {
        return this.polFlFntAder;
    }

    public void setPolFlFntTfr(char polFlFntTfr) {
        this.polFlFntTfr = polFlFntTfr;
    }

    public char getPolFlFntTfr() {
        return this.polFlFntTfr;
    }

    public void setPolFlFntVolo(char polFlFntVolo) {
        this.polFlFntVolo = polFlFntVolo;
    }

    public char getPolFlFntVolo() {
        return this.polFlFntVolo;
    }

    public void setPolTpOpzAScad(String polTpOpzAScad) {
        this.polTpOpzAScad = Functions.subString(polTpOpzAScad, Len.POL_TP_OPZ_A_SCAD);
    }

    public String getPolTpOpzAScad() {
        return this.polTpOpzAScad;
    }

    public void setPolAaDiffProrDflt(int polAaDiffProrDflt) {
        this.polAaDiffProrDflt = polAaDiffProrDflt;
    }

    public int getPolAaDiffProrDflt() {
        return this.polAaDiffProrDflt;
    }

    public void setPolFlVerProd(String polFlVerProd) {
        this.polFlVerProd = Functions.subString(polFlVerProd, Len.POL_FL_VER_PROD);
    }

    public String getPolFlVerProd() {
        return this.polFlVerProd;
    }

    public void setPolDurGg(int polDurGg) {
        this.polDurGg = polDurGg;
    }

    public int getPolDurGg() {
        return this.polDurGg;
    }

    public void setPolDirQuiet(AfDecimal polDirQuiet) {
        this.polDirQuiet.assign(polDirQuiet);
    }

    public AfDecimal getPolDirQuiet() {
        return this.polDirQuiet.copy();
    }

    public void setPolTpPtfEstno(String polTpPtfEstno) {
        this.polTpPtfEstno = Functions.subString(polTpPtfEstno, Len.POL_TP_PTF_ESTNO);
    }

    public String getPolTpPtfEstno() {
        return this.polTpPtfEstno;
    }

    public void setPolFlCumPreCntr(char polFlCumPreCntr) {
        this.polFlCumPreCntr = polFlCumPreCntr;
    }

    public char getPolFlCumPreCntr() {
        return this.polFlCumPreCntr;
    }

    public void setPolFlAmmbMovi(char polFlAmmbMovi) {
        this.polFlAmmbMovi = polFlAmmbMovi;
    }

    public char getPolFlAmmbMovi() {
        return this.polFlAmmbMovi;
    }

    public void setPolConvGeco(String polConvGeco) {
        this.polConvGeco = Functions.subString(polConvGeco, Len.POL_CONV_GECO);
    }

    public String getPolConvGeco() {
        return this.polConvGeco;
    }

    public void setPolDsRiga(long polDsRiga) {
        this.polDsRiga = polDsRiga;
    }

    public long getPolDsRiga() {
        return this.polDsRiga;
    }

    public void setPolDsOperSql(char polDsOperSql) {
        this.polDsOperSql = polDsOperSql;
    }

    public char getPolDsOperSql() {
        return this.polDsOperSql;
    }

    public void setPolDsVer(int polDsVer) {
        this.polDsVer = polDsVer;
    }

    public int getPolDsVer() {
        return this.polDsVer;
    }

    public void setPolDsTsIniCptz(long polDsTsIniCptz) {
        this.polDsTsIniCptz = polDsTsIniCptz;
    }

    public long getPolDsTsIniCptz() {
        return this.polDsTsIniCptz;
    }

    public void setPolDsTsEndCptz(long polDsTsEndCptz) {
        this.polDsTsEndCptz = polDsTsEndCptz;
    }

    public long getPolDsTsEndCptz() {
        return this.polDsTsEndCptz;
    }

    public void setPolDsUtente(String polDsUtente) {
        this.polDsUtente = Functions.subString(polDsUtente, Len.POL_DS_UTENTE);
    }

    public String getPolDsUtente() {
        return this.polDsUtente;
    }

    public void setPolDsStatoElab(char polDsStatoElab) {
        this.polDsStatoElab = polDsStatoElab;
    }

    public char getPolDsStatoElab() {
        return this.polDsStatoElab;
    }

    public void setPolFlScudoFisc(char polFlScudoFisc) {
        this.polFlScudoFisc = polFlScudoFisc;
    }

    public char getPolFlScudoFisc() {
        return this.polFlScudoFisc;
    }

    public void setPolFlTrasfe(char polFlTrasfe) {
        this.polFlTrasfe = polFlTrasfe;
    }

    public char getPolFlTrasfe() {
        return this.polFlTrasfe;
    }

    public void setPolFlTfrStrc(char polFlTfrStrc) {
        this.polFlTfrStrc = polFlTfrStrc;
    }

    public char getPolFlTfrStrc() {
        return this.polFlTfrStrc;
    }

    public void setPolDtPresc(int polDtPresc) {
        this.polDtPresc = polDtPresc;
    }

    public int getPolDtPresc() {
        return this.polDtPresc;
    }

    public void setPolCodConvAgg(String polCodConvAgg) {
        this.polCodConvAgg = Functions.subString(polCodConvAgg, Len.POL_COD_CONV_AGG);
    }

    public String getPolCodConvAgg() {
        return this.polCodConvAgg;
    }

    public void setPolSubcatProd(String polSubcatProd) {
        this.polSubcatProd = Functions.subString(polSubcatProd, Len.POL_SUBCAT_PROD);
    }

    public String getPolSubcatProd() {
        return this.polSubcatProd;
    }

    public void setPolFlQuestAdegzAss(char polFlQuestAdegzAss) {
        this.polFlQuestAdegzAss = polFlQuestAdegzAss;
    }

    public char getPolFlQuestAdegzAss() {
        return this.polFlQuestAdegzAss;
    }

    public void setPolCodTpa(String polCodTpa) {
        this.polCodTpa = Functions.subString(polCodTpa, Len.POL_COD_TPA);
    }

    public String getPolCodTpa() {
        return this.polCodTpa;
    }

    public void setPolIdAccComm(int polIdAccComm) {
        this.polIdAccComm = polIdAccComm;
    }

    public int getPolIdAccComm() {
        return this.polIdAccComm;
    }

    public void setPolFlPoliCpiPr(char polFlPoliCpiPr) {
        this.polFlPoliCpiPr = polFlPoliCpiPr;
    }

    public char getPolFlPoliCpiPr() {
        return this.polFlPoliCpiPr;
    }

    public void setPolFlPoliBundling(char polFlPoliBundling) {
        this.polFlPoliBundling = polFlPoliBundling;
    }

    public char getPolFlPoliBundling() {
        return this.polFlPoliBundling;
    }

    public void setPolIndPoliPrinColl(char polIndPoliPrinColl) {
        this.polIndPoliPrinColl = polIndPoliPrinColl;
    }

    public char getPolIndPoliPrinColl() {
        return this.polIndPoliPrinColl;
    }

    public void setPolFlVndBundle(char polFlVndBundle) {
        this.polFlVndBundle = polFlVndBundle;
    }

    public char getPolFlVndBundle() {
        return this.polFlVndBundle;
    }

    public void setPolIbBs(String polIbBs) {
        this.polIbBs = Functions.subString(polIbBs, Len.POL_IB_BS);
    }

    public String getPolIbBs() {
        return this.polIbBs;
    }

    public void setFlPoliIfp(char flPoliIfp) {
        this.flPoliIfp = flPoliIfp;
    }

    public char getFlPoliIfp() {
        return this.flPoliIfp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_ID_POLI = 5;
        public static final int POL_ID_MOVI_CRZ = 5;
        public static final int POL_ID_MOVI_CHIU = 5;
        public static final int POL_IB_OGG = 40;
        public static final int POL_IB_PROP = 40;
        public static final int POL_DT_PROP = 5;
        public static final int POL_DT_INI_EFF = 5;
        public static final int POL_DT_END_EFF = 5;
        public static final int POL_COD_COMP_ANIA = 3;
        public static final int POL_DT_DECOR = 5;
        public static final int POL_DT_EMIS = 5;
        public static final int POL_TP_POLI = 2;
        public static final int POL_DUR_AA = 3;
        public static final int POL_DUR_MM = 3;
        public static final int POL_DT_SCAD = 5;
        public static final int POL_COD_PROD = 12;
        public static final int POL_DT_INI_VLDT_PROD = 5;
        public static final int POL_COD_CONV = 12;
        public static final int POL_COD_RAMO = 12;
        public static final int POL_DT_INI_VLDT_CONV = 5;
        public static final int POL_DT_APPLZ_CONV = 5;
        public static final int POL_TP_FRM_ASSVA = 2;
        public static final int POL_TP_RGM_FISC = 2;
        public static final int POL_FL_ESTAS = 1;
        public static final int POL_FL_RSH_COMUN = 1;
        public static final int POL_FL_RSH_COMUN_COND = 1;
        public static final int POL_TP_LIV_GENZ_TIT = 2;
        public static final int POL_FL_COP_FINANZ = 1;
        public static final int POL_TP_APPLZ_DIR = 2;
        public static final int POL_SPE_MED = 8;
        public static final int POL_DIR_EMIS = 8;
        public static final int POL_DIR1O_VERS = 8;
        public static final int POL_DIR_VERS_AGG = 8;
        public static final int POL_COD_DVS = 20;
        public static final int POL_FL_FNT_AZ = 1;
        public static final int POL_FL_FNT_ADER = 1;
        public static final int POL_FL_FNT_TFR = 1;
        public static final int POL_FL_FNT_VOLO = 1;
        public static final int POL_TP_OPZ_A_SCAD = 2;
        public static final int POL_AA_DIFF_PROR_DFLT = 3;
        public static final int POL_FL_VER_PROD = 2;
        public static final int POL_DUR_GG = 3;
        public static final int POL_DIR_QUIET = 8;
        public static final int POL_TP_PTF_ESTNO = 2;
        public static final int POL_FL_CUM_PRE_CNTR = 1;
        public static final int POL_FL_AMMB_MOVI = 1;
        public static final int POL_CONV_GECO = 5;
        public static final int POL_DS_RIGA = 6;
        public static final int POL_DS_OPER_SQL = 1;
        public static final int POL_DS_VER = 5;
        public static final int POL_DS_TS_INI_CPTZ = 10;
        public static final int POL_DS_TS_END_CPTZ = 10;
        public static final int POL_DS_UTENTE = 20;
        public static final int POL_DS_STATO_ELAB = 1;
        public static final int POL_FL_SCUDO_FISC = 1;
        public static final int POL_FL_TRASFE = 1;
        public static final int POL_FL_TFR_STRC = 1;
        public static final int POL_DT_PRESC = 5;
        public static final int POL_COD_CONV_AGG = 12;
        public static final int POL_SUBCAT_PROD = 12;
        public static final int POL_FL_QUEST_ADEGZ_ASS = 1;
        public static final int POL_COD_TPA = 4;
        public static final int POL_ID_ACC_COMM = 5;
        public static final int POL_FL_POLI_CPI_PR = 1;
        public static final int POL_FL_POLI_BUNDLING = 1;
        public static final int POL_IND_POLI_PRIN_COLL = 1;
        public static final int POL_FL_VND_BUNDLE = 1;
        public static final int POL_IB_BS = 40;
        public static final int FL_POLI_IFP = 1;
        public static final int LDBVC821_POLI = POL_ID_POLI + POL_ID_MOVI_CRZ + POL_ID_MOVI_CHIU + POL_IB_OGG + POL_IB_PROP + POL_DT_PROP + POL_DT_INI_EFF + POL_DT_END_EFF + POL_COD_COMP_ANIA + POL_DT_DECOR + POL_DT_EMIS + POL_TP_POLI + POL_DUR_AA + POL_DUR_MM + POL_DT_SCAD + POL_COD_PROD + POL_DT_INI_VLDT_PROD + POL_COD_CONV + POL_COD_RAMO + POL_DT_INI_VLDT_CONV + POL_DT_APPLZ_CONV + POL_TP_FRM_ASSVA + POL_TP_RGM_FISC + POL_FL_ESTAS + POL_FL_RSH_COMUN + POL_FL_RSH_COMUN_COND + POL_TP_LIV_GENZ_TIT + POL_FL_COP_FINANZ + POL_TP_APPLZ_DIR + POL_SPE_MED + POL_DIR_EMIS + POL_DIR1O_VERS + POL_DIR_VERS_AGG + POL_COD_DVS + POL_FL_FNT_AZ + POL_FL_FNT_ADER + POL_FL_FNT_TFR + POL_FL_FNT_VOLO + POL_TP_OPZ_A_SCAD + POL_AA_DIFF_PROR_DFLT + POL_FL_VER_PROD + POL_DUR_GG + POL_DIR_QUIET + POL_TP_PTF_ESTNO + POL_FL_CUM_PRE_CNTR + POL_FL_AMMB_MOVI + POL_CONV_GECO + POL_DS_RIGA + POL_DS_OPER_SQL + POL_DS_VER + POL_DS_TS_INI_CPTZ + POL_DS_TS_END_CPTZ + POL_DS_UTENTE + POL_DS_STATO_ELAB + POL_FL_SCUDO_FISC + POL_FL_TRASFE + POL_FL_TFR_STRC + POL_DT_PRESC + POL_COD_CONV_AGG + POL_SUBCAT_PROD + POL_FL_QUEST_ADEGZ_ASS + POL_COD_TPA + POL_ID_ACC_COMM + POL_FL_POLI_CPI_PR + POL_FL_POLI_BUNDLING + POL_IND_POLI_PRIN_COLL + POL_FL_VND_BUNDLE + POL_IB_BS + FL_POLI_IFP;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_ID_POLI = 9;
            public static final int POL_ID_MOVI_CRZ = 9;
            public static final int POL_ID_MOVI_CHIU = 9;
            public static final int POL_DT_PROP = 8;
            public static final int POL_DT_INI_EFF = 8;
            public static final int POL_DT_END_EFF = 8;
            public static final int POL_COD_COMP_ANIA = 5;
            public static final int POL_DT_DECOR = 8;
            public static final int POL_DT_EMIS = 8;
            public static final int POL_DUR_AA = 5;
            public static final int POL_DUR_MM = 5;
            public static final int POL_DT_SCAD = 8;
            public static final int POL_DT_INI_VLDT_PROD = 8;
            public static final int POL_DT_INI_VLDT_CONV = 8;
            public static final int POL_DT_APPLZ_CONV = 8;
            public static final int POL_SPE_MED = 12;
            public static final int POL_DIR_EMIS = 12;
            public static final int POL_DIR1O_VERS = 12;
            public static final int POL_DIR_VERS_AGG = 12;
            public static final int POL_AA_DIFF_PROR_DFLT = 5;
            public static final int POL_DUR_GG = 5;
            public static final int POL_DIR_QUIET = 12;
            public static final int POL_DS_RIGA = 10;
            public static final int POL_DS_VER = 9;
            public static final int POL_DS_TS_INI_CPTZ = 18;
            public static final int POL_DS_TS_END_CPTZ = 18;
            public static final int POL_DT_PRESC = 8;
            public static final int POL_ID_ACC_COMM = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int POL_SPE_MED = 3;
            public static final int POL_DIR_EMIS = 3;
            public static final int POL_DIR1O_VERS = 3;
            public static final int POL_DIR_VERS_AGG = 3;
            public static final int POL_DIR_QUIET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
