package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.WkApFndTab;

/**Original name: LRGC0041<br>
 * Variable: LRGC0041 from copybook LRGC0041<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lrgc0041 {

    //==== PROPERTIES ====
    public static final int WK_AP_FND_TAB_MAXOCCURS = 100;
    //Original name: WK-AP-ID-GAR
    private String wkApIdGar = DefaultValues.stringVal(Len.WK_AP_ID_GAR);
    //Original name: WK-AP-PREST-MATUR
    private AfDecimal wkApPrestMatur = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-AP-CNTRVAL-TOT
    private AfDecimal wkApCntrvalTot = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-AP-FND-ELE-MAX
    private String wkApFndEleMax = DefaultValues.stringVal(Len.WK_AP_FND_ELE_MAX);
    //Original name: WK-AP-FND-TAB
    private WkApFndTab[] wkApFndTab = new WkApFndTab[WK_AP_FND_TAB_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Lrgc0041() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wkApFndTabIdx = 1; wkApFndTabIdx <= WK_AP_FND_TAB_MAXOCCURS; wkApFndTabIdx++) {
            wkApFndTab[wkApFndTabIdx - 1] = new WkApFndTab();
        }
    }

    public void setWkApIdGarFormatted(String wkApIdGar) {
        this.wkApIdGar = Trunc.toUnsignedNumeric(wkApIdGar, Len.WK_AP_ID_GAR);
    }

    public int getWkApIdGar() {
        return NumericDisplay.asInt(this.wkApIdGar);
    }

    public void setWkApPrestMatur(AfDecimal wkApPrestMatur) {
        this.wkApPrestMatur.assign(wkApPrestMatur);
    }

    public AfDecimal getWkApPrestMatur() {
        return this.wkApPrestMatur.copy();
    }

    public void setWkApCntrvalTot(AfDecimal wkApCntrvalTot) {
        this.wkApCntrvalTot.assign(wkApCntrvalTot);
    }

    public AfDecimal getWkApCntrvalTot() {
        return this.wkApCntrvalTot.copy();
    }

    public void setWkApFndEleMaxFormatted(String wkApFndEleMax) {
        this.wkApFndEleMax = Trunc.toUnsignedNumeric(wkApFndEleMax, Len.WK_AP_FND_ELE_MAX);
    }

    public short getWkApFndEleMax() {
        return NumericDisplay.asShort(this.wkApFndEleMax);
    }

    public WkApFndTab getWkApFndTab(int idx) {
        return wkApFndTab[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LRGC0041_DT_ESTR_CNT_X = 8;
        public static final int LRGC0041_DT_ESTR_CNT_ATT = 8;
        public static final int LRGC0041_DT_DECOR_POLI = 8;
        public static final int WK_AP_ID_GAR = 9;
        public static final int WK_AP_FND_ELE_MAX = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
