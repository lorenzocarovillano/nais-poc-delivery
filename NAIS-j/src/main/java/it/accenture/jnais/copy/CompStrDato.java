package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.CsdLunghezzaDato;
import it.accenture.jnais.ws.redefines.CsdPrecisioneDato;
import it.accenture.jnais.ws.redefines.CsdRicorrenza;

/**Original name: COMP-STR-DATO<br>
 * Variable: COMP-STR-DATO from copybook IDBVCSD1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CompStrDato {

    //==== PROPERTIES ====
    //Original name: CSD-COD-COMPAGNIA-ANIA
    private int csdCodCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: CSD-COD-STR-DATO
    private String csdCodStrDato = DefaultValues.stringVal(Len.CSD_COD_STR_DATO);
    //Original name: CSD-POSIZIONE
    private int csdPosizione = DefaultValues.INT_VAL;
    //Original name: CSD-COD-STR-DATO2
    private String csdCodStrDato2 = DefaultValues.stringVal(Len.CSD_COD_STR_DATO2);
    //Original name: CSD-COD-DATO
    private String csdCodDato = DefaultValues.stringVal(Len.CSD_COD_DATO);
    //Original name: CSD-FLAG-KEY
    private char csdFlagKey = DefaultValues.CHAR_VAL;
    //Original name: CSD-FLAG-RETURN-CODE
    private char csdFlagReturnCode = DefaultValues.CHAR_VAL;
    //Original name: CSD-FLAG-CALL-USING
    private char csdFlagCallUsing = DefaultValues.CHAR_VAL;
    //Original name: CSD-FLAG-WHERE-COND
    private char csdFlagWhereCond = DefaultValues.CHAR_VAL;
    //Original name: CSD-FLAG-REDEFINES
    private char csdFlagRedefines = DefaultValues.CHAR_VAL;
    //Original name: CSD-TIPO-DATO
    private String csdTipoDato = DefaultValues.stringVal(Len.CSD_TIPO_DATO);
    //Original name: CSD-LUNGHEZZA-DATO
    private CsdLunghezzaDato csdLunghezzaDato = new CsdLunghezzaDato();
    //Original name: CSD-PRECISIONE-DATO
    private CsdPrecisioneDato csdPrecisioneDato = new CsdPrecisioneDato();
    //Original name: CSD-COD-DOMINIO
    private String csdCodDominio = DefaultValues.stringVal(Len.CSD_COD_DOMINIO);
    //Original name: CSD-FORMATTAZIONE-DATO
    private String csdFormattazioneDato = DefaultValues.stringVal(Len.CSD_FORMATTAZIONE_DATO);
    //Original name: CSD-SERVIZIO-CONVERS
    private String csdServizioConvers = DefaultValues.stringVal(Len.CSD_SERVIZIO_CONVERS);
    //Original name: CSD-AREA-CONV-STANDARD
    private char csdAreaConvStandard = DefaultValues.CHAR_VAL;
    //Original name: CSD-RICORRENZA
    private CsdRicorrenza csdRicorrenza = new CsdRicorrenza();
    //Original name: CSD-OBBLIGATORIETA
    private char csdObbligatorieta = DefaultValues.CHAR_VAL;
    //Original name: CSD-VALORE-DEFAULT
    private String csdValoreDefault = DefaultValues.stringVal(Len.CSD_VALORE_DEFAULT);
    //Original name: CSD-DS-UTENTE
    private String csdDsUtente = DefaultValues.stringVal(Len.CSD_DS_UTENTE);

    //==== METHODS ====
    public void initCompStrDatoSpaces() {
        csdCodCompagniaAnia = Types.INVALID_INT_VAL;
        csdCodStrDato = "";
        csdPosizione = Types.INVALID_INT_VAL;
        csdCodStrDato2 = "";
        csdCodDato = "";
        csdFlagKey = Types.SPACE_CHAR;
        csdFlagReturnCode = Types.SPACE_CHAR;
        csdFlagCallUsing = Types.SPACE_CHAR;
        csdFlagWhereCond = Types.SPACE_CHAR;
        csdFlagRedefines = Types.SPACE_CHAR;
        csdTipoDato = "";
        csdLunghezzaDato.initCsdLunghezzaDatoSpaces();
        csdPrecisioneDato.initCsdPrecisioneDatoSpaces();
        csdCodDominio = "";
        csdFormattazioneDato = "";
        csdServizioConvers = "";
        csdAreaConvStandard = Types.SPACE_CHAR;
        csdRicorrenza.initCsdRicorrenzaSpaces();
        csdObbligatorieta = Types.SPACE_CHAR;
        csdValoreDefault = "";
        csdDsUtente = "";
    }

    public void setCsdCodCompagniaAnia(int csdCodCompagniaAnia) {
        this.csdCodCompagniaAnia = csdCodCompagniaAnia;
    }

    public int getCsdCodCompagniaAnia() {
        return this.csdCodCompagniaAnia;
    }

    public void setCsdCodStrDato(String csdCodStrDato) {
        this.csdCodStrDato = Functions.subString(csdCodStrDato, Len.CSD_COD_STR_DATO);
    }

    public String getCsdCodStrDato() {
        return this.csdCodStrDato;
    }

    public void setCsdPosizione(int csdPosizione) {
        this.csdPosizione = csdPosizione;
    }

    public int getCsdPosizione() {
        return this.csdPosizione;
    }

    public void setCsdCodStrDato2(String csdCodStrDato2) {
        this.csdCodStrDato2 = Functions.subString(csdCodStrDato2, Len.CSD_COD_STR_DATO2);
    }

    public String getCsdCodStrDato2() {
        return this.csdCodStrDato2;
    }

    public void setCsdCodDato(String csdCodDato) {
        this.csdCodDato = Functions.subString(csdCodDato, Len.CSD_COD_DATO);
    }

    public String getCsdCodDato() {
        return this.csdCodDato;
    }

    public void setCsdFlagKey(char csdFlagKey) {
        this.csdFlagKey = csdFlagKey;
    }

    public char getCsdFlagKey() {
        return this.csdFlagKey;
    }

    public void setCsdFlagReturnCode(char csdFlagReturnCode) {
        this.csdFlagReturnCode = csdFlagReturnCode;
    }

    public char getCsdFlagReturnCode() {
        return this.csdFlagReturnCode;
    }

    public void setCsdFlagCallUsing(char csdFlagCallUsing) {
        this.csdFlagCallUsing = csdFlagCallUsing;
    }

    public char getCsdFlagCallUsing() {
        return this.csdFlagCallUsing;
    }

    public void setCsdFlagWhereCond(char csdFlagWhereCond) {
        this.csdFlagWhereCond = csdFlagWhereCond;
    }

    public char getCsdFlagWhereCond() {
        return this.csdFlagWhereCond;
    }

    public void setCsdFlagRedefines(char csdFlagRedefines) {
        this.csdFlagRedefines = csdFlagRedefines;
    }

    public char getCsdFlagRedefines() {
        return this.csdFlagRedefines;
    }

    public void setCsdTipoDato(String csdTipoDato) {
        this.csdTipoDato = Functions.subString(csdTipoDato, Len.CSD_TIPO_DATO);
    }

    public String getCsdTipoDato() {
        return this.csdTipoDato;
    }

    public String getCsdTipoDatoFormatted() {
        return Functions.padBlanks(getCsdTipoDato(), Len.CSD_TIPO_DATO);
    }

    public void setCsdCodDominio(String csdCodDominio) {
        this.csdCodDominio = Functions.subString(csdCodDominio, Len.CSD_COD_DOMINIO);
    }

    public String getCsdCodDominio() {
        return this.csdCodDominio;
    }

    public void setCsdFormattazioneDato(String csdFormattazioneDato) {
        this.csdFormattazioneDato = Functions.subString(csdFormattazioneDato, Len.CSD_FORMATTAZIONE_DATO);
    }

    public String getCsdFormattazioneDato() {
        return this.csdFormattazioneDato;
    }

    public void setCsdServizioConvers(String csdServizioConvers) {
        this.csdServizioConvers = Functions.subString(csdServizioConvers, Len.CSD_SERVIZIO_CONVERS);
    }

    public String getCsdServizioConvers() {
        return this.csdServizioConvers;
    }

    public void setCsdAreaConvStandard(char csdAreaConvStandard) {
        this.csdAreaConvStandard = csdAreaConvStandard;
    }

    public char getCsdAreaConvStandard() {
        return this.csdAreaConvStandard;
    }

    public void setCsdObbligatorieta(char csdObbligatorieta) {
        this.csdObbligatorieta = csdObbligatorieta;
    }

    public char getCsdObbligatorieta() {
        return this.csdObbligatorieta;
    }

    public void setCsdValoreDefault(String csdValoreDefault) {
        this.csdValoreDefault = Functions.subString(csdValoreDefault, Len.CSD_VALORE_DEFAULT);
    }

    public String getCsdValoreDefault() {
        return this.csdValoreDefault;
    }

    public void setCsdDsUtente(String csdDsUtente) {
        this.csdDsUtente = Functions.subString(csdDsUtente, Len.CSD_DS_UTENTE);
    }

    public String getCsdDsUtente() {
        return this.csdDsUtente;
    }

    public CsdLunghezzaDato getCsdLunghezzaDato() {
        return csdLunghezzaDato;
    }

    public CsdPrecisioneDato getCsdPrecisioneDato() {
        return csdPrecisioneDato;
    }

    public CsdRicorrenza getCsdRicorrenza() {
        return csdRicorrenza;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CSD_COD_STR_DATO = 30;
        public static final int CSD_COD_STR_DATO2 = 30;
        public static final int CSD_COD_DATO = 30;
        public static final int CSD_TIPO_DATO = 2;
        public static final int CSD_COD_DOMINIO = 30;
        public static final int CSD_FORMATTAZIONE_DATO = 20;
        public static final int CSD_SERVIZIO_CONVERS = 8;
        public static final int CSD_VALORE_DEFAULT = 10;
        public static final int CSD_DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
