package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVRRE3<br>
 * Copybook: IDBVRRE3 from copybook IDBVRRE3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvrre3 {

    //==== PROPERTIES ====
    //Original name: RRE-DT-INI-EFF-DB
    private String rreDtIniEffDb = DefaultValues.stringVal(Len.RRE_DT_INI_EFF_DB);
    //Original name: RRE-DT-END-EFF-DB
    private String rreDtEndEffDb = DefaultValues.stringVal(Len.RRE_DT_END_EFF_DB);

    //==== METHODS ====
    public void setRreDtIniEffDb(String rreDtIniEffDb) {
        this.rreDtIniEffDb = Functions.subString(rreDtIniEffDb, Len.RRE_DT_INI_EFF_DB);
    }

    public String getRreDtIniEffDb() {
        return this.rreDtIniEffDb;
    }

    public void setRreDtEndEffDb(String rreDtEndEffDb) {
        this.rreDtEndEffDb = Functions.subString(rreDtEndEffDb, Len.RRE_DT_END_EFF_DB);
    }

    public String getRreDtEndEffDb() {
        return this.rreDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RRE_DT_INI_EFF_DB = 10;
        public static final int RRE_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
