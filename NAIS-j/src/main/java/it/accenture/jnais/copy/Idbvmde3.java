package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVMDE3<br>
 * Copybook: IDBVMDE3 from copybook IDBVMDE3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvmde3 {

    //==== PROPERTIES ====
    //Original name: MDE-DT-INI-EFF-DB
    private String mdeDtIniEffDb = DefaultValues.stringVal(Len.MDE_DT_INI_EFF_DB);
    //Original name: MDE-DT-END-EFF-DB
    private String mdeDtEndEffDb = DefaultValues.stringVal(Len.MDE_DT_END_EFF_DB);

    //==== METHODS ====
    public void setMdeDtIniEffDb(String mdeDtIniEffDb) {
        this.mdeDtIniEffDb = Functions.subString(mdeDtIniEffDb, Len.MDE_DT_INI_EFF_DB);
    }

    public String getMdeDtIniEffDb() {
        return this.mdeDtIniEffDb;
    }

    public void setMdeDtEndEffDb(String mdeDtEndEffDb) {
        this.mdeDtEndEffDb = Functions.subString(mdeDtEndEffDb, Len.MDE_DT_END_EFF_DB);
    }

    public String getMdeDtEndEffDb() {
        return this.mdeDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MDE_DT_INI_EFF_DB = 10;
        public static final int MDE_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
