package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.AlfanumericoTrovato;
import it.accenture.jnais.ws.enums.DatoInputTrovato;
import it.accenture.jnais.ws.enums.DecTrovato;
import it.accenture.jnais.ws.enums.FineStringa;
import it.accenture.jnais.ws.enums.Idsv0501PosSegno;
import it.accenture.jnais.ws.enums.Idsv0501ReturnCode;
import it.accenture.jnais.ws.enums.Idsv0501SegnoPostiv;
import it.accenture.jnais.ws.enums.IntTrovato;
import it.accenture.jnais.ws.enums.PrimaVolta;
import it.accenture.jnais.ws.enums.TipoSegno;
import it.accenture.jnais.ws.occurs.Idsv0501TabVariabili;
import it.accenture.jnais.ws.occurs.TabellaTpDato;
import it.accenture.jnais.ws.redefines.CampoAlfa;
import it.accenture.jnais.ws.redefines.CampoDecimali;
import it.accenture.jnais.ws.redefines.CampoInteri;
import it.accenture.jnais.ws.redefines.StructAlfa;
import it.accenture.jnais.ws.redefines.StructNum;

/**Original name: IDSV0502<br>
 * Variable: IDSV0502 from copybook IDSV0502<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv0502 {

    //==== PROPERTIES ====
    public static final int IDSV0501_TAB_VARIABILI_MAXOCCURS = 250;
    public static final int TABELLA_TP_DATO_MAXOCCURS = 200;
    //Original name: IDSV0501-TAB-VARIABILI
    private Idsv0501TabVariabili[] idsv0501TabVariabili = new Idsv0501TabVariabili[IDSV0501_TAB_VARIABILI_MAXOCCURS];
    //Original name: TIPO-FORMATO
    private TipoFormato tipoFormato = new TipoFormato();
    //Original name: WK-TP-DATO-PARALLELO
    private char wkTpDatoParallelo = DefaultValues.CHAR_VAL;
    //Original name: MISTA
    private char mista = 'X';
    //Original name: PRIMA-VOLTA
    private PrimaVolta primaVolta = new PrimaVolta();
    //Original name: TIPO-SEGNO
    private TipoSegno tipoSegno = new TipoSegno();
    //Original name: DATO-INPUT-TROVATO
    private DatoInputTrovato datoInputTrovato = new DatoInputTrovato();
    //Original name: FINE-STRINGA
    private FineStringa fineStringa = new FineStringa();
    //Original name: ALFANUMERICO-TROVATO
    private AlfanumericoTrovato alfanumericoTrovato = new AlfanumericoTrovato();
    //Original name: DEC-TROVATO
    private DecTrovato decTrovato = new DecTrovato();
    //Original name: INT-TROVATO
    private IntTrovato intTrovato = new IntTrovato();
    //Original name: IDSV0501-SEGNO-POSTIV
    private Idsv0501SegnoPostiv idsv0501SegnoPostiv = new Idsv0501SegnoPostiv();
    //Original name: IDSV0501-POS-SEGNO
    private Idsv0501PosSegno idsv0501PosSegno = new Idsv0501PosSegno();
    /**Original name: IND-VAR<br>
	 * <pre>**************************************************************
	 *  INDICI
	 * **************************************************************</pre>*/
    private short indVar = DefaultValues.SHORT_VAL;
    //Original name: IND-STRINGA
    private short indStringa = DefaultValues.SHORT_VAL;
    //Original name: IND-ALFA
    private short indAlfa = DefaultValues.SHORT_VAL;
    //Original name: IND-INT
    private short indInt = DefaultValues.SHORT_VAL;
    //Original name: IND-DEC
    private short indDec = DefaultValues.SHORT_VAL;
    //Original name: IND-OUT
    private short indOut = DefaultValues.SHORT_VAL;
    //Original name: IND-RICERCA
    private short indRicerca = DefaultValues.SHORT_VAL;
    /**Original name: LIMITE-INT-DEC<br>
	 * <pre>**************************************************************
	 *  LIMITI
	 * **************************************************************</pre>*/
    private String limiteIntDec = "18";
    //Original name: LIMITE-STRINGA
    private String limiteStringa = "60";
    //Original name: LIMITE-ARRAY-INPUT
    private short limiteArrayInput = ((short)250);
    //Original name: LIMITE-ARRAY-OUTPUT
    private short limiteArrayOutput = ((short)125);
    //Original name: LIMITE-DECIMALI
    private String limiteDecimali = "00";
    //Original name: LIMITE-INTERI
    private String limiteInteri = "00";
    //Original name: LIM-INT-IMP
    private String limIntImp = "11";
    //Original name: LIM-DEC-IMP
    private String limDecImp = "12";
    //Original name: LIM-INT-PERC
    private String limIntPerc = "09";
    //Original name: LIM-DEC-PERC
    private String limDecPerc = "10";
    //Original name: IDSV0501-DECIMALI-ESPOSTI-IMP
    private short idsv0501DecimaliEspostiImp = DefaultValues.SHORT_VAL;
    //Original name: IDSV0501-DECIMALI-ESPOSTI-PERC
    private short idsv0501DecimaliEspostiPerc = DefaultValues.SHORT_VAL;
    //Original name: IDSV0501-DECIMALI-ESPOSTI-TASS
    private short idsv0501DecimaliEspostiTass = DefaultValues.SHORT_VAL;
    //Original name: IDSV0501-SEPARATORE
    private char idsv0501Separatore = DefaultValues.CHAR_VAL;
    //Original name: IDSV0501-SIMBOLO-DECIMALE
    private char idsv0501SimboloDecimale = DefaultValues.CHAR_VAL;
    //Original name: SPAZIO-RICHIESTO
    private short spazioRichiesto = DefaultValues.SHORT_VAL;
    //Original name: SPAZIO-RESTANTE
    private String spazioRestante = DefaultValues.stringVal(Len.SPAZIO_RESTANTE);
    //Original name: POSIZIONE
    private short posizione = DefaultValues.SHORT_VAL;
    //Original name: TABELLA-TP-DATO
    private TabellaTpDato[] tabellaTpDato = new TabellaTpDato[TABELLA_TP_DATO_MAXOCCURS];
    //Original name: STRUCT-NUM
    private StructNum structNum = new StructNum();
    //Original name: STRUCT-ALFA
    private StructAlfa structAlfa = new StructAlfa();
    //Original name: CAMPO-INTERI
    private CampoInteri campoInteri = new CampoInteri();
    //Original name: CAMPO-DECIMALI
    private CampoDecimali campoDecimali = new CampoDecimali();
    //Original name: CAMPO-ALFA
    private CampoAlfa campoAlfa = new CampoAlfa();
    //Original name: IDSV0501-OUTPUT
    private Idsv0501OutputLvvs3480 idsv0501Output = new Idsv0501OutputLvvs3480();
    //Original name: IDSV0501-RETURN-CODE
    private Idsv0501ReturnCode idsv0501ReturnCode = new Idsv0501ReturnCode();
    //Original name: IDSV0501-DESCRIZ-ERR
    private String idsv0501DescrizErr = DefaultValues.stringVal(Len.IDSV0501_DESCRIZ_ERR);

    //==== CONSTRUCTORS ====
    public Idsv0502() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int idsv0501TabVariabiliIdx = 1; idsv0501TabVariabiliIdx <= IDSV0501_TAB_VARIABILI_MAXOCCURS; idsv0501TabVariabiliIdx++) {
            idsv0501TabVariabili[idsv0501TabVariabiliIdx - 1] = new Idsv0501TabVariabili();
        }
        for (int tabellaTpDatoIdx = 1; tabellaTpDatoIdx <= TABELLA_TP_DATO_MAXOCCURS; tabellaTpDatoIdx++) {
            tabellaTpDato[tabellaTpDatoIdx - 1] = new TabellaTpDato();
        }
    }

    public void setWkTpDatoParallelo(char wkTpDatoParallelo) {
        this.wkTpDatoParallelo = wkTpDatoParallelo;
    }

    public char getWkTpDatoParallelo() {
        return this.wkTpDatoParallelo;
    }

    public char getMista() {
        return this.mista;
    }

    public void setIndVar(short indVar) {
        this.indVar = indVar;
    }

    public short getIndVar() {
        return this.indVar;
    }

    public String getIndVarFormatted() {
        return PicFormatter.display("9(2)").format(getIndVar()).toString();
    }

    public String getIndVarAsString() {
        return getIndVarFormatted();
    }

    public void setIndStringa(short indStringa) {
        this.indStringa = indStringa;
    }

    public short getIndStringa() {
        return this.indStringa;
    }

    public void setIndAlfa(short indAlfa) {
        this.indAlfa = indAlfa;
    }

    public short getIndAlfa() {
        return this.indAlfa;
    }

    public void setIndInt(short indInt) {
        this.indInt = indInt;
    }

    public short getIndInt() {
        return this.indInt;
    }

    public void setIndDec(short indDec) {
        this.indDec = indDec;
    }

    public short getIndDec() {
        return this.indDec;
    }

    public void setIndOut(short indOut) {
        this.indOut = indOut;
    }

    public short getIndOut() {
        return this.indOut;
    }

    public void setIndRicerca(short indRicerca) {
        this.indRicerca = indRicerca;
    }

    public short getIndRicerca() {
        return this.indRicerca;
    }

    public short getLimiteIntDec() {
        return NumericDisplay.asShort(this.limiteIntDec);
    }

    public String getLimiteIntDecFormatted() {
        return this.limiteIntDec;
    }

    public short getLimiteStringa() {
        return NumericDisplay.asShort(this.limiteStringa);
    }

    public String getLimiteStringaFormatted() {
        return this.limiteStringa;
    }

    public short getLimiteArrayInput() {
        return this.limiteArrayInput;
    }

    public short getLimiteArrayOutput() {
        return this.limiteArrayOutput;
    }

    public void setLimiteDecimaliFormatted(String limiteDecimali) {
        this.limiteDecimali = Trunc.toUnsignedNumeric(limiteDecimali, Len.LIMITE_DECIMALI);
    }

    public short getLimiteDecimali() {
        return NumericDisplay.asShort(this.limiteDecimali);
    }

    public void setLimiteInteriFormatted(String limiteInteri) {
        this.limiteInteri = Trunc.toUnsignedNumeric(limiteInteri, Len.LIMITE_INTERI);
    }

    public short getLimiteInteri() {
        return NumericDisplay.asShort(this.limiteInteri);
    }

    public String getLimIntImpFormatted() {
        return this.limIntImp;
    }

    public String getLimDecImpFormatted() {
        return this.limDecImp;
    }

    public String getLimIntPercFormatted() {
        return this.limIntPerc;
    }

    public String getLimDecPercFormatted() {
        return this.limDecPerc;
    }

    public void setIdsv0501DecimaliEspostiImp(short idsv0501DecimaliEspostiImp) {
        this.idsv0501DecimaliEspostiImp = idsv0501DecimaliEspostiImp;
    }

    public short getIdsv0501DecimaliEspostiImp() {
        return this.idsv0501DecimaliEspostiImp;
    }

    public void setIdsv0501DecimaliEspostiPerc(short idsv0501DecimaliEspostiPerc) {
        this.idsv0501DecimaliEspostiPerc = idsv0501DecimaliEspostiPerc;
    }

    public short getIdsv0501DecimaliEspostiPerc() {
        return this.idsv0501DecimaliEspostiPerc;
    }

    public void setIdsv0501DecimaliEspostiTass(short idsv0501DecimaliEspostiTass) {
        this.idsv0501DecimaliEspostiTass = idsv0501DecimaliEspostiTass;
    }

    public short getIdsv0501DecimaliEspostiTass() {
        return this.idsv0501DecimaliEspostiTass;
    }

    public void setIdsv0501Separatore(char idsv0501Separatore) {
        this.idsv0501Separatore = idsv0501Separatore;
    }

    public void setIdsv0501SeparatoreFormatted(String idsv0501Separatore) {
        setIdsv0501Separatore(Functions.charAt(idsv0501Separatore, Types.CHAR_SIZE));
    }

    public char getIdsv0501Separatore() {
        return this.idsv0501Separatore;
    }

    public void setIdsv0501SimboloDecimale(char idsv0501SimboloDecimale) {
        this.idsv0501SimboloDecimale = idsv0501SimboloDecimale;
    }

    public void setIdsv0501SimboloDecimaleFormatted(String idsv0501SimboloDecimale) {
        setIdsv0501SimboloDecimale(Functions.charAt(idsv0501SimboloDecimale, Types.CHAR_SIZE));
    }

    public char getIdsv0501SimboloDecimale() {
        return this.idsv0501SimboloDecimale;
    }

    public void setSpazioRichiesto(short spazioRichiesto) {
        this.spazioRichiesto = spazioRichiesto;
    }

    public short getSpazioRichiesto() {
        return this.spazioRichiesto;
    }

    public void setSpazioRestante(short spazioRestante) {
        this.spazioRestante = NumericDisplay.asString(spazioRestante, Len.SPAZIO_RESTANTE);
    }

    public void setSpazioRestanteFormatted(String spazioRestante) {
        this.spazioRestante = Trunc.toUnsignedNumeric(spazioRestante, Len.SPAZIO_RESTANTE);
    }

    public short getSpazioRestante() {
        return NumericDisplay.asShort(this.spazioRestante);
    }

    public void setPosizione(short posizione) {
        this.posizione = posizione;
    }

    public short getPosizione() {
        return this.posizione;
    }

    public void setIdsv0501DescrizErr(String idsv0501DescrizErr) {
        this.idsv0501DescrizErr = Functions.subString(idsv0501DescrizErr, Len.IDSV0501_DESCRIZ_ERR);
    }

    public String getIdsv0501DescrizErr() {
        return this.idsv0501DescrizErr;
    }

    public String getIdsv0501DescrizErrFormatted() {
        return Functions.padBlanks(getIdsv0501DescrizErr(), Len.IDSV0501_DESCRIZ_ERR);
    }

    public AlfanumericoTrovato getAlfanumericoTrovato() {
        return alfanumericoTrovato;
    }

    public CampoAlfa getCampoAlfa() {
        return campoAlfa;
    }

    public CampoDecimali getCampoDecimali() {
        return campoDecimali;
    }

    public CampoInteri getCampoInteri() {
        return campoInteri;
    }

    public DatoInputTrovato getDatoInputTrovato() {
        return datoInputTrovato;
    }

    public DecTrovato getDecTrovato() {
        return decTrovato;
    }

    public FineStringa getFineStringa() {
        return fineStringa;
    }

    public Idsv0501OutputLvvs3480 getIdsv0501Output() {
        return idsv0501Output;
    }

    public Idsv0501PosSegno getIdsv0501PosSegno() {
        return idsv0501PosSegno;
    }

    public Idsv0501ReturnCode getIdsv0501ReturnCode() {
        return idsv0501ReturnCode;
    }

    public Idsv0501SegnoPostiv getIdsv0501SegnoPostiv() {
        return idsv0501SegnoPostiv;
    }

    public Idsv0501TabVariabili getIdsv0501TabVariabili(int idx) {
        return idsv0501TabVariabili[idx - 1];
    }

    public IntTrovato getIntTrovato() {
        return intTrovato;
    }

    public PrimaVolta getPrimaVolta() {
        return primaVolta;
    }

    public StructAlfa getStructAlfa() {
        return structAlfa;
    }

    public StructNum getStructNum() {
        return structNum;
    }

    public TabellaTpDato getTabellaTpDato(int idx) {
        return tabellaTpDato[idx - 1];
    }

    public TipoFormato getTipoFormato() {
        return tipoFormato;
    }

    public TipoSegno getTipoSegno() {
        return tipoSegno;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IND_TP_DATO = 2;
        public static final int START_RICERCA = 2;
        public static final int START_INTERO = 2;
        public static final int START_DECIMALE = 2;
        public static final int IDSV0501_DECIMALI_ESPOSTI_IMP = 1;
        public static final int IDSV0501_DECIMALI_ESPOSTI_PERC = 1;
        public static final int IDSV0501_DECIMALI_ESPOSTI_TASS = 1;
        public static final int SPAZIO_RICHIESTO = 2;
        public static final int SPAZIO_RESTANTE = 2;
        public static final int POSIZIONE = 2;
        public static final int STRUCT_ALF = 18;
        public static final int IDSV0501_DESCRIZ_ERR = 300;
        public static final int LIMITE_INTERI = 2;
        public static final int LIMITE_DECIMALI = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
