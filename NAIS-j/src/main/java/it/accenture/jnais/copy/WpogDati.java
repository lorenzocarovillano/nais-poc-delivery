package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpogIdMoviChiu;
import it.accenture.jnais.ws.redefines.WpogValDt;
import it.accenture.jnais.ws.redefines.WpogValImp;
import it.accenture.jnais.ws.redefines.WpogValNum;
import it.accenture.jnais.ws.redefines.WpogValPc;
import it.accenture.jnais.ws.redefines.WpogValTs;

/**Original name: WPOG-DATI<br>
 * Variable: WPOG-DATI from copybook LCCVPOG1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WpogDati {

    //==== PROPERTIES ====
    //Original name: WPOG-ID-PARAM-OGG
    private int wpogIdParamOgg = DefaultValues.INT_VAL;
    //Original name: WPOG-ID-OGG
    private int wpogIdOgg = DefaultValues.INT_VAL;
    //Original name: WPOG-TP-OGG
    private String wpogTpOgg = DefaultValues.stringVal(Len.WPOG_TP_OGG);
    //Original name: WPOG-ID-MOVI-CRZ
    private int wpogIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WPOG-ID-MOVI-CHIU
    private WpogIdMoviChiu wpogIdMoviChiu = new WpogIdMoviChiu();
    //Original name: WPOG-DT-INI-EFF
    private int wpogDtIniEff = DefaultValues.INT_VAL;
    //Original name: WPOG-DT-END-EFF
    private int wpogDtEndEff = DefaultValues.INT_VAL;
    //Original name: WPOG-COD-COMP-ANIA
    private int wpogCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WPOG-COD-PARAM
    private String wpogCodParam = DefaultValues.stringVal(Len.WPOG_COD_PARAM);
    //Original name: WPOG-TP-PARAM
    private char wpogTpParam = DefaultValues.CHAR_VAL;
    //Original name: WPOG-TP-D
    private String wpogTpD = DefaultValues.stringVal(Len.WPOG_TP_D);
    //Original name: WPOG-VAL-IMP
    private WpogValImp wpogValImp = new WpogValImp();
    //Original name: WPOG-VAL-DT
    private WpogValDt wpogValDt = new WpogValDt();
    //Original name: WPOG-VAL-TS
    private WpogValTs wpogValTs = new WpogValTs();
    //Original name: WPOG-VAL-TXT
    private String wpogValTxt = DefaultValues.stringVal(Len.WPOG_VAL_TXT);
    //Original name: WPOG-VAL-FL
    private char wpogValFl = DefaultValues.CHAR_VAL;
    //Original name: WPOG-VAL-NUM
    private WpogValNum wpogValNum = new WpogValNum();
    //Original name: WPOG-VAL-PC
    private WpogValPc wpogValPc = new WpogValPc();
    //Original name: WPOG-DS-RIGA
    private long wpogDsRiga = DefaultValues.LONG_VAL;
    //Original name: WPOG-DS-OPER-SQL
    private char wpogDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WPOG-DS-VER
    private int wpogDsVer = DefaultValues.INT_VAL;
    //Original name: WPOG-DS-TS-INI-CPTZ
    private long wpogDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WPOG-DS-TS-END-CPTZ
    private long wpogDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WPOG-DS-UTENTE
    private String wpogDsUtente = DefaultValues.stringVal(Len.WPOG_DS_UTENTE);
    //Original name: WPOG-DS-STATO-ELAB
    private char wpogDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wpogIdParamOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOG_ID_PARAM_OGG, 0);
        position += Len.WPOG_ID_PARAM_OGG;
        wpogIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOG_ID_OGG, 0);
        position += Len.WPOG_ID_OGG;
        wpogTpOgg = MarshalByte.readString(buffer, position, Len.WPOG_TP_OGG);
        position += Len.WPOG_TP_OGG;
        wpogIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOG_ID_MOVI_CRZ, 0);
        position += Len.WPOG_ID_MOVI_CRZ;
        wpogIdMoviChiu.setWpogIdMoviChiuFromBuffer(buffer, position);
        position += WpogIdMoviChiu.Len.WPOG_ID_MOVI_CHIU;
        wpogDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOG_DT_INI_EFF, 0);
        position += Len.WPOG_DT_INI_EFF;
        wpogDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOG_DT_END_EFF, 0);
        position += Len.WPOG_DT_END_EFF;
        wpogCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOG_COD_COMP_ANIA, 0);
        position += Len.WPOG_COD_COMP_ANIA;
        wpogCodParam = MarshalByte.readString(buffer, position, Len.WPOG_COD_PARAM);
        position += Len.WPOG_COD_PARAM;
        wpogTpParam = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpogTpD = MarshalByte.readString(buffer, position, Len.WPOG_TP_D);
        position += Len.WPOG_TP_D;
        wpogValImp.setWpogValImpFromBuffer(buffer, position);
        position += WpogValImp.Len.WPOG_VAL_IMP;
        wpogValDt.setWpogValDtFromBuffer(buffer, position);
        position += WpogValDt.Len.WPOG_VAL_DT;
        wpogValTs.setWpogValTsFromBuffer(buffer, position);
        position += WpogValTs.Len.WPOG_VAL_TS;
        wpogValTxt = MarshalByte.readString(buffer, position, Len.WPOG_VAL_TXT);
        position += Len.WPOG_VAL_TXT;
        wpogValFl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpogValNum.setWpogValNumFromBuffer(buffer, position);
        position += WpogValNum.Len.WPOG_VAL_NUM;
        wpogValPc.setWpogValPcFromBuffer(buffer, position);
        position += WpogValPc.Len.WPOG_VAL_PC;
        wpogDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPOG_DS_RIGA, 0);
        position += Len.WPOG_DS_RIGA;
        wpogDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpogDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOG_DS_VER, 0);
        position += Len.WPOG_DS_VER;
        wpogDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPOG_DS_TS_INI_CPTZ, 0);
        position += Len.WPOG_DS_TS_INI_CPTZ;
        wpogDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPOG_DS_TS_END_CPTZ, 0);
        position += Len.WPOG_DS_TS_END_CPTZ;
        wpogDsUtente = MarshalByte.readString(buffer, position, Len.WPOG_DS_UTENTE);
        position += Len.WPOG_DS_UTENTE;
        wpogDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wpogIdParamOgg, Len.Int.WPOG_ID_PARAM_OGG, 0);
        position += Len.WPOG_ID_PARAM_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, wpogIdOgg, Len.Int.WPOG_ID_OGG, 0);
        position += Len.WPOG_ID_OGG;
        MarshalByte.writeString(buffer, position, wpogTpOgg, Len.WPOG_TP_OGG);
        position += Len.WPOG_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, wpogIdMoviCrz, Len.Int.WPOG_ID_MOVI_CRZ, 0);
        position += Len.WPOG_ID_MOVI_CRZ;
        wpogIdMoviChiu.getWpogIdMoviChiuAsBuffer(buffer, position);
        position += WpogIdMoviChiu.Len.WPOG_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wpogDtIniEff, Len.Int.WPOG_DT_INI_EFF, 0);
        position += Len.WPOG_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpogDtEndEff, Len.Int.WPOG_DT_END_EFF, 0);
        position += Len.WPOG_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpogCodCompAnia, Len.Int.WPOG_COD_COMP_ANIA, 0);
        position += Len.WPOG_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wpogCodParam, Len.WPOG_COD_PARAM);
        position += Len.WPOG_COD_PARAM;
        MarshalByte.writeChar(buffer, position, wpogTpParam);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wpogTpD, Len.WPOG_TP_D);
        position += Len.WPOG_TP_D;
        wpogValImp.getWpogValImpAsBuffer(buffer, position);
        position += WpogValImp.Len.WPOG_VAL_IMP;
        wpogValDt.getWpogValDtAsBuffer(buffer, position);
        position += WpogValDt.Len.WPOG_VAL_DT;
        wpogValTs.getWpogValTsAsBuffer(buffer, position);
        position += WpogValTs.Len.WPOG_VAL_TS;
        MarshalByte.writeString(buffer, position, wpogValTxt, Len.WPOG_VAL_TXT);
        position += Len.WPOG_VAL_TXT;
        MarshalByte.writeChar(buffer, position, wpogValFl);
        position += Types.CHAR_SIZE;
        wpogValNum.getWpogValNumAsBuffer(buffer, position);
        position += WpogValNum.Len.WPOG_VAL_NUM;
        wpogValPc.getWpogValPcAsBuffer(buffer, position);
        position += WpogValPc.Len.WPOG_VAL_PC;
        MarshalByte.writeLongAsPacked(buffer, position, wpogDsRiga, Len.Int.WPOG_DS_RIGA, 0);
        position += Len.WPOG_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wpogDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wpogDsVer, Len.Int.WPOG_DS_VER, 0);
        position += Len.WPOG_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wpogDsTsIniCptz, Len.Int.WPOG_DS_TS_INI_CPTZ, 0);
        position += Len.WPOG_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wpogDsTsEndCptz, Len.Int.WPOG_DS_TS_END_CPTZ, 0);
        position += Len.WPOG_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wpogDsUtente, Len.WPOG_DS_UTENTE);
        position += Len.WPOG_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wpogDsStatoElab);
        return buffer;
    }

    public void initDatiSpaces() {
        wpogIdParamOgg = Types.INVALID_INT_VAL;
        wpogIdOgg = Types.INVALID_INT_VAL;
        wpogTpOgg = "";
        wpogIdMoviCrz = Types.INVALID_INT_VAL;
        wpogIdMoviChiu.initWpogIdMoviChiuSpaces();
        wpogDtIniEff = Types.INVALID_INT_VAL;
        wpogDtEndEff = Types.INVALID_INT_VAL;
        wpogCodCompAnia = Types.INVALID_INT_VAL;
        wpogCodParam = "";
        wpogTpParam = Types.SPACE_CHAR;
        wpogTpD = "";
        wpogValImp.initWpogValImpSpaces();
        wpogValDt.initWpogValDtSpaces();
        wpogValTs.initWpogValTsSpaces();
        wpogValTxt = "";
        wpogValFl = Types.SPACE_CHAR;
        wpogValNum.initWpogValNumSpaces();
        wpogValPc.initWpogValPcSpaces();
        wpogDsRiga = Types.INVALID_LONG_VAL;
        wpogDsOperSql = Types.SPACE_CHAR;
        wpogDsVer = Types.INVALID_INT_VAL;
        wpogDsTsIniCptz = Types.INVALID_LONG_VAL;
        wpogDsTsEndCptz = Types.INVALID_LONG_VAL;
        wpogDsUtente = "";
        wpogDsStatoElab = Types.SPACE_CHAR;
    }

    public void setWpogIdParamOgg(int wpogIdParamOgg) {
        this.wpogIdParamOgg = wpogIdParamOgg;
    }

    public int getWpogIdParamOgg() {
        return this.wpogIdParamOgg;
    }

    public void setWpogIdOgg(int wpogIdOgg) {
        this.wpogIdOgg = wpogIdOgg;
    }

    public int getWpogIdOgg() {
        return this.wpogIdOgg;
    }

    public void setWpogTpOgg(String wpogTpOgg) {
        this.wpogTpOgg = Functions.subString(wpogTpOgg, Len.WPOG_TP_OGG);
    }

    public String getWpogTpOgg() {
        return this.wpogTpOgg;
    }

    public void setWpogIdMoviCrz(int wpogIdMoviCrz) {
        this.wpogIdMoviCrz = wpogIdMoviCrz;
    }

    public int getWpogIdMoviCrz() {
        return this.wpogIdMoviCrz;
    }

    public void setWpogDtIniEff(int wpogDtIniEff) {
        this.wpogDtIniEff = wpogDtIniEff;
    }

    public int getWpogDtIniEff() {
        return this.wpogDtIniEff;
    }

    public void setWpogDtEndEff(int wpogDtEndEff) {
        this.wpogDtEndEff = wpogDtEndEff;
    }

    public int getWpogDtEndEff() {
        return this.wpogDtEndEff;
    }

    public void setWpogCodCompAnia(int wpogCodCompAnia) {
        this.wpogCodCompAnia = wpogCodCompAnia;
    }

    public int getWpogCodCompAnia() {
        return this.wpogCodCompAnia;
    }

    public void setWpogCodParam(String wpogCodParam) {
        this.wpogCodParam = Functions.subString(wpogCodParam, Len.WPOG_COD_PARAM);
    }

    public String getWpogCodParam() {
        return this.wpogCodParam;
    }

    public void setWpogTpParam(char wpogTpParam) {
        this.wpogTpParam = wpogTpParam;
    }

    public char getWpogTpParam() {
        return this.wpogTpParam;
    }

    public void setWpogTpD(String wpogTpD) {
        this.wpogTpD = Functions.subString(wpogTpD, Len.WPOG_TP_D);
    }

    public String getWpogTpD() {
        return this.wpogTpD;
    }

    public String getDpogTpDFormatted() {
        return Functions.padBlanks(getWpogTpD(), Len.WPOG_TP_D);
    }

    public void setWpogValTxt(String wpogValTxt) {
        this.wpogValTxt = Functions.subString(wpogValTxt, Len.WPOG_VAL_TXT);
    }

    public String getWpogValTxt() {
        return this.wpogValTxt;
    }

    public void setWpogValFl(char wpogValFl) {
        this.wpogValFl = wpogValFl;
    }

    public char getWpogValFl() {
        return this.wpogValFl;
    }

    public void setWpogDsRiga(long wpogDsRiga) {
        this.wpogDsRiga = wpogDsRiga;
    }

    public long getWpogDsRiga() {
        return this.wpogDsRiga;
    }

    public void setWpogDsOperSql(char wpogDsOperSql) {
        this.wpogDsOperSql = wpogDsOperSql;
    }

    public char getWpogDsOperSql() {
        return this.wpogDsOperSql;
    }

    public void setWpogDsVer(int wpogDsVer) {
        this.wpogDsVer = wpogDsVer;
    }

    public int getWpogDsVer() {
        return this.wpogDsVer;
    }

    public void setWpogDsTsIniCptz(long wpogDsTsIniCptz) {
        this.wpogDsTsIniCptz = wpogDsTsIniCptz;
    }

    public long getWpogDsTsIniCptz() {
        return this.wpogDsTsIniCptz;
    }

    public void setWpogDsTsEndCptz(long wpogDsTsEndCptz) {
        this.wpogDsTsEndCptz = wpogDsTsEndCptz;
    }

    public long getWpogDsTsEndCptz() {
        return this.wpogDsTsEndCptz;
    }

    public void setWpogDsUtente(String wpogDsUtente) {
        this.wpogDsUtente = Functions.subString(wpogDsUtente, Len.WPOG_DS_UTENTE);
    }

    public String getWpogDsUtente() {
        return this.wpogDsUtente;
    }

    public void setWpogDsStatoElab(char wpogDsStatoElab) {
        this.wpogDsStatoElab = wpogDsStatoElab;
    }

    public char getWpogDsStatoElab() {
        return this.wpogDsStatoElab;
    }

    public WpogIdMoviChiu getWpogIdMoviChiu() {
        return wpogIdMoviChiu;
    }

    public WpogValDt getWpogValDt() {
        return wpogValDt;
    }

    public WpogValImp getWpogValImp() {
        return wpogValImp;
    }

    public WpogValNum getWpogValNum() {
        return wpogValNum;
    }

    public WpogValPc getWpogValPc() {
        return wpogValPc;
    }

    public WpogValTs getWpogValTs() {
        return wpogValTs;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOG_ID_PARAM_OGG = 5;
        public static final int WPOG_ID_OGG = 5;
        public static final int WPOG_TP_OGG = 2;
        public static final int WPOG_ID_MOVI_CRZ = 5;
        public static final int WPOG_DT_INI_EFF = 5;
        public static final int WPOG_DT_END_EFF = 5;
        public static final int WPOG_COD_COMP_ANIA = 3;
        public static final int WPOG_COD_PARAM = 20;
        public static final int WPOG_TP_PARAM = 1;
        public static final int WPOG_TP_D = 2;
        public static final int WPOG_VAL_TXT = 100;
        public static final int WPOG_VAL_FL = 1;
        public static final int WPOG_DS_RIGA = 6;
        public static final int WPOG_DS_OPER_SQL = 1;
        public static final int WPOG_DS_VER = 5;
        public static final int WPOG_DS_TS_INI_CPTZ = 10;
        public static final int WPOG_DS_TS_END_CPTZ = 10;
        public static final int WPOG_DS_UTENTE = 20;
        public static final int WPOG_DS_STATO_ELAB = 1;
        public static final int DATI = WPOG_ID_PARAM_OGG + WPOG_ID_OGG + WPOG_TP_OGG + WPOG_ID_MOVI_CRZ + WpogIdMoviChiu.Len.WPOG_ID_MOVI_CHIU + WPOG_DT_INI_EFF + WPOG_DT_END_EFF + WPOG_COD_COMP_ANIA + WPOG_COD_PARAM + WPOG_TP_PARAM + WPOG_TP_D + WpogValImp.Len.WPOG_VAL_IMP + WpogValDt.Len.WPOG_VAL_DT + WpogValTs.Len.WPOG_VAL_TS + WPOG_VAL_TXT + WPOG_VAL_FL + WpogValNum.Len.WPOG_VAL_NUM + WpogValPc.Len.WPOG_VAL_PC + WPOG_DS_RIGA + WPOG_DS_OPER_SQL + WPOG_DS_VER + WPOG_DS_TS_INI_CPTZ + WPOG_DS_TS_END_CPTZ + WPOG_DS_UTENTE + WPOG_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOG_ID_PARAM_OGG = 9;
            public static final int WPOG_ID_OGG = 9;
            public static final int WPOG_ID_MOVI_CRZ = 9;
            public static final int WPOG_DT_INI_EFF = 8;
            public static final int WPOG_DT_END_EFF = 8;
            public static final int WPOG_COD_COMP_ANIA = 5;
            public static final int WPOG_DS_RIGA = 10;
            public static final int WPOG_DS_VER = 9;
            public static final int WPOG_DS_TS_INI_CPTZ = 18;
            public static final int WPOG_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
