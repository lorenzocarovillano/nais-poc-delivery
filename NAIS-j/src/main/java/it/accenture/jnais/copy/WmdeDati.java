package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WmdeIdMoviChiu;

/**Original name: WMDE-DATI<br>
 * Variable: WMDE-DATI from copybook LCCVMDE1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WmdeDati {

    //==== PROPERTIES ====
    //Original name: WMDE-ID-MOT-DEROGA
    private int wmdeIdMotDeroga = DefaultValues.INT_VAL;
    //Original name: WMDE-ID-OGG-DEROGA
    private int wmdeIdOggDeroga = DefaultValues.INT_VAL;
    //Original name: WMDE-ID-MOVI-CRZ
    private int wmdeIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WMDE-ID-MOVI-CHIU
    private WmdeIdMoviChiu wmdeIdMoviChiu = new WmdeIdMoviChiu();
    //Original name: WMDE-DT-INI-EFF
    private int wmdeDtIniEff = DefaultValues.INT_VAL;
    //Original name: WMDE-DT-END-EFF
    private int wmdeDtEndEff = DefaultValues.INT_VAL;
    //Original name: WMDE-COD-COMP-ANIA
    private int wmdeCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WMDE-TP-MOT-DEROGA
    private String wmdeTpMotDeroga = DefaultValues.stringVal(Len.WMDE_TP_MOT_DEROGA);
    //Original name: WMDE-COD-LIV-AUT
    private int wmdeCodLivAut = DefaultValues.INT_VAL;
    //Original name: WMDE-COD-ERR
    private String wmdeCodErr = DefaultValues.stringVal(Len.WMDE_COD_ERR);
    //Original name: WMDE-TP-ERR
    private String wmdeTpErr = DefaultValues.stringVal(Len.WMDE_TP_ERR);
    //Original name: WMDE-DESC-ERR-BREVE
    private String wmdeDescErrBreve = DefaultValues.stringVal(Len.WMDE_DESC_ERR_BREVE);
    //Original name: WMDE-DESC-ERR-EST
    private String wmdeDescErrEst = DefaultValues.stringVal(Len.WMDE_DESC_ERR_EST);
    //Original name: WMDE-DS-RIGA
    private long wmdeDsRiga = DefaultValues.LONG_VAL;
    //Original name: WMDE-DS-OPER-SQL
    private char wmdeDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WMDE-DS-VER
    private int wmdeDsVer = DefaultValues.INT_VAL;
    //Original name: WMDE-DS-TS-INI-CPTZ
    private long wmdeDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WMDE-DS-TS-END-CPTZ
    private long wmdeDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WMDE-DS-UTENTE
    private String wmdeDsUtente = DefaultValues.stringVal(Len.WMDE_DS_UTENTE);
    //Original name: WMDE-DS-STATO-ELAB
    private char wmdeDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setWmdeIdMotDeroga(int wmdeIdMotDeroga) {
        this.wmdeIdMotDeroga = wmdeIdMotDeroga;
    }

    public int getWmdeIdMotDeroga() {
        return this.wmdeIdMotDeroga;
    }

    public void setWmdeIdOggDeroga(int wmdeIdOggDeroga) {
        this.wmdeIdOggDeroga = wmdeIdOggDeroga;
    }

    public int getWmdeIdOggDeroga() {
        return this.wmdeIdOggDeroga;
    }

    public void setWmdeIdMoviCrz(int wmdeIdMoviCrz) {
        this.wmdeIdMoviCrz = wmdeIdMoviCrz;
    }

    public int getWmdeIdMoviCrz() {
        return this.wmdeIdMoviCrz;
    }

    public void setWmdeDtIniEff(int wmdeDtIniEff) {
        this.wmdeDtIniEff = wmdeDtIniEff;
    }

    public int getWmdeDtIniEff() {
        return this.wmdeDtIniEff;
    }

    public void setWmdeDtEndEff(int wmdeDtEndEff) {
        this.wmdeDtEndEff = wmdeDtEndEff;
    }

    public int getWmdeDtEndEff() {
        return this.wmdeDtEndEff;
    }

    public void setWmdeCodCompAnia(int wmdeCodCompAnia) {
        this.wmdeCodCompAnia = wmdeCodCompAnia;
    }

    public int getWmdeCodCompAnia() {
        return this.wmdeCodCompAnia;
    }

    public void setWmdeTpMotDeroga(String wmdeTpMotDeroga) {
        this.wmdeTpMotDeroga = Functions.subString(wmdeTpMotDeroga, Len.WMDE_TP_MOT_DEROGA);
    }

    public String getWmdeTpMotDeroga() {
        return this.wmdeTpMotDeroga;
    }

    public void setWmdeCodLivAut(int wmdeCodLivAut) {
        this.wmdeCodLivAut = wmdeCodLivAut;
    }

    public int getWmdeCodLivAut() {
        return this.wmdeCodLivAut;
    }

    public void setWmdeCodErr(String wmdeCodErr) {
        this.wmdeCodErr = Functions.subString(wmdeCodErr, Len.WMDE_COD_ERR);
    }

    public String getWmdeCodErr() {
        return this.wmdeCodErr;
    }

    public void setWmdeTpErr(String wmdeTpErr) {
        this.wmdeTpErr = Functions.subString(wmdeTpErr, Len.WMDE_TP_ERR);
    }

    public String getWmdeTpErr() {
        return this.wmdeTpErr;
    }

    public void setWmdeDescErrBreve(String wmdeDescErrBreve) {
        this.wmdeDescErrBreve = Functions.subString(wmdeDescErrBreve, Len.WMDE_DESC_ERR_BREVE);
    }

    public String getWmdeDescErrBreve() {
        return this.wmdeDescErrBreve;
    }

    public void setWmdeDescErrEst(String wmdeDescErrEst) {
        this.wmdeDescErrEst = Functions.subString(wmdeDescErrEst, Len.WMDE_DESC_ERR_EST);
    }

    public String getWmdeDescErrEst() {
        return this.wmdeDescErrEst;
    }

    public void setWmdeDsRiga(long wmdeDsRiga) {
        this.wmdeDsRiga = wmdeDsRiga;
    }

    public long getWmdeDsRiga() {
        return this.wmdeDsRiga;
    }

    public void setWmdeDsOperSql(char wmdeDsOperSql) {
        this.wmdeDsOperSql = wmdeDsOperSql;
    }

    public char getWmdeDsOperSql() {
        return this.wmdeDsOperSql;
    }

    public void setWmdeDsVer(int wmdeDsVer) {
        this.wmdeDsVer = wmdeDsVer;
    }

    public int getWmdeDsVer() {
        return this.wmdeDsVer;
    }

    public void setWmdeDsTsIniCptz(long wmdeDsTsIniCptz) {
        this.wmdeDsTsIniCptz = wmdeDsTsIniCptz;
    }

    public long getWmdeDsTsIniCptz() {
        return this.wmdeDsTsIniCptz;
    }

    public void setWmdeDsTsEndCptz(long wmdeDsTsEndCptz) {
        this.wmdeDsTsEndCptz = wmdeDsTsEndCptz;
    }

    public long getWmdeDsTsEndCptz() {
        return this.wmdeDsTsEndCptz;
    }

    public void setWmdeDsUtente(String wmdeDsUtente) {
        this.wmdeDsUtente = Functions.subString(wmdeDsUtente, Len.WMDE_DS_UTENTE);
    }

    public String getWmdeDsUtente() {
        return this.wmdeDsUtente;
    }

    public void setWmdeDsStatoElab(char wmdeDsStatoElab) {
        this.wmdeDsStatoElab = wmdeDsStatoElab;
    }

    public char getWmdeDsStatoElab() {
        return this.wmdeDsStatoElab;
    }

    public WmdeIdMoviChiu getWmdeIdMoviChiu() {
        return wmdeIdMoviChiu;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WMDE_TP_MOT_DEROGA = 2;
        public static final int WMDE_COD_ERR = 12;
        public static final int WMDE_TP_ERR = 2;
        public static final int WMDE_DESC_ERR_BREVE = 100;
        public static final int WMDE_DESC_ERR_EST = 250;
        public static final int WMDE_DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
