package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.WcdgFndXCdgCalc;
import it.accenture.jnais.ws.occurs.WcdgFndXCdgVv;
import it.accenture.jnais.ws.occurs.WcdgFndXReccdg;

/**Original name: IVVC0224<br>
 * Variable: IVVC0224 from copybook IVVC0224<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0224 {

    //==== PROPERTIES ====
    public static final int FND_X_CDG_CALC_MAXOCCURS = 200;
    public static final int FND_X_CDG_VV_MAXOCCURS = 200;
    public static final int FND_X_RECCDG_MAXOCCURS = 200;
    //Original name: WCDG-ELE-MAX-CDG-CALC
    private String eleMaxCdgCalc = DefaultValues.stringVal(Len.ELE_MAX_CDG_CALC);
    //Original name: WCDG-FND-X-CDG-CALC
    private WcdgFndXCdgCalc[] fndXCdgCalc = new WcdgFndXCdgCalc[FND_X_CDG_CALC_MAXOCCURS];
    //Original name: WCDG-ELE-MAX-CDG-VV
    private String eleMaxCdgVv = DefaultValues.stringVal(Len.ELE_MAX_CDG_VV);
    //Original name: WCDG-FND-X-CDG-VV
    private WcdgFndXCdgVv[] fndXCdgVv = new WcdgFndXCdgVv[FND_X_CDG_VV_MAXOCCURS];
    //Original name: WCDG-ELE-MAX-RECCDG
    private String eleMaxReccdg = DefaultValues.stringVal(Len.ELE_MAX_RECCDG);
    //Original name: WCDG-FND-X-RECCDG
    private WcdgFndXReccdg[] fndXReccdg = new WcdgFndXReccdg[FND_X_RECCDG_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ivvc0224() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int fndXCdgCalcIdx = 1; fndXCdgCalcIdx <= FND_X_CDG_CALC_MAXOCCURS; fndXCdgCalcIdx++) {
            fndXCdgCalc[fndXCdgCalcIdx - 1] = new WcdgFndXCdgCalc();
        }
        for (int fndXCdgVvIdx = 1; fndXCdgVvIdx <= FND_X_CDG_VV_MAXOCCURS; fndXCdgVvIdx++) {
            fndXCdgVv[fndXCdgVvIdx - 1] = new WcdgFndXCdgVv();
        }
        for (int fndXReccdgIdx = 1; fndXReccdgIdx <= FND_X_RECCDG_MAXOCCURS; fndXReccdgIdx++) {
            fndXReccdg[fndXReccdgIdx - 1] = new WcdgFndXReccdg();
        }
    }

    public void setAreaCommisGestCalcBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxCdgCalc = MarshalByte.readFixedString(buffer, position, Len.ELE_MAX_CDG_CALC);
        position += Len.ELE_MAX_CDG_CALC;
        for (int idx = 1; idx <= FND_X_CDG_CALC_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                fndXCdgCalc[idx - 1].setFndXCdgCalcBytes(buffer, position);
                position += WcdgFndXCdgCalc.Len.FND_X_CDG_CALC;
            }
            else {
                fndXCdgCalc[idx - 1].initFndXCdgCalcSpaces();
                position += WcdgFndXCdgCalc.Len.FND_X_CDG_CALC;
            }
        }
    }

    public byte[] getAreaCommisGestCalcBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, eleMaxCdgCalc, Len.ELE_MAX_CDG_CALC);
        position += Len.ELE_MAX_CDG_CALC;
        for (int idx = 1; idx <= FND_X_CDG_CALC_MAXOCCURS; idx++) {
            fndXCdgCalc[idx - 1].getFndXCdgCalcBytes(buffer, position);
            position += WcdgFndXCdgCalc.Len.FND_X_CDG_CALC;
        }
        return buffer;
    }

    public void setAreaCommisGestVvFormatted(String data) {
        byte[] buffer = new byte[Len.AREA_COMMIS_GEST_VV];
        MarshalByte.writeString(buffer, 1, data, Len.AREA_COMMIS_GEST_VV);
        setAreaCommisGestVvBytes(buffer, 1);
    }

    public String getAreaCommisGestVvFormatted() {
        return MarshalByteExt.bufferToStr(getAreaCommisGestVvBytes());
    }

    /**Original name: WCDG-AREA-COMMIS-GEST-VV<br>
	 * <pre> -- AREA I/O VALORIZZATORE VARIABILI</pre>*/
    public byte[] getAreaCommisGestVvBytes() {
        byte[] buffer = new byte[Len.AREA_COMMIS_GEST_VV];
        return getAreaCommisGestVvBytes(buffer, 1);
    }

    public void setAreaCommisGestVvBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxCdgVv = MarshalByte.readFixedString(buffer, position, Len.ELE_MAX_CDG_VV);
        position += Len.ELE_MAX_CDG_VV;
        for (int idx = 1; idx <= FND_X_CDG_VV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                fndXCdgVv[idx - 1].setFndXCdgVvBytes(buffer, position);
                position += WcdgFndXCdgVv.Len.FND_X_CDG_VV;
            }
            else {
                fndXCdgVv[idx - 1].initFndXCdgVvSpaces();
                position += WcdgFndXCdgVv.Len.FND_X_CDG_VV;
            }
        }
    }

    public byte[] getAreaCommisGestVvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, eleMaxCdgVv, Len.ELE_MAX_CDG_VV);
        position += Len.ELE_MAX_CDG_VV;
        for (int idx = 1; idx <= FND_X_CDG_VV_MAXOCCURS; idx++) {
            fndXCdgVv[idx - 1].getFndXCdgVvBytes(buffer, position);
            position += WcdgFndXCdgVv.Len.FND_X_CDG_VV;
        }
        return buffer;
    }

    public void setEleMaxCdgVvFormatted(String eleMaxCdgVv) {
        this.eleMaxCdgVv = Trunc.toUnsignedNumeric(eleMaxCdgVv, Len.ELE_MAX_CDG_VV);
    }

    public short getEleMaxCdgVv() {
        return NumericDisplay.asShort(this.eleMaxCdgVv);
    }

    public String getEleMaxCdgVvFormatted() {
        return this.eleMaxCdgVv;
    }

    public void setAreaCommisGestOutBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxReccdg = MarshalByte.readFixedString(buffer, position, Len.ELE_MAX_RECCDG);
        position += Len.ELE_MAX_RECCDG;
        for (int idx = 1; idx <= FND_X_RECCDG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                fndXReccdg[idx - 1].setFndXReccdgBytes(buffer, position);
                position += WcdgFndXReccdg.Len.FND_X_RECCDG;
            }
            else {
                fndXReccdg[idx - 1].initFndXReccdgSpaces();
                position += WcdgFndXReccdg.Len.FND_X_RECCDG;
            }
        }
    }

    public byte[] getAreaCommisGestOutBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, eleMaxReccdg, Len.ELE_MAX_RECCDG);
        position += Len.ELE_MAX_RECCDG;
        for (int idx = 1; idx <= FND_X_RECCDG_MAXOCCURS; idx++) {
            fndXReccdg[idx - 1].getFndXReccdgBytes(buffer, position);
            position += WcdgFndXReccdg.Len.FND_X_RECCDG;
        }
        return buffer;
    }

    public WcdgFndXCdgVv getFndXCdgVv(int idx) {
        return fndXCdgVv[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_MAX_CDG_CALC = 4;
        public static final int AREA_COMMIS_GEST_CALC = ELE_MAX_CDG_CALC + Ivvc0224.FND_X_CDG_CALC_MAXOCCURS * WcdgFndXCdgCalc.Len.FND_X_CDG_CALC;
        public static final int ELE_MAX_CDG_VV = 4;
        public static final int AREA_COMMIS_GEST_VV = ELE_MAX_CDG_VV + Ivvc0224.FND_X_CDG_VV_MAXOCCURS * WcdgFndXCdgVv.Len.FND_X_CDG_VV;
        public static final int ELE_MAX_RECCDG = 4;
        public static final int AREA_COMMIS_GEST_OUT = ELE_MAX_RECCDG + Ivvc0224.FND_X_RECCDG_MAXOCCURS * WcdgFndXReccdg.Len.FND_X_RECCDG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
