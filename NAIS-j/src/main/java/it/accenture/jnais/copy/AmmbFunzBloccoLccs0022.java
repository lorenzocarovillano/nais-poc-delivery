package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.L16TpMoviRifto;

/**Original name: AMMB-FUNZ-BLOCCO<br>
 * Variable: AMMB-FUNZ-BLOCCO from copybook IDBVL161<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AmmbFunzBloccoLccs0022 {

    //==== PROPERTIES ====
    //Original name: L16-COD-COMP-ANIA
    private int l16CodCompAnia = DefaultValues.INT_VAL;
    //Original name: L16-COD-CAN
    private int l16CodCan = DefaultValues.INT_VAL;
    //Original name: L16-COD-BLOCCO
    private String l16CodBlocco = DefaultValues.stringVal(Len.L16_COD_BLOCCO);
    //Original name: L16-TP-MOVI
    private int l16TpMovi = DefaultValues.INT_VAL;
    //Original name: L16-GRAV-FUNZ-BLOCCO
    private char l16GravFunzBlocco = DefaultValues.CHAR_VAL;
    //Original name: L16-DS-OPER-SQL
    private char l16DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: L16-DS-VER
    private int l16DsVer = DefaultValues.INT_VAL;
    //Original name: L16-DS-TS-CPTZ
    private long l16DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: L16-DS-UTENTE
    private String l16DsUtente = DefaultValues.stringVal(Len.L16_DS_UTENTE);
    //Original name: L16-DS-STATO-ELAB
    private char l16DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: L16-TP-MOVI-RIFTO
    private L16TpMoviRifto l16TpMoviRifto = new L16TpMoviRifto();

    //==== METHODS ====
    public void setAmmbFunzBloccoFormatted(String data) {
        byte[] buffer = new byte[Len.AMMB_FUNZ_BLOCCO];
        MarshalByte.writeString(buffer, 1, data, Len.AMMB_FUNZ_BLOCCO);
        setAmmbFunzBloccoBytes(buffer, 1);
    }

    public String getAmmbFunzBloccoFormatted() {
        return MarshalByteExt.bufferToStr(getAmmbFunzBloccoBytes());
    }

    public byte[] getAmmbFunzBloccoBytes() {
        byte[] buffer = new byte[Len.AMMB_FUNZ_BLOCCO];
        return getAmmbFunzBloccoBytes(buffer, 1);
    }

    public void setAmmbFunzBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        l16CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L16_COD_COMP_ANIA, 0);
        position += Len.L16_COD_COMP_ANIA;
        l16CodCan = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L16_COD_CAN, 0);
        position += Len.L16_COD_CAN;
        l16CodBlocco = MarshalByte.readString(buffer, position, Len.L16_COD_BLOCCO);
        position += Len.L16_COD_BLOCCO;
        l16TpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L16_TP_MOVI, 0);
        position += Len.L16_TP_MOVI;
        l16GravFunzBlocco = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l16DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l16DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L16_DS_VER, 0);
        position += Len.L16_DS_VER;
        l16DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L16_DS_TS_CPTZ, 0);
        position += Len.L16_DS_TS_CPTZ;
        l16DsUtente = MarshalByte.readString(buffer, position, Len.L16_DS_UTENTE);
        position += Len.L16_DS_UTENTE;
        l16DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l16TpMoviRifto.setL16TpMoviRiftoFromBuffer(buffer, position);
    }

    public byte[] getAmmbFunzBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, l16CodCompAnia, Len.Int.L16_COD_COMP_ANIA, 0);
        position += Len.L16_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, l16CodCan, Len.Int.L16_COD_CAN, 0);
        position += Len.L16_COD_CAN;
        MarshalByte.writeString(buffer, position, l16CodBlocco, Len.L16_COD_BLOCCO);
        position += Len.L16_COD_BLOCCO;
        MarshalByte.writeIntAsPacked(buffer, position, l16TpMovi, Len.Int.L16_TP_MOVI, 0);
        position += Len.L16_TP_MOVI;
        MarshalByte.writeChar(buffer, position, l16GravFunzBlocco);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, l16DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, l16DsVer, Len.Int.L16_DS_VER, 0);
        position += Len.L16_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, l16DsTsCptz, Len.Int.L16_DS_TS_CPTZ, 0);
        position += Len.L16_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, l16DsUtente, Len.L16_DS_UTENTE);
        position += Len.L16_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, l16DsStatoElab);
        position += Types.CHAR_SIZE;
        l16TpMoviRifto.getL16TpMoviRiftoAsBuffer(buffer, position);
        return buffer;
    }

    public void setL16CodCompAnia(int l16CodCompAnia) {
        this.l16CodCompAnia = l16CodCompAnia;
    }

    public int getL16CodCompAnia() {
        return this.l16CodCompAnia;
    }

    public void setL16CodCan(int l16CodCan) {
        this.l16CodCan = l16CodCan;
    }

    public int getL16CodCan() {
        return this.l16CodCan;
    }

    public void setL16CodBlocco(String l16CodBlocco) {
        this.l16CodBlocco = Functions.subString(l16CodBlocco, Len.L16_COD_BLOCCO);
    }

    public String getL16CodBlocco() {
        return this.l16CodBlocco;
    }

    public void setL16TpMovi(int l16TpMovi) {
        this.l16TpMovi = l16TpMovi;
    }

    public int getL16TpMovi() {
        return this.l16TpMovi;
    }

    public void setL16GravFunzBlocco(char l16GravFunzBlocco) {
        this.l16GravFunzBlocco = l16GravFunzBlocco;
    }

    public char getL16GravFunzBlocco() {
        return this.l16GravFunzBlocco;
    }

    public void setL16DsOperSql(char l16DsOperSql) {
        this.l16DsOperSql = l16DsOperSql;
    }

    public char getL16DsOperSql() {
        return this.l16DsOperSql;
    }

    public void setL16DsVer(int l16DsVer) {
        this.l16DsVer = l16DsVer;
    }

    public int getL16DsVer() {
        return this.l16DsVer;
    }

    public void setL16DsTsCptz(long l16DsTsCptz) {
        this.l16DsTsCptz = l16DsTsCptz;
    }

    public long getL16DsTsCptz() {
        return this.l16DsTsCptz;
    }

    public void setL16DsUtente(String l16DsUtente) {
        this.l16DsUtente = Functions.subString(l16DsUtente, Len.L16_DS_UTENTE);
    }

    public String getL16DsUtente() {
        return this.l16DsUtente;
    }

    public void setL16DsStatoElab(char l16DsStatoElab) {
        this.l16DsStatoElab = l16DsStatoElab;
    }

    public char getL16DsStatoElab() {
        return this.l16DsStatoElab;
    }

    public L16TpMoviRifto getL16TpMoviRifto() {
        return l16TpMoviRifto;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L16_COD_BLOCCO = 5;
        public static final int L16_DS_UTENTE = 20;
        public static final int L16_COD_COMP_ANIA = 3;
        public static final int L16_COD_CAN = 3;
        public static final int L16_TP_MOVI = 3;
        public static final int L16_GRAV_FUNZ_BLOCCO = 1;
        public static final int L16_DS_OPER_SQL = 1;
        public static final int L16_DS_VER = 5;
        public static final int L16_DS_TS_CPTZ = 10;
        public static final int L16_DS_STATO_ELAB = 1;
        public static final int AMMB_FUNZ_BLOCCO = L16_COD_COMP_ANIA + L16_COD_CAN + L16_COD_BLOCCO + L16_TP_MOVI + L16_GRAV_FUNZ_BLOCCO + L16_DS_OPER_SQL + L16_DS_VER + L16_DS_TS_CPTZ + L16_DS_UTENTE + L16_DS_STATO_ELAB + L16TpMoviRifto.Len.L16_TP_MOVI_RIFTO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L16_COD_COMP_ANIA = 5;
            public static final int L16_COD_CAN = 5;
            public static final int L16_TP_MOVI = 5;
            public static final int L16_DS_VER = 9;
            public static final int L16_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
