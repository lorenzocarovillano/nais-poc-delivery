package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-CTRL-AUT-OPER<br>
 * Variable: IND-CTRL-AUT-OPER from copybook IDBVCAO2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndCtrlAutOper {

    //==== PROPERTIES ====
    //Original name: IND-CAO-TP-MOVI
    private short tpMovi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CAO-KEY-AUT-OPER1
    private short keyAutOper1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CAO-KEY-AUT-OPER2
    private short keyAutOper2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CAO-KEY-AUT-OPER3
    private short keyAutOper3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CAO-KEY-AUT-OPER4
    private short keyAutOper4 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CAO-KEY-AUT-OPER5
    private short keyAutOper5 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CAO-MODULO-VERIFICA
    private short moduloVerifica = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CAO-COD-COND
    private short codCond = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CAO-PROG-COND
    private short progCond = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CAO-RISULT-COND
    private short risultCond = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CAO-PARAM-AUT-OPER
    private short paramAutOper = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CAO-STATO-ATTIVAZIONE
    private short statoAttivazione = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setTpMovi(short tpMovi) {
        this.tpMovi = tpMovi;
    }

    public short getTpMovi() {
        return this.tpMovi;
    }

    public void setKeyAutOper1(short keyAutOper1) {
        this.keyAutOper1 = keyAutOper1;
    }

    public short getKeyAutOper1() {
        return this.keyAutOper1;
    }

    public void setKeyAutOper2(short keyAutOper2) {
        this.keyAutOper2 = keyAutOper2;
    }

    public short getKeyAutOper2() {
        return this.keyAutOper2;
    }

    public void setKeyAutOper3(short keyAutOper3) {
        this.keyAutOper3 = keyAutOper3;
    }

    public short getKeyAutOper3() {
        return this.keyAutOper3;
    }

    public void setKeyAutOper4(short keyAutOper4) {
        this.keyAutOper4 = keyAutOper4;
    }

    public short getKeyAutOper4() {
        return this.keyAutOper4;
    }

    public void setKeyAutOper5(short keyAutOper5) {
        this.keyAutOper5 = keyAutOper5;
    }

    public short getKeyAutOper5() {
        return this.keyAutOper5;
    }

    public void setModuloVerifica(short moduloVerifica) {
        this.moduloVerifica = moduloVerifica;
    }

    public short getModuloVerifica() {
        return this.moduloVerifica;
    }

    public void setCodCond(short codCond) {
        this.codCond = codCond;
    }

    public short getCodCond() {
        return this.codCond;
    }

    public void setProgCond(short progCond) {
        this.progCond = progCond;
    }

    public short getProgCond() {
        return this.progCond;
    }

    public void setRisultCond(short risultCond) {
        this.risultCond = risultCond;
    }

    public short getRisultCond() {
        return this.risultCond;
    }

    public void setParamAutOper(short paramAutOper) {
        this.paramAutOper = paramAutOper;
    }

    public short getParamAutOper() {
        return this.paramAutOper;
    }

    public void setStatoAttivazione(short statoAttivazione) {
        this.statoAttivazione = statoAttivazione;
    }

    public short getStatoAttivazione() {
        return this.statoAttivazione;
    }
}
