package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.FileStatus1;
import it.accenture.jnais.ws.enums.SequentialEsito;

/**Original name: IABCSQ99<br>
 * Variable: IABCSQ99 from copybook IABCSQ99<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Iabcsq99 {

    //==== PROPERTIES ====
    //Original name: FS-FILESQS1
    private String fsFilesqs1 = "00";
    //Original name: FS-FILESQS2
    private String fsFilesqs2 = "00";
    //Original name: FS-FILESQS3
    private String fsFilesqs3 = "00";
    //Original name: FS-FILESQS4
    private String fsFilesqs4 = "00";
    //Original name: FS-FILESQS5
    private String fsFilesqs5 = "00";
    //Original name: FS-FILESQS6
    private String fsFilesqs6 = "00";
    //Original name: NOME-FILESQS1
    private String nomeFilesqs1 = "FILESQS1";
    //Original name: NOME-FILESQS2
    private String nomeFilesqs2 = "FILESQS2";
    //Original name: NOME-FILESQS3
    private String nomeFilesqs3 = "FILESQS3";
    //Original name: NOME-FILESQS4
    private String nomeFilesqs4 = "FILESQS4";
    //Original name: NOME-FILESQS5
    private String nomeFilesqs5 = "FILESQS5";
    //Original name: NOME-FILESQS6
    private String nomeFilesqs6 = "FILESQS6";
    /**Original name: SEQUENTIAL-ESITO<br>
	 * <pre>*****************************************************************
	 *  COMODO X FILES SEQUENZIALI
	 * *****************************************************************
	 * *****************************************************************
	 *  STRUTTURA MESSAGGIO X FILES SEQUENZIALI
	 * *****************************************************************</pre>*/
    private SequentialEsito sequentialEsito = new SequentialEsito();
    //Original name: SEQUENTIAL-DESC-ERRORE-ESTESA
    private String sequentialDescErroreEstesa = DefaultValues.stringVal(Len.SEQUENTIAL_DESC_ERRORE_ESTESA);
    //Original name: MSG-ERR-FILE
    private MsgErrFile msgErrFile = new MsgErrFile();
    //Original name: FILE-STATUS
    private FileStatus1 fileStatus = new FileStatus1();

    //==== METHODS ====
    public void setFsFilesqs1(String fsFilesqs1) {
        this.fsFilesqs1 = Functions.subString(fsFilesqs1, Len.FS_FILESQS1);
    }

    public String getFsFilesqs1() {
        return this.fsFilesqs1;
    }

    public void setFsFilesqs2(String fsFilesqs2) {
        this.fsFilesqs2 = Functions.subString(fsFilesqs2, Len.FS_FILESQS2);
    }

    public String getFsFilesqs2() {
        return this.fsFilesqs2;
    }

    public void setFsFilesqs3(String fsFilesqs3) {
        this.fsFilesqs3 = Functions.subString(fsFilesqs3, Len.FS_FILESQS3);
    }

    public String getFsFilesqs3() {
        return this.fsFilesqs3;
    }

    public void setFsFilesqs4(String fsFilesqs4) {
        this.fsFilesqs4 = Functions.subString(fsFilesqs4, Len.FS_FILESQS4);
    }

    public String getFsFilesqs4() {
        return this.fsFilesqs4;
    }

    public void setFsFilesqs5(String fsFilesqs5) {
        this.fsFilesqs5 = Functions.subString(fsFilesqs5, Len.FS_FILESQS5);
    }

    public String getFsFilesqs5() {
        return this.fsFilesqs5;
    }

    public void setFsFilesqs6(String fsFilesqs6) {
        this.fsFilesqs6 = Functions.subString(fsFilesqs6, Len.FS_FILESQS6);
    }

    public String getFsFilesqs6() {
        return this.fsFilesqs6;
    }

    public String getNomeFilesqs1() {
        return this.nomeFilesqs1;
    }

    public String getNomeFilesqs2() {
        return this.nomeFilesqs2;
    }

    public String getNomeFilesqs3() {
        return this.nomeFilesqs3;
    }

    public String getNomeFilesqs4() {
        return this.nomeFilesqs4;
    }

    public String getNomeFilesqs5() {
        return this.nomeFilesqs5;
    }

    public String getNomeFilesqs6() {
        return this.nomeFilesqs6;
    }

    public void setSequentialDescErroreEstesa(String sequentialDescErroreEstesa) {
        this.sequentialDescErroreEstesa = Functions.subString(sequentialDescErroreEstesa, Len.SEQUENTIAL_DESC_ERRORE_ESTESA);
    }

    public String getSequentialDescErroreEstesa() {
        return this.sequentialDescErroreEstesa;
    }

    public FileStatus1 getFileStatus() {
        return fileStatus;
    }

    public MsgErrFile getMsgErrFile() {
        return msgErrFile;
    }

    public SequentialEsito getSequentialEsito() {
        return sequentialEsito;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int SEQUENTIAL_DESC_ERRORE_ESTESA = 200;
        public static final int FS_FILESQS1 = 2;
        public static final int FS_FILESQS2 = 2;
        public static final int FS_FILESQS3 = 2;
        public static final int FS_FILESQS4 = 2;
        public static final int FS_FILESQS5 = 2;
        public static final int FS_FILESQS6 = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
