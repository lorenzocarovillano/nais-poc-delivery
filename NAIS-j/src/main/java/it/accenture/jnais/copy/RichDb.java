package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: RICH-DB<br>
 * Variable: RICH-DB from copybook IDBVRIC3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class RichDb {

    //==== PROPERTIES ====
    //Original name: RIC-DT-EFF-DB
    private String effDb = DefaultValues.stringVal(Len.EFF_DB);
    //Original name: RIC-DT-RGSTRZ-RICH-DB
    private String rgstrzRichDb = DefaultValues.stringVal(Len.RGSTRZ_RICH_DB);
    //Original name: RIC-DT-PERV-RICH-DB
    private String pervRichDb = DefaultValues.stringVal(Len.PERV_RICH_DB);
    //Original name: RIC-DT-ESEC-RICH-DB
    private String esecRichDb = DefaultValues.stringVal(Len.ESEC_RICH_DB);

    //==== METHODS ====
    public void setEffDb(String effDb) {
        this.effDb = Functions.subString(effDb, Len.EFF_DB);
    }

    public String getEffDb() {
        return this.effDb;
    }

    public void setRgstrzRichDb(String rgstrzRichDb) {
        this.rgstrzRichDb = Functions.subString(rgstrzRichDb, Len.RGSTRZ_RICH_DB);
    }

    public String getRgstrzRichDb() {
        return this.rgstrzRichDb;
    }

    public void setPervRichDb(String pervRichDb) {
        this.pervRichDb = Functions.subString(pervRichDb, Len.PERV_RICH_DB);
    }

    public String getPervRichDb() {
        return this.pervRichDb;
    }

    public void setEsecRichDb(String esecRichDb) {
        this.esecRichDb = Functions.subString(esecRichDb, Len.ESEC_RICH_DB);
    }

    public String getEsecRichDb() {
        return this.esecRichDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int EFF_DB = 10;
        public static final int RGSTRZ_RICH_DB = 10;
        public static final int PERV_RICH_DB = 10;
        public static final int ESEC_RICH_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
