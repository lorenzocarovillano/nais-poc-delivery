package it.accenture.jnais.copy;

import it.accenture.jnais.ws.enums.Ispv0000CodRamo;
import it.accenture.jnais.ws.enums.Ispv0000PeriodoPrem;
import it.accenture.jnais.ws.enums.Ispv0000TpPoli;

/**Original name: ISPV0000<br>
 * Variable: ISPV0000 from copybook ISPV0000<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ispv0000 {

    //==== PROPERTIES ====
    /**Original name: ISPV0000-TP-POLI<br>
	 * <pre>    MODIFICA SIR CQPrd00019650
	 * ----------------------------------------------------------------*
	 *    PORTAFOGLIO VITA
	 *    La copy contiene i valori di dominio di actuator
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     Dominio tipo polizza
	 * ----------------------------------------------------------------*</pre>*/
    private Ispv0000TpPoli tpPoli = new Ispv0000TpPoli();
    /**Original name: ISPV0000-PERIODO-PREM<br>
	 * <pre>----------------------------------------------------------------*
	 *     Dominio tipo periodo premio
	 * ----------------------------------------------------------------*</pre>*/
    private Ispv0000PeriodoPrem periodoPrem = new Ispv0000PeriodoPrem();
    /**Original name: ISPV0000-COD-RAMO<br>
	 * <pre>----------------------------------------------------------------*
	 *     Ramo gestionale (ACT_VITA.LINEE)
	 * ----------------------------------------------------------------*</pre>*/
    private Ispv0000CodRamo codRamo = new Ispv0000CodRamo();

    //==== METHODS ====
    public Ispv0000CodRamo getCodRamo() {
        return codRamo;
    }

    public Ispv0000PeriodoPrem getPeriodoPrem() {
        return periodoPrem;
    }

    public Ispv0000TpPoli getTpPoli() {
        return tpPoli;
    }
}
