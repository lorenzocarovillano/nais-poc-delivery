package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Wp61CptIni30062014;
import it.accenture.jnais.ws.redefines.Wp61CptRivto31122011;
import it.accenture.jnais.ws.redefines.Wp61IdAdes;
import it.accenture.jnais.ws.redefines.Wp61IdMoviChiu;
import it.accenture.jnais.ws.redefines.Wp61IdTrchDiGar;
import it.accenture.jnais.ws.redefines.Wp61ImpbIs30062014;
import it.accenture.jnais.ws.redefines.Wp61ImpbIs31122011;
import it.accenture.jnais.ws.redefines.Wp61ImpbIsRpP2011;
import it.accenture.jnais.ws.redefines.Wp61ImpbIsRpP62014;
import it.accenture.jnais.ws.redefines.Wp61ImpbVis30062014;
import it.accenture.jnais.ws.redefines.Wp61ImpbVis31122011;
import it.accenture.jnais.ws.redefines.Wp61ImpbVisRpP2011;
import it.accenture.jnais.ws.redefines.Wp61ImpbVisRpP62014;
import it.accenture.jnais.ws.redefines.Wp61MontLrdDal2007;
import it.accenture.jnais.ws.redefines.Wp61MontLrdEnd2000;
import it.accenture.jnais.ws.redefines.Wp61MontLrdEnd2006;
import it.accenture.jnais.ws.redefines.Wp61PreLrdDal2007;
import it.accenture.jnais.ws.redefines.Wp61PreLrdEnd2000;
import it.accenture.jnais.ws.redefines.Wp61PreLrdEnd2006;
import it.accenture.jnais.ws.redefines.Wp61PreRshV30062014;
import it.accenture.jnais.ws.redefines.Wp61PreRshV31122011;
import it.accenture.jnais.ws.redefines.Wp61PreV30062014;
import it.accenture.jnais.ws.redefines.Wp61PreV31122011;
import it.accenture.jnais.ws.redefines.Wp61RendtoLrdDal2007;
import it.accenture.jnais.ws.redefines.Wp61RendtoLrdEnd2000;
import it.accenture.jnais.ws.redefines.Wp61RendtoLrdEnd2006;
import it.accenture.jnais.ws.redefines.Wp61RisMat30062014;
import it.accenture.jnais.ws.redefines.Wp61RisMat31122011;

/**Original name: WP61-DATI<br>
 * Variable: WP61-DATI from copybook LCCVP611<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wp61Dati {

    //==== PROPERTIES ====
    //Original name: WP61-ID-D-CRIST
    private int wp61IdDCrist = DefaultValues.INT_VAL;
    //Original name: WP61-ID-POLI
    private int wp61IdPoli = DefaultValues.INT_VAL;
    //Original name: WP61-COD-COMP-ANIA
    private int wp61CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WP61-ID-MOVI-CRZ
    private int wp61IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WP61-ID-MOVI-CHIU
    private Wp61IdMoviChiu wp61IdMoviChiu = new Wp61IdMoviChiu();
    //Original name: WP61-DT-INI-EFF
    private int wp61DtIniEff = DefaultValues.INT_VAL;
    //Original name: WP61-DT-END-EFF
    private int wp61DtEndEff = DefaultValues.INT_VAL;
    //Original name: WP61-COD-PROD
    private String wp61CodProd = DefaultValues.stringVal(Len.WP61_COD_PROD);
    //Original name: WP61-DT-DECOR
    private int wp61DtDecor = DefaultValues.INT_VAL;
    //Original name: WP61-DS-RIGA
    private long wp61DsRiga = DefaultValues.LONG_VAL;
    //Original name: WP61-DS-OPER-SQL
    private char wp61DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WP61-DS-VER
    private int wp61DsVer = DefaultValues.INT_VAL;
    //Original name: WP61-DS-TS-INI-CPTZ
    private long wp61DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WP61-DS-TS-END-CPTZ
    private long wp61DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WP61-DS-UTENTE
    private String wp61DsUtente = DefaultValues.stringVal(Len.WP61_DS_UTENTE);
    //Original name: WP61-DS-STATO-ELAB
    private char wp61DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WP61-RIS-MAT-31122011
    private Wp61RisMat31122011 wp61RisMat31122011 = new Wp61RisMat31122011();
    //Original name: WP61-PRE-V-31122011
    private Wp61PreV31122011 wp61PreV31122011 = new Wp61PreV31122011();
    //Original name: WP61-PRE-RSH-V-31122011
    private Wp61PreRshV31122011 wp61PreRshV31122011 = new Wp61PreRshV31122011();
    //Original name: WP61-CPT-RIVTO-31122011
    private Wp61CptRivto31122011 wp61CptRivto31122011 = new Wp61CptRivto31122011();
    //Original name: WP61-IMPB-VIS-31122011
    private Wp61ImpbVis31122011 wp61ImpbVis31122011 = new Wp61ImpbVis31122011();
    //Original name: WP61-IMPB-IS-31122011
    private Wp61ImpbIs31122011 wp61ImpbIs31122011 = new Wp61ImpbIs31122011();
    //Original name: WP61-IMPB-VIS-RP-P2011
    private Wp61ImpbVisRpP2011 wp61ImpbVisRpP2011 = new Wp61ImpbVisRpP2011();
    //Original name: WP61-IMPB-IS-RP-P2011
    private Wp61ImpbIsRpP2011 wp61ImpbIsRpP2011 = new Wp61ImpbIsRpP2011();
    //Original name: WP61-PRE-V-30062014
    private Wp61PreV30062014 wp61PreV30062014 = new Wp61PreV30062014();
    //Original name: WP61-PRE-RSH-V-30062014
    private Wp61PreRshV30062014 wp61PreRshV30062014 = new Wp61PreRshV30062014();
    //Original name: WP61-CPT-INI-30062014
    private Wp61CptIni30062014 wp61CptIni30062014 = new Wp61CptIni30062014();
    //Original name: WP61-IMPB-VIS-30062014
    private Wp61ImpbVis30062014 wp61ImpbVis30062014 = new Wp61ImpbVis30062014();
    //Original name: WP61-IMPB-IS-30062014
    private Wp61ImpbIs30062014 wp61ImpbIs30062014 = new Wp61ImpbIs30062014();
    //Original name: WP61-IMPB-VIS-RP-P62014
    private Wp61ImpbVisRpP62014 wp61ImpbVisRpP62014 = new Wp61ImpbVisRpP62014();
    //Original name: WP61-IMPB-IS-RP-P62014
    private Wp61ImpbIsRpP62014 wp61ImpbIsRpP62014 = new Wp61ImpbIsRpP62014();
    //Original name: WP61-RIS-MAT-30062014
    private Wp61RisMat30062014 wp61RisMat30062014 = new Wp61RisMat30062014();
    //Original name: WP61-ID-ADES
    private Wp61IdAdes wp61IdAdes = new Wp61IdAdes();
    //Original name: WP61-MONT-LRD-END2000
    private Wp61MontLrdEnd2000 wp61MontLrdEnd2000 = new Wp61MontLrdEnd2000();
    //Original name: WP61-PRE-LRD-END2000
    private Wp61PreLrdEnd2000 wp61PreLrdEnd2000 = new Wp61PreLrdEnd2000();
    //Original name: WP61-RENDTO-LRD-END2000
    private Wp61RendtoLrdEnd2000 wp61RendtoLrdEnd2000 = new Wp61RendtoLrdEnd2000();
    //Original name: WP61-MONT-LRD-END2006
    private Wp61MontLrdEnd2006 wp61MontLrdEnd2006 = new Wp61MontLrdEnd2006();
    //Original name: WP61-PRE-LRD-END2006
    private Wp61PreLrdEnd2006 wp61PreLrdEnd2006 = new Wp61PreLrdEnd2006();
    //Original name: WP61-RENDTO-LRD-END2006
    private Wp61RendtoLrdEnd2006 wp61RendtoLrdEnd2006 = new Wp61RendtoLrdEnd2006();
    //Original name: WP61-MONT-LRD-DAL2007
    private Wp61MontLrdDal2007 wp61MontLrdDal2007 = new Wp61MontLrdDal2007();
    //Original name: WP61-PRE-LRD-DAL2007
    private Wp61PreLrdDal2007 wp61PreLrdDal2007 = new Wp61PreLrdDal2007();
    //Original name: WP61-RENDTO-LRD-DAL2007
    private Wp61RendtoLrdDal2007 wp61RendtoLrdDal2007 = new Wp61RendtoLrdDal2007();
    //Original name: WP61-ID-TRCH-DI-GAR
    private Wp61IdTrchDiGar wp61IdTrchDiGar = new Wp61IdTrchDiGar();

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wp61IdDCrist = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP61_ID_D_CRIST, 0);
        position += Len.WP61_ID_D_CRIST;
        wp61IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP61_ID_POLI, 0);
        position += Len.WP61_ID_POLI;
        wp61CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP61_COD_COMP_ANIA, 0);
        position += Len.WP61_COD_COMP_ANIA;
        wp61IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP61_ID_MOVI_CRZ, 0);
        position += Len.WP61_ID_MOVI_CRZ;
        wp61IdMoviChiu.setWp61IdMoviChiuFromBuffer(buffer, position);
        position += Wp61IdMoviChiu.Len.WP61_ID_MOVI_CHIU;
        wp61DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP61_DT_INI_EFF, 0);
        position += Len.WP61_DT_INI_EFF;
        wp61DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP61_DT_END_EFF, 0);
        position += Len.WP61_DT_END_EFF;
        wp61CodProd = MarshalByte.readString(buffer, position, Len.WP61_COD_PROD);
        position += Len.WP61_COD_PROD;
        wp61DtDecor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP61_DT_DECOR, 0);
        position += Len.WP61_DT_DECOR;
        wp61DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WP61_DS_RIGA, 0);
        position += Len.WP61_DS_RIGA;
        wp61DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wp61DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP61_DS_VER, 0);
        position += Len.WP61_DS_VER;
        wp61DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WP61_DS_TS_INI_CPTZ, 0);
        position += Len.WP61_DS_TS_INI_CPTZ;
        wp61DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WP61_DS_TS_END_CPTZ, 0);
        position += Len.WP61_DS_TS_END_CPTZ;
        wp61DsUtente = MarshalByte.readString(buffer, position, Len.WP61_DS_UTENTE);
        position += Len.WP61_DS_UTENTE;
        wp61DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wp61RisMat31122011.setWp61RisMat31122011FromBuffer(buffer, position);
        position += Wp61RisMat31122011.Len.WP61_RIS_MAT31122011;
        wp61PreV31122011.setWp61PreV31122011FromBuffer(buffer, position);
        position += Wp61PreV31122011.Len.WP61_PRE_V31122011;
        wp61PreRshV31122011.setWp61PreRshV31122011FromBuffer(buffer, position);
        position += Wp61PreRshV31122011.Len.WP61_PRE_RSH_V31122011;
        wp61CptRivto31122011.setWp61CptRivto31122011FromBuffer(buffer, position);
        position += Wp61CptRivto31122011.Len.WP61_CPT_RIVTO31122011;
        wp61ImpbVis31122011.setWp61ImpbVis31122011FromBuffer(buffer, position);
        position += Wp61ImpbVis31122011.Len.WP61_IMPB_VIS31122011;
        wp61ImpbIs31122011.setWp61ImpbIs31122011FromBuffer(buffer, position);
        position += Wp61ImpbIs31122011.Len.WP61_IMPB_IS31122011;
        wp61ImpbVisRpP2011.setWp61ImpbVisRpP2011FromBuffer(buffer, position);
        position += Wp61ImpbVisRpP2011.Len.WP61_IMPB_VIS_RP_P2011;
        wp61ImpbIsRpP2011.setWp61ImpbIsRpP2011FromBuffer(buffer, position);
        position += Wp61ImpbIsRpP2011.Len.WP61_IMPB_IS_RP_P2011;
        wp61PreV30062014.setWp61PreV30062014FromBuffer(buffer, position);
        position += Wp61PreV30062014.Len.WP61_PRE_V30062014;
        wp61PreRshV30062014.setWp61PreRshV30062014FromBuffer(buffer, position);
        position += Wp61PreRshV30062014.Len.WP61_PRE_RSH_V30062014;
        wp61CptIni30062014.setWp61CptIni30062014FromBuffer(buffer, position);
        position += Wp61CptIni30062014.Len.WP61_CPT_INI30062014;
        wp61ImpbVis30062014.setWp61ImpbVis30062014FromBuffer(buffer, position);
        position += Wp61ImpbVis30062014.Len.WP61_IMPB_VIS30062014;
        wp61ImpbIs30062014.setWp61ImpbIs30062014FromBuffer(buffer, position);
        position += Wp61ImpbIs30062014.Len.WP61_IMPB_IS30062014;
        wp61ImpbVisRpP62014.setWp61ImpbVisRpP62014FromBuffer(buffer, position);
        position += Wp61ImpbVisRpP62014.Len.WP61_IMPB_VIS_RP_P62014;
        wp61ImpbIsRpP62014.setWp61ImpbIsRpP62014FromBuffer(buffer, position);
        position += Wp61ImpbIsRpP62014.Len.WP61_IMPB_IS_RP_P62014;
        wp61RisMat30062014.setWp61RisMat30062014FromBuffer(buffer, position);
        position += Wp61RisMat30062014.Len.WP61_RIS_MAT30062014;
        wp61IdAdes.setWp61IdAdesFromBuffer(buffer, position);
        position += Wp61IdAdes.Len.WP61_ID_ADES;
        wp61MontLrdEnd2000.setWp61MontLrdEnd2000FromBuffer(buffer, position);
        position += Wp61MontLrdEnd2000.Len.WP61_MONT_LRD_END2000;
        wp61PreLrdEnd2000.setWp61PreLrdEnd2000FromBuffer(buffer, position);
        position += Wp61PreLrdEnd2000.Len.WP61_PRE_LRD_END2000;
        wp61RendtoLrdEnd2000.setWp61RendtoLrdEnd2000FromBuffer(buffer, position);
        position += Wp61RendtoLrdEnd2000.Len.WP61_RENDTO_LRD_END2000;
        wp61MontLrdEnd2006.setWp61MontLrdEnd2006FromBuffer(buffer, position);
        position += Wp61MontLrdEnd2006.Len.WP61_MONT_LRD_END2006;
        wp61PreLrdEnd2006.setWp61PreLrdEnd2006FromBuffer(buffer, position);
        position += Wp61PreLrdEnd2006.Len.WP61_PRE_LRD_END2006;
        wp61RendtoLrdEnd2006.setWp61RendtoLrdEnd2006FromBuffer(buffer, position);
        position += Wp61RendtoLrdEnd2006.Len.WP61_RENDTO_LRD_END2006;
        wp61MontLrdDal2007.setWp61MontLrdDal2007FromBuffer(buffer, position);
        position += Wp61MontLrdDal2007.Len.WP61_MONT_LRD_DAL2007;
        wp61PreLrdDal2007.setWp61PreLrdDal2007FromBuffer(buffer, position);
        position += Wp61PreLrdDal2007.Len.WP61_PRE_LRD_DAL2007;
        wp61RendtoLrdDal2007.setWp61RendtoLrdDal2007FromBuffer(buffer, position);
        position += Wp61RendtoLrdDal2007.Len.WP61_RENDTO_LRD_DAL2007;
        wp61IdTrchDiGar.setWp61IdTrchDiGarFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wp61IdDCrist, Len.Int.WP61_ID_D_CRIST, 0);
        position += Len.WP61_ID_D_CRIST;
        MarshalByte.writeIntAsPacked(buffer, position, wp61IdPoli, Len.Int.WP61_ID_POLI, 0);
        position += Len.WP61_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wp61CodCompAnia, Len.Int.WP61_COD_COMP_ANIA, 0);
        position += Len.WP61_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wp61IdMoviCrz, Len.Int.WP61_ID_MOVI_CRZ, 0);
        position += Len.WP61_ID_MOVI_CRZ;
        wp61IdMoviChiu.getWp61IdMoviChiuAsBuffer(buffer, position);
        position += Wp61IdMoviChiu.Len.WP61_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wp61DtIniEff, Len.Int.WP61_DT_INI_EFF, 0);
        position += Len.WP61_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wp61DtEndEff, Len.Int.WP61_DT_END_EFF, 0);
        position += Len.WP61_DT_END_EFF;
        MarshalByte.writeString(buffer, position, wp61CodProd, Len.WP61_COD_PROD);
        position += Len.WP61_COD_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, wp61DtDecor, Len.Int.WP61_DT_DECOR, 0);
        position += Len.WP61_DT_DECOR;
        MarshalByte.writeLongAsPacked(buffer, position, wp61DsRiga, Len.Int.WP61_DS_RIGA, 0);
        position += Len.WP61_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wp61DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wp61DsVer, Len.Int.WP61_DS_VER, 0);
        position += Len.WP61_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wp61DsTsIniCptz, Len.Int.WP61_DS_TS_INI_CPTZ, 0);
        position += Len.WP61_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wp61DsTsEndCptz, Len.Int.WP61_DS_TS_END_CPTZ, 0);
        position += Len.WP61_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wp61DsUtente, Len.WP61_DS_UTENTE);
        position += Len.WP61_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wp61DsStatoElab);
        position += Types.CHAR_SIZE;
        wp61RisMat31122011.getWp61RisMat31122011AsBuffer(buffer, position);
        position += Wp61RisMat31122011.Len.WP61_RIS_MAT31122011;
        wp61PreV31122011.getWp61PreV31122011AsBuffer(buffer, position);
        position += Wp61PreV31122011.Len.WP61_PRE_V31122011;
        wp61PreRshV31122011.getWp61PreRshV31122011AsBuffer(buffer, position);
        position += Wp61PreRshV31122011.Len.WP61_PRE_RSH_V31122011;
        wp61CptRivto31122011.getWp61CptRivto31122011AsBuffer(buffer, position);
        position += Wp61CptRivto31122011.Len.WP61_CPT_RIVTO31122011;
        wp61ImpbVis31122011.getWp61ImpbVis31122011AsBuffer(buffer, position);
        position += Wp61ImpbVis31122011.Len.WP61_IMPB_VIS31122011;
        wp61ImpbIs31122011.getWp61ImpbIs31122011AsBuffer(buffer, position);
        position += Wp61ImpbIs31122011.Len.WP61_IMPB_IS31122011;
        wp61ImpbVisRpP2011.getWp61ImpbVisRpP2011AsBuffer(buffer, position);
        position += Wp61ImpbVisRpP2011.Len.WP61_IMPB_VIS_RP_P2011;
        wp61ImpbIsRpP2011.getWp61ImpbIsRpP2011AsBuffer(buffer, position);
        position += Wp61ImpbIsRpP2011.Len.WP61_IMPB_IS_RP_P2011;
        wp61PreV30062014.getWp61PreV30062014AsBuffer(buffer, position);
        position += Wp61PreV30062014.Len.WP61_PRE_V30062014;
        wp61PreRshV30062014.getWp61PreRshV30062014AsBuffer(buffer, position);
        position += Wp61PreRshV30062014.Len.WP61_PRE_RSH_V30062014;
        wp61CptIni30062014.getWp61CptIni30062014AsBuffer(buffer, position);
        position += Wp61CptIni30062014.Len.WP61_CPT_INI30062014;
        wp61ImpbVis30062014.getWp61ImpbVis30062014AsBuffer(buffer, position);
        position += Wp61ImpbVis30062014.Len.WP61_IMPB_VIS30062014;
        wp61ImpbIs30062014.getWp61ImpbIs30062014AsBuffer(buffer, position);
        position += Wp61ImpbIs30062014.Len.WP61_IMPB_IS30062014;
        wp61ImpbVisRpP62014.getWp61ImpbVisRpP62014AsBuffer(buffer, position);
        position += Wp61ImpbVisRpP62014.Len.WP61_IMPB_VIS_RP_P62014;
        wp61ImpbIsRpP62014.getWp61ImpbIsRpP62014AsBuffer(buffer, position);
        position += Wp61ImpbIsRpP62014.Len.WP61_IMPB_IS_RP_P62014;
        wp61RisMat30062014.getWp61RisMat30062014AsBuffer(buffer, position);
        position += Wp61RisMat30062014.Len.WP61_RIS_MAT30062014;
        wp61IdAdes.getWp61IdAdesAsBuffer(buffer, position);
        position += Wp61IdAdes.Len.WP61_ID_ADES;
        wp61MontLrdEnd2000.getWp61MontLrdEnd2000AsBuffer(buffer, position);
        position += Wp61MontLrdEnd2000.Len.WP61_MONT_LRD_END2000;
        wp61PreLrdEnd2000.getWp61PreLrdEnd2000AsBuffer(buffer, position);
        position += Wp61PreLrdEnd2000.Len.WP61_PRE_LRD_END2000;
        wp61RendtoLrdEnd2000.getWp61RendtoLrdEnd2000AsBuffer(buffer, position);
        position += Wp61RendtoLrdEnd2000.Len.WP61_RENDTO_LRD_END2000;
        wp61MontLrdEnd2006.getWp61MontLrdEnd2006AsBuffer(buffer, position);
        position += Wp61MontLrdEnd2006.Len.WP61_MONT_LRD_END2006;
        wp61PreLrdEnd2006.getWp61PreLrdEnd2006AsBuffer(buffer, position);
        position += Wp61PreLrdEnd2006.Len.WP61_PRE_LRD_END2006;
        wp61RendtoLrdEnd2006.getWp61RendtoLrdEnd2006AsBuffer(buffer, position);
        position += Wp61RendtoLrdEnd2006.Len.WP61_RENDTO_LRD_END2006;
        wp61MontLrdDal2007.getWp61MontLrdDal2007AsBuffer(buffer, position);
        position += Wp61MontLrdDal2007.Len.WP61_MONT_LRD_DAL2007;
        wp61PreLrdDal2007.getWp61PreLrdDal2007AsBuffer(buffer, position);
        position += Wp61PreLrdDal2007.Len.WP61_PRE_LRD_DAL2007;
        wp61RendtoLrdDal2007.getWp61RendtoLrdDal2007AsBuffer(buffer, position);
        position += Wp61RendtoLrdDal2007.Len.WP61_RENDTO_LRD_DAL2007;
        wp61IdTrchDiGar.getWp61IdTrchDiGarAsBuffer(buffer, position);
        return buffer;
    }

    public void setWp61IdDCrist(int wp61IdDCrist) {
        this.wp61IdDCrist = wp61IdDCrist;
    }

    public int getWp61IdDCrist() {
        return this.wp61IdDCrist;
    }

    public void setWp61IdPoli(int wp61IdPoli) {
        this.wp61IdPoli = wp61IdPoli;
    }

    public int getWp61IdPoli() {
        return this.wp61IdPoli;
    }

    public void setWp61CodCompAnia(int wp61CodCompAnia) {
        this.wp61CodCompAnia = wp61CodCompAnia;
    }

    public int getWp61CodCompAnia() {
        return this.wp61CodCompAnia;
    }

    public void setWp61IdMoviCrz(int wp61IdMoviCrz) {
        this.wp61IdMoviCrz = wp61IdMoviCrz;
    }

    public int getWp61IdMoviCrz() {
        return this.wp61IdMoviCrz;
    }

    public void setWp61DtIniEff(int wp61DtIniEff) {
        this.wp61DtIniEff = wp61DtIniEff;
    }

    public int getWp61DtIniEff() {
        return this.wp61DtIniEff;
    }

    public void setWp61DtEndEff(int wp61DtEndEff) {
        this.wp61DtEndEff = wp61DtEndEff;
    }

    public int getWp61DtEndEff() {
        return this.wp61DtEndEff;
    }

    public void setWp61CodProd(String wp61CodProd) {
        this.wp61CodProd = Functions.subString(wp61CodProd, Len.WP61_COD_PROD);
    }

    public String getWp61CodProd() {
        return this.wp61CodProd;
    }

    public void setWp61DtDecor(int wp61DtDecor) {
        this.wp61DtDecor = wp61DtDecor;
    }

    public int getWp61DtDecor() {
        return this.wp61DtDecor;
    }

    public void setWp61DsRiga(long wp61DsRiga) {
        this.wp61DsRiga = wp61DsRiga;
    }

    public long getWp61DsRiga() {
        return this.wp61DsRiga;
    }

    public void setWp61DsOperSql(char wp61DsOperSql) {
        this.wp61DsOperSql = wp61DsOperSql;
    }

    public char getWp61DsOperSql() {
        return this.wp61DsOperSql;
    }

    public void setWp61DsVer(int wp61DsVer) {
        this.wp61DsVer = wp61DsVer;
    }

    public int getWp61DsVer() {
        return this.wp61DsVer;
    }

    public void setWp61DsTsIniCptz(long wp61DsTsIniCptz) {
        this.wp61DsTsIniCptz = wp61DsTsIniCptz;
    }

    public long getWp61DsTsIniCptz() {
        return this.wp61DsTsIniCptz;
    }

    public void setWp61DsTsEndCptz(long wp61DsTsEndCptz) {
        this.wp61DsTsEndCptz = wp61DsTsEndCptz;
    }

    public long getWp61DsTsEndCptz() {
        return this.wp61DsTsEndCptz;
    }

    public void setWp61DsUtente(String wp61DsUtente) {
        this.wp61DsUtente = Functions.subString(wp61DsUtente, Len.WP61_DS_UTENTE);
    }

    public String getWp61DsUtente() {
        return this.wp61DsUtente;
    }

    public void setWp61DsStatoElab(char wp61DsStatoElab) {
        this.wp61DsStatoElab = wp61DsStatoElab;
    }

    public char getWp61DsStatoElab() {
        return this.wp61DsStatoElab;
    }

    public Wp61CptIni30062014 getWp61CptIni30062014() {
        return wp61CptIni30062014;
    }

    public Wp61CptRivto31122011 getWp61CptRivto31122011() {
        return wp61CptRivto31122011;
    }

    public Wp61IdAdes getWp61IdAdes() {
        return wp61IdAdes;
    }

    public Wp61IdMoviChiu getWp61IdMoviChiu() {
        return wp61IdMoviChiu;
    }

    public Wp61IdTrchDiGar getWp61IdTrchDiGar() {
        return wp61IdTrchDiGar;
    }

    public Wp61ImpbIs30062014 getWp61ImpbIs30062014() {
        return wp61ImpbIs30062014;
    }

    public Wp61ImpbIs31122011 getWp61ImpbIs31122011() {
        return wp61ImpbIs31122011;
    }

    public Wp61ImpbIsRpP2011 getWp61ImpbIsRpP2011() {
        return wp61ImpbIsRpP2011;
    }

    public Wp61ImpbIsRpP62014 getWp61ImpbIsRpP62014() {
        return wp61ImpbIsRpP62014;
    }

    public Wp61ImpbVis30062014 getWp61ImpbVis30062014() {
        return wp61ImpbVis30062014;
    }

    public Wp61ImpbVis31122011 getWp61ImpbVis31122011() {
        return wp61ImpbVis31122011;
    }

    public Wp61ImpbVisRpP2011 getWp61ImpbVisRpP2011() {
        return wp61ImpbVisRpP2011;
    }

    public Wp61ImpbVisRpP62014 getWp61ImpbVisRpP62014() {
        return wp61ImpbVisRpP62014;
    }

    public Wp61MontLrdDal2007 getWp61MontLrdDal2007() {
        return wp61MontLrdDal2007;
    }

    public Wp61MontLrdEnd2000 getWp61MontLrdEnd2000() {
        return wp61MontLrdEnd2000;
    }

    public Wp61MontLrdEnd2006 getWp61MontLrdEnd2006() {
        return wp61MontLrdEnd2006;
    }

    public Wp61PreLrdDal2007 getWp61PreLrdDal2007() {
        return wp61PreLrdDal2007;
    }

    public Wp61PreLrdEnd2000 getWp61PreLrdEnd2000() {
        return wp61PreLrdEnd2000;
    }

    public Wp61PreLrdEnd2006 getWp61PreLrdEnd2006() {
        return wp61PreLrdEnd2006;
    }

    public Wp61PreRshV30062014 getWp61PreRshV30062014() {
        return wp61PreRshV30062014;
    }

    public Wp61PreRshV31122011 getWp61PreRshV31122011() {
        return wp61PreRshV31122011;
    }

    public Wp61PreV30062014 getWp61PreV30062014() {
        return wp61PreV30062014;
    }

    public Wp61PreV31122011 getWp61PreV31122011() {
        return wp61PreV31122011;
    }

    public Wp61RendtoLrdDal2007 getWp61RendtoLrdDal2007() {
        return wp61RendtoLrdDal2007;
    }

    public Wp61RendtoLrdEnd2000 getWp61RendtoLrdEnd2000() {
        return wp61RendtoLrdEnd2000;
    }

    public Wp61RendtoLrdEnd2006 getWp61RendtoLrdEnd2006() {
        return wp61RendtoLrdEnd2006;
    }

    public Wp61RisMat30062014 getWp61RisMat30062014() {
        return wp61RisMat30062014;
    }

    public Wp61RisMat31122011 getWp61RisMat31122011() {
        return wp61RisMat31122011;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_ID_D_CRIST = 5;
        public static final int WP61_ID_POLI = 5;
        public static final int WP61_COD_COMP_ANIA = 3;
        public static final int WP61_ID_MOVI_CRZ = 5;
        public static final int WP61_DT_INI_EFF = 5;
        public static final int WP61_DT_END_EFF = 5;
        public static final int WP61_COD_PROD = 12;
        public static final int WP61_DT_DECOR = 5;
        public static final int WP61_DS_RIGA = 6;
        public static final int WP61_DS_OPER_SQL = 1;
        public static final int WP61_DS_VER = 5;
        public static final int WP61_DS_TS_INI_CPTZ = 10;
        public static final int WP61_DS_TS_END_CPTZ = 10;
        public static final int WP61_DS_UTENTE = 20;
        public static final int WP61_DS_STATO_ELAB = 1;
        public static final int DATI = WP61_ID_D_CRIST + WP61_ID_POLI + WP61_COD_COMP_ANIA + WP61_ID_MOVI_CRZ + Wp61IdMoviChiu.Len.WP61_ID_MOVI_CHIU + WP61_DT_INI_EFF + WP61_DT_END_EFF + WP61_COD_PROD + WP61_DT_DECOR + WP61_DS_RIGA + WP61_DS_OPER_SQL + WP61_DS_VER + WP61_DS_TS_INI_CPTZ + WP61_DS_TS_END_CPTZ + WP61_DS_UTENTE + WP61_DS_STATO_ELAB + Wp61RisMat31122011.Len.WP61_RIS_MAT31122011 + Wp61PreV31122011.Len.WP61_PRE_V31122011 + Wp61PreRshV31122011.Len.WP61_PRE_RSH_V31122011 + Wp61CptRivto31122011.Len.WP61_CPT_RIVTO31122011 + Wp61ImpbVis31122011.Len.WP61_IMPB_VIS31122011 + Wp61ImpbIs31122011.Len.WP61_IMPB_IS31122011 + Wp61ImpbVisRpP2011.Len.WP61_IMPB_VIS_RP_P2011 + Wp61ImpbIsRpP2011.Len.WP61_IMPB_IS_RP_P2011 + Wp61PreV30062014.Len.WP61_PRE_V30062014 + Wp61PreRshV30062014.Len.WP61_PRE_RSH_V30062014 + Wp61CptIni30062014.Len.WP61_CPT_INI30062014 + Wp61ImpbVis30062014.Len.WP61_IMPB_VIS30062014 + Wp61ImpbIs30062014.Len.WP61_IMPB_IS30062014 + Wp61ImpbVisRpP62014.Len.WP61_IMPB_VIS_RP_P62014 + Wp61ImpbIsRpP62014.Len.WP61_IMPB_IS_RP_P62014 + Wp61RisMat30062014.Len.WP61_RIS_MAT30062014 + Wp61IdAdes.Len.WP61_ID_ADES + Wp61MontLrdEnd2000.Len.WP61_MONT_LRD_END2000 + Wp61PreLrdEnd2000.Len.WP61_PRE_LRD_END2000 + Wp61RendtoLrdEnd2000.Len.WP61_RENDTO_LRD_END2000 + Wp61MontLrdEnd2006.Len.WP61_MONT_LRD_END2006 + Wp61PreLrdEnd2006.Len.WP61_PRE_LRD_END2006 + Wp61RendtoLrdEnd2006.Len.WP61_RENDTO_LRD_END2006 + Wp61MontLrdDal2007.Len.WP61_MONT_LRD_DAL2007 + Wp61PreLrdDal2007.Len.WP61_PRE_LRD_DAL2007 + Wp61RendtoLrdDal2007.Len.WP61_RENDTO_LRD_DAL2007 + Wp61IdTrchDiGar.Len.WP61_ID_TRCH_DI_GAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_ID_D_CRIST = 9;
            public static final int WP61_ID_POLI = 9;
            public static final int WP61_COD_COMP_ANIA = 5;
            public static final int WP61_ID_MOVI_CRZ = 9;
            public static final int WP61_DT_INI_EFF = 8;
            public static final int WP61_DT_END_EFF = 8;
            public static final int WP61_DT_DECOR = 8;
            public static final int WP61_DS_RIGA = 10;
            public static final int WP61_DS_VER = 9;
            public static final int WP61_DS_TS_INI_CPTZ = 18;
            public static final int WP61_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
