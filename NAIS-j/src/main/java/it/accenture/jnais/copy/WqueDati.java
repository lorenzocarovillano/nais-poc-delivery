package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WqueIdMoviChiu;

/**Original name: WQUE-DATI<br>
 * Variable: WQUE-DATI from copybook LCCVQUE1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WqueDati {

    //==== PROPERTIES ====
    //Original name: WQUE-ID-QUEST
    private int wqueIdQuest = DefaultValues.INT_VAL;
    //Original name: WQUE-ID-RAPP-ANA
    private int wqueIdRappAna = DefaultValues.INT_VAL;
    //Original name: WQUE-ID-MOVI-CRZ
    private int wqueIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WQUE-ID-MOVI-CHIU
    private WqueIdMoviChiu wqueIdMoviChiu = new WqueIdMoviChiu();
    //Original name: WQUE-DT-INI-EFF
    private int wqueDtIniEff = DefaultValues.INT_VAL;
    //Original name: WQUE-DT-END-EFF
    private int wqueDtEndEff = DefaultValues.INT_VAL;
    //Original name: WQUE-COD-COMP-ANIA
    private int wqueCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WQUE-COD-QUEST
    private String wqueCodQuest = DefaultValues.stringVal(Len.WQUE_COD_QUEST);
    //Original name: WQUE-TP-QUEST
    private String wqueTpQuest = DefaultValues.stringVal(Len.WQUE_TP_QUEST);
    //Original name: WQUE-FL-VST-MED
    private char wqueFlVstMed = DefaultValues.CHAR_VAL;
    //Original name: WQUE-FL-STAT-BUON-SAL
    private char wqueFlStatBuonSal = DefaultValues.CHAR_VAL;
    //Original name: WQUE-DS-RIGA
    private long wqueDsRiga = DefaultValues.LONG_VAL;
    //Original name: WQUE-DS-OPER-SQL
    private char wqueDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WQUE-DS-VER
    private int wqueDsVer = DefaultValues.INT_VAL;
    //Original name: WQUE-DS-TS-INI-CPTZ
    private long wqueDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WQUE-DS-TS-END-CPTZ
    private long wqueDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WQUE-DS-UTENTE
    private String wqueDsUtente = DefaultValues.stringVal(Len.WQUE_DS_UTENTE);
    //Original name: WQUE-DS-STATO-ELAB
    private char wqueDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WQUE-TP-ADEGZ
    private String wqueTpAdegz = DefaultValues.stringVal(Len.WQUE_TP_ADEGZ);

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wqueIdQuest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WQUE_ID_QUEST, 0);
        position += Len.WQUE_ID_QUEST;
        wqueIdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WQUE_ID_RAPP_ANA, 0);
        position += Len.WQUE_ID_RAPP_ANA;
        wqueIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WQUE_ID_MOVI_CRZ, 0);
        position += Len.WQUE_ID_MOVI_CRZ;
        wqueIdMoviChiu.setWqueIdMoviChiuFromBuffer(buffer, position);
        position += WqueIdMoviChiu.Len.WQUE_ID_MOVI_CHIU;
        wqueDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WQUE_DT_INI_EFF, 0);
        position += Len.WQUE_DT_INI_EFF;
        wqueDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WQUE_DT_END_EFF, 0);
        position += Len.WQUE_DT_END_EFF;
        wqueCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WQUE_COD_COMP_ANIA, 0);
        position += Len.WQUE_COD_COMP_ANIA;
        wqueCodQuest = MarshalByte.readString(buffer, position, Len.WQUE_COD_QUEST);
        position += Len.WQUE_COD_QUEST;
        wqueTpQuest = MarshalByte.readString(buffer, position, Len.WQUE_TP_QUEST);
        position += Len.WQUE_TP_QUEST;
        wqueFlVstMed = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wqueFlStatBuonSal = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wqueDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WQUE_DS_RIGA, 0);
        position += Len.WQUE_DS_RIGA;
        wqueDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wqueDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WQUE_DS_VER, 0);
        position += Len.WQUE_DS_VER;
        wqueDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WQUE_DS_TS_INI_CPTZ, 0);
        position += Len.WQUE_DS_TS_INI_CPTZ;
        wqueDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WQUE_DS_TS_END_CPTZ, 0);
        position += Len.WQUE_DS_TS_END_CPTZ;
        wqueDsUtente = MarshalByte.readString(buffer, position, Len.WQUE_DS_UTENTE);
        position += Len.WQUE_DS_UTENTE;
        wqueDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wqueTpAdegz = MarshalByte.readString(buffer, position, Len.WQUE_TP_ADEGZ);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wqueIdQuest, Len.Int.WQUE_ID_QUEST, 0);
        position += Len.WQUE_ID_QUEST;
        MarshalByte.writeIntAsPacked(buffer, position, wqueIdRappAna, Len.Int.WQUE_ID_RAPP_ANA, 0);
        position += Len.WQUE_ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, wqueIdMoviCrz, Len.Int.WQUE_ID_MOVI_CRZ, 0);
        position += Len.WQUE_ID_MOVI_CRZ;
        wqueIdMoviChiu.getWqueIdMoviChiuAsBuffer(buffer, position);
        position += WqueIdMoviChiu.Len.WQUE_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wqueDtIniEff, Len.Int.WQUE_DT_INI_EFF, 0);
        position += Len.WQUE_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wqueDtEndEff, Len.Int.WQUE_DT_END_EFF, 0);
        position += Len.WQUE_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wqueCodCompAnia, Len.Int.WQUE_COD_COMP_ANIA, 0);
        position += Len.WQUE_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wqueCodQuest, Len.WQUE_COD_QUEST);
        position += Len.WQUE_COD_QUEST;
        MarshalByte.writeString(buffer, position, wqueTpQuest, Len.WQUE_TP_QUEST);
        position += Len.WQUE_TP_QUEST;
        MarshalByte.writeChar(buffer, position, wqueFlVstMed);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wqueFlStatBuonSal);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, wqueDsRiga, Len.Int.WQUE_DS_RIGA, 0);
        position += Len.WQUE_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wqueDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wqueDsVer, Len.Int.WQUE_DS_VER, 0);
        position += Len.WQUE_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wqueDsTsIniCptz, Len.Int.WQUE_DS_TS_INI_CPTZ, 0);
        position += Len.WQUE_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wqueDsTsEndCptz, Len.Int.WQUE_DS_TS_END_CPTZ, 0);
        position += Len.WQUE_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wqueDsUtente, Len.WQUE_DS_UTENTE);
        position += Len.WQUE_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wqueDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wqueTpAdegz, Len.WQUE_TP_ADEGZ);
        return buffer;
    }

    public void initDatiSpaces() {
        wqueIdQuest = Types.INVALID_INT_VAL;
        wqueIdRappAna = Types.INVALID_INT_VAL;
        wqueIdMoviCrz = Types.INVALID_INT_VAL;
        wqueIdMoviChiu.initWqueIdMoviChiuSpaces();
        wqueDtIniEff = Types.INVALID_INT_VAL;
        wqueDtEndEff = Types.INVALID_INT_VAL;
        wqueCodCompAnia = Types.INVALID_INT_VAL;
        wqueCodQuest = "";
        wqueTpQuest = "";
        wqueFlVstMed = Types.SPACE_CHAR;
        wqueFlStatBuonSal = Types.SPACE_CHAR;
        wqueDsRiga = Types.INVALID_LONG_VAL;
        wqueDsOperSql = Types.SPACE_CHAR;
        wqueDsVer = Types.INVALID_INT_VAL;
        wqueDsTsIniCptz = Types.INVALID_LONG_VAL;
        wqueDsTsEndCptz = Types.INVALID_LONG_VAL;
        wqueDsUtente = "";
        wqueDsStatoElab = Types.SPACE_CHAR;
        wqueTpAdegz = "";
    }

    public void setWqueIdQuest(int wqueIdQuest) {
        this.wqueIdQuest = wqueIdQuest;
    }

    public int getWqueIdQuest() {
        return this.wqueIdQuest;
    }

    public void setWqueIdRappAna(int wqueIdRappAna) {
        this.wqueIdRappAna = wqueIdRappAna;
    }

    public int getWqueIdRappAna() {
        return this.wqueIdRappAna;
    }

    public void setWqueIdMoviCrz(int wqueIdMoviCrz) {
        this.wqueIdMoviCrz = wqueIdMoviCrz;
    }

    public int getWqueIdMoviCrz() {
        return this.wqueIdMoviCrz;
    }

    public void setWqueDtIniEff(int wqueDtIniEff) {
        this.wqueDtIniEff = wqueDtIniEff;
    }

    public int getWqueDtIniEff() {
        return this.wqueDtIniEff;
    }

    public void setWqueDtEndEff(int wqueDtEndEff) {
        this.wqueDtEndEff = wqueDtEndEff;
    }

    public int getWqueDtEndEff() {
        return this.wqueDtEndEff;
    }

    public void setWqueCodCompAnia(int wqueCodCompAnia) {
        this.wqueCodCompAnia = wqueCodCompAnia;
    }

    public int getWqueCodCompAnia() {
        return this.wqueCodCompAnia;
    }

    public void setWqueCodQuest(String wqueCodQuest) {
        this.wqueCodQuest = Functions.subString(wqueCodQuest, Len.WQUE_COD_QUEST);
    }

    public String getWqueCodQuest() {
        return this.wqueCodQuest;
    }

    public void setWqueTpQuest(String wqueTpQuest) {
        this.wqueTpQuest = Functions.subString(wqueTpQuest, Len.WQUE_TP_QUEST);
    }

    public String getWqueTpQuest() {
        return this.wqueTpQuest;
    }

    public void setWqueFlVstMed(char wqueFlVstMed) {
        this.wqueFlVstMed = wqueFlVstMed;
    }

    public char getWqueFlVstMed() {
        return this.wqueFlVstMed;
    }

    public void setWqueFlStatBuonSal(char wqueFlStatBuonSal) {
        this.wqueFlStatBuonSal = wqueFlStatBuonSal;
    }

    public char getWqueFlStatBuonSal() {
        return this.wqueFlStatBuonSal;
    }

    public void setWqueDsRiga(long wqueDsRiga) {
        this.wqueDsRiga = wqueDsRiga;
    }

    public long getWqueDsRiga() {
        return this.wqueDsRiga;
    }

    public void setWqueDsOperSql(char wqueDsOperSql) {
        this.wqueDsOperSql = wqueDsOperSql;
    }

    public char getWqueDsOperSql() {
        return this.wqueDsOperSql;
    }

    public void setWqueDsVer(int wqueDsVer) {
        this.wqueDsVer = wqueDsVer;
    }

    public int getWqueDsVer() {
        return this.wqueDsVer;
    }

    public void setWqueDsTsIniCptz(long wqueDsTsIniCptz) {
        this.wqueDsTsIniCptz = wqueDsTsIniCptz;
    }

    public long getWqueDsTsIniCptz() {
        return this.wqueDsTsIniCptz;
    }

    public void setWqueDsTsEndCptz(long wqueDsTsEndCptz) {
        this.wqueDsTsEndCptz = wqueDsTsEndCptz;
    }

    public long getWqueDsTsEndCptz() {
        return this.wqueDsTsEndCptz;
    }

    public void setWqueDsUtente(String wqueDsUtente) {
        this.wqueDsUtente = Functions.subString(wqueDsUtente, Len.WQUE_DS_UTENTE);
    }

    public String getWqueDsUtente() {
        return this.wqueDsUtente;
    }

    public void setWqueDsStatoElab(char wqueDsStatoElab) {
        this.wqueDsStatoElab = wqueDsStatoElab;
    }

    public char getWqueDsStatoElab() {
        return this.wqueDsStatoElab;
    }

    public void setWqueTpAdegz(String wqueTpAdegz) {
        this.wqueTpAdegz = Functions.subString(wqueTpAdegz, Len.WQUE_TP_ADEGZ);
    }

    public String getWqueTpAdegz() {
        return this.wqueTpAdegz;
    }

    public WqueIdMoviChiu getWqueIdMoviChiu() {
        return wqueIdMoviChiu;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WQUE_ID_QUEST = 5;
        public static final int WQUE_ID_RAPP_ANA = 5;
        public static final int WQUE_ID_MOVI_CRZ = 5;
        public static final int WQUE_DT_INI_EFF = 5;
        public static final int WQUE_DT_END_EFF = 5;
        public static final int WQUE_COD_COMP_ANIA = 3;
        public static final int WQUE_COD_QUEST = 20;
        public static final int WQUE_TP_QUEST = 20;
        public static final int WQUE_FL_VST_MED = 1;
        public static final int WQUE_FL_STAT_BUON_SAL = 1;
        public static final int WQUE_DS_RIGA = 6;
        public static final int WQUE_DS_OPER_SQL = 1;
        public static final int WQUE_DS_VER = 5;
        public static final int WQUE_DS_TS_INI_CPTZ = 10;
        public static final int WQUE_DS_TS_END_CPTZ = 10;
        public static final int WQUE_DS_UTENTE = 20;
        public static final int WQUE_DS_STATO_ELAB = 1;
        public static final int WQUE_TP_ADEGZ = 2;
        public static final int DATI = WQUE_ID_QUEST + WQUE_ID_RAPP_ANA + WQUE_ID_MOVI_CRZ + WqueIdMoviChiu.Len.WQUE_ID_MOVI_CHIU + WQUE_DT_INI_EFF + WQUE_DT_END_EFF + WQUE_COD_COMP_ANIA + WQUE_COD_QUEST + WQUE_TP_QUEST + WQUE_FL_VST_MED + WQUE_FL_STAT_BUON_SAL + WQUE_DS_RIGA + WQUE_DS_OPER_SQL + WQUE_DS_VER + WQUE_DS_TS_INI_CPTZ + WQUE_DS_TS_END_CPTZ + WQUE_DS_UTENTE + WQUE_DS_STATO_ELAB + WQUE_TP_ADEGZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WQUE_ID_QUEST = 9;
            public static final int WQUE_ID_RAPP_ANA = 9;
            public static final int WQUE_ID_MOVI_CRZ = 9;
            public static final int WQUE_DT_INI_EFF = 8;
            public static final int WQUE_DT_END_EFF = 8;
            public static final int WQUE_COD_COMP_ANIA = 5;
            public static final int WQUE_DS_RIGA = 10;
            public static final int WQUE_DS_VER = 9;
            public static final int WQUE_DS_TS_INI_CPTZ = 18;
            public static final int WQUE_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
