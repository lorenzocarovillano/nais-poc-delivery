package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IABV0010<br>
 * Variable: IABV0010 from copybook IABV0010<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Iabv0010 {

    //==== PROPERTIES ====
    //Original name: FILLER-LOGO-JCL-REPORT
    private String flr1 = "*";
    //Original name: FILLER-LOGO-JCL-REPORT-1
    private String flr2 = "R E P O R T   D I   E L A B O R A Z I O N E   B A T C H";
    //Original name: DATA-ELABORAZIONE-REPORT
    private DataElaborazioneReport dataElaborazioneReport = new DataElaborazioneReport();
    //Original name: ORA-ELABORAZIONE-REPORT
    private OraElaborazioneReport oraElaborazioneReport = new OraElaborazioneReport();
    //Original name: FILLER-COD-COMPAGNIA-REPORT
    private String flr3 = "* CODICE COMPAGNIA     :";
    //Original name: REP-COD-COMPAGNIA
    private String repCodCompagnia = "00000";
    //Original name: FILLER-DESC-BATCH-REPORT
    private String flr4 = "* DESCRIZIONE BATCH    :";
    //Original name: REP-DESC-BATCH
    private String repDescBatch = "";
    //Original name: FILLER-USER-NAME-REPORT
    private String flr5 = "* USER NAME            :";
    //Original name: REP-USER-NAME
    private String repUserName = "";
    //Original name: FILLER-GRUPPO-AUT-REPORT
    private String flr6 = "* GRUPPO AUTORIZZANTE  :";
    //Original name: REP-GRUPPO-AUT
    private String repGruppoAut = "0000000000";
    //Original name: FILLER-MACROFUNZIONALITA-REPORT
    private String flr7 = "* MACROFUNZIONALITA    :";
    //Original name: REP-MACROFUNZIONALITA
    private String repMacrofunzionalita = "";
    //Original name: FILLER-TIPO-MOVIMENTO-REPORT
    private String flr8 = "* TIPO MOVIMENTO       :";
    //Original name: REP-TIPO-MOVIMENTO
    private String repTipoMovimento = "00000";
    //Original name: FILLER-DATA-EFFETTO-REPORT
    private String flr9 = "* DATA EFFETTO         :";
    //Original name: REP-DATA-EFFETTO
    private String repDataEffetto = "";
    //Original name: FILLER-TIMESTAMP-COMPETENZA-REPORT
    private String flr10 = "* TIMESTAMP COMPETENZA :";
    //Original name: REP-TIMESTAMP-COMPETENZA
    private String repTimestampCompetenza = "";
    //Original name: FILLER-SIMULAZIONE-REPORT
    private String flr11 = "* SIMULAZIONE          :";
    //Original name: REP-SIMULAZIONE
    private char repSimulazione = Types.SPACE_CHAR;
    //Original name: FILLER-CANALE-VENDITA-REPORT
    private String flr12 = "* CANALE VENDITA       :";
    //Original name: REP-CANALE-VENDITA
    private String repCanaleVendita = "00000";
    //Original name: FILLER-PUNTO-VENDITA-REPORT
    private String flr13 = "* PUNTO VENDITA        :";
    //Original name: REP-PUNTO-VENDITA
    private String repPuntoVendita = "00000";
    /**Original name: LOGO-BATCH-REPORT<br>
	 * <pre>*****************************************************************
	 *  TESTATA BATCH
	 * *****************************************************************</pre>*/
    private String logoBatchReport = "* TESTATA BATCH";
    //Original name: FILLER-ID-BATCH-REPORT
    private String flr14 = "IDENTIFICATIVO BATCH       :";
    //Original name: REP-ID-BATCH
    private String repIdBatch = "000000000";
    //Original name: FILLER-PROTOCOL-REPORT
    private String flr15 = "PROTOCOLLO                 :";
    //Original name: REP-PROTOCOL
    private String repProtocol = "";
    //Original name: FILLER-PROGR-PROTOCOL-REPORT
    private String flr16 = "ELEMENTO PARALLELO         :";
    //Original name: REP-PROGR-PROTOCOL
    private String repProgrProtocol = "00000";
    /**Original name: DETT-LOG-ERRORE-REPORT<br>
	 * <pre>*****************************************************************
	 *  DETTAGLIO LOG ERRORE
	 * *****************************************************************</pre>*/
    private String dettLogErroreReport = "* DETTAGLIO LOG ERRORE";
    //Original name: CAMPI-LOG-ERRORE-REPORT-RIGA-1
    private CampiLogErroreReportRiga1 campiLogErroreReportRiga1 = new CampiLogErroreReportRiga1();
    //Original name: CAMPI-LOG-ERRORE-REPORT-RIGA-2
    private CampiLogErroreReportRiga2 campiLogErroreReportRiga2 = new CampiLogErroreReportRiga2();
    //Original name: SL-CAMPI-LOG-ERRORE-REPORT
    private SlCampiLogErroreReport slCampiLogErroreReport = new SlCampiLogErroreReport();
    //Original name: VALORI-CAMPI-LOG-ERRORE-REPORT
    private ValoriCampiLogErroreReport valoriCampiLogErroreReport = new ValoriCampiLogErroreReport();
    /**Original name: LOGO-ESITI-BATCH-REPORT<br>
	 * <pre>*****************************************************************
	 *  RIEPILOGO ESITI BATCH
	 * *****************************************************************</pre>*/
    private String logoEsitiBatchReport = "* RIEPILOGO ESITI BATCH";
    //Original name: SL-COUNT
    private String slCount = LiteralGenerator.create("-", Len.SL_COUNT);
    //Original name: FILLER-LOGO-STANDARD-COUNT
    private String flr17 = "---";
    //Original name: FILLER-LOGO-STANDARD-COUNT-1
    private String flr18 = "CONTATORI STANDARD                    ---";
    //Original name: BUS-SERV-ESEG-REPORT
    private BusServEsegReport busServEsegReport = new BusServEsegReport();
    //Original name: BUS-SERV-OK-REPORT
    private BusServOkReport busServOkReport = new BusServOkReport();
    //Original name: BUS-SERV-OK-WARN-REPORT
    private BusServOkWarnReport busServOkWarnReport = new BusServOkWarnReport();
    //Original name: BUS-SERV-KO-REPORT
    private BusServKoReport busServKoReport = new BusServKoReport();
    //Original name: FILLER-LOGO-CUSTOM-COUNT
    private String flr19 = "---";
    //Original name: FILLER-LOGO-CUSTOM-COUNT-1
    private String flr20 = "CONTATORI PERSONALIZZATI                 ---";
    //Original name: TAB-GUIDE-LETTE-REPORT
    private TabGuideLetteReport tabGuideLetteReport = new TabGuideLetteReport();
    //Original name: CUSTOM-REPORT
    private CustomReport customReport = new CustomReport();
    //Original name: STATO-FINALE-BATCH-REPORT
    private StatoFinaleBatchReport statoFinaleBatchReport = new StatoFinaleBatchReport();
    //Original name: STATO-FINALE-PARALLELO-REPORT
    private StatoFinaleParalleloReport statoFinaleParalleloReport = new StatoFinaleParalleloReport();
    //Original name: RESTO
    private int resto = 0;
    //Original name: RESULT
    private int result = 0;

    //==== METHODS ====
    public String getLogoJclReportFormatted() {
        return MarshalByteExt.bufferToStr(getLogoJclReportBytes());
    }

    /**Original name: LOGO-JCL-REPORT<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE REPORT BATCH
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *  TESTATA JCL
	 * *****************************************************************</pre>*/
    public byte[] getLogoJclReportBytes() {
        byte[] buffer = new byte[Len.LOGO_JCL_REPORT];
        return getLogoJclReportBytes(buffer, 1);
    }

    public byte[] getLogoJclReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public String getCodCompagniaReportFormatted() {
        return MarshalByteExt.bufferToStr(getCodCompagniaReportBytes());
    }

    /**Original name: COD-COMPAGNIA-REPORT<br>*/
    public byte[] getCodCompagniaReportBytes() {
        byte[] buffer = new byte[Len.COD_COMPAGNIA_REPORT];
        return getCodCompagniaReportBytes(buffer, 1);
    }

    public byte[] getCodCompagniaReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, repCodCompagnia, Len.REP_COD_COMPAGNIA);
        return buffer;
    }

    public String getFlr3() {
        return this.flr3;
    }

    public void setRepCodCompagnia(long repCodCompagnia) {
        this.repCodCompagnia = PicFormatter.display("Z(4)9").format(repCodCompagnia).toString();
    }

    public long getRepCodCompagnia() {
        return PicParser.display("Z(4)9").parseLong(this.repCodCompagnia);
    }

    public String getDescBatchReportFormatted() {
        return MarshalByteExt.bufferToStr(getDescBatchReportBytes());
    }

    /**Original name: DESC-BATCH-REPORT<br>*/
    public byte[] getDescBatchReportBytes() {
        byte[] buffer = new byte[Len.DESC_BATCH_REPORT];
        return getDescBatchReportBytes(buffer, 1);
    }

    public byte[] getDescBatchReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr4, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, repDescBatch, Len.REP_DESC_BATCH);
        return buffer;
    }

    public String getFlr4() {
        return this.flr4;
    }

    public void setRepDescBatch(String repDescBatch) {
        this.repDescBatch = Functions.subString(repDescBatch, Len.REP_DESC_BATCH);
    }

    public String getRepDescBatch() {
        return this.repDescBatch;
    }

    public String getUserNameReportFormatted() {
        return MarshalByteExt.bufferToStr(getUserNameReportBytes());
    }

    /**Original name: USER-NAME-REPORT<br>*/
    public byte[] getUserNameReportBytes() {
        byte[] buffer = new byte[Len.USER_NAME_REPORT];
        return getUserNameReportBytes(buffer, 1);
    }

    public byte[] getUserNameReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, repUserName, Len.REP_USER_NAME);
        return buffer;
    }

    public String getFlr5() {
        return this.flr5;
    }

    public void setRepUserName(String repUserName) {
        this.repUserName = Functions.subString(repUserName, Len.REP_USER_NAME);
    }

    public String getRepUserName() {
        return this.repUserName;
    }

    public String getGruppoAutReportFormatted() {
        return MarshalByteExt.bufferToStr(getGruppoAutReportBytes());
    }

    /**Original name: GRUPPO-AUT-REPORT<br>*/
    public byte[] getGruppoAutReportBytes() {
        byte[] buffer = new byte[Len.GRUPPO_AUT_REPORT];
        return getGruppoAutReportBytes(buffer, 1);
    }

    public byte[] getGruppoAutReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr6, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, repGruppoAut, Len.REP_GRUPPO_AUT);
        return buffer;
    }

    public String getFlr6() {
        return this.flr6;
    }

    public void setRepGruppoAutFormatted(String repGruppoAut) {
        this.repGruppoAut = Trunc.toUnsignedNumeric(repGruppoAut, Len.REP_GRUPPO_AUT);
    }

    public long getRepGruppoAut() {
        return NumericDisplay.asLong(this.repGruppoAut);
    }

    public String getMacrofunzionalitaReportFormatted() {
        return MarshalByteExt.bufferToStr(getMacrofunzionalitaReportBytes());
    }

    /**Original name: MACROFUNZIONALITA-REPORT<br>*/
    public byte[] getMacrofunzionalitaReportBytes() {
        byte[] buffer = new byte[Len.MACROFUNZIONALITA_REPORT];
        return getMacrofunzionalitaReportBytes(buffer, 1);
    }

    public byte[] getMacrofunzionalitaReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr7, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, repMacrofunzionalita, Len.REP_MACROFUNZIONALITA);
        return buffer;
    }

    public String getFlr7() {
        return this.flr7;
    }

    public void setRepMacrofunzionalita(String repMacrofunzionalita) {
        this.repMacrofunzionalita = Functions.subString(repMacrofunzionalita, Len.REP_MACROFUNZIONALITA);
    }

    public String getRepMacrofunzionalita() {
        return this.repMacrofunzionalita;
    }

    public String getRepMacrofunzionalitaFormatted() {
        return Functions.padBlanks(getRepMacrofunzionalita(), Len.REP_MACROFUNZIONALITA);
    }

    public String getTipoMovimentoReportFormatted() {
        return MarshalByteExt.bufferToStr(getTipoMovimentoReportBytes());
    }

    /**Original name: TIPO-MOVIMENTO-REPORT<br>*/
    public byte[] getTipoMovimentoReportBytes() {
        byte[] buffer = new byte[Len.TIPO_MOVIMENTO_REPORT];
        return getTipoMovimentoReportBytes(buffer, 1);
    }

    public byte[] getTipoMovimentoReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr8, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, repTipoMovimento, Len.REP_TIPO_MOVIMENTO);
        return buffer;
    }

    public String getFlr8() {
        return this.flr8;
    }

    public void setRepTipoMovimentoFormatted(String repTipoMovimento) {
        this.repTipoMovimento = Trunc.toUnsignedNumeric(repTipoMovimento, Len.REP_TIPO_MOVIMENTO);
    }

    public int getRepTipoMovimento() {
        return NumericDisplay.asInt(this.repTipoMovimento);
    }

    public String getRepTipoMovimentoFormatted() {
        return this.repTipoMovimento;
    }

    public String getDataEffettoReportFormatted() {
        return MarshalByteExt.bufferToStr(getDataEffettoReportBytes());
    }

    /**Original name: DATA-EFFETTO-REPORT<br>*/
    public byte[] getDataEffettoReportBytes() {
        byte[] buffer = new byte[Len.DATA_EFFETTO_REPORT];
        return getDataEffettoReportBytes(buffer, 1);
    }

    public byte[] getDataEffettoReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr9, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, repDataEffetto, Len.REP_DATA_EFFETTO);
        return buffer;
    }

    public String getFlr9() {
        return this.flr9;
    }

    public void setRepDataEffetto(String repDataEffetto) {
        this.repDataEffetto = Functions.subString(repDataEffetto, Len.REP_DATA_EFFETTO);
    }

    public String getRepDataEffetto() {
        return this.repDataEffetto;
    }

    public String getTimestampCompetenzaReportFormatted() {
        return MarshalByteExt.bufferToStr(getTimestampCompetenzaReportBytes());
    }

    /**Original name: TIMESTAMP-COMPETENZA-REPORT<br>*/
    public byte[] getTimestampCompetenzaReportBytes() {
        byte[] buffer = new byte[Len.TIMESTAMP_COMPETENZA_REPORT];
        return getTimestampCompetenzaReportBytes(buffer, 1);
    }

    public byte[] getTimestampCompetenzaReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr10, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, repTimestampCompetenza, Len.REP_TIMESTAMP_COMPETENZA);
        return buffer;
    }

    public String getFlr10() {
        return this.flr10;
    }

    public void setRepTimestampCompetenza(String repTimestampCompetenza) {
        this.repTimestampCompetenza = Functions.subString(repTimestampCompetenza, Len.REP_TIMESTAMP_COMPETENZA);
    }

    public String getRepTimestampCompetenza() {
        return this.repTimestampCompetenza;
    }

    public String getSimulazioneReportFormatted() {
        return MarshalByteExt.bufferToStr(getSimulazioneReportBytes());
    }

    /**Original name: SIMULAZIONE-REPORT<br>*/
    public byte[] getSimulazioneReportBytes() {
        byte[] buffer = new byte[Len.SIMULAZIONE_REPORT];
        return getSimulazioneReportBytes(buffer, 1);
    }

    public byte[] getSimulazioneReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr11, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeChar(buffer, position, repSimulazione);
        return buffer;
    }

    public String getFlr11() {
        return this.flr11;
    }

    public void setRepSimulazione(char repSimulazione) {
        this.repSimulazione = repSimulazione;
    }

    public char getRepSimulazione() {
        return this.repSimulazione;
    }

    public String getCanaleVenditaReportFormatted() {
        return MarshalByteExt.bufferToStr(getCanaleVenditaReportBytes());
    }

    /**Original name: CANALE-VENDITA-REPORT<br>*/
    public byte[] getCanaleVenditaReportBytes() {
        byte[] buffer = new byte[Len.CANALE_VENDITA_REPORT];
        return getCanaleVenditaReportBytes(buffer, 1);
    }

    public byte[] getCanaleVenditaReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr12, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, repCanaleVendita, Len.REP_CANALE_VENDITA);
        return buffer;
    }

    public String getFlr12() {
        return this.flr12;
    }

    public void setRepCanaleVendita(long repCanaleVendita) {
        this.repCanaleVendita = PicFormatter.display("Z(4)9").format(repCanaleVendita).toString();
    }

    public long getRepCanaleVendita() {
        return PicParser.display("Z(4)9").parseLong(this.repCanaleVendita);
    }

    public String getPuntoVenditaReportFormatted() {
        return MarshalByteExt.bufferToStr(getPuntoVenditaReportBytes());
    }

    /**Original name: PUNTO-VENDITA-REPORT<br>*/
    public byte[] getPuntoVenditaReportBytes() {
        byte[] buffer = new byte[Len.PUNTO_VENDITA_REPORT];
        return getPuntoVenditaReportBytes(buffer, 1);
    }

    public byte[] getPuntoVenditaReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr13, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, repPuntoVendita, Len.REP_PUNTO_VENDITA);
        return buffer;
    }

    public String getFlr13() {
        return this.flr13;
    }

    public void setRepPuntoVenditaFormatted(String repPuntoVendita) {
        this.repPuntoVendita = PicFormatter.display("Z(4)9").format(repPuntoVendita).toString();
    }

    public long getRepPuntoVendita() {
        return PicParser.display("Z(4)9").parseLong(this.repPuntoVendita);
    }

    public String getLogoBatchReport() {
        return this.logoBatchReport;
    }

    public String getIdBatchReportFormatted() {
        return MarshalByteExt.bufferToStr(getIdBatchReportBytes());
    }

    /**Original name: ID-BATCH-REPORT<br>*/
    public byte[] getIdBatchReportBytes() {
        byte[] buffer = new byte[Len.ID_BATCH_REPORT];
        return getIdBatchReportBytes(buffer, 1);
    }

    public byte[] getIdBatchReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr14, Len.FLR14);
        position += Len.FLR14;
        MarshalByte.writeString(buffer, position, repIdBatch, Len.REP_ID_BATCH);
        return buffer;
    }

    public String getFlr14() {
        return this.flr14;
    }

    public void setRepIdBatch(long repIdBatch) {
        this.repIdBatch = PicFormatter.display("Z(8)9").format(repIdBatch).toString();
    }

    public long getRepIdBatch() {
        return PicParser.display("Z(8)9").parseLong(this.repIdBatch);
    }

    public String getProtocolReportFormatted() {
        return MarshalByteExt.bufferToStr(getProtocolReportBytes());
    }

    /**Original name: PROTOCOL-REPORT<br>*/
    public byte[] getProtocolReportBytes() {
        byte[] buffer = new byte[Len.PROTOCOL_REPORT];
        return getProtocolReportBytes(buffer, 1);
    }

    public byte[] getProtocolReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr15, Len.FLR14);
        position += Len.FLR14;
        MarshalByte.writeString(buffer, position, repProtocol, Len.REP_PROTOCOL);
        return buffer;
    }

    public String getFlr15() {
        return this.flr15;
    }

    public void setRepProtocol(String repProtocol) {
        this.repProtocol = Functions.subString(repProtocol, Len.REP_PROTOCOL);
    }

    public String getRepProtocol() {
        return this.repProtocol;
    }

    public String getProgrProtocolReportFormatted() {
        return MarshalByteExt.bufferToStr(getProgrProtocolReportBytes());
    }

    /**Original name: PROGR-PROTOCOL-REPORT<br>*/
    public byte[] getProgrProtocolReportBytes() {
        byte[] buffer = new byte[Len.PROGR_PROTOCOL_REPORT];
        return getProgrProtocolReportBytes(buffer, 1);
    }

    public byte[] getProgrProtocolReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr16, Len.FLR16);
        position += Len.FLR16;
        MarshalByte.writeString(buffer, position, repProgrProtocol, Len.REP_PROGR_PROTOCOL);
        return buffer;
    }

    public String getFlr16() {
        return this.flr16;
    }

    public void setRepProgrProtocol(long repProgrProtocol) {
        this.repProgrProtocol = PicFormatter.display("Z(4)9").format(repProgrProtocol).toString();
    }

    public long getRepProgrProtocol() {
        return PicParser.display("Z(4)9").parseLong(this.repProgrProtocol);
    }

    public String getDettLogErroreReport() {
        return this.dettLogErroreReport;
    }

    public String getLogoEsitiBatchReport() {
        return this.logoEsitiBatchReport;
    }

    public String getSlCount() {
        return this.slCount;
    }

    public String getLogoStandardCountFormatted() {
        return MarshalByteExt.bufferToStr(getLogoStandardCountBytes());
    }

    /**Original name: LOGO-STANDARD-COUNT<br>*/
    public byte[] getLogoStandardCountBytes() {
        byte[] buffer = new byte[Len.LOGO_STANDARD_COUNT];
        return getLogoStandardCountBytes(buffer, 1);
    }

    public byte[] getLogoStandardCountBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr17, Len.FLR17);
        position += Len.FLR17;
        MarshalByte.writeString(buffer, position, flr18, Len.FLR18);
        return buffer;
    }

    public String getFlr17() {
        return this.flr17;
    }

    public String getFlr18() {
        return this.flr18;
    }

    public String getLogoCustomCountFormatted() {
        return MarshalByteExt.bufferToStr(getLogoCustomCountBytes());
    }

    /**Original name: LOGO-CUSTOM-COUNT<br>*/
    public byte[] getLogoCustomCountBytes() {
        byte[] buffer = new byte[Len.LOGO_CUSTOM_COUNT];
        return getLogoCustomCountBytes(buffer, 1);
    }

    public byte[] getLogoCustomCountBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr19, Len.FLR19);
        position += Len.FLR19;
        MarshalByte.writeString(buffer, position, flr20, Len.FLR20);
        return buffer;
    }

    public String getFlr19() {
        return this.flr19;
    }

    public String getFlr20() {
        return this.flr20;
    }

    public void setResto(int resto) {
        this.resto = resto;
    }

    public int getResto() {
        return this.resto;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public int getResult() {
        return this.result;
    }

    public BusServEsegReport getBusServEsegReport() {
        return busServEsegReport;
    }

    public BusServKoReport getBusServKoReport() {
        return busServKoReport;
    }

    public BusServOkReport getBusServOkReport() {
        return busServOkReport;
    }

    public BusServOkWarnReport getBusServOkWarnReport() {
        return busServOkWarnReport;
    }

    public CampiLogErroreReportRiga1 getCampiLogErroreReportRiga1() {
        return campiLogErroreReportRiga1;
    }

    public CampiLogErroreReportRiga2 getCampiLogErroreReportRiga2() {
        return campiLogErroreReportRiga2;
    }

    public CustomReport getCustomReport() {
        return customReport;
    }

    public DataElaborazioneReport getDataElaborazioneReport() {
        return dataElaborazioneReport;
    }

    public OraElaborazioneReport getOraElaborazioneReport() {
        return oraElaborazioneReport;
    }

    public SlCampiLogErroreReport getSlCampiLogErroreReport() {
        return slCampiLogErroreReport;
    }

    public StatoFinaleBatchReport getStatoFinaleBatchReport() {
        return statoFinaleBatchReport;
    }

    public StatoFinaleParalleloReport getStatoFinaleParalleloReport() {
        return statoFinaleParalleloReport;
    }

    public TabGuideLetteReport getTabGuideLetteReport() {
        return tabGuideLetteReport;
    }

    public ValoriCampiLogErroreReport getValoriCampiLogErroreReport() {
        return valoriCampiLogErroreReport;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int REP_COD_COMPAGNIA = 5;
        public static final int REP_GRUPPO_AUT = 10;
        public static final int REP_TIPO_MOVIMENTO = 5;
        public static final int REP_CANALE_VENDITA = 5;
        public static final int REP_PUNTO_VENDITA = 5;
        public static final int REP_ID_BATCH = 9;
        public static final int REP_PROGR_PROTOCOL = 5;
        public static final int SL_COUNT = 63;
        public static final int RESTO = 5;
        public static final int RESULT = 9;
        public static final int FLR1 = 32;
        public static final int FLR2 = 100;
        public static final int LOGO_JCL_REPORT = FLR1 + FLR2;
        public static final int FLR3 = 26;
        public static final int COD_COMPAGNIA_REPORT = REP_COD_COMPAGNIA + FLR3;
        public static final int REP_DESC_BATCH = 100;
        public static final int DESC_BATCH_REPORT = REP_DESC_BATCH + FLR3;
        public static final int REP_USER_NAME = 8;
        public static final int USER_NAME_REPORT = REP_USER_NAME + FLR3;
        public static final int GRUPPO_AUT_REPORT = REP_GRUPPO_AUT + FLR3;
        public static final int REP_MACROFUNZIONALITA = 2;
        public static final int MACROFUNZIONALITA_REPORT = REP_MACROFUNZIONALITA + FLR3;
        public static final int TIPO_MOVIMENTO_REPORT = REP_TIPO_MOVIMENTO + FLR3;
        public static final int REP_DATA_EFFETTO = 10;
        public static final int DATA_EFFETTO_REPORT = REP_DATA_EFFETTO + FLR3;
        public static final int REP_TIMESTAMP_COMPETENZA = 26;
        public static final int TIMESTAMP_COMPETENZA_REPORT = REP_TIMESTAMP_COMPETENZA + FLR3;
        public static final int REP_SIMULAZIONE = 1;
        public static final int SIMULAZIONE_REPORT = REP_SIMULAZIONE + FLR3;
        public static final int CANALE_VENDITA_REPORT = REP_CANALE_VENDITA + FLR3;
        public static final int PUNTO_VENDITA_REPORT = REP_PUNTO_VENDITA + FLR3;
        public static final int FLR14 = 30;
        public static final int ID_BATCH_REPORT = REP_ID_BATCH + FLR14;
        public static final int REP_PROTOCOL = 50;
        public static final int PROTOCOL_REPORT = REP_PROTOCOL + FLR14;
        public static final int FLR16 = 38;
        public static final int PROGR_PROTOCOL_REPORT = REP_PROGR_PROTOCOL + FLR16;
        public static final int FLR17 = 22;
        public static final int FLR18 = 41;
        public static final int LOGO_STANDARD_COUNT = FLR17 + FLR18;
        public static final int FLR19 = 19;
        public static final int FLR20 = 44;
        public static final int LOGO_CUSTOM_COUNT = FLR20 + FLR19;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
