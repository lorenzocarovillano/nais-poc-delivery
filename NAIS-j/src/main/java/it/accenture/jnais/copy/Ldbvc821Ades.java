package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVC821-ADES<br>
 * Variable: LDBVC821-ADES from copybook LDBVC821<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbvc821Ades {

    //==== PROPERTIES ====
    //Original name: LC821-ADE-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-ID-MOVI-CHIU
    private int idMoviChiu = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-DT-INI-EFF
    private int dtIniEff = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-DT-END-EFF
    private int dtEndEff = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-IB-PREV
    private String ibPrev = DefaultValues.stringVal(Len.IB_PREV);
    //Original name: LC821-ADE-IB-OGG
    private String ibOgg = DefaultValues.stringVal(Len.IB_OGG);
    //Original name: LC821-ADE-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-DT-DECOR
    private int dtDecor = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-DT-SCAD
    private int dtScad = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-ETA-A-SCAD
    private int etaAScad = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-DUR-AA
    private int durAa = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-DUR-MM
    private int durMm = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-DUR-GG
    private int durGg = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-TP-RGM-FISC
    private String tpRgmFisc = DefaultValues.stringVal(Len.TP_RGM_FISC);
    //Original name: LC821-ADE-TP-RIAT
    private String tpRiat = DefaultValues.stringVal(Len.TP_RIAT);
    //Original name: LC821-ADE-TP-MOD-PAG-TIT
    private String tpModPagTit = DefaultValues.stringVal(Len.TP_MOD_PAG_TIT);
    //Original name: LC821-ADE-TP-IAS
    private String tpIas = DefaultValues.stringVal(Len.TP_IAS);
    //Original name: LC821-ADE-DT-VARZ-TP-IAS
    private int dtVarzTpIas = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-PRE-NET-IND
    private AfDecimal preNetInd = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-PRE-LRD-IND
    private AfDecimal preLrdInd = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-RAT-LRD-IND
    private AfDecimal ratLrdInd = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-PRSTZ-INI-IND
    private AfDecimal prstzIniInd = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-FL-COINC-ASSTO
    private char flCoincAssto = DefaultValues.CHAR_VAL;
    //Original name: LC821-ADE-IB-DFLT
    private String ibDflt = DefaultValues.stringVal(Len.IB_DFLT);
    //Original name: LC821-ADE-MOD-CALC
    private String modCalc = DefaultValues.stringVal(Len.MOD_CALC);
    //Original name: LC821-ADE-TP-FNT-CNBTVA
    private String tpFntCnbtva = DefaultValues.stringVal(Len.TP_FNT_CNBTVA);
    //Original name: LC821-ADE-IMP-AZ
    private AfDecimal impAz = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-IMP-ADER
    private AfDecimal impAder = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-IMP-TFR
    private AfDecimal impTfr = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-IMP-VOLO
    private AfDecimal impVolo = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-PC-AZ
    private AfDecimal pcAz = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LC821-ADE-PC-ADER
    private AfDecimal pcAder = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LC821-ADE-PC-TFR
    private AfDecimal pcTfr = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LC821-ADE-PC-VOLO
    private AfDecimal pcVolo = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LC821-ADE-DT-NOVA-RGM-FISC
    private int dtNovaRgmFisc = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-FL-ATTIV
    private char flAttiv = DefaultValues.CHAR_VAL;
    //Original name: LC821-ADE-IMP-REC-RIT-VIS
    private AfDecimal impRecRitVis = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-IMP-REC-RIT-ACC
    private AfDecimal impRecRitAcc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-FL-VARZ-STAT-TBGC
    private char flVarzStatTbgc = DefaultValues.CHAR_VAL;
    //Original name: LC821-ADE-FL-PROVZA-MIGRAZ
    private char flProvzaMigraz = DefaultValues.CHAR_VAL;
    //Original name: LC821-ADE-IMPB-VIS-DA-REC
    private AfDecimal impbVisDaRec = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-DT-DECOR-PREST-BAN
    private int dtDecorPrestBan = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-DT-EFF-VARZ-STAT-T
    private int dtEffVarzStatT = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-DS-RIGA
    private long dsRiga = DefaultValues.LONG_VAL;
    //Original name: LC821-ADE-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: LC821-ADE-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-DS-TS-INI-CPTZ
    private long dsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: LC821-ADE-DS-TS-END-CPTZ
    private long dsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: LC821-ADE-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: LC821-ADE-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: LC821-ADE-CUM-CNBT-CAP
    private AfDecimal cumCnbtCap = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-IMP-GAR-CNBT
    private AfDecimal impGarCnbt = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC821-ADE-DT-ULT-CONS-CNBT
    private int dtUltConsCnbt = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-IDEN-ISC-FND
    private String idenIscFnd = DefaultValues.stringVal(Len.IDEN_ISC_FND);
    //Original name: LC821-ADE-NUM-RAT-PIAN
    private AfDecimal numRatPian = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: LC821-ADE-DT-PRESC
    private int dtPresc = DefaultValues.INT_VAL;
    //Original name: LC821-ADE-CONCS-PREST
    private char concsPrest = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setLdbvc821AdesBytes(byte[] buffer) {
        setLdbvc821AdesBytes(buffer, 1);
    }

    public byte[] getLdbvc821AdesBytes() {
        byte[] buffer = new byte[Len.LDBVC821_ADES];
        return getLdbvc821AdesBytes(buffer, 1);
    }

    public void setLdbvc821AdesBytes(byte[] buffer, int offset) {
        int position = offset;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        idMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        idMoviChiu = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        dtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        dtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        ibPrev = MarshalByte.readString(buffer, position, Len.IB_PREV);
        position += Len.IB_PREV;
        ibOgg = MarshalByte.readString(buffer, position, Len.IB_OGG);
        position += Len.IB_OGG;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        dtDecor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DECOR, 0);
        position += Len.DT_DECOR;
        dtScad = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_SCAD, 0);
        position += Len.DT_SCAD;
        etaAScad = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ETA_A_SCAD, 0);
        position += Len.ETA_A_SCAD;
        durAa = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DUR_AA, 0);
        position += Len.DUR_AA;
        durMm = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DUR_MM, 0);
        position += Len.DUR_MM;
        durGg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DUR_GG, 0);
        position += Len.DUR_GG;
        tpRgmFisc = MarshalByte.readString(buffer, position, Len.TP_RGM_FISC);
        position += Len.TP_RGM_FISC;
        tpRiat = MarshalByte.readString(buffer, position, Len.TP_RIAT);
        position += Len.TP_RIAT;
        tpModPagTit = MarshalByte.readString(buffer, position, Len.TP_MOD_PAG_TIT);
        position += Len.TP_MOD_PAG_TIT;
        tpIas = MarshalByte.readString(buffer, position, Len.TP_IAS);
        position += Len.TP_IAS;
        dtVarzTpIas = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_VARZ_TP_IAS, 0);
        position += Len.DT_VARZ_TP_IAS;
        preNetInd.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_NET_IND, Len.Fract.PRE_NET_IND));
        position += Len.PRE_NET_IND;
        preLrdInd.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_LRD_IND, Len.Fract.PRE_LRD_IND));
        position += Len.PRE_LRD_IND;
        ratLrdInd.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.RAT_LRD_IND, Len.Fract.RAT_LRD_IND));
        position += Len.RAT_LRD_IND;
        prstzIniInd.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRSTZ_INI_IND, Len.Fract.PRSTZ_INI_IND));
        position += Len.PRSTZ_INI_IND;
        flCoincAssto = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ibDflt = MarshalByte.readString(buffer, position, Len.IB_DFLT);
        position += Len.IB_DFLT;
        modCalc = MarshalByte.readString(buffer, position, Len.MOD_CALC);
        position += Len.MOD_CALC;
        tpFntCnbtva = MarshalByte.readString(buffer, position, Len.TP_FNT_CNBTVA);
        position += Len.TP_FNT_CNBTVA;
        impAz.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_AZ, Len.Fract.IMP_AZ));
        position += Len.IMP_AZ;
        impAder.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_ADER, Len.Fract.IMP_ADER));
        position += Len.IMP_ADER;
        impTfr.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_TFR, Len.Fract.IMP_TFR));
        position += Len.IMP_TFR;
        impVolo.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_VOLO, Len.Fract.IMP_VOLO));
        position += Len.IMP_VOLO;
        pcAz.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_AZ, Len.Fract.PC_AZ));
        position += Len.PC_AZ;
        pcAder.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_ADER, Len.Fract.PC_ADER));
        position += Len.PC_ADER;
        pcTfr.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_TFR, Len.Fract.PC_TFR));
        position += Len.PC_TFR;
        pcVolo.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_VOLO, Len.Fract.PC_VOLO));
        position += Len.PC_VOLO;
        dtNovaRgmFisc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_NOVA_RGM_FISC, 0);
        position += Len.DT_NOVA_RGM_FISC;
        flAttiv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        impRecRitVis.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_REC_RIT_VIS, Len.Fract.IMP_REC_RIT_VIS));
        position += Len.IMP_REC_RIT_VIS;
        impRecRitAcc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_REC_RIT_ACC, Len.Fract.IMP_REC_RIT_ACC));
        position += Len.IMP_REC_RIT_ACC;
        flVarzStatTbgc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flProvzaMigraz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        impbVisDaRec.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_VIS_DA_REC, Len.Fract.IMPB_VIS_DA_REC));
        position += Len.IMPB_VIS_DA_REC;
        dtDecorPrestBan = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DECOR_PREST_BAN, 0);
        position += Len.DT_DECOR_PREST_BAN;
        dtEffVarzStatT = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_EFF_VARZ_STAT_T, 0);
        position += Len.DT_EFF_VARZ_STAT_T;
        dsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        dsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        cumCnbtCap.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.CUM_CNBT_CAP, Len.Fract.CUM_CNBT_CAP));
        position += Len.CUM_CNBT_CAP;
        impGarCnbt.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_GAR_CNBT, Len.Fract.IMP_GAR_CNBT));
        position += Len.IMP_GAR_CNBT;
        dtUltConsCnbt = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_ULT_CONS_CNBT, 0);
        position += Len.DT_ULT_CONS_CNBT;
        idenIscFnd = MarshalByte.readString(buffer, position, Len.IDEN_ISC_FND);
        position += Len.IDEN_ISC_FND;
        numRatPian.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.NUM_RAT_PIAN, Len.Fract.NUM_RAT_PIAN));
        position += Len.NUM_RAT_PIAN;
        dtPresc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_PRESC, 0);
        position += Len.DT_PRESC;
        concsPrest = MarshalByte.readChar(buffer, position);
    }

    public byte[] getLdbvc821AdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviCrz, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviChiu, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniEff, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtEndEff, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        MarshalByte.writeString(buffer, position, ibPrev, Len.IB_PREV);
        position += Len.IB_PREV;
        MarshalByte.writeString(buffer, position, ibOgg, Len.IB_OGG);
        position += Len.IB_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, dtDecor, Len.Int.DT_DECOR, 0);
        position += Len.DT_DECOR;
        MarshalByte.writeIntAsPacked(buffer, position, dtScad, Len.Int.DT_SCAD, 0);
        position += Len.DT_SCAD;
        MarshalByte.writeIntAsPacked(buffer, position, etaAScad, Len.Int.ETA_A_SCAD, 0);
        position += Len.ETA_A_SCAD;
        MarshalByte.writeIntAsPacked(buffer, position, durAa, Len.Int.DUR_AA, 0);
        position += Len.DUR_AA;
        MarshalByte.writeIntAsPacked(buffer, position, durMm, Len.Int.DUR_MM, 0);
        position += Len.DUR_MM;
        MarshalByte.writeIntAsPacked(buffer, position, durGg, Len.Int.DUR_GG, 0);
        position += Len.DUR_GG;
        MarshalByte.writeString(buffer, position, tpRgmFisc, Len.TP_RGM_FISC);
        position += Len.TP_RGM_FISC;
        MarshalByte.writeString(buffer, position, tpRiat, Len.TP_RIAT);
        position += Len.TP_RIAT;
        MarshalByte.writeString(buffer, position, tpModPagTit, Len.TP_MOD_PAG_TIT);
        position += Len.TP_MOD_PAG_TIT;
        MarshalByte.writeString(buffer, position, tpIas, Len.TP_IAS);
        position += Len.TP_IAS;
        MarshalByte.writeIntAsPacked(buffer, position, dtVarzTpIas, Len.Int.DT_VARZ_TP_IAS, 0);
        position += Len.DT_VARZ_TP_IAS;
        MarshalByte.writeDecimalAsPacked(buffer, position, preNetInd.copy());
        position += Len.PRE_NET_IND;
        MarshalByte.writeDecimalAsPacked(buffer, position, preLrdInd.copy());
        position += Len.PRE_LRD_IND;
        MarshalByte.writeDecimalAsPacked(buffer, position, ratLrdInd.copy());
        position += Len.RAT_LRD_IND;
        MarshalByte.writeDecimalAsPacked(buffer, position, prstzIniInd.copy());
        position += Len.PRSTZ_INI_IND;
        MarshalByte.writeChar(buffer, position, flCoincAssto);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ibDflt, Len.IB_DFLT);
        position += Len.IB_DFLT;
        MarshalByte.writeString(buffer, position, modCalc, Len.MOD_CALC);
        position += Len.MOD_CALC;
        MarshalByte.writeString(buffer, position, tpFntCnbtva, Len.TP_FNT_CNBTVA);
        position += Len.TP_FNT_CNBTVA;
        MarshalByte.writeDecimalAsPacked(buffer, position, impAz.copy());
        position += Len.IMP_AZ;
        MarshalByte.writeDecimalAsPacked(buffer, position, impAder.copy());
        position += Len.IMP_ADER;
        MarshalByte.writeDecimalAsPacked(buffer, position, impTfr.copy());
        position += Len.IMP_TFR;
        MarshalByte.writeDecimalAsPacked(buffer, position, impVolo.copy());
        position += Len.IMP_VOLO;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcAz.copy());
        position += Len.PC_AZ;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcAder.copy());
        position += Len.PC_ADER;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcTfr.copy());
        position += Len.PC_TFR;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcVolo.copy());
        position += Len.PC_VOLO;
        MarshalByte.writeIntAsPacked(buffer, position, dtNovaRgmFisc, Len.Int.DT_NOVA_RGM_FISC, 0);
        position += Len.DT_NOVA_RGM_FISC;
        MarshalByte.writeChar(buffer, position, flAttiv);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, impRecRitVis.copy());
        position += Len.IMP_REC_RIT_VIS;
        MarshalByte.writeDecimalAsPacked(buffer, position, impRecRitAcc.copy());
        position += Len.IMP_REC_RIT_ACC;
        MarshalByte.writeChar(buffer, position, flVarzStatTbgc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flProvzaMigraz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbVisDaRec.copy());
        position += Len.IMPB_VIS_DA_REC;
        MarshalByte.writeIntAsPacked(buffer, position, dtDecorPrestBan, Len.Int.DT_DECOR_PREST_BAN, 0);
        position += Len.DT_DECOR_PREST_BAN;
        MarshalByte.writeIntAsPacked(buffer, position, dtEffVarzStatT, Len.Int.DT_EFF_VARZ_STAT_T, 0);
        position += Len.DT_EFF_VARZ_STAT_T;
        MarshalByte.writeLongAsPacked(buffer, position, dsRiga, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, cumCnbtCap.copy());
        position += Len.CUM_CNBT_CAP;
        MarshalByte.writeDecimalAsPacked(buffer, position, impGarCnbt.copy());
        position += Len.IMP_GAR_CNBT;
        MarshalByte.writeIntAsPacked(buffer, position, dtUltConsCnbt, Len.Int.DT_ULT_CONS_CNBT, 0);
        position += Len.DT_ULT_CONS_CNBT;
        MarshalByte.writeString(buffer, position, idenIscFnd, Len.IDEN_ISC_FND);
        position += Len.IDEN_ISC_FND;
        MarshalByte.writeDecimalAsPacked(buffer, position, numRatPian.copy());
        position += Len.NUM_RAT_PIAN;
        MarshalByte.writeIntAsPacked(buffer, position, dtPresc, Len.Int.DT_PRESC, 0);
        position += Len.DT_PRESC;
        MarshalByte.writeChar(buffer, position, concsPrest);
        return buffer;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setIdMoviChiu(int idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public int getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtIniEff(int dtIniEff) {
        this.dtIniEff = dtIniEff;
    }

    public int getDtIniEff() {
        return this.dtIniEff;
    }

    public void setDtEndEff(int dtEndEff) {
        this.dtEndEff = dtEndEff;
    }

    public int getDtEndEff() {
        return this.dtEndEff;
    }

    public void setIbPrev(String ibPrev) {
        this.ibPrev = Functions.subString(ibPrev, Len.IB_PREV);
    }

    public String getIbPrev() {
        return this.ibPrev;
    }

    public void setIbOgg(String ibOgg) {
        this.ibOgg = Functions.subString(ibOgg, Len.IB_OGG);
    }

    public String getIbOgg() {
        return this.ibOgg;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setDtDecor(int dtDecor) {
        this.dtDecor = dtDecor;
    }

    public int getDtDecor() {
        return this.dtDecor;
    }

    public void setDtScad(int dtScad) {
        this.dtScad = dtScad;
    }

    public int getDtScad() {
        return this.dtScad;
    }

    public void setEtaAScad(int etaAScad) {
        this.etaAScad = etaAScad;
    }

    public int getEtaAScad() {
        return this.etaAScad;
    }

    public void setDurAa(int durAa) {
        this.durAa = durAa;
    }

    public int getDurAa() {
        return this.durAa;
    }

    public void setDurMm(int durMm) {
        this.durMm = durMm;
    }

    public int getDurMm() {
        return this.durMm;
    }

    public void setDurGg(int durGg) {
        this.durGg = durGg;
    }

    public int getDurGg() {
        return this.durGg;
    }

    public void setTpRgmFisc(String tpRgmFisc) {
        this.tpRgmFisc = Functions.subString(tpRgmFisc, Len.TP_RGM_FISC);
    }

    public String getTpRgmFisc() {
        return this.tpRgmFisc;
    }

    public void setTpRiat(String tpRiat) {
        this.tpRiat = Functions.subString(tpRiat, Len.TP_RIAT);
    }

    public String getTpRiat() {
        return this.tpRiat;
    }

    public void setTpModPagTit(String tpModPagTit) {
        this.tpModPagTit = Functions.subString(tpModPagTit, Len.TP_MOD_PAG_TIT);
    }

    public String getTpModPagTit() {
        return this.tpModPagTit;
    }

    public void setTpIas(String tpIas) {
        this.tpIas = Functions.subString(tpIas, Len.TP_IAS);
    }

    public String getTpIas() {
        return this.tpIas;
    }

    public void setDtVarzTpIas(int dtVarzTpIas) {
        this.dtVarzTpIas = dtVarzTpIas;
    }

    public int getDtVarzTpIas() {
        return this.dtVarzTpIas;
    }

    public void setPreNetInd(AfDecimal preNetInd) {
        this.preNetInd.assign(preNetInd);
    }

    public AfDecimal getPreNetInd() {
        return this.preNetInd.copy();
    }

    public void setPreLrdInd(AfDecimal preLrdInd) {
        this.preLrdInd.assign(preLrdInd);
    }

    public AfDecimal getPreLrdInd() {
        return this.preLrdInd.copy();
    }

    public void setRatLrdInd(AfDecimal ratLrdInd) {
        this.ratLrdInd.assign(ratLrdInd);
    }

    public AfDecimal getRatLrdInd() {
        return this.ratLrdInd.copy();
    }

    public void setPrstzIniInd(AfDecimal prstzIniInd) {
        this.prstzIniInd.assign(prstzIniInd);
    }

    public AfDecimal getPrstzIniInd() {
        return this.prstzIniInd.copy();
    }

    public void setFlCoincAssto(char flCoincAssto) {
        this.flCoincAssto = flCoincAssto;
    }

    public char getFlCoincAssto() {
        return this.flCoincAssto;
    }

    public void setIbDflt(String ibDflt) {
        this.ibDflt = Functions.subString(ibDflt, Len.IB_DFLT);
    }

    public String getIbDflt() {
        return this.ibDflt;
    }

    public void setModCalc(String modCalc) {
        this.modCalc = Functions.subString(modCalc, Len.MOD_CALC);
    }

    public String getModCalc() {
        return this.modCalc;
    }

    public void setTpFntCnbtva(String tpFntCnbtva) {
        this.tpFntCnbtva = Functions.subString(tpFntCnbtva, Len.TP_FNT_CNBTVA);
    }

    public String getTpFntCnbtva() {
        return this.tpFntCnbtva;
    }

    public void setImpAz(AfDecimal impAz) {
        this.impAz.assign(impAz);
    }

    public AfDecimal getImpAz() {
        return this.impAz.copy();
    }

    public void setImpAder(AfDecimal impAder) {
        this.impAder.assign(impAder);
    }

    public AfDecimal getImpAder() {
        return this.impAder.copy();
    }

    public void setImpTfr(AfDecimal impTfr) {
        this.impTfr.assign(impTfr);
    }

    public AfDecimal getImpTfr() {
        return this.impTfr.copy();
    }

    public void setImpVolo(AfDecimal impVolo) {
        this.impVolo.assign(impVolo);
    }

    public AfDecimal getImpVolo() {
        return this.impVolo.copy();
    }

    public void setPcAz(AfDecimal pcAz) {
        this.pcAz.assign(pcAz);
    }

    public AfDecimal getPcAz() {
        return this.pcAz.copy();
    }

    public void setPcAder(AfDecimal pcAder) {
        this.pcAder.assign(pcAder);
    }

    public AfDecimal getPcAder() {
        return this.pcAder.copy();
    }

    public void setPcTfr(AfDecimal pcTfr) {
        this.pcTfr.assign(pcTfr);
    }

    public AfDecimal getPcTfr() {
        return this.pcTfr.copy();
    }

    public void setPcVolo(AfDecimal pcVolo) {
        this.pcVolo.assign(pcVolo);
    }

    public AfDecimal getPcVolo() {
        return this.pcVolo.copy();
    }

    public void setDtNovaRgmFisc(int dtNovaRgmFisc) {
        this.dtNovaRgmFisc = dtNovaRgmFisc;
    }

    public int getDtNovaRgmFisc() {
        return this.dtNovaRgmFisc;
    }

    public void setFlAttiv(char flAttiv) {
        this.flAttiv = flAttiv;
    }

    public char getFlAttiv() {
        return this.flAttiv;
    }

    public void setImpRecRitVis(AfDecimal impRecRitVis) {
        this.impRecRitVis.assign(impRecRitVis);
    }

    public AfDecimal getImpRecRitVis() {
        return this.impRecRitVis.copy();
    }

    public void setImpRecRitAcc(AfDecimal impRecRitAcc) {
        this.impRecRitAcc.assign(impRecRitAcc);
    }

    public AfDecimal getImpRecRitAcc() {
        return this.impRecRitAcc.copy();
    }

    public void setFlVarzStatTbgc(char flVarzStatTbgc) {
        this.flVarzStatTbgc = flVarzStatTbgc;
    }

    public char getFlVarzStatTbgc() {
        return this.flVarzStatTbgc;
    }

    public void setFlProvzaMigraz(char flProvzaMigraz) {
        this.flProvzaMigraz = flProvzaMigraz;
    }

    public char getFlProvzaMigraz() {
        return this.flProvzaMigraz;
    }

    public void setImpbVisDaRec(AfDecimal impbVisDaRec) {
        this.impbVisDaRec.assign(impbVisDaRec);
    }

    public AfDecimal getImpbVisDaRec() {
        return this.impbVisDaRec.copy();
    }

    public void setDtDecorPrestBan(int dtDecorPrestBan) {
        this.dtDecorPrestBan = dtDecorPrestBan;
    }

    public int getDtDecorPrestBan() {
        return this.dtDecorPrestBan;
    }

    public void setDtEffVarzStatT(int dtEffVarzStatT) {
        this.dtEffVarzStatT = dtEffVarzStatT;
    }

    public int getDtEffVarzStatT() {
        return this.dtEffVarzStatT;
    }

    public void setDsRiga(long dsRiga) {
        this.dsRiga = dsRiga;
    }

    public long getDsRiga() {
        return this.dsRiga;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dsTsIniCptz = dsTsIniCptz;
    }

    public long getDsTsIniCptz() {
        return this.dsTsIniCptz;
    }

    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dsTsEndCptz = dsTsEndCptz;
    }

    public long getDsTsEndCptz() {
        return this.dsTsEndCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setCumCnbtCap(AfDecimal cumCnbtCap) {
        this.cumCnbtCap.assign(cumCnbtCap);
    }

    public AfDecimal getCumCnbtCap() {
        return this.cumCnbtCap.copy();
    }

    public void setImpGarCnbt(AfDecimal impGarCnbt) {
        this.impGarCnbt.assign(impGarCnbt);
    }

    public AfDecimal getImpGarCnbt() {
        return this.impGarCnbt.copy();
    }

    public void setDtUltConsCnbt(int dtUltConsCnbt) {
        this.dtUltConsCnbt = dtUltConsCnbt;
    }

    public int getDtUltConsCnbt() {
        return this.dtUltConsCnbt;
    }

    public void setIdenIscFnd(String idenIscFnd) {
        this.idenIscFnd = Functions.subString(idenIscFnd, Len.IDEN_ISC_FND);
    }

    public String getIdenIscFnd() {
        return this.idenIscFnd;
    }

    public void setNumRatPian(AfDecimal numRatPian) {
        this.numRatPian.assign(numRatPian);
    }

    public AfDecimal getNumRatPian() {
        return this.numRatPian.copy();
    }

    public void setDtPresc(int dtPresc) {
        this.dtPresc = dtPresc;
    }

    public int getDtPresc() {
        return this.dtPresc;
    }

    public void setConcsPrest(char concsPrest) {
        this.concsPrest = concsPrest;
    }

    public char getConcsPrest() {
        return this.concsPrest;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_ADES = 5;
        public static final int ID_POLI = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int IB_PREV = 40;
        public static final int IB_OGG = 40;
        public static final int COD_COMP_ANIA = 3;
        public static final int DT_DECOR = 5;
        public static final int DT_SCAD = 5;
        public static final int ETA_A_SCAD = 3;
        public static final int DUR_AA = 3;
        public static final int DUR_MM = 3;
        public static final int DUR_GG = 3;
        public static final int TP_RGM_FISC = 2;
        public static final int TP_RIAT = 20;
        public static final int TP_MOD_PAG_TIT = 2;
        public static final int TP_IAS = 2;
        public static final int DT_VARZ_TP_IAS = 5;
        public static final int PRE_NET_IND = 8;
        public static final int PRE_LRD_IND = 8;
        public static final int RAT_LRD_IND = 8;
        public static final int PRSTZ_INI_IND = 8;
        public static final int FL_COINC_ASSTO = 1;
        public static final int IB_DFLT = 40;
        public static final int MOD_CALC = 2;
        public static final int TP_FNT_CNBTVA = 2;
        public static final int IMP_AZ = 8;
        public static final int IMP_ADER = 8;
        public static final int IMP_TFR = 8;
        public static final int IMP_VOLO = 8;
        public static final int PC_AZ = 4;
        public static final int PC_ADER = 4;
        public static final int PC_TFR = 4;
        public static final int PC_VOLO = 4;
        public static final int DT_NOVA_RGM_FISC = 5;
        public static final int FL_ATTIV = 1;
        public static final int IMP_REC_RIT_VIS = 8;
        public static final int IMP_REC_RIT_ACC = 8;
        public static final int FL_VARZ_STAT_TBGC = 1;
        public static final int FL_PROVZA_MIGRAZ = 1;
        public static final int IMPB_VIS_DA_REC = 8;
        public static final int DT_DECOR_PREST_BAN = 5;
        public static final int DT_EFF_VARZ_STAT_T = 5;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int CUM_CNBT_CAP = 8;
        public static final int IMP_GAR_CNBT = 8;
        public static final int DT_ULT_CONS_CNBT = 5;
        public static final int IDEN_ISC_FND = 40;
        public static final int NUM_RAT_PIAN = 7;
        public static final int DT_PRESC = 5;
        public static final int CONCS_PREST = 1;
        public static final int LDBVC821_ADES = ID_ADES + ID_POLI + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + IB_PREV + IB_OGG + COD_COMP_ANIA + DT_DECOR + DT_SCAD + ETA_A_SCAD + DUR_AA + DUR_MM + DUR_GG + TP_RGM_FISC + TP_RIAT + TP_MOD_PAG_TIT + TP_IAS + DT_VARZ_TP_IAS + PRE_NET_IND + PRE_LRD_IND + RAT_LRD_IND + PRSTZ_INI_IND + FL_COINC_ASSTO + IB_DFLT + MOD_CALC + TP_FNT_CNBTVA + IMP_AZ + IMP_ADER + IMP_TFR + IMP_VOLO + PC_AZ + PC_ADER + PC_TFR + PC_VOLO + DT_NOVA_RGM_FISC + FL_ATTIV + IMP_REC_RIT_VIS + IMP_REC_RIT_ACC + FL_VARZ_STAT_TBGC + FL_PROVZA_MIGRAZ + IMPB_VIS_DA_REC + DT_DECOR_PREST_BAN + DT_EFF_VARZ_STAT_T + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + CUM_CNBT_CAP + IMP_GAR_CNBT + DT_ULT_CONS_CNBT + IDEN_ISC_FND + NUM_RAT_PIAN + DT_PRESC + CONCS_PREST;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_ADES = 9;
            public static final int ID_POLI = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_DECOR = 8;
            public static final int DT_SCAD = 8;
            public static final int ETA_A_SCAD = 5;
            public static final int DUR_AA = 5;
            public static final int DUR_MM = 5;
            public static final int DUR_GG = 5;
            public static final int DT_VARZ_TP_IAS = 8;
            public static final int PRE_NET_IND = 12;
            public static final int PRE_LRD_IND = 12;
            public static final int RAT_LRD_IND = 12;
            public static final int PRSTZ_INI_IND = 12;
            public static final int IMP_AZ = 12;
            public static final int IMP_ADER = 12;
            public static final int IMP_TFR = 12;
            public static final int IMP_VOLO = 12;
            public static final int PC_AZ = 3;
            public static final int PC_ADER = 3;
            public static final int PC_TFR = 3;
            public static final int PC_VOLO = 3;
            public static final int DT_NOVA_RGM_FISC = 8;
            public static final int IMP_REC_RIT_VIS = 12;
            public static final int IMP_REC_RIT_ACC = 12;
            public static final int IMPB_VIS_DA_REC = 12;
            public static final int DT_DECOR_PREST_BAN = 8;
            public static final int DT_EFF_VARZ_STAT_T = 8;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int CUM_CNBT_CAP = 12;
            public static final int IMP_GAR_CNBT = 12;
            public static final int DT_ULT_CONS_CNBT = 8;
            public static final int NUM_RAT_PIAN = 7;
            public static final int DT_PRESC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_NET_IND = 3;
            public static final int PRE_LRD_IND = 3;
            public static final int RAT_LRD_IND = 3;
            public static final int PRSTZ_INI_IND = 3;
            public static final int IMP_AZ = 3;
            public static final int IMP_ADER = 3;
            public static final int IMP_TFR = 3;
            public static final int IMP_VOLO = 3;
            public static final int PC_AZ = 3;
            public static final int PC_ADER = 3;
            public static final int PC_TFR = 3;
            public static final int PC_VOLO = 3;
            public static final int IMP_REC_RIT_VIS = 3;
            public static final int IMP_REC_RIT_ACC = 3;
            public static final int IMPB_VIS_DA_REC = 3;
            public static final int CUM_CNBT_CAP = 3;
            public static final int IMP_GAR_CNBT = 3;
            public static final int NUM_RAT_PIAN = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
