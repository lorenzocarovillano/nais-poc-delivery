package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-RICH-INVST-FND<br>
 * Variable: IND-RICH-INVST-FND from copybook IDBVRIF2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndRichInvstFnd {

    //==== PROPERTIES ====
    //Original name: IND-RIF-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-COD-FND
    private short codFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-NUM-QUO
    private short numQuo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-PC
    private short pc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-IMP-MOVTO
    private short impMovto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-DT-INVST
    private short dtInvst = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-COD-TARI
    private short codTari = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-TP-STAT
    private short tpStat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-TP-MOD-INVST
    private short tpModInvst = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-DT-CAMBIO-VLT
    private short dtCambioVlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-DT-INVST-CALC
    private short dtInvstCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-FL-CALC-INVTO
    private short flCalcInvto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-IMP-GAP-EVENT
    private short impGapEvent = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIF-FL-SWM-BP2S
    private short flSwmBp2s = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setCodFnd(short codFnd) {
        this.codFnd = codFnd;
    }

    public short getCodFnd() {
        return this.codFnd;
    }

    public void setNumQuo(short numQuo) {
        this.numQuo = numQuo;
    }

    public short getNumQuo() {
        return this.numQuo;
    }

    public void setPc(short pc) {
        this.pc = pc;
    }

    public short getPc() {
        return this.pc;
    }

    public void setImpMovto(short impMovto) {
        this.impMovto = impMovto;
    }

    public short getImpMovto() {
        return this.impMovto;
    }

    public void setDtInvst(short dtInvst) {
        this.dtInvst = dtInvst;
    }

    public short getDtInvst() {
        return this.dtInvst;
    }

    public void setCodTari(short codTari) {
        this.codTari = codTari;
    }

    public short getCodTari() {
        return this.codTari;
    }

    public void setTpStat(short tpStat) {
        this.tpStat = tpStat;
    }

    public short getTpStat() {
        return this.tpStat;
    }

    public void setTpModInvst(short tpModInvst) {
        this.tpModInvst = tpModInvst;
    }

    public short getTpModInvst() {
        return this.tpModInvst;
    }

    public void setDtCambioVlt(short dtCambioVlt) {
        this.dtCambioVlt = dtCambioVlt;
    }

    public short getDtCambioVlt() {
        return this.dtCambioVlt;
    }

    public void setDtInvstCalc(short dtInvstCalc) {
        this.dtInvstCalc = dtInvstCalc;
    }

    public short getDtInvstCalc() {
        return this.dtInvstCalc;
    }

    public void setFlCalcInvto(short flCalcInvto) {
        this.flCalcInvto = flCalcInvto;
    }

    public short getFlCalcInvto() {
        return this.flCalcInvto;
    }

    public void setImpGapEvent(short impGapEvent) {
        this.impGapEvent = impGapEvent;
    }

    public short getImpGapEvent() {
        return this.impGapEvent;
    }

    public void setFlSwmBp2s(short flSwmBp2s) {
        this.flSwmBp2s = flSwmBp2s;
    }

    public short getFlSwmBp2s() {
        return this.flSwmBp2s;
    }
}
