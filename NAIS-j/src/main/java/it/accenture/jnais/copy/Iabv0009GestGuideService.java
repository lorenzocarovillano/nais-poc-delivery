package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABV0009-GEST-GUIDE-SERVICE<br>
 * Variable: IABV0009-GEST-GUIDE-SERVICE from copybook IABV0002<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Iabv0009GestGuideService {

    //==== PROPERTIES ====
    //Original name: IABV0009-ID-BATCH
    private int idBatch = DefaultValues.BIN_INT_VAL;
    //Original name: IABV0009-ID-OGG-DA
    private int idOggDa = DefaultValues.INT_VAL;
    //Original name: IABV0009-ID-OGG-A
    private int idOggA = DefaultValues.INT_VAL;
    //Original name: IABV0009-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: IABV0009-VERSIONING
    private int versioning = DefaultValues.INT_VAL;
    //Original name: IABV0009-BLOB-DATA-TYPE-REC
    private String blobDataTypeRec = DefaultValues.stringVal(Len.BLOB_DATA_TYPE_REC);
    //Original name: IABV0009-BLOB-DATA-REC
    private String blobDataRec = DefaultValues.stringVal(Len.BLOB_DATA_REC);

    //==== METHODS ====
    public void setIabv0009GestGuideServiceBytes(byte[] buffer, int offset) {
        int position = offset;
        idBatch = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        idOggDa = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG_DA, 0);
        position += Len.ID_OGG_DA;
        idOggA = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG_A, 0);
        position += Len.ID_OGG_A;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        versioning = MarshalByte.readPackedAsInt(buffer, position, Len.Int.VERSIONING, 0);
        position += Len.VERSIONING;
        setBlobDataBytes(buffer, position);
    }

    public byte[] getIabv0009GestGuideServiceBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, idBatch);
        position += Types.INT_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, idOggDa, Len.Int.ID_OGG_DA, 0);
        position += Len.ID_OGG_DA;
        MarshalByte.writeIntAsPacked(buffer, position, idOggA, Len.Int.ID_OGG_A, 0);
        position += Len.ID_OGG_A;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, versioning, Len.Int.VERSIONING, 0);
        position += Len.VERSIONING;
        getBlobDataBytes(buffer, position);
        return buffer;
    }

    public void setIdBatch(int idBatch) {
        this.idBatch = idBatch;
    }

    public int getIdBatch() {
        return this.idBatch;
    }

    public void setIdOggDa(int idOggDa) {
        this.idOggDa = idOggDa;
    }

    public int getIdOggDa() {
        return this.idOggDa;
    }

    public void setIdOggA(int idOggA) {
        this.idOggA = idOggA;
    }

    public int getIdOggA() {
        return this.idOggA;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setVersioning(int versioning) {
        this.versioning = versioning;
    }

    public int getVersioning() {
        return this.versioning;
    }

    public void setBlobDataBytes(byte[] buffer, int offset) {
        int position = offset;
        blobDataTypeRec = MarshalByte.readString(buffer, position, Len.BLOB_DATA_TYPE_REC);
        position += Len.BLOB_DATA_TYPE_REC;
        blobDataRec = MarshalByte.readString(buffer, position, Len.BLOB_DATA_REC);
    }

    public byte[] getBlobDataBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, blobDataTypeRec, Len.BLOB_DATA_TYPE_REC);
        position += Len.BLOB_DATA_TYPE_REC;
        MarshalByte.writeString(buffer, position, blobDataRec, Len.BLOB_DATA_REC);
        return buffer;
    }

    public void setBlobDataTypeRec(String blobDataTypeRec) {
        this.blobDataTypeRec = Functions.subString(blobDataTypeRec, Len.BLOB_DATA_TYPE_REC);
    }

    public String getBlobDataTypeRec() {
        return this.blobDataTypeRec;
    }

    public void setBlobDataRec(String blobDataRec) {
        this.blobDataRec = Functions.subString(blobDataRec, Len.BLOB_DATA_REC);
    }

    public String getBlobDataRec() {
        return this.blobDataRec;
    }

    public String getIabv0009BlobDataRecFormatted() {
        return Functions.padBlanks(getBlobDataRec(), Len.BLOB_DATA_REC);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_BATCH = 4;
        public static final int ID_OGG_DA = 5;
        public static final int ID_OGG_A = 5;
        public static final int TP_OGG = 2;
        public static final int VERSIONING = 5;
        public static final int BLOB_DATA_TYPE_REC = 3;
        public static final int BLOB_DATA_REC = 10000;
        public static final int BLOB_DATA = BLOB_DATA_TYPE_REC + BLOB_DATA_REC;
        public static final int IABV0009_GEST_GUIDE_SERVICE = ID_BATCH + ID_OGG_DA + ID_OGG_A + TP_OGG + VERSIONING + BLOB_DATA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG_DA = 9;
            public static final int ID_OGG_A = 9;
            public static final int VERSIONING = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
