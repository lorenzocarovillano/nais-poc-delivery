package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVBJS3<br>
 * Copybook: IDBVBJS3 from copybook IDBVBJS3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvbjs3 {

    //==== PROPERTIES ====
    //Original name: BJS-DT-START-DB
    private String bjsDtStartDb = DefaultValues.stringVal(Len.BJS_DT_START_DB);
    //Original name: BJS-DT-END-DB
    private String bjsDtEndDb = DefaultValues.stringVal(Len.BJS_DT_END_DB);

    //==== METHODS ====
    public void setBjsDtStartDb(String bjsDtStartDb) {
        this.bjsDtStartDb = Functions.subString(bjsDtStartDb, Len.BJS_DT_START_DB);
    }

    public String getBjsDtStartDb() {
        return this.bjsDtStartDb;
    }

    public void setBjsDtEndDb(String bjsDtEndDb) {
        this.bjsDtEndDb = Functions.subString(bjsDtEndDb, Len.BJS_DT_END_DB);
    }

    public String getBjsDtEndDb() {
        return this.bjsDtEndDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BJS_DT_START_DB = 26;
        public static final int BJS_DT_END_DB = 26;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
