package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: BUS-SERV-OK-REPORT<br>
 * Variable: BUS-SERV-OK-REPORT from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class BusServOkReport {

    //==== PROPERTIES ====
    //Original name: FILLER-BUS-SERV-OK-REPORT
    private String flr1 = "BUSINESS SERVICES ESITO OK";
    //Original name: FILLER-BUS-SERV-OK-REPORT-1
    private String flr2 = " :";
    //Original name: REP-BUS-SERV-ESITO-OK
    private String repBusServEsitoOk = "000000000";

    //==== METHODS ====
    public String getBusServOkReportFormatted() {
        return MarshalByteExt.bufferToStr(getBusServOkReportBytes());
    }

    public byte[] getBusServOkReportBytes() {
        byte[] buffer = new byte[Len.BUS_SERV_OK_REPORT];
        return getBusServOkReportBytes(buffer, 1);
    }

    public byte[] getBusServOkReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, repBusServEsitoOk, Len.REP_BUS_SERV_ESITO_OK);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public void setRepBusServEsitoOk(long repBusServEsitoOk) {
        this.repBusServEsitoOk = PicFormatter.display("Z(8)9").format(repBusServEsitoOk).toString();
    }

    public long getRepBusServEsitoOk() {
        return PicParser.display("Z(8)9").parseLong(this.repBusServEsitoOk);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int REP_BUS_SERV_ESITO_OK = 9;
        public static final int FLR1 = 50;
        public static final int FLR2 = 4;
        public static final int BUS_SERV_OK_REPORT = REP_BUS_SERV_ESITO_OK + FLR1 + FLR2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
