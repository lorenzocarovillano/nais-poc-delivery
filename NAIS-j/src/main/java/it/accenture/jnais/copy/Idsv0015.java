package it.accenture.jnais.copy;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IDSV0015<br>
 * Variable: IDSV0015 from copybook IDSV0015<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv0015 {

    //==== PROPERTIES ====
    //Original name: WS-DT-INFINITO-1-N
    private int dtInfinito1N = 99991230;
    //Original name: WS-TS-INFINITO-1-N
    private long tsInfinito1N = 999912304023595999L;

    //==== METHODS ====
    public int getDtInfinito1N() {
        return this.dtInfinito1N;
    }

    public long getTsInfinito1N() {
        return this.tsInfinito1N;
    }
}
