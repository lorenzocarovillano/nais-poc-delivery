package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Wb05DtRis;
import it.accenture.jnais.ws.redefines.Wb05IdRichEstrazAgg;
import it.accenture.jnais.ws.redefines.Wb05ProgSchedaValor;
import it.accenture.jnais.ws.redefines.Wb05ValImp;
import it.accenture.jnais.ws.redefines.Wb05ValPc;

/**Original name: WB05-DATI<br>
 * Variable: WB05-DATI from copybook LCCVB051<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wb05Dati {

    //==== PROPERTIES ====
    //Original name: WB05-ID-BILA-VAR-CALC-T
    private int wb05IdBilaVarCalcT = DefaultValues.INT_VAL;
    //Original name: WB05-COD-COMP-ANIA
    private int wb05CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WB05-ID-BILA-TRCH-ESTR
    private int wb05IdBilaTrchEstr = DefaultValues.INT_VAL;
    //Original name: WB05-ID-RICH-ESTRAZ-MAS
    private int wb05IdRichEstrazMas = DefaultValues.INT_VAL;
    //Original name: WB05-ID-RICH-ESTRAZ-AGG
    private Wb05IdRichEstrazAgg wb05IdRichEstrazAgg = new Wb05IdRichEstrazAgg();
    //Original name: WB05-DT-RIS
    private Wb05DtRis wb05DtRis = new Wb05DtRis();
    //Original name: WB05-ID-POLI
    private int wb05IdPoli = DefaultValues.INT_VAL;
    //Original name: WB05-ID-ADES
    private int wb05IdAdes = DefaultValues.INT_VAL;
    //Original name: WB05-ID-TRCH-DI-GAR
    private int wb05IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: WB05-PROG-SCHEDA-VALOR
    private Wb05ProgSchedaValor wb05ProgSchedaValor = new Wb05ProgSchedaValor();
    //Original name: WB05-DT-INI-VLDT-TARI
    private int wb05DtIniVldtTari = DefaultValues.INT_VAL;
    //Original name: WB05-TP-RGM-FISC
    private String wb05TpRgmFisc = DefaultValues.stringVal(Len.WB05_TP_RGM_FISC);
    //Original name: WB05-DT-INI-VLDT-PROD
    private int wb05DtIniVldtProd = DefaultValues.INT_VAL;
    //Original name: WB05-DT-DECOR-TRCH
    private int wb05DtDecorTrch = DefaultValues.INT_VAL;
    //Original name: WB05-COD-VAR
    private String wb05CodVar = DefaultValues.stringVal(Len.WB05_COD_VAR);
    //Original name: WB05-TP-D
    private char wb05TpD = DefaultValues.CHAR_VAL;
    //Original name: WB05-VAL-IMP
    private Wb05ValImp wb05ValImp = new Wb05ValImp();
    //Original name: WB05-VAL-PC
    private Wb05ValPc wb05ValPc = new Wb05ValPc();
    //Original name: WB05-VAL-STRINGA
    private String wb05ValStringa = DefaultValues.stringVal(Len.WB05_VAL_STRINGA);
    //Original name: WB05-DS-OPER-SQL
    private char wb05DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WB05-DS-VER
    private int wb05DsVer = DefaultValues.INT_VAL;
    //Original name: WB05-DS-TS-CPTZ
    private long wb05DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WB05-DS-UTENTE
    private String wb05DsUtente = DefaultValues.stringVal(Len.WB05_DS_UTENTE);
    //Original name: WB05-DS-STATO-ELAB
    private char wb05DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WB05-AREA-D-VALOR-VAR-LEN
    private short wb05AreaDValorVarLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: WB05-AREA-D-VALOR-VAR
    private String wb05AreaDValorVar = DefaultValues.stringVal(Len.WB05_AREA_D_VALOR_VAR);

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wb05IdBilaVarCalcT = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_BILA_VAR_CALC_T, 0);
        position += Len.WB05_ID_BILA_VAR_CALC_T;
        wb05CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_COD_COMP_ANIA, 0);
        position += Len.WB05_COD_COMP_ANIA;
        wb05IdBilaTrchEstr = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_BILA_TRCH_ESTR, 0);
        position += Len.WB05_ID_BILA_TRCH_ESTR;
        wb05IdRichEstrazMas = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.WB05_ID_RICH_ESTRAZ_MAS;
        wb05IdRichEstrazAgg.setWb05IdRichEstrazAggFromBuffer(buffer, position);
        position += Wb05IdRichEstrazAgg.Len.WB05_ID_RICH_ESTRAZ_AGG;
        wb05DtRis.setWb05DtRisFromBuffer(buffer, position);
        position += Wb05DtRis.Len.WB05_DT_RIS;
        wb05IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_POLI, 0);
        position += Len.WB05_ID_POLI;
        wb05IdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_ADES, 0);
        position += Len.WB05_ID_ADES;
        wb05IdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_TRCH_DI_GAR, 0);
        position += Len.WB05_ID_TRCH_DI_GAR;
        wb05ProgSchedaValor.setWb05ProgSchedaValorFromBuffer(buffer, position);
        position += Wb05ProgSchedaValor.Len.WB05_PROG_SCHEDA_VALOR;
        wb05DtIniVldtTari = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_DT_INI_VLDT_TARI, 0);
        position += Len.WB05_DT_INI_VLDT_TARI;
        wb05TpRgmFisc = MarshalByte.readString(buffer, position, Len.WB05_TP_RGM_FISC);
        position += Len.WB05_TP_RGM_FISC;
        wb05DtIniVldtProd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_DT_INI_VLDT_PROD, 0);
        position += Len.WB05_DT_INI_VLDT_PROD;
        wb05DtDecorTrch = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_DT_DECOR_TRCH, 0);
        position += Len.WB05_DT_DECOR_TRCH;
        wb05CodVar = MarshalByte.readString(buffer, position, Len.WB05_COD_VAR);
        position += Len.WB05_COD_VAR;
        wb05TpD = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb05ValImp.setWb05ValImpFromBuffer(buffer, position);
        position += Wb05ValImp.Len.WB05_VAL_IMP;
        wb05ValPc.setWb05ValPcFromBuffer(buffer, position);
        position += Wb05ValPc.Len.WB05_VAL_PC;
        wb05ValStringa = MarshalByte.readString(buffer, position, Len.WB05_VAL_STRINGA);
        position += Len.WB05_VAL_STRINGA;
        wb05DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb05DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_DS_VER, 0);
        position += Len.WB05_DS_VER;
        wb05DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WB05_DS_TS_CPTZ, 0);
        position += Len.WB05_DS_TS_CPTZ;
        wb05DsUtente = MarshalByte.readString(buffer, position, Len.WB05_DS_UTENTE);
        position += Len.WB05_DS_UTENTE;
        wb05DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setWb05AreaDValorVarVcharBytes(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wb05IdBilaVarCalcT, Len.Int.WB05_ID_BILA_VAR_CALC_T, 0);
        position += Len.WB05_ID_BILA_VAR_CALC_T;
        MarshalByte.writeIntAsPacked(buffer, position, wb05CodCompAnia, Len.Int.WB05_COD_COMP_ANIA, 0);
        position += Len.WB05_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wb05IdBilaTrchEstr, Len.Int.WB05_ID_BILA_TRCH_ESTR, 0);
        position += Len.WB05_ID_BILA_TRCH_ESTR;
        MarshalByte.writeIntAsPacked(buffer, position, wb05IdRichEstrazMas, Len.Int.WB05_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.WB05_ID_RICH_ESTRAZ_MAS;
        wb05IdRichEstrazAgg.getWb05IdRichEstrazAggAsBuffer(buffer, position);
        position += Wb05IdRichEstrazAgg.Len.WB05_ID_RICH_ESTRAZ_AGG;
        wb05DtRis.getWb05DtRisAsBuffer(buffer, position);
        position += Wb05DtRis.Len.WB05_DT_RIS;
        MarshalByte.writeIntAsPacked(buffer, position, wb05IdPoli, Len.Int.WB05_ID_POLI, 0);
        position += Len.WB05_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wb05IdAdes, Len.Int.WB05_ID_ADES, 0);
        position += Len.WB05_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wb05IdTrchDiGar, Len.Int.WB05_ID_TRCH_DI_GAR, 0);
        position += Len.WB05_ID_TRCH_DI_GAR;
        wb05ProgSchedaValor.getWb05ProgSchedaValorAsBuffer(buffer, position);
        position += Wb05ProgSchedaValor.Len.WB05_PROG_SCHEDA_VALOR;
        MarshalByte.writeIntAsPacked(buffer, position, wb05DtIniVldtTari, Len.Int.WB05_DT_INI_VLDT_TARI, 0);
        position += Len.WB05_DT_INI_VLDT_TARI;
        MarshalByte.writeString(buffer, position, wb05TpRgmFisc, Len.WB05_TP_RGM_FISC);
        position += Len.WB05_TP_RGM_FISC;
        MarshalByte.writeIntAsPacked(buffer, position, wb05DtIniVldtProd, Len.Int.WB05_DT_INI_VLDT_PROD, 0);
        position += Len.WB05_DT_INI_VLDT_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, wb05DtDecorTrch, Len.Int.WB05_DT_DECOR_TRCH, 0);
        position += Len.WB05_DT_DECOR_TRCH;
        MarshalByte.writeString(buffer, position, wb05CodVar, Len.WB05_COD_VAR);
        position += Len.WB05_COD_VAR;
        MarshalByte.writeChar(buffer, position, wb05TpD);
        position += Types.CHAR_SIZE;
        wb05ValImp.getWb05ValImpAsBuffer(buffer, position);
        position += Wb05ValImp.Len.WB05_VAL_IMP;
        wb05ValPc.getWb05ValPcAsBuffer(buffer, position);
        position += Wb05ValPc.Len.WB05_VAL_PC;
        MarshalByte.writeString(buffer, position, wb05ValStringa, Len.WB05_VAL_STRINGA);
        position += Len.WB05_VAL_STRINGA;
        MarshalByte.writeChar(buffer, position, wb05DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wb05DsVer, Len.Int.WB05_DS_VER, 0);
        position += Len.WB05_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wb05DsTsCptz, Len.Int.WB05_DS_TS_CPTZ, 0);
        position += Len.WB05_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wb05DsUtente, Len.WB05_DS_UTENTE);
        position += Len.WB05_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wb05DsStatoElab);
        position += Types.CHAR_SIZE;
        getWb05AreaDValorVarVcharBytes(buffer, position);
        return buffer;
    }

    public void setWb05IdBilaVarCalcT(int wb05IdBilaVarCalcT) {
        this.wb05IdBilaVarCalcT = wb05IdBilaVarCalcT;
    }

    public int getWb05IdBilaVarCalcT() {
        return this.wb05IdBilaVarCalcT;
    }

    public void setWb05CodCompAnia(int wb05CodCompAnia) {
        this.wb05CodCompAnia = wb05CodCompAnia;
    }

    public int getWb05CodCompAnia() {
        return this.wb05CodCompAnia;
    }

    public void setWb05IdBilaTrchEstr(int wb05IdBilaTrchEstr) {
        this.wb05IdBilaTrchEstr = wb05IdBilaTrchEstr;
    }

    public int getWb05IdBilaTrchEstr() {
        return this.wb05IdBilaTrchEstr;
    }

    public void setWb05IdRichEstrazMas(int wb05IdRichEstrazMas) {
        this.wb05IdRichEstrazMas = wb05IdRichEstrazMas;
    }

    public int getWb05IdRichEstrazMas() {
        return this.wb05IdRichEstrazMas;
    }

    public void setWb05IdPoli(int wb05IdPoli) {
        this.wb05IdPoli = wb05IdPoli;
    }

    public int getWb05IdPoli() {
        return this.wb05IdPoli;
    }

    public void setWb05IdAdes(int wb05IdAdes) {
        this.wb05IdAdes = wb05IdAdes;
    }

    public int getWb05IdAdes() {
        return this.wb05IdAdes;
    }

    public void setWb05IdTrchDiGar(int wb05IdTrchDiGar) {
        this.wb05IdTrchDiGar = wb05IdTrchDiGar;
    }

    public int getWb05IdTrchDiGar() {
        return this.wb05IdTrchDiGar;
    }

    public void setWb05DtIniVldtTari(int wb05DtIniVldtTari) {
        this.wb05DtIniVldtTari = wb05DtIniVldtTari;
    }

    public void setWb05DtIniVldtTariFormatted(String wb05DtIniVldtTari) {
        setWb05DtIniVldtTari(PicParser.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).parseInt(wb05DtIniVldtTari));
    }

    public int getWb05DtIniVldtTari() {
        return this.wb05DtIniVldtTari;
    }

    public void setWb05TpRgmFisc(String wb05TpRgmFisc) {
        this.wb05TpRgmFisc = Functions.subString(wb05TpRgmFisc, Len.WB05_TP_RGM_FISC);
    }

    public String getWb05TpRgmFisc() {
        return this.wb05TpRgmFisc;
    }

    public void setWb05DtIniVldtProd(int wb05DtIniVldtProd) {
        this.wb05DtIniVldtProd = wb05DtIniVldtProd;
    }

    public void setWb05DtIniVldtProdFormatted(String wb05DtIniVldtProd) {
        setWb05DtIniVldtProd(PicParser.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).parseInt(wb05DtIniVldtProd));
    }

    public int getWb05DtIniVldtProd() {
        return this.wb05DtIniVldtProd;
    }

    public void setWb05DtDecorTrch(int wb05DtDecorTrch) {
        this.wb05DtDecorTrch = wb05DtDecorTrch;
    }

    public void setWb05DtDecorTrchFormatted(String wb05DtDecorTrch) {
        setWb05DtDecorTrch(PicParser.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).parseInt(wb05DtDecorTrch));
    }

    public int getWb05DtDecorTrch() {
        return this.wb05DtDecorTrch;
    }

    public void setWb05CodVar(String wb05CodVar) {
        this.wb05CodVar = Functions.subString(wb05CodVar, Len.WB05_COD_VAR);
    }

    public String getWb05CodVar() {
        return this.wb05CodVar;
    }

    public void setWb05TpD(char wb05TpD) {
        this.wb05TpD = wb05TpD;
    }

    public char getWb05TpD() {
        return this.wb05TpD;
    }

    public void setWb05ValStringa(String wb05ValStringa) {
        this.wb05ValStringa = Functions.subString(wb05ValStringa, Len.WB05_VAL_STRINGA);
    }

    public String getWb05ValStringa() {
        return this.wb05ValStringa;
    }

    public void setWb05DsOperSql(char wb05DsOperSql) {
        this.wb05DsOperSql = wb05DsOperSql;
    }

    public char getWb05DsOperSql() {
        return this.wb05DsOperSql;
    }

    public void setWb05DsVer(int wb05DsVer) {
        this.wb05DsVer = wb05DsVer;
    }

    public int getWb05DsVer() {
        return this.wb05DsVer;
    }

    public void setWb05DsTsCptz(long wb05DsTsCptz) {
        this.wb05DsTsCptz = wb05DsTsCptz;
    }

    public long getWb05DsTsCptz() {
        return this.wb05DsTsCptz;
    }

    public void setWb05DsUtente(String wb05DsUtente) {
        this.wb05DsUtente = Functions.subString(wb05DsUtente, Len.WB05_DS_UTENTE);
    }

    public String getWb05DsUtente() {
        return this.wb05DsUtente;
    }

    public void setWb05DsStatoElab(char wb05DsStatoElab) {
        this.wb05DsStatoElab = wb05DsStatoElab;
    }

    public char getWb05DsStatoElab() {
        return this.wb05DsStatoElab;
    }

    /**Original name: WB05-AREA-D-VALOR-VAR-VCHAR<br>*/
    public byte[] getWb05AreaDValorVarVcharBytes() {
        byte[] buffer = new byte[Len.WB05_AREA_D_VALOR_VAR_VCHAR];
        return getWb05AreaDValorVarVcharBytes(buffer, 1);
    }

    public void setWb05AreaDValorVarVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        wb05AreaDValorVarLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wb05AreaDValorVar = MarshalByte.readString(buffer, position, Len.WB05_AREA_D_VALOR_VAR);
    }

    public byte[] getWb05AreaDValorVarVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wb05AreaDValorVarLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, wb05AreaDValorVar, Len.WB05_AREA_D_VALOR_VAR);
        return buffer;
    }

    public void setWb05AreaDValorVarLen(short wb05AreaDValorVarLen) {
        this.wb05AreaDValorVarLen = wb05AreaDValorVarLen;
    }

    public short getWb05AreaDValorVarLen() {
        return this.wb05AreaDValorVarLen;
    }

    public void setWb05AreaDValorVar(String wb05AreaDValorVar) {
        this.wb05AreaDValorVar = Functions.subString(wb05AreaDValorVar, Len.WB05_AREA_D_VALOR_VAR);
    }

    public String getWb05AreaDValorVar() {
        return this.wb05AreaDValorVar;
    }

    public Wb05DtRis getWb05DtRis() {
        return wb05DtRis;
    }

    public Wb05IdRichEstrazAgg getWb05IdRichEstrazAgg() {
        return wb05IdRichEstrazAgg;
    }

    public Wb05ProgSchedaValor getWb05ProgSchedaValor() {
        return wb05ProgSchedaValor;
    }

    public Wb05ValImp getWb05ValImp() {
        return wb05ValImp;
    }

    public Wb05ValPc getWb05ValPc() {
        return wb05ValPc;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WB05_ID_BILA_VAR_CALC_T = 5;
        public static final int WB05_COD_COMP_ANIA = 3;
        public static final int WB05_ID_BILA_TRCH_ESTR = 5;
        public static final int WB05_ID_RICH_ESTRAZ_MAS = 5;
        public static final int WB05_ID_POLI = 5;
        public static final int WB05_ID_ADES = 5;
        public static final int WB05_ID_TRCH_DI_GAR = 5;
        public static final int WB05_DT_INI_VLDT_TARI = 5;
        public static final int WB05_TP_RGM_FISC = 2;
        public static final int WB05_DT_INI_VLDT_PROD = 5;
        public static final int WB05_DT_DECOR_TRCH = 5;
        public static final int WB05_COD_VAR = 30;
        public static final int WB05_TP_D = 1;
        public static final int WB05_VAL_STRINGA = 60;
        public static final int WB05_DS_OPER_SQL = 1;
        public static final int WB05_DS_VER = 5;
        public static final int WB05_DS_TS_CPTZ = 10;
        public static final int WB05_DS_UTENTE = 20;
        public static final int WB05_DS_STATO_ELAB = 1;
        public static final int WB05_AREA_D_VALOR_VAR_LEN = 2;
        public static final int WB05_AREA_D_VALOR_VAR = 4000;
        public static final int WB05_AREA_D_VALOR_VAR_VCHAR = WB05_AREA_D_VALOR_VAR_LEN + WB05_AREA_D_VALOR_VAR;
        public static final int DATI = WB05_ID_BILA_VAR_CALC_T + WB05_COD_COMP_ANIA + WB05_ID_BILA_TRCH_ESTR + WB05_ID_RICH_ESTRAZ_MAS + Wb05IdRichEstrazAgg.Len.WB05_ID_RICH_ESTRAZ_AGG + Wb05DtRis.Len.WB05_DT_RIS + WB05_ID_POLI + WB05_ID_ADES + WB05_ID_TRCH_DI_GAR + Wb05ProgSchedaValor.Len.WB05_PROG_SCHEDA_VALOR + WB05_DT_INI_VLDT_TARI + WB05_TP_RGM_FISC + WB05_DT_INI_VLDT_PROD + WB05_DT_DECOR_TRCH + WB05_COD_VAR + WB05_TP_D + Wb05ValImp.Len.WB05_VAL_IMP + Wb05ValPc.Len.WB05_VAL_PC + WB05_VAL_STRINGA + WB05_DS_OPER_SQL + WB05_DS_VER + WB05_DS_TS_CPTZ + WB05_DS_UTENTE + WB05_DS_STATO_ELAB + WB05_AREA_D_VALOR_VAR_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB05_ID_BILA_VAR_CALC_T = 9;
            public static final int WB05_COD_COMP_ANIA = 5;
            public static final int WB05_ID_BILA_TRCH_ESTR = 9;
            public static final int WB05_ID_RICH_ESTRAZ_MAS = 9;
            public static final int WB05_ID_POLI = 9;
            public static final int WB05_ID_ADES = 9;
            public static final int WB05_ID_TRCH_DI_GAR = 9;
            public static final int WB05_DT_INI_VLDT_TARI = 8;
            public static final int WB05_DT_INI_VLDT_PROD = 8;
            public static final int WB05_DT_DECOR_TRCH = 8;
            public static final int WB05_DS_VER = 9;
            public static final int WB05_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
