package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.B05DtRis;
import it.accenture.jnais.ws.redefines.B05IdRichEstrazAgg;
import it.accenture.jnais.ws.redefines.B05ProgSchedaValor;
import it.accenture.jnais.ws.redefines.B05ValImp;
import it.accenture.jnais.ws.redefines.B05ValPc;

/**Original name: BILA-VAR-CALC-T<br>
 * Variable: BILA-VAR-CALC-T from copybook IDBVB051<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class BilaVarCalcT {

    //==== PROPERTIES ====
    //Original name: B05-ID-BILA-VAR-CALC-T
    private int b05IdBilaVarCalcT = DefaultValues.INT_VAL;
    //Original name: B05-COD-COMP-ANIA
    private int b05CodCompAnia = DefaultValues.INT_VAL;
    //Original name: B05-ID-BILA-TRCH-ESTR
    private int b05IdBilaTrchEstr = DefaultValues.INT_VAL;
    //Original name: B05-ID-RICH-ESTRAZ-MAS
    private int b05IdRichEstrazMas = DefaultValues.INT_VAL;
    //Original name: B05-ID-RICH-ESTRAZ-AGG
    private B05IdRichEstrazAgg b05IdRichEstrazAgg = new B05IdRichEstrazAgg();
    //Original name: B05-DT-RIS
    private B05DtRis b05DtRis = new B05DtRis();
    //Original name: B05-ID-POLI
    private int b05IdPoli = DefaultValues.INT_VAL;
    //Original name: B05-ID-ADES
    private int b05IdAdes = DefaultValues.INT_VAL;
    //Original name: B05-ID-TRCH-DI-GAR
    private int b05IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: B05-PROG-SCHEDA-VALOR
    private B05ProgSchedaValor b05ProgSchedaValor = new B05ProgSchedaValor();
    //Original name: B05-DT-INI-VLDT-TARI
    private int b05DtIniVldtTari = DefaultValues.INT_VAL;
    //Original name: B05-TP-RGM-FISC
    private String b05TpRgmFisc = DefaultValues.stringVal(Len.B05_TP_RGM_FISC);
    //Original name: B05-DT-INI-VLDT-PROD
    private int b05DtIniVldtProd = DefaultValues.INT_VAL;
    //Original name: B05-DT-DECOR-TRCH
    private int b05DtDecorTrch = DefaultValues.INT_VAL;
    //Original name: B05-COD-VAR
    private String b05CodVar = DefaultValues.stringVal(Len.B05_COD_VAR);
    //Original name: B05-TP-D
    private char b05TpD = DefaultValues.CHAR_VAL;
    //Original name: B05-VAL-IMP
    private B05ValImp b05ValImp = new B05ValImp();
    //Original name: B05-VAL-PC
    private B05ValPc b05ValPc = new B05ValPc();
    //Original name: B05-VAL-STRINGA
    private String b05ValStringa = DefaultValues.stringVal(Len.B05_VAL_STRINGA);
    //Original name: B05-DS-OPER-SQL
    private char b05DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: B05-DS-VER
    private int b05DsVer = DefaultValues.INT_VAL;
    //Original name: B05-DS-TS-CPTZ
    private long b05DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: B05-DS-UTENTE
    private String b05DsUtente = DefaultValues.stringVal(Len.B05_DS_UTENTE);
    //Original name: B05-DS-STATO-ELAB
    private char b05DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: B05-AREA-D-VALOR-VAR-LEN
    private short b05AreaDValorVarLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: B05-AREA-D-VALOR-VAR
    private String b05AreaDValorVar = DefaultValues.stringVal(Len.B05_AREA_D_VALOR_VAR);

    //==== METHODS ====
    public String getBilaVarCalcTFormatted() {
        return MarshalByteExt.bufferToStr(getBilaVarCalcTBytes());
    }

    public byte[] getBilaVarCalcTBytes() {
        byte[] buffer = new byte[Len.BILA_VAR_CALC_T];
        return getBilaVarCalcTBytes(buffer, 1);
    }

    public byte[] getBilaVarCalcTBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, b05IdBilaVarCalcT, Len.Int.B05_ID_BILA_VAR_CALC_T, 0);
        position += Len.B05_ID_BILA_VAR_CALC_T;
        MarshalByte.writeIntAsPacked(buffer, position, b05CodCompAnia, Len.Int.B05_COD_COMP_ANIA, 0);
        position += Len.B05_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, b05IdBilaTrchEstr, Len.Int.B05_ID_BILA_TRCH_ESTR, 0);
        position += Len.B05_ID_BILA_TRCH_ESTR;
        MarshalByte.writeIntAsPacked(buffer, position, b05IdRichEstrazMas, Len.Int.B05_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.B05_ID_RICH_ESTRAZ_MAS;
        b05IdRichEstrazAgg.getB05IdRichEstrazAggAsBuffer(buffer, position);
        position += B05IdRichEstrazAgg.Len.B05_ID_RICH_ESTRAZ_AGG;
        b05DtRis.getB05DtRisAsBuffer(buffer, position);
        position += B05DtRis.Len.B05_DT_RIS;
        MarshalByte.writeIntAsPacked(buffer, position, b05IdPoli, Len.Int.B05_ID_POLI, 0);
        position += Len.B05_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, b05IdAdes, Len.Int.B05_ID_ADES, 0);
        position += Len.B05_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, b05IdTrchDiGar, Len.Int.B05_ID_TRCH_DI_GAR, 0);
        position += Len.B05_ID_TRCH_DI_GAR;
        b05ProgSchedaValor.getB05ProgSchedaValorAsBuffer(buffer, position);
        position += B05ProgSchedaValor.Len.B05_PROG_SCHEDA_VALOR;
        MarshalByte.writeIntAsPacked(buffer, position, b05DtIniVldtTari, Len.Int.B05_DT_INI_VLDT_TARI, 0);
        position += Len.B05_DT_INI_VLDT_TARI;
        MarshalByte.writeString(buffer, position, b05TpRgmFisc, Len.B05_TP_RGM_FISC);
        position += Len.B05_TP_RGM_FISC;
        MarshalByte.writeIntAsPacked(buffer, position, b05DtIniVldtProd, Len.Int.B05_DT_INI_VLDT_PROD, 0);
        position += Len.B05_DT_INI_VLDT_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, b05DtDecorTrch, Len.Int.B05_DT_DECOR_TRCH, 0);
        position += Len.B05_DT_DECOR_TRCH;
        MarshalByte.writeString(buffer, position, b05CodVar, Len.B05_COD_VAR);
        position += Len.B05_COD_VAR;
        MarshalByte.writeChar(buffer, position, b05TpD);
        position += Types.CHAR_SIZE;
        b05ValImp.getB05ValImpAsBuffer(buffer, position);
        position += B05ValImp.Len.B05_VAL_IMP;
        b05ValPc.getB05ValPcAsBuffer(buffer, position);
        position += B05ValPc.Len.B05_VAL_PC;
        MarshalByte.writeString(buffer, position, b05ValStringa, Len.B05_VAL_STRINGA);
        position += Len.B05_VAL_STRINGA;
        MarshalByte.writeChar(buffer, position, b05DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, b05DsVer, Len.Int.B05_DS_VER, 0);
        position += Len.B05_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, b05DsTsCptz, Len.Int.B05_DS_TS_CPTZ, 0);
        position += Len.B05_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, b05DsUtente, Len.B05_DS_UTENTE);
        position += Len.B05_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, b05DsStatoElab);
        position += Types.CHAR_SIZE;
        getB05AreaDValorVarVcharBytes(buffer, position);
        return buffer;
    }

    public void setB05IdBilaVarCalcT(int b05IdBilaVarCalcT) {
        this.b05IdBilaVarCalcT = b05IdBilaVarCalcT;
    }

    public int getB05IdBilaVarCalcT() {
        return this.b05IdBilaVarCalcT;
    }

    public void setB05CodCompAnia(int b05CodCompAnia) {
        this.b05CodCompAnia = b05CodCompAnia;
    }

    public int getB05CodCompAnia() {
        return this.b05CodCompAnia;
    }

    public void setB05IdBilaTrchEstr(int b05IdBilaTrchEstr) {
        this.b05IdBilaTrchEstr = b05IdBilaTrchEstr;
    }

    public int getB05IdBilaTrchEstr() {
        return this.b05IdBilaTrchEstr;
    }

    public void setB05IdRichEstrazMas(int b05IdRichEstrazMas) {
        this.b05IdRichEstrazMas = b05IdRichEstrazMas;
    }

    public int getB05IdRichEstrazMas() {
        return this.b05IdRichEstrazMas;
    }

    public void setB05IdPoli(int b05IdPoli) {
        this.b05IdPoli = b05IdPoli;
    }

    public int getB05IdPoli() {
        return this.b05IdPoli;
    }

    public void setB05IdAdes(int b05IdAdes) {
        this.b05IdAdes = b05IdAdes;
    }

    public int getB05IdAdes() {
        return this.b05IdAdes;
    }

    public void setB05IdTrchDiGar(int b05IdTrchDiGar) {
        this.b05IdTrchDiGar = b05IdTrchDiGar;
    }

    public int getB05IdTrchDiGar() {
        return this.b05IdTrchDiGar;
    }

    public void setB05DtIniVldtTari(int b05DtIniVldtTari) {
        this.b05DtIniVldtTari = b05DtIniVldtTari;
    }

    public int getB05DtIniVldtTari() {
        return this.b05DtIniVldtTari;
    }

    public void setB05TpRgmFisc(String b05TpRgmFisc) {
        this.b05TpRgmFisc = Functions.subString(b05TpRgmFisc, Len.B05_TP_RGM_FISC);
    }

    public String getB05TpRgmFisc() {
        return this.b05TpRgmFisc;
    }

    public void setB05DtIniVldtProd(int b05DtIniVldtProd) {
        this.b05DtIniVldtProd = b05DtIniVldtProd;
    }

    public int getB05DtIniVldtProd() {
        return this.b05DtIniVldtProd;
    }

    public void setB05DtDecorTrch(int b05DtDecorTrch) {
        this.b05DtDecorTrch = b05DtDecorTrch;
    }

    public int getB05DtDecorTrch() {
        return this.b05DtDecorTrch;
    }

    public void setB05CodVar(String b05CodVar) {
        this.b05CodVar = Functions.subString(b05CodVar, Len.B05_COD_VAR);
    }

    public String getB05CodVar() {
        return this.b05CodVar;
    }

    public void setB05TpD(char b05TpD) {
        this.b05TpD = b05TpD;
    }

    public char getB05TpD() {
        return this.b05TpD;
    }

    public void setB05ValStringa(String b05ValStringa) {
        this.b05ValStringa = Functions.subString(b05ValStringa, Len.B05_VAL_STRINGA);
    }

    public String getB05ValStringa() {
        return this.b05ValStringa;
    }

    public void setB05DsOperSql(char b05DsOperSql) {
        this.b05DsOperSql = b05DsOperSql;
    }

    public char getB05DsOperSql() {
        return this.b05DsOperSql;
    }

    public void setB05DsVer(int b05DsVer) {
        this.b05DsVer = b05DsVer;
    }

    public int getB05DsVer() {
        return this.b05DsVer;
    }

    public void setB05DsTsCptz(long b05DsTsCptz) {
        this.b05DsTsCptz = b05DsTsCptz;
    }

    public long getB05DsTsCptz() {
        return this.b05DsTsCptz;
    }

    public void setB05DsUtente(String b05DsUtente) {
        this.b05DsUtente = Functions.subString(b05DsUtente, Len.B05_DS_UTENTE);
    }

    public String getB05DsUtente() {
        return this.b05DsUtente;
    }

    public void setB05DsStatoElab(char b05DsStatoElab) {
        this.b05DsStatoElab = b05DsStatoElab;
    }

    public char getB05DsStatoElab() {
        return this.b05DsStatoElab;
    }

    public void setB05AreaDValorVarVcharBytes(byte[] buffer) {
        setB05AreaDValorVarVcharBytes(buffer, 1);
    }

    public void setB05AreaDValorVarVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        b05AreaDValorVarLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        b05AreaDValorVar = MarshalByte.readString(buffer, position, Len.B05_AREA_D_VALOR_VAR);
    }

    public byte[] getB05AreaDValorVarVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, b05AreaDValorVarLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, b05AreaDValorVar, Len.B05_AREA_D_VALOR_VAR);
        return buffer;
    }

    public void setB05AreaDValorVarLen(short b05AreaDValorVarLen) {
        this.b05AreaDValorVarLen = b05AreaDValorVarLen;
    }

    public short getB05AreaDValorVarLen() {
        return this.b05AreaDValorVarLen;
    }

    public void setB05AreaDValorVar(String b05AreaDValorVar) {
        this.b05AreaDValorVar = Functions.subString(b05AreaDValorVar, Len.B05_AREA_D_VALOR_VAR);
    }

    public String getB05AreaDValorVar() {
        return this.b05AreaDValorVar;
    }

    public B05DtRis getB05DtRis() {
        return b05DtRis;
    }

    public B05IdRichEstrazAgg getB05IdRichEstrazAgg() {
        return b05IdRichEstrazAgg;
    }

    public B05ProgSchedaValor getB05ProgSchedaValor() {
        return b05ProgSchedaValor;
    }

    public B05ValImp getB05ValImp() {
        return b05ValImp;
    }

    public B05ValPc getB05ValPc() {
        return b05ValPc;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int B05_TP_RGM_FISC = 2;
        public static final int B05_COD_VAR = 30;
        public static final int B05_VAL_STRINGA = 60;
        public static final int B05_DS_UTENTE = 20;
        public static final int B05_AREA_D_VALOR_VAR = 4000;
        public static final int B05_ID_BILA_VAR_CALC_T = 5;
        public static final int B05_COD_COMP_ANIA = 3;
        public static final int B05_ID_BILA_TRCH_ESTR = 5;
        public static final int B05_ID_RICH_ESTRAZ_MAS = 5;
        public static final int B05_ID_POLI = 5;
        public static final int B05_ID_ADES = 5;
        public static final int B05_ID_TRCH_DI_GAR = 5;
        public static final int B05_DT_INI_VLDT_TARI = 5;
        public static final int B05_DT_INI_VLDT_PROD = 5;
        public static final int B05_DT_DECOR_TRCH = 5;
        public static final int B05_TP_D = 1;
        public static final int B05_DS_OPER_SQL = 1;
        public static final int B05_DS_VER = 5;
        public static final int B05_DS_TS_CPTZ = 10;
        public static final int B05_DS_STATO_ELAB = 1;
        public static final int B05_AREA_D_VALOR_VAR_LEN = 2;
        public static final int B05_AREA_D_VALOR_VAR_VCHAR = B05_AREA_D_VALOR_VAR_LEN + B05_AREA_D_VALOR_VAR;
        public static final int BILA_VAR_CALC_T = B05_ID_BILA_VAR_CALC_T + B05_COD_COMP_ANIA + B05_ID_BILA_TRCH_ESTR + B05_ID_RICH_ESTRAZ_MAS + B05IdRichEstrazAgg.Len.B05_ID_RICH_ESTRAZ_AGG + B05DtRis.Len.B05_DT_RIS + B05_ID_POLI + B05_ID_ADES + B05_ID_TRCH_DI_GAR + B05ProgSchedaValor.Len.B05_PROG_SCHEDA_VALOR + B05_DT_INI_VLDT_TARI + B05_TP_RGM_FISC + B05_DT_INI_VLDT_PROD + B05_DT_DECOR_TRCH + B05_COD_VAR + B05_TP_D + B05ValImp.Len.B05_VAL_IMP + B05ValPc.Len.B05_VAL_PC + B05_VAL_STRINGA + B05_DS_OPER_SQL + B05_DS_VER + B05_DS_TS_CPTZ + B05_DS_UTENTE + B05_DS_STATO_ELAB + B05_AREA_D_VALOR_VAR_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B05_ID_BILA_VAR_CALC_T = 9;
            public static final int B05_COD_COMP_ANIA = 5;
            public static final int B05_ID_BILA_TRCH_ESTR = 9;
            public static final int B05_ID_RICH_ESTRAZ_MAS = 9;
            public static final int B05_ID_POLI = 9;
            public static final int B05_ID_ADES = 9;
            public static final int B05_ID_TRCH_DI_GAR = 9;
            public static final int B05_DT_INI_VLDT_TARI = 8;
            public static final int B05_DT_INI_VLDT_PROD = 8;
            public static final int B05_DT_DECOR_TRCH = 8;
            public static final int B05_DS_VER = 9;
            public static final int B05_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
