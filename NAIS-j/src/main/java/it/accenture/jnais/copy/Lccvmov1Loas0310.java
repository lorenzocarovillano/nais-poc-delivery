package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LCCVMOV1<br>
 * Variable: LCCVMOV1 from copybook LCCVMOV1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvmov1Loas0310 {

    //==== PROPERTIES ====
    //Original name: ZMOV-DATI
    private WmovDati dati = new WmovDati();

    //==== METHODS ====
    public WmovDati getDati() {
        return dati;
    }
}
