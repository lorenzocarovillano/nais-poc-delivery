package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: LDBV4151<br>
 * Copybook: LDBV4151 from copybook LDBV4151<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Ldbv4151Llbs0230 {

    //==== PROPERTIES ====
    //Original name: LDBV4151-DATI-INPUT
    private Ldbv4151DatiInput ldbv4151DatiInput = new Ldbv4151DatiInput();
    //Original name: LDBV4151-PRE-TOT
    private AfDecimal ldbv4151PreTot = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    public void setLdbv4151Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV4151];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV4151);
        setLdbv4151Bytes(buffer, 1);
    }

    public String getLdbv4151Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv4151Bytes());
    }

    /**Original name: LDBV4151<br>
	 * <pre>--> AREA WHERE CONDITION AD HOC PER LDBS4150</pre>*/
    public byte[] getLdbv4151Bytes() {
        byte[] buffer = new byte[Len.LDBV4151];
        return getLdbv4151Bytes(buffer, 1);
    }

    public void setLdbv4151Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv4151DatiInput.setDatiInputBytes(buffer, position);
        position += Ldbv4151DatiInput.Len.DATI_INPUT;
        setLdbv4151DatiOutputBytes(buffer, position);
    }

    public byte[] getLdbv4151Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv4151DatiInput.getDatiInputBytes(buffer, position);
        position += Ldbv4151DatiInput.Len.DATI_INPUT;
        getLdbv4151DatiOutputBytes(buffer, position);
        return buffer;
    }

    public void setLdbv4151DatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv4151PreTot.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LDBV4151_PRE_TOT, Len.Fract.LDBV4151_PRE_TOT));
    }

    public byte[] getLdbv4151DatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimalAsPacked(buffer, position, ldbv4151PreTot.copy());
        return buffer;
    }

    public void setLdbv4151PreTot(AfDecimal ldbv4151PreTot) {
        this.ldbv4151PreTot.assign(ldbv4151PreTot);
    }

    public AfDecimal getLdbv4151PreTot() {
        return this.ldbv4151PreTot.copy();
    }

    public Ldbv4151DatiInput getLdbv4151DatiInput() {
        return ldbv4151DatiInput;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV4151_PRE_TOT = 8;
        public static final int LDBV4151_DATI_OUTPUT = LDBV4151_PRE_TOT;
        public static final int LDBV4151 = Ldbv4151DatiInput.Len.DATI_INPUT + LDBV4151_DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV4151_PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LDBV4151_PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
