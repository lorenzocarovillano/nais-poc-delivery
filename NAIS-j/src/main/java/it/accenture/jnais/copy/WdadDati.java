package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WdadDtDecorDflt;
import it.accenture.jnais.ws.redefines.WdadDtScadAdesDflt;
import it.accenture.jnais.ws.redefines.WdadDurAaAdesDflt;
import it.accenture.jnais.ws.redefines.WdadDurGgAdesDflt;
import it.accenture.jnais.ws.redefines.WdadDurMmAdesDflt;
import it.accenture.jnais.ws.redefines.WdadEtaScadFemmDflt;
import it.accenture.jnais.ws.redefines.WdadEtaScadMascDflt;
import it.accenture.jnais.ws.redefines.WdadFrazDflt;
import it.accenture.jnais.ws.redefines.WdadImpAder;
import it.accenture.jnais.ws.redefines.WdadImpAz;
import it.accenture.jnais.ws.redefines.WdadImpPreDflt;
import it.accenture.jnais.ws.redefines.WdadImpProvIncDflt;
import it.accenture.jnais.ws.redefines.WdadImpTfr;
import it.accenture.jnais.ws.redefines.WdadImpVolo;
import it.accenture.jnais.ws.redefines.WdadPcAder;
import it.accenture.jnais.ws.redefines.WdadPcAz;
import it.accenture.jnais.ws.redefines.WdadPcProvIncDflt;
import it.accenture.jnais.ws.redefines.WdadPcTfr;
import it.accenture.jnais.ws.redefines.WdadPcVolo;

/**Original name: WDAD-DATI<br>
 * Variable: WDAD-DATI from copybook LCCVDAD1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WdadDati {

    //==== PROPERTIES ====
    //Original name: WDAD-ID-DFLT-ADES
    private int wdadIdDfltAdes = DefaultValues.INT_VAL;
    //Original name: WDAD-ID-POLI
    private int wdadIdPoli = DefaultValues.INT_VAL;
    //Original name: WDAD-IB-POLI
    private String wdadIbPoli = DefaultValues.stringVal(Len.WDAD_IB_POLI);
    //Original name: WDAD-IB-DFLT
    private String wdadIbDflt = DefaultValues.stringVal(Len.WDAD_IB_DFLT);
    //Original name: WDAD-COD-GAR-1
    private String wdadCodGar1 = DefaultValues.stringVal(Len.WDAD_COD_GAR1);
    //Original name: WDAD-COD-GAR-2
    private String wdadCodGar2 = DefaultValues.stringVal(Len.WDAD_COD_GAR2);
    //Original name: WDAD-COD-GAR-3
    private String wdadCodGar3 = DefaultValues.stringVal(Len.WDAD_COD_GAR3);
    //Original name: WDAD-COD-GAR-4
    private String wdadCodGar4 = DefaultValues.stringVal(Len.WDAD_COD_GAR4);
    //Original name: WDAD-COD-GAR-5
    private String wdadCodGar5 = DefaultValues.stringVal(Len.WDAD_COD_GAR5);
    //Original name: WDAD-COD-GAR-6
    private String wdadCodGar6 = DefaultValues.stringVal(Len.WDAD_COD_GAR6);
    //Original name: WDAD-DT-DECOR-DFLT
    private WdadDtDecorDflt wdadDtDecorDflt = new WdadDtDecorDflt();
    //Original name: WDAD-ETA-SCAD-MASC-DFLT
    private WdadEtaScadMascDflt wdadEtaScadMascDflt = new WdadEtaScadMascDflt();
    //Original name: WDAD-ETA-SCAD-FEMM-DFLT
    private WdadEtaScadFemmDflt wdadEtaScadFemmDflt = new WdadEtaScadFemmDflt();
    //Original name: WDAD-DUR-AA-ADES-DFLT
    private WdadDurAaAdesDflt wdadDurAaAdesDflt = new WdadDurAaAdesDflt();
    //Original name: WDAD-DUR-MM-ADES-DFLT
    private WdadDurMmAdesDflt wdadDurMmAdesDflt = new WdadDurMmAdesDflt();
    //Original name: WDAD-DUR-GG-ADES-DFLT
    private WdadDurGgAdesDflt wdadDurGgAdesDflt = new WdadDurGgAdesDflt();
    //Original name: WDAD-DT-SCAD-ADES-DFLT
    private WdadDtScadAdesDflt wdadDtScadAdesDflt = new WdadDtScadAdesDflt();
    //Original name: WDAD-FRAZ-DFLT
    private WdadFrazDflt wdadFrazDflt = new WdadFrazDflt();
    //Original name: WDAD-PC-PROV-INC-DFLT
    private WdadPcProvIncDflt wdadPcProvIncDflt = new WdadPcProvIncDflt();
    //Original name: WDAD-IMP-PROV-INC-DFLT
    private WdadImpProvIncDflt wdadImpProvIncDflt = new WdadImpProvIncDflt();
    //Original name: WDAD-IMP-AZ
    private WdadImpAz wdadImpAz = new WdadImpAz();
    //Original name: WDAD-IMP-ADER
    private WdadImpAder wdadImpAder = new WdadImpAder();
    //Original name: WDAD-IMP-TFR
    private WdadImpTfr wdadImpTfr = new WdadImpTfr();
    //Original name: WDAD-IMP-VOLO
    private WdadImpVolo wdadImpVolo = new WdadImpVolo();
    //Original name: WDAD-PC-AZ
    private WdadPcAz wdadPcAz = new WdadPcAz();
    //Original name: WDAD-PC-ADER
    private WdadPcAder wdadPcAder = new WdadPcAder();
    //Original name: WDAD-PC-TFR
    private WdadPcTfr wdadPcTfr = new WdadPcTfr();
    //Original name: WDAD-PC-VOLO
    private WdadPcVolo wdadPcVolo = new WdadPcVolo();
    //Original name: WDAD-TP-FNT-CNBTVA
    private String wdadTpFntCnbtva = DefaultValues.stringVal(Len.WDAD_TP_FNT_CNBTVA);
    //Original name: WDAD-IMP-PRE-DFLT
    private WdadImpPreDflt wdadImpPreDflt = new WdadImpPreDflt();
    //Original name: WDAD-COD-COMP-ANIA
    private int wdadCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WDAD-TP-PRE
    private char wdadTpPre = DefaultValues.CHAR_VAL;
    //Original name: WDAD-DS-OPER-SQL
    private char wdadDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WDAD-DS-VER
    private int wdadDsVer = DefaultValues.INT_VAL;
    //Original name: WDAD-DS-TS-CPTZ
    private long wdadDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WDAD-DS-UTENTE
    private String wdadDsUtente = DefaultValues.stringVal(Len.WDAD_DS_UTENTE);
    //Original name: WDAD-DS-STATO-ELAB
    private char wdadDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wdadIdDfltAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDAD_ID_DFLT_ADES, 0);
        position += Len.WDAD_ID_DFLT_ADES;
        wdadIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDAD_ID_POLI, 0);
        position += Len.WDAD_ID_POLI;
        wdadIbPoli = MarshalByte.readString(buffer, position, Len.WDAD_IB_POLI);
        position += Len.WDAD_IB_POLI;
        wdadIbDflt = MarshalByte.readString(buffer, position, Len.WDAD_IB_DFLT);
        position += Len.WDAD_IB_DFLT;
        wdadCodGar1 = MarshalByte.readString(buffer, position, Len.WDAD_COD_GAR1);
        position += Len.WDAD_COD_GAR1;
        wdadCodGar2 = MarshalByte.readString(buffer, position, Len.WDAD_COD_GAR2);
        position += Len.WDAD_COD_GAR2;
        wdadCodGar3 = MarshalByte.readString(buffer, position, Len.WDAD_COD_GAR3);
        position += Len.WDAD_COD_GAR3;
        wdadCodGar4 = MarshalByte.readString(buffer, position, Len.WDAD_COD_GAR4);
        position += Len.WDAD_COD_GAR4;
        wdadCodGar5 = MarshalByte.readString(buffer, position, Len.WDAD_COD_GAR5);
        position += Len.WDAD_COD_GAR5;
        wdadCodGar6 = MarshalByte.readString(buffer, position, Len.WDAD_COD_GAR6);
        position += Len.WDAD_COD_GAR6;
        wdadDtDecorDflt.setWdadDtDecorDfltFromBuffer(buffer, position);
        position += WdadDtDecorDflt.Len.WDAD_DT_DECOR_DFLT;
        wdadEtaScadMascDflt.setWdadEtaScadMascDfltFromBuffer(buffer, position);
        position += WdadEtaScadMascDflt.Len.WDAD_ETA_SCAD_MASC_DFLT;
        wdadEtaScadFemmDflt.setWdadEtaScadFemmDfltFromBuffer(buffer, position);
        position += WdadEtaScadFemmDflt.Len.WDAD_ETA_SCAD_FEMM_DFLT;
        wdadDurAaAdesDflt.setWdadDurAaAdesDfltFromBuffer(buffer, position);
        position += WdadDurAaAdesDflt.Len.WDAD_DUR_AA_ADES_DFLT;
        wdadDurMmAdesDflt.setWdadDurMmAdesDfltFromBuffer(buffer, position);
        position += WdadDurMmAdesDflt.Len.WDAD_DUR_MM_ADES_DFLT;
        wdadDurGgAdesDflt.setWdadDurGgAdesDfltFromBuffer(buffer, position);
        position += WdadDurGgAdesDflt.Len.WDAD_DUR_GG_ADES_DFLT;
        wdadDtScadAdesDflt.setWdadDtScadAdesDfltFromBuffer(buffer, position);
        position += WdadDtScadAdesDflt.Len.WDAD_DT_SCAD_ADES_DFLT;
        wdadFrazDflt.setWdadFrazDfltFromBuffer(buffer, position);
        position += WdadFrazDflt.Len.WDAD_FRAZ_DFLT;
        wdadPcProvIncDflt.setWdadPcProvIncDfltFromBuffer(buffer, position);
        position += WdadPcProvIncDflt.Len.WDAD_PC_PROV_INC_DFLT;
        wdadImpProvIncDflt.setWdadImpProvIncDfltFromBuffer(buffer, position);
        position += WdadImpProvIncDflt.Len.WDAD_IMP_PROV_INC_DFLT;
        wdadImpAz.setWdadImpAzFromBuffer(buffer, position);
        position += WdadImpAz.Len.WDAD_IMP_AZ;
        wdadImpAder.setWdadImpAderFromBuffer(buffer, position);
        position += WdadImpAder.Len.WDAD_IMP_ADER;
        wdadImpTfr.setWdadImpTfrFromBuffer(buffer, position);
        position += WdadImpTfr.Len.WDAD_IMP_TFR;
        wdadImpVolo.setWdadImpVoloFromBuffer(buffer, position);
        position += WdadImpVolo.Len.WDAD_IMP_VOLO;
        wdadPcAz.setWdadPcAzFromBuffer(buffer, position);
        position += WdadPcAz.Len.WDAD_PC_AZ;
        wdadPcAder.setWdadPcAderFromBuffer(buffer, position);
        position += WdadPcAder.Len.WDAD_PC_ADER;
        wdadPcTfr.setWdadPcTfrFromBuffer(buffer, position);
        position += WdadPcTfr.Len.WDAD_PC_TFR;
        wdadPcVolo.setWdadPcVoloFromBuffer(buffer, position);
        position += WdadPcVolo.Len.WDAD_PC_VOLO;
        wdadTpFntCnbtva = MarshalByte.readString(buffer, position, Len.WDAD_TP_FNT_CNBTVA);
        position += Len.WDAD_TP_FNT_CNBTVA;
        wdadImpPreDflt.setWdadImpPreDfltFromBuffer(buffer, position);
        position += WdadImpPreDflt.Len.WDAD_IMP_PRE_DFLT;
        wdadCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDAD_COD_COMP_ANIA, 0);
        position += Len.WDAD_COD_COMP_ANIA;
        wdadTpPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdadDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdadDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDAD_DS_VER, 0);
        position += Len.WDAD_DS_VER;
        wdadDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDAD_DS_TS_CPTZ, 0);
        position += Len.WDAD_DS_TS_CPTZ;
        wdadDsUtente = MarshalByte.readString(buffer, position, Len.WDAD_DS_UTENTE);
        position += Len.WDAD_DS_UTENTE;
        wdadDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wdadIdDfltAdes, Len.Int.WDAD_ID_DFLT_ADES, 0);
        position += Len.WDAD_ID_DFLT_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wdadIdPoli, Len.Int.WDAD_ID_POLI, 0);
        position += Len.WDAD_ID_POLI;
        MarshalByte.writeString(buffer, position, wdadIbPoli, Len.WDAD_IB_POLI);
        position += Len.WDAD_IB_POLI;
        MarshalByte.writeString(buffer, position, wdadIbDflt, Len.WDAD_IB_DFLT);
        position += Len.WDAD_IB_DFLT;
        MarshalByte.writeString(buffer, position, wdadCodGar1, Len.WDAD_COD_GAR1);
        position += Len.WDAD_COD_GAR1;
        MarshalByte.writeString(buffer, position, wdadCodGar2, Len.WDAD_COD_GAR2);
        position += Len.WDAD_COD_GAR2;
        MarshalByte.writeString(buffer, position, wdadCodGar3, Len.WDAD_COD_GAR3);
        position += Len.WDAD_COD_GAR3;
        MarshalByte.writeString(buffer, position, wdadCodGar4, Len.WDAD_COD_GAR4);
        position += Len.WDAD_COD_GAR4;
        MarshalByte.writeString(buffer, position, wdadCodGar5, Len.WDAD_COD_GAR5);
        position += Len.WDAD_COD_GAR5;
        MarshalByte.writeString(buffer, position, wdadCodGar6, Len.WDAD_COD_GAR6);
        position += Len.WDAD_COD_GAR6;
        wdadDtDecorDflt.getWdadDtDecorDfltAsBuffer(buffer, position);
        position += WdadDtDecorDflt.Len.WDAD_DT_DECOR_DFLT;
        wdadEtaScadMascDflt.getWdadEtaScadMascDfltAsBuffer(buffer, position);
        position += WdadEtaScadMascDflt.Len.WDAD_ETA_SCAD_MASC_DFLT;
        wdadEtaScadFemmDflt.getWdadEtaScadFemmDfltAsBuffer(buffer, position);
        position += WdadEtaScadFemmDflt.Len.WDAD_ETA_SCAD_FEMM_DFLT;
        wdadDurAaAdesDflt.getWdadDurAaAdesDfltAsBuffer(buffer, position);
        position += WdadDurAaAdesDflt.Len.WDAD_DUR_AA_ADES_DFLT;
        wdadDurMmAdesDflt.getWdadDurMmAdesDfltAsBuffer(buffer, position);
        position += WdadDurMmAdesDflt.Len.WDAD_DUR_MM_ADES_DFLT;
        wdadDurGgAdesDflt.getWdadDurGgAdesDfltAsBuffer(buffer, position);
        position += WdadDurGgAdesDflt.Len.WDAD_DUR_GG_ADES_DFLT;
        wdadDtScadAdesDflt.getWdadDtScadAdesDfltAsBuffer(buffer, position);
        position += WdadDtScadAdesDflt.Len.WDAD_DT_SCAD_ADES_DFLT;
        wdadFrazDflt.getWdadFrazDfltAsBuffer(buffer, position);
        position += WdadFrazDflt.Len.WDAD_FRAZ_DFLT;
        wdadPcProvIncDflt.getWdadPcProvIncDfltAsBuffer(buffer, position);
        position += WdadPcProvIncDflt.Len.WDAD_PC_PROV_INC_DFLT;
        wdadImpProvIncDflt.getWdadImpProvIncDfltAsBuffer(buffer, position);
        position += WdadImpProvIncDflt.Len.WDAD_IMP_PROV_INC_DFLT;
        wdadImpAz.getWdadImpAzAsBuffer(buffer, position);
        position += WdadImpAz.Len.WDAD_IMP_AZ;
        wdadImpAder.getWdadImpAderAsBuffer(buffer, position);
        position += WdadImpAder.Len.WDAD_IMP_ADER;
        wdadImpTfr.getWdadImpTfrAsBuffer(buffer, position);
        position += WdadImpTfr.Len.WDAD_IMP_TFR;
        wdadImpVolo.getWdadImpVoloAsBuffer(buffer, position);
        position += WdadImpVolo.Len.WDAD_IMP_VOLO;
        wdadPcAz.getWdadPcAzAsBuffer(buffer, position);
        position += WdadPcAz.Len.WDAD_PC_AZ;
        wdadPcAder.getWdadPcAderAsBuffer(buffer, position);
        position += WdadPcAder.Len.WDAD_PC_ADER;
        wdadPcTfr.getWdadPcTfrAsBuffer(buffer, position);
        position += WdadPcTfr.Len.WDAD_PC_TFR;
        wdadPcVolo.getWdadPcVoloAsBuffer(buffer, position);
        position += WdadPcVolo.Len.WDAD_PC_VOLO;
        MarshalByte.writeString(buffer, position, wdadTpFntCnbtva, Len.WDAD_TP_FNT_CNBTVA);
        position += Len.WDAD_TP_FNT_CNBTVA;
        wdadImpPreDflt.getWdadImpPreDfltAsBuffer(buffer, position);
        position += WdadImpPreDflt.Len.WDAD_IMP_PRE_DFLT;
        MarshalByte.writeIntAsPacked(buffer, position, wdadCodCompAnia, Len.Int.WDAD_COD_COMP_ANIA, 0);
        position += Len.WDAD_COD_COMP_ANIA;
        MarshalByte.writeChar(buffer, position, wdadTpPre);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wdadDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wdadDsVer, Len.Int.WDAD_DS_VER, 0);
        position += Len.WDAD_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wdadDsTsCptz, Len.Int.WDAD_DS_TS_CPTZ, 0);
        position += Len.WDAD_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wdadDsUtente, Len.WDAD_DS_UTENTE);
        position += Len.WDAD_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wdadDsStatoElab);
        return buffer;
    }

    public void setWdadIdDfltAdes(int wdadIdDfltAdes) {
        this.wdadIdDfltAdes = wdadIdDfltAdes;
    }

    public int getWdadIdDfltAdes() {
        return this.wdadIdDfltAdes;
    }

    public void setWdadIdPoli(int wdadIdPoli) {
        this.wdadIdPoli = wdadIdPoli;
    }

    public int getWdadIdPoli() {
        return this.wdadIdPoli;
    }

    public void setWdadIbPoli(String wdadIbPoli) {
        this.wdadIbPoli = Functions.subString(wdadIbPoli, Len.WDAD_IB_POLI);
    }

    public String getWdadIbPoli() {
        return this.wdadIbPoli;
    }

    public void setWdadIbDflt(String wdadIbDflt) {
        this.wdadIbDflt = Functions.subString(wdadIbDflt, Len.WDAD_IB_DFLT);
    }

    public String getWdadIbDflt() {
        return this.wdadIbDflt;
    }

    public void setWdadCodGar1(String wdadCodGar1) {
        this.wdadCodGar1 = Functions.subString(wdadCodGar1, Len.WDAD_COD_GAR1);
    }

    public String getWdadCodGar1() {
        return this.wdadCodGar1;
    }

    public void setWdadCodGar2(String wdadCodGar2) {
        this.wdadCodGar2 = Functions.subString(wdadCodGar2, Len.WDAD_COD_GAR2);
    }

    public String getWdadCodGar2() {
        return this.wdadCodGar2;
    }

    public void setWdadCodGar3(String wdadCodGar3) {
        this.wdadCodGar3 = Functions.subString(wdadCodGar3, Len.WDAD_COD_GAR3);
    }

    public String getWdadCodGar3() {
        return this.wdadCodGar3;
    }

    public void setWdadCodGar4(String wdadCodGar4) {
        this.wdadCodGar4 = Functions.subString(wdadCodGar4, Len.WDAD_COD_GAR4);
    }

    public String getWdadCodGar4() {
        return this.wdadCodGar4;
    }

    public void setWdadCodGar5(String wdadCodGar5) {
        this.wdadCodGar5 = Functions.subString(wdadCodGar5, Len.WDAD_COD_GAR5);
    }

    public String getWdadCodGar5() {
        return this.wdadCodGar5;
    }

    public void setWdadCodGar6(String wdadCodGar6) {
        this.wdadCodGar6 = Functions.subString(wdadCodGar6, Len.WDAD_COD_GAR6);
    }

    public String getWdadCodGar6() {
        return this.wdadCodGar6;
    }

    public void setWdadTpFntCnbtva(String wdadTpFntCnbtva) {
        this.wdadTpFntCnbtva = Functions.subString(wdadTpFntCnbtva, Len.WDAD_TP_FNT_CNBTVA);
    }

    public String getWdadTpFntCnbtva() {
        return this.wdadTpFntCnbtva;
    }

    public void setWdadCodCompAnia(int wdadCodCompAnia) {
        this.wdadCodCompAnia = wdadCodCompAnia;
    }

    public int getWdadCodCompAnia() {
        return this.wdadCodCompAnia;
    }

    public void setWdadTpPre(char wdadTpPre) {
        this.wdadTpPre = wdadTpPre;
    }

    public char getWdadTpPre() {
        return this.wdadTpPre;
    }

    public void setWdadDsOperSql(char wdadDsOperSql) {
        this.wdadDsOperSql = wdadDsOperSql;
    }

    public char getWdadDsOperSql() {
        return this.wdadDsOperSql;
    }

    public void setWdadDsVer(int wdadDsVer) {
        this.wdadDsVer = wdadDsVer;
    }

    public int getWdadDsVer() {
        return this.wdadDsVer;
    }

    public void setWdadDsTsCptz(long wdadDsTsCptz) {
        this.wdadDsTsCptz = wdadDsTsCptz;
    }

    public long getWdadDsTsCptz() {
        return this.wdadDsTsCptz;
    }

    public void setWdadDsUtente(String wdadDsUtente) {
        this.wdadDsUtente = Functions.subString(wdadDsUtente, Len.WDAD_DS_UTENTE);
    }

    public String getWdadDsUtente() {
        return this.wdadDsUtente;
    }

    public void setWdadDsStatoElab(char wdadDsStatoElab) {
        this.wdadDsStatoElab = wdadDsStatoElab;
    }

    public char getWdadDsStatoElab() {
        return this.wdadDsStatoElab;
    }

    public WdadDtDecorDflt getWdadDtDecorDflt() {
        return wdadDtDecorDflt;
    }

    public WdadDtScadAdesDflt getWdadDtScadAdesDflt() {
        return wdadDtScadAdesDflt;
    }

    public WdadDurAaAdesDflt getWdadDurAaAdesDflt() {
        return wdadDurAaAdesDflt;
    }

    public WdadDurGgAdesDflt getWdadDurGgAdesDflt() {
        return wdadDurGgAdesDflt;
    }

    public WdadDurMmAdesDflt getWdadDurMmAdesDflt() {
        return wdadDurMmAdesDflt;
    }

    public WdadEtaScadFemmDflt getWdadEtaScadFemmDflt() {
        return wdadEtaScadFemmDflt;
    }

    public WdadEtaScadMascDflt getWdadEtaScadMascDflt() {
        return wdadEtaScadMascDflt;
    }

    public WdadFrazDflt getWdadFrazDflt() {
        return wdadFrazDflt;
    }

    public WdadImpAder getWdadImpAder() {
        return wdadImpAder;
    }

    public WdadImpAz getWdadImpAz() {
        return wdadImpAz;
    }

    public WdadImpPreDflt getWdadImpPreDflt() {
        return wdadImpPreDflt;
    }

    public WdadImpProvIncDflt getWdadImpProvIncDflt() {
        return wdadImpProvIncDflt;
    }

    public WdadImpTfr getWdadImpTfr() {
        return wdadImpTfr;
    }

    public WdadImpVolo getWdadImpVolo() {
        return wdadImpVolo;
    }

    public WdadPcAder getWdadPcAder() {
        return wdadPcAder;
    }

    public WdadPcAz getWdadPcAz() {
        return wdadPcAz;
    }

    public WdadPcProvIncDflt getWdadPcProvIncDflt() {
        return wdadPcProvIncDflt;
    }

    public WdadPcTfr getWdadPcTfr() {
        return wdadPcTfr;
    }

    public WdadPcVolo getWdadPcVolo() {
        return wdadPcVolo;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_ID_DFLT_ADES = 5;
        public static final int WDAD_ID_POLI = 5;
        public static final int WDAD_IB_POLI = 40;
        public static final int WDAD_IB_DFLT = 40;
        public static final int WDAD_COD_GAR1 = 12;
        public static final int WDAD_COD_GAR2 = 12;
        public static final int WDAD_COD_GAR3 = 12;
        public static final int WDAD_COD_GAR4 = 12;
        public static final int WDAD_COD_GAR5 = 12;
        public static final int WDAD_COD_GAR6 = 12;
        public static final int WDAD_TP_FNT_CNBTVA = 2;
        public static final int WDAD_COD_COMP_ANIA = 3;
        public static final int WDAD_TP_PRE = 1;
        public static final int WDAD_DS_OPER_SQL = 1;
        public static final int WDAD_DS_VER = 5;
        public static final int WDAD_DS_TS_CPTZ = 10;
        public static final int WDAD_DS_UTENTE = 20;
        public static final int WDAD_DS_STATO_ELAB = 1;
        public static final int DATI = WDAD_ID_DFLT_ADES + WDAD_ID_POLI + WDAD_IB_POLI + WDAD_IB_DFLT + WDAD_COD_GAR1 + WDAD_COD_GAR2 + WDAD_COD_GAR3 + WDAD_COD_GAR4 + WDAD_COD_GAR5 + WDAD_COD_GAR6 + WdadDtDecorDflt.Len.WDAD_DT_DECOR_DFLT + WdadEtaScadMascDflt.Len.WDAD_ETA_SCAD_MASC_DFLT + WdadEtaScadFemmDflt.Len.WDAD_ETA_SCAD_FEMM_DFLT + WdadDurAaAdesDflt.Len.WDAD_DUR_AA_ADES_DFLT + WdadDurMmAdesDflt.Len.WDAD_DUR_MM_ADES_DFLT + WdadDurGgAdesDflt.Len.WDAD_DUR_GG_ADES_DFLT + WdadDtScadAdesDflt.Len.WDAD_DT_SCAD_ADES_DFLT + WdadFrazDflt.Len.WDAD_FRAZ_DFLT + WdadPcProvIncDflt.Len.WDAD_PC_PROV_INC_DFLT + WdadImpProvIncDflt.Len.WDAD_IMP_PROV_INC_DFLT + WdadImpAz.Len.WDAD_IMP_AZ + WdadImpAder.Len.WDAD_IMP_ADER + WdadImpTfr.Len.WDAD_IMP_TFR + WdadImpVolo.Len.WDAD_IMP_VOLO + WdadPcAz.Len.WDAD_PC_AZ + WdadPcAder.Len.WDAD_PC_ADER + WdadPcTfr.Len.WDAD_PC_TFR + WdadPcVolo.Len.WDAD_PC_VOLO + WDAD_TP_FNT_CNBTVA + WdadImpPreDflt.Len.WDAD_IMP_PRE_DFLT + WDAD_COD_COMP_ANIA + WDAD_TP_PRE + WDAD_DS_OPER_SQL + WDAD_DS_VER + WDAD_DS_TS_CPTZ + WDAD_DS_UTENTE + WDAD_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_ID_DFLT_ADES = 9;
            public static final int WDAD_ID_POLI = 9;
            public static final int WDAD_COD_COMP_ANIA = 5;
            public static final int WDAD_DS_VER = 9;
            public static final int WDAD_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
