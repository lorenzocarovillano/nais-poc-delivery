package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV5681<br>
 * Variable: LDBV5681 from copybook LDBV5681<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv5681 {

    //==== PROPERTIES ====
    //Original name: LDBV5681-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LDBV5681-TP-FRM-ASSVA-1
    private String tpFrmAssva1 = DefaultValues.stringVal(Len.TP_FRM_ASSVA1);
    //Original name: LDBV5681-TP-FRM-ASSVA-2
    private String tpFrmAssva2 = DefaultValues.stringVal(Len.TP_FRM_ASSVA2);
    //Original name: LDBV5681-DS-TS-INI-CPTZ
    private long dsTsIniCptz = DefaultValues.LONG_VAL;

    //==== METHODS ====
    public void setLdbv5681Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV5681];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV5681);
        setLdbv5681Bytes(buffer, 1);
    }

    public String getLdbv5681Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv5681Bytes());
    }

    public byte[] getLdbv5681Bytes() {
        byte[] buffer = new byte[Len.LDBV5681];
        return getLdbv5681Bytes(buffer, 1);
    }

    public void setLdbv5681Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        tpFrmAssva1 = MarshalByte.readString(buffer, position, Len.TP_FRM_ASSVA1);
        position += Len.TP_FRM_ASSVA1;
        tpFrmAssva2 = MarshalByte.readString(buffer, position, Len.TP_FRM_ASSVA2);
        position += Len.TP_FRM_ASSVA2;
        dsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_INI_CPTZ, 0);
    }

    public byte[] getLdbv5681Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeString(buffer, position, tpFrmAssva1, Len.TP_FRM_ASSVA1);
        position += Len.TP_FRM_ASSVA1;
        MarshalByte.writeString(buffer, position, tpFrmAssva2, Len.TP_FRM_ASSVA2);
        position += Len.TP_FRM_ASSVA2;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ, 0);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setTpFrmAssva1(String tpFrmAssva1) {
        this.tpFrmAssva1 = Functions.subString(tpFrmAssva1, Len.TP_FRM_ASSVA1);
    }

    public String getTpFrmAssva1() {
        return this.tpFrmAssva1;
    }

    public void setTpFrmAssva2(String tpFrmAssva2) {
        this.tpFrmAssva2 = Functions.subString(tpFrmAssva2, Len.TP_FRM_ASSVA2);
    }

    public String getTpFrmAssva2() {
        return this.tpFrmAssva2;
    }

    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dsTsIniCptz = dsTsIniCptz;
    }

    public long getDsTsIniCptz() {
        return this.dsTsIniCptz;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_FRM_ASSVA1 = 2;
        public static final int TP_FRM_ASSVA2 = 2;
        public static final int ID_POLI = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int LDBV5681 = ID_POLI + TP_FRM_ASSVA1 + TP_FRM_ASSVA2 + DS_TS_INI_CPTZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int DS_TS_INI_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
