package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.P01DtEff;
import it.accenture.jnais.ws.redefines.P01DtEstFinanz;
import it.accenture.jnais.ws.redefines.P01DtManCop;
import it.accenture.jnais.ws.redefines.P01DtSin;
import it.accenture.jnais.ws.redefines.P01IdLiq;
import it.accenture.jnais.ws.redefines.P01IdRichEstCollg;
import it.accenture.jnais.ws.redefines.P01IdRichiedente;

/**Original name: RICH-EST<br>
 * Variable: RICH-EST from copybook IDBVP011<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class RichEst {

    //==== PROPERTIES ====
    //Original name: P01-ID-RICH-EST
    private int p01IdRichEst = DefaultValues.INT_VAL;
    //Original name: P01-ID-RICH-EST-COLLG
    private P01IdRichEstCollg p01IdRichEstCollg = new P01IdRichEstCollg();
    //Original name: P01-ID-LIQ
    private P01IdLiq p01IdLiq = new P01IdLiq();
    //Original name: P01-COD-COMP-ANIA
    private int p01CodCompAnia = DefaultValues.INT_VAL;
    //Original name: P01-IB-RICH-EST
    private String p01IbRichEst = DefaultValues.stringVal(Len.P01_IB_RICH_EST);
    //Original name: P01-TP-MOVI
    private int p01TpMovi = DefaultValues.INT_VAL;
    //Original name: P01-DT-FORM-RICH
    private int p01DtFormRich = DefaultValues.INT_VAL;
    //Original name: P01-DT-INVIO-RICH
    private int p01DtInvioRich = DefaultValues.INT_VAL;
    //Original name: P01-DT-PERV-RICH
    private int p01DtPervRich = DefaultValues.INT_VAL;
    //Original name: P01-DT-RGSTRZ-RICH
    private int p01DtRgstrzRich = DefaultValues.INT_VAL;
    //Original name: P01-DT-EFF
    private P01DtEff p01DtEff = new P01DtEff();
    //Original name: P01-DT-SIN
    private P01DtSin p01DtSin = new P01DtSin();
    //Original name: P01-TP-OGG
    private String p01TpOgg = DefaultValues.stringVal(Len.P01_TP_OGG);
    //Original name: P01-ID-OGG
    private int p01IdOgg = DefaultValues.INT_VAL;
    //Original name: P01-IB-OGG
    private String p01IbOgg = DefaultValues.stringVal(Len.P01_IB_OGG);
    //Original name: P01-FL-MOD-EXEC
    private char p01FlModExec = DefaultValues.CHAR_VAL;
    //Original name: P01-ID-RICHIEDENTE
    private P01IdRichiedente p01IdRichiedente = new P01IdRichiedente();
    //Original name: P01-COD-PROD
    private String p01CodProd = DefaultValues.stringVal(Len.P01_COD_PROD);
    //Original name: P01-DS-OPER-SQL
    private char p01DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P01-DS-VER
    private int p01DsVer = DefaultValues.INT_VAL;
    //Original name: P01-DS-TS-CPTZ
    private long p01DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: P01-DS-UTENTE
    private String p01DsUtente = DefaultValues.stringVal(Len.P01_DS_UTENTE);
    //Original name: P01-DS-STATO-ELAB
    private char p01DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: P01-COD-CAN
    private int p01CodCan = DefaultValues.INT_VAL;
    //Original name: P01-IB-OGG-ORIG
    private String p01IbOggOrig = DefaultValues.stringVal(Len.P01_IB_OGG_ORIG);
    //Original name: P01-DT-EST-FINANZ
    private P01DtEstFinanz p01DtEstFinanz = new P01DtEstFinanz();
    //Original name: P01-DT-MAN-COP
    private P01DtManCop p01DtManCop = new P01DtManCop();
    //Original name: P01-FL-GEST-PROTEZIONE
    private char p01FlGestProtezione = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setRichEstFormatted(String data) {
        byte[] buffer = new byte[Len.RICH_EST];
        MarshalByte.writeString(buffer, 1, data, Len.RICH_EST);
        setRichEstBytes(buffer, 1);
    }

    public String getRichEstFormatted() {
        return MarshalByteExt.bufferToStr(getRichEstBytes());
    }

    public byte[] getRichEstBytes() {
        byte[] buffer = new byte[Len.RICH_EST];
        return getRichEstBytes(buffer, 1);
    }

    public void setRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        p01IdRichEst = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_ID_RICH_EST, 0);
        position += Len.P01_ID_RICH_EST;
        p01IdRichEstCollg.setP01IdRichEstCollgFromBuffer(buffer, position);
        position += P01IdRichEstCollg.Len.P01_ID_RICH_EST_COLLG;
        p01IdLiq.setP01IdLiqFromBuffer(buffer, position);
        position += P01IdLiq.Len.P01_ID_LIQ;
        p01CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_COD_COMP_ANIA, 0);
        position += Len.P01_COD_COMP_ANIA;
        p01IbRichEst = MarshalByte.readString(buffer, position, Len.P01_IB_RICH_EST);
        position += Len.P01_IB_RICH_EST;
        p01TpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_TP_MOVI, 0);
        position += Len.P01_TP_MOVI;
        p01DtFormRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_DT_FORM_RICH, 0);
        position += Len.P01_DT_FORM_RICH;
        p01DtInvioRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_DT_INVIO_RICH, 0);
        position += Len.P01_DT_INVIO_RICH;
        p01DtPervRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_DT_PERV_RICH, 0);
        position += Len.P01_DT_PERV_RICH;
        p01DtRgstrzRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_DT_RGSTRZ_RICH, 0);
        position += Len.P01_DT_RGSTRZ_RICH;
        p01DtEff.setP01DtEffFromBuffer(buffer, position);
        position += P01DtEff.Len.P01_DT_EFF;
        p01DtSin.setP01DtSinFromBuffer(buffer, position);
        position += P01DtSin.Len.P01_DT_SIN;
        p01TpOgg = MarshalByte.readString(buffer, position, Len.P01_TP_OGG);
        position += Len.P01_TP_OGG;
        p01IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_ID_OGG, 0);
        position += Len.P01_ID_OGG;
        p01IbOgg = MarshalByte.readString(buffer, position, Len.P01_IB_OGG);
        position += Len.P01_IB_OGG;
        p01FlModExec = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p01IdRichiedente.setP01IdRichiedenteFromBuffer(buffer, position);
        position += P01IdRichiedente.Len.P01_ID_RICHIEDENTE;
        p01CodProd = MarshalByte.readString(buffer, position, Len.P01_COD_PROD);
        position += Len.P01_COD_PROD;
        p01DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p01DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_DS_VER, 0);
        position += Len.P01_DS_VER;
        p01DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P01_DS_TS_CPTZ, 0);
        position += Len.P01_DS_TS_CPTZ;
        p01DsUtente = MarshalByte.readString(buffer, position, Len.P01_DS_UTENTE);
        position += Len.P01_DS_UTENTE;
        p01DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p01CodCan = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P01_COD_CAN, 0);
        position += Len.P01_COD_CAN;
        p01IbOggOrig = MarshalByte.readString(buffer, position, Len.P01_IB_OGG_ORIG);
        position += Len.P01_IB_OGG_ORIG;
        p01DtEstFinanz.setP01DtEstFinanzFromBuffer(buffer, position);
        position += P01DtEstFinanz.Len.P01_DT_EST_FINANZ;
        p01DtManCop.setP01DtManCopFromBuffer(buffer, position);
        position += P01DtManCop.Len.P01_DT_MAN_COP;
        p01FlGestProtezione = MarshalByte.readChar(buffer, position);
    }

    public byte[] getRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, p01IdRichEst, Len.Int.P01_ID_RICH_EST, 0);
        position += Len.P01_ID_RICH_EST;
        p01IdRichEstCollg.getP01IdRichEstCollgAsBuffer(buffer, position);
        position += P01IdRichEstCollg.Len.P01_ID_RICH_EST_COLLG;
        p01IdLiq.getP01IdLiqAsBuffer(buffer, position);
        position += P01IdLiq.Len.P01_ID_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, p01CodCompAnia, Len.Int.P01_COD_COMP_ANIA, 0);
        position += Len.P01_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, p01IbRichEst, Len.P01_IB_RICH_EST);
        position += Len.P01_IB_RICH_EST;
        MarshalByte.writeIntAsPacked(buffer, position, p01TpMovi, Len.Int.P01_TP_MOVI, 0);
        position += Len.P01_TP_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, p01DtFormRich, Len.Int.P01_DT_FORM_RICH, 0);
        position += Len.P01_DT_FORM_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, p01DtInvioRich, Len.Int.P01_DT_INVIO_RICH, 0);
        position += Len.P01_DT_INVIO_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, p01DtPervRich, Len.Int.P01_DT_PERV_RICH, 0);
        position += Len.P01_DT_PERV_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, p01DtRgstrzRich, Len.Int.P01_DT_RGSTRZ_RICH, 0);
        position += Len.P01_DT_RGSTRZ_RICH;
        p01DtEff.getP01DtEffAsBuffer(buffer, position);
        position += P01DtEff.Len.P01_DT_EFF;
        p01DtSin.getP01DtSinAsBuffer(buffer, position);
        position += P01DtSin.Len.P01_DT_SIN;
        MarshalByte.writeString(buffer, position, p01TpOgg, Len.P01_TP_OGG);
        position += Len.P01_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, p01IdOgg, Len.Int.P01_ID_OGG, 0);
        position += Len.P01_ID_OGG;
        MarshalByte.writeString(buffer, position, p01IbOgg, Len.P01_IB_OGG);
        position += Len.P01_IB_OGG;
        MarshalByte.writeChar(buffer, position, p01FlModExec);
        position += Types.CHAR_SIZE;
        p01IdRichiedente.getP01IdRichiedenteAsBuffer(buffer, position);
        position += P01IdRichiedente.Len.P01_ID_RICHIEDENTE;
        MarshalByte.writeString(buffer, position, p01CodProd, Len.P01_COD_PROD);
        position += Len.P01_COD_PROD;
        MarshalByte.writeChar(buffer, position, p01DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, p01DsVer, Len.Int.P01_DS_VER, 0);
        position += Len.P01_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, p01DsTsCptz, Len.Int.P01_DS_TS_CPTZ, 0);
        position += Len.P01_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, p01DsUtente, Len.P01_DS_UTENTE);
        position += Len.P01_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, p01DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, p01CodCan, Len.Int.P01_COD_CAN, 0);
        position += Len.P01_COD_CAN;
        MarshalByte.writeString(buffer, position, p01IbOggOrig, Len.P01_IB_OGG_ORIG);
        position += Len.P01_IB_OGG_ORIG;
        p01DtEstFinanz.getP01DtEstFinanzAsBuffer(buffer, position);
        position += P01DtEstFinanz.Len.P01_DT_EST_FINANZ;
        p01DtManCop.getP01DtManCopAsBuffer(buffer, position);
        position += P01DtManCop.Len.P01_DT_MAN_COP;
        MarshalByte.writeChar(buffer, position, p01FlGestProtezione);
        return buffer;
    }

    public void setP01IdRichEst(int p01IdRichEst) {
        this.p01IdRichEst = p01IdRichEst;
    }

    public int getP01IdRichEst() {
        return this.p01IdRichEst;
    }

    public void setP01CodCompAnia(int p01CodCompAnia) {
        this.p01CodCompAnia = p01CodCompAnia;
    }

    public int getP01CodCompAnia() {
        return this.p01CodCompAnia;
    }

    public void setP01IbRichEst(String p01IbRichEst) {
        this.p01IbRichEst = Functions.subString(p01IbRichEst, Len.P01_IB_RICH_EST);
    }

    public String getP01IbRichEst() {
        return this.p01IbRichEst;
    }

    public void setP01TpMovi(int p01TpMovi) {
        this.p01TpMovi = p01TpMovi;
    }

    public int getP01TpMovi() {
        return this.p01TpMovi;
    }

    public void setP01DtFormRich(int p01DtFormRich) {
        this.p01DtFormRich = p01DtFormRich;
    }

    public int getP01DtFormRich() {
        return this.p01DtFormRich;
    }

    public void setP01DtInvioRich(int p01DtInvioRich) {
        this.p01DtInvioRich = p01DtInvioRich;
    }

    public int getP01DtInvioRich() {
        return this.p01DtInvioRich;
    }

    public void setP01DtPervRich(int p01DtPervRich) {
        this.p01DtPervRich = p01DtPervRich;
    }

    public int getP01DtPervRich() {
        return this.p01DtPervRich;
    }

    public void setP01DtRgstrzRich(int p01DtRgstrzRich) {
        this.p01DtRgstrzRich = p01DtRgstrzRich;
    }

    public int getP01DtRgstrzRich() {
        return this.p01DtRgstrzRich;
    }

    public void setP01TpOgg(String p01TpOgg) {
        this.p01TpOgg = Functions.subString(p01TpOgg, Len.P01_TP_OGG);
    }

    public String getP01TpOgg() {
        return this.p01TpOgg;
    }

    public void setP01IdOgg(int p01IdOgg) {
        this.p01IdOgg = p01IdOgg;
    }

    public int getP01IdOgg() {
        return this.p01IdOgg;
    }

    public void setP01IbOgg(String p01IbOgg) {
        this.p01IbOgg = Functions.subString(p01IbOgg, Len.P01_IB_OGG);
    }

    public String getP01IbOgg() {
        return this.p01IbOgg;
    }

    public void setP01FlModExec(char p01FlModExec) {
        this.p01FlModExec = p01FlModExec;
    }

    public char getP01FlModExec() {
        return this.p01FlModExec;
    }

    public void setP01CodProd(String p01CodProd) {
        this.p01CodProd = Functions.subString(p01CodProd, Len.P01_COD_PROD);
    }

    public String getP01CodProd() {
        return this.p01CodProd;
    }

    public void setP01DsOperSql(char p01DsOperSql) {
        this.p01DsOperSql = p01DsOperSql;
    }

    public char getP01DsOperSql() {
        return this.p01DsOperSql;
    }

    public void setP01DsVer(int p01DsVer) {
        this.p01DsVer = p01DsVer;
    }

    public int getP01DsVer() {
        return this.p01DsVer;
    }

    public void setP01DsTsCptz(long p01DsTsCptz) {
        this.p01DsTsCptz = p01DsTsCptz;
    }

    public long getP01DsTsCptz() {
        return this.p01DsTsCptz;
    }

    public void setP01DsUtente(String p01DsUtente) {
        this.p01DsUtente = Functions.subString(p01DsUtente, Len.P01_DS_UTENTE);
    }

    public String getP01DsUtente() {
        return this.p01DsUtente;
    }

    public void setP01DsStatoElab(char p01DsStatoElab) {
        this.p01DsStatoElab = p01DsStatoElab;
    }

    public char getP01DsStatoElab() {
        return this.p01DsStatoElab;
    }

    public void setP01CodCan(int p01CodCan) {
        this.p01CodCan = p01CodCan;
    }

    public int getP01CodCan() {
        return this.p01CodCan;
    }

    public void setP01IbOggOrig(String p01IbOggOrig) {
        this.p01IbOggOrig = Functions.subString(p01IbOggOrig, Len.P01_IB_OGG_ORIG);
    }

    public String getP01IbOggOrig() {
        return this.p01IbOggOrig;
    }

    public void setP01FlGestProtezione(char p01FlGestProtezione) {
        this.p01FlGestProtezione = p01FlGestProtezione;
    }

    public char getP01FlGestProtezione() {
        return this.p01FlGestProtezione;
    }

    public P01DtEff getP01DtEff() {
        return p01DtEff;
    }

    public P01DtEstFinanz getP01DtEstFinanz() {
        return p01DtEstFinanz;
    }

    public P01DtManCop getP01DtManCop() {
        return p01DtManCop;
    }

    public P01DtSin getP01DtSin() {
        return p01DtSin;
    }

    public P01IdLiq getP01IdLiq() {
        return p01IdLiq;
    }

    public P01IdRichEstCollg getP01IdRichEstCollg() {
        return p01IdRichEstCollg;
    }

    public P01IdRichiedente getP01IdRichiedente() {
        return p01IdRichiedente;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P01_IB_RICH_EST = 40;
        public static final int P01_TP_OGG = 2;
        public static final int P01_IB_OGG = 40;
        public static final int P01_COD_PROD = 12;
        public static final int P01_DS_UTENTE = 20;
        public static final int P01_IB_OGG_ORIG = 40;
        public static final int P01_ID_RICH_EST = 5;
        public static final int P01_COD_COMP_ANIA = 3;
        public static final int P01_TP_MOVI = 3;
        public static final int P01_DT_FORM_RICH = 5;
        public static final int P01_DT_INVIO_RICH = 5;
        public static final int P01_DT_PERV_RICH = 5;
        public static final int P01_DT_RGSTRZ_RICH = 5;
        public static final int P01_ID_OGG = 5;
        public static final int P01_FL_MOD_EXEC = 1;
        public static final int P01_DS_OPER_SQL = 1;
        public static final int P01_DS_VER = 5;
        public static final int P01_DS_TS_CPTZ = 10;
        public static final int P01_DS_STATO_ELAB = 1;
        public static final int P01_COD_CAN = 3;
        public static final int P01_FL_GEST_PROTEZIONE = 1;
        public static final int RICH_EST = P01_ID_RICH_EST + P01IdRichEstCollg.Len.P01_ID_RICH_EST_COLLG + P01IdLiq.Len.P01_ID_LIQ + P01_COD_COMP_ANIA + P01_IB_RICH_EST + P01_TP_MOVI + P01_DT_FORM_RICH + P01_DT_INVIO_RICH + P01_DT_PERV_RICH + P01_DT_RGSTRZ_RICH + P01DtEff.Len.P01_DT_EFF + P01DtSin.Len.P01_DT_SIN + P01_TP_OGG + P01_ID_OGG + P01_IB_OGG + P01_FL_MOD_EXEC + P01IdRichiedente.Len.P01_ID_RICHIEDENTE + P01_COD_PROD + P01_DS_OPER_SQL + P01_DS_VER + P01_DS_TS_CPTZ + P01_DS_UTENTE + P01_DS_STATO_ELAB + P01_COD_CAN + P01_IB_OGG_ORIG + P01DtEstFinanz.Len.P01_DT_EST_FINANZ + P01DtManCop.Len.P01_DT_MAN_COP + P01_FL_GEST_PROTEZIONE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P01_ID_RICH_EST = 9;
            public static final int P01_COD_COMP_ANIA = 5;
            public static final int P01_TP_MOVI = 5;
            public static final int P01_DT_FORM_RICH = 8;
            public static final int P01_DT_INVIO_RICH = 8;
            public static final int P01_DT_PERV_RICH = 8;
            public static final int P01_DT_RGSTRZ_RICH = 8;
            public static final int P01_ID_OGG = 9;
            public static final int P01_DS_VER = 9;
            public static final int P01_DS_TS_CPTZ = 18;
            public static final int P01_COD_CAN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
