package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV4911<br>
 * Variable: LDBV4911 from copybook LDBV4911<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv4911 {

    //==== PROPERTIES ====
    //Original name: LDBV4911-TP-VAL-AST-1
    private String tpValAst1 = DefaultValues.stringVal(Len.TP_VAL_AST1);
    //Original name: LDBV4911-TP-VAL-AST-2
    private String tpValAst2 = DefaultValues.stringVal(Len.TP_VAL_AST2);
    //Original name: LDBV4911-TP-VAL-AST-3
    private String tpValAst3 = DefaultValues.stringVal(Len.TP_VAL_AST3);
    //Original name: LDBV4911-TP-VAL-AST-4
    private String tpValAst4 = DefaultValues.stringVal(Len.TP_VAL_AST4);
    //Original name: LDBV4911-TP-VAL-AST-5
    private String tpValAst5 = DefaultValues.stringVal(Len.TP_VAL_AST5);
    //Original name: LDBV4911-TP-VAL-AST-6
    private String tpValAst6 = DefaultValues.stringVal(Len.TP_VAL_AST6);
    //Original name: LDBV4911-TP-VAL-AST-7
    private String tpValAst7 = DefaultValues.stringVal(Len.TP_VAL_AST7);
    //Original name: LDBV4911-TP-VAL-AST-8
    private String tpValAst8 = DefaultValues.stringVal(Len.TP_VAL_AST8);
    //Original name: LDBV4911-TP-VAL-AST-9
    private String tpValAst9 = DefaultValues.stringVal(Len.TP_VAL_AST9);
    //Original name: LDBV4911-TP-VAL-AST-10
    private String tpValAst10 = DefaultValues.stringVal(Len.TP_VAL_AST10);
    //Original name: LDBV4911-ID-TRCH-DI-GAR
    private int idTrchDiGar = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbv4911Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV4911];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV4911);
        setLdbv4911Bytes(buffer, 1);
    }

    public String getLdbv4911Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv4911Bytes());
    }

    public byte[] getLdbv4911Bytes() {
        byte[] buffer = new byte[Len.LDBV4911];
        return getLdbv4911Bytes(buffer, 1);
    }

    public void setLdbv4911Bytes(byte[] buffer, int offset) {
        int position = offset;
        tpValAst1 = MarshalByte.readString(buffer, position, Len.TP_VAL_AST1);
        position += Len.TP_VAL_AST1;
        tpValAst2 = MarshalByte.readString(buffer, position, Len.TP_VAL_AST2);
        position += Len.TP_VAL_AST2;
        tpValAst3 = MarshalByte.readString(buffer, position, Len.TP_VAL_AST3);
        position += Len.TP_VAL_AST3;
        tpValAst4 = MarshalByte.readString(buffer, position, Len.TP_VAL_AST4);
        position += Len.TP_VAL_AST4;
        tpValAst5 = MarshalByte.readString(buffer, position, Len.TP_VAL_AST5);
        position += Len.TP_VAL_AST5;
        tpValAst6 = MarshalByte.readString(buffer, position, Len.TP_VAL_AST6);
        position += Len.TP_VAL_AST6;
        tpValAst7 = MarshalByte.readString(buffer, position, Len.TP_VAL_AST7);
        position += Len.TP_VAL_AST7;
        tpValAst8 = MarshalByte.readString(buffer, position, Len.TP_VAL_AST8);
        position += Len.TP_VAL_AST8;
        tpValAst9 = MarshalByte.readString(buffer, position, Len.TP_VAL_AST9);
        position += Len.TP_VAL_AST9;
        tpValAst10 = MarshalByte.readString(buffer, position, Len.TP_VAL_AST10);
        position += Len.TP_VAL_AST10;
        idTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_TRCH_DI_GAR, 0);
    }

    public byte[] getLdbv4911Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tpValAst1, Len.TP_VAL_AST1);
        position += Len.TP_VAL_AST1;
        MarshalByte.writeString(buffer, position, tpValAst2, Len.TP_VAL_AST2);
        position += Len.TP_VAL_AST2;
        MarshalByte.writeString(buffer, position, tpValAst3, Len.TP_VAL_AST3);
        position += Len.TP_VAL_AST3;
        MarshalByte.writeString(buffer, position, tpValAst4, Len.TP_VAL_AST4);
        position += Len.TP_VAL_AST4;
        MarshalByte.writeString(buffer, position, tpValAst5, Len.TP_VAL_AST5);
        position += Len.TP_VAL_AST5;
        MarshalByte.writeString(buffer, position, tpValAst6, Len.TP_VAL_AST6);
        position += Len.TP_VAL_AST6;
        MarshalByte.writeString(buffer, position, tpValAst7, Len.TP_VAL_AST7);
        position += Len.TP_VAL_AST7;
        MarshalByte.writeString(buffer, position, tpValAst8, Len.TP_VAL_AST8);
        position += Len.TP_VAL_AST8;
        MarshalByte.writeString(buffer, position, tpValAst9, Len.TP_VAL_AST9);
        position += Len.TP_VAL_AST9;
        MarshalByte.writeString(buffer, position, tpValAst10, Len.TP_VAL_AST10);
        position += Len.TP_VAL_AST10;
        MarshalByte.writeIntAsPacked(buffer, position, idTrchDiGar, Len.Int.ID_TRCH_DI_GAR, 0);
        return buffer;
    }

    public void setTpValAst1(String tpValAst1) {
        this.tpValAst1 = Functions.subString(tpValAst1, Len.TP_VAL_AST1);
    }

    public String getTpValAst1() {
        return this.tpValAst1;
    }

    public void setTpValAst2(String tpValAst2) {
        this.tpValAst2 = Functions.subString(tpValAst2, Len.TP_VAL_AST2);
    }

    public String getTpValAst2() {
        return this.tpValAst2;
    }

    public void setTpValAst3(String tpValAst3) {
        this.tpValAst3 = Functions.subString(tpValAst3, Len.TP_VAL_AST3);
    }

    public String getTpValAst3() {
        return this.tpValAst3;
    }

    public void setTpValAst4(String tpValAst4) {
        this.tpValAst4 = Functions.subString(tpValAst4, Len.TP_VAL_AST4);
    }

    public String getTpValAst4() {
        return this.tpValAst4;
    }

    public void setTpValAst5(String tpValAst5) {
        this.tpValAst5 = Functions.subString(tpValAst5, Len.TP_VAL_AST5);
    }

    public String getTpValAst5() {
        return this.tpValAst5;
    }

    public void setTpValAst6(String tpValAst6) {
        this.tpValAst6 = Functions.subString(tpValAst6, Len.TP_VAL_AST6);
    }

    public String getTpValAst6() {
        return this.tpValAst6;
    }

    public void setTpValAst7(String tpValAst7) {
        this.tpValAst7 = Functions.subString(tpValAst7, Len.TP_VAL_AST7);
    }

    public String getTpValAst7() {
        return this.tpValAst7;
    }

    public void setTpValAst8(String tpValAst8) {
        this.tpValAst8 = Functions.subString(tpValAst8, Len.TP_VAL_AST8);
    }

    public String getTpValAst8() {
        return this.tpValAst8;
    }

    public void setTpValAst9(String tpValAst9) {
        this.tpValAst9 = Functions.subString(tpValAst9, Len.TP_VAL_AST9);
    }

    public String getTpValAst9() {
        return this.tpValAst9;
    }

    public void setTpValAst10(String tpValAst10) {
        this.tpValAst10 = Functions.subString(tpValAst10, Len.TP_VAL_AST10);
    }

    public String getTpValAst10() {
        return this.tpValAst10;
    }

    public void setIdTrchDiGar(int idTrchDiGar) {
        this.idTrchDiGar = idTrchDiGar;
    }

    public int getIdTrchDiGar() {
        return this.idTrchDiGar;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_VAL_AST1 = 2;
        public static final int TP_VAL_AST2 = 2;
        public static final int TP_VAL_AST3 = 2;
        public static final int TP_VAL_AST4 = 2;
        public static final int TP_VAL_AST5 = 2;
        public static final int TP_VAL_AST6 = 2;
        public static final int TP_VAL_AST7 = 2;
        public static final int TP_VAL_AST8 = 2;
        public static final int TP_VAL_AST9 = 2;
        public static final int TP_VAL_AST10 = 2;
        public static final int ID_TRCH_DI_GAR = 5;
        public static final int LDBV4911 = TP_VAL_AST1 + TP_VAL_AST2 + TP_VAL_AST3 + TP_VAL_AST4 + TP_VAL_AST5 + TP_VAL_AST6 + TP_VAL_AST7 + TP_VAL_AST8 + TP_VAL_AST9 + TP_VAL_AST10 + ID_TRCH_DI_GAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_TRCH_DI_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
