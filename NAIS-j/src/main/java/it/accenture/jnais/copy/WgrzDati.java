package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WgrzAaPagPreUni;
import it.accenture.jnais.ws.redefines.WgrzAaStab;
import it.accenture.jnais.ws.redefines.WgrzDtDecor;
import it.accenture.jnais.ws.redefines.WgrzDtEndCarz;
import it.accenture.jnais.ws.redefines.WgrzDtIniValTar;
import it.accenture.jnais.ws.redefines.WgrzDtPresc;
import it.accenture.jnais.ws.redefines.WgrzDtScad;
import it.accenture.jnais.ws.redefines.WgrzDtVarzTpIas;
import it.accenture.jnais.ws.redefines.WgrzDurAa;
import it.accenture.jnais.ws.redefines.WgrzDurGg;
import it.accenture.jnais.ws.redefines.WgrzDurMm;
import it.accenture.jnais.ws.redefines.WgrzEtaAa1oAssto;
import it.accenture.jnais.ws.redefines.WgrzEtaAa2oAssto;
import it.accenture.jnais.ws.redefines.WgrzEtaAa3oAssto;
import it.accenture.jnais.ws.redefines.WgrzEtaAScad;
import it.accenture.jnais.ws.redefines.WgrzEtaMm1oAssto;
import it.accenture.jnais.ws.redefines.WgrzEtaMm2oAssto;
import it.accenture.jnais.ws.redefines.WgrzEtaMm3oAssto;
import it.accenture.jnais.ws.redefines.WgrzFrazDecrCpt;
import it.accenture.jnais.ws.redefines.WgrzFrazIniErogRen;
import it.accenture.jnais.ws.redefines.WgrzId1oAssto;
import it.accenture.jnais.ws.redefines.WgrzId2oAssto;
import it.accenture.jnais.ws.redefines.WgrzId3oAssto;
import it.accenture.jnais.ws.redefines.WgrzIdAdes;
import it.accenture.jnais.ws.redefines.WgrzIdMoviChiu;
import it.accenture.jnais.ws.redefines.WgrzMm1oRat;
import it.accenture.jnais.ws.redefines.WgrzMmPagPreUni;
import it.accenture.jnais.ws.redefines.WgrzNumAaPagPre;
import it.accenture.jnais.ws.redefines.WgrzPc1oRat;
import it.accenture.jnais.ws.redefines.WgrzPcOpz;
import it.accenture.jnais.ws.redefines.WgrzPcRevrsb;
import it.accenture.jnais.ws.redefines.WgrzPcRipPre;
import it.accenture.jnais.ws.redefines.WgrzTpGar;
import it.accenture.jnais.ws.redefines.WgrzTpInvst;
import it.accenture.jnais.ws.redefines.WgrzTsStabLimitata;

/**Original name: WGRZ-DATI<br>
 * Variable: WGRZ-DATI from copybook LCCVGRZ1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WgrzDati {

    //==== PROPERTIES ====
    //Original name: WGRZ-ID-GAR
    private int wgrzIdGar = DefaultValues.INT_VAL;
    //Original name: WGRZ-ID-ADES
    private WgrzIdAdes wgrzIdAdes = new WgrzIdAdes();
    //Original name: WGRZ-ID-POLI
    private int wgrzIdPoli = DefaultValues.INT_VAL;
    //Original name: WGRZ-ID-MOVI-CRZ
    private int wgrzIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WGRZ-ID-MOVI-CHIU
    private WgrzIdMoviChiu wgrzIdMoviChiu = new WgrzIdMoviChiu();
    //Original name: WGRZ-DT-INI-EFF
    private int wgrzDtIniEff = DefaultValues.INT_VAL;
    //Original name: WGRZ-DT-END-EFF
    private int wgrzDtEndEff = DefaultValues.INT_VAL;
    //Original name: WGRZ-COD-COMP-ANIA
    private int wgrzCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WGRZ-IB-OGG
    private String wgrzIbOgg = DefaultValues.stringVal(Len.WGRZ_IB_OGG);
    //Original name: WGRZ-DT-DECOR
    private WgrzDtDecor wgrzDtDecor = new WgrzDtDecor();
    //Original name: WGRZ-DT-SCAD
    private WgrzDtScad wgrzDtScad = new WgrzDtScad();
    //Original name: WGRZ-COD-SEZ
    private String wgrzCodSez = DefaultValues.stringVal(Len.WGRZ_COD_SEZ);
    //Original name: WGRZ-COD-TARI
    private String wgrzCodTari = DefaultValues.stringVal(Len.WGRZ_COD_TARI);
    //Original name: WGRZ-RAMO-BILA
    private String wgrzRamoBila = DefaultValues.stringVal(Len.WGRZ_RAMO_BILA);
    //Original name: WGRZ-DT-INI-VAL-TAR
    private WgrzDtIniValTar wgrzDtIniValTar = new WgrzDtIniValTar();
    //Original name: WGRZ-ID-1O-ASSTO
    private WgrzId1oAssto wgrzId1oAssto = new WgrzId1oAssto();
    //Original name: WGRZ-ID-2O-ASSTO
    private WgrzId2oAssto wgrzId2oAssto = new WgrzId2oAssto();
    //Original name: WGRZ-ID-3O-ASSTO
    private WgrzId3oAssto wgrzId3oAssto = new WgrzId3oAssto();
    //Original name: WGRZ-TP-GAR
    private WgrzTpGar wgrzTpGar = new WgrzTpGar();
    //Original name: WGRZ-TP-RSH
    private String wgrzTpRsh = DefaultValues.stringVal(Len.WGRZ_TP_RSH);
    //Original name: WGRZ-TP-INVST
    private WgrzTpInvst wgrzTpInvst = new WgrzTpInvst();
    //Original name: WGRZ-MOD-PAG-GARCOL
    private String wgrzModPagGarcol = DefaultValues.stringVal(Len.WGRZ_MOD_PAG_GARCOL);
    //Original name: WGRZ-TP-PER-PRE
    private String wgrzTpPerPre = DefaultValues.stringVal(Len.WGRZ_TP_PER_PRE);
    //Original name: WGRZ-ETA-AA-1O-ASSTO
    private WgrzEtaAa1oAssto wgrzEtaAa1oAssto = new WgrzEtaAa1oAssto();
    //Original name: WGRZ-ETA-MM-1O-ASSTO
    private WgrzEtaMm1oAssto wgrzEtaMm1oAssto = new WgrzEtaMm1oAssto();
    //Original name: WGRZ-ETA-AA-2O-ASSTO
    private WgrzEtaAa2oAssto wgrzEtaAa2oAssto = new WgrzEtaAa2oAssto();
    //Original name: WGRZ-ETA-MM-2O-ASSTO
    private WgrzEtaMm2oAssto wgrzEtaMm2oAssto = new WgrzEtaMm2oAssto();
    //Original name: WGRZ-ETA-AA-3O-ASSTO
    private WgrzEtaAa3oAssto wgrzEtaAa3oAssto = new WgrzEtaAa3oAssto();
    //Original name: WGRZ-ETA-MM-3O-ASSTO
    private WgrzEtaMm3oAssto wgrzEtaMm3oAssto = new WgrzEtaMm3oAssto();
    //Original name: WGRZ-TP-EMIS-PUR
    private char wgrzTpEmisPur = DefaultValues.CHAR_VAL;
    //Original name: WGRZ-ETA-A-SCAD
    private WgrzEtaAScad wgrzEtaAScad = new WgrzEtaAScad();
    //Original name: WGRZ-TP-CALC-PRE-PRSTZ
    private String wgrzTpCalcPrePrstz = DefaultValues.stringVal(Len.WGRZ_TP_CALC_PRE_PRSTZ);
    //Original name: WGRZ-TP-PRE
    private char wgrzTpPre = DefaultValues.CHAR_VAL;
    //Original name: WGRZ-TP-DUR
    private String wgrzTpDur = DefaultValues.stringVal(Len.WGRZ_TP_DUR);
    //Original name: WGRZ-DUR-AA
    private WgrzDurAa wgrzDurAa = new WgrzDurAa();
    //Original name: WGRZ-DUR-MM
    private WgrzDurMm wgrzDurMm = new WgrzDurMm();
    //Original name: WGRZ-DUR-GG
    private WgrzDurGg wgrzDurGg = new WgrzDurGg();
    //Original name: WGRZ-NUM-AA-PAG-PRE
    private WgrzNumAaPagPre wgrzNumAaPagPre = new WgrzNumAaPagPre();
    //Original name: WGRZ-AA-PAG-PRE-UNI
    private WgrzAaPagPreUni wgrzAaPagPreUni = new WgrzAaPagPreUni();
    //Original name: WGRZ-MM-PAG-PRE-UNI
    private WgrzMmPagPreUni wgrzMmPagPreUni = new WgrzMmPagPreUni();
    //Original name: WGRZ-FRAZ-INI-EROG-REN
    private WgrzFrazIniErogRen wgrzFrazIniErogRen = new WgrzFrazIniErogRen();
    //Original name: WGRZ-MM-1O-RAT
    private WgrzMm1oRat wgrzMm1oRat = new WgrzMm1oRat();
    //Original name: WGRZ-PC-1O-RAT
    private WgrzPc1oRat wgrzPc1oRat = new WgrzPc1oRat();
    //Original name: WGRZ-TP-PRSTZ-ASSTA
    private String wgrzTpPrstzAssta = DefaultValues.stringVal(Len.WGRZ_TP_PRSTZ_ASSTA);
    //Original name: WGRZ-DT-END-CARZ
    private WgrzDtEndCarz wgrzDtEndCarz = new WgrzDtEndCarz();
    //Original name: WGRZ-PC-RIP-PRE
    private WgrzPcRipPre wgrzPcRipPre = new WgrzPcRipPre();
    //Original name: WGRZ-COD-FND
    private String wgrzCodFnd = DefaultValues.stringVal(Len.WGRZ_COD_FND);
    //Original name: WGRZ-AA-REN-CER
    private String wgrzAaRenCer = DefaultValues.stringVal(Len.WGRZ_AA_REN_CER);
    //Original name: WGRZ-PC-REVRSB
    private WgrzPcRevrsb wgrzPcRevrsb = new WgrzPcRevrsb();
    //Original name: WGRZ-TP-PC-RIP
    private String wgrzTpPcRip = DefaultValues.stringVal(Len.WGRZ_TP_PC_RIP);
    //Original name: WGRZ-PC-OPZ
    private WgrzPcOpz wgrzPcOpz = new WgrzPcOpz();
    //Original name: WGRZ-TP-IAS
    private String wgrzTpIas = DefaultValues.stringVal(Len.WGRZ_TP_IAS);
    //Original name: WGRZ-TP-STAB
    private String wgrzTpStab = DefaultValues.stringVal(Len.WGRZ_TP_STAB);
    //Original name: WGRZ-TP-ADEG-PRE
    private char wgrzTpAdegPre = DefaultValues.CHAR_VAL;
    //Original name: WGRZ-DT-VARZ-TP-IAS
    private WgrzDtVarzTpIas wgrzDtVarzTpIas = new WgrzDtVarzTpIas();
    //Original name: WGRZ-FRAZ-DECR-CPT
    private WgrzFrazDecrCpt wgrzFrazDecrCpt = new WgrzFrazDecrCpt();
    //Original name: WGRZ-COD-TRAT-RIASS
    private String wgrzCodTratRiass = DefaultValues.stringVal(Len.WGRZ_COD_TRAT_RIASS);
    //Original name: WGRZ-TP-DT-EMIS-RIASS
    private String wgrzTpDtEmisRiass = DefaultValues.stringVal(Len.WGRZ_TP_DT_EMIS_RIASS);
    //Original name: WGRZ-TP-CESS-RIASS
    private String wgrzTpCessRiass = DefaultValues.stringVal(Len.WGRZ_TP_CESS_RIASS);
    //Original name: WGRZ-DS-RIGA
    private long wgrzDsRiga = DefaultValues.LONG_VAL;
    //Original name: WGRZ-DS-OPER-SQL
    private char wgrzDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WGRZ-DS-VER
    private int wgrzDsVer = DefaultValues.INT_VAL;
    //Original name: WGRZ-DS-TS-INI-CPTZ
    private long wgrzDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WGRZ-DS-TS-END-CPTZ
    private long wgrzDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WGRZ-DS-UTENTE
    private String wgrzDsUtente = DefaultValues.stringVal(Len.WGRZ_DS_UTENTE);
    //Original name: WGRZ-DS-STATO-ELAB
    private char wgrzDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WGRZ-AA-STAB
    private WgrzAaStab wgrzAaStab = new WgrzAaStab();
    //Original name: WGRZ-TS-STAB-LIMITATA
    private WgrzTsStabLimitata wgrzTsStabLimitata = new WgrzTsStabLimitata();
    //Original name: WGRZ-DT-PRESC
    private WgrzDtPresc wgrzDtPresc = new WgrzDtPresc();
    //Original name: WGRZ-RSH-INVST
    private char wgrzRshInvst = DefaultValues.CHAR_VAL;
    //Original name: WGRZ-TP-RAMO-BILA
    private String wgrzTpRamoBila = DefaultValues.stringVal(Len.WGRZ_TP_RAMO_BILA);

    //==== METHODS ====
    public String getWgrzDatiFormatted() {
        return MarshalByteExt.bufferToStr(getWgrzDatiBytes());
    }

    public byte[] getWgrzDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wgrzIdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRZ_ID_GAR, 0);
        position += Len.WGRZ_ID_GAR;
        wgrzIdAdes.setWgrzIdAdesFromBuffer(buffer, position);
        position += WgrzIdAdes.Len.WGRZ_ID_ADES;
        wgrzIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRZ_ID_POLI, 0);
        position += Len.WGRZ_ID_POLI;
        wgrzIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRZ_ID_MOVI_CRZ, 0);
        position += Len.WGRZ_ID_MOVI_CRZ;
        wgrzIdMoviChiu.setWgrzIdMoviChiuFromBuffer(buffer, position);
        position += WgrzIdMoviChiu.Len.WGRZ_ID_MOVI_CHIU;
        wgrzDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRZ_DT_INI_EFF, 0);
        position += Len.WGRZ_DT_INI_EFF;
        wgrzDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRZ_DT_END_EFF, 0);
        position += Len.WGRZ_DT_END_EFF;
        wgrzCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRZ_COD_COMP_ANIA, 0);
        position += Len.WGRZ_COD_COMP_ANIA;
        wgrzIbOgg = MarshalByte.readString(buffer, position, Len.WGRZ_IB_OGG);
        position += Len.WGRZ_IB_OGG;
        wgrzDtDecor.setWgrzDtDecorFromBuffer(buffer, position);
        position += WgrzDtDecor.Len.WGRZ_DT_DECOR;
        wgrzDtScad.setWgrzDtScadFromBuffer(buffer, position);
        position += WgrzDtScad.Len.WGRZ_DT_SCAD;
        wgrzCodSez = MarshalByte.readString(buffer, position, Len.WGRZ_COD_SEZ);
        position += Len.WGRZ_COD_SEZ;
        wgrzCodTari = MarshalByte.readString(buffer, position, Len.WGRZ_COD_TARI);
        position += Len.WGRZ_COD_TARI;
        wgrzRamoBila = MarshalByte.readString(buffer, position, Len.WGRZ_RAMO_BILA);
        position += Len.WGRZ_RAMO_BILA;
        wgrzDtIniValTar.setWgrzDtIniValTarFromBuffer(buffer, position);
        position += WgrzDtIniValTar.Len.WGRZ_DT_INI_VAL_TAR;
        wgrzId1oAssto.setWgrzId1oAsstoFromBuffer(buffer, position);
        position += WgrzId1oAssto.Len.WGRZ_ID1O_ASSTO;
        wgrzId2oAssto.setWgrzId2oAsstoFromBuffer(buffer, position);
        position += WgrzId2oAssto.Len.WGRZ_ID2O_ASSTO;
        wgrzId3oAssto.setWgrzId3oAsstoFromBuffer(buffer, position);
        position += WgrzId3oAssto.Len.WGRZ_ID3O_ASSTO;
        wgrzTpGar.setWgrzTpGarFromBuffer(buffer, position);
        position += WgrzTpGar.Len.WGRZ_TP_GAR;
        wgrzTpRsh = MarshalByte.readString(buffer, position, Len.WGRZ_TP_RSH);
        position += Len.WGRZ_TP_RSH;
        wgrzTpInvst.setWgrzTpInvstFromBuffer(buffer, position);
        position += WgrzTpInvst.Len.WGRZ_TP_INVST;
        wgrzModPagGarcol = MarshalByte.readString(buffer, position, Len.WGRZ_MOD_PAG_GARCOL);
        position += Len.WGRZ_MOD_PAG_GARCOL;
        wgrzTpPerPre = MarshalByte.readString(buffer, position, Len.WGRZ_TP_PER_PRE);
        position += Len.WGRZ_TP_PER_PRE;
        wgrzEtaAa1oAssto.setWgrzEtaAa1oAsstoFromBuffer(buffer, position);
        position += WgrzEtaAa1oAssto.Len.WGRZ_ETA_AA1O_ASSTO;
        wgrzEtaMm1oAssto.setWgrzEtaMm1oAsstoFromBuffer(buffer, position);
        position += WgrzEtaMm1oAssto.Len.WGRZ_ETA_MM1O_ASSTO;
        wgrzEtaAa2oAssto.setWgrzEtaAa2oAsstoFromBuffer(buffer, position);
        position += WgrzEtaAa2oAssto.Len.WGRZ_ETA_AA2O_ASSTO;
        wgrzEtaMm2oAssto.setWgrzEtaMm2oAsstoFromBuffer(buffer, position);
        position += WgrzEtaMm2oAssto.Len.WGRZ_ETA_MM2O_ASSTO;
        wgrzEtaAa3oAssto.setWgrzEtaAa3oAsstoFromBuffer(buffer, position);
        position += WgrzEtaAa3oAssto.Len.WGRZ_ETA_AA3O_ASSTO;
        wgrzEtaMm3oAssto.setWgrzEtaMm3oAsstoFromBuffer(buffer, position);
        position += WgrzEtaMm3oAssto.Len.WGRZ_ETA_MM3O_ASSTO;
        wgrzTpEmisPur = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wgrzEtaAScad.setWgrzEtaAScadFromBuffer(buffer, position);
        position += WgrzEtaAScad.Len.WGRZ_ETA_A_SCAD;
        wgrzTpCalcPrePrstz = MarshalByte.readString(buffer, position, Len.WGRZ_TP_CALC_PRE_PRSTZ);
        position += Len.WGRZ_TP_CALC_PRE_PRSTZ;
        wgrzTpPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wgrzTpDur = MarshalByte.readString(buffer, position, Len.WGRZ_TP_DUR);
        position += Len.WGRZ_TP_DUR;
        wgrzDurAa.setWgrzDurAaFromBuffer(buffer, position);
        position += WgrzDurAa.Len.WGRZ_DUR_AA;
        wgrzDurMm.setWgrzDurMmFromBuffer(buffer, position);
        position += WgrzDurMm.Len.WGRZ_DUR_MM;
        wgrzDurGg.setWgrzDurGgFromBuffer(buffer, position);
        position += WgrzDurGg.Len.WGRZ_DUR_GG;
        wgrzNumAaPagPre.setWgrzNumAaPagPreFromBuffer(buffer, position);
        position += WgrzNumAaPagPre.Len.WGRZ_NUM_AA_PAG_PRE;
        wgrzAaPagPreUni.setWgrzAaPagPreUniFromBuffer(buffer, position);
        position += WgrzAaPagPreUni.Len.WGRZ_AA_PAG_PRE_UNI;
        wgrzMmPagPreUni.setWgrzMmPagPreUniFromBuffer(buffer, position);
        position += WgrzMmPagPreUni.Len.WGRZ_MM_PAG_PRE_UNI;
        wgrzFrazIniErogRen.setWgrzFrazIniErogRenFromBuffer(buffer, position);
        position += WgrzFrazIniErogRen.Len.WGRZ_FRAZ_INI_EROG_REN;
        wgrzMm1oRat.setWgrzMm1oRatFromBuffer(buffer, position);
        position += WgrzMm1oRat.Len.WGRZ_MM1O_RAT;
        wgrzPc1oRat.setWgrzPc1oRatFromBuffer(buffer, position);
        position += WgrzPc1oRat.Len.WGRZ_PC1O_RAT;
        wgrzTpPrstzAssta = MarshalByte.readString(buffer, position, Len.WGRZ_TP_PRSTZ_ASSTA);
        position += Len.WGRZ_TP_PRSTZ_ASSTA;
        wgrzDtEndCarz.setWgrzDtEndCarzFromBuffer(buffer, position);
        position += WgrzDtEndCarz.Len.WGRZ_DT_END_CARZ;
        wgrzPcRipPre.setWgrzPcRipPreFromBuffer(buffer, position);
        position += WgrzPcRipPre.Len.WGRZ_PC_RIP_PRE;
        wgrzCodFnd = MarshalByte.readString(buffer, position, Len.WGRZ_COD_FND);
        position += Len.WGRZ_COD_FND;
        wgrzAaRenCer = MarshalByte.readString(buffer, position, Len.WGRZ_AA_REN_CER);
        position += Len.WGRZ_AA_REN_CER;
        wgrzPcRevrsb.setWgrzPcRevrsbFromBuffer(buffer, position);
        position += WgrzPcRevrsb.Len.WGRZ_PC_REVRSB;
        wgrzTpPcRip = MarshalByte.readString(buffer, position, Len.WGRZ_TP_PC_RIP);
        position += Len.WGRZ_TP_PC_RIP;
        wgrzPcOpz.setWgrzPcOpzFromBuffer(buffer, position);
        position += WgrzPcOpz.Len.WGRZ_PC_OPZ;
        wgrzTpIas = MarshalByte.readString(buffer, position, Len.WGRZ_TP_IAS);
        position += Len.WGRZ_TP_IAS;
        wgrzTpStab = MarshalByte.readString(buffer, position, Len.WGRZ_TP_STAB);
        position += Len.WGRZ_TP_STAB;
        wgrzTpAdegPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wgrzDtVarzTpIas.setWgrzDtVarzTpIasFromBuffer(buffer, position);
        position += WgrzDtVarzTpIas.Len.WGRZ_DT_VARZ_TP_IAS;
        wgrzFrazDecrCpt.setWgrzFrazDecrCptFromBuffer(buffer, position);
        position += WgrzFrazDecrCpt.Len.WGRZ_FRAZ_DECR_CPT;
        wgrzCodTratRiass = MarshalByte.readString(buffer, position, Len.WGRZ_COD_TRAT_RIASS);
        position += Len.WGRZ_COD_TRAT_RIASS;
        wgrzTpDtEmisRiass = MarshalByte.readString(buffer, position, Len.WGRZ_TP_DT_EMIS_RIASS);
        position += Len.WGRZ_TP_DT_EMIS_RIASS;
        wgrzTpCessRiass = MarshalByte.readString(buffer, position, Len.WGRZ_TP_CESS_RIASS);
        position += Len.WGRZ_TP_CESS_RIASS;
        wgrzDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WGRZ_DS_RIGA, 0);
        position += Len.WGRZ_DS_RIGA;
        wgrzDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wgrzDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRZ_DS_VER, 0);
        position += Len.WGRZ_DS_VER;
        wgrzDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WGRZ_DS_TS_INI_CPTZ, 0);
        position += Len.WGRZ_DS_TS_INI_CPTZ;
        wgrzDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WGRZ_DS_TS_END_CPTZ, 0);
        position += Len.WGRZ_DS_TS_END_CPTZ;
        wgrzDsUtente = MarshalByte.readString(buffer, position, Len.WGRZ_DS_UTENTE);
        position += Len.WGRZ_DS_UTENTE;
        wgrzDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wgrzAaStab.setWgrzAaStabFromBuffer(buffer, position);
        position += WgrzAaStab.Len.WGRZ_AA_STAB;
        wgrzTsStabLimitata.setWgrzTsStabLimitataFromBuffer(buffer, position);
        position += WgrzTsStabLimitata.Len.WGRZ_TS_STAB_LIMITATA;
        wgrzDtPresc.setWgrzDtPrescFromBuffer(buffer, position);
        position += WgrzDtPresc.Len.WGRZ_DT_PRESC;
        wgrzRshInvst = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wgrzTpRamoBila = MarshalByte.readString(buffer, position, Len.WGRZ_TP_RAMO_BILA);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wgrzIdGar, Len.Int.WGRZ_ID_GAR, 0);
        position += Len.WGRZ_ID_GAR;
        wgrzIdAdes.getWgrzIdAdesAsBuffer(buffer, position);
        position += WgrzIdAdes.Len.WGRZ_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wgrzIdPoli, Len.Int.WGRZ_ID_POLI, 0);
        position += Len.WGRZ_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wgrzIdMoviCrz, Len.Int.WGRZ_ID_MOVI_CRZ, 0);
        position += Len.WGRZ_ID_MOVI_CRZ;
        wgrzIdMoviChiu.getWgrzIdMoviChiuAsBuffer(buffer, position);
        position += WgrzIdMoviChiu.Len.WGRZ_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wgrzDtIniEff, Len.Int.WGRZ_DT_INI_EFF, 0);
        position += Len.WGRZ_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wgrzDtEndEff, Len.Int.WGRZ_DT_END_EFF, 0);
        position += Len.WGRZ_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wgrzCodCompAnia, Len.Int.WGRZ_COD_COMP_ANIA, 0);
        position += Len.WGRZ_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wgrzIbOgg, Len.WGRZ_IB_OGG);
        position += Len.WGRZ_IB_OGG;
        wgrzDtDecor.getWgrzDtDecorAsBuffer(buffer, position);
        position += WgrzDtDecor.Len.WGRZ_DT_DECOR;
        wgrzDtScad.getWgrzDtScadAsBuffer(buffer, position);
        position += WgrzDtScad.Len.WGRZ_DT_SCAD;
        MarshalByte.writeString(buffer, position, wgrzCodSez, Len.WGRZ_COD_SEZ);
        position += Len.WGRZ_COD_SEZ;
        MarshalByte.writeString(buffer, position, wgrzCodTari, Len.WGRZ_COD_TARI);
        position += Len.WGRZ_COD_TARI;
        MarshalByte.writeString(buffer, position, wgrzRamoBila, Len.WGRZ_RAMO_BILA);
        position += Len.WGRZ_RAMO_BILA;
        wgrzDtIniValTar.getWgrzDtIniValTarAsBuffer(buffer, position);
        position += WgrzDtIniValTar.Len.WGRZ_DT_INI_VAL_TAR;
        wgrzId1oAssto.getWgrzId1oAsstoAsBuffer(buffer, position);
        position += WgrzId1oAssto.Len.WGRZ_ID1O_ASSTO;
        wgrzId2oAssto.getWgrzId2oAsstoAsBuffer(buffer, position);
        position += WgrzId2oAssto.Len.WGRZ_ID2O_ASSTO;
        wgrzId3oAssto.getWgrzId3oAsstoAsBuffer(buffer, position);
        position += WgrzId3oAssto.Len.WGRZ_ID3O_ASSTO;
        wgrzTpGar.getWgrzTpGarAsBuffer(buffer, position);
        position += WgrzTpGar.Len.WGRZ_TP_GAR;
        MarshalByte.writeString(buffer, position, wgrzTpRsh, Len.WGRZ_TP_RSH);
        position += Len.WGRZ_TP_RSH;
        wgrzTpInvst.getWgrzTpInvstAsBuffer(buffer, position);
        position += WgrzTpInvst.Len.WGRZ_TP_INVST;
        MarshalByte.writeString(buffer, position, wgrzModPagGarcol, Len.WGRZ_MOD_PAG_GARCOL);
        position += Len.WGRZ_MOD_PAG_GARCOL;
        MarshalByte.writeString(buffer, position, wgrzTpPerPre, Len.WGRZ_TP_PER_PRE);
        position += Len.WGRZ_TP_PER_PRE;
        wgrzEtaAa1oAssto.getWgrzEtaAa1oAsstoAsBuffer(buffer, position);
        position += WgrzEtaAa1oAssto.Len.WGRZ_ETA_AA1O_ASSTO;
        wgrzEtaMm1oAssto.getWgrzEtaMm1oAsstoAsBuffer(buffer, position);
        position += WgrzEtaMm1oAssto.Len.WGRZ_ETA_MM1O_ASSTO;
        wgrzEtaAa2oAssto.getWgrzEtaAa2oAsstoAsBuffer(buffer, position);
        position += WgrzEtaAa2oAssto.Len.WGRZ_ETA_AA2O_ASSTO;
        wgrzEtaMm2oAssto.getWgrzEtaMm2oAsstoAsBuffer(buffer, position);
        position += WgrzEtaMm2oAssto.Len.WGRZ_ETA_MM2O_ASSTO;
        wgrzEtaAa3oAssto.getWgrzEtaAa3oAsstoAsBuffer(buffer, position);
        position += WgrzEtaAa3oAssto.Len.WGRZ_ETA_AA3O_ASSTO;
        wgrzEtaMm3oAssto.getWgrzEtaMm3oAsstoAsBuffer(buffer, position);
        position += WgrzEtaMm3oAssto.Len.WGRZ_ETA_MM3O_ASSTO;
        MarshalByte.writeChar(buffer, position, wgrzTpEmisPur);
        position += Types.CHAR_SIZE;
        wgrzEtaAScad.getWgrzEtaAScadAsBuffer(buffer, position);
        position += WgrzEtaAScad.Len.WGRZ_ETA_A_SCAD;
        MarshalByte.writeString(buffer, position, wgrzTpCalcPrePrstz, Len.WGRZ_TP_CALC_PRE_PRSTZ);
        position += Len.WGRZ_TP_CALC_PRE_PRSTZ;
        MarshalByte.writeChar(buffer, position, wgrzTpPre);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wgrzTpDur, Len.WGRZ_TP_DUR);
        position += Len.WGRZ_TP_DUR;
        wgrzDurAa.getWgrzDurAaAsBuffer(buffer, position);
        position += WgrzDurAa.Len.WGRZ_DUR_AA;
        wgrzDurMm.getWgrzDurMmAsBuffer(buffer, position);
        position += WgrzDurMm.Len.WGRZ_DUR_MM;
        wgrzDurGg.getWgrzDurGgAsBuffer(buffer, position);
        position += WgrzDurGg.Len.WGRZ_DUR_GG;
        wgrzNumAaPagPre.getWgrzNumAaPagPreAsBuffer(buffer, position);
        position += WgrzNumAaPagPre.Len.WGRZ_NUM_AA_PAG_PRE;
        wgrzAaPagPreUni.getWgrzAaPagPreUniAsBuffer(buffer, position);
        position += WgrzAaPagPreUni.Len.WGRZ_AA_PAG_PRE_UNI;
        wgrzMmPagPreUni.getWgrzMmPagPreUniAsBuffer(buffer, position);
        position += WgrzMmPagPreUni.Len.WGRZ_MM_PAG_PRE_UNI;
        wgrzFrazIniErogRen.getWgrzFrazIniErogRenAsBuffer(buffer, position);
        position += WgrzFrazIniErogRen.Len.WGRZ_FRAZ_INI_EROG_REN;
        wgrzMm1oRat.getWgrzMm1oRatAsBuffer(buffer, position);
        position += WgrzMm1oRat.Len.WGRZ_MM1O_RAT;
        wgrzPc1oRat.getWgrzPc1oRatAsBuffer(buffer, position);
        position += WgrzPc1oRat.Len.WGRZ_PC1O_RAT;
        MarshalByte.writeString(buffer, position, wgrzTpPrstzAssta, Len.WGRZ_TP_PRSTZ_ASSTA);
        position += Len.WGRZ_TP_PRSTZ_ASSTA;
        wgrzDtEndCarz.getWgrzDtEndCarzAsBuffer(buffer, position);
        position += WgrzDtEndCarz.Len.WGRZ_DT_END_CARZ;
        wgrzPcRipPre.getWgrzPcRipPreAsBuffer(buffer, position);
        position += WgrzPcRipPre.Len.WGRZ_PC_RIP_PRE;
        MarshalByte.writeString(buffer, position, wgrzCodFnd, Len.WGRZ_COD_FND);
        position += Len.WGRZ_COD_FND;
        MarshalByte.writeString(buffer, position, wgrzAaRenCer, Len.WGRZ_AA_REN_CER);
        position += Len.WGRZ_AA_REN_CER;
        wgrzPcRevrsb.getWgrzPcRevrsbAsBuffer(buffer, position);
        position += WgrzPcRevrsb.Len.WGRZ_PC_REVRSB;
        MarshalByte.writeString(buffer, position, wgrzTpPcRip, Len.WGRZ_TP_PC_RIP);
        position += Len.WGRZ_TP_PC_RIP;
        wgrzPcOpz.getWgrzPcOpzAsBuffer(buffer, position);
        position += WgrzPcOpz.Len.WGRZ_PC_OPZ;
        MarshalByte.writeString(buffer, position, wgrzTpIas, Len.WGRZ_TP_IAS);
        position += Len.WGRZ_TP_IAS;
        MarshalByte.writeString(buffer, position, wgrzTpStab, Len.WGRZ_TP_STAB);
        position += Len.WGRZ_TP_STAB;
        MarshalByte.writeChar(buffer, position, wgrzTpAdegPre);
        position += Types.CHAR_SIZE;
        wgrzDtVarzTpIas.getWgrzDtVarzTpIasAsBuffer(buffer, position);
        position += WgrzDtVarzTpIas.Len.WGRZ_DT_VARZ_TP_IAS;
        wgrzFrazDecrCpt.getWgrzFrazDecrCptAsBuffer(buffer, position);
        position += WgrzFrazDecrCpt.Len.WGRZ_FRAZ_DECR_CPT;
        MarshalByte.writeString(buffer, position, wgrzCodTratRiass, Len.WGRZ_COD_TRAT_RIASS);
        position += Len.WGRZ_COD_TRAT_RIASS;
        MarshalByte.writeString(buffer, position, wgrzTpDtEmisRiass, Len.WGRZ_TP_DT_EMIS_RIASS);
        position += Len.WGRZ_TP_DT_EMIS_RIASS;
        MarshalByte.writeString(buffer, position, wgrzTpCessRiass, Len.WGRZ_TP_CESS_RIASS);
        position += Len.WGRZ_TP_CESS_RIASS;
        MarshalByte.writeLongAsPacked(buffer, position, wgrzDsRiga, Len.Int.WGRZ_DS_RIGA, 0);
        position += Len.WGRZ_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wgrzDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wgrzDsVer, Len.Int.WGRZ_DS_VER, 0);
        position += Len.WGRZ_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wgrzDsTsIniCptz, Len.Int.WGRZ_DS_TS_INI_CPTZ, 0);
        position += Len.WGRZ_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wgrzDsTsEndCptz, Len.Int.WGRZ_DS_TS_END_CPTZ, 0);
        position += Len.WGRZ_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wgrzDsUtente, Len.WGRZ_DS_UTENTE);
        position += Len.WGRZ_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wgrzDsStatoElab);
        position += Types.CHAR_SIZE;
        wgrzAaStab.getWgrzAaStabAsBuffer(buffer, position);
        position += WgrzAaStab.Len.WGRZ_AA_STAB;
        wgrzTsStabLimitata.getWgrzTsStabLimitataAsBuffer(buffer, position);
        position += WgrzTsStabLimitata.Len.WGRZ_TS_STAB_LIMITATA;
        wgrzDtPresc.getWgrzDtPrescAsBuffer(buffer, position);
        position += WgrzDtPresc.Len.WGRZ_DT_PRESC;
        MarshalByte.writeChar(buffer, position, wgrzRshInvst);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wgrzTpRamoBila, Len.WGRZ_TP_RAMO_BILA);
        return buffer;
    }

    public void initDatiSpaces() {
        wgrzIdGar = Types.INVALID_INT_VAL;
        wgrzIdAdes.initWgrzIdAdesSpaces();
        wgrzIdPoli = Types.INVALID_INT_VAL;
        wgrzIdMoviCrz = Types.INVALID_INT_VAL;
        wgrzIdMoviChiu.initWgrzIdMoviChiuSpaces();
        wgrzDtIniEff = Types.INVALID_INT_VAL;
        wgrzDtEndEff = Types.INVALID_INT_VAL;
        wgrzCodCompAnia = Types.INVALID_INT_VAL;
        wgrzIbOgg = "";
        wgrzDtDecor.initWgrzDtDecorSpaces();
        wgrzDtScad.initWgrzDtScadSpaces();
        wgrzCodSez = "";
        wgrzCodTari = "";
        wgrzRamoBila = "";
        wgrzDtIniValTar.initWgrzDtIniValTarSpaces();
        wgrzId1oAssto.initWgrzId1oAsstoSpaces();
        wgrzId2oAssto.initWgrzId2oAsstoSpaces();
        wgrzId3oAssto.initWgrzId3oAsstoSpaces();
        wgrzTpGar.initWgrzTpGarSpaces();
        wgrzTpRsh = "";
        wgrzTpInvst.initWgrzTpInvstSpaces();
        wgrzModPagGarcol = "";
        wgrzTpPerPre = "";
        wgrzEtaAa1oAssto.initWgrzEtaAa1oAsstoSpaces();
        wgrzEtaMm1oAssto.initWgrzEtaMm1oAsstoSpaces();
        wgrzEtaAa2oAssto.initWgrzEtaAa2oAsstoSpaces();
        wgrzEtaMm2oAssto.initWgrzEtaMm2oAsstoSpaces();
        wgrzEtaAa3oAssto.initWgrzEtaAa3oAsstoSpaces();
        wgrzEtaMm3oAssto.initWgrzEtaMm3oAsstoSpaces();
        wgrzTpEmisPur = Types.SPACE_CHAR;
        wgrzEtaAScad.initWgrzEtaAScadSpaces();
        wgrzTpCalcPrePrstz = "";
        wgrzTpPre = Types.SPACE_CHAR;
        wgrzTpDur = "";
        wgrzDurAa.initWgrzDurAaSpaces();
        wgrzDurMm.initWgrzDurMmSpaces();
        wgrzDurGg.initWgrzDurGgSpaces();
        wgrzNumAaPagPre.initWgrzNumAaPagPreSpaces();
        wgrzAaPagPreUni.initWgrzAaPagPreUniSpaces();
        wgrzMmPagPreUni.initWgrzMmPagPreUniSpaces();
        wgrzFrazIniErogRen.initWgrzFrazIniErogRenSpaces();
        wgrzMm1oRat.initWgrzMm1oRatSpaces();
        wgrzPc1oRat.initWgrzPc1oRatSpaces();
        wgrzTpPrstzAssta = "";
        wgrzDtEndCarz.initWgrzDtEndCarzSpaces();
        wgrzPcRipPre.initWgrzPcRipPreSpaces();
        wgrzCodFnd = "";
        wgrzAaRenCer = "";
        wgrzPcRevrsb.initWgrzPcRevrsbSpaces();
        wgrzTpPcRip = "";
        wgrzPcOpz.initWgrzPcOpzSpaces();
        wgrzTpIas = "";
        wgrzTpStab = "";
        wgrzTpAdegPre = Types.SPACE_CHAR;
        wgrzDtVarzTpIas.initWgrzDtVarzTpIasSpaces();
        wgrzFrazDecrCpt.initWgrzFrazDecrCptSpaces();
        wgrzCodTratRiass = "";
        wgrzTpDtEmisRiass = "";
        wgrzTpCessRiass = "";
        wgrzDsRiga = Types.INVALID_LONG_VAL;
        wgrzDsOperSql = Types.SPACE_CHAR;
        wgrzDsVer = Types.INVALID_INT_VAL;
        wgrzDsTsIniCptz = Types.INVALID_LONG_VAL;
        wgrzDsTsEndCptz = Types.INVALID_LONG_VAL;
        wgrzDsUtente = "";
        wgrzDsStatoElab = Types.SPACE_CHAR;
        wgrzAaStab.initWgrzAaStabSpaces();
        wgrzTsStabLimitata.initWgrzTsStabLimitataSpaces();
        wgrzDtPresc.initWgrzDtPrescSpaces();
        wgrzRshInvst = Types.SPACE_CHAR;
        wgrzTpRamoBila = "";
    }

    public void setWgrzIdGar(int wgrzIdGar) {
        this.wgrzIdGar = wgrzIdGar;
    }

    public int getWgrzIdGar() {
        return this.wgrzIdGar;
    }

    public void setWgrzIdPoli(int wgrzIdPoli) {
        this.wgrzIdPoli = wgrzIdPoli;
    }

    public int getWgrzIdPoli() {
        return this.wgrzIdPoli;
    }

    public void setWgrzIdMoviCrz(int wgrzIdMoviCrz) {
        this.wgrzIdMoviCrz = wgrzIdMoviCrz;
    }

    public int getWgrzIdMoviCrz() {
        return this.wgrzIdMoviCrz;
    }

    public void setWgrzDtIniEff(int wgrzDtIniEff) {
        this.wgrzDtIniEff = wgrzDtIniEff;
    }

    public int getWgrzDtIniEff() {
        return this.wgrzDtIniEff;
    }

    public void setWgrzDtEndEff(int wgrzDtEndEff) {
        this.wgrzDtEndEff = wgrzDtEndEff;
    }

    public int getWgrzDtEndEff() {
        return this.wgrzDtEndEff;
    }

    public void setWgrzCodCompAnia(int wgrzCodCompAnia) {
        this.wgrzCodCompAnia = wgrzCodCompAnia;
    }

    public int getWgrzCodCompAnia() {
        return this.wgrzCodCompAnia;
    }

    public void setWgrzIbOgg(String wgrzIbOgg) {
        this.wgrzIbOgg = Functions.subString(wgrzIbOgg, Len.WGRZ_IB_OGG);
    }

    public String getWgrzIbOgg() {
        return this.wgrzIbOgg;
    }

    public void setWgrzCodSez(String wgrzCodSez) {
        this.wgrzCodSez = Functions.subString(wgrzCodSez, Len.WGRZ_COD_SEZ);
    }

    public String getWgrzCodSez() {
        return this.wgrzCodSez;
    }

    public void setWgrzCodTari(String wgrzCodTari) {
        this.wgrzCodTari = Functions.subString(wgrzCodTari, Len.WGRZ_COD_TARI);
    }

    public String getWgrzCodTari() {
        return this.wgrzCodTari;
    }

    public void setWgrzRamoBila(String wgrzRamoBila) {
        this.wgrzRamoBila = Functions.subString(wgrzRamoBila, Len.WGRZ_RAMO_BILA);
    }

    public String getWgrzRamoBila() {
        return this.wgrzRamoBila;
    }

    public String getWstrRamoBilaFormatted() {
        return Functions.padBlanks(getWgrzRamoBila(), Len.WGRZ_RAMO_BILA);
    }

    public void setWgrzTpRsh(String wgrzTpRsh) {
        this.wgrzTpRsh = Functions.subString(wgrzTpRsh, Len.WGRZ_TP_RSH);
    }

    public String getWgrzTpRsh() {
        return this.wgrzTpRsh;
    }

    public void setWgrzModPagGarcol(String wgrzModPagGarcol) {
        this.wgrzModPagGarcol = Functions.subString(wgrzModPagGarcol, Len.WGRZ_MOD_PAG_GARCOL);
    }

    public String getWgrzModPagGarcol() {
        return this.wgrzModPagGarcol;
    }

    public void setWgrzTpPerPre(String wgrzTpPerPre) {
        this.wgrzTpPerPre = Functions.subString(wgrzTpPerPre, Len.WGRZ_TP_PER_PRE);
    }

    public String getWgrzTpPerPre() {
        return this.wgrzTpPerPre;
    }

    public String getWgrzTpPerPreFormatted() {
        return Functions.padBlanks(getWgrzTpPerPre(), Len.WGRZ_TP_PER_PRE);
    }

    public void setWgrzTpEmisPur(char wgrzTpEmisPur) {
        this.wgrzTpEmisPur = wgrzTpEmisPur;
    }

    public char getWgrzTpEmisPur() {
        return this.wgrzTpEmisPur;
    }

    public void setWgrzTpCalcPrePrstz(String wgrzTpCalcPrePrstz) {
        this.wgrzTpCalcPrePrstz = Functions.subString(wgrzTpCalcPrePrstz, Len.WGRZ_TP_CALC_PRE_PRSTZ);
    }

    public String getWgrzTpCalcPrePrstz() {
        return this.wgrzTpCalcPrePrstz;
    }

    public void setWgrzTpPre(char wgrzTpPre) {
        this.wgrzTpPre = wgrzTpPre;
    }

    public char getWgrzTpPre() {
        return this.wgrzTpPre;
    }

    public void setWgrzTpDur(String wgrzTpDur) {
        this.wgrzTpDur = Functions.subString(wgrzTpDur, Len.WGRZ_TP_DUR);
    }

    public String getWgrzTpDur() {
        return this.wgrzTpDur;
    }

    public void setWgrzTpPrstzAssta(String wgrzTpPrstzAssta) {
        this.wgrzTpPrstzAssta = Functions.subString(wgrzTpPrstzAssta, Len.WGRZ_TP_PRSTZ_ASSTA);
    }

    public String getWgrzTpPrstzAssta() {
        return this.wgrzTpPrstzAssta;
    }

    public String getWgrzTpPrstzAsstaFormatted() {
        return Functions.padBlanks(getWgrzTpPrstzAssta(), Len.WGRZ_TP_PRSTZ_ASSTA);
    }

    public void setWgrzCodFnd(String wgrzCodFnd) {
        this.wgrzCodFnd = Functions.subString(wgrzCodFnd, Len.WGRZ_COD_FND);
    }

    public String getWgrzCodFnd() {
        return this.wgrzCodFnd;
    }

    public String getWgrzCodFndFormatted() {
        return Functions.padBlanks(getWgrzCodFnd(), Len.WGRZ_COD_FND);
    }

    public void setWgrzAaRenCer(String wgrzAaRenCer) {
        this.wgrzAaRenCer = Functions.subString(wgrzAaRenCer, Len.WGRZ_AA_REN_CER);
    }

    public String getWgrzAaRenCer() {
        return this.wgrzAaRenCer;
    }

    public void setWgrzTpPcRip(String wgrzTpPcRip) {
        this.wgrzTpPcRip = Functions.subString(wgrzTpPcRip, Len.WGRZ_TP_PC_RIP);
    }

    public String getWgrzTpPcRip() {
        return this.wgrzTpPcRip;
    }

    public void setWgrzTpIas(String wgrzTpIas) {
        this.wgrzTpIas = Functions.subString(wgrzTpIas, Len.WGRZ_TP_IAS);
    }

    public String getWgrzTpIas() {
        return this.wgrzTpIas;
    }

    public String getWgrzTpIasFormatted() {
        return Functions.padBlanks(getWgrzTpIas(), Len.WGRZ_TP_IAS);
    }

    public void setWgrzTpStab(String wgrzTpStab) {
        this.wgrzTpStab = Functions.subString(wgrzTpStab, Len.WGRZ_TP_STAB);
    }

    public String getWgrzTpStab() {
        return this.wgrzTpStab;
    }

    public void setWgrzTpAdegPre(char wgrzTpAdegPre) {
        this.wgrzTpAdegPre = wgrzTpAdegPre;
    }

    public char getWgrzTpAdegPre() {
        return this.wgrzTpAdegPre;
    }

    public void setWgrzCodTratRiass(String wgrzCodTratRiass) {
        this.wgrzCodTratRiass = Functions.subString(wgrzCodTratRiass, Len.WGRZ_COD_TRAT_RIASS);
    }

    public String getWgrzCodTratRiass() {
        return this.wgrzCodTratRiass;
    }

    public String getWgrzCodTratRiassFormatted() {
        return Functions.padBlanks(getWgrzCodTratRiass(), Len.WGRZ_COD_TRAT_RIASS);
    }

    public void setWgrzTpDtEmisRiass(String wgrzTpDtEmisRiass) {
        this.wgrzTpDtEmisRiass = Functions.subString(wgrzTpDtEmisRiass, Len.WGRZ_TP_DT_EMIS_RIASS);
    }

    public String getWgrzTpDtEmisRiass() {
        return this.wgrzTpDtEmisRiass;
    }

    public void setWgrzTpCessRiass(String wgrzTpCessRiass) {
        this.wgrzTpCessRiass = Functions.subString(wgrzTpCessRiass, Len.WGRZ_TP_CESS_RIASS);
    }

    public String getWgrzTpCessRiass() {
        return this.wgrzTpCessRiass;
    }

    public void setWgrzDsRiga(long wgrzDsRiga) {
        this.wgrzDsRiga = wgrzDsRiga;
    }

    public long getWgrzDsRiga() {
        return this.wgrzDsRiga;
    }

    public void setWgrzDsOperSql(char wgrzDsOperSql) {
        this.wgrzDsOperSql = wgrzDsOperSql;
    }

    public char getWgrzDsOperSql() {
        return this.wgrzDsOperSql;
    }

    public void setWgrzDsVer(int wgrzDsVer) {
        this.wgrzDsVer = wgrzDsVer;
    }

    public int getWgrzDsVer() {
        return this.wgrzDsVer;
    }

    public void setWgrzDsTsIniCptz(long wgrzDsTsIniCptz) {
        this.wgrzDsTsIniCptz = wgrzDsTsIniCptz;
    }

    public long getWgrzDsTsIniCptz() {
        return this.wgrzDsTsIniCptz;
    }

    public void setWgrzDsTsEndCptz(long wgrzDsTsEndCptz) {
        this.wgrzDsTsEndCptz = wgrzDsTsEndCptz;
    }

    public long getWgrzDsTsEndCptz() {
        return this.wgrzDsTsEndCptz;
    }

    public void setWgrzDsUtente(String wgrzDsUtente) {
        this.wgrzDsUtente = Functions.subString(wgrzDsUtente, Len.WGRZ_DS_UTENTE);
    }

    public String getWgrzDsUtente() {
        return this.wgrzDsUtente;
    }

    public void setWgrzDsStatoElab(char wgrzDsStatoElab) {
        this.wgrzDsStatoElab = wgrzDsStatoElab;
    }

    public char getWgrzDsStatoElab() {
        return this.wgrzDsStatoElab;
    }

    public void setWgrzRshInvst(char wgrzRshInvst) {
        this.wgrzRshInvst = wgrzRshInvst;
    }

    public char getWgrzRshInvst() {
        return this.wgrzRshInvst;
    }

    public void setWgrzTpRamoBila(String wgrzTpRamoBila) {
        this.wgrzTpRamoBila = Functions.subString(wgrzTpRamoBila, Len.WGRZ_TP_RAMO_BILA);
    }

    public String getWgrzTpRamoBila() {
        return this.wgrzTpRamoBila;
    }

    public WgrzAaPagPreUni getWgrzAaPagPreUni() {
        return wgrzAaPagPreUni;
    }

    public WgrzAaStab getWgrzAaStab() {
        return wgrzAaStab;
    }

    public WgrzDtDecor getWgrzDtDecor() {
        return wgrzDtDecor;
    }

    public WgrzDtEndCarz getWgrzDtEndCarz() {
        return wgrzDtEndCarz;
    }

    public WgrzDtIniValTar getWgrzDtIniValTar() {
        return wgrzDtIniValTar;
    }

    public WgrzDtPresc getWgrzDtPresc() {
        return wgrzDtPresc;
    }

    public WgrzDtScad getWgrzDtScad() {
        return wgrzDtScad;
    }

    public WgrzDtVarzTpIas getWgrzDtVarzTpIas() {
        return wgrzDtVarzTpIas;
    }

    public WgrzDurAa getWgrzDurAa() {
        return wgrzDurAa;
    }

    public WgrzDurGg getWgrzDurGg() {
        return wgrzDurGg;
    }

    public WgrzDurMm getWgrzDurMm() {
        return wgrzDurMm;
    }

    public WgrzEtaAScad getWgrzEtaAScad() {
        return wgrzEtaAScad;
    }

    public WgrzEtaAa1oAssto getWgrzEtaAa1oAssto() {
        return wgrzEtaAa1oAssto;
    }

    public WgrzEtaAa2oAssto getWgrzEtaAa2oAssto() {
        return wgrzEtaAa2oAssto;
    }

    public WgrzEtaAa3oAssto getWgrzEtaAa3oAssto() {
        return wgrzEtaAa3oAssto;
    }

    public WgrzEtaMm1oAssto getWgrzEtaMm1oAssto() {
        return wgrzEtaMm1oAssto;
    }

    public WgrzEtaMm2oAssto getWgrzEtaMm2oAssto() {
        return wgrzEtaMm2oAssto;
    }

    public WgrzEtaMm3oAssto getWgrzEtaMm3oAssto() {
        return wgrzEtaMm3oAssto;
    }

    public WgrzFrazDecrCpt getWgrzFrazDecrCpt() {
        return wgrzFrazDecrCpt;
    }

    public WgrzFrazIniErogRen getWgrzFrazIniErogRen() {
        return wgrzFrazIniErogRen;
    }

    public WgrzId1oAssto getWgrzId1oAssto() {
        return wgrzId1oAssto;
    }

    public WgrzId2oAssto getWgrzId2oAssto() {
        return wgrzId2oAssto;
    }

    public WgrzId3oAssto getWgrzId3oAssto() {
        return wgrzId3oAssto;
    }

    public WgrzIdAdes getWgrzIdAdes() {
        return wgrzIdAdes;
    }

    public WgrzIdMoviChiu getWgrzIdMoviChiu() {
        return wgrzIdMoviChiu;
    }

    public WgrzMm1oRat getWgrzMm1oRat() {
        return wgrzMm1oRat;
    }

    public WgrzMmPagPreUni getWgrzMmPagPreUni() {
        return wgrzMmPagPreUni;
    }

    public WgrzNumAaPagPre getWgrzNumAaPagPre() {
        return wgrzNumAaPagPre;
    }

    public WgrzPc1oRat getWgrzPc1oRat() {
        return wgrzPc1oRat;
    }

    public WgrzPcOpz getWgrzPcOpz() {
        return wgrzPcOpz;
    }

    public WgrzPcRevrsb getWgrzPcRevrsb() {
        return wgrzPcRevrsb;
    }

    public WgrzPcRipPre getWgrzPcRipPre() {
        return wgrzPcRipPre;
    }

    public WgrzTpGar getWgrzTpGar() {
        return wgrzTpGar;
    }

    public WgrzTpInvst getWgrzTpInvst() {
        return wgrzTpInvst;
    }

    public WgrzTsStabLimitata getWgrzTsStabLimitata() {
        return wgrzTsStabLimitata;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_ID_GAR = 5;
        public static final int WGRZ_ID_POLI = 5;
        public static final int WGRZ_ID_MOVI_CRZ = 5;
        public static final int WGRZ_DT_INI_EFF = 5;
        public static final int WGRZ_DT_END_EFF = 5;
        public static final int WGRZ_COD_COMP_ANIA = 3;
        public static final int WGRZ_IB_OGG = 40;
        public static final int WGRZ_COD_SEZ = 12;
        public static final int WGRZ_COD_TARI = 12;
        public static final int WGRZ_RAMO_BILA = 12;
        public static final int WGRZ_TP_RSH = 2;
        public static final int WGRZ_MOD_PAG_GARCOL = 2;
        public static final int WGRZ_TP_PER_PRE = 2;
        public static final int WGRZ_TP_EMIS_PUR = 1;
        public static final int WGRZ_TP_CALC_PRE_PRSTZ = 2;
        public static final int WGRZ_TP_PRE = 1;
        public static final int WGRZ_TP_DUR = 2;
        public static final int WGRZ_TP_PRSTZ_ASSTA = 2;
        public static final int WGRZ_COD_FND = 12;
        public static final int WGRZ_AA_REN_CER = 18;
        public static final int WGRZ_TP_PC_RIP = 2;
        public static final int WGRZ_TP_IAS = 2;
        public static final int WGRZ_TP_STAB = 2;
        public static final int WGRZ_TP_ADEG_PRE = 1;
        public static final int WGRZ_COD_TRAT_RIASS = 12;
        public static final int WGRZ_TP_DT_EMIS_RIASS = 2;
        public static final int WGRZ_TP_CESS_RIASS = 2;
        public static final int WGRZ_DS_RIGA = 6;
        public static final int WGRZ_DS_OPER_SQL = 1;
        public static final int WGRZ_DS_VER = 5;
        public static final int WGRZ_DS_TS_INI_CPTZ = 10;
        public static final int WGRZ_DS_TS_END_CPTZ = 10;
        public static final int WGRZ_DS_UTENTE = 20;
        public static final int WGRZ_DS_STATO_ELAB = 1;
        public static final int WGRZ_RSH_INVST = 1;
        public static final int WGRZ_TP_RAMO_BILA = 2;
        public static final int DATI = WGRZ_ID_GAR + WgrzIdAdes.Len.WGRZ_ID_ADES + WGRZ_ID_POLI + WGRZ_ID_MOVI_CRZ + WgrzIdMoviChiu.Len.WGRZ_ID_MOVI_CHIU + WGRZ_DT_INI_EFF + WGRZ_DT_END_EFF + WGRZ_COD_COMP_ANIA + WGRZ_IB_OGG + WgrzDtDecor.Len.WGRZ_DT_DECOR + WgrzDtScad.Len.WGRZ_DT_SCAD + WGRZ_COD_SEZ + WGRZ_COD_TARI + WGRZ_RAMO_BILA + WgrzDtIniValTar.Len.WGRZ_DT_INI_VAL_TAR + WgrzId1oAssto.Len.WGRZ_ID1O_ASSTO + WgrzId2oAssto.Len.WGRZ_ID2O_ASSTO + WgrzId3oAssto.Len.WGRZ_ID3O_ASSTO + WgrzTpGar.Len.WGRZ_TP_GAR + WGRZ_TP_RSH + WgrzTpInvst.Len.WGRZ_TP_INVST + WGRZ_MOD_PAG_GARCOL + WGRZ_TP_PER_PRE + WgrzEtaAa1oAssto.Len.WGRZ_ETA_AA1O_ASSTO + WgrzEtaMm1oAssto.Len.WGRZ_ETA_MM1O_ASSTO + WgrzEtaAa2oAssto.Len.WGRZ_ETA_AA2O_ASSTO + WgrzEtaMm2oAssto.Len.WGRZ_ETA_MM2O_ASSTO + WgrzEtaAa3oAssto.Len.WGRZ_ETA_AA3O_ASSTO + WgrzEtaMm3oAssto.Len.WGRZ_ETA_MM3O_ASSTO + WGRZ_TP_EMIS_PUR + WgrzEtaAScad.Len.WGRZ_ETA_A_SCAD + WGRZ_TP_CALC_PRE_PRSTZ + WGRZ_TP_PRE + WGRZ_TP_DUR + WgrzDurAa.Len.WGRZ_DUR_AA + WgrzDurMm.Len.WGRZ_DUR_MM + WgrzDurGg.Len.WGRZ_DUR_GG + WgrzNumAaPagPre.Len.WGRZ_NUM_AA_PAG_PRE + WgrzAaPagPreUni.Len.WGRZ_AA_PAG_PRE_UNI + WgrzMmPagPreUni.Len.WGRZ_MM_PAG_PRE_UNI + WgrzFrazIniErogRen.Len.WGRZ_FRAZ_INI_EROG_REN + WgrzMm1oRat.Len.WGRZ_MM1O_RAT + WgrzPc1oRat.Len.WGRZ_PC1O_RAT + WGRZ_TP_PRSTZ_ASSTA + WgrzDtEndCarz.Len.WGRZ_DT_END_CARZ + WgrzPcRipPre.Len.WGRZ_PC_RIP_PRE + WGRZ_COD_FND + WGRZ_AA_REN_CER + WgrzPcRevrsb.Len.WGRZ_PC_REVRSB + WGRZ_TP_PC_RIP + WgrzPcOpz.Len.WGRZ_PC_OPZ + WGRZ_TP_IAS + WGRZ_TP_STAB + WGRZ_TP_ADEG_PRE + WgrzDtVarzTpIas.Len.WGRZ_DT_VARZ_TP_IAS + WgrzFrazDecrCpt.Len.WGRZ_FRAZ_DECR_CPT + WGRZ_COD_TRAT_RIASS + WGRZ_TP_DT_EMIS_RIASS + WGRZ_TP_CESS_RIASS + WGRZ_DS_RIGA + WGRZ_DS_OPER_SQL + WGRZ_DS_VER + WGRZ_DS_TS_INI_CPTZ + WGRZ_DS_TS_END_CPTZ + WGRZ_DS_UTENTE + WGRZ_DS_STATO_ELAB + WgrzAaStab.Len.WGRZ_AA_STAB + WgrzTsStabLimitata.Len.WGRZ_TS_STAB_LIMITATA + WgrzDtPresc.Len.WGRZ_DT_PRESC + WGRZ_RSH_INVST + WGRZ_TP_RAMO_BILA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_ID_GAR = 9;
            public static final int WGRZ_ID_POLI = 9;
            public static final int WGRZ_ID_MOVI_CRZ = 9;
            public static final int WGRZ_DT_INI_EFF = 8;
            public static final int WGRZ_DT_END_EFF = 8;
            public static final int WGRZ_COD_COMP_ANIA = 5;
            public static final int WGRZ_DS_RIGA = 10;
            public static final int WGRZ_DS_VER = 9;
            public static final int WGRZ_DS_TS_INI_CPTZ = 18;
            public static final int WGRZ_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
