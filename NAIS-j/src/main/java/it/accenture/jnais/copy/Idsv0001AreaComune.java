package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Idsv0001FlagIdTmpryData;
import it.accenture.jnais.ws.enums.Idsv0001FormatoDataDb;
import it.accenture.jnais.ws.enums.Idsv0001ForzRc04;
import it.accenture.jnais.ws.enums.Idsv0001LettUltImmagine;
import it.accenture.jnais.ws.enums.Idsv0001LivelloDebug;
import it.accenture.jnais.ws.enums.Idsv0001ModalitaEsecutiva;
import it.accenture.jnais.ws.enums.Idsv0001TemporaryTable;
import it.accenture.jnais.ws.enums.Idsv0001TipoEnteChiamante;
import it.accenture.jnais.ws.enums.Idsv0001TrattamentoStoricita;
import it.accenture.jnais.ws.occurs.Idsv0001Addresses;

/**Original name: IDSV0001-AREA-COMUNE<br>
 * Variable: IDSV0001-AREA-COMUNE from copybook IDSV0001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv0001AreaComune {

    //==== PROPERTIES ====
    public static final int ADDRESSES_MAXOCCURS = 5;
    //Original name: IDSV0001-MODALITA-ESECUTIVA
    private Idsv0001ModalitaEsecutiva modalitaEsecutiva = new Idsv0001ModalitaEsecutiva();
    //Original name: IDSV0001-COD-COMPAGNIA-ANIA
    private String codCompagniaAnia = DefaultValues.stringVal(Len.COD_COMPAGNIA_ANIA);
    //Original name: IDSV0001-LINGUA
    private String lingua = DefaultValues.stringVal(Len.LINGUA);
    //Original name: IDSV0001-PAESE
    private String paese = DefaultValues.stringVal(Len.PAESE);
    //Original name: IDSV0001-USER-NAME
    private String userName = DefaultValues.stringVal(Len.USER_NAME);
    //Original name: IDSV0001-PROFILO-UTENTE
    private String profiloUtente = DefaultValues.stringVal(Len.PROFILO_UTENTE);
    //Original name: IDSV0001-KEY-BUSINESS1
    private String keyBusiness1 = DefaultValues.stringVal(Len.KEY_BUSINESS1);
    //Original name: IDSV0001-KEY-BUSINESS2
    private String keyBusiness2 = DefaultValues.stringVal(Len.KEY_BUSINESS2);
    //Original name: IDSV0001-KEY-BUSINESS3
    private String keyBusiness3 = DefaultValues.stringVal(Len.KEY_BUSINESS3);
    //Original name: IDSV0001-KEY-BUSINESS4
    private String keyBusiness4 = DefaultValues.stringVal(Len.KEY_BUSINESS4);
    //Original name: IDSV0001-KEY-BUSINESS5
    private String keyBusiness5 = DefaultValues.stringVal(Len.KEY_BUSINESS5);
    //Original name: IDSV0001-SESSIONE
    private String sessione = DefaultValues.stringVal(Len.SESSIONE);
    //Original name: IDSV0001-COD-MAIN-BATCH
    private String codMainBatch = DefaultValues.stringVal(Len.COD_MAIN_BATCH);
    //Original name: IDSV0001-TIPO-MOVIMENTO
    private String tipoMovimento = DefaultValues.stringVal(Len.TIPO_MOVIMENTO);
    //Original name: IDSV0001-DATA-EFFETTO
    private String dataEffetto = DefaultValues.stringVal(Len.DATA_EFFETTO);
    //Original name: IDSV0001-DATA-COMPETENZA
    private String dataCompetenza = DefaultValues.stringVal(Len.DATA_COMPETENZA);
    //Original name: IDSV0001-DATA-COMP-AGG-STOR
    private String dataCompAggStor = DefaultValues.stringVal(Len.DATA_COMP_AGG_STOR);
    //Original name: IDSV0001-TRATTAMENTO-STORICITA
    private Idsv0001TrattamentoStoricita trattamentoStoricita = new Idsv0001TrattamentoStoricita();
    //Original name: IDSV0001-FORMATO-DATA-DB
    private Idsv0001FormatoDataDb formatoDataDb = new Idsv0001FormatoDataDb();
    //Original name: IDSV0001-TIPO-ENTE-CHIAMANTE
    private Idsv0001TipoEnteChiamante tipoEnteChiamante = new Idsv0001TipoEnteChiamante();
    //Original name: IDSV0001-LIVELLO-DEBUG
    private Idsv0001LivelloDebug livelloDebug = new Idsv0001LivelloDebug();
    //Original name: IDSV0001-DT-PERV-RICH
    private String dtPervRich = DefaultValues.stringVal(Len.DT_PERV_RICH);
    //Original name: IDSV0001-LETT-ULT-IMMAGINE
    private Idsv0001LettUltImmagine lettUltImmagine = new Idsv0001LettUltImmagine();
    //Original name: IDSV0001-FLAG-ID-TMPRY-DATA
    private Idsv0001FlagIdTmpryData flagIdTmpryData = new Idsv0001FlagIdTmpryData();
    //Original name: IDSV0001-ID-TEMPORARY-DATA
    private long idTemporaryData = DefaultValues.LONG_VAL;
    //Original name: IDSV0001-TEMPORARY-TABLE
    private Idsv0001TemporaryTable temporaryTable = new Idsv0001TemporaryTable();
    //Original name: IDSV0001-ADDRESSES
    private Idsv0001Addresses[] addresses = new Idsv0001Addresses[ADDRESSES_MAXOCCURS];
    //Original name: IDSV0001-FORZ-RC-04
    private Idsv0001ForzRc04 forzRc04 = new Idsv0001ForzRc04();
    //Original name: FILLER-IDSV0001-AREA-COMUNE
    private String flr1 = DefaultValues.stringVal(Len.FLR1);

    //==== CONSTRUCTORS ====
    public Idsv0001AreaComune() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int addressesIdx = 1; addressesIdx <= ADDRESSES_MAXOCCURS; addressesIdx++) {
            addresses[addressesIdx - 1] = new Idsv0001Addresses();
        }
    }

    public void setAreaComuneBytes(byte[] buffer, int offset) {
        int position = offset;
        modalitaEsecutiva.setModalitaEsecutiva(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        codCompagniaAnia = MarshalByte.readFixedString(buffer, position, Len.COD_COMPAGNIA_ANIA);
        position += Len.COD_COMPAGNIA_ANIA;
        lingua = MarshalByte.readString(buffer, position, Len.LINGUA);
        position += Len.LINGUA;
        paese = MarshalByte.readString(buffer, position, Len.PAESE);
        position += Len.PAESE;
        userName = MarshalByte.readString(buffer, position, Len.USER_NAME);
        position += Len.USER_NAME;
        profiloUtente = MarshalByte.readString(buffer, position, Len.PROFILO_UTENTE);
        position += Len.PROFILO_UTENTE;
        keyBusiness1 = MarshalByte.readString(buffer, position, Len.KEY_BUSINESS1);
        position += Len.KEY_BUSINESS1;
        keyBusiness2 = MarshalByte.readString(buffer, position, Len.KEY_BUSINESS2);
        position += Len.KEY_BUSINESS2;
        keyBusiness3 = MarshalByte.readString(buffer, position, Len.KEY_BUSINESS3);
        position += Len.KEY_BUSINESS3;
        keyBusiness4 = MarshalByte.readString(buffer, position, Len.KEY_BUSINESS4);
        position += Len.KEY_BUSINESS4;
        keyBusiness5 = MarshalByte.readString(buffer, position, Len.KEY_BUSINESS5);
        position += Len.KEY_BUSINESS5;
        sessione = MarshalByte.readString(buffer, position, Len.SESSIONE);
        position += Len.SESSIONE;
        codMainBatch = MarshalByte.readString(buffer, position, Len.COD_MAIN_BATCH);
        position += Len.COD_MAIN_BATCH;
        tipoMovimento = MarshalByte.readFixedString(buffer, position, Len.TIPO_MOVIMENTO);
        position += Len.TIPO_MOVIMENTO;
        dataEffetto = MarshalByte.readFixedString(buffer, position, Len.DATA_EFFETTO);
        position += Len.DATA_EFFETTO;
        dataCompetenza = MarshalByte.readFixedString(buffer, position, Len.DATA_COMPETENZA);
        position += Len.DATA_COMPETENZA;
        dataCompAggStor = MarshalByte.readFixedString(buffer, position, Len.DATA_COMP_AGG_STOR);
        position += Len.DATA_COMP_AGG_STOR;
        trattamentoStoricita.setTrattamentoStoricita(MarshalByte.readString(buffer, position, Idsv0001TrattamentoStoricita.Len.TRATTAMENTO_STORICITA));
        position += Idsv0001TrattamentoStoricita.Len.TRATTAMENTO_STORICITA;
        formatoDataDb.setFormatoDataDb(MarshalByte.readString(buffer, position, Idsv0001FormatoDataDb.Len.FORMATO_DATA_DB));
        position += Idsv0001FormatoDataDb.Len.FORMATO_DATA_DB;
        tipoEnteChiamante.value = MarshalByte.readFixedString(buffer, position, Idsv0001TipoEnteChiamante.Len.TIPO_ENTE_CHIAMANTE);
        position += Idsv0001TipoEnteChiamante.Len.TIPO_ENTE_CHIAMANTE;
        livelloDebug.value = MarshalByte.readFixedString(buffer, position, Idsv0001LivelloDebug.Len.LIVELLO_DEBUG);
        position += Idsv0001LivelloDebug.Len.LIVELLO_DEBUG;
        dtPervRich = MarshalByte.readFixedString(buffer, position, Len.DT_PERV_RICH);
        position += Len.DT_PERV_RICH;
        lettUltImmagine.setLettUltImmagine(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        flagIdTmpryData.setFlagIdTmpryData(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        idTemporaryData = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ID_TEMPORARY_DATA, 0);
        position += Len.ID_TEMPORARY_DATA;
        temporaryTable.setTemporaryTable(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        setAreaAddressesBytes(buffer, position);
        position += Len.AREA_ADDRESSES;
        forzRc04.setForzRc04(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
    }

    public byte[] getAreaComuneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, modalitaEsecutiva.getModalitaEsecutiva());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
        position += Len.COD_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, lingua, Len.LINGUA);
        position += Len.LINGUA;
        MarshalByte.writeString(buffer, position, paese, Len.PAESE);
        position += Len.PAESE;
        MarshalByte.writeString(buffer, position, userName, Len.USER_NAME);
        position += Len.USER_NAME;
        MarshalByte.writeString(buffer, position, profiloUtente, Len.PROFILO_UTENTE);
        position += Len.PROFILO_UTENTE;
        MarshalByte.writeString(buffer, position, keyBusiness1, Len.KEY_BUSINESS1);
        position += Len.KEY_BUSINESS1;
        MarshalByte.writeString(buffer, position, keyBusiness2, Len.KEY_BUSINESS2);
        position += Len.KEY_BUSINESS2;
        MarshalByte.writeString(buffer, position, keyBusiness3, Len.KEY_BUSINESS3);
        position += Len.KEY_BUSINESS3;
        MarshalByte.writeString(buffer, position, keyBusiness4, Len.KEY_BUSINESS4);
        position += Len.KEY_BUSINESS4;
        MarshalByte.writeString(buffer, position, keyBusiness5, Len.KEY_BUSINESS5);
        position += Len.KEY_BUSINESS5;
        MarshalByte.writeString(buffer, position, sessione, Len.SESSIONE);
        position += Len.SESSIONE;
        MarshalByte.writeString(buffer, position, codMainBatch, Len.COD_MAIN_BATCH);
        position += Len.COD_MAIN_BATCH;
        MarshalByte.writeString(buffer, position, tipoMovimento, Len.TIPO_MOVIMENTO);
        position += Len.TIPO_MOVIMENTO;
        MarshalByte.writeString(buffer, position, dataEffetto, Len.DATA_EFFETTO);
        position += Len.DATA_EFFETTO;
        MarshalByte.writeString(buffer, position, dataCompetenza, Len.DATA_COMPETENZA);
        position += Len.DATA_COMPETENZA;
        MarshalByte.writeString(buffer, position, dataCompAggStor, Len.DATA_COMP_AGG_STOR);
        position += Len.DATA_COMP_AGG_STOR;
        MarshalByte.writeString(buffer, position, trattamentoStoricita.getTrattamentoStoricita(), Idsv0001TrattamentoStoricita.Len.TRATTAMENTO_STORICITA);
        position += Idsv0001TrattamentoStoricita.Len.TRATTAMENTO_STORICITA;
        MarshalByte.writeString(buffer, position, formatoDataDb.getFormatoDataDb(), Idsv0001FormatoDataDb.Len.FORMATO_DATA_DB);
        position += Idsv0001FormatoDataDb.Len.FORMATO_DATA_DB;
        MarshalByte.writeString(buffer, position, tipoEnteChiamante.value, Idsv0001TipoEnteChiamante.Len.TIPO_ENTE_CHIAMANTE);
        position += Idsv0001TipoEnteChiamante.Len.TIPO_ENTE_CHIAMANTE;
        MarshalByte.writeString(buffer, position, livelloDebug.value, Idsv0001LivelloDebug.Len.LIVELLO_DEBUG);
        position += Idsv0001LivelloDebug.Len.LIVELLO_DEBUG;
        MarshalByte.writeString(buffer, position, dtPervRich, Len.DT_PERV_RICH);
        position += Len.DT_PERV_RICH;
        MarshalByte.writeChar(buffer, position, lettUltImmagine.getLettUltImmagine());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flagIdTmpryData.getFlagIdTmpryData());
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, idTemporaryData, Len.Int.ID_TEMPORARY_DATA, 0);
        position += Len.ID_TEMPORARY_DATA;
        MarshalByte.writeChar(buffer, position, temporaryTable.getTemporaryTable());
        position += Types.CHAR_SIZE;
        getAreaAddressesBytes(buffer, position);
        position += Len.AREA_ADDRESSES;
        MarshalByte.writeChar(buffer, position, forzRc04.getForzRc04());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        return buffer;
    }

    public void setIdsv0001CodCompagniaAnia(int idsv0001CodCompagniaAnia) {
        this.codCompagniaAnia = NumericDisplay.asString(idsv0001CodCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
    }

    public void setIdsv0001CodCompagniaAniaFormatted(String idsv0001CodCompagniaAnia) {
        this.codCompagniaAnia = Trunc.toUnsignedNumeric(idsv0001CodCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
    }

    public int getIdsv0001CodCompagniaAnia() {
        return NumericDisplay.asInt(this.codCompagniaAnia);
    }

    public String getIdsv0001CodCompagniaAniaFormatted() {
        return this.codCompagniaAnia;
    }

    public String getIdsv0001CodCompagniaAniaAsString() {
        return getIdsv0001CodCompagniaAniaFormatted();
    }

    public void setLingua(String lingua) {
        this.lingua = Functions.subString(lingua, Len.LINGUA);
    }

    public String getLingua() {
        return this.lingua;
    }

    public String getIdsv0001LinguaFormatted() {
        return Functions.padBlanks(getLingua(), Len.LINGUA);
    }

    public void setPaese(String paese) {
        this.paese = Functions.subString(paese, Len.PAESE);
    }

    public String getPaese() {
        return this.paese;
    }

    public String getIdsv0001PaeseFormatted() {
        return Functions.padBlanks(getPaese(), Len.PAESE);
    }

    public void setUserName(String userName) {
        this.userName = Functions.subString(userName, Len.USER_NAME);
    }

    public String getUserName() {
        return this.userName;
    }

    public void setProfiloUtente(String profiloUtente) {
        this.profiloUtente = Functions.subString(profiloUtente, Len.PROFILO_UTENTE);
    }

    public String getProfiloUtente() {
        return this.profiloUtente;
    }

    public void setKeyBusiness1(String keyBusiness1) {
        this.keyBusiness1 = Functions.subString(keyBusiness1, Len.KEY_BUSINESS1);
    }

    public String getKeyBusiness1() {
        return this.keyBusiness1;
    }

    public String getIdsv0001KeyBusiness1Formatted() {
        return Functions.padBlanks(getKeyBusiness1(), Len.KEY_BUSINESS1);
    }

    public void setKeyBusiness2(String keyBusiness2) {
        this.keyBusiness2 = Functions.subString(keyBusiness2, Len.KEY_BUSINESS2);
    }

    public String getKeyBusiness2() {
        return this.keyBusiness2;
    }

    public String getIdsv0001KeyBusiness2Formatted() {
        return Functions.padBlanks(getKeyBusiness2(), Len.KEY_BUSINESS2);
    }

    public void setKeyBusiness3(String keyBusiness3) {
        this.keyBusiness3 = Functions.subString(keyBusiness3, Len.KEY_BUSINESS3);
    }

    public String getKeyBusiness3() {
        return this.keyBusiness3;
    }

    public String getIdsv0001KeyBusiness3Formatted() {
        return Functions.padBlanks(getKeyBusiness3(), Len.KEY_BUSINESS3);
    }

    public void setKeyBusiness4(String keyBusiness4) {
        this.keyBusiness4 = Functions.subString(keyBusiness4, Len.KEY_BUSINESS4);
    }

    public String getKeyBusiness4() {
        return this.keyBusiness4;
    }

    public String getIdsv0001KeyBusiness4Formatted() {
        return Functions.padBlanks(getKeyBusiness4(), Len.KEY_BUSINESS4);
    }

    public void setKeyBusiness5(String keyBusiness5) {
        this.keyBusiness5 = Functions.subString(keyBusiness5, Len.KEY_BUSINESS5);
    }

    public String getKeyBusiness5() {
        return this.keyBusiness5;
    }

    public String getIdsv0001KeyBusiness5Formatted() {
        return Functions.padBlanks(getKeyBusiness5(), Len.KEY_BUSINESS5);
    }

    public void setSessione(String sessione) {
        this.sessione = Functions.subString(sessione, Len.SESSIONE);
    }

    public String getSessione() {
        return this.sessione;
    }

    public String getSessioneFormatted() {
        return Functions.padBlanks(getSessione(), Len.SESSIONE);
    }

    public void setCodMainBatch(String codMainBatch) {
        this.codMainBatch = Functions.subString(codMainBatch, Len.COD_MAIN_BATCH);
    }

    public String getCodMainBatch() {
        return this.codMainBatch;
    }

    public String getCodMainBatchFormatted() {
        return Functions.padBlanks(getCodMainBatch(), Len.COD_MAIN_BATCH);
    }

    public void setIdsv0001TipoMovimento(int idsv0001TipoMovimento) {
        this.tipoMovimento = NumericDisplay.asString(idsv0001TipoMovimento, Len.TIPO_MOVIMENTO);
    }

    public void setIdsv0001TipoMovimentoFormatted(String idsv0001TipoMovimento) {
        this.tipoMovimento = Trunc.toUnsignedNumeric(idsv0001TipoMovimento, Len.TIPO_MOVIMENTO);
    }

    public int getIdsv0001TipoMovimento() {
        return NumericDisplay.asInt(this.tipoMovimento);
    }

    public String getIdsv0001TipoMovimentoFormatted() {
        return this.tipoMovimento;
    }

    public void setIdsv0001DataEffetto(int idsv0001DataEffetto) {
        this.dataEffetto = NumericDisplay.asString(idsv0001DataEffetto, Len.DATA_EFFETTO);
    }

    public void setIdsv0001DataEffettoFormatted(String idsv0001DataEffetto) {
        this.dataEffetto = Trunc.toUnsignedNumeric(idsv0001DataEffetto, Len.DATA_EFFETTO);
    }

    public int getIdsv0001DataEffetto() {
        return NumericDisplay.asInt(this.dataEffetto);
    }

    public String getIdsv0001DataEffettoFormatted() {
        return this.dataEffetto;
    }

    public void setIdsv0001DataCompetenzaFormatted(String idsv0001DataCompetenza) {
        this.dataCompetenza = Trunc.toUnsignedNumeric(idsv0001DataCompetenza, Len.DATA_COMPETENZA);
    }

    public long getIdsv0001DataCompetenza() {
        return NumericDisplay.asLong(this.dataCompetenza);
    }

    public String getIdsv0001DataCompetenzaFormatted() {
        return this.dataCompetenza;
    }

    public void setIdsv0001DataCompAggStorFormatted(String idsv0001DataCompAggStor) {
        this.dataCompAggStor = Trunc.toUnsignedNumeric(idsv0001DataCompAggStor, Len.DATA_COMP_AGG_STOR);
    }

    public long getIdsv0001DataCompAggStor() {
        return NumericDisplay.asLong(this.dataCompAggStor);
    }

    public void setIdTemporaryData(long idTemporaryData) {
        this.idTemporaryData = idTemporaryData;
    }

    public long getIdTemporaryData() {
        return this.idTemporaryData;
    }

    /**Original name: IDSV0001-AREA-ADDRESSES<br>*/
    public byte[] getIdsv0001AreaAddressesBytes() {
        byte[] buffer = new byte[Len.AREA_ADDRESSES];
        return getAreaAddressesBytes(buffer, 1);
    }

    public void setAreaAddressesBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ADDRESSES_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                addresses[idx - 1].setAddressesBytes(buffer, position);
                position += Idsv0001Addresses.Len.ADDRESSES;
            }
            else {
                addresses[idx - 1].initAddressesSpaces();
                position += Idsv0001Addresses.Len.ADDRESSES;
            }
        }
    }

    public byte[] getAreaAddressesBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ADDRESSES_MAXOCCURS; idx++) {
            addresses[idx - 1].getAddressesBytes(buffer, position);
            position += Idsv0001Addresses.Len.ADDRESSES;
        }
        return buffer;
    }

    public void setFlr1(String flr1) {
        this.flr1 = Functions.subString(flr1, Len.FLR1);
    }

    public String getFlr1() {
        return this.flr1;
    }

    public Idsv0001Addresses getAddresses(int idx) {
        return addresses[idx - 1];
    }

    public Idsv0001FlagIdTmpryData getFlagIdTmpryData() {
        return flagIdTmpryData;
    }

    public Idsv0001FormatoDataDb getFormatoDataDb() {
        return formatoDataDb;
    }

    public Idsv0001ForzRc04 getForzRc04() {
        return forzRc04;
    }

    public Idsv0001LettUltImmagine getLettUltImmagine() {
        return lettUltImmagine;
    }

    public Idsv0001LivelloDebug getLivelloDebug() {
        return livelloDebug;
    }

    public Idsv0001ModalitaEsecutiva getModalitaEsecutiva() {
        return modalitaEsecutiva;
    }

    public Idsv0001TemporaryTable getTemporaryTable() {
        return temporaryTable;
    }

    public Idsv0001TrattamentoStoricita getTrattamentoStoricita() {
        return trattamentoStoricita;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_COMPAGNIA_ANIA = 5;
        public static final int LINGUA = 2;
        public static final int PAESE = 3;
        public static final int USER_NAME = 20;
        public static final int PROFILO_UTENTE = 20;
        public static final int KEY_BUSINESS1 = 20;
        public static final int KEY_BUSINESS2 = 20;
        public static final int KEY_BUSINESS3 = 20;
        public static final int KEY_BUSINESS4 = 20;
        public static final int KEY_BUSINESS5 = 20;
        public static final int SESSIONE = 20;
        public static final int COD_MAIN_BATCH = 8;
        public static final int TIPO_MOVIMENTO = 5;
        public static final int DATA_EFFETTO = 8;
        public static final int DATA_COMPETENZA = 18;
        public static final int DATA_COMP_AGG_STOR = 18;
        public static final int DT_PERV_RICH = 8;
        public static final int ID_TEMPORARY_DATA = 6;
        public static final int AREA_ADDRESSES = Idsv0001AreaComune.ADDRESSES_MAXOCCURS * Idsv0001Addresses.Len.ADDRESSES;
        public static final int FLR1 = 921;
        public static final int AREA_COMUNE = Idsv0001ModalitaEsecutiva.Len.MODALITA_ESECUTIVA + COD_COMPAGNIA_ANIA + LINGUA + PAESE + USER_NAME + PROFILO_UTENTE + KEY_BUSINESS1 + KEY_BUSINESS2 + KEY_BUSINESS3 + KEY_BUSINESS4 + KEY_BUSINESS5 + SESSIONE + COD_MAIN_BATCH + TIPO_MOVIMENTO + DATA_EFFETTO + DATA_COMPETENZA + DATA_COMP_AGG_STOR + Idsv0001TrattamentoStoricita.Len.TRATTAMENTO_STORICITA + Idsv0001FormatoDataDb.Len.FORMATO_DATA_DB + Idsv0001TipoEnteChiamante.Len.TIPO_ENTE_CHIAMANTE + Idsv0001LivelloDebug.Len.LIVELLO_DEBUG + DT_PERV_RICH + Idsv0001LettUltImmagine.Len.LETT_ULT_IMMAGINE + Idsv0001FlagIdTmpryData.Len.FLAG_ID_TMPRY_DATA + ID_TEMPORARY_DATA + Idsv0001TemporaryTable.Len.TEMPORARY_TABLE + AREA_ADDRESSES + Idsv0001ForzRc04.Len.FORZ_RC04 + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_TEMPORARY_DATA = 10;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
