package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Wp67Amm1aRat;
import it.accenture.jnais.ws.redefines.Wp67AmmRatEnd;
import it.accenture.jnais.ws.redefines.Wp67CptFin;
import it.accenture.jnais.ws.redefines.Wp67Dt1oUtlzCRev;
import it.accenture.jnais.ws.redefines.Wp67DtEndFinanz;
import it.accenture.jnais.ws.redefines.Wp67DtErogFinanz;
import it.accenture.jnais.ws.redefines.Wp67DtEstFinanz;
import it.accenture.jnais.ws.redefines.Wp67DtManCop;
import it.accenture.jnais.ws.redefines.Wp67DtScad1aRat;
import it.accenture.jnais.ws.redefines.Wp67DtScadCop;
import it.accenture.jnais.ws.redefines.Wp67DtStipulaFinanz;
import it.accenture.jnais.ws.redefines.Wp67DurMmFinanz;
import it.accenture.jnais.ws.redefines.Wp67GgDelMmScadRat;
import it.accenture.jnais.ws.redefines.Wp67IdMoviChiu;
import it.accenture.jnais.ws.redefines.Wp67ImpAssto;
import it.accenture.jnais.ws.redefines.Wp67ImpCanoneAntic;
import it.accenture.jnais.ws.redefines.Wp67ImpDebRes;
import it.accenture.jnais.ws.redefines.Wp67ImpFinRevolving;
import it.accenture.jnais.ws.redefines.Wp67ImpRatFinanz;
import it.accenture.jnais.ws.redefines.Wp67ImpRatRevolving;
import it.accenture.jnais.ws.redefines.Wp67ImpUtilCRev;
import it.accenture.jnais.ws.redefines.Wp67MmPreamm;
import it.accenture.jnais.ws.redefines.Wp67NumTstFin;
import it.accenture.jnais.ws.redefines.Wp67PerRatFinanz;
import it.accenture.jnais.ws.redefines.Wp67PreVers;
import it.accenture.jnais.ws.redefines.Wp67TsCreRatFinanz;
import it.accenture.jnais.ws.redefines.Wp67TsFinanz;
import it.accenture.jnais.ws.redefines.Wp67ValRiscBene;
import it.accenture.jnais.ws.redefines.Wp67ValRiscEndLeas;

/**Original name: WP67-DATI<br>
 * Variable: WP67-DATI from copybook LCCVP671<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wp67Dati {

    //==== PROPERTIES ====
    //Original name: WP67-ID-EST-POLI-CPI-PR
    private int wp67IdEstPoliCpiPr = DefaultValues.INT_VAL;
    //Original name: WP67-ID-MOVI-CRZ
    private int wp67IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WP67-ID-MOVI-CHIU
    private Wp67IdMoviChiu wp67IdMoviChiu = new Wp67IdMoviChiu();
    //Original name: WP67-DT-INI-EFF
    private int wp67DtIniEff = DefaultValues.INT_VAL;
    //Original name: WP67-DT-END-EFF
    private int wp67DtEndEff = DefaultValues.INT_VAL;
    //Original name: WP67-COD-COMP-ANIA
    private int wp67CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WP67-IB-OGG
    private String wp67IbOgg = DefaultValues.stringVal(Len.WP67_IB_OGG);
    //Original name: WP67-DS-RIGA
    private long wp67DsRiga = DefaultValues.LONG_VAL;
    //Original name: WP67-DS-OPER-SQL
    private char wp67DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WP67-DS-VER
    private int wp67DsVer = DefaultValues.INT_VAL;
    //Original name: WP67-DS-TS-INI-CPTZ
    private long wp67DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WP67-DS-TS-END-CPTZ
    private long wp67DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WP67-DS-UTENTE
    private String wp67DsUtente = DefaultValues.stringVal(Len.WP67_DS_UTENTE);
    //Original name: WP67-DS-STATO-ELAB
    private char wp67DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WP67-COD-PROD-ESTNO
    private String wp67CodProdEstno = DefaultValues.stringVal(Len.WP67_COD_PROD_ESTNO);
    //Original name: WP67-CPT-FIN
    private Wp67CptFin wp67CptFin = new Wp67CptFin();
    //Original name: WP67-NUM-TST-FIN
    private Wp67NumTstFin wp67NumTstFin = new Wp67NumTstFin();
    //Original name: WP67-TS-FINANZ
    private Wp67TsFinanz wp67TsFinanz = new Wp67TsFinanz();
    //Original name: WP67-DUR-MM-FINANZ
    private Wp67DurMmFinanz wp67DurMmFinanz = new Wp67DurMmFinanz();
    //Original name: WP67-DT-END-FINANZ
    private Wp67DtEndFinanz wp67DtEndFinanz = new Wp67DtEndFinanz();
    //Original name: WP67-AMM-1A-RAT
    private Wp67Amm1aRat wp67Amm1aRat = new Wp67Amm1aRat();
    //Original name: WP67-VAL-RISC-BENE
    private Wp67ValRiscBene wp67ValRiscBene = new Wp67ValRiscBene();
    //Original name: WP67-AMM-RAT-END
    private Wp67AmmRatEnd wp67AmmRatEnd = new Wp67AmmRatEnd();
    //Original name: WP67-TS-CRE-RAT-FINANZ
    private Wp67TsCreRatFinanz wp67TsCreRatFinanz = new Wp67TsCreRatFinanz();
    //Original name: WP67-IMP-FIN-REVOLVING
    private Wp67ImpFinRevolving wp67ImpFinRevolving = new Wp67ImpFinRevolving();
    //Original name: WP67-IMP-UTIL-C-REV
    private Wp67ImpUtilCRev wp67ImpUtilCRev = new Wp67ImpUtilCRev();
    //Original name: WP67-IMP-RAT-REVOLVING
    private Wp67ImpRatRevolving wp67ImpRatRevolving = new Wp67ImpRatRevolving();
    //Original name: WP67-DT-1O-UTLZ-C-REV
    private Wp67Dt1oUtlzCRev wp67Dt1oUtlzCRev = new Wp67Dt1oUtlzCRev();
    //Original name: WP67-IMP-ASSTO
    private Wp67ImpAssto wp67ImpAssto = new Wp67ImpAssto();
    //Original name: WP67-PRE-VERS
    private Wp67PreVers wp67PreVers = new Wp67PreVers();
    //Original name: WP67-DT-SCAD-COP
    private Wp67DtScadCop wp67DtScadCop = new Wp67DtScadCop();
    //Original name: WP67-GG-DEL-MM-SCAD-RAT
    private Wp67GgDelMmScadRat wp67GgDelMmScadRat = new Wp67GgDelMmScadRat();
    //Original name: WP67-FL-PRE-FIN
    private char wp67FlPreFin = DefaultValues.CHAR_VAL;
    //Original name: WP67-DT-SCAD-1A-RAT
    private Wp67DtScad1aRat wp67DtScad1aRat = new Wp67DtScad1aRat();
    //Original name: WP67-DT-EROG-FINANZ
    private Wp67DtErogFinanz wp67DtErogFinanz = new Wp67DtErogFinanz();
    //Original name: WP67-DT-STIPULA-FINANZ
    private Wp67DtStipulaFinanz wp67DtStipulaFinanz = new Wp67DtStipulaFinanz();
    //Original name: WP67-MM-PREAMM
    private Wp67MmPreamm wp67MmPreamm = new Wp67MmPreamm();
    //Original name: WP67-IMP-DEB-RES
    private Wp67ImpDebRes wp67ImpDebRes = new Wp67ImpDebRes();
    //Original name: WP67-IMP-RAT-FINANZ
    private Wp67ImpRatFinanz wp67ImpRatFinanz = new Wp67ImpRatFinanz();
    //Original name: WP67-IMP-CANONE-ANTIC
    private Wp67ImpCanoneAntic wp67ImpCanoneAntic = new Wp67ImpCanoneAntic();
    //Original name: WP67-PER-RAT-FINANZ
    private Wp67PerRatFinanz wp67PerRatFinanz = new Wp67PerRatFinanz();
    //Original name: WP67-TP-MOD-PAG-RAT
    private String wp67TpModPagRat = DefaultValues.stringVal(Len.WP67_TP_MOD_PAG_RAT);
    //Original name: WP67-TP-FINANZ-ER
    private String wp67TpFinanzEr = DefaultValues.stringVal(Len.WP67_TP_FINANZ_ER);
    //Original name: WP67-CAT-FINANZ-ER
    private String wp67CatFinanzEr = DefaultValues.stringVal(Len.WP67_CAT_FINANZ_ER);
    //Original name: WP67-VAL-RISC-END-LEAS
    private Wp67ValRiscEndLeas wp67ValRiscEndLeas = new Wp67ValRiscEndLeas();
    //Original name: WP67-DT-EST-FINANZ
    private Wp67DtEstFinanz wp67DtEstFinanz = new Wp67DtEstFinanz();
    //Original name: WP67-DT-MAN-COP
    private Wp67DtManCop wp67DtManCop = new Wp67DtManCop();
    //Original name: WP67-NUM-FINANZ
    private String wp67NumFinanz = DefaultValues.stringVal(Len.WP67_NUM_FINANZ);
    //Original name: WP67-TP-MOD-ACQS
    private String wp67TpModAcqs = DefaultValues.stringVal(Len.WP67_TP_MOD_ACQS);

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wp67IdEstPoliCpiPr = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP67_ID_EST_POLI_CPI_PR, 0);
        position += Len.WP67_ID_EST_POLI_CPI_PR;
        wp67IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP67_ID_MOVI_CRZ, 0);
        position += Len.WP67_ID_MOVI_CRZ;
        wp67IdMoviChiu.setWp67IdMoviChiuFromBuffer(buffer, position);
        position += Wp67IdMoviChiu.Len.WP67_ID_MOVI_CHIU;
        wp67DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP67_DT_INI_EFF, 0);
        position += Len.WP67_DT_INI_EFF;
        wp67DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP67_DT_END_EFF, 0);
        position += Len.WP67_DT_END_EFF;
        wp67CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP67_COD_COMP_ANIA, 0);
        position += Len.WP67_COD_COMP_ANIA;
        wp67IbOgg = MarshalByte.readString(buffer, position, Len.WP67_IB_OGG);
        position += Len.WP67_IB_OGG;
        wp67DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WP67_DS_RIGA, 0);
        position += Len.WP67_DS_RIGA;
        wp67DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wp67DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP67_DS_VER, 0);
        position += Len.WP67_DS_VER;
        wp67DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WP67_DS_TS_INI_CPTZ, 0);
        position += Len.WP67_DS_TS_INI_CPTZ;
        wp67DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WP67_DS_TS_END_CPTZ, 0);
        position += Len.WP67_DS_TS_END_CPTZ;
        wp67DsUtente = MarshalByte.readString(buffer, position, Len.WP67_DS_UTENTE);
        position += Len.WP67_DS_UTENTE;
        wp67DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wp67CodProdEstno = MarshalByte.readString(buffer, position, Len.WP67_COD_PROD_ESTNO);
        position += Len.WP67_COD_PROD_ESTNO;
        wp67CptFin.setWp67CptFinFromBuffer(buffer, position);
        position += Wp67CptFin.Len.WP67_CPT_FIN;
        wp67NumTstFin.setWp67NumTstFinFromBuffer(buffer, position);
        position += Wp67NumTstFin.Len.WP67_NUM_TST_FIN;
        wp67TsFinanz.setWp67TsFinanzFromBuffer(buffer, position);
        position += Wp67TsFinanz.Len.WP67_TS_FINANZ;
        wp67DurMmFinanz.setWp67DurMmFinanzFromBuffer(buffer, position);
        position += Wp67DurMmFinanz.Len.WP67_DUR_MM_FINANZ;
        wp67DtEndFinanz.setWp67DtEndFinanzFromBuffer(buffer, position);
        position += Wp67DtEndFinanz.Len.WP67_DT_END_FINANZ;
        wp67Amm1aRat.setWp67Amm1aRatFromBuffer(buffer, position);
        position += Wp67Amm1aRat.Len.WP67_AMM1A_RAT;
        wp67ValRiscBene.setWp67ValRiscBeneFromBuffer(buffer, position);
        position += Wp67ValRiscBene.Len.WP67_VAL_RISC_BENE;
        wp67AmmRatEnd.setWp67AmmRatEndFromBuffer(buffer, position);
        position += Wp67AmmRatEnd.Len.WP67_AMM_RAT_END;
        wp67TsCreRatFinanz.setWp67TsCreRatFinanzFromBuffer(buffer, position);
        position += Wp67TsCreRatFinanz.Len.WP67_TS_CRE_RAT_FINANZ;
        wp67ImpFinRevolving.setWp67ImpFinRevolvingFromBuffer(buffer, position);
        position += Wp67ImpFinRevolving.Len.WP67_IMP_FIN_REVOLVING;
        wp67ImpUtilCRev.setWp67ImpUtilCRevFromBuffer(buffer, position);
        position += Wp67ImpUtilCRev.Len.WP67_IMP_UTIL_C_REV;
        wp67ImpRatRevolving.setWp67ImpRatRevolvingFromBuffer(buffer, position);
        position += Wp67ImpRatRevolving.Len.WP67_IMP_RAT_REVOLVING;
        wp67Dt1oUtlzCRev.setWp67Dt1oUtlzCRevFromBuffer(buffer, position);
        position += Wp67Dt1oUtlzCRev.Len.WP67_DT1O_UTLZ_C_REV;
        wp67ImpAssto.setWp67ImpAsstoFromBuffer(buffer, position);
        position += Wp67ImpAssto.Len.WP67_IMP_ASSTO;
        wp67PreVers.setWp67PreVersFromBuffer(buffer, position);
        position += Wp67PreVers.Len.WP67_PRE_VERS;
        wp67DtScadCop.setWp67DtScadCopFromBuffer(buffer, position);
        position += Wp67DtScadCop.Len.WP67_DT_SCAD_COP;
        wp67GgDelMmScadRat.setWp67GgDelMmScadRatFromBuffer(buffer, position);
        position += Wp67GgDelMmScadRat.Len.WP67_GG_DEL_MM_SCAD_RAT;
        wp67FlPreFin = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wp67DtScad1aRat.setWp67DtScad1aRatFromBuffer(buffer, position);
        position += Wp67DtScad1aRat.Len.WP67_DT_SCAD1A_RAT;
        wp67DtErogFinanz.setWp67DtErogFinanzFromBuffer(buffer, position);
        position += Wp67DtErogFinanz.Len.WP67_DT_EROG_FINANZ;
        wp67DtStipulaFinanz.setWp67DtStipulaFinanzFromBuffer(buffer, position);
        position += Wp67DtStipulaFinanz.Len.WP67_DT_STIPULA_FINANZ;
        wp67MmPreamm.setWp67MmPreammFromBuffer(buffer, position);
        position += Wp67MmPreamm.Len.WP67_MM_PREAMM;
        wp67ImpDebRes.setWp67ImpDebResFromBuffer(buffer, position);
        position += Wp67ImpDebRes.Len.WP67_IMP_DEB_RES;
        wp67ImpRatFinanz.setWp67ImpRatFinanzFromBuffer(buffer, position);
        position += Wp67ImpRatFinanz.Len.WP67_IMP_RAT_FINANZ;
        wp67ImpCanoneAntic.setWp67ImpCanoneAnticFromBuffer(buffer, position);
        position += Wp67ImpCanoneAntic.Len.WP67_IMP_CANONE_ANTIC;
        wp67PerRatFinanz.setWp67PerRatFinanzFromBuffer(buffer, position);
        position += Wp67PerRatFinanz.Len.WP67_PER_RAT_FINANZ;
        wp67TpModPagRat = MarshalByte.readString(buffer, position, Len.WP67_TP_MOD_PAG_RAT);
        position += Len.WP67_TP_MOD_PAG_RAT;
        wp67TpFinanzEr = MarshalByte.readString(buffer, position, Len.WP67_TP_FINANZ_ER);
        position += Len.WP67_TP_FINANZ_ER;
        wp67CatFinanzEr = MarshalByte.readString(buffer, position, Len.WP67_CAT_FINANZ_ER);
        position += Len.WP67_CAT_FINANZ_ER;
        wp67ValRiscEndLeas.setWp67ValRiscEndLeasFromBuffer(buffer, position);
        position += Wp67ValRiscEndLeas.Len.WP67_VAL_RISC_END_LEAS;
        wp67DtEstFinanz.setWp67DtEstFinanzFromBuffer(buffer, position);
        position += Wp67DtEstFinanz.Len.WP67_DT_EST_FINANZ;
        wp67DtManCop.setWp67DtManCopFromBuffer(buffer, position);
        position += Wp67DtManCop.Len.WP67_DT_MAN_COP;
        wp67NumFinanz = MarshalByte.readString(buffer, position, Len.WP67_NUM_FINANZ);
        position += Len.WP67_NUM_FINANZ;
        wp67TpModAcqs = MarshalByte.readString(buffer, position, Len.WP67_TP_MOD_ACQS);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wp67IdEstPoliCpiPr, Len.Int.WP67_ID_EST_POLI_CPI_PR, 0);
        position += Len.WP67_ID_EST_POLI_CPI_PR;
        MarshalByte.writeIntAsPacked(buffer, position, wp67IdMoviCrz, Len.Int.WP67_ID_MOVI_CRZ, 0);
        position += Len.WP67_ID_MOVI_CRZ;
        wp67IdMoviChiu.getWp67IdMoviChiuAsBuffer(buffer, position);
        position += Wp67IdMoviChiu.Len.WP67_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wp67DtIniEff, Len.Int.WP67_DT_INI_EFF, 0);
        position += Len.WP67_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wp67DtEndEff, Len.Int.WP67_DT_END_EFF, 0);
        position += Len.WP67_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wp67CodCompAnia, Len.Int.WP67_COD_COMP_ANIA, 0);
        position += Len.WP67_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wp67IbOgg, Len.WP67_IB_OGG);
        position += Len.WP67_IB_OGG;
        MarshalByte.writeLongAsPacked(buffer, position, wp67DsRiga, Len.Int.WP67_DS_RIGA, 0);
        position += Len.WP67_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wp67DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wp67DsVer, Len.Int.WP67_DS_VER, 0);
        position += Len.WP67_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wp67DsTsIniCptz, Len.Int.WP67_DS_TS_INI_CPTZ, 0);
        position += Len.WP67_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wp67DsTsEndCptz, Len.Int.WP67_DS_TS_END_CPTZ, 0);
        position += Len.WP67_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wp67DsUtente, Len.WP67_DS_UTENTE);
        position += Len.WP67_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wp67DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wp67CodProdEstno, Len.WP67_COD_PROD_ESTNO);
        position += Len.WP67_COD_PROD_ESTNO;
        wp67CptFin.getWp67CptFinAsBuffer(buffer, position);
        position += Wp67CptFin.Len.WP67_CPT_FIN;
        wp67NumTstFin.getWp67NumTstFinAsBuffer(buffer, position);
        position += Wp67NumTstFin.Len.WP67_NUM_TST_FIN;
        wp67TsFinanz.getWp67TsFinanzAsBuffer(buffer, position);
        position += Wp67TsFinanz.Len.WP67_TS_FINANZ;
        wp67DurMmFinanz.getWp67DurMmFinanzAsBuffer(buffer, position);
        position += Wp67DurMmFinanz.Len.WP67_DUR_MM_FINANZ;
        wp67DtEndFinanz.getWp67DtEndFinanzAsBuffer(buffer, position);
        position += Wp67DtEndFinanz.Len.WP67_DT_END_FINANZ;
        wp67Amm1aRat.getWp67Amm1aRatAsBuffer(buffer, position);
        position += Wp67Amm1aRat.Len.WP67_AMM1A_RAT;
        wp67ValRiscBene.getWp67ValRiscBeneAsBuffer(buffer, position);
        position += Wp67ValRiscBene.Len.WP67_VAL_RISC_BENE;
        wp67AmmRatEnd.getWp67AmmRatEndAsBuffer(buffer, position);
        position += Wp67AmmRatEnd.Len.WP67_AMM_RAT_END;
        wp67TsCreRatFinanz.getWp67TsCreRatFinanzAsBuffer(buffer, position);
        position += Wp67TsCreRatFinanz.Len.WP67_TS_CRE_RAT_FINANZ;
        wp67ImpFinRevolving.getWp67ImpFinRevolvingAsBuffer(buffer, position);
        position += Wp67ImpFinRevolving.Len.WP67_IMP_FIN_REVOLVING;
        wp67ImpUtilCRev.getWp67ImpUtilCRevAsBuffer(buffer, position);
        position += Wp67ImpUtilCRev.Len.WP67_IMP_UTIL_C_REV;
        wp67ImpRatRevolving.getWp67ImpRatRevolvingAsBuffer(buffer, position);
        position += Wp67ImpRatRevolving.Len.WP67_IMP_RAT_REVOLVING;
        wp67Dt1oUtlzCRev.getWp67Dt1oUtlzCRevAsBuffer(buffer, position);
        position += Wp67Dt1oUtlzCRev.Len.WP67_DT1O_UTLZ_C_REV;
        wp67ImpAssto.getWp67ImpAsstoAsBuffer(buffer, position);
        position += Wp67ImpAssto.Len.WP67_IMP_ASSTO;
        wp67PreVers.getWp67PreVersAsBuffer(buffer, position);
        position += Wp67PreVers.Len.WP67_PRE_VERS;
        wp67DtScadCop.getWp67DtScadCopAsBuffer(buffer, position);
        position += Wp67DtScadCop.Len.WP67_DT_SCAD_COP;
        wp67GgDelMmScadRat.getWp67GgDelMmScadRatAsBuffer(buffer, position);
        position += Wp67GgDelMmScadRat.Len.WP67_GG_DEL_MM_SCAD_RAT;
        MarshalByte.writeChar(buffer, position, wp67FlPreFin);
        position += Types.CHAR_SIZE;
        wp67DtScad1aRat.getWp67DtScad1aRatAsBuffer(buffer, position);
        position += Wp67DtScad1aRat.Len.WP67_DT_SCAD1A_RAT;
        wp67DtErogFinanz.getWp67DtErogFinanzAsBuffer(buffer, position);
        position += Wp67DtErogFinanz.Len.WP67_DT_EROG_FINANZ;
        wp67DtStipulaFinanz.getWp67DtStipulaFinanzAsBuffer(buffer, position);
        position += Wp67DtStipulaFinanz.Len.WP67_DT_STIPULA_FINANZ;
        wp67MmPreamm.getWp67MmPreammAsBuffer(buffer, position);
        position += Wp67MmPreamm.Len.WP67_MM_PREAMM;
        wp67ImpDebRes.getWp67ImpDebResAsBuffer(buffer, position);
        position += Wp67ImpDebRes.Len.WP67_IMP_DEB_RES;
        wp67ImpRatFinanz.getWp67ImpRatFinanzAsBuffer(buffer, position);
        position += Wp67ImpRatFinanz.Len.WP67_IMP_RAT_FINANZ;
        wp67ImpCanoneAntic.getWp67ImpCanoneAnticAsBuffer(buffer, position);
        position += Wp67ImpCanoneAntic.Len.WP67_IMP_CANONE_ANTIC;
        wp67PerRatFinanz.getWp67PerRatFinanzAsBuffer(buffer, position);
        position += Wp67PerRatFinanz.Len.WP67_PER_RAT_FINANZ;
        MarshalByte.writeString(buffer, position, wp67TpModPagRat, Len.WP67_TP_MOD_PAG_RAT);
        position += Len.WP67_TP_MOD_PAG_RAT;
        MarshalByte.writeString(buffer, position, wp67TpFinanzEr, Len.WP67_TP_FINANZ_ER);
        position += Len.WP67_TP_FINANZ_ER;
        MarshalByte.writeString(buffer, position, wp67CatFinanzEr, Len.WP67_CAT_FINANZ_ER);
        position += Len.WP67_CAT_FINANZ_ER;
        wp67ValRiscEndLeas.getWp67ValRiscEndLeasAsBuffer(buffer, position);
        position += Wp67ValRiscEndLeas.Len.WP67_VAL_RISC_END_LEAS;
        wp67DtEstFinanz.getWp67DtEstFinanzAsBuffer(buffer, position);
        position += Wp67DtEstFinanz.Len.WP67_DT_EST_FINANZ;
        wp67DtManCop.getWp67DtManCopAsBuffer(buffer, position);
        position += Wp67DtManCop.Len.WP67_DT_MAN_COP;
        MarshalByte.writeString(buffer, position, wp67NumFinanz, Len.WP67_NUM_FINANZ);
        position += Len.WP67_NUM_FINANZ;
        MarshalByte.writeString(buffer, position, wp67TpModAcqs, Len.WP67_TP_MOD_ACQS);
        return buffer;
    }

    public void setWp67IdEstPoliCpiPr(int wp67IdEstPoliCpiPr) {
        this.wp67IdEstPoliCpiPr = wp67IdEstPoliCpiPr;
    }

    public int getWp67IdEstPoliCpiPr() {
        return this.wp67IdEstPoliCpiPr;
    }

    public void setWp67IdMoviCrz(int wp67IdMoviCrz) {
        this.wp67IdMoviCrz = wp67IdMoviCrz;
    }

    public int getWp67IdMoviCrz() {
        return this.wp67IdMoviCrz;
    }

    public void setWp67DtIniEff(int wp67DtIniEff) {
        this.wp67DtIniEff = wp67DtIniEff;
    }

    public int getWp67DtIniEff() {
        return this.wp67DtIniEff;
    }

    public void setWp67DtEndEff(int wp67DtEndEff) {
        this.wp67DtEndEff = wp67DtEndEff;
    }

    public int getWp67DtEndEff() {
        return this.wp67DtEndEff;
    }

    public void setWp67CodCompAnia(int wp67CodCompAnia) {
        this.wp67CodCompAnia = wp67CodCompAnia;
    }

    public int getWp67CodCompAnia() {
        return this.wp67CodCompAnia;
    }

    public void setWp67IbOgg(String wp67IbOgg) {
        this.wp67IbOgg = Functions.subString(wp67IbOgg, Len.WP67_IB_OGG);
    }

    public String getWp67IbOgg() {
        return this.wp67IbOgg;
    }

    public void setWp67DsRiga(long wp67DsRiga) {
        this.wp67DsRiga = wp67DsRiga;
    }

    public long getWp67DsRiga() {
        return this.wp67DsRiga;
    }

    public void setWp67DsOperSql(char wp67DsOperSql) {
        this.wp67DsOperSql = wp67DsOperSql;
    }

    public char getWp67DsOperSql() {
        return this.wp67DsOperSql;
    }

    public void setWp67DsVer(int wp67DsVer) {
        this.wp67DsVer = wp67DsVer;
    }

    public int getWp67DsVer() {
        return this.wp67DsVer;
    }

    public void setWp67DsTsIniCptz(long wp67DsTsIniCptz) {
        this.wp67DsTsIniCptz = wp67DsTsIniCptz;
    }

    public long getWp67DsTsIniCptz() {
        return this.wp67DsTsIniCptz;
    }

    public void setWp67DsTsEndCptz(long wp67DsTsEndCptz) {
        this.wp67DsTsEndCptz = wp67DsTsEndCptz;
    }

    public long getWp67DsTsEndCptz() {
        return this.wp67DsTsEndCptz;
    }

    public void setWp67DsUtente(String wp67DsUtente) {
        this.wp67DsUtente = Functions.subString(wp67DsUtente, Len.WP67_DS_UTENTE);
    }

    public String getWp67DsUtente() {
        return this.wp67DsUtente;
    }

    public void setWp67DsStatoElab(char wp67DsStatoElab) {
        this.wp67DsStatoElab = wp67DsStatoElab;
    }

    public char getWp67DsStatoElab() {
        return this.wp67DsStatoElab;
    }

    public void setWp67CodProdEstno(String wp67CodProdEstno) {
        this.wp67CodProdEstno = Functions.subString(wp67CodProdEstno, Len.WP67_COD_PROD_ESTNO);
    }

    public String getWp67CodProdEstno() {
        return this.wp67CodProdEstno;
    }

    public void setWp67FlPreFin(char wp67FlPreFin) {
        this.wp67FlPreFin = wp67FlPreFin;
    }

    public char getWp67FlPreFin() {
        return this.wp67FlPreFin;
    }

    public void setWp67TpModPagRat(String wp67TpModPagRat) {
        this.wp67TpModPagRat = Functions.subString(wp67TpModPagRat, Len.WP67_TP_MOD_PAG_RAT);
    }

    public String getWp67TpModPagRat() {
        return this.wp67TpModPagRat;
    }

    public void setWp67TpFinanzEr(String wp67TpFinanzEr) {
        this.wp67TpFinanzEr = Functions.subString(wp67TpFinanzEr, Len.WP67_TP_FINANZ_ER);
    }

    public String getWp67TpFinanzEr() {
        return this.wp67TpFinanzEr;
    }

    public void setWp67CatFinanzEr(String wp67CatFinanzEr) {
        this.wp67CatFinanzEr = Functions.subString(wp67CatFinanzEr, Len.WP67_CAT_FINANZ_ER);
    }

    public String getWp67CatFinanzEr() {
        return this.wp67CatFinanzEr;
    }

    public void setWp67NumFinanz(String wp67NumFinanz) {
        this.wp67NumFinanz = Functions.subString(wp67NumFinanz, Len.WP67_NUM_FINANZ);
    }

    public String getWp67NumFinanz() {
        return this.wp67NumFinanz;
    }

    public void setWp67TpModAcqs(String wp67TpModAcqs) {
        this.wp67TpModAcqs = Functions.subString(wp67TpModAcqs, Len.WP67_TP_MOD_ACQS);
    }

    public String getWp67TpModAcqs() {
        return this.wp67TpModAcqs;
    }

    public Wp67Amm1aRat getWp67Amm1aRat() {
        return wp67Amm1aRat;
    }

    public Wp67AmmRatEnd getWp67AmmRatEnd() {
        return wp67AmmRatEnd;
    }

    public Wp67CptFin getWp67CptFin() {
        return wp67CptFin;
    }

    public Wp67Dt1oUtlzCRev getWp67Dt1oUtlzCRev() {
        return wp67Dt1oUtlzCRev;
    }

    public Wp67DtEndFinanz getWp67DtEndFinanz() {
        return wp67DtEndFinanz;
    }

    public Wp67DtErogFinanz getWp67DtErogFinanz() {
        return wp67DtErogFinanz;
    }

    public Wp67DtEstFinanz getWp67DtEstFinanz() {
        return wp67DtEstFinanz;
    }

    public Wp67DtManCop getWp67DtManCop() {
        return wp67DtManCop;
    }

    public Wp67DtScad1aRat getWp67DtScad1aRat() {
        return wp67DtScad1aRat;
    }

    public Wp67DtScadCop getWp67DtScadCop() {
        return wp67DtScadCop;
    }

    public Wp67DtStipulaFinanz getWp67DtStipulaFinanz() {
        return wp67DtStipulaFinanz;
    }

    public Wp67DurMmFinanz getWp67DurMmFinanz() {
        return wp67DurMmFinanz;
    }

    public Wp67GgDelMmScadRat getWp67GgDelMmScadRat() {
        return wp67GgDelMmScadRat;
    }

    public Wp67IdMoviChiu getWp67IdMoviChiu() {
        return wp67IdMoviChiu;
    }

    public Wp67ImpAssto getWp67ImpAssto() {
        return wp67ImpAssto;
    }

    public Wp67ImpCanoneAntic getWp67ImpCanoneAntic() {
        return wp67ImpCanoneAntic;
    }

    public Wp67ImpDebRes getWp67ImpDebRes() {
        return wp67ImpDebRes;
    }

    public Wp67ImpFinRevolving getWp67ImpFinRevolving() {
        return wp67ImpFinRevolving;
    }

    public Wp67ImpRatFinanz getWp67ImpRatFinanz() {
        return wp67ImpRatFinanz;
    }

    public Wp67ImpRatRevolving getWp67ImpRatRevolving() {
        return wp67ImpRatRevolving;
    }

    public Wp67ImpUtilCRev getWp67ImpUtilCRev() {
        return wp67ImpUtilCRev;
    }

    public Wp67MmPreamm getWp67MmPreamm() {
        return wp67MmPreamm;
    }

    public Wp67NumTstFin getWp67NumTstFin() {
        return wp67NumTstFin;
    }

    public Wp67PerRatFinanz getWp67PerRatFinanz() {
        return wp67PerRatFinanz;
    }

    public Wp67PreVers getWp67PreVers() {
        return wp67PreVers;
    }

    public Wp67TsCreRatFinanz getWp67TsCreRatFinanz() {
        return wp67TsCreRatFinanz;
    }

    public Wp67TsFinanz getWp67TsFinanz() {
        return wp67TsFinanz;
    }

    public Wp67ValRiscBene getWp67ValRiscBene() {
        return wp67ValRiscBene;
    }

    public Wp67ValRiscEndLeas getWp67ValRiscEndLeas() {
        return wp67ValRiscEndLeas;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_ID_EST_POLI_CPI_PR = 5;
        public static final int WP67_ID_MOVI_CRZ = 5;
        public static final int WP67_DT_INI_EFF = 5;
        public static final int WP67_DT_END_EFF = 5;
        public static final int WP67_COD_COMP_ANIA = 3;
        public static final int WP67_IB_OGG = 40;
        public static final int WP67_DS_RIGA = 6;
        public static final int WP67_DS_OPER_SQL = 1;
        public static final int WP67_DS_VER = 5;
        public static final int WP67_DS_TS_INI_CPTZ = 10;
        public static final int WP67_DS_TS_END_CPTZ = 10;
        public static final int WP67_DS_UTENTE = 20;
        public static final int WP67_DS_STATO_ELAB = 1;
        public static final int WP67_COD_PROD_ESTNO = 20;
        public static final int WP67_FL_PRE_FIN = 1;
        public static final int WP67_TP_MOD_PAG_RAT = 2;
        public static final int WP67_TP_FINANZ_ER = 2;
        public static final int WP67_CAT_FINANZ_ER = 20;
        public static final int WP67_NUM_FINANZ = 40;
        public static final int WP67_TP_MOD_ACQS = 4;
        public static final int DATI = WP67_ID_EST_POLI_CPI_PR + WP67_ID_MOVI_CRZ + Wp67IdMoviChiu.Len.WP67_ID_MOVI_CHIU + WP67_DT_INI_EFF + WP67_DT_END_EFF + WP67_COD_COMP_ANIA + WP67_IB_OGG + WP67_DS_RIGA + WP67_DS_OPER_SQL + WP67_DS_VER + WP67_DS_TS_INI_CPTZ + WP67_DS_TS_END_CPTZ + WP67_DS_UTENTE + WP67_DS_STATO_ELAB + WP67_COD_PROD_ESTNO + Wp67CptFin.Len.WP67_CPT_FIN + Wp67NumTstFin.Len.WP67_NUM_TST_FIN + Wp67TsFinanz.Len.WP67_TS_FINANZ + Wp67DurMmFinanz.Len.WP67_DUR_MM_FINANZ + Wp67DtEndFinanz.Len.WP67_DT_END_FINANZ + Wp67Amm1aRat.Len.WP67_AMM1A_RAT + Wp67ValRiscBene.Len.WP67_VAL_RISC_BENE + Wp67AmmRatEnd.Len.WP67_AMM_RAT_END + Wp67TsCreRatFinanz.Len.WP67_TS_CRE_RAT_FINANZ + Wp67ImpFinRevolving.Len.WP67_IMP_FIN_REVOLVING + Wp67ImpUtilCRev.Len.WP67_IMP_UTIL_C_REV + Wp67ImpRatRevolving.Len.WP67_IMP_RAT_REVOLVING + Wp67Dt1oUtlzCRev.Len.WP67_DT1O_UTLZ_C_REV + Wp67ImpAssto.Len.WP67_IMP_ASSTO + Wp67PreVers.Len.WP67_PRE_VERS + Wp67DtScadCop.Len.WP67_DT_SCAD_COP + Wp67GgDelMmScadRat.Len.WP67_GG_DEL_MM_SCAD_RAT + WP67_FL_PRE_FIN + Wp67DtScad1aRat.Len.WP67_DT_SCAD1A_RAT + Wp67DtErogFinanz.Len.WP67_DT_EROG_FINANZ + Wp67DtStipulaFinanz.Len.WP67_DT_STIPULA_FINANZ + Wp67MmPreamm.Len.WP67_MM_PREAMM + Wp67ImpDebRes.Len.WP67_IMP_DEB_RES + Wp67ImpRatFinanz.Len.WP67_IMP_RAT_FINANZ + Wp67ImpCanoneAntic.Len.WP67_IMP_CANONE_ANTIC + Wp67PerRatFinanz.Len.WP67_PER_RAT_FINANZ + WP67_TP_MOD_PAG_RAT + WP67_TP_FINANZ_ER + WP67_CAT_FINANZ_ER + Wp67ValRiscEndLeas.Len.WP67_VAL_RISC_END_LEAS + Wp67DtEstFinanz.Len.WP67_DT_EST_FINANZ + Wp67DtManCop.Len.WP67_DT_MAN_COP + WP67_NUM_FINANZ + WP67_TP_MOD_ACQS;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_ID_EST_POLI_CPI_PR = 9;
            public static final int WP67_ID_MOVI_CRZ = 9;
            public static final int WP67_DT_INI_EFF = 8;
            public static final int WP67_DT_END_EFF = 8;
            public static final int WP67_COD_COMP_ANIA = 5;
            public static final int WP67_DS_RIGA = 10;
            public static final int WP67_DS_VER = 9;
            public static final int WP67_DS_TS_INI_CPTZ = 18;
            public static final int WP67_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
