package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Wp85CommisGest;
import it.accenture.jnais.ws.redefines.Wp85MinGarto;
import it.accenture.jnais.ws.redefines.Wp85MinTrnut;
import it.accenture.jnais.ws.redefines.Wp85PcRetr;
import it.accenture.jnais.ws.redefines.Wp85RendtoLrd;
import it.accenture.jnais.ws.redefines.Wp85RendtoRetr;
import it.accenture.jnais.ws.redefines.Wp85TsRivalNet;

/**Original name: WP85-DATI<br>
 * Variable: WP85-DATI from copybook LCCVP851<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wp85Dati {

    //==== PROPERTIES ====
    //Original name: WP85-COD-COMP-ANIA
    private int wp85CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WP85-ID-POLI
    private int wp85IdPoli = DefaultValues.INT_VAL;
    //Original name: WP85-DT-RIVAL
    private int wp85DtRival = DefaultValues.INT_VAL;
    //Original name: WP85-COD-TARI
    private String wp85CodTari = DefaultValues.stringVal(Len.WP85_COD_TARI);
    //Original name: WP85-RENDTO-LRD
    private Wp85RendtoLrd wp85RendtoLrd = new Wp85RendtoLrd();
    //Original name: WP85-PC-RETR
    private Wp85PcRetr wp85PcRetr = new Wp85PcRetr();
    //Original name: WP85-RENDTO-RETR
    private Wp85RendtoRetr wp85RendtoRetr = new Wp85RendtoRetr();
    //Original name: WP85-COMMIS-GEST
    private Wp85CommisGest wp85CommisGest = new Wp85CommisGest();
    //Original name: WP85-TS-RIVAL-NET
    private Wp85TsRivalNet wp85TsRivalNet = new Wp85TsRivalNet();
    //Original name: WP85-MIN-GARTO
    private Wp85MinGarto wp85MinGarto = new Wp85MinGarto();
    //Original name: WP85-MIN-TRNUT
    private Wp85MinTrnut wp85MinTrnut = new Wp85MinTrnut();
    //Original name: WP85-IB-POLI
    private String wp85IbPoli = DefaultValues.stringVal(Len.WP85_IB_POLI);
    //Original name: WP85-DS-OPER-SQL
    private char wp85DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WP85-DS-VER
    private int wp85DsVer = DefaultValues.INT_VAL;
    //Original name: WP85-DS-TS-CPTZ
    private long wp85DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WP85-DS-UTENTE
    private String wp85DsUtente = DefaultValues.stringVal(Len.WP85_DS_UTENTE);
    //Original name: WP85-DS-STATO-ELAB
    private char wp85DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setWp85CodCompAnia(int wp85CodCompAnia) {
        this.wp85CodCompAnia = wp85CodCompAnia;
    }

    public int getWp85CodCompAnia() {
        return this.wp85CodCompAnia;
    }

    public void setWp85IdPoli(int wp85IdPoli) {
        this.wp85IdPoli = wp85IdPoli;
    }

    public int getWp85IdPoli() {
        return this.wp85IdPoli;
    }

    public void setWp85DtRival(int wp85DtRival) {
        this.wp85DtRival = wp85DtRival;
    }

    public int getWp85DtRival() {
        return this.wp85DtRival;
    }

    public void setWp85CodTari(String wp85CodTari) {
        this.wp85CodTari = Functions.subString(wp85CodTari, Len.WP85_COD_TARI);
    }

    public String getWp85CodTari() {
        return this.wp85CodTari;
    }

    public void setWp85IbPoli(String wp85IbPoli) {
        this.wp85IbPoli = Functions.subString(wp85IbPoli, Len.WP85_IB_POLI);
    }

    public String getWp85IbPoli() {
        return this.wp85IbPoli;
    }

    public void setWp85DsOperSql(char wp85DsOperSql) {
        this.wp85DsOperSql = wp85DsOperSql;
    }

    public char getWp85DsOperSql() {
        return this.wp85DsOperSql;
    }

    public void setWp85DsVer(int wp85DsVer) {
        this.wp85DsVer = wp85DsVer;
    }

    public int getWp85DsVer() {
        return this.wp85DsVer;
    }

    public void setWp85DsTsCptz(long wp85DsTsCptz) {
        this.wp85DsTsCptz = wp85DsTsCptz;
    }

    public long getWp85DsTsCptz() {
        return this.wp85DsTsCptz;
    }

    public void setWp85DsUtente(String wp85DsUtente) {
        this.wp85DsUtente = Functions.subString(wp85DsUtente, Len.WP85_DS_UTENTE);
    }

    public String getWp85DsUtente() {
        return this.wp85DsUtente;
    }

    public void setWp85DsStatoElab(char wp85DsStatoElab) {
        this.wp85DsStatoElab = wp85DsStatoElab;
    }

    public char getWp85DsStatoElab() {
        return this.wp85DsStatoElab;
    }

    public Wp85CommisGest getWp85CommisGest() {
        return wp85CommisGest;
    }

    public Wp85MinGarto getWp85MinGarto() {
        return wp85MinGarto;
    }

    public Wp85MinTrnut getWp85MinTrnut() {
        return wp85MinTrnut;
    }

    public Wp85PcRetr getWp85PcRetr() {
        return wp85PcRetr;
    }

    public Wp85RendtoLrd getWp85RendtoLrd() {
        return wp85RendtoLrd;
    }

    public Wp85RendtoRetr getWp85RendtoRetr() {
        return wp85RendtoRetr;
    }

    public Wp85TsRivalNet getWp85TsRivalNet() {
        return wp85TsRivalNet;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WP85_COD_TARI = 12;
        public static final int WP85_IB_POLI = 40;
        public static final int WP85_DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
