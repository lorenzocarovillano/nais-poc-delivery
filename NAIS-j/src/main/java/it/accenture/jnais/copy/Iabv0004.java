package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABV0004<br>
 * Variable: IABV0004 from copybook IABV0004<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Iabv0004 {

    //==== PROPERTIES ====
    //Original name: PGM-IABS0030
    private String pgmIabs0030 = "IABS0030";
    //Original name: PGM-IABS0040
    private String pgmIabs0040 = "IABS0040";
    //Original name: PGM-IABS0050
    private String pgmIabs0050 = "IABS0050";
    //Original name: PGM-IABS0060
    private String pgmIabs0060 = "IABS0060";
    //Original name: PGM-IABS0070
    private String pgmIabs0070 = "IABS0070";
    //Original name: PGM-IABS0080
    private String pgmIabs0080 = "IABS0080";
    //Original name: PGM-IABS0090
    private String pgmIabs0090 = "IABS0090";
    //Original name: PGM-IABS0110
    private String pgmIabs0110 = "IABS0110";
    //Original name: PGM-IABS0120
    private String pgmIabs0120 = "IABS0120";
    //Original name: PGM-IABS0130
    private String pgmIabs0130 = "IABS0130";
    //Original name: PGM-IABS0140
    private String pgmIabs0140 = "IABS0140";
    //Original name: PGM-IABS0900
    private String pgmIabs0900 = "IABS0900";
    //Original name: PGM-IDSS0150
    private String pgmIdss0150 = "IDSS0150";
    //Original name: PGM-IDSS0300
    private String pgmIdss0300 = "IDSS0300";
    //Original name: PGM-LCCS0090
    private String pgmLccs0090 = "LCCS0090";
    //Original name: PGM-IDES0020
    private String pgmIdes0020 = "IDES0020";
    /**Original name: BATCH-DA-ESEGUIRE<br>
	 * <pre>**************************************************************
	 *  TRASCODIFICA STATI BATCH
	 * **************************************************************</pre>*/
    private char batchDaEseguire = '1';
    //Original name: BATCH-ESEGUITO
    private char batchEseguito = '2';
    //Original name: BATCH-DA-RIESEGUIRE
    private char batchDaRieseguire = '3';
    //Original name: BATCH-IN-ESECUZIONE
    private char batchInEsecuzione = '4';
    //Original name: BATCH-SIMULATO-OK
    private char batchSimulatoOk = '5';
    //Original name: BATCH-SIMULATO-KO
    private char batchSimulatoKo = '6';
    /**Original name: JOB-DA-ESEGUIRE<br>
	 * <pre>**************************************************************
	 *  TRASCODIFICA STATI JOB
	 * **************************************************************</pre>*/
    private char jobDaEseguire = '1';
    //Original name: JOB-ESEGUITO-OK
    private char jobEseguitoOk = '2';
    //Original name: JOB-DA-RIESEGUIRE
    private char jobDaRieseguire = '3';
    //Original name: JOB-IN-ESECUZIONE
    private char jobInEsecuzione = '4';
    //Original name: JOB-ESEGUITO-KO
    private char jobEseguitoKo = '5';
    //Original name: JOB-STARTING
    private char jobStarting = '6';
    //Original name: ESEGUITA
    private char eseguita = 'E';
    //Original name: ESEGUITA-KO
    private char eseguitaKo = 'K';
    //Original name: WK-STATE-CURRENT
    private char wkStateCurrent = Types.SPACE_CHAR;

    //==== METHODS ====
    public String getPgmIabs0030() {
        return this.pgmIabs0030;
    }

    public String getPgmIabs0030Formatted() {
        return Functions.padBlanks(getPgmIabs0030(), Len.PGM_IABS0030);
    }

    public String getPgmIabs0040() {
        return this.pgmIabs0040;
    }

    public String getPgmIabs0040Formatted() {
        return Functions.padBlanks(getPgmIabs0040(), Len.PGM_IABS0040);
    }

    public String getPgmIabs0050() {
        return this.pgmIabs0050;
    }

    public String getPgmIabs0050Formatted() {
        return Functions.padBlanks(getPgmIabs0050(), Len.PGM_IABS0050);
    }

    public String getPgmIabs0060() {
        return this.pgmIabs0060;
    }

    public String getPgmIabs0060Formatted() {
        return Functions.padBlanks(getPgmIabs0060(), Len.PGM_IABS0060);
    }

    public String getPgmIabs0070() {
        return this.pgmIabs0070;
    }

    public String getPgmIabs0070Formatted() {
        return Functions.padBlanks(getPgmIabs0070(), Len.PGM_IABS0070);
    }

    public String getPgmIabs0080() {
        return this.pgmIabs0080;
    }

    public String getPgmIabs0080Formatted() {
        return Functions.padBlanks(getPgmIabs0080(), Len.PGM_IABS0080);
    }

    public String getPgmIabs0090() {
        return this.pgmIabs0090;
    }

    public String getPgmIabs0090Formatted() {
        return Functions.padBlanks(getPgmIabs0090(), Len.PGM_IABS0090);
    }

    public String getPgmIabs0110() {
        return this.pgmIabs0110;
    }

    public String getPgmIabs0110Formatted() {
        return Functions.padBlanks(getPgmIabs0110(), Len.PGM_IABS0110);
    }

    public String getPgmIabs0120() {
        return this.pgmIabs0120;
    }

    public String getPgmIabs0120Formatted() {
        return Functions.padBlanks(getPgmIabs0120(), Len.PGM_IABS0120);
    }

    public String getPgmIabs0130() {
        return this.pgmIabs0130;
    }

    public String getPgmIabs0130Formatted() {
        return Functions.padBlanks(getPgmIabs0130(), Len.PGM_IABS0130);
    }

    public String getPgmIabs0140() {
        return this.pgmIabs0140;
    }

    public String getPgmIabs0140Formatted() {
        return Functions.padBlanks(getPgmIabs0140(), Len.PGM_IABS0140);
    }

    public String getPgmIabs0900() {
        return this.pgmIabs0900;
    }

    public String getPgmIabs0900Formatted() {
        return Functions.padBlanks(getPgmIabs0900(), Len.PGM_IABS0900);
    }

    public String getPgmIdss0150() {
        return this.pgmIdss0150;
    }

    public String getPgmIdss0150Formatted() {
        return Functions.padBlanks(getPgmIdss0150(), Len.PGM_IDSS0150);
    }

    public String getPgmIdss0300() {
        return this.pgmIdss0300;
    }

    public String getPgmIdss0300Formatted() {
        return Functions.padBlanks(getPgmIdss0300(), Len.PGM_IDSS0300);
    }

    public String getPgmLccs0090() {
        return this.pgmLccs0090;
    }

    public String getPgmLccs0090Formatted() {
        return Functions.padBlanks(getPgmLccs0090(), Len.PGM_LCCS0090);
    }

    public String getPgmIdes0020() {
        return this.pgmIdes0020;
    }

    public String getPgmIdes0020Formatted() {
        return Functions.padBlanks(getPgmIdes0020(), Len.PGM_IDES0020);
    }

    public char getBatchDaEseguire() {
        return this.batchDaEseguire;
    }

    public char getBatchEseguito() {
        return this.batchEseguito;
    }

    public char getBatchDaRieseguire() {
        return this.batchDaRieseguire;
    }

    public char getBatchInEsecuzione() {
        return this.batchInEsecuzione;
    }

    public char getBatchSimulatoOk() {
        return this.batchSimulatoOk;
    }

    public char getBatchSimulatoKo() {
        return this.batchSimulatoKo;
    }

    public char getJobDaEseguire() {
        return this.jobDaEseguire;
    }

    public char getJobEseguitoOk() {
        return this.jobEseguitoOk;
    }

    public char getJobDaRieseguire() {
        return this.jobDaRieseguire;
    }

    public char getJobInEsecuzione() {
        return this.jobInEsecuzione;
    }

    public char getJobEseguitoKo() {
        return this.jobEseguitoKo;
    }

    public char getJobStarting() {
        return this.jobStarting;
    }

    public char getEseguita() {
        return this.eseguita;
    }

    public char getEseguitaKo() {
        return this.eseguitaKo;
    }

    public void setWkStateCurrent(char wkStateCurrent) {
        this.wkStateCurrent = wkStateCurrent;
    }

    public char getWkStateCurrent() {
        return this.wkStateCurrent;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PGM_IABS0140 = 8;
        public static final int PGM_IDSS0150 = 8;
        public static final int PGM_LCCS0090 = 8;
        public static final int PGM_IABS0070 = 8;
        public static final int PGM_IABS0030 = 8;
        public static final int PGM_IABS0040 = 8;
        public static final int PGM_IABS0050 = 8;
        public static final int PGM_IABS0900 = 8;
        public static final int PGM_IABS0060 = 8;
        public static final int PGM_IDES0020 = 8;
        public static final int PGM_IABS0130 = 8;
        public static final int PGM_IABS0120 = 8;
        public static final int PGM_IDSS0300 = 8;
        public static final int PGM_IABS0110 = 8;
        public static final int PGM_IABS0080 = 8;
        public static final int PGM_IABS0090 = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
