package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.E12AccprePag;
import it.accenture.jnais.ws.redefines.E12CumPreAtt;
import it.accenture.jnais.ws.redefines.E12CumPrstz;
import it.accenture.jnais.ws.redefines.E12DtEmis;
import it.accenture.jnais.ws.redefines.E12IdMoviChiu;

/**Original name: EST-TRCH-DI-GAR<br>
 * Variable: EST-TRCH-DI-GAR from copybook IDBVE121<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class EstTrchDiGar {

    //==== PROPERTIES ====
    //Original name: E12-ID-EST-TRCH-DI-GAR
    private int e12IdEstTrchDiGar = DefaultValues.INT_VAL;
    //Original name: E12-ID-TRCH-DI-GAR
    private int e12IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: E12-ID-GAR
    private int e12IdGar = DefaultValues.INT_VAL;
    //Original name: E12-ID-ADES
    private int e12IdAdes = DefaultValues.INT_VAL;
    //Original name: E12-ID-POLI
    private int e12IdPoli = DefaultValues.INT_VAL;
    //Original name: E12-ID-MOVI-CRZ
    private int e12IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: E12-ID-MOVI-CHIU
    private E12IdMoviChiu e12IdMoviChiu = new E12IdMoviChiu();
    //Original name: E12-DT-INI-EFF
    private int e12DtIniEff = DefaultValues.INT_VAL;
    //Original name: E12-DT-END-EFF
    private int e12DtEndEff = DefaultValues.INT_VAL;
    //Original name: E12-COD-COMP-ANIA
    private int e12CodCompAnia = DefaultValues.INT_VAL;
    //Original name: E12-DT-EMIS
    private E12DtEmis e12DtEmis = new E12DtEmis();
    //Original name: E12-CUM-PRE-ATT
    private E12CumPreAtt e12CumPreAtt = new E12CumPreAtt();
    //Original name: E12-ACCPRE-PAG
    private E12AccprePag e12AccprePag = new E12AccprePag();
    //Original name: E12-CUM-PRSTZ
    private E12CumPrstz e12CumPrstz = new E12CumPrstz();
    //Original name: E12-DS-RIGA
    private long e12DsRiga = DefaultValues.LONG_VAL;
    //Original name: E12-DS-OPER-SQL
    private char e12DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: E12-DS-VER
    private int e12DsVer = DefaultValues.INT_VAL;
    //Original name: E12-DS-TS-INI-CPTZ
    private long e12DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: E12-DS-TS-END-CPTZ
    private long e12DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: E12-DS-UTENTE
    private String e12DsUtente = DefaultValues.stringVal(Len.E12_DS_UTENTE);
    //Original name: E12-DS-STATO-ELAB
    private char e12DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: E12-TP-TRCH
    private String e12TpTrch = DefaultValues.stringVal(Len.E12_TP_TRCH);
    //Original name: E12-CAUS-SCON
    private String e12CausScon = DefaultValues.stringVal(Len.E12_CAUS_SCON);

    //==== METHODS ====
    public void setEstTrchDiGarFormatted(String data) {
        byte[] buffer = new byte[Len.EST_TRCH_DI_GAR];
        MarshalByte.writeString(buffer, 1, data, Len.EST_TRCH_DI_GAR);
        setEstTrchDiGarBytes(buffer, 1);
    }

    public String getEstTrchDiGarFormatted() {
        return MarshalByteExt.bufferToStr(getEstTrchDiGarBytes());
    }

    public byte[] getEstTrchDiGarBytes() {
        byte[] buffer = new byte[Len.EST_TRCH_DI_GAR];
        return getEstTrchDiGarBytes(buffer, 1);
    }

    public void setEstTrchDiGarBytes(byte[] buffer, int offset) {
        int position = offset;
        e12IdEstTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E12_ID_EST_TRCH_DI_GAR, 0);
        position += Len.E12_ID_EST_TRCH_DI_GAR;
        e12IdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E12_ID_TRCH_DI_GAR, 0);
        position += Len.E12_ID_TRCH_DI_GAR;
        e12IdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E12_ID_GAR, 0);
        position += Len.E12_ID_GAR;
        e12IdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E12_ID_ADES, 0);
        position += Len.E12_ID_ADES;
        e12IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E12_ID_POLI, 0);
        position += Len.E12_ID_POLI;
        e12IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E12_ID_MOVI_CRZ, 0);
        position += Len.E12_ID_MOVI_CRZ;
        e12IdMoviChiu.setE12IdMoviChiuFromBuffer(buffer, position);
        position += E12IdMoviChiu.Len.E12_ID_MOVI_CHIU;
        e12DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E12_DT_INI_EFF, 0);
        position += Len.E12_DT_INI_EFF;
        e12DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E12_DT_END_EFF, 0);
        position += Len.E12_DT_END_EFF;
        e12CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E12_COD_COMP_ANIA, 0);
        position += Len.E12_COD_COMP_ANIA;
        e12DtEmis.setE12DtEmisFromBuffer(buffer, position);
        position += E12DtEmis.Len.E12_DT_EMIS;
        e12CumPreAtt.setE12CumPreAttFromBuffer(buffer, position);
        position += E12CumPreAtt.Len.E12_CUM_PRE_ATT;
        e12AccprePag.setE12AccprePagFromBuffer(buffer, position);
        position += E12AccprePag.Len.E12_ACCPRE_PAG;
        e12CumPrstz.setE12CumPrstzFromBuffer(buffer, position);
        position += E12CumPrstz.Len.E12_CUM_PRSTZ;
        e12DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.E12_DS_RIGA, 0);
        position += Len.E12_DS_RIGA;
        e12DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        e12DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.E12_DS_VER, 0);
        position += Len.E12_DS_VER;
        e12DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.E12_DS_TS_INI_CPTZ, 0);
        position += Len.E12_DS_TS_INI_CPTZ;
        e12DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.E12_DS_TS_END_CPTZ, 0);
        position += Len.E12_DS_TS_END_CPTZ;
        e12DsUtente = MarshalByte.readString(buffer, position, Len.E12_DS_UTENTE);
        position += Len.E12_DS_UTENTE;
        e12DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        e12TpTrch = MarshalByte.readString(buffer, position, Len.E12_TP_TRCH);
        position += Len.E12_TP_TRCH;
        e12CausScon = MarshalByte.readString(buffer, position, Len.E12_CAUS_SCON);
    }

    public byte[] getEstTrchDiGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, e12IdEstTrchDiGar, Len.Int.E12_ID_EST_TRCH_DI_GAR, 0);
        position += Len.E12_ID_EST_TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, e12IdTrchDiGar, Len.Int.E12_ID_TRCH_DI_GAR, 0);
        position += Len.E12_ID_TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, e12IdGar, Len.Int.E12_ID_GAR, 0);
        position += Len.E12_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, e12IdAdes, Len.Int.E12_ID_ADES, 0);
        position += Len.E12_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, e12IdPoli, Len.Int.E12_ID_POLI, 0);
        position += Len.E12_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, e12IdMoviCrz, Len.Int.E12_ID_MOVI_CRZ, 0);
        position += Len.E12_ID_MOVI_CRZ;
        e12IdMoviChiu.getE12IdMoviChiuAsBuffer(buffer, position);
        position += E12IdMoviChiu.Len.E12_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, e12DtIniEff, Len.Int.E12_DT_INI_EFF, 0);
        position += Len.E12_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, e12DtEndEff, Len.Int.E12_DT_END_EFF, 0);
        position += Len.E12_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, e12CodCompAnia, Len.Int.E12_COD_COMP_ANIA, 0);
        position += Len.E12_COD_COMP_ANIA;
        e12DtEmis.getE12DtEmisAsBuffer(buffer, position);
        position += E12DtEmis.Len.E12_DT_EMIS;
        e12CumPreAtt.getE12CumPreAttAsBuffer(buffer, position);
        position += E12CumPreAtt.Len.E12_CUM_PRE_ATT;
        e12AccprePag.getE12AccprePagAsBuffer(buffer, position);
        position += E12AccprePag.Len.E12_ACCPRE_PAG;
        e12CumPrstz.getE12CumPrstzAsBuffer(buffer, position);
        position += E12CumPrstz.Len.E12_CUM_PRSTZ;
        MarshalByte.writeLongAsPacked(buffer, position, e12DsRiga, Len.Int.E12_DS_RIGA, 0);
        position += Len.E12_DS_RIGA;
        MarshalByte.writeChar(buffer, position, e12DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, e12DsVer, Len.Int.E12_DS_VER, 0);
        position += Len.E12_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, e12DsTsIniCptz, Len.Int.E12_DS_TS_INI_CPTZ, 0);
        position += Len.E12_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, e12DsTsEndCptz, Len.Int.E12_DS_TS_END_CPTZ, 0);
        position += Len.E12_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, e12DsUtente, Len.E12_DS_UTENTE);
        position += Len.E12_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, e12DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, e12TpTrch, Len.E12_TP_TRCH);
        position += Len.E12_TP_TRCH;
        MarshalByte.writeString(buffer, position, e12CausScon, Len.E12_CAUS_SCON);
        return buffer;
    }

    public void setE12IdEstTrchDiGar(int e12IdEstTrchDiGar) {
        this.e12IdEstTrchDiGar = e12IdEstTrchDiGar;
    }

    public int getE12IdEstTrchDiGar() {
        return this.e12IdEstTrchDiGar;
    }

    public void setE12IdTrchDiGar(int e12IdTrchDiGar) {
        this.e12IdTrchDiGar = e12IdTrchDiGar;
    }

    public int getE12IdTrchDiGar() {
        return this.e12IdTrchDiGar;
    }

    public void setE12IdGar(int e12IdGar) {
        this.e12IdGar = e12IdGar;
    }

    public int getE12IdGar() {
        return this.e12IdGar;
    }

    public void setE12IdAdes(int e12IdAdes) {
        this.e12IdAdes = e12IdAdes;
    }

    public int getE12IdAdes() {
        return this.e12IdAdes;
    }

    public void setE12IdPoli(int e12IdPoli) {
        this.e12IdPoli = e12IdPoli;
    }

    public int getE12IdPoli() {
        return this.e12IdPoli;
    }

    public void setE12IdMoviCrz(int e12IdMoviCrz) {
        this.e12IdMoviCrz = e12IdMoviCrz;
    }

    public int getE12IdMoviCrz() {
        return this.e12IdMoviCrz;
    }

    public void setE12DtIniEff(int e12DtIniEff) {
        this.e12DtIniEff = e12DtIniEff;
    }

    public int getE12DtIniEff() {
        return this.e12DtIniEff;
    }

    public void setE12DtEndEff(int e12DtEndEff) {
        this.e12DtEndEff = e12DtEndEff;
    }

    public int getE12DtEndEff() {
        return this.e12DtEndEff;
    }

    public void setE12CodCompAnia(int e12CodCompAnia) {
        this.e12CodCompAnia = e12CodCompAnia;
    }

    public int getE12CodCompAnia() {
        return this.e12CodCompAnia;
    }

    public void setE12DsRiga(long e12DsRiga) {
        this.e12DsRiga = e12DsRiga;
    }

    public long getE12DsRiga() {
        return this.e12DsRiga;
    }

    public void setE12DsOperSql(char e12DsOperSql) {
        this.e12DsOperSql = e12DsOperSql;
    }

    public char getE12DsOperSql() {
        return this.e12DsOperSql;
    }

    public void setE12DsVer(int e12DsVer) {
        this.e12DsVer = e12DsVer;
    }

    public int getE12DsVer() {
        return this.e12DsVer;
    }

    public void setE12DsTsIniCptz(long e12DsTsIniCptz) {
        this.e12DsTsIniCptz = e12DsTsIniCptz;
    }

    public long getE12DsTsIniCptz() {
        return this.e12DsTsIniCptz;
    }

    public void setE12DsTsEndCptz(long e12DsTsEndCptz) {
        this.e12DsTsEndCptz = e12DsTsEndCptz;
    }

    public long getE12DsTsEndCptz() {
        return this.e12DsTsEndCptz;
    }

    public void setE12DsUtente(String e12DsUtente) {
        this.e12DsUtente = Functions.subString(e12DsUtente, Len.E12_DS_UTENTE);
    }

    public String getE12DsUtente() {
        return this.e12DsUtente;
    }

    public void setE12DsStatoElab(char e12DsStatoElab) {
        this.e12DsStatoElab = e12DsStatoElab;
    }

    public char getE12DsStatoElab() {
        return this.e12DsStatoElab;
    }

    public void setE12TpTrch(String e12TpTrch) {
        this.e12TpTrch = Functions.subString(e12TpTrch, Len.E12_TP_TRCH);
    }

    public String getE12TpTrch() {
        return this.e12TpTrch;
    }

    public void setE12CausScon(String e12CausScon) {
        this.e12CausScon = Functions.subString(e12CausScon, Len.E12_CAUS_SCON);
    }

    public String getE12CausScon() {
        return this.e12CausScon;
    }

    public String getE12CausSconFormatted() {
        return Functions.padBlanks(getE12CausScon(), Len.E12_CAUS_SCON);
    }

    public E12AccprePag getE12AccprePag() {
        return e12AccprePag;
    }

    public E12CumPreAtt getE12CumPreAtt() {
        return e12CumPreAtt;
    }

    public E12CumPrstz getE12CumPrstz() {
        return e12CumPrstz;
    }

    public E12DtEmis getE12DtEmis() {
        return e12DtEmis;
    }

    public E12IdMoviChiu getE12IdMoviChiu() {
        return e12IdMoviChiu;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int E12_DS_UTENTE = 20;
        public static final int E12_TP_TRCH = 2;
        public static final int E12_CAUS_SCON = 12;
        public static final int E12_ID_EST_TRCH_DI_GAR = 5;
        public static final int E12_ID_TRCH_DI_GAR = 5;
        public static final int E12_ID_GAR = 5;
        public static final int E12_ID_ADES = 5;
        public static final int E12_ID_POLI = 5;
        public static final int E12_ID_MOVI_CRZ = 5;
        public static final int E12_DT_INI_EFF = 5;
        public static final int E12_DT_END_EFF = 5;
        public static final int E12_COD_COMP_ANIA = 3;
        public static final int E12_DS_RIGA = 6;
        public static final int E12_DS_OPER_SQL = 1;
        public static final int E12_DS_VER = 5;
        public static final int E12_DS_TS_INI_CPTZ = 10;
        public static final int E12_DS_TS_END_CPTZ = 10;
        public static final int E12_DS_STATO_ELAB = 1;
        public static final int EST_TRCH_DI_GAR = E12_ID_EST_TRCH_DI_GAR + E12_ID_TRCH_DI_GAR + E12_ID_GAR + E12_ID_ADES + E12_ID_POLI + E12_ID_MOVI_CRZ + E12IdMoviChiu.Len.E12_ID_MOVI_CHIU + E12_DT_INI_EFF + E12_DT_END_EFF + E12_COD_COMP_ANIA + E12DtEmis.Len.E12_DT_EMIS + E12CumPreAtt.Len.E12_CUM_PRE_ATT + E12AccprePag.Len.E12_ACCPRE_PAG + E12CumPrstz.Len.E12_CUM_PRSTZ + E12_DS_RIGA + E12_DS_OPER_SQL + E12_DS_VER + E12_DS_TS_INI_CPTZ + E12_DS_TS_END_CPTZ + E12_DS_UTENTE + E12_DS_STATO_ELAB + E12_TP_TRCH + E12_CAUS_SCON;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E12_ID_EST_TRCH_DI_GAR = 9;
            public static final int E12_ID_TRCH_DI_GAR = 9;
            public static final int E12_ID_GAR = 9;
            public static final int E12_ID_ADES = 9;
            public static final int E12_ID_POLI = 9;
            public static final int E12_ID_MOVI_CRZ = 9;
            public static final int E12_DT_INI_EFF = 8;
            public static final int E12_DT_END_EFF = 8;
            public static final int E12_COD_COMP_ANIA = 5;
            public static final int E12_DS_RIGA = 10;
            public static final int E12_DS_VER = 9;
            public static final int E12_DS_TS_INI_CPTZ = 18;
            public static final int E12_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
