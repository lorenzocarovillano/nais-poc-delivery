package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-RAPP-ANA<br>
 * Variable: IND-RAPP-ANA from copybook IDBVRAN2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndRappAna {

    //==== PROPERTIES ====
    //Original name: IND-RAN-ID-RAPP-ANA-COLLG
    private short idRappAnaCollg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-COD-SOGG
    private short codSogg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-PERS
    private short tpPers = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-SEX
    private short sex = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-DT-NASC
    private short dtNasc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-FL-ESTAS
    private short flEstas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-INDIR-1
    private short indir1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-INDIR-2
    private short indir2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-INDIR-3
    private short indir3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-UTLZ-INDIR-1
    private short tpUtlzIndir1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-UTLZ-INDIR-2
    private short tpUtlzIndir2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-UTLZ-INDIR-3
    private short tpUtlzIndir3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-ESTR-CNT-CORR-ACCR
    private short estrCntCorrAccr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-ESTR-CNT-CORR-ADD
    private short estrCntCorrAdd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-ESTR-DOCTO
    private short estrDocto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-PC-NEL-RAPP
    private short pcNelRapp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-MEZ-PAG-ADD
    private short tpMezPagAdd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-MEZ-PAG-ACCR
    private short tpMezPagAccr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-COD-MATR
    private short codMatr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-ADEGZ
    private short tpAdegz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-FL-TST-RSH
    private short flTstRsh = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-COD-AZ
    private short codAz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-IND-PRINC
    private short indPrinc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-DT-DELIBERA-CDA
    private short dtDeliberaCda = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-DLG-AL-RISC
    private short dlgAlRisc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-LEGALE-RAPPR-PRINC
    private short legaleRapprPrinc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-LEGALE-RAPPR
    private short tpLegaleRappr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-IND-PRINC
    private short tpIndPrinc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-STAT-RID
    private short tpStatRid = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-NOME-INT-RID
    private short nomeIntRid = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-COGN-INT-RID
    private short cognIntRid = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-COGN-INT-TRATT
    private short cognIntTratt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-NOME-INT-TRATT
    private short nomeIntTratt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-CF-INT-RID
    private short cfIntRid = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-FL-COINC-DIP-CNTR
    private short flCoincDipCntr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-FL-COINC-INT-CNTR
    private short flCoincIntCntr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-DT-DECES
    private short dtDeces = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-FL-FUMATORE
    private short flFumatore = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-FL-LAV-DIP
    private short flLavDip = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-VARZ-PAGAT
    private short tpVarzPagat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-COD-RID
    private short codRid = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-TP-CAUS-RID
    private short tpCausRid = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-IND-MASSA-CORP
    private short indMassaCorp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RAN-CAT-RSH-PROF
    private short catRshProf = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdRappAnaCollg(short idRappAnaCollg) {
        this.idRappAnaCollg = idRappAnaCollg;
    }

    public short getIdRappAnaCollg() {
        return this.idRappAnaCollg;
    }

    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setCodSogg(short codSogg) {
        this.codSogg = codSogg;
    }

    public short getCodSogg() {
        return this.codSogg;
    }

    public void setTpPers(short tpPers) {
        this.tpPers = tpPers;
    }

    public short getTpPers() {
        return this.tpPers;
    }

    public void setSex(short sex) {
        this.sex = sex;
    }

    public short getSex() {
        return this.sex;
    }

    public void setDtNasc(short dtNasc) {
        this.dtNasc = dtNasc;
    }

    public short getDtNasc() {
        return this.dtNasc;
    }

    public void setFlEstas(short flEstas) {
        this.flEstas = flEstas;
    }

    public short getFlEstas() {
        return this.flEstas;
    }

    public void setIndir1(short indir1) {
        this.indir1 = indir1;
    }

    public short getIndir1() {
        return this.indir1;
    }

    public void setIndir2(short indir2) {
        this.indir2 = indir2;
    }

    public short getIndir2() {
        return this.indir2;
    }

    public void setIndir3(short indir3) {
        this.indir3 = indir3;
    }

    public short getIndir3() {
        return this.indir3;
    }

    public void setTpUtlzIndir1(short tpUtlzIndir1) {
        this.tpUtlzIndir1 = tpUtlzIndir1;
    }

    public short getTpUtlzIndir1() {
        return this.tpUtlzIndir1;
    }

    public void setTpUtlzIndir2(short tpUtlzIndir2) {
        this.tpUtlzIndir2 = tpUtlzIndir2;
    }

    public short getTpUtlzIndir2() {
        return this.tpUtlzIndir2;
    }

    public void setTpUtlzIndir3(short tpUtlzIndir3) {
        this.tpUtlzIndir3 = tpUtlzIndir3;
    }

    public short getTpUtlzIndir3() {
        return this.tpUtlzIndir3;
    }

    public void setEstrCntCorrAccr(short estrCntCorrAccr) {
        this.estrCntCorrAccr = estrCntCorrAccr;
    }

    public short getEstrCntCorrAccr() {
        return this.estrCntCorrAccr;
    }

    public void setEstrCntCorrAdd(short estrCntCorrAdd) {
        this.estrCntCorrAdd = estrCntCorrAdd;
    }

    public short getEstrCntCorrAdd() {
        return this.estrCntCorrAdd;
    }

    public void setEstrDocto(short estrDocto) {
        this.estrDocto = estrDocto;
    }

    public short getEstrDocto() {
        return this.estrDocto;
    }

    public void setPcNelRapp(short pcNelRapp) {
        this.pcNelRapp = pcNelRapp;
    }

    public short getPcNelRapp() {
        return this.pcNelRapp;
    }

    public void setTpMezPagAdd(short tpMezPagAdd) {
        this.tpMezPagAdd = tpMezPagAdd;
    }

    public short getTpMezPagAdd() {
        return this.tpMezPagAdd;
    }

    public void setTpMezPagAccr(short tpMezPagAccr) {
        this.tpMezPagAccr = tpMezPagAccr;
    }

    public short getTpMezPagAccr() {
        return this.tpMezPagAccr;
    }

    public void setCodMatr(short codMatr) {
        this.codMatr = codMatr;
    }

    public short getCodMatr() {
        return this.codMatr;
    }

    public void setTpAdegz(short tpAdegz) {
        this.tpAdegz = tpAdegz;
    }

    public short getTpAdegz() {
        return this.tpAdegz;
    }

    public void setFlTstRsh(short flTstRsh) {
        this.flTstRsh = flTstRsh;
    }

    public short getFlTstRsh() {
        return this.flTstRsh;
    }

    public void setCodAz(short codAz) {
        this.codAz = codAz;
    }

    public short getCodAz() {
        return this.codAz;
    }

    public void setIndPrinc(short indPrinc) {
        this.indPrinc = indPrinc;
    }

    public short getIndPrinc() {
        return this.indPrinc;
    }

    public void setDtDeliberaCda(short dtDeliberaCda) {
        this.dtDeliberaCda = dtDeliberaCda;
    }

    public short getDtDeliberaCda() {
        return this.dtDeliberaCda;
    }

    public void setDlgAlRisc(short dlgAlRisc) {
        this.dlgAlRisc = dlgAlRisc;
    }

    public short getDlgAlRisc() {
        return this.dlgAlRisc;
    }

    public void setLegaleRapprPrinc(short legaleRapprPrinc) {
        this.legaleRapprPrinc = legaleRapprPrinc;
    }

    public short getLegaleRapprPrinc() {
        return this.legaleRapprPrinc;
    }

    public void setTpLegaleRappr(short tpLegaleRappr) {
        this.tpLegaleRappr = tpLegaleRappr;
    }

    public short getTpLegaleRappr() {
        return this.tpLegaleRappr;
    }

    public void setTpIndPrinc(short tpIndPrinc) {
        this.tpIndPrinc = tpIndPrinc;
    }

    public short getTpIndPrinc() {
        return this.tpIndPrinc;
    }

    public void setTpStatRid(short tpStatRid) {
        this.tpStatRid = tpStatRid;
    }

    public short getTpStatRid() {
        return this.tpStatRid;
    }

    public void setNomeIntRid(short nomeIntRid) {
        this.nomeIntRid = nomeIntRid;
    }

    public short getNomeIntRid() {
        return this.nomeIntRid;
    }

    public void setCognIntRid(short cognIntRid) {
        this.cognIntRid = cognIntRid;
    }

    public short getCognIntRid() {
        return this.cognIntRid;
    }

    public void setCognIntTratt(short cognIntTratt) {
        this.cognIntTratt = cognIntTratt;
    }

    public short getCognIntTratt() {
        return this.cognIntTratt;
    }

    public void setNomeIntTratt(short nomeIntTratt) {
        this.nomeIntTratt = nomeIntTratt;
    }

    public short getNomeIntTratt() {
        return this.nomeIntTratt;
    }

    public void setCfIntRid(short cfIntRid) {
        this.cfIntRid = cfIntRid;
    }

    public short getCfIntRid() {
        return this.cfIntRid;
    }

    public void setFlCoincDipCntr(short flCoincDipCntr) {
        this.flCoincDipCntr = flCoincDipCntr;
    }

    public short getFlCoincDipCntr() {
        return this.flCoincDipCntr;
    }

    public void setFlCoincIntCntr(short flCoincIntCntr) {
        this.flCoincIntCntr = flCoincIntCntr;
    }

    public short getFlCoincIntCntr() {
        return this.flCoincIntCntr;
    }

    public void setDtDeces(short dtDeces) {
        this.dtDeces = dtDeces;
    }

    public short getDtDeces() {
        return this.dtDeces;
    }

    public void setFlFumatore(short flFumatore) {
        this.flFumatore = flFumatore;
    }

    public short getFlFumatore() {
        return this.flFumatore;
    }

    public void setFlLavDip(short flLavDip) {
        this.flLavDip = flLavDip;
    }

    public short getFlLavDip() {
        return this.flLavDip;
    }

    public void setTpVarzPagat(short tpVarzPagat) {
        this.tpVarzPagat = tpVarzPagat;
    }

    public short getTpVarzPagat() {
        return this.tpVarzPagat;
    }

    public void setCodRid(short codRid) {
        this.codRid = codRid;
    }

    public short getCodRid() {
        return this.codRid;
    }

    public void setTpCausRid(short tpCausRid) {
        this.tpCausRid = tpCausRid;
    }

    public short getTpCausRid() {
        return this.tpCausRid;
    }

    public void setIndMassaCorp(short indMassaCorp) {
        this.indMassaCorp = indMassaCorp;
    }

    public short getIndMassaCorp() {
        return this.indMassaCorp;
    }

    public void setCatRshProf(short catRshProf) {
        this.catRshProf = catRshProf;
    }

    public short getCatRshProf() {
        return this.catRshProf;
    }
}
