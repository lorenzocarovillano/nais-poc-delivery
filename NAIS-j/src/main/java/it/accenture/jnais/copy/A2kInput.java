package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.A2kIndata;

/**Original name: A2K-INPUT<br>
 * Variable: A2K-INPUT from copybook LCCC0003<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class A2kInput {

    //==== PROPERTIES ====
    //Original name: A2K-FUNZ
    private String a2kFunz = DefaultValues.stringVal(Len.A2K_FUNZ);
    /**Original name: A2K-INFO<br>
	 * <pre>                                       formato data input
	 *                                        01 : GGMMAAAA   in INGMA
	 *                                        02 : GG/MM/AAAA in INGBMBA
	 *                                        03 : AAAAMMGG   in INAMG
	 *                                        04 : AAAAMMGGs  in INAMGP
	 *                                        05 : AAAAMMGG0s in INAMG0P
	 *                                        06 : GGGGGs     in INPROG9
	 *                                        07 : GGGGGGGs   in INPROG
	 *                                        08 : AAAAGGG    in INJUL
	 *                                        09 : GGGGGs     in INPROGC
	 *                                        10 : GGGGGGGs   in INPROGO
	 *                                        11 : GG.MM.AAAA in INGBMBA</pre>*/
    private String a2kInfo = DefaultValues.stringVal(Len.A2K_INFO);
    //Original name: A2K-DELTA
    private String a2kDelta = DefaultValues.stringVal(Len.A2K_DELTA);
    /**Original name: A2K-TDELTA<br>
	 * <pre>                                       tipo delta : G = giorni
	 *                                                     M = mesi
	 *                                                     A = anni</pre>*/
    private char a2kTdelta = DefaultValues.CHAR_VAL;
    /**Original name: A2K-FISLAV<br>
	 * <pre>                                       tipo giorni da sommare
	 *                                        0 = fissi
	 *                                        1 = lavorativi</pre>*/
    private char a2kFislav = DefaultValues.CHAR_VAL;
    /**Original name: A2K-INICON<br>
	 * <pre>                                       giorno inizio conteggio
	 *                                        0 = stesso giorno
	 *                                        1 = primo giorno lavorat.
	 *                                        2 = se fest.,lavorat. prec</pre>*/
    private char a2kInicon = DefaultValues.CHAR_VAL;
    /**Original name: A2K-INDATA<br>
	 * <pre>                                       data di input</pre>*/
    private A2kIndata a2kIndata = new A2kIndata();

    //==== METHODS ====
    public void setInputBytes(byte[] buffer, int offset) {
        int position = offset;
        a2kFunz = MarshalByte.readString(buffer, position, Len.A2K_FUNZ);
        position += Len.A2K_FUNZ;
        a2kInfo = MarshalByte.readString(buffer, position, Len.A2K_INFO);
        position += Len.A2K_INFO;
        setA2kDeltaXBytes(buffer, position);
        position += Len.A2K_DELTA_X;
        a2kTdelta = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a2kFislav = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a2kInicon = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a2kIndata.setA2kIndataFromBuffer(buffer, position);
    }

    public byte[] getInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, a2kFunz, Len.A2K_FUNZ);
        position += Len.A2K_FUNZ;
        MarshalByte.writeString(buffer, position, a2kInfo, Len.A2K_INFO);
        position += Len.A2K_INFO;
        getA2kDeltaXBytes(buffer, position);
        position += Len.A2K_DELTA_X;
        MarshalByte.writeChar(buffer, position, a2kTdelta);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, a2kFislav);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, a2kInicon);
        position += Types.CHAR_SIZE;
        a2kIndata.getA2kIndataAsBuffer(buffer, position);
        return buffer;
    }

    public void setA2kFunz(String a2kFunz) {
        this.a2kFunz = Functions.subString(a2kFunz, Len.A2K_FUNZ);
    }

    public String getA2kFunz() {
        return this.a2kFunz;
    }

    public void setA2kInfo(String a2kInfo) {
        this.a2kInfo = Functions.subString(a2kInfo, Len.A2K_INFO);
    }

    public String getA2kInfo() {
        return this.a2kInfo;
    }

    /**Original name: A2K-DELTA-X<br>
	 * <pre>                                       valore da sommare</pre>*/
    public byte[] getA2kDeltaXBytes() {
        byte[] buffer = new byte[Len.A2K_DELTA_X];
        return getA2kDeltaXBytes(buffer, 1);
    }

    public void setA2kDeltaXBytes(byte[] buffer, int offset) {
        int position = offset;
        a2kDelta = MarshalByte.readFixedString(buffer, position, Len.A2K_DELTA);
    }

    public byte[] getA2kDeltaXBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, a2kDelta, Len.A2K_DELTA);
        return buffer;
    }

    public void setA2kDelta(short a2kDelta) {
        this.a2kDelta = NumericDisplay.asString(a2kDelta, Len.A2K_DELTA);
    }

    public void setA2kDeltaFormatted(String a2kDelta) {
        this.a2kDelta = Trunc.toUnsignedNumeric(a2kDelta, Len.A2K_DELTA);
    }

    public short getA2kDelta() {
        return NumericDisplay.asShort(this.a2kDelta);
    }

    public String getA2kDeltaFormatted() {
        return this.a2kDelta;
    }

    public void setA2kTdelta(char a2kTdelta) {
        this.a2kTdelta = a2kTdelta;
    }

    public void setA2kTdeltaFormatted(String a2kTdelta) {
        setA2kTdelta(Functions.charAt(a2kTdelta, Types.CHAR_SIZE));
    }

    public char getA2kTdelta() {
        return this.a2kTdelta;
    }

    public void setA2kFislav(char a2kFislav) {
        this.a2kFislav = a2kFislav;
    }

    public void setA2kFislavFormatted(String a2kFislav) {
        setA2kFislav(Functions.charAt(a2kFislav, Types.CHAR_SIZE));
    }

    public char getA2kFislav() {
        return this.a2kFislav;
    }

    public void setA2kInicon(char a2kInicon) {
        this.a2kInicon = a2kInicon;
    }

    public void setA2kIniconFormatted(String a2kInicon) {
        setA2kInicon(Functions.charAt(a2kInicon, Types.CHAR_SIZE));
    }

    public char getA2kInicon() {
        return this.a2kInicon;
    }

    public A2kIndata getA2kIndata() {
        return a2kIndata;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int A2K_FUNZ = 2;
        public static final int A2K_INFO = 2;
        public static final int A2K_DELTA = 3;
        public static final int A2K_DELTA_X = A2K_DELTA;
        public static final int A2K_TDELTA = 1;
        public static final int A2K_FISLAV = 1;
        public static final int A2K_INICON = 1;
        public static final int INPUT = A2K_FUNZ + A2K_INFO + A2K_DELTA_X + A2K_TDELTA + A2K_FISLAV + A2K_INICON + A2kIndata.Len.A2K_INDATA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
