package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVB043<br>
 * Copybook: IDBVB043 from copybook IDBVB043<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvb043 {

    //==== PROPERTIES ====
    //Original name: B04-DT-RIS-DB
    private String b04DtRisDb = DefaultValues.stringVal(Len.B04_DT_RIS_DB);
    //Original name: B04-DT-INI-VLDT-PROD-DB
    private String b04DtIniVldtProdDb = DefaultValues.stringVal(Len.B04_DT_INI_VLDT_PROD_DB);

    //==== METHODS ====
    public void setB04DtRisDb(String b04DtRisDb) {
        this.b04DtRisDb = Functions.subString(b04DtRisDb, Len.B04_DT_RIS_DB);
    }

    public String getB04DtRisDb() {
        return this.b04DtRisDb;
    }

    public void setB04DtIniVldtProdDb(String b04DtIniVldtProdDb) {
        this.b04DtIniVldtProdDb = Functions.subString(b04DtIniVldtProdDb, Len.B04_DT_INI_VLDT_PROD_DB);
    }

    public String getB04DtIniVldtProdDb() {
        return this.b04DtIniVldtProdDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int B04_DT_RIS_DB = 10;
        public static final int B04_DT_INI_VLDT_PROD_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
