package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WdcoDtScadAdesDflt;
import it.accenture.jnais.ws.redefines.WdcoDtUltRinnTac;
import it.accenture.jnais.ws.redefines.WdcoDurAaAdesDflt;
import it.accenture.jnais.ws.redefines.WdcoDurGgAdesDflt;
import it.accenture.jnais.ws.redefines.WdcoDurMmAdesDflt;
import it.accenture.jnais.ws.redefines.WdcoEtaScadFemmDflt;
import it.accenture.jnais.ws.redefines.WdcoEtaScadMascDflt;
import it.accenture.jnais.ws.redefines.WdcoFrazDflt;
import it.accenture.jnais.ws.redefines.WdcoIdMoviChiu;
import it.accenture.jnais.ws.redefines.WdcoImpArrotPre;
import it.accenture.jnais.ws.redefines.WdcoImpScon;
import it.accenture.jnais.ws.redefines.WdcoPcScon;

/**Original name: WDCO-DATI<br>
 * Variable: WDCO-DATI from copybook LCCVDCO1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WdcoDati {

    //==== PROPERTIES ====
    //Original name: WDCO-ID-D-COLL
    private int wdcoIdDColl = DefaultValues.INT_VAL;
    //Original name: WDCO-ID-POLI
    private int wdcoIdPoli = DefaultValues.INT_VAL;
    //Original name: WDCO-ID-MOVI-CRZ
    private int wdcoIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WDCO-ID-MOVI-CHIU
    private WdcoIdMoviChiu wdcoIdMoviChiu = new WdcoIdMoviChiu();
    //Original name: WDCO-DT-INI-EFF
    private int wdcoDtIniEff = DefaultValues.INT_VAL;
    //Original name: WDCO-DT-END-EFF
    private int wdcoDtEndEff = DefaultValues.INT_VAL;
    //Original name: WDCO-COD-COMP-ANIA
    private int wdcoCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WDCO-IMP-ARROT-PRE
    private WdcoImpArrotPre wdcoImpArrotPre = new WdcoImpArrotPre();
    //Original name: WDCO-PC-SCON
    private WdcoPcScon wdcoPcScon = new WdcoPcScon();
    //Original name: WDCO-FL-ADES-SING
    private char wdcoFlAdesSing = DefaultValues.CHAR_VAL;
    //Original name: WDCO-TP-IMP
    private String wdcoTpImp = DefaultValues.stringVal(Len.WDCO_TP_IMP);
    //Original name: WDCO-FL-RICL-PRE-DA-CPT
    private char wdcoFlRiclPreDaCpt = DefaultValues.CHAR_VAL;
    //Original name: WDCO-TP-ADES
    private String wdcoTpAdes = DefaultValues.stringVal(Len.WDCO_TP_ADES);
    //Original name: WDCO-DT-ULT-RINN-TAC
    private WdcoDtUltRinnTac wdcoDtUltRinnTac = new WdcoDtUltRinnTac();
    //Original name: WDCO-IMP-SCON
    private WdcoImpScon wdcoImpScon = new WdcoImpScon();
    //Original name: WDCO-FRAZ-DFLT
    private WdcoFrazDflt wdcoFrazDflt = new WdcoFrazDflt();
    //Original name: WDCO-ETA-SCAD-MASC-DFLT
    private WdcoEtaScadMascDflt wdcoEtaScadMascDflt = new WdcoEtaScadMascDflt();
    //Original name: WDCO-ETA-SCAD-FEMM-DFLT
    private WdcoEtaScadFemmDflt wdcoEtaScadFemmDflt = new WdcoEtaScadFemmDflt();
    //Original name: WDCO-TP-DFLT-DUR
    private String wdcoTpDfltDur = DefaultValues.stringVal(Len.WDCO_TP_DFLT_DUR);
    //Original name: WDCO-DUR-AA-ADES-DFLT
    private WdcoDurAaAdesDflt wdcoDurAaAdesDflt = new WdcoDurAaAdesDflt();
    //Original name: WDCO-DUR-MM-ADES-DFLT
    private WdcoDurMmAdesDflt wdcoDurMmAdesDflt = new WdcoDurMmAdesDflt();
    //Original name: WDCO-DUR-GG-ADES-DFLT
    private WdcoDurGgAdesDflt wdcoDurGgAdesDflt = new WdcoDurGgAdesDflt();
    //Original name: WDCO-DT-SCAD-ADES-DFLT
    private WdcoDtScadAdesDflt wdcoDtScadAdesDflt = new WdcoDtScadAdesDflt();
    //Original name: WDCO-COD-FND-DFLT
    private String wdcoCodFndDflt = DefaultValues.stringVal(Len.WDCO_COD_FND_DFLT);
    //Original name: WDCO-TP-DUR
    private String wdcoTpDur = DefaultValues.stringVal(Len.WDCO_TP_DUR);
    //Original name: WDCO-TP-CALC-DUR
    private String wdcoTpCalcDur = DefaultValues.stringVal(Len.WDCO_TP_CALC_DUR);
    //Original name: WDCO-FL-NO-ADERENTI
    private char wdcoFlNoAderenti = DefaultValues.CHAR_VAL;
    //Original name: WDCO-FL-DISTINTA-CNBTVA
    private char wdcoFlDistintaCnbtva = DefaultValues.CHAR_VAL;
    //Original name: WDCO-FL-CNBT-AUTES
    private char wdcoFlCnbtAutes = DefaultValues.CHAR_VAL;
    //Original name: WDCO-FL-QTZ-POST-EMIS
    private char wdcoFlQtzPostEmis = DefaultValues.CHAR_VAL;
    //Original name: WDCO-FL-COMNZ-FND-IS
    private char wdcoFlComnzFndIs = DefaultValues.CHAR_VAL;
    //Original name: WDCO-DS-RIGA
    private long wdcoDsRiga = DefaultValues.LONG_VAL;
    //Original name: WDCO-DS-OPER-SQL
    private char wdcoDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WDCO-DS-VER
    private int wdcoDsVer = DefaultValues.INT_VAL;
    //Original name: WDCO-DS-TS-INI-CPTZ
    private long wdcoDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WDCO-DS-TS-END-CPTZ
    private long wdcoDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WDCO-DS-UTENTE
    private String wdcoDsUtente = DefaultValues.stringVal(Len.WDCO_DS_UTENTE);
    //Original name: WDCO-DS-STATO-ELAB
    private char wdcoDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wdcoIdDColl = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDCO_ID_D_COLL, 0);
        position += Len.WDCO_ID_D_COLL;
        wdcoIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDCO_ID_POLI, 0);
        position += Len.WDCO_ID_POLI;
        wdcoIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDCO_ID_MOVI_CRZ, 0);
        position += Len.WDCO_ID_MOVI_CRZ;
        wdcoIdMoviChiu.setWdcoIdMoviChiuFromBuffer(buffer, position);
        position += WdcoIdMoviChiu.Len.WDCO_ID_MOVI_CHIU;
        wdcoDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDCO_DT_INI_EFF, 0);
        position += Len.WDCO_DT_INI_EFF;
        wdcoDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDCO_DT_END_EFF, 0);
        position += Len.WDCO_DT_END_EFF;
        wdcoCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDCO_COD_COMP_ANIA, 0);
        position += Len.WDCO_COD_COMP_ANIA;
        wdcoImpArrotPre.setWdcoImpArrotPreFromBuffer(buffer, position);
        position += WdcoImpArrotPre.Len.WDCO_IMP_ARROT_PRE;
        wdcoPcScon.setWdcoPcSconFromBuffer(buffer, position);
        position += WdcoPcScon.Len.WDCO_PC_SCON;
        wdcoFlAdesSing = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdcoTpImp = MarshalByte.readString(buffer, position, Len.WDCO_TP_IMP);
        position += Len.WDCO_TP_IMP;
        wdcoFlRiclPreDaCpt = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdcoTpAdes = MarshalByte.readString(buffer, position, Len.WDCO_TP_ADES);
        position += Len.WDCO_TP_ADES;
        wdcoDtUltRinnTac.setWdcoDtUltRinnTacFromBuffer(buffer, position);
        position += WdcoDtUltRinnTac.Len.WDCO_DT_ULT_RINN_TAC;
        wdcoImpScon.setWdcoImpSconFromBuffer(buffer, position);
        position += WdcoImpScon.Len.WDCO_IMP_SCON;
        wdcoFrazDflt.setWdcoFrazDfltFromBuffer(buffer, position);
        position += WdcoFrazDflt.Len.WDCO_FRAZ_DFLT;
        wdcoEtaScadMascDflt.setWdcoEtaScadMascDfltFromBuffer(buffer, position);
        position += WdcoEtaScadMascDflt.Len.WDCO_ETA_SCAD_MASC_DFLT;
        wdcoEtaScadFemmDflt.setWdcoEtaScadFemmDfltFromBuffer(buffer, position);
        position += WdcoEtaScadFemmDflt.Len.WDCO_ETA_SCAD_FEMM_DFLT;
        wdcoTpDfltDur = MarshalByte.readString(buffer, position, Len.WDCO_TP_DFLT_DUR);
        position += Len.WDCO_TP_DFLT_DUR;
        wdcoDurAaAdesDflt.setWdcoDurAaAdesDfltFromBuffer(buffer, position);
        position += WdcoDurAaAdesDflt.Len.WDCO_DUR_AA_ADES_DFLT;
        wdcoDurMmAdesDflt.setWdcoDurMmAdesDfltFromBuffer(buffer, position);
        position += WdcoDurMmAdesDflt.Len.WDCO_DUR_MM_ADES_DFLT;
        wdcoDurGgAdesDflt.setWdcoDurGgAdesDfltFromBuffer(buffer, position);
        position += WdcoDurGgAdesDflt.Len.WDCO_DUR_GG_ADES_DFLT;
        wdcoDtScadAdesDflt.setWdcoDtScadAdesDfltFromBuffer(buffer, position);
        position += WdcoDtScadAdesDflt.Len.WDCO_DT_SCAD_ADES_DFLT;
        wdcoCodFndDflt = MarshalByte.readString(buffer, position, Len.WDCO_COD_FND_DFLT);
        position += Len.WDCO_COD_FND_DFLT;
        wdcoTpDur = MarshalByte.readString(buffer, position, Len.WDCO_TP_DUR);
        position += Len.WDCO_TP_DUR;
        wdcoTpCalcDur = MarshalByte.readString(buffer, position, Len.WDCO_TP_CALC_DUR);
        position += Len.WDCO_TP_CALC_DUR;
        wdcoFlNoAderenti = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdcoFlDistintaCnbtva = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdcoFlCnbtAutes = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdcoFlQtzPostEmis = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdcoFlComnzFndIs = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdcoDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDCO_DS_RIGA, 0);
        position += Len.WDCO_DS_RIGA;
        wdcoDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdcoDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDCO_DS_VER, 0);
        position += Len.WDCO_DS_VER;
        wdcoDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDCO_DS_TS_INI_CPTZ, 0);
        position += Len.WDCO_DS_TS_INI_CPTZ;
        wdcoDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDCO_DS_TS_END_CPTZ, 0);
        position += Len.WDCO_DS_TS_END_CPTZ;
        wdcoDsUtente = MarshalByte.readString(buffer, position, Len.WDCO_DS_UTENTE);
        position += Len.WDCO_DS_UTENTE;
        wdcoDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wdcoIdDColl, Len.Int.WDCO_ID_D_COLL, 0);
        position += Len.WDCO_ID_D_COLL;
        MarshalByte.writeIntAsPacked(buffer, position, wdcoIdPoli, Len.Int.WDCO_ID_POLI, 0);
        position += Len.WDCO_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wdcoIdMoviCrz, Len.Int.WDCO_ID_MOVI_CRZ, 0);
        position += Len.WDCO_ID_MOVI_CRZ;
        wdcoIdMoviChiu.getWdcoIdMoviChiuAsBuffer(buffer, position);
        position += WdcoIdMoviChiu.Len.WDCO_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wdcoDtIniEff, Len.Int.WDCO_DT_INI_EFF, 0);
        position += Len.WDCO_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wdcoDtEndEff, Len.Int.WDCO_DT_END_EFF, 0);
        position += Len.WDCO_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wdcoCodCompAnia, Len.Int.WDCO_COD_COMP_ANIA, 0);
        position += Len.WDCO_COD_COMP_ANIA;
        wdcoImpArrotPre.getWdcoImpArrotPreAsBuffer(buffer, position);
        position += WdcoImpArrotPre.Len.WDCO_IMP_ARROT_PRE;
        wdcoPcScon.getWdcoPcSconAsBuffer(buffer, position);
        position += WdcoPcScon.Len.WDCO_PC_SCON;
        MarshalByte.writeChar(buffer, position, wdcoFlAdesSing);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wdcoTpImp, Len.WDCO_TP_IMP);
        position += Len.WDCO_TP_IMP;
        MarshalByte.writeChar(buffer, position, wdcoFlRiclPreDaCpt);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wdcoTpAdes, Len.WDCO_TP_ADES);
        position += Len.WDCO_TP_ADES;
        wdcoDtUltRinnTac.getWdcoDtUltRinnTacAsBuffer(buffer, position);
        position += WdcoDtUltRinnTac.Len.WDCO_DT_ULT_RINN_TAC;
        wdcoImpScon.getWdcoImpSconAsBuffer(buffer, position);
        position += WdcoImpScon.Len.WDCO_IMP_SCON;
        wdcoFrazDflt.getWdcoFrazDfltAsBuffer(buffer, position);
        position += WdcoFrazDflt.Len.WDCO_FRAZ_DFLT;
        wdcoEtaScadMascDflt.getWdcoEtaScadMascDfltAsBuffer(buffer, position);
        position += WdcoEtaScadMascDflt.Len.WDCO_ETA_SCAD_MASC_DFLT;
        wdcoEtaScadFemmDflt.getWdcoEtaScadFemmDfltAsBuffer(buffer, position);
        position += WdcoEtaScadFemmDflt.Len.WDCO_ETA_SCAD_FEMM_DFLT;
        MarshalByte.writeString(buffer, position, wdcoTpDfltDur, Len.WDCO_TP_DFLT_DUR);
        position += Len.WDCO_TP_DFLT_DUR;
        wdcoDurAaAdesDflt.getWdcoDurAaAdesDfltAsBuffer(buffer, position);
        position += WdcoDurAaAdesDflt.Len.WDCO_DUR_AA_ADES_DFLT;
        wdcoDurMmAdesDflt.getWdcoDurMmAdesDfltAsBuffer(buffer, position);
        position += WdcoDurMmAdesDflt.Len.WDCO_DUR_MM_ADES_DFLT;
        wdcoDurGgAdesDflt.getWdcoDurGgAdesDfltAsBuffer(buffer, position);
        position += WdcoDurGgAdesDflt.Len.WDCO_DUR_GG_ADES_DFLT;
        wdcoDtScadAdesDflt.getWdcoDtScadAdesDfltAsBuffer(buffer, position);
        position += WdcoDtScadAdesDflt.Len.WDCO_DT_SCAD_ADES_DFLT;
        MarshalByte.writeString(buffer, position, wdcoCodFndDflt, Len.WDCO_COD_FND_DFLT);
        position += Len.WDCO_COD_FND_DFLT;
        MarshalByte.writeString(buffer, position, wdcoTpDur, Len.WDCO_TP_DUR);
        position += Len.WDCO_TP_DUR;
        MarshalByte.writeString(buffer, position, wdcoTpCalcDur, Len.WDCO_TP_CALC_DUR);
        position += Len.WDCO_TP_CALC_DUR;
        MarshalByte.writeChar(buffer, position, wdcoFlNoAderenti);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wdcoFlDistintaCnbtva);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wdcoFlCnbtAutes);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wdcoFlQtzPostEmis);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wdcoFlComnzFndIs);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, wdcoDsRiga, Len.Int.WDCO_DS_RIGA, 0);
        position += Len.WDCO_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wdcoDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wdcoDsVer, Len.Int.WDCO_DS_VER, 0);
        position += Len.WDCO_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wdcoDsTsIniCptz, Len.Int.WDCO_DS_TS_INI_CPTZ, 0);
        position += Len.WDCO_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wdcoDsTsEndCptz, Len.Int.WDCO_DS_TS_END_CPTZ, 0);
        position += Len.WDCO_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wdcoDsUtente, Len.WDCO_DS_UTENTE);
        position += Len.WDCO_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wdcoDsStatoElab);
        return buffer;
    }

    public void setWdcoIdDColl(int wdcoIdDColl) {
        this.wdcoIdDColl = wdcoIdDColl;
    }

    public int getWdcoIdDColl() {
        return this.wdcoIdDColl;
    }

    public void setWdcoIdPoli(int wdcoIdPoli) {
        this.wdcoIdPoli = wdcoIdPoli;
    }

    public int getWdcoIdPoli() {
        return this.wdcoIdPoli;
    }

    public void setWdcoIdMoviCrz(int wdcoIdMoviCrz) {
        this.wdcoIdMoviCrz = wdcoIdMoviCrz;
    }

    public int getWdcoIdMoviCrz() {
        return this.wdcoIdMoviCrz;
    }

    public void setWdcoDtIniEff(int wdcoDtIniEff) {
        this.wdcoDtIniEff = wdcoDtIniEff;
    }

    public int getWdcoDtIniEff() {
        return this.wdcoDtIniEff;
    }

    public void setWdcoDtEndEff(int wdcoDtEndEff) {
        this.wdcoDtEndEff = wdcoDtEndEff;
    }

    public int getWdcoDtEndEff() {
        return this.wdcoDtEndEff;
    }

    public void setWdcoCodCompAnia(int wdcoCodCompAnia) {
        this.wdcoCodCompAnia = wdcoCodCompAnia;
    }

    public int getWdcoCodCompAnia() {
        return this.wdcoCodCompAnia;
    }

    public void setWdcoFlAdesSing(char wdcoFlAdesSing) {
        this.wdcoFlAdesSing = wdcoFlAdesSing;
    }

    public char getWdcoFlAdesSing() {
        return this.wdcoFlAdesSing;
    }

    public void setWdcoTpImp(String wdcoTpImp) {
        this.wdcoTpImp = Functions.subString(wdcoTpImp, Len.WDCO_TP_IMP);
    }

    public String getWdcoTpImp() {
        return this.wdcoTpImp;
    }

    public void setWdcoFlRiclPreDaCpt(char wdcoFlRiclPreDaCpt) {
        this.wdcoFlRiclPreDaCpt = wdcoFlRiclPreDaCpt;
    }

    public char getWdcoFlRiclPreDaCpt() {
        return this.wdcoFlRiclPreDaCpt;
    }

    public void setWdcoTpAdes(String wdcoTpAdes) {
        this.wdcoTpAdes = Functions.subString(wdcoTpAdes, Len.WDCO_TP_ADES);
    }

    public String getWdcoTpAdes() {
        return this.wdcoTpAdes;
    }

    public void setWdcoTpDfltDur(String wdcoTpDfltDur) {
        this.wdcoTpDfltDur = Functions.subString(wdcoTpDfltDur, Len.WDCO_TP_DFLT_DUR);
    }

    public String getWdcoTpDfltDur() {
        return this.wdcoTpDfltDur;
    }

    public void setWdcoCodFndDflt(String wdcoCodFndDflt) {
        this.wdcoCodFndDflt = Functions.subString(wdcoCodFndDflt, Len.WDCO_COD_FND_DFLT);
    }

    public String getWdcoCodFndDflt() {
        return this.wdcoCodFndDflt;
    }

    public void setWdcoTpDur(String wdcoTpDur) {
        this.wdcoTpDur = Functions.subString(wdcoTpDur, Len.WDCO_TP_DUR);
    }

    public String getWdcoTpDur() {
        return this.wdcoTpDur;
    }

    public void setWdcoTpCalcDur(String wdcoTpCalcDur) {
        this.wdcoTpCalcDur = Functions.subString(wdcoTpCalcDur, Len.WDCO_TP_CALC_DUR);
    }

    public String getWdcoTpCalcDur() {
        return this.wdcoTpCalcDur;
    }

    public void setWdcoFlNoAderenti(char wdcoFlNoAderenti) {
        this.wdcoFlNoAderenti = wdcoFlNoAderenti;
    }

    public char getWdcoFlNoAderenti() {
        return this.wdcoFlNoAderenti;
    }

    public void setWdcoFlDistintaCnbtva(char wdcoFlDistintaCnbtva) {
        this.wdcoFlDistintaCnbtva = wdcoFlDistintaCnbtva;
    }

    public char getWdcoFlDistintaCnbtva() {
        return this.wdcoFlDistintaCnbtva;
    }

    public void setWdcoFlCnbtAutes(char wdcoFlCnbtAutes) {
        this.wdcoFlCnbtAutes = wdcoFlCnbtAutes;
    }

    public char getWdcoFlCnbtAutes() {
        return this.wdcoFlCnbtAutes;
    }

    public void setWdcoFlQtzPostEmis(char wdcoFlQtzPostEmis) {
        this.wdcoFlQtzPostEmis = wdcoFlQtzPostEmis;
    }

    public char getWdcoFlQtzPostEmis() {
        return this.wdcoFlQtzPostEmis;
    }

    public void setWdcoFlComnzFndIs(char wdcoFlComnzFndIs) {
        this.wdcoFlComnzFndIs = wdcoFlComnzFndIs;
    }

    public char getWdcoFlComnzFndIs() {
        return this.wdcoFlComnzFndIs;
    }

    public void setWdcoDsRiga(long wdcoDsRiga) {
        this.wdcoDsRiga = wdcoDsRiga;
    }

    public long getWdcoDsRiga() {
        return this.wdcoDsRiga;
    }

    public void setWdcoDsOperSql(char wdcoDsOperSql) {
        this.wdcoDsOperSql = wdcoDsOperSql;
    }

    public char getWdcoDsOperSql() {
        return this.wdcoDsOperSql;
    }

    public void setWdcoDsVer(int wdcoDsVer) {
        this.wdcoDsVer = wdcoDsVer;
    }

    public int getWdcoDsVer() {
        return this.wdcoDsVer;
    }

    public void setWdcoDsTsIniCptz(long wdcoDsTsIniCptz) {
        this.wdcoDsTsIniCptz = wdcoDsTsIniCptz;
    }

    public long getWdcoDsTsIniCptz() {
        return this.wdcoDsTsIniCptz;
    }

    public void setWdcoDsTsEndCptz(long wdcoDsTsEndCptz) {
        this.wdcoDsTsEndCptz = wdcoDsTsEndCptz;
    }

    public long getWdcoDsTsEndCptz() {
        return this.wdcoDsTsEndCptz;
    }

    public void setWdcoDsUtente(String wdcoDsUtente) {
        this.wdcoDsUtente = Functions.subString(wdcoDsUtente, Len.WDCO_DS_UTENTE);
    }

    public String getWdcoDsUtente() {
        return this.wdcoDsUtente;
    }

    public void setWdcoDsStatoElab(char wdcoDsStatoElab) {
        this.wdcoDsStatoElab = wdcoDsStatoElab;
    }

    public char getWdcoDsStatoElab() {
        return this.wdcoDsStatoElab;
    }

    public WdcoDtScadAdesDflt getWdcoDtScadAdesDflt() {
        return wdcoDtScadAdesDflt;
    }

    public WdcoDtUltRinnTac getWdcoDtUltRinnTac() {
        return wdcoDtUltRinnTac;
    }

    public WdcoDurAaAdesDflt getWdcoDurAaAdesDflt() {
        return wdcoDurAaAdesDflt;
    }

    public WdcoDurGgAdesDflt getWdcoDurGgAdesDflt() {
        return wdcoDurGgAdesDflt;
    }

    public WdcoDurMmAdesDflt getWdcoDurMmAdesDflt() {
        return wdcoDurMmAdesDflt;
    }

    public WdcoEtaScadFemmDflt getWdcoEtaScadFemmDflt() {
        return wdcoEtaScadFemmDflt;
    }

    public WdcoEtaScadMascDflt getWdcoEtaScadMascDflt() {
        return wdcoEtaScadMascDflt;
    }

    public WdcoFrazDflt getWdcoFrazDflt() {
        return wdcoFrazDflt;
    }

    public WdcoIdMoviChiu getWdcoIdMoviChiu() {
        return wdcoIdMoviChiu;
    }

    public WdcoImpArrotPre getWdcoImpArrotPre() {
        return wdcoImpArrotPre;
    }

    public WdcoImpScon getWdcoImpScon() {
        return wdcoImpScon;
    }

    public WdcoPcScon getWdcoPcScon() {
        return wdcoPcScon;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_ID_D_COLL = 5;
        public static final int WDCO_ID_POLI = 5;
        public static final int WDCO_ID_MOVI_CRZ = 5;
        public static final int WDCO_DT_INI_EFF = 5;
        public static final int WDCO_DT_END_EFF = 5;
        public static final int WDCO_COD_COMP_ANIA = 3;
        public static final int WDCO_FL_ADES_SING = 1;
        public static final int WDCO_TP_IMP = 2;
        public static final int WDCO_FL_RICL_PRE_DA_CPT = 1;
        public static final int WDCO_TP_ADES = 2;
        public static final int WDCO_TP_DFLT_DUR = 2;
        public static final int WDCO_COD_FND_DFLT = 12;
        public static final int WDCO_TP_DUR = 2;
        public static final int WDCO_TP_CALC_DUR = 12;
        public static final int WDCO_FL_NO_ADERENTI = 1;
        public static final int WDCO_FL_DISTINTA_CNBTVA = 1;
        public static final int WDCO_FL_CNBT_AUTES = 1;
        public static final int WDCO_FL_QTZ_POST_EMIS = 1;
        public static final int WDCO_FL_COMNZ_FND_IS = 1;
        public static final int WDCO_DS_RIGA = 6;
        public static final int WDCO_DS_OPER_SQL = 1;
        public static final int WDCO_DS_VER = 5;
        public static final int WDCO_DS_TS_INI_CPTZ = 10;
        public static final int WDCO_DS_TS_END_CPTZ = 10;
        public static final int WDCO_DS_UTENTE = 20;
        public static final int WDCO_DS_STATO_ELAB = 1;
        public static final int DATI = WDCO_ID_D_COLL + WDCO_ID_POLI + WDCO_ID_MOVI_CRZ + WdcoIdMoviChiu.Len.WDCO_ID_MOVI_CHIU + WDCO_DT_INI_EFF + WDCO_DT_END_EFF + WDCO_COD_COMP_ANIA + WdcoImpArrotPre.Len.WDCO_IMP_ARROT_PRE + WdcoPcScon.Len.WDCO_PC_SCON + WDCO_FL_ADES_SING + WDCO_TP_IMP + WDCO_FL_RICL_PRE_DA_CPT + WDCO_TP_ADES + WdcoDtUltRinnTac.Len.WDCO_DT_ULT_RINN_TAC + WdcoImpScon.Len.WDCO_IMP_SCON + WdcoFrazDflt.Len.WDCO_FRAZ_DFLT + WdcoEtaScadMascDflt.Len.WDCO_ETA_SCAD_MASC_DFLT + WdcoEtaScadFemmDflt.Len.WDCO_ETA_SCAD_FEMM_DFLT + WDCO_TP_DFLT_DUR + WdcoDurAaAdesDflt.Len.WDCO_DUR_AA_ADES_DFLT + WdcoDurMmAdesDflt.Len.WDCO_DUR_MM_ADES_DFLT + WdcoDurGgAdesDflt.Len.WDCO_DUR_GG_ADES_DFLT + WdcoDtScadAdesDflt.Len.WDCO_DT_SCAD_ADES_DFLT + WDCO_COD_FND_DFLT + WDCO_TP_DUR + WDCO_TP_CALC_DUR + WDCO_FL_NO_ADERENTI + WDCO_FL_DISTINTA_CNBTVA + WDCO_FL_CNBT_AUTES + WDCO_FL_QTZ_POST_EMIS + WDCO_FL_COMNZ_FND_IS + WDCO_DS_RIGA + WDCO_DS_OPER_SQL + WDCO_DS_VER + WDCO_DS_TS_INI_CPTZ + WDCO_DS_TS_END_CPTZ + WDCO_DS_UTENTE + WDCO_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_ID_D_COLL = 9;
            public static final int WDCO_ID_POLI = 9;
            public static final int WDCO_ID_MOVI_CRZ = 9;
            public static final int WDCO_DT_INI_EFF = 8;
            public static final int WDCO_DT_END_EFF = 8;
            public static final int WDCO_COD_COMP_ANIA = 5;
            public static final int WDCO_DS_RIGA = 10;
            public static final int WDCO_DS_VER = 9;
            public static final int WDCO_DS_TS_INI_CPTZ = 18;
            public static final int WDCO_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
