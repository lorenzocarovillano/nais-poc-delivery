package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-LOG-ERRORE<br>
 * Variable: WK-LOG-ERRORE from copybook IABV0007<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkLogErrore {

    //==== PROPERTIES ====
    //Original name: WK-LOR-ID-LOG-ERRORE
    private int idLogErrore = DefaultValues.INT_VAL;
    //Original name: WK-LOR-ID-GRAVITA-ERRORE
    private int idGravitaErrore = DefaultValues.INT_VAL;
    //Original name: WK-LOR-DESC-ERRORE-ESTESA
    private String descErroreEstesa = DefaultValues.stringVal(Len.DESC_ERRORE_ESTESA);
    //Original name: WK-LOR-COD-MAIN-BATCH
    private String codMainBatch = DefaultValues.stringVal(Len.COD_MAIN_BATCH);
    //Original name: WK-LOR-COD-SERVIZIO-BE
    private String codServizioBe = DefaultValues.stringVal(Len.COD_SERVIZIO_BE);
    //Original name: WK-LOR-LABEL-ERR
    private String labelErr = DefaultValues.stringVal(Len.LABEL_ERR);
    //Original name: WK-LOR-OPER-TABELLA
    private String operTabella = DefaultValues.stringVal(Len.OPER_TABELLA);
    //Original name: WK-LOR-NOME-TABELLA
    private String nomeTabella = DefaultValues.stringVal(Len.NOME_TABELLA);
    //Original name: WK-LOR-STATUS-TABELLA
    private String statusTabella = DefaultValues.stringVal(Len.STATUS_TABELLA);
    //Original name: WK-LOR-KEY-TABELLA
    private String keyTabella = DefaultValues.stringVal(Len.KEY_TABELLA);

    //==== METHODS ====
    public void initWkLogErroreHighValues() {
        idLogErrore = Types.HIGH_INT_VAL;
        idGravitaErrore = Types.HIGH_INT_VAL;
        descErroreEstesa = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.DESC_ERRORE_ESTESA);
        codMainBatch = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.COD_MAIN_BATCH);
        codServizioBe = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.COD_SERVIZIO_BE);
        labelErr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LABEL_ERR);
        operTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.OPER_TABELLA);
        nomeTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NOME_TABELLA);
        statusTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.STATUS_TABELLA);
        keyTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.KEY_TABELLA);
    }

    public void setIdLogErrore(int idLogErrore) {
        this.idLogErrore = idLogErrore;
    }

    public int getIdLogErrore() {
        return this.idLogErrore;
    }

    public void setIdGravitaErrore(int idGravitaErrore) {
        this.idGravitaErrore = idGravitaErrore;
    }

    public int getIdGravitaErrore() {
        return this.idGravitaErrore;
    }

    public void setDescErroreEstesa(String descErroreEstesa) {
        this.descErroreEstesa = Functions.subString(descErroreEstesa, Len.DESC_ERRORE_ESTESA);
    }

    public String getDescErroreEstesa() {
        return this.descErroreEstesa;
    }

    public String getDescErroreEstesaFormatted() {
        return Functions.padBlanks(getDescErroreEstesa(), Len.DESC_ERRORE_ESTESA);
    }

    public void setCodMainBatch(String codMainBatch) {
        this.codMainBatch = Functions.subString(codMainBatch, Len.COD_MAIN_BATCH);
    }

    public String getCodMainBatch() {
        return this.codMainBatch;
    }

    public void setCodServizioBe(String codServizioBe) {
        this.codServizioBe = Functions.subString(codServizioBe, Len.COD_SERVIZIO_BE);
    }

    public String getCodServizioBe() {
        return this.codServizioBe;
    }

    public void setLabelErr(String labelErr) {
        this.labelErr = Functions.subString(labelErr, Len.LABEL_ERR);
    }

    public String getLabelErr() {
        return this.labelErr;
    }

    public void setOperTabella(String operTabella) {
        this.operTabella = Functions.subString(operTabella, Len.OPER_TABELLA);
    }

    public String getOperTabella() {
        return this.operTabella;
    }

    public void setNomeTabella(String nomeTabella) {
        this.nomeTabella = Functions.subString(nomeTabella, Len.NOME_TABELLA);
    }

    public String getNomeTabella() {
        return this.nomeTabella;
    }

    public void setStatusTabella(String statusTabella) {
        this.statusTabella = Functions.subString(statusTabella, Len.STATUS_TABELLA);
    }

    public String getStatusTabella() {
        return this.statusTabella;
    }

    public void setKeyTabella(String keyTabella) {
        this.keyTabella = Functions.subString(keyTabella, Len.KEY_TABELLA);
    }

    public String getKeyTabella() {
        return this.keyTabella;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESC_ERRORE_ESTESA = 200;
        public static final int COD_MAIN_BATCH = 8;
        public static final int COD_SERVIZIO_BE = 8;
        public static final int LABEL_ERR = 30;
        public static final int OPER_TABELLA = 2;
        public static final int NOME_TABELLA = 18;
        public static final int STATUS_TABELLA = 10;
        public static final int KEY_TABELLA = 100;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
