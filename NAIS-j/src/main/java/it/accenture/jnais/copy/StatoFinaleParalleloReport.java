package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: STATO-FINALE-PARALLELO-REPORT<br>
 * Variable: STATO-FINALE-PARALLELO-REPORT from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class StatoFinaleParalleloReport {

    //==== PROPERTIES ====
    //Original name: FILLER-STATO-FINALE-PARALLELO-REPORT
    private String flr1 = "STATO FINALE PARALLELO";
    //Original name: FILLER-STATO-FINALE-PARALLELO-REPORT-1
    private String flr2 = " :";
    //Original name: REP-STATO-FINALE-P
    private String repStatoFinaleP = "";

    //==== METHODS ====
    public String getStatoFinaleParalleloReportFormatted() {
        return MarshalByteExt.bufferToStr(getStatoFinaleParalleloReportBytes());
    }

    public byte[] getStatoFinaleParalleloReportBytes() {
        byte[] buffer = new byte[Len.STATO_FINALE_PARALLELO_REPORT];
        return getStatoFinaleParalleloReportBytes(buffer, 1);
    }

    public byte[] getStatoFinaleParalleloReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, repStatoFinaleP, Len.REP_STATO_FINALE_P);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public void setRepStatoFinaleP(String repStatoFinaleP) {
        this.repStatoFinaleP = Functions.subString(repStatoFinaleP, Len.REP_STATO_FINALE_P);
    }

    public String getRepStatoFinaleP() {
        return this.repStatoFinaleP;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 50;
        public static final int FLR2 = 4;
        public static final int REP_STATO_FINALE_P = 50;
        public static final int STATO_FINALE_PARALLELO_REPORT = REP_STATO_FINALE_P + FLR1 + FLR2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
