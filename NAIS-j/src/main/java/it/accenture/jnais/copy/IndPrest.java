package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-PREST<br>
 * Variable: IND-PREST from copybook IDBVPRE2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndPrest {

    //==== PROPERTIES ====
    //Original name: IND-PRE-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-DT-CONCS-PREST
    private short dtConcsPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-DT-DECOR-PREST
    private short dtDecorPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-IMP-PREST
    private short impPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-INTR-PREST
    private short intrPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-TP-PREST
    private short tpPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-FRAZ-PAG-INTR
    private short frazPagIntr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-DT-RIMB
    private short dtRimb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-IMP-RIMB
    private short impRimb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-COD-DVS
    private short codDvs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-MOD-INTR-PREST
    private short modIntrPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-SPE-PREST
    private short spePrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-IMP-PREST-LIQTO
    private short impPrestLiqto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-SDO-INTR
    private short sdoIntr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-RIMB-EFF
    private short rimbEff = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PRE-PREST-RES-EFF
    private short prestResEff = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtConcsPrest(short dtConcsPrest) {
        this.dtConcsPrest = dtConcsPrest;
    }

    public short getDtConcsPrest() {
        return this.dtConcsPrest;
    }

    public void setDtDecorPrest(short dtDecorPrest) {
        this.dtDecorPrest = dtDecorPrest;
    }

    public short getDtDecorPrest() {
        return this.dtDecorPrest;
    }

    public void setImpPrest(short impPrest) {
        this.impPrest = impPrest;
    }

    public short getImpPrest() {
        return this.impPrest;
    }

    public void setIntrPrest(short intrPrest) {
        this.intrPrest = intrPrest;
    }

    public short getIntrPrest() {
        return this.intrPrest;
    }

    public void setTpPrest(short tpPrest) {
        this.tpPrest = tpPrest;
    }

    public short getTpPrest() {
        return this.tpPrest;
    }

    public void setFrazPagIntr(short frazPagIntr) {
        this.frazPagIntr = frazPagIntr;
    }

    public short getFrazPagIntr() {
        return this.frazPagIntr;
    }

    public void setDtRimb(short dtRimb) {
        this.dtRimb = dtRimb;
    }

    public short getDtRimb() {
        return this.dtRimb;
    }

    public void setImpRimb(short impRimb) {
        this.impRimb = impRimb;
    }

    public short getImpRimb() {
        return this.impRimb;
    }

    public void setCodDvs(short codDvs) {
        this.codDvs = codDvs;
    }

    public short getCodDvs() {
        return this.codDvs;
    }

    public void setModIntrPrest(short modIntrPrest) {
        this.modIntrPrest = modIntrPrest;
    }

    public short getModIntrPrest() {
        return this.modIntrPrest;
    }

    public void setSpePrest(short spePrest) {
        this.spePrest = spePrest;
    }

    public short getSpePrest() {
        return this.spePrest;
    }

    public void setImpPrestLiqto(short impPrestLiqto) {
        this.impPrestLiqto = impPrestLiqto;
    }

    public short getImpPrestLiqto() {
        return this.impPrestLiqto;
    }

    public void setSdoIntr(short sdoIntr) {
        this.sdoIntr = sdoIntr;
    }

    public short getSdoIntr() {
        return this.sdoIntr;
    }

    public void setRimbEff(short rimbEff) {
        this.rimbEff = rimbEff;
    }

    public short getRimbEff() {
        return this.rimbEff;
    }

    public void setPrestResEff(short prestResEff) {
        this.prestResEff = prestResEff;
    }

    public short getPrestResEff() {
        return this.prestResEff;
    }
}
