package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-D-CRIST<br>
 * Variable: IND-D-CRIST from copybook IDBVP612<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndDCrist {

    //==== PROPERTIES ====
    //Original name: IND-P61-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-RIS-MAT-31122011
    private short risMat31122011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-PRE-V-31122011
    private short preV31122011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-PRE-RSH-V-31122011
    private short preRshV31122011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-CPT-RIVTO-31122011
    private short cptRivto31122011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-IMPB-VIS-31122011
    private short impbVis31122011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-IMPB-IS-31122011
    private short impbIs31122011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-IMPB-VIS-RP-P2011
    private short impbVisRpP2011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-IMPB-IS-RP-P2011
    private short impbIsRpP2011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-PRE-V-30062014
    private short preV30062014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-PRE-RSH-V-30062014
    private short preRshV30062014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-CPT-INI-30062014
    private short cptIni30062014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-IMPB-VIS-30062014
    private short impbVis30062014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-IMPB-IS-30062014
    private short impbIs30062014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-IMPB-VIS-RP-P62014
    private short impbVisRpP62014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-IMPB-IS-RP-P62014
    private short impbIsRpP62014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-RIS-MAT-30062014
    private short risMat30062014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-ID-ADES
    private short idAdes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-MONT-LRD-END2000
    private short montLrdEnd2000 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-PRE-LRD-END2000
    private short preLrdEnd2000 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-RENDTO-LRD-END2000
    private short rendtoLrdEnd2000 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-MONT-LRD-END2006
    private short montLrdEnd2006 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-PRE-LRD-END2006
    private short preLrdEnd2006 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-RENDTO-LRD-END2006
    private short rendtoLrdEnd2006 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-MONT-LRD-DAL2007
    private short montLrdDal2007 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-PRE-LRD-DAL2007
    private short preLrdDal2007 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-RENDTO-LRD-DAL2007
    private short rendtoLrdDal2007 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P61-ID-TRCH-DI-GAR
    private short idTrchDiGar = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setRisMat31122011(short risMat31122011) {
        this.risMat31122011 = risMat31122011;
    }

    public short getRisMat31122011() {
        return this.risMat31122011;
    }

    public void setPreV31122011(short preV31122011) {
        this.preV31122011 = preV31122011;
    }

    public short getPreV31122011() {
        return this.preV31122011;
    }

    public void setPreRshV31122011(short preRshV31122011) {
        this.preRshV31122011 = preRshV31122011;
    }

    public short getPreRshV31122011() {
        return this.preRshV31122011;
    }

    public void setCptRivto31122011(short cptRivto31122011) {
        this.cptRivto31122011 = cptRivto31122011;
    }

    public short getCptRivto31122011() {
        return this.cptRivto31122011;
    }

    public void setImpbVis31122011(short impbVis31122011) {
        this.impbVis31122011 = impbVis31122011;
    }

    public short getImpbVis31122011() {
        return this.impbVis31122011;
    }

    public void setImpbIs31122011(short impbIs31122011) {
        this.impbIs31122011 = impbIs31122011;
    }

    public short getImpbIs31122011() {
        return this.impbIs31122011;
    }

    public void setImpbVisRpP2011(short impbVisRpP2011) {
        this.impbVisRpP2011 = impbVisRpP2011;
    }

    public short getImpbVisRpP2011() {
        return this.impbVisRpP2011;
    }

    public void setImpbIsRpP2011(short impbIsRpP2011) {
        this.impbIsRpP2011 = impbIsRpP2011;
    }

    public short getImpbIsRpP2011() {
        return this.impbIsRpP2011;
    }

    public void setPreV30062014(short preV30062014) {
        this.preV30062014 = preV30062014;
    }

    public short getPreV30062014() {
        return this.preV30062014;
    }

    public void setPreRshV30062014(short preRshV30062014) {
        this.preRshV30062014 = preRshV30062014;
    }

    public short getPreRshV30062014() {
        return this.preRshV30062014;
    }

    public void setCptIni30062014(short cptIni30062014) {
        this.cptIni30062014 = cptIni30062014;
    }

    public short getCptIni30062014() {
        return this.cptIni30062014;
    }

    public void setImpbVis30062014(short impbVis30062014) {
        this.impbVis30062014 = impbVis30062014;
    }

    public short getImpbVis30062014() {
        return this.impbVis30062014;
    }

    public void setImpbIs30062014(short impbIs30062014) {
        this.impbIs30062014 = impbIs30062014;
    }

    public short getImpbIs30062014() {
        return this.impbIs30062014;
    }

    public void setImpbVisRpP62014(short impbVisRpP62014) {
        this.impbVisRpP62014 = impbVisRpP62014;
    }

    public short getImpbVisRpP62014() {
        return this.impbVisRpP62014;
    }

    public void setImpbIsRpP62014(short impbIsRpP62014) {
        this.impbIsRpP62014 = impbIsRpP62014;
    }

    public short getImpbIsRpP62014() {
        return this.impbIsRpP62014;
    }

    public void setRisMat30062014(short risMat30062014) {
        this.risMat30062014 = risMat30062014;
    }

    public short getRisMat30062014() {
        return this.risMat30062014;
    }

    public void setIdAdes(short idAdes) {
        this.idAdes = idAdes;
    }

    public short getIdAdes() {
        return this.idAdes;
    }

    public void setMontLrdEnd2000(short montLrdEnd2000) {
        this.montLrdEnd2000 = montLrdEnd2000;
    }

    public short getMontLrdEnd2000() {
        return this.montLrdEnd2000;
    }

    public void setPreLrdEnd2000(short preLrdEnd2000) {
        this.preLrdEnd2000 = preLrdEnd2000;
    }

    public short getPreLrdEnd2000() {
        return this.preLrdEnd2000;
    }

    public void setRendtoLrdEnd2000(short rendtoLrdEnd2000) {
        this.rendtoLrdEnd2000 = rendtoLrdEnd2000;
    }

    public short getRendtoLrdEnd2000() {
        return this.rendtoLrdEnd2000;
    }

    public void setMontLrdEnd2006(short montLrdEnd2006) {
        this.montLrdEnd2006 = montLrdEnd2006;
    }

    public short getMontLrdEnd2006() {
        return this.montLrdEnd2006;
    }

    public void setPreLrdEnd2006(short preLrdEnd2006) {
        this.preLrdEnd2006 = preLrdEnd2006;
    }

    public short getPreLrdEnd2006() {
        return this.preLrdEnd2006;
    }

    public void setRendtoLrdEnd2006(short rendtoLrdEnd2006) {
        this.rendtoLrdEnd2006 = rendtoLrdEnd2006;
    }

    public short getRendtoLrdEnd2006() {
        return this.rendtoLrdEnd2006;
    }

    public void setMontLrdDal2007(short montLrdDal2007) {
        this.montLrdDal2007 = montLrdDal2007;
    }

    public short getMontLrdDal2007() {
        return this.montLrdDal2007;
    }

    public void setPreLrdDal2007(short preLrdDal2007) {
        this.preLrdDal2007 = preLrdDal2007;
    }

    public short getPreLrdDal2007() {
        return this.preLrdDal2007;
    }

    public void setRendtoLrdDal2007(short rendtoLrdDal2007) {
        this.rendtoLrdDal2007 = rendtoLrdDal2007;
    }

    public short getRendtoLrdDal2007() {
        return this.rendtoLrdDal2007;
    }

    public void setIdTrchDiGar(short idTrchDiGar) {
        this.idTrchDiGar = idTrchDiGar;
    }

    public short getIdTrchDiGar() {
        return this.idTrchDiGar;
    }
}
