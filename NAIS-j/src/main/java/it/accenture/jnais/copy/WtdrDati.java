package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WtdrDtApplzMora;
import it.accenture.jnais.ws.redefines.WtdrDtEndCop;
import it.accenture.jnais.ws.redefines.WtdrDtEsiTit;
import it.accenture.jnais.ws.redefines.WtdrDtIniCop;
import it.accenture.jnais.ws.redefines.WtdrFraz;
import it.accenture.jnais.ws.redefines.WtdrIdMoviChiu;
import it.accenture.jnais.ws.redefines.WtdrIdRappAna;
import it.accenture.jnais.ws.redefines.WtdrIdRappRete;
import it.accenture.jnais.ws.redefines.WtdrImpAder;
import it.accenture.jnais.ws.redefines.WtdrImpAz;
import it.accenture.jnais.ws.redefines.WtdrImpPag;
import it.accenture.jnais.ws.redefines.WtdrImpTfr;
import it.accenture.jnais.ws.redefines.WtdrImpTfrStrc;
import it.accenture.jnais.ws.redefines.WtdrImpTrasfe;
import it.accenture.jnais.ws.redefines.WtdrImpVolo;
import it.accenture.jnais.ws.redefines.WtdrProgTit;
import it.accenture.jnais.ws.redefines.WtdrTotAcqExp;
import it.accenture.jnais.ws.redefines.WtdrTotCarAcq;
import it.accenture.jnais.ws.redefines.WtdrTotCarGest;
import it.accenture.jnais.ws.redefines.WtdrTotCarIas;
import it.accenture.jnais.ws.redefines.WtdrTotCarInc;
import it.accenture.jnais.ws.redefines.WtdrTotCnbtAntirac;
import it.accenture.jnais.ws.redefines.WtdrTotCommisInter;
import it.accenture.jnais.ws.redefines.WtdrTotDir;
import it.accenture.jnais.ws.redefines.WtdrTotIntrFraz;
import it.accenture.jnais.ws.redefines.WtdrTotIntrMora;
import it.accenture.jnais.ws.redefines.WtdrTotIntrPrest;
import it.accenture.jnais.ws.redefines.WtdrTotIntrRetdt;
import it.accenture.jnais.ws.redefines.WtdrTotIntrRiat;
import it.accenture.jnais.ws.redefines.WtdrTotManfeeAntic;
import it.accenture.jnais.ws.redefines.WtdrTotManfeeRec;
import it.accenture.jnais.ws.redefines.WtdrTotManfeeRicor;
import it.accenture.jnais.ws.redefines.WtdrTotPreNet;
import it.accenture.jnais.ws.redefines.WtdrTotPrePpIas;
import it.accenture.jnais.ws.redefines.WtdrTotPreSoloRsh;
import it.accenture.jnais.ws.redefines.WtdrTotPreTot;
import it.accenture.jnais.ws.redefines.WtdrTotProvAcq1aa;
import it.accenture.jnais.ws.redefines.WtdrTotProvAcq2aa;
import it.accenture.jnais.ws.redefines.WtdrTotProvDaRec;
import it.accenture.jnais.ws.redefines.WtdrTotProvInc;
import it.accenture.jnais.ws.redefines.WtdrTotProvRicor;
import it.accenture.jnais.ws.redefines.WtdrTotRemunAss;
import it.accenture.jnais.ws.redefines.WtdrTotSoprAlt;
import it.accenture.jnais.ws.redefines.WtdrTotSoprProf;
import it.accenture.jnais.ws.redefines.WtdrTotSoprSan;
import it.accenture.jnais.ws.redefines.WtdrTotSoprSpo;
import it.accenture.jnais.ws.redefines.WtdrTotSoprTec;
import it.accenture.jnais.ws.redefines.WtdrTotSpeAge;
import it.accenture.jnais.ws.redefines.WtdrTotSpeMed;
import it.accenture.jnais.ws.redefines.WtdrTotTax;

/**Original name: WTDR-DATI<br>
 * Variable: WTDR-DATI from copybook LCCVTDR1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WtdrDati {

    //==== PROPERTIES ====
    //Original name: WTDR-ID-TIT-RAT
    private int wtdrIdTitRat = DefaultValues.INT_VAL;
    //Original name: WTDR-ID-OGG
    private int wtdrIdOgg = DefaultValues.INT_VAL;
    //Original name: WTDR-TP-OGG
    private String wtdrTpOgg = DefaultValues.stringVal(Len.WTDR_TP_OGG);
    //Original name: WTDR-ID-MOVI-CRZ
    private int wtdrIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WTDR-ID-MOVI-CHIU
    private WtdrIdMoviChiu wtdrIdMoviChiu = new WtdrIdMoviChiu();
    //Original name: WTDR-DT-INI-EFF
    private int wtdrDtIniEff = DefaultValues.INT_VAL;
    //Original name: WTDR-DT-END-EFF
    private int wtdrDtEndEff = DefaultValues.INT_VAL;
    //Original name: WTDR-COD-COMP-ANIA
    private int wtdrCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WTDR-TP-TIT
    private String wtdrTpTit = DefaultValues.stringVal(Len.WTDR_TP_TIT);
    //Original name: WTDR-PROG-TIT
    private WtdrProgTit wtdrProgTit = new WtdrProgTit();
    //Original name: WTDR-TP-PRE-TIT
    private String wtdrTpPreTit = DefaultValues.stringVal(Len.WTDR_TP_PRE_TIT);
    //Original name: WTDR-TP-STAT-TIT
    private String wtdrTpStatTit = DefaultValues.stringVal(Len.WTDR_TP_STAT_TIT);
    //Original name: WTDR-DT-INI-COP
    private WtdrDtIniCop wtdrDtIniCop = new WtdrDtIniCop();
    //Original name: WTDR-DT-END-COP
    private WtdrDtEndCop wtdrDtEndCop = new WtdrDtEndCop();
    //Original name: WTDR-IMP-PAG
    private WtdrImpPag wtdrImpPag = new WtdrImpPag();
    //Original name: WTDR-FL-SOLL
    private char wtdrFlSoll = DefaultValues.CHAR_VAL;
    //Original name: WTDR-FRAZ
    private WtdrFraz wtdrFraz = new WtdrFraz();
    //Original name: WTDR-DT-APPLZ-MORA
    private WtdrDtApplzMora wtdrDtApplzMora = new WtdrDtApplzMora();
    //Original name: WTDR-FL-MORA
    private char wtdrFlMora = DefaultValues.CHAR_VAL;
    //Original name: WTDR-ID-RAPP-RETE
    private WtdrIdRappRete wtdrIdRappRete = new WtdrIdRappRete();
    //Original name: WTDR-ID-RAPP-ANA
    private WtdrIdRappAna wtdrIdRappAna = new WtdrIdRappAna();
    //Original name: WTDR-COD-DVS
    private String wtdrCodDvs = DefaultValues.stringVal(Len.WTDR_COD_DVS);
    //Original name: WTDR-DT-EMIS-TIT
    private int wtdrDtEmisTit = DefaultValues.INT_VAL;
    //Original name: WTDR-DT-ESI-TIT
    private WtdrDtEsiTit wtdrDtEsiTit = new WtdrDtEsiTit();
    //Original name: WTDR-TOT-PRE-NET
    private WtdrTotPreNet wtdrTotPreNet = new WtdrTotPreNet();
    //Original name: WTDR-TOT-INTR-FRAZ
    private WtdrTotIntrFraz wtdrTotIntrFraz = new WtdrTotIntrFraz();
    //Original name: WTDR-TOT-INTR-MORA
    private WtdrTotIntrMora wtdrTotIntrMora = new WtdrTotIntrMora();
    //Original name: WTDR-TOT-INTR-PREST
    private WtdrTotIntrPrest wtdrTotIntrPrest = new WtdrTotIntrPrest();
    //Original name: WTDR-TOT-INTR-RETDT
    private WtdrTotIntrRetdt wtdrTotIntrRetdt = new WtdrTotIntrRetdt();
    //Original name: WTDR-TOT-INTR-RIAT
    private WtdrTotIntrRiat wtdrTotIntrRiat = new WtdrTotIntrRiat();
    //Original name: WTDR-TOT-DIR
    private WtdrTotDir wtdrTotDir = new WtdrTotDir();
    //Original name: WTDR-TOT-SPE-MED
    private WtdrTotSpeMed wtdrTotSpeMed = new WtdrTotSpeMed();
    //Original name: WTDR-TOT-SPE-AGE
    private WtdrTotSpeAge wtdrTotSpeAge = new WtdrTotSpeAge();
    //Original name: WTDR-TOT-TAX
    private WtdrTotTax wtdrTotTax = new WtdrTotTax();
    //Original name: WTDR-TOT-SOPR-SAN
    private WtdrTotSoprSan wtdrTotSoprSan = new WtdrTotSoprSan();
    //Original name: WTDR-TOT-SOPR-TEC
    private WtdrTotSoprTec wtdrTotSoprTec = new WtdrTotSoprTec();
    //Original name: WTDR-TOT-SOPR-SPO
    private WtdrTotSoprSpo wtdrTotSoprSpo = new WtdrTotSoprSpo();
    //Original name: WTDR-TOT-SOPR-PROF
    private WtdrTotSoprProf wtdrTotSoprProf = new WtdrTotSoprProf();
    //Original name: WTDR-TOT-SOPR-ALT
    private WtdrTotSoprAlt wtdrTotSoprAlt = new WtdrTotSoprAlt();
    //Original name: WTDR-TOT-PRE-TOT
    private WtdrTotPreTot wtdrTotPreTot = new WtdrTotPreTot();
    //Original name: WTDR-TOT-PRE-PP-IAS
    private WtdrTotPrePpIas wtdrTotPrePpIas = new WtdrTotPrePpIas();
    //Original name: WTDR-TOT-CAR-IAS
    private WtdrTotCarIas wtdrTotCarIas = new WtdrTotCarIas();
    //Original name: WTDR-TOT-PRE-SOLO-RSH
    private WtdrTotPreSoloRsh wtdrTotPreSoloRsh = new WtdrTotPreSoloRsh();
    //Original name: WTDR-TOT-PROV-ACQ-1AA
    private WtdrTotProvAcq1aa wtdrTotProvAcq1aa = new WtdrTotProvAcq1aa();
    //Original name: WTDR-TOT-PROV-ACQ-2AA
    private WtdrTotProvAcq2aa wtdrTotProvAcq2aa = new WtdrTotProvAcq2aa();
    //Original name: WTDR-TOT-PROV-RICOR
    private WtdrTotProvRicor wtdrTotProvRicor = new WtdrTotProvRicor();
    //Original name: WTDR-TOT-PROV-INC
    private WtdrTotProvInc wtdrTotProvInc = new WtdrTotProvInc();
    //Original name: WTDR-TOT-PROV-DA-REC
    private WtdrTotProvDaRec wtdrTotProvDaRec = new WtdrTotProvDaRec();
    //Original name: WTDR-IMP-AZ
    private WtdrImpAz wtdrImpAz = new WtdrImpAz();
    //Original name: WTDR-IMP-ADER
    private WtdrImpAder wtdrImpAder = new WtdrImpAder();
    //Original name: WTDR-IMP-TFR
    private WtdrImpTfr wtdrImpTfr = new WtdrImpTfr();
    //Original name: WTDR-IMP-VOLO
    private WtdrImpVolo wtdrImpVolo = new WtdrImpVolo();
    //Original name: WTDR-FL-VLDT-TIT
    private char wtdrFlVldtTit = DefaultValues.CHAR_VAL;
    //Original name: WTDR-TOT-CAR-ACQ
    private WtdrTotCarAcq wtdrTotCarAcq = new WtdrTotCarAcq();
    //Original name: WTDR-TOT-CAR-GEST
    private WtdrTotCarGest wtdrTotCarGest = new WtdrTotCarGest();
    //Original name: WTDR-TOT-CAR-INC
    private WtdrTotCarInc wtdrTotCarInc = new WtdrTotCarInc();
    //Original name: WTDR-TOT-MANFEE-ANTIC
    private WtdrTotManfeeAntic wtdrTotManfeeAntic = new WtdrTotManfeeAntic();
    //Original name: WTDR-TOT-MANFEE-RICOR
    private WtdrTotManfeeRicor wtdrTotManfeeRicor = new WtdrTotManfeeRicor();
    //Original name: WTDR-TOT-MANFEE-REC
    private WtdrTotManfeeRec wtdrTotManfeeRec = new WtdrTotManfeeRec();
    //Original name: WTDR-DS-RIGA
    private long wtdrDsRiga = DefaultValues.LONG_VAL;
    //Original name: WTDR-DS-OPER-SQL
    private char wtdrDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WTDR-DS-VER
    private int wtdrDsVer = DefaultValues.INT_VAL;
    //Original name: WTDR-DS-TS-INI-CPTZ
    private long wtdrDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WTDR-DS-TS-END-CPTZ
    private long wtdrDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WTDR-DS-UTENTE
    private String wtdrDsUtente = DefaultValues.stringVal(Len.WTDR_DS_UTENTE);
    //Original name: WTDR-DS-STATO-ELAB
    private char wtdrDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WTDR-IMP-TRASFE
    private WtdrImpTrasfe wtdrImpTrasfe = new WtdrImpTrasfe();
    //Original name: WTDR-IMP-TFR-STRC
    private WtdrImpTfrStrc wtdrImpTfrStrc = new WtdrImpTfrStrc();
    //Original name: WTDR-TOT-ACQ-EXP
    private WtdrTotAcqExp wtdrTotAcqExp = new WtdrTotAcqExp();
    //Original name: WTDR-TOT-REMUN-ASS
    private WtdrTotRemunAss wtdrTotRemunAss = new WtdrTotRemunAss();
    //Original name: WTDR-TOT-COMMIS-INTER
    private WtdrTotCommisInter wtdrTotCommisInter = new WtdrTotCommisInter();
    //Original name: WTDR-TOT-CNBT-ANTIRAC
    private WtdrTotCnbtAntirac wtdrTotCnbtAntirac = new WtdrTotCnbtAntirac();
    //Original name: WTDR-FL-INC-AUTOGEN
    private char wtdrFlIncAutogen = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setWtdrIdTitRat(int wtdrIdTitRat) {
        this.wtdrIdTitRat = wtdrIdTitRat;
    }

    public int getWtdrIdTitRat() {
        return this.wtdrIdTitRat;
    }

    public void setWtdrIdOgg(int wtdrIdOgg) {
        this.wtdrIdOgg = wtdrIdOgg;
    }

    public int getWtdrIdOgg() {
        return this.wtdrIdOgg;
    }

    public void setWtdrTpOgg(String wtdrTpOgg) {
        this.wtdrTpOgg = Functions.subString(wtdrTpOgg, Len.WTDR_TP_OGG);
    }

    public String getWtdrTpOgg() {
        return this.wtdrTpOgg;
    }

    public void setWtdrIdMoviCrz(int wtdrIdMoviCrz) {
        this.wtdrIdMoviCrz = wtdrIdMoviCrz;
    }

    public int getWtdrIdMoviCrz() {
        return this.wtdrIdMoviCrz;
    }

    public void setWtdrDtIniEff(int wtdrDtIniEff) {
        this.wtdrDtIniEff = wtdrDtIniEff;
    }

    public int getWtdrDtIniEff() {
        return this.wtdrDtIniEff;
    }

    public void setWtdrDtEndEff(int wtdrDtEndEff) {
        this.wtdrDtEndEff = wtdrDtEndEff;
    }

    public int getWtdrDtEndEff() {
        return this.wtdrDtEndEff;
    }

    public void setWtdrCodCompAnia(int wtdrCodCompAnia) {
        this.wtdrCodCompAnia = wtdrCodCompAnia;
    }

    public int getWtdrCodCompAnia() {
        return this.wtdrCodCompAnia;
    }

    public void setWtdrTpTit(String wtdrTpTit) {
        this.wtdrTpTit = Functions.subString(wtdrTpTit, Len.WTDR_TP_TIT);
    }

    public String getWtdrTpTit() {
        return this.wtdrTpTit;
    }

    public void setWtdrTpPreTit(String wtdrTpPreTit) {
        this.wtdrTpPreTit = Functions.subString(wtdrTpPreTit, Len.WTDR_TP_PRE_TIT);
    }

    public String getWtdrTpPreTit() {
        return this.wtdrTpPreTit;
    }

    public void setWtdrTpStatTit(String wtdrTpStatTit) {
        this.wtdrTpStatTit = Functions.subString(wtdrTpStatTit, Len.WTDR_TP_STAT_TIT);
    }

    public String getWtdrTpStatTit() {
        return this.wtdrTpStatTit;
    }

    public void setWtdrFlSoll(char wtdrFlSoll) {
        this.wtdrFlSoll = wtdrFlSoll;
    }

    public char getWtdrFlSoll() {
        return this.wtdrFlSoll;
    }

    public void setWtdrFlMora(char wtdrFlMora) {
        this.wtdrFlMora = wtdrFlMora;
    }

    public char getWtdrFlMora() {
        return this.wtdrFlMora;
    }

    public void setWtdrCodDvs(String wtdrCodDvs) {
        this.wtdrCodDvs = Functions.subString(wtdrCodDvs, Len.WTDR_COD_DVS);
    }

    public String getWtdrCodDvs() {
        return this.wtdrCodDvs;
    }

    public String getWtdrCodDvsFormatted() {
        return Functions.padBlanks(getWtdrCodDvs(), Len.WTDR_COD_DVS);
    }

    public void setWtdrDtEmisTit(int wtdrDtEmisTit) {
        this.wtdrDtEmisTit = wtdrDtEmisTit;
    }

    public int getWtdrDtEmisTit() {
        return this.wtdrDtEmisTit;
    }

    public void setWtdrFlVldtTit(char wtdrFlVldtTit) {
        this.wtdrFlVldtTit = wtdrFlVldtTit;
    }

    public void setWtdrFlVldtTitFormatted(String wtdrFlVldtTit) {
        setWtdrFlVldtTit(Functions.charAt(wtdrFlVldtTit, Types.CHAR_SIZE));
    }

    public char getWtdrFlVldtTit() {
        return this.wtdrFlVldtTit;
    }

    public void setWtdrDsRiga(long wtdrDsRiga) {
        this.wtdrDsRiga = wtdrDsRiga;
    }

    public long getWtdrDsRiga() {
        return this.wtdrDsRiga;
    }

    public void setWtdrDsOperSql(char wtdrDsOperSql) {
        this.wtdrDsOperSql = wtdrDsOperSql;
    }

    public char getWtdrDsOperSql() {
        return this.wtdrDsOperSql;
    }

    public void setWtdrDsVer(int wtdrDsVer) {
        this.wtdrDsVer = wtdrDsVer;
    }

    public int getWtdrDsVer() {
        return this.wtdrDsVer;
    }

    public void setWtdrDsTsIniCptz(long wtdrDsTsIniCptz) {
        this.wtdrDsTsIniCptz = wtdrDsTsIniCptz;
    }

    public long getWtdrDsTsIniCptz() {
        return this.wtdrDsTsIniCptz;
    }

    public void setWtdrDsTsEndCptz(long wtdrDsTsEndCptz) {
        this.wtdrDsTsEndCptz = wtdrDsTsEndCptz;
    }

    public long getWtdrDsTsEndCptz() {
        return this.wtdrDsTsEndCptz;
    }

    public void setWtdrDsUtente(String wtdrDsUtente) {
        this.wtdrDsUtente = Functions.subString(wtdrDsUtente, Len.WTDR_DS_UTENTE);
    }

    public String getWtdrDsUtente() {
        return this.wtdrDsUtente;
    }

    public void setWtdrDsStatoElab(char wtdrDsStatoElab) {
        this.wtdrDsStatoElab = wtdrDsStatoElab;
    }

    public char getWtdrDsStatoElab() {
        return this.wtdrDsStatoElab;
    }

    public void setWtdrFlIncAutogen(char wtdrFlIncAutogen) {
        this.wtdrFlIncAutogen = wtdrFlIncAutogen;
    }

    public char getWtdrFlIncAutogen() {
        return this.wtdrFlIncAutogen;
    }

    public WtdrDtApplzMora getWtdrDtApplzMora() {
        return wtdrDtApplzMora;
    }

    public WtdrDtEndCop getWtdrDtEndCop() {
        return wtdrDtEndCop;
    }

    public WtdrDtEsiTit getWtdrDtEsiTit() {
        return wtdrDtEsiTit;
    }

    public WtdrDtIniCop getWtdrDtIniCop() {
        return wtdrDtIniCop;
    }

    public WtdrFraz getWtdrFraz() {
        return wtdrFraz;
    }

    public WtdrIdMoviChiu getWtdrIdMoviChiu() {
        return wtdrIdMoviChiu;
    }

    public WtdrIdRappAna getWtdrIdRappAna() {
        return wtdrIdRappAna;
    }

    public WtdrIdRappRete getWtdrIdRappRete() {
        return wtdrIdRappRete;
    }

    public WtdrImpAder getWtdrImpAder() {
        return wtdrImpAder;
    }

    public WtdrImpAz getWtdrImpAz() {
        return wtdrImpAz;
    }

    public WtdrImpPag getWtdrImpPag() {
        return wtdrImpPag;
    }

    public WtdrImpTfr getWtdrImpTfr() {
        return wtdrImpTfr;
    }

    public WtdrImpTfrStrc getWtdrImpTfrStrc() {
        return wtdrImpTfrStrc;
    }

    public WtdrImpTrasfe getWtdrImpTrasfe() {
        return wtdrImpTrasfe;
    }

    public WtdrImpVolo getWtdrImpVolo() {
        return wtdrImpVolo;
    }

    public WtdrProgTit getWtdrProgTit() {
        return wtdrProgTit;
    }

    public WtdrTotAcqExp getWtdrTotAcqExp() {
        return wtdrTotAcqExp;
    }

    public WtdrTotCarAcq getWtdrTotCarAcq() {
        return wtdrTotCarAcq;
    }

    public WtdrTotCarGest getWtdrTotCarGest() {
        return wtdrTotCarGest;
    }

    public WtdrTotCarIas getWtdrTotCarIas() {
        return wtdrTotCarIas;
    }

    public WtdrTotCarInc getWtdrTotCarInc() {
        return wtdrTotCarInc;
    }

    public WtdrTotCnbtAntirac getWtdrTotCnbtAntirac() {
        return wtdrTotCnbtAntirac;
    }

    public WtdrTotCommisInter getWtdrTotCommisInter() {
        return wtdrTotCommisInter;
    }

    public WtdrTotDir getWtdrTotDir() {
        return wtdrTotDir;
    }

    public WtdrTotIntrFraz getWtdrTotIntrFraz() {
        return wtdrTotIntrFraz;
    }

    public WtdrTotIntrMora getWtdrTotIntrMora() {
        return wtdrTotIntrMora;
    }

    public WtdrTotIntrPrest getWtdrTotIntrPrest() {
        return wtdrTotIntrPrest;
    }

    public WtdrTotIntrRetdt getWtdrTotIntrRetdt() {
        return wtdrTotIntrRetdt;
    }

    public WtdrTotIntrRiat getWtdrTotIntrRiat() {
        return wtdrTotIntrRiat;
    }

    public WtdrTotManfeeAntic getWtdrTotManfeeAntic() {
        return wtdrTotManfeeAntic;
    }

    public WtdrTotManfeeRec getWtdrTotManfeeRec() {
        return wtdrTotManfeeRec;
    }

    public WtdrTotManfeeRicor getWtdrTotManfeeRicor() {
        return wtdrTotManfeeRicor;
    }

    public WtdrTotPreNet getWtdrTotPreNet() {
        return wtdrTotPreNet;
    }

    public WtdrTotPrePpIas getWtdrTotPrePpIas() {
        return wtdrTotPrePpIas;
    }

    public WtdrTotPreSoloRsh getWtdrTotPreSoloRsh() {
        return wtdrTotPreSoloRsh;
    }

    public WtdrTotPreTot getWtdrTotPreTot() {
        return wtdrTotPreTot;
    }

    public WtdrTotProvAcq1aa getWtdrTotProvAcq1aa() {
        return wtdrTotProvAcq1aa;
    }

    public WtdrTotProvAcq2aa getWtdrTotProvAcq2aa() {
        return wtdrTotProvAcq2aa;
    }

    public WtdrTotProvDaRec getWtdrTotProvDaRec() {
        return wtdrTotProvDaRec;
    }

    public WtdrTotProvInc getWtdrTotProvInc() {
        return wtdrTotProvInc;
    }

    public WtdrTotProvRicor getWtdrTotProvRicor() {
        return wtdrTotProvRicor;
    }

    public WtdrTotRemunAss getWtdrTotRemunAss() {
        return wtdrTotRemunAss;
    }

    public WtdrTotSoprAlt getWtdrTotSoprAlt() {
        return wtdrTotSoprAlt;
    }

    public WtdrTotSoprProf getWtdrTotSoprProf() {
        return wtdrTotSoprProf;
    }

    public WtdrTotSoprSan getWtdrTotSoprSan() {
        return wtdrTotSoprSan;
    }

    public WtdrTotSoprSpo getWtdrTotSoprSpo() {
        return wtdrTotSoprSpo;
    }

    public WtdrTotSoprTec getWtdrTotSoprTec() {
        return wtdrTotSoprTec;
    }

    public WtdrTotSpeAge getWtdrTotSpeAge() {
        return wtdrTotSpeAge;
    }

    public WtdrTotSpeMed getWtdrTotSpeMed() {
        return wtdrTotSpeMed;
    }

    public WtdrTotTax getWtdrTotTax() {
        return wtdrTotTax;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TP_OGG = 2;
        public static final int WTDR_TP_TIT = 2;
        public static final int WTDR_TP_PRE_TIT = 2;
        public static final int WTDR_TP_STAT_TIT = 2;
        public static final int WTDR_COD_DVS = 20;
        public static final int WTDR_DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
