package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV2631<br>
 * Variable: LDBV2631 from copybook LDBV2631<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbv2631 {

    //==== PROPERTIES ====
    //Original name: LDBV2631-ID-OGG
    private int ldbv2631IdOgg = DefaultValues.INT_VAL;
    //Original name: LDBV2631-TP-OGG
    private String ldbv2631TpOgg = DefaultValues.stringVal(Len.LDBV2631_TP_OGG);
    //Original name: LDBV2631-TP-MOV1
    private int ldbv2631TpMov1 = DefaultValues.INT_VAL;
    //Original name: LDBV2631-TP-MOV2
    private int ldbv2631TpMov2 = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbv2631Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV2631];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV2631);
        setLdbv2631Bytes(buffer, 1);
    }

    public String getLdbv2631Formatted() {
        return getLdbv2631DatiInputFormatted();
    }

    public void setLdbv2631Bytes(byte[] buffer, int offset) {
        int position = offset;
        setLdbv2631DatiInputBytes(buffer, position);
    }

    public String getLdbv2631DatiInputFormatted() {
        return MarshalByteExt.bufferToStr(getLdbv2631DatiInputBytes());
    }

    /**Original name: LDBV2631-DATI-INPUT<br>*/
    public byte[] getLdbv2631DatiInputBytes() {
        byte[] buffer = new byte[Len.LDBV2631_DATI_INPUT];
        return getLdbv2631DatiInputBytes(buffer, 1);
    }

    public void setLdbv2631DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv2631IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV2631_ID_OGG, 0);
        position += Len.LDBV2631_ID_OGG;
        ldbv2631TpOgg = MarshalByte.readString(buffer, position, Len.LDBV2631_TP_OGG);
        position += Len.LDBV2631_TP_OGG;
        ldbv2631TpMov1 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV2631_TP_MOV1, 0);
        position += Len.LDBV2631_TP_MOV1;
        ldbv2631TpMov2 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV2631_TP_MOV2, 0);
    }

    public byte[] getLdbv2631DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv2631IdOgg, Len.Int.LDBV2631_ID_OGG, 0);
        position += Len.LDBV2631_ID_OGG;
        MarshalByte.writeString(buffer, position, ldbv2631TpOgg, Len.LDBV2631_TP_OGG);
        position += Len.LDBV2631_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv2631TpMov1, Len.Int.LDBV2631_TP_MOV1, 0);
        position += Len.LDBV2631_TP_MOV1;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv2631TpMov2, Len.Int.LDBV2631_TP_MOV2, 0);
        return buffer;
    }

    public void setLdbv2631IdOgg(int ldbv2631IdOgg) {
        this.ldbv2631IdOgg = ldbv2631IdOgg;
    }

    public int getLdbv2631IdOgg() {
        return this.ldbv2631IdOgg;
    }

    public void setLdbv2631TpOgg(String ldbv2631TpOgg) {
        this.ldbv2631TpOgg = Functions.subString(ldbv2631TpOgg, Len.LDBV2631_TP_OGG);
    }

    public String getLdbv2631TpOgg() {
        return this.ldbv2631TpOgg;
    }

    public void setLdbv2631TpMov1(int ldbv2631TpMov1) {
        this.ldbv2631TpMov1 = ldbv2631TpMov1;
    }

    public int getLdbv2631TpMov1() {
        return this.ldbv2631TpMov1;
    }

    public void setLdbv2631TpMov2(int ldbv2631TpMov2) {
        this.ldbv2631TpMov2 = ldbv2631TpMov2;
    }

    public int getLdbv2631TpMov2() {
        return this.ldbv2631TpMov2;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV2631_TP_OGG = 2;
        public static final int LDBV2631_ID_OGG = 5;
        public static final int LDBV2631_TP_MOV1 = 3;
        public static final int LDBV2631_TP_MOV2 = 3;
        public static final int LDBV2631_DATI_INPUT = LDBV2631_ID_OGG + LDBV2631_TP_OGG + LDBV2631_TP_MOV1 + LDBV2631_TP_MOV2;
        public static final int LDBV2631 = LDBV2631_DATI_INPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV2631_ID_OGG = 9;
            public static final int LDBV2631_TP_MOV1 = 5;
            public static final int LDBV2631_TP_MOV2 = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
