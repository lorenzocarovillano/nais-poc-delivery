package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IVVC0221<br>
 * Variable: IVVC0221 from copybook IVVC0221<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0221 {

    //==== PROPERTIES ====
    //Original name: IVVC0221-DETT-QUEST-WC
    private Ivvc0221DettQuestWc ivvc0221DettQuestWc = new Ivvc0221DettQuestWc();

    //==== METHODS ====
    public Ivvc0221DettQuestWc getIvvc0221DettQuestWc() {
        return ivvc0221DettQuestWc;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IVVC0211_COD_DOMANDA = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
