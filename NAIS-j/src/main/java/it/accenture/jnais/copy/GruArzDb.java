package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRU-ARZ-DB<br>
 * Variable: GRU-ARZ-DB from copybook IDBVGRU3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class GruArzDb {

    //==== PROPERTIES ====
    //Original name: GRU-DAT-INI-GRU-ARZ-DB
    private String datIniGruArzDb = DefaultValues.stringVal(Len.DAT_INI_GRU_ARZ_DB);
    //Original name: GRU-DAT-FINE-GRU-ARZ-DB
    private String datFineGruArzDb = DefaultValues.stringVal(Len.DAT_FINE_GRU_ARZ_DB);
    //Original name: GRU-TMST-INS-RIG-DB
    private String tmstInsRigDb = DefaultValues.stringVal(Len.TMST_INS_RIG_DB);
    //Original name: GRU-TMST-AGR-RIG-DB
    private String tmstAgrRigDb = DefaultValues.stringVal(Len.TMST_AGR_RIG_DB);

    //==== METHODS ====
    public void setDatIniGruArzDb(String datIniGruArzDb) {
        this.datIniGruArzDb = Functions.subString(datIniGruArzDb, Len.DAT_INI_GRU_ARZ_DB);
    }

    public String getDatIniGruArzDb() {
        return this.datIniGruArzDb;
    }

    public void setDatFineGruArzDb(String datFineGruArzDb) {
        this.datFineGruArzDb = Functions.subString(datFineGruArzDb, Len.DAT_FINE_GRU_ARZ_DB);
    }

    public String getDatFineGruArzDb() {
        return this.datFineGruArzDb;
    }

    public void setTmstInsRigDb(String tmstInsRigDb) {
        this.tmstInsRigDb = Functions.subString(tmstInsRigDb, Len.TMST_INS_RIG_DB);
    }

    public String getTmstInsRigDb() {
        return this.tmstInsRigDb;
    }

    public void setTmstAgrRigDb(String tmstAgrRigDb) {
        this.tmstAgrRigDb = Functions.subString(tmstAgrRigDb, Len.TMST_AGR_RIG_DB);
    }

    public String getTmstAgrRigDb() {
        return this.tmstAgrRigDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DAT_INI_GRU_ARZ_DB = 10;
        public static final int DAT_FINE_GRU_ARZ_DB = 10;
        public static final int TMST_INS_RIG_DB = 26;
        public static final int TMST_AGR_RIG_DB = 26;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
