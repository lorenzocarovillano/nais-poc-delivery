package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.PcoAaUti;
import it.accenture.jnais.ws.redefines.PcoArrotPre;
import it.accenture.jnais.ws.redefines.PcoDtCont;
import it.accenture.jnais.ws.redefines.PcoDtEstrAssMag70a;
import it.accenture.jnais.ws.redefines.PcoDtEstrAssMin70a;
import it.accenture.jnais.ws.redefines.PcoDtRiatRiassComm;
import it.accenture.jnais.ws.redefines.PcoDtRiatRiassRsh;
import it.accenture.jnais.ws.redefines.PcoDtRiclRiriasCom;
import it.accenture.jnais.ws.redefines.PcoDtUltAggErogRe;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCoriC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCoriI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCotrC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCotrI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollEmes;
import it.accenture.jnais.ws.redefines.PcoDtUltBollEmesI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollLiq;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPerfC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPerfI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPreC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPreI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollQuieC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollQuieI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRiat;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRiatI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRpCl;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRpIn;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRspCl;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRspIn;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSdI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSdnlI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSnden;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSndnlq;
import it.accenture.jnais.ws.redefines.PcoDtUltBollStor;
import it.accenture.jnais.ws.redefines.PcoDtUltBollStorI;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsfdtCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsfdtIn;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsricCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsricIn;
import it.accenture.jnais.ws.redefines.PcoDtUltcIsCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcIsIn;
import it.accenture.jnais.ws.redefines.PcoDtUltcMarsol;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiAaC;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiAaI;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiMmC;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiMmI;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiTrI;
import it.accenture.jnais.ws.redefines.PcoDtUltcRbCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcRbIn;
import it.accenture.jnais.ws.redefines.PcoDtUltEcIlColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcIlInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcMrmColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcMrmInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcRivColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcRivInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcTcmColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcTcmInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcUlColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcUlInd;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt92C;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt92I;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt93C;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt93I;
import it.accenture.jnais.ws.redefines.PcoDtUltElabCommef;
import it.accenture.jnais.ws.redefines.PcoDtUltElabCosAt;
import it.accenture.jnais.ws.redefines.PcoDtUltElabCosSt;
import it.accenture.jnais.ws.redefines.PcoDtUltElabLiqmef;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPaspas;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPrAut;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPrCon;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPrlcos;
import it.accenture.jnais.ws.redefines.PcoDtUltElabRedpro;
import it.accenture.jnais.ws.redefines.PcoDtUltElabSpeIn;
import it.accenture.jnais.ws.redefines.PcoDtUltElabTakeP;
import it.accenture.jnais.ws.redefines.PcoDtUltelriscparPr;
import it.accenture.jnais.ws.redefines.PcoDtUltEstrazFug;
import it.accenture.jnais.ws.redefines.PcoDtUltEstrDecCo;
import it.accenture.jnais.ws.redefines.PcoDtUltgzCed;
import it.accenture.jnais.ws.redefines.PcoDtUltgzCedColl;
import it.accenture.jnais.ws.redefines.PcoDtUltgzTrchECl;
import it.accenture.jnais.ws.redefines.PcoDtUltgzTrchEIn;
import it.accenture.jnais.ws.redefines.PcoDtUltQtzoCl;
import it.accenture.jnais.ws.redefines.PcoDtUltQtzoIn;
import it.accenture.jnais.ws.redefines.PcoDtUltRiclPre;
import it.accenture.jnais.ws.redefines.PcoDtUltRiclRiass;
import it.accenture.jnais.ws.redefines.PcoDtUltRinnColl;
import it.accenture.jnais.ws.redefines.PcoDtUltRinnGarac;
import it.accenture.jnais.ws.redefines.PcoDtUltRinnTac;
import it.accenture.jnais.ws.redefines.PcoDtUltRivalCl;
import it.accenture.jnais.ws.redefines.PcoDtUltRivalIn;
import it.accenture.jnais.ws.redefines.PcoDtUltscElabCl;
import it.accenture.jnais.ws.redefines.PcoDtUltscElabIn;
import it.accenture.jnais.ws.redefines.PcoDtUltscOpzCl;
import it.accenture.jnais.ws.redefines.PcoDtUltscOpzIn;
import it.accenture.jnais.ws.redefines.PcoDtUltTabulRiass;
import it.accenture.jnais.ws.redefines.PcoFrqCostiAtt;
import it.accenture.jnais.ws.redefines.PcoFrqCostiStornati;
import it.accenture.jnais.ws.redefines.PcoGgIntrRitPag;
import it.accenture.jnais.ws.redefines.PcoGgMaxRecProv;
import it.accenture.jnais.ws.redefines.PcoImpAssSociale;
import it.accenture.jnais.ws.redefines.PcoLimVltr;
import it.accenture.jnais.ws.redefines.PcoLmCSubrshConIn;
import it.accenture.jnais.ws.redefines.PcoLmRisConInt;
import it.accenture.jnais.ws.redefines.PcoNumGgArrIntrPr;
import it.accenture.jnais.ws.redefines.PcoNumMmCalcMora;
import it.accenture.jnais.ws.redefines.PcoPcCSubrshMarsol;
import it.accenture.jnais.ws.redefines.PcoPcGarNoriskMars;
import it.accenture.jnais.ws.redefines.PcoPcProv1aaAcq;
import it.accenture.jnais.ws.redefines.PcoPcRidImp1382011;
import it.accenture.jnais.ws.redefines.PcoPcRidImp662014;
import it.accenture.jnais.ws.redefines.PcoPcRmMarsol;
import it.accenture.jnais.ws.redefines.PcoSoglAmlPrePer;
import it.accenture.jnais.ws.redefines.PcoSoglAmlPreSavR;
import it.accenture.jnais.ws.redefines.PcoSoglAmlPreUni;
import it.accenture.jnais.ws.redefines.PcoStstXRegione;

/**Original name: PARAM-COMP<br>
 * Variable: PARAM-COMP from copybook IDBVPCO1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ParamComp {

    //==== PROPERTIES ====
    //Original name: PCO-COD-COMP-ANIA
    private int pcoCodCompAnia = DefaultValues.INT_VAL;
    //Original name: PCO-COD-TRAT-CIRT
    private String pcoCodTratCirt = DefaultValues.stringVal(Len.PCO_COD_TRAT_CIRT);
    //Original name: PCO-LIM-VLTR
    private PcoLimVltr pcoLimVltr = new PcoLimVltr();
    //Original name: PCO-TP-RAT-PERF
    private char pcoTpRatPerf = DefaultValues.CHAR_VAL;
    //Original name: PCO-TP-LIV-GENZ-TIT
    private String pcoTpLivGenzTit = DefaultValues.stringVal(Len.PCO_TP_LIV_GENZ_TIT);
    //Original name: PCO-ARROT-PRE
    private PcoArrotPre pcoArrotPre = new PcoArrotPre();
    //Original name: PCO-DT-CONT
    private PcoDtCont pcoDtCont = new PcoDtCont();
    //Original name: PCO-DT-ULT-RIVAL-IN
    private PcoDtUltRivalIn pcoDtUltRivalIn = new PcoDtUltRivalIn();
    //Original name: PCO-DT-ULT-QTZO-IN
    private PcoDtUltQtzoIn pcoDtUltQtzoIn = new PcoDtUltQtzoIn();
    //Original name: PCO-DT-ULT-RICL-RIASS
    private PcoDtUltRiclRiass pcoDtUltRiclRiass = new PcoDtUltRiclRiass();
    //Original name: PCO-DT-ULT-TABUL-RIASS
    private PcoDtUltTabulRiass pcoDtUltTabulRiass = new PcoDtUltTabulRiass();
    //Original name: PCO-DT-ULT-BOLL-EMES
    private PcoDtUltBollEmes pcoDtUltBollEmes = new PcoDtUltBollEmes();
    //Original name: PCO-DT-ULT-BOLL-STOR
    private PcoDtUltBollStor pcoDtUltBollStor = new PcoDtUltBollStor();
    //Original name: PCO-DT-ULT-BOLL-LIQ
    private PcoDtUltBollLiq pcoDtUltBollLiq = new PcoDtUltBollLiq();
    //Original name: PCO-DT-ULT-BOLL-RIAT
    private PcoDtUltBollRiat pcoDtUltBollRiat = new PcoDtUltBollRiat();
    //Original name: PCO-DT-ULTELRISCPAR-PR
    private PcoDtUltelriscparPr pcoDtUltelriscparPr = new PcoDtUltelriscparPr();
    //Original name: PCO-DT-ULTC-IS-IN
    private PcoDtUltcIsIn pcoDtUltcIsIn = new PcoDtUltcIsIn();
    //Original name: PCO-DT-ULT-RICL-PRE
    private PcoDtUltRiclPre pcoDtUltRiclPre = new PcoDtUltRiclPre();
    //Original name: PCO-DT-ULTC-MARSOL
    private PcoDtUltcMarsol pcoDtUltcMarsol = new PcoDtUltcMarsol();
    //Original name: PCO-DT-ULTC-RB-IN
    private PcoDtUltcRbIn pcoDtUltcRbIn = new PcoDtUltcRbIn();
    //Original name: PCO-PC-PROV-1AA-ACQ
    private PcoPcProv1aaAcq pcoPcProv1aaAcq = new PcoPcProv1aaAcq();
    //Original name: PCO-MOD-INTR-PREST
    private String pcoModIntrPrest = DefaultValues.stringVal(Len.PCO_MOD_INTR_PREST);
    //Original name: PCO-GG-MAX-REC-PROV
    private PcoGgMaxRecProv pcoGgMaxRecProv = new PcoGgMaxRecProv();
    //Original name: PCO-DT-ULTGZ-TRCH-E-IN
    private PcoDtUltgzTrchEIn pcoDtUltgzTrchEIn = new PcoDtUltgzTrchEIn();
    //Original name: PCO-DT-ULT-BOLL-SNDEN
    private PcoDtUltBollSnden pcoDtUltBollSnden = new PcoDtUltBollSnden();
    //Original name: PCO-DT-ULT-BOLL-SNDNLQ
    private PcoDtUltBollSndnlq pcoDtUltBollSndnlq = new PcoDtUltBollSndnlq();
    //Original name: PCO-DT-ULTSC-ELAB-IN
    private PcoDtUltscElabIn pcoDtUltscElabIn = new PcoDtUltscElabIn();
    //Original name: PCO-DT-ULTSC-OPZ-IN
    private PcoDtUltscOpzIn pcoDtUltscOpzIn = new PcoDtUltscOpzIn();
    //Original name: PCO-DT-ULTC-BNSRIC-IN
    private PcoDtUltcBnsricIn pcoDtUltcBnsricIn = new PcoDtUltcBnsricIn();
    //Original name: PCO-DT-ULTC-BNSFDT-IN
    private PcoDtUltcBnsfdtIn pcoDtUltcBnsfdtIn = new PcoDtUltcBnsfdtIn();
    //Original name: PCO-DT-ULT-RINN-GARAC
    private PcoDtUltRinnGarac pcoDtUltRinnGarac = new PcoDtUltRinnGarac();
    //Original name: PCO-DT-ULTGZ-CED
    private PcoDtUltgzCed pcoDtUltgzCed = new PcoDtUltgzCed();
    //Original name: PCO-DT-ULT-ELAB-PRLCOS
    private PcoDtUltElabPrlcos pcoDtUltElabPrlcos = new PcoDtUltElabPrlcos();
    //Original name: PCO-DT-ULT-RINN-COLL
    private PcoDtUltRinnColl pcoDtUltRinnColl = new PcoDtUltRinnColl();
    //Original name: PCO-FL-RVC-PERF
    private char pcoFlRvcPerf = DefaultValues.CHAR_VAL;
    //Original name: PCO-FL-RCS-POLI-NOPERF
    private char pcoFlRcsPoliNoperf = DefaultValues.CHAR_VAL;
    //Original name: PCO-FL-GEST-PLUSV
    private char pcoFlGestPlusv = DefaultValues.CHAR_VAL;
    //Original name: PCO-DT-ULT-RIVAL-CL
    private PcoDtUltRivalCl pcoDtUltRivalCl = new PcoDtUltRivalCl();
    //Original name: PCO-DT-ULT-QTZO-CL
    private PcoDtUltQtzoCl pcoDtUltQtzoCl = new PcoDtUltQtzoCl();
    //Original name: PCO-DT-ULTC-BNSRIC-CL
    private PcoDtUltcBnsricCl pcoDtUltcBnsricCl = new PcoDtUltcBnsricCl();
    //Original name: PCO-DT-ULTC-BNSFDT-CL
    private PcoDtUltcBnsfdtCl pcoDtUltcBnsfdtCl = new PcoDtUltcBnsfdtCl();
    //Original name: PCO-DT-ULTC-IS-CL
    private PcoDtUltcIsCl pcoDtUltcIsCl = new PcoDtUltcIsCl();
    //Original name: PCO-DT-ULTC-RB-CL
    private PcoDtUltcRbCl pcoDtUltcRbCl = new PcoDtUltcRbCl();
    //Original name: PCO-DT-ULTGZ-TRCH-E-CL
    private PcoDtUltgzTrchECl pcoDtUltgzTrchECl = new PcoDtUltgzTrchECl();
    //Original name: PCO-DT-ULTSC-ELAB-CL
    private PcoDtUltscElabCl pcoDtUltscElabCl = new PcoDtUltscElabCl();
    //Original name: PCO-DT-ULTSC-OPZ-CL
    private PcoDtUltscOpzCl pcoDtUltscOpzCl = new PcoDtUltscOpzCl();
    //Original name: PCO-STST-X-REGIONE
    private PcoStstXRegione pcoStstXRegione = new PcoStstXRegione();
    //Original name: PCO-DT-ULTGZ-CED-COLL
    private PcoDtUltgzCedColl pcoDtUltgzCedColl = new PcoDtUltgzCedColl();
    //Original name: PCO-TP-MOD-RIVAL
    private String pcoTpModRival = DefaultValues.stringVal(Len.PCO_TP_MOD_RIVAL);
    //Original name: PCO-NUM-MM-CALC-MORA
    private PcoNumMmCalcMora pcoNumMmCalcMora = new PcoNumMmCalcMora();
    //Original name: PCO-DT-ULT-EC-RIV-COLL
    private PcoDtUltEcRivColl pcoDtUltEcRivColl = new PcoDtUltEcRivColl();
    //Original name: PCO-DT-ULT-EC-RIV-IND
    private PcoDtUltEcRivInd pcoDtUltEcRivInd = new PcoDtUltEcRivInd();
    //Original name: PCO-DT-ULT-EC-IL-COLL
    private PcoDtUltEcIlColl pcoDtUltEcIlColl = new PcoDtUltEcIlColl();
    //Original name: PCO-DT-ULT-EC-IL-IND
    private PcoDtUltEcIlInd pcoDtUltEcIlInd = new PcoDtUltEcIlInd();
    //Original name: PCO-DT-ULT-EC-UL-COLL
    private PcoDtUltEcUlColl pcoDtUltEcUlColl = new PcoDtUltEcUlColl();
    //Original name: PCO-DT-ULT-EC-UL-IND
    private PcoDtUltEcUlInd pcoDtUltEcUlInd = new PcoDtUltEcUlInd();
    //Original name: PCO-AA-UTI
    private PcoAaUti pcoAaUti = new PcoAaUti();
    //Original name: PCO-CALC-RSH-COMUN
    private char pcoCalcRshComun = DefaultValues.CHAR_VAL;
    //Original name: PCO-FL-LIV-DEBUG
    private short pcoFlLivDebug = DefaultValues.SHORT_VAL;
    //Original name: PCO-DT-ULT-BOLL-PERF-C
    private PcoDtUltBollPerfC pcoDtUltBollPerfC = new PcoDtUltBollPerfC();
    //Original name: PCO-DT-ULT-BOLL-RSP-IN
    private PcoDtUltBollRspIn pcoDtUltBollRspIn = new PcoDtUltBollRspIn();
    //Original name: PCO-DT-ULT-BOLL-RSP-CL
    private PcoDtUltBollRspCl pcoDtUltBollRspCl = new PcoDtUltBollRspCl();
    //Original name: PCO-DT-ULT-BOLL-EMES-I
    private PcoDtUltBollEmesI pcoDtUltBollEmesI = new PcoDtUltBollEmesI();
    //Original name: PCO-DT-ULT-BOLL-STOR-I
    private PcoDtUltBollStorI pcoDtUltBollStorI = new PcoDtUltBollStorI();
    //Original name: PCO-DT-ULT-BOLL-RIAT-I
    private PcoDtUltBollRiatI pcoDtUltBollRiatI = new PcoDtUltBollRiatI();
    //Original name: PCO-DT-ULT-BOLL-SD-I
    private PcoDtUltBollSdI pcoDtUltBollSdI = new PcoDtUltBollSdI();
    //Original name: PCO-DT-ULT-BOLL-SDNL-I
    private PcoDtUltBollSdnlI pcoDtUltBollSdnlI = new PcoDtUltBollSdnlI();
    //Original name: PCO-DT-ULT-BOLL-PERF-I
    private PcoDtUltBollPerfI pcoDtUltBollPerfI = new PcoDtUltBollPerfI();
    //Original name: PCO-DT-RICL-RIRIAS-COM
    private PcoDtRiclRiriasCom pcoDtRiclRiriasCom = new PcoDtRiclRiriasCom();
    //Original name: PCO-DT-ULT-ELAB-AT92-C
    private PcoDtUltElabAt92C pcoDtUltElabAt92C = new PcoDtUltElabAt92C();
    //Original name: PCO-DT-ULT-ELAB-AT92-I
    private PcoDtUltElabAt92I pcoDtUltElabAt92I = new PcoDtUltElabAt92I();
    //Original name: PCO-DT-ULT-ELAB-AT93-C
    private PcoDtUltElabAt93C pcoDtUltElabAt93C = new PcoDtUltElabAt93C();
    //Original name: PCO-DT-ULT-ELAB-AT93-I
    private PcoDtUltElabAt93I pcoDtUltElabAt93I = new PcoDtUltElabAt93I();
    //Original name: PCO-DT-ULT-ELAB-SPE-IN
    private PcoDtUltElabSpeIn pcoDtUltElabSpeIn = new PcoDtUltElabSpeIn();
    //Original name: PCO-DT-ULT-ELAB-PR-CON
    private PcoDtUltElabPrCon pcoDtUltElabPrCon = new PcoDtUltElabPrCon();
    //Original name: PCO-DT-ULT-BOLL-RP-CL
    private PcoDtUltBollRpCl pcoDtUltBollRpCl = new PcoDtUltBollRpCl();
    //Original name: PCO-DT-ULT-BOLL-RP-IN
    private PcoDtUltBollRpIn pcoDtUltBollRpIn = new PcoDtUltBollRpIn();
    //Original name: PCO-DT-ULT-BOLL-PRE-I
    private PcoDtUltBollPreI pcoDtUltBollPreI = new PcoDtUltBollPreI();
    //Original name: PCO-DT-ULT-BOLL-PRE-C
    private PcoDtUltBollPreC pcoDtUltBollPreC = new PcoDtUltBollPreC();
    //Original name: PCO-DT-ULTC-PILDI-MM-C
    private PcoDtUltcPildiMmC pcoDtUltcPildiMmC = new PcoDtUltcPildiMmC();
    //Original name: PCO-DT-ULTC-PILDI-AA-C
    private PcoDtUltcPildiAaC pcoDtUltcPildiAaC = new PcoDtUltcPildiAaC();
    //Original name: PCO-DT-ULTC-PILDI-MM-I
    private PcoDtUltcPildiMmI pcoDtUltcPildiMmI = new PcoDtUltcPildiMmI();
    //Original name: PCO-DT-ULTC-PILDI-TR-I
    private PcoDtUltcPildiTrI pcoDtUltcPildiTrI = new PcoDtUltcPildiTrI();
    //Original name: PCO-DT-ULTC-PILDI-AA-I
    private PcoDtUltcPildiAaI pcoDtUltcPildiAaI = new PcoDtUltcPildiAaI();
    //Original name: PCO-DT-ULT-BOLL-QUIE-C
    private PcoDtUltBollQuieC pcoDtUltBollQuieC = new PcoDtUltBollQuieC();
    //Original name: PCO-DT-ULT-BOLL-QUIE-I
    private PcoDtUltBollQuieI pcoDtUltBollQuieI = new PcoDtUltBollQuieI();
    //Original name: PCO-DT-ULT-BOLL-COTR-I
    private PcoDtUltBollCotrI pcoDtUltBollCotrI = new PcoDtUltBollCotrI();
    //Original name: PCO-DT-ULT-BOLL-COTR-C
    private PcoDtUltBollCotrC pcoDtUltBollCotrC = new PcoDtUltBollCotrC();
    //Original name: PCO-DT-ULT-BOLL-CORI-C
    private PcoDtUltBollCoriC pcoDtUltBollCoriC = new PcoDtUltBollCoriC();
    //Original name: PCO-DT-ULT-BOLL-CORI-I
    private PcoDtUltBollCoriI pcoDtUltBollCoriI = new PcoDtUltBollCoriI();
    //Original name: PCO-DS-OPER-SQL
    private char pcoDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: PCO-DS-VER
    private int pcoDsVer = DefaultValues.INT_VAL;
    //Original name: PCO-DS-TS-CPTZ
    private long pcoDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: PCO-DS-UTENTE
    private String pcoDsUtente = DefaultValues.stringVal(Len.PCO_DS_UTENTE);
    //Original name: PCO-DS-STATO-ELAB
    private char pcoDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: PCO-TP-VALZZ-DT-VLT
    private String pcoTpValzzDtVlt = DefaultValues.stringVal(Len.PCO_TP_VALZZ_DT_VLT);
    //Original name: PCO-FL-FRAZ-PROV-ACQ
    private char pcoFlFrazProvAcq = DefaultValues.CHAR_VAL;
    //Original name: PCO-DT-ULT-AGG-EROG-RE
    private PcoDtUltAggErogRe pcoDtUltAggErogRe = new PcoDtUltAggErogRe();
    //Original name: PCO-PC-RM-MARSOL
    private PcoPcRmMarsol pcoPcRmMarsol = new PcoPcRmMarsol();
    //Original name: PCO-PC-C-SUBRSH-MARSOL
    private PcoPcCSubrshMarsol pcoPcCSubrshMarsol = new PcoPcCSubrshMarsol();
    //Original name: PCO-COD-COMP-ISVAP
    private String pcoCodCompIsvap = DefaultValues.stringVal(Len.PCO_COD_COMP_ISVAP);
    //Original name: PCO-LM-RIS-CON-INT
    private PcoLmRisConInt pcoLmRisConInt = new PcoLmRisConInt();
    //Original name: PCO-LM-C-SUBRSH-CON-IN
    private PcoLmCSubrshConIn pcoLmCSubrshConIn = new PcoLmCSubrshConIn();
    //Original name: PCO-PC-GAR-NORISK-MARS
    private PcoPcGarNoriskMars pcoPcGarNoriskMars = new PcoPcGarNoriskMars();
    //Original name: PCO-CRZ-1A-RAT-INTR-PR
    private char pcoCrz1aRatIntrPr = DefaultValues.CHAR_VAL;
    //Original name: PCO-NUM-GG-ARR-INTR-PR
    private PcoNumGgArrIntrPr pcoNumGgArrIntrPr = new PcoNumGgArrIntrPr();
    //Original name: PCO-FL-VISUAL-VINPG
    private char pcoFlVisualVinpg = DefaultValues.CHAR_VAL;
    //Original name: PCO-DT-ULT-ESTRAZ-FUG
    private PcoDtUltEstrazFug pcoDtUltEstrazFug = new PcoDtUltEstrazFug();
    //Original name: PCO-DT-ULT-ELAB-PR-AUT
    private PcoDtUltElabPrAut pcoDtUltElabPrAut = new PcoDtUltElabPrAut();
    //Original name: PCO-DT-ULT-ELAB-COMMEF
    private PcoDtUltElabCommef pcoDtUltElabCommef = new PcoDtUltElabCommef();
    //Original name: PCO-DT-ULT-ELAB-LIQMEF
    private PcoDtUltElabLiqmef pcoDtUltElabLiqmef = new PcoDtUltElabLiqmef();
    //Original name: PCO-COD-FISC-MEF
    private String pcoCodFiscMef = DefaultValues.stringVal(Len.PCO_COD_FISC_MEF);
    //Original name: PCO-IMP-ASS-SOCIALE
    private PcoImpAssSociale pcoImpAssSociale = new PcoImpAssSociale();
    //Original name: PCO-MOD-COMNZ-INVST-SW
    private char pcoModComnzInvstSw = DefaultValues.CHAR_VAL;
    //Original name: PCO-DT-RIAT-RIASS-RSH
    private PcoDtRiatRiassRsh pcoDtRiatRiassRsh = new PcoDtRiatRiassRsh();
    //Original name: PCO-DT-RIAT-RIASS-COMM
    private PcoDtRiatRiassComm pcoDtRiatRiassComm = new PcoDtRiatRiassComm();
    //Original name: PCO-GG-INTR-RIT-PAG
    private PcoGgIntrRitPag pcoGgIntrRitPag = new PcoGgIntrRitPag();
    //Original name: PCO-DT-ULT-RINN-TAC
    private PcoDtUltRinnTac pcoDtUltRinnTac = new PcoDtUltRinnTac();
    //Original name: PCO-DESC-COMP-LEN
    private short pcoDescCompLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: PCO-DESC-COMP
    private String pcoDescComp = DefaultValues.stringVal(Len.PCO_DESC_COMP);
    //Original name: PCO-DT-ULT-EC-TCM-IND
    private PcoDtUltEcTcmInd pcoDtUltEcTcmInd = new PcoDtUltEcTcmInd();
    //Original name: PCO-DT-ULT-EC-TCM-COLL
    private PcoDtUltEcTcmColl pcoDtUltEcTcmColl = new PcoDtUltEcTcmColl();
    //Original name: PCO-DT-ULT-EC-MRM-IND
    private PcoDtUltEcMrmInd pcoDtUltEcMrmInd = new PcoDtUltEcMrmInd();
    //Original name: PCO-DT-ULT-EC-MRM-COLL
    private PcoDtUltEcMrmColl pcoDtUltEcMrmColl = new PcoDtUltEcMrmColl();
    //Original name: PCO-COD-COMP-LDAP
    private String pcoCodCompLdap = DefaultValues.stringVal(Len.PCO_COD_COMP_LDAP);
    //Original name: PCO-PC-RID-IMP-1382011
    private PcoPcRidImp1382011 pcoPcRidImp1382011 = new PcoPcRidImp1382011();
    //Original name: PCO-PC-RID-IMP-662014
    private PcoPcRidImp662014 pcoPcRidImp662014 = new PcoPcRidImp662014();
    //Original name: PCO-SOGL-AML-PRE-UNI
    private PcoSoglAmlPreUni pcoSoglAmlPreUni = new PcoSoglAmlPreUni();
    //Original name: PCO-SOGL-AML-PRE-PER
    private PcoSoglAmlPrePer pcoSoglAmlPrePer = new PcoSoglAmlPrePer();
    //Original name: PCO-COD-SOGG-FTZ-ASSTO
    private String pcoCodSoggFtzAssto = DefaultValues.stringVal(Len.PCO_COD_SOGG_FTZ_ASSTO);
    //Original name: PCO-DT-ULT-ELAB-REDPRO
    private PcoDtUltElabRedpro pcoDtUltElabRedpro = new PcoDtUltElabRedpro();
    //Original name: PCO-DT-ULT-ELAB-TAKE-P
    private PcoDtUltElabTakeP pcoDtUltElabTakeP = new PcoDtUltElabTakeP();
    //Original name: PCO-DT-ULT-ELAB-PASPAS
    private PcoDtUltElabPaspas pcoDtUltElabPaspas = new PcoDtUltElabPaspas();
    //Original name: PCO-SOGL-AML-PRE-SAV-R
    private PcoSoglAmlPreSavR pcoSoglAmlPreSavR = new PcoSoglAmlPreSavR();
    //Original name: PCO-DT-ULT-ESTR-DEC-CO
    private PcoDtUltEstrDecCo pcoDtUltEstrDecCo = new PcoDtUltEstrDecCo();
    //Original name: PCO-DT-ULT-ELAB-COS-AT
    private PcoDtUltElabCosAt pcoDtUltElabCosAt = new PcoDtUltElabCosAt();
    //Original name: PCO-FRQ-COSTI-ATT
    private PcoFrqCostiAtt pcoFrqCostiAtt = new PcoFrqCostiAtt();
    //Original name: PCO-DT-ULT-ELAB-COS-ST
    private PcoDtUltElabCosSt pcoDtUltElabCosSt = new PcoDtUltElabCosSt();
    //Original name: PCO-FRQ-COSTI-STORNATI
    private PcoFrqCostiStornati pcoFrqCostiStornati = new PcoFrqCostiStornati();
    //Original name: PCO-DT-ESTR-ASS-MIN70A
    private PcoDtEstrAssMin70a pcoDtEstrAssMin70a = new PcoDtEstrAssMin70a();
    //Original name: PCO-DT-ESTR-ASS-MAG70A
    private PcoDtEstrAssMag70a pcoDtEstrAssMag70a = new PcoDtEstrAssMag70a();

    //==== METHODS ====
    public void setParamCompFormatted(String data) {
        byte[] buffer = new byte[Len.PARAM_COMP];
        MarshalByte.writeString(buffer, 1, data, Len.PARAM_COMP);
        setParamCompBytes(buffer, 1);
    }

    public String getParamCompFormatted() {
        return MarshalByteExt.bufferToStr(getParamCompBytes());
    }

    public byte[] getParamCompBytes() {
        byte[] buffer = new byte[Len.PARAM_COMP];
        return getParamCompBytes(buffer, 1);
    }

    public void setParamCompBytes(byte[] buffer, int offset) {
        int position = offset;
        pcoCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PCO_COD_COMP_ANIA, 0);
        position += Len.PCO_COD_COMP_ANIA;
        pcoCodTratCirt = MarshalByte.readString(buffer, position, Len.PCO_COD_TRAT_CIRT);
        position += Len.PCO_COD_TRAT_CIRT;
        pcoLimVltr.setPcoLimVltrFromBuffer(buffer, position);
        position += PcoLimVltr.Len.PCO_LIM_VLTR;
        pcoTpRatPerf = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcoTpLivGenzTit = MarshalByte.readString(buffer, position, Len.PCO_TP_LIV_GENZ_TIT);
        position += Len.PCO_TP_LIV_GENZ_TIT;
        pcoArrotPre.setPcoArrotPreFromBuffer(buffer, position);
        position += PcoArrotPre.Len.PCO_ARROT_PRE;
        pcoDtCont.setPcoDtContFromBuffer(buffer, position);
        position += PcoDtCont.Len.PCO_DT_CONT;
        pcoDtUltRivalIn.setPcoDtUltRivalInFromBuffer(buffer, position);
        position += PcoDtUltRivalIn.Len.PCO_DT_ULT_RIVAL_IN;
        pcoDtUltQtzoIn.setPcoDtUltQtzoInFromBuffer(buffer, position);
        position += PcoDtUltQtzoIn.Len.PCO_DT_ULT_QTZO_IN;
        pcoDtUltRiclRiass.setPcoDtUltRiclRiassFromBuffer(buffer, position);
        position += PcoDtUltRiclRiass.Len.PCO_DT_ULT_RICL_RIASS;
        pcoDtUltTabulRiass.setPcoDtUltTabulRiassFromBuffer(buffer, position);
        position += PcoDtUltTabulRiass.Len.PCO_DT_ULT_TABUL_RIASS;
        pcoDtUltBollEmes.setPcoDtUltBollEmesFromBuffer(buffer, position);
        position += PcoDtUltBollEmes.Len.PCO_DT_ULT_BOLL_EMES;
        pcoDtUltBollStor.setPcoDtUltBollStorFromBuffer(buffer, position);
        position += PcoDtUltBollStor.Len.PCO_DT_ULT_BOLL_STOR;
        pcoDtUltBollLiq.setPcoDtUltBollLiqFromBuffer(buffer, position);
        position += PcoDtUltBollLiq.Len.PCO_DT_ULT_BOLL_LIQ;
        pcoDtUltBollRiat.setPcoDtUltBollRiatFromBuffer(buffer, position);
        position += PcoDtUltBollRiat.Len.PCO_DT_ULT_BOLL_RIAT;
        pcoDtUltelriscparPr.setPcoDtUltelriscparPrFromBuffer(buffer, position);
        position += PcoDtUltelriscparPr.Len.PCO_DT_ULTELRISCPAR_PR;
        pcoDtUltcIsIn.setPcoDtUltcIsInFromBuffer(buffer, position);
        position += PcoDtUltcIsIn.Len.PCO_DT_ULTC_IS_IN;
        pcoDtUltRiclPre.setPcoDtUltRiclPreFromBuffer(buffer, position);
        position += PcoDtUltRiclPre.Len.PCO_DT_ULT_RICL_PRE;
        pcoDtUltcMarsol.setPcoDtUltcMarsolFromBuffer(buffer, position);
        position += PcoDtUltcMarsol.Len.PCO_DT_ULTC_MARSOL;
        pcoDtUltcRbIn.setPcoDtUltcRbInFromBuffer(buffer, position);
        position += PcoDtUltcRbIn.Len.PCO_DT_ULTC_RB_IN;
        pcoPcProv1aaAcq.setPcoPcProv1aaAcqFromBuffer(buffer, position);
        position += PcoPcProv1aaAcq.Len.PCO_PC_PROV1AA_ACQ;
        pcoModIntrPrest = MarshalByte.readString(buffer, position, Len.PCO_MOD_INTR_PREST);
        position += Len.PCO_MOD_INTR_PREST;
        pcoGgMaxRecProv.setPcoGgMaxRecProvFromBuffer(buffer, position);
        position += PcoGgMaxRecProv.Len.PCO_GG_MAX_REC_PROV;
        pcoDtUltgzTrchEIn.setPcoDtUltgzTrchEInFromBuffer(buffer, position);
        position += PcoDtUltgzTrchEIn.Len.PCO_DT_ULTGZ_TRCH_E_IN;
        pcoDtUltBollSnden.setPcoDtUltBollSndenFromBuffer(buffer, position);
        position += PcoDtUltBollSnden.Len.PCO_DT_ULT_BOLL_SNDEN;
        pcoDtUltBollSndnlq.setPcoDtUltBollSndnlqFromBuffer(buffer, position);
        position += PcoDtUltBollSndnlq.Len.PCO_DT_ULT_BOLL_SNDNLQ;
        pcoDtUltscElabIn.setPcoDtUltscElabInFromBuffer(buffer, position);
        position += PcoDtUltscElabIn.Len.PCO_DT_ULTSC_ELAB_IN;
        pcoDtUltscOpzIn.setPcoDtUltscOpzInFromBuffer(buffer, position);
        position += PcoDtUltscOpzIn.Len.PCO_DT_ULTSC_OPZ_IN;
        pcoDtUltcBnsricIn.setPcoDtUltcBnsricInFromBuffer(buffer, position);
        position += PcoDtUltcBnsricIn.Len.PCO_DT_ULTC_BNSRIC_IN;
        pcoDtUltcBnsfdtIn.setPcoDtUltcBnsfdtInFromBuffer(buffer, position);
        position += PcoDtUltcBnsfdtIn.Len.PCO_DT_ULTC_BNSFDT_IN;
        pcoDtUltRinnGarac.setPcoDtUltRinnGaracFromBuffer(buffer, position);
        position += PcoDtUltRinnGarac.Len.PCO_DT_ULT_RINN_GARAC;
        pcoDtUltgzCed.setPcoDtUltgzCedFromBuffer(buffer, position);
        position += PcoDtUltgzCed.Len.PCO_DT_ULTGZ_CED;
        pcoDtUltElabPrlcos.setPcoDtUltElabPrlcosFromBuffer(buffer, position);
        position += PcoDtUltElabPrlcos.Len.PCO_DT_ULT_ELAB_PRLCOS;
        pcoDtUltRinnColl.setPcoDtUltRinnCollFromBuffer(buffer, position);
        position += PcoDtUltRinnColl.Len.PCO_DT_ULT_RINN_COLL;
        pcoFlRvcPerf = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcoFlRcsPoliNoperf = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcoFlGestPlusv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcoDtUltRivalCl.setPcoDtUltRivalClFromBuffer(buffer, position);
        position += PcoDtUltRivalCl.Len.PCO_DT_ULT_RIVAL_CL;
        pcoDtUltQtzoCl.setPcoDtUltQtzoClFromBuffer(buffer, position);
        position += PcoDtUltQtzoCl.Len.PCO_DT_ULT_QTZO_CL;
        pcoDtUltcBnsricCl.setPcoDtUltcBnsricClFromBuffer(buffer, position);
        position += PcoDtUltcBnsricCl.Len.PCO_DT_ULTC_BNSRIC_CL;
        pcoDtUltcBnsfdtCl.setPcoDtUltcBnsfdtClFromBuffer(buffer, position);
        position += PcoDtUltcBnsfdtCl.Len.PCO_DT_ULTC_BNSFDT_CL;
        pcoDtUltcIsCl.setPcoDtUltcIsClFromBuffer(buffer, position);
        position += PcoDtUltcIsCl.Len.PCO_DT_ULTC_IS_CL;
        pcoDtUltcRbCl.setPcoDtUltcRbClFromBuffer(buffer, position);
        position += PcoDtUltcRbCl.Len.PCO_DT_ULTC_RB_CL;
        pcoDtUltgzTrchECl.setPcoDtUltgzTrchEClFromBuffer(buffer, position);
        position += PcoDtUltgzTrchECl.Len.PCO_DT_ULTGZ_TRCH_E_CL;
        pcoDtUltscElabCl.setPcoDtUltscElabClFromBuffer(buffer, position);
        position += PcoDtUltscElabCl.Len.PCO_DT_ULTSC_ELAB_CL;
        pcoDtUltscOpzCl.setPcoDtUltscOpzClFromBuffer(buffer, position);
        position += PcoDtUltscOpzCl.Len.PCO_DT_ULTSC_OPZ_CL;
        pcoStstXRegione.setPcoStstXRegioneFromBuffer(buffer, position);
        position += PcoStstXRegione.Len.PCO_STST_X_REGIONE;
        pcoDtUltgzCedColl.setPcoDtUltgzCedCollFromBuffer(buffer, position);
        position += PcoDtUltgzCedColl.Len.PCO_DT_ULTGZ_CED_COLL;
        pcoTpModRival = MarshalByte.readString(buffer, position, Len.PCO_TP_MOD_RIVAL);
        position += Len.PCO_TP_MOD_RIVAL;
        pcoNumMmCalcMora.setPcoNumMmCalcMoraFromBuffer(buffer, position);
        position += PcoNumMmCalcMora.Len.PCO_NUM_MM_CALC_MORA;
        pcoDtUltEcRivColl.setPcoDtUltEcRivCollFromBuffer(buffer, position);
        position += PcoDtUltEcRivColl.Len.PCO_DT_ULT_EC_RIV_COLL;
        pcoDtUltEcRivInd.setPcoDtUltEcRivIndFromBuffer(buffer, position);
        position += PcoDtUltEcRivInd.Len.PCO_DT_ULT_EC_RIV_IND;
        pcoDtUltEcIlColl.setPcoDtUltEcIlCollFromBuffer(buffer, position);
        position += PcoDtUltEcIlColl.Len.PCO_DT_ULT_EC_IL_COLL;
        pcoDtUltEcIlInd.setPcoDtUltEcIlIndFromBuffer(buffer, position);
        position += PcoDtUltEcIlInd.Len.PCO_DT_ULT_EC_IL_IND;
        pcoDtUltEcUlColl.setPcoDtUltEcUlCollFromBuffer(buffer, position);
        position += PcoDtUltEcUlColl.Len.PCO_DT_ULT_EC_UL_COLL;
        pcoDtUltEcUlInd.setPcoDtUltEcUlIndFromBuffer(buffer, position);
        position += PcoDtUltEcUlInd.Len.PCO_DT_ULT_EC_UL_IND;
        pcoAaUti.setPcoAaUtiFromBuffer(buffer, position);
        position += PcoAaUti.Len.PCO_AA_UTI;
        pcoCalcRshComun = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcoFlLivDebug = MarshalByte.readPackedAsShort(buffer, position, Len.Int.PCO_FL_LIV_DEBUG, 0);
        position += Len.PCO_FL_LIV_DEBUG;
        pcoDtUltBollPerfC.setPcoDtUltBollPerfCFromBuffer(buffer, position);
        position += PcoDtUltBollPerfC.Len.PCO_DT_ULT_BOLL_PERF_C;
        pcoDtUltBollRspIn.setPcoDtUltBollRspInFromBuffer(buffer, position);
        position += PcoDtUltBollRspIn.Len.PCO_DT_ULT_BOLL_RSP_IN;
        pcoDtUltBollRspCl.setPcoDtUltBollRspClFromBuffer(buffer, position);
        position += PcoDtUltBollRspCl.Len.PCO_DT_ULT_BOLL_RSP_CL;
        pcoDtUltBollEmesI.setPcoDtUltBollEmesIFromBuffer(buffer, position);
        position += PcoDtUltBollEmesI.Len.PCO_DT_ULT_BOLL_EMES_I;
        pcoDtUltBollStorI.setPcoDtUltBollStorIFromBuffer(buffer, position);
        position += PcoDtUltBollStorI.Len.PCO_DT_ULT_BOLL_STOR_I;
        pcoDtUltBollRiatI.setPcoDtUltBollRiatIFromBuffer(buffer, position);
        position += PcoDtUltBollRiatI.Len.PCO_DT_ULT_BOLL_RIAT_I;
        pcoDtUltBollSdI.setPcoDtUltBollSdIFromBuffer(buffer, position);
        position += PcoDtUltBollSdI.Len.PCO_DT_ULT_BOLL_SD_I;
        pcoDtUltBollSdnlI.setPcoDtUltBollSdnlIFromBuffer(buffer, position);
        position += PcoDtUltBollSdnlI.Len.PCO_DT_ULT_BOLL_SDNL_I;
        pcoDtUltBollPerfI.setPcoDtUltBollPerfIFromBuffer(buffer, position);
        position += PcoDtUltBollPerfI.Len.PCO_DT_ULT_BOLL_PERF_I;
        pcoDtRiclRiriasCom.setPcoDtRiclRiriasComFromBuffer(buffer, position);
        position += PcoDtRiclRiriasCom.Len.PCO_DT_RICL_RIRIAS_COM;
        pcoDtUltElabAt92C.setPcoDtUltElabAt92CFromBuffer(buffer, position);
        position += PcoDtUltElabAt92C.Len.PCO_DT_ULT_ELAB_AT92_C;
        pcoDtUltElabAt92I.setPcoDtUltElabAt92IFromBuffer(buffer, position);
        position += PcoDtUltElabAt92I.Len.PCO_DT_ULT_ELAB_AT92_I;
        pcoDtUltElabAt93C.setPcoDtUltElabAt93CFromBuffer(buffer, position);
        position += PcoDtUltElabAt93C.Len.PCO_DT_ULT_ELAB_AT93_C;
        pcoDtUltElabAt93I.setPcoDtUltElabAt93IFromBuffer(buffer, position);
        position += PcoDtUltElabAt93I.Len.PCO_DT_ULT_ELAB_AT93_I;
        pcoDtUltElabSpeIn.setPcoDtUltElabSpeInFromBuffer(buffer, position);
        position += PcoDtUltElabSpeIn.Len.PCO_DT_ULT_ELAB_SPE_IN;
        pcoDtUltElabPrCon.setPcoDtUltElabPrConFromBuffer(buffer, position);
        position += PcoDtUltElabPrCon.Len.PCO_DT_ULT_ELAB_PR_CON;
        pcoDtUltBollRpCl.setPcoDtUltBollRpClFromBuffer(buffer, position);
        position += PcoDtUltBollRpCl.Len.PCO_DT_ULT_BOLL_RP_CL;
        pcoDtUltBollRpIn.setPcoDtUltBollRpInFromBuffer(buffer, position);
        position += PcoDtUltBollRpIn.Len.PCO_DT_ULT_BOLL_RP_IN;
        pcoDtUltBollPreI.setPcoDtUltBollPreIFromBuffer(buffer, position);
        position += PcoDtUltBollPreI.Len.PCO_DT_ULT_BOLL_PRE_I;
        pcoDtUltBollPreC.setPcoDtUltBollPreCFromBuffer(buffer, position);
        position += PcoDtUltBollPreC.Len.PCO_DT_ULT_BOLL_PRE_C;
        pcoDtUltcPildiMmC.setPcoDtUltcPildiMmCFromBuffer(buffer, position);
        position += PcoDtUltcPildiMmC.Len.PCO_DT_ULTC_PILDI_MM_C;
        pcoDtUltcPildiAaC.setPcoDtUltcPildiAaCFromBuffer(buffer, position);
        position += PcoDtUltcPildiAaC.Len.PCO_DT_ULTC_PILDI_AA_C;
        pcoDtUltcPildiMmI.setPcoDtUltcPildiMmIFromBuffer(buffer, position);
        position += PcoDtUltcPildiMmI.Len.PCO_DT_ULTC_PILDI_MM_I;
        pcoDtUltcPildiTrI.setPcoDtUltcPildiTrIFromBuffer(buffer, position);
        position += PcoDtUltcPildiTrI.Len.PCO_DT_ULTC_PILDI_TR_I;
        pcoDtUltcPildiAaI.setPcoDtUltcPildiAaIFromBuffer(buffer, position);
        position += PcoDtUltcPildiAaI.Len.PCO_DT_ULTC_PILDI_AA_I;
        pcoDtUltBollQuieC.setPcoDtUltBollQuieCFromBuffer(buffer, position);
        position += PcoDtUltBollQuieC.Len.PCO_DT_ULT_BOLL_QUIE_C;
        pcoDtUltBollQuieI.setPcoDtUltBollQuieIFromBuffer(buffer, position);
        position += PcoDtUltBollQuieI.Len.PCO_DT_ULT_BOLL_QUIE_I;
        pcoDtUltBollCotrI.setPcoDtUltBollCotrIFromBuffer(buffer, position);
        position += PcoDtUltBollCotrI.Len.PCO_DT_ULT_BOLL_COTR_I;
        pcoDtUltBollCotrC.setPcoDtUltBollCotrCFromBuffer(buffer, position);
        position += PcoDtUltBollCotrC.Len.PCO_DT_ULT_BOLL_COTR_C;
        pcoDtUltBollCoriC.setPcoDtUltBollCoriCFromBuffer(buffer, position);
        position += PcoDtUltBollCoriC.Len.PCO_DT_ULT_BOLL_CORI_C;
        pcoDtUltBollCoriI.setPcoDtUltBollCoriIFromBuffer(buffer, position);
        position += PcoDtUltBollCoriI.Len.PCO_DT_ULT_BOLL_CORI_I;
        pcoDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcoDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PCO_DS_VER, 0);
        position += Len.PCO_DS_VER;
        pcoDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PCO_DS_TS_CPTZ, 0);
        position += Len.PCO_DS_TS_CPTZ;
        pcoDsUtente = MarshalByte.readString(buffer, position, Len.PCO_DS_UTENTE);
        position += Len.PCO_DS_UTENTE;
        pcoDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcoTpValzzDtVlt = MarshalByte.readString(buffer, position, Len.PCO_TP_VALZZ_DT_VLT);
        position += Len.PCO_TP_VALZZ_DT_VLT;
        pcoFlFrazProvAcq = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcoDtUltAggErogRe.setPcoDtUltAggErogReFromBuffer(buffer, position);
        position += PcoDtUltAggErogRe.Len.PCO_DT_ULT_AGG_EROG_RE;
        pcoPcRmMarsol.setPcoPcRmMarsolFromBuffer(buffer, position);
        position += PcoPcRmMarsol.Len.PCO_PC_RM_MARSOL;
        pcoPcCSubrshMarsol.setPcoPcCSubrshMarsolFromBuffer(buffer, position);
        position += PcoPcCSubrshMarsol.Len.PCO_PC_C_SUBRSH_MARSOL;
        pcoCodCompIsvap = MarshalByte.readString(buffer, position, Len.PCO_COD_COMP_ISVAP);
        position += Len.PCO_COD_COMP_ISVAP;
        pcoLmRisConInt.setPcoLmRisConIntFromBuffer(buffer, position);
        position += PcoLmRisConInt.Len.PCO_LM_RIS_CON_INT;
        pcoLmCSubrshConIn.setPcoLmCSubrshConInFromBuffer(buffer, position);
        position += PcoLmCSubrshConIn.Len.PCO_LM_C_SUBRSH_CON_IN;
        pcoPcGarNoriskMars.setPcoPcGarNoriskMarsFromBuffer(buffer, position);
        position += PcoPcGarNoriskMars.Len.PCO_PC_GAR_NORISK_MARS;
        pcoCrz1aRatIntrPr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcoNumGgArrIntrPr.setPcoNumGgArrIntrPrFromBuffer(buffer, position);
        position += PcoNumGgArrIntrPr.Len.PCO_NUM_GG_ARR_INTR_PR;
        pcoFlVisualVinpg = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcoDtUltEstrazFug.setPcoDtUltEstrazFugFromBuffer(buffer, position);
        position += PcoDtUltEstrazFug.Len.PCO_DT_ULT_ESTRAZ_FUG;
        pcoDtUltElabPrAut.setPcoDtUltElabPrAutFromBuffer(buffer, position);
        position += PcoDtUltElabPrAut.Len.PCO_DT_ULT_ELAB_PR_AUT;
        pcoDtUltElabCommef.setPcoDtUltElabCommefFromBuffer(buffer, position);
        position += PcoDtUltElabCommef.Len.PCO_DT_ULT_ELAB_COMMEF;
        pcoDtUltElabLiqmef.setPcoDtUltElabLiqmefFromBuffer(buffer, position);
        position += PcoDtUltElabLiqmef.Len.PCO_DT_ULT_ELAB_LIQMEF;
        pcoCodFiscMef = MarshalByte.readString(buffer, position, Len.PCO_COD_FISC_MEF);
        position += Len.PCO_COD_FISC_MEF;
        pcoImpAssSociale.setPcoImpAssSocialeFromBuffer(buffer, position);
        position += PcoImpAssSociale.Len.PCO_IMP_ASS_SOCIALE;
        pcoModComnzInvstSw = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcoDtRiatRiassRsh.setPcoDtRiatRiassRshFromBuffer(buffer, position);
        position += PcoDtRiatRiassRsh.Len.PCO_DT_RIAT_RIASS_RSH;
        pcoDtRiatRiassComm.setPcoDtRiatRiassCommFromBuffer(buffer, position);
        position += PcoDtRiatRiassComm.Len.PCO_DT_RIAT_RIASS_COMM;
        pcoGgIntrRitPag.setPcoGgIntrRitPagFromBuffer(buffer, position);
        position += PcoGgIntrRitPag.Len.PCO_GG_INTR_RIT_PAG;
        pcoDtUltRinnTac.setPcoDtUltRinnTacFromBuffer(buffer, position);
        position += PcoDtUltRinnTac.Len.PCO_DT_ULT_RINN_TAC;
        setPcoDescCompVcharBytes(buffer, position);
        position += Len.PCO_DESC_COMP_VCHAR;
        pcoDtUltEcTcmInd.setPcoDtUltEcTcmIndFromBuffer(buffer, position);
        position += PcoDtUltEcTcmInd.Len.PCO_DT_ULT_EC_TCM_IND;
        pcoDtUltEcTcmColl.setPcoDtUltEcTcmCollFromBuffer(buffer, position);
        position += PcoDtUltEcTcmColl.Len.PCO_DT_ULT_EC_TCM_COLL;
        pcoDtUltEcMrmInd.setPcoDtUltEcMrmIndFromBuffer(buffer, position);
        position += PcoDtUltEcMrmInd.Len.PCO_DT_ULT_EC_MRM_IND;
        pcoDtUltEcMrmColl.setPcoDtUltEcMrmCollFromBuffer(buffer, position);
        position += PcoDtUltEcMrmColl.Len.PCO_DT_ULT_EC_MRM_COLL;
        pcoCodCompLdap = MarshalByte.readString(buffer, position, Len.PCO_COD_COMP_LDAP);
        position += Len.PCO_COD_COMP_LDAP;
        pcoPcRidImp1382011.setPcoPcRidImp1382011FromBuffer(buffer, position);
        position += PcoPcRidImp1382011.Len.PCO_PC_RID_IMP1382011;
        pcoPcRidImp662014.setPcoPcRidImp662014FromBuffer(buffer, position);
        position += PcoPcRidImp662014.Len.PCO_PC_RID_IMP662014;
        pcoSoglAmlPreUni.setPcoSoglAmlPreUniFromBuffer(buffer, position);
        position += PcoSoglAmlPreUni.Len.PCO_SOGL_AML_PRE_UNI;
        pcoSoglAmlPrePer.setPcoSoglAmlPrePerFromBuffer(buffer, position);
        position += PcoSoglAmlPrePer.Len.PCO_SOGL_AML_PRE_PER;
        pcoCodSoggFtzAssto = MarshalByte.readString(buffer, position, Len.PCO_COD_SOGG_FTZ_ASSTO);
        position += Len.PCO_COD_SOGG_FTZ_ASSTO;
        pcoDtUltElabRedpro.setPcoDtUltElabRedproFromBuffer(buffer, position);
        position += PcoDtUltElabRedpro.Len.PCO_DT_ULT_ELAB_REDPRO;
        pcoDtUltElabTakeP.setPcoDtUltElabTakePFromBuffer(buffer, position);
        position += PcoDtUltElabTakeP.Len.PCO_DT_ULT_ELAB_TAKE_P;
        pcoDtUltElabPaspas.setPcoDtUltElabPaspasFromBuffer(buffer, position);
        position += PcoDtUltElabPaspas.Len.PCO_DT_ULT_ELAB_PASPAS;
        pcoSoglAmlPreSavR.setPcoSoglAmlPreSavRFromBuffer(buffer, position);
        position += PcoSoglAmlPreSavR.Len.PCO_SOGL_AML_PRE_SAV_R;
        pcoDtUltEstrDecCo.setPcoDtUltEstrDecCoFromBuffer(buffer, position);
        position += PcoDtUltEstrDecCo.Len.PCO_DT_ULT_ESTR_DEC_CO;
        pcoDtUltElabCosAt.setPcoDtUltElabCosAtFromBuffer(buffer, position);
        position += PcoDtUltElabCosAt.Len.PCO_DT_ULT_ELAB_COS_AT;
        pcoFrqCostiAtt.setPcoFrqCostiAttFromBuffer(buffer, position);
        position += PcoFrqCostiAtt.Len.PCO_FRQ_COSTI_ATT;
        pcoDtUltElabCosSt.setPcoDtUltElabCosStFromBuffer(buffer, position);
        position += PcoDtUltElabCosSt.Len.PCO_DT_ULT_ELAB_COS_ST;
        pcoFrqCostiStornati.setPcoFrqCostiStornatiFromBuffer(buffer, position);
        position += PcoFrqCostiStornati.Len.PCO_FRQ_COSTI_STORNATI;
        pcoDtEstrAssMin70a.setPcoDtEstrAssMin70aFromBuffer(buffer, position);
        position += PcoDtEstrAssMin70a.Len.PCO_DT_ESTR_ASS_MIN70A;
        pcoDtEstrAssMag70a.setPcoDtEstrAssMag70aFromBuffer(buffer, position);
    }

    public byte[] getParamCompBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, pcoCodCompAnia, Len.Int.PCO_COD_COMP_ANIA, 0);
        position += Len.PCO_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, pcoCodTratCirt, Len.PCO_COD_TRAT_CIRT);
        position += Len.PCO_COD_TRAT_CIRT;
        pcoLimVltr.getPcoLimVltrAsBuffer(buffer, position);
        position += PcoLimVltr.Len.PCO_LIM_VLTR;
        MarshalByte.writeChar(buffer, position, pcoTpRatPerf);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, pcoTpLivGenzTit, Len.PCO_TP_LIV_GENZ_TIT);
        position += Len.PCO_TP_LIV_GENZ_TIT;
        pcoArrotPre.getPcoArrotPreAsBuffer(buffer, position);
        position += PcoArrotPre.Len.PCO_ARROT_PRE;
        pcoDtCont.getPcoDtContAsBuffer(buffer, position);
        position += PcoDtCont.Len.PCO_DT_CONT;
        pcoDtUltRivalIn.getPcoDtUltRivalInAsBuffer(buffer, position);
        position += PcoDtUltRivalIn.Len.PCO_DT_ULT_RIVAL_IN;
        pcoDtUltQtzoIn.getPcoDtUltQtzoInAsBuffer(buffer, position);
        position += PcoDtUltQtzoIn.Len.PCO_DT_ULT_QTZO_IN;
        pcoDtUltRiclRiass.getPcoDtUltRiclRiassAsBuffer(buffer, position);
        position += PcoDtUltRiclRiass.Len.PCO_DT_ULT_RICL_RIASS;
        pcoDtUltTabulRiass.getPcoDtUltTabulRiassAsBuffer(buffer, position);
        position += PcoDtUltTabulRiass.Len.PCO_DT_ULT_TABUL_RIASS;
        pcoDtUltBollEmes.getPcoDtUltBollEmesAsBuffer(buffer, position);
        position += PcoDtUltBollEmes.Len.PCO_DT_ULT_BOLL_EMES;
        pcoDtUltBollStor.getPcoDtUltBollStorAsBuffer(buffer, position);
        position += PcoDtUltBollStor.Len.PCO_DT_ULT_BOLL_STOR;
        pcoDtUltBollLiq.getPcoDtUltBollLiqAsBuffer(buffer, position);
        position += PcoDtUltBollLiq.Len.PCO_DT_ULT_BOLL_LIQ;
        pcoDtUltBollRiat.getPcoDtUltBollRiatAsBuffer(buffer, position);
        position += PcoDtUltBollRiat.Len.PCO_DT_ULT_BOLL_RIAT;
        pcoDtUltelriscparPr.getPcoDtUltelriscparPrAsBuffer(buffer, position);
        position += PcoDtUltelriscparPr.Len.PCO_DT_ULTELRISCPAR_PR;
        pcoDtUltcIsIn.getPcoDtUltcIsInAsBuffer(buffer, position);
        position += PcoDtUltcIsIn.Len.PCO_DT_ULTC_IS_IN;
        pcoDtUltRiclPre.getPcoDtUltRiclPreAsBuffer(buffer, position);
        position += PcoDtUltRiclPre.Len.PCO_DT_ULT_RICL_PRE;
        pcoDtUltcMarsol.getPcoDtUltcMarsolAsBuffer(buffer, position);
        position += PcoDtUltcMarsol.Len.PCO_DT_ULTC_MARSOL;
        pcoDtUltcRbIn.getPcoDtUltcRbInAsBuffer(buffer, position);
        position += PcoDtUltcRbIn.Len.PCO_DT_ULTC_RB_IN;
        pcoPcProv1aaAcq.getPcoPcProv1aaAcqAsBuffer(buffer, position);
        position += PcoPcProv1aaAcq.Len.PCO_PC_PROV1AA_ACQ;
        MarshalByte.writeString(buffer, position, pcoModIntrPrest, Len.PCO_MOD_INTR_PREST);
        position += Len.PCO_MOD_INTR_PREST;
        pcoGgMaxRecProv.getPcoGgMaxRecProvAsBuffer(buffer, position);
        position += PcoGgMaxRecProv.Len.PCO_GG_MAX_REC_PROV;
        pcoDtUltgzTrchEIn.getPcoDtUltgzTrchEInAsBuffer(buffer, position);
        position += PcoDtUltgzTrchEIn.Len.PCO_DT_ULTGZ_TRCH_E_IN;
        pcoDtUltBollSnden.getPcoDtUltBollSndenAsBuffer(buffer, position);
        position += PcoDtUltBollSnden.Len.PCO_DT_ULT_BOLL_SNDEN;
        pcoDtUltBollSndnlq.getPcoDtUltBollSndnlqAsBuffer(buffer, position);
        position += PcoDtUltBollSndnlq.Len.PCO_DT_ULT_BOLL_SNDNLQ;
        pcoDtUltscElabIn.getPcoDtUltscElabInAsBuffer(buffer, position);
        position += PcoDtUltscElabIn.Len.PCO_DT_ULTSC_ELAB_IN;
        pcoDtUltscOpzIn.getPcoDtUltscOpzInAsBuffer(buffer, position);
        position += PcoDtUltscOpzIn.Len.PCO_DT_ULTSC_OPZ_IN;
        pcoDtUltcBnsricIn.getPcoDtUltcBnsricInAsBuffer(buffer, position);
        position += PcoDtUltcBnsricIn.Len.PCO_DT_ULTC_BNSRIC_IN;
        pcoDtUltcBnsfdtIn.getPcoDtUltcBnsfdtInAsBuffer(buffer, position);
        position += PcoDtUltcBnsfdtIn.Len.PCO_DT_ULTC_BNSFDT_IN;
        pcoDtUltRinnGarac.getPcoDtUltRinnGaracAsBuffer(buffer, position);
        position += PcoDtUltRinnGarac.Len.PCO_DT_ULT_RINN_GARAC;
        pcoDtUltgzCed.getPcoDtUltgzCedAsBuffer(buffer, position);
        position += PcoDtUltgzCed.Len.PCO_DT_ULTGZ_CED;
        pcoDtUltElabPrlcos.getPcoDtUltElabPrlcosAsBuffer(buffer, position);
        position += PcoDtUltElabPrlcos.Len.PCO_DT_ULT_ELAB_PRLCOS;
        pcoDtUltRinnColl.getPcoDtUltRinnCollAsBuffer(buffer, position);
        position += PcoDtUltRinnColl.Len.PCO_DT_ULT_RINN_COLL;
        MarshalByte.writeChar(buffer, position, pcoFlRvcPerf);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, pcoFlRcsPoliNoperf);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, pcoFlGestPlusv);
        position += Types.CHAR_SIZE;
        pcoDtUltRivalCl.getPcoDtUltRivalClAsBuffer(buffer, position);
        position += PcoDtUltRivalCl.Len.PCO_DT_ULT_RIVAL_CL;
        pcoDtUltQtzoCl.getPcoDtUltQtzoClAsBuffer(buffer, position);
        position += PcoDtUltQtzoCl.Len.PCO_DT_ULT_QTZO_CL;
        pcoDtUltcBnsricCl.getPcoDtUltcBnsricClAsBuffer(buffer, position);
        position += PcoDtUltcBnsricCl.Len.PCO_DT_ULTC_BNSRIC_CL;
        pcoDtUltcBnsfdtCl.getPcoDtUltcBnsfdtClAsBuffer(buffer, position);
        position += PcoDtUltcBnsfdtCl.Len.PCO_DT_ULTC_BNSFDT_CL;
        pcoDtUltcIsCl.getPcoDtUltcIsClAsBuffer(buffer, position);
        position += PcoDtUltcIsCl.Len.PCO_DT_ULTC_IS_CL;
        pcoDtUltcRbCl.getPcoDtUltcRbClAsBuffer(buffer, position);
        position += PcoDtUltcRbCl.Len.PCO_DT_ULTC_RB_CL;
        pcoDtUltgzTrchECl.getPcoDtUltgzTrchEClAsBuffer(buffer, position);
        position += PcoDtUltgzTrchECl.Len.PCO_DT_ULTGZ_TRCH_E_CL;
        pcoDtUltscElabCl.getPcoDtUltscElabClAsBuffer(buffer, position);
        position += PcoDtUltscElabCl.Len.PCO_DT_ULTSC_ELAB_CL;
        pcoDtUltscOpzCl.getPcoDtUltscOpzClAsBuffer(buffer, position);
        position += PcoDtUltscOpzCl.Len.PCO_DT_ULTSC_OPZ_CL;
        pcoStstXRegione.getPcoStstXRegioneAsBuffer(buffer, position);
        position += PcoStstXRegione.Len.PCO_STST_X_REGIONE;
        pcoDtUltgzCedColl.getPcoDtUltgzCedCollAsBuffer(buffer, position);
        position += PcoDtUltgzCedColl.Len.PCO_DT_ULTGZ_CED_COLL;
        MarshalByte.writeString(buffer, position, pcoTpModRival, Len.PCO_TP_MOD_RIVAL);
        position += Len.PCO_TP_MOD_RIVAL;
        pcoNumMmCalcMora.getPcoNumMmCalcMoraAsBuffer(buffer, position);
        position += PcoNumMmCalcMora.Len.PCO_NUM_MM_CALC_MORA;
        pcoDtUltEcRivColl.getPcoDtUltEcRivCollAsBuffer(buffer, position);
        position += PcoDtUltEcRivColl.Len.PCO_DT_ULT_EC_RIV_COLL;
        pcoDtUltEcRivInd.getPcoDtUltEcRivIndAsBuffer(buffer, position);
        position += PcoDtUltEcRivInd.Len.PCO_DT_ULT_EC_RIV_IND;
        pcoDtUltEcIlColl.getPcoDtUltEcIlCollAsBuffer(buffer, position);
        position += PcoDtUltEcIlColl.Len.PCO_DT_ULT_EC_IL_COLL;
        pcoDtUltEcIlInd.getPcoDtUltEcIlIndAsBuffer(buffer, position);
        position += PcoDtUltEcIlInd.Len.PCO_DT_ULT_EC_IL_IND;
        pcoDtUltEcUlColl.getPcoDtUltEcUlCollAsBuffer(buffer, position);
        position += PcoDtUltEcUlColl.Len.PCO_DT_ULT_EC_UL_COLL;
        pcoDtUltEcUlInd.getPcoDtUltEcUlIndAsBuffer(buffer, position);
        position += PcoDtUltEcUlInd.Len.PCO_DT_ULT_EC_UL_IND;
        pcoAaUti.getPcoAaUtiAsBuffer(buffer, position);
        position += PcoAaUti.Len.PCO_AA_UTI;
        MarshalByte.writeChar(buffer, position, pcoCalcRshComun);
        position += Types.CHAR_SIZE;
        MarshalByte.writeShortAsPacked(buffer, position, pcoFlLivDebug, Len.Int.PCO_FL_LIV_DEBUG, 0);
        position += Len.PCO_FL_LIV_DEBUG;
        pcoDtUltBollPerfC.getPcoDtUltBollPerfCAsBuffer(buffer, position);
        position += PcoDtUltBollPerfC.Len.PCO_DT_ULT_BOLL_PERF_C;
        pcoDtUltBollRspIn.getPcoDtUltBollRspInAsBuffer(buffer, position);
        position += PcoDtUltBollRspIn.Len.PCO_DT_ULT_BOLL_RSP_IN;
        pcoDtUltBollRspCl.getPcoDtUltBollRspClAsBuffer(buffer, position);
        position += PcoDtUltBollRspCl.Len.PCO_DT_ULT_BOLL_RSP_CL;
        pcoDtUltBollEmesI.getPcoDtUltBollEmesIAsBuffer(buffer, position);
        position += PcoDtUltBollEmesI.Len.PCO_DT_ULT_BOLL_EMES_I;
        pcoDtUltBollStorI.getPcoDtUltBollStorIAsBuffer(buffer, position);
        position += PcoDtUltBollStorI.Len.PCO_DT_ULT_BOLL_STOR_I;
        pcoDtUltBollRiatI.getPcoDtUltBollRiatIAsBuffer(buffer, position);
        position += PcoDtUltBollRiatI.Len.PCO_DT_ULT_BOLL_RIAT_I;
        pcoDtUltBollSdI.getPcoDtUltBollSdIAsBuffer(buffer, position);
        position += PcoDtUltBollSdI.Len.PCO_DT_ULT_BOLL_SD_I;
        pcoDtUltBollSdnlI.getPcoDtUltBollSdnlIAsBuffer(buffer, position);
        position += PcoDtUltBollSdnlI.Len.PCO_DT_ULT_BOLL_SDNL_I;
        pcoDtUltBollPerfI.getPcoDtUltBollPerfIAsBuffer(buffer, position);
        position += PcoDtUltBollPerfI.Len.PCO_DT_ULT_BOLL_PERF_I;
        pcoDtRiclRiriasCom.getPcoDtRiclRiriasComAsBuffer(buffer, position);
        position += PcoDtRiclRiriasCom.Len.PCO_DT_RICL_RIRIAS_COM;
        pcoDtUltElabAt92C.getPcoDtUltElabAt92CAsBuffer(buffer, position);
        position += PcoDtUltElabAt92C.Len.PCO_DT_ULT_ELAB_AT92_C;
        pcoDtUltElabAt92I.getPcoDtUltElabAt92IAsBuffer(buffer, position);
        position += PcoDtUltElabAt92I.Len.PCO_DT_ULT_ELAB_AT92_I;
        pcoDtUltElabAt93C.getPcoDtUltElabAt93CAsBuffer(buffer, position);
        position += PcoDtUltElabAt93C.Len.PCO_DT_ULT_ELAB_AT93_C;
        pcoDtUltElabAt93I.getPcoDtUltElabAt93IAsBuffer(buffer, position);
        position += PcoDtUltElabAt93I.Len.PCO_DT_ULT_ELAB_AT93_I;
        pcoDtUltElabSpeIn.getPcoDtUltElabSpeInAsBuffer(buffer, position);
        position += PcoDtUltElabSpeIn.Len.PCO_DT_ULT_ELAB_SPE_IN;
        pcoDtUltElabPrCon.getPcoDtUltElabPrConAsBuffer(buffer, position);
        position += PcoDtUltElabPrCon.Len.PCO_DT_ULT_ELAB_PR_CON;
        pcoDtUltBollRpCl.getPcoDtUltBollRpClAsBuffer(buffer, position);
        position += PcoDtUltBollRpCl.Len.PCO_DT_ULT_BOLL_RP_CL;
        pcoDtUltBollRpIn.getPcoDtUltBollRpInAsBuffer(buffer, position);
        position += PcoDtUltBollRpIn.Len.PCO_DT_ULT_BOLL_RP_IN;
        pcoDtUltBollPreI.getPcoDtUltBollPreIAsBuffer(buffer, position);
        position += PcoDtUltBollPreI.Len.PCO_DT_ULT_BOLL_PRE_I;
        pcoDtUltBollPreC.getPcoDtUltBollPreCAsBuffer(buffer, position);
        position += PcoDtUltBollPreC.Len.PCO_DT_ULT_BOLL_PRE_C;
        pcoDtUltcPildiMmC.getPcoDtUltcPildiMmCAsBuffer(buffer, position);
        position += PcoDtUltcPildiMmC.Len.PCO_DT_ULTC_PILDI_MM_C;
        pcoDtUltcPildiAaC.getPcoDtUltcPildiAaCAsBuffer(buffer, position);
        position += PcoDtUltcPildiAaC.Len.PCO_DT_ULTC_PILDI_AA_C;
        pcoDtUltcPildiMmI.getPcoDtUltcPildiMmIAsBuffer(buffer, position);
        position += PcoDtUltcPildiMmI.Len.PCO_DT_ULTC_PILDI_MM_I;
        pcoDtUltcPildiTrI.getPcoDtUltcPildiTrIAsBuffer(buffer, position);
        position += PcoDtUltcPildiTrI.Len.PCO_DT_ULTC_PILDI_TR_I;
        pcoDtUltcPildiAaI.getPcoDtUltcPildiAaIAsBuffer(buffer, position);
        position += PcoDtUltcPildiAaI.Len.PCO_DT_ULTC_PILDI_AA_I;
        pcoDtUltBollQuieC.getPcoDtUltBollQuieCAsBuffer(buffer, position);
        position += PcoDtUltBollQuieC.Len.PCO_DT_ULT_BOLL_QUIE_C;
        pcoDtUltBollQuieI.getPcoDtUltBollQuieIAsBuffer(buffer, position);
        position += PcoDtUltBollQuieI.Len.PCO_DT_ULT_BOLL_QUIE_I;
        pcoDtUltBollCotrI.getPcoDtUltBollCotrIAsBuffer(buffer, position);
        position += PcoDtUltBollCotrI.Len.PCO_DT_ULT_BOLL_COTR_I;
        pcoDtUltBollCotrC.getPcoDtUltBollCotrCAsBuffer(buffer, position);
        position += PcoDtUltBollCotrC.Len.PCO_DT_ULT_BOLL_COTR_C;
        pcoDtUltBollCoriC.getPcoDtUltBollCoriCAsBuffer(buffer, position);
        position += PcoDtUltBollCoriC.Len.PCO_DT_ULT_BOLL_CORI_C;
        pcoDtUltBollCoriI.getPcoDtUltBollCoriIAsBuffer(buffer, position);
        position += PcoDtUltBollCoriI.Len.PCO_DT_ULT_BOLL_CORI_I;
        MarshalByte.writeChar(buffer, position, pcoDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, pcoDsVer, Len.Int.PCO_DS_VER, 0);
        position += Len.PCO_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, pcoDsTsCptz, Len.Int.PCO_DS_TS_CPTZ, 0);
        position += Len.PCO_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, pcoDsUtente, Len.PCO_DS_UTENTE);
        position += Len.PCO_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, pcoDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, pcoTpValzzDtVlt, Len.PCO_TP_VALZZ_DT_VLT);
        position += Len.PCO_TP_VALZZ_DT_VLT;
        MarshalByte.writeChar(buffer, position, pcoFlFrazProvAcq);
        position += Types.CHAR_SIZE;
        pcoDtUltAggErogRe.getPcoDtUltAggErogReAsBuffer(buffer, position);
        position += PcoDtUltAggErogRe.Len.PCO_DT_ULT_AGG_EROG_RE;
        pcoPcRmMarsol.getPcoPcRmMarsolAsBuffer(buffer, position);
        position += PcoPcRmMarsol.Len.PCO_PC_RM_MARSOL;
        pcoPcCSubrshMarsol.getPcoPcCSubrshMarsolAsBuffer(buffer, position);
        position += PcoPcCSubrshMarsol.Len.PCO_PC_C_SUBRSH_MARSOL;
        MarshalByte.writeString(buffer, position, pcoCodCompIsvap, Len.PCO_COD_COMP_ISVAP);
        position += Len.PCO_COD_COMP_ISVAP;
        pcoLmRisConInt.getPcoLmRisConIntAsBuffer(buffer, position);
        position += PcoLmRisConInt.Len.PCO_LM_RIS_CON_INT;
        pcoLmCSubrshConIn.getPcoLmCSubrshConInAsBuffer(buffer, position);
        position += PcoLmCSubrshConIn.Len.PCO_LM_C_SUBRSH_CON_IN;
        pcoPcGarNoriskMars.getPcoPcGarNoriskMarsAsBuffer(buffer, position);
        position += PcoPcGarNoriskMars.Len.PCO_PC_GAR_NORISK_MARS;
        MarshalByte.writeChar(buffer, position, pcoCrz1aRatIntrPr);
        position += Types.CHAR_SIZE;
        pcoNumGgArrIntrPr.getPcoNumGgArrIntrPrAsBuffer(buffer, position);
        position += PcoNumGgArrIntrPr.Len.PCO_NUM_GG_ARR_INTR_PR;
        MarshalByte.writeChar(buffer, position, pcoFlVisualVinpg);
        position += Types.CHAR_SIZE;
        pcoDtUltEstrazFug.getPcoDtUltEstrazFugAsBuffer(buffer, position);
        position += PcoDtUltEstrazFug.Len.PCO_DT_ULT_ESTRAZ_FUG;
        pcoDtUltElabPrAut.getPcoDtUltElabPrAutAsBuffer(buffer, position);
        position += PcoDtUltElabPrAut.Len.PCO_DT_ULT_ELAB_PR_AUT;
        pcoDtUltElabCommef.getPcoDtUltElabCommefAsBuffer(buffer, position);
        position += PcoDtUltElabCommef.Len.PCO_DT_ULT_ELAB_COMMEF;
        pcoDtUltElabLiqmef.getPcoDtUltElabLiqmefAsBuffer(buffer, position);
        position += PcoDtUltElabLiqmef.Len.PCO_DT_ULT_ELAB_LIQMEF;
        MarshalByte.writeString(buffer, position, pcoCodFiscMef, Len.PCO_COD_FISC_MEF);
        position += Len.PCO_COD_FISC_MEF;
        pcoImpAssSociale.getPcoImpAssSocialeAsBuffer(buffer, position);
        position += PcoImpAssSociale.Len.PCO_IMP_ASS_SOCIALE;
        MarshalByte.writeChar(buffer, position, pcoModComnzInvstSw);
        position += Types.CHAR_SIZE;
        pcoDtRiatRiassRsh.getPcoDtRiatRiassRshAsBuffer(buffer, position);
        position += PcoDtRiatRiassRsh.Len.PCO_DT_RIAT_RIASS_RSH;
        pcoDtRiatRiassComm.getPcoDtRiatRiassCommAsBuffer(buffer, position);
        position += PcoDtRiatRiassComm.Len.PCO_DT_RIAT_RIASS_COMM;
        pcoGgIntrRitPag.getPcoGgIntrRitPagAsBuffer(buffer, position);
        position += PcoGgIntrRitPag.Len.PCO_GG_INTR_RIT_PAG;
        pcoDtUltRinnTac.getPcoDtUltRinnTacAsBuffer(buffer, position);
        position += PcoDtUltRinnTac.Len.PCO_DT_ULT_RINN_TAC;
        getPcoDescCompVcharBytes(buffer, position);
        position += Len.PCO_DESC_COMP_VCHAR;
        pcoDtUltEcTcmInd.getPcoDtUltEcTcmIndAsBuffer(buffer, position);
        position += PcoDtUltEcTcmInd.Len.PCO_DT_ULT_EC_TCM_IND;
        pcoDtUltEcTcmColl.getPcoDtUltEcTcmCollAsBuffer(buffer, position);
        position += PcoDtUltEcTcmColl.Len.PCO_DT_ULT_EC_TCM_COLL;
        pcoDtUltEcMrmInd.getPcoDtUltEcMrmIndAsBuffer(buffer, position);
        position += PcoDtUltEcMrmInd.Len.PCO_DT_ULT_EC_MRM_IND;
        pcoDtUltEcMrmColl.getPcoDtUltEcMrmCollAsBuffer(buffer, position);
        position += PcoDtUltEcMrmColl.Len.PCO_DT_ULT_EC_MRM_COLL;
        MarshalByte.writeString(buffer, position, pcoCodCompLdap, Len.PCO_COD_COMP_LDAP);
        position += Len.PCO_COD_COMP_LDAP;
        pcoPcRidImp1382011.getPcoPcRidImp1382011AsBuffer(buffer, position);
        position += PcoPcRidImp1382011.Len.PCO_PC_RID_IMP1382011;
        pcoPcRidImp662014.getPcoPcRidImp662014AsBuffer(buffer, position);
        position += PcoPcRidImp662014.Len.PCO_PC_RID_IMP662014;
        pcoSoglAmlPreUni.getPcoSoglAmlPreUniAsBuffer(buffer, position);
        position += PcoSoglAmlPreUni.Len.PCO_SOGL_AML_PRE_UNI;
        pcoSoglAmlPrePer.getPcoSoglAmlPrePerAsBuffer(buffer, position);
        position += PcoSoglAmlPrePer.Len.PCO_SOGL_AML_PRE_PER;
        MarshalByte.writeString(buffer, position, pcoCodSoggFtzAssto, Len.PCO_COD_SOGG_FTZ_ASSTO);
        position += Len.PCO_COD_SOGG_FTZ_ASSTO;
        pcoDtUltElabRedpro.getPcoDtUltElabRedproAsBuffer(buffer, position);
        position += PcoDtUltElabRedpro.Len.PCO_DT_ULT_ELAB_REDPRO;
        pcoDtUltElabTakeP.getPcoDtUltElabTakePAsBuffer(buffer, position);
        position += PcoDtUltElabTakeP.Len.PCO_DT_ULT_ELAB_TAKE_P;
        pcoDtUltElabPaspas.getPcoDtUltElabPaspasAsBuffer(buffer, position);
        position += PcoDtUltElabPaspas.Len.PCO_DT_ULT_ELAB_PASPAS;
        pcoSoglAmlPreSavR.getPcoSoglAmlPreSavRAsBuffer(buffer, position);
        position += PcoSoglAmlPreSavR.Len.PCO_SOGL_AML_PRE_SAV_R;
        pcoDtUltEstrDecCo.getPcoDtUltEstrDecCoAsBuffer(buffer, position);
        position += PcoDtUltEstrDecCo.Len.PCO_DT_ULT_ESTR_DEC_CO;
        pcoDtUltElabCosAt.getPcoDtUltElabCosAtAsBuffer(buffer, position);
        position += PcoDtUltElabCosAt.Len.PCO_DT_ULT_ELAB_COS_AT;
        pcoFrqCostiAtt.getPcoFrqCostiAttAsBuffer(buffer, position);
        position += PcoFrqCostiAtt.Len.PCO_FRQ_COSTI_ATT;
        pcoDtUltElabCosSt.getPcoDtUltElabCosStAsBuffer(buffer, position);
        position += PcoDtUltElabCosSt.Len.PCO_DT_ULT_ELAB_COS_ST;
        pcoFrqCostiStornati.getPcoFrqCostiStornatiAsBuffer(buffer, position);
        position += PcoFrqCostiStornati.Len.PCO_FRQ_COSTI_STORNATI;
        pcoDtEstrAssMin70a.getPcoDtEstrAssMin70aAsBuffer(buffer, position);
        position += PcoDtEstrAssMin70a.Len.PCO_DT_ESTR_ASS_MIN70A;
        pcoDtEstrAssMag70a.getPcoDtEstrAssMag70aAsBuffer(buffer, position);
        return buffer;
    }

    public void initParamCompHighValues() {
        pcoCodCompAnia = Types.HIGH_INT_VAL;
        pcoCodTratCirt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PCO_COD_TRAT_CIRT);
        pcoLimVltr.initPcoLimVltrHighValues();
        pcoTpRatPerf = Types.HIGH_CHAR_VAL;
        pcoTpLivGenzTit = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PCO_TP_LIV_GENZ_TIT);
        pcoArrotPre.initPcoArrotPreHighValues();
        pcoDtCont.initPcoDtContHighValues();
        pcoDtUltRivalIn.initPcoDtUltRivalInHighValues();
        pcoDtUltQtzoIn.initPcoDtUltQtzoInHighValues();
        pcoDtUltRiclRiass.initPcoDtUltRiclRiassHighValues();
        pcoDtUltTabulRiass.initPcoDtUltTabulRiassHighValues();
        pcoDtUltBollEmes.initPcoDtUltBollEmesHighValues();
        pcoDtUltBollStor.initPcoDtUltBollStorHighValues();
        pcoDtUltBollLiq.initPcoDtUltBollLiqHighValues();
        pcoDtUltBollRiat.initPcoDtUltBollRiatHighValues();
        pcoDtUltelriscparPr.initPcoDtUltelriscparPrHighValues();
        pcoDtUltcIsIn.initPcoDtUltcIsInHighValues();
        pcoDtUltRiclPre.initPcoDtUltRiclPreHighValues();
        pcoDtUltcMarsol.initPcoDtUltcMarsolHighValues();
        pcoDtUltcRbIn.initPcoDtUltcRbInHighValues();
        pcoPcProv1aaAcq.initPcoPcProv1aaAcqHighValues();
        pcoModIntrPrest = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PCO_MOD_INTR_PREST);
        pcoGgMaxRecProv.initPcoGgMaxRecProvHighValues();
        pcoDtUltgzTrchEIn.initPcoDtUltgzTrchEInHighValues();
        pcoDtUltBollSnden.initPcoDtUltBollSndenHighValues();
        pcoDtUltBollSndnlq.initPcoDtUltBollSndnlqHighValues();
        pcoDtUltscElabIn.initPcoDtUltscElabInHighValues();
        pcoDtUltscOpzIn.initPcoDtUltscOpzInHighValues();
        pcoDtUltcBnsricIn.initPcoDtUltcBnsricInHighValues();
        pcoDtUltcBnsfdtIn.initPcoDtUltcBnsfdtInHighValues();
        pcoDtUltRinnGarac.initPcoDtUltRinnGaracHighValues();
        pcoDtUltgzCed.initPcoDtUltgzCedHighValues();
        pcoDtUltElabPrlcos.initPcoDtUltElabPrlcosHighValues();
        pcoDtUltRinnColl.initPcoDtUltRinnCollHighValues();
        pcoFlRvcPerf = Types.HIGH_CHAR_VAL;
        pcoFlRcsPoliNoperf = Types.HIGH_CHAR_VAL;
        pcoFlGestPlusv = Types.HIGH_CHAR_VAL;
        pcoDtUltRivalCl.initPcoDtUltRivalClHighValues();
        pcoDtUltQtzoCl.initPcoDtUltQtzoClHighValues();
        pcoDtUltcBnsricCl.initPcoDtUltcBnsricClHighValues();
        pcoDtUltcBnsfdtCl.initPcoDtUltcBnsfdtClHighValues();
        pcoDtUltcIsCl.initPcoDtUltcIsClHighValues();
        pcoDtUltcRbCl.initPcoDtUltcRbClHighValues();
        pcoDtUltgzTrchECl.initPcoDtUltgzTrchEClHighValues();
        pcoDtUltscElabCl.initPcoDtUltscElabClHighValues();
        pcoDtUltscOpzCl.initPcoDtUltscOpzClHighValues();
        pcoStstXRegione.initPcoStstXRegioneHighValues();
        pcoDtUltgzCedColl.initPcoDtUltgzCedCollHighValues();
        pcoTpModRival = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PCO_TP_MOD_RIVAL);
        pcoNumMmCalcMora.initPcoNumMmCalcMoraHighValues();
        pcoDtUltEcRivColl.initPcoDtUltEcRivCollHighValues();
        pcoDtUltEcRivInd.initPcoDtUltEcRivIndHighValues();
        pcoDtUltEcIlColl.initPcoDtUltEcIlCollHighValues();
        pcoDtUltEcIlInd.initPcoDtUltEcIlIndHighValues();
        pcoDtUltEcUlColl.initPcoDtUltEcUlCollHighValues();
        pcoDtUltEcUlInd.initPcoDtUltEcUlIndHighValues();
        pcoAaUti.initPcoAaUtiHighValues();
        pcoCalcRshComun = Types.HIGH_CHAR_VAL;
        pcoFlLivDebug = Types.HIGH_SHORT_VAL;
        pcoDtUltBollPerfC.initPcoDtUltBollPerfCHighValues();
        pcoDtUltBollRspIn.initPcoDtUltBollRspInHighValues();
        pcoDtUltBollRspCl.initPcoDtUltBollRspClHighValues();
        pcoDtUltBollEmesI.initPcoDtUltBollEmesIHighValues();
        pcoDtUltBollStorI.initPcoDtUltBollStorIHighValues();
        pcoDtUltBollRiatI.initPcoDtUltBollRiatIHighValues();
        pcoDtUltBollSdI.initPcoDtUltBollSdIHighValues();
        pcoDtUltBollSdnlI.initPcoDtUltBollSdnlIHighValues();
        pcoDtUltBollPerfI.initPcoDtUltBollPerfIHighValues();
        pcoDtRiclRiriasCom.initPcoDtRiclRiriasComHighValues();
        pcoDtUltElabAt92C.initPcoDtUltElabAt92CHighValues();
        pcoDtUltElabAt92I.initPcoDtUltElabAt92IHighValues();
        pcoDtUltElabAt93C.initPcoDtUltElabAt93CHighValues();
        pcoDtUltElabAt93I.initPcoDtUltElabAt93IHighValues();
        pcoDtUltElabSpeIn.initPcoDtUltElabSpeInHighValues();
        pcoDtUltElabPrCon.initPcoDtUltElabPrConHighValues();
        pcoDtUltBollRpCl.initPcoDtUltBollRpClHighValues();
        pcoDtUltBollRpIn.initPcoDtUltBollRpInHighValues();
        pcoDtUltBollPreI.initPcoDtUltBollPreIHighValues();
        pcoDtUltBollPreC.initPcoDtUltBollPreCHighValues();
        pcoDtUltcPildiMmC.initPcoDtUltcPildiMmCHighValues();
        pcoDtUltcPildiAaC.initPcoDtUltcPildiAaCHighValues();
        pcoDtUltcPildiMmI.initPcoDtUltcPildiMmIHighValues();
        pcoDtUltcPildiTrI.initPcoDtUltcPildiTrIHighValues();
        pcoDtUltcPildiAaI.initPcoDtUltcPildiAaIHighValues();
        pcoDtUltBollQuieC.initPcoDtUltBollQuieCHighValues();
        pcoDtUltBollQuieI.initPcoDtUltBollQuieIHighValues();
        pcoDtUltBollCotrI.initPcoDtUltBollCotrIHighValues();
        pcoDtUltBollCotrC.initPcoDtUltBollCotrCHighValues();
        pcoDtUltBollCoriC.initPcoDtUltBollCoriCHighValues();
        pcoDtUltBollCoriI.initPcoDtUltBollCoriIHighValues();
        pcoDsOperSql = Types.HIGH_CHAR_VAL;
        pcoDsVer = Types.HIGH_INT_VAL;
        pcoDsTsCptz = Types.HIGH_LONG_VAL;
        pcoDsUtente = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PCO_DS_UTENTE);
        pcoDsStatoElab = Types.HIGH_CHAR_VAL;
        pcoTpValzzDtVlt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PCO_TP_VALZZ_DT_VLT);
        pcoFlFrazProvAcq = Types.HIGH_CHAR_VAL;
        pcoDtUltAggErogRe.initPcoDtUltAggErogReHighValues();
        pcoPcRmMarsol.initPcoPcRmMarsolHighValues();
        pcoPcCSubrshMarsol.initPcoPcCSubrshMarsolHighValues();
        pcoCodCompIsvap = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PCO_COD_COMP_ISVAP);
        pcoLmRisConInt.initPcoLmRisConIntHighValues();
        pcoLmCSubrshConIn.initPcoLmCSubrshConInHighValues();
        pcoPcGarNoriskMars.initPcoPcGarNoriskMarsHighValues();
        pcoCrz1aRatIntrPr = Types.HIGH_CHAR_VAL;
        pcoNumGgArrIntrPr.initPcoNumGgArrIntrPrHighValues();
        pcoFlVisualVinpg = Types.HIGH_CHAR_VAL;
        pcoDtUltEstrazFug.initPcoDtUltEstrazFugHighValues();
        pcoDtUltElabPrAut.initPcoDtUltElabPrAutHighValues();
        pcoDtUltElabCommef.initPcoDtUltElabCommefHighValues();
        pcoDtUltElabLiqmef.initPcoDtUltElabLiqmefHighValues();
        pcoCodFiscMef = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PCO_COD_FISC_MEF);
        pcoImpAssSociale.initPcoImpAssSocialeHighValues();
        pcoModComnzInvstSw = Types.HIGH_CHAR_VAL;
        pcoDtRiatRiassRsh.initPcoDtRiatRiassRshHighValues();
        pcoDtRiatRiassComm.initPcoDtRiatRiassCommHighValues();
        pcoGgIntrRitPag.initPcoGgIntrRitPagHighValues();
        pcoDtUltRinnTac.initPcoDtUltRinnTacHighValues();
        initPcoDescCompVcharHighValues();
        pcoDtUltEcTcmInd.initPcoDtUltEcTcmIndHighValues();
        pcoDtUltEcTcmColl.initPcoDtUltEcTcmCollHighValues();
        pcoDtUltEcMrmInd.initPcoDtUltEcMrmIndHighValues();
        pcoDtUltEcMrmColl.initPcoDtUltEcMrmCollHighValues();
        pcoCodCompLdap = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PCO_COD_COMP_LDAP);
        pcoPcRidImp1382011.initPcoPcRidImp1382011HighValues();
        pcoPcRidImp662014.initPcoPcRidImp662014HighValues();
        pcoSoglAmlPreUni.initPcoSoglAmlPreUniHighValues();
        pcoSoglAmlPrePer.initPcoSoglAmlPrePerHighValues();
        pcoCodSoggFtzAssto = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PCO_COD_SOGG_FTZ_ASSTO);
        pcoDtUltElabRedpro.initPcoDtUltElabRedproHighValues();
        pcoDtUltElabTakeP.initPcoDtUltElabTakePHighValues();
        pcoDtUltElabPaspas.initPcoDtUltElabPaspasHighValues();
        pcoSoglAmlPreSavR.initPcoSoglAmlPreSavRHighValues();
        pcoDtUltEstrDecCo.initPcoDtUltEstrDecCoHighValues();
        pcoDtUltElabCosAt.initPcoDtUltElabCosAtHighValues();
        pcoFrqCostiAtt.initPcoFrqCostiAttHighValues();
        pcoDtUltElabCosSt.initPcoDtUltElabCosStHighValues();
        pcoFrqCostiStornati.initPcoFrqCostiStornatiHighValues();
        pcoDtEstrAssMin70a.initPcoDtEstrAssMin70aHighValues();
        pcoDtEstrAssMag70a.initPcoDtEstrAssMag70aHighValues();
    }

    public void setPcoCodCompAnia(int pcoCodCompAnia) {
        this.pcoCodCompAnia = pcoCodCompAnia;
    }

    public int getPcoCodCompAnia() {
        return this.pcoCodCompAnia;
    }

    public void setPcoCodTratCirt(String pcoCodTratCirt) {
        this.pcoCodTratCirt = Functions.subString(pcoCodTratCirt, Len.PCO_COD_TRAT_CIRT);
    }

    public String getPcoCodTratCirt() {
        return this.pcoCodTratCirt;
    }

    public String getPcoCodTratCirtFormatted() {
        return Functions.padBlanks(getPcoCodTratCirt(), Len.PCO_COD_TRAT_CIRT);
    }

    public void setPcoTpRatPerf(char pcoTpRatPerf) {
        this.pcoTpRatPerf = pcoTpRatPerf;
    }

    public char getPcoTpRatPerf() {
        return this.pcoTpRatPerf;
    }

    public void setPcoTpLivGenzTit(String pcoTpLivGenzTit) {
        this.pcoTpLivGenzTit = Functions.subString(pcoTpLivGenzTit, Len.PCO_TP_LIV_GENZ_TIT);
    }

    public String getPcoTpLivGenzTit() {
        return this.pcoTpLivGenzTit;
    }

    public void setPcoModIntrPrest(String pcoModIntrPrest) {
        this.pcoModIntrPrest = Functions.subString(pcoModIntrPrest, Len.PCO_MOD_INTR_PREST);
    }

    public String getPcoModIntrPrest() {
        return this.pcoModIntrPrest;
    }

    public String getPcoModIntrPrestFormatted() {
        return Functions.padBlanks(getPcoModIntrPrest(), Len.PCO_MOD_INTR_PREST);
    }

    public void setPcoFlRvcPerf(char pcoFlRvcPerf) {
        this.pcoFlRvcPerf = pcoFlRvcPerf;
    }

    public char getPcoFlRvcPerf() {
        return this.pcoFlRvcPerf;
    }

    public void setPcoFlRcsPoliNoperf(char pcoFlRcsPoliNoperf) {
        this.pcoFlRcsPoliNoperf = pcoFlRcsPoliNoperf;
    }

    public char getPcoFlRcsPoliNoperf() {
        return this.pcoFlRcsPoliNoperf;
    }

    public void setPcoFlGestPlusv(char pcoFlGestPlusv) {
        this.pcoFlGestPlusv = pcoFlGestPlusv;
    }

    public char getPcoFlGestPlusv() {
        return this.pcoFlGestPlusv;
    }

    public void setPcoTpModRival(String pcoTpModRival) {
        this.pcoTpModRival = Functions.subString(pcoTpModRival, Len.PCO_TP_MOD_RIVAL);
    }

    public String getPcoTpModRival() {
        return this.pcoTpModRival;
    }

    public void setPcoCalcRshComun(char pcoCalcRshComun) {
        this.pcoCalcRshComun = pcoCalcRshComun;
    }

    public char getPcoCalcRshComun() {
        return this.pcoCalcRshComun;
    }

    public void setPcoFlLivDebug(short pcoFlLivDebug) {
        this.pcoFlLivDebug = pcoFlLivDebug;
    }

    public short getPcoFlLivDebug() {
        return this.pcoFlLivDebug;
    }

    public void setPcoDsOperSql(char pcoDsOperSql) {
        this.pcoDsOperSql = pcoDsOperSql;
    }

    public char getPcoDsOperSql() {
        return this.pcoDsOperSql;
    }

    public void setPcoDsVer(int pcoDsVer) {
        this.pcoDsVer = pcoDsVer;
    }

    public int getPcoDsVer() {
        return this.pcoDsVer;
    }

    public void setPcoDsTsCptz(long pcoDsTsCptz) {
        this.pcoDsTsCptz = pcoDsTsCptz;
    }

    public long getPcoDsTsCptz() {
        return this.pcoDsTsCptz;
    }

    public void setPcoDsUtente(String pcoDsUtente) {
        this.pcoDsUtente = Functions.subString(pcoDsUtente, Len.PCO_DS_UTENTE);
    }

    public String getPcoDsUtente() {
        return this.pcoDsUtente;
    }

    public void setPcoDsStatoElab(char pcoDsStatoElab) {
        this.pcoDsStatoElab = pcoDsStatoElab;
    }

    public char getPcoDsStatoElab() {
        return this.pcoDsStatoElab;
    }

    public void setPcoTpValzzDtVlt(String pcoTpValzzDtVlt) {
        this.pcoTpValzzDtVlt = Functions.subString(pcoTpValzzDtVlt, Len.PCO_TP_VALZZ_DT_VLT);
    }

    public String getPcoTpValzzDtVlt() {
        return this.pcoTpValzzDtVlt;
    }

    public String getPcoTpValzzDtVltFormatted() {
        return Functions.padBlanks(getPcoTpValzzDtVlt(), Len.PCO_TP_VALZZ_DT_VLT);
    }

    public void setPcoFlFrazProvAcq(char pcoFlFrazProvAcq) {
        this.pcoFlFrazProvAcq = pcoFlFrazProvAcq;
    }

    public char getPcoFlFrazProvAcq() {
        return this.pcoFlFrazProvAcq;
    }

    public void setPcoCodCompIsvap(String pcoCodCompIsvap) {
        this.pcoCodCompIsvap = Functions.subString(pcoCodCompIsvap, Len.PCO_COD_COMP_ISVAP);
    }

    public String getPcoCodCompIsvap() {
        return this.pcoCodCompIsvap;
    }

    public String getPcoCodCompIsvapFormatted() {
        return Functions.padBlanks(getPcoCodCompIsvap(), Len.PCO_COD_COMP_ISVAP);
    }

    public void setPcoCrz1aRatIntrPr(char pcoCrz1aRatIntrPr) {
        this.pcoCrz1aRatIntrPr = pcoCrz1aRatIntrPr;
    }

    public char getPcoCrz1aRatIntrPr() {
        return this.pcoCrz1aRatIntrPr;
    }

    public void setPcoFlVisualVinpg(char pcoFlVisualVinpg) {
        this.pcoFlVisualVinpg = pcoFlVisualVinpg;
    }

    public char getPcoFlVisualVinpg() {
        return this.pcoFlVisualVinpg;
    }

    public void setPcoCodFiscMef(String pcoCodFiscMef) {
        this.pcoCodFiscMef = Functions.subString(pcoCodFiscMef, Len.PCO_COD_FISC_MEF);
    }

    public String getPcoCodFiscMef() {
        return this.pcoCodFiscMef;
    }

    public String getPcoCodFiscMefFormatted() {
        return Functions.padBlanks(getPcoCodFiscMef(), Len.PCO_COD_FISC_MEF);
    }

    public void setPcoModComnzInvstSw(char pcoModComnzInvstSw) {
        this.pcoModComnzInvstSw = pcoModComnzInvstSw;
    }

    public char getPcoModComnzInvstSw() {
        return this.pcoModComnzInvstSw;
    }

    public void setPcoDescCompVcharBytes(byte[] buffer) {
        setPcoDescCompVcharBytes(buffer, 1);
    }

    /**Original name: PCO-DESC-COMP-VCHAR<br>*/
    public byte[] getPcoDescCompVcharBytes() {
        byte[] buffer = new byte[Len.PCO_DESC_COMP_VCHAR];
        return getPcoDescCompVcharBytes(buffer, 1);
    }

    public void setPcoDescCompVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        pcoDescCompLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        pcoDescComp = MarshalByte.readString(buffer, position, Len.PCO_DESC_COMP);
    }

    public byte[] getPcoDescCompVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, pcoDescCompLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, pcoDescComp, Len.PCO_DESC_COMP);
        return buffer;
    }

    public void initPcoDescCompVcharHighValues() {
        pcoDescCompLen = Types.HIGH_SHORT_VAL;
        pcoDescComp = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PCO_DESC_COMP);
    }

    public void setPcoDescCompLen(short pcoDescCompLen) {
        this.pcoDescCompLen = pcoDescCompLen;
    }

    public short getPcoDescCompLen() {
        return this.pcoDescCompLen;
    }

    public void setPcoDescComp(String pcoDescComp) {
        this.pcoDescComp = Functions.subString(pcoDescComp, Len.PCO_DESC_COMP);
    }

    public String getPcoDescComp() {
        return this.pcoDescComp;
    }

    public void setPcoCodCompLdap(String pcoCodCompLdap) {
        this.pcoCodCompLdap = Functions.subString(pcoCodCompLdap, Len.PCO_COD_COMP_LDAP);
    }

    public String getPcoCodCompLdap() {
        return this.pcoCodCompLdap;
    }

    public String getPcoCodCompLdapFormatted() {
        return Functions.padBlanks(getPcoCodCompLdap(), Len.PCO_COD_COMP_LDAP);
    }

    public void setPcoCodSoggFtzAssto(String pcoCodSoggFtzAssto) {
        this.pcoCodSoggFtzAssto = Functions.subString(pcoCodSoggFtzAssto, Len.PCO_COD_SOGG_FTZ_ASSTO);
    }

    public String getPcoCodSoggFtzAssto() {
        return this.pcoCodSoggFtzAssto;
    }

    public String getPcoCodSoggFtzAsstoFormatted() {
        return Functions.padBlanks(getPcoCodSoggFtzAssto(), Len.PCO_COD_SOGG_FTZ_ASSTO);
    }

    public PcoAaUti getPcoAaUti() {
        return pcoAaUti;
    }

    public PcoArrotPre getPcoArrotPre() {
        return pcoArrotPre;
    }

    public PcoDtCont getPcoDtCont() {
        return pcoDtCont;
    }

    public PcoDtEstrAssMag70a getPcoDtEstrAssMag70a() {
        return pcoDtEstrAssMag70a;
    }

    public PcoDtEstrAssMin70a getPcoDtEstrAssMin70a() {
        return pcoDtEstrAssMin70a;
    }

    public PcoDtRiatRiassComm getPcoDtRiatRiassComm() {
        return pcoDtRiatRiassComm;
    }

    public PcoDtRiatRiassRsh getPcoDtRiatRiassRsh() {
        return pcoDtRiatRiassRsh;
    }

    public PcoDtRiclRiriasCom getPcoDtRiclRiriasCom() {
        return pcoDtRiclRiriasCom;
    }

    public PcoDtUltAggErogRe getPcoDtUltAggErogRe() {
        return pcoDtUltAggErogRe;
    }

    public PcoDtUltBollCoriC getPcoDtUltBollCoriC() {
        return pcoDtUltBollCoriC;
    }

    public PcoDtUltBollCoriI getPcoDtUltBollCoriI() {
        return pcoDtUltBollCoriI;
    }

    public PcoDtUltBollCotrC getPcoDtUltBollCotrC() {
        return pcoDtUltBollCotrC;
    }

    public PcoDtUltBollCotrI getPcoDtUltBollCotrI() {
        return pcoDtUltBollCotrI;
    }

    public PcoDtUltBollEmes getPcoDtUltBollEmes() {
        return pcoDtUltBollEmes;
    }

    public PcoDtUltBollEmesI getPcoDtUltBollEmesI() {
        return pcoDtUltBollEmesI;
    }

    public PcoDtUltBollLiq getPcoDtUltBollLiq() {
        return pcoDtUltBollLiq;
    }

    public PcoDtUltBollPerfC getPcoDtUltBollPerfC() {
        return pcoDtUltBollPerfC;
    }

    public PcoDtUltBollPerfI getPcoDtUltBollPerfI() {
        return pcoDtUltBollPerfI;
    }

    public PcoDtUltBollPreC getPcoDtUltBollPreC() {
        return pcoDtUltBollPreC;
    }

    public PcoDtUltBollPreI getPcoDtUltBollPreI() {
        return pcoDtUltBollPreI;
    }

    public PcoDtUltBollQuieC getPcoDtUltBollQuieC() {
        return pcoDtUltBollQuieC;
    }

    public PcoDtUltBollQuieI getPcoDtUltBollQuieI() {
        return pcoDtUltBollQuieI;
    }

    public PcoDtUltBollRiat getPcoDtUltBollRiat() {
        return pcoDtUltBollRiat;
    }

    public PcoDtUltBollRiatI getPcoDtUltBollRiatI() {
        return pcoDtUltBollRiatI;
    }

    public PcoDtUltBollRpCl getPcoDtUltBollRpCl() {
        return pcoDtUltBollRpCl;
    }

    public PcoDtUltBollRpIn getPcoDtUltBollRpIn() {
        return pcoDtUltBollRpIn;
    }

    public PcoDtUltBollRspCl getPcoDtUltBollRspCl() {
        return pcoDtUltBollRspCl;
    }

    public PcoDtUltBollRspIn getPcoDtUltBollRspIn() {
        return pcoDtUltBollRspIn;
    }

    public PcoDtUltBollSdI getPcoDtUltBollSdI() {
        return pcoDtUltBollSdI;
    }

    public PcoDtUltBollSdnlI getPcoDtUltBollSdnlI() {
        return pcoDtUltBollSdnlI;
    }

    public PcoDtUltBollSnden getPcoDtUltBollSnden() {
        return pcoDtUltBollSnden;
    }

    public PcoDtUltBollSndnlq getPcoDtUltBollSndnlq() {
        return pcoDtUltBollSndnlq;
    }

    public PcoDtUltBollStor getPcoDtUltBollStor() {
        return pcoDtUltBollStor;
    }

    public PcoDtUltBollStorI getPcoDtUltBollStorI() {
        return pcoDtUltBollStorI;
    }

    public PcoDtUltEcIlColl getPcoDtUltEcIlColl() {
        return pcoDtUltEcIlColl;
    }

    public PcoDtUltEcIlInd getPcoDtUltEcIlInd() {
        return pcoDtUltEcIlInd;
    }

    public PcoDtUltEcMrmColl getPcoDtUltEcMrmColl() {
        return pcoDtUltEcMrmColl;
    }

    public PcoDtUltEcMrmInd getPcoDtUltEcMrmInd() {
        return pcoDtUltEcMrmInd;
    }

    public PcoDtUltEcRivColl getPcoDtUltEcRivColl() {
        return pcoDtUltEcRivColl;
    }

    public PcoDtUltEcRivInd getPcoDtUltEcRivInd() {
        return pcoDtUltEcRivInd;
    }

    public PcoDtUltEcTcmColl getPcoDtUltEcTcmColl() {
        return pcoDtUltEcTcmColl;
    }

    public PcoDtUltEcTcmInd getPcoDtUltEcTcmInd() {
        return pcoDtUltEcTcmInd;
    }

    public PcoDtUltEcUlColl getPcoDtUltEcUlColl() {
        return pcoDtUltEcUlColl;
    }

    public PcoDtUltEcUlInd getPcoDtUltEcUlInd() {
        return pcoDtUltEcUlInd;
    }

    public PcoDtUltElabAt92C getPcoDtUltElabAt92C() {
        return pcoDtUltElabAt92C;
    }

    public PcoDtUltElabAt92I getPcoDtUltElabAt92I() {
        return pcoDtUltElabAt92I;
    }

    public PcoDtUltElabAt93C getPcoDtUltElabAt93C() {
        return pcoDtUltElabAt93C;
    }

    public PcoDtUltElabAt93I getPcoDtUltElabAt93I() {
        return pcoDtUltElabAt93I;
    }

    public PcoDtUltElabCommef getPcoDtUltElabCommef() {
        return pcoDtUltElabCommef;
    }

    public PcoDtUltElabCosAt getPcoDtUltElabCosAt() {
        return pcoDtUltElabCosAt;
    }

    public PcoDtUltElabCosSt getPcoDtUltElabCosSt() {
        return pcoDtUltElabCosSt;
    }

    public PcoDtUltElabLiqmef getPcoDtUltElabLiqmef() {
        return pcoDtUltElabLiqmef;
    }

    public PcoDtUltElabPaspas getPcoDtUltElabPaspas() {
        return pcoDtUltElabPaspas;
    }

    public PcoDtUltElabPrAut getPcoDtUltElabPrAut() {
        return pcoDtUltElabPrAut;
    }

    public PcoDtUltElabPrCon getPcoDtUltElabPrCon() {
        return pcoDtUltElabPrCon;
    }

    public PcoDtUltElabPrlcos getPcoDtUltElabPrlcos() {
        return pcoDtUltElabPrlcos;
    }

    public PcoDtUltElabRedpro getPcoDtUltElabRedpro() {
        return pcoDtUltElabRedpro;
    }

    public PcoDtUltElabSpeIn getPcoDtUltElabSpeIn() {
        return pcoDtUltElabSpeIn;
    }

    public PcoDtUltElabTakeP getPcoDtUltElabTakeP() {
        return pcoDtUltElabTakeP;
    }

    public PcoDtUltEstrDecCo getPcoDtUltEstrDecCo() {
        return pcoDtUltEstrDecCo;
    }

    public PcoDtUltEstrazFug getPcoDtUltEstrazFug() {
        return pcoDtUltEstrazFug;
    }

    public PcoDtUltQtzoCl getPcoDtUltQtzoCl() {
        return pcoDtUltQtzoCl;
    }

    public PcoDtUltQtzoIn getPcoDtUltQtzoIn() {
        return pcoDtUltQtzoIn;
    }

    public PcoDtUltRiclPre getPcoDtUltRiclPre() {
        return pcoDtUltRiclPre;
    }

    public PcoDtUltRiclRiass getPcoDtUltRiclRiass() {
        return pcoDtUltRiclRiass;
    }

    public PcoDtUltRinnColl getPcoDtUltRinnColl() {
        return pcoDtUltRinnColl;
    }

    public PcoDtUltRinnGarac getPcoDtUltRinnGarac() {
        return pcoDtUltRinnGarac;
    }

    public PcoDtUltRinnTac getPcoDtUltRinnTac() {
        return pcoDtUltRinnTac;
    }

    public PcoDtUltRivalCl getPcoDtUltRivalCl() {
        return pcoDtUltRivalCl;
    }

    public PcoDtUltRivalIn getPcoDtUltRivalIn() {
        return pcoDtUltRivalIn;
    }

    public PcoDtUltTabulRiass getPcoDtUltTabulRiass() {
        return pcoDtUltTabulRiass;
    }

    public PcoDtUltcBnsfdtCl getPcoDtUltcBnsfdtCl() {
        return pcoDtUltcBnsfdtCl;
    }

    public PcoDtUltcBnsfdtIn getPcoDtUltcBnsfdtIn() {
        return pcoDtUltcBnsfdtIn;
    }

    public PcoDtUltcBnsricCl getPcoDtUltcBnsricCl() {
        return pcoDtUltcBnsricCl;
    }

    public PcoDtUltcBnsricIn getPcoDtUltcBnsricIn() {
        return pcoDtUltcBnsricIn;
    }

    public PcoDtUltcIsCl getPcoDtUltcIsCl() {
        return pcoDtUltcIsCl;
    }

    public PcoDtUltcIsIn getPcoDtUltcIsIn() {
        return pcoDtUltcIsIn;
    }

    public PcoDtUltcMarsol getPcoDtUltcMarsol() {
        return pcoDtUltcMarsol;
    }

    public PcoDtUltcPildiAaC getPcoDtUltcPildiAaC() {
        return pcoDtUltcPildiAaC;
    }

    public PcoDtUltcPildiAaI getPcoDtUltcPildiAaI() {
        return pcoDtUltcPildiAaI;
    }

    public PcoDtUltcPildiMmC getPcoDtUltcPildiMmC() {
        return pcoDtUltcPildiMmC;
    }

    public PcoDtUltcPildiMmI getPcoDtUltcPildiMmI() {
        return pcoDtUltcPildiMmI;
    }

    public PcoDtUltcPildiTrI getPcoDtUltcPildiTrI() {
        return pcoDtUltcPildiTrI;
    }

    public PcoDtUltcRbCl getPcoDtUltcRbCl() {
        return pcoDtUltcRbCl;
    }

    public PcoDtUltcRbIn getPcoDtUltcRbIn() {
        return pcoDtUltcRbIn;
    }

    public PcoDtUltelriscparPr getPcoDtUltelriscparPr() {
        return pcoDtUltelriscparPr;
    }

    public PcoDtUltgzCed getPcoDtUltgzCed() {
        return pcoDtUltgzCed;
    }

    public PcoDtUltgzCedColl getPcoDtUltgzCedColl() {
        return pcoDtUltgzCedColl;
    }

    public PcoDtUltgzTrchECl getPcoDtUltgzTrchECl() {
        return pcoDtUltgzTrchECl;
    }

    public PcoDtUltgzTrchEIn getPcoDtUltgzTrchEIn() {
        return pcoDtUltgzTrchEIn;
    }

    public PcoDtUltscElabCl getPcoDtUltscElabCl() {
        return pcoDtUltscElabCl;
    }

    public PcoDtUltscElabIn getPcoDtUltscElabIn() {
        return pcoDtUltscElabIn;
    }

    public PcoDtUltscOpzCl getPcoDtUltscOpzCl() {
        return pcoDtUltscOpzCl;
    }

    public PcoDtUltscOpzIn getPcoDtUltscOpzIn() {
        return pcoDtUltscOpzIn;
    }

    public PcoFrqCostiAtt getPcoFrqCostiAtt() {
        return pcoFrqCostiAtt;
    }

    public PcoFrqCostiStornati getPcoFrqCostiStornati() {
        return pcoFrqCostiStornati;
    }

    public PcoGgIntrRitPag getPcoGgIntrRitPag() {
        return pcoGgIntrRitPag;
    }

    public PcoGgMaxRecProv getPcoGgMaxRecProv() {
        return pcoGgMaxRecProv;
    }

    public PcoImpAssSociale getPcoImpAssSociale() {
        return pcoImpAssSociale;
    }

    public PcoLimVltr getPcoLimVltr() {
        return pcoLimVltr;
    }

    public PcoLmCSubrshConIn getPcoLmCSubrshConIn() {
        return pcoLmCSubrshConIn;
    }

    public PcoLmRisConInt getPcoLmRisConInt() {
        return pcoLmRisConInt;
    }

    public PcoNumGgArrIntrPr getPcoNumGgArrIntrPr() {
        return pcoNumGgArrIntrPr;
    }

    public PcoNumMmCalcMora getPcoNumMmCalcMora() {
        return pcoNumMmCalcMora;
    }

    public PcoPcCSubrshMarsol getPcoPcCSubrshMarsol() {
        return pcoPcCSubrshMarsol;
    }

    public PcoPcGarNoriskMars getPcoPcGarNoriskMars() {
        return pcoPcGarNoriskMars;
    }

    public PcoPcProv1aaAcq getPcoPcProv1aaAcq() {
        return pcoPcProv1aaAcq;
    }

    public PcoPcRidImp1382011 getPcoPcRidImp1382011() {
        return pcoPcRidImp1382011;
    }

    public PcoPcRidImp662014 getPcoPcRidImp662014() {
        return pcoPcRidImp662014;
    }

    public PcoPcRmMarsol getPcoPcRmMarsol() {
        return pcoPcRmMarsol;
    }

    public PcoSoglAmlPrePer getPcoSoglAmlPrePer() {
        return pcoSoglAmlPrePer;
    }

    public PcoSoglAmlPreSavR getPcoSoglAmlPreSavR() {
        return pcoSoglAmlPreSavR;
    }

    public PcoSoglAmlPreUni getPcoSoglAmlPreUni() {
        return pcoSoglAmlPreUni;
    }

    public PcoStstXRegione getPcoStstXRegione() {
        return pcoStstXRegione;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_COD_TRAT_CIRT = 20;
        public static final int PCO_TP_LIV_GENZ_TIT = 2;
        public static final int PCO_MOD_INTR_PREST = 2;
        public static final int PCO_TP_MOD_RIVAL = 2;
        public static final int PCO_DS_UTENTE = 20;
        public static final int PCO_TP_VALZZ_DT_VLT = 2;
        public static final int PCO_COD_COMP_ISVAP = 5;
        public static final int PCO_COD_FISC_MEF = 16;
        public static final int PCO_DESC_COMP = 250;
        public static final int PCO_COD_COMP_LDAP = 20;
        public static final int PCO_COD_SOGG_FTZ_ASSTO = 20;
        public static final int PCO_COD_COMP_ANIA = 3;
        public static final int PCO_TP_RAT_PERF = 1;
        public static final int PCO_FL_RVC_PERF = 1;
        public static final int PCO_FL_RCS_POLI_NOPERF = 1;
        public static final int PCO_FL_GEST_PLUSV = 1;
        public static final int PCO_CALC_RSH_COMUN = 1;
        public static final int PCO_FL_LIV_DEBUG = 1;
        public static final int PCO_DS_OPER_SQL = 1;
        public static final int PCO_DS_VER = 5;
        public static final int PCO_DS_TS_CPTZ = 10;
        public static final int PCO_DS_STATO_ELAB = 1;
        public static final int PCO_FL_FRAZ_PROV_ACQ = 1;
        public static final int PCO_CRZ1A_RAT_INTR_PR = 1;
        public static final int PCO_FL_VISUAL_VINPG = 1;
        public static final int PCO_MOD_COMNZ_INVST_SW = 1;
        public static final int PCO_DESC_COMP_LEN = 2;
        public static final int PCO_DESC_COMP_VCHAR = PCO_DESC_COMP_LEN + PCO_DESC_COMP;
        public static final int PARAM_COMP = PCO_COD_COMP_ANIA + PCO_COD_TRAT_CIRT + PcoLimVltr.Len.PCO_LIM_VLTR + PCO_TP_RAT_PERF + PCO_TP_LIV_GENZ_TIT + PcoArrotPre.Len.PCO_ARROT_PRE + PcoDtCont.Len.PCO_DT_CONT + PcoDtUltRivalIn.Len.PCO_DT_ULT_RIVAL_IN + PcoDtUltQtzoIn.Len.PCO_DT_ULT_QTZO_IN + PcoDtUltRiclRiass.Len.PCO_DT_ULT_RICL_RIASS + PcoDtUltTabulRiass.Len.PCO_DT_ULT_TABUL_RIASS + PcoDtUltBollEmes.Len.PCO_DT_ULT_BOLL_EMES + PcoDtUltBollStor.Len.PCO_DT_ULT_BOLL_STOR + PcoDtUltBollLiq.Len.PCO_DT_ULT_BOLL_LIQ + PcoDtUltBollRiat.Len.PCO_DT_ULT_BOLL_RIAT + PcoDtUltelriscparPr.Len.PCO_DT_ULTELRISCPAR_PR + PcoDtUltcIsIn.Len.PCO_DT_ULTC_IS_IN + PcoDtUltRiclPre.Len.PCO_DT_ULT_RICL_PRE + PcoDtUltcMarsol.Len.PCO_DT_ULTC_MARSOL + PcoDtUltcRbIn.Len.PCO_DT_ULTC_RB_IN + PcoPcProv1aaAcq.Len.PCO_PC_PROV1AA_ACQ + PCO_MOD_INTR_PREST + PcoGgMaxRecProv.Len.PCO_GG_MAX_REC_PROV + PcoDtUltgzTrchEIn.Len.PCO_DT_ULTGZ_TRCH_E_IN + PcoDtUltBollSnden.Len.PCO_DT_ULT_BOLL_SNDEN + PcoDtUltBollSndnlq.Len.PCO_DT_ULT_BOLL_SNDNLQ + PcoDtUltscElabIn.Len.PCO_DT_ULTSC_ELAB_IN + PcoDtUltscOpzIn.Len.PCO_DT_ULTSC_OPZ_IN + PcoDtUltcBnsricIn.Len.PCO_DT_ULTC_BNSRIC_IN + PcoDtUltcBnsfdtIn.Len.PCO_DT_ULTC_BNSFDT_IN + PcoDtUltRinnGarac.Len.PCO_DT_ULT_RINN_GARAC + PcoDtUltgzCed.Len.PCO_DT_ULTGZ_CED + PcoDtUltElabPrlcos.Len.PCO_DT_ULT_ELAB_PRLCOS + PcoDtUltRinnColl.Len.PCO_DT_ULT_RINN_COLL + PCO_FL_RVC_PERF + PCO_FL_RCS_POLI_NOPERF + PCO_FL_GEST_PLUSV + PcoDtUltRivalCl.Len.PCO_DT_ULT_RIVAL_CL + PcoDtUltQtzoCl.Len.PCO_DT_ULT_QTZO_CL + PcoDtUltcBnsricCl.Len.PCO_DT_ULTC_BNSRIC_CL + PcoDtUltcBnsfdtCl.Len.PCO_DT_ULTC_BNSFDT_CL + PcoDtUltcIsCl.Len.PCO_DT_ULTC_IS_CL + PcoDtUltcRbCl.Len.PCO_DT_ULTC_RB_CL + PcoDtUltgzTrchECl.Len.PCO_DT_ULTGZ_TRCH_E_CL + PcoDtUltscElabCl.Len.PCO_DT_ULTSC_ELAB_CL + PcoDtUltscOpzCl.Len.PCO_DT_ULTSC_OPZ_CL + PcoStstXRegione.Len.PCO_STST_X_REGIONE + PcoDtUltgzCedColl.Len.PCO_DT_ULTGZ_CED_COLL + PCO_TP_MOD_RIVAL + PcoNumMmCalcMora.Len.PCO_NUM_MM_CALC_MORA + PcoDtUltEcRivColl.Len.PCO_DT_ULT_EC_RIV_COLL + PcoDtUltEcRivInd.Len.PCO_DT_ULT_EC_RIV_IND + PcoDtUltEcIlColl.Len.PCO_DT_ULT_EC_IL_COLL + PcoDtUltEcIlInd.Len.PCO_DT_ULT_EC_IL_IND + PcoDtUltEcUlColl.Len.PCO_DT_ULT_EC_UL_COLL + PcoDtUltEcUlInd.Len.PCO_DT_ULT_EC_UL_IND + PcoAaUti.Len.PCO_AA_UTI + PCO_CALC_RSH_COMUN + PCO_FL_LIV_DEBUG + PcoDtUltBollPerfC.Len.PCO_DT_ULT_BOLL_PERF_C + PcoDtUltBollRspIn.Len.PCO_DT_ULT_BOLL_RSP_IN + PcoDtUltBollRspCl.Len.PCO_DT_ULT_BOLL_RSP_CL + PcoDtUltBollEmesI.Len.PCO_DT_ULT_BOLL_EMES_I + PcoDtUltBollStorI.Len.PCO_DT_ULT_BOLL_STOR_I + PcoDtUltBollRiatI.Len.PCO_DT_ULT_BOLL_RIAT_I + PcoDtUltBollSdI.Len.PCO_DT_ULT_BOLL_SD_I + PcoDtUltBollSdnlI.Len.PCO_DT_ULT_BOLL_SDNL_I + PcoDtUltBollPerfI.Len.PCO_DT_ULT_BOLL_PERF_I + PcoDtRiclRiriasCom.Len.PCO_DT_RICL_RIRIAS_COM + PcoDtUltElabAt92C.Len.PCO_DT_ULT_ELAB_AT92_C + PcoDtUltElabAt92I.Len.PCO_DT_ULT_ELAB_AT92_I + PcoDtUltElabAt93C.Len.PCO_DT_ULT_ELAB_AT93_C + PcoDtUltElabAt93I.Len.PCO_DT_ULT_ELAB_AT93_I + PcoDtUltElabSpeIn.Len.PCO_DT_ULT_ELAB_SPE_IN + PcoDtUltElabPrCon.Len.PCO_DT_ULT_ELAB_PR_CON + PcoDtUltBollRpCl.Len.PCO_DT_ULT_BOLL_RP_CL + PcoDtUltBollRpIn.Len.PCO_DT_ULT_BOLL_RP_IN + PcoDtUltBollPreI.Len.PCO_DT_ULT_BOLL_PRE_I + PcoDtUltBollPreC.Len.PCO_DT_ULT_BOLL_PRE_C + PcoDtUltcPildiMmC.Len.PCO_DT_ULTC_PILDI_MM_C + PcoDtUltcPildiAaC.Len.PCO_DT_ULTC_PILDI_AA_C + PcoDtUltcPildiMmI.Len.PCO_DT_ULTC_PILDI_MM_I + PcoDtUltcPildiTrI.Len.PCO_DT_ULTC_PILDI_TR_I + PcoDtUltcPildiAaI.Len.PCO_DT_ULTC_PILDI_AA_I + PcoDtUltBollQuieC.Len.PCO_DT_ULT_BOLL_QUIE_C + PcoDtUltBollQuieI.Len.PCO_DT_ULT_BOLL_QUIE_I + PcoDtUltBollCotrI.Len.PCO_DT_ULT_BOLL_COTR_I + PcoDtUltBollCotrC.Len.PCO_DT_ULT_BOLL_COTR_C + PcoDtUltBollCoriC.Len.PCO_DT_ULT_BOLL_CORI_C + PcoDtUltBollCoriI.Len.PCO_DT_ULT_BOLL_CORI_I + PCO_DS_OPER_SQL + PCO_DS_VER + PCO_DS_TS_CPTZ + PCO_DS_UTENTE + PCO_DS_STATO_ELAB + PCO_TP_VALZZ_DT_VLT + PCO_FL_FRAZ_PROV_ACQ + PcoDtUltAggErogRe.Len.PCO_DT_ULT_AGG_EROG_RE + PcoPcRmMarsol.Len.PCO_PC_RM_MARSOL + PcoPcCSubrshMarsol.Len.PCO_PC_C_SUBRSH_MARSOL + PCO_COD_COMP_ISVAP + PcoLmRisConInt.Len.PCO_LM_RIS_CON_INT + PcoLmCSubrshConIn.Len.PCO_LM_C_SUBRSH_CON_IN + PcoPcGarNoriskMars.Len.PCO_PC_GAR_NORISK_MARS + PCO_CRZ1A_RAT_INTR_PR + PcoNumGgArrIntrPr.Len.PCO_NUM_GG_ARR_INTR_PR + PCO_FL_VISUAL_VINPG + PcoDtUltEstrazFug.Len.PCO_DT_ULT_ESTRAZ_FUG + PcoDtUltElabPrAut.Len.PCO_DT_ULT_ELAB_PR_AUT + PcoDtUltElabCommef.Len.PCO_DT_ULT_ELAB_COMMEF + PcoDtUltElabLiqmef.Len.PCO_DT_ULT_ELAB_LIQMEF + PCO_COD_FISC_MEF + PcoImpAssSociale.Len.PCO_IMP_ASS_SOCIALE + PCO_MOD_COMNZ_INVST_SW + PcoDtRiatRiassRsh.Len.PCO_DT_RIAT_RIASS_RSH + PcoDtRiatRiassComm.Len.PCO_DT_RIAT_RIASS_COMM + PcoGgIntrRitPag.Len.PCO_GG_INTR_RIT_PAG + PcoDtUltRinnTac.Len.PCO_DT_ULT_RINN_TAC + PCO_DESC_COMP_VCHAR + PcoDtUltEcTcmInd.Len.PCO_DT_ULT_EC_TCM_IND + PcoDtUltEcTcmColl.Len.PCO_DT_ULT_EC_TCM_COLL + PcoDtUltEcMrmInd.Len.PCO_DT_ULT_EC_MRM_IND + PcoDtUltEcMrmColl.Len.PCO_DT_ULT_EC_MRM_COLL + PCO_COD_COMP_LDAP + PcoPcRidImp1382011.Len.PCO_PC_RID_IMP1382011 + PcoPcRidImp662014.Len.PCO_PC_RID_IMP662014 + PcoSoglAmlPreUni.Len.PCO_SOGL_AML_PRE_UNI + PcoSoglAmlPrePer.Len.PCO_SOGL_AML_PRE_PER + PCO_COD_SOGG_FTZ_ASSTO + PcoDtUltElabRedpro.Len.PCO_DT_ULT_ELAB_REDPRO + PcoDtUltElabTakeP.Len.PCO_DT_ULT_ELAB_TAKE_P + PcoDtUltElabPaspas.Len.PCO_DT_ULT_ELAB_PASPAS + PcoSoglAmlPreSavR.Len.PCO_SOGL_AML_PRE_SAV_R + PcoDtUltEstrDecCo.Len.PCO_DT_ULT_ESTR_DEC_CO + PcoDtUltElabCosAt.Len.PCO_DT_ULT_ELAB_COS_AT + PcoFrqCostiAtt.Len.PCO_FRQ_COSTI_ATT + PcoDtUltElabCosSt.Len.PCO_DT_ULT_ELAB_COS_ST + PcoFrqCostiStornati.Len.PCO_FRQ_COSTI_STORNATI + PcoDtEstrAssMin70a.Len.PCO_DT_ESTR_ASS_MIN70A + PcoDtEstrAssMag70a.Len.PCO_DT_ESTR_ASS_MAG70A;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_COD_COMP_ANIA = 5;
            public static final int PCO_FL_LIV_DEBUG = 1;
            public static final int PCO_DS_VER = 9;
            public static final int PCO_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
