package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.P04IdMoviCrz;

/**Original name: STAT-RICH-EST<br>
 * Variable: STAT-RICH-EST from copybook IDBVP041<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class StatRichEst {

    //==== PROPERTIES ====
    //Original name: P04-ID-STAT-RICH-EST
    private int p04IdStatRichEst = DefaultValues.INT_VAL;
    //Original name: P04-ID-RICH-EST
    private int p04IdRichEst = DefaultValues.INT_VAL;
    //Original name: P04-TS-INI-VLDT
    private long p04TsIniVldt = DefaultValues.LONG_VAL;
    //Original name: P04-TS-END-VLDT
    private long p04TsEndVldt = DefaultValues.LONG_VAL;
    //Original name: P04-COD-COMP-ANIA
    private int p04CodCompAnia = DefaultValues.INT_VAL;
    //Original name: P04-COD-PRCS
    private String p04CodPrcs = DefaultValues.stringVal(Len.P04_COD_PRCS);
    //Original name: P04-COD-ATTVT
    private String p04CodAttvt = DefaultValues.stringVal(Len.P04_COD_ATTVT);
    //Original name: P04-STAT-RICH-EST
    private String p04StatRichEst = DefaultValues.stringVal(Len.P04_STAT_RICH_EST);
    //Original name: P04-FL-STAT-END
    private char p04FlStatEnd = DefaultValues.CHAR_VAL;
    //Original name: P04-DS-OPER-SQL
    private char p04DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P04-DS-VER
    private int p04DsVer = DefaultValues.INT_VAL;
    //Original name: P04-DS-TS-CPTZ
    private long p04DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: P04-DS-UTENTE
    private String p04DsUtente = DefaultValues.stringVal(Len.P04_DS_UTENTE);
    //Original name: P04-DS-STATO-ELAB
    private char p04DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: P04-TP-CAUS-SCARTO
    private String p04TpCausScarto = DefaultValues.stringVal(Len.P04_TP_CAUS_SCARTO);
    //Original name: P04-DESC-ERR-LEN
    private short p04DescErrLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P04-DESC-ERR
    private String p04DescErr = DefaultValues.stringVal(Len.P04_DESC_ERR);
    //Original name: P04-UTENTE-INS-AGG
    private String p04UtenteInsAgg = DefaultValues.stringVal(Len.P04_UTENTE_INS_AGG);
    //Original name: P04-COD-ERR-SCARTO
    private String p04CodErrScarto = DefaultValues.stringVal(Len.P04_COD_ERR_SCARTO);
    //Original name: P04-ID-MOVI-CRZ
    private P04IdMoviCrz p04IdMoviCrz = new P04IdMoviCrz();

    //==== METHODS ====
    public void setStatRichEstFormatted(String data) {
        byte[] buffer = new byte[Len.STAT_RICH_EST];
        MarshalByte.writeString(buffer, 1, data, Len.STAT_RICH_EST);
        setStatRichEstBytes(buffer, 1);
    }

    public String getStatRichEstFormatted() {
        return MarshalByteExt.bufferToStr(getStatRichEstBytes());
    }

    public byte[] getStatRichEstBytes() {
        byte[] buffer = new byte[Len.STAT_RICH_EST];
        return getStatRichEstBytes(buffer, 1);
    }

    public void setStatRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        p04IdStatRichEst = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P04_ID_STAT_RICH_EST, 0);
        position += Len.P04_ID_STAT_RICH_EST;
        p04IdRichEst = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P04_ID_RICH_EST, 0);
        position += Len.P04_ID_RICH_EST;
        p04TsIniVldt = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P04_TS_INI_VLDT, 0);
        position += Len.P04_TS_INI_VLDT;
        p04TsEndVldt = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P04_TS_END_VLDT, 0);
        position += Len.P04_TS_END_VLDT;
        p04CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P04_COD_COMP_ANIA, 0);
        position += Len.P04_COD_COMP_ANIA;
        p04CodPrcs = MarshalByte.readString(buffer, position, Len.P04_COD_PRCS);
        position += Len.P04_COD_PRCS;
        p04CodAttvt = MarshalByte.readString(buffer, position, Len.P04_COD_ATTVT);
        position += Len.P04_COD_ATTVT;
        p04StatRichEst = MarshalByte.readString(buffer, position, Len.P04_STAT_RICH_EST);
        position += Len.P04_STAT_RICH_EST;
        p04FlStatEnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p04DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p04DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P04_DS_VER, 0);
        position += Len.P04_DS_VER;
        p04DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P04_DS_TS_CPTZ, 0);
        position += Len.P04_DS_TS_CPTZ;
        p04DsUtente = MarshalByte.readString(buffer, position, Len.P04_DS_UTENTE);
        position += Len.P04_DS_UTENTE;
        p04DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p04TpCausScarto = MarshalByte.readString(buffer, position, Len.P04_TP_CAUS_SCARTO);
        position += Len.P04_TP_CAUS_SCARTO;
        setP04DescErrVcharBytes(buffer, position);
        position += Len.P04_DESC_ERR_VCHAR;
        p04UtenteInsAgg = MarshalByte.readString(buffer, position, Len.P04_UTENTE_INS_AGG);
        position += Len.P04_UTENTE_INS_AGG;
        p04CodErrScarto = MarshalByte.readString(buffer, position, Len.P04_COD_ERR_SCARTO);
        position += Len.P04_COD_ERR_SCARTO;
        p04IdMoviCrz.setP04IdMoviCrzFromBuffer(buffer, position);
    }

    public byte[] getStatRichEstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, p04IdStatRichEst, Len.Int.P04_ID_STAT_RICH_EST, 0);
        position += Len.P04_ID_STAT_RICH_EST;
        MarshalByte.writeIntAsPacked(buffer, position, p04IdRichEst, Len.Int.P04_ID_RICH_EST, 0);
        position += Len.P04_ID_RICH_EST;
        MarshalByte.writeLongAsPacked(buffer, position, p04TsIniVldt, Len.Int.P04_TS_INI_VLDT, 0);
        position += Len.P04_TS_INI_VLDT;
        MarshalByte.writeLongAsPacked(buffer, position, p04TsEndVldt, Len.Int.P04_TS_END_VLDT, 0);
        position += Len.P04_TS_END_VLDT;
        MarshalByte.writeIntAsPacked(buffer, position, p04CodCompAnia, Len.Int.P04_COD_COMP_ANIA, 0);
        position += Len.P04_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, p04CodPrcs, Len.P04_COD_PRCS);
        position += Len.P04_COD_PRCS;
        MarshalByte.writeString(buffer, position, p04CodAttvt, Len.P04_COD_ATTVT);
        position += Len.P04_COD_ATTVT;
        MarshalByte.writeString(buffer, position, p04StatRichEst, Len.P04_STAT_RICH_EST);
        position += Len.P04_STAT_RICH_EST;
        MarshalByte.writeChar(buffer, position, p04FlStatEnd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p04DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, p04DsVer, Len.Int.P04_DS_VER, 0);
        position += Len.P04_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, p04DsTsCptz, Len.Int.P04_DS_TS_CPTZ, 0);
        position += Len.P04_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, p04DsUtente, Len.P04_DS_UTENTE);
        position += Len.P04_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, p04DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p04TpCausScarto, Len.P04_TP_CAUS_SCARTO);
        position += Len.P04_TP_CAUS_SCARTO;
        getP04DescErrVcharBytes(buffer, position);
        position += Len.P04_DESC_ERR_VCHAR;
        MarshalByte.writeString(buffer, position, p04UtenteInsAgg, Len.P04_UTENTE_INS_AGG);
        position += Len.P04_UTENTE_INS_AGG;
        MarshalByte.writeString(buffer, position, p04CodErrScarto, Len.P04_COD_ERR_SCARTO);
        position += Len.P04_COD_ERR_SCARTO;
        p04IdMoviCrz.getP04IdMoviCrzAsBuffer(buffer, position);
        return buffer;
    }

    public void setP04IdStatRichEst(int p04IdStatRichEst) {
        this.p04IdStatRichEst = p04IdStatRichEst;
    }

    public int getP04IdStatRichEst() {
        return this.p04IdStatRichEst;
    }

    public void setP04IdRichEst(int p04IdRichEst) {
        this.p04IdRichEst = p04IdRichEst;
    }

    public int getP04IdRichEst() {
        return this.p04IdRichEst;
    }

    public void setP04TsIniVldt(long p04TsIniVldt) {
        this.p04TsIniVldt = p04TsIniVldt;
    }

    public long getP04TsIniVldt() {
        return this.p04TsIniVldt;
    }

    public void setP04TsEndVldt(long p04TsEndVldt) {
        this.p04TsEndVldt = p04TsEndVldt;
    }

    public long getP04TsEndVldt() {
        return this.p04TsEndVldt;
    }

    public void setP04CodCompAnia(int p04CodCompAnia) {
        this.p04CodCompAnia = p04CodCompAnia;
    }

    public int getP04CodCompAnia() {
        return this.p04CodCompAnia;
    }

    public void setP04CodPrcs(String p04CodPrcs) {
        this.p04CodPrcs = Functions.subString(p04CodPrcs, Len.P04_COD_PRCS);
    }

    public String getP04CodPrcs() {
        return this.p04CodPrcs;
    }

    public void setP04CodAttvt(String p04CodAttvt) {
        this.p04CodAttvt = Functions.subString(p04CodAttvt, Len.P04_COD_ATTVT);
    }

    public String getP04CodAttvt() {
        return this.p04CodAttvt;
    }

    public void setP04StatRichEst(String p04StatRichEst) {
        this.p04StatRichEst = Functions.subString(p04StatRichEst, Len.P04_STAT_RICH_EST);
    }

    public String getP04StatRichEst() {
        return this.p04StatRichEst;
    }

    public void setP04FlStatEnd(char p04FlStatEnd) {
        this.p04FlStatEnd = p04FlStatEnd;
    }

    public char getP04FlStatEnd() {
        return this.p04FlStatEnd;
    }

    public void setP04DsOperSql(char p04DsOperSql) {
        this.p04DsOperSql = p04DsOperSql;
    }

    public char getP04DsOperSql() {
        return this.p04DsOperSql;
    }

    public void setP04DsVer(int p04DsVer) {
        this.p04DsVer = p04DsVer;
    }

    public int getP04DsVer() {
        return this.p04DsVer;
    }

    public void setP04DsTsCptz(long p04DsTsCptz) {
        this.p04DsTsCptz = p04DsTsCptz;
    }

    public long getP04DsTsCptz() {
        return this.p04DsTsCptz;
    }

    public void setP04DsUtente(String p04DsUtente) {
        this.p04DsUtente = Functions.subString(p04DsUtente, Len.P04_DS_UTENTE);
    }

    public String getP04DsUtente() {
        return this.p04DsUtente;
    }

    public void setP04DsStatoElab(char p04DsStatoElab) {
        this.p04DsStatoElab = p04DsStatoElab;
    }

    public char getP04DsStatoElab() {
        return this.p04DsStatoElab;
    }

    public void setP04TpCausScarto(String p04TpCausScarto) {
        this.p04TpCausScarto = Functions.subString(p04TpCausScarto, Len.P04_TP_CAUS_SCARTO);
    }

    public String getP04TpCausScarto() {
        return this.p04TpCausScarto;
    }

    public void setP04DescErrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p04DescErrLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p04DescErr = MarshalByte.readString(buffer, position, Len.P04_DESC_ERR);
    }

    public byte[] getP04DescErrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p04DescErrLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p04DescErr, Len.P04_DESC_ERR);
        return buffer;
    }

    public void setP04DescErrLen(short p04DescErrLen) {
        this.p04DescErrLen = p04DescErrLen;
    }

    public short getP04DescErrLen() {
        return this.p04DescErrLen;
    }

    public void setP04DescErr(String p04DescErr) {
        this.p04DescErr = Functions.subString(p04DescErr, Len.P04_DESC_ERR);
    }

    public String getP04DescErr() {
        return this.p04DescErr;
    }

    public void setP04UtenteInsAgg(String p04UtenteInsAgg) {
        this.p04UtenteInsAgg = Functions.subString(p04UtenteInsAgg, Len.P04_UTENTE_INS_AGG);
    }

    public String getP04UtenteInsAgg() {
        return this.p04UtenteInsAgg;
    }

    public void setP04CodErrScarto(String p04CodErrScarto) {
        this.p04CodErrScarto = Functions.subString(p04CodErrScarto, Len.P04_COD_ERR_SCARTO);
    }

    public String getP04CodErrScarto() {
        return this.p04CodErrScarto;
    }

    public P04IdMoviCrz getP04IdMoviCrz() {
        return p04IdMoviCrz;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P04_COD_PRCS = 3;
        public static final int P04_COD_ATTVT = 10;
        public static final int P04_STAT_RICH_EST = 2;
        public static final int P04_DS_UTENTE = 20;
        public static final int P04_TP_CAUS_SCARTO = 2;
        public static final int P04_DESC_ERR = 250;
        public static final int P04_UTENTE_INS_AGG = 20;
        public static final int P04_COD_ERR_SCARTO = 10;
        public static final int P04_ID_STAT_RICH_EST = 5;
        public static final int P04_ID_RICH_EST = 5;
        public static final int P04_TS_INI_VLDT = 10;
        public static final int P04_TS_END_VLDT = 10;
        public static final int P04_COD_COMP_ANIA = 3;
        public static final int P04_FL_STAT_END = 1;
        public static final int P04_DS_OPER_SQL = 1;
        public static final int P04_DS_VER = 5;
        public static final int P04_DS_TS_CPTZ = 10;
        public static final int P04_DS_STATO_ELAB = 1;
        public static final int P04_DESC_ERR_LEN = 2;
        public static final int P04_DESC_ERR_VCHAR = P04_DESC_ERR_LEN + P04_DESC_ERR;
        public static final int STAT_RICH_EST = P04_ID_STAT_RICH_EST + P04_ID_RICH_EST + P04_TS_INI_VLDT + P04_TS_END_VLDT + P04_COD_COMP_ANIA + P04_COD_PRCS + P04_COD_ATTVT + P04_STAT_RICH_EST + P04_FL_STAT_END + P04_DS_OPER_SQL + P04_DS_VER + P04_DS_TS_CPTZ + P04_DS_UTENTE + P04_DS_STATO_ELAB + P04_TP_CAUS_SCARTO + P04_DESC_ERR_VCHAR + P04_UTENTE_INS_AGG + P04_COD_ERR_SCARTO + P04IdMoviCrz.Len.P04_ID_MOVI_CRZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P04_ID_STAT_RICH_EST = 9;
            public static final int P04_ID_RICH_EST = 9;
            public static final int P04_TS_INI_VLDT = 18;
            public static final int P04_TS_END_VLDT = 18;
            public static final int P04_COD_COMP_ANIA = 5;
            public static final int P04_DS_VER = 9;
            public static final int P04_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
