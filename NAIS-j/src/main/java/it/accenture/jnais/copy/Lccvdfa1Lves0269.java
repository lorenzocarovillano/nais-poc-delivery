package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVDFA1<br>
 * Variable: LCCVDFA1 from copybook LCCVDFA1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvdfa1Lves0269 {

    //==== PROPERTIES ====
    /**Original name: VDFA-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA D_FISC_ADES
	 *    ALIAS DFA
	 *    ULTIMO AGG. 22 APR 2010
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: VDFA-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: VDFA-DATI
    private WdfaDati dati = new WdfaDati();

    //==== METHODS ====
    public int getIdPtf() {
        return this.idPtf;
    }

    public WdfaDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
