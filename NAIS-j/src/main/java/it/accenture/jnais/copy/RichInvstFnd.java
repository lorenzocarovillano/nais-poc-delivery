package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.RifDtCambioVlt;
import it.accenture.jnais.ws.redefines.RifDtInvst;
import it.accenture.jnais.ws.redefines.RifDtInvstCalc;
import it.accenture.jnais.ws.redefines.RifIdMoviChiu;
import it.accenture.jnais.ws.redefines.RifImpGapEvent;
import it.accenture.jnais.ws.redefines.RifImpMovto;
import it.accenture.jnais.ws.redefines.RifNumQuo;
import it.accenture.jnais.ws.redefines.RifPc;

/**Original name: RICH-INVST-FND<br>
 * Variable: RICH-INVST-FND from copybook IDBVRIF1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class RichInvstFnd {

    //==== PROPERTIES ====
    //Original name: RIF-ID-RICH-INVST-FND
    private int rifIdRichInvstFnd = DefaultValues.INT_VAL;
    //Original name: RIF-ID-MOVI-FINRIO
    private int rifIdMoviFinrio = DefaultValues.INT_VAL;
    //Original name: RIF-ID-MOVI-CRZ
    private int rifIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: RIF-ID-MOVI-CHIU
    private RifIdMoviChiu rifIdMoviChiu = new RifIdMoviChiu();
    //Original name: RIF-DT-INI-EFF
    private int rifDtIniEff = DefaultValues.INT_VAL;
    //Original name: RIF-DT-END-EFF
    private int rifDtEndEff = DefaultValues.INT_VAL;
    //Original name: RIF-COD-COMP-ANIA
    private int rifCodCompAnia = DefaultValues.INT_VAL;
    //Original name: RIF-COD-FND
    private String rifCodFnd = DefaultValues.stringVal(Len.RIF_COD_FND);
    //Original name: RIF-NUM-QUO
    private RifNumQuo rifNumQuo = new RifNumQuo();
    //Original name: RIF-PC
    private RifPc rifPc = new RifPc();
    //Original name: RIF-IMP-MOVTO
    private RifImpMovto rifImpMovto = new RifImpMovto();
    //Original name: RIF-DT-INVST
    private RifDtInvst rifDtInvst = new RifDtInvst();
    //Original name: RIF-COD-TARI
    private String rifCodTari = DefaultValues.stringVal(Len.RIF_COD_TARI);
    //Original name: RIF-TP-STAT
    private String rifTpStat = DefaultValues.stringVal(Len.RIF_TP_STAT);
    //Original name: RIF-TP-MOD-INVST
    private String rifTpModInvst = DefaultValues.stringVal(Len.RIF_TP_MOD_INVST);
    //Original name: RIF-COD-DIV
    private String rifCodDiv = DefaultValues.stringVal(Len.RIF_COD_DIV);
    //Original name: RIF-DT-CAMBIO-VLT
    private RifDtCambioVlt rifDtCambioVlt = new RifDtCambioVlt();
    //Original name: RIF-TP-FND
    private char rifTpFnd = DefaultValues.CHAR_VAL;
    //Original name: RIF-DS-RIGA
    private long rifDsRiga = DefaultValues.LONG_VAL;
    //Original name: RIF-DS-OPER-SQL
    private char rifDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: RIF-DS-VER
    private int rifDsVer = DefaultValues.INT_VAL;
    //Original name: RIF-DS-TS-INI-CPTZ
    private long rifDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: RIF-DS-TS-END-CPTZ
    private long rifDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: RIF-DS-UTENTE
    private String rifDsUtente = DefaultValues.stringVal(Len.RIF_DS_UTENTE);
    //Original name: RIF-DS-STATO-ELAB
    private char rifDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: RIF-DT-INVST-CALC
    private RifDtInvstCalc rifDtInvstCalc = new RifDtInvstCalc();
    //Original name: RIF-FL-CALC-INVTO
    private char rifFlCalcInvto = DefaultValues.CHAR_VAL;
    //Original name: RIF-IMP-GAP-EVENT
    private RifImpGapEvent rifImpGapEvent = new RifImpGapEvent();
    //Original name: RIF-FL-SWM-BP2S
    private char rifFlSwmBp2s = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setRichInvstFndFormatted(String data) {
        byte[] buffer = new byte[Len.RICH_INVST_FND];
        MarshalByte.writeString(buffer, 1, data, Len.RICH_INVST_FND);
        setRichInvstFndBytes(buffer, 1);
    }

    public String getRichInvstFndFormatted() {
        return MarshalByteExt.bufferToStr(getRichInvstFndBytes());
    }

    public byte[] getRichInvstFndBytes() {
        byte[] buffer = new byte[Len.RICH_INVST_FND];
        return getRichInvstFndBytes(buffer, 1);
    }

    public void setRichInvstFndBytes(byte[] buffer, int offset) {
        int position = offset;
        rifIdRichInvstFnd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIF_ID_RICH_INVST_FND, 0);
        position += Len.RIF_ID_RICH_INVST_FND;
        rifIdMoviFinrio = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIF_ID_MOVI_FINRIO, 0);
        position += Len.RIF_ID_MOVI_FINRIO;
        rifIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIF_ID_MOVI_CRZ, 0);
        position += Len.RIF_ID_MOVI_CRZ;
        rifIdMoviChiu.setRifIdMoviChiuFromBuffer(buffer, position);
        position += RifIdMoviChiu.Len.RIF_ID_MOVI_CHIU;
        rifDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIF_DT_INI_EFF, 0);
        position += Len.RIF_DT_INI_EFF;
        rifDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIF_DT_END_EFF, 0);
        position += Len.RIF_DT_END_EFF;
        rifCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIF_COD_COMP_ANIA, 0);
        position += Len.RIF_COD_COMP_ANIA;
        rifCodFnd = MarshalByte.readString(buffer, position, Len.RIF_COD_FND);
        position += Len.RIF_COD_FND;
        rifNumQuo.setRifNumQuoFromBuffer(buffer, position);
        position += RifNumQuo.Len.RIF_NUM_QUO;
        rifPc.setRifPcFromBuffer(buffer, position);
        position += RifPc.Len.RIF_PC;
        rifImpMovto.setRifImpMovtoFromBuffer(buffer, position);
        position += RifImpMovto.Len.RIF_IMP_MOVTO;
        rifDtInvst.setRifDtInvstFromBuffer(buffer, position);
        position += RifDtInvst.Len.RIF_DT_INVST;
        rifCodTari = MarshalByte.readString(buffer, position, Len.RIF_COD_TARI);
        position += Len.RIF_COD_TARI;
        rifTpStat = MarshalByte.readString(buffer, position, Len.RIF_TP_STAT);
        position += Len.RIF_TP_STAT;
        rifTpModInvst = MarshalByte.readString(buffer, position, Len.RIF_TP_MOD_INVST);
        position += Len.RIF_TP_MOD_INVST;
        rifCodDiv = MarshalByte.readString(buffer, position, Len.RIF_COD_DIV);
        position += Len.RIF_COD_DIV;
        rifDtCambioVlt.setRifDtCambioVltFromBuffer(buffer, position);
        position += RifDtCambioVlt.Len.RIF_DT_CAMBIO_VLT;
        rifTpFnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rifDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RIF_DS_RIGA, 0);
        position += Len.RIF_DS_RIGA;
        rifDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rifDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIF_DS_VER, 0);
        position += Len.RIF_DS_VER;
        rifDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RIF_DS_TS_INI_CPTZ, 0);
        position += Len.RIF_DS_TS_INI_CPTZ;
        rifDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RIF_DS_TS_END_CPTZ, 0);
        position += Len.RIF_DS_TS_END_CPTZ;
        rifDsUtente = MarshalByte.readString(buffer, position, Len.RIF_DS_UTENTE);
        position += Len.RIF_DS_UTENTE;
        rifDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rifDtInvstCalc.setRifDtInvstCalcFromBuffer(buffer, position);
        position += RifDtInvstCalc.Len.RIF_DT_INVST_CALC;
        rifFlCalcInvto = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rifImpGapEvent.setRifImpGapEventFromBuffer(buffer, position);
        position += RifImpGapEvent.Len.RIF_IMP_GAP_EVENT;
        rifFlSwmBp2s = MarshalByte.readChar(buffer, position);
    }

    public byte[] getRichInvstFndBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, rifIdRichInvstFnd, Len.Int.RIF_ID_RICH_INVST_FND, 0);
        position += Len.RIF_ID_RICH_INVST_FND;
        MarshalByte.writeIntAsPacked(buffer, position, rifIdMoviFinrio, Len.Int.RIF_ID_MOVI_FINRIO, 0);
        position += Len.RIF_ID_MOVI_FINRIO;
        MarshalByte.writeIntAsPacked(buffer, position, rifIdMoviCrz, Len.Int.RIF_ID_MOVI_CRZ, 0);
        position += Len.RIF_ID_MOVI_CRZ;
        rifIdMoviChiu.getRifIdMoviChiuAsBuffer(buffer, position);
        position += RifIdMoviChiu.Len.RIF_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, rifDtIniEff, Len.Int.RIF_DT_INI_EFF, 0);
        position += Len.RIF_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, rifDtEndEff, Len.Int.RIF_DT_END_EFF, 0);
        position += Len.RIF_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, rifCodCompAnia, Len.Int.RIF_COD_COMP_ANIA, 0);
        position += Len.RIF_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, rifCodFnd, Len.RIF_COD_FND);
        position += Len.RIF_COD_FND;
        rifNumQuo.getRifNumQuoAsBuffer(buffer, position);
        position += RifNumQuo.Len.RIF_NUM_QUO;
        rifPc.getRifPcAsBuffer(buffer, position);
        position += RifPc.Len.RIF_PC;
        rifImpMovto.getRifImpMovtoAsBuffer(buffer, position);
        position += RifImpMovto.Len.RIF_IMP_MOVTO;
        rifDtInvst.getRifDtInvstAsBuffer(buffer, position);
        position += RifDtInvst.Len.RIF_DT_INVST;
        MarshalByte.writeString(buffer, position, rifCodTari, Len.RIF_COD_TARI);
        position += Len.RIF_COD_TARI;
        MarshalByte.writeString(buffer, position, rifTpStat, Len.RIF_TP_STAT);
        position += Len.RIF_TP_STAT;
        MarshalByte.writeString(buffer, position, rifTpModInvst, Len.RIF_TP_MOD_INVST);
        position += Len.RIF_TP_MOD_INVST;
        MarshalByte.writeString(buffer, position, rifCodDiv, Len.RIF_COD_DIV);
        position += Len.RIF_COD_DIV;
        rifDtCambioVlt.getRifDtCambioVltAsBuffer(buffer, position);
        position += RifDtCambioVlt.Len.RIF_DT_CAMBIO_VLT;
        MarshalByte.writeChar(buffer, position, rifTpFnd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, rifDsRiga, Len.Int.RIF_DS_RIGA, 0);
        position += Len.RIF_DS_RIGA;
        MarshalByte.writeChar(buffer, position, rifDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, rifDsVer, Len.Int.RIF_DS_VER, 0);
        position += Len.RIF_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, rifDsTsIniCptz, Len.Int.RIF_DS_TS_INI_CPTZ, 0);
        position += Len.RIF_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, rifDsTsEndCptz, Len.Int.RIF_DS_TS_END_CPTZ, 0);
        position += Len.RIF_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, rifDsUtente, Len.RIF_DS_UTENTE);
        position += Len.RIF_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, rifDsStatoElab);
        position += Types.CHAR_SIZE;
        rifDtInvstCalc.getRifDtInvstCalcAsBuffer(buffer, position);
        position += RifDtInvstCalc.Len.RIF_DT_INVST_CALC;
        MarshalByte.writeChar(buffer, position, rifFlCalcInvto);
        position += Types.CHAR_SIZE;
        rifImpGapEvent.getRifImpGapEventAsBuffer(buffer, position);
        position += RifImpGapEvent.Len.RIF_IMP_GAP_EVENT;
        MarshalByte.writeChar(buffer, position, rifFlSwmBp2s);
        return buffer;
    }

    public void setRifIdRichInvstFnd(int rifIdRichInvstFnd) {
        this.rifIdRichInvstFnd = rifIdRichInvstFnd;
    }

    public int getRifIdRichInvstFnd() {
        return this.rifIdRichInvstFnd;
    }

    public void setRifIdMoviFinrio(int rifIdMoviFinrio) {
        this.rifIdMoviFinrio = rifIdMoviFinrio;
    }

    public int getRifIdMoviFinrio() {
        return this.rifIdMoviFinrio;
    }

    public void setRifIdMoviCrz(int rifIdMoviCrz) {
        this.rifIdMoviCrz = rifIdMoviCrz;
    }

    public int getRifIdMoviCrz() {
        return this.rifIdMoviCrz;
    }

    public void setRifDtIniEff(int rifDtIniEff) {
        this.rifDtIniEff = rifDtIniEff;
    }

    public int getRifDtIniEff() {
        return this.rifDtIniEff;
    }

    public void setRifDtEndEff(int rifDtEndEff) {
        this.rifDtEndEff = rifDtEndEff;
    }

    public int getRifDtEndEff() {
        return this.rifDtEndEff;
    }

    public void setRifCodCompAnia(int rifCodCompAnia) {
        this.rifCodCompAnia = rifCodCompAnia;
    }

    public int getRifCodCompAnia() {
        return this.rifCodCompAnia;
    }

    public void setRifCodFnd(String rifCodFnd) {
        this.rifCodFnd = Functions.subString(rifCodFnd, Len.RIF_COD_FND);
    }

    public String getRifCodFnd() {
        return this.rifCodFnd;
    }

    public String getRifCodFndFormatted() {
        return Functions.padBlanks(getRifCodFnd(), Len.RIF_COD_FND);
    }

    public void setRifCodTari(String rifCodTari) {
        this.rifCodTari = Functions.subString(rifCodTari, Len.RIF_COD_TARI);
    }

    public String getRifCodTari() {
        return this.rifCodTari;
    }

    public String getRifCodTariFormatted() {
        return Functions.padBlanks(getRifCodTari(), Len.RIF_COD_TARI);
    }

    public void setRifTpStat(String rifTpStat) {
        this.rifTpStat = Functions.subString(rifTpStat, Len.RIF_TP_STAT);
    }

    public String getRifTpStat() {
        return this.rifTpStat;
    }

    public String getRifTpStatFormatted() {
        return Functions.padBlanks(getRifTpStat(), Len.RIF_TP_STAT);
    }

    public void setRifTpModInvst(String rifTpModInvst) {
        this.rifTpModInvst = Functions.subString(rifTpModInvst, Len.RIF_TP_MOD_INVST);
    }

    public String getRifTpModInvst() {
        return this.rifTpModInvst;
    }

    public String getRifTpModInvstFormatted() {
        return Functions.padBlanks(getRifTpModInvst(), Len.RIF_TP_MOD_INVST);
    }

    public void setRifCodDiv(String rifCodDiv) {
        this.rifCodDiv = Functions.subString(rifCodDiv, Len.RIF_COD_DIV);
    }

    public String getRifCodDiv() {
        return this.rifCodDiv;
    }

    public void setRifTpFnd(char rifTpFnd) {
        this.rifTpFnd = rifTpFnd;
    }

    public char getRifTpFnd() {
        return this.rifTpFnd;
    }

    public void setRifDsRiga(long rifDsRiga) {
        this.rifDsRiga = rifDsRiga;
    }

    public long getRifDsRiga() {
        return this.rifDsRiga;
    }

    public void setRifDsOperSql(char rifDsOperSql) {
        this.rifDsOperSql = rifDsOperSql;
    }

    public char getRifDsOperSql() {
        return this.rifDsOperSql;
    }

    public void setRifDsVer(int rifDsVer) {
        this.rifDsVer = rifDsVer;
    }

    public int getRifDsVer() {
        return this.rifDsVer;
    }

    public void setRifDsTsIniCptz(long rifDsTsIniCptz) {
        this.rifDsTsIniCptz = rifDsTsIniCptz;
    }

    public long getRifDsTsIniCptz() {
        return this.rifDsTsIniCptz;
    }

    public void setRifDsTsEndCptz(long rifDsTsEndCptz) {
        this.rifDsTsEndCptz = rifDsTsEndCptz;
    }

    public long getRifDsTsEndCptz() {
        return this.rifDsTsEndCptz;
    }

    public void setRifDsUtente(String rifDsUtente) {
        this.rifDsUtente = Functions.subString(rifDsUtente, Len.RIF_DS_UTENTE);
    }

    public String getRifDsUtente() {
        return this.rifDsUtente;
    }

    public void setRifDsStatoElab(char rifDsStatoElab) {
        this.rifDsStatoElab = rifDsStatoElab;
    }

    public char getRifDsStatoElab() {
        return this.rifDsStatoElab;
    }

    public void setRifFlCalcInvto(char rifFlCalcInvto) {
        this.rifFlCalcInvto = rifFlCalcInvto;
    }

    public char getRifFlCalcInvto() {
        return this.rifFlCalcInvto;
    }

    public void setRifFlSwmBp2s(char rifFlSwmBp2s) {
        this.rifFlSwmBp2s = rifFlSwmBp2s;
    }

    public char getRifFlSwmBp2s() {
        return this.rifFlSwmBp2s;
    }

    public RifDtCambioVlt getRifDtCambioVlt() {
        return rifDtCambioVlt;
    }

    public RifDtInvst getRifDtInvst() {
        return rifDtInvst;
    }

    public RifDtInvstCalc getRifDtInvstCalc() {
        return rifDtInvstCalc;
    }

    public RifIdMoviChiu getRifIdMoviChiu() {
        return rifIdMoviChiu;
    }

    public RifImpGapEvent getRifImpGapEvent() {
        return rifImpGapEvent;
    }

    public RifImpMovto getRifImpMovto() {
        return rifImpMovto;
    }

    public RifNumQuo getRifNumQuo() {
        return rifNumQuo;
    }

    public RifPc getRifPc() {
        return rifPc;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RIF_COD_FND = 20;
        public static final int RIF_COD_TARI = 12;
        public static final int RIF_TP_STAT = 2;
        public static final int RIF_TP_MOD_INVST = 2;
        public static final int RIF_COD_DIV = 20;
        public static final int RIF_DS_UTENTE = 20;
        public static final int RIF_ID_RICH_INVST_FND = 5;
        public static final int RIF_ID_MOVI_FINRIO = 5;
        public static final int RIF_ID_MOVI_CRZ = 5;
        public static final int RIF_DT_INI_EFF = 5;
        public static final int RIF_DT_END_EFF = 5;
        public static final int RIF_COD_COMP_ANIA = 3;
        public static final int RIF_TP_FND = 1;
        public static final int RIF_DS_RIGA = 6;
        public static final int RIF_DS_OPER_SQL = 1;
        public static final int RIF_DS_VER = 5;
        public static final int RIF_DS_TS_INI_CPTZ = 10;
        public static final int RIF_DS_TS_END_CPTZ = 10;
        public static final int RIF_DS_STATO_ELAB = 1;
        public static final int RIF_FL_CALC_INVTO = 1;
        public static final int RIF_FL_SWM_BP2S = 1;
        public static final int RICH_INVST_FND = RIF_ID_RICH_INVST_FND + RIF_ID_MOVI_FINRIO + RIF_ID_MOVI_CRZ + RifIdMoviChiu.Len.RIF_ID_MOVI_CHIU + RIF_DT_INI_EFF + RIF_DT_END_EFF + RIF_COD_COMP_ANIA + RIF_COD_FND + RifNumQuo.Len.RIF_NUM_QUO + RifPc.Len.RIF_PC + RifImpMovto.Len.RIF_IMP_MOVTO + RifDtInvst.Len.RIF_DT_INVST + RIF_COD_TARI + RIF_TP_STAT + RIF_TP_MOD_INVST + RIF_COD_DIV + RifDtCambioVlt.Len.RIF_DT_CAMBIO_VLT + RIF_TP_FND + RIF_DS_RIGA + RIF_DS_OPER_SQL + RIF_DS_VER + RIF_DS_TS_INI_CPTZ + RIF_DS_TS_END_CPTZ + RIF_DS_UTENTE + RIF_DS_STATO_ELAB + RifDtInvstCalc.Len.RIF_DT_INVST_CALC + RIF_FL_CALC_INVTO + RifImpGapEvent.Len.RIF_IMP_GAP_EVENT + RIF_FL_SWM_BP2S;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIF_ID_RICH_INVST_FND = 9;
            public static final int RIF_ID_MOVI_FINRIO = 9;
            public static final int RIF_ID_MOVI_CRZ = 9;
            public static final int RIF_DT_INI_EFF = 8;
            public static final int RIF_DT_END_EFF = 8;
            public static final int RIF_COD_COMP_ANIA = 5;
            public static final int RIF_DS_RIGA = 10;
            public static final int RIF_DS_VER = 9;
            public static final int RIF_DS_TS_INI_CPTZ = 18;
            public static final int RIF_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
