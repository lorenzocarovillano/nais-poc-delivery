package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: ANA-BLOCCO<br>
 * Variable: ANA-BLOCCO from copybook IDBVXAB1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AnaBlocco {

    //==== PROPERTIES ====
    //Original name: XAB-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: XAB-COD-BLOCCO
    private String codBlocco = DefaultValues.stringVal(Len.COD_BLOCCO);
    //Original name: XAB-DESC-LEN
    private short descLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: XAB-DESC
    private String desc = DefaultValues.stringVal(Len.DESC);
    //Original name: XAB-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: XAB-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: XAB-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: XAB-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: XAB-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: XAB-FL-REC-AUT
    private char flRecAut = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setAnaBloccoFormatted(String data) {
        byte[] buffer = new byte[Len.ANA_BLOCCO];
        MarshalByte.writeString(buffer, 1, data, Len.ANA_BLOCCO);
        setAnaBloccoBytes(buffer, 1);
    }

    public String getAnaBloccoFormatted() {
        return MarshalByteExt.bufferToStr(getAnaBloccoBytes());
    }

    public byte[] getAnaBloccoBytes() {
        byte[] buffer = new byte[Len.ANA_BLOCCO];
        return getAnaBloccoBytes(buffer, 1);
    }

    public void setAnaBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        codBlocco = MarshalByte.readString(buffer, position, Len.COD_BLOCCO);
        position += Len.COD_BLOCCO;
        setDescVcharBytes(buffer, position);
        position += Len.DESC_VCHAR;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flRecAut = MarshalByte.readChar(buffer, position);
    }

    public byte[] getAnaBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, codBlocco, Len.COD_BLOCCO);
        position += Len.COD_BLOCCO;
        getDescVcharBytes(buffer, position);
        position += Len.DESC_VCHAR;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsCptz, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flRecAut);
        return buffer;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setCodBlocco(String codBlocco) {
        this.codBlocco = Functions.subString(codBlocco, Len.COD_BLOCCO);
    }

    public String getCodBlocco() {
        return this.codBlocco;
    }

    public void setDescVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        descLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        desc = MarshalByte.readString(buffer, position, Len.DESC);
    }

    public byte[] getDescVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, descLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, desc, Len.DESC);
        return buffer;
    }

    public void setDescLen(short descLen) {
        this.descLen = descLen;
    }

    public short getDescLen() {
        return this.descLen;
    }

    public void setDesc(String desc) {
        this.desc = Functions.subString(desc, Len.DESC);
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setFlRecAut(char flRecAut) {
        this.flRecAut = flRecAut;
    }

    public char getFlRecAut() {
        return this.flRecAut;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_BLOCCO = 5;
        public static final int DESC = 100;
        public static final int DS_UTENTE = 20;
        public static final int COD_COMP_ANIA = 3;
        public static final int DESC_LEN = 2;
        public static final int DESC_VCHAR = DESC_LEN + DESC;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_CPTZ = 10;
        public static final int DS_STATO_ELAB = 1;
        public static final int FL_REC_AUT = 1;
        public static final int ANA_BLOCCO = COD_COMP_ANIA + COD_BLOCCO + DESC_VCHAR + DS_OPER_SQL + DS_VER + DS_TS_CPTZ + DS_UTENTE + DS_STATO_ELAB + FL_REC_AUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_COMP_ANIA = 5;
            public static final int DS_VER = 9;
            public static final int DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
