package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: EST-TRCH-DI-GAR-DB<br>
 * Variable: EST-TRCH-DI-GAR-DB from copybook IDBVE123<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class EstTrchDiGarDb {

    //==== PROPERTIES ====
    //Original name: E12-DT-INI-EFF-DB
    private String iniEffDb = DefaultValues.stringVal(Len.INI_EFF_DB);
    //Original name: E12-DT-END-EFF-DB
    private String endEffDb = DefaultValues.stringVal(Len.END_EFF_DB);
    //Original name: E12-DT-EMIS-DB
    private String emisDb = DefaultValues.stringVal(Len.EMIS_DB);

    //==== METHODS ====
    public void setIniEffDb(String iniEffDb) {
        this.iniEffDb = Functions.subString(iniEffDb, Len.INI_EFF_DB);
    }

    public String getIniEffDb() {
        return this.iniEffDb;
    }

    public void setEndEffDb(String endEffDb) {
        this.endEffDb = Functions.subString(endEffDb, Len.END_EFF_DB);
    }

    public String getEndEffDb() {
        return this.endEffDb;
    }

    public void setEmisDb(String emisDb) {
        this.emisDb = Functions.subString(emisDb, Len.EMIS_DB);
    }

    public String getEmisDb() {
        return this.emisDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INI_EFF_DB = 10;
        public static final int END_EFF_DB = 10;
        public static final int EMIS_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
