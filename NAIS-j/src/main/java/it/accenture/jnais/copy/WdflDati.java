package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WdflAaCnbzDal2007Ef;
import it.accenture.jnais.ws.redefines.WdflAaCnbzEnd2000Ef;
import it.accenture.jnais.ws.redefines.WdflAaCnbzEnd2006Ef;
import it.accenture.jnais.ws.redefines.WdflAccpreAccCalc;
import it.accenture.jnais.ws.redefines.WdflAccpreAccDfz;
import it.accenture.jnais.ws.redefines.WdflAccpreAccEfflq;
import it.accenture.jnais.ws.redefines.WdflAccpreSostCalc;
import it.accenture.jnais.ws.redefines.WdflAccpreSostDfz;
import it.accenture.jnais.ws.redefines.WdflAccpreSostEfflq;
import it.accenture.jnais.ws.redefines.WdflAccpreVisCalc;
import it.accenture.jnais.ws.redefines.WdflAccpreVisDfz;
import it.accenture.jnais.ws.redefines.WdflAccpreVisEfflq;
import it.accenture.jnais.ws.redefines.WdflAlqCnbtInpstfmC;
import it.accenture.jnais.ws.redefines.WdflAlqCnbtInpstfmD;
import it.accenture.jnais.ws.redefines.WdflAlqCnbtInpstfmE;
import it.accenture.jnais.ws.redefines.WdflAlqTaxSepCalc;
import it.accenture.jnais.ws.redefines.WdflAlqTaxSepDfz;
import it.accenture.jnais.ws.redefines.WdflAlqTaxSepEfflq;
import it.accenture.jnais.ws.redefines.WdflCnbtInpstfmCalc;
import it.accenture.jnais.ws.redefines.WdflCnbtInpstfmDfz;
import it.accenture.jnais.ws.redefines.WdflCnbtInpstfmEfflq;
import it.accenture.jnais.ws.redefines.WdflCndeDal2007Calc;
import it.accenture.jnais.ws.redefines.WdflCndeDal2007Dfz;
import it.accenture.jnais.ws.redefines.WdflCndeDal2007Efflq;
import it.accenture.jnais.ws.redefines.WdflCndeEnd2000Calc;
import it.accenture.jnais.ws.redefines.WdflCndeEnd2000Dfz;
import it.accenture.jnais.ws.redefines.WdflCndeEnd2000Efflq;
import it.accenture.jnais.ws.redefines.WdflCndeEnd2006Calc;
import it.accenture.jnais.ws.redefines.WdflCndeEnd2006Dfz;
import it.accenture.jnais.ws.redefines.WdflCndeEnd2006Efflq;
import it.accenture.jnais.ws.redefines.WdflIcnbInpstfmCalc;
import it.accenture.jnais.ws.redefines.WdflIcnbInpstfmDfz;
import it.accenture.jnais.ws.redefines.WdflIcnbInpstfmEfflq;
import it.accenture.jnais.ws.redefines.WdflIdMoviChiu;
import it.accenture.jnais.ws.redefines.WdflIimpst252Calc;
import it.accenture.jnais.ws.redefines.WdflIimpst252Dfz;
import it.accenture.jnais.ws.redefines.WdflIimpst252Efflq;
import it.accenture.jnais.ws.redefines.WdflIimpstPrvrCalc;
import it.accenture.jnais.ws.redefines.WdflIimpstPrvrDfz;
import it.accenture.jnais.ws.redefines.WdflIimpstPrvrEfflq;
import it.accenture.jnais.ws.redefines.WdflIintPrestCalc;
import it.accenture.jnais.ws.redefines.WdflIintPrestDfz;
import it.accenture.jnais.ws.redefines.WdflIintPrestEfflq;
import it.accenture.jnais.ws.redefines.WdflImpbBolloDettC;
import it.accenture.jnais.ws.redefines.WdflImpbBolloDettD;
import it.accenture.jnais.ws.redefines.WdflImpbBolloDettL;
import it.accenture.jnais.ws.redefines.WdflImpbIs1382011c;
import it.accenture.jnais.ws.redefines.WdflImpbIs1382011d;
import it.accenture.jnais.ws.redefines.WdflImpbIs1382011l;
import it.accenture.jnais.ws.redefines.WdflImpbIs662014c;
import it.accenture.jnais.ws.redefines.WdflImpbIs662014d;
import it.accenture.jnais.ws.redefines.WdflImpbIs662014l;
import it.accenture.jnais.ws.redefines.WdflImpbIsCalc;
import it.accenture.jnais.ws.redefines.WdflImpbIsDfz;
import it.accenture.jnais.ws.redefines.WdflImpbIsEfflq;
import it.accenture.jnais.ws.redefines.WdflImpbRitAccCalc;
import it.accenture.jnais.ws.redefines.WdflImpbRitAccDfz;
import it.accenture.jnais.ws.redefines.WdflImpbRitAccEfflq;
import it.accenture.jnais.ws.redefines.WdflImpbTaxSepCalc;
import it.accenture.jnais.ws.redefines.WdflImpbTaxSepDfz;
import it.accenture.jnais.ws.redefines.WdflImpbTaxSepEfflq;
import it.accenture.jnais.ws.redefines.WdflImpbTfrCalc;
import it.accenture.jnais.ws.redefines.WdflImpbTfrDfz;
import it.accenture.jnais.ws.redefines.WdflImpbTfrEfflq;
import it.accenture.jnais.ws.redefines.WdflImpbVis1382011c;
import it.accenture.jnais.ws.redefines.WdflImpbVis1382011d;
import it.accenture.jnais.ws.redefines.WdflImpbVis1382011l;
import it.accenture.jnais.ws.redefines.WdflImpbVis662014c;
import it.accenture.jnais.ws.redefines.WdflImpbVis662014d;
import it.accenture.jnais.ws.redefines.WdflImpbVis662014l;
import it.accenture.jnais.ws.redefines.WdflImpbVisCalc;
import it.accenture.jnais.ws.redefines.WdflImpbVisDfz;
import it.accenture.jnais.ws.redefines.WdflImpbVisEfflq;
import it.accenture.jnais.ws.redefines.WdflImpExcontrEff;
import it.accenture.jnais.ws.redefines.WdflImpIntrRitPagC;
import it.accenture.jnais.ws.redefines.WdflImpIntrRitPagD;
import it.accenture.jnais.ws.redefines.WdflImpIntrRitPagL;
import it.accenture.jnais.ws.redefines.WdflImpLrdCalc;
import it.accenture.jnais.ws.redefines.WdflImpLrdDfz;
import it.accenture.jnais.ws.redefines.WdflImpLrdEfflq;
import it.accenture.jnais.ws.redefines.WdflImpNetCalc;
import it.accenture.jnais.ws.redefines.WdflImpNetDfz;
import it.accenture.jnais.ws.redefines.WdflImpNetEfflq;
import it.accenture.jnais.ws.redefines.WdflImpst252Calc;
import it.accenture.jnais.ws.redefines.WdflImpst252Efflq;
import it.accenture.jnais.ws.redefines.WdflImpstBolloDettC;
import it.accenture.jnais.ws.redefines.WdflImpstBolloDettD;
import it.accenture.jnais.ws.redefines.WdflImpstBolloDettL;
import it.accenture.jnais.ws.redefines.WdflImpstBolloTotVc;
import it.accenture.jnais.ws.redefines.WdflImpstBolloTotVd;
import it.accenture.jnais.ws.redefines.WdflImpstBolloTotVl;
import it.accenture.jnais.ws.redefines.WdflImpstDaRimbEff;
import it.accenture.jnais.ws.redefines.WdflImpstPrvrCalc;
import it.accenture.jnais.ws.redefines.WdflImpstPrvrDfz;
import it.accenture.jnais.ws.redefines.WdflImpstPrvrEfflq;
import it.accenture.jnais.ws.redefines.WdflImpstSostCalc;
import it.accenture.jnais.ws.redefines.WdflImpstSostDfz;
import it.accenture.jnais.ws.redefines.WdflImpstSostEfflq;
import it.accenture.jnais.ws.redefines.WdflImpstVis1382011c;
import it.accenture.jnais.ws.redefines.WdflImpstVis1382011d;
import it.accenture.jnais.ws.redefines.WdflImpstVis1382011l;
import it.accenture.jnais.ws.redefines.WdflImpstVis662014c;
import it.accenture.jnais.ws.redefines.WdflImpstVis662014d;
import it.accenture.jnais.ws.redefines.WdflImpstVis662014l;
import it.accenture.jnais.ws.redefines.WdflImpstVisCalc;
import it.accenture.jnais.ws.redefines.WdflImpstVisDfz;
import it.accenture.jnais.ws.redefines.WdflImpstVisEfflq;
import it.accenture.jnais.ws.redefines.WdflIntrPrestCalc;
import it.accenture.jnais.ws.redefines.WdflIntrPrestDfz;
import it.accenture.jnais.ws.redefines.WdflIntrPrestEfflq;
import it.accenture.jnais.ws.redefines.WdflIs1382011c;
import it.accenture.jnais.ws.redefines.WdflIs1382011d;
import it.accenture.jnais.ws.redefines.WdflIs1382011l;
import it.accenture.jnais.ws.redefines.WdflIs662014c;
import it.accenture.jnais.ws.redefines.WdflIs662014d;
import it.accenture.jnais.ws.redefines.WdflIs662014l;
import it.accenture.jnais.ws.redefines.WdflMmCnbzDal2007Ef;
import it.accenture.jnais.ws.redefines.WdflMmCnbzEnd2000Ef;
import it.accenture.jnais.ws.redefines.WdflMmCnbzEnd2006Ef;
import it.accenture.jnais.ws.redefines.WdflMontDal2007Calc;
import it.accenture.jnais.ws.redefines.WdflMontDal2007Dfz;
import it.accenture.jnais.ws.redefines.WdflMontDal2007Efflq;
import it.accenture.jnais.ws.redefines.WdflMontEnd2000Calc;
import it.accenture.jnais.ws.redefines.WdflMontEnd2000Dfz;
import it.accenture.jnais.ws.redefines.WdflMontEnd2000Efflq;
import it.accenture.jnais.ws.redefines.WdflMontEnd2006Calc;
import it.accenture.jnais.ws.redefines.WdflMontEnd2006Dfz;
import it.accenture.jnais.ws.redefines.WdflMontEnd2006Efflq;
import it.accenture.jnais.ws.redefines.WdflResPreAttCalc;
import it.accenture.jnais.ws.redefines.WdflResPreAttDfz;
import it.accenture.jnais.ws.redefines.WdflResPreAttEfflq;
import it.accenture.jnais.ws.redefines.WdflResPrstzCalc;
import it.accenture.jnais.ws.redefines.WdflResPrstzDfz;
import it.accenture.jnais.ws.redefines.WdflResPrstzEfflq;
import it.accenture.jnais.ws.redefines.WdflRitAccCalc;
import it.accenture.jnais.ws.redefines.WdflRitAccDfz;
import it.accenture.jnais.ws.redefines.WdflRitAccEfflq;
import it.accenture.jnais.ws.redefines.WdflRitIrpefCalc;
import it.accenture.jnais.ws.redefines.WdflRitIrpefDfz;
import it.accenture.jnais.ws.redefines.WdflRitIrpefEfflq;
import it.accenture.jnais.ws.redefines.WdflRitTfrCalc;
import it.accenture.jnais.ws.redefines.WdflRitTfrDfz;
import it.accenture.jnais.ws.redefines.WdflRitTfrEfflq;
import it.accenture.jnais.ws.redefines.WdflTaxSepCalc;
import it.accenture.jnais.ws.redefines.WdflTaxSepDfz;
import it.accenture.jnais.ws.redefines.WdflTaxSepEfflq;

/**Original name: WDFL-DATI<br>
 * Variable: WDFL-DATI from copybook LCCVDFL1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WdflDati {

    //==== PROPERTIES ====
    //Original name: WDFL-ID-D-FORZ-LIQ
    private int wdflIdDForzLiq = DefaultValues.INT_VAL;
    //Original name: WDFL-ID-LIQ
    private int wdflIdLiq = DefaultValues.INT_VAL;
    //Original name: WDFL-ID-MOVI-CRZ
    private int wdflIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WDFL-ID-MOVI-CHIU
    private WdflIdMoviChiu wdflIdMoviChiu = new WdflIdMoviChiu();
    //Original name: WDFL-COD-COMP-ANIA
    private int wdflCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WDFL-DT-INI-EFF
    private int wdflDtIniEff = DefaultValues.INT_VAL;
    //Original name: WDFL-DT-END-EFF
    private int wdflDtEndEff = DefaultValues.INT_VAL;
    //Original name: WDFL-IMP-LRD-CALC
    private WdflImpLrdCalc wdflImpLrdCalc = new WdflImpLrdCalc();
    //Original name: WDFL-IMP-LRD-DFZ
    private WdflImpLrdDfz wdflImpLrdDfz = new WdflImpLrdDfz();
    //Original name: WDFL-IMP-LRD-EFFLQ
    private WdflImpLrdEfflq wdflImpLrdEfflq = new WdflImpLrdEfflq();
    //Original name: WDFL-IMP-NET-CALC
    private WdflImpNetCalc wdflImpNetCalc = new WdflImpNetCalc();
    //Original name: WDFL-IMP-NET-DFZ
    private WdflImpNetDfz wdflImpNetDfz = new WdflImpNetDfz();
    //Original name: WDFL-IMP-NET-EFFLQ
    private WdflImpNetEfflq wdflImpNetEfflq = new WdflImpNetEfflq();
    //Original name: WDFL-IMPST-PRVR-CALC
    private WdflImpstPrvrCalc wdflImpstPrvrCalc = new WdflImpstPrvrCalc();
    //Original name: WDFL-IMPST-PRVR-DFZ
    private WdflImpstPrvrDfz wdflImpstPrvrDfz = new WdflImpstPrvrDfz();
    //Original name: WDFL-IMPST-PRVR-EFFLQ
    private WdflImpstPrvrEfflq wdflImpstPrvrEfflq = new WdflImpstPrvrEfflq();
    //Original name: WDFL-IMPST-VIS-CALC
    private WdflImpstVisCalc wdflImpstVisCalc = new WdflImpstVisCalc();
    //Original name: WDFL-IMPST-VIS-DFZ
    private WdflImpstVisDfz wdflImpstVisDfz = new WdflImpstVisDfz();
    //Original name: WDFL-IMPST-VIS-EFFLQ
    private WdflImpstVisEfflq wdflImpstVisEfflq = new WdflImpstVisEfflq();
    //Original name: WDFL-RIT-ACC-CALC
    private WdflRitAccCalc wdflRitAccCalc = new WdflRitAccCalc();
    //Original name: WDFL-RIT-ACC-DFZ
    private WdflRitAccDfz wdflRitAccDfz = new WdflRitAccDfz();
    //Original name: WDFL-RIT-ACC-EFFLQ
    private WdflRitAccEfflq wdflRitAccEfflq = new WdflRitAccEfflq();
    //Original name: WDFL-RIT-IRPEF-CALC
    private WdflRitIrpefCalc wdflRitIrpefCalc = new WdflRitIrpefCalc();
    //Original name: WDFL-RIT-IRPEF-DFZ
    private WdflRitIrpefDfz wdflRitIrpefDfz = new WdflRitIrpefDfz();
    //Original name: WDFL-RIT-IRPEF-EFFLQ
    private WdflRitIrpefEfflq wdflRitIrpefEfflq = new WdflRitIrpefEfflq();
    //Original name: WDFL-IMPST-SOST-CALC
    private WdflImpstSostCalc wdflImpstSostCalc = new WdflImpstSostCalc();
    //Original name: WDFL-IMPST-SOST-DFZ
    private WdflImpstSostDfz wdflImpstSostDfz = new WdflImpstSostDfz();
    //Original name: WDFL-IMPST-SOST-EFFLQ
    private WdflImpstSostEfflq wdflImpstSostEfflq = new WdflImpstSostEfflq();
    //Original name: WDFL-TAX-SEP-CALC
    private WdflTaxSepCalc wdflTaxSepCalc = new WdflTaxSepCalc();
    //Original name: WDFL-TAX-SEP-DFZ
    private WdflTaxSepDfz wdflTaxSepDfz = new WdflTaxSepDfz();
    //Original name: WDFL-TAX-SEP-EFFLQ
    private WdflTaxSepEfflq wdflTaxSepEfflq = new WdflTaxSepEfflq();
    //Original name: WDFL-INTR-PREST-CALC
    private WdflIntrPrestCalc wdflIntrPrestCalc = new WdflIntrPrestCalc();
    //Original name: WDFL-INTR-PREST-DFZ
    private WdflIntrPrestDfz wdflIntrPrestDfz = new WdflIntrPrestDfz();
    //Original name: WDFL-INTR-PREST-EFFLQ
    private WdflIntrPrestEfflq wdflIntrPrestEfflq = new WdflIntrPrestEfflq();
    //Original name: WDFL-ACCPRE-SOST-CALC
    private WdflAccpreSostCalc wdflAccpreSostCalc = new WdflAccpreSostCalc();
    //Original name: WDFL-ACCPRE-SOST-DFZ
    private WdflAccpreSostDfz wdflAccpreSostDfz = new WdflAccpreSostDfz();
    //Original name: WDFL-ACCPRE-SOST-EFFLQ
    private WdflAccpreSostEfflq wdflAccpreSostEfflq = new WdflAccpreSostEfflq();
    //Original name: WDFL-ACCPRE-VIS-CALC
    private WdflAccpreVisCalc wdflAccpreVisCalc = new WdflAccpreVisCalc();
    //Original name: WDFL-ACCPRE-VIS-DFZ
    private WdflAccpreVisDfz wdflAccpreVisDfz = new WdflAccpreVisDfz();
    //Original name: WDFL-ACCPRE-VIS-EFFLQ
    private WdflAccpreVisEfflq wdflAccpreVisEfflq = new WdflAccpreVisEfflq();
    //Original name: WDFL-ACCPRE-ACC-CALC
    private WdflAccpreAccCalc wdflAccpreAccCalc = new WdflAccpreAccCalc();
    //Original name: WDFL-ACCPRE-ACC-DFZ
    private WdflAccpreAccDfz wdflAccpreAccDfz = new WdflAccpreAccDfz();
    //Original name: WDFL-ACCPRE-ACC-EFFLQ
    private WdflAccpreAccEfflq wdflAccpreAccEfflq = new WdflAccpreAccEfflq();
    //Original name: WDFL-RES-PRSTZ-CALC
    private WdflResPrstzCalc wdflResPrstzCalc = new WdflResPrstzCalc();
    //Original name: WDFL-RES-PRSTZ-DFZ
    private WdflResPrstzDfz wdflResPrstzDfz = new WdflResPrstzDfz();
    //Original name: WDFL-RES-PRSTZ-EFFLQ
    private WdflResPrstzEfflq wdflResPrstzEfflq = new WdflResPrstzEfflq();
    //Original name: WDFL-RES-PRE-ATT-CALC
    private WdflResPreAttCalc wdflResPreAttCalc = new WdflResPreAttCalc();
    //Original name: WDFL-RES-PRE-ATT-DFZ
    private WdflResPreAttDfz wdflResPreAttDfz = new WdflResPreAttDfz();
    //Original name: WDFL-RES-PRE-ATT-EFFLQ
    private WdflResPreAttEfflq wdflResPreAttEfflq = new WdflResPreAttEfflq();
    //Original name: WDFL-IMP-EXCONTR-EFF
    private WdflImpExcontrEff wdflImpExcontrEff = new WdflImpExcontrEff();
    //Original name: WDFL-IMPB-VIS-CALC
    private WdflImpbVisCalc wdflImpbVisCalc = new WdflImpbVisCalc();
    //Original name: WDFL-IMPB-VIS-EFFLQ
    private WdflImpbVisEfflq wdflImpbVisEfflq = new WdflImpbVisEfflq();
    //Original name: WDFL-IMPB-VIS-DFZ
    private WdflImpbVisDfz wdflImpbVisDfz = new WdflImpbVisDfz();
    //Original name: WDFL-IMPB-RIT-ACC-CALC
    private WdflImpbRitAccCalc wdflImpbRitAccCalc = new WdflImpbRitAccCalc();
    //Original name: WDFL-IMPB-RIT-ACC-EFFLQ
    private WdflImpbRitAccEfflq wdflImpbRitAccEfflq = new WdflImpbRitAccEfflq();
    //Original name: WDFL-IMPB-RIT-ACC-DFZ
    private WdflImpbRitAccDfz wdflImpbRitAccDfz = new WdflImpbRitAccDfz();
    //Original name: WDFL-IMPB-TFR-CALC
    private WdflImpbTfrCalc wdflImpbTfrCalc = new WdflImpbTfrCalc();
    //Original name: WDFL-IMPB-TFR-EFFLQ
    private WdflImpbTfrEfflq wdflImpbTfrEfflq = new WdflImpbTfrEfflq();
    //Original name: WDFL-IMPB-TFR-DFZ
    private WdflImpbTfrDfz wdflImpbTfrDfz = new WdflImpbTfrDfz();
    //Original name: WDFL-IMPB-IS-CALC
    private WdflImpbIsCalc wdflImpbIsCalc = new WdflImpbIsCalc();
    //Original name: WDFL-IMPB-IS-EFFLQ
    private WdflImpbIsEfflq wdflImpbIsEfflq = new WdflImpbIsEfflq();
    //Original name: WDFL-IMPB-IS-DFZ
    private WdflImpbIsDfz wdflImpbIsDfz = new WdflImpbIsDfz();
    //Original name: WDFL-IMPB-TAX-SEP-CALC
    private WdflImpbTaxSepCalc wdflImpbTaxSepCalc = new WdflImpbTaxSepCalc();
    //Original name: WDFL-IMPB-TAX-SEP-EFFLQ
    private WdflImpbTaxSepEfflq wdflImpbTaxSepEfflq = new WdflImpbTaxSepEfflq();
    //Original name: WDFL-IMPB-TAX-SEP-DFZ
    private WdflImpbTaxSepDfz wdflImpbTaxSepDfz = new WdflImpbTaxSepDfz();
    //Original name: WDFL-IINT-PREST-CALC
    private WdflIintPrestCalc wdflIintPrestCalc = new WdflIintPrestCalc();
    //Original name: WDFL-IINT-PREST-EFFLQ
    private WdflIintPrestEfflq wdflIintPrestEfflq = new WdflIintPrestEfflq();
    //Original name: WDFL-IINT-PREST-DFZ
    private WdflIintPrestDfz wdflIintPrestDfz = new WdflIintPrestDfz();
    //Original name: WDFL-MONT-END2000-CALC
    private WdflMontEnd2000Calc wdflMontEnd2000Calc = new WdflMontEnd2000Calc();
    //Original name: WDFL-MONT-END2000-EFFLQ
    private WdflMontEnd2000Efflq wdflMontEnd2000Efflq = new WdflMontEnd2000Efflq();
    //Original name: WDFL-MONT-END2000-DFZ
    private WdflMontEnd2000Dfz wdflMontEnd2000Dfz = new WdflMontEnd2000Dfz();
    //Original name: WDFL-MONT-END2006-CALC
    private WdflMontEnd2006Calc wdflMontEnd2006Calc = new WdflMontEnd2006Calc();
    //Original name: WDFL-MONT-END2006-EFFLQ
    private WdflMontEnd2006Efflq wdflMontEnd2006Efflq = new WdflMontEnd2006Efflq();
    //Original name: WDFL-MONT-END2006-DFZ
    private WdflMontEnd2006Dfz wdflMontEnd2006Dfz = new WdflMontEnd2006Dfz();
    //Original name: WDFL-MONT-DAL2007-CALC
    private WdflMontDal2007Calc wdflMontDal2007Calc = new WdflMontDal2007Calc();
    //Original name: WDFL-MONT-DAL2007-EFFLQ
    private WdflMontDal2007Efflq wdflMontDal2007Efflq = new WdflMontDal2007Efflq();
    //Original name: WDFL-MONT-DAL2007-DFZ
    private WdflMontDal2007Dfz wdflMontDal2007Dfz = new WdflMontDal2007Dfz();
    //Original name: WDFL-IIMPST-PRVR-CALC
    private WdflIimpstPrvrCalc wdflIimpstPrvrCalc = new WdflIimpstPrvrCalc();
    //Original name: WDFL-IIMPST-PRVR-EFFLQ
    private WdflIimpstPrvrEfflq wdflIimpstPrvrEfflq = new WdflIimpstPrvrEfflq();
    //Original name: WDFL-IIMPST-PRVR-DFZ
    private WdflIimpstPrvrDfz wdflIimpstPrvrDfz = new WdflIimpstPrvrDfz();
    //Original name: WDFL-IIMPST-252-CALC
    private WdflIimpst252Calc wdflIimpst252Calc = new WdflIimpst252Calc();
    //Original name: WDFL-IIMPST-252-EFFLQ
    private WdflIimpst252Efflq wdflIimpst252Efflq = new WdflIimpst252Efflq();
    //Original name: WDFL-IIMPST-252-DFZ
    private WdflIimpst252Dfz wdflIimpst252Dfz = new WdflIimpst252Dfz();
    //Original name: WDFL-IMPST-252-CALC
    private WdflImpst252Calc wdflImpst252Calc = new WdflImpst252Calc();
    //Original name: WDFL-IMPST-252-EFFLQ
    private WdflImpst252Efflq wdflImpst252Efflq = new WdflImpst252Efflq();
    //Original name: WDFL-RIT-TFR-CALC
    private WdflRitTfrCalc wdflRitTfrCalc = new WdflRitTfrCalc();
    //Original name: WDFL-RIT-TFR-EFFLQ
    private WdflRitTfrEfflq wdflRitTfrEfflq = new WdflRitTfrEfflq();
    //Original name: WDFL-RIT-TFR-DFZ
    private WdflRitTfrDfz wdflRitTfrDfz = new WdflRitTfrDfz();
    //Original name: WDFL-CNBT-INPSTFM-CALC
    private WdflCnbtInpstfmCalc wdflCnbtInpstfmCalc = new WdflCnbtInpstfmCalc();
    //Original name: WDFL-CNBT-INPSTFM-EFFLQ
    private WdflCnbtInpstfmEfflq wdflCnbtInpstfmEfflq = new WdflCnbtInpstfmEfflq();
    //Original name: WDFL-CNBT-INPSTFM-DFZ
    private WdflCnbtInpstfmDfz wdflCnbtInpstfmDfz = new WdflCnbtInpstfmDfz();
    //Original name: WDFL-ICNB-INPSTFM-CALC
    private WdflIcnbInpstfmCalc wdflIcnbInpstfmCalc = new WdflIcnbInpstfmCalc();
    //Original name: WDFL-ICNB-INPSTFM-EFFLQ
    private WdflIcnbInpstfmEfflq wdflIcnbInpstfmEfflq = new WdflIcnbInpstfmEfflq();
    //Original name: WDFL-ICNB-INPSTFM-DFZ
    private WdflIcnbInpstfmDfz wdflIcnbInpstfmDfz = new WdflIcnbInpstfmDfz();
    //Original name: WDFL-CNDE-END2000-CALC
    private WdflCndeEnd2000Calc wdflCndeEnd2000Calc = new WdflCndeEnd2000Calc();
    //Original name: WDFL-CNDE-END2000-EFFLQ
    private WdflCndeEnd2000Efflq wdflCndeEnd2000Efflq = new WdflCndeEnd2000Efflq();
    //Original name: WDFL-CNDE-END2000-DFZ
    private WdflCndeEnd2000Dfz wdflCndeEnd2000Dfz = new WdflCndeEnd2000Dfz();
    //Original name: WDFL-CNDE-END2006-CALC
    private WdflCndeEnd2006Calc wdflCndeEnd2006Calc = new WdflCndeEnd2006Calc();
    //Original name: WDFL-CNDE-END2006-EFFLQ
    private WdflCndeEnd2006Efflq wdflCndeEnd2006Efflq = new WdflCndeEnd2006Efflq();
    //Original name: WDFL-CNDE-END2006-DFZ
    private WdflCndeEnd2006Dfz wdflCndeEnd2006Dfz = new WdflCndeEnd2006Dfz();
    //Original name: WDFL-CNDE-DAL2007-CALC
    private WdflCndeDal2007Calc wdflCndeDal2007Calc = new WdflCndeDal2007Calc();
    //Original name: WDFL-CNDE-DAL2007-EFFLQ
    private WdflCndeDal2007Efflq wdflCndeDal2007Efflq = new WdflCndeDal2007Efflq();
    //Original name: WDFL-CNDE-DAL2007-DFZ
    private WdflCndeDal2007Dfz wdflCndeDal2007Dfz = new WdflCndeDal2007Dfz();
    //Original name: WDFL-AA-CNBZ-END2000-EF
    private WdflAaCnbzEnd2000Ef wdflAaCnbzEnd2000Ef = new WdflAaCnbzEnd2000Ef();
    //Original name: WDFL-AA-CNBZ-END2006-EF
    private WdflAaCnbzEnd2006Ef wdflAaCnbzEnd2006Ef = new WdflAaCnbzEnd2006Ef();
    //Original name: WDFL-AA-CNBZ-DAL2007-EF
    private WdflAaCnbzDal2007Ef wdflAaCnbzDal2007Ef = new WdflAaCnbzDal2007Ef();
    //Original name: WDFL-MM-CNBZ-END2000-EF
    private WdflMmCnbzEnd2000Ef wdflMmCnbzEnd2000Ef = new WdflMmCnbzEnd2000Ef();
    //Original name: WDFL-MM-CNBZ-END2006-EF
    private WdflMmCnbzEnd2006Ef wdflMmCnbzEnd2006Ef = new WdflMmCnbzEnd2006Ef();
    //Original name: WDFL-MM-CNBZ-DAL2007-EF
    private WdflMmCnbzDal2007Ef wdflMmCnbzDal2007Ef = new WdflMmCnbzDal2007Ef();
    //Original name: WDFL-IMPST-DA-RIMB-EFF
    private WdflImpstDaRimbEff wdflImpstDaRimbEff = new WdflImpstDaRimbEff();
    //Original name: WDFL-ALQ-TAX-SEP-CALC
    private WdflAlqTaxSepCalc wdflAlqTaxSepCalc = new WdflAlqTaxSepCalc();
    //Original name: WDFL-ALQ-TAX-SEP-EFFLQ
    private WdflAlqTaxSepEfflq wdflAlqTaxSepEfflq = new WdflAlqTaxSepEfflq();
    //Original name: WDFL-ALQ-TAX-SEP-DFZ
    private WdflAlqTaxSepDfz wdflAlqTaxSepDfz = new WdflAlqTaxSepDfz();
    //Original name: WDFL-ALQ-CNBT-INPSTFM-C
    private WdflAlqCnbtInpstfmC wdflAlqCnbtInpstfmC = new WdflAlqCnbtInpstfmC();
    //Original name: WDFL-ALQ-CNBT-INPSTFM-E
    private WdflAlqCnbtInpstfmE wdflAlqCnbtInpstfmE = new WdflAlqCnbtInpstfmE();
    //Original name: WDFL-ALQ-CNBT-INPSTFM-D
    private WdflAlqCnbtInpstfmD wdflAlqCnbtInpstfmD = new WdflAlqCnbtInpstfmD();
    //Original name: WDFL-DS-RIGA
    private long wdflDsRiga = DefaultValues.LONG_VAL;
    //Original name: WDFL-DS-OPER-SQL
    private char wdflDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WDFL-DS-VER
    private int wdflDsVer = DefaultValues.INT_VAL;
    //Original name: WDFL-DS-TS-INI-CPTZ
    private long wdflDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WDFL-DS-TS-END-CPTZ
    private long wdflDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WDFL-DS-UTENTE
    private String wdflDsUtente = DefaultValues.stringVal(Len.WDFL_DS_UTENTE);
    //Original name: WDFL-DS-STATO-ELAB
    private char wdflDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WDFL-IMPB-VIS-1382011C
    private WdflImpbVis1382011c wdflImpbVis1382011c = new WdflImpbVis1382011c();
    //Original name: WDFL-IMPB-VIS-1382011D
    private WdflImpbVis1382011d wdflImpbVis1382011d = new WdflImpbVis1382011d();
    //Original name: WDFL-IMPB-VIS-1382011L
    private WdflImpbVis1382011l wdflImpbVis1382011l = new WdflImpbVis1382011l();
    //Original name: WDFL-IMPST-VIS-1382011C
    private WdflImpstVis1382011c wdflImpstVis1382011c = new WdflImpstVis1382011c();
    //Original name: WDFL-IMPST-VIS-1382011D
    private WdflImpstVis1382011d wdflImpstVis1382011d = new WdflImpstVis1382011d();
    //Original name: WDFL-IMPST-VIS-1382011L
    private WdflImpstVis1382011l wdflImpstVis1382011l = new WdflImpstVis1382011l();
    //Original name: WDFL-IMPB-IS-1382011C
    private WdflImpbIs1382011c wdflImpbIs1382011c = new WdflImpbIs1382011c();
    //Original name: WDFL-IMPB-IS-1382011D
    private WdflImpbIs1382011d wdflImpbIs1382011d = new WdflImpbIs1382011d();
    //Original name: WDFL-IMPB-IS-1382011L
    private WdflImpbIs1382011l wdflImpbIs1382011l = new WdflImpbIs1382011l();
    //Original name: WDFL-IS-1382011C
    private WdflIs1382011c wdflIs1382011c = new WdflIs1382011c();
    //Original name: WDFL-IS-1382011D
    private WdflIs1382011d wdflIs1382011d = new WdflIs1382011d();
    //Original name: WDFL-IS-1382011L
    private WdflIs1382011l wdflIs1382011l = new WdflIs1382011l();
    //Original name: WDFL-IMP-INTR-RIT-PAG-C
    private WdflImpIntrRitPagC wdflImpIntrRitPagC = new WdflImpIntrRitPagC();
    //Original name: WDFL-IMP-INTR-RIT-PAG-D
    private WdflImpIntrRitPagD wdflImpIntrRitPagD = new WdflImpIntrRitPagD();
    //Original name: WDFL-IMP-INTR-RIT-PAG-L
    private WdflImpIntrRitPagL wdflImpIntrRitPagL = new WdflImpIntrRitPagL();
    //Original name: WDFL-IMPB-BOLLO-DETT-C
    private WdflImpbBolloDettC wdflImpbBolloDettC = new WdflImpbBolloDettC();
    //Original name: WDFL-IMPB-BOLLO-DETT-D
    private WdflImpbBolloDettD wdflImpbBolloDettD = new WdflImpbBolloDettD();
    //Original name: WDFL-IMPB-BOLLO-DETT-L
    private WdflImpbBolloDettL wdflImpbBolloDettL = new WdflImpbBolloDettL();
    //Original name: WDFL-IMPST-BOLLO-DETT-C
    private WdflImpstBolloDettC wdflImpstBolloDettC = new WdflImpstBolloDettC();
    //Original name: WDFL-IMPST-BOLLO-DETT-D
    private WdflImpstBolloDettD wdflImpstBolloDettD = new WdflImpstBolloDettD();
    //Original name: WDFL-IMPST-BOLLO-DETT-L
    private WdflImpstBolloDettL wdflImpstBolloDettL = new WdflImpstBolloDettL();
    //Original name: WDFL-IMPST-BOLLO-TOT-VC
    private WdflImpstBolloTotVc wdflImpstBolloTotVc = new WdflImpstBolloTotVc();
    //Original name: WDFL-IMPST-BOLLO-TOT-VD
    private WdflImpstBolloTotVd wdflImpstBolloTotVd = new WdflImpstBolloTotVd();
    //Original name: WDFL-IMPST-BOLLO-TOT-VL
    private WdflImpstBolloTotVl wdflImpstBolloTotVl = new WdflImpstBolloTotVl();
    //Original name: WDFL-IMPB-VIS-662014C
    private WdflImpbVis662014c wdflImpbVis662014c = new WdflImpbVis662014c();
    //Original name: WDFL-IMPB-VIS-662014D
    private WdflImpbVis662014d wdflImpbVis662014d = new WdflImpbVis662014d();
    //Original name: WDFL-IMPB-VIS-662014L
    private WdflImpbVis662014l wdflImpbVis662014l = new WdflImpbVis662014l();
    //Original name: WDFL-IMPST-VIS-662014C
    private WdflImpstVis662014c wdflImpstVis662014c = new WdflImpstVis662014c();
    //Original name: WDFL-IMPST-VIS-662014D
    private WdflImpstVis662014d wdflImpstVis662014d = new WdflImpstVis662014d();
    //Original name: WDFL-IMPST-VIS-662014L
    private WdflImpstVis662014l wdflImpstVis662014l = new WdflImpstVis662014l();
    //Original name: WDFL-IMPB-IS-662014C
    private WdflImpbIs662014c wdflImpbIs662014c = new WdflImpbIs662014c();
    //Original name: WDFL-IMPB-IS-662014D
    private WdflImpbIs662014d wdflImpbIs662014d = new WdflImpbIs662014d();
    //Original name: WDFL-IMPB-IS-662014L
    private WdflImpbIs662014l wdflImpbIs662014l = new WdflImpbIs662014l();
    //Original name: WDFL-IS-662014C
    private WdflIs662014c wdflIs662014c = new WdflIs662014c();
    //Original name: WDFL-IS-662014D
    private WdflIs662014d wdflIs662014d = new WdflIs662014d();
    //Original name: WDFL-IS-662014L
    private WdflIs662014l wdflIs662014l = new WdflIs662014l();

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wdflIdDForzLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFL_ID_D_FORZ_LIQ, 0);
        position += Len.WDFL_ID_D_FORZ_LIQ;
        wdflIdLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFL_ID_LIQ, 0);
        position += Len.WDFL_ID_LIQ;
        wdflIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFL_ID_MOVI_CRZ, 0);
        position += Len.WDFL_ID_MOVI_CRZ;
        wdflIdMoviChiu.setWdflIdMoviChiuFromBuffer(buffer, position);
        position += WdflIdMoviChiu.Len.WDFL_ID_MOVI_CHIU;
        wdflCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFL_COD_COMP_ANIA, 0);
        position += Len.WDFL_COD_COMP_ANIA;
        wdflDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFL_DT_INI_EFF, 0);
        position += Len.WDFL_DT_INI_EFF;
        wdflDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFL_DT_END_EFF, 0);
        position += Len.WDFL_DT_END_EFF;
        wdflImpLrdCalc.setWdflImpLrdCalcFromBuffer(buffer, position);
        position += WdflImpLrdCalc.Len.WDFL_IMP_LRD_CALC;
        wdflImpLrdDfz.setWdflImpLrdDfzFromBuffer(buffer, position);
        position += WdflImpLrdDfz.Len.WDFL_IMP_LRD_DFZ;
        wdflImpLrdEfflq.setWdflImpLrdEfflqFromBuffer(buffer, position);
        position += WdflImpLrdEfflq.Len.WDFL_IMP_LRD_EFFLQ;
        wdflImpNetCalc.setWdflImpNetCalcFromBuffer(buffer, position);
        position += WdflImpNetCalc.Len.WDFL_IMP_NET_CALC;
        wdflImpNetDfz.setWdflImpNetDfzFromBuffer(buffer, position);
        position += WdflImpNetDfz.Len.WDFL_IMP_NET_DFZ;
        wdflImpNetEfflq.setWdflImpNetEfflqFromBuffer(buffer, position);
        position += WdflImpNetEfflq.Len.WDFL_IMP_NET_EFFLQ;
        wdflImpstPrvrCalc.setWdflImpstPrvrCalcFromBuffer(buffer, position);
        position += WdflImpstPrvrCalc.Len.WDFL_IMPST_PRVR_CALC;
        wdflImpstPrvrDfz.setWdflImpstPrvrDfzFromBuffer(buffer, position);
        position += WdflImpstPrvrDfz.Len.WDFL_IMPST_PRVR_DFZ;
        wdflImpstPrvrEfflq.setWdflImpstPrvrEfflqFromBuffer(buffer, position);
        position += WdflImpstPrvrEfflq.Len.WDFL_IMPST_PRVR_EFFLQ;
        wdflImpstVisCalc.setWdflImpstVisCalcFromBuffer(buffer, position);
        position += WdflImpstVisCalc.Len.WDFL_IMPST_VIS_CALC;
        wdflImpstVisDfz.setWdflImpstVisDfzFromBuffer(buffer, position);
        position += WdflImpstVisDfz.Len.WDFL_IMPST_VIS_DFZ;
        wdflImpstVisEfflq.setWdflImpstVisEfflqFromBuffer(buffer, position);
        position += WdflImpstVisEfflq.Len.WDFL_IMPST_VIS_EFFLQ;
        wdflRitAccCalc.setWdflRitAccCalcFromBuffer(buffer, position);
        position += WdflRitAccCalc.Len.WDFL_RIT_ACC_CALC;
        wdflRitAccDfz.setWdflRitAccDfzFromBuffer(buffer, position);
        position += WdflRitAccDfz.Len.WDFL_RIT_ACC_DFZ;
        wdflRitAccEfflq.setWdflRitAccEfflqFromBuffer(buffer, position);
        position += WdflRitAccEfflq.Len.WDFL_RIT_ACC_EFFLQ;
        wdflRitIrpefCalc.setWdflRitIrpefCalcFromBuffer(buffer, position);
        position += WdflRitIrpefCalc.Len.WDFL_RIT_IRPEF_CALC;
        wdflRitIrpefDfz.setWdflRitIrpefDfzFromBuffer(buffer, position);
        position += WdflRitIrpefDfz.Len.WDFL_RIT_IRPEF_DFZ;
        wdflRitIrpefEfflq.setWdflRitIrpefEfflqFromBuffer(buffer, position);
        position += WdflRitIrpefEfflq.Len.WDFL_RIT_IRPEF_EFFLQ;
        wdflImpstSostCalc.setWdflImpstSostCalcFromBuffer(buffer, position);
        position += WdflImpstSostCalc.Len.WDFL_IMPST_SOST_CALC;
        wdflImpstSostDfz.setWdflImpstSostDfzFromBuffer(buffer, position);
        position += WdflImpstSostDfz.Len.WDFL_IMPST_SOST_DFZ;
        wdflImpstSostEfflq.setWdflImpstSostEfflqFromBuffer(buffer, position);
        position += WdflImpstSostEfflq.Len.WDFL_IMPST_SOST_EFFLQ;
        wdflTaxSepCalc.setWdflTaxSepCalcFromBuffer(buffer, position);
        position += WdflTaxSepCalc.Len.WDFL_TAX_SEP_CALC;
        wdflTaxSepDfz.setWdflTaxSepDfzFromBuffer(buffer, position);
        position += WdflTaxSepDfz.Len.WDFL_TAX_SEP_DFZ;
        wdflTaxSepEfflq.setWdflTaxSepEfflqFromBuffer(buffer, position);
        position += WdflTaxSepEfflq.Len.WDFL_TAX_SEP_EFFLQ;
        wdflIntrPrestCalc.setWdflIntrPrestCalcFromBuffer(buffer, position);
        position += WdflIntrPrestCalc.Len.WDFL_INTR_PREST_CALC;
        wdflIntrPrestDfz.setWdflIntrPrestDfzFromBuffer(buffer, position);
        position += WdflIntrPrestDfz.Len.WDFL_INTR_PREST_DFZ;
        wdflIntrPrestEfflq.setWdflIntrPrestEfflqFromBuffer(buffer, position);
        position += WdflIntrPrestEfflq.Len.WDFL_INTR_PREST_EFFLQ;
        wdflAccpreSostCalc.setWdflAccpreSostCalcFromBuffer(buffer, position);
        position += WdflAccpreSostCalc.Len.WDFL_ACCPRE_SOST_CALC;
        wdflAccpreSostDfz.setWdflAccpreSostDfzFromBuffer(buffer, position);
        position += WdflAccpreSostDfz.Len.WDFL_ACCPRE_SOST_DFZ;
        wdflAccpreSostEfflq.setWdflAccpreSostEfflqFromBuffer(buffer, position);
        position += WdflAccpreSostEfflq.Len.WDFL_ACCPRE_SOST_EFFLQ;
        wdflAccpreVisCalc.setWdflAccpreVisCalcFromBuffer(buffer, position);
        position += WdflAccpreVisCalc.Len.WDFL_ACCPRE_VIS_CALC;
        wdflAccpreVisDfz.setWdflAccpreVisDfzFromBuffer(buffer, position);
        position += WdflAccpreVisDfz.Len.WDFL_ACCPRE_VIS_DFZ;
        wdflAccpreVisEfflq.setWdflAccpreVisEfflqFromBuffer(buffer, position);
        position += WdflAccpreVisEfflq.Len.WDFL_ACCPRE_VIS_EFFLQ;
        wdflAccpreAccCalc.setWdflAccpreAccCalcFromBuffer(buffer, position);
        position += WdflAccpreAccCalc.Len.WDFL_ACCPRE_ACC_CALC;
        wdflAccpreAccDfz.setWdflAccpreAccDfzFromBuffer(buffer, position);
        position += WdflAccpreAccDfz.Len.WDFL_ACCPRE_ACC_DFZ;
        wdflAccpreAccEfflq.setWdflAccpreAccEfflqFromBuffer(buffer, position);
        position += WdflAccpreAccEfflq.Len.WDFL_ACCPRE_ACC_EFFLQ;
        wdflResPrstzCalc.setWdflResPrstzCalcFromBuffer(buffer, position);
        position += WdflResPrstzCalc.Len.WDFL_RES_PRSTZ_CALC;
        wdflResPrstzDfz.setWdflResPrstzDfzFromBuffer(buffer, position);
        position += WdflResPrstzDfz.Len.WDFL_RES_PRSTZ_DFZ;
        wdflResPrstzEfflq.setWdflResPrstzEfflqFromBuffer(buffer, position);
        position += WdflResPrstzEfflq.Len.WDFL_RES_PRSTZ_EFFLQ;
        wdflResPreAttCalc.setWdflResPreAttCalcFromBuffer(buffer, position);
        position += WdflResPreAttCalc.Len.WDFL_RES_PRE_ATT_CALC;
        wdflResPreAttDfz.setWdflResPreAttDfzFromBuffer(buffer, position);
        position += WdflResPreAttDfz.Len.WDFL_RES_PRE_ATT_DFZ;
        wdflResPreAttEfflq.setWdflResPreAttEfflqFromBuffer(buffer, position);
        position += WdflResPreAttEfflq.Len.WDFL_RES_PRE_ATT_EFFLQ;
        wdflImpExcontrEff.setWdflImpExcontrEffFromBuffer(buffer, position);
        position += WdflImpExcontrEff.Len.WDFL_IMP_EXCONTR_EFF;
        wdflImpbVisCalc.setWdflImpbVisCalcFromBuffer(buffer, position);
        position += WdflImpbVisCalc.Len.WDFL_IMPB_VIS_CALC;
        wdflImpbVisEfflq.setWdflImpbVisEfflqFromBuffer(buffer, position);
        position += WdflImpbVisEfflq.Len.WDFL_IMPB_VIS_EFFLQ;
        wdflImpbVisDfz.setWdflImpbVisDfzFromBuffer(buffer, position);
        position += WdflImpbVisDfz.Len.WDFL_IMPB_VIS_DFZ;
        wdflImpbRitAccCalc.setWdflImpbRitAccCalcFromBuffer(buffer, position);
        position += WdflImpbRitAccCalc.Len.WDFL_IMPB_RIT_ACC_CALC;
        wdflImpbRitAccEfflq.setWdflImpbRitAccEfflqFromBuffer(buffer, position);
        position += WdflImpbRitAccEfflq.Len.WDFL_IMPB_RIT_ACC_EFFLQ;
        wdflImpbRitAccDfz.setWdflImpbRitAccDfzFromBuffer(buffer, position);
        position += WdflImpbRitAccDfz.Len.WDFL_IMPB_RIT_ACC_DFZ;
        wdflImpbTfrCalc.setWdflImpbTfrCalcFromBuffer(buffer, position);
        position += WdflImpbTfrCalc.Len.WDFL_IMPB_TFR_CALC;
        wdflImpbTfrEfflq.setWdflImpbTfrEfflqFromBuffer(buffer, position);
        position += WdflImpbTfrEfflq.Len.WDFL_IMPB_TFR_EFFLQ;
        wdflImpbTfrDfz.setWdflImpbTfrDfzFromBuffer(buffer, position);
        position += WdflImpbTfrDfz.Len.WDFL_IMPB_TFR_DFZ;
        wdflImpbIsCalc.setWdflImpbIsCalcFromBuffer(buffer, position);
        position += WdflImpbIsCalc.Len.WDFL_IMPB_IS_CALC;
        wdflImpbIsEfflq.setWdflImpbIsEfflqFromBuffer(buffer, position);
        position += WdflImpbIsEfflq.Len.WDFL_IMPB_IS_EFFLQ;
        wdflImpbIsDfz.setWdflImpbIsDfzFromBuffer(buffer, position);
        position += WdflImpbIsDfz.Len.WDFL_IMPB_IS_DFZ;
        wdflImpbTaxSepCalc.setWdflImpbTaxSepCalcFromBuffer(buffer, position);
        position += WdflImpbTaxSepCalc.Len.WDFL_IMPB_TAX_SEP_CALC;
        wdflImpbTaxSepEfflq.setWdflImpbTaxSepEfflqFromBuffer(buffer, position);
        position += WdflImpbTaxSepEfflq.Len.WDFL_IMPB_TAX_SEP_EFFLQ;
        wdflImpbTaxSepDfz.setWdflImpbTaxSepDfzFromBuffer(buffer, position);
        position += WdflImpbTaxSepDfz.Len.WDFL_IMPB_TAX_SEP_DFZ;
        wdflIintPrestCalc.setWdflIintPrestCalcFromBuffer(buffer, position);
        position += WdflIintPrestCalc.Len.WDFL_IINT_PREST_CALC;
        wdflIintPrestEfflq.setWdflIintPrestEfflqFromBuffer(buffer, position);
        position += WdflIintPrestEfflq.Len.WDFL_IINT_PREST_EFFLQ;
        wdflIintPrestDfz.setWdflIintPrestDfzFromBuffer(buffer, position);
        position += WdflIintPrestDfz.Len.WDFL_IINT_PREST_DFZ;
        wdflMontEnd2000Calc.setWdflMontEnd2000CalcFromBuffer(buffer, position);
        position += WdflMontEnd2000Calc.Len.WDFL_MONT_END2000_CALC;
        wdflMontEnd2000Efflq.setWdflMontEnd2000EfflqFromBuffer(buffer, position);
        position += WdflMontEnd2000Efflq.Len.WDFL_MONT_END2000_EFFLQ;
        wdflMontEnd2000Dfz.setWdflMontEnd2000DfzFromBuffer(buffer, position);
        position += WdflMontEnd2000Dfz.Len.WDFL_MONT_END2000_DFZ;
        wdflMontEnd2006Calc.setWdflMontEnd2006CalcFromBuffer(buffer, position);
        position += WdflMontEnd2006Calc.Len.WDFL_MONT_END2006_CALC;
        wdflMontEnd2006Efflq.setWdflMontEnd2006EfflqFromBuffer(buffer, position);
        position += WdflMontEnd2006Efflq.Len.WDFL_MONT_END2006_EFFLQ;
        wdflMontEnd2006Dfz.setWdflMontEnd2006DfzFromBuffer(buffer, position);
        position += WdflMontEnd2006Dfz.Len.WDFL_MONT_END2006_DFZ;
        wdflMontDal2007Calc.setWdflMontDal2007CalcFromBuffer(buffer, position);
        position += WdflMontDal2007Calc.Len.WDFL_MONT_DAL2007_CALC;
        wdflMontDal2007Efflq.setWdflMontDal2007EfflqFromBuffer(buffer, position);
        position += WdflMontDal2007Efflq.Len.WDFL_MONT_DAL2007_EFFLQ;
        wdflMontDal2007Dfz.setWdflMontDal2007DfzFromBuffer(buffer, position);
        position += WdflMontDal2007Dfz.Len.WDFL_MONT_DAL2007_DFZ;
        wdflIimpstPrvrCalc.setWdflIimpstPrvrCalcFromBuffer(buffer, position);
        position += WdflIimpstPrvrCalc.Len.WDFL_IIMPST_PRVR_CALC;
        wdflIimpstPrvrEfflq.setWdflIimpstPrvrEfflqFromBuffer(buffer, position);
        position += WdflIimpstPrvrEfflq.Len.WDFL_IIMPST_PRVR_EFFLQ;
        wdflIimpstPrvrDfz.setWdflIimpstPrvrDfzFromBuffer(buffer, position);
        position += WdflIimpstPrvrDfz.Len.WDFL_IIMPST_PRVR_DFZ;
        wdflIimpst252Calc.setWdflIimpst252CalcFromBuffer(buffer, position);
        position += WdflIimpst252Calc.Len.WDFL_IIMPST252_CALC;
        wdflIimpst252Efflq.setWdflIimpst252EfflqFromBuffer(buffer, position);
        position += WdflIimpst252Efflq.Len.WDFL_IIMPST252_EFFLQ;
        wdflIimpst252Dfz.setWdflIimpst252DfzFromBuffer(buffer, position);
        position += WdflIimpst252Dfz.Len.WDFL_IIMPST252_DFZ;
        wdflImpst252Calc.setWdflImpst252CalcFromBuffer(buffer, position);
        position += WdflImpst252Calc.Len.WDFL_IMPST252_CALC;
        wdflImpst252Efflq.setWdflImpst252EfflqFromBuffer(buffer, position);
        position += WdflImpst252Efflq.Len.WDFL_IMPST252_EFFLQ;
        wdflRitTfrCalc.setWdflRitTfrCalcFromBuffer(buffer, position);
        position += WdflRitTfrCalc.Len.WDFL_RIT_TFR_CALC;
        wdflRitTfrEfflq.setWdflRitTfrEfflqFromBuffer(buffer, position);
        position += WdflRitTfrEfflq.Len.WDFL_RIT_TFR_EFFLQ;
        wdflRitTfrDfz.setWdflRitTfrDfzFromBuffer(buffer, position);
        position += WdflRitTfrDfz.Len.WDFL_RIT_TFR_DFZ;
        wdflCnbtInpstfmCalc.setWdflCnbtInpstfmCalcFromBuffer(buffer, position);
        position += WdflCnbtInpstfmCalc.Len.WDFL_CNBT_INPSTFM_CALC;
        wdflCnbtInpstfmEfflq.setWdflCnbtInpstfmEfflqFromBuffer(buffer, position);
        position += WdflCnbtInpstfmEfflq.Len.WDFL_CNBT_INPSTFM_EFFLQ;
        wdflCnbtInpstfmDfz.setWdflCnbtInpstfmDfzFromBuffer(buffer, position);
        position += WdflCnbtInpstfmDfz.Len.WDFL_CNBT_INPSTFM_DFZ;
        wdflIcnbInpstfmCalc.setWdflIcnbInpstfmCalcFromBuffer(buffer, position);
        position += WdflIcnbInpstfmCalc.Len.WDFL_ICNB_INPSTFM_CALC;
        wdflIcnbInpstfmEfflq.setWdflIcnbInpstfmEfflqFromBuffer(buffer, position);
        position += WdflIcnbInpstfmEfflq.Len.WDFL_ICNB_INPSTFM_EFFLQ;
        wdflIcnbInpstfmDfz.setWdflIcnbInpstfmDfzFromBuffer(buffer, position);
        position += WdflIcnbInpstfmDfz.Len.WDFL_ICNB_INPSTFM_DFZ;
        wdflCndeEnd2000Calc.setWdflCndeEnd2000CalcFromBuffer(buffer, position);
        position += WdflCndeEnd2000Calc.Len.WDFL_CNDE_END2000_CALC;
        wdflCndeEnd2000Efflq.setWdflCndeEnd2000EfflqFromBuffer(buffer, position);
        position += WdflCndeEnd2000Efflq.Len.WDFL_CNDE_END2000_EFFLQ;
        wdflCndeEnd2000Dfz.setWdflCndeEnd2000DfzFromBuffer(buffer, position);
        position += WdflCndeEnd2000Dfz.Len.WDFL_CNDE_END2000_DFZ;
        wdflCndeEnd2006Calc.setWdflCndeEnd2006CalcFromBuffer(buffer, position);
        position += WdflCndeEnd2006Calc.Len.WDFL_CNDE_END2006_CALC;
        wdflCndeEnd2006Efflq.setWdflCndeEnd2006EfflqFromBuffer(buffer, position);
        position += WdflCndeEnd2006Efflq.Len.WDFL_CNDE_END2006_EFFLQ;
        wdflCndeEnd2006Dfz.setWdflCndeEnd2006DfzFromBuffer(buffer, position);
        position += WdflCndeEnd2006Dfz.Len.WDFL_CNDE_END2006_DFZ;
        wdflCndeDal2007Calc.setWdflCndeDal2007CalcFromBuffer(buffer, position);
        position += WdflCndeDal2007Calc.Len.WDFL_CNDE_DAL2007_CALC;
        wdflCndeDal2007Efflq.setWdflCndeDal2007EfflqFromBuffer(buffer, position);
        position += WdflCndeDal2007Efflq.Len.WDFL_CNDE_DAL2007_EFFLQ;
        wdflCndeDal2007Dfz.setWdflCndeDal2007DfzFromBuffer(buffer, position);
        position += WdflCndeDal2007Dfz.Len.WDFL_CNDE_DAL2007_DFZ;
        wdflAaCnbzEnd2000Ef.setWdflAaCnbzEnd2000EfFromBuffer(buffer, position);
        position += WdflAaCnbzEnd2000Ef.Len.WDFL_AA_CNBZ_END2000_EF;
        wdflAaCnbzEnd2006Ef.setWdflAaCnbzEnd2006EfFromBuffer(buffer, position);
        position += WdflAaCnbzEnd2006Ef.Len.WDFL_AA_CNBZ_END2006_EF;
        wdflAaCnbzDal2007Ef.setWdflAaCnbzDal2007EfFromBuffer(buffer, position);
        position += WdflAaCnbzDal2007Ef.Len.WDFL_AA_CNBZ_DAL2007_EF;
        wdflMmCnbzEnd2000Ef.setWdflMmCnbzEnd2000EfFromBuffer(buffer, position);
        position += WdflMmCnbzEnd2000Ef.Len.WDFL_MM_CNBZ_END2000_EF;
        wdflMmCnbzEnd2006Ef.setWdflMmCnbzEnd2006EfFromBuffer(buffer, position);
        position += WdflMmCnbzEnd2006Ef.Len.WDFL_MM_CNBZ_END2006_EF;
        wdflMmCnbzDal2007Ef.setWdflMmCnbzDal2007EfFromBuffer(buffer, position);
        position += WdflMmCnbzDal2007Ef.Len.WDFL_MM_CNBZ_DAL2007_EF;
        wdflImpstDaRimbEff.setWdflImpstDaRimbEffFromBuffer(buffer, position);
        position += WdflImpstDaRimbEff.Len.WDFL_IMPST_DA_RIMB_EFF;
        wdflAlqTaxSepCalc.setWdflAlqTaxSepCalcFromBuffer(buffer, position);
        position += WdflAlqTaxSepCalc.Len.WDFL_ALQ_TAX_SEP_CALC;
        wdflAlqTaxSepEfflq.setWdflAlqTaxSepEfflqFromBuffer(buffer, position);
        position += WdflAlqTaxSepEfflq.Len.WDFL_ALQ_TAX_SEP_EFFLQ;
        wdflAlqTaxSepDfz.setWdflAlqTaxSepDfzFromBuffer(buffer, position);
        position += WdflAlqTaxSepDfz.Len.WDFL_ALQ_TAX_SEP_DFZ;
        wdflAlqCnbtInpstfmC.setWdflAlqCnbtInpstfmCFromBuffer(buffer, position);
        position += WdflAlqCnbtInpstfmC.Len.WDFL_ALQ_CNBT_INPSTFM_C;
        wdflAlqCnbtInpstfmE.setWdflAlqCnbtInpstfmEFromBuffer(buffer, position);
        position += WdflAlqCnbtInpstfmE.Len.WDFL_ALQ_CNBT_INPSTFM_E;
        wdflAlqCnbtInpstfmD.setWdflAlqCnbtInpstfmDFromBuffer(buffer, position);
        position += WdflAlqCnbtInpstfmD.Len.WDFL_ALQ_CNBT_INPSTFM_D;
        wdflDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDFL_DS_RIGA, 0);
        position += Len.WDFL_DS_RIGA;
        wdflDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdflDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDFL_DS_VER, 0);
        position += Len.WDFL_DS_VER;
        wdflDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDFL_DS_TS_INI_CPTZ, 0);
        position += Len.WDFL_DS_TS_INI_CPTZ;
        wdflDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDFL_DS_TS_END_CPTZ, 0);
        position += Len.WDFL_DS_TS_END_CPTZ;
        wdflDsUtente = MarshalByte.readString(buffer, position, Len.WDFL_DS_UTENTE);
        position += Len.WDFL_DS_UTENTE;
        wdflDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdflImpbVis1382011c.setWdflImpbVis1382011cFromBuffer(buffer, position);
        position += WdflImpbVis1382011c.Len.WDFL_IMPB_VIS1382011C;
        wdflImpbVis1382011d.setWdflImpbVis1382011dFromBuffer(buffer, position);
        position += WdflImpbVis1382011d.Len.WDFL_IMPB_VIS1382011D;
        wdflImpbVis1382011l.setWdflImpbVis1382011lFromBuffer(buffer, position);
        position += WdflImpbVis1382011l.Len.WDFL_IMPB_VIS1382011L;
        wdflImpstVis1382011c.setWdflImpstVis1382011cFromBuffer(buffer, position);
        position += WdflImpstVis1382011c.Len.WDFL_IMPST_VIS1382011C;
        wdflImpstVis1382011d.setWdflImpstVis1382011dFromBuffer(buffer, position);
        position += WdflImpstVis1382011d.Len.WDFL_IMPST_VIS1382011D;
        wdflImpstVis1382011l.setWdflImpstVis1382011lFromBuffer(buffer, position);
        position += WdflImpstVis1382011l.Len.WDFL_IMPST_VIS1382011L;
        wdflImpbIs1382011c.setWdflImpbIs1382011cFromBuffer(buffer, position);
        position += WdflImpbIs1382011c.Len.WDFL_IMPB_IS1382011C;
        wdflImpbIs1382011d.setWdflImpbIs1382011dFromBuffer(buffer, position);
        position += WdflImpbIs1382011d.Len.WDFL_IMPB_IS1382011D;
        wdflImpbIs1382011l.setWdflImpbIs1382011lFromBuffer(buffer, position);
        position += WdflImpbIs1382011l.Len.WDFL_IMPB_IS1382011L;
        wdflIs1382011c.setWdflIs1382011cFromBuffer(buffer, position);
        position += WdflIs1382011c.Len.WDFL_IS1382011C;
        wdflIs1382011d.setWdflIs1382011dFromBuffer(buffer, position);
        position += WdflIs1382011d.Len.WDFL_IS1382011D;
        wdflIs1382011l.setWdflIs1382011lFromBuffer(buffer, position);
        position += WdflIs1382011l.Len.WDFL_IS1382011L;
        wdflImpIntrRitPagC.setWdflImpIntrRitPagCFromBuffer(buffer, position);
        position += WdflImpIntrRitPagC.Len.WDFL_IMP_INTR_RIT_PAG_C;
        wdflImpIntrRitPagD.setWdflImpIntrRitPagDFromBuffer(buffer, position);
        position += WdflImpIntrRitPagD.Len.WDFL_IMP_INTR_RIT_PAG_D;
        wdflImpIntrRitPagL.setWdflImpIntrRitPagLFromBuffer(buffer, position);
        position += WdflImpIntrRitPagL.Len.WDFL_IMP_INTR_RIT_PAG_L;
        wdflImpbBolloDettC.setWdflImpbBolloDettCFromBuffer(buffer, position);
        position += WdflImpbBolloDettC.Len.WDFL_IMPB_BOLLO_DETT_C;
        wdflImpbBolloDettD.setWdflImpbBolloDettDFromBuffer(buffer, position);
        position += WdflImpbBolloDettD.Len.WDFL_IMPB_BOLLO_DETT_D;
        wdflImpbBolloDettL.setWdflImpbBolloDettLFromBuffer(buffer, position);
        position += WdflImpbBolloDettL.Len.WDFL_IMPB_BOLLO_DETT_L;
        wdflImpstBolloDettC.setWdflImpstBolloDettCFromBuffer(buffer, position);
        position += WdflImpstBolloDettC.Len.WDFL_IMPST_BOLLO_DETT_C;
        wdflImpstBolloDettD.setWdflImpstBolloDettDFromBuffer(buffer, position);
        position += WdflImpstBolloDettD.Len.WDFL_IMPST_BOLLO_DETT_D;
        wdflImpstBolloDettL.setWdflImpstBolloDettLFromBuffer(buffer, position);
        position += WdflImpstBolloDettL.Len.WDFL_IMPST_BOLLO_DETT_L;
        wdflImpstBolloTotVc.setWdflImpstBolloTotVcFromBuffer(buffer, position);
        position += WdflImpstBolloTotVc.Len.WDFL_IMPST_BOLLO_TOT_VC;
        wdflImpstBolloTotVd.setWdflImpstBolloTotVdFromBuffer(buffer, position);
        position += WdflImpstBolloTotVd.Len.WDFL_IMPST_BOLLO_TOT_VD;
        wdflImpstBolloTotVl.setWdflImpstBolloTotVlFromBuffer(buffer, position);
        position += WdflImpstBolloTotVl.Len.WDFL_IMPST_BOLLO_TOT_VL;
        wdflImpbVis662014c.setWdflImpbVis662014cFromBuffer(buffer, position);
        position += WdflImpbVis662014c.Len.WDFL_IMPB_VIS662014C;
        wdflImpbVis662014d.setWdflImpbVis662014dFromBuffer(buffer, position);
        position += WdflImpbVis662014d.Len.WDFL_IMPB_VIS662014D;
        wdflImpbVis662014l.setWdflImpbVis662014lFromBuffer(buffer, position);
        position += WdflImpbVis662014l.Len.WDFL_IMPB_VIS662014L;
        wdflImpstVis662014c.setWdflImpstVis662014cFromBuffer(buffer, position);
        position += WdflImpstVis662014c.Len.WDFL_IMPST_VIS662014C;
        wdflImpstVis662014d.setWdflImpstVis662014dFromBuffer(buffer, position);
        position += WdflImpstVis662014d.Len.WDFL_IMPST_VIS662014D;
        wdflImpstVis662014l.setWdflImpstVis662014lFromBuffer(buffer, position);
        position += WdflImpstVis662014l.Len.WDFL_IMPST_VIS662014L;
        wdflImpbIs662014c.setWdflImpbIs662014cFromBuffer(buffer, position);
        position += WdflImpbIs662014c.Len.WDFL_IMPB_IS662014C;
        wdflImpbIs662014d.setWdflImpbIs662014dFromBuffer(buffer, position);
        position += WdflImpbIs662014d.Len.WDFL_IMPB_IS662014D;
        wdflImpbIs662014l.setWdflImpbIs662014lFromBuffer(buffer, position);
        position += WdflImpbIs662014l.Len.WDFL_IMPB_IS662014L;
        wdflIs662014c.setWdflIs662014cFromBuffer(buffer, position);
        position += WdflIs662014c.Len.WDFL_IS662014C;
        wdflIs662014d.setWdflIs662014dFromBuffer(buffer, position);
        position += WdflIs662014d.Len.WDFL_IS662014D;
        wdflIs662014l.setWdflIs662014lFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wdflIdDForzLiq, Len.Int.WDFL_ID_D_FORZ_LIQ, 0);
        position += Len.WDFL_ID_D_FORZ_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, wdflIdLiq, Len.Int.WDFL_ID_LIQ, 0);
        position += Len.WDFL_ID_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, wdflIdMoviCrz, Len.Int.WDFL_ID_MOVI_CRZ, 0);
        position += Len.WDFL_ID_MOVI_CRZ;
        wdflIdMoviChiu.getWdflIdMoviChiuAsBuffer(buffer, position);
        position += WdflIdMoviChiu.Len.WDFL_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wdflCodCompAnia, Len.Int.WDFL_COD_COMP_ANIA, 0);
        position += Len.WDFL_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wdflDtIniEff, Len.Int.WDFL_DT_INI_EFF, 0);
        position += Len.WDFL_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wdflDtEndEff, Len.Int.WDFL_DT_END_EFF, 0);
        position += Len.WDFL_DT_END_EFF;
        wdflImpLrdCalc.getWdflImpLrdCalcAsBuffer(buffer, position);
        position += WdflImpLrdCalc.Len.WDFL_IMP_LRD_CALC;
        wdflImpLrdDfz.getWdflImpLrdDfzAsBuffer(buffer, position);
        position += WdflImpLrdDfz.Len.WDFL_IMP_LRD_DFZ;
        wdflImpLrdEfflq.getWdflImpLrdEfflqAsBuffer(buffer, position);
        position += WdflImpLrdEfflq.Len.WDFL_IMP_LRD_EFFLQ;
        wdflImpNetCalc.getWdflImpNetCalcAsBuffer(buffer, position);
        position += WdflImpNetCalc.Len.WDFL_IMP_NET_CALC;
        wdflImpNetDfz.getWdflImpNetDfzAsBuffer(buffer, position);
        position += WdflImpNetDfz.Len.WDFL_IMP_NET_DFZ;
        wdflImpNetEfflq.getWdflImpNetEfflqAsBuffer(buffer, position);
        position += WdflImpNetEfflq.Len.WDFL_IMP_NET_EFFLQ;
        wdflImpstPrvrCalc.getWdflImpstPrvrCalcAsBuffer(buffer, position);
        position += WdflImpstPrvrCalc.Len.WDFL_IMPST_PRVR_CALC;
        wdflImpstPrvrDfz.getWdflImpstPrvrDfzAsBuffer(buffer, position);
        position += WdflImpstPrvrDfz.Len.WDFL_IMPST_PRVR_DFZ;
        wdflImpstPrvrEfflq.getWdflImpstPrvrEfflqAsBuffer(buffer, position);
        position += WdflImpstPrvrEfflq.Len.WDFL_IMPST_PRVR_EFFLQ;
        wdflImpstVisCalc.getWdflImpstVisCalcAsBuffer(buffer, position);
        position += WdflImpstVisCalc.Len.WDFL_IMPST_VIS_CALC;
        wdflImpstVisDfz.getWdflImpstVisDfzAsBuffer(buffer, position);
        position += WdflImpstVisDfz.Len.WDFL_IMPST_VIS_DFZ;
        wdflImpstVisEfflq.getWdflImpstVisEfflqAsBuffer(buffer, position);
        position += WdflImpstVisEfflq.Len.WDFL_IMPST_VIS_EFFLQ;
        wdflRitAccCalc.getWdflRitAccCalcAsBuffer(buffer, position);
        position += WdflRitAccCalc.Len.WDFL_RIT_ACC_CALC;
        wdflRitAccDfz.getWdflRitAccDfzAsBuffer(buffer, position);
        position += WdflRitAccDfz.Len.WDFL_RIT_ACC_DFZ;
        wdflRitAccEfflq.getWdflRitAccEfflqAsBuffer(buffer, position);
        position += WdflRitAccEfflq.Len.WDFL_RIT_ACC_EFFLQ;
        wdflRitIrpefCalc.getWdflRitIrpefCalcAsBuffer(buffer, position);
        position += WdflRitIrpefCalc.Len.WDFL_RIT_IRPEF_CALC;
        wdflRitIrpefDfz.getWdflRitIrpefDfzAsBuffer(buffer, position);
        position += WdflRitIrpefDfz.Len.WDFL_RIT_IRPEF_DFZ;
        wdflRitIrpefEfflq.getWdflRitIrpefEfflqAsBuffer(buffer, position);
        position += WdflRitIrpefEfflq.Len.WDFL_RIT_IRPEF_EFFLQ;
        wdflImpstSostCalc.getWdflImpstSostCalcAsBuffer(buffer, position);
        position += WdflImpstSostCalc.Len.WDFL_IMPST_SOST_CALC;
        wdflImpstSostDfz.getWdflImpstSostDfzAsBuffer(buffer, position);
        position += WdflImpstSostDfz.Len.WDFL_IMPST_SOST_DFZ;
        wdflImpstSostEfflq.getWdflImpstSostEfflqAsBuffer(buffer, position);
        position += WdflImpstSostEfflq.Len.WDFL_IMPST_SOST_EFFLQ;
        wdflTaxSepCalc.getWdflTaxSepCalcAsBuffer(buffer, position);
        position += WdflTaxSepCalc.Len.WDFL_TAX_SEP_CALC;
        wdflTaxSepDfz.getWdflTaxSepDfzAsBuffer(buffer, position);
        position += WdflTaxSepDfz.Len.WDFL_TAX_SEP_DFZ;
        wdflTaxSepEfflq.getWdflTaxSepEfflqAsBuffer(buffer, position);
        position += WdflTaxSepEfflq.Len.WDFL_TAX_SEP_EFFLQ;
        wdflIntrPrestCalc.getWdflIntrPrestCalcAsBuffer(buffer, position);
        position += WdflIntrPrestCalc.Len.WDFL_INTR_PREST_CALC;
        wdflIntrPrestDfz.getWdflIntrPrestDfzAsBuffer(buffer, position);
        position += WdflIntrPrestDfz.Len.WDFL_INTR_PREST_DFZ;
        wdflIntrPrestEfflq.getWdflIntrPrestEfflqAsBuffer(buffer, position);
        position += WdflIntrPrestEfflq.Len.WDFL_INTR_PREST_EFFLQ;
        wdflAccpreSostCalc.getWdflAccpreSostCalcAsBuffer(buffer, position);
        position += WdflAccpreSostCalc.Len.WDFL_ACCPRE_SOST_CALC;
        wdflAccpreSostDfz.getWdflAccpreSostDfzAsBuffer(buffer, position);
        position += WdflAccpreSostDfz.Len.WDFL_ACCPRE_SOST_DFZ;
        wdflAccpreSostEfflq.getWdflAccpreSostEfflqAsBuffer(buffer, position);
        position += WdflAccpreSostEfflq.Len.WDFL_ACCPRE_SOST_EFFLQ;
        wdflAccpreVisCalc.getWdflAccpreVisCalcAsBuffer(buffer, position);
        position += WdflAccpreVisCalc.Len.WDFL_ACCPRE_VIS_CALC;
        wdflAccpreVisDfz.getWdflAccpreVisDfzAsBuffer(buffer, position);
        position += WdflAccpreVisDfz.Len.WDFL_ACCPRE_VIS_DFZ;
        wdflAccpreVisEfflq.getWdflAccpreVisEfflqAsBuffer(buffer, position);
        position += WdflAccpreVisEfflq.Len.WDFL_ACCPRE_VIS_EFFLQ;
        wdflAccpreAccCalc.getWdflAccpreAccCalcAsBuffer(buffer, position);
        position += WdflAccpreAccCalc.Len.WDFL_ACCPRE_ACC_CALC;
        wdflAccpreAccDfz.getWdflAccpreAccDfzAsBuffer(buffer, position);
        position += WdflAccpreAccDfz.Len.WDFL_ACCPRE_ACC_DFZ;
        wdflAccpreAccEfflq.getWdflAccpreAccEfflqAsBuffer(buffer, position);
        position += WdflAccpreAccEfflq.Len.WDFL_ACCPRE_ACC_EFFLQ;
        wdflResPrstzCalc.getWdflResPrstzCalcAsBuffer(buffer, position);
        position += WdflResPrstzCalc.Len.WDFL_RES_PRSTZ_CALC;
        wdflResPrstzDfz.getWdflResPrstzDfzAsBuffer(buffer, position);
        position += WdflResPrstzDfz.Len.WDFL_RES_PRSTZ_DFZ;
        wdflResPrstzEfflq.getWdflResPrstzEfflqAsBuffer(buffer, position);
        position += WdflResPrstzEfflq.Len.WDFL_RES_PRSTZ_EFFLQ;
        wdflResPreAttCalc.getWdflResPreAttCalcAsBuffer(buffer, position);
        position += WdflResPreAttCalc.Len.WDFL_RES_PRE_ATT_CALC;
        wdflResPreAttDfz.getWdflResPreAttDfzAsBuffer(buffer, position);
        position += WdflResPreAttDfz.Len.WDFL_RES_PRE_ATT_DFZ;
        wdflResPreAttEfflq.getWdflResPreAttEfflqAsBuffer(buffer, position);
        position += WdflResPreAttEfflq.Len.WDFL_RES_PRE_ATT_EFFLQ;
        wdflImpExcontrEff.getWdflImpExcontrEffAsBuffer(buffer, position);
        position += WdflImpExcontrEff.Len.WDFL_IMP_EXCONTR_EFF;
        wdflImpbVisCalc.getWdflImpbVisCalcAsBuffer(buffer, position);
        position += WdflImpbVisCalc.Len.WDFL_IMPB_VIS_CALC;
        wdflImpbVisEfflq.getWdflImpbVisEfflqAsBuffer(buffer, position);
        position += WdflImpbVisEfflq.Len.WDFL_IMPB_VIS_EFFLQ;
        wdflImpbVisDfz.getWdflImpbVisDfzAsBuffer(buffer, position);
        position += WdflImpbVisDfz.Len.WDFL_IMPB_VIS_DFZ;
        wdflImpbRitAccCalc.getWdflImpbRitAccCalcAsBuffer(buffer, position);
        position += WdflImpbRitAccCalc.Len.WDFL_IMPB_RIT_ACC_CALC;
        wdflImpbRitAccEfflq.getWdflImpbRitAccEfflqAsBuffer(buffer, position);
        position += WdflImpbRitAccEfflq.Len.WDFL_IMPB_RIT_ACC_EFFLQ;
        wdflImpbRitAccDfz.getWdflImpbRitAccDfzAsBuffer(buffer, position);
        position += WdflImpbRitAccDfz.Len.WDFL_IMPB_RIT_ACC_DFZ;
        wdflImpbTfrCalc.getWdflImpbTfrCalcAsBuffer(buffer, position);
        position += WdflImpbTfrCalc.Len.WDFL_IMPB_TFR_CALC;
        wdflImpbTfrEfflq.getWdflImpbTfrEfflqAsBuffer(buffer, position);
        position += WdflImpbTfrEfflq.Len.WDFL_IMPB_TFR_EFFLQ;
        wdflImpbTfrDfz.getWdflImpbTfrDfzAsBuffer(buffer, position);
        position += WdflImpbTfrDfz.Len.WDFL_IMPB_TFR_DFZ;
        wdflImpbIsCalc.getWdflImpbIsCalcAsBuffer(buffer, position);
        position += WdflImpbIsCalc.Len.WDFL_IMPB_IS_CALC;
        wdflImpbIsEfflq.getWdflImpbIsEfflqAsBuffer(buffer, position);
        position += WdflImpbIsEfflq.Len.WDFL_IMPB_IS_EFFLQ;
        wdflImpbIsDfz.getWdflImpbIsDfzAsBuffer(buffer, position);
        position += WdflImpbIsDfz.Len.WDFL_IMPB_IS_DFZ;
        wdflImpbTaxSepCalc.getWdflImpbTaxSepCalcAsBuffer(buffer, position);
        position += WdflImpbTaxSepCalc.Len.WDFL_IMPB_TAX_SEP_CALC;
        wdflImpbTaxSepEfflq.getWdflImpbTaxSepEfflqAsBuffer(buffer, position);
        position += WdflImpbTaxSepEfflq.Len.WDFL_IMPB_TAX_SEP_EFFLQ;
        wdflImpbTaxSepDfz.getWdflImpbTaxSepDfzAsBuffer(buffer, position);
        position += WdflImpbTaxSepDfz.Len.WDFL_IMPB_TAX_SEP_DFZ;
        wdflIintPrestCalc.getWdflIintPrestCalcAsBuffer(buffer, position);
        position += WdflIintPrestCalc.Len.WDFL_IINT_PREST_CALC;
        wdflIintPrestEfflq.getWdflIintPrestEfflqAsBuffer(buffer, position);
        position += WdflIintPrestEfflq.Len.WDFL_IINT_PREST_EFFLQ;
        wdflIintPrestDfz.getWdflIintPrestDfzAsBuffer(buffer, position);
        position += WdflIintPrestDfz.Len.WDFL_IINT_PREST_DFZ;
        wdflMontEnd2000Calc.getWdflMontEnd2000CalcAsBuffer(buffer, position);
        position += WdflMontEnd2000Calc.Len.WDFL_MONT_END2000_CALC;
        wdflMontEnd2000Efflq.getWdflMontEnd2000EfflqAsBuffer(buffer, position);
        position += WdflMontEnd2000Efflq.Len.WDFL_MONT_END2000_EFFLQ;
        wdflMontEnd2000Dfz.getWdflMontEnd2000DfzAsBuffer(buffer, position);
        position += WdflMontEnd2000Dfz.Len.WDFL_MONT_END2000_DFZ;
        wdflMontEnd2006Calc.getWdflMontEnd2006CalcAsBuffer(buffer, position);
        position += WdflMontEnd2006Calc.Len.WDFL_MONT_END2006_CALC;
        wdflMontEnd2006Efflq.getWdflMontEnd2006EfflqAsBuffer(buffer, position);
        position += WdflMontEnd2006Efflq.Len.WDFL_MONT_END2006_EFFLQ;
        wdflMontEnd2006Dfz.getWdflMontEnd2006DfzAsBuffer(buffer, position);
        position += WdflMontEnd2006Dfz.Len.WDFL_MONT_END2006_DFZ;
        wdflMontDal2007Calc.getWdflMontDal2007CalcAsBuffer(buffer, position);
        position += WdflMontDal2007Calc.Len.WDFL_MONT_DAL2007_CALC;
        wdflMontDal2007Efflq.getWdflMontDal2007EfflqAsBuffer(buffer, position);
        position += WdflMontDal2007Efflq.Len.WDFL_MONT_DAL2007_EFFLQ;
        wdflMontDal2007Dfz.getWdflMontDal2007DfzAsBuffer(buffer, position);
        position += WdflMontDal2007Dfz.Len.WDFL_MONT_DAL2007_DFZ;
        wdflIimpstPrvrCalc.getWdflIimpstPrvrCalcAsBuffer(buffer, position);
        position += WdflIimpstPrvrCalc.Len.WDFL_IIMPST_PRVR_CALC;
        wdflIimpstPrvrEfflq.getWdflIimpstPrvrEfflqAsBuffer(buffer, position);
        position += WdflIimpstPrvrEfflq.Len.WDFL_IIMPST_PRVR_EFFLQ;
        wdflIimpstPrvrDfz.getWdflIimpstPrvrDfzAsBuffer(buffer, position);
        position += WdflIimpstPrvrDfz.Len.WDFL_IIMPST_PRVR_DFZ;
        wdflIimpst252Calc.getWdflIimpst252CalcAsBuffer(buffer, position);
        position += WdflIimpst252Calc.Len.WDFL_IIMPST252_CALC;
        wdflIimpst252Efflq.getWdflIimpst252EfflqAsBuffer(buffer, position);
        position += WdflIimpst252Efflq.Len.WDFL_IIMPST252_EFFLQ;
        wdflIimpst252Dfz.getWdflIimpst252DfzAsBuffer(buffer, position);
        position += WdflIimpst252Dfz.Len.WDFL_IIMPST252_DFZ;
        wdflImpst252Calc.getWdflImpst252CalcAsBuffer(buffer, position);
        position += WdflImpst252Calc.Len.WDFL_IMPST252_CALC;
        wdflImpst252Efflq.getWdflImpst252EfflqAsBuffer(buffer, position);
        position += WdflImpst252Efflq.Len.WDFL_IMPST252_EFFLQ;
        wdflRitTfrCalc.getWdflRitTfrCalcAsBuffer(buffer, position);
        position += WdflRitTfrCalc.Len.WDFL_RIT_TFR_CALC;
        wdflRitTfrEfflq.getWdflRitTfrEfflqAsBuffer(buffer, position);
        position += WdflRitTfrEfflq.Len.WDFL_RIT_TFR_EFFLQ;
        wdflRitTfrDfz.getWdflRitTfrDfzAsBuffer(buffer, position);
        position += WdflRitTfrDfz.Len.WDFL_RIT_TFR_DFZ;
        wdflCnbtInpstfmCalc.getWdflCnbtInpstfmCalcAsBuffer(buffer, position);
        position += WdflCnbtInpstfmCalc.Len.WDFL_CNBT_INPSTFM_CALC;
        wdflCnbtInpstfmEfflq.getWdflCnbtInpstfmEfflqAsBuffer(buffer, position);
        position += WdflCnbtInpstfmEfflq.Len.WDFL_CNBT_INPSTFM_EFFLQ;
        wdflCnbtInpstfmDfz.getWdflCnbtInpstfmDfzAsBuffer(buffer, position);
        position += WdflCnbtInpstfmDfz.Len.WDFL_CNBT_INPSTFM_DFZ;
        wdflIcnbInpstfmCalc.getWdflIcnbInpstfmCalcAsBuffer(buffer, position);
        position += WdflIcnbInpstfmCalc.Len.WDFL_ICNB_INPSTFM_CALC;
        wdflIcnbInpstfmEfflq.getWdflIcnbInpstfmEfflqAsBuffer(buffer, position);
        position += WdflIcnbInpstfmEfflq.Len.WDFL_ICNB_INPSTFM_EFFLQ;
        wdflIcnbInpstfmDfz.getWdflIcnbInpstfmDfzAsBuffer(buffer, position);
        position += WdflIcnbInpstfmDfz.Len.WDFL_ICNB_INPSTFM_DFZ;
        wdflCndeEnd2000Calc.getWdflCndeEnd2000CalcAsBuffer(buffer, position);
        position += WdflCndeEnd2000Calc.Len.WDFL_CNDE_END2000_CALC;
        wdflCndeEnd2000Efflq.getWdflCndeEnd2000EfflqAsBuffer(buffer, position);
        position += WdflCndeEnd2000Efflq.Len.WDFL_CNDE_END2000_EFFLQ;
        wdflCndeEnd2000Dfz.getWdflCndeEnd2000DfzAsBuffer(buffer, position);
        position += WdflCndeEnd2000Dfz.Len.WDFL_CNDE_END2000_DFZ;
        wdflCndeEnd2006Calc.getWdflCndeEnd2006CalcAsBuffer(buffer, position);
        position += WdflCndeEnd2006Calc.Len.WDFL_CNDE_END2006_CALC;
        wdflCndeEnd2006Efflq.getWdflCndeEnd2006EfflqAsBuffer(buffer, position);
        position += WdflCndeEnd2006Efflq.Len.WDFL_CNDE_END2006_EFFLQ;
        wdflCndeEnd2006Dfz.getWdflCndeEnd2006DfzAsBuffer(buffer, position);
        position += WdflCndeEnd2006Dfz.Len.WDFL_CNDE_END2006_DFZ;
        wdflCndeDal2007Calc.getWdflCndeDal2007CalcAsBuffer(buffer, position);
        position += WdflCndeDal2007Calc.Len.WDFL_CNDE_DAL2007_CALC;
        wdflCndeDal2007Efflq.getWdflCndeDal2007EfflqAsBuffer(buffer, position);
        position += WdflCndeDal2007Efflq.Len.WDFL_CNDE_DAL2007_EFFLQ;
        wdflCndeDal2007Dfz.getWdflCndeDal2007DfzAsBuffer(buffer, position);
        position += WdflCndeDal2007Dfz.Len.WDFL_CNDE_DAL2007_DFZ;
        wdflAaCnbzEnd2000Ef.getWdflAaCnbzEnd2000EfAsBuffer(buffer, position);
        position += WdflAaCnbzEnd2000Ef.Len.WDFL_AA_CNBZ_END2000_EF;
        wdflAaCnbzEnd2006Ef.getWdflAaCnbzEnd2006EfAsBuffer(buffer, position);
        position += WdflAaCnbzEnd2006Ef.Len.WDFL_AA_CNBZ_END2006_EF;
        wdflAaCnbzDal2007Ef.getWdflAaCnbzDal2007EfAsBuffer(buffer, position);
        position += WdflAaCnbzDal2007Ef.Len.WDFL_AA_CNBZ_DAL2007_EF;
        wdflMmCnbzEnd2000Ef.getWdflMmCnbzEnd2000EfAsBuffer(buffer, position);
        position += WdflMmCnbzEnd2000Ef.Len.WDFL_MM_CNBZ_END2000_EF;
        wdflMmCnbzEnd2006Ef.getWdflMmCnbzEnd2006EfAsBuffer(buffer, position);
        position += WdflMmCnbzEnd2006Ef.Len.WDFL_MM_CNBZ_END2006_EF;
        wdflMmCnbzDal2007Ef.getWdflMmCnbzDal2007EfAsBuffer(buffer, position);
        position += WdflMmCnbzDal2007Ef.Len.WDFL_MM_CNBZ_DAL2007_EF;
        wdflImpstDaRimbEff.getWdflImpstDaRimbEffAsBuffer(buffer, position);
        position += WdflImpstDaRimbEff.Len.WDFL_IMPST_DA_RIMB_EFF;
        wdflAlqTaxSepCalc.getWdflAlqTaxSepCalcAsBuffer(buffer, position);
        position += WdflAlqTaxSepCalc.Len.WDFL_ALQ_TAX_SEP_CALC;
        wdflAlqTaxSepEfflq.getWdflAlqTaxSepEfflqAsBuffer(buffer, position);
        position += WdflAlqTaxSepEfflq.Len.WDFL_ALQ_TAX_SEP_EFFLQ;
        wdflAlqTaxSepDfz.getWdflAlqTaxSepDfzAsBuffer(buffer, position);
        position += WdflAlqTaxSepDfz.Len.WDFL_ALQ_TAX_SEP_DFZ;
        wdflAlqCnbtInpstfmC.getWdflAlqCnbtInpstfmCAsBuffer(buffer, position);
        position += WdflAlqCnbtInpstfmC.Len.WDFL_ALQ_CNBT_INPSTFM_C;
        wdflAlqCnbtInpstfmE.getWdflAlqCnbtInpstfmEAsBuffer(buffer, position);
        position += WdflAlqCnbtInpstfmE.Len.WDFL_ALQ_CNBT_INPSTFM_E;
        wdflAlqCnbtInpstfmD.getWdflAlqCnbtInpstfmDAsBuffer(buffer, position);
        position += WdflAlqCnbtInpstfmD.Len.WDFL_ALQ_CNBT_INPSTFM_D;
        MarshalByte.writeLongAsPacked(buffer, position, wdflDsRiga, Len.Int.WDFL_DS_RIGA, 0);
        position += Len.WDFL_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wdflDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wdflDsVer, Len.Int.WDFL_DS_VER, 0);
        position += Len.WDFL_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wdflDsTsIniCptz, Len.Int.WDFL_DS_TS_INI_CPTZ, 0);
        position += Len.WDFL_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wdflDsTsEndCptz, Len.Int.WDFL_DS_TS_END_CPTZ, 0);
        position += Len.WDFL_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wdflDsUtente, Len.WDFL_DS_UTENTE);
        position += Len.WDFL_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wdflDsStatoElab);
        position += Types.CHAR_SIZE;
        wdflImpbVis1382011c.getWdflImpbVis1382011cAsBuffer(buffer, position);
        position += WdflImpbVis1382011c.Len.WDFL_IMPB_VIS1382011C;
        wdflImpbVis1382011d.getWdflImpbVis1382011dAsBuffer(buffer, position);
        position += WdflImpbVis1382011d.Len.WDFL_IMPB_VIS1382011D;
        wdflImpbVis1382011l.getWdflImpbVis1382011lAsBuffer(buffer, position);
        position += WdflImpbVis1382011l.Len.WDFL_IMPB_VIS1382011L;
        wdflImpstVis1382011c.getWdflImpstVis1382011cAsBuffer(buffer, position);
        position += WdflImpstVis1382011c.Len.WDFL_IMPST_VIS1382011C;
        wdflImpstVis1382011d.getWdflImpstVis1382011dAsBuffer(buffer, position);
        position += WdflImpstVis1382011d.Len.WDFL_IMPST_VIS1382011D;
        wdflImpstVis1382011l.getWdflImpstVis1382011lAsBuffer(buffer, position);
        position += WdflImpstVis1382011l.Len.WDFL_IMPST_VIS1382011L;
        wdflImpbIs1382011c.getWdflImpbIs1382011cAsBuffer(buffer, position);
        position += WdflImpbIs1382011c.Len.WDFL_IMPB_IS1382011C;
        wdflImpbIs1382011d.getWdflImpbIs1382011dAsBuffer(buffer, position);
        position += WdflImpbIs1382011d.Len.WDFL_IMPB_IS1382011D;
        wdflImpbIs1382011l.getWdflImpbIs1382011lAsBuffer(buffer, position);
        position += WdflImpbIs1382011l.Len.WDFL_IMPB_IS1382011L;
        wdflIs1382011c.getWdflIs1382011cAsBuffer(buffer, position);
        position += WdflIs1382011c.Len.WDFL_IS1382011C;
        wdflIs1382011d.getWdflIs1382011dAsBuffer(buffer, position);
        position += WdflIs1382011d.Len.WDFL_IS1382011D;
        wdflIs1382011l.getWdflIs1382011lAsBuffer(buffer, position);
        position += WdflIs1382011l.Len.WDFL_IS1382011L;
        wdflImpIntrRitPagC.getWdflImpIntrRitPagCAsBuffer(buffer, position);
        position += WdflImpIntrRitPagC.Len.WDFL_IMP_INTR_RIT_PAG_C;
        wdflImpIntrRitPagD.getWdflImpIntrRitPagDAsBuffer(buffer, position);
        position += WdflImpIntrRitPagD.Len.WDFL_IMP_INTR_RIT_PAG_D;
        wdflImpIntrRitPagL.getWdflImpIntrRitPagLAsBuffer(buffer, position);
        position += WdflImpIntrRitPagL.Len.WDFL_IMP_INTR_RIT_PAG_L;
        wdflImpbBolloDettC.getWdflImpbBolloDettCAsBuffer(buffer, position);
        position += WdflImpbBolloDettC.Len.WDFL_IMPB_BOLLO_DETT_C;
        wdflImpbBolloDettD.getWdflImpbBolloDettDAsBuffer(buffer, position);
        position += WdflImpbBolloDettD.Len.WDFL_IMPB_BOLLO_DETT_D;
        wdflImpbBolloDettL.getWdflImpbBolloDettLAsBuffer(buffer, position);
        position += WdflImpbBolloDettL.Len.WDFL_IMPB_BOLLO_DETT_L;
        wdflImpstBolloDettC.getWdflImpstBolloDettCAsBuffer(buffer, position);
        position += WdflImpstBolloDettC.Len.WDFL_IMPST_BOLLO_DETT_C;
        wdflImpstBolloDettD.getWdflImpstBolloDettDAsBuffer(buffer, position);
        position += WdflImpstBolloDettD.Len.WDFL_IMPST_BOLLO_DETT_D;
        wdflImpstBolloDettL.getWdflImpstBolloDettLAsBuffer(buffer, position);
        position += WdflImpstBolloDettL.Len.WDFL_IMPST_BOLLO_DETT_L;
        wdflImpstBolloTotVc.getWdflImpstBolloTotVcAsBuffer(buffer, position);
        position += WdflImpstBolloTotVc.Len.WDFL_IMPST_BOLLO_TOT_VC;
        wdflImpstBolloTotVd.getWdflImpstBolloTotVdAsBuffer(buffer, position);
        position += WdflImpstBolloTotVd.Len.WDFL_IMPST_BOLLO_TOT_VD;
        wdflImpstBolloTotVl.getWdflImpstBolloTotVlAsBuffer(buffer, position);
        position += WdflImpstBolloTotVl.Len.WDFL_IMPST_BOLLO_TOT_VL;
        wdflImpbVis662014c.getWdflImpbVis662014cAsBuffer(buffer, position);
        position += WdflImpbVis662014c.Len.WDFL_IMPB_VIS662014C;
        wdflImpbVis662014d.getWdflImpbVis662014dAsBuffer(buffer, position);
        position += WdflImpbVis662014d.Len.WDFL_IMPB_VIS662014D;
        wdflImpbVis662014l.getWdflImpbVis662014lAsBuffer(buffer, position);
        position += WdflImpbVis662014l.Len.WDFL_IMPB_VIS662014L;
        wdflImpstVis662014c.getWdflImpstVis662014cAsBuffer(buffer, position);
        position += WdflImpstVis662014c.Len.WDFL_IMPST_VIS662014C;
        wdflImpstVis662014d.getWdflImpstVis662014dAsBuffer(buffer, position);
        position += WdflImpstVis662014d.Len.WDFL_IMPST_VIS662014D;
        wdflImpstVis662014l.getWdflImpstVis662014lAsBuffer(buffer, position);
        position += WdflImpstVis662014l.Len.WDFL_IMPST_VIS662014L;
        wdflImpbIs662014c.getWdflImpbIs662014cAsBuffer(buffer, position);
        position += WdflImpbIs662014c.Len.WDFL_IMPB_IS662014C;
        wdflImpbIs662014d.getWdflImpbIs662014dAsBuffer(buffer, position);
        position += WdflImpbIs662014d.Len.WDFL_IMPB_IS662014D;
        wdflImpbIs662014l.getWdflImpbIs662014lAsBuffer(buffer, position);
        position += WdflImpbIs662014l.Len.WDFL_IMPB_IS662014L;
        wdflIs662014c.getWdflIs662014cAsBuffer(buffer, position);
        position += WdflIs662014c.Len.WDFL_IS662014C;
        wdflIs662014d.getWdflIs662014dAsBuffer(buffer, position);
        position += WdflIs662014d.Len.WDFL_IS662014D;
        wdflIs662014l.getWdflIs662014lAsBuffer(buffer, position);
        return buffer;
    }

    public void setWdflIdDForzLiq(int wdflIdDForzLiq) {
        this.wdflIdDForzLiq = wdflIdDForzLiq;
    }

    public int getWdflIdDForzLiq() {
        return this.wdflIdDForzLiq;
    }

    public void setWdflIdLiq(int wdflIdLiq) {
        this.wdflIdLiq = wdflIdLiq;
    }

    public int getWdflIdLiq() {
        return this.wdflIdLiq;
    }

    public void setWdflIdMoviCrz(int wdflIdMoviCrz) {
        this.wdflIdMoviCrz = wdflIdMoviCrz;
    }

    public int getWdflIdMoviCrz() {
        return this.wdflIdMoviCrz;
    }

    public void setWdflCodCompAnia(int wdflCodCompAnia) {
        this.wdflCodCompAnia = wdflCodCompAnia;
    }

    public int getWdflCodCompAnia() {
        return this.wdflCodCompAnia;
    }

    public void setWdflDtIniEff(int wdflDtIniEff) {
        this.wdflDtIniEff = wdflDtIniEff;
    }

    public int getWdflDtIniEff() {
        return this.wdflDtIniEff;
    }

    public void setWdflDtEndEff(int wdflDtEndEff) {
        this.wdflDtEndEff = wdflDtEndEff;
    }

    public int getWdflDtEndEff() {
        return this.wdflDtEndEff;
    }

    public void setWdflDsRiga(long wdflDsRiga) {
        this.wdflDsRiga = wdflDsRiga;
    }

    public long getWdflDsRiga() {
        return this.wdflDsRiga;
    }

    public void setWdflDsOperSql(char wdflDsOperSql) {
        this.wdflDsOperSql = wdflDsOperSql;
    }

    public char getWdflDsOperSql() {
        return this.wdflDsOperSql;
    }

    public void setWdflDsVer(int wdflDsVer) {
        this.wdflDsVer = wdflDsVer;
    }

    public int getWdflDsVer() {
        return this.wdflDsVer;
    }

    public void setWdflDsTsIniCptz(long wdflDsTsIniCptz) {
        this.wdflDsTsIniCptz = wdflDsTsIniCptz;
    }

    public long getWdflDsTsIniCptz() {
        return this.wdflDsTsIniCptz;
    }

    public void setWdflDsTsEndCptz(long wdflDsTsEndCptz) {
        this.wdflDsTsEndCptz = wdflDsTsEndCptz;
    }

    public long getWdflDsTsEndCptz() {
        return this.wdflDsTsEndCptz;
    }

    public void setWdflDsUtente(String wdflDsUtente) {
        this.wdflDsUtente = Functions.subString(wdflDsUtente, Len.WDFL_DS_UTENTE);
    }

    public String getWdflDsUtente() {
        return this.wdflDsUtente;
    }

    public void setWdflDsStatoElab(char wdflDsStatoElab) {
        this.wdflDsStatoElab = wdflDsStatoElab;
    }

    public char getWdflDsStatoElab() {
        return this.wdflDsStatoElab;
    }

    public WdflAaCnbzDal2007Ef getWdflAaCnbzDal2007Ef() {
        return wdflAaCnbzDal2007Ef;
    }

    public WdflAaCnbzEnd2000Ef getWdflAaCnbzEnd2000Ef() {
        return wdflAaCnbzEnd2000Ef;
    }

    public WdflAaCnbzEnd2006Ef getWdflAaCnbzEnd2006Ef() {
        return wdflAaCnbzEnd2006Ef;
    }

    public WdflAccpreAccCalc getWdflAccpreAccCalc() {
        return wdflAccpreAccCalc;
    }

    public WdflAccpreAccDfz getWdflAccpreAccDfz() {
        return wdflAccpreAccDfz;
    }

    public WdflAccpreAccEfflq getWdflAccpreAccEfflq() {
        return wdflAccpreAccEfflq;
    }

    public WdflAccpreSostCalc getWdflAccpreSostCalc() {
        return wdflAccpreSostCalc;
    }

    public WdflAccpreSostDfz getWdflAccpreSostDfz() {
        return wdflAccpreSostDfz;
    }

    public WdflAccpreSostEfflq getWdflAccpreSostEfflq() {
        return wdflAccpreSostEfflq;
    }

    public WdflAccpreVisCalc getWdflAccpreVisCalc() {
        return wdflAccpreVisCalc;
    }

    public WdflAccpreVisDfz getWdflAccpreVisDfz() {
        return wdflAccpreVisDfz;
    }

    public WdflAccpreVisEfflq getWdflAccpreVisEfflq() {
        return wdflAccpreVisEfflq;
    }

    public WdflAlqCnbtInpstfmC getWdflAlqCnbtInpstfmC() {
        return wdflAlqCnbtInpstfmC;
    }

    public WdflAlqCnbtInpstfmD getWdflAlqCnbtInpstfmD() {
        return wdflAlqCnbtInpstfmD;
    }

    public WdflAlqCnbtInpstfmE getWdflAlqCnbtInpstfmE() {
        return wdflAlqCnbtInpstfmE;
    }

    public WdflAlqTaxSepCalc getWdflAlqTaxSepCalc() {
        return wdflAlqTaxSepCalc;
    }

    public WdflAlqTaxSepDfz getWdflAlqTaxSepDfz() {
        return wdflAlqTaxSepDfz;
    }

    public WdflAlqTaxSepEfflq getWdflAlqTaxSepEfflq() {
        return wdflAlqTaxSepEfflq;
    }

    public WdflCnbtInpstfmCalc getWdflCnbtInpstfmCalc() {
        return wdflCnbtInpstfmCalc;
    }

    public WdflCnbtInpstfmDfz getWdflCnbtInpstfmDfz() {
        return wdflCnbtInpstfmDfz;
    }

    public WdflCnbtInpstfmEfflq getWdflCnbtInpstfmEfflq() {
        return wdflCnbtInpstfmEfflq;
    }

    public WdflCndeDal2007Calc getWdflCndeDal2007Calc() {
        return wdflCndeDal2007Calc;
    }

    public WdflCndeDal2007Dfz getWdflCndeDal2007Dfz() {
        return wdflCndeDal2007Dfz;
    }

    public WdflCndeDal2007Efflq getWdflCndeDal2007Efflq() {
        return wdflCndeDal2007Efflq;
    }

    public WdflCndeEnd2000Calc getWdflCndeEnd2000Calc() {
        return wdflCndeEnd2000Calc;
    }

    public WdflCndeEnd2000Dfz getWdflCndeEnd2000Dfz() {
        return wdflCndeEnd2000Dfz;
    }

    public WdflCndeEnd2000Efflq getWdflCndeEnd2000Efflq() {
        return wdflCndeEnd2000Efflq;
    }

    public WdflCndeEnd2006Calc getWdflCndeEnd2006Calc() {
        return wdflCndeEnd2006Calc;
    }

    public WdflCndeEnd2006Dfz getWdflCndeEnd2006Dfz() {
        return wdflCndeEnd2006Dfz;
    }

    public WdflCndeEnd2006Efflq getWdflCndeEnd2006Efflq() {
        return wdflCndeEnd2006Efflq;
    }

    public WdflIcnbInpstfmCalc getWdflIcnbInpstfmCalc() {
        return wdflIcnbInpstfmCalc;
    }

    public WdflIcnbInpstfmDfz getWdflIcnbInpstfmDfz() {
        return wdflIcnbInpstfmDfz;
    }

    public WdflIcnbInpstfmEfflq getWdflIcnbInpstfmEfflq() {
        return wdflIcnbInpstfmEfflq;
    }

    public WdflIdMoviChiu getWdflIdMoviChiu() {
        return wdflIdMoviChiu;
    }

    public WdflIimpst252Calc getWdflIimpst252Calc() {
        return wdflIimpst252Calc;
    }

    public WdflIimpst252Dfz getWdflIimpst252Dfz() {
        return wdflIimpst252Dfz;
    }

    public WdflIimpst252Efflq getWdflIimpst252Efflq() {
        return wdflIimpst252Efflq;
    }

    public WdflIimpstPrvrCalc getWdflIimpstPrvrCalc() {
        return wdflIimpstPrvrCalc;
    }

    public WdflIimpstPrvrDfz getWdflIimpstPrvrDfz() {
        return wdflIimpstPrvrDfz;
    }

    public WdflIimpstPrvrEfflq getWdflIimpstPrvrEfflq() {
        return wdflIimpstPrvrEfflq;
    }

    public WdflIintPrestCalc getWdflIintPrestCalc() {
        return wdflIintPrestCalc;
    }

    public WdflIintPrestDfz getWdflIintPrestDfz() {
        return wdflIintPrestDfz;
    }

    public WdflIintPrestEfflq getWdflIintPrestEfflq() {
        return wdflIintPrestEfflq;
    }

    public WdflImpExcontrEff getWdflImpExcontrEff() {
        return wdflImpExcontrEff;
    }

    public WdflImpIntrRitPagC getWdflImpIntrRitPagC() {
        return wdflImpIntrRitPagC;
    }

    public WdflImpIntrRitPagD getWdflImpIntrRitPagD() {
        return wdflImpIntrRitPagD;
    }

    public WdflImpIntrRitPagL getWdflImpIntrRitPagL() {
        return wdflImpIntrRitPagL;
    }

    public WdflImpLrdCalc getWdflImpLrdCalc() {
        return wdflImpLrdCalc;
    }

    public WdflImpLrdDfz getWdflImpLrdDfz() {
        return wdflImpLrdDfz;
    }

    public WdflImpLrdEfflq getWdflImpLrdEfflq() {
        return wdflImpLrdEfflq;
    }

    public WdflImpNetCalc getWdflImpNetCalc() {
        return wdflImpNetCalc;
    }

    public WdflImpNetDfz getWdflImpNetDfz() {
        return wdflImpNetDfz;
    }

    public WdflImpNetEfflq getWdflImpNetEfflq() {
        return wdflImpNetEfflq;
    }

    public WdflImpbBolloDettC getWdflImpbBolloDettC() {
        return wdflImpbBolloDettC;
    }

    public WdflImpbBolloDettD getWdflImpbBolloDettD() {
        return wdflImpbBolloDettD;
    }

    public WdflImpbBolloDettL getWdflImpbBolloDettL() {
        return wdflImpbBolloDettL;
    }

    public WdflImpbIs1382011c getWdflImpbIs1382011c() {
        return wdflImpbIs1382011c;
    }

    public WdflImpbIs1382011d getWdflImpbIs1382011d() {
        return wdflImpbIs1382011d;
    }

    public WdflImpbIs1382011l getWdflImpbIs1382011l() {
        return wdflImpbIs1382011l;
    }

    public WdflImpbIs662014c getWdflImpbIs662014c() {
        return wdflImpbIs662014c;
    }

    public WdflImpbIs662014d getWdflImpbIs662014d() {
        return wdflImpbIs662014d;
    }

    public WdflImpbIs662014l getWdflImpbIs662014l() {
        return wdflImpbIs662014l;
    }

    public WdflImpbIsCalc getWdflImpbIsCalc() {
        return wdflImpbIsCalc;
    }

    public WdflImpbIsDfz getWdflImpbIsDfz() {
        return wdflImpbIsDfz;
    }

    public WdflImpbIsEfflq getWdflImpbIsEfflq() {
        return wdflImpbIsEfflq;
    }

    public WdflImpbRitAccCalc getWdflImpbRitAccCalc() {
        return wdflImpbRitAccCalc;
    }

    public WdflImpbRitAccDfz getWdflImpbRitAccDfz() {
        return wdflImpbRitAccDfz;
    }

    public WdflImpbRitAccEfflq getWdflImpbRitAccEfflq() {
        return wdflImpbRitAccEfflq;
    }

    public WdflImpbTaxSepCalc getWdflImpbTaxSepCalc() {
        return wdflImpbTaxSepCalc;
    }

    public WdflImpbTaxSepDfz getWdflImpbTaxSepDfz() {
        return wdflImpbTaxSepDfz;
    }

    public WdflImpbTaxSepEfflq getWdflImpbTaxSepEfflq() {
        return wdflImpbTaxSepEfflq;
    }

    public WdflImpbTfrCalc getWdflImpbTfrCalc() {
        return wdflImpbTfrCalc;
    }

    public WdflImpbTfrDfz getWdflImpbTfrDfz() {
        return wdflImpbTfrDfz;
    }

    public WdflImpbTfrEfflq getWdflImpbTfrEfflq() {
        return wdflImpbTfrEfflq;
    }

    public WdflImpbVis1382011c getWdflImpbVis1382011c() {
        return wdflImpbVis1382011c;
    }

    public WdflImpbVis1382011d getWdflImpbVis1382011d() {
        return wdflImpbVis1382011d;
    }

    public WdflImpbVis1382011l getWdflImpbVis1382011l() {
        return wdflImpbVis1382011l;
    }

    public WdflImpbVis662014c getWdflImpbVis662014c() {
        return wdflImpbVis662014c;
    }

    public WdflImpbVis662014d getWdflImpbVis662014d() {
        return wdflImpbVis662014d;
    }

    public WdflImpbVis662014l getWdflImpbVis662014l() {
        return wdflImpbVis662014l;
    }

    public WdflImpbVisCalc getWdflImpbVisCalc() {
        return wdflImpbVisCalc;
    }

    public WdflImpbVisDfz getWdflImpbVisDfz() {
        return wdflImpbVisDfz;
    }

    public WdflImpbVisEfflq getWdflImpbVisEfflq() {
        return wdflImpbVisEfflq;
    }

    public WdflImpst252Calc getWdflImpst252Calc() {
        return wdflImpst252Calc;
    }

    public WdflImpst252Efflq getWdflImpst252Efflq() {
        return wdflImpst252Efflq;
    }

    public WdflImpstBolloDettC getWdflImpstBolloDettC() {
        return wdflImpstBolloDettC;
    }

    public WdflImpstBolloDettD getWdflImpstBolloDettD() {
        return wdflImpstBolloDettD;
    }

    public WdflImpstBolloDettL getWdflImpstBolloDettL() {
        return wdflImpstBolloDettL;
    }

    public WdflImpstBolloTotVc getWdflImpstBolloTotVc() {
        return wdflImpstBolloTotVc;
    }

    public WdflImpstBolloTotVd getWdflImpstBolloTotVd() {
        return wdflImpstBolloTotVd;
    }

    public WdflImpstBolloTotVl getWdflImpstBolloTotVl() {
        return wdflImpstBolloTotVl;
    }

    public WdflImpstDaRimbEff getWdflImpstDaRimbEff() {
        return wdflImpstDaRimbEff;
    }

    public WdflImpstPrvrCalc getWdflImpstPrvrCalc() {
        return wdflImpstPrvrCalc;
    }

    public WdflImpstPrvrDfz getWdflImpstPrvrDfz() {
        return wdflImpstPrvrDfz;
    }

    public WdflImpstPrvrEfflq getWdflImpstPrvrEfflq() {
        return wdflImpstPrvrEfflq;
    }

    public WdflImpstSostCalc getWdflImpstSostCalc() {
        return wdflImpstSostCalc;
    }

    public WdflImpstSostDfz getWdflImpstSostDfz() {
        return wdflImpstSostDfz;
    }

    public WdflImpstSostEfflq getWdflImpstSostEfflq() {
        return wdflImpstSostEfflq;
    }

    public WdflImpstVis1382011c getWdflImpstVis1382011c() {
        return wdflImpstVis1382011c;
    }

    public WdflImpstVis1382011d getWdflImpstVis1382011d() {
        return wdflImpstVis1382011d;
    }

    public WdflImpstVis1382011l getWdflImpstVis1382011l() {
        return wdflImpstVis1382011l;
    }

    public WdflImpstVis662014c getWdflImpstVis662014c() {
        return wdflImpstVis662014c;
    }

    public WdflImpstVis662014d getWdflImpstVis662014d() {
        return wdflImpstVis662014d;
    }

    public WdflImpstVis662014l getWdflImpstVis662014l() {
        return wdflImpstVis662014l;
    }

    public WdflImpstVisCalc getWdflImpstVisCalc() {
        return wdflImpstVisCalc;
    }

    public WdflImpstVisDfz getWdflImpstVisDfz() {
        return wdflImpstVisDfz;
    }

    public WdflImpstVisEfflq getWdflImpstVisEfflq() {
        return wdflImpstVisEfflq;
    }

    public WdflIntrPrestCalc getWdflIntrPrestCalc() {
        return wdflIntrPrestCalc;
    }

    public WdflIntrPrestDfz getWdflIntrPrestDfz() {
        return wdflIntrPrestDfz;
    }

    public WdflIntrPrestEfflq getWdflIntrPrestEfflq() {
        return wdflIntrPrestEfflq;
    }

    public WdflIs1382011c getWdflIs1382011c() {
        return wdflIs1382011c;
    }

    public WdflIs1382011d getWdflIs1382011d() {
        return wdflIs1382011d;
    }

    public WdflIs1382011l getWdflIs1382011l() {
        return wdflIs1382011l;
    }

    public WdflIs662014c getWdflIs662014c() {
        return wdflIs662014c;
    }

    public WdflIs662014d getWdflIs662014d() {
        return wdflIs662014d;
    }

    public WdflIs662014l getWdflIs662014l() {
        return wdflIs662014l;
    }

    public WdflMmCnbzDal2007Ef getWdflMmCnbzDal2007Ef() {
        return wdflMmCnbzDal2007Ef;
    }

    public WdflMmCnbzEnd2000Ef getWdflMmCnbzEnd2000Ef() {
        return wdflMmCnbzEnd2000Ef;
    }

    public WdflMmCnbzEnd2006Ef getWdflMmCnbzEnd2006Ef() {
        return wdflMmCnbzEnd2006Ef;
    }

    public WdflMontDal2007Calc getWdflMontDal2007Calc() {
        return wdflMontDal2007Calc;
    }

    public WdflMontDal2007Dfz getWdflMontDal2007Dfz() {
        return wdflMontDal2007Dfz;
    }

    public WdflMontDal2007Efflq getWdflMontDal2007Efflq() {
        return wdflMontDal2007Efflq;
    }

    public WdflMontEnd2000Calc getWdflMontEnd2000Calc() {
        return wdflMontEnd2000Calc;
    }

    public WdflMontEnd2000Dfz getWdflMontEnd2000Dfz() {
        return wdflMontEnd2000Dfz;
    }

    public WdflMontEnd2000Efflq getWdflMontEnd2000Efflq() {
        return wdflMontEnd2000Efflq;
    }

    public WdflMontEnd2006Calc getWdflMontEnd2006Calc() {
        return wdflMontEnd2006Calc;
    }

    public WdflMontEnd2006Dfz getWdflMontEnd2006Dfz() {
        return wdflMontEnd2006Dfz;
    }

    public WdflMontEnd2006Efflq getWdflMontEnd2006Efflq() {
        return wdflMontEnd2006Efflq;
    }

    public WdflResPreAttCalc getWdflResPreAttCalc() {
        return wdflResPreAttCalc;
    }

    public WdflResPreAttDfz getWdflResPreAttDfz() {
        return wdflResPreAttDfz;
    }

    public WdflResPreAttEfflq getWdflResPreAttEfflq() {
        return wdflResPreAttEfflq;
    }

    public WdflResPrstzCalc getWdflResPrstzCalc() {
        return wdflResPrstzCalc;
    }

    public WdflResPrstzDfz getWdflResPrstzDfz() {
        return wdflResPrstzDfz;
    }

    public WdflResPrstzEfflq getWdflResPrstzEfflq() {
        return wdflResPrstzEfflq;
    }

    public WdflRitAccCalc getWdflRitAccCalc() {
        return wdflRitAccCalc;
    }

    public WdflRitAccDfz getWdflRitAccDfz() {
        return wdflRitAccDfz;
    }

    public WdflRitAccEfflq getWdflRitAccEfflq() {
        return wdflRitAccEfflq;
    }

    public WdflRitIrpefCalc getWdflRitIrpefCalc() {
        return wdflRitIrpefCalc;
    }

    public WdflRitIrpefDfz getWdflRitIrpefDfz() {
        return wdflRitIrpefDfz;
    }

    public WdflRitIrpefEfflq getWdflRitIrpefEfflq() {
        return wdflRitIrpefEfflq;
    }

    public WdflRitTfrCalc getWdflRitTfrCalc() {
        return wdflRitTfrCalc;
    }

    public WdflRitTfrDfz getWdflRitTfrDfz() {
        return wdflRitTfrDfz;
    }

    public WdflRitTfrEfflq getWdflRitTfrEfflq() {
        return wdflRitTfrEfflq;
    }

    public WdflTaxSepCalc getWdflTaxSepCalc() {
        return wdflTaxSepCalc;
    }

    public WdflTaxSepDfz getWdflTaxSepDfz() {
        return wdflTaxSepDfz;
    }

    public WdflTaxSepEfflq getWdflTaxSepEfflq() {
        return wdflTaxSepEfflq;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ID_D_FORZ_LIQ = 5;
        public static final int WDFL_ID_LIQ = 5;
        public static final int WDFL_ID_MOVI_CRZ = 5;
        public static final int WDFL_COD_COMP_ANIA = 3;
        public static final int WDFL_DT_INI_EFF = 5;
        public static final int WDFL_DT_END_EFF = 5;
        public static final int WDFL_DS_RIGA = 6;
        public static final int WDFL_DS_OPER_SQL = 1;
        public static final int WDFL_DS_VER = 5;
        public static final int WDFL_DS_TS_INI_CPTZ = 10;
        public static final int WDFL_DS_TS_END_CPTZ = 10;
        public static final int WDFL_DS_UTENTE = 20;
        public static final int WDFL_DS_STATO_ELAB = 1;
        public static final int DATI = WDFL_ID_D_FORZ_LIQ + WDFL_ID_LIQ + WDFL_ID_MOVI_CRZ + WdflIdMoviChiu.Len.WDFL_ID_MOVI_CHIU + WDFL_COD_COMP_ANIA + WDFL_DT_INI_EFF + WDFL_DT_END_EFF + WdflImpLrdCalc.Len.WDFL_IMP_LRD_CALC + WdflImpLrdDfz.Len.WDFL_IMP_LRD_DFZ + WdflImpLrdEfflq.Len.WDFL_IMP_LRD_EFFLQ + WdflImpNetCalc.Len.WDFL_IMP_NET_CALC + WdflImpNetDfz.Len.WDFL_IMP_NET_DFZ + WdflImpNetEfflq.Len.WDFL_IMP_NET_EFFLQ + WdflImpstPrvrCalc.Len.WDFL_IMPST_PRVR_CALC + WdflImpstPrvrDfz.Len.WDFL_IMPST_PRVR_DFZ + WdflImpstPrvrEfflq.Len.WDFL_IMPST_PRVR_EFFLQ + WdflImpstVisCalc.Len.WDFL_IMPST_VIS_CALC + WdflImpstVisDfz.Len.WDFL_IMPST_VIS_DFZ + WdflImpstVisEfflq.Len.WDFL_IMPST_VIS_EFFLQ + WdflRitAccCalc.Len.WDFL_RIT_ACC_CALC + WdflRitAccDfz.Len.WDFL_RIT_ACC_DFZ + WdflRitAccEfflq.Len.WDFL_RIT_ACC_EFFLQ + WdflRitIrpefCalc.Len.WDFL_RIT_IRPEF_CALC + WdflRitIrpefDfz.Len.WDFL_RIT_IRPEF_DFZ + WdflRitIrpefEfflq.Len.WDFL_RIT_IRPEF_EFFLQ + WdflImpstSostCalc.Len.WDFL_IMPST_SOST_CALC + WdflImpstSostDfz.Len.WDFL_IMPST_SOST_DFZ + WdflImpstSostEfflq.Len.WDFL_IMPST_SOST_EFFLQ + WdflTaxSepCalc.Len.WDFL_TAX_SEP_CALC + WdflTaxSepDfz.Len.WDFL_TAX_SEP_DFZ + WdflTaxSepEfflq.Len.WDFL_TAX_SEP_EFFLQ + WdflIntrPrestCalc.Len.WDFL_INTR_PREST_CALC + WdflIntrPrestDfz.Len.WDFL_INTR_PREST_DFZ + WdflIntrPrestEfflq.Len.WDFL_INTR_PREST_EFFLQ + WdflAccpreSostCalc.Len.WDFL_ACCPRE_SOST_CALC + WdflAccpreSostDfz.Len.WDFL_ACCPRE_SOST_DFZ + WdflAccpreSostEfflq.Len.WDFL_ACCPRE_SOST_EFFLQ + WdflAccpreVisCalc.Len.WDFL_ACCPRE_VIS_CALC + WdflAccpreVisDfz.Len.WDFL_ACCPRE_VIS_DFZ + WdflAccpreVisEfflq.Len.WDFL_ACCPRE_VIS_EFFLQ + WdflAccpreAccCalc.Len.WDFL_ACCPRE_ACC_CALC + WdflAccpreAccDfz.Len.WDFL_ACCPRE_ACC_DFZ + WdflAccpreAccEfflq.Len.WDFL_ACCPRE_ACC_EFFLQ + WdflResPrstzCalc.Len.WDFL_RES_PRSTZ_CALC + WdflResPrstzDfz.Len.WDFL_RES_PRSTZ_DFZ + WdflResPrstzEfflq.Len.WDFL_RES_PRSTZ_EFFLQ + WdflResPreAttCalc.Len.WDFL_RES_PRE_ATT_CALC + WdflResPreAttDfz.Len.WDFL_RES_PRE_ATT_DFZ + WdflResPreAttEfflq.Len.WDFL_RES_PRE_ATT_EFFLQ + WdflImpExcontrEff.Len.WDFL_IMP_EXCONTR_EFF + WdflImpbVisCalc.Len.WDFL_IMPB_VIS_CALC + WdflImpbVisEfflq.Len.WDFL_IMPB_VIS_EFFLQ + WdflImpbVisDfz.Len.WDFL_IMPB_VIS_DFZ + WdflImpbRitAccCalc.Len.WDFL_IMPB_RIT_ACC_CALC + WdflImpbRitAccEfflq.Len.WDFL_IMPB_RIT_ACC_EFFLQ + WdflImpbRitAccDfz.Len.WDFL_IMPB_RIT_ACC_DFZ + WdflImpbTfrCalc.Len.WDFL_IMPB_TFR_CALC + WdflImpbTfrEfflq.Len.WDFL_IMPB_TFR_EFFLQ + WdflImpbTfrDfz.Len.WDFL_IMPB_TFR_DFZ + WdflImpbIsCalc.Len.WDFL_IMPB_IS_CALC + WdflImpbIsEfflq.Len.WDFL_IMPB_IS_EFFLQ + WdflImpbIsDfz.Len.WDFL_IMPB_IS_DFZ + WdflImpbTaxSepCalc.Len.WDFL_IMPB_TAX_SEP_CALC + WdflImpbTaxSepEfflq.Len.WDFL_IMPB_TAX_SEP_EFFLQ + WdflImpbTaxSepDfz.Len.WDFL_IMPB_TAX_SEP_DFZ + WdflIintPrestCalc.Len.WDFL_IINT_PREST_CALC + WdflIintPrestEfflq.Len.WDFL_IINT_PREST_EFFLQ + WdflIintPrestDfz.Len.WDFL_IINT_PREST_DFZ + WdflMontEnd2000Calc.Len.WDFL_MONT_END2000_CALC + WdflMontEnd2000Efflq.Len.WDFL_MONT_END2000_EFFLQ + WdflMontEnd2000Dfz.Len.WDFL_MONT_END2000_DFZ + WdflMontEnd2006Calc.Len.WDFL_MONT_END2006_CALC + WdflMontEnd2006Efflq.Len.WDFL_MONT_END2006_EFFLQ + WdflMontEnd2006Dfz.Len.WDFL_MONT_END2006_DFZ + WdflMontDal2007Calc.Len.WDFL_MONT_DAL2007_CALC + WdflMontDal2007Efflq.Len.WDFL_MONT_DAL2007_EFFLQ + WdflMontDal2007Dfz.Len.WDFL_MONT_DAL2007_DFZ + WdflIimpstPrvrCalc.Len.WDFL_IIMPST_PRVR_CALC + WdflIimpstPrvrEfflq.Len.WDFL_IIMPST_PRVR_EFFLQ + WdflIimpstPrvrDfz.Len.WDFL_IIMPST_PRVR_DFZ + WdflIimpst252Calc.Len.WDFL_IIMPST252_CALC + WdflIimpst252Efflq.Len.WDFL_IIMPST252_EFFLQ + WdflIimpst252Dfz.Len.WDFL_IIMPST252_DFZ + WdflImpst252Calc.Len.WDFL_IMPST252_CALC + WdflImpst252Efflq.Len.WDFL_IMPST252_EFFLQ + WdflRitTfrCalc.Len.WDFL_RIT_TFR_CALC + WdflRitTfrEfflq.Len.WDFL_RIT_TFR_EFFLQ + WdflRitTfrDfz.Len.WDFL_RIT_TFR_DFZ + WdflCnbtInpstfmCalc.Len.WDFL_CNBT_INPSTFM_CALC + WdflCnbtInpstfmEfflq.Len.WDFL_CNBT_INPSTFM_EFFLQ + WdflCnbtInpstfmDfz.Len.WDFL_CNBT_INPSTFM_DFZ + WdflIcnbInpstfmCalc.Len.WDFL_ICNB_INPSTFM_CALC + WdflIcnbInpstfmEfflq.Len.WDFL_ICNB_INPSTFM_EFFLQ + WdflIcnbInpstfmDfz.Len.WDFL_ICNB_INPSTFM_DFZ + WdflCndeEnd2000Calc.Len.WDFL_CNDE_END2000_CALC + WdflCndeEnd2000Efflq.Len.WDFL_CNDE_END2000_EFFLQ + WdflCndeEnd2000Dfz.Len.WDFL_CNDE_END2000_DFZ + WdflCndeEnd2006Calc.Len.WDFL_CNDE_END2006_CALC + WdflCndeEnd2006Efflq.Len.WDFL_CNDE_END2006_EFFLQ + WdflCndeEnd2006Dfz.Len.WDFL_CNDE_END2006_DFZ + WdflCndeDal2007Calc.Len.WDFL_CNDE_DAL2007_CALC + WdflCndeDal2007Efflq.Len.WDFL_CNDE_DAL2007_EFFLQ + WdflCndeDal2007Dfz.Len.WDFL_CNDE_DAL2007_DFZ + WdflAaCnbzEnd2000Ef.Len.WDFL_AA_CNBZ_END2000_EF + WdflAaCnbzEnd2006Ef.Len.WDFL_AA_CNBZ_END2006_EF + WdflAaCnbzDal2007Ef.Len.WDFL_AA_CNBZ_DAL2007_EF + WdflMmCnbzEnd2000Ef.Len.WDFL_MM_CNBZ_END2000_EF + WdflMmCnbzEnd2006Ef.Len.WDFL_MM_CNBZ_END2006_EF + WdflMmCnbzDal2007Ef.Len.WDFL_MM_CNBZ_DAL2007_EF + WdflImpstDaRimbEff.Len.WDFL_IMPST_DA_RIMB_EFF + WdflAlqTaxSepCalc.Len.WDFL_ALQ_TAX_SEP_CALC + WdflAlqTaxSepEfflq.Len.WDFL_ALQ_TAX_SEP_EFFLQ + WdflAlqTaxSepDfz.Len.WDFL_ALQ_TAX_SEP_DFZ + WdflAlqCnbtInpstfmC.Len.WDFL_ALQ_CNBT_INPSTFM_C + WdflAlqCnbtInpstfmE.Len.WDFL_ALQ_CNBT_INPSTFM_E + WdflAlqCnbtInpstfmD.Len.WDFL_ALQ_CNBT_INPSTFM_D + WDFL_DS_RIGA + WDFL_DS_OPER_SQL + WDFL_DS_VER + WDFL_DS_TS_INI_CPTZ + WDFL_DS_TS_END_CPTZ + WDFL_DS_UTENTE + WDFL_DS_STATO_ELAB + WdflImpbVis1382011c.Len.WDFL_IMPB_VIS1382011C + WdflImpbVis1382011d.Len.WDFL_IMPB_VIS1382011D + WdflImpbVis1382011l.Len.WDFL_IMPB_VIS1382011L + WdflImpstVis1382011c.Len.WDFL_IMPST_VIS1382011C + WdflImpstVis1382011d.Len.WDFL_IMPST_VIS1382011D + WdflImpstVis1382011l.Len.WDFL_IMPST_VIS1382011L + WdflImpbIs1382011c.Len.WDFL_IMPB_IS1382011C + WdflImpbIs1382011d.Len.WDFL_IMPB_IS1382011D + WdflImpbIs1382011l.Len.WDFL_IMPB_IS1382011L + WdflIs1382011c.Len.WDFL_IS1382011C + WdflIs1382011d.Len.WDFL_IS1382011D + WdflIs1382011l.Len.WDFL_IS1382011L + WdflImpIntrRitPagC.Len.WDFL_IMP_INTR_RIT_PAG_C + WdflImpIntrRitPagD.Len.WDFL_IMP_INTR_RIT_PAG_D + WdflImpIntrRitPagL.Len.WDFL_IMP_INTR_RIT_PAG_L + WdflImpbBolloDettC.Len.WDFL_IMPB_BOLLO_DETT_C + WdflImpbBolloDettD.Len.WDFL_IMPB_BOLLO_DETT_D + WdflImpbBolloDettL.Len.WDFL_IMPB_BOLLO_DETT_L + WdflImpstBolloDettC.Len.WDFL_IMPST_BOLLO_DETT_C + WdflImpstBolloDettD.Len.WDFL_IMPST_BOLLO_DETT_D + WdflImpstBolloDettL.Len.WDFL_IMPST_BOLLO_DETT_L + WdflImpstBolloTotVc.Len.WDFL_IMPST_BOLLO_TOT_VC + WdflImpstBolloTotVd.Len.WDFL_IMPST_BOLLO_TOT_VD + WdflImpstBolloTotVl.Len.WDFL_IMPST_BOLLO_TOT_VL + WdflImpbVis662014c.Len.WDFL_IMPB_VIS662014C + WdflImpbVis662014d.Len.WDFL_IMPB_VIS662014D + WdflImpbVis662014l.Len.WDFL_IMPB_VIS662014L + WdflImpstVis662014c.Len.WDFL_IMPST_VIS662014C + WdflImpstVis662014d.Len.WDFL_IMPST_VIS662014D + WdflImpstVis662014l.Len.WDFL_IMPST_VIS662014L + WdflImpbIs662014c.Len.WDFL_IMPB_IS662014C + WdflImpbIs662014d.Len.WDFL_IMPB_IS662014D + WdflImpbIs662014l.Len.WDFL_IMPB_IS662014L + WdflIs662014c.Len.WDFL_IS662014C + WdflIs662014d.Len.WDFL_IS662014D + WdflIs662014l.Len.WDFL_IS662014L;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ID_D_FORZ_LIQ = 9;
            public static final int WDFL_ID_LIQ = 9;
            public static final int WDFL_ID_MOVI_CRZ = 9;
            public static final int WDFL_COD_COMP_ANIA = 5;
            public static final int WDFL_DT_INI_EFF = 8;
            public static final int WDFL_DT_END_EFF = 8;
            public static final int WDFL_DS_RIGA = 10;
            public static final int WDFL_DS_VER = 9;
            public static final int WDFL_DS_TS_INI_CPTZ = 18;
            public static final int WDFL_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
