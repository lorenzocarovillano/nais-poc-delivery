package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.StwIdMoviChiu;

/**Original name: STAT-OGG-WF<br>
 * Variable: STAT-OGG-WF from copybook IDBVSTW1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class StatOggWf {

    //==== PROPERTIES ====
    //Original name: STW-ID-STAT-OGG-WF
    private int stwIdStatOggWf = DefaultValues.INT_VAL;
    //Original name: STW-ID-OGG
    private int stwIdOgg = DefaultValues.INT_VAL;
    //Original name: STW-TP-OGG
    private String stwTpOgg = DefaultValues.stringVal(Len.STW_TP_OGG);
    //Original name: STW-ID-MOVI-CRZ
    private int stwIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: STW-ID-MOVI-CHIU
    private StwIdMoviChiu stwIdMoviChiu = new StwIdMoviChiu();
    //Original name: STW-DT-INI-EFF
    private int stwDtIniEff = DefaultValues.INT_VAL;
    //Original name: STW-DT-END-EFF
    private int stwDtEndEff = DefaultValues.INT_VAL;
    //Original name: STW-COD-COMP-ANIA
    private int stwCodCompAnia = DefaultValues.INT_VAL;
    //Original name: STW-COD-PRCS
    private String stwCodPrcs = DefaultValues.stringVal(Len.STW_COD_PRCS);
    //Original name: STW-COD-ATTVT
    private String stwCodAttvt = DefaultValues.stringVal(Len.STW_COD_ATTVT);
    //Original name: STW-STAT-OGG-WF
    private String stwStatOggWf = DefaultValues.stringVal(Len.STW_STAT_OGG_WF);
    //Original name: STW-FL-STAT-END
    private char stwFlStatEnd = DefaultValues.CHAR_VAL;
    //Original name: STW-DS-RIGA
    private long stwDsRiga = DefaultValues.LONG_VAL;
    //Original name: STW-DS-OPER-SQL
    private char stwDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: STW-DS-VER
    private int stwDsVer = DefaultValues.INT_VAL;
    //Original name: STW-DS-TS-INI-CPTZ
    private long stwDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: STW-DS-TS-END-CPTZ
    private long stwDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: STW-DS-UTENTE
    private String stwDsUtente = DefaultValues.stringVal(Len.STW_DS_UTENTE);
    //Original name: STW-DS-STATO-ELAB
    private char stwDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setStatOggWfFormatted(String data) {
        byte[] buffer = new byte[Len.STAT_OGG_WF];
        MarshalByte.writeString(buffer, 1, data, Len.STAT_OGG_WF);
        setStatOggWfBytes(buffer, 1);
    }

    public String getStatOggWfFormatted() {
        return MarshalByteExt.bufferToStr(getStatOggWfBytes());
    }

    public byte[] getStatOggWfBytes() {
        byte[] buffer = new byte[Len.STAT_OGG_WF];
        return getStatOggWfBytes(buffer, 1);
    }

    public void setStatOggWfBytes(byte[] buffer, int offset) {
        int position = offset;
        stwIdStatOggWf = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STW_ID_STAT_OGG_WF, 0);
        position += Len.STW_ID_STAT_OGG_WF;
        stwIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STW_ID_OGG, 0);
        position += Len.STW_ID_OGG;
        stwTpOgg = MarshalByte.readString(buffer, position, Len.STW_TP_OGG);
        position += Len.STW_TP_OGG;
        stwIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STW_ID_MOVI_CRZ, 0);
        position += Len.STW_ID_MOVI_CRZ;
        stwIdMoviChiu.setStwIdMoviChiuFromBuffer(buffer, position);
        position += StwIdMoviChiu.Len.STW_ID_MOVI_CHIU;
        stwDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STW_DT_INI_EFF, 0);
        position += Len.STW_DT_INI_EFF;
        stwDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STW_DT_END_EFF, 0);
        position += Len.STW_DT_END_EFF;
        stwCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STW_COD_COMP_ANIA, 0);
        position += Len.STW_COD_COMP_ANIA;
        stwCodPrcs = MarshalByte.readString(buffer, position, Len.STW_COD_PRCS);
        position += Len.STW_COD_PRCS;
        stwCodAttvt = MarshalByte.readString(buffer, position, Len.STW_COD_ATTVT);
        position += Len.STW_COD_ATTVT;
        stwStatOggWf = MarshalByte.readString(buffer, position, Len.STW_STAT_OGG_WF);
        position += Len.STW_STAT_OGG_WF;
        stwFlStatEnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        stwDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.STW_DS_RIGA, 0);
        position += Len.STW_DS_RIGA;
        stwDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        stwDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.STW_DS_VER, 0);
        position += Len.STW_DS_VER;
        stwDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.STW_DS_TS_INI_CPTZ, 0);
        position += Len.STW_DS_TS_INI_CPTZ;
        stwDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.STW_DS_TS_END_CPTZ, 0);
        position += Len.STW_DS_TS_END_CPTZ;
        stwDsUtente = MarshalByte.readString(buffer, position, Len.STW_DS_UTENTE);
        position += Len.STW_DS_UTENTE;
        stwDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getStatOggWfBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, stwIdStatOggWf, Len.Int.STW_ID_STAT_OGG_WF, 0);
        position += Len.STW_ID_STAT_OGG_WF;
        MarshalByte.writeIntAsPacked(buffer, position, stwIdOgg, Len.Int.STW_ID_OGG, 0);
        position += Len.STW_ID_OGG;
        MarshalByte.writeString(buffer, position, stwTpOgg, Len.STW_TP_OGG);
        position += Len.STW_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, stwIdMoviCrz, Len.Int.STW_ID_MOVI_CRZ, 0);
        position += Len.STW_ID_MOVI_CRZ;
        stwIdMoviChiu.getStwIdMoviChiuAsBuffer(buffer, position);
        position += StwIdMoviChiu.Len.STW_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, stwDtIniEff, Len.Int.STW_DT_INI_EFF, 0);
        position += Len.STW_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, stwDtEndEff, Len.Int.STW_DT_END_EFF, 0);
        position += Len.STW_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, stwCodCompAnia, Len.Int.STW_COD_COMP_ANIA, 0);
        position += Len.STW_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, stwCodPrcs, Len.STW_COD_PRCS);
        position += Len.STW_COD_PRCS;
        MarshalByte.writeString(buffer, position, stwCodAttvt, Len.STW_COD_ATTVT);
        position += Len.STW_COD_ATTVT;
        MarshalByte.writeString(buffer, position, stwStatOggWf, Len.STW_STAT_OGG_WF);
        position += Len.STW_STAT_OGG_WF;
        MarshalByte.writeChar(buffer, position, stwFlStatEnd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, stwDsRiga, Len.Int.STW_DS_RIGA, 0);
        position += Len.STW_DS_RIGA;
        MarshalByte.writeChar(buffer, position, stwDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, stwDsVer, Len.Int.STW_DS_VER, 0);
        position += Len.STW_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, stwDsTsIniCptz, Len.Int.STW_DS_TS_INI_CPTZ, 0);
        position += Len.STW_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, stwDsTsEndCptz, Len.Int.STW_DS_TS_END_CPTZ, 0);
        position += Len.STW_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, stwDsUtente, Len.STW_DS_UTENTE);
        position += Len.STW_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, stwDsStatoElab);
        return buffer;
    }

    public void setStwIdStatOggWf(int stwIdStatOggWf) {
        this.stwIdStatOggWf = stwIdStatOggWf;
    }

    public int getStwIdStatOggWf() {
        return this.stwIdStatOggWf;
    }

    public void setStwIdOgg(int stwIdOgg) {
        this.stwIdOgg = stwIdOgg;
    }

    public int getStwIdOgg() {
        return this.stwIdOgg;
    }

    public void setStwTpOgg(String stwTpOgg) {
        this.stwTpOgg = Functions.subString(stwTpOgg, Len.STW_TP_OGG);
    }

    public String getStwTpOgg() {
        return this.stwTpOgg;
    }

    public void setStwIdMoviCrz(int stwIdMoviCrz) {
        this.stwIdMoviCrz = stwIdMoviCrz;
    }

    public int getStwIdMoviCrz() {
        return this.stwIdMoviCrz;
    }

    public void setStwDtIniEff(int stwDtIniEff) {
        this.stwDtIniEff = stwDtIniEff;
    }

    public int getStwDtIniEff() {
        return this.stwDtIniEff;
    }

    public void setStwDtEndEff(int stwDtEndEff) {
        this.stwDtEndEff = stwDtEndEff;
    }

    public int getStwDtEndEff() {
        return this.stwDtEndEff;
    }

    public void setStwCodCompAnia(int stwCodCompAnia) {
        this.stwCodCompAnia = stwCodCompAnia;
    }

    public int getStwCodCompAnia() {
        return this.stwCodCompAnia;
    }

    public void setStwCodPrcs(String stwCodPrcs) {
        this.stwCodPrcs = Functions.subString(stwCodPrcs, Len.STW_COD_PRCS);
    }

    public String getStwCodPrcs() {
        return this.stwCodPrcs;
    }

    public void setStwCodAttvt(String stwCodAttvt) {
        this.stwCodAttvt = Functions.subString(stwCodAttvt, Len.STW_COD_ATTVT);
    }

    public String getStwCodAttvt() {
        return this.stwCodAttvt;
    }

    public void setStwStatOggWf(String stwStatOggWf) {
        this.stwStatOggWf = Functions.subString(stwStatOggWf, Len.STW_STAT_OGG_WF);
    }

    public String getStwStatOggWf() {
        return this.stwStatOggWf;
    }

    public void setStwFlStatEnd(char stwFlStatEnd) {
        this.stwFlStatEnd = stwFlStatEnd;
    }

    public char getStwFlStatEnd() {
        return this.stwFlStatEnd;
    }

    public void setStwDsRiga(long stwDsRiga) {
        this.stwDsRiga = stwDsRiga;
    }

    public long getStwDsRiga() {
        return this.stwDsRiga;
    }

    public void setStwDsOperSql(char stwDsOperSql) {
        this.stwDsOperSql = stwDsOperSql;
    }

    public char getStwDsOperSql() {
        return this.stwDsOperSql;
    }

    public void setStwDsVer(int stwDsVer) {
        this.stwDsVer = stwDsVer;
    }

    public int getStwDsVer() {
        return this.stwDsVer;
    }

    public void setStwDsTsIniCptz(long stwDsTsIniCptz) {
        this.stwDsTsIniCptz = stwDsTsIniCptz;
    }

    public long getStwDsTsIniCptz() {
        return this.stwDsTsIniCptz;
    }

    public void setStwDsTsEndCptz(long stwDsTsEndCptz) {
        this.stwDsTsEndCptz = stwDsTsEndCptz;
    }

    public long getStwDsTsEndCptz() {
        return this.stwDsTsEndCptz;
    }

    public void setStwDsUtente(String stwDsUtente) {
        this.stwDsUtente = Functions.subString(stwDsUtente, Len.STW_DS_UTENTE);
    }

    public String getStwDsUtente() {
        return this.stwDsUtente;
    }

    public void setStwDsStatoElab(char stwDsStatoElab) {
        this.stwDsStatoElab = stwDsStatoElab;
    }

    public char getStwDsStatoElab() {
        return this.stwDsStatoElab;
    }

    public StwIdMoviChiu getStwIdMoviChiu() {
        return stwIdMoviChiu;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int STW_TP_OGG = 2;
        public static final int STW_COD_PRCS = 3;
        public static final int STW_COD_ATTVT = 10;
        public static final int STW_STAT_OGG_WF = 2;
        public static final int STW_DS_UTENTE = 20;
        public static final int STW_ID_STAT_OGG_WF = 5;
        public static final int STW_ID_OGG = 5;
        public static final int STW_ID_MOVI_CRZ = 5;
        public static final int STW_DT_INI_EFF = 5;
        public static final int STW_DT_END_EFF = 5;
        public static final int STW_COD_COMP_ANIA = 3;
        public static final int STW_FL_STAT_END = 1;
        public static final int STW_DS_RIGA = 6;
        public static final int STW_DS_OPER_SQL = 1;
        public static final int STW_DS_VER = 5;
        public static final int STW_DS_TS_INI_CPTZ = 10;
        public static final int STW_DS_TS_END_CPTZ = 10;
        public static final int STW_DS_STATO_ELAB = 1;
        public static final int STAT_OGG_WF = STW_ID_STAT_OGG_WF + STW_ID_OGG + STW_TP_OGG + STW_ID_MOVI_CRZ + StwIdMoviChiu.Len.STW_ID_MOVI_CHIU + STW_DT_INI_EFF + STW_DT_END_EFF + STW_COD_COMP_ANIA + STW_COD_PRCS + STW_COD_ATTVT + STW_STAT_OGG_WF + STW_FL_STAT_END + STW_DS_RIGA + STW_DS_OPER_SQL + STW_DS_VER + STW_DS_TS_INI_CPTZ + STW_DS_TS_END_CPTZ + STW_DS_UTENTE + STW_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int STW_ID_STAT_OGG_WF = 9;
            public static final int STW_ID_OGG = 9;
            public static final int STW_ID_MOVI_CRZ = 9;
            public static final int STW_DT_INI_EFF = 8;
            public static final int STW_DT_END_EFF = 8;
            public static final int STW_COD_COMP_ANIA = 5;
            public static final int STW_DS_RIGA = 10;
            public static final int STW_DS_VER = 9;
            public static final int STW_DS_TS_INI_CPTZ = 18;
            public static final int STW_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
