package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.L3401AaPagPreUni;
import it.accenture.jnais.ws.redefines.L3401DtDecor;
import it.accenture.jnais.ws.redefines.L3401DtEndCarz;
import it.accenture.jnais.ws.redefines.L3401DtIniValTar;
import it.accenture.jnais.ws.redefines.L3401DtScad;
import it.accenture.jnais.ws.redefines.L3401DtVarzTpIas;
import it.accenture.jnais.ws.redefines.L3401DurAa;
import it.accenture.jnais.ws.redefines.L3401DurGg;
import it.accenture.jnais.ws.redefines.L3401DurMm;
import it.accenture.jnais.ws.redefines.L3401EtaAa1oAssto;
import it.accenture.jnais.ws.redefines.L3401EtaAa2oAssto;
import it.accenture.jnais.ws.redefines.L3401EtaAa3oAssto;
import it.accenture.jnais.ws.redefines.L3401EtaAScad;
import it.accenture.jnais.ws.redefines.L3401EtaMm1oAssto;
import it.accenture.jnais.ws.redefines.L3401EtaMm2oAssto;
import it.accenture.jnais.ws.redefines.L3401EtaMm3oAssto;
import it.accenture.jnais.ws.redefines.L3401FrazDecrCpt;
import it.accenture.jnais.ws.redefines.L3401FrazIniErogRen;
import it.accenture.jnais.ws.redefines.L3401Id1oAssto;
import it.accenture.jnais.ws.redefines.L3401Id2oAssto;
import it.accenture.jnais.ws.redefines.L3401Id3oAssto;
import it.accenture.jnais.ws.redefines.L3401IdAdes;
import it.accenture.jnais.ws.redefines.L3401IdMoviChiu;
import it.accenture.jnais.ws.redefines.L3401Mm1oRat;
import it.accenture.jnais.ws.redefines.L3401MmPagPreUni;
import it.accenture.jnais.ws.redefines.L3401NumAaPagPre;
import it.accenture.jnais.ws.redefines.L3401Pc1oRat;
import it.accenture.jnais.ws.redefines.L3401PcOpz;
import it.accenture.jnais.ws.redefines.L3401PcRevrsb;
import it.accenture.jnais.ws.redefines.L3401PcRipPre;
import it.accenture.jnais.ws.redefines.L3401TpGar;
import it.accenture.jnais.ws.redefines.L3401TpInvst;
import it.accenture.jnais.ws.redefines.L3401TsStabLimitata;

/**Original name: LDBV3401-GAR-OUTPUT<br>
 * Variable: LDBV3401-GAR-OUTPUT from copybook LDBV3401<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbv3401GarOutput {

    //==== PROPERTIES ====
    //Original name: L3401-ID-GAR
    private int l3401IdGar = DefaultValues.INT_VAL;
    //Original name: L3401-ID-ADES
    private L3401IdAdes l3401IdAdes = new L3401IdAdes();
    //Original name: L3401-ID-POLI
    private int l3401IdPoli = DefaultValues.INT_VAL;
    //Original name: L3401-ID-MOVI-CRZ
    private int l3401IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: L3401-ID-MOVI-CHIU
    private L3401IdMoviChiu l3401IdMoviChiu = new L3401IdMoviChiu();
    //Original name: L3401-DT-INI-EFF
    private int l3401DtIniEff = DefaultValues.INT_VAL;
    //Original name: L3401-DT-END-EFF
    private int l3401DtEndEff = DefaultValues.INT_VAL;
    //Original name: L3401-COD-COMP-ANIA
    private int l3401CodCompAnia = DefaultValues.INT_VAL;
    //Original name: L3401-IB-OGG
    private String l3401IbOgg = DefaultValues.stringVal(Len.L3401_IB_OGG);
    //Original name: L3401-DT-DECOR
    private L3401DtDecor l3401DtDecor = new L3401DtDecor();
    //Original name: L3401-DT-SCAD
    private L3401DtScad l3401DtScad = new L3401DtScad();
    //Original name: L3401-COD-SEZ
    private String l3401CodSez = DefaultValues.stringVal(Len.L3401_COD_SEZ);
    //Original name: L3401-COD-TARI
    private String l3401CodTari = DefaultValues.stringVal(Len.L3401_COD_TARI);
    //Original name: L3401-RAMO-BILA
    private String l3401RamoBila = DefaultValues.stringVal(Len.L3401_RAMO_BILA);
    //Original name: L3401-DT-INI-VAL-TAR
    private L3401DtIniValTar l3401DtIniValTar = new L3401DtIniValTar();
    //Original name: L3401-ID-1O-ASSTO
    private L3401Id1oAssto l3401Id1oAssto = new L3401Id1oAssto();
    //Original name: L3401-ID-2O-ASSTO
    private L3401Id2oAssto l3401Id2oAssto = new L3401Id2oAssto();
    //Original name: L3401-ID-3O-ASSTO
    private L3401Id3oAssto l3401Id3oAssto = new L3401Id3oAssto();
    //Original name: L3401-TP-GAR
    private L3401TpGar l3401TpGar = new L3401TpGar();
    //Original name: L3401-TP-RSH
    private String l3401TpRsh = DefaultValues.stringVal(Len.L3401_TP_RSH);
    //Original name: L3401-TP-INVST
    private L3401TpInvst l3401TpInvst = new L3401TpInvst();
    //Original name: L3401-MOD-PAG-GARCOL
    private String l3401ModPagGarcol = DefaultValues.stringVal(Len.L3401_MOD_PAG_GARCOL);
    //Original name: L3401-TP-PER-PRE
    private String l3401TpPerPre = DefaultValues.stringVal(Len.L3401_TP_PER_PRE);
    //Original name: L3401-ETA-AA-1O-ASSTO
    private L3401EtaAa1oAssto l3401EtaAa1oAssto = new L3401EtaAa1oAssto();
    //Original name: L3401-ETA-MM-1O-ASSTO
    private L3401EtaMm1oAssto l3401EtaMm1oAssto = new L3401EtaMm1oAssto();
    //Original name: L3401-ETA-AA-2O-ASSTO
    private L3401EtaAa2oAssto l3401EtaAa2oAssto = new L3401EtaAa2oAssto();
    //Original name: L3401-ETA-MM-2O-ASSTO
    private L3401EtaMm2oAssto l3401EtaMm2oAssto = new L3401EtaMm2oAssto();
    //Original name: L3401-ETA-AA-3O-ASSTO
    private L3401EtaAa3oAssto l3401EtaAa3oAssto = new L3401EtaAa3oAssto();
    //Original name: L3401-ETA-MM-3O-ASSTO
    private L3401EtaMm3oAssto l3401EtaMm3oAssto = new L3401EtaMm3oAssto();
    //Original name: L3401-TP-EMIS-PUR
    private char l3401TpEmisPur = DefaultValues.CHAR_VAL;
    //Original name: L3401-ETA-A-SCAD
    private L3401EtaAScad l3401EtaAScad = new L3401EtaAScad();
    //Original name: L3401-TP-CALC-PRE-PRSTZ
    private String l3401TpCalcPrePrstz = DefaultValues.stringVal(Len.L3401_TP_CALC_PRE_PRSTZ);
    //Original name: L3401-TP-PRE
    private char l3401TpPre = DefaultValues.CHAR_VAL;
    //Original name: L3401-TP-DUR
    private String l3401TpDur = DefaultValues.stringVal(Len.L3401_TP_DUR);
    //Original name: L3401-DUR-AA
    private L3401DurAa l3401DurAa = new L3401DurAa();
    //Original name: L3401-DUR-MM
    private L3401DurMm l3401DurMm = new L3401DurMm();
    //Original name: L3401-DUR-GG
    private L3401DurGg l3401DurGg = new L3401DurGg();
    //Original name: L3401-NUM-AA-PAG-PRE
    private L3401NumAaPagPre l3401NumAaPagPre = new L3401NumAaPagPre();
    //Original name: L3401-AA-PAG-PRE-UNI
    private L3401AaPagPreUni l3401AaPagPreUni = new L3401AaPagPreUni();
    //Original name: L3401-MM-PAG-PRE-UNI
    private L3401MmPagPreUni l3401MmPagPreUni = new L3401MmPagPreUni();
    //Original name: L3401-FRAZ-INI-EROG-REN
    private L3401FrazIniErogRen l3401FrazIniErogRen = new L3401FrazIniErogRen();
    //Original name: L3401-MM-1O-RAT
    private L3401Mm1oRat l3401Mm1oRat = new L3401Mm1oRat();
    //Original name: L3401-PC-1O-RAT
    private L3401Pc1oRat l3401Pc1oRat = new L3401Pc1oRat();
    //Original name: L3401-TP-PRSTZ-ASSTA
    private String l3401TpPrstzAssta = DefaultValues.stringVal(Len.L3401_TP_PRSTZ_ASSTA);
    //Original name: L3401-DT-END-CARZ
    private L3401DtEndCarz l3401DtEndCarz = new L3401DtEndCarz();
    //Original name: L3401-PC-RIP-PRE
    private L3401PcRipPre l3401PcRipPre = new L3401PcRipPre();
    //Original name: L3401-COD-FND
    private String l3401CodFnd = DefaultValues.stringVal(Len.L3401_COD_FND);
    //Original name: L3401-AA-REN-CER
    private String l3401AaRenCer = DefaultValues.stringVal(Len.L3401_AA_REN_CER);
    //Original name: L3401-PC-REVRSB
    private L3401PcRevrsb l3401PcRevrsb = new L3401PcRevrsb();
    //Original name: L3401-TP-PC-RIP
    private String l3401TpPcRip = DefaultValues.stringVal(Len.L3401_TP_PC_RIP);
    //Original name: L3401-PC-OPZ
    private L3401PcOpz l3401PcOpz = new L3401PcOpz();
    //Original name: L3401-TP-IAS
    private String l3401TpIas = DefaultValues.stringVal(Len.L3401_TP_IAS);
    //Original name: L3401-TP-STAB
    private String l3401TpStab = DefaultValues.stringVal(Len.L3401_TP_STAB);
    //Original name: L3401-TP-ADEG-PRE
    private char l3401TpAdegPre = DefaultValues.CHAR_VAL;
    //Original name: L3401-DT-VARZ-TP-IAS
    private L3401DtVarzTpIas l3401DtVarzTpIas = new L3401DtVarzTpIas();
    //Original name: L3401-FRAZ-DECR-CPT
    private L3401FrazDecrCpt l3401FrazDecrCpt = new L3401FrazDecrCpt();
    //Original name: L3401-COD-TRAT-RIASS
    private String l3401CodTratRiass = DefaultValues.stringVal(Len.L3401_COD_TRAT_RIASS);
    //Original name: L3401-TP-DT-EMIS-RIASS
    private String l3401TpDtEmisRiass = DefaultValues.stringVal(Len.L3401_TP_DT_EMIS_RIASS);
    //Original name: L3401-TP-CESS-RIASS
    private String l3401TpCessRiass = DefaultValues.stringVal(Len.L3401_TP_CESS_RIASS);
    //Original name: L3401-DS-RIGA
    private long l3401DsRiga = DefaultValues.LONG_VAL;
    //Original name: L3401-DS-OPER-SQL
    private char l3401DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: L3401-DS-VER
    private int l3401DsVer = DefaultValues.INT_VAL;
    //Original name: L3401-DS-TS-INI-CPTZ
    private long l3401DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: L3401-DS-TS-END-CPTZ
    private long l3401DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: L3401-DS-UTENTE
    private String l3401DsUtente = DefaultValues.stringVal(Len.L3401_DS_UTENTE);
    //Original name: L3401-DS-STATO-ELAB
    private char l3401DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: L3401-AA-STAB
    private int l3401AaStab = DefaultValues.INT_VAL;
    //Original name: L3401-TS-STAB-LIMITATA
    private L3401TsStabLimitata l3401TsStabLimitata = new L3401TsStabLimitata();
    //Original name: L3401-DT-PRESC
    private int l3401DtPresc = DefaultValues.INT_VAL;
    //Original name: L3401-RSH-INVST
    private char l3401RshInvst = DefaultValues.CHAR_VAL;
    //Original name: L3401-TP-RAMO-BILA
    private String l3401TpRamoBila = DefaultValues.stringVal(Len.L3401_TP_RAMO_BILA);

    //==== METHODS ====
    public void setLdbv3401GarOutputBytes(byte[] buffer) {
        setLdbv3401GarOutputBytes(buffer, 1);
    }

    public void setLdbv3401GarOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        l3401IdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3401_ID_GAR, 0);
        position += Len.L3401_ID_GAR;
        l3401IdAdes.setL3401IdAdesFromBuffer(buffer, position);
        position += L3401IdAdes.Len.L3401_ID_ADES;
        l3401IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3401_ID_POLI, 0);
        position += Len.L3401_ID_POLI;
        l3401IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3401_ID_MOVI_CRZ, 0);
        position += Len.L3401_ID_MOVI_CRZ;
        l3401IdMoviChiu.setL3401IdMoviChiuFromBuffer(buffer, position);
        position += L3401IdMoviChiu.Len.L3401_ID_MOVI_CHIU;
        l3401DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3401_DT_INI_EFF, 0);
        position += Len.L3401_DT_INI_EFF;
        l3401DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3401_DT_END_EFF, 0);
        position += Len.L3401_DT_END_EFF;
        l3401CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3401_COD_COMP_ANIA, 0);
        position += Len.L3401_COD_COMP_ANIA;
        l3401IbOgg = MarshalByte.readString(buffer, position, Len.L3401_IB_OGG);
        position += Len.L3401_IB_OGG;
        l3401DtDecor.setL3401DtDecorFromBuffer(buffer, position);
        position += L3401DtDecor.Len.L3401_DT_DECOR;
        l3401DtScad.setL3401DtScadFromBuffer(buffer, position);
        position += L3401DtScad.Len.L3401_DT_SCAD;
        l3401CodSez = MarshalByte.readString(buffer, position, Len.L3401_COD_SEZ);
        position += Len.L3401_COD_SEZ;
        l3401CodTari = MarshalByte.readString(buffer, position, Len.L3401_COD_TARI);
        position += Len.L3401_COD_TARI;
        l3401RamoBila = MarshalByte.readString(buffer, position, Len.L3401_RAMO_BILA);
        position += Len.L3401_RAMO_BILA;
        l3401DtIniValTar.setL3401DtIniValTarFromBuffer(buffer, position);
        position += L3401DtIniValTar.Len.L3401_DT_INI_VAL_TAR;
        l3401Id1oAssto.setL3401Id1oAsstoFromBuffer(buffer, position);
        position += L3401Id1oAssto.Len.L3401_ID1O_ASSTO;
        l3401Id2oAssto.setL3401Id2oAsstoFromBuffer(buffer, position);
        position += L3401Id2oAssto.Len.L3401_ID2O_ASSTO;
        l3401Id3oAssto.setL3401Id3oAsstoFromBuffer(buffer, position);
        position += L3401Id3oAssto.Len.L3401_ID3O_ASSTO;
        l3401TpGar.setL3401TpGarFromBuffer(buffer, position);
        position += L3401TpGar.Len.L3401_TP_GAR;
        l3401TpRsh = MarshalByte.readString(buffer, position, Len.L3401_TP_RSH);
        position += Len.L3401_TP_RSH;
        l3401TpInvst.setL3401TpInvstFromBuffer(buffer, position);
        position += L3401TpInvst.Len.L3401_TP_INVST;
        l3401ModPagGarcol = MarshalByte.readString(buffer, position, Len.L3401_MOD_PAG_GARCOL);
        position += Len.L3401_MOD_PAG_GARCOL;
        l3401TpPerPre = MarshalByte.readString(buffer, position, Len.L3401_TP_PER_PRE);
        position += Len.L3401_TP_PER_PRE;
        l3401EtaAa1oAssto.setL3401EtaAa1oAsstoFromBuffer(buffer, position);
        position += L3401EtaAa1oAssto.Len.L3401_ETA_AA1O_ASSTO;
        l3401EtaMm1oAssto.setL3401EtaMm1oAsstoFromBuffer(buffer, position);
        position += L3401EtaMm1oAssto.Len.L3401_ETA_MM1O_ASSTO;
        l3401EtaAa2oAssto.setL3401EtaAa2oAsstoFromBuffer(buffer, position);
        position += L3401EtaAa2oAssto.Len.L3401_ETA_AA2O_ASSTO;
        l3401EtaMm2oAssto.setL3401EtaMm2oAsstoFromBuffer(buffer, position);
        position += L3401EtaMm2oAssto.Len.L3401_ETA_MM2O_ASSTO;
        l3401EtaAa3oAssto.setL3401EtaAa3oAsstoFromBuffer(buffer, position);
        position += L3401EtaAa3oAssto.Len.L3401_ETA_AA3O_ASSTO;
        l3401EtaMm3oAssto.setL3401EtaMm3oAsstoFromBuffer(buffer, position);
        position += L3401EtaMm3oAssto.Len.L3401_ETA_MM3O_ASSTO;
        l3401TpEmisPur = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3401EtaAScad.setL3401EtaAScadFromBuffer(buffer, position);
        position += L3401EtaAScad.Len.L3401_ETA_A_SCAD;
        l3401TpCalcPrePrstz = MarshalByte.readString(buffer, position, Len.L3401_TP_CALC_PRE_PRSTZ);
        position += Len.L3401_TP_CALC_PRE_PRSTZ;
        l3401TpPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3401TpDur = MarshalByte.readString(buffer, position, Len.L3401_TP_DUR);
        position += Len.L3401_TP_DUR;
        l3401DurAa.setL3401DurAaFromBuffer(buffer, position);
        position += L3401DurAa.Len.L3401_DUR_AA;
        l3401DurMm.setL3401DurMmFromBuffer(buffer, position);
        position += L3401DurMm.Len.L3401_DUR_MM;
        l3401DurGg.setL3401DurGgFromBuffer(buffer, position);
        position += L3401DurGg.Len.L3401_DUR_GG;
        l3401NumAaPagPre.setL3401NumAaPagPreFromBuffer(buffer, position);
        position += L3401NumAaPagPre.Len.L3401_NUM_AA_PAG_PRE;
        l3401AaPagPreUni.setL3401AaPagPreUniFromBuffer(buffer, position);
        position += L3401AaPagPreUni.Len.L3401_AA_PAG_PRE_UNI;
        l3401MmPagPreUni.setL3401MmPagPreUniFromBuffer(buffer, position);
        position += L3401MmPagPreUni.Len.L3401_MM_PAG_PRE_UNI;
        l3401FrazIniErogRen.setL3401FrazIniErogRenFromBuffer(buffer, position);
        position += L3401FrazIniErogRen.Len.L3401_FRAZ_INI_EROG_REN;
        l3401Mm1oRat.setL3401Mm1oRatFromBuffer(buffer, position);
        position += L3401Mm1oRat.Len.L3401_MM1O_RAT;
        l3401Pc1oRat.setL3401Pc1oRatFromBuffer(buffer, position);
        position += L3401Pc1oRat.Len.L3401_PC1O_RAT;
        l3401TpPrstzAssta = MarshalByte.readString(buffer, position, Len.L3401_TP_PRSTZ_ASSTA);
        position += Len.L3401_TP_PRSTZ_ASSTA;
        l3401DtEndCarz.setL3401DtEndCarzFromBuffer(buffer, position);
        position += L3401DtEndCarz.Len.L3401_DT_END_CARZ;
        l3401PcRipPre.setL3401PcRipPreFromBuffer(buffer, position);
        position += L3401PcRipPre.Len.L3401_PC_RIP_PRE;
        l3401CodFnd = MarshalByte.readString(buffer, position, Len.L3401_COD_FND);
        position += Len.L3401_COD_FND;
        l3401AaRenCer = MarshalByte.readString(buffer, position, Len.L3401_AA_REN_CER);
        position += Len.L3401_AA_REN_CER;
        l3401PcRevrsb.setL3401PcRevrsbFromBuffer(buffer, position);
        position += L3401PcRevrsb.Len.L3401_PC_REVRSB;
        l3401TpPcRip = MarshalByte.readString(buffer, position, Len.L3401_TP_PC_RIP);
        position += Len.L3401_TP_PC_RIP;
        l3401PcOpz.setL3401PcOpzFromBuffer(buffer, position);
        position += L3401PcOpz.Len.L3401_PC_OPZ;
        l3401TpIas = MarshalByte.readString(buffer, position, Len.L3401_TP_IAS);
        position += Len.L3401_TP_IAS;
        l3401TpStab = MarshalByte.readString(buffer, position, Len.L3401_TP_STAB);
        position += Len.L3401_TP_STAB;
        l3401TpAdegPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3401DtVarzTpIas.setL3401DtVarzTpIasFromBuffer(buffer, position);
        position += L3401DtVarzTpIas.Len.L3401_DT_VARZ_TP_IAS;
        l3401FrazDecrCpt.setL3401FrazDecrCptFromBuffer(buffer, position);
        position += L3401FrazDecrCpt.Len.L3401_FRAZ_DECR_CPT;
        l3401CodTratRiass = MarshalByte.readString(buffer, position, Len.L3401_COD_TRAT_RIASS);
        position += Len.L3401_COD_TRAT_RIASS;
        l3401TpDtEmisRiass = MarshalByte.readString(buffer, position, Len.L3401_TP_DT_EMIS_RIASS);
        position += Len.L3401_TP_DT_EMIS_RIASS;
        l3401TpCessRiass = MarshalByte.readString(buffer, position, Len.L3401_TP_CESS_RIASS);
        position += Len.L3401_TP_CESS_RIASS;
        l3401DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L3401_DS_RIGA, 0);
        position += Len.L3401_DS_RIGA;
        l3401DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3401DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3401_DS_VER, 0);
        position += Len.L3401_DS_VER;
        l3401DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L3401_DS_TS_INI_CPTZ, 0);
        position += Len.L3401_DS_TS_INI_CPTZ;
        l3401DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L3401_DS_TS_END_CPTZ, 0);
        position += Len.L3401_DS_TS_END_CPTZ;
        l3401DsUtente = MarshalByte.readString(buffer, position, Len.L3401_DS_UTENTE);
        position += Len.L3401_DS_UTENTE;
        l3401DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3401AaStab = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3401_AA_STAB, 0);
        position += Len.L3401_AA_STAB;
        l3401TsStabLimitata.setL3401TsStabLimitataFromBuffer(buffer, position);
        position += L3401TsStabLimitata.Len.L3401_TS_STAB_LIMITATA;
        l3401DtPresc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3401_DT_PRESC, 0);
        position += Len.L3401_DT_PRESC;
        l3401RshInvst = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3401TpRamoBila = MarshalByte.readString(buffer, position, Len.L3401_TP_RAMO_BILA);
    }

    public byte[] getLdbv3401GarOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, l3401IdGar, Len.Int.L3401_ID_GAR, 0);
        position += Len.L3401_ID_GAR;
        l3401IdAdes.getL3401IdAdesAsBuffer(buffer, position);
        position += L3401IdAdes.Len.L3401_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, l3401IdPoli, Len.Int.L3401_ID_POLI, 0);
        position += Len.L3401_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, l3401IdMoviCrz, Len.Int.L3401_ID_MOVI_CRZ, 0);
        position += Len.L3401_ID_MOVI_CRZ;
        l3401IdMoviChiu.getL3401IdMoviChiuAsBuffer(buffer, position);
        position += L3401IdMoviChiu.Len.L3401_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, l3401DtIniEff, Len.Int.L3401_DT_INI_EFF, 0);
        position += Len.L3401_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, l3401DtEndEff, Len.Int.L3401_DT_END_EFF, 0);
        position += Len.L3401_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, l3401CodCompAnia, Len.Int.L3401_COD_COMP_ANIA, 0);
        position += Len.L3401_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, l3401IbOgg, Len.L3401_IB_OGG);
        position += Len.L3401_IB_OGG;
        l3401DtDecor.getL3401DtDecorAsBuffer(buffer, position);
        position += L3401DtDecor.Len.L3401_DT_DECOR;
        l3401DtScad.getL3401DtScadAsBuffer(buffer, position);
        position += L3401DtScad.Len.L3401_DT_SCAD;
        MarshalByte.writeString(buffer, position, l3401CodSez, Len.L3401_COD_SEZ);
        position += Len.L3401_COD_SEZ;
        MarshalByte.writeString(buffer, position, l3401CodTari, Len.L3401_COD_TARI);
        position += Len.L3401_COD_TARI;
        MarshalByte.writeString(buffer, position, l3401RamoBila, Len.L3401_RAMO_BILA);
        position += Len.L3401_RAMO_BILA;
        l3401DtIniValTar.getL3401DtIniValTarAsBuffer(buffer, position);
        position += L3401DtIniValTar.Len.L3401_DT_INI_VAL_TAR;
        l3401Id1oAssto.getL3401Id1oAsstoAsBuffer(buffer, position);
        position += L3401Id1oAssto.Len.L3401_ID1O_ASSTO;
        l3401Id2oAssto.getL3401Id2oAsstoAsBuffer(buffer, position);
        position += L3401Id2oAssto.Len.L3401_ID2O_ASSTO;
        l3401Id3oAssto.getL3401Id3oAsstoAsBuffer(buffer, position);
        position += L3401Id3oAssto.Len.L3401_ID3O_ASSTO;
        l3401TpGar.getL3401TpGarAsBuffer(buffer, position);
        position += L3401TpGar.Len.L3401_TP_GAR;
        MarshalByte.writeString(buffer, position, l3401TpRsh, Len.L3401_TP_RSH);
        position += Len.L3401_TP_RSH;
        l3401TpInvst.getL3401TpInvstAsBuffer(buffer, position);
        position += L3401TpInvst.Len.L3401_TP_INVST;
        MarshalByte.writeString(buffer, position, l3401ModPagGarcol, Len.L3401_MOD_PAG_GARCOL);
        position += Len.L3401_MOD_PAG_GARCOL;
        MarshalByte.writeString(buffer, position, l3401TpPerPre, Len.L3401_TP_PER_PRE);
        position += Len.L3401_TP_PER_PRE;
        l3401EtaAa1oAssto.getL3401EtaAa1oAsstoAsBuffer(buffer, position);
        position += L3401EtaAa1oAssto.Len.L3401_ETA_AA1O_ASSTO;
        l3401EtaMm1oAssto.getL3401EtaMm1oAsstoAsBuffer(buffer, position);
        position += L3401EtaMm1oAssto.Len.L3401_ETA_MM1O_ASSTO;
        l3401EtaAa2oAssto.getL3401EtaAa2oAsstoAsBuffer(buffer, position);
        position += L3401EtaAa2oAssto.Len.L3401_ETA_AA2O_ASSTO;
        l3401EtaMm2oAssto.getL3401EtaMm2oAsstoAsBuffer(buffer, position);
        position += L3401EtaMm2oAssto.Len.L3401_ETA_MM2O_ASSTO;
        l3401EtaAa3oAssto.getL3401EtaAa3oAsstoAsBuffer(buffer, position);
        position += L3401EtaAa3oAssto.Len.L3401_ETA_AA3O_ASSTO;
        l3401EtaMm3oAssto.getL3401EtaMm3oAsstoAsBuffer(buffer, position);
        position += L3401EtaMm3oAssto.Len.L3401_ETA_MM3O_ASSTO;
        MarshalByte.writeChar(buffer, position, l3401TpEmisPur);
        position += Types.CHAR_SIZE;
        l3401EtaAScad.getL3401EtaAScadAsBuffer(buffer, position);
        position += L3401EtaAScad.Len.L3401_ETA_A_SCAD;
        MarshalByte.writeString(buffer, position, l3401TpCalcPrePrstz, Len.L3401_TP_CALC_PRE_PRSTZ);
        position += Len.L3401_TP_CALC_PRE_PRSTZ;
        MarshalByte.writeChar(buffer, position, l3401TpPre);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, l3401TpDur, Len.L3401_TP_DUR);
        position += Len.L3401_TP_DUR;
        l3401DurAa.getL3401DurAaAsBuffer(buffer, position);
        position += L3401DurAa.Len.L3401_DUR_AA;
        l3401DurMm.getL3401DurMmAsBuffer(buffer, position);
        position += L3401DurMm.Len.L3401_DUR_MM;
        l3401DurGg.getL3401DurGgAsBuffer(buffer, position);
        position += L3401DurGg.Len.L3401_DUR_GG;
        l3401NumAaPagPre.getL3401NumAaPagPreAsBuffer(buffer, position);
        position += L3401NumAaPagPre.Len.L3401_NUM_AA_PAG_PRE;
        l3401AaPagPreUni.getL3401AaPagPreUniAsBuffer(buffer, position);
        position += L3401AaPagPreUni.Len.L3401_AA_PAG_PRE_UNI;
        l3401MmPagPreUni.getL3401MmPagPreUniAsBuffer(buffer, position);
        position += L3401MmPagPreUni.Len.L3401_MM_PAG_PRE_UNI;
        l3401FrazIniErogRen.getL3401FrazIniErogRenAsBuffer(buffer, position);
        position += L3401FrazIniErogRen.Len.L3401_FRAZ_INI_EROG_REN;
        l3401Mm1oRat.getL3401Mm1oRatAsBuffer(buffer, position);
        position += L3401Mm1oRat.Len.L3401_MM1O_RAT;
        l3401Pc1oRat.getL3401Pc1oRatAsBuffer(buffer, position);
        position += L3401Pc1oRat.Len.L3401_PC1O_RAT;
        MarshalByte.writeString(buffer, position, l3401TpPrstzAssta, Len.L3401_TP_PRSTZ_ASSTA);
        position += Len.L3401_TP_PRSTZ_ASSTA;
        l3401DtEndCarz.getL3401DtEndCarzAsBuffer(buffer, position);
        position += L3401DtEndCarz.Len.L3401_DT_END_CARZ;
        l3401PcRipPre.getL3401PcRipPreAsBuffer(buffer, position);
        position += L3401PcRipPre.Len.L3401_PC_RIP_PRE;
        MarshalByte.writeString(buffer, position, l3401CodFnd, Len.L3401_COD_FND);
        position += Len.L3401_COD_FND;
        MarshalByte.writeString(buffer, position, l3401AaRenCer, Len.L3401_AA_REN_CER);
        position += Len.L3401_AA_REN_CER;
        l3401PcRevrsb.getL3401PcRevrsbAsBuffer(buffer, position);
        position += L3401PcRevrsb.Len.L3401_PC_REVRSB;
        MarshalByte.writeString(buffer, position, l3401TpPcRip, Len.L3401_TP_PC_RIP);
        position += Len.L3401_TP_PC_RIP;
        l3401PcOpz.getL3401PcOpzAsBuffer(buffer, position);
        position += L3401PcOpz.Len.L3401_PC_OPZ;
        MarshalByte.writeString(buffer, position, l3401TpIas, Len.L3401_TP_IAS);
        position += Len.L3401_TP_IAS;
        MarshalByte.writeString(buffer, position, l3401TpStab, Len.L3401_TP_STAB);
        position += Len.L3401_TP_STAB;
        MarshalByte.writeChar(buffer, position, l3401TpAdegPre);
        position += Types.CHAR_SIZE;
        l3401DtVarzTpIas.getL3401DtVarzTpIasAsBuffer(buffer, position);
        position += L3401DtVarzTpIas.Len.L3401_DT_VARZ_TP_IAS;
        l3401FrazDecrCpt.getL3401FrazDecrCptAsBuffer(buffer, position);
        position += L3401FrazDecrCpt.Len.L3401_FRAZ_DECR_CPT;
        MarshalByte.writeString(buffer, position, l3401CodTratRiass, Len.L3401_COD_TRAT_RIASS);
        position += Len.L3401_COD_TRAT_RIASS;
        MarshalByte.writeString(buffer, position, l3401TpDtEmisRiass, Len.L3401_TP_DT_EMIS_RIASS);
        position += Len.L3401_TP_DT_EMIS_RIASS;
        MarshalByte.writeString(buffer, position, l3401TpCessRiass, Len.L3401_TP_CESS_RIASS);
        position += Len.L3401_TP_CESS_RIASS;
        MarshalByte.writeLongAsPacked(buffer, position, l3401DsRiga, Len.Int.L3401_DS_RIGA, 0);
        position += Len.L3401_DS_RIGA;
        MarshalByte.writeChar(buffer, position, l3401DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, l3401DsVer, Len.Int.L3401_DS_VER, 0);
        position += Len.L3401_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, l3401DsTsIniCptz, Len.Int.L3401_DS_TS_INI_CPTZ, 0);
        position += Len.L3401_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, l3401DsTsEndCptz, Len.Int.L3401_DS_TS_END_CPTZ, 0);
        position += Len.L3401_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, l3401DsUtente, Len.L3401_DS_UTENTE);
        position += Len.L3401_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, l3401DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, l3401AaStab, Len.Int.L3401_AA_STAB, 0);
        position += Len.L3401_AA_STAB;
        l3401TsStabLimitata.getL3401TsStabLimitataAsBuffer(buffer, position);
        position += L3401TsStabLimitata.Len.L3401_TS_STAB_LIMITATA;
        MarshalByte.writeIntAsPacked(buffer, position, l3401DtPresc, Len.Int.L3401_DT_PRESC, 0);
        position += Len.L3401_DT_PRESC;
        MarshalByte.writeChar(buffer, position, l3401RshInvst);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, l3401TpRamoBila, Len.L3401_TP_RAMO_BILA);
        return buffer;
    }

    public void setL3401IdGar(int l3401IdGar) {
        this.l3401IdGar = l3401IdGar;
    }

    public int getL3401IdGar() {
        return this.l3401IdGar;
    }

    public void setL3401IdPoli(int l3401IdPoli) {
        this.l3401IdPoli = l3401IdPoli;
    }

    public int getL3401IdPoli() {
        return this.l3401IdPoli;
    }

    public void setL3401IdMoviCrz(int l3401IdMoviCrz) {
        this.l3401IdMoviCrz = l3401IdMoviCrz;
    }

    public int getL3401IdMoviCrz() {
        return this.l3401IdMoviCrz;
    }

    public void setL3401DtIniEff(int l3401DtIniEff) {
        this.l3401DtIniEff = l3401DtIniEff;
    }

    public int getL3401DtIniEff() {
        return this.l3401DtIniEff;
    }

    public void setL3401DtEndEff(int l3401DtEndEff) {
        this.l3401DtEndEff = l3401DtEndEff;
    }

    public int getL3401DtEndEff() {
        return this.l3401DtEndEff;
    }

    public void setL3401CodCompAnia(int l3401CodCompAnia) {
        this.l3401CodCompAnia = l3401CodCompAnia;
    }

    public int getL3401CodCompAnia() {
        return this.l3401CodCompAnia;
    }

    public void setL3401IbOgg(String l3401IbOgg) {
        this.l3401IbOgg = Functions.subString(l3401IbOgg, Len.L3401_IB_OGG);
    }

    public String getL3401IbOgg() {
        return this.l3401IbOgg;
    }

    public void setL3401CodSez(String l3401CodSez) {
        this.l3401CodSez = Functions.subString(l3401CodSez, Len.L3401_COD_SEZ);
    }

    public String getL3401CodSez() {
        return this.l3401CodSez;
    }

    public String getL3401CodSezFormatted() {
        return Functions.padBlanks(getL3401CodSez(), Len.L3401_COD_SEZ);
    }

    public void setL3401CodTari(String l3401CodTari) {
        this.l3401CodTari = Functions.subString(l3401CodTari, Len.L3401_COD_TARI);
    }

    public String getL3401CodTari() {
        return this.l3401CodTari;
    }

    public void setL3401RamoBila(String l3401RamoBila) {
        this.l3401RamoBila = Functions.subString(l3401RamoBila, Len.L3401_RAMO_BILA);
    }

    public String getL3401RamoBila() {
        return this.l3401RamoBila;
    }

    public String getL3411RamoBilaFormatted() {
        return Functions.padBlanks(getL3401RamoBila(), Len.L3401_RAMO_BILA);
    }

    public void setL3401TpRsh(String l3401TpRsh) {
        this.l3401TpRsh = Functions.subString(l3401TpRsh, Len.L3401_TP_RSH);
    }

    public String getL3401TpRsh() {
        return this.l3401TpRsh;
    }

    public String getL3401TpRshFormatted() {
        return Functions.padBlanks(getL3401TpRsh(), Len.L3401_TP_RSH);
    }

    public void setL3401ModPagGarcol(String l3401ModPagGarcol) {
        this.l3401ModPagGarcol = Functions.subString(l3401ModPagGarcol, Len.L3401_MOD_PAG_GARCOL);
    }

    public String getL3401ModPagGarcol() {
        return this.l3401ModPagGarcol;
    }

    public String getL3401ModPagGarcolFormatted() {
        return Functions.padBlanks(getL3401ModPagGarcol(), Len.L3401_MOD_PAG_GARCOL);
    }

    public void setL3401TpPerPre(String l3401TpPerPre) {
        this.l3401TpPerPre = Functions.subString(l3401TpPerPre, Len.L3401_TP_PER_PRE);
    }

    public String getL3401TpPerPre() {
        return this.l3401TpPerPre;
    }

    public String getL3401TpPerPreFormatted() {
        return Functions.padBlanks(getL3401TpPerPre(), Len.L3401_TP_PER_PRE);
    }

    public void setL3401TpEmisPur(char l3401TpEmisPur) {
        this.l3401TpEmisPur = l3401TpEmisPur;
    }

    public char getL3401TpEmisPur() {
        return this.l3401TpEmisPur;
    }

    public void setL3401TpCalcPrePrstz(String l3401TpCalcPrePrstz) {
        this.l3401TpCalcPrePrstz = Functions.subString(l3401TpCalcPrePrstz, Len.L3401_TP_CALC_PRE_PRSTZ);
    }

    public String getL3401TpCalcPrePrstz() {
        return this.l3401TpCalcPrePrstz;
    }

    public String getL3401TpCalcPrePrstzFormatted() {
        return Functions.padBlanks(getL3401TpCalcPrePrstz(), Len.L3401_TP_CALC_PRE_PRSTZ);
    }

    public void setL3401TpPre(char l3401TpPre) {
        this.l3401TpPre = l3401TpPre;
    }

    public char getL3401TpPre() {
        return this.l3401TpPre;
    }

    public void setL3401TpDur(String l3401TpDur) {
        this.l3401TpDur = Functions.subString(l3401TpDur, Len.L3401_TP_DUR);
    }

    public String getL3401TpDur() {
        return this.l3401TpDur;
    }

    public String getL3401TpDurFormatted() {
        return Functions.padBlanks(getL3401TpDur(), Len.L3401_TP_DUR);
    }

    public void setL3401TpPrstzAssta(String l3401TpPrstzAssta) {
        this.l3401TpPrstzAssta = Functions.subString(l3401TpPrstzAssta, Len.L3401_TP_PRSTZ_ASSTA);
    }

    public String getL3401TpPrstzAssta() {
        return this.l3401TpPrstzAssta;
    }

    public String getL3401TpPrstzAsstaFormatted() {
        return Functions.padBlanks(getL3401TpPrstzAssta(), Len.L3401_TP_PRSTZ_ASSTA);
    }

    public void setL3401CodFnd(String l3401CodFnd) {
        this.l3401CodFnd = Functions.subString(l3401CodFnd, Len.L3401_COD_FND);
    }

    public String getL3401CodFnd() {
        return this.l3401CodFnd;
    }

    public String getL3401CodFndFormatted() {
        return Functions.padBlanks(getL3401CodFnd(), Len.L3401_COD_FND);
    }

    public void setL3401AaRenCer(String l3401AaRenCer) {
        this.l3401AaRenCer = Functions.subString(l3401AaRenCer, Len.L3401_AA_REN_CER);
    }

    public String getL3401AaRenCer() {
        return this.l3401AaRenCer;
    }

    public String getL3401AaRenCerFormatted() {
        return Functions.padBlanks(getL3401AaRenCer(), Len.L3401_AA_REN_CER);
    }

    public void setL3401TpPcRip(String l3401TpPcRip) {
        this.l3401TpPcRip = Functions.subString(l3401TpPcRip, Len.L3401_TP_PC_RIP);
    }

    public String getL3401TpPcRip() {
        return this.l3401TpPcRip;
    }

    public String getL3401TpPcRipFormatted() {
        return Functions.padBlanks(getL3401TpPcRip(), Len.L3401_TP_PC_RIP);
    }

    public void setL3401TpIas(String l3401TpIas) {
        this.l3401TpIas = Functions.subString(l3401TpIas, Len.L3401_TP_IAS);
    }

    public String getL3401TpIas() {
        return this.l3401TpIas;
    }

    public String getL3401TpIasFormatted() {
        return Functions.padBlanks(getL3401TpIas(), Len.L3401_TP_IAS);
    }

    public void setL3401TpStab(String l3401TpStab) {
        this.l3401TpStab = Functions.subString(l3401TpStab, Len.L3401_TP_STAB);
    }

    public String getL3401TpStab() {
        return this.l3401TpStab;
    }

    public String getL3401TpStabFormatted() {
        return Functions.padBlanks(getL3401TpStab(), Len.L3401_TP_STAB);
    }

    public void setL3401TpAdegPre(char l3401TpAdegPre) {
        this.l3401TpAdegPre = l3401TpAdegPre;
    }

    public char getL3401TpAdegPre() {
        return this.l3401TpAdegPre;
    }

    public void setL3401CodTratRiass(String l3401CodTratRiass) {
        this.l3401CodTratRiass = Functions.subString(l3401CodTratRiass, Len.L3401_COD_TRAT_RIASS);
    }

    public String getL3401CodTratRiass() {
        return this.l3401CodTratRiass;
    }

    public void setL3401TpDtEmisRiass(String l3401TpDtEmisRiass) {
        this.l3401TpDtEmisRiass = Functions.subString(l3401TpDtEmisRiass, Len.L3401_TP_DT_EMIS_RIASS);
    }

    public String getL3401TpDtEmisRiass() {
        return this.l3401TpDtEmisRiass;
    }

    public void setL3401TpCessRiass(String l3401TpCessRiass) {
        this.l3401TpCessRiass = Functions.subString(l3401TpCessRiass, Len.L3401_TP_CESS_RIASS);
    }

    public String getL3401TpCessRiass() {
        return this.l3401TpCessRiass;
    }

    public void setL3401DsRiga(long l3401DsRiga) {
        this.l3401DsRiga = l3401DsRiga;
    }

    public long getL3401DsRiga() {
        return this.l3401DsRiga;
    }

    public void setL3401DsOperSql(char l3401DsOperSql) {
        this.l3401DsOperSql = l3401DsOperSql;
    }

    public char getL3401DsOperSql() {
        return this.l3401DsOperSql;
    }

    public void setL3401DsVer(int l3401DsVer) {
        this.l3401DsVer = l3401DsVer;
    }

    public int getL3401DsVer() {
        return this.l3401DsVer;
    }

    public void setL3401DsTsIniCptz(long l3401DsTsIniCptz) {
        this.l3401DsTsIniCptz = l3401DsTsIniCptz;
    }

    public long getL3401DsTsIniCptz() {
        return this.l3401DsTsIniCptz;
    }

    public void setL3401DsTsEndCptz(long l3401DsTsEndCptz) {
        this.l3401DsTsEndCptz = l3401DsTsEndCptz;
    }

    public long getL3401DsTsEndCptz() {
        return this.l3401DsTsEndCptz;
    }

    public void setL3401DsUtente(String l3401DsUtente) {
        this.l3401DsUtente = Functions.subString(l3401DsUtente, Len.L3401_DS_UTENTE);
    }

    public String getL3401DsUtente() {
        return this.l3401DsUtente;
    }

    public void setL3401DsStatoElab(char l3401DsStatoElab) {
        this.l3401DsStatoElab = l3401DsStatoElab;
    }

    public char getL3401DsStatoElab() {
        return this.l3401DsStatoElab;
    }

    public void setL3401AaStab(int l3401AaStab) {
        this.l3401AaStab = l3401AaStab;
    }

    public int getL3401AaStab() {
        return this.l3401AaStab;
    }

    public void setL3401DtPresc(int l3401DtPresc) {
        this.l3401DtPresc = l3401DtPresc;
    }

    public int getL3401DtPresc() {
        return this.l3401DtPresc;
    }

    public void setL3401RshInvst(char l3401RshInvst) {
        this.l3401RshInvst = l3401RshInvst;
    }

    public char getL3401RshInvst() {
        return this.l3401RshInvst;
    }

    public void setL3401TpRamoBila(String l3401TpRamoBila) {
        this.l3401TpRamoBila = Functions.subString(l3401TpRamoBila, Len.L3401_TP_RAMO_BILA);
    }

    public String getL3401TpRamoBila() {
        return this.l3401TpRamoBila;
    }

    public L3401AaPagPreUni getL3401AaPagPreUni() {
        return l3401AaPagPreUni;
    }

    public L3401DtDecor getL3401DtDecor() {
        return l3401DtDecor;
    }

    public L3401DtEndCarz getL3401DtEndCarz() {
        return l3401DtEndCarz;
    }

    public L3401DtIniValTar getL3401DtIniValTar() {
        return l3401DtIniValTar;
    }

    public L3401DtScad getL3401DtScad() {
        return l3401DtScad;
    }

    public L3401DtVarzTpIas getL3401DtVarzTpIas() {
        return l3401DtVarzTpIas;
    }

    public L3401DurAa getL3401DurAa() {
        return l3401DurAa;
    }

    public L3401DurGg getL3401DurGg() {
        return l3401DurGg;
    }

    public L3401DurMm getL3401DurMm() {
        return l3401DurMm;
    }

    public L3401EtaAScad getL3401EtaAScad() {
        return l3401EtaAScad;
    }

    public L3401EtaAa1oAssto getL3401EtaAa1oAssto() {
        return l3401EtaAa1oAssto;
    }

    public L3401EtaAa2oAssto getL3401EtaAa2oAssto() {
        return l3401EtaAa2oAssto;
    }

    public L3401EtaAa3oAssto getL3401EtaAa3oAssto() {
        return l3401EtaAa3oAssto;
    }

    public L3401EtaMm1oAssto getL3401EtaMm1oAssto() {
        return l3401EtaMm1oAssto;
    }

    public L3401EtaMm2oAssto getL3401EtaMm2oAssto() {
        return l3401EtaMm2oAssto;
    }

    public L3401EtaMm3oAssto getL3401EtaMm3oAssto() {
        return l3401EtaMm3oAssto;
    }

    public L3401FrazDecrCpt getL3401FrazDecrCpt() {
        return l3401FrazDecrCpt;
    }

    public L3401FrazIniErogRen getL3401FrazIniErogRen() {
        return l3401FrazIniErogRen;
    }

    public L3401Id1oAssto getL3401Id1oAssto() {
        return l3401Id1oAssto;
    }

    public L3401Id2oAssto getL3401Id2oAssto() {
        return l3401Id2oAssto;
    }

    public L3401Id3oAssto getL3401Id3oAssto() {
        return l3401Id3oAssto;
    }

    public L3401IdAdes getL3401IdAdes() {
        return l3401IdAdes;
    }

    public L3401IdMoviChiu getL3401IdMoviChiu() {
        return l3401IdMoviChiu;
    }

    public L3401Mm1oRat getL3401Mm1oRat() {
        return l3401Mm1oRat;
    }

    public L3401MmPagPreUni getL3401MmPagPreUni() {
        return l3401MmPagPreUni;
    }

    public L3401NumAaPagPre getL3401NumAaPagPre() {
        return l3401NumAaPagPre;
    }

    public L3401Pc1oRat getL3401Pc1oRat() {
        return l3401Pc1oRat;
    }

    public L3401PcOpz getL3401PcOpz() {
        return l3401PcOpz;
    }

    public L3401PcRevrsb getL3401PcRevrsb() {
        return l3401PcRevrsb;
    }

    public L3401PcRipPre getL3401PcRipPre() {
        return l3401PcRipPre;
    }

    public L3401TpGar getL3401TpGar() {
        return l3401TpGar;
    }

    public L3401TpInvst getL3401TpInvst() {
        return l3401TpInvst;
    }

    public L3401TsStabLimitata getL3401TsStabLimitata() {
        return l3401TsStabLimitata;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_ID_GAR = 5;
        public static final int L3401_ID_POLI = 5;
        public static final int L3401_ID_MOVI_CRZ = 5;
        public static final int L3401_DT_INI_EFF = 5;
        public static final int L3401_DT_END_EFF = 5;
        public static final int L3401_COD_COMP_ANIA = 3;
        public static final int L3401_IB_OGG = 40;
        public static final int L3401_COD_SEZ = 12;
        public static final int L3401_COD_TARI = 12;
        public static final int L3401_RAMO_BILA = 12;
        public static final int L3401_TP_RSH = 2;
        public static final int L3401_MOD_PAG_GARCOL = 2;
        public static final int L3401_TP_PER_PRE = 2;
        public static final int L3401_TP_EMIS_PUR = 1;
        public static final int L3401_TP_CALC_PRE_PRSTZ = 2;
        public static final int L3401_TP_PRE = 1;
        public static final int L3401_TP_DUR = 2;
        public static final int L3401_TP_PRSTZ_ASSTA = 2;
        public static final int L3401_COD_FND = 12;
        public static final int L3401_AA_REN_CER = 18;
        public static final int L3401_TP_PC_RIP = 2;
        public static final int L3401_TP_IAS = 2;
        public static final int L3401_TP_STAB = 2;
        public static final int L3401_TP_ADEG_PRE = 1;
        public static final int L3401_COD_TRAT_RIASS = 12;
        public static final int L3401_TP_DT_EMIS_RIASS = 2;
        public static final int L3401_TP_CESS_RIASS = 2;
        public static final int L3401_DS_RIGA = 6;
        public static final int L3401_DS_OPER_SQL = 1;
        public static final int L3401_DS_VER = 5;
        public static final int L3401_DS_TS_INI_CPTZ = 10;
        public static final int L3401_DS_TS_END_CPTZ = 10;
        public static final int L3401_DS_UTENTE = 20;
        public static final int L3401_DS_STATO_ELAB = 1;
        public static final int L3401_AA_STAB = 3;
        public static final int L3401_DT_PRESC = 5;
        public static final int L3401_RSH_INVST = 1;
        public static final int L3401_TP_RAMO_BILA = 2;
        public static final int LDBV3401_GAR_OUTPUT = L3401_ID_GAR + L3401IdAdes.Len.L3401_ID_ADES + L3401_ID_POLI + L3401_ID_MOVI_CRZ + L3401IdMoviChiu.Len.L3401_ID_MOVI_CHIU + L3401_DT_INI_EFF + L3401_DT_END_EFF + L3401_COD_COMP_ANIA + L3401_IB_OGG + L3401DtDecor.Len.L3401_DT_DECOR + L3401DtScad.Len.L3401_DT_SCAD + L3401_COD_SEZ + L3401_COD_TARI + L3401_RAMO_BILA + L3401DtIniValTar.Len.L3401_DT_INI_VAL_TAR + L3401Id1oAssto.Len.L3401_ID1O_ASSTO + L3401Id2oAssto.Len.L3401_ID2O_ASSTO + L3401Id3oAssto.Len.L3401_ID3O_ASSTO + L3401TpGar.Len.L3401_TP_GAR + L3401_TP_RSH + L3401TpInvst.Len.L3401_TP_INVST + L3401_MOD_PAG_GARCOL + L3401_TP_PER_PRE + L3401EtaAa1oAssto.Len.L3401_ETA_AA1O_ASSTO + L3401EtaMm1oAssto.Len.L3401_ETA_MM1O_ASSTO + L3401EtaAa2oAssto.Len.L3401_ETA_AA2O_ASSTO + L3401EtaMm2oAssto.Len.L3401_ETA_MM2O_ASSTO + L3401EtaAa3oAssto.Len.L3401_ETA_AA3O_ASSTO + L3401EtaMm3oAssto.Len.L3401_ETA_MM3O_ASSTO + L3401_TP_EMIS_PUR + L3401EtaAScad.Len.L3401_ETA_A_SCAD + L3401_TP_CALC_PRE_PRSTZ + L3401_TP_PRE + L3401_TP_DUR + L3401DurAa.Len.L3401_DUR_AA + L3401DurMm.Len.L3401_DUR_MM + L3401DurGg.Len.L3401_DUR_GG + L3401NumAaPagPre.Len.L3401_NUM_AA_PAG_PRE + L3401AaPagPreUni.Len.L3401_AA_PAG_PRE_UNI + L3401MmPagPreUni.Len.L3401_MM_PAG_PRE_UNI + L3401FrazIniErogRen.Len.L3401_FRAZ_INI_EROG_REN + L3401Mm1oRat.Len.L3401_MM1O_RAT + L3401Pc1oRat.Len.L3401_PC1O_RAT + L3401_TP_PRSTZ_ASSTA + L3401DtEndCarz.Len.L3401_DT_END_CARZ + L3401PcRipPre.Len.L3401_PC_RIP_PRE + L3401_COD_FND + L3401_AA_REN_CER + L3401PcRevrsb.Len.L3401_PC_REVRSB + L3401_TP_PC_RIP + L3401PcOpz.Len.L3401_PC_OPZ + L3401_TP_IAS + L3401_TP_STAB + L3401_TP_ADEG_PRE + L3401DtVarzTpIas.Len.L3401_DT_VARZ_TP_IAS + L3401FrazDecrCpt.Len.L3401_FRAZ_DECR_CPT + L3401_COD_TRAT_RIASS + L3401_TP_DT_EMIS_RIASS + L3401_TP_CESS_RIASS + L3401_DS_RIGA + L3401_DS_OPER_SQL + L3401_DS_VER + L3401_DS_TS_INI_CPTZ + L3401_DS_TS_END_CPTZ + L3401_DS_UTENTE + L3401_DS_STATO_ELAB + L3401_AA_STAB + L3401TsStabLimitata.Len.L3401_TS_STAB_LIMITATA + L3401_DT_PRESC + L3401_RSH_INVST + L3401_TP_RAMO_BILA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_ID_GAR = 9;
            public static final int L3401_ID_POLI = 9;
            public static final int L3401_ID_MOVI_CRZ = 9;
            public static final int L3401_DT_INI_EFF = 8;
            public static final int L3401_DT_END_EFF = 8;
            public static final int L3401_COD_COMP_ANIA = 5;
            public static final int L3401_DS_RIGA = 10;
            public static final int L3401_DS_VER = 9;
            public static final int L3401_DS_TS_INI_CPTZ = 18;
            public static final int L3401_DS_TS_END_CPTZ = 18;
            public static final int L3401_AA_STAB = 5;
            public static final int L3401_DT_PRESC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
