package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVSTB3<br>
 * Copybook: IDBVSTB3 from copybook IDBVSTB3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvstb3 {

    //==== PROPERTIES ====
    //Original name: STB-DT-INI-EFF-DB
    private String stbDtIniEffDb = DefaultValues.stringVal(Len.STB_DT_INI_EFF_DB);
    //Original name: STB-DT-END-EFF-DB
    private String stbDtEndEffDb = DefaultValues.stringVal(Len.STB_DT_END_EFF_DB);

    //==== METHODS ====
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        this.stbDtIniEffDb = Functions.subString(stbDtIniEffDb, Len.STB_DT_INI_EFF_DB);
    }

    public String getStbDtIniEffDb() {
        return this.stbDtIniEffDb;
    }

    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        this.stbDtEndEffDb = Functions.subString(stbDtEndEffDb, Len.STB_DT_END_EFF_DB);
    }

    public String getStbDtEndEffDb() {
        return this.stbDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int STB_DT_INI_EFF_DB = 10;
        public static final int STB_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
