package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVTLI3<br>
 * Copybook: IDBVTLI3 from copybook IDBVTLI3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvtli3 {

    //==== PROPERTIES ====
    //Original name: TLI-DT-INI-EFF-DB
    private String tliDtIniEffDb = DefaultValues.stringVal(Len.TLI_DT_INI_EFF_DB);
    //Original name: TLI-DT-END-EFF-DB
    private String tliDtEndEffDb = DefaultValues.stringVal(Len.TLI_DT_END_EFF_DB);

    //==== METHODS ====
    public void setTliDtIniEffDb(String tliDtIniEffDb) {
        this.tliDtIniEffDb = Functions.subString(tliDtIniEffDb, Len.TLI_DT_INI_EFF_DB);
    }

    public String getTliDtIniEffDb() {
        return this.tliDtIniEffDb;
    }

    public void setTliDtEndEffDb(String tliDtEndEffDb) {
        this.tliDtEndEffDb = Functions.subString(tliDtEndEffDb, Len.TLI_DT_END_EFF_DB);
    }

    public String getTliDtEndEffDb() {
        return this.tliDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_DT_INI_EFF_DB = 10;
        public static final int TLI_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
