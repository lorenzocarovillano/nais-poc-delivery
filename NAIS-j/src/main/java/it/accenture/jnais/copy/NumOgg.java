package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.NogUltProgr;

/**Original name: NUM-OGG<br>
 * Variable: NUM-OGG from copybook IDBVNOG1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class NumOgg {

    //==== PROPERTIES ====
    //Original name: NOG-COD-COMPAGNIA-ANIA
    private int nogCodCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: NOG-FORMA-ASSICURATIVA
    private String nogFormaAssicurativa = DefaultValues.stringVal(Len.NOG_FORMA_ASSICURATIVA);
    //Original name: NOG-COD-OGGETTO
    private String nogCodOggetto = DefaultValues.stringVal(Len.NOG_COD_OGGETTO);
    //Original name: NOG-TIPO-OGGETTO
    private String nogTipoOggetto = DefaultValues.stringVal(Len.NOG_TIPO_OGGETTO);
    //Original name: NOG-ULT-PROGR
    private NogUltProgr nogUltProgr = new NogUltProgr();
    //Original name: NOG-DESC-OGGETTO-LEN
    private short nogDescOggettoLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: NOG-DESC-OGGETTO
    private String nogDescOggetto = DefaultValues.stringVal(Len.NOG_DESC_OGGETTO);

    //==== METHODS ====
    public void setNumOggFormatted(String data) {
        byte[] buffer = new byte[Len.NUM_OGG];
        MarshalByte.writeString(buffer, 1, data, Len.NUM_OGG);
        setNumOggBytes(buffer, 1);
    }

    public String getNumOggFormatted() {
        return MarshalByteExt.bufferToStr(getNumOggBytes());
    }

    public byte[] getNumOggBytes() {
        byte[] buffer = new byte[Len.NUM_OGG];
        return getNumOggBytes(buffer, 1);
    }

    public void setNumOggBytes(byte[] buffer, int offset) {
        int position = offset;
        nogCodCompagniaAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NOG_COD_COMPAGNIA_ANIA, 0);
        position += Len.NOG_COD_COMPAGNIA_ANIA;
        nogFormaAssicurativa = MarshalByte.readString(buffer, position, Len.NOG_FORMA_ASSICURATIVA);
        position += Len.NOG_FORMA_ASSICURATIVA;
        nogCodOggetto = MarshalByte.readString(buffer, position, Len.NOG_COD_OGGETTO);
        position += Len.NOG_COD_OGGETTO;
        nogTipoOggetto = MarshalByte.readString(buffer, position, Len.NOG_TIPO_OGGETTO);
        position += Len.NOG_TIPO_OGGETTO;
        nogUltProgr.setNogUltProgrFromBuffer(buffer, position);
        position += NogUltProgr.Len.NOG_ULT_PROGR;
        setNogDescOggettoVcharBytes(buffer, position);
    }

    public byte[] getNumOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, nogCodCompagniaAnia, Len.Int.NOG_COD_COMPAGNIA_ANIA, 0);
        position += Len.NOG_COD_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, nogFormaAssicurativa, Len.NOG_FORMA_ASSICURATIVA);
        position += Len.NOG_FORMA_ASSICURATIVA;
        MarshalByte.writeString(buffer, position, nogCodOggetto, Len.NOG_COD_OGGETTO);
        position += Len.NOG_COD_OGGETTO;
        MarshalByte.writeString(buffer, position, nogTipoOggetto, Len.NOG_TIPO_OGGETTO);
        position += Len.NOG_TIPO_OGGETTO;
        nogUltProgr.getNogUltProgrAsBuffer(buffer, position);
        position += NogUltProgr.Len.NOG_ULT_PROGR;
        getNogDescOggettoVcharBytes(buffer, position);
        return buffer;
    }

    public void setNogCodCompagniaAnia(int nogCodCompagniaAnia) {
        this.nogCodCompagniaAnia = nogCodCompagniaAnia;
    }

    public int getNogCodCompagniaAnia() {
        return this.nogCodCompagniaAnia;
    }

    public void setNogFormaAssicurativa(String nogFormaAssicurativa) {
        this.nogFormaAssicurativa = Functions.subString(nogFormaAssicurativa, Len.NOG_FORMA_ASSICURATIVA);
    }

    public String getNogFormaAssicurativa() {
        return this.nogFormaAssicurativa;
    }

    public void setNogCodOggetto(String nogCodOggetto) {
        this.nogCodOggetto = Functions.subString(nogCodOggetto, Len.NOG_COD_OGGETTO);
    }

    public String getNogCodOggetto() {
        return this.nogCodOggetto;
    }

    public void setNogTipoOggetto(String nogTipoOggetto) {
        this.nogTipoOggetto = Functions.subString(nogTipoOggetto, Len.NOG_TIPO_OGGETTO);
    }

    public String getNogTipoOggetto() {
        return this.nogTipoOggetto;
    }

    public void setNogDescOggettoVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        nogDescOggettoLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        nogDescOggetto = MarshalByte.readString(buffer, position, Len.NOG_DESC_OGGETTO);
    }

    public byte[] getNogDescOggettoVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, nogDescOggettoLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, nogDescOggetto, Len.NOG_DESC_OGGETTO);
        return buffer;
    }

    public void setNogDescOggettoLen(short nogDescOggettoLen) {
        this.nogDescOggettoLen = nogDescOggettoLen;
    }

    public short getNogDescOggettoLen() {
        return this.nogDescOggettoLen;
    }

    public void setNogDescOggetto(String nogDescOggetto) {
        this.nogDescOggetto = Functions.subString(nogDescOggetto, Len.NOG_DESC_OGGETTO);
    }

    public String getNogDescOggetto() {
        return this.nogDescOggetto;
    }

    public NogUltProgr getNogUltProgr() {
        return nogUltProgr;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NOG_FORMA_ASSICURATIVA = 2;
        public static final int NOG_COD_OGGETTO = 30;
        public static final int NOG_TIPO_OGGETTO = 2;
        public static final int NOG_DESC_OGGETTO = 250;
        public static final int NOG_COD_COMPAGNIA_ANIA = 3;
        public static final int NOG_DESC_OGGETTO_LEN = 2;
        public static final int NOG_DESC_OGGETTO_VCHAR = NOG_DESC_OGGETTO_LEN + NOG_DESC_OGGETTO;
        public static final int NUM_OGG = NOG_COD_COMPAGNIA_ANIA + NOG_FORMA_ASSICURATIVA + NOG_COD_OGGETTO + NOG_TIPO_OGGETTO + NogUltProgr.Len.NOG_ULT_PROGR + NOG_DESC_OGGETTO_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int NOG_COD_COMPAGNIA_ANIA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
