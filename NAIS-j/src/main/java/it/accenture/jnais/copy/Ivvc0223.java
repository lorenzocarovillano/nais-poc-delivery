package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.occurs.Ivvc0223TabLivelliGar;

/**Original name: IVVC0223<br>
 * Copybook: IVVC0223 from copybook IVVC0223<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Ivvc0223 {

    //==== PROPERTIES ====
    public static final int TAB_LIVELLI_GAR_MAXOCCURS = 20;
    //Original name: IVVC0223-ELE-MAX-AREA-GAR
    private short eleMaxAreaGar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IVVC0223-TAB-LIVELLI-GAR
    private Ivvc0223TabLivelliGar[] tabLivelliGar = new Ivvc0223TabLivelliGar[TAB_LIVELLI_GAR_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ivvc0223() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabLivelliGarIdx = 1; tabLivelliGarIdx <= TAB_LIVELLI_GAR_MAXOCCURS; tabLivelliGarIdx++) {
            tabLivelliGar[tabLivelliGarIdx - 1] = new Ivvc0223TabLivelliGar();
        }
    }

    public void setAreaVariabiliGarBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxAreaGar = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_LIVELLI_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabLivelliGar[idx - 1].setTabLivelliGarBytes(buffer, position);
                position += Ivvc0223TabLivelliGar.Len.TAB_LIVELLI_GAR;
            }
            else {
                tabLivelliGar[idx - 1].initTabLivelliGarSpaces();
                position += Ivvc0223TabLivelliGar.Len.TAB_LIVELLI_GAR;
            }
        }
    }

    public byte[] getAreaVariabiliGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleMaxAreaGar);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_LIVELLI_GAR_MAXOCCURS; idx++) {
            tabLivelliGar[idx - 1].getTabLivelliGarBytes(buffer, position);
            position += Ivvc0223TabLivelliGar.Len.TAB_LIVELLI_GAR;
        }
        return buffer;
    }

    public void setEleMaxAreaGar(short eleMaxAreaGar) {
        this.eleMaxAreaGar = eleMaxAreaGar;
    }

    public short getEleMaxAreaGar() {
        return this.eleMaxAreaGar;
    }

    public Ivvc0223TabLivelliGar getTabLivelliGar(int idx) {
        return tabLivelliGar[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_MAX_AREA_GAR = 2;
        public static final int AREA_VARIABILI_GAR = ELE_MAX_AREA_GAR + Ivvc0223.TAB_LIVELLI_GAR_MAXOCCURS * Ivvc0223TabLivelliGar.Len.TAB_LIVELLI_GAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
