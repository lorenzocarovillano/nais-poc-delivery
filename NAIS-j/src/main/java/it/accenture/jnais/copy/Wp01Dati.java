package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Wp01DtEff;
import it.accenture.jnais.ws.redefines.Wp01DtEstFinanz;
import it.accenture.jnais.ws.redefines.Wp01DtManCop;
import it.accenture.jnais.ws.redefines.Wp01DtSin;
import it.accenture.jnais.ws.redefines.Wp01IdLiq;
import it.accenture.jnais.ws.redefines.Wp01IdRichEstCollg;
import it.accenture.jnais.ws.redefines.Wp01IdRichiedente;

/**Original name: WP01-DATI<br>
 * Variable: WP01-DATI from copybook LCCVP011<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wp01Dati {

    //==== PROPERTIES ====
    //Original name: WP01-ID-RICH-EST
    private int wp01IdRichEst = DefaultValues.INT_VAL;
    //Original name: WP01-ID-RICH-EST-COLLG
    private Wp01IdRichEstCollg wp01IdRichEstCollg = new Wp01IdRichEstCollg();
    //Original name: WP01-ID-LIQ
    private Wp01IdLiq wp01IdLiq = new Wp01IdLiq();
    //Original name: WP01-COD-COMP-ANIA
    private int wp01CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WP01-IB-RICH-EST
    private String wp01IbRichEst = DefaultValues.stringVal(Len.WP01_IB_RICH_EST);
    //Original name: WP01-TP-MOVI
    private int wp01TpMovi = DefaultValues.INT_VAL;
    //Original name: WP01-DT-FORM-RICH
    private int wp01DtFormRich = DefaultValues.INT_VAL;
    //Original name: WP01-DT-INVIO-RICH
    private int wp01DtInvioRich = DefaultValues.INT_VAL;
    //Original name: WP01-DT-PERV-RICH
    private int wp01DtPervRich = DefaultValues.INT_VAL;
    //Original name: WP01-DT-RGSTRZ-RICH
    private int wp01DtRgstrzRich = DefaultValues.INT_VAL;
    //Original name: WP01-DT-EFF
    private Wp01DtEff wp01DtEff = new Wp01DtEff();
    //Original name: WP01-DT-SIN
    private Wp01DtSin wp01DtSin = new Wp01DtSin();
    //Original name: WP01-TP-OGG
    private String wp01TpOgg = DefaultValues.stringVal(Len.WP01_TP_OGG);
    //Original name: WP01-ID-OGG
    private int wp01IdOgg = DefaultValues.INT_VAL;
    //Original name: WP01-IB-OGG
    private String wp01IbOgg = DefaultValues.stringVal(Len.WP01_IB_OGG);
    //Original name: WP01-FL-MOD-EXEC
    private char wp01FlModExec = DefaultValues.CHAR_VAL;
    //Original name: WP01-ID-RICHIEDENTE
    private Wp01IdRichiedente wp01IdRichiedente = new Wp01IdRichiedente();
    //Original name: WP01-COD-PROD
    private String wp01CodProd = DefaultValues.stringVal(Len.WP01_COD_PROD);
    //Original name: WP01-DS-OPER-SQL
    private char wp01DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WP01-DS-VER
    private int wp01DsVer = DefaultValues.INT_VAL;
    //Original name: WP01-DS-TS-CPTZ
    private long wp01DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WP01-DS-UTENTE
    private String wp01DsUtente = DefaultValues.stringVal(Len.WP01_DS_UTENTE);
    //Original name: WP01-DS-STATO-ELAB
    private char wp01DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WP01-COD-CAN
    private int wp01CodCan = DefaultValues.INT_VAL;
    //Original name: WP01-IB-OGG-ORIG
    private String wp01IbOggOrig = DefaultValues.stringVal(Len.WP01_IB_OGG_ORIG);
    //Original name: WP01-DT-EST-FINANZ
    private Wp01DtEstFinanz wp01DtEstFinanz = new Wp01DtEstFinanz();
    //Original name: WP01-DT-MAN-COP
    private Wp01DtManCop wp01DtManCop = new Wp01DtManCop();
    //Original name: WP01-FL-GEST-PROTEZIONE
    private char wp01FlGestProtezione = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wp01IdRichEst = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP01_ID_RICH_EST, 0);
        position += Len.WP01_ID_RICH_EST;
        wp01IdRichEstCollg.setWp01IdRichEstCollgFromBuffer(buffer, position);
        position += Wp01IdRichEstCollg.Len.WP01_ID_RICH_EST_COLLG;
        wp01IdLiq.setWp01IdLiqFromBuffer(buffer, position);
        position += Wp01IdLiq.Len.WP01_ID_LIQ;
        wp01CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP01_COD_COMP_ANIA, 0);
        position += Len.WP01_COD_COMP_ANIA;
        wp01IbRichEst = MarshalByte.readString(buffer, position, Len.WP01_IB_RICH_EST);
        position += Len.WP01_IB_RICH_EST;
        wp01TpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP01_TP_MOVI, 0);
        position += Len.WP01_TP_MOVI;
        wp01DtFormRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP01_DT_FORM_RICH, 0);
        position += Len.WP01_DT_FORM_RICH;
        wp01DtInvioRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP01_DT_INVIO_RICH, 0);
        position += Len.WP01_DT_INVIO_RICH;
        wp01DtPervRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP01_DT_PERV_RICH, 0);
        position += Len.WP01_DT_PERV_RICH;
        wp01DtRgstrzRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP01_DT_RGSTRZ_RICH, 0);
        position += Len.WP01_DT_RGSTRZ_RICH;
        wp01DtEff.setWp01DtEffFromBuffer(buffer, position);
        position += Wp01DtEff.Len.WP01_DT_EFF;
        wp01DtSin.setWp01DtSinFromBuffer(buffer, position);
        position += Wp01DtSin.Len.WP01_DT_SIN;
        wp01TpOgg = MarshalByte.readString(buffer, position, Len.WP01_TP_OGG);
        position += Len.WP01_TP_OGG;
        wp01IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP01_ID_OGG, 0);
        position += Len.WP01_ID_OGG;
        wp01IbOgg = MarshalByte.readString(buffer, position, Len.WP01_IB_OGG);
        position += Len.WP01_IB_OGG;
        wp01FlModExec = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wp01IdRichiedente.setWp01IdRichiedenteFromBuffer(buffer, position);
        position += Wp01IdRichiedente.Len.WP01_ID_RICHIEDENTE;
        wp01CodProd = MarshalByte.readString(buffer, position, Len.WP01_COD_PROD);
        position += Len.WP01_COD_PROD;
        wp01DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wp01DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP01_DS_VER, 0);
        position += Len.WP01_DS_VER;
        wp01DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WP01_DS_TS_CPTZ, 0);
        position += Len.WP01_DS_TS_CPTZ;
        wp01DsUtente = MarshalByte.readString(buffer, position, Len.WP01_DS_UTENTE);
        position += Len.WP01_DS_UTENTE;
        wp01DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wp01CodCan = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP01_COD_CAN, 0);
        position += Len.WP01_COD_CAN;
        wp01IbOggOrig = MarshalByte.readString(buffer, position, Len.WP01_IB_OGG_ORIG);
        position += Len.WP01_IB_OGG_ORIG;
        wp01DtEstFinanz.setWp01DtEstFinanzFromBuffer(buffer, position);
        position += Wp01DtEstFinanz.Len.WP01_DT_EST_FINANZ;
        wp01DtManCop.setWp01DtManCopFromBuffer(buffer, position);
        position += Wp01DtManCop.Len.WP01_DT_MAN_COP;
        wp01FlGestProtezione = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wp01IdRichEst, Len.Int.WP01_ID_RICH_EST, 0);
        position += Len.WP01_ID_RICH_EST;
        wp01IdRichEstCollg.getWp01IdRichEstCollgAsBuffer(buffer, position);
        position += Wp01IdRichEstCollg.Len.WP01_ID_RICH_EST_COLLG;
        wp01IdLiq.getWp01IdLiqAsBuffer(buffer, position);
        position += Wp01IdLiq.Len.WP01_ID_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, wp01CodCompAnia, Len.Int.WP01_COD_COMP_ANIA, 0);
        position += Len.WP01_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wp01IbRichEst, Len.WP01_IB_RICH_EST);
        position += Len.WP01_IB_RICH_EST;
        MarshalByte.writeIntAsPacked(buffer, position, wp01TpMovi, Len.Int.WP01_TP_MOVI, 0);
        position += Len.WP01_TP_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, wp01DtFormRich, Len.Int.WP01_DT_FORM_RICH, 0);
        position += Len.WP01_DT_FORM_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, wp01DtInvioRich, Len.Int.WP01_DT_INVIO_RICH, 0);
        position += Len.WP01_DT_INVIO_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, wp01DtPervRich, Len.Int.WP01_DT_PERV_RICH, 0);
        position += Len.WP01_DT_PERV_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, wp01DtRgstrzRich, Len.Int.WP01_DT_RGSTRZ_RICH, 0);
        position += Len.WP01_DT_RGSTRZ_RICH;
        wp01DtEff.getWp01DtEffAsBuffer(buffer, position);
        position += Wp01DtEff.Len.WP01_DT_EFF;
        wp01DtSin.getWp01DtSinAsBuffer(buffer, position);
        position += Wp01DtSin.Len.WP01_DT_SIN;
        MarshalByte.writeString(buffer, position, wp01TpOgg, Len.WP01_TP_OGG);
        position += Len.WP01_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, wp01IdOgg, Len.Int.WP01_ID_OGG, 0);
        position += Len.WP01_ID_OGG;
        MarshalByte.writeString(buffer, position, wp01IbOgg, Len.WP01_IB_OGG);
        position += Len.WP01_IB_OGG;
        MarshalByte.writeChar(buffer, position, wp01FlModExec);
        position += Types.CHAR_SIZE;
        wp01IdRichiedente.getWp01IdRichiedenteAsBuffer(buffer, position);
        position += Wp01IdRichiedente.Len.WP01_ID_RICHIEDENTE;
        MarshalByte.writeString(buffer, position, wp01CodProd, Len.WP01_COD_PROD);
        position += Len.WP01_COD_PROD;
        MarshalByte.writeChar(buffer, position, wp01DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wp01DsVer, Len.Int.WP01_DS_VER, 0);
        position += Len.WP01_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wp01DsTsCptz, Len.Int.WP01_DS_TS_CPTZ, 0);
        position += Len.WP01_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wp01DsUtente, Len.WP01_DS_UTENTE);
        position += Len.WP01_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wp01DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wp01CodCan, Len.Int.WP01_COD_CAN, 0);
        position += Len.WP01_COD_CAN;
        MarshalByte.writeString(buffer, position, wp01IbOggOrig, Len.WP01_IB_OGG_ORIG);
        position += Len.WP01_IB_OGG_ORIG;
        wp01DtEstFinanz.getWp01DtEstFinanzAsBuffer(buffer, position);
        position += Wp01DtEstFinanz.Len.WP01_DT_EST_FINANZ;
        wp01DtManCop.getWp01DtManCopAsBuffer(buffer, position);
        position += Wp01DtManCop.Len.WP01_DT_MAN_COP;
        MarshalByte.writeChar(buffer, position, wp01FlGestProtezione);
        return buffer;
    }

    public void setWp01IdRichEst(int wp01IdRichEst) {
        this.wp01IdRichEst = wp01IdRichEst;
    }

    public int getWp01IdRichEst() {
        return this.wp01IdRichEst;
    }

    public void setWp01CodCompAnia(int wp01CodCompAnia) {
        this.wp01CodCompAnia = wp01CodCompAnia;
    }

    public int getWp01CodCompAnia() {
        return this.wp01CodCompAnia;
    }

    public void setWp01IbRichEst(String wp01IbRichEst) {
        this.wp01IbRichEst = Functions.subString(wp01IbRichEst, Len.WP01_IB_RICH_EST);
    }

    public String getWp01IbRichEst() {
        return this.wp01IbRichEst;
    }

    public void setWp01TpMovi(int wp01TpMovi) {
        this.wp01TpMovi = wp01TpMovi;
    }

    public int getWp01TpMovi() {
        return this.wp01TpMovi;
    }

    public void setWp01DtFormRich(int wp01DtFormRich) {
        this.wp01DtFormRich = wp01DtFormRich;
    }

    public int getWp01DtFormRich() {
        return this.wp01DtFormRich;
    }

    public void setWp01DtInvioRich(int wp01DtInvioRich) {
        this.wp01DtInvioRich = wp01DtInvioRich;
    }

    public int getWp01DtInvioRich() {
        return this.wp01DtInvioRich;
    }

    public void setWp01DtPervRich(int wp01DtPervRich) {
        this.wp01DtPervRich = wp01DtPervRich;
    }

    public int getWp01DtPervRich() {
        return this.wp01DtPervRich;
    }

    public void setWp01DtRgstrzRich(int wp01DtRgstrzRich) {
        this.wp01DtRgstrzRich = wp01DtRgstrzRich;
    }

    public int getWp01DtRgstrzRich() {
        return this.wp01DtRgstrzRich;
    }

    public void setWp01TpOgg(String wp01TpOgg) {
        this.wp01TpOgg = Functions.subString(wp01TpOgg, Len.WP01_TP_OGG);
    }

    public String getWp01TpOgg() {
        return this.wp01TpOgg;
    }

    public void setWp01IdOgg(int wp01IdOgg) {
        this.wp01IdOgg = wp01IdOgg;
    }

    public int getWp01IdOgg() {
        return this.wp01IdOgg;
    }

    public void setWp01IbOgg(String wp01IbOgg) {
        this.wp01IbOgg = Functions.subString(wp01IbOgg, Len.WP01_IB_OGG);
    }

    public String getWp01IbOgg() {
        return this.wp01IbOgg;
    }

    public void setWp01FlModExec(char wp01FlModExec) {
        this.wp01FlModExec = wp01FlModExec;
    }

    public char getWp01FlModExec() {
        return this.wp01FlModExec;
    }

    public void setWp01CodProd(String wp01CodProd) {
        this.wp01CodProd = Functions.subString(wp01CodProd, Len.WP01_COD_PROD);
    }

    public String getWp01CodProd() {
        return this.wp01CodProd;
    }

    public void setWp01DsOperSql(char wp01DsOperSql) {
        this.wp01DsOperSql = wp01DsOperSql;
    }

    public char getWp01DsOperSql() {
        return this.wp01DsOperSql;
    }

    public void setWp01DsVer(int wp01DsVer) {
        this.wp01DsVer = wp01DsVer;
    }

    public int getWp01DsVer() {
        return this.wp01DsVer;
    }

    public void setWp01DsTsCptz(long wp01DsTsCptz) {
        this.wp01DsTsCptz = wp01DsTsCptz;
    }

    public long getWp01DsTsCptz() {
        return this.wp01DsTsCptz;
    }

    public void setWp01DsUtente(String wp01DsUtente) {
        this.wp01DsUtente = Functions.subString(wp01DsUtente, Len.WP01_DS_UTENTE);
    }

    public String getWp01DsUtente() {
        return this.wp01DsUtente;
    }

    public void setWp01DsStatoElab(char wp01DsStatoElab) {
        this.wp01DsStatoElab = wp01DsStatoElab;
    }

    public char getWp01DsStatoElab() {
        return this.wp01DsStatoElab;
    }

    public void setWp01CodCan(int wp01CodCan) {
        this.wp01CodCan = wp01CodCan;
    }

    public int getWp01CodCan() {
        return this.wp01CodCan;
    }

    public void setWp01IbOggOrig(String wp01IbOggOrig) {
        this.wp01IbOggOrig = Functions.subString(wp01IbOggOrig, Len.WP01_IB_OGG_ORIG);
    }

    public String getWp01IbOggOrig() {
        return this.wp01IbOggOrig;
    }

    public void setWp01FlGestProtezione(char wp01FlGestProtezione) {
        this.wp01FlGestProtezione = wp01FlGestProtezione;
    }

    public char getWp01FlGestProtezione() {
        return this.wp01FlGestProtezione;
    }

    public Wp01DtEff getWp01DtEff() {
        return wp01DtEff;
    }

    public Wp01DtEstFinanz getWp01DtEstFinanz() {
        return wp01DtEstFinanz;
    }

    public Wp01DtManCop getWp01DtManCop() {
        return wp01DtManCop;
    }

    public Wp01DtSin getWp01DtSin() {
        return wp01DtSin;
    }

    public Wp01IdLiq getWp01IdLiq() {
        return wp01IdLiq;
    }

    public Wp01IdRichEstCollg getWp01IdRichEstCollg() {
        return wp01IdRichEstCollg;
    }

    public Wp01IdRichiedente getWp01IdRichiedente() {
        return wp01IdRichiedente;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WP01_ID_RICH_EST = 5;
        public static final int WP01_COD_COMP_ANIA = 3;
        public static final int WP01_IB_RICH_EST = 40;
        public static final int WP01_TP_MOVI = 3;
        public static final int WP01_DT_FORM_RICH = 5;
        public static final int WP01_DT_INVIO_RICH = 5;
        public static final int WP01_DT_PERV_RICH = 5;
        public static final int WP01_DT_RGSTRZ_RICH = 5;
        public static final int WP01_TP_OGG = 2;
        public static final int WP01_ID_OGG = 5;
        public static final int WP01_IB_OGG = 40;
        public static final int WP01_FL_MOD_EXEC = 1;
        public static final int WP01_COD_PROD = 12;
        public static final int WP01_DS_OPER_SQL = 1;
        public static final int WP01_DS_VER = 5;
        public static final int WP01_DS_TS_CPTZ = 10;
        public static final int WP01_DS_UTENTE = 20;
        public static final int WP01_DS_STATO_ELAB = 1;
        public static final int WP01_COD_CAN = 3;
        public static final int WP01_IB_OGG_ORIG = 40;
        public static final int WP01_FL_GEST_PROTEZIONE = 1;
        public static final int DATI = WP01_ID_RICH_EST + Wp01IdRichEstCollg.Len.WP01_ID_RICH_EST_COLLG + Wp01IdLiq.Len.WP01_ID_LIQ + WP01_COD_COMP_ANIA + WP01_IB_RICH_EST + WP01_TP_MOVI + WP01_DT_FORM_RICH + WP01_DT_INVIO_RICH + WP01_DT_PERV_RICH + WP01_DT_RGSTRZ_RICH + Wp01DtEff.Len.WP01_DT_EFF + Wp01DtSin.Len.WP01_DT_SIN + WP01_TP_OGG + WP01_ID_OGG + WP01_IB_OGG + WP01_FL_MOD_EXEC + Wp01IdRichiedente.Len.WP01_ID_RICHIEDENTE + WP01_COD_PROD + WP01_DS_OPER_SQL + WP01_DS_VER + WP01_DS_TS_CPTZ + WP01_DS_UTENTE + WP01_DS_STATO_ELAB + WP01_COD_CAN + WP01_IB_OGG_ORIG + Wp01DtEstFinanz.Len.WP01_DT_EST_FINANZ + Wp01DtManCop.Len.WP01_DT_MAN_COP + WP01_FL_GEST_PROTEZIONE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP01_ID_RICH_EST = 9;
            public static final int WP01_COD_COMP_ANIA = 5;
            public static final int WP01_TP_MOVI = 5;
            public static final int WP01_DT_FORM_RICH = 8;
            public static final int WP01_DT_INVIO_RICH = 8;
            public static final int WP01_DT_PERV_RICH = 8;
            public static final int WP01_DT_RGSTRZ_RICH = 8;
            public static final int WP01_ID_OGG = 9;
            public static final int WP01_DS_VER = 9;
            public static final int WP01_DS_TS_CPTZ = 18;
            public static final int WP01_COD_CAN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
