package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-ADES<br>
 * Variable: IND-ADES from copybook IDBVADE2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndAdes {

    //==== PROPERTIES ====
    //Original name: IND-ADE-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IB-PREV
    private short ibPrev = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IB-OGG
    private short ibOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-DT-DECOR
    private short dtDecor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-DT-SCAD
    private short dtScad = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-ETA-A-SCAD
    private short etaAScad = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-DUR-AA
    private short durAa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-DUR-MM
    private short durMm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-DUR-GG
    private short durGg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-TP-RIAT
    private short tpRiat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-TP-IAS
    private short tpIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-DT-VARZ-TP-IAS
    private short dtVarzTpIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-PRE-NET-IND
    private short preNetInd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-PRE-LRD-IND
    private short preLrdInd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-RAT-LRD-IND
    private short ratLrdInd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-PRSTZ-INI-IND
    private short prstzIniInd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-FL-COINC-ASSTO
    private short flCoincAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IB-DFLT
    private short ibDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-MOD-CALC
    private short modCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-TP-FNT-CNBTVA
    private short tpFntCnbtva = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IMP-AZ
    private short impAz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IMP-ADER
    private short impAder = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IMP-TFR
    private short impTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IMP-VOLO
    private short impVolo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-PC-AZ
    private short pcAz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-PC-ADER
    private short pcAder = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-PC-TFR
    private short pcTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-PC-VOLO
    private short pcVolo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-DT-NOVA-RGM-FISC
    private short dtNovaRgmFisc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-FL-ATTIV
    private short flAttiv = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IMP-REC-RIT-VIS
    private short impRecRitVis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IMP-REC-RIT-ACC
    private short impRecRitAcc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-FL-VARZ-STAT-TBGC
    private short flVarzStatTbgc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-FL-PROVZA-MIGRAZ
    private short flProvzaMigraz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IMPB-VIS-DA-REC
    private short impbVisDaRec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-DT-DECOR-PREST-BAN
    private short dtDecorPrestBan = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-DT-EFF-VARZ-STAT-T
    private short dtEffVarzStatT = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-CUM-CNBT-CAP
    private short cumCnbtCap = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IMP-GAR-CNBT
    private short impGarCnbt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-DT-ULT-CONS-CNBT
    private short dtUltConsCnbt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-IDEN-ISC-FND
    private short idenIscFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-NUM-RAT-PIAN
    private short numRatPian = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-DT-PRESC
    private short dtPresc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADE-CONCS-PREST
    private short concsPrest = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setIbPrev(short ibPrev) {
        this.ibPrev = ibPrev;
    }

    public short getIbPrev() {
        return this.ibPrev;
    }

    public void setIbOgg(short ibOgg) {
        this.ibOgg = ibOgg;
    }

    public short getIbOgg() {
        return this.ibOgg;
    }

    public void setDtDecor(short dtDecor) {
        this.dtDecor = dtDecor;
    }

    public short getDtDecor() {
        return this.dtDecor;
    }

    public void setDtScad(short dtScad) {
        this.dtScad = dtScad;
    }

    public short getDtScad() {
        return this.dtScad;
    }

    public void setEtaAScad(short etaAScad) {
        this.etaAScad = etaAScad;
    }

    public short getEtaAScad() {
        return this.etaAScad;
    }

    public void setDurAa(short durAa) {
        this.durAa = durAa;
    }

    public short getDurAa() {
        return this.durAa;
    }

    public void setDurMm(short durMm) {
        this.durMm = durMm;
    }

    public short getDurMm() {
        return this.durMm;
    }

    public void setDurGg(short durGg) {
        this.durGg = durGg;
    }

    public short getDurGg() {
        return this.durGg;
    }

    public void setTpRiat(short tpRiat) {
        this.tpRiat = tpRiat;
    }

    public short getTpRiat() {
        return this.tpRiat;
    }

    public void setTpIas(short tpIas) {
        this.tpIas = tpIas;
    }

    public short getTpIas() {
        return this.tpIas;
    }

    public void setDtVarzTpIas(short dtVarzTpIas) {
        this.dtVarzTpIas = dtVarzTpIas;
    }

    public short getDtVarzTpIas() {
        return this.dtVarzTpIas;
    }

    public void setPreNetInd(short preNetInd) {
        this.preNetInd = preNetInd;
    }

    public short getPreNetInd() {
        return this.preNetInd;
    }

    public void setPreLrdInd(short preLrdInd) {
        this.preLrdInd = preLrdInd;
    }

    public short getPreLrdInd() {
        return this.preLrdInd;
    }

    public void setRatLrdInd(short ratLrdInd) {
        this.ratLrdInd = ratLrdInd;
    }

    public short getRatLrdInd() {
        return this.ratLrdInd;
    }

    public void setPrstzIniInd(short prstzIniInd) {
        this.prstzIniInd = prstzIniInd;
    }

    public short getPrstzIniInd() {
        return this.prstzIniInd;
    }

    public void setFlCoincAssto(short flCoincAssto) {
        this.flCoincAssto = flCoincAssto;
    }

    public short getFlCoincAssto() {
        return this.flCoincAssto;
    }

    public void setIbDflt(short ibDflt) {
        this.ibDflt = ibDflt;
    }

    public short getIbDflt() {
        return this.ibDflt;
    }

    public void setModCalc(short modCalc) {
        this.modCalc = modCalc;
    }

    public short getModCalc() {
        return this.modCalc;
    }

    public void setTpFntCnbtva(short tpFntCnbtva) {
        this.tpFntCnbtva = tpFntCnbtva;
    }

    public short getTpFntCnbtva() {
        return this.tpFntCnbtva;
    }

    public void setImpAz(short impAz) {
        this.impAz = impAz;
    }

    public short getImpAz() {
        return this.impAz;
    }

    public void setImpAder(short impAder) {
        this.impAder = impAder;
    }

    public short getImpAder() {
        return this.impAder;
    }

    public void setImpTfr(short impTfr) {
        this.impTfr = impTfr;
    }

    public short getImpTfr() {
        return this.impTfr;
    }

    public void setImpVolo(short impVolo) {
        this.impVolo = impVolo;
    }

    public short getImpVolo() {
        return this.impVolo;
    }

    public void setPcAz(short pcAz) {
        this.pcAz = pcAz;
    }

    public short getPcAz() {
        return this.pcAz;
    }

    public void setPcAder(short pcAder) {
        this.pcAder = pcAder;
    }

    public short getPcAder() {
        return this.pcAder;
    }

    public void setPcTfr(short pcTfr) {
        this.pcTfr = pcTfr;
    }

    public short getPcTfr() {
        return this.pcTfr;
    }

    public void setPcVolo(short pcVolo) {
        this.pcVolo = pcVolo;
    }

    public short getPcVolo() {
        return this.pcVolo;
    }

    public void setDtNovaRgmFisc(short dtNovaRgmFisc) {
        this.dtNovaRgmFisc = dtNovaRgmFisc;
    }

    public short getDtNovaRgmFisc() {
        return this.dtNovaRgmFisc;
    }

    public void setFlAttiv(short flAttiv) {
        this.flAttiv = flAttiv;
    }

    public short getFlAttiv() {
        return this.flAttiv;
    }

    public void setImpRecRitVis(short impRecRitVis) {
        this.impRecRitVis = impRecRitVis;
    }

    public short getImpRecRitVis() {
        return this.impRecRitVis;
    }

    public void setImpRecRitAcc(short impRecRitAcc) {
        this.impRecRitAcc = impRecRitAcc;
    }

    public short getImpRecRitAcc() {
        return this.impRecRitAcc;
    }

    public void setFlVarzStatTbgc(short flVarzStatTbgc) {
        this.flVarzStatTbgc = flVarzStatTbgc;
    }

    public short getFlVarzStatTbgc() {
        return this.flVarzStatTbgc;
    }

    public void setFlProvzaMigraz(short flProvzaMigraz) {
        this.flProvzaMigraz = flProvzaMigraz;
    }

    public short getFlProvzaMigraz() {
        return this.flProvzaMigraz;
    }

    public void setImpbVisDaRec(short impbVisDaRec) {
        this.impbVisDaRec = impbVisDaRec;
    }

    public short getImpbVisDaRec() {
        return this.impbVisDaRec;
    }

    public void setDtDecorPrestBan(short dtDecorPrestBan) {
        this.dtDecorPrestBan = dtDecorPrestBan;
    }

    public short getDtDecorPrestBan() {
        return this.dtDecorPrestBan;
    }

    public void setDtEffVarzStatT(short dtEffVarzStatT) {
        this.dtEffVarzStatT = dtEffVarzStatT;
    }

    public short getDtEffVarzStatT() {
        return this.dtEffVarzStatT;
    }

    public void setCumCnbtCap(short cumCnbtCap) {
        this.cumCnbtCap = cumCnbtCap;
    }

    public short getCumCnbtCap() {
        return this.cumCnbtCap;
    }

    public void setImpGarCnbt(short impGarCnbt) {
        this.impGarCnbt = impGarCnbt;
    }

    public short getImpGarCnbt() {
        return this.impGarCnbt;
    }

    public void setDtUltConsCnbt(short dtUltConsCnbt) {
        this.dtUltConsCnbt = dtUltConsCnbt;
    }

    public short getDtUltConsCnbt() {
        return this.dtUltConsCnbt;
    }

    public void setIdenIscFnd(short idenIscFnd) {
        this.idenIscFnd = idenIscFnd;
    }

    public short getIdenIscFnd() {
        return this.idenIscFnd;
    }

    public void setNumRatPian(short numRatPian) {
        this.numRatPian = numRatPian;
    }

    public short getNumRatPian() {
        return this.numRatPian;
    }

    public void setDtPresc(short dtPresc) {
        this.dtPresc = dtPresc;
    }

    public short getDtPresc() {
        return this.dtPresc;
    }

    public void setConcsPrest(short concsPrest) {
        this.concsPrest = concsPrest;
    }

    public short getConcsPrest() {
        return this.concsPrest;
    }
}
