package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-TASTI-DA-ABILITARE<br>
 * Variable: WCOM-TASTI-DA-ABILITARE from copybook LCCC0001<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WcomTastiDaAbilitare {

    //==== PROPERTIES ====
    /**Original name: WCOM-TASTO-ANNULLA<br>
	 * <pre>--> IL TASTO "ANNULLA" SI RIFERISCE AL TASTO "ESCI"</pre>*/
    private char annulla = DefaultValues.CHAR_VAL;
    //Original name: WCOM-TASTO-DA-AUTORIZZARE
    private char daAutorizzare = DefaultValues.CHAR_VAL;
    //Original name: WCOM-TASTO-AUTORIZZA
    private char autorizza = DefaultValues.CHAR_VAL;
    //Original name: WCOM-TASTO-AGGIORNA-PTF
    private char aggiornaPtf = DefaultValues.CHAR_VAL;
    //Original name: WCOM-TASTO-RESPINGI
    private char respingi = DefaultValues.CHAR_VAL;
    //Original name: WCOM-TASTO-ELIMINA
    private char elimina = DefaultValues.CHAR_VAL;
    //Original name: WCOM-TASTO-AVANTI
    private char avanti = DefaultValues.CHAR_VAL;
    //Original name: WCOM-TASTO-INDIETRO
    private char indietro = DefaultValues.CHAR_VAL;
    //Original name: WCOM-TASTO-NOTE
    private char note = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setTastiDaAbilitareBytes(byte[] buffer, int offset) {
        int position = offset;
        annulla = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        daAutorizzare = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        autorizza = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        aggiornaPtf = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        respingi = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        elimina = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        avanti = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        indietro = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        note = MarshalByte.readChar(buffer, position);
    }

    public byte[] getTastiDaAbilitareBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, annulla);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, daAutorizzare);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, autorizza);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, aggiornaPtf);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, respingi);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, elimina);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, avanti);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, indietro);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, note);
        return buffer;
    }

    public void setAnnulla(char annulla) {
        this.annulla = annulla;
    }

    public void setWcomTastoAnnullaFormatted(String wcomTastoAnnulla) {
        setAnnulla(Functions.charAt(wcomTastoAnnulla, Types.CHAR_SIZE));
    }

    public char getAnnulla() {
        return this.annulla;
    }

    public void setDaAutorizzare(char daAutorizzare) {
        this.daAutorizzare = daAutorizzare;
    }

    public void setWcomTastoDaAutorizzareFormatted(String wcomTastoDaAutorizzare) {
        setDaAutorizzare(Functions.charAt(wcomTastoDaAutorizzare, Types.CHAR_SIZE));
    }

    public char getDaAutorizzare() {
        return this.daAutorizzare;
    }

    public void setAutorizza(char autorizza) {
        this.autorizza = autorizza;
    }

    public void setWcomTastoAutorizzaFormatted(String wcomTastoAutorizza) {
        setAutorizza(Functions.charAt(wcomTastoAutorizza, Types.CHAR_SIZE));
    }

    public char getAutorizza() {
        return this.autorizza;
    }

    public void setAggiornaPtf(char aggiornaPtf) {
        this.aggiornaPtf = aggiornaPtf;
    }

    public void setWcomTastoAggiornaPtfFormatted(String wcomTastoAggiornaPtf) {
        setAggiornaPtf(Functions.charAt(wcomTastoAggiornaPtf, Types.CHAR_SIZE));
    }

    public char getAggiornaPtf() {
        return this.aggiornaPtf;
    }

    public void setRespingi(char respingi) {
        this.respingi = respingi;
    }

    public void setWcomTastoRespingiFormatted(String wcomTastoRespingi) {
        setRespingi(Functions.charAt(wcomTastoRespingi, Types.CHAR_SIZE));
    }

    public char getRespingi() {
        return this.respingi;
    }

    public void setElimina(char elimina) {
        this.elimina = elimina;
    }

    public void setWcomTastoEliminaFormatted(String wcomTastoElimina) {
        setElimina(Functions.charAt(wcomTastoElimina, Types.CHAR_SIZE));
    }

    public char getElimina() {
        return this.elimina;
    }

    public void setAvanti(char avanti) {
        this.avanti = avanti;
    }

    public void setWcomTastoAvantiFormatted(String wcomTastoAvanti) {
        setAvanti(Functions.charAt(wcomTastoAvanti, Types.CHAR_SIZE));
    }

    public char getAvanti() {
        return this.avanti;
    }

    public void setIndietro(char indietro) {
        this.indietro = indietro;
    }

    public void setWcomTastoIndietroFormatted(String wcomTastoIndietro) {
        setIndietro(Functions.charAt(wcomTastoIndietro, Types.CHAR_SIZE));
    }

    public char getIndietro() {
        return this.indietro;
    }

    public void setNote(char note) {
        this.note = note;
    }

    public void setWcomTastoNoteFormatted(String wcomTastoNote) {
        setNote(Functions.charAt(wcomTastoNote, Types.CHAR_SIZE));
    }

    public char getNote() {
        return this.note;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ANNULLA = 1;
        public static final int DA_AUTORIZZARE = 1;
        public static final int AUTORIZZA = 1;
        public static final int AGGIORNA_PTF = 1;
        public static final int RESPINGI = 1;
        public static final int ELIMINA = 1;
        public static final int AVANTI = 1;
        public static final int INDIETRO = 1;
        public static final int NOTE = 1;
        public static final int TASTI_DA_ABILITARE = ANNULLA + DA_AUTORIZZARE + AUTORIZZA + AGGIORNA_PTF + RESPINGI + ELIMINA + AVANTI + INDIETRO + NOTE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
