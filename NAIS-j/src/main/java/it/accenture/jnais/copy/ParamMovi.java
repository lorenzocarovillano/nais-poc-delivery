package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.PmoAaRenCer;
import it.accenture.jnais.ws.redefines.PmoCosOner;
import it.accenture.jnais.ws.redefines.PmoDtRicorPrec;
import it.accenture.jnais.ws.redefines.PmoDtRicorSucc;
import it.accenture.jnais.ws.redefines.PmoDtUltErogManfee;
import it.accenture.jnais.ws.redefines.PmoDurAa;
import it.accenture.jnais.ws.redefines.PmoDurGg;
import it.accenture.jnais.ws.redefines.PmoDurMm;
import it.accenture.jnais.ws.redefines.PmoEtaAaSoglBnficr;
import it.accenture.jnais.ws.redefines.PmoFrqMovi;
import it.accenture.jnais.ws.redefines.PmoIdAdes;
import it.accenture.jnais.ws.redefines.PmoIdMoviChiu;
import it.accenture.jnais.ws.redefines.PmoImpBnsDaSco;
import it.accenture.jnais.ws.redefines.PmoImpBnsDaScoTot;
import it.accenture.jnais.ws.redefines.PmoImpLrdDiRat;
import it.accenture.jnais.ws.redefines.PmoImpRatManfee;
import it.accenture.jnais.ws.redefines.PmoImpRiscParzPrgt;
import it.accenture.jnais.ws.redefines.PmoMmDiff;
import it.accenture.jnais.ws.redefines.PmoNumRatPagPre;
import it.accenture.jnais.ws.redefines.PmoPcAnticBns;
import it.accenture.jnais.ws.redefines.PmoPcApplzOpz;
import it.accenture.jnais.ws.redefines.PmoPcIntrFraz;
import it.accenture.jnais.ws.redefines.PmoPcRevrsb;
import it.accenture.jnais.ws.redefines.PmoPcServVal;
import it.accenture.jnais.ws.redefines.PmoSomAsstaGarac;
import it.accenture.jnais.ws.redefines.PmoSpePc;
import it.accenture.jnais.ws.redefines.PmoTotAaGiaPror;
import it.accenture.jnais.ws.redefines.PmoTpMovi;
import it.accenture.jnais.ws.redefines.PmoUltPcPerd;

/**Original name: PARAM-MOVI<br>
 * Variable: PARAM-MOVI from copybook IDBVPMO1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ParamMovi {

    //==== PROPERTIES ====
    //Original name: PMO-ID-PARAM-MOVI
    private int pmoIdParamMovi = DefaultValues.INT_VAL;
    //Original name: PMO-ID-OGG
    private int pmoIdOgg = DefaultValues.INT_VAL;
    //Original name: PMO-TP-OGG
    private String pmoTpOgg = DefaultValues.stringVal(Len.PMO_TP_OGG);
    //Original name: PMO-ID-MOVI-CRZ
    private int pmoIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: PMO-ID-MOVI-CHIU
    private PmoIdMoviChiu pmoIdMoviChiu = new PmoIdMoviChiu();
    //Original name: PMO-DT-INI-EFF
    private int pmoDtIniEff = DefaultValues.INT_VAL;
    //Original name: PMO-DT-END-EFF
    private int pmoDtEndEff = DefaultValues.INT_VAL;
    //Original name: PMO-COD-COMP-ANIA
    private int pmoCodCompAnia = DefaultValues.INT_VAL;
    //Original name: PMO-TP-MOVI
    private PmoTpMovi pmoTpMovi = new PmoTpMovi();
    //Original name: PMO-FRQ-MOVI
    private PmoFrqMovi pmoFrqMovi = new PmoFrqMovi();
    //Original name: PMO-DUR-AA
    private PmoDurAa pmoDurAa = new PmoDurAa();
    //Original name: PMO-DUR-MM
    private PmoDurMm pmoDurMm = new PmoDurMm();
    //Original name: PMO-DUR-GG
    private PmoDurGg pmoDurGg = new PmoDurGg();
    //Original name: PMO-DT-RICOR-PREC
    private PmoDtRicorPrec pmoDtRicorPrec = new PmoDtRicorPrec();
    //Original name: PMO-DT-RICOR-SUCC
    private PmoDtRicorSucc pmoDtRicorSucc = new PmoDtRicorSucc();
    //Original name: PMO-PC-INTR-FRAZ
    private PmoPcIntrFraz pmoPcIntrFraz = new PmoPcIntrFraz();
    //Original name: PMO-IMP-BNS-DA-SCO-TOT
    private PmoImpBnsDaScoTot pmoImpBnsDaScoTot = new PmoImpBnsDaScoTot();
    //Original name: PMO-IMP-BNS-DA-SCO
    private PmoImpBnsDaSco pmoImpBnsDaSco = new PmoImpBnsDaSco();
    //Original name: PMO-PC-ANTIC-BNS
    private PmoPcAnticBns pmoPcAnticBns = new PmoPcAnticBns();
    //Original name: PMO-TP-RINN-COLL
    private String pmoTpRinnColl = DefaultValues.stringVal(Len.PMO_TP_RINN_COLL);
    //Original name: PMO-TP-RIVAL-PRE
    private String pmoTpRivalPre = DefaultValues.stringVal(Len.PMO_TP_RIVAL_PRE);
    //Original name: PMO-TP-RIVAL-PRSTZ
    private String pmoTpRivalPrstz = DefaultValues.stringVal(Len.PMO_TP_RIVAL_PRSTZ);
    //Original name: PMO-FL-EVID-RIVAL
    private char pmoFlEvidRival = DefaultValues.CHAR_VAL;
    //Original name: PMO-ULT-PC-PERD
    private PmoUltPcPerd pmoUltPcPerd = new PmoUltPcPerd();
    //Original name: PMO-TOT-AA-GIA-PROR
    private PmoTotAaGiaPror pmoTotAaGiaPror = new PmoTotAaGiaPror();
    //Original name: PMO-TP-OPZ
    private String pmoTpOpz = DefaultValues.stringVal(Len.PMO_TP_OPZ);
    //Original name: PMO-AA-REN-CER
    private PmoAaRenCer pmoAaRenCer = new PmoAaRenCer();
    //Original name: PMO-PC-REVRSB
    private PmoPcRevrsb pmoPcRevrsb = new PmoPcRevrsb();
    //Original name: PMO-IMP-RISC-PARZ-PRGT
    private PmoImpRiscParzPrgt pmoImpRiscParzPrgt = new PmoImpRiscParzPrgt();
    //Original name: PMO-IMP-LRD-DI-RAT
    private PmoImpLrdDiRat pmoImpLrdDiRat = new PmoImpLrdDiRat();
    //Original name: PMO-IB-OGG
    private String pmoIbOgg = DefaultValues.stringVal(Len.PMO_IB_OGG);
    //Original name: PMO-COS-ONER
    private PmoCosOner pmoCosOner = new PmoCosOner();
    //Original name: PMO-SPE-PC
    private PmoSpePc pmoSpePc = new PmoSpePc();
    //Original name: PMO-FL-ATTIV-GAR
    private char pmoFlAttivGar = DefaultValues.CHAR_VAL;
    //Original name: PMO-CAMBIO-VER-PROD
    private char pmoCambioVerProd = DefaultValues.CHAR_VAL;
    //Original name: PMO-MM-DIFF
    private PmoMmDiff pmoMmDiff = new PmoMmDiff();
    //Original name: PMO-IMP-RAT-MANFEE
    private PmoImpRatManfee pmoImpRatManfee = new PmoImpRatManfee();
    //Original name: PMO-DT-ULT-EROG-MANFEE
    private PmoDtUltErogManfee pmoDtUltErogManfee = new PmoDtUltErogManfee();
    //Original name: PMO-TP-OGG-RIVAL
    private String pmoTpOggRival = DefaultValues.stringVal(Len.PMO_TP_OGG_RIVAL);
    //Original name: PMO-SOM-ASSTA-GARAC
    private PmoSomAsstaGarac pmoSomAsstaGarac = new PmoSomAsstaGarac();
    //Original name: PMO-PC-APPLZ-OPZ
    private PmoPcApplzOpz pmoPcApplzOpz = new PmoPcApplzOpz();
    //Original name: PMO-ID-ADES
    private PmoIdAdes pmoIdAdes = new PmoIdAdes();
    //Original name: PMO-ID-POLI
    private int pmoIdPoli = DefaultValues.INT_VAL;
    //Original name: PMO-TP-FRM-ASSVA
    private String pmoTpFrmAssva = DefaultValues.stringVal(Len.PMO_TP_FRM_ASSVA);
    //Original name: PMO-DS-RIGA
    private long pmoDsRiga = DefaultValues.LONG_VAL;
    //Original name: PMO-DS-OPER-SQL
    private char pmoDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: PMO-DS-VER
    private int pmoDsVer = DefaultValues.INT_VAL;
    //Original name: PMO-DS-TS-INI-CPTZ
    private long pmoDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: PMO-DS-TS-END-CPTZ
    private long pmoDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: PMO-DS-UTENTE
    private String pmoDsUtente = DefaultValues.stringVal(Len.PMO_DS_UTENTE);
    //Original name: PMO-DS-STATO-ELAB
    private char pmoDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: PMO-TP-ESTR-CNT
    private String pmoTpEstrCnt = DefaultValues.stringVal(Len.PMO_TP_ESTR_CNT);
    //Original name: PMO-COD-RAMO
    private String pmoCodRamo = DefaultValues.stringVal(Len.PMO_COD_RAMO);
    //Original name: PMO-GEN-DA-SIN
    private char pmoGenDaSin = DefaultValues.CHAR_VAL;
    //Original name: PMO-COD-TARI
    private String pmoCodTari = DefaultValues.stringVal(Len.PMO_COD_TARI);
    //Original name: PMO-NUM-RAT-PAG-PRE
    private PmoNumRatPagPre pmoNumRatPagPre = new PmoNumRatPagPre();
    //Original name: PMO-PC-SERV-VAL
    private PmoPcServVal pmoPcServVal = new PmoPcServVal();
    //Original name: PMO-ETA-AA-SOGL-BNFICR
    private PmoEtaAaSoglBnficr pmoEtaAaSoglBnficr = new PmoEtaAaSoglBnficr();

    //==== METHODS ====
    public void setParamMoviFormatted(String data) {
        byte[] buffer = new byte[Len.PARAM_MOVI];
        MarshalByte.writeString(buffer, 1, data, Len.PARAM_MOVI);
        setParamMoviBytes(buffer, 1);
    }

    public String getParamMoviFormatted() {
        return MarshalByteExt.bufferToStr(getParamMoviBytes());
    }

    public byte[] getParamMoviBytes() {
        byte[] buffer = new byte[Len.PARAM_MOVI];
        return getParamMoviBytes(buffer, 1);
    }

    public void setParamMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        pmoIdParamMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PMO_ID_PARAM_MOVI, 0);
        position += Len.PMO_ID_PARAM_MOVI;
        pmoIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PMO_ID_OGG, 0);
        position += Len.PMO_ID_OGG;
        pmoTpOgg = MarshalByte.readString(buffer, position, Len.PMO_TP_OGG);
        position += Len.PMO_TP_OGG;
        pmoIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PMO_ID_MOVI_CRZ, 0);
        position += Len.PMO_ID_MOVI_CRZ;
        pmoIdMoviChiu.setPmoIdMoviChiuFromBuffer(buffer, position);
        position += PmoIdMoviChiu.Len.PMO_ID_MOVI_CHIU;
        pmoDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PMO_DT_INI_EFF, 0);
        position += Len.PMO_DT_INI_EFF;
        pmoDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PMO_DT_END_EFF, 0);
        position += Len.PMO_DT_END_EFF;
        pmoCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PMO_COD_COMP_ANIA, 0);
        position += Len.PMO_COD_COMP_ANIA;
        pmoTpMovi.setPmoTpMoviFromBuffer(buffer, position);
        position += PmoTpMovi.Len.PMO_TP_MOVI;
        pmoFrqMovi.setPmoFrqMoviFromBuffer(buffer, position);
        position += PmoFrqMovi.Len.PMO_FRQ_MOVI;
        pmoDurAa.setPmoDurAaFromBuffer(buffer, position);
        position += PmoDurAa.Len.PMO_DUR_AA;
        pmoDurMm.setPmoDurMmFromBuffer(buffer, position);
        position += PmoDurMm.Len.PMO_DUR_MM;
        pmoDurGg.setPmoDurGgFromBuffer(buffer, position);
        position += PmoDurGg.Len.PMO_DUR_GG;
        pmoDtRicorPrec.setPmoDtRicorPrecFromBuffer(buffer, position);
        position += PmoDtRicorPrec.Len.PMO_DT_RICOR_PREC;
        pmoDtRicorSucc.setPmoDtRicorSuccFromBuffer(buffer, position);
        position += PmoDtRicorSucc.Len.PMO_DT_RICOR_SUCC;
        pmoPcIntrFraz.setPmoPcIntrFrazFromBuffer(buffer, position);
        position += PmoPcIntrFraz.Len.PMO_PC_INTR_FRAZ;
        pmoImpBnsDaScoTot.setPmoImpBnsDaScoTotFromBuffer(buffer, position);
        position += PmoImpBnsDaScoTot.Len.PMO_IMP_BNS_DA_SCO_TOT;
        pmoImpBnsDaSco.setPmoImpBnsDaScoFromBuffer(buffer, position);
        position += PmoImpBnsDaSco.Len.PMO_IMP_BNS_DA_SCO;
        pmoPcAnticBns.setPmoPcAnticBnsFromBuffer(buffer, position);
        position += PmoPcAnticBns.Len.PMO_PC_ANTIC_BNS;
        pmoTpRinnColl = MarshalByte.readString(buffer, position, Len.PMO_TP_RINN_COLL);
        position += Len.PMO_TP_RINN_COLL;
        pmoTpRivalPre = MarshalByte.readString(buffer, position, Len.PMO_TP_RIVAL_PRE);
        position += Len.PMO_TP_RIVAL_PRE;
        pmoTpRivalPrstz = MarshalByte.readString(buffer, position, Len.PMO_TP_RIVAL_PRSTZ);
        position += Len.PMO_TP_RIVAL_PRSTZ;
        pmoFlEvidRival = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pmoUltPcPerd.setPmoUltPcPerdFromBuffer(buffer, position);
        position += PmoUltPcPerd.Len.PMO_ULT_PC_PERD;
        pmoTotAaGiaPror.setPmoTotAaGiaProrFromBuffer(buffer, position);
        position += PmoTotAaGiaPror.Len.PMO_TOT_AA_GIA_PROR;
        pmoTpOpz = MarshalByte.readString(buffer, position, Len.PMO_TP_OPZ);
        position += Len.PMO_TP_OPZ;
        pmoAaRenCer.setPmoAaRenCerFromBuffer(buffer, position);
        position += PmoAaRenCer.Len.PMO_AA_REN_CER;
        pmoPcRevrsb.setPmoPcRevrsbFromBuffer(buffer, position);
        position += PmoPcRevrsb.Len.PMO_PC_REVRSB;
        pmoImpRiscParzPrgt.setPmoImpRiscParzPrgtFromBuffer(buffer, position);
        position += PmoImpRiscParzPrgt.Len.PMO_IMP_RISC_PARZ_PRGT;
        pmoImpLrdDiRat.setPmoImpLrdDiRatFromBuffer(buffer, position);
        position += PmoImpLrdDiRat.Len.PMO_IMP_LRD_DI_RAT;
        pmoIbOgg = MarshalByte.readString(buffer, position, Len.PMO_IB_OGG);
        position += Len.PMO_IB_OGG;
        pmoCosOner.setPmoCosOnerFromBuffer(buffer, position);
        position += PmoCosOner.Len.PMO_COS_ONER;
        pmoSpePc.setPmoSpePcFromBuffer(buffer, position);
        position += PmoSpePc.Len.PMO_SPE_PC;
        pmoFlAttivGar = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pmoCambioVerProd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pmoMmDiff.setPmoMmDiffFromBuffer(buffer, position);
        position += PmoMmDiff.Len.PMO_MM_DIFF;
        pmoImpRatManfee.setPmoImpRatManfeeFromBuffer(buffer, position);
        position += PmoImpRatManfee.Len.PMO_IMP_RAT_MANFEE;
        pmoDtUltErogManfee.setPmoDtUltErogManfeeFromBuffer(buffer, position);
        position += PmoDtUltErogManfee.Len.PMO_DT_ULT_EROG_MANFEE;
        pmoTpOggRival = MarshalByte.readString(buffer, position, Len.PMO_TP_OGG_RIVAL);
        position += Len.PMO_TP_OGG_RIVAL;
        pmoSomAsstaGarac.setPmoSomAsstaGaracFromBuffer(buffer, position);
        position += PmoSomAsstaGarac.Len.PMO_SOM_ASSTA_GARAC;
        pmoPcApplzOpz.setPmoPcApplzOpzFromBuffer(buffer, position);
        position += PmoPcApplzOpz.Len.PMO_PC_APPLZ_OPZ;
        pmoIdAdes.setPmoIdAdesFromBuffer(buffer, position);
        position += PmoIdAdes.Len.PMO_ID_ADES;
        pmoIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PMO_ID_POLI, 0);
        position += Len.PMO_ID_POLI;
        pmoTpFrmAssva = MarshalByte.readString(buffer, position, Len.PMO_TP_FRM_ASSVA);
        position += Len.PMO_TP_FRM_ASSVA;
        pmoDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PMO_DS_RIGA, 0);
        position += Len.PMO_DS_RIGA;
        pmoDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pmoDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PMO_DS_VER, 0);
        position += Len.PMO_DS_VER;
        pmoDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PMO_DS_TS_INI_CPTZ, 0);
        position += Len.PMO_DS_TS_INI_CPTZ;
        pmoDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PMO_DS_TS_END_CPTZ, 0);
        position += Len.PMO_DS_TS_END_CPTZ;
        pmoDsUtente = MarshalByte.readString(buffer, position, Len.PMO_DS_UTENTE);
        position += Len.PMO_DS_UTENTE;
        pmoDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pmoTpEstrCnt = MarshalByte.readString(buffer, position, Len.PMO_TP_ESTR_CNT);
        position += Len.PMO_TP_ESTR_CNT;
        pmoCodRamo = MarshalByte.readString(buffer, position, Len.PMO_COD_RAMO);
        position += Len.PMO_COD_RAMO;
        pmoGenDaSin = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pmoCodTari = MarshalByte.readString(buffer, position, Len.PMO_COD_TARI);
        position += Len.PMO_COD_TARI;
        pmoNumRatPagPre.setPmoNumRatPagPreFromBuffer(buffer, position);
        position += PmoNumRatPagPre.Len.PMO_NUM_RAT_PAG_PRE;
        pmoPcServVal.setPmoPcServValFromBuffer(buffer, position);
        position += PmoPcServVal.Len.PMO_PC_SERV_VAL;
        pmoEtaAaSoglBnficr.setPmoEtaAaSoglBnficrFromBuffer(buffer, position);
    }

    public byte[] getParamMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, pmoIdParamMovi, Len.Int.PMO_ID_PARAM_MOVI, 0);
        position += Len.PMO_ID_PARAM_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, pmoIdOgg, Len.Int.PMO_ID_OGG, 0);
        position += Len.PMO_ID_OGG;
        MarshalByte.writeString(buffer, position, pmoTpOgg, Len.PMO_TP_OGG);
        position += Len.PMO_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, pmoIdMoviCrz, Len.Int.PMO_ID_MOVI_CRZ, 0);
        position += Len.PMO_ID_MOVI_CRZ;
        pmoIdMoviChiu.getPmoIdMoviChiuAsBuffer(buffer, position);
        position += PmoIdMoviChiu.Len.PMO_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, pmoDtIniEff, Len.Int.PMO_DT_INI_EFF, 0);
        position += Len.PMO_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, pmoDtEndEff, Len.Int.PMO_DT_END_EFF, 0);
        position += Len.PMO_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, pmoCodCompAnia, Len.Int.PMO_COD_COMP_ANIA, 0);
        position += Len.PMO_COD_COMP_ANIA;
        pmoTpMovi.getPmoTpMoviAsBuffer(buffer, position);
        position += PmoTpMovi.Len.PMO_TP_MOVI;
        pmoFrqMovi.getPmoFrqMoviAsBuffer(buffer, position);
        position += PmoFrqMovi.Len.PMO_FRQ_MOVI;
        pmoDurAa.getPmoDurAaAsBuffer(buffer, position);
        position += PmoDurAa.Len.PMO_DUR_AA;
        pmoDurMm.getPmoDurMmAsBuffer(buffer, position);
        position += PmoDurMm.Len.PMO_DUR_MM;
        pmoDurGg.getPmoDurGgAsBuffer(buffer, position);
        position += PmoDurGg.Len.PMO_DUR_GG;
        pmoDtRicorPrec.getPmoDtRicorPrecAsBuffer(buffer, position);
        position += PmoDtRicorPrec.Len.PMO_DT_RICOR_PREC;
        pmoDtRicorSucc.getPmoDtRicorSuccAsBuffer(buffer, position);
        position += PmoDtRicorSucc.Len.PMO_DT_RICOR_SUCC;
        pmoPcIntrFraz.getPmoPcIntrFrazAsBuffer(buffer, position);
        position += PmoPcIntrFraz.Len.PMO_PC_INTR_FRAZ;
        pmoImpBnsDaScoTot.getPmoImpBnsDaScoTotAsBuffer(buffer, position);
        position += PmoImpBnsDaScoTot.Len.PMO_IMP_BNS_DA_SCO_TOT;
        pmoImpBnsDaSco.getPmoImpBnsDaScoAsBuffer(buffer, position);
        position += PmoImpBnsDaSco.Len.PMO_IMP_BNS_DA_SCO;
        pmoPcAnticBns.getPmoPcAnticBnsAsBuffer(buffer, position);
        position += PmoPcAnticBns.Len.PMO_PC_ANTIC_BNS;
        MarshalByte.writeString(buffer, position, pmoTpRinnColl, Len.PMO_TP_RINN_COLL);
        position += Len.PMO_TP_RINN_COLL;
        MarshalByte.writeString(buffer, position, pmoTpRivalPre, Len.PMO_TP_RIVAL_PRE);
        position += Len.PMO_TP_RIVAL_PRE;
        MarshalByte.writeString(buffer, position, pmoTpRivalPrstz, Len.PMO_TP_RIVAL_PRSTZ);
        position += Len.PMO_TP_RIVAL_PRSTZ;
        MarshalByte.writeChar(buffer, position, pmoFlEvidRival);
        position += Types.CHAR_SIZE;
        pmoUltPcPerd.getPmoUltPcPerdAsBuffer(buffer, position);
        position += PmoUltPcPerd.Len.PMO_ULT_PC_PERD;
        pmoTotAaGiaPror.getPmoTotAaGiaProrAsBuffer(buffer, position);
        position += PmoTotAaGiaPror.Len.PMO_TOT_AA_GIA_PROR;
        MarshalByte.writeString(buffer, position, pmoTpOpz, Len.PMO_TP_OPZ);
        position += Len.PMO_TP_OPZ;
        pmoAaRenCer.getPmoAaRenCerAsBuffer(buffer, position);
        position += PmoAaRenCer.Len.PMO_AA_REN_CER;
        pmoPcRevrsb.getPmoPcRevrsbAsBuffer(buffer, position);
        position += PmoPcRevrsb.Len.PMO_PC_REVRSB;
        pmoImpRiscParzPrgt.getPmoImpRiscParzPrgtAsBuffer(buffer, position);
        position += PmoImpRiscParzPrgt.Len.PMO_IMP_RISC_PARZ_PRGT;
        pmoImpLrdDiRat.getPmoImpLrdDiRatAsBuffer(buffer, position);
        position += PmoImpLrdDiRat.Len.PMO_IMP_LRD_DI_RAT;
        MarshalByte.writeString(buffer, position, pmoIbOgg, Len.PMO_IB_OGG);
        position += Len.PMO_IB_OGG;
        pmoCosOner.getPmoCosOnerAsBuffer(buffer, position);
        position += PmoCosOner.Len.PMO_COS_ONER;
        pmoSpePc.getPmoSpePcAsBuffer(buffer, position);
        position += PmoSpePc.Len.PMO_SPE_PC;
        MarshalByte.writeChar(buffer, position, pmoFlAttivGar);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, pmoCambioVerProd);
        position += Types.CHAR_SIZE;
        pmoMmDiff.getPmoMmDiffAsBuffer(buffer, position);
        position += PmoMmDiff.Len.PMO_MM_DIFF;
        pmoImpRatManfee.getPmoImpRatManfeeAsBuffer(buffer, position);
        position += PmoImpRatManfee.Len.PMO_IMP_RAT_MANFEE;
        pmoDtUltErogManfee.getPmoDtUltErogManfeeAsBuffer(buffer, position);
        position += PmoDtUltErogManfee.Len.PMO_DT_ULT_EROG_MANFEE;
        MarshalByte.writeString(buffer, position, pmoTpOggRival, Len.PMO_TP_OGG_RIVAL);
        position += Len.PMO_TP_OGG_RIVAL;
        pmoSomAsstaGarac.getPmoSomAsstaGaracAsBuffer(buffer, position);
        position += PmoSomAsstaGarac.Len.PMO_SOM_ASSTA_GARAC;
        pmoPcApplzOpz.getPmoPcApplzOpzAsBuffer(buffer, position);
        position += PmoPcApplzOpz.Len.PMO_PC_APPLZ_OPZ;
        pmoIdAdes.getPmoIdAdesAsBuffer(buffer, position);
        position += PmoIdAdes.Len.PMO_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, pmoIdPoli, Len.Int.PMO_ID_POLI, 0);
        position += Len.PMO_ID_POLI;
        MarshalByte.writeString(buffer, position, pmoTpFrmAssva, Len.PMO_TP_FRM_ASSVA);
        position += Len.PMO_TP_FRM_ASSVA;
        MarshalByte.writeLongAsPacked(buffer, position, pmoDsRiga, Len.Int.PMO_DS_RIGA, 0);
        position += Len.PMO_DS_RIGA;
        MarshalByte.writeChar(buffer, position, pmoDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, pmoDsVer, Len.Int.PMO_DS_VER, 0);
        position += Len.PMO_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, pmoDsTsIniCptz, Len.Int.PMO_DS_TS_INI_CPTZ, 0);
        position += Len.PMO_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, pmoDsTsEndCptz, Len.Int.PMO_DS_TS_END_CPTZ, 0);
        position += Len.PMO_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, pmoDsUtente, Len.PMO_DS_UTENTE);
        position += Len.PMO_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, pmoDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, pmoTpEstrCnt, Len.PMO_TP_ESTR_CNT);
        position += Len.PMO_TP_ESTR_CNT;
        MarshalByte.writeString(buffer, position, pmoCodRamo, Len.PMO_COD_RAMO);
        position += Len.PMO_COD_RAMO;
        MarshalByte.writeChar(buffer, position, pmoGenDaSin);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, pmoCodTari, Len.PMO_COD_TARI);
        position += Len.PMO_COD_TARI;
        pmoNumRatPagPre.getPmoNumRatPagPreAsBuffer(buffer, position);
        position += PmoNumRatPagPre.Len.PMO_NUM_RAT_PAG_PRE;
        pmoPcServVal.getPmoPcServValAsBuffer(buffer, position);
        position += PmoPcServVal.Len.PMO_PC_SERV_VAL;
        pmoEtaAaSoglBnficr.getPmoEtaAaSoglBnficrAsBuffer(buffer, position);
        return buffer;
    }

    public void setPmoIdParamMovi(int pmoIdParamMovi) {
        this.pmoIdParamMovi = pmoIdParamMovi;
    }

    public int getPmoIdParamMovi() {
        return this.pmoIdParamMovi;
    }

    public void setPmoIdOgg(int pmoIdOgg) {
        this.pmoIdOgg = pmoIdOgg;
    }

    public int getPmoIdOgg() {
        return this.pmoIdOgg;
    }

    public void setPmoTpOgg(String pmoTpOgg) {
        this.pmoTpOgg = Functions.subString(pmoTpOgg, Len.PMO_TP_OGG);
    }

    public String getPmoTpOgg() {
        return this.pmoTpOgg;
    }

    public void setPmoIdMoviCrz(int pmoIdMoviCrz) {
        this.pmoIdMoviCrz = pmoIdMoviCrz;
    }

    public int getPmoIdMoviCrz() {
        return this.pmoIdMoviCrz;
    }

    public void setPmoDtIniEff(int pmoDtIniEff) {
        this.pmoDtIniEff = pmoDtIniEff;
    }

    public int getPmoDtIniEff() {
        return this.pmoDtIniEff;
    }

    public void setPmoDtEndEff(int pmoDtEndEff) {
        this.pmoDtEndEff = pmoDtEndEff;
    }

    public int getPmoDtEndEff() {
        return this.pmoDtEndEff;
    }

    public void setPmoCodCompAnia(int pmoCodCompAnia) {
        this.pmoCodCompAnia = pmoCodCompAnia;
    }

    public int getPmoCodCompAnia() {
        return this.pmoCodCompAnia;
    }

    public void setPmoTpRinnColl(String pmoTpRinnColl) {
        this.pmoTpRinnColl = Functions.subString(pmoTpRinnColl, Len.PMO_TP_RINN_COLL);
    }

    public String getPmoTpRinnColl() {
        return this.pmoTpRinnColl;
    }

    public String getPmoTpRinnCollFormatted() {
        return Functions.padBlanks(getPmoTpRinnColl(), Len.PMO_TP_RINN_COLL);
    }

    public void setPmoTpRivalPre(String pmoTpRivalPre) {
        this.pmoTpRivalPre = Functions.subString(pmoTpRivalPre, Len.PMO_TP_RIVAL_PRE);
    }

    public String getPmoTpRivalPre() {
        return this.pmoTpRivalPre;
    }

    public String getPmoTpRivalPreFormatted() {
        return Functions.padBlanks(getPmoTpRivalPre(), Len.PMO_TP_RIVAL_PRE);
    }

    public void setPmoTpRivalPrstz(String pmoTpRivalPrstz) {
        this.pmoTpRivalPrstz = Functions.subString(pmoTpRivalPrstz, Len.PMO_TP_RIVAL_PRSTZ);
    }

    public String getPmoTpRivalPrstz() {
        return this.pmoTpRivalPrstz;
    }

    public String getPmoTpRivalPrstzFormatted() {
        return Functions.padBlanks(getPmoTpRivalPrstz(), Len.PMO_TP_RIVAL_PRSTZ);
    }

    public void setPmoFlEvidRival(char pmoFlEvidRival) {
        this.pmoFlEvidRival = pmoFlEvidRival;
    }

    public char getPmoFlEvidRival() {
        return this.pmoFlEvidRival;
    }

    public void setPmoTpOpz(String pmoTpOpz) {
        this.pmoTpOpz = Functions.subString(pmoTpOpz, Len.PMO_TP_OPZ);
    }

    public String getPmoTpOpz() {
        return this.pmoTpOpz;
    }

    public String getPmoTpOpzFormatted() {
        return Functions.padBlanks(getPmoTpOpz(), Len.PMO_TP_OPZ);
    }

    public void setPmoIbOgg(String pmoIbOgg) {
        this.pmoIbOgg = Functions.subString(pmoIbOgg, Len.PMO_IB_OGG);
    }

    public String getPmoIbOgg() {
        return this.pmoIbOgg;
    }

    public void setPmoFlAttivGar(char pmoFlAttivGar) {
        this.pmoFlAttivGar = pmoFlAttivGar;
    }

    public char getPmoFlAttivGar() {
        return this.pmoFlAttivGar;
    }

    public void setPmoCambioVerProd(char pmoCambioVerProd) {
        this.pmoCambioVerProd = pmoCambioVerProd;
    }

    public char getPmoCambioVerProd() {
        return this.pmoCambioVerProd;
    }

    public void setPmoTpOggRival(String pmoTpOggRival) {
        this.pmoTpOggRival = Functions.subString(pmoTpOggRival, Len.PMO_TP_OGG_RIVAL);
    }

    public String getPmoTpOggRival() {
        return this.pmoTpOggRival;
    }

    public String getPmoTpOggRivalFormatted() {
        return Functions.padBlanks(getPmoTpOggRival(), Len.PMO_TP_OGG_RIVAL);
    }

    public void setPmoIdPoli(int pmoIdPoli) {
        this.pmoIdPoli = pmoIdPoli;
    }

    public int getPmoIdPoli() {
        return this.pmoIdPoli;
    }

    public void setPmoTpFrmAssva(String pmoTpFrmAssva) {
        this.pmoTpFrmAssva = Functions.subString(pmoTpFrmAssva, Len.PMO_TP_FRM_ASSVA);
    }

    public String getPmoTpFrmAssva() {
        return this.pmoTpFrmAssva;
    }

    public void setPmoDsRiga(long pmoDsRiga) {
        this.pmoDsRiga = pmoDsRiga;
    }

    public long getPmoDsRiga() {
        return this.pmoDsRiga;
    }

    public void setPmoDsOperSql(char pmoDsOperSql) {
        this.pmoDsOperSql = pmoDsOperSql;
    }

    public char getPmoDsOperSql() {
        return this.pmoDsOperSql;
    }

    public void setPmoDsVer(int pmoDsVer) {
        this.pmoDsVer = pmoDsVer;
    }

    public int getPmoDsVer() {
        return this.pmoDsVer;
    }

    public void setPmoDsTsIniCptz(long pmoDsTsIniCptz) {
        this.pmoDsTsIniCptz = pmoDsTsIniCptz;
    }

    public long getPmoDsTsIniCptz() {
        return this.pmoDsTsIniCptz;
    }

    public void setPmoDsTsEndCptz(long pmoDsTsEndCptz) {
        this.pmoDsTsEndCptz = pmoDsTsEndCptz;
    }

    public long getPmoDsTsEndCptz() {
        return this.pmoDsTsEndCptz;
    }

    public void setPmoDsUtente(String pmoDsUtente) {
        this.pmoDsUtente = Functions.subString(pmoDsUtente, Len.PMO_DS_UTENTE);
    }

    public String getPmoDsUtente() {
        return this.pmoDsUtente;
    }

    public void setPmoDsStatoElab(char pmoDsStatoElab) {
        this.pmoDsStatoElab = pmoDsStatoElab;
    }

    public char getPmoDsStatoElab() {
        return this.pmoDsStatoElab;
    }

    public void setPmoTpEstrCnt(String pmoTpEstrCnt) {
        this.pmoTpEstrCnt = Functions.subString(pmoTpEstrCnt, Len.PMO_TP_ESTR_CNT);
    }

    public String getPmoTpEstrCnt() {
        return this.pmoTpEstrCnt;
    }

    public String getPmoTpEstrCntFormatted() {
        return Functions.padBlanks(getPmoTpEstrCnt(), Len.PMO_TP_ESTR_CNT);
    }

    public void setPmoCodRamo(String pmoCodRamo) {
        this.pmoCodRamo = Functions.subString(pmoCodRamo, Len.PMO_COD_RAMO);
    }

    public String getPmoCodRamo() {
        return this.pmoCodRamo;
    }

    public String getPmoCodRamoFormatted() {
        return Functions.padBlanks(getPmoCodRamo(), Len.PMO_COD_RAMO);
    }

    public void setPmoGenDaSin(char pmoGenDaSin) {
        this.pmoGenDaSin = pmoGenDaSin;
    }

    public char getPmoGenDaSin() {
        return this.pmoGenDaSin;
    }

    public void setPmoCodTari(String pmoCodTari) {
        this.pmoCodTari = Functions.subString(pmoCodTari, Len.PMO_COD_TARI);
    }

    public String getPmoCodTari() {
        return this.pmoCodTari;
    }

    public String getPmoCodTariFormatted() {
        return Functions.padBlanks(getPmoCodTari(), Len.PMO_COD_TARI);
    }

    public PmoAaRenCer getPmoAaRenCer() {
        return pmoAaRenCer;
    }

    public PmoCosOner getPmoCosOner() {
        return pmoCosOner;
    }

    public PmoDtRicorPrec getPmoDtRicorPrec() {
        return pmoDtRicorPrec;
    }

    public PmoDtRicorSucc getPmoDtRicorSucc() {
        return pmoDtRicorSucc;
    }

    public PmoDtUltErogManfee getPmoDtUltErogManfee() {
        return pmoDtUltErogManfee;
    }

    public PmoDurAa getPmoDurAa() {
        return pmoDurAa;
    }

    public PmoDurGg getPmoDurGg() {
        return pmoDurGg;
    }

    public PmoDurMm getPmoDurMm() {
        return pmoDurMm;
    }

    public PmoEtaAaSoglBnficr getPmoEtaAaSoglBnficr() {
        return pmoEtaAaSoglBnficr;
    }

    public PmoFrqMovi getPmoFrqMovi() {
        return pmoFrqMovi;
    }

    public PmoIdAdes getPmoIdAdes() {
        return pmoIdAdes;
    }

    public PmoIdMoviChiu getPmoIdMoviChiu() {
        return pmoIdMoviChiu;
    }

    public PmoImpBnsDaSco getPmoImpBnsDaSco() {
        return pmoImpBnsDaSco;
    }

    public PmoImpBnsDaScoTot getPmoImpBnsDaScoTot() {
        return pmoImpBnsDaScoTot;
    }

    public PmoImpLrdDiRat getPmoImpLrdDiRat() {
        return pmoImpLrdDiRat;
    }

    public PmoImpRatManfee getPmoImpRatManfee() {
        return pmoImpRatManfee;
    }

    public PmoImpRiscParzPrgt getPmoImpRiscParzPrgt() {
        return pmoImpRiscParzPrgt;
    }

    public PmoMmDiff getPmoMmDiff() {
        return pmoMmDiff;
    }

    public PmoNumRatPagPre getPmoNumRatPagPre() {
        return pmoNumRatPagPre;
    }

    public PmoPcAnticBns getPmoPcAnticBns() {
        return pmoPcAnticBns;
    }

    public PmoPcApplzOpz getPmoPcApplzOpz() {
        return pmoPcApplzOpz;
    }

    public PmoPcIntrFraz getPmoPcIntrFraz() {
        return pmoPcIntrFraz;
    }

    public PmoPcRevrsb getPmoPcRevrsb() {
        return pmoPcRevrsb;
    }

    public PmoPcServVal getPmoPcServVal() {
        return pmoPcServVal;
    }

    public PmoSomAsstaGarac getPmoSomAsstaGarac() {
        return pmoSomAsstaGarac;
    }

    public PmoSpePc getPmoSpePc() {
        return pmoSpePc;
    }

    public PmoTotAaGiaPror getPmoTotAaGiaPror() {
        return pmoTotAaGiaPror;
    }

    public PmoTpMovi getPmoTpMovi() {
        return pmoTpMovi;
    }

    public PmoUltPcPerd getPmoUltPcPerd() {
        return pmoUltPcPerd;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_TP_OGG = 2;
        public static final int PMO_TP_RINN_COLL = 2;
        public static final int PMO_TP_RIVAL_PRE = 2;
        public static final int PMO_TP_RIVAL_PRSTZ = 2;
        public static final int PMO_TP_OPZ = 12;
        public static final int PMO_IB_OGG = 40;
        public static final int PMO_TP_OGG_RIVAL = 2;
        public static final int PMO_TP_FRM_ASSVA = 2;
        public static final int PMO_DS_UTENTE = 20;
        public static final int PMO_TP_ESTR_CNT = 2;
        public static final int PMO_COD_RAMO = 12;
        public static final int PMO_COD_TARI = 12;
        public static final int PMO_ID_PARAM_MOVI = 5;
        public static final int PMO_ID_OGG = 5;
        public static final int PMO_ID_MOVI_CRZ = 5;
        public static final int PMO_DT_INI_EFF = 5;
        public static final int PMO_DT_END_EFF = 5;
        public static final int PMO_COD_COMP_ANIA = 3;
        public static final int PMO_FL_EVID_RIVAL = 1;
        public static final int PMO_FL_ATTIV_GAR = 1;
        public static final int PMO_CAMBIO_VER_PROD = 1;
        public static final int PMO_ID_POLI = 5;
        public static final int PMO_DS_RIGA = 6;
        public static final int PMO_DS_OPER_SQL = 1;
        public static final int PMO_DS_VER = 5;
        public static final int PMO_DS_TS_INI_CPTZ = 10;
        public static final int PMO_DS_TS_END_CPTZ = 10;
        public static final int PMO_DS_STATO_ELAB = 1;
        public static final int PMO_GEN_DA_SIN = 1;
        public static final int PARAM_MOVI = PMO_ID_PARAM_MOVI + PMO_ID_OGG + PMO_TP_OGG + PMO_ID_MOVI_CRZ + PmoIdMoviChiu.Len.PMO_ID_MOVI_CHIU + PMO_DT_INI_EFF + PMO_DT_END_EFF + PMO_COD_COMP_ANIA + PmoTpMovi.Len.PMO_TP_MOVI + PmoFrqMovi.Len.PMO_FRQ_MOVI + PmoDurAa.Len.PMO_DUR_AA + PmoDurMm.Len.PMO_DUR_MM + PmoDurGg.Len.PMO_DUR_GG + PmoDtRicorPrec.Len.PMO_DT_RICOR_PREC + PmoDtRicorSucc.Len.PMO_DT_RICOR_SUCC + PmoPcIntrFraz.Len.PMO_PC_INTR_FRAZ + PmoImpBnsDaScoTot.Len.PMO_IMP_BNS_DA_SCO_TOT + PmoImpBnsDaSco.Len.PMO_IMP_BNS_DA_SCO + PmoPcAnticBns.Len.PMO_PC_ANTIC_BNS + PMO_TP_RINN_COLL + PMO_TP_RIVAL_PRE + PMO_TP_RIVAL_PRSTZ + PMO_FL_EVID_RIVAL + PmoUltPcPerd.Len.PMO_ULT_PC_PERD + PmoTotAaGiaPror.Len.PMO_TOT_AA_GIA_PROR + PMO_TP_OPZ + PmoAaRenCer.Len.PMO_AA_REN_CER + PmoPcRevrsb.Len.PMO_PC_REVRSB + PmoImpRiscParzPrgt.Len.PMO_IMP_RISC_PARZ_PRGT + PmoImpLrdDiRat.Len.PMO_IMP_LRD_DI_RAT + PMO_IB_OGG + PmoCosOner.Len.PMO_COS_ONER + PmoSpePc.Len.PMO_SPE_PC + PMO_FL_ATTIV_GAR + PMO_CAMBIO_VER_PROD + PmoMmDiff.Len.PMO_MM_DIFF + PmoImpRatManfee.Len.PMO_IMP_RAT_MANFEE + PmoDtUltErogManfee.Len.PMO_DT_ULT_EROG_MANFEE + PMO_TP_OGG_RIVAL + PmoSomAsstaGarac.Len.PMO_SOM_ASSTA_GARAC + PmoPcApplzOpz.Len.PMO_PC_APPLZ_OPZ + PmoIdAdes.Len.PMO_ID_ADES + PMO_ID_POLI + PMO_TP_FRM_ASSVA + PMO_DS_RIGA + PMO_DS_OPER_SQL + PMO_DS_VER + PMO_DS_TS_INI_CPTZ + PMO_DS_TS_END_CPTZ + PMO_DS_UTENTE + PMO_DS_STATO_ELAB + PMO_TP_ESTR_CNT + PMO_COD_RAMO + PMO_GEN_DA_SIN + PMO_COD_TARI + PmoNumRatPagPre.Len.PMO_NUM_RAT_PAG_PRE + PmoPcServVal.Len.PMO_PC_SERV_VAL + PmoEtaAaSoglBnficr.Len.PMO_ETA_AA_SOGL_BNFICR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_ID_PARAM_MOVI = 9;
            public static final int PMO_ID_OGG = 9;
            public static final int PMO_ID_MOVI_CRZ = 9;
            public static final int PMO_DT_INI_EFF = 8;
            public static final int PMO_DT_END_EFF = 8;
            public static final int PMO_COD_COMP_ANIA = 5;
            public static final int PMO_ID_POLI = 9;
            public static final int PMO_DS_RIGA = 10;
            public static final int PMO_DS_VER = 9;
            public static final int PMO_DS_TS_INI_CPTZ = 18;
            public static final int PMO_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
