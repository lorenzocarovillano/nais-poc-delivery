package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: PREST-DB<br>
 * Variable: PREST-DB from copybook IDBVPRE3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class PrestDb {

    //==== PROPERTIES ====
    //Original name: PRE-DT-INI-EFF-DB
    private String iniEffDb = DefaultValues.stringVal(Len.INI_EFF_DB);
    //Original name: PRE-DT-END-EFF-DB
    private String endEffDb = DefaultValues.stringVal(Len.END_EFF_DB);
    //Original name: PRE-DT-CONCS-PREST-DB
    private String concsPrestDb = DefaultValues.stringVal(Len.CONCS_PREST_DB);
    //Original name: PRE-DT-DECOR-PREST-DB
    private String decorPrestDb = DefaultValues.stringVal(Len.DECOR_PREST_DB);
    //Original name: PRE-DT-RIMB-DB
    private String rimbDb = DefaultValues.stringVal(Len.RIMB_DB);
    //Original name: PRE-DT-RICH-PREST-DB
    private String richPrestDb = DefaultValues.stringVal(Len.RICH_PREST_DB);

    //==== METHODS ====
    public void setIniEffDb(String iniEffDb) {
        this.iniEffDb = Functions.subString(iniEffDb, Len.INI_EFF_DB);
    }

    public String getIniEffDb() {
        return this.iniEffDb;
    }

    public void setEndEffDb(String endEffDb) {
        this.endEffDb = Functions.subString(endEffDb, Len.END_EFF_DB);
    }

    public String getEndEffDb() {
        return this.endEffDb;
    }

    public void setConcsPrestDb(String concsPrestDb) {
        this.concsPrestDb = Functions.subString(concsPrestDb, Len.CONCS_PREST_DB);
    }

    public String getConcsPrestDb() {
        return this.concsPrestDb;
    }

    public void setDecorPrestDb(String decorPrestDb) {
        this.decorPrestDb = Functions.subString(decorPrestDb, Len.DECOR_PREST_DB);
    }

    public String getDecorPrestDb() {
        return this.decorPrestDb;
    }

    public void setRimbDb(String rimbDb) {
        this.rimbDb = Functions.subString(rimbDb, Len.RIMB_DB);
    }

    public String getRimbDb() {
        return this.rimbDb;
    }

    public void setRichPrestDb(String richPrestDb) {
        this.richPrestDb = Functions.subString(richPrestDb, Len.RICH_PREST_DB);
    }

    public String getRichPrestDb() {
        return this.richPrestDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INI_EFF_DB = 10;
        public static final int END_EFF_DB = 10;
        public static final int CONCS_PREST_DB = 10;
        public static final int DECOR_PREST_DB = 10;
        public static final int RIMB_DB = 10;
        public static final int RICH_PREST_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
