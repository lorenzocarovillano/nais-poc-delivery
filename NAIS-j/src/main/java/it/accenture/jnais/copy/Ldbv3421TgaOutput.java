package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.L3421AbbAnnuUlt;
import it.accenture.jnais.ws.redefines.L3421AbbTotIni;
import it.accenture.jnais.ws.redefines.L3421AbbTotUlt;
import it.accenture.jnais.ws.redefines.L3421AcqExp;
import it.accenture.jnais.ws.redefines.L3421AlqCommisInter;
import it.accenture.jnais.ws.redefines.L3421AlqProvAcq;
import it.accenture.jnais.ws.redefines.L3421AlqProvInc;
import it.accenture.jnais.ws.redefines.L3421AlqProvRicor;
import it.accenture.jnais.ws.redefines.L3421AlqRemunAss;
import it.accenture.jnais.ws.redefines.L3421AlqScon;
import it.accenture.jnais.ws.redefines.L3421BnsGiaLiqto;
import it.accenture.jnais.ws.redefines.L3421CommisInter;
import it.accenture.jnais.ws.redefines.L3421CosRunAssva;
import it.accenture.jnais.ws.redefines.L3421CosRunAssvaIdc;
import it.accenture.jnais.ws.redefines.L3421CptInOpzRivto;
import it.accenture.jnais.ws.redefines.L3421CptRshMor;
import it.accenture.jnais.ws.redefines.L3421DtEffStab;
import it.accenture.jnais.ws.redefines.L3421DtEmis;
import it.accenture.jnais.ws.redefines.L3421DtIniValTar;
import it.accenture.jnais.ws.redefines.L3421DtScad;
import it.accenture.jnais.ws.redefines.L3421DtUltAdegPrePr;
import it.accenture.jnais.ws.redefines.L3421DtVldtProd;
import it.accenture.jnais.ws.redefines.L3421DurAa;
import it.accenture.jnais.ws.redefines.L3421DurAbb;
import it.accenture.jnais.ws.redefines.L3421DurGg;
import it.accenture.jnais.ws.redefines.L3421DurMm;
import it.accenture.jnais.ws.redefines.L3421EtaAa1oAssto;
import it.accenture.jnais.ws.redefines.L3421EtaAa2oAssto;
import it.accenture.jnais.ws.redefines.L3421EtaAa3oAssto;
import it.accenture.jnais.ws.redefines.L3421EtaMm1oAssto;
import it.accenture.jnais.ws.redefines.L3421EtaMm2oAssto;
import it.accenture.jnais.ws.redefines.L3421EtaMm3oAssto;
import it.accenture.jnais.ws.redefines.L3421IdMoviChiu;
import it.accenture.jnais.ws.redefines.L3421ImpAder;
import it.accenture.jnais.ws.redefines.L3421ImpAltSopr;
import it.accenture.jnais.ws.redefines.L3421ImpAz;
import it.accenture.jnais.ws.redefines.L3421ImpbCommisInter;
import it.accenture.jnais.ws.redefines.L3421ImpBns;
import it.accenture.jnais.ws.redefines.L3421ImpBnsAntic;
import it.accenture.jnais.ws.redefines.L3421ImpbProvAcq;
import it.accenture.jnais.ws.redefines.L3421ImpbProvInc;
import it.accenture.jnais.ws.redefines.L3421ImpbProvRicor;
import it.accenture.jnais.ws.redefines.L3421ImpbRemunAss;
import it.accenture.jnais.ws.redefines.L3421ImpbVisEnd2000;
import it.accenture.jnais.ws.redefines.L3421ImpCarAcq;
import it.accenture.jnais.ws.redefines.L3421ImpCarGest;
import it.accenture.jnais.ws.redefines.L3421ImpCarInc;
import it.accenture.jnais.ws.redefines.L3421ImpScon;
import it.accenture.jnais.ws.redefines.L3421ImpSoprProf;
import it.accenture.jnais.ws.redefines.L3421ImpSoprSan;
import it.accenture.jnais.ws.redefines.L3421ImpSoprSpo;
import it.accenture.jnais.ws.redefines.L3421ImpSoprTec;
import it.accenture.jnais.ws.redefines.L3421ImpTfr;
import it.accenture.jnais.ws.redefines.L3421ImpTfrStrc;
import it.accenture.jnais.ws.redefines.L3421ImpTrasfe;
import it.accenture.jnais.ws.redefines.L3421ImpVolo;
import it.accenture.jnais.ws.redefines.L3421IncrPre;
import it.accenture.jnais.ws.redefines.L3421IncrPrstz;
import it.accenture.jnais.ws.redefines.L3421IntrMora;
import it.accenture.jnais.ws.redefines.L3421ManfeeAntic;
import it.accenture.jnais.ws.redefines.L3421ManfeeRicor;
import it.accenture.jnais.ws.redefines.L3421MatuEnd2000;
import it.accenture.jnais.ws.redefines.L3421MinGarto;
import it.accenture.jnais.ws.redefines.L3421MinTrnut;
import it.accenture.jnais.ws.redefines.L3421NumGgRival;
import it.accenture.jnais.ws.redefines.L3421OldTsTec;
import it.accenture.jnais.ws.redefines.L3421PcCommisGest;
import it.accenture.jnais.ws.redefines.L3421PcIntrRiat;
import it.accenture.jnais.ws.redefines.L3421PcRetr;
import it.accenture.jnais.ws.redefines.L3421PcRipPre;
import it.accenture.jnais.ws.redefines.L3421PreAttDiTrch;
import it.accenture.jnais.ws.redefines.L3421PreCasoMor;
import it.accenture.jnais.ws.redefines.L3421PreIniNet;
import it.accenture.jnais.ws.redefines.L3421PreInvrioIni;
import it.accenture.jnais.ws.redefines.L3421PreInvrioUlt;
import it.accenture.jnais.ws.redefines.L3421PreLrd;
import it.accenture.jnais.ws.redefines.L3421PrePattuito;
import it.accenture.jnais.ws.redefines.L3421PrePpIni;
import it.accenture.jnais.ws.redefines.L3421PrePpUlt;
import it.accenture.jnais.ws.redefines.L3421PreRivto;
import it.accenture.jnais.ws.redefines.L3421PreStab;
import it.accenture.jnais.ws.redefines.L3421PreTariIni;
import it.accenture.jnais.ws.redefines.L3421PreTariUlt;
import it.accenture.jnais.ws.redefines.L3421PreUniRivto;
import it.accenture.jnais.ws.redefines.L3421Prov1aaAcq;
import it.accenture.jnais.ws.redefines.L3421Prov2aaAcq;
import it.accenture.jnais.ws.redefines.L3421ProvInc;
import it.accenture.jnais.ws.redefines.L3421ProvRicor;
import it.accenture.jnais.ws.redefines.L3421PrstzAggIni;
import it.accenture.jnais.ws.redefines.L3421PrstzAggUlt;
import it.accenture.jnais.ws.redefines.L3421PrstzIni;
import it.accenture.jnais.ws.redefines.L3421PrstzIniNewfis;
import it.accenture.jnais.ws.redefines.L3421PrstzIniNforz;
import it.accenture.jnais.ws.redefines.L3421PrstzIniStab;
import it.accenture.jnais.ws.redefines.L3421PrstzRidIni;
import it.accenture.jnais.ws.redefines.L3421PrstzUlt;
import it.accenture.jnais.ws.redefines.L3421RatLrd;
import it.accenture.jnais.ws.redefines.L3421RemunAss;
import it.accenture.jnais.ws.redefines.L3421RendtoLrd;
import it.accenture.jnais.ws.redefines.L3421RendtoRetr;
import it.accenture.jnais.ws.redefines.L3421RenIniTsTec0;
import it.accenture.jnais.ws.redefines.L3421RisMat;
import it.accenture.jnais.ws.redefines.L3421TsRivalFis;
import it.accenture.jnais.ws.redefines.L3421TsRivalIndiciz;
import it.accenture.jnais.ws.redefines.L3421TsRivalNet;
import it.accenture.jnais.ws.redefines.L3421VisEnd2000;
import it.accenture.jnais.ws.redefines.L3421VisEnd2000Nforz;

/**Original name: LDBV3421-TGA-OUTPUT<br>
 * Variable: LDBV3421-TGA-OUTPUT from copybook LDBV3421<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbv3421TgaOutput {

    //==== PROPERTIES ====
    //Original name: L3421-ID-TRCH-DI-GAR
    private int l3421IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: L3421-ID-GAR
    private int l3421IdGar = DefaultValues.INT_VAL;
    //Original name: L3421-ID-ADES
    private int l3421IdAdes = DefaultValues.INT_VAL;
    //Original name: L3421-ID-POLI
    private int l3421IdPoli = DefaultValues.INT_VAL;
    //Original name: L3421-ID-MOVI-CRZ
    private int l3421IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: L3421-ID-MOVI-CHIU
    private L3421IdMoviChiu l3421IdMoviChiu = new L3421IdMoviChiu();
    //Original name: L3421-DT-INI-EFF
    private int l3421DtIniEff = DefaultValues.INT_VAL;
    //Original name: L3421-DT-END-EFF
    private int l3421DtEndEff = DefaultValues.INT_VAL;
    //Original name: L3421-COD-COMP-ANIA
    private int l3421CodCompAnia = DefaultValues.INT_VAL;
    //Original name: L3421-DT-DECOR
    private int l3421DtDecor = DefaultValues.INT_VAL;
    //Original name: L3421-DT-SCAD
    private L3421DtScad l3421DtScad = new L3421DtScad();
    //Original name: L3421-IB-OGG
    private String l3421IbOgg = DefaultValues.stringVal(Len.L3421_IB_OGG);
    //Original name: L3421-TP-RGM-FISC
    private String l3421TpRgmFisc = DefaultValues.stringVal(Len.L3421_TP_RGM_FISC);
    //Original name: L3421-DT-EMIS
    private L3421DtEmis l3421DtEmis = new L3421DtEmis();
    //Original name: L3421-TP-TRCH
    private String l3421TpTrch = DefaultValues.stringVal(Len.L3421_TP_TRCH);
    //Original name: L3421-DUR-AA
    private L3421DurAa l3421DurAa = new L3421DurAa();
    //Original name: L3421-DUR-MM
    private L3421DurMm l3421DurMm = new L3421DurMm();
    //Original name: L3421-DUR-GG
    private L3421DurGg l3421DurGg = new L3421DurGg();
    //Original name: L3421-PRE-CASO-MOR
    private L3421PreCasoMor l3421PreCasoMor = new L3421PreCasoMor();
    //Original name: L3421-PC-INTR-RIAT
    private L3421PcIntrRiat l3421PcIntrRiat = new L3421PcIntrRiat();
    //Original name: L3421-IMP-BNS-ANTIC
    private L3421ImpBnsAntic l3421ImpBnsAntic = new L3421ImpBnsAntic();
    //Original name: L3421-PRE-INI-NET
    private L3421PreIniNet l3421PreIniNet = new L3421PreIniNet();
    //Original name: L3421-PRE-PP-INI
    private L3421PrePpIni l3421PrePpIni = new L3421PrePpIni();
    //Original name: L3421-PRE-PP-ULT
    private L3421PrePpUlt l3421PrePpUlt = new L3421PrePpUlt();
    //Original name: L3421-PRE-TARI-INI
    private L3421PreTariIni l3421PreTariIni = new L3421PreTariIni();
    //Original name: L3421-PRE-TARI-ULT
    private L3421PreTariUlt l3421PreTariUlt = new L3421PreTariUlt();
    //Original name: L3421-PRE-INVRIO-INI
    private L3421PreInvrioIni l3421PreInvrioIni = new L3421PreInvrioIni();
    //Original name: L3421-PRE-INVRIO-ULT
    private L3421PreInvrioUlt l3421PreInvrioUlt = new L3421PreInvrioUlt();
    //Original name: L3421-PRE-RIVTO
    private L3421PreRivto l3421PreRivto = new L3421PreRivto();
    //Original name: L3421-IMP-SOPR-PROF
    private L3421ImpSoprProf l3421ImpSoprProf = new L3421ImpSoprProf();
    //Original name: L3421-IMP-SOPR-SAN
    private L3421ImpSoprSan l3421ImpSoprSan = new L3421ImpSoprSan();
    //Original name: L3421-IMP-SOPR-SPO
    private L3421ImpSoprSpo l3421ImpSoprSpo = new L3421ImpSoprSpo();
    //Original name: L3421-IMP-SOPR-TEC
    private L3421ImpSoprTec l3421ImpSoprTec = new L3421ImpSoprTec();
    //Original name: L3421-IMP-ALT-SOPR
    private L3421ImpAltSopr l3421ImpAltSopr = new L3421ImpAltSopr();
    //Original name: L3421-PRE-STAB
    private L3421PreStab l3421PreStab = new L3421PreStab();
    //Original name: L3421-DT-EFF-STAB
    private L3421DtEffStab l3421DtEffStab = new L3421DtEffStab();
    //Original name: L3421-TS-RIVAL-FIS
    private L3421TsRivalFis l3421TsRivalFis = new L3421TsRivalFis();
    //Original name: L3421-TS-RIVAL-INDICIZ
    private L3421TsRivalIndiciz l3421TsRivalIndiciz = new L3421TsRivalIndiciz();
    //Original name: L3421-OLD-TS-TEC
    private L3421OldTsTec l3421OldTsTec = new L3421OldTsTec();
    //Original name: L3421-RAT-LRD
    private L3421RatLrd l3421RatLrd = new L3421RatLrd();
    //Original name: L3421-PRE-LRD
    private L3421PreLrd l3421PreLrd = new L3421PreLrd();
    //Original name: L3421-PRSTZ-INI
    private L3421PrstzIni l3421PrstzIni = new L3421PrstzIni();
    //Original name: L3421-PRSTZ-ULT
    private L3421PrstzUlt l3421PrstzUlt = new L3421PrstzUlt();
    //Original name: L3421-CPT-IN-OPZ-RIVTO
    private L3421CptInOpzRivto l3421CptInOpzRivto = new L3421CptInOpzRivto();
    //Original name: L3421-PRSTZ-INI-STAB
    private L3421PrstzIniStab l3421PrstzIniStab = new L3421PrstzIniStab();
    //Original name: L3421-CPT-RSH-MOR
    private L3421CptRshMor l3421CptRshMor = new L3421CptRshMor();
    //Original name: L3421-PRSTZ-RID-INI
    private L3421PrstzRidIni l3421PrstzRidIni = new L3421PrstzRidIni();
    //Original name: L3421-FL-CAR-CONT
    private char l3421FlCarCont = DefaultValues.CHAR_VAL;
    //Original name: L3421-BNS-GIA-LIQTO
    private L3421BnsGiaLiqto l3421BnsGiaLiqto = new L3421BnsGiaLiqto();
    //Original name: L3421-IMP-BNS
    private L3421ImpBns l3421ImpBns = new L3421ImpBns();
    //Original name: L3421-COD-DVS
    private String l3421CodDvs = DefaultValues.stringVal(Len.L3421_COD_DVS);
    //Original name: L3421-PRSTZ-INI-NEWFIS
    private L3421PrstzIniNewfis l3421PrstzIniNewfis = new L3421PrstzIniNewfis();
    //Original name: L3421-IMP-SCON
    private L3421ImpScon l3421ImpScon = new L3421ImpScon();
    //Original name: L3421-ALQ-SCON
    private L3421AlqScon l3421AlqScon = new L3421AlqScon();
    //Original name: L3421-IMP-CAR-ACQ
    private L3421ImpCarAcq l3421ImpCarAcq = new L3421ImpCarAcq();
    //Original name: L3421-IMP-CAR-INC
    private L3421ImpCarInc l3421ImpCarInc = new L3421ImpCarInc();
    //Original name: L3421-IMP-CAR-GEST
    private L3421ImpCarGest l3421ImpCarGest = new L3421ImpCarGest();
    //Original name: L3421-ETA-AA-1O-ASSTO
    private L3421EtaAa1oAssto l3421EtaAa1oAssto = new L3421EtaAa1oAssto();
    //Original name: L3421-ETA-MM-1O-ASSTO
    private L3421EtaMm1oAssto l3421EtaMm1oAssto = new L3421EtaMm1oAssto();
    //Original name: L3421-ETA-AA-2O-ASSTO
    private L3421EtaAa2oAssto l3421EtaAa2oAssto = new L3421EtaAa2oAssto();
    //Original name: L3421-ETA-MM-2O-ASSTO
    private L3421EtaMm2oAssto l3421EtaMm2oAssto = new L3421EtaMm2oAssto();
    //Original name: L3421-ETA-AA-3O-ASSTO
    private L3421EtaAa3oAssto l3421EtaAa3oAssto = new L3421EtaAa3oAssto();
    //Original name: L3421-ETA-MM-3O-ASSTO
    private L3421EtaMm3oAssto l3421EtaMm3oAssto = new L3421EtaMm3oAssto();
    //Original name: L3421-RENDTO-LRD
    private L3421RendtoLrd l3421RendtoLrd = new L3421RendtoLrd();
    //Original name: L3421-PC-RETR
    private L3421PcRetr l3421PcRetr = new L3421PcRetr();
    //Original name: L3421-RENDTO-RETR
    private L3421RendtoRetr l3421RendtoRetr = new L3421RendtoRetr();
    //Original name: L3421-MIN-GARTO
    private L3421MinGarto l3421MinGarto = new L3421MinGarto();
    //Original name: L3421-MIN-TRNUT
    private L3421MinTrnut l3421MinTrnut = new L3421MinTrnut();
    //Original name: L3421-PRE-ATT-DI-TRCH
    private L3421PreAttDiTrch l3421PreAttDiTrch = new L3421PreAttDiTrch();
    //Original name: L3421-MATU-END2000
    private L3421MatuEnd2000 l3421MatuEnd2000 = new L3421MatuEnd2000();
    //Original name: L3421-ABB-TOT-INI
    private L3421AbbTotIni l3421AbbTotIni = new L3421AbbTotIni();
    //Original name: L3421-ABB-TOT-ULT
    private L3421AbbTotUlt l3421AbbTotUlt = new L3421AbbTotUlt();
    //Original name: L3421-ABB-ANNU-ULT
    private L3421AbbAnnuUlt l3421AbbAnnuUlt = new L3421AbbAnnuUlt();
    //Original name: L3421-DUR-ABB
    private L3421DurAbb l3421DurAbb = new L3421DurAbb();
    //Original name: L3421-TP-ADEG-ABB
    private char l3421TpAdegAbb = DefaultValues.CHAR_VAL;
    //Original name: L3421-MOD-CALC
    private String l3421ModCalc = DefaultValues.stringVal(Len.L3421_MOD_CALC);
    //Original name: L3421-IMP-AZ
    private L3421ImpAz l3421ImpAz = new L3421ImpAz();
    //Original name: L3421-IMP-ADER
    private L3421ImpAder l3421ImpAder = new L3421ImpAder();
    //Original name: L3421-IMP-TFR
    private L3421ImpTfr l3421ImpTfr = new L3421ImpTfr();
    //Original name: L3421-IMP-VOLO
    private L3421ImpVolo l3421ImpVolo = new L3421ImpVolo();
    //Original name: L3421-VIS-END2000
    private L3421VisEnd2000 l3421VisEnd2000 = new L3421VisEnd2000();
    //Original name: L3421-DT-VLDT-PROD
    private L3421DtVldtProd l3421DtVldtProd = new L3421DtVldtProd();
    //Original name: L3421-DT-INI-VAL-TAR
    private L3421DtIniValTar l3421DtIniValTar = new L3421DtIniValTar();
    //Original name: L3421-IMPB-VIS-END2000
    private L3421ImpbVisEnd2000 l3421ImpbVisEnd2000 = new L3421ImpbVisEnd2000();
    //Original name: L3421-REN-INI-TS-TEC-0
    private L3421RenIniTsTec0 l3421RenIniTsTec0 = new L3421RenIniTsTec0();
    //Original name: L3421-PC-RIP-PRE
    private L3421PcRipPre l3421PcRipPre = new L3421PcRipPre();
    //Original name: L3421-FL-IMPORTI-FORZ
    private char l3421FlImportiForz = DefaultValues.CHAR_VAL;
    //Original name: L3421-PRSTZ-INI-NFORZ
    private L3421PrstzIniNforz l3421PrstzIniNforz = new L3421PrstzIniNforz();
    //Original name: L3421-VIS-END2000-NFORZ
    private L3421VisEnd2000Nforz l3421VisEnd2000Nforz = new L3421VisEnd2000Nforz();
    //Original name: L3421-INTR-MORA
    private L3421IntrMora l3421IntrMora = new L3421IntrMora();
    //Original name: L3421-MANFEE-ANTIC
    private L3421ManfeeAntic l3421ManfeeAntic = new L3421ManfeeAntic();
    //Original name: L3421-MANFEE-RICOR
    private L3421ManfeeRicor l3421ManfeeRicor = new L3421ManfeeRicor();
    //Original name: L3421-PRE-UNI-RIVTO
    private L3421PreUniRivto l3421PreUniRivto = new L3421PreUniRivto();
    //Original name: L3421-PROV-1AA-ACQ
    private L3421Prov1aaAcq l3421Prov1aaAcq = new L3421Prov1aaAcq();
    //Original name: L3421-PROV-2AA-ACQ
    private L3421Prov2aaAcq l3421Prov2aaAcq = new L3421Prov2aaAcq();
    //Original name: L3421-PROV-RICOR
    private L3421ProvRicor l3421ProvRicor = new L3421ProvRicor();
    //Original name: L3421-PROV-INC
    private L3421ProvInc l3421ProvInc = new L3421ProvInc();
    //Original name: L3421-ALQ-PROV-ACQ
    private L3421AlqProvAcq l3421AlqProvAcq = new L3421AlqProvAcq();
    //Original name: L3421-ALQ-PROV-INC
    private L3421AlqProvInc l3421AlqProvInc = new L3421AlqProvInc();
    //Original name: L3421-ALQ-PROV-RICOR
    private L3421AlqProvRicor l3421AlqProvRicor = new L3421AlqProvRicor();
    //Original name: L3421-IMPB-PROV-ACQ
    private L3421ImpbProvAcq l3421ImpbProvAcq = new L3421ImpbProvAcq();
    //Original name: L3421-IMPB-PROV-INC
    private L3421ImpbProvInc l3421ImpbProvInc = new L3421ImpbProvInc();
    //Original name: L3421-IMPB-PROV-RICOR
    private L3421ImpbProvRicor l3421ImpbProvRicor = new L3421ImpbProvRicor();
    //Original name: L3421-FL-PROV-FORZ
    private char l3421FlProvForz = DefaultValues.CHAR_VAL;
    //Original name: L3421-PRSTZ-AGG-INI
    private L3421PrstzAggIni l3421PrstzAggIni = new L3421PrstzAggIni();
    //Original name: L3421-INCR-PRE
    private L3421IncrPre l3421IncrPre = new L3421IncrPre();
    //Original name: L3421-INCR-PRSTZ
    private L3421IncrPrstz l3421IncrPrstz = new L3421IncrPrstz();
    //Original name: L3421-DT-ULT-ADEG-PRE-PR
    private L3421DtUltAdegPrePr l3421DtUltAdegPrePr = new L3421DtUltAdegPrePr();
    //Original name: L3421-PRSTZ-AGG-ULT
    private L3421PrstzAggUlt l3421PrstzAggUlt = new L3421PrstzAggUlt();
    //Original name: L3421-TS-RIVAL-NET
    private L3421TsRivalNet l3421TsRivalNet = new L3421TsRivalNet();
    //Original name: L3421-PRE-PATTUITO
    private L3421PrePattuito l3421PrePattuito = new L3421PrePattuito();
    //Original name: L3421-TP-RIVAL
    private String l3421TpRival = DefaultValues.stringVal(Len.L3421_TP_RIVAL);
    //Original name: L3421-RIS-MAT
    private L3421RisMat l3421RisMat = new L3421RisMat();
    //Original name: L3421-CPT-MIN-SCAD
    private AfDecimal l3421CptMinScad = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: L3421-COMMIS-GEST
    private AfDecimal l3421CommisGest = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: L3421-TP-MANFEE-APPL
    private String l3421TpManfeeAppl = DefaultValues.stringVal(Len.L3421_TP_MANFEE_APPL);
    //Original name: L3421-DS-RIGA
    private long l3421DsRiga = DefaultValues.LONG_VAL;
    //Original name: L3421-DS-OPER-SQL
    private char l3421DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: L3421-DS-VER
    private int l3421DsVer = DefaultValues.INT_VAL;
    //Original name: L3421-DS-TS-INI-CPTZ
    private long l3421DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: L3421-DS-TS-END-CPTZ
    private long l3421DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: L3421-DS-UTENTE
    private String l3421DsUtente = DefaultValues.stringVal(Len.L3421_DS_UTENTE);
    //Original name: L3421-DS-STATO-ELAB
    private char l3421DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: L3421-PC-COMMIS-GEST
    private L3421PcCommisGest l3421PcCommisGest = new L3421PcCommisGest();
    //Original name: L3421-NUM-GG-RIVAL
    private L3421NumGgRival l3421NumGgRival = new L3421NumGgRival();
    //Original name: L3421-IMP-TRASFE
    private L3421ImpTrasfe l3421ImpTrasfe = new L3421ImpTrasfe();
    //Original name: L3421-IMP-TFR-STRC
    private L3421ImpTfrStrc l3421ImpTfrStrc = new L3421ImpTfrStrc();
    //Original name: L3421-ACQ-EXP
    private L3421AcqExp l3421AcqExp = new L3421AcqExp();
    //Original name: L3421-REMUN-ASS
    private L3421RemunAss l3421RemunAss = new L3421RemunAss();
    //Original name: L3421-COMMIS-INTER
    private L3421CommisInter l3421CommisInter = new L3421CommisInter();
    //Original name: L3421-ALQ-REMUN-ASS
    private L3421AlqRemunAss l3421AlqRemunAss = new L3421AlqRemunAss();
    //Original name: L3421-ALQ-COMMIS-INTER
    private L3421AlqCommisInter l3421AlqCommisInter = new L3421AlqCommisInter();
    //Original name: L3421-IMPB-REMUN-ASS
    private L3421ImpbRemunAss l3421ImpbRemunAss = new L3421ImpbRemunAss();
    //Original name: L3421-IMPB-COMMIS-INTER
    private L3421ImpbCommisInter l3421ImpbCommisInter = new L3421ImpbCommisInter();
    /**Original name: L3421-COS-RUN-ASSVA<br>
	 * <pre> INC000006400747</pre>*/
    private L3421CosRunAssva l3421CosRunAssva = new L3421CosRunAssva();
    //Original name: L3421-COS-RUN-ASSVA-IDC
    private L3421CosRunAssvaIdc l3421CosRunAssvaIdc = new L3421CosRunAssvaIdc();

    //==== METHODS ====
    public void setLdbv3421TgaOutputBytes(byte[] buffer) {
        setLdbv3421TgaOutputBytes(buffer, 1);
    }

    public void setLdbv3421TgaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        l3421IdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3421_ID_TRCH_DI_GAR, 0);
        position += Len.L3421_ID_TRCH_DI_GAR;
        l3421IdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3421_ID_GAR, 0);
        position += Len.L3421_ID_GAR;
        l3421IdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3421_ID_ADES, 0);
        position += Len.L3421_ID_ADES;
        l3421IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3421_ID_POLI, 0);
        position += Len.L3421_ID_POLI;
        l3421IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3421_ID_MOVI_CRZ, 0);
        position += Len.L3421_ID_MOVI_CRZ;
        l3421IdMoviChiu.setL3421IdMoviChiuFromBuffer(buffer, position);
        position += L3421IdMoviChiu.Len.L3421_ID_MOVI_CHIU;
        l3421DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3421_DT_INI_EFF, 0);
        position += Len.L3421_DT_INI_EFF;
        l3421DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3421_DT_END_EFF, 0);
        position += Len.L3421_DT_END_EFF;
        l3421CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3421_COD_COMP_ANIA, 0);
        position += Len.L3421_COD_COMP_ANIA;
        l3421DtDecor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3421_DT_DECOR, 0);
        position += Len.L3421_DT_DECOR;
        l3421DtScad.setL3421DtScadFromBuffer(buffer, position);
        position += L3421DtScad.Len.L3421_DT_SCAD;
        l3421IbOgg = MarshalByte.readString(buffer, position, Len.L3421_IB_OGG);
        position += Len.L3421_IB_OGG;
        l3421TpRgmFisc = MarshalByte.readString(buffer, position, Len.L3421_TP_RGM_FISC);
        position += Len.L3421_TP_RGM_FISC;
        l3421DtEmis.setL3421DtEmisFromBuffer(buffer, position);
        position += L3421DtEmis.Len.L3421_DT_EMIS;
        l3421TpTrch = MarshalByte.readString(buffer, position, Len.L3421_TP_TRCH);
        position += Len.L3421_TP_TRCH;
        l3421DurAa.setL3421DurAaFromBuffer(buffer, position);
        position += L3421DurAa.Len.L3421_DUR_AA;
        l3421DurMm.setL3421DurMmFromBuffer(buffer, position);
        position += L3421DurMm.Len.L3421_DUR_MM;
        l3421DurGg.setL3421DurGgFromBuffer(buffer, position);
        position += L3421DurGg.Len.L3421_DUR_GG;
        l3421PreCasoMor.setL3421PreCasoMorFromBuffer(buffer, position);
        position += L3421PreCasoMor.Len.L3421_PRE_CASO_MOR;
        l3421PcIntrRiat.setL3421PcIntrRiatFromBuffer(buffer, position);
        position += L3421PcIntrRiat.Len.L3421_PC_INTR_RIAT;
        l3421ImpBnsAntic.setL3421ImpBnsAnticFromBuffer(buffer, position);
        position += L3421ImpBnsAntic.Len.L3421_IMP_BNS_ANTIC;
        l3421PreIniNet.setL3421PreIniNetFromBuffer(buffer, position);
        position += L3421PreIniNet.Len.L3421_PRE_INI_NET;
        l3421PrePpIni.setL3421PrePpIniFromBuffer(buffer, position);
        position += L3421PrePpIni.Len.L3421_PRE_PP_INI;
        l3421PrePpUlt.setL3421PrePpUltFromBuffer(buffer, position);
        position += L3421PrePpUlt.Len.L3421_PRE_PP_ULT;
        l3421PreTariIni.setL3421PreTariIniFromBuffer(buffer, position);
        position += L3421PreTariIni.Len.L3421_PRE_TARI_INI;
        l3421PreTariUlt.setL3421PreTariUltFromBuffer(buffer, position);
        position += L3421PreTariUlt.Len.L3421_PRE_TARI_ULT;
        l3421PreInvrioIni.setL3421PreInvrioIniFromBuffer(buffer, position);
        position += L3421PreInvrioIni.Len.L3421_PRE_INVRIO_INI;
        l3421PreInvrioUlt.setL3421PreInvrioUltFromBuffer(buffer, position);
        position += L3421PreInvrioUlt.Len.L3421_PRE_INVRIO_ULT;
        l3421PreRivto.setL3421PreRivtoFromBuffer(buffer, position);
        position += L3421PreRivto.Len.L3421_PRE_RIVTO;
        l3421ImpSoprProf.setL3421ImpSoprProfFromBuffer(buffer, position);
        position += L3421ImpSoprProf.Len.L3421_IMP_SOPR_PROF;
        l3421ImpSoprSan.setL3421ImpSoprSanFromBuffer(buffer, position);
        position += L3421ImpSoprSan.Len.L3421_IMP_SOPR_SAN;
        l3421ImpSoprSpo.setL3421ImpSoprSpoFromBuffer(buffer, position);
        position += L3421ImpSoprSpo.Len.L3421_IMP_SOPR_SPO;
        l3421ImpSoprTec.setL3421ImpSoprTecFromBuffer(buffer, position);
        position += L3421ImpSoprTec.Len.L3421_IMP_SOPR_TEC;
        l3421ImpAltSopr.setL3421ImpAltSoprFromBuffer(buffer, position);
        position += L3421ImpAltSopr.Len.L3421_IMP_ALT_SOPR;
        l3421PreStab.setL3421PreStabFromBuffer(buffer, position);
        position += L3421PreStab.Len.L3421_PRE_STAB;
        l3421DtEffStab.setL3421DtEffStabFromBuffer(buffer, position);
        position += L3421DtEffStab.Len.L3421_DT_EFF_STAB;
        l3421TsRivalFis.setL3421TsRivalFisFromBuffer(buffer, position);
        position += L3421TsRivalFis.Len.L3421_TS_RIVAL_FIS;
        l3421TsRivalIndiciz.setL3421TsRivalIndicizFromBuffer(buffer, position);
        position += L3421TsRivalIndiciz.Len.L3421_TS_RIVAL_INDICIZ;
        l3421OldTsTec.setL3421OldTsTecFromBuffer(buffer, position);
        position += L3421OldTsTec.Len.L3421_OLD_TS_TEC;
        l3421RatLrd.setL3421RatLrdFromBuffer(buffer, position);
        position += L3421RatLrd.Len.L3421_RAT_LRD;
        l3421PreLrd.setL3421PreLrdFromBuffer(buffer, position);
        position += L3421PreLrd.Len.L3421_PRE_LRD;
        l3421PrstzIni.setL3421PrstzIniFromBuffer(buffer, position);
        position += L3421PrstzIni.Len.L3421_PRSTZ_INI;
        l3421PrstzUlt.setL3421PrstzUltFromBuffer(buffer, position);
        position += L3421PrstzUlt.Len.L3421_PRSTZ_ULT;
        l3421CptInOpzRivto.setL3421CptInOpzRivtoFromBuffer(buffer, position);
        position += L3421CptInOpzRivto.Len.L3421_CPT_IN_OPZ_RIVTO;
        l3421PrstzIniStab.setL3421PrstzIniStabFromBuffer(buffer, position);
        position += L3421PrstzIniStab.Len.L3421_PRSTZ_INI_STAB;
        l3421CptRshMor.setL3421CptRshMorFromBuffer(buffer, position);
        position += L3421CptRshMor.Len.L3421_CPT_RSH_MOR;
        l3421PrstzRidIni.setL3421PrstzRidIniFromBuffer(buffer, position);
        position += L3421PrstzRidIni.Len.L3421_PRSTZ_RID_INI;
        l3421FlCarCont = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3421BnsGiaLiqto.setL3421BnsGiaLiqtoFromBuffer(buffer, position);
        position += L3421BnsGiaLiqto.Len.L3421_BNS_GIA_LIQTO;
        l3421ImpBns.setL3421ImpBnsFromBuffer(buffer, position);
        position += L3421ImpBns.Len.L3421_IMP_BNS;
        l3421CodDvs = MarshalByte.readString(buffer, position, Len.L3421_COD_DVS);
        position += Len.L3421_COD_DVS;
        l3421PrstzIniNewfis.setL3421PrstzIniNewfisFromBuffer(buffer, position);
        position += L3421PrstzIniNewfis.Len.L3421_PRSTZ_INI_NEWFIS;
        l3421ImpScon.setL3421ImpSconFromBuffer(buffer, position);
        position += L3421ImpScon.Len.L3421_IMP_SCON;
        l3421AlqScon.setL3421AlqSconFromBuffer(buffer, position);
        position += L3421AlqScon.Len.L3421_ALQ_SCON;
        l3421ImpCarAcq.setL3421ImpCarAcqFromBuffer(buffer, position);
        position += L3421ImpCarAcq.Len.L3421_IMP_CAR_ACQ;
        l3421ImpCarInc.setL3421ImpCarIncFromBuffer(buffer, position);
        position += L3421ImpCarInc.Len.L3421_IMP_CAR_INC;
        l3421ImpCarGest.setL3421ImpCarGestFromBuffer(buffer, position);
        position += L3421ImpCarGest.Len.L3421_IMP_CAR_GEST;
        l3421EtaAa1oAssto.setL3421EtaAa1oAsstoFromBuffer(buffer, position);
        position += L3421EtaAa1oAssto.Len.L3421_ETA_AA1O_ASSTO;
        l3421EtaMm1oAssto.setL3421EtaMm1oAsstoFromBuffer(buffer, position);
        position += L3421EtaMm1oAssto.Len.L3421_ETA_MM1O_ASSTO;
        l3421EtaAa2oAssto.setL3421EtaAa2oAsstoFromBuffer(buffer, position);
        position += L3421EtaAa2oAssto.Len.L3421_ETA_AA2O_ASSTO;
        l3421EtaMm2oAssto.setL3421EtaMm2oAsstoFromBuffer(buffer, position);
        position += L3421EtaMm2oAssto.Len.L3421_ETA_MM2O_ASSTO;
        l3421EtaAa3oAssto.setL3421EtaAa3oAsstoFromBuffer(buffer, position);
        position += L3421EtaAa3oAssto.Len.L3421_ETA_AA3O_ASSTO;
        l3421EtaMm3oAssto.setL3421EtaMm3oAsstoFromBuffer(buffer, position);
        position += L3421EtaMm3oAssto.Len.L3421_ETA_MM3O_ASSTO;
        l3421RendtoLrd.setL3421RendtoLrdFromBuffer(buffer, position);
        position += L3421RendtoLrd.Len.L3421_RENDTO_LRD;
        l3421PcRetr.setL3421PcRetrFromBuffer(buffer, position);
        position += L3421PcRetr.Len.L3421_PC_RETR;
        l3421RendtoRetr.setL3421RendtoRetrFromBuffer(buffer, position);
        position += L3421RendtoRetr.Len.L3421_RENDTO_RETR;
        l3421MinGarto.setL3421MinGartoFromBuffer(buffer, position);
        position += L3421MinGarto.Len.L3421_MIN_GARTO;
        l3421MinTrnut.setL3421MinTrnutFromBuffer(buffer, position);
        position += L3421MinTrnut.Len.L3421_MIN_TRNUT;
        l3421PreAttDiTrch.setL3421PreAttDiTrchFromBuffer(buffer, position);
        position += L3421PreAttDiTrch.Len.L3421_PRE_ATT_DI_TRCH;
        l3421MatuEnd2000.setL3421MatuEnd2000FromBuffer(buffer, position);
        position += L3421MatuEnd2000.Len.L3421_MATU_END2000;
        l3421AbbTotIni.setL3421AbbTotIniFromBuffer(buffer, position);
        position += L3421AbbTotIni.Len.L3421_ABB_TOT_INI;
        l3421AbbTotUlt.setL3421AbbTotUltFromBuffer(buffer, position);
        position += L3421AbbTotUlt.Len.L3421_ABB_TOT_ULT;
        l3421AbbAnnuUlt.setL3421AbbAnnuUltFromBuffer(buffer, position);
        position += L3421AbbAnnuUlt.Len.L3421_ABB_ANNU_ULT;
        l3421DurAbb.setL3421DurAbbFromBuffer(buffer, position);
        position += L3421DurAbb.Len.L3421_DUR_ABB;
        l3421TpAdegAbb = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3421ModCalc = MarshalByte.readString(buffer, position, Len.L3421_MOD_CALC);
        position += Len.L3421_MOD_CALC;
        l3421ImpAz.setL3421ImpAzFromBuffer(buffer, position);
        position += L3421ImpAz.Len.L3421_IMP_AZ;
        l3421ImpAder.setL3421ImpAderFromBuffer(buffer, position);
        position += L3421ImpAder.Len.L3421_IMP_ADER;
        l3421ImpTfr.setL3421ImpTfrFromBuffer(buffer, position);
        position += L3421ImpTfr.Len.L3421_IMP_TFR;
        l3421ImpVolo.setL3421ImpVoloFromBuffer(buffer, position);
        position += L3421ImpVolo.Len.L3421_IMP_VOLO;
        l3421VisEnd2000.setL3421VisEnd2000FromBuffer(buffer, position);
        position += L3421VisEnd2000.Len.L3421_VIS_END2000;
        l3421DtVldtProd.setL3421DtVldtProdFromBuffer(buffer, position);
        position += L3421DtVldtProd.Len.L3421_DT_VLDT_PROD;
        l3421DtIniValTar.setL3421DtIniValTarFromBuffer(buffer, position);
        position += L3421DtIniValTar.Len.L3421_DT_INI_VAL_TAR;
        l3421ImpbVisEnd2000.setL3421ImpbVisEnd2000FromBuffer(buffer, position);
        position += L3421ImpbVisEnd2000.Len.L3421_IMPB_VIS_END2000;
        l3421RenIniTsTec0.setL3421RenIniTsTec0FromBuffer(buffer, position);
        position += L3421RenIniTsTec0.Len.L3421_REN_INI_TS_TEC0;
        l3421PcRipPre.setL3421PcRipPreFromBuffer(buffer, position);
        position += L3421PcRipPre.Len.L3421_PC_RIP_PRE;
        l3421FlImportiForz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3421PrstzIniNforz.setL3421PrstzIniNforzFromBuffer(buffer, position);
        position += L3421PrstzIniNforz.Len.L3421_PRSTZ_INI_NFORZ;
        l3421VisEnd2000Nforz.setL3421VisEnd2000NforzFromBuffer(buffer, position);
        position += L3421VisEnd2000Nforz.Len.L3421_VIS_END2000_NFORZ;
        l3421IntrMora.setL3421IntrMoraFromBuffer(buffer, position);
        position += L3421IntrMora.Len.L3421_INTR_MORA;
        l3421ManfeeAntic.setL3421ManfeeAnticFromBuffer(buffer, position);
        position += L3421ManfeeAntic.Len.L3421_MANFEE_ANTIC;
        l3421ManfeeRicor.setL3421ManfeeRicorFromBuffer(buffer, position);
        position += L3421ManfeeRicor.Len.L3421_MANFEE_RICOR;
        l3421PreUniRivto.setL3421PreUniRivtoFromBuffer(buffer, position);
        position += L3421PreUniRivto.Len.L3421_PRE_UNI_RIVTO;
        l3421Prov1aaAcq.setL3421Prov1aaAcqFromBuffer(buffer, position);
        position += L3421Prov1aaAcq.Len.L3421_PROV1AA_ACQ;
        l3421Prov2aaAcq.setL3421Prov2aaAcqFromBuffer(buffer, position);
        position += L3421Prov2aaAcq.Len.L3421_PROV2AA_ACQ;
        l3421ProvRicor.setL3421ProvRicorFromBuffer(buffer, position);
        position += L3421ProvRicor.Len.L3421_PROV_RICOR;
        l3421ProvInc.setL3421ProvIncFromBuffer(buffer, position);
        position += L3421ProvInc.Len.L3421_PROV_INC;
        l3421AlqProvAcq.setL3421AlqProvAcqFromBuffer(buffer, position);
        position += L3421AlqProvAcq.Len.L3421_ALQ_PROV_ACQ;
        l3421AlqProvInc.setL3421AlqProvIncFromBuffer(buffer, position);
        position += L3421AlqProvInc.Len.L3421_ALQ_PROV_INC;
        l3421AlqProvRicor.setL3421AlqProvRicorFromBuffer(buffer, position);
        position += L3421AlqProvRicor.Len.L3421_ALQ_PROV_RICOR;
        l3421ImpbProvAcq.setL3421ImpbProvAcqFromBuffer(buffer, position);
        position += L3421ImpbProvAcq.Len.L3421_IMPB_PROV_ACQ;
        l3421ImpbProvInc.setL3421ImpbProvIncFromBuffer(buffer, position);
        position += L3421ImpbProvInc.Len.L3421_IMPB_PROV_INC;
        l3421ImpbProvRicor.setL3421ImpbProvRicorFromBuffer(buffer, position);
        position += L3421ImpbProvRicor.Len.L3421_IMPB_PROV_RICOR;
        l3421FlProvForz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3421PrstzAggIni.setL3421PrstzAggIniFromBuffer(buffer, position);
        position += L3421PrstzAggIni.Len.L3421_PRSTZ_AGG_INI;
        l3421IncrPre.setL3421IncrPreFromBuffer(buffer, position);
        position += L3421IncrPre.Len.L3421_INCR_PRE;
        l3421IncrPrstz.setL3421IncrPrstzFromBuffer(buffer, position);
        position += L3421IncrPrstz.Len.L3421_INCR_PRSTZ;
        l3421DtUltAdegPrePr.setL3421DtUltAdegPrePrFromBuffer(buffer, position);
        position += L3421DtUltAdegPrePr.Len.L3421_DT_ULT_ADEG_PRE_PR;
        l3421PrstzAggUlt.setL3421PrstzAggUltFromBuffer(buffer, position);
        position += L3421PrstzAggUlt.Len.L3421_PRSTZ_AGG_ULT;
        l3421TsRivalNet.setL3421TsRivalNetFromBuffer(buffer, position);
        position += L3421TsRivalNet.Len.L3421_TS_RIVAL_NET;
        l3421PrePattuito.setL3421PrePattuitoFromBuffer(buffer, position);
        position += L3421PrePattuito.Len.L3421_PRE_PATTUITO;
        l3421TpRival = MarshalByte.readString(buffer, position, Len.L3421_TP_RIVAL);
        position += Len.L3421_TP_RIVAL;
        l3421RisMat.setL3421RisMatFromBuffer(buffer, position);
        position += L3421RisMat.Len.L3421_RIS_MAT;
        l3421CptMinScad.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.L3421_CPT_MIN_SCAD, Len.Fract.L3421_CPT_MIN_SCAD));
        position += Len.L3421_CPT_MIN_SCAD;
        l3421CommisGest.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.L3421_COMMIS_GEST, Len.Fract.L3421_COMMIS_GEST));
        position += Len.L3421_COMMIS_GEST;
        l3421TpManfeeAppl = MarshalByte.readString(buffer, position, Len.L3421_TP_MANFEE_APPL);
        position += Len.L3421_TP_MANFEE_APPL;
        l3421DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L3421_DS_RIGA, 0);
        position += Len.L3421_DS_RIGA;
        l3421DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3421DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L3421_DS_VER, 0);
        position += Len.L3421_DS_VER;
        l3421DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L3421_DS_TS_INI_CPTZ, 0);
        position += Len.L3421_DS_TS_INI_CPTZ;
        l3421DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L3421_DS_TS_END_CPTZ, 0);
        position += Len.L3421_DS_TS_END_CPTZ;
        l3421DsUtente = MarshalByte.readString(buffer, position, Len.L3421_DS_UTENTE);
        position += Len.L3421_DS_UTENTE;
        l3421DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l3421PcCommisGest.setL3421PcCommisGestFromBuffer(buffer, position);
        position += L3421PcCommisGest.Len.L3421_PC_COMMIS_GEST;
        l3421NumGgRival.setL3421NumGgRivalFromBuffer(buffer, position);
        position += L3421NumGgRival.Len.L3421_NUM_GG_RIVAL;
        l3421ImpTrasfe.setL3421ImpTrasfeFromBuffer(buffer, position);
        position += L3421ImpTrasfe.Len.L3421_IMP_TRASFE;
        l3421ImpTfrStrc.setL3421ImpTfrStrcFromBuffer(buffer, position);
        position += L3421ImpTfrStrc.Len.L3421_IMP_TFR_STRC;
        l3421AcqExp.setL3421AcqExpFromBuffer(buffer, position);
        position += L3421AcqExp.Len.L3421_ACQ_EXP;
        l3421RemunAss.setL3421RemunAssFromBuffer(buffer, position);
        position += L3421RemunAss.Len.L3421_REMUN_ASS;
        l3421CommisInter.setL3421CommisInterFromBuffer(buffer, position);
        position += L3421CommisInter.Len.L3421_COMMIS_INTER;
        l3421AlqRemunAss.setL3421AlqRemunAssFromBuffer(buffer, position);
        position += L3421AlqRemunAss.Len.L3421_ALQ_REMUN_ASS;
        l3421AlqCommisInter.setL3421AlqCommisInterFromBuffer(buffer, position);
        position += L3421AlqCommisInter.Len.L3421_ALQ_COMMIS_INTER;
        l3421ImpbRemunAss.setL3421ImpbRemunAssFromBuffer(buffer, position);
        position += L3421ImpbRemunAss.Len.L3421_IMPB_REMUN_ASS;
        l3421ImpbCommisInter.setL3421ImpbCommisInterFromBuffer(buffer, position);
        position += L3421ImpbCommisInter.Len.L3421_IMPB_COMMIS_INTER;
        l3421CosRunAssva.setL3421CosRunAssvaFromBuffer(buffer, position);
        position += L3421CosRunAssva.Len.L3421_COS_RUN_ASSVA;
        l3421CosRunAssvaIdc.setL3421CosRunAssvaIdcFromBuffer(buffer, position);
    }

    public byte[] getLdbv3421TgaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, l3421IdTrchDiGar, Len.Int.L3421_ID_TRCH_DI_GAR, 0);
        position += Len.L3421_ID_TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, l3421IdGar, Len.Int.L3421_ID_GAR, 0);
        position += Len.L3421_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, l3421IdAdes, Len.Int.L3421_ID_ADES, 0);
        position += Len.L3421_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, l3421IdPoli, Len.Int.L3421_ID_POLI, 0);
        position += Len.L3421_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, l3421IdMoviCrz, Len.Int.L3421_ID_MOVI_CRZ, 0);
        position += Len.L3421_ID_MOVI_CRZ;
        l3421IdMoviChiu.getL3421IdMoviChiuAsBuffer(buffer, position);
        position += L3421IdMoviChiu.Len.L3421_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, l3421DtIniEff, Len.Int.L3421_DT_INI_EFF, 0);
        position += Len.L3421_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, l3421DtEndEff, Len.Int.L3421_DT_END_EFF, 0);
        position += Len.L3421_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, l3421CodCompAnia, Len.Int.L3421_COD_COMP_ANIA, 0);
        position += Len.L3421_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, l3421DtDecor, Len.Int.L3421_DT_DECOR, 0);
        position += Len.L3421_DT_DECOR;
        l3421DtScad.getL3421DtScadAsBuffer(buffer, position);
        position += L3421DtScad.Len.L3421_DT_SCAD;
        MarshalByte.writeString(buffer, position, l3421IbOgg, Len.L3421_IB_OGG);
        position += Len.L3421_IB_OGG;
        MarshalByte.writeString(buffer, position, l3421TpRgmFisc, Len.L3421_TP_RGM_FISC);
        position += Len.L3421_TP_RGM_FISC;
        l3421DtEmis.getL3421DtEmisAsBuffer(buffer, position);
        position += L3421DtEmis.Len.L3421_DT_EMIS;
        MarshalByte.writeString(buffer, position, l3421TpTrch, Len.L3421_TP_TRCH);
        position += Len.L3421_TP_TRCH;
        l3421DurAa.getL3421DurAaAsBuffer(buffer, position);
        position += L3421DurAa.Len.L3421_DUR_AA;
        l3421DurMm.getL3421DurMmAsBuffer(buffer, position);
        position += L3421DurMm.Len.L3421_DUR_MM;
        l3421DurGg.getL3421DurGgAsBuffer(buffer, position);
        position += L3421DurGg.Len.L3421_DUR_GG;
        l3421PreCasoMor.getL3421PreCasoMorAsBuffer(buffer, position);
        position += L3421PreCasoMor.Len.L3421_PRE_CASO_MOR;
        l3421PcIntrRiat.getL3421PcIntrRiatAsBuffer(buffer, position);
        position += L3421PcIntrRiat.Len.L3421_PC_INTR_RIAT;
        l3421ImpBnsAntic.getL3421ImpBnsAnticAsBuffer(buffer, position);
        position += L3421ImpBnsAntic.Len.L3421_IMP_BNS_ANTIC;
        l3421PreIniNet.getL3421PreIniNetAsBuffer(buffer, position);
        position += L3421PreIniNet.Len.L3421_PRE_INI_NET;
        l3421PrePpIni.getL3421PrePpIniAsBuffer(buffer, position);
        position += L3421PrePpIni.Len.L3421_PRE_PP_INI;
        l3421PrePpUlt.getL3421PrePpUltAsBuffer(buffer, position);
        position += L3421PrePpUlt.Len.L3421_PRE_PP_ULT;
        l3421PreTariIni.getL3421PreTariIniAsBuffer(buffer, position);
        position += L3421PreTariIni.Len.L3421_PRE_TARI_INI;
        l3421PreTariUlt.getL3421PreTariUltAsBuffer(buffer, position);
        position += L3421PreTariUlt.Len.L3421_PRE_TARI_ULT;
        l3421PreInvrioIni.getL3421PreInvrioIniAsBuffer(buffer, position);
        position += L3421PreInvrioIni.Len.L3421_PRE_INVRIO_INI;
        l3421PreInvrioUlt.getL3421PreInvrioUltAsBuffer(buffer, position);
        position += L3421PreInvrioUlt.Len.L3421_PRE_INVRIO_ULT;
        l3421PreRivto.getL3421PreRivtoAsBuffer(buffer, position);
        position += L3421PreRivto.Len.L3421_PRE_RIVTO;
        l3421ImpSoprProf.getL3421ImpSoprProfAsBuffer(buffer, position);
        position += L3421ImpSoprProf.Len.L3421_IMP_SOPR_PROF;
        l3421ImpSoprSan.getL3421ImpSoprSanAsBuffer(buffer, position);
        position += L3421ImpSoprSan.Len.L3421_IMP_SOPR_SAN;
        l3421ImpSoprSpo.getL3421ImpSoprSpoAsBuffer(buffer, position);
        position += L3421ImpSoprSpo.Len.L3421_IMP_SOPR_SPO;
        l3421ImpSoprTec.getL3421ImpSoprTecAsBuffer(buffer, position);
        position += L3421ImpSoprTec.Len.L3421_IMP_SOPR_TEC;
        l3421ImpAltSopr.getL3421ImpAltSoprAsBuffer(buffer, position);
        position += L3421ImpAltSopr.Len.L3421_IMP_ALT_SOPR;
        l3421PreStab.getL3421PreStabAsBuffer(buffer, position);
        position += L3421PreStab.Len.L3421_PRE_STAB;
        l3421DtEffStab.getL3421DtEffStabAsBuffer(buffer, position);
        position += L3421DtEffStab.Len.L3421_DT_EFF_STAB;
        l3421TsRivalFis.getL3421TsRivalFisAsBuffer(buffer, position);
        position += L3421TsRivalFis.Len.L3421_TS_RIVAL_FIS;
        l3421TsRivalIndiciz.getL3421TsRivalIndicizAsBuffer(buffer, position);
        position += L3421TsRivalIndiciz.Len.L3421_TS_RIVAL_INDICIZ;
        l3421OldTsTec.getL3421OldTsTecAsBuffer(buffer, position);
        position += L3421OldTsTec.Len.L3421_OLD_TS_TEC;
        l3421RatLrd.getL3421RatLrdAsBuffer(buffer, position);
        position += L3421RatLrd.Len.L3421_RAT_LRD;
        l3421PreLrd.getL3421PreLrdAsBuffer(buffer, position);
        position += L3421PreLrd.Len.L3421_PRE_LRD;
        l3421PrstzIni.getL3421PrstzIniAsBuffer(buffer, position);
        position += L3421PrstzIni.Len.L3421_PRSTZ_INI;
        l3421PrstzUlt.getL3421PrstzUltAsBuffer(buffer, position);
        position += L3421PrstzUlt.Len.L3421_PRSTZ_ULT;
        l3421CptInOpzRivto.getL3421CptInOpzRivtoAsBuffer(buffer, position);
        position += L3421CptInOpzRivto.Len.L3421_CPT_IN_OPZ_RIVTO;
        l3421PrstzIniStab.getL3421PrstzIniStabAsBuffer(buffer, position);
        position += L3421PrstzIniStab.Len.L3421_PRSTZ_INI_STAB;
        l3421CptRshMor.getL3421CptRshMorAsBuffer(buffer, position);
        position += L3421CptRshMor.Len.L3421_CPT_RSH_MOR;
        l3421PrstzRidIni.getL3421PrstzRidIniAsBuffer(buffer, position);
        position += L3421PrstzRidIni.Len.L3421_PRSTZ_RID_INI;
        MarshalByte.writeChar(buffer, position, l3421FlCarCont);
        position += Types.CHAR_SIZE;
        l3421BnsGiaLiqto.getL3421BnsGiaLiqtoAsBuffer(buffer, position);
        position += L3421BnsGiaLiqto.Len.L3421_BNS_GIA_LIQTO;
        l3421ImpBns.getL3421ImpBnsAsBuffer(buffer, position);
        position += L3421ImpBns.Len.L3421_IMP_BNS;
        MarshalByte.writeString(buffer, position, l3421CodDvs, Len.L3421_COD_DVS);
        position += Len.L3421_COD_DVS;
        l3421PrstzIniNewfis.getL3421PrstzIniNewfisAsBuffer(buffer, position);
        position += L3421PrstzIniNewfis.Len.L3421_PRSTZ_INI_NEWFIS;
        l3421ImpScon.getL3421ImpSconAsBuffer(buffer, position);
        position += L3421ImpScon.Len.L3421_IMP_SCON;
        l3421AlqScon.getL3421AlqSconAsBuffer(buffer, position);
        position += L3421AlqScon.Len.L3421_ALQ_SCON;
        l3421ImpCarAcq.getL3421ImpCarAcqAsBuffer(buffer, position);
        position += L3421ImpCarAcq.Len.L3421_IMP_CAR_ACQ;
        l3421ImpCarInc.getL3421ImpCarIncAsBuffer(buffer, position);
        position += L3421ImpCarInc.Len.L3421_IMP_CAR_INC;
        l3421ImpCarGest.getL3421ImpCarGestAsBuffer(buffer, position);
        position += L3421ImpCarGest.Len.L3421_IMP_CAR_GEST;
        l3421EtaAa1oAssto.getL3421EtaAa1oAsstoAsBuffer(buffer, position);
        position += L3421EtaAa1oAssto.Len.L3421_ETA_AA1O_ASSTO;
        l3421EtaMm1oAssto.getL3421EtaMm1oAsstoAsBuffer(buffer, position);
        position += L3421EtaMm1oAssto.Len.L3421_ETA_MM1O_ASSTO;
        l3421EtaAa2oAssto.getL3421EtaAa2oAsstoAsBuffer(buffer, position);
        position += L3421EtaAa2oAssto.Len.L3421_ETA_AA2O_ASSTO;
        l3421EtaMm2oAssto.getL3421EtaMm2oAsstoAsBuffer(buffer, position);
        position += L3421EtaMm2oAssto.Len.L3421_ETA_MM2O_ASSTO;
        l3421EtaAa3oAssto.getL3421EtaAa3oAsstoAsBuffer(buffer, position);
        position += L3421EtaAa3oAssto.Len.L3421_ETA_AA3O_ASSTO;
        l3421EtaMm3oAssto.getL3421EtaMm3oAsstoAsBuffer(buffer, position);
        position += L3421EtaMm3oAssto.Len.L3421_ETA_MM3O_ASSTO;
        l3421RendtoLrd.getL3421RendtoLrdAsBuffer(buffer, position);
        position += L3421RendtoLrd.Len.L3421_RENDTO_LRD;
        l3421PcRetr.getL3421PcRetrAsBuffer(buffer, position);
        position += L3421PcRetr.Len.L3421_PC_RETR;
        l3421RendtoRetr.getL3421RendtoRetrAsBuffer(buffer, position);
        position += L3421RendtoRetr.Len.L3421_RENDTO_RETR;
        l3421MinGarto.getL3421MinGartoAsBuffer(buffer, position);
        position += L3421MinGarto.Len.L3421_MIN_GARTO;
        l3421MinTrnut.getL3421MinTrnutAsBuffer(buffer, position);
        position += L3421MinTrnut.Len.L3421_MIN_TRNUT;
        l3421PreAttDiTrch.getL3421PreAttDiTrchAsBuffer(buffer, position);
        position += L3421PreAttDiTrch.Len.L3421_PRE_ATT_DI_TRCH;
        l3421MatuEnd2000.getL3421MatuEnd2000AsBuffer(buffer, position);
        position += L3421MatuEnd2000.Len.L3421_MATU_END2000;
        l3421AbbTotIni.getL3421AbbTotIniAsBuffer(buffer, position);
        position += L3421AbbTotIni.Len.L3421_ABB_TOT_INI;
        l3421AbbTotUlt.getL3421AbbTotUltAsBuffer(buffer, position);
        position += L3421AbbTotUlt.Len.L3421_ABB_TOT_ULT;
        l3421AbbAnnuUlt.getL3421AbbAnnuUltAsBuffer(buffer, position);
        position += L3421AbbAnnuUlt.Len.L3421_ABB_ANNU_ULT;
        l3421DurAbb.getL3421DurAbbAsBuffer(buffer, position);
        position += L3421DurAbb.Len.L3421_DUR_ABB;
        MarshalByte.writeChar(buffer, position, l3421TpAdegAbb);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, l3421ModCalc, Len.L3421_MOD_CALC);
        position += Len.L3421_MOD_CALC;
        l3421ImpAz.getL3421ImpAzAsBuffer(buffer, position);
        position += L3421ImpAz.Len.L3421_IMP_AZ;
        l3421ImpAder.getL3421ImpAderAsBuffer(buffer, position);
        position += L3421ImpAder.Len.L3421_IMP_ADER;
        l3421ImpTfr.getL3421ImpTfrAsBuffer(buffer, position);
        position += L3421ImpTfr.Len.L3421_IMP_TFR;
        l3421ImpVolo.getL3421ImpVoloAsBuffer(buffer, position);
        position += L3421ImpVolo.Len.L3421_IMP_VOLO;
        l3421VisEnd2000.getL3421VisEnd2000AsBuffer(buffer, position);
        position += L3421VisEnd2000.Len.L3421_VIS_END2000;
        l3421DtVldtProd.getL3421DtVldtProdAsBuffer(buffer, position);
        position += L3421DtVldtProd.Len.L3421_DT_VLDT_PROD;
        l3421DtIniValTar.getL3421DtIniValTarAsBuffer(buffer, position);
        position += L3421DtIniValTar.Len.L3421_DT_INI_VAL_TAR;
        l3421ImpbVisEnd2000.getL3421ImpbVisEnd2000AsBuffer(buffer, position);
        position += L3421ImpbVisEnd2000.Len.L3421_IMPB_VIS_END2000;
        l3421RenIniTsTec0.getL3421RenIniTsTec0AsBuffer(buffer, position);
        position += L3421RenIniTsTec0.Len.L3421_REN_INI_TS_TEC0;
        l3421PcRipPre.getL3421PcRipPreAsBuffer(buffer, position);
        position += L3421PcRipPre.Len.L3421_PC_RIP_PRE;
        MarshalByte.writeChar(buffer, position, l3421FlImportiForz);
        position += Types.CHAR_SIZE;
        l3421PrstzIniNforz.getL3421PrstzIniNforzAsBuffer(buffer, position);
        position += L3421PrstzIniNforz.Len.L3421_PRSTZ_INI_NFORZ;
        l3421VisEnd2000Nforz.getL3421VisEnd2000NforzAsBuffer(buffer, position);
        position += L3421VisEnd2000Nforz.Len.L3421_VIS_END2000_NFORZ;
        l3421IntrMora.getL3421IntrMoraAsBuffer(buffer, position);
        position += L3421IntrMora.Len.L3421_INTR_MORA;
        l3421ManfeeAntic.getL3421ManfeeAnticAsBuffer(buffer, position);
        position += L3421ManfeeAntic.Len.L3421_MANFEE_ANTIC;
        l3421ManfeeRicor.getL3421ManfeeRicorAsBuffer(buffer, position);
        position += L3421ManfeeRicor.Len.L3421_MANFEE_RICOR;
        l3421PreUniRivto.getL3421PreUniRivtoAsBuffer(buffer, position);
        position += L3421PreUniRivto.Len.L3421_PRE_UNI_RIVTO;
        l3421Prov1aaAcq.getL3421Prov1aaAcqAsBuffer(buffer, position);
        position += L3421Prov1aaAcq.Len.L3421_PROV1AA_ACQ;
        l3421Prov2aaAcq.getL3421Prov2aaAcqAsBuffer(buffer, position);
        position += L3421Prov2aaAcq.Len.L3421_PROV2AA_ACQ;
        l3421ProvRicor.getL3421ProvRicorAsBuffer(buffer, position);
        position += L3421ProvRicor.Len.L3421_PROV_RICOR;
        l3421ProvInc.getL3421ProvIncAsBuffer(buffer, position);
        position += L3421ProvInc.Len.L3421_PROV_INC;
        l3421AlqProvAcq.getL3421AlqProvAcqAsBuffer(buffer, position);
        position += L3421AlqProvAcq.Len.L3421_ALQ_PROV_ACQ;
        l3421AlqProvInc.getL3421AlqProvIncAsBuffer(buffer, position);
        position += L3421AlqProvInc.Len.L3421_ALQ_PROV_INC;
        l3421AlqProvRicor.getL3421AlqProvRicorAsBuffer(buffer, position);
        position += L3421AlqProvRicor.Len.L3421_ALQ_PROV_RICOR;
        l3421ImpbProvAcq.getL3421ImpbProvAcqAsBuffer(buffer, position);
        position += L3421ImpbProvAcq.Len.L3421_IMPB_PROV_ACQ;
        l3421ImpbProvInc.getL3421ImpbProvIncAsBuffer(buffer, position);
        position += L3421ImpbProvInc.Len.L3421_IMPB_PROV_INC;
        l3421ImpbProvRicor.getL3421ImpbProvRicorAsBuffer(buffer, position);
        position += L3421ImpbProvRicor.Len.L3421_IMPB_PROV_RICOR;
        MarshalByte.writeChar(buffer, position, l3421FlProvForz);
        position += Types.CHAR_SIZE;
        l3421PrstzAggIni.getL3421PrstzAggIniAsBuffer(buffer, position);
        position += L3421PrstzAggIni.Len.L3421_PRSTZ_AGG_INI;
        l3421IncrPre.getL3421IncrPreAsBuffer(buffer, position);
        position += L3421IncrPre.Len.L3421_INCR_PRE;
        l3421IncrPrstz.getL3421IncrPrstzAsBuffer(buffer, position);
        position += L3421IncrPrstz.Len.L3421_INCR_PRSTZ;
        l3421DtUltAdegPrePr.getL3421DtUltAdegPrePrAsBuffer(buffer, position);
        position += L3421DtUltAdegPrePr.Len.L3421_DT_ULT_ADEG_PRE_PR;
        l3421PrstzAggUlt.getL3421PrstzAggUltAsBuffer(buffer, position);
        position += L3421PrstzAggUlt.Len.L3421_PRSTZ_AGG_ULT;
        l3421TsRivalNet.getL3421TsRivalNetAsBuffer(buffer, position);
        position += L3421TsRivalNet.Len.L3421_TS_RIVAL_NET;
        l3421PrePattuito.getL3421PrePattuitoAsBuffer(buffer, position);
        position += L3421PrePattuito.Len.L3421_PRE_PATTUITO;
        MarshalByte.writeString(buffer, position, l3421TpRival, Len.L3421_TP_RIVAL);
        position += Len.L3421_TP_RIVAL;
        l3421RisMat.getL3421RisMatAsBuffer(buffer, position);
        position += L3421RisMat.Len.L3421_RIS_MAT;
        MarshalByte.writeDecimalAsPacked(buffer, position, l3421CptMinScad.copy());
        position += Len.L3421_CPT_MIN_SCAD;
        MarshalByte.writeDecimalAsPacked(buffer, position, l3421CommisGest.copy());
        position += Len.L3421_COMMIS_GEST;
        MarshalByte.writeString(buffer, position, l3421TpManfeeAppl, Len.L3421_TP_MANFEE_APPL);
        position += Len.L3421_TP_MANFEE_APPL;
        MarshalByte.writeLongAsPacked(buffer, position, l3421DsRiga, Len.Int.L3421_DS_RIGA, 0);
        position += Len.L3421_DS_RIGA;
        MarshalByte.writeChar(buffer, position, l3421DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, l3421DsVer, Len.Int.L3421_DS_VER, 0);
        position += Len.L3421_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, l3421DsTsIniCptz, Len.Int.L3421_DS_TS_INI_CPTZ, 0);
        position += Len.L3421_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, l3421DsTsEndCptz, Len.Int.L3421_DS_TS_END_CPTZ, 0);
        position += Len.L3421_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, l3421DsUtente, Len.L3421_DS_UTENTE);
        position += Len.L3421_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, l3421DsStatoElab);
        position += Types.CHAR_SIZE;
        l3421PcCommisGest.getL3421PcCommisGestAsBuffer(buffer, position);
        position += L3421PcCommisGest.Len.L3421_PC_COMMIS_GEST;
        l3421NumGgRival.getL3421NumGgRivalAsBuffer(buffer, position);
        position += L3421NumGgRival.Len.L3421_NUM_GG_RIVAL;
        l3421ImpTrasfe.getL3421ImpTrasfeAsBuffer(buffer, position);
        position += L3421ImpTrasfe.Len.L3421_IMP_TRASFE;
        l3421ImpTfrStrc.getL3421ImpTfrStrcAsBuffer(buffer, position);
        position += L3421ImpTfrStrc.Len.L3421_IMP_TFR_STRC;
        l3421AcqExp.getL3421AcqExpAsBuffer(buffer, position);
        position += L3421AcqExp.Len.L3421_ACQ_EXP;
        l3421RemunAss.getL3421RemunAssAsBuffer(buffer, position);
        position += L3421RemunAss.Len.L3421_REMUN_ASS;
        l3421CommisInter.getL3421CommisInterAsBuffer(buffer, position);
        position += L3421CommisInter.Len.L3421_COMMIS_INTER;
        l3421AlqRemunAss.getL3421AlqRemunAssAsBuffer(buffer, position);
        position += L3421AlqRemunAss.Len.L3421_ALQ_REMUN_ASS;
        l3421AlqCommisInter.getL3421AlqCommisInterAsBuffer(buffer, position);
        position += L3421AlqCommisInter.Len.L3421_ALQ_COMMIS_INTER;
        l3421ImpbRemunAss.getL3421ImpbRemunAssAsBuffer(buffer, position);
        position += L3421ImpbRemunAss.Len.L3421_IMPB_REMUN_ASS;
        l3421ImpbCommisInter.getL3421ImpbCommisInterAsBuffer(buffer, position);
        position += L3421ImpbCommisInter.Len.L3421_IMPB_COMMIS_INTER;
        l3421CosRunAssva.getL3421CosRunAssvaAsBuffer(buffer, position);
        position += L3421CosRunAssva.Len.L3421_COS_RUN_ASSVA;
        l3421CosRunAssvaIdc.getL3421CosRunAssvaIdcAsBuffer(buffer, position);
        return buffer;
    }

    public void setL3421IdTrchDiGar(int l3421IdTrchDiGar) {
        this.l3421IdTrchDiGar = l3421IdTrchDiGar;
    }

    public int getL3421IdTrchDiGar() {
        return this.l3421IdTrchDiGar;
    }

    public void setL3421IdGar(int l3421IdGar) {
        this.l3421IdGar = l3421IdGar;
    }

    public int getL3421IdGar() {
        return this.l3421IdGar;
    }

    public void setL3421IdAdes(int l3421IdAdes) {
        this.l3421IdAdes = l3421IdAdes;
    }

    public int getL3421IdAdes() {
        return this.l3421IdAdes;
    }

    public void setL3421IdPoli(int l3421IdPoli) {
        this.l3421IdPoli = l3421IdPoli;
    }

    public int getL3421IdPoli() {
        return this.l3421IdPoli;
    }

    public void setL3421IdMoviCrz(int l3421IdMoviCrz) {
        this.l3421IdMoviCrz = l3421IdMoviCrz;
    }

    public int getL3421IdMoviCrz() {
        return this.l3421IdMoviCrz;
    }

    public void setL3421DtIniEff(int l3421DtIniEff) {
        this.l3421DtIniEff = l3421DtIniEff;
    }

    public int getL3421DtIniEff() {
        return this.l3421DtIniEff;
    }

    public void setL3421DtEndEff(int l3421DtEndEff) {
        this.l3421DtEndEff = l3421DtEndEff;
    }

    public int getL3421DtEndEff() {
        return this.l3421DtEndEff;
    }

    public void setL3421CodCompAnia(int l3421CodCompAnia) {
        this.l3421CodCompAnia = l3421CodCompAnia;
    }

    public int getL3421CodCompAnia() {
        return this.l3421CodCompAnia;
    }

    public void setL3421DtDecor(int l3421DtDecor) {
        this.l3421DtDecor = l3421DtDecor;
    }

    public int getL3421DtDecor() {
        return this.l3421DtDecor;
    }

    public void setL3421IbOgg(String l3421IbOgg) {
        this.l3421IbOgg = Functions.subString(l3421IbOgg, Len.L3421_IB_OGG);
    }

    public String getL3421IbOgg() {
        return this.l3421IbOgg;
    }

    public void setL3421TpRgmFisc(String l3421TpRgmFisc) {
        this.l3421TpRgmFisc = Functions.subString(l3421TpRgmFisc, Len.L3421_TP_RGM_FISC);
    }

    public String getL3421TpRgmFisc() {
        return this.l3421TpRgmFisc;
    }

    public void setL3421TpTrch(String l3421TpTrch) {
        this.l3421TpTrch = Functions.subString(l3421TpTrch, Len.L3421_TP_TRCH);
    }

    public String getL3421TpTrch() {
        return this.l3421TpTrch;
    }

    public void setL3421FlCarCont(char l3421FlCarCont) {
        this.l3421FlCarCont = l3421FlCarCont;
    }

    public char getL3421FlCarCont() {
        return this.l3421FlCarCont;
    }

    public void setL3421CodDvs(String l3421CodDvs) {
        this.l3421CodDvs = Functions.subString(l3421CodDvs, Len.L3421_COD_DVS);
    }

    public String getL3421CodDvs() {
        return this.l3421CodDvs;
    }

    public void setL3421TpAdegAbb(char l3421TpAdegAbb) {
        this.l3421TpAdegAbb = l3421TpAdegAbb;
    }

    public char getL3421TpAdegAbb() {
        return this.l3421TpAdegAbb;
    }

    public void setL3421ModCalc(String l3421ModCalc) {
        this.l3421ModCalc = Functions.subString(l3421ModCalc, Len.L3421_MOD_CALC);
    }

    public String getL3421ModCalc() {
        return this.l3421ModCalc;
    }

    public String getL3421ModCalcFormatted() {
        return Functions.padBlanks(getL3421ModCalc(), Len.L3421_MOD_CALC);
    }

    public void setL3421FlImportiForz(char l3421FlImportiForz) {
        this.l3421FlImportiForz = l3421FlImportiForz;
    }

    public char getL3421FlImportiForz() {
        return this.l3421FlImportiForz;
    }

    public void setL3421FlProvForz(char l3421FlProvForz) {
        this.l3421FlProvForz = l3421FlProvForz;
    }

    public char getL3421FlProvForz() {
        return this.l3421FlProvForz;
    }

    public void setL3421TpRival(String l3421TpRival) {
        this.l3421TpRival = Functions.subString(l3421TpRival, Len.L3421_TP_RIVAL);
    }

    public String getL3421TpRival() {
        return this.l3421TpRival;
    }

    public String getL3421TpRivalFormatted() {
        return Functions.padBlanks(getL3421TpRival(), Len.L3421_TP_RIVAL);
    }

    public void setL3421CptMinScad(AfDecimal l3421CptMinScad) {
        this.l3421CptMinScad.assign(l3421CptMinScad);
    }

    public AfDecimal getL3421CptMinScad() {
        return this.l3421CptMinScad.copy();
    }

    public void setL3421CommisGest(AfDecimal l3421CommisGest) {
        this.l3421CommisGest.assign(l3421CommisGest);
    }

    public AfDecimal getL3421CommisGest() {
        return this.l3421CommisGest.copy();
    }

    public void setL3421TpManfeeAppl(String l3421TpManfeeAppl) {
        this.l3421TpManfeeAppl = Functions.subString(l3421TpManfeeAppl, Len.L3421_TP_MANFEE_APPL);
    }

    public String getL3421TpManfeeAppl() {
        return this.l3421TpManfeeAppl;
    }

    public void setL3421DsRiga(long l3421DsRiga) {
        this.l3421DsRiga = l3421DsRiga;
    }

    public long getL3421DsRiga() {
        return this.l3421DsRiga;
    }

    public void setL3421DsOperSql(char l3421DsOperSql) {
        this.l3421DsOperSql = l3421DsOperSql;
    }

    public char getL3421DsOperSql() {
        return this.l3421DsOperSql;
    }

    public void setL3421DsVer(int l3421DsVer) {
        this.l3421DsVer = l3421DsVer;
    }

    public int getL3421DsVer() {
        return this.l3421DsVer;
    }

    public void setL3421DsTsIniCptz(long l3421DsTsIniCptz) {
        this.l3421DsTsIniCptz = l3421DsTsIniCptz;
    }

    public long getL3421DsTsIniCptz() {
        return this.l3421DsTsIniCptz;
    }

    public void setL3421DsTsEndCptz(long l3421DsTsEndCptz) {
        this.l3421DsTsEndCptz = l3421DsTsEndCptz;
    }

    public long getL3421DsTsEndCptz() {
        return this.l3421DsTsEndCptz;
    }

    public void setL3421DsUtente(String l3421DsUtente) {
        this.l3421DsUtente = Functions.subString(l3421DsUtente, Len.L3421_DS_UTENTE);
    }

    public String getL3421DsUtente() {
        return this.l3421DsUtente;
    }

    public void setL3421DsStatoElab(char l3421DsStatoElab) {
        this.l3421DsStatoElab = l3421DsStatoElab;
    }

    public char getL3421DsStatoElab() {
        return this.l3421DsStatoElab;
    }

    public L3421AbbAnnuUlt getL3421AbbAnnuUlt() {
        return l3421AbbAnnuUlt;
    }

    public L3421AbbTotIni getL3421AbbTotIni() {
        return l3421AbbTotIni;
    }

    public L3421AbbTotUlt getL3421AbbTotUlt() {
        return l3421AbbTotUlt;
    }

    public L3421AcqExp getL3421AcqExp() {
        return l3421AcqExp;
    }

    public L3421AlqCommisInter getL3421AlqCommisInter() {
        return l3421AlqCommisInter;
    }

    public L3421AlqProvAcq getL3421AlqProvAcq() {
        return l3421AlqProvAcq;
    }

    public L3421AlqProvInc getL3421AlqProvInc() {
        return l3421AlqProvInc;
    }

    public L3421AlqProvRicor getL3421AlqProvRicor() {
        return l3421AlqProvRicor;
    }

    public L3421AlqRemunAss getL3421AlqRemunAss() {
        return l3421AlqRemunAss;
    }

    public L3421AlqScon getL3421AlqScon() {
        return l3421AlqScon;
    }

    public L3421BnsGiaLiqto getL3421BnsGiaLiqto() {
        return l3421BnsGiaLiqto;
    }

    public L3421CommisInter getL3421CommisInter() {
        return l3421CommisInter;
    }

    public L3421CosRunAssva getL3421CosRunAssva() {
        return l3421CosRunAssva;
    }

    public L3421CosRunAssvaIdc getL3421CosRunAssvaIdc() {
        return l3421CosRunAssvaIdc;
    }

    public L3421CptInOpzRivto getL3421CptInOpzRivto() {
        return l3421CptInOpzRivto;
    }

    public L3421CptRshMor getL3421CptRshMor() {
        return l3421CptRshMor;
    }

    public L3421DtEffStab getL3421DtEffStab() {
        return l3421DtEffStab;
    }

    public L3421DtEmis getL3421DtEmis() {
        return l3421DtEmis;
    }

    public L3421DtIniValTar getL3421DtIniValTar() {
        return l3421DtIniValTar;
    }

    public L3421DtScad getL3421DtScad() {
        return l3421DtScad;
    }

    public L3421DtUltAdegPrePr getL3421DtUltAdegPrePr() {
        return l3421DtUltAdegPrePr;
    }

    public L3421DtVldtProd getL3421DtVldtProd() {
        return l3421DtVldtProd;
    }

    public L3421DurAa getL3421DurAa() {
        return l3421DurAa;
    }

    public L3421DurAbb getL3421DurAbb() {
        return l3421DurAbb;
    }

    public L3421DurGg getL3421DurGg() {
        return l3421DurGg;
    }

    public L3421DurMm getL3421DurMm() {
        return l3421DurMm;
    }

    public L3421EtaAa1oAssto getL3421EtaAa1oAssto() {
        return l3421EtaAa1oAssto;
    }

    public L3421EtaAa2oAssto getL3421EtaAa2oAssto() {
        return l3421EtaAa2oAssto;
    }

    public L3421EtaAa3oAssto getL3421EtaAa3oAssto() {
        return l3421EtaAa3oAssto;
    }

    public L3421EtaMm1oAssto getL3421EtaMm1oAssto() {
        return l3421EtaMm1oAssto;
    }

    public L3421EtaMm2oAssto getL3421EtaMm2oAssto() {
        return l3421EtaMm2oAssto;
    }

    public L3421EtaMm3oAssto getL3421EtaMm3oAssto() {
        return l3421EtaMm3oAssto;
    }

    public L3421IdMoviChiu getL3421IdMoviChiu() {
        return l3421IdMoviChiu;
    }

    public L3421ImpAder getL3421ImpAder() {
        return l3421ImpAder;
    }

    public L3421ImpAltSopr getL3421ImpAltSopr() {
        return l3421ImpAltSopr;
    }

    public L3421ImpAz getL3421ImpAz() {
        return l3421ImpAz;
    }

    public L3421ImpBns getL3421ImpBns() {
        return l3421ImpBns;
    }

    public L3421ImpBnsAntic getL3421ImpBnsAntic() {
        return l3421ImpBnsAntic;
    }

    public L3421ImpCarAcq getL3421ImpCarAcq() {
        return l3421ImpCarAcq;
    }

    public L3421ImpCarGest getL3421ImpCarGest() {
        return l3421ImpCarGest;
    }

    public L3421ImpCarInc getL3421ImpCarInc() {
        return l3421ImpCarInc;
    }

    public L3421ImpScon getL3421ImpScon() {
        return l3421ImpScon;
    }

    public L3421ImpSoprProf getL3421ImpSoprProf() {
        return l3421ImpSoprProf;
    }

    public L3421ImpSoprSan getL3421ImpSoprSan() {
        return l3421ImpSoprSan;
    }

    public L3421ImpSoprSpo getL3421ImpSoprSpo() {
        return l3421ImpSoprSpo;
    }

    public L3421ImpSoprTec getL3421ImpSoprTec() {
        return l3421ImpSoprTec;
    }

    public L3421ImpTfr getL3421ImpTfr() {
        return l3421ImpTfr;
    }

    public L3421ImpTfrStrc getL3421ImpTfrStrc() {
        return l3421ImpTfrStrc;
    }

    public L3421ImpTrasfe getL3421ImpTrasfe() {
        return l3421ImpTrasfe;
    }

    public L3421ImpVolo getL3421ImpVolo() {
        return l3421ImpVolo;
    }

    public L3421ImpbCommisInter getL3421ImpbCommisInter() {
        return l3421ImpbCommisInter;
    }

    public L3421ImpbProvAcq getL3421ImpbProvAcq() {
        return l3421ImpbProvAcq;
    }

    public L3421ImpbProvInc getL3421ImpbProvInc() {
        return l3421ImpbProvInc;
    }

    public L3421ImpbProvRicor getL3421ImpbProvRicor() {
        return l3421ImpbProvRicor;
    }

    public L3421ImpbRemunAss getL3421ImpbRemunAss() {
        return l3421ImpbRemunAss;
    }

    public L3421ImpbVisEnd2000 getL3421ImpbVisEnd2000() {
        return l3421ImpbVisEnd2000;
    }

    public L3421IncrPre getL3421IncrPre() {
        return l3421IncrPre;
    }

    public L3421IncrPrstz getL3421IncrPrstz() {
        return l3421IncrPrstz;
    }

    public L3421IntrMora getL3421IntrMora() {
        return l3421IntrMora;
    }

    public L3421ManfeeAntic getL3421ManfeeAntic() {
        return l3421ManfeeAntic;
    }

    public L3421ManfeeRicor getL3421ManfeeRicor() {
        return l3421ManfeeRicor;
    }

    public L3421MatuEnd2000 getL3421MatuEnd2000() {
        return l3421MatuEnd2000;
    }

    public L3421MinGarto getL3421MinGarto() {
        return l3421MinGarto;
    }

    public L3421MinTrnut getL3421MinTrnut() {
        return l3421MinTrnut;
    }

    public L3421NumGgRival getL3421NumGgRival() {
        return l3421NumGgRival;
    }

    public L3421OldTsTec getL3421OldTsTec() {
        return l3421OldTsTec;
    }

    public L3421PcCommisGest getL3421PcCommisGest() {
        return l3421PcCommisGest;
    }

    public L3421PcIntrRiat getL3421PcIntrRiat() {
        return l3421PcIntrRiat;
    }

    public L3421PcRetr getL3421PcRetr() {
        return l3421PcRetr;
    }

    public L3421PcRipPre getL3421PcRipPre() {
        return l3421PcRipPre;
    }

    public L3421PreAttDiTrch getL3421PreAttDiTrch() {
        return l3421PreAttDiTrch;
    }

    public L3421PreCasoMor getL3421PreCasoMor() {
        return l3421PreCasoMor;
    }

    public L3421PreIniNet getL3421PreIniNet() {
        return l3421PreIniNet;
    }

    public L3421PreInvrioIni getL3421PreInvrioIni() {
        return l3421PreInvrioIni;
    }

    public L3421PreInvrioUlt getL3421PreInvrioUlt() {
        return l3421PreInvrioUlt;
    }

    public L3421PreLrd getL3421PreLrd() {
        return l3421PreLrd;
    }

    public L3421PrePattuito getL3421PrePattuito() {
        return l3421PrePattuito;
    }

    public L3421PrePpIni getL3421PrePpIni() {
        return l3421PrePpIni;
    }

    public L3421PrePpUlt getL3421PrePpUlt() {
        return l3421PrePpUlt;
    }

    public L3421PreRivto getL3421PreRivto() {
        return l3421PreRivto;
    }

    public L3421PreStab getL3421PreStab() {
        return l3421PreStab;
    }

    public L3421PreTariIni getL3421PreTariIni() {
        return l3421PreTariIni;
    }

    public L3421PreTariUlt getL3421PreTariUlt() {
        return l3421PreTariUlt;
    }

    public L3421PreUniRivto getL3421PreUniRivto() {
        return l3421PreUniRivto;
    }

    public L3421Prov1aaAcq getL3421Prov1aaAcq() {
        return l3421Prov1aaAcq;
    }

    public L3421Prov2aaAcq getL3421Prov2aaAcq() {
        return l3421Prov2aaAcq;
    }

    public L3421ProvInc getL3421ProvInc() {
        return l3421ProvInc;
    }

    public L3421ProvRicor getL3421ProvRicor() {
        return l3421ProvRicor;
    }

    public L3421PrstzAggIni getL3421PrstzAggIni() {
        return l3421PrstzAggIni;
    }

    public L3421PrstzAggUlt getL3421PrstzAggUlt() {
        return l3421PrstzAggUlt;
    }

    public L3421PrstzIni getL3421PrstzIni() {
        return l3421PrstzIni;
    }

    public L3421PrstzIniNewfis getL3421PrstzIniNewfis() {
        return l3421PrstzIniNewfis;
    }

    public L3421PrstzIniNforz getL3421PrstzIniNforz() {
        return l3421PrstzIniNforz;
    }

    public L3421PrstzIniStab getL3421PrstzIniStab() {
        return l3421PrstzIniStab;
    }

    public L3421PrstzRidIni getL3421PrstzRidIni() {
        return l3421PrstzRidIni;
    }

    public L3421PrstzUlt getL3421PrstzUlt() {
        return l3421PrstzUlt;
    }

    public L3421RatLrd getL3421RatLrd() {
        return l3421RatLrd;
    }

    public L3421RemunAss getL3421RemunAss() {
        return l3421RemunAss;
    }

    public L3421RenIniTsTec0 getL3421RenIniTsTec0() {
        return l3421RenIniTsTec0;
    }

    public L3421RendtoLrd getL3421RendtoLrd() {
        return l3421RendtoLrd;
    }

    public L3421RendtoRetr getL3421RendtoRetr() {
        return l3421RendtoRetr;
    }

    public L3421RisMat getL3421RisMat() {
        return l3421RisMat;
    }

    public L3421TsRivalFis getL3421TsRivalFis() {
        return l3421TsRivalFis;
    }

    public L3421TsRivalIndiciz getL3421TsRivalIndiciz() {
        return l3421TsRivalIndiciz;
    }

    public L3421TsRivalNet getL3421TsRivalNet() {
        return l3421TsRivalNet;
    }

    public L3421VisEnd2000 getL3421VisEnd2000() {
        return l3421VisEnd2000;
    }

    public L3421VisEnd2000Nforz getL3421VisEnd2000Nforz() {
        return l3421VisEnd2000Nforz;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_ID_TRCH_DI_GAR = 5;
        public static final int L3421_ID_GAR = 5;
        public static final int L3421_ID_ADES = 5;
        public static final int L3421_ID_POLI = 5;
        public static final int L3421_ID_MOVI_CRZ = 5;
        public static final int L3421_DT_INI_EFF = 5;
        public static final int L3421_DT_END_EFF = 5;
        public static final int L3421_COD_COMP_ANIA = 3;
        public static final int L3421_DT_DECOR = 5;
        public static final int L3421_IB_OGG = 40;
        public static final int L3421_TP_RGM_FISC = 2;
        public static final int L3421_TP_TRCH = 2;
        public static final int L3421_FL_CAR_CONT = 1;
        public static final int L3421_COD_DVS = 20;
        public static final int L3421_TP_ADEG_ABB = 1;
        public static final int L3421_MOD_CALC = 2;
        public static final int L3421_FL_IMPORTI_FORZ = 1;
        public static final int L3421_FL_PROV_FORZ = 1;
        public static final int L3421_TP_RIVAL = 2;
        public static final int L3421_CPT_MIN_SCAD = 8;
        public static final int L3421_COMMIS_GEST = 8;
        public static final int L3421_TP_MANFEE_APPL = 2;
        public static final int L3421_DS_RIGA = 6;
        public static final int L3421_DS_OPER_SQL = 1;
        public static final int L3421_DS_VER = 5;
        public static final int L3421_DS_TS_INI_CPTZ = 10;
        public static final int L3421_DS_TS_END_CPTZ = 10;
        public static final int L3421_DS_UTENTE = 20;
        public static final int L3421_DS_STATO_ELAB = 1;
        public static final int LDBV3421_TGA_OUTPUT = L3421_ID_TRCH_DI_GAR + L3421_ID_GAR + L3421_ID_ADES + L3421_ID_POLI + L3421_ID_MOVI_CRZ + L3421IdMoviChiu.Len.L3421_ID_MOVI_CHIU + L3421_DT_INI_EFF + L3421_DT_END_EFF + L3421_COD_COMP_ANIA + L3421_DT_DECOR + L3421DtScad.Len.L3421_DT_SCAD + L3421_IB_OGG + L3421_TP_RGM_FISC + L3421DtEmis.Len.L3421_DT_EMIS + L3421_TP_TRCH + L3421DurAa.Len.L3421_DUR_AA + L3421DurMm.Len.L3421_DUR_MM + L3421DurGg.Len.L3421_DUR_GG + L3421PreCasoMor.Len.L3421_PRE_CASO_MOR + L3421PcIntrRiat.Len.L3421_PC_INTR_RIAT + L3421ImpBnsAntic.Len.L3421_IMP_BNS_ANTIC + L3421PreIniNet.Len.L3421_PRE_INI_NET + L3421PrePpIni.Len.L3421_PRE_PP_INI + L3421PrePpUlt.Len.L3421_PRE_PP_ULT + L3421PreTariIni.Len.L3421_PRE_TARI_INI + L3421PreTariUlt.Len.L3421_PRE_TARI_ULT + L3421PreInvrioIni.Len.L3421_PRE_INVRIO_INI + L3421PreInvrioUlt.Len.L3421_PRE_INVRIO_ULT + L3421PreRivto.Len.L3421_PRE_RIVTO + L3421ImpSoprProf.Len.L3421_IMP_SOPR_PROF + L3421ImpSoprSan.Len.L3421_IMP_SOPR_SAN + L3421ImpSoprSpo.Len.L3421_IMP_SOPR_SPO + L3421ImpSoprTec.Len.L3421_IMP_SOPR_TEC + L3421ImpAltSopr.Len.L3421_IMP_ALT_SOPR + L3421PreStab.Len.L3421_PRE_STAB + L3421DtEffStab.Len.L3421_DT_EFF_STAB + L3421TsRivalFis.Len.L3421_TS_RIVAL_FIS + L3421TsRivalIndiciz.Len.L3421_TS_RIVAL_INDICIZ + L3421OldTsTec.Len.L3421_OLD_TS_TEC + L3421RatLrd.Len.L3421_RAT_LRD + L3421PreLrd.Len.L3421_PRE_LRD + L3421PrstzIni.Len.L3421_PRSTZ_INI + L3421PrstzUlt.Len.L3421_PRSTZ_ULT + L3421CptInOpzRivto.Len.L3421_CPT_IN_OPZ_RIVTO + L3421PrstzIniStab.Len.L3421_PRSTZ_INI_STAB + L3421CptRshMor.Len.L3421_CPT_RSH_MOR + L3421PrstzRidIni.Len.L3421_PRSTZ_RID_INI + L3421_FL_CAR_CONT + L3421BnsGiaLiqto.Len.L3421_BNS_GIA_LIQTO + L3421ImpBns.Len.L3421_IMP_BNS + L3421_COD_DVS + L3421PrstzIniNewfis.Len.L3421_PRSTZ_INI_NEWFIS + L3421ImpScon.Len.L3421_IMP_SCON + L3421AlqScon.Len.L3421_ALQ_SCON + L3421ImpCarAcq.Len.L3421_IMP_CAR_ACQ + L3421ImpCarInc.Len.L3421_IMP_CAR_INC + L3421ImpCarGest.Len.L3421_IMP_CAR_GEST + L3421EtaAa1oAssto.Len.L3421_ETA_AA1O_ASSTO + L3421EtaMm1oAssto.Len.L3421_ETA_MM1O_ASSTO + L3421EtaAa2oAssto.Len.L3421_ETA_AA2O_ASSTO + L3421EtaMm2oAssto.Len.L3421_ETA_MM2O_ASSTO + L3421EtaAa3oAssto.Len.L3421_ETA_AA3O_ASSTO + L3421EtaMm3oAssto.Len.L3421_ETA_MM3O_ASSTO + L3421RendtoLrd.Len.L3421_RENDTO_LRD + L3421PcRetr.Len.L3421_PC_RETR + L3421RendtoRetr.Len.L3421_RENDTO_RETR + L3421MinGarto.Len.L3421_MIN_GARTO + L3421MinTrnut.Len.L3421_MIN_TRNUT + L3421PreAttDiTrch.Len.L3421_PRE_ATT_DI_TRCH + L3421MatuEnd2000.Len.L3421_MATU_END2000 + L3421AbbTotIni.Len.L3421_ABB_TOT_INI + L3421AbbTotUlt.Len.L3421_ABB_TOT_ULT + L3421AbbAnnuUlt.Len.L3421_ABB_ANNU_ULT + L3421DurAbb.Len.L3421_DUR_ABB + L3421_TP_ADEG_ABB + L3421_MOD_CALC + L3421ImpAz.Len.L3421_IMP_AZ + L3421ImpAder.Len.L3421_IMP_ADER + L3421ImpTfr.Len.L3421_IMP_TFR + L3421ImpVolo.Len.L3421_IMP_VOLO + L3421VisEnd2000.Len.L3421_VIS_END2000 + L3421DtVldtProd.Len.L3421_DT_VLDT_PROD + L3421DtIniValTar.Len.L3421_DT_INI_VAL_TAR + L3421ImpbVisEnd2000.Len.L3421_IMPB_VIS_END2000 + L3421RenIniTsTec0.Len.L3421_REN_INI_TS_TEC0 + L3421PcRipPre.Len.L3421_PC_RIP_PRE + L3421_FL_IMPORTI_FORZ + L3421PrstzIniNforz.Len.L3421_PRSTZ_INI_NFORZ + L3421VisEnd2000Nforz.Len.L3421_VIS_END2000_NFORZ + L3421IntrMora.Len.L3421_INTR_MORA + L3421ManfeeAntic.Len.L3421_MANFEE_ANTIC + L3421ManfeeRicor.Len.L3421_MANFEE_RICOR + L3421PreUniRivto.Len.L3421_PRE_UNI_RIVTO + L3421Prov1aaAcq.Len.L3421_PROV1AA_ACQ + L3421Prov2aaAcq.Len.L3421_PROV2AA_ACQ + L3421ProvRicor.Len.L3421_PROV_RICOR + L3421ProvInc.Len.L3421_PROV_INC + L3421AlqProvAcq.Len.L3421_ALQ_PROV_ACQ + L3421AlqProvInc.Len.L3421_ALQ_PROV_INC + L3421AlqProvRicor.Len.L3421_ALQ_PROV_RICOR + L3421ImpbProvAcq.Len.L3421_IMPB_PROV_ACQ + L3421ImpbProvInc.Len.L3421_IMPB_PROV_INC + L3421ImpbProvRicor.Len.L3421_IMPB_PROV_RICOR + L3421_FL_PROV_FORZ + L3421PrstzAggIni.Len.L3421_PRSTZ_AGG_INI + L3421IncrPre.Len.L3421_INCR_PRE + L3421IncrPrstz.Len.L3421_INCR_PRSTZ + L3421DtUltAdegPrePr.Len.L3421_DT_ULT_ADEG_PRE_PR + L3421PrstzAggUlt.Len.L3421_PRSTZ_AGG_ULT + L3421TsRivalNet.Len.L3421_TS_RIVAL_NET + L3421PrePattuito.Len.L3421_PRE_PATTUITO + L3421_TP_RIVAL + L3421RisMat.Len.L3421_RIS_MAT + L3421_CPT_MIN_SCAD + L3421_COMMIS_GEST + L3421_TP_MANFEE_APPL + L3421_DS_RIGA + L3421_DS_OPER_SQL + L3421_DS_VER + L3421_DS_TS_INI_CPTZ + L3421_DS_TS_END_CPTZ + L3421_DS_UTENTE + L3421_DS_STATO_ELAB + L3421PcCommisGest.Len.L3421_PC_COMMIS_GEST + L3421NumGgRival.Len.L3421_NUM_GG_RIVAL + L3421ImpTrasfe.Len.L3421_IMP_TRASFE + L3421ImpTfrStrc.Len.L3421_IMP_TFR_STRC + L3421AcqExp.Len.L3421_ACQ_EXP + L3421RemunAss.Len.L3421_REMUN_ASS + L3421CommisInter.Len.L3421_COMMIS_INTER + L3421AlqRemunAss.Len.L3421_ALQ_REMUN_ASS + L3421AlqCommisInter.Len.L3421_ALQ_COMMIS_INTER + L3421ImpbRemunAss.Len.L3421_IMPB_REMUN_ASS + L3421ImpbCommisInter.Len.L3421_IMPB_COMMIS_INTER + L3421CosRunAssva.Len.L3421_COS_RUN_ASSVA + L3421CosRunAssvaIdc.Len.L3421_COS_RUN_ASSVA_IDC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_ID_TRCH_DI_GAR = 9;
            public static final int L3421_ID_GAR = 9;
            public static final int L3421_ID_ADES = 9;
            public static final int L3421_ID_POLI = 9;
            public static final int L3421_ID_MOVI_CRZ = 9;
            public static final int L3421_DT_INI_EFF = 8;
            public static final int L3421_DT_END_EFF = 8;
            public static final int L3421_COD_COMP_ANIA = 5;
            public static final int L3421_DT_DECOR = 8;
            public static final int L3421_CPT_MIN_SCAD = 12;
            public static final int L3421_COMMIS_GEST = 12;
            public static final int L3421_DS_RIGA = 10;
            public static final int L3421_DS_VER = 9;
            public static final int L3421_DS_TS_INI_CPTZ = 18;
            public static final int L3421_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_CPT_MIN_SCAD = 3;
            public static final int L3421_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
