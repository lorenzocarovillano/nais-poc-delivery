package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.MovIdMoviAnn;
import it.accenture.jnais.ws.redefines.MovIdMoviCollg;
import it.accenture.jnais.ws.redefines.MovIdOgg;
import it.accenture.jnais.ws.redefines.MovIdRich;
import it.accenture.jnais.ws.redefines.MovTpMovi;

/**Original name: MOVI<br>
 * Variable: MOVI from copybook IDBVMOV1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Movi {

    //==== PROPERTIES ====
    //Original name: MOV-ID-MOVI
    private int movIdMovi = DefaultValues.INT_VAL;
    //Original name: MOV-COD-COMP-ANIA
    private int movCodCompAnia = DefaultValues.INT_VAL;
    //Original name: MOV-ID-OGG
    private MovIdOgg movIdOgg = new MovIdOgg();
    //Original name: MOV-IB-OGG
    private String movIbOgg = DefaultValues.stringVal(Len.MOV_IB_OGG);
    //Original name: MOV-IB-MOVI
    private String movIbMovi = DefaultValues.stringVal(Len.MOV_IB_MOVI);
    //Original name: MOV-TP-OGG
    private String movTpOgg = DefaultValues.stringVal(Len.MOV_TP_OGG);
    //Original name: MOV-ID-RICH
    private MovIdRich movIdRich = new MovIdRich();
    //Original name: MOV-TP-MOVI
    private MovTpMovi movTpMovi = new MovTpMovi();
    //Original name: MOV-DT-EFF
    private int movDtEff = DefaultValues.INT_VAL;
    //Original name: MOV-ID-MOVI-ANN
    private MovIdMoviAnn movIdMoviAnn = new MovIdMoviAnn();
    //Original name: MOV-ID-MOVI-COLLG
    private MovIdMoviCollg movIdMoviCollg = new MovIdMoviCollg();
    //Original name: MOV-DS-OPER-SQL
    private char movDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: MOV-DS-VER
    private int movDsVer = DefaultValues.INT_VAL;
    //Original name: MOV-DS-TS-CPTZ
    private long movDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: MOV-DS-UTENTE
    private String movDsUtente = DefaultValues.stringVal(Len.MOV_DS_UTENTE);
    //Original name: MOV-DS-STATO-ELAB
    private char movDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setMoviFormatted(String data) {
        byte[] buffer = new byte[Len.MOVI];
        MarshalByte.writeString(buffer, 1, data, Len.MOVI);
        setMoviBytes(buffer, 1);
    }

    public String getMoviFormatted() {
        return MarshalByteExt.bufferToStr(getMoviBytes());
    }

    public void setMoviBytes(byte[] buffer) {
        setMoviBytes(buffer, 1);
    }

    public byte[] getMoviBytes() {
        byte[] buffer = new byte[Len.MOVI];
        return getMoviBytes(buffer, 1);
    }

    public void setMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        movIdMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV_ID_MOVI, 0);
        position += Len.MOV_ID_MOVI;
        movCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV_COD_COMP_ANIA, 0);
        position += Len.MOV_COD_COMP_ANIA;
        movIdOgg.setMovIdOggFromBuffer(buffer, position);
        position += MovIdOgg.Len.MOV_ID_OGG;
        movIbOgg = MarshalByte.readString(buffer, position, Len.MOV_IB_OGG);
        position += Len.MOV_IB_OGG;
        movIbMovi = MarshalByte.readString(buffer, position, Len.MOV_IB_MOVI);
        position += Len.MOV_IB_MOVI;
        movTpOgg = MarshalByte.readString(buffer, position, Len.MOV_TP_OGG);
        position += Len.MOV_TP_OGG;
        movIdRich.setMovIdRichFromBuffer(buffer, position);
        position += MovIdRich.Len.MOV_ID_RICH;
        movTpMovi.setMovTpMoviFromBuffer(buffer, position);
        position += MovTpMovi.Len.MOV_TP_MOVI;
        movDtEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV_DT_EFF, 0);
        position += Len.MOV_DT_EFF;
        movIdMoviAnn.setMovIdMoviAnnFromBuffer(buffer, position);
        position += MovIdMoviAnn.Len.MOV_ID_MOVI_ANN;
        movIdMoviCollg.setMovIdMoviCollgFromBuffer(buffer, position);
        position += MovIdMoviCollg.Len.MOV_ID_MOVI_COLLG;
        movDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        movDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV_DS_VER, 0);
        position += Len.MOV_DS_VER;
        movDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.MOV_DS_TS_CPTZ, 0);
        position += Len.MOV_DS_TS_CPTZ;
        movDsUtente = MarshalByte.readString(buffer, position, Len.MOV_DS_UTENTE);
        position += Len.MOV_DS_UTENTE;
        movDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, movIdMovi, Len.Int.MOV_ID_MOVI, 0);
        position += Len.MOV_ID_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, movCodCompAnia, Len.Int.MOV_COD_COMP_ANIA, 0);
        position += Len.MOV_COD_COMP_ANIA;
        movIdOgg.getMovIdOggAsBuffer(buffer, position);
        position += MovIdOgg.Len.MOV_ID_OGG;
        MarshalByte.writeString(buffer, position, movIbOgg, Len.MOV_IB_OGG);
        position += Len.MOV_IB_OGG;
        MarshalByte.writeString(buffer, position, movIbMovi, Len.MOV_IB_MOVI);
        position += Len.MOV_IB_MOVI;
        MarshalByte.writeString(buffer, position, movTpOgg, Len.MOV_TP_OGG);
        position += Len.MOV_TP_OGG;
        movIdRich.getMovIdRichAsBuffer(buffer, position);
        position += MovIdRich.Len.MOV_ID_RICH;
        movTpMovi.getMovTpMoviAsBuffer(buffer, position);
        position += MovTpMovi.Len.MOV_TP_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, movDtEff, Len.Int.MOV_DT_EFF, 0);
        position += Len.MOV_DT_EFF;
        movIdMoviAnn.getMovIdMoviAnnAsBuffer(buffer, position);
        position += MovIdMoviAnn.Len.MOV_ID_MOVI_ANN;
        movIdMoviCollg.getMovIdMoviCollgAsBuffer(buffer, position);
        position += MovIdMoviCollg.Len.MOV_ID_MOVI_COLLG;
        MarshalByte.writeChar(buffer, position, movDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, movDsVer, Len.Int.MOV_DS_VER, 0);
        position += Len.MOV_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, movDsTsCptz, Len.Int.MOV_DS_TS_CPTZ, 0);
        position += Len.MOV_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, movDsUtente, Len.MOV_DS_UTENTE);
        position += Len.MOV_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, movDsStatoElab);
        return buffer;
    }

    public void setMovIdMovi(int movIdMovi) {
        this.movIdMovi = movIdMovi;
    }

    public int getMovIdMovi() {
        return this.movIdMovi;
    }

    public void setMovCodCompAnia(int movCodCompAnia) {
        this.movCodCompAnia = movCodCompAnia;
    }

    public int getMovCodCompAnia() {
        return this.movCodCompAnia;
    }

    public void setMovIbOgg(String movIbOgg) {
        this.movIbOgg = Functions.subString(movIbOgg, Len.MOV_IB_OGG);
    }

    public String getMovIbOgg() {
        return this.movIbOgg;
    }

    public void setMovIbMovi(String movIbMovi) {
        this.movIbMovi = Functions.subString(movIbMovi, Len.MOV_IB_MOVI);
    }

    public String getMovIbMovi() {
        return this.movIbMovi;
    }

    public void setMovTpOgg(String movTpOgg) {
        this.movTpOgg = Functions.subString(movTpOgg, Len.MOV_TP_OGG);
    }

    public String getMovTpOgg() {
        return this.movTpOgg;
    }

    public String getMovTpOggFormatted() {
        return Functions.padBlanks(getMovTpOgg(), Len.MOV_TP_OGG);
    }

    public void setMovDtEff(int movDtEff) {
        this.movDtEff = movDtEff;
    }

    public int getMovDtEff() {
        return this.movDtEff;
    }

    public void setMovDsOperSql(char movDsOperSql) {
        this.movDsOperSql = movDsOperSql;
    }

    public char getMovDsOperSql() {
        return this.movDsOperSql;
    }

    public void setMovDsVer(int movDsVer) {
        this.movDsVer = movDsVer;
    }

    public int getMovDsVer() {
        return this.movDsVer;
    }

    public void setMovDsTsCptz(long movDsTsCptz) {
        this.movDsTsCptz = movDsTsCptz;
    }

    public long getMovDsTsCptz() {
        return this.movDsTsCptz;
    }

    public void setMovDsUtente(String movDsUtente) {
        this.movDsUtente = Functions.subString(movDsUtente, Len.MOV_DS_UTENTE);
    }

    public String getMovDsUtente() {
        return this.movDsUtente;
    }

    public void setMovDsStatoElab(char movDsStatoElab) {
        this.movDsStatoElab = movDsStatoElab;
    }

    public char getMovDsStatoElab() {
        return this.movDsStatoElab;
    }

    public MovIdMoviAnn getMovIdMoviAnn() {
        return movIdMoviAnn;
    }

    public MovIdMoviCollg getMovIdMoviCollg() {
        return movIdMoviCollg;
    }

    public MovIdOgg getMovIdOgg() {
        return movIdOgg;
    }

    public MovIdRich getMovIdRich() {
        return movIdRich;
    }

    public MovTpMovi getMovTpMovi() {
        return movTpMovi;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MOV_IB_OGG = 40;
        public static final int MOV_IB_MOVI = 40;
        public static final int MOV_TP_OGG = 2;
        public static final int MOV_DS_UTENTE = 20;
        public static final int MOV_ID_MOVI = 5;
        public static final int MOV_COD_COMP_ANIA = 3;
        public static final int MOV_DT_EFF = 5;
        public static final int MOV_DS_OPER_SQL = 1;
        public static final int MOV_DS_VER = 5;
        public static final int MOV_DS_TS_CPTZ = 10;
        public static final int MOV_DS_STATO_ELAB = 1;
        public static final int MOVI = MOV_ID_MOVI + MOV_COD_COMP_ANIA + MovIdOgg.Len.MOV_ID_OGG + MOV_IB_OGG + MOV_IB_MOVI + MOV_TP_OGG + MovIdRich.Len.MOV_ID_RICH + MovTpMovi.Len.MOV_TP_MOVI + MOV_DT_EFF + MovIdMoviAnn.Len.MOV_ID_MOVI_ANN + MovIdMoviCollg.Len.MOV_ID_MOVI_COLLG + MOV_DS_OPER_SQL + MOV_DS_VER + MOV_DS_TS_CPTZ + MOV_DS_UTENTE + MOV_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MOV_ID_MOVI = 9;
            public static final int MOV_COD_COMP_ANIA = 5;
            public static final int MOV_DT_EFF = 8;
            public static final int MOV_DS_VER = 9;
            public static final int MOV_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
