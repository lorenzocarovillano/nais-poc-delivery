package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.LquAddizComun;
import it.accenture.jnais.ws.redefines.LquAddizRegion;
import it.accenture.jnais.ws.redefines.LquBnsNonGoduto;
import it.accenture.jnais.ws.redefines.LquCnbtInpstfm;
import it.accenture.jnais.ws.redefines.LquComponTaxRimb;
import it.accenture.jnais.ws.redefines.LquCosTunnelUscita;
import it.accenture.jnais.ws.redefines.LquDtDen;
import it.accenture.jnais.ws.redefines.LquDtEndIstr;
import it.accenture.jnais.ws.redefines.LquDtLiq;
import it.accenture.jnais.ws.redefines.LquDtMor;
import it.accenture.jnais.ws.redefines.LquDtPervDen;
import it.accenture.jnais.ws.redefines.LquDtRich;
import it.accenture.jnais.ws.redefines.LquDtVlt;
import it.accenture.jnais.ws.redefines.LquIdMoviChiu;
import it.accenture.jnais.ws.redefines.LquImpbAddizComun;
import it.accenture.jnais.ws.redefines.LquImpbAddizRegion;
import it.accenture.jnais.ws.redefines.LquImpbBolloDettC;
import it.accenture.jnais.ws.redefines.LquImpbCnbtInpstfm;
import it.accenture.jnais.ws.redefines.LquImpbImpst252;
import it.accenture.jnais.ws.redefines.LquImpbImpstPrvr;
import it.accenture.jnais.ws.redefines.LquImpbIntrSuPrest;
import it.accenture.jnais.ws.redefines.LquImpbIrpef;
import it.accenture.jnais.ws.redefines.LquImpbIs;
import it.accenture.jnais.ws.redefines.LquImpbIs1382011;
import it.accenture.jnais.ws.redefines.LquImpbIs662014;
import it.accenture.jnais.ws.redefines.LquImpbTaxSep;
import it.accenture.jnais.ws.redefines.LquImpbVis1382011;
import it.accenture.jnais.ws.redefines.LquImpbVis662014;
import it.accenture.jnais.ws.redefines.LquImpDirDaRimb;
import it.accenture.jnais.ws.redefines.LquImpDirLiq;
import it.accenture.jnais.ws.redefines.LquImpExcontr;
import it.accenture.jnais.ws.redefines.LquImpIntrRitPag;
import it.accenture.jnais.ws.redefines.LquImpLrdCalcCp;
import it.accenture.jnais.ws.redefines.LquImpLrdDaRimb;
import it.accenture.jnais.ws.redefines.LquImpLrdLiqtoRilt;
import it.accenture.jnais.ws.redefines.LquImpOnerLiq;
import it.accenture.jnais.ws.redefines.LquImpPnl;
import it.accenture.jnais.ws.redefines.LquImpRenK1;
import it.accenture.jnais.ws.redefines.LquImpRenK2;
import it.accenture.jnais.ws.redefines.LquImpRenK3;
import it.accenture.jnais.ws.redefines.LquImpst252;
import it.accenture.jnais.ws.redefines.LquImpstApplRilt;
import it.accenture.jnais.ws.redefines.LquImpstBolloDettC;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotAa;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotSw;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotV;
import it.accenture.jnais.ws.redefines.LquImpstDaRimb;
import it.accenture.jnais.ws.redefines.LquImpstIrpef;
import it.accenture.jnais.ws.redefines.LquImpstPrvr;
import it.accenture.jnais.ws.redefines.LquImpstSost1382011;
import it.accenture.jnais.ws.redefines.LquImpstSost662014;
import it.accenture.jnais.ws.redefines.LquImpstVis1382011;
import it.accenture.jnais.ws.redefines.LquImpstVis662014;
import it.accenture.jnais.ws.redefines.LquMontDal2007;
import it.accenture.jnais.ws.redefines.LquMontEnd2000;
import it.accenture.jnais.ws.redefines.LquMontEnd2006;
import it.accenture.jnais.ws.redefines.LquPcAbbTitStat;
import it.accenture.jnais.ws.redefines.LquPcAbbTs662014;
import it.accenture.jnais.ws.redefines.LquPcRen;
import it.accenture.jnais.ws.redefines.LquPcRenK1;
import it.accenture.jnais.ws.redefines.LquPcRenK2;
import it.accenture.jnais.ws.redefines.LquPcRenK3;
import it.accenture.jnais.ws.redefines.LquPcRiscParz;
import it.accenture.jnais.ws.redefines.LquRisMat;
import it.accenture.jnais.ws.redefines.LquRisSpe;
import it.accenture.jnais.ws.redefines.LquSpeRcs;
import it.accenture.jnais.ws.redefines.LquTaxSep;
import it.accenture.jnais.ws.redefines.LquTotIasMggSin;
import it.accenture.jnais.ws.redefines.LquTotIasOnerPrvnt;
import it.accenture.jnais.ws.redefines.LquTotIasPnl;
import it.accenture.jnais.ws.redefines.LquTotIasRstDpst;
import it.accenture.jnais.ws.redefines.LquTotImpbAcc;
import it.accenture.jnais.ws.redefines.LquTotImpbTfr;
import it.accenture.jnais.ws.redefines.LquTotImpbVis;
import it.accenture.jnais.ws.redefines.LquTotImpIntrPrest;
import it.accenture.jnais.ws.redefines.LquTotImpIs;
import it.accenture.jnais.ws.redefines.LquTotImpLrdLiqto;
import it.accenture.jnais.ws.redefines.LquTotImpNetLiqto;
import it.accenture.jnais.ws.redefines.LquTotImpPrest;
import it.accenture.jnais.ws.redefines.LquTotImpRimb;
import it.accenture.jnais.ws.redefines.LquTotImpRitAcc;
import it.accenture.jnais.ws.redefines.LquTotImpRitTfr;
import it.accenture.jnais.ws.redefines.LquTotImpRitVis;
import it.accenture.jnais.ws.redefines.LquTotImpUti;
import it.accenture.jnais.ws.redefines.LquTpMetRisc;

/**Original name: LIQ<br>
 * Variable: LIQ from copybook IDBVLQU1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Liq {

    //==== PROPERTIES ====
    //Original name: LQU-ID-LIQ
    private int lquIdLiq = DefaultValues.INT_VAL;
    //Original name: LQU-ID-OGG
    private int lquIdOgg = DefaultValues.INT_VAL;
    //Original name: LQU-TP-OGG
    private String lquTpOgg = DefaultValues.stringVal(Len.LQU_TP_OGG);
    //Original name: LQU-ID-MOVI-CRZ
    private int lquIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: LQU-ID-MOVI-CHIU
    private LquIdMoviChiu lquIdMoviChiu = new LquIdMoviChiu();
    //Original name: LQU-DT-INI-EFF
    private int lquDtIniEff = DefaultValues.INT_VAL;
    //Original name: LQU-DT-END-EFF
    private int lquDtEndEff = DefaultValues.INT_VAL;
    //Original name: LQU-COD-COMP-ANIA
    private int lquCodCompAnia = DefaultValues.INT_VAL;
    //Original name: LQU-IB-OGG
    private String lquIbOgg = DefaultValues.stringVal(Len.LQU_IB_OGG);
    //Original name: LQU-TP-LIQ
    private String lquTpLiq = DefaultValues.stringVal(Len.LQU_TP_LIQ);
    //Original name: LQU-DESC-CAU-EVE-SIN-LEN
    private short lquDescCauEveSinLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: LQU-DESC-CAU-EVE-SIN
    private String lquDescCauEveSin = DefaultValues.stringVal(Len.LQU_DESC_CAU_EVE_SIN);
    //Original name: LQU-COD-CAU-SIN
    private String lquCodCauSin = DefaultValues.stringVal(Len.LQU_COD_CAU_SIN);
    //Original name: LQU-COD-SIN-CATSTRF
    private String lquCodSinCatstrf = DefaultValues.stringVal(Len.LQU_COD_SIN_CATSTRF);
    //Original name: LQU-DT-MOR
    private LquDtMor lquDtMor = new LquDtMor();
    //Original name: LQU-DT-DEN
    private LquDtDen lquDtDen = new LquDtDen();
    //Original name: LQU-DT-PERV-DEN
    private LquDtPervDen lquDtPervDen = new LquDtPervDen();
    //Original name: LQU-DT-RICH
    private LquDtRich lquDtRich = new LquDtRich();
    //Original name: LQU-TP-SIN
    private String lquTpSin = DefaultValues.stringVal(Len.LQU_TP_SIN);
    //Original name: LQU-TP-RISC
    private String lquTpRisc = DefaultValues.stringVal(Len.LQU_TP_RISC);
    //Original name: LQU-TP-MET-RISC
    private LquTpMetRisc lquTpMetRisc = new LquTpMetRisc();
    //Original name: LQU-DT-LIQ
    private LquDtLiq lquDtLiq = new LquDtLiq();
    //Original name: LQU-COD-DVS
    private String lquCodDvs = DefaultValues.stringVal(Len.LQU_COD_DVS);
    //Original name: LQU-TOT-IMP-LRD-LIQTO
    private LquTotImpLrdLiqto lquTotImpLrdLiqto = new LquTotImpLrdLiqto();
    //Original name: LQU-TOT-IMP-PREST
    private LquTotImpPrest lquTotImpPrest = new LquTotImpPrest();
    //Original name: LQU-TOT-IMP-INTR-PREST
    private LquTotImpIntrPrest lquTotImpIntrPrest = new LquTotImpIntrPrest();
    //Original name: LQU-TOT-IMP-UTI
    private LquTotImpUti lquTotImpUti = new LquTotImpUti();
    //Original name: LQU-TOT-IMP-RIT-TFR
    private LquTotImpRitTfr lquTotImpRitTfr = new LquTotImpRitTfr();
    //Original name: LQU-TOT-IMP-RIT-ACC
    private LquTotImpRitAcc lquTotImpRitAcc = new LquTotImpRitAcc();
    //Original name: LQU-TOT-IMP-RIT-VIS
    private LquTotImpRitVis lquTotImpRitVis = new LquTotImpRitVis();
    //Original name: LQU-TOT-IMPB-TFR
    private LquTotImpbTfr lquTotImpbTfr = new LquTotImpbTfr();
    //Original name: LQU-TOT-IMPB-ACC
    private LquTotImpbAcc lquTotImpbAcc = new LquTotImpbAcc();
    //Original name: LQU-TOT-IMPB-VIS
    private LquTotImpbVis lquTotImpbVis = new LquTotImpbVis();
    //Original name: LQU-TOT-IMP-RIMB
    private LquTotImpRimb lquTotImpRimb = new LquTotImpRimb();
    //Original name: LQU-IMPB-IMPST-PRVR
    private LquImpbImpstPrvr lquImpbImpstPrvr = new LquImpbImpstPrvr();
    //Original name: LQU-IMPST-PRVR
    private LquImpstPrvr lquImpstPrvr = new LquImpstPrvr();
    //Original name: LQU-IMPB-IMPST-252
    private LquImpbImpst252 lquImpbImpst252 = new LquImpbImpst252();
    //Original name: LQU-IMPST-252
    private LquImpst252 lquImpst252 = new LquImpst252();
    //Original name: LQU-TOT-IMP-IS
    private LquTotImpIs lquTotImpIs = new LquTotImpIs();
    //Original name: LQU-IMP-DIR-LIQ
    private LquImpDirLiq lquImpDirLiq = new LquImpDirLiq();
    //Original name: LQU-TOT-IMP-NET-LIQTO
    private LquTotImpNetLiqto lquTotImpNetLiqto = new LquTotImpNetLiqto();
    //Original name: LQU-MONT-END2000
    private LquMontEnd2000 lquMontEnd2000 = new LquMontEnd2000();
    //Original name: LQU-MONT-END2006
    private LquMontEnd2006 lquMontEnd2006 = new LquMontEnd2006();
    //Original name: LQU-PC-REN
    private LquPcRen lquPcRen = new LquPcRen();
    //Original name: LQU-IMP-PNL
    private LquImpPnl lquImpPnl = new LquImpPnl();
    //Original name: LQU-IMPB-IRPEF
    private LquImpbIrpef lquImpbIrpef = new LquImpbIrpef();
    //Original name: LQU-IMPST-IRPEF
    private LquImpstIrpef lquImpstIrpef = new LquImpstIrpef();
    //Original name: LQU-DT-VLT
    private LquDtVlt lquDtVlt = new LquDtVlt();
    //Original name: LQU-DT-END-ISTR
    private LquDtEndIstr lquDtEndIstr = new LquDtEndIstr();
    //Original name: LQU-TP-RIMB
    private String lquTpRimb = DefaultValues.stringVal(Len.LQU_TP_RIMB);
    //Original name: LQU-SPE-RCS
    private LquSpeRcs lquSpeRcs = new LquSpeRcs();
    //Original name: LQU-IB-LIQ
    private String lquIbLiq = DefaultValues.stringVal(Len.LQU_IB_LIQ);
    //Original name: LQU-TOT-IAS-ONER-PRVNT
    private LquTotIasOnerPrvnt lquTotIasOnerPrvnt = new LquTotIasOnerPrvnt();
    //Original name: LQU-TOT-IAS-MGG-SIN
    private LquTotIasMggSin lquTotIasMggSin = new LquTotIasMggSin();
    //Original name: LQU-TOT-IAS-RST-DPST
    private LquTotIasRstDpst lquTotIasRstDpst = new LquTotIasRstDpst();
    //Original name: LQU-IMP-ONER-LIQ
    private LquImpOnerLiq lquImpOnerLiq = new LquImpOnerLiq();
    //Original name: LQU-COMPON-TAX-RIMB
    private LquComponTaxRimb lquComponTaxRimb = new LquComponTaxRimb();
    //Original name: LQU-TP-MEZ-PAG
    private String lquTpMezPag = DefaultValues.stringVal(Len.LQU_TP_MEZ_PAG);
    //Original name: LQU-IMP-EXCONTR
    private LquImpExcontr lquImpExcontr = new LquImpExcontr();
    //Original name: LQU-IMP-INTR-RIT-PAG
    private LquImpIntrRitPag lquImpIntrRitPag = new LquImpIntrRitPag();
    //Original name: LQU-BNS-NON-GODUTO
    private LquBnsNonGoduto lquBnsNonGoduto = new LquBnsNonGoduto();
    //Original name: LQU-CNBT-INPSTFM
    private LquCnbtInpstfm lquCnbtInpstfm = new LquCnbtInpstfm();
    //Original name: LQU-IMPST-DA-RIMB
    private LquImpstDaRimb lquImpstDaRimb = new LquImpstDaRimb();
    //Original name: LQU-IMPB-IS
    private LquImpbIs lquImpbIs = new LquImpbIs();
    //Original name: LQU-TAX-SEP
    private LquTaxSep lquTaxSep = new LquTaxSep();
    //Original name: LQU-IMPB-TAX-SEP
    private LquImpbTaxSep lquImpbTaxSep = new LquImpbTaxSep();
    //Original name: LQU-IMPB-INTR-SU-PREST
    private LquImpbIntrSuPrest lquImpbIntrSuPrest = new LquImpbIntrSuPrest();
    //Original name: LQU-ADDIZ-COMUN
    private LquAddizComun lquAddizComun = new LquAddizComun();
    //Original name: LQU-IMPB-ADDIZ-COMUN
    private LquImpbAddizComun lquImpbAddizComun = new LquImpbAddizComun();
    //Original name: LQU-ADDIZ-REGION
    private LquAddizRegion lquAddizRegion = new LquAddizRegion();
    //Original name: LQU-IMPB-ADDIZ-REGION
    private LquImpbAddizRegion lquImpbAddizRegion = new LquImpbAddizRegion();
    //Original name: LQU-MONT-DAL2007
    private LquMontDal2007 lquMontDal2007 = new LquMontDal2007();
    //Original name: LQU-IMPB-CNBT-INPSTFM
    private LquImpbCnbtInpstfm lquImpbCnbtInpstfm = new LquImpbCnbtInpstfm();
    //Original name: LQU-IMP-LRD-DA-RIMB
    private LquImpLrdDaRimb lquImpLrdDaRimb = new LquImpLrdDaRimb();
    //Original name: LQU-IMP-DIR-DA-RIMB
    private LquImpDirDaRimb lquImpDirDaRimb = new LquImpDirDaRimb();
    //Original name: LQU-RIS-MAT
    private LquRisMat lquRisMat = new LquRisMat();
    //Original name: LQU-RIS-SPE
    private LquRisSpe lquRisSpe = new LquRisSpe();
    //Original name: LQU-DS-RIGA
    private long lquDsRiga = DefaultValues.LONG_VAL;
    //Original name: LQU-DS-OPER-SQL
    private char lquDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: LQU-DS-VER
    private int lquDsVer = DefaultValues.INT_VAL;
    //Original name: LQU-DS-TS-INI-CPTZ
    private long lquDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: LQU-DS-TS-END-CPTZ
    private long lquDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: LQU-DS-UTENTE
    private String lquDsUtente = DefaultValues.stringVal(Len.LQU_DS_UTENTE);
    //Original name: LQU-DS-STATO-ELAB
    private char lquDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: LQU-TOT-IAS-PNL
    private LquTotIasPnl lquTotIasPnl = new LquTotIasPnl();
    //Original name: LQU-FL-EVE-GARTO
    private char lquFlEveGarto = DefaultValues.CHAR_VAL;
    //Original name: LQU-IMP-REN-K1
    private LquImpRenK1 lquImpRenK1 = new LquImpRenK1();
    //Original name: LQU-IMP-REN-K2
    private LquImpRenK2 lquImpRenK2 = new LquImpRenK2();
    //Original name: LQU-IMP-REN-K3
    private LquImpRenK3 lquImpRenK3 = new LquImpRenK3();
    //Original name: LQU-PC-REN-K1
    private LquPcRenK1 lquPcRenK1 = new LquPcRenK1();
    //Original name: LQU-PC-REN-K2
    private LquPcRenK2 lquPcRenK2 = new LquPcRenK2();
    //Original name: LQU-PC-REN-K3
    private LquPcRenK3 lquPcRenK3 = new LquPcRenK3();
    //Original name: LQU-TP-CAUS-ANTIC
    private String lquTpCausAntic = DefaultValues.stringVal(Len.LQU_TP_CAUS_ANTIC);
    //Original name: LQU-IMP-LRD-LIQTO-RILT
    private LquImpLrdLiqtoRilt lquImpLrdLiqtoRilt = new LquImpLrdLiqtoRilt();
    //Original name: LQU-IMPST-APPL-RILT
    private LquImpstApplRilt lquImpstApplRilt = new LquImpstApplRilt();
    //Original name: LQU-PC-RISC-PARZ
    private LquPcRiscParz lquPcRiscParz = new LquPcRiscParz();
    //Original name: LQU-IMPST-BOLLO-TOT-V
    private LquImpstBolloTotV lquImpstBolloTotV = new LquImpstBolloTotV();
    //Original name: LQU-IMPST-BOLLO-DETT-C
    private LquImpstBolloDettC lquImpstBolloDettC = new LquImpstBolloDettC();
    //Original name: LQU-IMPST-BOLLO-TOT-SW
    private LquImpstBolloTotSw lquImpstBolloTotSw = new LquImpstBolloTotSw();
    //Original name: LQU-IMPST-BOLLO-TOT-AA
    private LquImpstBolloTotAa lquImpstBolloTotAa = new LquImpstBolloTotAa();
    //Original name: LQU-IMPB-VIS-1382011
    private LquImpbVis1382011 lquImpbVis1382011 = new LquImpbVis1382011();
    //Original name: LQU-IMPST-VIS-1382011
    private LquImpstVis1382011 lquImpstVis1382011 = new LquImpstVis1382011();
    //Original name: LQU-IMPB-IS-1382011
    private LquImpbIs1382011 lquImpbIs1382011 = new LquImpbIs1382011();
    //Original name: LQU-IMPST-SOST-1382011
    private LquImpstSost1382011 lquImpstSost1382011 = new LquImpstSost1382011();
    //Original name: LQU-PC-ABB-TIT-STAT
    private LquPcAbbTitStat lquPcAbbTitStat = new LquPcAbbTitStat();
    //Original name: LQU-IMPB-BOLLO-DETT-C
    private LquImpbBolloDettC lquImpbBolloDettC = new LquImpbBolloDettC();
    //Original name: LQU-FL-PRE-COMP
    private char lquFlPreComp = DefaultValues.CHAR_VAL;
    //Original name: LQU-IMPB-VIS-662014
    private LquImpbVis662014 lquImpbVis662014 = new LquImpbVis662014();
    //Original name: LQU-IMPST-VIS-662014
    private LquImpstVis662014 lquImpstVis662014 = new LquImpstVis662014();
    //Original name: LQU-IMPB-IS-662014
    private LquImpbIs662014 lquImpbIs662014 = new LquImpbIs662014();
    //Original name: LQU-IMPST-SOST-662014
    private LquImpstSost662014 lquImpstSost662014 = new LquImpstSost662014();
    //Original name: LQU-PC-ABB-TS-662014
    private LquPcAbbTs662014 lquPcAbbTs662014 = new LquPcAbbTs662014();
    //Original name: LQU-IMP-LRD-CALC-CP
    private LquImpLrdCalcCp lquImpLrdCalcCp = new LquImpLrdCalcCp();
    //Original name: LQU-COS-TUNNEL-USCITA
    private LquCosTunnelUscita lquCosTunnelUscita = new LquCosTunnelUscita();

    //==== METHODS ====
    public void setLiqFormatted(String data) {
        byte[] buffer = new byte[Len.LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.LIQ);
        setLiqBytes(buffer, 1);
    }

    public String getLiqFormatted() {
        return MarshalByteExt.bufferToStr(getLiqBytes());
    }

    public byte[] getLiqBytes() {
        byte[] buffer = new byte[Len.LIQ];
        return getLiqBytes(buffer, 1);
    }

    public void setLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        lquIdLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LQU_ID_LIQ, 0);
        position += Len.LQU_ID_LIQ;
        lquIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LQU_ID_OGG, 0);
        position += Len.LQU_ID_OGG;
        lquTpOgg = MarshalByte.readString(buffer, position, Len.LQU_TP_OGG);
        position += Len.LQU_TP_OGG;
        lquIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LQU_ID_MOVI_CRZ, 0);
        position += Len.LQU_ID_MOVI_CRZ;
        lquIdMoviChiu.setLquIdMoviChiuFromBuffer(buffer, position);
        position += LquIdMoviChiu.Len.LQU_ID_MOVI_CHIU;
        lquDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LQU_DT_INI_EFF, 0);
        position += Len.LQU_DT_INI_EFF;
        lquDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LQU_DT_END_EFF, 0);
        position += Len.LQU_DT_END_EFF;
        lquCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LQU_COD_COMP_ANIA, 0);
        position += Len.LQU_COD_COMP_ANIA;
        lquIbOgg = MarshalByte.readString(buffer, position, Len.LQU_IB_OGG);
        position += Len.LQU_IB_OGG;
        lquTpLiq = MarshalByte.readString(buffer, position, Len.LQU_TP_LIQ);
        position += Len.LQU_TP_LIQ;
        setLquDescCauEveSinVcharBytes(buffer, position);
        position += Len.LQU_DESC_CAU_EVE_SIN_VCHAR;
        lquCodCauSin = MarshalByte.readString(buffer, position, Len.LQU_COD_CAU_SIN);
        position += Len.LQU_COD_CAU_SIN;
        lquCodSinCatstrf = MarshalByte.readString(buffer, position, Len.LQU_COD_SIN_CATSTRF);
        position += Len.LQU_COD_SIN_CATSTRF;
        lquDtMor.setLquDtMorFromBuffer(buffer, position);
        position += LquDtMor.Len.LQU_DT_MOR;
        lquDtDen.setLquDtDenFromBuffer(buffer, position);
        position += LquDtDen.Len.LQU_DT_DEN;
        lquDtPervDen.setLquDtPervDenFromBuffer(buffer, position);
        position += LquDtPervDen.Len.LQU_DT_PERV_DEN;
        lquDtRich.setLquDtRichFromBuffer(buffer, position);
        position += LquDtRich.Len.LQU_DT_RICH;
        lquTpSin = MarshalByte.readString(buffer, position, Len.LQU_TP_SIN);
        position += Len.LQU_TP_SIN;
        lquTpRisc = MarshalByte.readString(buffer, position, Len.LQU_TP_RISC);
        position += Len.LQU_TP_RISC;
        lquTpMetRisc.setLquTpMetRiscFromBuffer(buffer, position);
        position += LquTpMetRisc.Len.LQU_TP_MET_RISC;
        lquDtLiq.setLquDtLiqFromBuffer(buffer, position);
        position += LquDtLiq.Len.LQU_DT_LIQ;
        lquCodDvs = MarshalByte.readString(buffer, position, Len.LQU_COD_DVS);
        position += Len.LQU_COD_DVS;
        lquTotImpLrdLiqto.setLquTotImpLrdLiqtoFromBuffer(buffer, position);
        position += LquTotImpLrdLiqto.Len.LQU_TOT_IMP_LRD_LIQTO;
        lquTotImpPrest.setLquTotImpPrestFromBuffer(buffer, position);
        position += LquTotImpPrest.Len.LQU_TOT_IMP_PREST;
        lquTotImpIntrPrest.setLquTotImpIntrPrestFromBuffer(buffer, position);
        position += LquTotImpIntrPrest.Len.LQU_TOT_IMP_INTR_PREST;
        lquTotImpUti.setLquTotImpUtiFromBuffer(buffer, position);
        position += LquTotImpUti.Len.LQU_TOT_IMP_UTI;
        lquTotImpRitTfr.setLquTotImpRitTfrFromBuffer(buffer, position);
        position += LquTotImpRitTfr.Len.LQU_TOT_IMP_RIT_TFR;
        lquTotImpRitAcc.setLquTotImpRitAccFromBuffer(buffer, position);
        position += LquTotImpRitAcc.Len.LQU_TOT_IMP_RIT_ACC;
        lquTotImpRitVis.setLquTotImpRitVisFromBuffer(buffer, position);
        position += LquTotImpRitVis.Len.LQU_TOT_IMP_RIT_VIS;
        lquTotImpbTfr.setLquTotImpbTfrFromBuffer(buffer, position);
        position += LquTotImpbTfr.Len.LQU_TOT_IMPB_TFR;
        lquTotImpbAcc.setLquTotImpbAccFromBuffer(buffer, position);
        position += LquTotImpbAcc.Len.LQU_TOT_IMPB_ACC;
        lquTotImpbVis.setLquTotImpbVisFromBuffer(buffer, position);
        position += LquTotImpbVis.Len.LQU_TOT_IMPB_VIS;
        lquTotImpRimb.setLquTotImpRimbFromBuffer(buffer, position);
        position += LquTotImpRimb.Len.LQU_TOT_IMP_RIMB;
        lquImpbImpstPrvr.setLquImpbImpstPrvrFromBuffer(buffer, position);
        position += LquImpbImpstPrvr.Len.LQU_IMPB_IMPST_PRVR;
        lquImpstPrvr.setLquImpstPrvrFromBuffer(buffer, position);
        position += LquImpstPrvr.Len.LQU_IMPST_PRVR;
        lquImpbImpst252.setLquImpbImpst252FromBuffer(buffer, position);
        position += LquImpbImpst252.Len.LQU_IMPB_IMPST252;
        lquImpst252.setLquImpst252FromBuffer(buffer, position);
        position += LquImpst252.Len.LQU_IMPST252;
        lquTotImpIs.setLquTotImpIsFromBuffer(buffer, position);
        position += LquTotImpIs.Len.LQU_TOT_IMP_IS;
        lquImpDirLiq.setLquImpDirLiqFromBuffer(buffer, position);
        position += LquImpDirLiq.Len.LQU_IMP_DIR_LIQ;
        lquTotImpNetLiqto.setLquTotImpNetLiqtoFromBuffer(buffer, position);
        position += LquTotImpNetLiqto.Len.LQU_TOT_IMP_NET_LIQTO;
        lquMontEnd2000.setLquMontEnd2000FromBuffer(buffer, position);
        position += LquMontEnd2000.Len.LQU_MONT_END2000;
        lquMontEnd2006.setLquMontEnd2006FromBuffer(buffer, position);
        position += LquMontEnd2006.Len.LQU_MONT_END2006;
        lquPcRen.setLquPcRenFromBuffer(buffer, position);
        position += LquPcRen.Len.LQU_PC_REN;
        lquImpPnl.setLquImpPnlFromBuffer(buffer, position);
        position += LquImpPnl.Len.LQU_IMP_PNL;
        lquImpbIrpef.setLquImpbIrpefFromBuffer(buffer, position);
        position += LquImpbIrpef.Len.LQU_IMPB_IRPEF;
        lquImpstIrpef.setLquImpstIrpefFromBuffer(buffer, position);
        position += LquImpstIrpef.Len.LQU_IMPST_IRPEF;
        lquDtVlt.setLquDtVltFromBuffer(buffer, position);
        position += LquDtVlt.Len.LQU_DT_VLT;
        lquDtEndIstr.setLquDtEndIstrFromBuffer(buffer, position);
        position += LquDtEndIstr.Len.LQU_DT_END_ISTR;
        lquTpRimb = MarshalByte.readString(buffer, position, Len.LQU_TP_RIMB);
        position += Len.LQU_TP_RIMB;
        lquSpeRcs.setLquSpeRcsFromBuffer(buffer, position);
        position += LquSpeRcs.Len.LQU_SPE_RCS;
        lquIbLiq = MarshalByte.readString(buffer, position, Len.LQU_IB_LIQ);
        position += Len.LQU_IB_LIQ;
        lquTotIasOnerPrvnt.setLquTotIasOnerPrvntFromBuffer(buffer, position);
        position += LquTotIasOnerPrvnt.Len.LQU_TOT_IAS_ONER_PRVNT;
        lquTotIasMggSin.setLquTotIasMggSinFromBuffer(buffer, position);
        position += LquTotIasMggSin.Len.LQU_TOT_IAS_MGG_SIN;
        lquTotIasRstDpst.setLquTotIasRstDpstFromBuffer(buffer, position);
        position += LquTotIasRstDpst.Len.LQU_TOT_IAS_RST_DPST;
        lquImpOnerLiq.setLquImpOnerLiqFromBuffer(buffer, position);
        position += LquImpOnerLiq.Len.LQU_IMP_ONER_LIQ;
        lquComponTaxRimb.setLquComponTaxRimbFromBuffer(buffer, position);
        position += LquComponTaxRimb.Len.LQU_COMPON_TAX_RIMB;
        lquTpMezPag = MarshalByte.readString(buffer, position, Len.LQU_TP_MEZ_PAG);
        position += Len.LQU_TP_MEZ_PAG;
        lquImpExcontr.setLquImpExcontrFromBuffer(buffer, position);
        position += LquImpExcontr.Len.LQU_IMP_EXCONTR;
        lquImpIntrRitPag.setLquImpIntrRitPagFromBuffer(buffer, position);
        position += LquImpIntrRitPag.Len.LQU_IMP_INTR_RIT_PAG;
        lquBnsNonGoduto.setLquBnsNonGodutoFromBuffer(buffer, position);
        position += LquBnsNonGoduto.Len.LQU_BNS_NON_GODUTO;
        lquCnbtInpstfm.setLquCnbtInpstfmFromBuffer(buffer, position);
        position += LquCnbtInpstfm.Len.LQU_CNBT_INPSTFM;
        lquImpstDaRimb.setLquImpstDaRimbFromBuffer(buffer, position);
        position += LquImpstDaRimb.Len.LQU_IMPST_DA_RIMB;
        lquImpbIs.setLquImpbIsFromBuffer(buffer, position);
        position += LquImpbIs.Len.LQU_IMPB_IS;
        lquTaxSep.setLquTaxSepFromBuffer(buffer, position);
        position += LquTaxSep.Len.LQU_TAX_SEP;
        lquImpbTaxSep.setLquImpbTaxSepFromBuffer(buffer, position);
        position += LquImpbTaxSep.Len.LQU_IMPB_TAX_SEP;
        lquImpbIntrSuPrest.setLquImpbIntrSuPrestFromBuffer(buffer, position);
        position += LquImpbIntrSuPrest.Len.LQU_IMPB_INTR_SU_PREST;
        lquAddizComun.setLquAddizComunFromBuffer(buffer, position);
        position += LquAddizComun.Len.LQU_ADDIZ_COMUN;
        lquImpbAddizComun.setLquImpbAddizComunFromBuffer(buffer, position);
        position += LquImpbAddizComun.Len.LQU_IMPB_ADDIZ_COMUN;
        lquAddizRegion.setLquAddizRegionFromBuffer(buffer, position);
        position += LquAddizRegion.Len.LQU_ADDIZ_REGION;
        lquImpbAddizRegion.setLquImpbAddizRegionFromBuffer(buffer, position);
        position += LquImpbAddizRegion.Len.LQU_IMPB_ADDIZ_REGION;
        lquMontDal2007.setLquMontDal2007FromBuffer(buffer, position);
        position += LquMontDal2007.Len.LQU_MONT_DAL2007;
        lquImpbCnbtInpstfm.setLquImpbCnbtInpstfmFromBuffer(buffer, position);
        position += LquImpbCnbtInpstfm.Len.LQU_IMPB_CNBT_INPSTFM;
        lquImpLrdDaRimb.setLquImpLrdDaRimbFromBuffer(buffer, position);
        position += LquImpLrdDaRimb.Len.LQU_IMP_LRD_DA_RIMB;
        lquImpDirDaRimb.setLquImpDirDaRimbFromBuffer(buffer, position);
        position += LquImpDirDaRimb.Len.LQU_IMP_DIR_DA_RIMB;
        lquRisMat.setLquRisMatFromBuffer(buffer, position);
        position += LquRisMat.Len.LQU_RIS_MAT;
        lquRisSpe.setLquRisSpeFromBuffer(buffer, position);
        position += LquRisSpe.Len.LQU_RIS_SPE;
        lquDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.LQU_DS_RIGA, 0);
        position += Len.LQU_DS_RIGA;
        lquDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        lquDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LQU_DS_VER, 0);
        position += Len.LQU_DS_VER;
        lquDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.LQU_DS_TS_INI_CPTZ, 0);
        position += Len.LQU_DS_TS_INI_CPTZ;
        lquDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.LQU_DS_TS_END_CPTZ, 0);
        position += Len.LQU_DS_TS_END_CPTZ;
        lquDsUtente = MarshalByte.readString(buffer, position, Len.LQU_DS_UTENTE);
        position += Len.LQU_DS_UTENTE;
        lquDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        lquTotIasPnl.setLquTotIasPnlFromBuffer(buffer, position);
        position += LquTotIasPnl.Len.LQU_TOT_IAS_PNL;
        lquFlEveGarto = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        lquImpRenK1.setLquImpRenK1FromBuffer(buffer, position);
        position += LquImpRenK1.Len.LQU_IMP_REN_K1;
        lquImpRenK2.setLquImpRenK2FromBuffer(buffer, position);
        position += LquImpRenK2.Len.LQU_IMP_REN_K2;
        lquImpRenK3.setLquImpRenK3FromBuffer(buffer, position);
        position += LquImpRenK3.Len.LQU_IMP_REN_K3;
        lquPcRenK1.setLquPcRenK1FromBuffer(buffer, position);
        position += LquPcRenK1.Len.LQU_PC_REN_K1;
        lquPcRenK2.setLquPcRenK2FromBuffer(buffer, position);
        position += LquPcRenK2.Len.LQU_PC_REN_K2;
        lquPcRenK3.setLquPcRenK3FromBuffer(buffer, position);
        position += LquPcRenK3.Len.LQU_PC_REN_K3;
        lquTpCausAntic = MarshalByte.readString(buffer, position, Len.LQU_TP_CAUS_ANTIC);
        position += Len.LQU_TP_CAUS_ANTIC;
        lquImpLrdLiqtoRilt.setLquImpLrdLiqtoRiltFromBuffer(buffer, position);
        position += LquImpLrdLiqtoRilt.Len.LQU_IMP_LRD_LIQTO_RILT;
        lquImpstApplRilt.setLquImpstApplRiltFromBuffer(buffer, position);
        position += LquImpstApplRilt.Len.LQU_IMPST_APPL_RILT;
        lquPcRiscParz.setLquPcRiscParzFromBuffer(buffer, position);
        position += LquPcRiscParz.Len.LQU_PC_RISC_PARZ;
        lquImpstBolloTotV.setLquImpstBolloTotVFromBuffer(buffer, position);
        position += LquImpstBolloTotV.Len.LQU_IMPST_BOLLO_TOT_V;
        lquImpstBolloDettC.setLquImpstBolloDettCFromBuffer(buffer, position);
        position += LquImpstBolloDettC.Len.LQU_IMPST_BOLLO_DETT_C;
        lquImpstBolloTotSw.setLquImpstBolloTotSwFromBuffer(buffer, position);
        position += LquImpstBolloTotSw.Len.LQU_IMPST_BOLLO_TOT_SW;
        lquImpstBolloTotAa.setLquImpstBolloTotAaFromBuffer(buffer, position);
        position += LquImpstBolloTotAa.Len.LQU_IMPST_BOLLO_TOT_AA;
        lquImpbVis1382011.setLquImpbVis1382011FromBuffer(buffer, position);
        position += LquImpbVis1382011.Len.LQU_IMPB_VIS1382011;
        lquImpstVis1382011.setLquImpstVis1382011FromBuffer(buffer, position);
        position += LquImpstVis1382011.Len.LQU_IMPST_VIS1382011;
        lquImpbIs1382011.setLquImpbIs1382011FromBuffer(buffer, position);
        position += LquImpbIs1382011.Len.LQU_IMPB_IS1382011;
        lquImpstSost1382011.setLquImpstSost1382011FromBuffer(buffer, position);
        position += LquImpstSost1382011.Len.LQU_IMPST_SOST1382011;
        lquPcAbbTitStat.setLquPcAbbTitStatFromBuffer(buffer, position);
        position += LquPcAbbTitStat.Len.LQU_PC_ABB_TIT_STAT;
        lquImpbBolloDettC.setLquImpbBolloDettCFromBuffer(buffer, position);
        position += LquImpbBolloDettC.Len.LQU_IMPB_BOLLO_DETT_C;
        lquFlPreComp = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        lquImpbVis662014.setLquImpbVis662014FromBuffer(buffer, position);
        position += LquImpbVis662014.Len.LQU_IMPB_VIS662014;
        lquImpstVis662014.setLquImpstVis662014FromBuffer(buffer, position);
        position += LquImpstVis662014.Len.LQU_IMPST_VIS662014;
        lquImpbIs662014.setLquImpbIs662014FromBuffer(buffer, position);
        position += LquImpbIs662014.Len.LQU_IMPB_IS662014;
        lquImpstSost662014.setLquImpstSost662014FromBuffer(buffer, position);
        position += LquImpstSost662014.Len.LQU_IMPST_SOST662014;
        lquPcAbbTs662014.setLquPcAbbTs662014FromBuffer(buffer, position);
        position += LquPcAbbTs662014.Len.LQU_PC_ABB_TS662014;
        lquImpLrdCalcCp.setLquImpLrdCalcCpFromBuffer(buffer, position);
        position += LquImpLrdCalcCp.Len.LQU_IMP_LRD_CALC_CP;
        lquCosTunnelUscita.setLquCosTunnelUscitaFromBuffer(buffer, position);
    }

    public byte[] getLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, lquIdLiq, Len.Int.LQU_ID_LIQ, 0);
        position += Len.LQU_ID_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, lquIdOgg, Len.Int.LQU_ID_OGG, 0);
        position += Len.LQU_ID_OGG;
        MarshalByte.writeString(buffer, position, lquTpOgg, Len.LQU_TP_OGG);
        position += Len.LQU_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, lquIdMoviCrz, Len.Int.LQU_ID_MOVI_CRZ, 0);
        position += Len.LQU_ID_MOVI_CRZ;
        lquIdMoviChiu.getLquIdMoviChiuAsBuffer(buffer, position);
        position += LquIdMoviChiu.Len.LQU_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, lquDtIniEff, Len.Int.LQU_DT_INI_EFF, 0);
        position += Len.LQU_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, lquDtEndEff, Len.Int.LQU_DT_END_EFF, 0);
        position += Len.LQU_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, lquCodCompAnia, Len.Int.LQU_COD_COMP_ANIA, 0);
        position += Len.LQU_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, lquIbOgg, Len.LQU_IB_OGG);
        position += Len.LQU_IB_OGG;
        MarshalByte.writeString(buffer, position, lquTpLiq, Len.LQU_TP_LIQ);
        position += Len.LQU_TP_LIQ;
        getLquDescCauEveSinVcharBytes(buffer, position);
        position += Len.LQU_DESC_CAU_EVE_SIN_VCHAR;
        MarshalByte.writeString(buffer, position, lquCodCauSin, Len.LQU_COD_CAU_SIN);
        position += Len.LQU_COD_CAU_SIN;
        MarshalByte.writeString(buffer, position, lquCodSinCatstrf, Len.LQU_COD_SIN_CATSTRF);
        position += Len.LQU_COD_SIN_CATSTRF;
        lquDtMor.getLquDtMorAsBuffer(buffer, position);
        position += LquDtMor.Len.LQU_DT_MOR;
        lquDtDen.getLquDtDenAsBuffer(buffer, position);
        position += LquDtDen.Len.LQU_DT_DEN;
        lquDtPervDen.getLquDtPervDenAsBuffer(buffer, position);
        position += LquDtPervDen.Len.LQU_DT_PERV_DEN;
        lquDtRich.getLquDtRichAsBuffer(buffer, position);
        position += LquDtRich.Len.LQU_DT_RICH;
        MarshalByte.writeString(buffer, position, lquTpSin, Len.LQU_TP_SIN);
        position += Len.LQU_TP_SIN;
        MarshalByte.writeString(buffer, position, lquTpRisc, Len.LQU_TP_RISC);
        position += Len.LQU_TP_RISC;
        lquTpMetRisc.getLquTpMetRiscAsBuffer(buffer, position);
        position += LquTpMetRisc.Len.LQU_TP_MET_RISC;
        lquDtLiq.getLquDtLiqAsBuffer(buffer, position);
        position += LquDtLiq.Len.LQU_DT_LIQ;
        MarshalByte.writeString(buffer, position, lquCodDvs, Len.LQU_COD_DVS);
        position += Len.LQU_COD_DVS;
        lquTotImpLrdLiqto.getLquTotImpLrdLiqtoAsBuffer(buffer, position);
        position += LquTotImpLrdLiqto.Len.LQU_TOT_IMP_LRD_LIQTO;
        lquTotImpPrest.getLquTotImpPrestAsBuffer(buffer, position);
        position += LquTotImpPrest.Len.LQU_TOT_IMP_PREST;
        lquTotImpIntrPrest.getLquTotImpIntrPrestAsBuffer(buffer, position);
        position += LquTotImpIntrPrest.Len.LQU_TOT_IMP_INTR_PREST;
        lquTotImpUti.getLquTotImpUtiAsBuffer(buffer, position);
        position += LquTotImpUti.Len.LQU_TOT_IMP_UTI;
        lquTotImpRitTfr.getLquTotImpRitTfrAsBuffer(buffer, position);
        position += LquTotImpRitTfr.Len.LQU_TOT_IMP_RIT_TFR;
        lquTotImpRitAcc.getLquTotImpRitAccAsBuffer(buffer, position);
        position += LquTotImpRitAcc.Len.LQU_TOT_IMP_RIT_ACC;
        lquTotImpRitVis.getLquTotImpRitVisAsBuffer(buffer, position);
        position += LquTotImpRitVis.Len.LQU_TOT_IMP_RIT_VIS;
        lquTotImpbTfr.getLquTotImpbTfrAsBuffer(buffer, position);
        position += LquTotImpbTfr.Len.LQU_TOT_IMPB_TFR;
        lquTotImpbAcc.getLquTotImpbAccAsBuffer(buffer, position);
        position += LquTotImpbAcc.Len.LQU_TOT_IMPB_ACC;
        lquTotImpbVis.getLquTotImpbVisAsBuffer(buffer, position);
        position += LquTotImpbVis.Len.LQU_TOT_IMPB_VIS;
        lquTotImpRimb.getLquTotImpRimbAsBuffer(buffer, position);
        position += LquTotImpRimb.Len.LQU_TOT_IMP_RIMB;
        lquImpbImpstPrvr.getLquImpbImpstPrvrAsBuffer(buffer, position);
        position += LquImpbImpstPrvr.Len.LQU_IMPB_IMPST_PRVR;
        lquImpstPrvr.getLquImpstPrvrAsBuffer(buffer, position);
        position += LquImpstPrvr.Len.LQU_IMPST_PRVR;
        lquImpbImpst252.getLquImpbImpst252AsBuffer(buffer, position);
        position += LquImpbImpst252.Len.LQU_IMPB_IMPST252;
        lquImpst252.getLquImpst252AsBuffer(buffer, position);
        position += LquImpst252.Len.LQU_IMPST252;
        lquTotImpIs.getLquTotImpIsAsBuffer(buffer, position);
        position += LquTotImpIs.Len.LQU_TOT_IMP_IS;
        lquImpDirLiq.getLquImpDirLiqAsBuffer(buffer, position);
        position += LquImpDirLiq.Len.LQU_IMP_DIR_LIQ;
        lquTotImpNetLiqto.getLquTotImpNetLiqtoAsBuffer(buffer, position);
        position += LquTotImpNetLiqto.Len.LQU_TOT_IMP_NET_LIQTO;
        lquMontEnd2000.getLquMontEnd2000AsBuffer(buffer, position);
        position += LquMontEnd2000.Len.LQU_MONT_END2000;
        lquMontEnd2006.getLquMontEnd2006AsBuffer(buffer, position);
        position += LquMontEnd2006.Len.LQU_MONT_END2006;
        lquPcRen.getLquPcRenAsBuffer(buffer, position);
        position += LquPcRen.Len.LQU_PC_REN;
        lquImpPnl.getLquImpPnlAsBuffer(buffer, position);
        position += LquImpPnl.Len.LQU_IMP_PNL;
        lquImpbIrpef.getLquImpbIrpefAsBuffer(buffer, position);
        position += LquImpbIrpef.Len.LQU_IMPB_IRPEF;
        lquImpstIrpef.getLquImpstIrpefAsBuffer(buffer, position);
        position += LquImpstIrpef.Len.LQU_IMPST_IRPEF;
        lquDtVlt.getLquDtVltAsBuffer(buffer, position);
        position += LquDtVlt.Len.LQU_DT_VLT;
        lquDtEndIstr.getLquDtEndIstrAsBuffer(buffer, position);
        position += LquDtEndIstr.Len.LQU_DT_END_ISTR;
        MarshalByte.writeString(buffer, position, lquTpRimb, Len.LQU_TP_RIMB);
        position += Len.LQU_TP_RIMB;
        lquSpeRcs.getLquSpeRcsAsBuffer(buffer, position);
        position += LquSpeRcs.Len.LQU_SPE_RCS;
        MarshalByte.writeString(buffer, position, lquIbLiq, Len.LQU_IB_LIQ);
        position += Len.LQU_IB_LIQ;
        lquTotIasOnerPrvnt.getLquTotIasOnerPrvntAsBuffer(buffer, position);
        position += LquTotIasOnerPrvnt.Len.LQU_TOT_IAS_ONER_PRVNT;
        lquTotIasMggSin.getLquTotIasMggSinAsBuffer(buffer, position);
        position += LquTotIasMggSin.Len.LQU_TOT_IAS_MGG_SIN;
        lquTotIasRstDpst.getLquTotIasRstDpstAsBuffer(buffer, position);
        position += LquTotIasRstDpst.Len.LQU_TOT_IAS_RST_DPST;
        lquImpOnerLiq.getLquImpOnerLiqAsBuffer(buffer, position);
        position += LquImpOnerLiq.Len.LQU_IMP_ONER_LIQ;
        lquComponTaxRimb.getLquComponTaxRimbAsBuffer(buffer, position);
        position += LquComponTaxRimb.Len.LQU_COMPON_TAX_RIMB;
        MarshalByte.writeString(buffer, position, lquTpMezPag, Len.LQU_TP_MEZ_PAG);
        position += Len.LQU_TP_MEZ_PAG;
        lquImpExcontr.getLquImpExcontrAsBuffer(buffer, position);
        position += LquImpExcontr.Len.LQU_IMP_EXCONTR;
        lquImpIntrRitPag.getLquImpIntrRitPagAsBuffer(buffer, position);
        position += LquImpIntrRitPag.Len.LQU_IMP_INTR_RIT_PAG;
        lquBnsNonGoduto.getLquBnsNonGodutoAsBuffer(buffer, position);
        position += LquBnsNonGoduto.Len.LQU_BNS_NON_GODUTO;
        lquCnbtInpstfm.getLquCnbtInpstfmAsBuffer(buffer, position);
        position += LquCnbtInpstfm.Len.LQU_CNBT_INPSTFM;
        lquImpstDaRimb.getLquImpstDaRimbAsBuffer(buffer, position);
        position += LquImpstDaRimb.Len.LQU_IMPST_DA_RIMB;
        lquImpbIs.getLquImpbIsAsBuffer(buffer, position);
        position += LquImpbIs.Len.LQU_IMPB_IS;
        lquTaxSep.getLquTaxSepAsBuffer(buffer, position);
        position += LquTaxSep.Len.LQU_TAX_SEP;
        lquImpbTaxSep.getLquImpbTaxSepAsBuffer(buffer, position);
        position += LquImpbTaxSep.Len.LQU_IMPB_TAX_SEP;
        lquImpbIntrSuPrest.getLquImpbIntrSuPrestAsBuffer(buffer, position);
        position += LquImpbIntrSuPrest.Len.LQU_IMPB_INTR_SU_PREST;
        lquAddizComun.getLquAddizComunAsBuffer(buffer, position);
        position += LquAddizComun.Len.LQU_ADDIZ_COMUN;
        lquImpbAddizComun.getLquImpbAddizComunAsBuffer(buffer, position);
        position += LquImpbAddizComun.Len.LQU_IMPB_ADDIZ_COMUN;
        lquAddizRegion.getLquAddizRegionAsBuffer(buffer, position);
        position += LquAddizRegion.Len.LQU_ADDIZ_REGION;
        lquImpbAddizRegion.getLquImpbAddizRegionAsBuffer(buffer, position);
        position += LquImpbAddizRegion.Len.LQU_IMPB_ADDIZ_REGION;
        lquMontDal2007.getLquMontDal2007AsBuffer(buffer, position);
        position += LquMontDal2007.Len.LQU_MONT_DAL2007;
        lquImpbCnbtInpstfm.getLquImpbCnbtInpstfmAsBuffer(buffer, position);
        position += LquImpbCnbtInpstfm.Len.LQU_IMPB_CNBT_INPSTFM;
        lquImpLrdDaRimb.getLquImpLrdDaRimbAsBuffer(buffer, position);
        position += LquImpLrdDaRimb.Len.LQU_IMP_LRD_DA_RIMB;
        lquImpDirDaRimb.getLquImpDirDaRimbAsBuffer(buffer, position);
        position += LquImpDirDaRimb.Len.LQU_IMP_DIR_DA_RIMB;
        lquRisMat.getLquRisMatAsBuffer(buffer, position);
        position += LquRisMat.Len.LQU_RIS_MAT;
        lquRisSpe.getLquRisSpeAsBuffer(buffer, position);
        position += LquRisSpe.Len.LQU_RIS_SPE;
        MarshalByte.writeLongAsPacked(buffer, position, lquDsRiga, Len.Int.LQU_DS_RIGA, 0);
        position += Len.LQU_DS_RIGA;
        MarshalByte.writeChar(buffer, position, lquDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lquDsVer, Len.Int.LQU_DS_VER, 0);
        position += Len.LQU_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, lquDsTsIniCptz, Len.Int.LQU_DS_TS_INI_CPTZ, 0);
        position += Len.LQU_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, lquDsTsEndCptz, Len.Int.LQU_DS_TS_END_CPTZ, 0);
        position += Len.LQU_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, lquDsUtente, Len.LQU_DS_UTENTE);
        position += Len.LQU_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, lquDsStatoElab);
        position += Types.CHAR_SIZE;
        lquTotIasPnl.getLquTotIasPnlAsBuffer(buffer, position);
        position += LquTotIasPnl.Len.LQU_TOT_IAS_PNL;
        MarshalByte.writeChar(buffer, position, lquFlEveGarto);
        position += Types.CHAR_SIZE;
        lquImpRenK1.getLquImpRenK1AsBuffer(buffer, position);
        position += LquImpRenK1.Len.LQU_IMP_REN_K1;
        lquImpRenK2.getLquImpRenK2AsBuffer(buffer, position);
        position += LquImpRenK2.Len.LQU_IMP_REN_K2;
        lquImpRenK3.getLquImpRenK3AsBuffer(buffer, position);
        position += LquImpRenK3.Len.LQU_IMP_REN_K3;
        lquPcRenK1.getLquPcRenK1AsBuffer(buffer, position);
        position += LquPcRenK1.Len.LQU_PC_REN_K1;
        lquPcRenK2.getLquPcRenK2AsBuffer(buffer, position);
        position += LquPcRenK2.Len.LQU_PC_REN_K2;
        lquPcRenK3.getLquPcRenK3AsBuffer(buffer, position);
        position += LquPcRenK3.Len.LQU_PC_REN_K3;
        MarshalByte.writeString(buffer, position, lquTpCausAntic, Len.LQU_TP_CAUS_ANTIC);
        position += Len.LQU_TP_CAUS_ANTIC;
        lquImpLrdLiqtoRilt.getLquImpLrdLiqtoRiltAsBuffer(buffer, position);
        position += LquImpLrdLiqtoRilt.Len.LQU_IMP_LRD_LIQTO_RILT;
        lquImpstApplRilt.getLquImpstApplRiltAsBuffer(buffer, position);
        position += LquImpstApplRilt.Len.LQU_IMPST_APPL_RILT;
        lquPcRiscParz.getLquPcRiscParzAsBuffer(buffer, position);
        position += LquPcRiscParz.Len.LQU_PC_RISC_PARZ;
        lquImpstBolloTotV.getLquImpstBolloTotVAsBuffer(buffer, position);
        position += LquImpstBolloTotV.Len.LQU_IMPST_BOLLO_TOT_V;
        lquImpstBolloDettC.getLquImpstBolloDettCAsBuffer(buffer, position);
        position += LquImpstBolloDettC.Len.LQU_IMPST_BOLLO_DETT_C;
        lquImpstBolloTotSw.getLquImpstBolloTotSwAsBuffer(buffer, position);
        position += LquImpstBolloTotSw.Len.LQU_IMPST_BOLLO_TOT_SW;
        lquImpstBolloTotAa.getLquImpstBolloTotAaAsBuffer(buffer, position);
        position += LquImpstBolloTotAa.Len.LQU_IMPST_BOLLO_TOT_AA;
        lquImpbVis1382011.getLquImpbVis1382011AsBuffer(buffer, position);
        position += LquImpbVis1382011.Len.LQU_IMPB_VIS1382011;
        lquImpstVis1382011.getLquImpstVis1382011AsBuffer(buffer, position);
        position += LquImpstVis1382011.Len.LQU_IMPST_VIS1382011;
        lquImpbIs1382011.getLquImpbIs1382011AsBuffer(buffer, position);
        position += LquImpbIs1382011.Len.LQU_IMPB_IS1382011;
        lquImpstSost1382011.getLquImpstSost1382011AsBuffer(buffer, position);
        position += LquImpstSost1382011.Len.LQU_IMPST_SOST1382011;
        lquPcAbbTitStat.getLquPcAbbTitStatAsBuffer(buffer, position);
        position += LquPcAbbTitStat.Len.LQU_PC_ABB_TIT_STAT;
        lquImpbBolloDettC.getLquImpbBolloDettCAsBuffer(buffer, position);
        position += LquImpbBolloDettC.Len.LQU_IMPB_BOLLO_DETT_C;
        MarshalByte.writeChar(buffer, position, lquFlPreComp);
        position += Types.CHAR_SIZE;
        lquImpbVis662014.getLquImpbVis662014AsBuffer(buffer, position);
        position += LquImpbVis662014.Len.LQU_IMPB_VIS662014;
        lquImpstVis662014.getLquImpstVis662014AsBuffer(buffer, position);
        position += LquImpstVis662014.Len.LQU_IMPST_VIS662014;
        lquImpbIs662014.getLquImpbIs662014AsBuffer(buffer, position);
        position += LquImpbIs662014.Len.LQU_IMPB_IS662014;
        lquImpstSost662014.getLquImpstSost662014AsBuffer(buffer, position);
        position += LquImpstSost662014.Len.LQU_IMPST_SOST662014;
        lquPcAbbTs662014.getLquPcAbbTs662014AsBuffer(buffer, position);
        position += LquPcAbbTs662014.Len.LQU_PC_ABB_TS662014;
        lquImpLrdCalcCp.getLquImpLrdCalcCpAsBuffer(buffer, position);
        position += LquImpLrdCalcCp.Len.LQU_IMP_LRD_CALC_CP;
        lquCosTunnelUscita.getLquCosTunnelUscitaAsBuffer(buffer, position);
        return buffer;
    }

    public void setLquIdLiq(int lquIdLiq) {
        this.lquIdLiq = lquIdLiq;
    }

    public int getLquIdLiq() {
        return this.lquIdLiq;
    }

    public void setLquIdOgg(int lquIdOgg) {
        this.lquIdOgg = lquIdOgg;
    }

    public int getLquIdOgg() {
        return this.lquIdOgg;
    }

    public void setLquTpOgg(String lquTpOgg) {
        this.lquTpOgg = Functions.subString(lquTpOgg, Len.LQU_TP_OGG);
    }

    public String getLquTpOgg() {
        return this.lquTpOgg;
    }

    public void setLquIdMoviCrz(int lquIdMoviCrz) {
        this.lquIdMoviCrz = lquIdMoviCrz;
    }

    public int getLquIdMoviCrz() {
        return this.lquIdMoviCrz;
    }

    public void setLquDtIniEff(int lquDtIniEff) {
        this.lquDtIniEff = lquDtIniEff;
    }

    public int getLquDtIniEff() {
        return this.lquDtIniEff;
    }

    public void setLquDtEndEff(int lquDtEndEff) {
        this.lquDtEndEff = lquDtEndEff;
    }

    public int getLquDtEndEff() {
        return this.lquDtEndEff;
    }

    public void setLquCodCompAnia(int lquCodCompAnia) {
        this.lquCodCompAnia = lquCodCompAnia;
    }

    public int getLquCodCompAnia() {
        return this.lquCodCompAnia;
    }

    public void setLquIbOgg(String lquIbOgg) {
        this.lquIbOgg = Functions.subString(lquIbOgg, Len.LQU_IB_OGG);
    }

    public String getLquIbOgg() {
        return this.lquIbOgg;
    }

    public void setLquTpLiq(String lquTpLiq) {
        this.lquTpLiq = Functions.subString(lquTpLiq, Len.LQU_TP_LIQ);
    }

    public String getLquTpLiq() {
        return this.lquTpLiq;
    }

    public void setLquDescCauEveSinVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        lquDescCauEveSinLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        lquDescCauEveSin = MarshalByte.readString(buffer, position, Len.LQU_DESC_CAU_EVE_SIN);
    }

    public byte[] getLquDescCauEveSinVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, lquDescCauEveSinLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, lquDescCauEveSin, Len.LQU_DESC_CAU_EVE_SIN);
        return buffer;
    }

    public void setLquDescCauEveSinLen(short lquDescCauEveSinLen) {
        this.lquDescCauEveSinLen = lquDescCauEveSinLen;
    }

    public short getLquDescCauEveSinLen() {
        return this.lquDescCauEveSinLen;
    }

    public void setLquDescCauEveSin(String lquDescCauEveSin) {
        this.lquDescCauEveSin = Functions.subString(lquDescCauEveSin, Len.LQU_DESC_CAU_EVE_SIN);
    }

    public String getLquDescCauEveSin() {
        return this.lquDescCauEveSin;
    }

    public void setLquCodCauSin(String lquCodCauSin) {
        this.lquCodCauSin = Functions.subString(lquCodCauSin, Len.LQU_COD_CAU_SIN);
    }

    public String getLquCodCauSin() {
        return this.lquCodCauSin;
    }

    public String getLquCodCauSinFormatted() {
        return Functions.padBlanks(getLquCodCauSin(), Len.LQU_COD_CAU_SIN);
    }

    public void setLquCodSinCatstrf(String lquCodSinCatstrf) {
        this.lquCodSinCatstrf = Functions.subString(lquCodSinCatstrf, Len.LQU_COD_SIN_CATSTRF);
    }

    public String getLquCodSinCatstrf() {
        return this.lquCodSinCatstrf;
    }

    public String getLquCodSinCatstrfFormatted() {
        return Functions.padBlanks(getLquCodSinCatstrf(), Len.LQU_COD_SIN_CATSTRF);
    }

    public void setLquTpSin(String lquTpSin) {
        this.lquTpSin = Functions.subString(lquTpSin, Len.LQU_TP_SIN);
    }

    public String getLquTpSin() {
        return this.lquTpSin;
    }

    public String getLquTpSinFormatted() {
        return Functions.padBlanks(getLquTpSin(), Len.LQU_TP_SIN);
    }

    public void setLquTpRisc(String lquTpRisc) {
        this.lquTpRisc = Functions.subString(lquTpRisc, Len.LQU_TP_RISC);
    }

    public String getLquTpRisc() {
        return this.lquTpRisc;
    }

    public String getLquTpRiscFormatted() {
        return Functions.padBlanks(getLquTpRisc(), Len.LQU_TP_RISC);
    }

    public void setLquCodDvs(String lquCodDvs) {
        this.lquCodDvs = Functions.subString(lquCodDvs, Len.LQU_COD_DVS);
    }

    public String getLquCodDvs() {
        return this.lquCodDvs;
    }

    public String getLquCodDvsFormatted() {
        return Functions.padBlanks(getLquCodDvs(), Len.LQU_COD_DVS);
    }

    public void setLquTpRimb(String lquTpRimb) {
        this.lquTpRimb = Functions.subString(lquTpRimb, Len.LQU_TP_RIMB);
    }

    public String getLquTpRimb() {
        return this.lquTpRimb;
    }

    public void setLquIbLiq(String lquIbLiq) {
        this.lquIbLiq = Functions.subString(lquIbLiq, Len.LQU_IB_LIQ);
    }

    public String getLquIbLiq() {
        return this.lquIbLiq;
    }

    public void setLquTpMezPag(String lquTpMezPag) {
        this.lquTpMezPag = Functions.subString(lquTpMezPag, Len.LQU_TP_MEZ_PAG);
    }

    public String getLquTpMezPag() {
        return this.lquTpMezPag;
    }

    public String getLquTpMezPagFormatted() {
        return Functions.padBlanks(getLquTpMezPag(), Len.LQU_TP_MEZ_PAG);
    }

    public void setLquDsRiga(long lquDsRiga) {
        this.lquDsRiga = lquDsRiga;
    }

    public long getLquDsRiga() {
        return this.lquDsRiga;
    }

    public void setLquDsOperSql(char lquDsOperSql) {
        this.lquDsOperSql = lquDsOperSql;
    }

    public char getLquDsOperSql() {
        return this.lquDsOperSql;
    }

    public void setLquDsVer(int lquDsVer) {
        this.lquDsVer = lquDsVer;
    }

    public int getLquDsVer() {
        return this.lquDsVer;
    }

    public void setLquDsTsIniCptz(long lquDsTsIniCptz) {
        this.lquDsTsIniCptz = lquDsTsIniCptz;
    }

    public long getLquDsTsIniCptz() {
        return this.lquDsTsIniCptz;
    }

    public void setLquDsTsEndCptz(long lquDsTsEndCptz) {
        this.lquDsTsEndCptz = lquDsTsEndCptz;
    }

    public long getLquDsTsEndCptz() {
        return this.lquDsTsEndCptz;
    }

    public void setLquDsUtente(String lquDsUtente) {
        this.lquDsUtente = Functions.subString(lquDsUtente, Len.LQU_DS_UTENTE);
    }

    public String getLquDsUtente() {
        return this.lquDsUtente;
    }

    public void setLquDsStatoElab(char lquDsStatoElab) {
        this.lquDsStatoElab = lquDsStatoElab;
    }

    public char getLquDsStatoElab() {
        return this.lquDsStatoElab;
    }

    public void setLquFlEveGarto(char lquFlEveGarto) {
        this.lquFlEveGarto = lquFlEveGarto;
    }

    public char getLquFlEveGarto() {
        return this.lquFlEveGarto;
    }

    public void setLquTpCausAntic(String lquTpCausAntic) {
        this.lquTpCausAntic = Functions.subString(lquTpCausAntic, Len.LQU_TP_CAUS_ANTIC);
    }

    public String getLquTpCausAntic() {
        return this.lquTpCausAntic;
    }

    public String getLquTpCausAnticFormatted() {
        return Functions.padBlanks(getLquTpCausAntic(), Len.LQU_TP_CAUS_ANTIC);
    }

    public void setLquFlPreComp(char lquFlPreComp) {
        this.lquFlPreComp = lquFlPreComp;
    }

    public char getLquFlPreComp() {
        return this.lquFlPreComp;
    }

    public LquAddizComun getLquAddizComun() {
        return lquAddizComun;
    }

    public LquAddizRegion getLquAddizRegion() {
        return lquAddizRegion;
    }

    public LquBnsNonGoduto getLquBnsNonGoduto() {
        return lquBnsNonGoduto;
    }

    public LquCnbtInpstfm getLquCnbtInpstfm() {
        return lquCnbtInpstfm;
    }

    public LquComponTaxRimb getLquComponTaxRimb() {
        return lquComponTaxRimb;
    }

    public LquCosTunnelUscita getLquCosTunnelUscita() {
        return lquCosTunnelUscita;
    }

    public LquDtDen getLquDtDen() {
        return lquDtDen;
    }

    public LquDtEndIstr getLquDtEndIstr() {
        return lquDtEndIstr;
    }

    public LquDtLiq getLquDtLiq() {
        return lquDtLiq;
    }

    public LquDtMor getLquDtMor() {
        return lquDtMor;
    }

    public LquDtPervDen getLquDtPervDen() {
        return lquDtPervDen;
    }

    public LquDtRich getLquDtRich() {
        return lquDtRich;
    }

    public LquDtVlt getLquDtVlt() {
        return lquDtVlt;
    }

    public LquIdMoviChiu getLquIdMoviChiu() {
        return lquIdMoviChiu;
    }

    public LquImpDirDaRimb getLquImpDirDaRimb() {
        return lquImpDirDaRimb;
    }

    public LquImpDirLiq getLquImpDirLiq() {
        return lquImpDirLiq;
    }

    public LquImpExcontr getLquImpExcontr() {
        return lquImpExcontr;
    }

    public LquImpIntrRitPag getLquImpIntrRitPag() {
        return lquImpIntrRitPag;
    }

    public LquImpLrdCalcCp getLquImpLrdCalcCp() {
        return lquImpLrdCalcCp;
    }

    public LquImpLrdDaRimb getLquImpLrdDaRimb() {
        return lquImpLrdDaRimb;
    }

    public LquImpLrdLiqtoRilt getLquImpLrdLiqtoRilt() {
        return lquImpLrdLiqtoRilt;
    }

    public LquImpOnerLiq getLquImpOnerLiq() {
        return lquImpOnerLiq;
    }

    public LquImpPnl getLquImpPnl() {
        return lquImpPnl;
    }

    public LquImpRenK1 getLquImpRenK1() {
        return lquImpRenK1;
    }

    public LquImpRenK2 getLquImpRenK2() {
        return lquImpRenK2;
    }

    public LquImpRenK3 getLquImpRenK3() {
        return lquImpRenK3;
    }

    public LquImpbAddizComun getLquImpbAddizComun() {
        return lquImpbAddizComun;
    }

    public LquImpbAddizRegion getLquImpbAddizRegion() {
        return lquImpbAddizRegion;
    }

    public LquImpbBolloDettC getLquImpbBolloDettC() {
        return lquImpbBolloDettC;
    }

    public LquImpbCnbtInpstfm getLquImpbCnbtInpstfm() {
        return lquImpbCnbtInpstfm;
    }

    public LquImpbImpst252 getLquImpbImpst252() {
        return lquImpbImpst252;
    }

    public LquImpbImpstPrvr getLquImpbImpstPrvr() {
        return lquImpbImpstPrvr;
    }

    public LquImpbIntrSuPrest getLquImpbIntrSuPrest() {
        return lquImpbIntrSuPrest;
    }

    public LquImpbIrpef getLquImpbIrpef() {
        return lquImpbIrpef;
    }

    public LquImpbIs1382011 getLquImpbIs1382011() {
        return lquImpbIs1382011;
    }

    public LquImpbIs662014 getLquImpbIs662014() {
        return lquImpbIs662014;
    }

    public LquImpbIs getLquImpbIs() {
        return lquImpbIs;
    }

    public LquImpbTaxSep getLquImpbTaxSep() {
        return lquImpbTaxSep;
    }

    public LquImpbVis1382011 getLquImpbVis1382011() {
        return lquImpbVis1382011;
    }

    public LquImpbVis662014 getLquImpbVis662014() {
        return lquImpbVis662014;
    }

    public LquImpst252 getLquImpst252() {
        return lquImpst252;
    }

    public LquImpstApplRilt getLquImpstApplRilt() {
        return lquImpstApplRilt;
    }

    public LquImpstBolloDettC getLquImpstBolloDettC() {
        return lquImpstBolloDettC;
    }

    public LquImpstBolloTotAa getLquImpstBolloTotAa() {
        return lquImpstBolloTotAa;
    }

    public LquImpstBolloTotSw getLquImpstBolloTotSw() {
        return lquImpstBolloTotSw;
    }

    public LquImpstBolloTotV getLquImpstBolloTotV() {
        return lquImpstBolloTotV;
    }

    public LquImpstDaRimb getLquImpstDaRimb() {
        return lquImpstDaRimb;
    }

    public LquImpstIrpef getLquImpstIrpef() {
        return lquImpstIrpef;
    }

    public LquImpstPrvr getLquImpstPrvr() {
        return lquImpstPrvr;
    }

    public LquImpstSost1382011 getLquImpstSost1382011() {
        return lquImpstSost1382011;
    }

    public LquImpstSost662014 getLquImpstSost662014() {
        return lquImpstSost662014;
    }

    public LquImpstVis1382011 getLquImpstVis1382011() {
        return lquImpstVis1382011;
    }

    public LquImpstVis662014 getLquImpstVis662014() {
        return lquImpstVis662014;
    }

    public LquMontDal2007 getLquMontDal2007() {
        return lquMontDal2007;
    }

    public LquMontEnd2000 getLquMontEnd2000() {
        return lquMontEnd2000;
    }

    public LquMontEnd2006 getLquMontEnd2006() {
        return lquMontEnd2006;
    }

    public LquPcAbbTitStat getLquPcAbbTitStat() {
        return lquPcAbbTitStat;
    }

    public LquPcAbbTs662014 getLquPcAbbTs662014() {
        return lquPcAbbTs662014;
    }

    public LquPcRen getLquPcRen() {
        return lquPcRen;
    }

    public LquPcRenK1 getLquPcRenK1() {
        return lquPcRenK1;
    }

    public LquPcRenK2 getLquPcRenK2() {
        return lquPcRenK2;
    }

    public LquPcRenK3 getLquPcRenK3() {
        return lquPcRenK3;
    }

    public LquPcRiscParz getLquPcRiscParz() {
        return lquPcRiscParz;
    }

    public LquRisMat getLquRisMat() {
        return lquRisMat;
    }

    public LquRisSpe getLquRisSpe() {
        return lquRisSpe;
    }

    public LquSpeRcs getLquSpeRcs() {
        return lquSpeRcs;
    }

    public LquTaxSep getLquTaxSep() {
        return lquTaxSep;
    }

    public LquTotIasMggSin getLquTotIasMggSin() {
        return lquTotIasMggSin;
    }

    public LquTotIasOnerPrvnt getLquTotIasOnerPrvnt() {
        return lquTotIasOnerPrvnt;
    }

    public LquTotIasPnl getLquTotIasPnl() {
        return lquTotIasPnl;
    }

    public LquTotIasRstDpst getLquTotIasRstDpst() {
        return lquTotIasRstDpst;
    }

    public LquTotImpIntrPrest getLquTotImpIntrPrest() {
        return lquTotImpIntrPrest;
    }

    public LquTotImpIs getLquTotImpIs() {
        return lquTotImpIs;
    }

    public LquTotImpLrdLiqto getLquTotImpLrdLiqto() {
        return lquTotImpLrdLiqto;
    }

    public LquTotImpNetLiqto getLquTotImpNetLiqto() {
        return lquTotImpNetLiqto;
    }

    public LquTotImpPrest getLquTotImpPrest() {
        return lquTotImpPrest;
    }

    public LquTotImpRimb getLquTotImpRimb() {
        return lquTotImpRimb;
    }

    public LquTotImpRitAcc getLquTotImpRitAcc() {
        return lquTotImpRitAcc;
    }

    public LquTotImpRitTfr getLquTotImpRitTfr() {
        return lquTotImpRitTfr;
    }

    public LquTotImpRitVis getLquTotImpRitVis() {
        return lquTotImpRitVis;
    }

    public LquTotImpUti getLquTotImpUti() {
        return lquTotImpUti;
    }

    public LquTotImpbAcc getLquTotImpbAcc() {
        return lquTotImpbAcc;
    }

    public LquTotImpbTfr getLquTotImpbTfr() {
        return lquTotImpbTfr;
    }

    public LquTotImpbVis getLquTotImpbVis() {
        return lquTotImpbVis;
    }

    public LquTpMetRisc getLquTpMetRisc() {
        return lquTpMetRisc;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TP_OGG = 2;
        public static final int LQU_IB_OGG = 40;
        public static final int LQU_TP_LIQ = 2;
        public static final int LQU_DESC_CAU_EVE_SIN = 100;
        public static final int LQU_COD_CAU_SIN = 20;
        public static final int LQU_COD_SIN_CATSTRF = 20;
        public static final int LQU_TP_SIN = 2;
        public static final int LQU_TP_RISC = 2;
        public static final int LQU_COD_DVS = 20;
        public static final int LQU_TP_RIMB = 2;
        public static final int LQU_IB_LIQ = 40;
        public static final int LQU_TP_MEZ_PAG = 2;
        public static final int LQU_DS_UTENTE = 20;
        public static final int LQU_TP_CAUS_ANTIC = 2;
        public static final int LQU_ID_LIQ = 5;
        public static final int LQU_ID_OGG = 5;
        public static final int LQU_ID_MOVI_CRZ = 5;
        public static final int LQU_DT_INI_EFF = 5;
        public static final int LQU_DT_END_EFF = 5;
        public static final int LQU_COD_COMP_ANIA = 3;
        public static final int LQU_DESC_CAU_EVE_SIN_LEN = 2;
        public static final int LQU_DESC_CAU_EVE_SIN_VCHAR = LQU_DESC_CAU_EVE_SIN_LEN + LQU_DESC_CAU_EVE_SIN;
        public static final int LQU_DS_RIGA = 6;
        public static final int LQU_DS_OPER_SQL = 1;
        public static final int LQU_DS_VER = 5;
        public static final int LQU_DS_TS_INI_CPTZ = 10;
        public static final int LQU_DS_TS_END_CPTZ = 10;
        public static final int LQU_DS_STATO_ELAB = 1;
        public static final int LQU_FL_EVE_GARTO = 1;
        public static final int LQU_FL_PRE_COMP = 1;
        public static final int LIQ = LQU_ID_LIQ + LQU_ID_OGG + LQU_TP_OGG + LQU_ID_MOVI_CRZ + LquIdMoviChiu.Len.LQU_ID_MOVI_CHIU + LQU_DT_INI_EFF + LQU_DT_END_EFF + LQU_COD_COMP_ANIA + LQU_IB_OGG + LQU_TP_LIQ + LQU_DESC_CAU_EVE_SIN_VCHAR + LQU_COD_CAU_SIN + LQU_COD_SIN_CATSTRF + LquDtMor.Len.LQU_DT_MOR + LquDtDen.Len.LQU_DT_DEN + LquDtPervDen.Len.LQU_DT_PERV_DEN + LquDtRich.Len.LQU_DT_RICH + LQU_TP_SIN + LQU_TP_RISC + LquTpMetRisc.Len.LQU_TP_MET_RISC + LquDtLiq.Len.LQU_DT_LIQ + LQU_COD_DVS + LquTotImpLrdLiqto.Len.LQU_TOT_IMP_LRD_LIQTO + LquTotImpPrest.Len.LQU_TOT_IMP_PREST + LquTotImpIntrPrest.Len.LQU_TOT_IMP_INTR_PREST + LquTotImpUti.Len.LQU_TOT_IMP_UTI + LquTotImpRitTfr.Len.LQU_TOT_IMP_RIT_TFR + LquTotImpRitAcc.Len.LQU_TOT_IMP_RIT_ACC + LquTotImpRitVis.Len.LQU_TOT_IMP_RIT_VIS + LquTotImpbTfr.Len.LQU_TOT_IMPB_TFR + LquTotImpbAcc.Len.LQU_TOT_IMPB_ACC + LquTotImpbVis.Len.LQU_TOT_IMPB_VIS + LquTotImpRimb.Len.LQU_TOT_IMP_RIMB + LquImpbImpstPrvr.Len.LQU_IMPB_IMPST_PRVR + LquImpstPrvr.Len.LQU_IMPST_PRVR + LquImpbImpst252.Len.LQU_IMPB_IMPST252 + LquImpst252.Len.LQU_IMPST252 + LquTotImpIs.Len.LQU_TOT_IMP_IS + LquImpDirLiq.Len.LQU_IMP_DIR_LIQ + LquTotImpNetLiqto.Len.LQU_TOT_IMP_NET_LIQTO + LquMontEnd2000.Len.LQU_MONT_END2000 + LquMontEnd2006.Len.LQU_MONT_END2006 + LquPcRen.Len.LQU_PC_REN + LquImpPnl.Len.LQU_IMP_PNL + LquImpbIrpef.Len.LQU_IMPB_IRPEF + LquImpstIrpef.Len.LQU_IMPST_IRPEF + LquDtVlt.Len.LQU_DT_VLT + LquDtEndIstr.Len.LQU_DT_END_ISTR + LQU_TP_RIMB + LquSpeRcs.Len.LQU_SPE_RCS + LQU_IB_LIQ + LquTotIasOnerPrvnt.Len.LQU_TOT_IAS_ONER_PRVNT + LquTotIasMggSin.Len.LQU_TOT_IAS_MGG_SIN + LquTotIasRstDpst.Len.LQU_TOT_IAS_RST_DPST + LquImpOnerLiq.Len.LQU_IMP_ONER_LIQ + LquComponTaxRimb.Len.LQU_COMPON_TAX_RIMB + LQU_TP_MEZ_PAG + LquImpExcontr.Len.LQU_IMP_EXCONTR + LquImpIntrRitPag.Len.LQU_IMP_INTR_RIT_PAG + LquBnsNonGoduto.Len.LQU_BNS_NON_GODUTO + LquCnbtInpstfm.Len.LQU_CNBT_INPSTFM + LquImpstDaRimb.Len.LQU_IMPST_DA_RIMB + LquImpbIs.Len.LQU_IMPB_IS + LquTaxSep.Len.LQU_TAX_SEP + LquImpbTaxSep.Len.LQU_IMPB_TAX_SEP + LquImpbIntrSuPrest.Len.LQU_IMPB_INTR_SU_PREST + LquAddizComun.Len.LQU_ADDIZ_COMUN + LquImpbAddizComun.Len.LQU_IMPB_ADDIZ_COMUN + LquAddizRegion.Len.LQU_ADDIZ_REGION + LquImpbAddizRegion.Len.LQU_IMPB_ADDIZ_REGION + LquMontDal2007.Len.LQU_MONT_DAL2007 + LquImpbCnbtInpstfm.Len.LQU_IMPB_CNBT_INPSTFM + LquImpLrdDaRimb.Len.LQU_IMP_LRD_DA_RIMB + LquImpDirDaRimb.Len.LQU_IMP_DIR_DA_RIMB + LquRisMat.Len.LQU_RIS_MAT + LquRisSpe.Len.LQU_RIS_SPE + LQU_DS_RIGA + LQU_DS_OPER_SQL + LQU_DS_VER + LQU_DS_TS_INI_CPTZ + LQU_DS_TS_END_CPTZ + LQU_DS_UTENTE + LQU_DS_STATO_ELAB + LquTotIasPnl.Len.LQU_TOT_IAS_PNL + LQU_FL_EVE_GARTO + LquImpRenK1.Len.LQU_IMP_REN_K1 + LquImpRenK2.Len.LQU_IMP_REN_K2 + LquImpRenK3.Len.LQU_IMP_REN_K3 + LquPcRenK1.Len.LQU_PC_REN_K1 + LquPcRenK2.Len.LQU_PC_REN_K2 + LquPcRenK3.Len.LQU_PC_REN_K3 + LQU_TP_CAUS_ANTIC + LquImpLrdLiqtoRilt.Len.LQU_IMP_LRD_LIQTO_RILT + LquImpstApplRilt.Len.LQU_IMPST_APPL_RILT + LquPcRiscParz.Len.LQU_PC_RISC_PARZ + LquImpstBolloTotV.Len.LQU_IMPST_BOLLO_TOT_V + LquImpstBolloDettC.Len.LQU_IMPST_BOLLO_DETT_C + LquImpstBolloTotSw.Len.LQU_IMPST_BOLLO_TOT_SW + LquImpstBolloTotAa.Len.LQU_IMPST_BOLLO_TOT_AA + LquImpbVis1382011.Len.LQU_IMPB_VIS1382011 + LquImpstVis1382011.Len.LQU_IMPST_VIS1382011 + LquImpbIs1382011.Len.LQU_IMPB_IS1382011 + LquImpstSost1382011.Len.LQU_IMPST_SOST1382011 + LquPcAbbTitStat.Len.LQU_PC_ABB_TIT_STAT + LquImpbBolloDettC.Len.LQU_IMPB_BOLLO_DETT_C + LQU_FL_PRE_COMP + LquImpbVis662014.Len.LQU_IMPB_VIS662014 + LquImpstVis662014.Len.LQU_IMPST_VIS662014 + LquImpbIs662014.Len.LQU_IMPB_IS662014 + LquImpstSost662014.Len.LQU_IMPST_SOST662014 + LquPcAbbTs662014.Len.LQU_PC_ABB_TS662014 + LquImpLrdCalcCp.Len.LQU_IMP_LRD_CALC_CP + LquCosTunnelUscita.Len.LQU_COS_TUNNEL_USCITA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_ID_LIQ = 9;
            public static final int LQU_ID_OGG = 9;
            public static final int LQU_ID_MOVI_CRZ = 9;
            public static final int LQU_DT_INI_EFF = 8;
            public static final int LQU_DT_END_EFF = 8;
            public static final int LQU_COD_COMP_ANIA = 5;
            public static final int LQU_DS_RIGA = 10;
            public static final int LQU_DS_VER = 9;
            public static final int LQU_DS_TS_INI_CPTZ = 18;
            public static final int LQU_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
