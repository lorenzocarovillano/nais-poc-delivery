package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WcomReturnCode;

/**Original name: LCCC0090<br>
 * Variable: LCCC0090 from copybook LCCC0090<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccc0090 {

    //==== PROPERTIES ====
    //Original name: WCOM-CONNECT
    private char connect = 'S';
    //Original name: WCOM-NOME-TABELLA
    private String nomeTabella = DefaultValues.stringVal(Len.NOME_TABELLA);
    //Original name: WCOM-SEQ-TABELLA
    private int seqTabella = DefaultValues.INT_VAL;
    //Original name: WCOM-RETURN-CODE
    private WcomReturnCode returnCode = new WcomReturnCode();
    //Original name: WCOM-SQLCODE
    private Idso0011SqlcodeSigned sqlcode = new Idso0011SqlcodeSigned();
    //Original name: WCOM-DESCRIZ-ERR-DB2
    private String descrizErrDb2 = DefaultValues.stringVal(Len.DESCRIZ_ERR_DB2);

    //==== METHODS ====
    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        connect = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        nomeTabella = MarshalByte.readString(buffer, position, Len.NOME_TABELLA);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, connect);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, nomeTabella, Len.NOME_TABELLA);
        return buffer;
    }

    public void setConnect(char connect) {
        this.connect = connect;
    }

    public char getConnect() {
        return this.connect;
    }

    public void setNomeTabella(String nomeTabella) {
        this.nomeTabella = Functions.subString(nomeTabella, Len.NOME_TABELLA);
    }

    public String getNomeTabella() {
        return this.nomeTabella;
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        seqTabella = MarshalByte.readPackedAsInt(buffer, position, Len.Int.SEQ_TABELLA, 0);
        position += Len.SEQ_TABELLA;
        returnCode.setReturnCode(MarshalByte.readString(buffer, position, WcomReturnCode.Len.RETURN_CODE));
        position += WcomReturnCode.Len.RETURN_CODE;
        sqlcode.setSqlcodeSigned(MarshalByte.readInt(buffer, position, Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED));
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        descrizErrDb2 = MarshalByte.readString(buffer, position, Len.DESCRIZ_ERR_DB2);
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, seqTabella, Len.Int.SEQ_TABELLA, 0);
        position += Len.SEQ_TABELLA;
        MarshalByte.writeString(buffer, position, returnCode.getReturnCode(), WcomReturnCode.Len.RETURN_CODE);
        position += WcomReturnCode.Len.RETURN_CODE;
        MarshalByte.writeInt(buffer, position, sqlcode.getSqlcodeSigned(), Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED);
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        MarshalByte.writeString(buffer, position, descrizErrDb2, Len.DESCRIZ_ERR_DB2);
        return buffer;
    }

    public void setSeqTabella(int seqTabella) {
        this.seqTabella = seqTabella;
    }

    public int getSeqTabella() {
        return this.seqTabella;
    }

    public void setDescrizErrDb2(String descrizErrDb2) {
        this.descrizErrDb2 = Functions.subString(descrizErrDb2, Len.DESCRIZ_ERR_DB2);
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public WcomReturnCode getReturnCode() {
        return returnCode;
    }

    public Idso0011SqlcodeSigned getSqlcode() {
        return sqlcode;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NOME_TABELLA = 20;
        public static final int DESCRIZ_ERR_DB2 = 300;
        public static final int CONNECT = 1;
        public static final int DATI_INPUT = CONNECT + NOME_TABELLA;
        public static final int SEQ_TABELLA = 5;
        public static final int DATI_OUTPUT = SEQ_TABELLA + WcomReturnCode.Len.RETURN_CODE + Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED + DESCRIZ_ERR_DB2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SEQ_TABELLA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
