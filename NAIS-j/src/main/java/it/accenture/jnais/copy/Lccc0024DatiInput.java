package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.occurs.Lccc0024DatiMovSospesi;

/**Original name: LCCC0024-DATI-INPUT<br>
 * Variable: LCCC0024-DATI-INPUT from copybook LCCC0024<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccc0024DatiInput {

    //==== PROPERTIES ====
    public static final int LCCC0024_DATI_MOV_SOSPESI_MAXOCCURS = 10;
    //Original name: LCCC0024-ID-OGG-PRIM
    private int lccc0024IdOggPrim = DefaultValues.INT_VAL;
    //Original name: LCCC0024-TP-OGG-PRIM
    private String lccc0024TpOggPrim = DefaultValues.stringVal(Len.LCCC0024_TP_OGG_PRIM);
    //Original name: LCCC0024-ID-OGGETTO
    private int lccc0024IdOggetto = DefaultValues.INT_VAL;
    //Original name: LCCC0024-TP-OGGETTO
    private String lccc0024TpOggetto = DefaultValues.stringVal(Len.LCCC0024_TP_OGGETTO);
    //Original name: LCCC0024-COD-BLOCCO
    private String lccc0024CodBlocco = DefaultValues.stringVal(Len.LCCC0024_COD_BLOCCO);
    //Original name: LCCC0024-ID-RICH
    private int lccc0024IdRich = DefaultValues.INT_VAL;
    //Original name: LCCC0024-ELE-MOV-SOS-MAX
    private short lccc0024EleMovSosMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCC0024-DATI-MOV-SOSPESI
    private Lccc0024DatiMovSospesi[] lccc0024DatiMovSospesi = new Lccc0024DatiMovSospesi[LCCC0024_DATI_MOV_SOSPESI_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Lccc0024DatiInput() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int lccc0024DatiMovSospesiIdx = 1; lccc0024DatiMovSospesiIdx <= LCCC0024_DATI_MOV_SOSPESI_MAXOCCURS; lccc0024DatiMovSospesiIdx++) {
            lccc0024DatiMovSospesi[lccc0024DatiMovSospesiIdx - 1] = new Lccc0024DatiMovSospesi();
        }
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiBloccoBytes(buffer, position);
        position += Len.DATI_BLOCCO;
        lccc0024EleMovSosMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= LCCC0024_DATI_MOV_SOSPESI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                lccc0024DatiMovSospesi[idx - 1].setLccc0024DatiMovSospesiBytes(buffer, position);
                position += Lccc0024DatiMovSospesi.Len.LCCC0024_DATI_MOV_SOSPESI;
            }
            else {
                lccc0024DatiMovSospesi[idx - 1].initLccc0024DatiMovSospesiSpaces();
                position += Lccc0024DatiMovSospesi.Len.LCCC0024_DATI_MOV_SOSPESI;
            }
        }
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiBloccoBytes(buffer, position);
        position += Len.DATI_BLOCCO;
        MarshalByte.writeBinaryShort(buffer, position, lccc0024EleMovSosMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= LCCC0024_DATI_MOV_SOSPESI_MAXOCCURS; idx++) {
            lccc0024DatiMovSospesi[idx - 1].getLccc0024DatiMovSospesiBytes(buffer, position);
            position += Lccc0024DatiMovSospesi.Len.LCCC0024_DATI_MOV_SOSPESI;
        }
        return buffer;
    }

    public void setDatiBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        lccc0024IdOggPrim = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LCCC0024_ID_OGG_PRIM, 0);
        position += Len.LCCC0024_ID_OGG_PRIM;
        lccc0024TpOggPrim = MarshalByte.readString(buffer, position, Len.LCCC0024_TP_OGG_PRIM);
        position += Len.LCCC0024_TP_OGG_PRIM;
        lccc0024IdOggetto = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LCCC0024_ID_OGGETTO, 0);
        position += Len.LCCC0024_ID_OGGETTO;
        lccc0024TpOggetto = MarshalByte.readString(buffer, position, Len.LCCC0024_TP_OGGETTO);
        position += Len.LCCC0024_TP_OGGETTO;
        lccc0024CodBlocco = MarshalByte.readString(buffer, position, Len.LCCC0024_COD_BLOCCO);
        position += Len.LCCC0024_COD_BLOCCO;
        lccc0024IdRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LCCC0024_ID_RICH, 0);
    }

    public byte[] getDatiBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0024IdOggPrim, Len.Int.LCCC0024_ID_OGG_PRIM, 0);
        position += Len.LCCC0024_ID_OGG_PRIM;
        MarshalByte.writeString(buffer, position, lccc0024TpOggPrim, Len.LCCC0024_TP_OGG_PRIM);
        position += Len.LCCC0024_TP_OGG_PRIM;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0024IdOggetto, Len.Int.LCCC0024_ID_OGGETTO, 0);
        position += Len.LCCC0024_ID_OGGETTO;
        MarshalByte.writeString(buffer, position, lccc0024TpOggetto, Len.LCCC0024_TP_OGGETTO);
        position += Len.LCCC0024_TP_OGGETTO;
        MarshalByte.writeString(buffer, position, lccc0024CodBlocco, Len.LCCC0024_COD_BLOCCO);
        position += Len.LCCC0024_COD_BLOCCO;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0024IdRich, Len.Int.LCCC0024_ID_RICH, 0);
        return buffer;
    }

    public void setLccc0024IdOggPrim(int lccc0024IdOggPrim) {
        this.lccc0024IdOggPrim = lccc0024IdOggPrim;
    }

    public int getLccc0024IdOggPrim() {
        return this.lccc0024IdOggPrim;
    }

    public void setLccc0024TpOggPrim(String lccc0024TpOggPrim) {
        this.lccc0024TpOggPrim = Functions.subString(lccc0024TpOggPrim, Len.LCCC0024_TP_OGG_PRIM);
    }

    public String getLccc0024TpOggPrim() {
        return this.lccc0024TpOggPrim;
    }

    public void setLccc0024IdOggetto(int lccc0024IdOggetto) {
        this.lccc0024IdOggetto = lccc0024IdOggetto;
    }

    public int getLccc0024IdOggetto() {
        return this.lccc0024IdOggetto;
    }

    public void setLccc0024TpOggetto(String lccc0024TpOggetto) {
        this.lccc0024TpOggetto = Functions.subString(lccc0024TpOggetto, Len.LCCC0024_TP_OGGETTO);
    }

    public String getLccc0024TpOggetto() {
        return this.lccc0024TpOggetto;
    }

    public String getLccc0024TpOggettoFormatted() {
        return Functions.padBlanks(getLccc0024TpOggetto(), Len.LCCC0024_TP_OGGETTO);
    }

    public void setLccc0024CodBlocco(String lccc0024CodBlocco) {
        this.lccc0024CodBlocco = Functions.subString(lccc0024CodBlocco, Len.LCCC0024_COD_BLOCCO);
    }

    public String getLccc0024CodBlocco() {
        return this.lccc0024CodBlocco;
    }

    public String getLccc0024CodBloccoFormatted() {
        return Functions.padBlanks(getLccc0024CodBlocco(), Len.LCCC0024_COD_BLOCCO);
    }

    public void setLccc0024IdRich(int lccc0024IdRich) {
        this.lccc0024IdRich = lccc0024IdRich;
    }

    public int getLccc0024IdRich() {
        return this.lccc0024IdRich;
    }

    public void setLccc0024EleMovSosMax(short lccc0024EleMovSosMax) {
        this.lccc0024EleMovSosMax = lccc0024EleMovSosMax;
    }

    public short getLccc0024EleMovSosMax() {
        return this.lccc0024EleMovSosMax;
    }

    public Lccc0024DatiMovSospesi getLccc0024DatiMovSospesi(int idx) {
        return lccc0024DatiMovSospesi[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0024_ID_OGG_PRIM = 5;
        public static final int LCCC0024_TP_OGG_PRIM = 2;
        public static final int LCCC0024_ID_OGGETTO = 5;
        public static final int LCCC0024_TP_OGGETTO = 2;
        public static final int LCCC0024_COD_BLOCCO = 5;
        public static final int LCCC0024_ID_RICH = 5;
        public static final int DATI_BLOCCO = LCCC0024_ID_OGG_PRIM + LCCC0024_TP_OGG_PRIM + LCCC0024_ID_OGGETTO + LCCC0024_TP_OGGETTO + LCCC0024_COD_BLOCCO + LCCC0024_ID_RICH;
        public static final int LCCC0024_ELE_MOV_SOS_MAX = 2;
        public static final int DATI_INPUT = DATI_BLOCCO + LCCC0024_ELE_MOV_SOS_MAX + Lccc0024DatiInput.LCCC0024_DATI_MOV_SOSPESI_MAXOCCURS * Lccc0024DatiMovSospesi.Len.LCCC0024_DATI_MOV_SOSPESI;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LCCC0024_ID_OGG_PRIM = 9;
            public static final int LCCC0024_ID_OGGETTO = 9;
            public static final int LCCC0024_ID_RICH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
