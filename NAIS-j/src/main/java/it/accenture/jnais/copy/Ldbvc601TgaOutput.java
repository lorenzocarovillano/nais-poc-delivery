package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVC601-TGA-OUTPUT<br>
 * Variable: LDBVC601-TGA-OUTPUT from copybook LDBVC601<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbvc601TgaOutput {

    //==== PROPERTIES ====
    //Original name: LC601-ID-TRCH-DI-GAR
    private int idTrchDiGar = DefaultValues.INT_VAL;
    //Original name: LC601-ID-GAR
    private int idGar = DefaultValues.INT_VAL;
    //Original name: LC601-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LC601-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LC601-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: LC601-ID-MOVI-CHIU
    private int idMoviChiu = DefaultValues.INT_VAL;
    //Original name: LC601-DT-INI-EFF
    private int dtIniEff = DefaultValues.INT_VAL;
    //Original name: LC601-DT-END-EFF
    private int dtEndEff = DefaultValues.INT_VAL;
    //Original name: LC601-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: LC601-DT-DECOR
    private int dtDecor = DefaultValues.INT_VAL;
    //Original name: LC601-DT-SCAD
    private int dtScad = DefaultValues.INT_VAL;
    //Original name: LC601-IB-OGG
    private String ibOgg = DefaultValues.stringVal(Len.IB_OGG);
    //Original name: LC601-TP-RGM-FISC
    private String tpRgmFisc = DefaultValues.stringVal(Len.TP_RGM_FISC);
    //Original name: LC601-DT-EMIS
    private int dtEmis = DefaultValues.INT_VAL;
    //Original name: LC601-TP-TRCH
    private String tpTrch = DefaultValues.stringVal(Len.TP_TRCH);
    //Original name: LC601-DUR-AA
    private int durAa = DefaultValues.INT_VAL;
    //Original name: LC601-DUR-MM
    private int durMm = DefaultValues.INT_VAL;
    //Original name: LC601-DUR-GG
    private int durGg = DefaultValues.INT_VAL;
    //Original name: LC601-PRE-CASO-MOR
    private AfDecimal preCasoMor = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PC-INTR-RIAT
    private AfDecimal pcIntrRiat = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LC601-IMP-BNS-ANTIC
    private AfDecimal impBnsAntic = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRE-INI-NET
    private AfDecimal preIniNet = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRE-PP-INI
    private AfDecimal prePpIni = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRE-PP-ULT
    private AfDecimal prePpUlt = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRE-TARI-INI
    private AfDecimal preTariIni = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRE-TARI-ULT
    private AfDecimal preTariUlt = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRE-INVRIO-INI
    private AfDecimal preInvrioIni = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRE-INVRIO-ULT
    private AfDecimal preInvrioUlt = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRE-RIVTO
    private AfDecimal preRivto = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-SOPR-PROF
    private AfDecimal impSoprProf = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-SOPR-SAN
    private AfDecimal impSoprSan = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-SOPR-SPO
    private AfDecimal impSoprSpo = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-SOPR-TEC
    private AfDecimal impSoprTec = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-ALT-SOPR
    private AfDecimal impAltSopr = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRE-STAB
    private AfDecimal preStab = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-DT-EFF-STAB
    private int dtEffStab = DefaultValues.INT_VAL;
    //Original name: LC601-TS-RIVAL-FIS
    private AfDecimal tsRivalFis = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: LC601-TS-RIVAL-INDICIZ
    private AfDecimal tsRivalIndiciz = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: LC601-OLD-TS-TEC
    private AfDecimal oldTsTec = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: LC601-RAT-LRD
    private AfDecimal ratLrd = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRE-LRD
    private AfDecimal preLrd = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRSTZ-INI
    private AfDecimal prstzIni = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRSTZ-ULT
    private AfDecimal prstzUlt = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-CPT-IN-OPZ-RIVTO
    private AfDecimal cptInOpzRivto = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRSTZ-INI-STAB
    private AfDecimal prstzIniStab = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-CPT-RSH-MOR
    private AfDecimal cptRshMor = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRSTZ-RID-INI
    private AfDecimal prstzRidIni = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-FL-CAR-CONT
    private char flCarCont = DefaultValues.CHAR_VAL;
    //Original name: LC601-BNS-GIA-LIQTO
    private AfDecimal bnsGiaLiqto = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-BNS
    private AfDecimal impBns = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-COD-DVS
    private String codDvs = DefaultValues.stringVal(Len.COD_DVS);
    //Original name: LC601-PRSTZ-INI-NEWFIS
    private AfDecimal prstzIniNewfis = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-SCON
    private AfDecimal impScon = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-ALQ-SCON
    private AfDecimal alqScon = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LC601-IMP-CAR-ACQ
    private AfDecimal impCarAcq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-CAR-INC
    private AfDecimal impCarInc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-CAR-GEST
    private AfDecimal impCarGest = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-ETA-AA-1O-ASSTO
    private short etaAa1oAssto = DefaultValues.SHORT_VAL;
    //Original name: LC601-ETA-MM-1O-ASSTO
    private short etaMm1oAssto = DefaultValues.SHORT_VAL;
    //Original name: LC601-ETA-AA-2O-ASSTO
    private short etaAa2oAssto = DefaultValues.SHORT_VAL;
    //Original name: LC601-ETA-MM-2O-ASSTO
    private short etaMm2oAssto = DefaultValues.SHORT_VAL;
    //Original name: LC601-ETA-AA-3O-ASSTO
    private short etaAa3oAssto = DefaultValues.SHORT_VAL;
    //Original name: LC601-ETA-MM-3O-ASSTO
    private short etaMm3oAssto = DefaultValues.SHORT_VAL;
    //Original name: LC601-RENDTO-LRD
    private AfDecimal rendtoLrd = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: LC601-PC-RETR
    private AfDecimal pcRetr = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LC601-RENDTO-RETR
    private AfDecimal rendtoRetr = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: LC601-MIN-GARTO
    private AfDecimal minGarto = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: LC601-MIN-TRNUT
    private AfDecimal minTrnut = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: LC601-PRE-ATT-DI-TRCH
    private AfDecimal preAttDiTrch = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-MATU-END2000
    private AfDecimal matuEnd2000 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-ABB-TOT-INI
    private AfDecimal abbTotIni = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-ABB-TOT-ULT
    private AfDecimal abbTotUlt = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-ABB-ANNU-ULT
    private AfDecimal abbAnnuUlt = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-DUR-ABB
    private int durAbb = DefaultValues.INT_VAL;
    //Original name: LC601-TP-ADEG-ABB
    private char tpAdegAbb = DefaultValues.CHAR_VAL;
    //Original name: LC601-MOD-CALC
    private String modCalc = DefaultValues.stringVal(Len.MOD_CALC);
    //Original name: LC601-IMP-AZ
    private AfDecimal impAz = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-ADER
    private AfDecimal impAder = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-TFR
    private AfDecimal impTfr = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMP-VOLO
    private AfDecimal impVolo = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-VIS-END2000
    private AfDecimal visEnd2000 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-DT-VLDT-PROD
    private int dtVldtProd = DefaultValues.INT_VAL;
    //Original name: LC601-DT-INI-VAL-TAR
    private int dtIniValTar = DefaultValues.INT_VAL;
    //Original name: LC601-IMPB-VIS-END2000
    private AfDecimal impbVisEnd2000 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-REN-INI-TS-TEC-0
    private AfDecimal renIniTsTec0 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PC-RIP-PRE
    private AfDecimal pcRipPre = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LC601-FL-IMPORTI-FORZ
    private char flImportiForz = DefaultValues.CHAR_VAL;
    //Original name: LC601-PRSTZ-INI-NFORZ
    private AfDecimal prstzIniNforz = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-VIS-END2000-NFORZ
    private AfDecimal visEnd2000Nforz = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-INTR-MORA
    private AfDecimal intrMora = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-MANFEE-ANTIC
    private AfDecimal manfeeAntic = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-MANFEE-RICOR
    private AfDecimal manfeeRicor = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PRE-UNI-RIVTO
    private AfDecimal preUniRivto = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PROV-1AA-ACQ
    private AfDecimal prov1aaAcq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PROV-2AA-ACQ
    private AfDecimal prov2aaAcq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PROV-RICOR
    private AfDecimal provRicor = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-PROV-INC
    private AfDecimal provInc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-ALQ-PROV-ACQ
    private AfDecimal alqProvAcq = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LC601-ALQ-PROV-INC
    private AfDecimal alqProvInc = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LC601-ALQ-PROV-RICOR
    private AfDecimal alqProvRicor = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: LC601-IMPB-PROV-ACQ
    private AfDecimal impbProvAcq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMPB-PROV-INC
    private AfDecimal impbProvInc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-IMPB-PROV-RICOR
    private AfDecimal impbProvRicor = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-FL-PROV-FORZ
    private char flProvForz = DefaultValues.CHAR_VAL;
    //Original name: LC601-PRSTZ-AGG-INI
    private AfDecimal prstzAggIni = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-INCR-PRE
    private AfDecimal incrPre = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-INCR-PRSTZ
    private AfDecimal incrPrstz = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-DT-ULT-ADEG-PRE-PR
    private int dtUltAdegPrePr = DefaultValues.INT_VAL;
    //Original name: LC601-PRSTZ-AGG-ULT
    private AfDecimal prstzAggUlt = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-TS-RIVAL-NET
    private AfDecimal tsRivalNet = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: LC601-PRE-PATTUITO
    private AfDecimal prePattuito = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-TP-RIVAL
    private String tpRival = DefaultValues.stringVal(Len.TP_RIVAL);
    //Original name: LC601-RIS-MAT
    private AfDecimal risMat = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-CPT-MIN-SCAD
    private AfDecimal cptMinScad = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-COMMIS-GEST
    private AfDecimal commisGest = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LC601-TP-MANFEE-APPL
    private String tpManfeeAppl = DefaultValues.stringVal(Len.TP_MANFEE_APPL);
    //Original name: LC601-DS-RIGA
    private long dsRiga = DefaultValues.LONG_VAL;
    //Original name: LC601-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: LC601-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: LC601-DS-TS-INI-CPTZ
    private long dsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: LC601-DS-TS-END-CPTZ
    private long dsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: LC601-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: LC601-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: LC601-PC-COMMIS-GEST
    private AfDecimal pcCommisGest = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);

    //==== METHODS ====
    public void setLdbvc601TgaOutputBytes(byte[] buffer) {
        setLdbvc601TgaOutputBytes(buffer, 1);
    }

    public void setLdbvc601TgaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        idTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_TRCH_DI_GAR, 0);
        position += Len.ID_TRCH_DI_GAR;
        idGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_GAR, 0);
        position += Len.ID_GAR;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        idMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        idMoviChiu = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        dtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        dtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        dtDecor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DECOR, 0);
        position += Len.DT_DECOR;
        dtScad = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_SCAD, 0);
        position += Len.DT_SCAD;
        ibOgg = MarshalByte.readString(buffer, position, Len.IB_OGG);
        position += Len.IB_OGG;
        tpRgmFisc = MarshalByte.readString(buffer, position, Len.TP_RGM_FISC);
        position += Len.TP_RGM_FISC;
        dtEmis = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_EMIS, 0);
        position += Len.DT_EMIS;
        tpTrch = MarshalByte.readString(buffer, position, Len.TP_TRCH);
        position += Len.TP_TRCH;
        durAa = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DUR_AA, 0);
        position += Len.DUR_AA;
        durMm = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DUR_MM, 0);
        position += Len.DUR_MM;
        durGg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DUR_GG, 0);
        position += Len.DUR_GG;
        preCasoMor.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_CASO_MOR, Len.Fract.PRE_CASO_MOR));
        position += Len.PRE_CASO_MOR;
        pcIntrRiat.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_INTR_RIAT, Len.Fract.PC_INTR_RIAT));
        position += Len.PC_INTR_RIAT;
        impBnsAntic.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_BNS_ANTIC, Len.Fract.IMP_BNS_ANTIC));
        position += Len.IMP_BNS_ANTIC;
        preIniNet.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_INI_NET, Len.Fract.PRE_INI_NET));
        position += Len.PRE_INI_NET;
        prePpIni.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_PP_INI, Len.Fract.PRE_PP_INI));
        position += Len.PRE_PP_INI;
        prePpUlt.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_PP_ULT, Len.Fract.PRE_PP_ULT));
        position += Len.PRE_PP_ULT;
        preTariIni.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_TARI_INI, Len.Fract.PRE_TARI_INI));
        position += Len.PRE_TARI_INI;
        preTariUlt.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_TARI_ULT, Len.Fract.PRE_TARI_ULT));
        position += Len.PRE_TARI_ULT;
        preInvrioIni.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_INVRIO_INI, Len.Fract.PRE_INVRIO_INI));
        position += Len.PRE_INVRIO_INI;
        preInvrioUlt.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_INVRIO_ULT, Len.Fract.PRE_INVRIO_ULT));
        position += Len.PRE_INVRIO_ULT;
        preRivto.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_RIVTO, Len.Fract.PRE_RIVTO));
        position += Len.PRE_RIVTO;
        impSoprProf.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_SOPR_PROF, Len.Fract.IMP_SOPR_PROF));
        position += Len.IMP_SOPR_PROF;
        impSoprSan.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_SOPR_SAN, Len.Fract.IMP_SOPR_SAN));
        position += Len.IMP_SOPR_SAN;
        impSoprSpo.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_SOPR_SPO, Len.Fract.IMP_SOPR_SPO));
        position += Len.IMP_SOPR_SPO;
        impSoprTec.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_SOPR_TEC, Len.Fract.IMP_SOPR_TEC));
        position += Len.IMP_SOPR_TEC;
        impAltSopr.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_ALT_SOPR, Len.Fract.IMP_ALT_SOPR));
        position += Len.IMP_ALT_SOPR;
        preStab.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_STAB, Len.Fract.PRE_STAB));
        position += Len.PRE_STAB;
        dtEffStab = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_EFF_STAB, 0);
        position += Len.DT_EFF_STAB;
        tsRivalFis.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.TS_RIVAL_FIS, Len.Fract.TS_RIVAL_FIS));
        position += Len.TS_RIVAL_FIS;
        tsRivalIndiciz.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.TS_RIVAL_INDICIZ, Len.Fract.TS_RIVAL_INDICIZ));
        position += Len.TS_RIVAL_INDICIZ;
        oldTsTec.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.OLD_TS_TEC, Len.Fract.OLD_TS_TEC));
        position += Len.OLD_TS_TEC;
        ratLrd.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.RAT_LRD, Len.Fract.RAT_LRD));
        position += Len.RAT_LRD;
        preLrd.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_LRD, Len.Fract.PRE_LRD));
        position += Len.PRE_LRD;
        prstzIni.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRSTZ_INI, Len.Fract.PRSTZ_INI));
        position += Len.PRSTZ_INI;
        prstzUlt.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRSTZ_ULT, Len.Fract.PRSTZ_ULT));
        position += Len.PRSTZ_ULT;
        cptInOpzRivto.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.CPT_IN_OPZ_RIVTO, Len.Fract.CPT_IN_OPZ_RIVTO));
        position += Len.CPT_IN_OPZ_RIVTO;
        prstzIniStab.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRSTZ_INI_STAB, Len.Fract.PRSTZ_INI_STAB));
        position += Len.PRSTZ_INI_STAB;
        cptRshMor.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.CPT_RSH_MOR, Len.Fract.CPT_RSH_MOR));
        position += Len.CPT_RSH_MOR;
        prstzRidIni.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRSTZ_RID_INI, Len.Fract.PRSTZ_RID_INI));
        position += Len.PRSTZ_RID_INI;
        flCarCont = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bnsGiaLiqto.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.BNS_GIA_LIQTO, Len.Fract.BNS_GIA_LIQTO));
        position += Len.BNS_GIA_LIQTO;
        impBns.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_BNS, Len.Fract.IMP_BNS));
        position += Len.IMP_BNS;
        codDvs = MarshalByte.readString(buffer, position, Len.COD_DVS);
        position += Len.COD_DVS;
        prstzIniNewfis.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRSTZ_INI_NEWFIS, Len.Fract.PRSTZ_INI_NEWFIS));
        position += Len.PRSTZ_INI_NEWFIS;
        impScon.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_SCON, Len.Fract.IMP_SCON));
        position += Len.IMP_SCON;
        alqScon.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ALQ_SCON, Len.Fract.ALQ_SCON));
        position += Len.ALQ_SCON;
        impCarAcq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_CAR_ACQ, Len.Fract.IMP_CAR_ACQ));
        position += Len.IMP_CAR_ACQ;
        impCarInc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_CAR_INC, Len.Fract.IMP_CAR_INC));
        position += Len.IMP_CAR_INC;
        impCarGest.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_CAR_GEST, Len.Fract.IMP_CAR_GEST));
        position += Len.IMP_CAR_GEST;
        etaAa1oAssto = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ETA_AA1O_ASSTO, 0);
        position += Len.ETA_AA1O_ASSTO;
        etaMm1oAssto = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ETA_MM1O_ASSTO, 0);
        position += Len.ETA_MM1O_ASSTO;
        etaAa2oAssto = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ETA_AA2O_ASSTO, 0);
        position += Len.ETA_AA2O_ASSTO;
        etaMm2oAssto = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ETA_MM2O_ASSTO, 0);
        position += Len.ETA_MM2O_ASSTO;
        etaAa3oAssto = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ETA_AA3O_ASSTO, 0);
        position += Len.ETA_AA3O_ASSTO;
        etaMm3oAssto = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ETA_MM3O_ASSTO, 0);
        position += Len.ETA_MM3O_ASSTO;
        rendtoLrd.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.RENDTO_LRD, Len.Fract.RENDTO_LRD));
        position += Len.RENDTO_LRD;
        pcRetr.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_RETR, Len.Fract.PC_RETR));
        position += Len.PC_RETR;
        rendtoRetr.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.RENDTO_RETR, Len.Fract.RENDTO_RETR));
        position += Len.RENDTO_RETR;
        minGarto.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.MIN_GARTO, Len.Fract.MIN_GARTO));
        position += Len.MIN_GARTO;
        minTrnut.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.MIN_TRNUT, Len.Fract.MIN_TRNUT));
        position += Len.MIN_TRNUT;
        preAttDiTrch.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_ATT_DI_TRCH, Len.Fract.PRE_ATT_DI_TRCH));
        position += Len.PRE_ATT_DI_TRCH;
        matuEnd2000.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.MATU_END2000, Len.Fract.MATU_END2000));
        position += Len.MATU_END2000;
        abbTotIni.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ABB_TOT_INI, Len.Fract.ABB_TOT_INI));
        position += Len.ABB_TOT_INI;
        abbTotUlt.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ABB_TOT_ULT, Len.Fract.ABB_TOT_ULT));
        position += Len.ABB_TOT_ULT;
        abbAnnuUlt.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ABB_ANNU_ULT, Len.Fract.ABB_ANNU_ULT));
        position += Len.ABB_ANNU_ULT;
        durAbb = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DUR_ABB, 0);
        position += Len.DUR_ABB;
        tpAdegAbb = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        modCalc = MarshalByte.readString(buffer, position, Len.MOD_CALC);
        position += Len.MOD_CALC;
        impAz.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_AZ, Len.Fract.IMP_AZ));
        position += Len.IMP_AZ;
        impAder.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_ADER, Len.Fract.IMP_ADER));
        position += Len.IMP_ADER;
        impTfr.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_TFR, Len.Fract.IMP_TFR));
        position += Len.IMP_TFR;
        impVolo.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_VOLO, Len.Fract.IMP_VOLO));
        position += Len.IMP_VOLO;
        visEnd2000.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.VIS_END2000, Len.Fract.VIS_END2000));
        position += Len.VIS_END2000;
        dtVldtProd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_VLDT_PROD, 0);
        position += Len.DT_VLDT_PROD;
        dtIniValTar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_VAL_TAR, 0);
        position += Len.DT_INI_VAL_TAR;
        impbVisEnd2000.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_VIS_END2000, Len.Fract.IMPB_VIS_END2000));
        position += Len.IMPB_VIS_END2000;
        renIniTsTec0.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.REN_INI_TS_TEC0, Len.Fract.REN_INI_TS_TEC0));
        position += Len.REN_INI_TS_TEC0;
        pcRipPre.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_RIP_PRE, Len.Fract.PC_RIP_PRE));
        position += Len.PC_RIP_PRE;
        flImportiForz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        prstzIniNforz.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRSTZ_INI_NFORZ, Len.Fract.PRSTZ_INI_NFORZ));
        position += Len.PRSTZ_INI_NFORZ;
        visEnd2000Nforz.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.VIS_END2000_NFORZ, Len.Fract.VIS_END2000_NFORZ));
        position += Len.VIS_END2000_NFORZ;
        intrMora.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.INTR_MORA, Len.Fract.INTR_MORA));
        position += Len.INTR_MORA;
        manfeeAntic.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.MANFEE_ANTIC, Len.Fract.MANFEE_ANTIC));
        position += Len.MANFEE_ANTIC;
        manfeeRicor.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.MANFEE_RICOR, Len.Fract.MANFEE_RICOR));
        position += Len.MANFEE_RICOR;
        preUniRivto.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_UNI_RIVTO, Len.Fract.PRE_UNI_RIVTO));
        position += Len.PRE_UNI_RIVTO;
        prov1aaAcq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PROV1AA_ACQ, Len.Fract.PROV1AA_ACQ));
        position += Len.PROV1AA_ACQ;
        prov2aaAcq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PROV2AA_ACQ, Len.Fract.PROV2AA_ACQ));
        position += Len.PROV2AA_ACQ;
        provRicor.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PROV_RICOR, Len.Fract.PROV_RICOR));
        position += Len.PROV_RICOR;
        provInc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PROV_INC, Len.Fract.PROV_INC));
        position += Len.PROV_INC;
        alqProvAcq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ALQ_PROV_ACQ, Len.Fract.ALQ_PROV_ACQ));
        position += Len.ALQ_PROV_ACQ;
        alqProvInc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ALQ_PROV_INC, Len.Fract.ALQ_PROV_INC));
        position += Len.ALQ_PROV_INC;
        alqProvRicor.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ALQ_PROV_RICOR, Len.Fract.ALQ_PROV_RICOR));
        position += Len.ALQ_PROV_RICOR;
        impbProvAcq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_PROV_ACQ, Len.Fract.IMPB_PROV_ACQ));
        position += Len.IMPB_PROV_ACQ;
        impbProvInc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_PROV_INC, Len.Fract.IMPB_PROV_INC));
        position += Len.IMPB_PROV_INC;
        impbProvRicor.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_PROV_RICOR, Len.Fract.IMPB_PROV_RICOR));
        position += Len.IMPB_PROV_RICOR;
        flProvForz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        prstzAggIni.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRSTZ_AGG_INI, Len.Fract.PRSTZ_AGG_INI));
        position += Len.PRSTZ_AGG_INI;
        incrPre.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.INCR_PRE, Len.Fract.INCR_PRE));
        position += Len.INCR_PRE;
        incrPrstz.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.INCR_PRSTZ, Len.Fract.INCR_PRSTZ));
        position += Len.INCR_PRSTZ;
        dtUltAdegPrePr = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_ULT_ADEG_PRE_PR, 0);
        position += Len.DT_ULT_ADEG_PRE_PR;
        prstzAggUlt.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRSTZ_AGG_ULT, Len.Fract.PRSTZ_AGG_ULT));
        position += Len.PRSTZ_AGG_ULT;
        tsRivalNet.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.TS_RIVAL_NET, Len.Fract.TS_RIVAL_NET));
        position += Len.TS_RIVAL_NET;
        prePattuito.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_PATTUITO, Len.Fract.PRE_PATTUITO));
        position += Len.PRE_PATTUITO;
        tpRival = MarshalByte.readString(buffer, position, Len.TP_RIVAL);
        position += Len.TP_RIVAL;
        risMat.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.RIS_MAT, Len.Fract.RIS_MAT));
        position += Len.RIS_MAT;
        cptMinScad.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.CPT_MIN_SCAD, Len.Fract.CPT_MIN_SCAD));
        position += Len.CPT_MIN_SCAD;
        commisGest.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.COMMIS_GEST, Len.Fract.COMMIS_GEST));
        position += Len.COMMIS_GEST;
        tpManfeeAppl = MarshalByte.readString(buffer, position, Len.TP_MANFEE_APPL);
        position += Len.TP_MANFEE_APPL;
        dsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        dsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcCommisGest.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_COMMIS_GEST, Len.Fract.PC_COMMIS_GEST));
    }

    public byte[] getLdbvc601TgaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idTrchDiGar, Len.Int.ID_TRCH_DI_GAR, 0);
        position += Len.ID_TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, idGar, Len.Int.ID_GAR, 0);
        position += Len.ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviCrz, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviChiu, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniEff, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtEndEff, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, dtDecor, Len.Int.DT_DECOR, 0);
        position += Len.DT_DECOR;
        MarshalByte.writeIntAsPacked(buffer, position, dtScad, Len.Int.DT_SCAD, 0);
        position += Len.DT_SCAD;
        MarshalByte.writeString(buffer, position, ibOgg, Len.IB_OGG);
        position += Len.IB_OGG;
        MarshalByte.writeString(buffer, position, tpRgmFisc, Len.TP_RGM_FISC);
        position += Len.TP_RGM_FISC;
        MarshalByte.writeIntAsPacked(buffer, position, dtEmis, Len.Int.DT_EMIS, 0);
        position += Len.DT_EMIS;
        MarshalByte.writeString(buffer, position, tpTrch, Len.TP_TRCH);
        position += Len.TP_TRCH;
        MarshalByte.writeIntAsPacked(buffer, position, durAa, Len.Int.DUR_AA, 0);
        position += Len.DUR_AA;
        MarshalByte.writeIntAsPacked(buffer, position, durMm, Len.Int.DUR_MM, 0);
        position += Len.DUR_MM;
        MarshalByte.writeIntAsPacked(buffer, position, durGg, Len.Int.DUR_GG, 0);
        position += Len.DUR_GG;
        MarshalByte.writeDecimalAsPacked(buffer, position, preCasoMor.copy());
        position += Len.PRE_CASO_MOR;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcIntrRiat.copy());
        position += Len.PC_INTR_RIAT;
        MarshalByte.writeDecimalAsPacked(buffer, position, impBnsAntic.copy());
        position += Len.IMP_BNS_ANTIC;
        MarshalByte.writeDecimalAsPacked(buffer, position, preIniNet.copy());
        position += Len.PRE_INI_NET;
        MarshalByte.writeDecimalAsPacked(buffer, position, prePpIni.copy());
        position += Len.PRE_PP_INI;
        MarshalByte.writeDecimalAsPacked(buffer, position, prePpUlt.copy());
        position += Len.PRE_PP_ULT;
        MarshalByte.writeDecimalAsPacked(buffer, position, preTariIni.copy());
        position += Len.PRE_TARI_INI;
        MarshalByte.writeDecimalAsPacked(buffer, position, preTariUlt.copy());
        position += Len.PRE_TARI_ULT;
        MarshalByte.writeDecimalAsPacked(buffer, position, preInvrioIni.copy());
        position += Len.PRE_INVRIO_INI;
        MarshalByte.writeDecimalAsPacked(buffer, position, preInvrioUlt.copy());
        position += Len.PRE_INVRIO_ULT;
        MarshalByte.writeDecimalAsPacked(buffer, position, preRivto.copy());
        position += Len.PRE_RIVTO;
        MarshalByte.writeDecimalAsPacked(buffer, position, impSoprProf.copy());
        position += Len.IMP_SOPR_PROF;
        MarshalByte.writeDecimalAsPacked(buffer, position, impSoprSan.copy());
        position += Len.IMP_SOPR_SAN;
        MarshalByte.writeDecimalAsPacked(buffer, position, impSoprSpo.copy());
        position += Len.IMP_SOPR_SPO;
        MarshalByte.writeDecimalAsPacked(buffer, position, impSoprTec.copy());
        position += Len.IMP_SOPR_TEC;
        MarshalByte.writeDecimalAsPacked(buffer, position, impAltSopr.copy());
        position += Len.IMP_ALT_SOPR;
        MarshalByte.writeDecimalAsPacked(buffer, position, preStab.copy());
        position += Len.PRE_STAB;
        MarshalByte.writeIntAsPacked(buffer, position, dtEffStab, Len.Int.DT_EFF_STAB, 0);
        position += Len.DT_EFF_STAB;
        MarshalByte.writeDecimalAsPacked(buffer, position, tsRivalFis.copy());
        position += Len.TS_RIVAL_FIS;
        MarshalByte.writeDecimalAsPacked(buffer, position, tsRivalIndiciz.copy());
        position += Len.TS_RIVAL_INDICIZ;
        MarshalByte.writeDecimalAsPacked(buffer, position, oldTsTec.copy());
        position += Len.OLD_TS_TEC;
        MarshalByte.writeDecimalAsPacked(buffer, position, ratLrd.copy());
        position += Len.RAT_LRD;
        MarshalByte.writeDecimalAsPacked(buffer, position, preLrd.copy());
        position += Len.PRE_LRD;
        MarshalByte.writeDecimalAsPacked(buffer, position, prstzIni.copy());
        position += Len.PRSTZ_INI;
        MarshalByte.writeDecimalAsPacked(buffer, position, prstzUlt.copy());
        position += Len.PRSTZ_ULT;
        MarshalByte.writeDecimalAsPacked(buffer, position, cptInOpzRivto.copy());
        position += Len.CPT_IN_OPZ_RIVTO;
        MarshalByte.writeDecimalAsPacked(buffer, position, prstzIniStab.copy());
        position += Len.PRSTZ_INI_STAB;
        MarshalByte.writeDecimalAsPacked(buffer, position, cptRshMor.copy());
        position += Len.CPT_RSH_MOR;
        MarshalByte.writeDecimalAsPacked(buffer, position, prstzRidIni.copy());
        position += Len.PRSTZ_RID_INI;
        MarshalByte.writeChar(buffer, position, flCarCont);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, bnsGiaLiqto.copy());
        position += Len.BNS_GIA_LIQTO;
        MarshalByte.writeDecimalAsPacked(buffer, position, impBns.copy());
        position += Len.IMP_BNS;
        MarshalByte.writeString(buffer, position, codDvs, Len.COD_DVS);
        position += Len.COD_DVS;
        MarshalByte.writeDecimalAsPacked(buffer, position, prstzIniNewfis.copy());
        position += Len.PRSTZ_INI_NEWFIS;
        MarshalByte.writeDecimalAsPacked(buffer, position, impScon.copy());
        position += Len.IMP_SCON;
        MarshalByte.writeDecimalAsPacked(buffer, position, alqScon.copy());
        position += Len.ALQ_SCON;
        MarshalByte.writeDecimalAsPacked(buffer, position, impCarAcq.copy());
        position += Len.IMP_CAR_ACQ;
        MarshalByte.writeDecimalAsPacked(buffer, position, impCarInc.copy());
        position += Len.IMP_CAR_INC;
        MarshalByte.writeDecimalAsPacked(buffer, position, impCarGest.copy());
        position += Len.IMP_CAR_GEST;
        MarshalByte.writeShortAsPacked(buffer, position, etaAa1oAssto, Len.Int.ETA_AA1O_ASSTO, 0);
        position += Len.ETA_AA1O_ASSTO;
        MarshalByte.writeShortAsPacked(buffer, position, etaMm1oAssto, Len.Int.ETA_MM1O_ASSTO, 0);
        position += Len.ETA_MM1O_ASSTO;
        MarshalByte.writeShortAsPacked(buffer, position, etaAa2oAssto, Len.Int.ETA_AA2O_ASSTO, 0);
        position += Len.ETA_AA2O_ASSTO;
        MarshalByte.writeShortAsPacked(buffer, position, etaMm2oAssto, Len.Int.ETA_MM2O_ASSTO, 0);
        position += Len.ETA_MM2O_ASSTO;
        MarshalByte.writeShortAsPacked(buffer, position, etaAa3oAssto, Len.Int.ETA_AA3O_ASSTO, 0);
        position += Len.ETA_AA3O_ASSTO;
        MarshalByte.writeShortAsPacked(buffer, position, etaMm3oAssto, Len.Int.ETA_MM3O_ASSTO, 0);
        position += Len.ETA_MM3O_ASSTO;
        MarshalByte.writeDecimalAsPacked(buffer, position, rendtoLrd.copy());
        position += Len.RENDTO_LRD;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcRetr.copy());
        position += Len.PC_RETR;
        MarshalByte.writeDecimalAsPacked(buffer, position, rendtoRetr.copy());
        position += Len.RENDTO_RETR;
        MarshalByte.writeDecimalAsPacked(buffer, position, minGarto.copy());
        position += Len.MIN_GARTO;
        MarshalByte.writeDecimalAsPacked(buffer, position, minTrnut.copy());
        position += Len.MIN_TRNUT;
        MarshalByte.writeDecimalAsPacked(buffer, position, preAttDiTrch.copy());
        position += Len.PRE_ATT_DI_TRCH;
        MarshalByte.writeDecimalAsPacked(buffer, position, matuEnd2000.copy());
        position += Len.MATU_END2000;
        MarshalByte.writeDecimalAsPacked(buffer, position, abbTotIni.copy());
        position += Len.ABB_TOT_INI;
        MarshalByte.writeDecimalAsPacked(buffer, position, abbTotUlt.copy());
        position += Len.ABB_TOT_ULT;
        MarshalByte.writeDecimalAsPacked(buffer, position, abbAnnuUlt.copy());
        position += Len.ABB_ANNU_ULT;
        MarshalByte.writeIntAsPacked(buffer, position, durAbb, Len.Int.DUR_ABB, 0);
        position += Len.DUR_ABB;
        MarshalByte.writeChar(buffer, position, tpAdegAbb);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, modCalc, Len.MOD_CALC);
        position += Len.MOD_CALC;
        MarshalByte.writeDecimalAsPacked(buffer, position, impAz.copy());
        position += Len.IMP_AZ;
        MarshalByte.writeDecimalAsPacked(buffer, position, impAder.copy());
        position += Len.IMP_ADER;
        MarshalByte.writeDecimalAsPacked(buffer, position, impTfr.copy());
        position += Len.IMP_TFR;
        MarshalByte.writeDecimalAsPacked(buffer, position, impVolo.copy());
        position += Len.IMP_VOLO;
        MarshalByte.writeDecimalAsPacked(buffer, position, visEnd2000.copy());
        position += Len.VIS_END2000;
        MarshalByte.writeIntAsPacked(buffer, position, dtVldtProd, Len.Int.DT_VLDT_PROD, 0);
        position += Len.DT_VLDT_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniValTar, Len.Int.DT_INI_VAL_TAR, 0);
        position += Len.DT_INI_VAL_TAR;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbVisEnd2000.copy());
        position += Len.IMPB_VIS_END2000;
        MarshalByte.writeDecimalAsPacked(buffer, position, renIniTsTec0.copy());
        position += Len.REN_INI_TS_TEC0;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcRipPre.copy());
        position += Len.PC_RIP_PRE;
        MarshalByte.writeChar(buffer, position, flImportiForz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, prstzIniNforz.copy());
        position += Len.PRSTZ_INI_NFORZ;
        MarshalByte.writeDecimalAsPacked(buffer, position, visEnd2000Nforz.copy());
        position += Len.VIS_END2000_NFORZ;
        MarshalByte.writeDecimalAsPacked(buffer, position, intrMora.copy());
        position += Len.INTR_MORA;
        MarshalByte.writeDecimalAsPacked(buffer, position, manfeeAntic.copy());
        position += Len.MANFEE_ANTIC;
        MarshalByte.writeDecimalAsPacked(buffer, position, manfeeRicor.copy());
        position += Len.MANFEE_RICOR;
        MarshalByte.writeDecimalAsPacked(buffer, position, preUniRivto.copy());
        position += Len.PRE_UNI_RIVTO;
        MarshalByte.writeDecimalAsPacked(buffer, position, prov1aaAcq.copy());
        position += Len.PROV1AA_ACQ;
        MarshalByte.writeDecimalAsPacked(buffer, position, prov2aaAcq.copy());
        position += Len.PROV2AA_ACQ;
        MarshalByte.writeDecimalAsPacked(buffer, position, provRicor.copy());
        position += Len.PROV_RICOR;
        MarshalByte.writeDecimalAsPacked(buffer, position, provInc.copy());
        position += Len.PROV_INC;
        MarshalByte.writeDecimalAsPacked(buffer, position, alqProvAcq.copy());
        position += Len.ALQ_PROV_ACQ;
        MarshalByte.writeDecimalAsPacked(buffer, position, alqProvInc.copy());
        position += Len.ALQ_PROV_INC;
        MarshalByte.writeDecimalAsPacked(buffer, position, alqProvRicor.copy());
        position += Len.ALQ_PROV_RICOR;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbProvAcq.copy());
        position += Len.IMPB_PROV_ACQ;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbProvInc.copy());
        position += Len.IMPB_PROV_INC;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbProvRicor.copy());
        position += Len.IMPB_PROV_RICOR;
        MarshalByte.writeChar(buffer, position, flProvForz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, prstzAggIni.copy());
        position += Len.PRSTZ_AGG_INI;
        MarshalByte.writeDecimalAsPacked(buffer, position, incrPre.copy());
        position += Len.INCR_PRE;
        MarshalByte.writeDecimalAsPacked(buffer, position, incrPrstz.copy());
        position += Len.INCR_PRSTZ;
        MarshalByte.writeIntAsPacked(buffer, position, dtUltAdegPrePr, Len.Int.DT_ULT_ADEG_PRE_PR, 0);
        position += Len.DT_ULT_ADEG_PRE_PR;
        MarshalByte.writeDecimalAsPacked(buffer, position, prstzAggUlt.copy());
        position += Len.PRSTZ_AGG_ULT;
        MarshalByte.writeDecimalAsPacked(buffer, position, tsRivalNet.copy());
        position += Len.TS_RIVAL_NET;
        MarshalByte.writeDecimalAsPacked(buffer, position, prePattuito.copy());
        position += Len.PRE_PATTUITO;
        MarshalByte.writeString(buffer, position, tpRival, Len.TP_RIVAL);
        position += Len.TP_RIVAL;
        MarshalByte.writeDecimalAsPacked(buffer, position, risMat.copy());
        position += Len.RIS_MAT;
        MarshalByte.writeDecimalAsPacked(buffer, position, cptMinScad.copy());
        position += Len.CPT_MIN_SCAD;
        MarshalByte.writeDecimalAsPacked(buffer, position, commisGest.copy());
        position += Len.COMMIS_GEST;
        MarshalByte.writeString(buffer, position, tpManfeeAppl, Len.TP_MANFEE_APPL);
        position += Len.TP_MANFEE_APPL;
        MarshalByte.writeLongAsPacked(buffer, position, dsRiga, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcCommisGest.copy());
        return buffer;
    }

    public void setIdTrchDiGar(int idTrchDiGar) {
        this.idTrchDiGar = idTrchDiGar;
    }

    public int getIdTrchDiGar() {
        return this.idTrchDiGar;
    }

    public void setIdGar(int idGar) {
        this.idGar = idGar;
    }

    public int getIdGar() {
        return this.idGar;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setIdMoviChiu(int idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public int getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtIniEff(int dtIniEff) {
        this.dtIniEff = dtIniEff;
    }

    public int getDtIniEff() {
        return this.dtIniEff;
    }

    public void setDtEndEff(int dtEndEff) {
        this.dtEndEff = dtEndEff;
    }

    public int getDtEndEff() {
        return this.dtEndEff;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setDtDecor(int dtDecor) {
        this.dtDecor = dtDecor;
    }

    public int getDtDecor() {
        return this.dtDecor;
    }

    public void setDtScad(int dtScad) {
        this.dtScad = dtScad;
    }

    public int getDtScad() {
        return this.dtScad;
    }

    public void setIbOgg(String ibOgg) {
        this.ibOgg = Functions.subString(ibOgg, Len.IB_OGG);
    }

    public String getIbOgg() {
        return this.ibOgg;
    }

    public void setTpRgmFisc(String tpRgmFisc) {
        this.tpRgmFisc = Functions.subString(tpRgmFisc, Len.TP_RGM_FISC);
    }

    public String getTpRgmFisc() {
        return this.tpRgmFisc;
    }

    public void setDtEmis(int dtEmis) {
        this.dtEmis = dtEmis;
    }

    public int getDtEmis() {
        return this.dtEmis;
    }

    public void setTpTrch(String tpTrch) {
        this.tpTrch = Functions.subString(tpTrch, Len.TP_TRCH);
    }

    public String getTpTrch() {
        return this.tpTrch;
    }

    public void setDurAa(int durAa) {
        this.durAa = durAa;
    }

    public int getDurAa() {
        return this.durAa;
    }

    public void setDurMm(int durMm) {
        this.durMm = durMm;
    }

    public int getDurMm() {
        return this.durMm;
    }

    public void setDurGg(int durGg) {
        this.durGg = durGg;
    }

    public int getDurGg() {
        return this.durGg;
    }

    public void setPreCasoMor(AfDecimal preCasoMor) {
        this.preCasoMor.assign(preCasoMor);
    }

    public AfDecimal getPreCasoMor() {
        return this.preCasoMor.copy();
    }

    public void setPcIntrRiat(AfDecimal pcIntrRiat) {
        this.pcIntrRiat.assign(pcIntrRiat);
    }

    public AfDecimal getPcIntrRiat() {
        return this.pcIntrRiat.copy();
    }

    public void setImpBnsAntic(AfDecimal impBnsAntic) {
        this.impBnsAntic.assign(impBnsAntic);
    }

    public AfDecimal getImpBnsAntic() {
        return this.impBnsAntic.copy();
    }

    public void setPreIniNet(AfDecimal preIniNet) {
        this.preIniNet.assign(preIniNet);
    }

    public AfDecimal getPreIniNet() {
        return this.preIniNet.copy();
    }

    public void setPrePpIni(AfDecimal prePpIni) {
        this.prePpIni.assign(prePpIni);
    }

    public AfDecimal getPrePpIni() {
        return this.prePpIni.copy();
    }

    public void setPrePpUlt(AfDecimal prePpUlt) {
        this.prePpUlt.assign(prePpUlt);
    }

    public AfDecimal getPrePpUlt() {
        return this.prePpUlt.copy();
    }

    public void setPreTariIni(AfDecimal preTariIni) {
        this.preTariIni.assign(preTariIni);
    }

    public AfDecimal getPreTariIni() {
        return this.preTariIni.copy();
    }

    public void setPreTariUlt(AfDecimal preTariUlt) {
        this.preTariUlt.assign(preTariUlt);
    }

    public AfDecimal getPreTariUlt() {
        return this.preTariUlt.copy();
    }

    public void setPreInvrioIni(AfDecimal preInvrioIni) {
        this.preInvrioIni.assign(preInvrioIni);
    }

    public AfDecimal getPreInvrioIni() {
        return this.preInvrioIni.copy();
    }

    public void setPreInvrioUlt(AfDecimal preInvrioUlt) {
        this.preInvrioUlt.assign(preInvrioUlt);
    }

    public AfDecimal getPreInvrioUlt() {
        return this.preInvrioUlt.copy();
    }

    public void setPreRivto(AfDecimal preRivto) {
        this.preRivto.assign(preRivto);
    }

    public AfDecimal getPreRivto() {
        return this.preRivto.copy();
    }

    public void setImpSoprProf(AfDecimal impSoprProf) {
        this.impSoprProf.assign(impSoprProf);
    }

    public AfDecimal getImpSoprProf() {
        return this.impSoprProf.copy();
    }

    public void setImpSoprSan(AfDecimal impSoprSan) {
        this.impSoprSan.assign(impSoprSan);
    }

    public AfDecimal getImpSoprSan() {
        return this.impSoprSan.copy();
    }

    public void setImpSoprSpo(AfDecimal impSoprSpo) {
        this.impSoprSpo.assign(impSoprSpo);
    }

    public AfDecimal getImpSoprSpo() {
        return this.impSoprSpo.copy();
    }

    public void setImpSoprTec(AfDecimal impSoprTec) {
        this.impSoprTec.assign(impSoprTec);
    }

    public AfDecimal getImpSoprTec() {
        return this.impSoprTec.copy();
    }

    public void setImpAltSopr(AfDecimal impAltSopr) {
        this.impAltSopr.assign(impAltSopr);
    }

    public AfDecimal getImpAltSopr() {
        return this.impAltSopr.copy();
    }

    public void setPreStab(AfDecimal preStab) {
        this.preStab.assign(preStab);
    }

    public AfDecimal getPreStab() {
        return this.preStab.copy();
    }

    public void setDtEffStab(int dtEffStab) {
        this.dtEffStab = dtEffStab;
    }

    public int getDtEffStab() {
        return this.dtEffStab;
    }

    public void setTsRivalFis(AfDecimal tsRivalFis) {
        this.tsRivalFis.assign(tsRivalFis);
    }

    public AfDecimal getTsRivalFis() {
        return this.tsRivalFis.copy();
    }

    public void setTsRivalIndiciz(AfDecimal tsRivalIndiciz) {
        this.tsRivalIndiciz.assign(tsRivalIndiciz);
    }

    public AfDecimal getTsRivalIndiciz() {
        return this.tsRivalIndiciz.copy();
    }

    public void setOldTsTec(AfDecimal oldTsTec) {
        this.oldTsTec.assign(oldTsTec);
    }

    public AfDecimal getOldTsTec() {
        return this.oldTsTec.copy();
    }

    public void setRatLrd(AfDecimal ratLrd) {
        this.ratLrd.assign(ratLrd);
    }

    public AfDecimal getRatLrd() {
        return this.ratLrd.copy();
    }

    public void setPreLrd(AfDecimal preLrd) {
        this.preLrd.assign(preLrd);
    }

    public AfDecimal getPreLrd() {
        return this.preLrd.copy();
    }

    public void setPrstzIni(AfDecimal prstzIni) {
        this.prstzIni.assign(prstzIni);
    }

    public AfDecimal getPrstzIni() {
        return this.prstzIni.copy();
    }

    public void setPrstzUlt(AfDecimal prstzUlt) {
        this.prstzUlt.assign(prstzUlt);
    }

    public AfDecimal getPrstzUlt() {
        return this.prstzUlt.copy();
    }

    public void setCptInOpzRivto(AfDecimal cptInOpzRivto) {
        this.cptInOpzRivto.assign(cptInOpzRivto);
    }

    public AfDecimal getCptInOpzRivto() {
        return this.cptInOpzRivto.copy();
    }

    public void setPrstzIniStab(AfDecimal prstzIniStab) {
        this.prstzIniStab.assign(prstzIniStab);
    }

    public AfDecimal getPrstzIniStab() {
        return this.prstzIniStab.copy();
    }

    public void setCptRshMor(AfDecimal cptRshMor) {
        this.cptRshMor.assign(cptRshMor);
    }

    public AfDecimal getCptRshMor() {
        return this.cptRshMor.copy();
    }

    public void setPrstzRidIni(AfDecimal prstzRidIni) {
        this.prstzRidIni.assign(prstzRidIni);
    }

    public AfDecimal getPrstzRidIni() {
        return this.prstzRidIni.copy();
    }

    public void setFlCarCont(char flCarCont) {
        this.flCarCont = flCarCont;
    }

    public char getFlCarCont() {
        return this.flCarCont;
    }

    public void setBnsGiaLiqto(AfDecimal bnsGiaLiqto) {
        this.bnsGiaLiqto.assign(bnsGiaLiqto);
    }

    public AfDecimal getBnsGiaLiqto() {
        return this.bnsGiaLiqto.copy();
    }

    public void setImpBns(AfDecimal impBns) {
        this.impBns.assign(impBns);
    }

    public AfDecimal getImpBns() {
        return this.impBns.copy();
    }

    public void setCodDvs(String codDvs) {
        this.codDvs = Functions.subString(codDvs, Len.COD_DVS);
    }

    public String getCodDvs() {
        return this.codDvs;
    }

    public void setPrstzIniNewfis(AfDecimal prstzIniNewfis) {
        this.prstzIniNewfis.assign(prstzIniNewfis);
    }

    public AfDecimal getPrstzIniNewfis() {
        return this.prstzIniNewfis.copy();
    }

    public void setImpScon(AfDecimal impScon) {
        this.impScon.assign(impScon);
    }

    public AfDecimal getImpScon() {
        return this.impScon.copy();
    }

    public void setAlqScon(AfDecimal alqScon) {
        this.alqScon.assign(alqScon);
    }

    public AfDecimal getAlqScon() {
        return this.alqScon.copy();
    }

    public void setImpCarAcq(AfDecimal impCarAcq) {
        this.impCarAcq.assign(impCarAcq);
    }

    public AfDecimal getImpCarAcq() {
        return this.impCarAcq.copy();
    }

    public void setImpCarInc(AfDecimal impCarInc) {
        this.impCarInc.assign(impCarInc);
    }

    public AfDecimal getImpCarInc() {
        return this.impCarInc.copy();
    }

    public void setImpCarGest(AfDecimal impCarGest) {
        this.impCarGest.assign(impCarGest);
    }

    public AfDecimal getImpCarGest() {
        return this.impCarGest.copy();
    }

    public void setEtaAa1oAssto(short etaAa1oAssto) {
        this.etaAa1oAssto = etaAa1oAssto;
    }

    public short getEtaAa1oAssto() {
        return this.etaAa1oAssto;
    }

    public void setEtaMm1oAssto(short etaMm1oAssto) {
        this.etaMm1oAssto = etaMm1oAssto;
    }

    public short getEtaMm1oAssto() {
        return this.etaMm1oAssto;
    }

    public void setEtaAa2oAssto(short etaAa2oAssto) {
        this.etaAa2oAssto = etaAa2oAssto;
    }

    public short getEtaAa2oAssto() {
        return this.etaAa2oAssto;
    }

    public void setEtaMm2oAssto(short etaMm2oAssto) {
        this.etaMm2oAssto = etaMm2oAssto;
    }

    public short getEtaMm2oAssto() {
        return this.etaMm2oAssto;
    }

    public void setEtaAa3oAssto(short etaAa3oAssto) {
        this.etaAa3oAssto = etaAa3oAssto;
    }

    public short getEtaAa3oAssto() {
        return this.etaAa3oAssto;
    }

    public void setEtaMm3oAssto(short etaMm3oAssto) {
        this.etaMm3oAssto = etaMm3oAssto;
    }

    public short getEtaMm3oAssto() {
        return this.etaMm3oAssto;
    }

    public void setRendtoLrd(AfDecimal rendtoLrd) {
        this.rendtoLrd.assign(rendtoLrd);
    }

    public AfDecimal getRendtoLrd() {
        return this.rendtoLrd.copy();
    }

    public void setPcRetr(AfDecimal pcRetr) {
        this.pcRetr.assign(pcRetr);
    }

    public AfDecimal getPcRetr() {
        return this.pcRetr.copy();
    }

    public void setRendtoRetr(AfDecimal rendtoRetr) {
        this.rendtoRetr.assign(rendtoRetr);
    }

    public AfDecimal getRendtoRetr() {
        return this.rendtoRetr.copy();
    }

    public void setMinGarto(AfDecimal minGarto) {
        this.minGarto.assign(minGarto);
    }

    public AfDecimal getMinGarto() {
        return this.minGarto.copy();
    }

    public void setMinTrnut(AfDecimal minTrnut) {
        this.minTrnut.assign(minTrnut);
    }

    public AfDecimal getMinTrnut() {
        return this.minTrnut.copy();
    }

    public void setPreAttDiTrch(AfDecimal preAttDiTrch) {
        this.preAttDiTrch.assign(preAttDiTrch);
    }

    public AfDecimal getPreAttDiTrch() {
        return this.preAttDiTrch.copy();
    }

    public void setMatuEnd2000(AfDecimal matuEnd2000) {
        this.matuEnd2000.assign(matuEnd2000);
    }

    public AfDecimal getMatuEnd2000() {
        return this.matuEnd2000.copy();
    }

    public void setAbbTotIni(AfDecimal abbTotIni) {
        this.abbTotIni.assign(abbTotIni);
    }

    public AfDecimal getAbbTotIni() {
        return this.abbTotIni.copy();
    }

    public void setAbbTotUlt(AfDecimal abbTotUlt) {
        this.abbTotUlt.assign(abbTotUlt);
    }

    public AfDecimal getAbbTotUlt() {
        return this.abbTotUlt.copy();
    }

    public void setAbbAnnuUlt(AfDecimal abbAnnuUlt) {
        this.abbAnnuUlt.assign(abbAnnuUlt);
    }

    public AfDecimal getAbbAnnuUlt() {
        return this.abbAnnuUlt.copy();
    }

    public void setDurAbb(int durAbb) {
        this.durAbb = durAbb;
    }

    public int getDurAbb() {
        return this.durAbb;
    }

    public void setTpAdegAbb(char tpAdegAbb) {
        this.tpAdegAbb = tpAdegAbb;
    }

    public char getTpAdegAbb() {
        return this.tpAdegAbb;
    }

    public void setModCalc(String modCalc) {
        this.modCalc = Functions.subString(modCalc, Len.MOD_CALC);
    }

    public String getModCalc() {
        return this.modCalc;
    }

    public void setImpAz(AfDecimal impAz) {
        this.impAz.assign(impAz);
    }

    public AfDecimal getImpAz() {
        return this.impAz.copy();
    }

    public void setImpAder(AfDecimal impAder) {
        this.impAder.assign(impAder);
    }

    public AfDecimal getImpAder() {
        return this.impAder.copy();
    }

    public void setImpTfr(AfDecimal impTfr) {
        this.impTfr.assign(impTfr);
    }

    public AfDecimal getImpTfr() {
        return this.impTfr.copy();
    }

    public void setImpVolo(AfDecimal impVolo) {
        this.impVolo.assign(impVolo);
    }

    public AfDecimal getImpVolo() {
        return this.impVolo.copy();
    }

    public void setVisEnd2000(AfDecimal visEnd2000) {
        this.visEnd2000.assign(visEnd2000);
    }

    public AfDecimal getVisEnd2000() {
        return this.visEnd2000.copy();
    }

    public void setDtVldtProd(int dtVldtProd) {
        this.dtVldtProd = dtVldtProd;
    }

    public int getDtVldtProd() {
        return this.dtVldtProd;
    }

    public void setDtIniValTar(int dtIniValTar) {
        this.dtIniValTar = dtIniValTar;
    }

    public int getDtIniValTar() {
        return this.dtIniValTar;
    }

    public void setImpbVisEnd2000(AfDecimal impbVisEnd2000) {
        this.impbVisEnd2000.assign(impbVisEnd2000);
    }

    public AfDecimal getImpbVisEnd2000() {
        return this.impbVisEnd2000.copy();
    }

    public void setRenIniTsTec0(AfDecimal renIniTsTec0) {
        this.renIniTsTec0.assign(renIniTsTec0);
    }

    public AfDecimal getRenIniTsTec0() {
        return this.renIniTsTec0.copy();
    }

    public void setPcRipPre(AfDecimal pcRipPre) {
        this.pcRipPre.assign(pcRipPre);
    }

    public AfDecimal getPcRipPre() {
        return this.pcRipPre.copy();
    }

    public void setFlImportiForz(char flImportiForz) {
        this.flImportiForz = flImportiForz;
    }

    public char getFlImportiForz() {
        return this.flImportiForz;
    }

    public void setPrstzIniNforz(AfDecimal prstzIniNforz) {
        this.prstzIniNforz.assign(prstzIniNforz);
    }

    public AfDecimal getPrstzIniNforz() {
        return this.prstzIniNforz.copy();
    }

    public void setVisEnd2000Nforz(AfDecimal visEnd2000Nforz) {
        this.visEnd2000Nforz.assign(visEnd2000Nforz);
    }

    public AfDecimal getVisEnd2000Nforz() {
        return this.visEnd2000Nforz.copy();
    }

    public void setIntrMora(AfDecimal intrMora) {
        this.intrMora.assign(intrMora);
    }

    public AfDecimal getIntrMora() {
        return this.intrMora.copy();
    }

    public void setManfeeAntic(AfDecimal manfeeAntic) {
        this.manfeeAntic.assign(manfeeAntic);
    }

    public AfDecimal getManfeeAntic() {
        return this.manfeeAntic.copy();
    }

    public void setManfeeRicor(AfDecimal manfeeRicor) {
        this.manfeeRicor.assign(manfeeRicor);
    }

    public AfDecimal getManfeeRicor() {
        return this.manfeeRicor.copy();
    }

    public void setPreUniRivto(AfDecimal preUniRivto) {
        this.preUniRivto.assign(preUniRivto);
    }

    public AfDecimal getPreUniRivto() {
        return this.preUniRivto.copy();
    }

    public void setProv1aaAcq(AfDecimal prov1aaAcq) {
        this.prov1aaAcq.assign(prov1aaAcq);
    }

    public AfDecimal getProv1aaAcq() {
        return this.prov1aaAcq.copy();
    }

    public void setProv2aaAcq(AfDecimal prov2aaAcq) {
        this.prov2aaAcq.assign(prov2aaAcq);
    }

    public AfDecimal getProv2aaAcq() {
        return this.prov2aaAcq.copy();
    }

    public void setProvRicor(AfDecimal provRicor) {
        this.provRicor.assign(provRicor);
    }

    public AfDecimal getProvRicor() {
        return this.provRicor.copy();
    }

    public void setProvInc(AfDecimal provInc) {
        this.provInc.assign(provInc);
    }

    public AfDecimal getProvInc() {
        return this.provInc.copy();
    }

    public void setAlqProvAcq(AfDecimal alqProvAcq) {
        this.alqProvAcq.assign(alqProvAcq);
    }

    public AfDecimal getAlqProvAcq() {
        return this.alqProvAcq.copy();
    }

    public void setAlqProvInc(AfDecimal alqProvInc) {
        this.alqProvInc.assign(alqProvInc);
    }

    public AfDecimal getAlqProvInc() {
        return this.alqProvInc.copy();
    }

    public void setAlqProvRicor(AfDecimal alqProvRicor) {
        this.alqProvRicor.assign(alqProvRicor);
    }

    public AfDecimal getAlqProvRicor() {
        return this.alqProvRicor.copy();
    }

    public void setImpbProvAcq(AfDecimal impbProvAcq) {
        this.impbProvAcq.assign(impbProvAcq);
    }

    public AfDecimal getImpbProvAcq() {
        return this.impbProvAcq.copy();
    }

    public void setImpbProvInc(AfDecimal impbProvInc) {
        this.impbProvInc.assign(impbProvInc);
    }

    public AfDecimal getImpbProvInc() {
        return this.impbProvInc.copy();
    }

    public void setImpbProvRicor(AfDecimal impbProvRicor) {
        this.impbProvRicor.assign(impbProvRicor);
    }

    public AfDecimal getImpbProvRicor() {
        return this.impbProvRicor.copy();
    }

    public void setFlProvForz(char flProvForz) {
        this.flProvForz = flProvForz;
    }

    public char getFlProvForz() {
        return this.flProvForz;
    }

    public void setPrstzAggIni(AfDecimal prstzAggIni) {
        this.prstzAggIni.assign(prstzAggIni);
    }

    public AfDecimal getPrstzAggIni() {
        return this.prstzAggIni.copy();
    }

    public void setIncrPre(AfDecimal incrPre) {
        this.incrPre.assign(incrPre);
    }

    public AfDecimal getIncrPre() {
        return this.incrPre.copy();
    }

    public void setIncrPrstz(AfDecimal incrPrstz) {
        this.incrPrstz.assign(incrPrstz);
    }

    public AfDecimal getIncrPrstz() {
        return this.incrPrstz.copy();
    }

    public void setDtUltAdegPrePr(int dtUltAdegPrePr) {
        this.dtUltAdegPrePr = dtUltAdegPrePr;
    }

    public int getDtUltAdegPrePr() {
        return this.dtUltAdegPrePr;
    }

    public void setPrstzAggUlt(AfDecimal prstzAggUlt) {
        this.prstzAggUlt.assign(prstzAggUlt);
    }

    public AfDecimal getPrstzAggUlt() {
        return this.prstzAggUlt.copy();
    }

    public void setTsRivalNet(AfDecimal tsRivalNet) {
        this.tsRivalNet.assign(tsRivalNet);
    }

    public AfDecimal getTsRivalNet() {
        return this.tsRivalNet.copy();
    }

    public void setPrePattuito(AfDecimal prePattuito) {
        this.prePattuito.assign(prePattuito);
    }

    public AfDecimal getPrePattuito() {
        return this.prePattuito.copy();
    }

    public void setTpRival(String tpRival) {
        this.tpRival = Functions.subString(tpRival, Len.TP_RIVAL);
    }

    public String getTpRival() {
        return this.tpRival;
    }

    public void setRisMat(AfDecimal risMat) {
        this.risMat.assign(risMat);
    }

    public AfDecimal getRisMat() {
        return this.risMat.copy();
    }

    public void setCptMinScad(AfDecimal cptMinScad) {
        this.cptMinScad.assign(cptMinScad);
    }

    public AfDecimal getCptMinScad() {
        return this.cptMinScad.copy();
    }

    public void setCommisGest(AfDecimal commisGest) {
        this.commisGest.assign(commisGest);
    }

    public AfDecimal getCommisGest() {
        return this.commisGest.copy();
    }

    public void setTpManfeeAppl(String tpManfeeAppl) {
        this.tpManfeeAppl = Functions.subString(tpManfeeAppl, Len.TP_MANFEE_APPL);
    }

    public String getTpManfeeAppl() {
        return this.tpManfeeAppl;
    }

    public void setDsRiga(long dsRiga) {
        this.dsRiga = dsRiga;
    }

    public long getDsRiga() {
        return this.dsRiga;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dsTsIniCptz = dsTsIniCptz;
    }

    public long getDsTsIniCptz() {
        return this.dsTsIniCptz;
    }

    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dsTsEndCptz = dsTsEndCptz;
    }

    public long getDsTsEndCptz() {
        return this.dsTsEndCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setPcCommisGest(AfDecimal pcCommisGest) {
        this.pcCommisGest.assign(pcCommisGest);
    }

    public AfDecimal getPcCommisGest() {
        return this.pcCommisGest.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_TRCH_DI_GAR = 5;
        public static final int ID_GAR = 5;
        public static final int ID_ADES = 5;
        public static final int ID_POLI = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int DT_DECOR = 5;
        public static final int DT_SCAD = 5;
        public static final int IB_OGG = 40;
        public static final int TP_RGM_FISC = 2;
        public static final int DT_EMIS = 5;
        public static final int TP_TRCH = 2;
        public static final int DUR_AA = 3;
        public static final int DUR_MM = 3;
        public static final int DUR_GG = 3;
        public static final int PRE_CASO_MOR = 8;
        public static final int PC_INTR_RIAT = 4;
        public static final int IMP_BNS_ANTIC = 8;
        public static final int PRE_INI_NET = 8;
        public static final int PRE_PP_INI = 8;
        public static final int PRE_PP_ULT = 8;
        public static final int PRE_TARI_INI = 8;
        public static final int PRE_TARI_ULT = 8;
        public static final int PRE_INVRIO_INI = 8;
        public static final int PRE_INVRIO_ULT = 8;
        public static final int PRE_RIVTO = 8;
        public static final int IMP_SOPR_PROF = 8;
        public static final int IMP_SOPR_SAN = 8;
        public static final int IMP_SOPR_SPO = 8;
        public static final int IMP_SOPR_TEC = 8;
        public static final int IMP_ALT_SOPR = 8;
        public static final int PRE_STAB = 8;
        public static final int DT_EFF_STAB = 5;
        public static final int TS_RIVAL_FIS = 8;
        public static final int TS_RIVAL_INDICIZ = 8;
        public static final int OLD_TS_TEC = 8;
        public static final int RAT_LRD = 8;
        public static final int PRE_LRD = 8;
        public static final int PRSTZ_INI = 8;
        public static final int PRSTZ_ULT = 8;
        public static final int CPT_IN_OPZ_RIVTO = 8;
        public static final int PRSTZ_INI_STAB = 8;
        public static final int CPT_RSH_MOR = 8;
        public static final int PRSTZ_RID_INI = 8;
        public static final int FL_CAR_CONT = 1;
        public static final int BNS_GIA_LIQTO = 8;
        public static final int IMP_BNS = 8;
        public static final int COD_DVS = 20;
        public static final int PRSTZ_INI_NEWFIS = 8;
        public static final int IMP_SCON = 8;
        public static final int ALQ_SCON = 4;
        public static final int IMP_CAR_ACQ = 8;
        public static final int IMP_CAR_INC = 8;
        public static final int IMP_CAR_GEST = 8;
        public static final int ETA_AA1O_ASSTO = 2;
        public static final int ETA_MM1O_ASSTO = 2;
        public static final int ETA_AA2O_ASSTO = 2;
        public static final int ETA_MM2O_ASSTO = 2;
        public static final int ETA_AA3O_ASSTO = 2;
        public static final int ETA_MM3O_ASSTO = 2;
        public static final int RENDTO_LRD = 8;
        public static final int PC_RETR = 4;
        public static final int RENDTO_RETR = 8;
        public static final int MIN_GARTO = 8;
        public static final int MIN_TRNUT = 8;
        public static final int PRE_ATT_DI_TRCH = 8;
        public static final int MATU_END2000 = 8;
        public static final int ABB_TOT_INI = 8;
        public static final int ABB_TOT_ULT = 8;
        public static final int ABB_ANNU_ULT = 8;
        public static final int DUR_ABB = 4;
        public static final int TP_ADEG_ABB = 1;
        public static final int MOD_CALC = 2;
        public static final int IMP_AZ = 8;
        public static final int IMP_ADER = 8;
        public static final int IMP_TFR = 8;
        public static final int IMP_VOLO = 8;
        public static final int VIS_END2000 = 8;
        public static final int DT_VLDT_PROD = 5;
        public static final int DT_INI_VAL_TAR = 5;
        public static final int IMPB_VIS_END2000 = 8;
        public static final int REN_INI_TS_TEC0 = 8;
        public static final int PC_RIP_PRE = 4;
        public static final int FL_IMPORTI_FORZ = 1;
        public static final int PRSTZ_INI_NFORZ = 8;
        public static final int VIS_END2000_NFORZ = 8;
        public static final int INTR_MORA = 8;
        public static final int MANFEE_ANTIC = 8;
        public static final int MANFEE_RICOR = 8;
        public static final int PRE_UNI_RIVTO = 8;
        public static final int PROV1AA_ACQ = 8;
        public static final int PROV2AA_ACQ = 8;
        public static final int PROV_RICOR = 8;
        public static final int PROV_INC = 8;
        public static final int ALQ_PROV_ACQ = 4;
        public static final int ALQ_PROV_INC = 4;
        public static final int ALQ_PROV_RICOR = 4;
        public static final int IMPB_PROV_ACQ = 8;
        public static final int IMPB_PROV_INC = 8;
        public static final int IMPB_PROV_RICOR = 8;
        public static final int FL_PROV_FORZ = 1;
        public static final int PRSTZ_AGG_INI = 8;
        public static final int INCR_PRE = 8;
        public static final int INCR_PRSTZ = 8;
        public static final int DT_ULT_ADEG_PRE_PR = 5;
        public static final int PRSTZ_AGG_ULT = 8;
        public static final int TS_RIVAL_NET = 8;
        public static final int PRE_PATTUITO = 8;
        public static final int TP_RIVAL = 2;
        public static final int RIS_MAT = 8;
        public static final int CPT_MIN_SCAD = 8;
        public static final int COMMIS_GEST = 8;
        public static final int TP_MANFEE_APPL = 2;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int PC_COMMIS_GEST = 4;
        public static final int LDBVC601_TGA_OUTPUT = ID_TRCH_DI_GAR + ID_GAR + ID_ADES + ID_POLI + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + DT_DECOR + DT_SCAD + IB_OGG + TP_RGM_FISC + DT_EMIS + TP_TRCH + DUR_AA + DUR_MM + DUR_GG + PRE_CASO_MOR + PC_INTR_RIAT + IMP_BNS_ANTIC + PRE_INI_NET + PRE_PP_INI + PRE_PP_ULT + PRE_TARI_INI + PRE_TARI_ULT + PRE_INVRIO_INI + PRE_INVRIO_ULT + PRE_RIVTO + IMP_SOPR_PROF + IMP_SOPR_SAN + IMP_SOPR_SPO + IMP_SOPR_TEC + IMP_ALT_SOPR + PRE_STAB + DT_EFF_STAB + TS_RIVAL_FIS + TS_RIVAL_INDICIZ + OLD_TS_TEC + RAT_LRD + PRE_LRD + PRSTZ_INI + PRSTZ_ULT + CPT_IN_OPZ_RIVTO + PRSTZ_INI_STAB + CPT_RSH_MOR + PRSTZ_RID_INI + FL_CAR_CONT + BNS_GIA_LIQTO + IMP_BNS + COD_DVS + PRSTZ_INI_NEWFIS + IMP_SCON + ALQ_SCON + IMP_CAR_ACQ + IMP_CAR_INC + IMP_CAR_GEST + ETA_AA1O_ASSTO + ETA_MM1O_ASSTO + ETA_AA2O_ASSTO + ETA_MM2O_ASSTO + ETA_AA3O_ASSTO + ETA_MM3O_ASSTO + RENDTO_LRD + PC_RETR + RENDTO_RETR + MIN_GARTO + MIN_TRNUT + PRE_ATT_DI_TRCH + MATU_END2000 + ABB_TOT_INI + ABB_TOT_ULT + ABB_ANNU_ULT + DUR_ABB + TP_ADEG_ABB + MOD_CALC + IMP_AZ + IMP_ADER + IMP_TFR + IMP_VOLO + VIS_END2000 + DT_VLDT_PROD + DT_INI_VAL_TAR + IMPB_VIS_END2000 + REN_INI_TS_TEC0 + PC_RIP_PRE + FL_IMPORTI_FORZ + PRSTZ_INI_NFORZ + VIS_END2000_NFORZ + INTR_MORA + MANFEE_ANTIC + MANFEE_RICOR + PRE_UNI_RIVTO + PROV1AA_ACQ + PROV2AA_ACQ + PROV_RICOR + PROV_INC + ALQ_PROV_ACQ + ALQ_PROV_INC + ALQ_PROV_RICOR + IMPB_PROV_ACQ + IMPB_PROV_INC + IMPB_PROV_RICOR + FL_PROV_FORZ + PRSTZ_AGG_INI + INCR_PRE + INCR_PRSTZ + DT_ULT_ADEG_PRE_PR + PRSTZ_AGG_ULT + TS_RIVAL_NET + PRE_PATTUITO + TP_RIVAL + RIS_MAT + CPT_MIN_SCAD + COMMIS_GEST + TP_MANFEE_APPL + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + PC_COMMIS_GEST;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_TRCH_DI_GAR = 9;
            public static final int ID_GAR = 9;
            public static final int ID_ADES = 9;
            public static final int ID_POLI = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_DECOR = 8;
            public static final int DT_SCAD = 8;
            public static final int DT_EMIS = 8;
            public static final int DUR_AA = 5;
            public static final int DUR_MM = 5;
            public static final int DUR_GG = 5;
            public static final int PRE_CASO_MOR = 12;
            public static final int PC_INTR_RIAT = 3;
            public static final int IMP_BNS_ANTIC = 12;
            public static final int PRE_INI_NET = 12;
            public static final int PRE_PP_INI = 12;
            public static final int PRE_PP_ULT = 12;
            public static final int PRE_TARI_INI = 12;
            public static final int PRE_TARI_ULT = 12;
            public static final int PRE_INVRIO_INI = 12;
            public static final int PRE_INVRIO_ULT = 12;
            public static final int PRE_RIVTO = 12;
            public static final int IMP_SOPR_PROF = 12;
            public static final int IMP_SOPR_SAN = 12;
            public static final int IMP_SOPR_SPO = 12;
            public static final int IMP_SOPR_TEC = 12;
            public static final int IMP_ALT_SOPR = 12;
            public static final int PRE_STAB = 12;
            public static final int DT_EFF_STAB = 8;
            public static final int TS_RIVAL_FIS = 5;
            public static final int TS_RIVAL_INDICIZ = 5;
            public static final int OLD_TS_TEC = 5;
            public static final int RAT_LRD = 12;
            public static final int PRE_LRD = 12;
            public static final int PRSTZ_INI = 12;
            public static final int PRSTZ_ULT = 12;
            public static final int CPT_IN_OPZ_RIVTO = 12;
            public static final int PRSTZ_INI_STAB = 12;
            public static final int CPT_RSH_MOR = 12;
            public static final int PRSTZ_RID_INI = 12;
            public static final int BNS_GIA_LIQTO = 12;
            public static final int IMP_BNS = 12;
            public static final int PRSTZ_INI_NEWFIS = 12;
            public static final int IMP_SCON = 12;
            public static final int ALQ_SCON = 3;
            public static final int IMP_CAR_ACQ = 12;
            public static final int IMP_CAR_INC = 12;
            public static final int IMP_CAR_GEST = 12;
            public static final int ETA_AA1O_ASSTO = 3;
            public static final int ETA_MM1O_ASSTO = 3;
            public static final int ETA_AA2O_ASSTO = 3;
            public static final int ETA_MM2O_ASSTO = 3;
            public static final int ETA_AA3O_ASSTO = 3;
            public static final int ETA_MM3O_ASSTO = 3;
            public static final int RENDTO_LRD = 5;
            public static final int PC_RETR = 3;
            public static final int RENDTO_RETR = 5;
            public static final int MIN_GARTO = 5;
            public static final int MIN_TRNUT = 5;
            public static final int PRE_ATT_DI_TRCH = 12;
            public static final int MATU_END2000 = 12;
            public static final int ABB_TOT_INI = 12;
            public static final int ABB_TOT_ULT = 12;
            public static final int ABB_ANNU_ULT = 12;
            public static final int DUR_ABB = 6;
            public static final int IMP_AZ = 12;
            public static final int IMP_ADER = 12;
            public static final int IMP_TFR = 12;
            public static final int IMP_VOLO = 12;
            public static final int VIS_END2000 = 12;
            public static final int DT_VLDT_PROD = 8;
            public static final int DT_INI_VAL_TAR = 8;
            public static final int IMPB_VIS_END2000 = 12;
            public static final int REN_INI_TS_TEC0 = 12;
            public static final int PC_RIP_PRE = 3;
            public static final int PRSTZ_INI_NFORZ = 12;
            public static final int VIS_END2000_NFORZ = 12;
            public static final int INTR_MORA = 12;
            public static final int MANFEE_ANTIC = 12;
            public static final int MANFEE_RICOR = 12;
            public static final int PRE_UNI_RIVTO = 12;
            public static final int PROV1AA_ACQ = 12;
            public static final int PROV2AA_ACQ = 12;
            public static final int PROV_RICOR = 12;
            public static final int PROV_INC = 12;
            public static final int ALQ_PROV_ACQ = 3;
            public static final int ALQ_PROV_INC = 3;
            public static final int ALQ_PROV_RICOR = 3;
            public static final int IMPB_PROV_ACQ = 12;
            public static final int IMPB_PROV_INC = 12;
            public static final int IMPB_PROV_RICOR = 12;
            public static final int PRSTZ_AGG_INI = 12;
            public static final int INCR_PRE = 12;
            public static final int INCR_PRSTZ = 12;
            public static final int DT_ULT_ADEG_PRE_PR = 8;
            public static final int PRSTZ_AGG_ULT = 12;
            public static final int TS_RIVAL_NET = 5;
            public static final int PRE_PATTUITO = 12;
            public static final int RIS_MAT = 12;
            public static final int CPT_MIN_SCAD = 12;
            public static final int COMMIS_GEST = 12;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int PC_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_CASO_MOR = 3;
            public static final int PC_INTR_RIAT = 3;
            public static final int IMP_BNS_ANTIC = 3;
            public static final int PRE_INI_NET = 3;
            public static final int PRE_PP_INI = 3;
            public static final int PRE_PP_ULT = 3;
            public static final int PRE_TARI_INI = 3;
            public static final int PRE_TARI_ULT = 3;
            public static final int PRE_INVRIO_INI = 3;
            public static final int PRE_INVRIO_ULT = 3;
            public static final int PRE_RIVTO = 3;
            public static final int IMP_SOPR_PROF = 3;
            public static final int IMP_SOPR_SAN = 3;
            public static final int IMP_SOPR_SPO = 3;
            public static final int IMP_SOPR_TEC = 3;
            public static final int IMP_ALT_SOPR = 3;
            public static final int PRE_STAB = 3;
            public static final int TS_RIVAL_FIS = 9;
            public static final int TS_RIVAL_INDICIZ = 9;
            public static final int OLD_TS_TEC = 9;
            public static final int RAT_LRD = 3;
            public static final int PRE_LRD = 3;
            public static final int PRSTZ_INI = 3;
            public static final int PRSTZ_ULT = 3;
            public static final int CPT_IN_OPZ_RIVTO = 3;
            public static final int PRSTZ_INI_STAB = 3;
            public static final int CPT_RSH_MOR = 3;
            public static final int PRSTZ_RID_INI = 3;
            public static final int BNS_GIA_LIQTO = 3;
            public static final int IMP_BNS = 3;
            public static final int PRSTZ_INI_NEWFIS = 3;
            public static final int IMP_SCON = 3;
            public static final int ALQ_SCON = 3;
            public static final int IMP_CAR_ACQ = 3;
            public static final int IMP_CAR_INC = 3;
            public static final int IMP_CAR_GEST = 3;
            public static final int RENDTO_LRD = 9;
            public static final int PC_RETR = 3;
            public static final int RENDTO_RETR = 9;
            public static final int MIN_GARTO = 9;
            public static final int MIN_TRNUT = 9;
            public static final int PRE_ATT_DI_TRCH = 3;
            public static final int MATU_END2000 = 3;
            public static final int ABB_TOT_INI = 3;
            public static final int ABB_TOT_ULT = 3;
            public static final int ABB_ANNU_ULT = 3;
            public static final int IMP_AZ = 3;
            public static final int IMP_ADER = 3;
            public static final int IMP_TFR = 3;
            public static final int IMP_VOLO = 3;
            public static final int VIS_END2000 = 3;
            public static final int IMPB_VIS_END2000 = 3;
            public static final int REN_INI_TS_TEC0 = 3;
            public static final int PC_RIP_PRE = 3;
            public static final int PRSTZ_INI_NFORZ = 3;
            public static final int VIS_END2000_NFORZ = 3;
            public static final int INTR_MORA = 3;
            public static final int MANFEE_ANTIC = 3;
            public static final int MANFEE_RICOR = 3;
            public static final int PRE_UNI_RIVTO = 3;
            public static final int PROV1AA_ACQ = 3;
            public static final int PROV2AA_ACQ = 3;
            public static final int PROV_RICOR = 3;
            public static final int PROV_INC = 3;
            public static final int ALQ_PROV_ACQ = 3;
            public static final int ALQ_PROV_INC = 3;
            public static final int ALQ_PROV_RICOR = 3;
            public static final int IMPB_PROV_ACQ = 3;
            public static final int IMPB_PROV_INC = 3;
            public static final int IMPB_PROV_RICOR = 3;
            public static final int PRSTZ_AGG_INI = 3;
            public static final int INCR_PRE = 3;
            public static final int INCR_PRSTZ = 3;
            public static final int PRSTZ_AGG_ULT = 3;
            public static final int TS_RIVAL_NET = 9;
            public static final int PRE_PATTUITO = 3;
            public static final int RIS_MAT = 3;
            public static final int CPT_MIN_SCAD = 3;
            public static final int COMMIS_GEST = 3;
            public static final int PC_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
