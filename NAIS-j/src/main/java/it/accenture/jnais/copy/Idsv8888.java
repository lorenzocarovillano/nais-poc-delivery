package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.enums.Idsv8888LivelloDebug;
import it.accenture.jnais.ws.ptr.Idsv8888AreaDisplay;
import it.accenture.jnais.ws.ptr.Idsv8888StrPerformanceDbg;

/**Original name: IDSV8888<br>
 * Variable: IDSV8888 from copybook IDSV8888<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv8888 {

    //==== PROPERTIES ====
    /**Original name: IDSV8888-LIVELLO-DEBUG<br>
	 * <pre>----------------------------------------------------------------*
	 *     CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
	 * ----------------------------------------------------------------*</pre>*/
    private Idsv8888LivelloDebug livelloDebug = new Idsv8888LivelloDebug();
    //Original name: IDSV8888-CALL-TIMESTAMP
    private String callTimestamp = "IJCSTMSP";
    //Original name: IDSV8888-DISPLAY-ADDRESS
    private int displayAddress = DefaultValues.BIN_INT_VAL;
    /**Original name: IDSV8888-STR-PERFORMANCE-DBG<br>
	 * <pre>-- IDENTIFICATIVO/FASE/NOME PGM/TIMESTAMP/INIZIO-FINE</pre>*/
    private Idsv8888StrPerformanceDbg strPerformanceDbg = new Idsv8888StrPerformanceDbg();
    //Original name: IDSV8888-AREA-DISPLAY
    private Idsv8888AreaDisplay areaDisplay = new Idsv8888AreaDisplay();

    //==== CONSTRUCTORS ====
    public Idsv8888() {
        init();
    }

    //==== METHODS ====
    public void init() {
        areaDisplay.setAreaDisplay("");
    }

    public String getCallTimestamp() {
        return this.callTimestamp;
    }

    public void setDisplayAddress(int displayAddress) {
        this.displayAddress = displayAddress;
    }

    public void setDisplayAddressFromBuffer(byte[] buffer) {
        displayAddress = MarshalByte.readBinaryInt(buffer, 1);
    }

    public int getDisplayAddress() {
        return this.displayAddress;
    }

    public String getDisplayAddressFormatted() {
        return "" + getDisplayAddress();
    }

    public Idsv8888AreaDisplay getAreaDisplay() {
        return areaDisplay;
    }

    public Idsv8888LivelloDebug getLivelloDebug() {
        return livelloDebug;
    }

    public Idsv8888StrPerformanceDbg getStrPerformanceDbg() {
        return strPerformanceDbg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 50;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
