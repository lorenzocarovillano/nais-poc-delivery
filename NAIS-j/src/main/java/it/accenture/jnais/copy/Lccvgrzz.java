package it.accenture.jnais.copy;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LCCVGRZZ<br>
 * Copybook: LCCVGRZZ from copybook LCCVGRZZ<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvgrzz {

    //==== PROPERTIES ====
    //Original name: WK-GRZ-MAX-B
    private short maxB = ((short)20);

    //==== METHODS ====
    public short getMaxB() {
        return this.maxB;
    }
}
