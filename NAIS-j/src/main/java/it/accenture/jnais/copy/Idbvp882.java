package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDBVP882<br>
 * Copybook: IDBVP882 from copybook IDBVP882<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvp882 {

    //==== PROPERTIES ====
    //Original name: IND-P88-ID-MOVI-CHIU
    private short p88IdMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P88-FL-ALL-FND
    private short p88FlAllFnd = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setP88IdMoviChiu(short p88IdMoviChiu) {
        this.p88IdMoviChiu = p88IdMoviChiu;
    }

    public short getP88IdMoviChiu() {
        return this.p88IdMoviChiu;
    }

    public void setP88FlAllFnd(short p88FlAllFnd) {
        this.p88FlAllFnd = p88FlAllFnd;
    }

    public short getP88FlAllFnd() {
        return this.p88FlAllFnd;
    }
}
