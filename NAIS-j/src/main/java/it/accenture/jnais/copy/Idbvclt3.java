package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVCLT3<br>
 * Copybook: IDBVCLT3 from copybook IDBVCLT3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvclt3 {

    //==== PROPERTIES ====
    //Original name: CLT-DT-INI-EFF-DB
    private String cltDtIniEffDb = DefaultValues.stringVal(Len.CLT_DT_INI_EFF_DB);
    //Original name: CLT-DT-END-EFF-DB
    private String cltDtEndEffDb = DefaultValues.stringVal(Len.CLT_DT_END_EFF_DB);

    //==== METHODS ====
    public void setCltDtIniEffDb(String cltDtIniEffDb) {
        this.cltDtIniEffDb = Functions.subString(cltDtIniEffDb, Len.CLT_DT_INI_EFF_DB);
    }

    public String getCltDtIniEffDb() {
        return this.cltDtIniEffDb;
    }

    public void setCltDtEndEffDb(String cltDtEndEffDb) {
        this.cltDtEndEffDb = Functions.subString(cltDtEndEffDb, Len.CLT_DT_END_EFF_DB);
    }

    public String getCltDtEndEffDb() {
        return this.cltDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CLT_DT_INI_EFF_DB = 10;
        public static final int CLT_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
