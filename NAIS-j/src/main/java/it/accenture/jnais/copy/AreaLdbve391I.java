package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: AREA-LDBVE391-I<br>
 * Variable: AREA-LDBVE391-I from copybook LDBVE391<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class AreaLdbve391I {

    //==== PROPERTIES ====
    //Original name: LDBVE391-COD-FISC
    private String codFisc = DefaultValues.stringVal(Len.COD_FISC);
    //Original name: LDBVE391-TP-CAUS-BOLLO-1
    private String tpCausBollo1 = DefaultValues.stringVal(Len.TP_CAUS_BOLLO1);
    //Original name: LDBVE391-TP-CAUS-BOLLO-2
    private String tpCausBollo2 = DefaultValues.stringVal(Len.TP_CAUS_BOLLO2);
    //Original name: LDBVE391-TP-CAUS-BOLLO-3
    private String tpCausBollo3 = DefaultValues.stringVal(Len.TP_CAUS_BOLLO3);
    //Original name: LDBVE391-TP-CAUS-BOLLO-4
    private String tpCausBollo4 = DefaultValues.stringVal(Len.TP_CAUS_BOLLO4);
    //Original name: LDBVE391-TP-CAUS-BOLLO-5
    private String tpCausBollo5 = DefaultValues.stringVal(Len.TP_CAUS_BOLLO5);
    //Original name: LDBVE391-TP-CAUS-BOLLO-6
    private String tpCausBollo6 = DefaultValues.stringVal(Len.TP_CAUS_BOLLO6);
    //Original name: LDBVE391-TP-CAUS-BOLLO-7
    private String tpCausBollo7 = DefaultValues.stringVal(Len.TP_CAUS_BOLLO7);
    //Original name: LDBVE391-DT-INI-CALC
    private int dtIniCalc = DefaultValues.INT_VAL;
    //Original name: LDBVE391-DT-END-CALC
    private int dtEndCalc = DefaultValues.INT_VAL;
    //Original name: LDBVE391-DT-INI-CALC-DB
    private String dtIniCalcDb = DefaultValues.stringVal(Len.DT_INI_CALC_DB);
    //Original name: LDBVE391-DT-END-CALC-DB
    private String dtEndCalcDb = DefaultValues.stringVal(Len.DT_END_CALC_DB);

    //==== METHODS ====
    public void setAreaLdbve391IBytes(byte[] buffer, int offset) {
        int position = offset;
        codFisc = MarshalByte.readString(buffer, position, Len.COD_FISC);
        position += Len.COD_FISC;
        tpCausBollo1 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BOLLO1);
        position += Len.TP_CAUS_BOLLO1;
        tpCausBollo2 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BOLLO2);
        position += Len.TP_CAUS_BOLLO2;
        tpCausBollo3 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BOLLO3);
        position += Len.TP_CAUS_BOLLO3;
        tpCausBollo4 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BOLLO4);
        position += Len.TP_CAUS_BOLLO4;
        tpCausBollo5 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BOLLO5);
        position += Len.TP_CAUS_BOLLO5;
        tpCausBollo6 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BOLLO6);
        position += Len.TP_CAUS_BOLLO6;
        tpCausBollo7 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BOLLO7);
        position += Len.TP_CAUS_BOLLO7;
        dtIniCalc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_CALC, 0);
        position += Len.DT_INI_CALC;
        dtEndCalc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_CALC, 0);
        position += Len.DT_END_CALC;
        dtIniCalcDb = MarshalByte.readString(buffer, position, Len.DT_INI_CALC_DB);
        position += Len.DT_INI_CALC_DB;
        dtEndCalcDb = MarshalByte.readString(buffer, position, Len.DT_END_CALC_DB);
    }

    public byte[] getAreaLdbve391IBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codFisc, Len.COD_FISC);
        position += Len.COD_FISC;
        MarshalByte.writeString(buffer, position, tpCausBollo1, Len.TP_CAUS_BOLLO1);
        position += Len.TP_CAUS_BOLLO1;
        MarshalByte.writeString(buffer, position, tpCausBollo2, Len.TP_CAUS_BOLLO2);
        position += Len.TP_CAUS_BOLLO2;
        MarshalByte.writeString(buffer, position, tpCausBollo3, Len.TP_CAUS_BOLLO3);
        position += Len.TP_CAUS_BOLLO3;
        MarshalByte.writeString(buffer, position, tpCausBollo4, Len.TP_CAUS_BOLLO4);
        position += Len.TP_CAUS_BOLLO4;
        MarshalByte.writeString(buffer, position, tpCausBollo5, Len.TP_CAUS_BOLLO5);
        position += Len.TP_CAUS_BOLLO5;
        MarshalByte.writeString(buffer, position, tpCausBollo6, Len.TP_CAUS_BOLLO6);
        position += Len.TP_CAUS_BOLLO6;
        MarshalByte.writeString(buffer, position, tpCausBollo7, Len.TP_CAUS_BOLLO7);
        position += Len.TP_CAUS_BOLLO7;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniCalc, Len.Int.DT_INI_CALC, 0);
        position += Len.DT_INI_CALC;
        MarshalByte.writeIntAsPacked(buffer, position, dtEndCalc, Len.Int.DT_END_CALC, 0);
        position += Len.DT_END_CALC;
        MarshalByte.writeString(buffer, position, dtIniCalcDb, Len.DT_INI_CALC_DB);
        position += Len.DT_INI_CALC_DB;
        MarshalByte.writeString(buffer, position, dtEndCalcDb, Len.DT_END_CALC_DB);
        return buffer;
    }

    public void setCodFisc(String codFisc) {
        this.codFisc = Functions.subString(codFisc, Len.COD_FISC);
    }

    public String getCodFisc() {
        return this.codFisc;
    }

    public void setTpCausBollo1(String tpCausBollo1) {
        this.tpCausBollo1 = Functions.subString(tpCausBollo1, Len.TP_CAUS_BOLLO1);
    }

    public String getTpCausBollo1() {
        return this.tpCausBollo1;
    }

    public void setTpCausBollo2(String tpCausBollo2) {
        this.tpCausBollo2 = Functions.subString(tpCausBollo2, Len.TP_CAUS_BOLLO2);
    }

    public String getTpCausBollo2() {
        return this.tpCausBollo2;
    }

    public void setTpCausBollo3(String tpCausBollo3) {
        this.tpCausBollo3 = Functions.subString(tpCausBollo3, Len.TP_CAUS_BOLLO3);
    }

    public String getTpCausBollo3() {
        return this.tpCausBollo3;
    }

    public void setTpCausBollo4(String tpCausBollo4) {
        this.tpCausBollo4 = Functions.subString(tpCausBollo4, Len.TP_CAUS_BOLLO4);
    }

    public String getTpCausBollo4() {
        return this.tpCausBollo4;
    }

    public void setTpCausBollo5(String tpCausBollo5) {
        this.tpCausBollo5 = Functions.subString(tpCausBollo5, Len.TP_CAUS_BOLLO5);
    }

    public String getTpCausBollo5() {
        return this.tpCausBollo5;
    }

    public void setTpCausBollo6(String tpCausBollo6) {
        this.tpCausBollo6 = Functions.subString(tpCausBollo6, Len.TP_CAUS_BOLLO6);
    }

    public String getTpCausBollo6() {
        return this.tpCausBollo6;
    }

    public void setTpCausBollo7(String tpCausBollo7) {
        this.tpCausBollo7 = Functions.subString(tpCausBollo7, Len.TP_CAUS_BOLLO7);
    }

    public String getTpCausBollo7() {
        return this.tpCausBollo7;
    }

    public void setDtIniCalc(int dtIniCalc) {
        this.dtIniCalc = dtIniCalc;
    }

    public int getDtIniCalc() {
        return this.dtIniCalc;
    }

    public void setDtEndCalc(int dtEndCalc) {
        this.dtEndCalc = dtEndCalc;
    }

    public int getDtEndCalc() {
        return this.dtEndCalc;
    }

    public void setDtIniCalcDb(String dtIniCalcDb) {
        this.dtIniCalcDb = Functions.subString(dtIniCalcDb, Len.DT_INI_CALC_DB);
    }

    public String getDtIniCalcDb() {
        return this.dtIniCalcDb;
    }

    public void setDtEndCalcDb(String dtEndCalcDb) {
        this.dtEndCalcDb = Functions.subString(dtEndCalcDb, Len.DT_END_CALC_DB);
    }

    public String getDtEndCalcDb() {
        return this.dtEndCalcDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_FISC = 16;
        public static final int TP_CAUS_BOLLO1 = 2;
        public static final int TP_CAUS_BOLLO2 = 2;
        public static final int TP_CAUS_BOLLO3 = 2;
        public static final int TP_CAUS_BOLLO4 = 2;
        public static final int TP_CAUS_BOLLO5 = 2;
        public static final int TP_CAUS_BOLLO6 = 2;
        public static final int TP_CAUS_BOLLO7 = 2;
        public static final int DT_INI_CALC = 5;
        public static final int DT_END_CALC = 5;
        public static final int DT_INI_CALC_DB = 10;
        public static final int DT_END_CALC_DB = 10;
        public static final int AREA_LDBVE391_I = COD_FISC + TP_CAUS_BOLLO1 + TP_CAUS_BOLLO2 + TP_CAUS_BOLLO3 + TP_CAUS_BOLLO4 + TP_CAUS_BOLLO5 + TP_CAUS_BOLLO6 + TP_CAUS_BOLLO7 + DT_INI_CALC + DT_END_CALC + DT_INI_CALC_DB + DT_END_CALC_DB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DT_INI_CALC = 8;
            public static final int DT_END_CALC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
