package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVB031<br>
 * Variable: LCCVB031 from copybook LCCVB031<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvb031 {

    //==== PROPERTIES ====
    /**Original name: WB03-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA BILA_TRCH_ESTR
	 *    ALIAS B03
	 *    ULTIMO AGG. 12 SET 2014
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WB03-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WB03-DATI
    private Wb03Dati dati = new Wb03Dati();

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public Wb03Dati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
