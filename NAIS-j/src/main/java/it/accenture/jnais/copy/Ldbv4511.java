package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV4511<br>
 * Variable: LDBV4511 from copybook LDBV4511<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv4511 {

    //==== PROPERTIES ====
    //Original name: LDBV4511-TP-RICH-00
    private String tpRich00 = DefaultValues.stringVal(Len.TP_RICH00);
    //Original name: LDBV4511-TP-RICH-01
    private String tpRich01 = DefaultValues.stringVal(Len.TP_RICH01);
    //Original name: LDBV4511-TP-RICH-02
    private String tpRich02 = DefaultValues.stringVal(Len.TP_RICH02);
    //Original name: LDBV4511-TP-RICH-03
    private String tpRich03 = DefaultValues.stringVal(Len.TP_RICH03);
    //Original name: LDBV4511-TP-RICH-04
    private String tpRich04 = DefaultValues.stringVal(Len.TP_RICH04);
    //Original name: LDBV4511-TP-RICH-05
    private String tpRich05 = DefaultValues.stringVal(Len.TP_RICH05);
    //Original name: LDBV4511-TP-RICH-06
    private String tpRich06 = DefaultValues.stringVal(Len.TP_RICH06);
    //Original name: LDBV4511-TP-RICH-07
    private String tpRich07 = DefaultValues.stringVal(Len.TP_RICH07);
    //Original name: LDBV4511-TP-RICH-08
    private String tpRich08 = DefaultValues.stringVal(Len.TP_RICH08);
    //Original name: LDBV4511-TP-RICH-09
    private String tpRich09 = DefaultValues.stringVal(Len.TP_RICH09);
    //Original name: LDBV4511-STATO-ELAB-00
    private char statoElab00 = DefaultValues.CHAR_VAL;
    //Original name: LDBV4511-STATO-ELAB-01
    private char statoElab01 = DefaultValues.CHAR_VAL;
    //Original name: LDBV4511-STATO-ELAB-02
    private char statoElab02 = DefaultValues.CHAR_VAL;
    //Original name: LDBV4511-STATO-ELAB-03
    private char statoElab03 = DefaultValues.CHAR_VAL;
    //Original name: LDBV4511-STATO-ELAB-04
    private char statoElab04 = DefaultValues.CHAR_VAL;
    //Original name: LDBV4511-STATO-ELAB-05
    private char statoElab05 = DefaultValues.CHAR_VAL;
    //Original name: LDBV4511-STATO-ELAB-06
    private char statoElab06 = DefaultValues.CHAR_VAL;
    //Original name: LDBV4511-STATO-ELAB-07
    private char statoElab07 = DefaultValues.CHAR_VAL;
    //Original name: LDBV4511-STATO-ELAB-08
    private char statoElab08 = DefaultValues.CHAR_VAL;
    //Original name: LDBV4511-STATO-ELAB-09
    private char statoElab09 = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setLdbv4511Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV4511];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV4511);
        setLdbv4511Bytes(buffer, 1);
    }

    public String getLdbv4511Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv4511Bytes());
    }

    public byte[] getLdbv4511Bytes() {
        byte[] buffer = new byte[Len.LDBV4511];
        return getLdbv4511Bytes(buffer, 1);
    }

    public void setLdbv4511Bytes(byte[] buffer, int offset) {
        int position = offset;
        tpRich00 = MarshalByte.readString(buffer, position, Len.TP_RICH00);
        position += Len.TP_RICH00;
        tpRich01 = MarshalByte.readString(buffer, position, Len.TP_RICH01);
        position += Len.TP_RICH01;
        tpRich02 = MarshalByte.readString(buffer, position, Len.TP_RICH02);
        position += Len.TP_RICH02;
        tpRich03 = MarshalByte.readString(buffer, position, Len.TP_RICH03);
        position += Len.TP_RICH03;
        tpRich04 = MarshalByte.readString(buffer, position, Len.TP_RICH04);
        position += Len.TP_RICH04;
        tpRich05 = MarshalByte.readString(buffer, position, Len.TP_RICH05);
        position += Len.TP_RICH05;
        tpRich06 = MarshalByte.readString(buffer, position, Len.TP_RICH06);
        position += Len.TP_RICH06;
        tpRich07 = MarshalByte.readString(buffer, position, Len.TP_RICH07);
        position += Len.TP_RICH07;
        tpRich08 = MarshalByte.readString(buffer, position, Len.TP_RICH08);
        position += Len.TP_RICH08;
        tpRich09 = MarshalByte.readString(buffer, position, Len.TP_RICH09);
        position += Len.TP_RICH09;
        statoElab00 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        statoElab01 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        statoElab02 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        statoElab03 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        statoElab04 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        statoElab05 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        statoElab06 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        statoElab07 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        statoElab08 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        statoElab09 = MarshalByte.readChar(buffer, position);
    }

    public byte[] getLdbv4511Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tpRich00, Len.TP_RICH00);
        position += Len.TP_RICH00;
        MarshalByte.writeString(buffer, position, tpRich01, Len.TP_RICH01);
        position += Len.TP_RICH01;
        MarshalByte.writeString(buffer, position, tpRich02, Len.TP_RICH02);
        position += Len.TP_RICH02;
        MarshalByte.writeString(buffer, position, tpRich03, Len.TP_RICH03);
        position += Len.TP_RICH03;
        MarshalByte.writeString(buffer, position, tpRich04, Len.TP_RICH04);
        position += Len.TP_RICH04;
        MarshalByte.writeString(buffer, position, tpRich05, Len.TP_RICH05);
        position += Len.TP_RICH05;
        MarshalByte.writeString(buffer, position, tpRich06, Len.TP_RICH06);
        position += Len.TP_RICH06;
        MarshalByte.writeString(buffer, position, tpRich07, Len.TP_RICH07);
        position += Len.TP_RICH07;
        MarshalByte.writeString(buffer, position, tpRich08, Len.TP_RICH08);
        position += Len.TP_RICH08;
        MarshalByte.writeString(buffer, position, tpRich09, Len.TP_RICH09);
        position += Len.TP_RICH09;
        MarshalByte.writeChar(buffer, position, statoElab00);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, statoElab01);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, statoElab02);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, statoElab03);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, statoElab04);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, statoElab05);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, statoElab06);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, statoElab07);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, statoElab08);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, statoElab09);
        return buffer;
    }

    public void setTpRich00(String tpRich00) {
        this.tpRich00 = Functions.subString(tpRich00, Len.TP_RICH00);
    }

    public String getTpRich00() {
        return this.tpRich00;
    }

    public void setTpRich01(String tpRich01) {
        this.tpRich01 = Functions.subString(tpRich01, Len.TP_RICH01);
    }

    public String getTpRich01() {
        return this.tpRich01;
    }

    public void setTpRich02(String tpRich02) {
        this.tpRich02 = Functions.subString(tpRich02, Len.TP_RICH02);
    }

    public String getTpRich02() {
        return this.tpRich02;
    }

    public void setTpRich03(String tpRich03) {
        this.tpRich03 = Functions.subString(tpRich03, Len.TP_RICH03);
    }

    public String getTpRich03() {
        return this.tpRich03;
    }

    public void setTpRich04(String tpRich04) {
        this.tpRich04 = Functions.subString(tpRich04, Len.TP_RICH04);
    }

    public String getTpRich04() {
        return this.tpRich04;
    }

    public void setTpRich05(String tpRich05) {
        this.tpRich05 = Functions.subString(tpRich05, Len.TP_RICH05);
    }

    public String getTpRich05() {
        return this.tpRich05;
    }

    public void setTpRich06(String tpRich06) {
        this.tpRich06 = Functions.subString(tpRich06, Len.TP_RICH06);
    }

    public String getTpRich06() {
        return this.tpRich06;
    }

    public void setTpRich07(String tpRich07) {
        this.tpRich07 = Functions.subString(tpRich07, Len.TP_RICH07);
    }

    public String getTpRich07() {
        return this.tpRich07;
    }

    public void setTpRich08(String tpRich08) {
        this.tpRich08 = Functions.subString(tpRich08, Len.TP_RICH08);
    }

    public String getTpRich08() {
        return this.tpRich08;
    }

    public void setTpRich09(String tpRich09) {
        this.tpRich09 = Functions.subString(tpRich09, Len.TP_RICH09);
    }

    public String getTpRich09() {
        return this.tpRich09;
    }

    public void setStatoElab00(char statoElab00) {
        this.statoElab00 = statoElab00;
    }

    public char getStatoElab00() {
        return this.statoElab00;
    }

    public void setStatoElab01(char statoElab01) {
        this.statoElab01 = statoElab01;
    }

    public char getStatoElab01() {
        return this.statoElab01;
    }

    public void setStatoElab02(char statoElab02) {
        this.statoElab02 = statoElab02;
    }

    public char getStatoElab02() {
        return this.statoElab02;
    }

    public void setStatoElab03(char statoElab03) {
        this.statoElab03 = statoElab03;
    }

    public char getStatoElab03() {
        return this.statoElab03;
    }

    public void setStatoElab04(char statoElab04) {
        this.statoElab04 = statoElab04;
    }

    public char getStatoElab04() {
        return this.statoElab04;
    }

    public void setStatoElab05(char statoElab05) {
        this.statoElab05 = statoElab05;
    }

    public char getStatoElab05() {
        return this.statoElab05;
    }

    public void setStatoElab06(char statoElab06) {
        this.statoElab06 = statoElab06;
    }

    public char getStatoElab06() {
        return this.statoElab06;
    }

    public void setStatoElab07(char statoElab07) {
        this.statoElab07 = statoElab07;
    }

    public char getStatoElab07() {
        return this.statoElab07;
    }

    public void setStatoElab08(char statoElab08) {
        this.statoElab08 = statoElab08;
    }

    public char getStatoElab08() {
        return this.statoElab08;
    }

    public void setStatoElab09(char statoElab09) {
        this.statoElab09 = statoElab09;
    }

    public char getStatoElab09() {
        return this.statoElab09;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_RICH00 = 2;
        public static final int TP_RICH01 = 2;
        public static final int TP_RICH02 = 2;
        public static final int TP_RICH03 = 2;
        public static final int TP_RICH04 = 2;
        public static final int TP_RICH05 = 2;
        public static final int TP_RICH06 = 2;
        public static final int TP_RICH07 = 2;
        public static final int TP_RICH08 = 2;
        public static final int TP_RICH09 = 2;
        public static final int STATO_ELAB00 = 1;
        public static final int STATO_ELAB01 = 1;
        public static final int STATO_ELAB02 = 1;
        public static final int STATO_ELAB03 = 1;
        public static final int STATO_ELAB04 = 1;
        public static final int STATO_ELAB05 = 1;
        public static final int STATO_ELAB06 = 1;
        public static final int STATO_ELAB07 = 1;
        public static final int STATO_ELAB08 = 1;
        public static final int STATO_ELAB09 = 1;
        public static final int LDBV4511 = TP_RICH00 + TP_RICH01 + TP_RICH02 + TP_RICH03 + TP_RICH04 + TP_RICH05 + TP_RICH06 + TP_RICH07 + TP_RICH08 + TP_RICH09 + STATO_ELAB00 + STATO_ELAB01 + STATO_ELAB02 + STATO_ELAB03 + STATO_ELAB04 + STATO_ELAB05 + STATO_ELAB06 + STATO_ELAB07 + STATO_ELAB08 + STATO_ELAB09;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
