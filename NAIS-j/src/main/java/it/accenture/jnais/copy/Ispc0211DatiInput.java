package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.Ispc0211Opzioni;

/**Original name: ISPC0211-DATI-INPUT<br>
 * Variable: ISPC0211-DATI-INPUT from copybook ISPC0211<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ispc0211DatiInput {

    //==== PROPERTIES ====
    public static final int OPZIONI_MAXOCCURS = 10;
    //Original name: ISPC0211-COD-COMPAGNIA
    private String codCompagnia = DefaultValues.stringVal(Len.COD_COMPAGNIA);
    /**Original name: ISPC0211-NUM-POLIZZA<br>
	 * <pre>----------------------------------------------------------------*
	 *    DATI CONTRATTO
	 * ----------------------------------------------------------------*</pre>*/
    private String numPolizza = DefaultValues.stringVal(Len.NUM_POLIZZA);
    //Original name: ISPC0211-COD-PRODOTTO
    private String codProdotto = DefaultValues.stringVal(Len.COD_PRODOTTO);
    //Original name: ISPC0211-DATA-DECORR-POLIZZA
    private String dataDecorrPolizza = DefaultValues.stringVal(Len.DATA_DECORR_POLIZZA);
    /**Original name: ISPC0211-COD-CONVENZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *    DATI CONVENZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private String codConvenzione = DefaultValues.stringVal(Len.COD_CONVENZIONE);
    //Original name: ISPC0211-DATA-INIZ-VALID-CONV
    private String dataInizValidConv = DefaultValues.stringVal(Len.DATA_INIZ_VALID_CONV);
    /**Original name: ISPC0211-DEE<br>
	 * <pre>----------------------------------------------------------------*
	 *    DATI OPERAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private String dee = DefaultValues.stringVal(Len.DEE);
    //Original name: ISPC0211-DATA-RIFERIMENTO
    private String dataRiferimento = DefaultValues.stringVal(Len.DATA_RIFERIMENTO);
    //Original name: ISPC0211-LIVELLO-UTENTE
    private String livelloUtente = DefaultValues.stringVal(Len.LIVELLO_UTENTE);
    //Original name: ISPC0211-OPZ-NUM-MAX-ELE
    private String opzNumMaxEle = DefaultValues.stringVal(Len.OPZ_NUM_MAX_ELE);
    //Original name: ISPC0211-OPZIONI
    private Ispc0211Opzioni[] opzioni = new Ispc0211Opzioni[OPZIONI_MAXOCCURS];
    //Original name: ISPC0211-SESSION-ID
    private String sessionId = DefaultValues.stringVal(Len.SESSION_ID);
    //Original name: ISPC0211-FLG-REC-PROV
    private char flgRecProv = DefaultValues.CHAR_VAL;
    //Original name: ISPC0211-FUNZIONALITA
    private String funzionalita = DefaultValues.stringVal(Len.FUNZIONALITA);
    //Original name: ISPC0211-COD-INIZIATIVA
    private String codIniziativa = DefaultValues.stringVal(Len.COD_INIZIATIVA);

    //==== CONSTRUCTORS ====
    public Ispc0211DatiInput() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int opzioniIdx = 1; opzioniIdx <= OPZIONI_MAXOCCURS; opzioniIdx++) {
            opzioni[opzioniIdx - 1] = new Ispc0211Opzioni();
        }
    }

    public void setIspc0211DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        codCompagnia = MarshalByte.readFixedString(buffer, position, Len.COD_COMPAGNIA);
        position += Len.COD_COMPAGNIA;
        numPolizza = MarshalByte.readString(buffer, position, Len.NUM_POLIZZA);
        position += Len.NUM_POLIZZA;
        codProdotto = MarshalByte.readString(buffer, position, Len.COD_PRODOTTO);
        position += Len.COD_PRODOTTO;
        dataDecorrPolizza = MarshalByte.readString(buffer, position, Len.DATA_DECORR_POLIZZA);
        position += Len.DATA_DECORR_POLIZZA;
        codConvenzione = MarshalByte.readString(buffer, position, Len.COD_CONVENZIONE);
        position += Len.COD_CONVENZIONE;
        dataInizValidConv = MarshalByte.readString(buffer, position, Len.DATA_INIZ_VALID_CONV);
        position += Len.DATA_INIZ_VALID_CONV;
        dee = MarshalByte.readString(buffer, position, Len.DEE);
        position += Len.DEE;
        dataRiferimento = MarshalByte.readString(buffer, position, Len.DATA_RIFERIMENTO);
        position += Len.DATA_RIFERIMENTO;
        livelloUtente = MarshalByte.readFixedString(buffer, position, Len.LIVELLO_UTENTE);
        position += Len.LIVELLO_UTENTE;
        opzNumMaxEle = MarshalByte.readFixedString(buffer, position, Len.OPZ_NUM_MAX_ELE);
        position += Len.OPZ_NUM_MAX_ELE;
        for (int idx = 1; idx <= OPZIONI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                opzioni[idx - 1].setOpzioniBytes(buffer, position);
                position += Ispc0211Opzioni.Len.OPZIONI;
            }
            else {
                opzioni[idx - 1].initOpzioniSpaces();
                position += Ispc0211Opzioni.Len.OPZIONI;
            }
        }
        sessionId = MarshalByte.readString(buffer, position, Len.SESSION_ID);
        position += Len.SESSION_ID;
        flgRecProv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        funzionalita = MarshalByte.readFixedString(buffer, position, Len.FUNZIONALITA);
        position += Len.FUNZIONALITA;
        codIniziativa = MarshalByte.readString(buffer, position, Len.COD_INIZIATIVA);
    }

    public byte[] getIspc0211DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codCompagnia, Len.COD_COMPAGNIA);
        position += Len.COD_COMPAGNIA;
        MarshalByte.writeString(buffer, position, numPolizza, Len.NUM_POLIZZA);
        position += Len.NUM_POLIZZA;
        MarshalByte.writeString(buffer, position, codProdotto, Len.COD_PRODOTTO);
        position += Len.COD_PRODOTTO;
        MarshalByte.writeString(buffer, position, dataDecorrPolizza, Len.DATA_DECORR_POLIZZA);
        position += Len.DATA_DECORR_POLIZZA;
        MarshalByte.writeString(buffer, position, codConvenzione, Len.COD_CONVENZIONE);
        position += Len.COD_CONVENZIONE;
        MarshalByte.writeString(buffer, position, dataInizValidConv, Len.DATA_INIZ_VALID_CONV);
        position += Len.DATA_INIZ_VALID_CONV;
        MarshalByte.writeString(buffer, position, dee, Len.DEE);
        position += Len.DEE;
        MarshalByte.writeString(buffer, position, dataRiferimento, Len.DATA_RIFERIMENTO);
        position += Len.DATA_RIFERIMENTO;
        MarshalByte.writeString(buffer, position, livelloUtente, Len.LIVELLO_UTENTE);
        position += Len.LIVELLO_UTENTE;
        MarshalByte.writeString(buffer, position, opzNumMaxEle, Len.OPZ_NUM_MAX_ELE);
        position += Len.OPZ_NUM_MAX_ELE;
        for (int idx = 1; idx <= OPZIONI_MAXOCCURS; idx++) {
            opzioni[idx - 1].getOpzioniBytes(buffer, position);
            position += Ispc0211Opzioni.Len.OPZIONI;
        }
        MarshalByte.writeString(buffer, position, sessionId, Len.SESSION_ID);
        position += Len.SESSION_ID;
        MarshalByte.writeChar(buffer, position, flgRecProv);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, funzionalita, Len.FUNZIONALITA);
        position += Len.FUNZIONALITA;
        MarshalByte.writeString(buffer, position, codIniziativa, Len.COD_INIZIATIVA);
        return buffer;
    }

    public void setIspc0211CodCompagniaFormatted(String ispc0211CodCompagnia) {
        this.codCompagnia = Trunc.toUnsignedNumeric(ispc0211CodCompagnia, Len.COD_COMPAGNIA);
    }

    public int getIspc0211CodCompagnia() {
        return NumericDisplay.asInt(this.codCompagnia);
    }

    public void setNumPolizza(String numPolizza) {
        this.numPolizza = Functions.subString(numPolizza, Len.NUM_POLIZZA);
    }

    public String getNumPolizza() {
        return this.numPolizza;
    }

    public void setCodProdotto(String codProdotto) {
        this.codProdotto = Functions.subString(codProdotto, Len.COD_PRODOTTO);
    }

    public String getCodProdotto() {
        return this.codProdotto;
    }

    public void setDataDecorrPolizza(String dataDecorrPolizza) {
        this.dataDecorrPolizza = Functions.subString(dataDecorrPolizza, Len.DATA_DECORR_POLIZZA);
    }

    public String getDataDecorrPolizza() {
        return this.dataDecorrPolizza;
    }

    public void setCodConvenzione(String codConvenzione) {
        this.codConvenzione = Functions.subString(codConvenzione, Len.COD_CONVENZIONE);
    }

    public String getCodConvenzione() {
        return this.codConvenzione;
    }

    public void setDataInizValidConv(String dataInizValidConv) {
        this.dataInizValidConv = Functions.subString(dataInizValidConv, Len.DATA_INIZ_VALID_CONV);
    }

    public String getDataInizValidConv() {
        return this.dataInizValidConv;
    }

    public void setDee(String dee) {
        this.dee = Functions.subString(dee, Len.DEE);
    }

    public String getDee() {
        return this.dee;
    }

    public void setDataRiferimento(String dataRiferimento) {
        this.dataRiferimento = Functions.subString(dataRiferimento, Len.DATA_RIFERIMENTO);
    }

    public String getDataRiferimento() {
        return this.dataRiferimento;
    }

    public void setIspc0211LivelloUtente(short ispc0211LivelloUtente) {
        this.livelloUtente = NumericDisplay.asString(ispc0211LivelloUtente, Len.LIVELLO_UTENTE);
    }

    public void setIspc0211LivelloUtenteFormatted(String ispc0211LivelloUtente) {
        this.livelloUtente = Trunc.toUnsignedNumeric(ispc0211LivelloUtente, Len.LIVELLO_UTENTE);
    }

    public short getIspc0211LivelloUtente() {
        return NumericDisplay.asShort(this.livelloUtente);
    }

    public void setIspc0211OpzNumMaxEleFormatted(String ispc0211OpzNumMaxEle) {
        this.opzNumMaxEle = Trunc.toUnsignedNumeric(ispc0211OpzNumMaxEle, Len.OPZ_NUM_MAX_ELE);
    }

    public short getIspc0211OpzNumMaxEle() {
        return NumericDisplay.asShort(this.opzNumMaxEle);
    }

    public void setSessionId(String sessionId) {
        this.sessionId = Functions.subString(sessionId, Len.SESSION_ID);
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setFlgRecProv(char flgRecProv) {
        this.flgRecProv = flgRecProv;
    }

    public void setIspc0211FlgRecProvFormatted(String ispc0211FlgRecProv) {
        setFlgRecProv(Functions.charAt(ispc0211FlgRecProv, Types.CHAR_SIZE));
    }

    public char getFlgRecProv() {
        return this.flgRecProv;
    }

    public void setIspc0211FunzionalitaFormatted(String ispc0211Funzionalita) {
        this.funzionalita = Trunc.toUnsignedNumeric(ispc0211Funzionalita, Len.FUNZIONALITA);
    }

    public int getIspc0211Funzionalita() {
        return NumericDisplay.asInt(this.funzionalita);
    }

    public void setCodIniziativa(String codIniziativa) {
        this.codIniziativa = Functions.subString(codIniziativa, Len.COD_INIZIATIVA);
    }

    public String getCodIniziativa() {
        return this.codIniziativa;
    }

    public Ispc0211Opzioni getOpzioni(int idx) {
        return opzioni[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_COMPAGNIA = 5;
        public static final int NUM_POLIZZA = 11;
        public static final int COD_PRODOTTO = 12;
        public static final int DATA_DECORR_POLIZZA = 8;
        public static final int COD_CONVENZIONE = 12;
        public static final int DATA_INIZ_VALID_CONV = 8;
        public static final int DEE = 8;
        public static final int DATA_RIFERIMENTO = 8;
        public static final int LIVELLO_UTENTE = 2;
        public static final int OPZ_NUM_MAX_ELE = 3;
        public static final int SESSION_ID = 20;
        public static final int FLG_REC_PROV = 1;
        public static final int FUNZIONALITA = 5;
        public static final int COD_INIZIATIVA = 12;
        public static final int ISPC0211_DATI_INPUT = COD_COMPAGNIA + NUM_POLIZZA + COD_PRODOTTO + DATA_DECORR_POLIZZA + COD_CONVENZIONE + DATA_INIZ_VALID_CONV + DEE + DATA_RIFERIMENTO + LIVELLO_UTENTE + OPZ_NUM_MAX_ELE + Ispc0211DatiInput.OPZIONI_MAXOCCURS * Ispc0211Opzioni.Len.OPZIONI + SESSION_ID + FLG_REC_PROV + FUNZIONALITA + COD_INIZIATIVA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
