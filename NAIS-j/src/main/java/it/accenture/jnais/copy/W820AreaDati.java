package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: W820-AREA-DATI<br>
 * Variable: W820-AREA-DATI from copybook LOAR0820<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class W820AreaDati {

    //==== PROPERTIES ====
    //Original name: W820-DT-ELAB
    private String dtElab = "";
    //Original name: FILLER-W820-AREA-DATI
    private char flr1 = ';';
    //Original name: W820-COD-ANIA
    private String codAnia = "00000";
    //Original name: FILLER-W820-AREA-DATI-1
    private char flr2 = ';';
    //Original name: W820-COD-AGE
    private String codAge = "00000";
    //Original name: FILLER-W820-AREA-DATI-2
    private char flr3 = ';';
    //Original name: W820-RAMO-GEST
    private String ramoGest = "";
    //Original name: FILLER-W820-AREA-DATI-3
    private char flr4 = ';';
    //Original name: W820-NUM-POLI
    private String numPoli = "";
    //Original name: FILLER-W820-AREA-DATI-4
    private char flr5 = ';';
    //Original name: W820-COD-TARI
    private String codTari = "";
    //Original name: FILLER-W820-AREA-DATI-5
    private char flr6 = ';';
    //Original name: W820-DT-EMIS
    private String dtEmis = "";
    //Original name: FILLER-W820-AREA-DATI-6
    private char flr7 = ';';
    //Original name: W820-DT-EFF
    private String dtEff = "";
    //Original name: FILLER-W820-AREA-DATI-7
    private char flr8 = ';';
    //Original name: W820-DT-RICOR
    private String dtRicor = "";
    //Original name: FILLER-W820-AREA-DATI-8
    private char flr9 = ';';
    //Original name: W820-DT-PAG
    private String dtPag = "";
    //Original name: FILLER-W820-AREA-DATI-9
    private char flr10 = ';';
    //Original name: W820-PREMIO-NET
    private String premioNet = LiteralGenerator.create("0", Len.PREMIO_NET);
    //Original name: FILLER-W820-AREA-DATI-10
    private char flr11 = ';';
    //Original name: W820-CPT-RIVTO
    private String cptRivto = LiteralGenerator.create("0", Len.CPT_RIVTO);
    //Original name: FILLER-W820-AREA-DATI-11
    private char flr12 = ';';
    //Original name: W820-ULT-INCR
    private String ultIncr = LiteralGenerator.create("0", Len.ULT_INCR);
    //Original name: FILLER-W820-AREA-DATI-12
    private char flr13 = ';';
    //Original name: W820-RISERVA
    private String riserva = LiteralGenerator.create("0", Len.RISERVA);
    //Original name: FILLER-W820-AREA-DATI-13
    private char flr14 = ';';
    //Original name: W820-FLAG-VERS-AGG
    private char flagVersAgg = Types.SPACE_CHAR;
    //Original name: FILLER-W820-AREA-DATI-14
    private char flr15 = ';';
    //Original name: W820-FLAG-RIS-PARZ
    private char flagRisParz = Types.SPACE_CHAR;
    //Original name: FILLER-W820-AREA-DATI-15
    private char flr16 = ';';
    //Original name: W820-IMP-RISC
    private String impRisc = LiteralGenerator.create("0", Len.IMP_RISC);
    //Original name: FILLER-W820-AREA-DATI-16
    private char flr17 = ';';
    //Original name: W820-PC-REMUNER
    private String pcRemuner = "0000000";
    //Original name: FILLER-W820-AREA-DATI-17
    private char flr18 = ';';
    //Original name: W820-IMP-REMUNER
    private String impRemuner = LiteralGenerator.create("0", Len.IMP_REMUNER);
    //Original name: FILLER-W820-AREA-DATI-18
    private char flr19 = ';';
    //Original name: W820-TP-ELAB
    private String tpElab = "";
    //Original name: FILLER-W820-AREA-DATI-19
    private char flr20 = ';';
    //Original name: FILLER-W820-AREA-DATI-20
    private String flr21 = "";

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, dtElab, Len.DT_ELAB);
        position += Len.DT_ELAB;
        MarshalByte.writeChar(buffer, position, flr1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codAnia, Len.COD_ANIA);
        position += Len.COD_ANIA;
        MarshalByte.writeChar(buffer, position, flr2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codAge, Len.COD_AGE);
        position += Len.COD_AGE;
        MarshalByte.writeChar(buffer, position, flr3);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ramoGest, Len.RAMO_GEST);
        position += Len.RAMO_GEST;
        MarshalByte.writeChar(buffer, position, flr4);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, numPoli, Len.NUM_POLI);
        position += Len.NUM_POLI;
        MarshalByte.writeChar(buffer, position, flr5);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codTari, Len.COD_TARI);
        position += Len.COD_TARI;
        MarshalByte.writeChar(buffer, position, flr6);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dtEmis, Len.DT_EMIS);
        position += Len.DT_EMIS;
        MarshalByte.writeChar(buffer, position, flr7);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dtEff, Len.DT_EFF);
        position += Len.DT_EFF;
        MarshalByte.writeChar(buffer, position, flr8);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dtRicor, Len.DT_RICOR);
        position += Len.DT_RICOR;
        MarshalByte.writeChar(buffer, position, flr9);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dtPag, Len.DT_PAG);
        position += Len.DT_PAG;
        MarshalByte.writeChar(buffer, position, flr10);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, premioNet, Len.PREMIO_NET);
        position += Len.PREMIO_NET;
        MarshalByte.writeChar(buffer, position, flr11);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, cptRivto, Len.CPT_RIVTO);
        position += Len.CPT_RIVTO;
        MarshalByte.writeChar(buffer, position, flr12);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ultIncr, Len.ULT_INCR);
        position += Len.ULT_INCR;
        MarshalByte.writeChar(buffer, position, flr13);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, riserva, Len.RISERVA);
        position += Len.RISERVA;
        MarshalByte.writeChar(buffer, position, flr14);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flagVersAgg);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flr15);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flagRisParz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flr16);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, impRisc, Len.IMP_RISC);
        position += Len.IMP_RISC;
        MarshalByte.writeChar(buffer, position, flr17);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, pcRemuner, Len.PC_REMUNER);
        position += Len.PC_REMUNER;
        MarshalByte.writeChar(buffer, position, flr18);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, impRemuner, Len.IMP_REMUNER);
        position += Len.IMP_REMUNER;
        MarshalByte.writeChar(buffer, position, flr19);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpElab, Len.TP_ELAB);
        position += Len.TP_ELAB;
        MarshalByte.writeChar(buffer, position, flr20);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr21, Len.FLR21);
        return buffer;
    }

    public void setDtElab(String dtElab) {
        this.dtElab = Functions.subString(dtElab, Len.DT_ELAB);
    }

    public String getDtElab() {
        return this.dtElab;
    }

    public char getFlr1() {
        return this.flr1;
    }

    public void setCodAniaFormatted(String codAnia) {
        this.codAnia = Trunc.toUnsignedNumeric(codAnia, Len.COD_ANIA);
    }

    public int getCodAnia() {
        return NumericDisplay.asInt(this.codAnia);
    }

    public char getFlr2() {
        return this.flr2;
    }

    public void setCodAge(int codAge) {
        this.codAge = NumericDisplay.asString(codAge, Len.COD_AGE);
    }

    public void setCodAgeFormatted(String codAge) {
        this.codAge = Trunc.toUnsignedNumeric(codAge, Len.COD_AGE);
    }

    public int getCodAge() {
        return NumericDisplay.asInt(this.codAge);
    }

    public char getFlr3() {
        return this.flr3;
    }

    public void setRamoGest(String ramoGest) {
        this.ramoGest = Functions.subString(ramoGest, Len.RAMO_GEST);
    }

    public String getRamoGest() {
        return this.ramoGest;
    }

    public char getFlr4() {
        return this.flr4;
    }

    public void setNumPoli(String numPoli) {
        this.numPoli = Functions.subString(numPoli, Len.NUM_POLI);
    }

    public String getNumPoli() {
        return this.numPoli;
    }

    public char getFlr5() {
        return this.flr5;
    }

    public void setCodTari(String codTari) {
        this.codTari = Functions.subString(codTari, Len.COD_TARI);
    }

    public String getCodTari() {
        return this.codTari;
    }

    public char getFlr6() {
        return this.flr6;
    }

    public void setDtEmis(String dtEmis) {
        this.dtEmis = Functions.subString(dtEmis, Len.DT_EMIS);
    }

    public String getDtEmis() {
        return this.dtEmis;
    }

    public char getFlr7() {
        return this.flr7;
    }

    public void setDtEff(String dtEff) {
        this.dtEff = Functions.subString(dtEff, Len.DT_EFF);
    }

    public String getDtEff() {
        return this.dtEff;
    }

    public char getFlr8() {
        return this.flr8;
    }

    public void setDtRicor(String dtRicor) {
        this.dtRicor = Functions.subString(dtRicor, Len.DT_RICOR);
    }

    public String getDtRicor() {
        return this.dtRicor;
    }

    public char getFlr9() {
        return this.flr9;
    }

    public void setDtPag(String dtPag) {
        this.dtPag = Functions.subString(dtPag, Len.DT_PAG);
    }

    public String getDtPag() {
        return this.dtPag;
    }

    public char getFlr10() {
        return this.flr10;
    }

    public void setPremioNet(AfDecimal premioNet) {
        this.premioNet = PicFormatter.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").format(premioNet.copy()).toString();
    }

    public AfDecimal getPremioNet() {
        return PicParser.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").parseDecimal(Len.Int.PREMIO_NET + Len.Fract.PREMIO_NET, Len.Fract.PREMIO_NET, this.premioNet);
    }

    public char getFlr11() {
        return this.flr11;
    }

    public void setCptRivto(AfDecimal cptRivto) {
        this.cptRivto = PicFormatter.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").format(cptRivto.copy()).toString();
    }

    public AfDecimal getCptRivto() {
        return PicParser.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").parseDecimal(Len.Int.CPT_RIVTO + Len.Fract.CPT_RIVTO, Len.Fract.CPT_RIVTO, this.cptRivto);
    }

    public char getFlr12() {
        return this.flr12;
    }

    public void setUltIncr(AfDecimal ultIncr) {
        this.ultIncr = PicFormatter.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").format(ultIncr.copy()).toString();
    }

    public AfDecimal getUltIncr() {
        return PicParser.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").parseDecimal(Len.Int.ULT_INCR + Len.Fract.ULT_INCR, Len.Fract.ULT_INCR, this.ultIncr);
    }

    public char getFlr13() {
        return this.flr13;
    }

    public void setRiserva(AfDecimal riserva) {
        this.riserva = PicFormatter.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").format(riserva.copy()).toString();
    }

    public AfDecimal getRiserva() {
        return PicParser.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").parseDecimal(Len.Int.RISERVA + Len.Fract.RISERVA, Len.Fract.RISERVA, this.riserva);
    }

    public char getFlr14() {
        return this.flr14;
    }

    public void setFlagVersAgg(char flagVersAgg) {
        this.flagVersAgg = flagVersAgg;
    }

    public void setFlagVersAggFormatted(String flagVersAgg) {
        setFlagVersAgg(Functions.charAt(flagVersAgg, Types.CHAR_SIZE));
    }

    public char getFlagVersAgg() {
        return this.flagVersAgg;
    }

    public char getFlr15() {
        return this.flr15;
    }

    public void setFlagRisParz(char flagRisParz) {
        this.flagRisParz = flagRisParz;
    }

    public void setFlagRisParzFormatted(String flagRisParz) {
        setFlagRisParz(Functions.charAt(flagRisParz, Types.CHAR_SIZE));
    }

    public char getFlagRisParz() {
        return this.flagRisParz;
    }

    public char getFlr16() {
        return this.flr16;
    }

    public void setImpRisc(AfDecimal impRisc) {
        this.impRisc = PicFormatter.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").format(impRisc.copy()).toString();
    }

    public AfDecimal getImpRisc() {
        return PicParser.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").parseDecimal(Len.Int.IMP_RISC + Len.Fract.IMP_RISC, Len.Fract.IMP_RISC, this.impRisc);
    }

    public char getFlr17() {
        return this.flr17;
    }

    public void setPcRemuner(AfDecimal pcRemuner) {
        this.pcRemuner = PicFormatter.display("Z(2)9.9(3)").format(pcRemuner.copy()).toString();
    }

    public void setPcRemunerFormatted(String pcRemuner) {
        this.pcRemuner = PicFormatter.display("Z(2)9.9(3)").format(pcRemuner).toString();
    }

    public AfDecimal getPcRemuner() {
        return PicParser.display("Z(2)9.9(3)").parseDecimal(Len.Int.PC_REMUNER + Len.Fract.PC_REMUNER, Len.Fract.PC_REMUNER, this.pcRemuner);
    }

    public char getFlr18() {
        return this.flr18;
    }

    public void setImpRemuner(AfDecimal impRemuner) {
        this.impRemuner = PicFormatter.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").format(impRemuner.copy()).toString();
    }

    public AfDecimal getImpRemuner() {
        return PicParser.display("Z(3),Z(3),Z(3),Z(2)9.9(3)").parseDecimal(Len.Int.IMP_REMUNER + Len.Fract.IMP_REMUNER, Len.Fract.IMP_REMUNER, this.impRemuner);
    }

    public char getFlr19() {
        return this.flr19;
    }

    public void setTpElab(String tpElab) {
        this.tpElab = Functions.subString(tpElab, Len.TP_ELAB);
    }

    public String getTpElab() {
        return this.tpElab;
    }

    public char getFlr20() {
        return this.flr20;
    }

    public String getFlr21() {
        return this.flr21;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_ANIA = 5;
        public static final int COD_AGE = 5;
        public static final int PREMIO_NET = 19;
        public static final int CPT_RIVTO = 19;
        public static final int ULT_INCR = 19;
        public static final int RISERVA = 19;
        public static final int IMP_RISC = 19;
        public static final int PC_REMUNER = 7;
        public static final int IMP_REMUNER = 19;
        public static final int DT_ELAB = 10;
        public static final int RAMO_GEST = 12;
        public static final int NUM_POLI = 40;
        public static final int COD_TARI = 12;
        public static final int DT_EMIS = 10;
        public static final int DT_EFF = 10;
        public static final int DT_RICOR = 7;
        public static final int DT_PAG = 10;
        public static final int TP_ELAB = 12;
        public static final int FLR1 = 1;
        public static final int FLAG_VERS_AGG = 1;
        public static final int FLAG_RIS_PARZ = 1;
        public static final int FLR21 = 125;
        public static final int DATI = DT_ELAB + COD_ANIA + COD_AGE + RAMO_GEST + NUM_POLI + COD_TARI + DT_EMIS + DT_EFF + DT_RICOR + DT_PAG + PREMIO_NET + CPT_RIVTO + ULT_INCR + RISERVA + FLAG_VERS_AGG + FLAG_RIS_PARZ + IMP_RISC + PC_REMUNER + IMP_REMUNER + TP_ELAB + FLR21 + 20 * FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PREMIO_NET = 12;
            public static final int CPT_RIVTO = 12;
            public static final int ULT_INCR = 12;
            public static final int RISERVA = 12;
            public static final int IMP_RISC = 12;
            public static final int PC_REMUNER = 3;
            public static final int IMP_REMUNER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PREMIO_NET = 3;
            public static final int CPT_RIVTO = 3;
            public static final int ULT_INCR = 3;
            public static final int RISERVA = 3;
            public static final int IMP_RISC = 3;
            public static final int PC_REMUNER = 3;
            public static final int IMP_REMUNER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
