package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: LDBVC591<br>
 * Copybook: LDBVC591 from copybook LDBVC591<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Ldbvc591 {

    //==== PROPERTIES ====
    //Original name: LDBVC591-TP-MOVI-1
    private int ldbvc591TpMovi1 = DefaultValues.INT_VAL;
    //Original name: LDBVC591-TP-MOVI-2
    private int ldbvc591TpMovi2 = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbvc591Formatted(String data) {
        byte[] buffer = new byte[Len.LDBVC591];
        MarshalByte.writeString(buffer, 1, data, Len.LDBVC591);
        setLdbvc591Bytes(buffer, 1);
    }

    public String getLdbvc591Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvc591Bytes());
    }

    /**Original name: LDBVC591<br>*/
    public byte[] getLdbvc591Bytes() {
        byte[] buffer = new byte[Len.LDBVC591];
        return getLdbvc591Bytes(buffer, 1);
    }

    public void setLdbvc591Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbvc591TpMovi1 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBVC591_TP_MOVI1, 0);
        position += Len.LDBVC591_TP_MOVI1;
        ldbvc591TpMovi2 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBVC591_TP_MOVI2, 0);
    }

    public byte[] getLdbvc591Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbvc591TpMovi1, Len.Int.LDBVC591_TP_MOVI1, 0);
        position += Len.LDBVC591_TP_MOVI1;
        MarshalByte.writeIntAsPacked(buffer, position, ldbvc591TpMovi2, Len.Int.LDBVC591_TP_MOVI2, 0);
        return buffer;
    }

    public void setLdbvc591TpMovi1(int ldbvc591TpMovi1) {
        this.ldbvc591TpMovi1 = ldbvc591TpMovi1;
    }

    public int getLdbvc591TpMovi1() {
        return this.ldbvc591TpMovi1;
    }

    public void setLdbvc591TpMovi2(int ldbvc591TpMovi2) {
        this.ldbvc591TpMovi2 = ldbvc591TpMovi2;
    }

    public int getLdbvc591TpMovi2() {
        return this.ldbvc591TpMovi2;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBVC591_TP_MOVI1 = 3;
        public static final int LDBVC591_TP_MOVI2 = 3;
        public static final int LDBVC591 = LDBVC591_TP_MOVI1 + LDBVC591_TP_MOVI2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBVC591_TP_MOVI1 = 5;
            public static final int LDBVC591_TP_MOVI2 = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
