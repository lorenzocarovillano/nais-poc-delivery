package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.B03AaRenCer;
import it.accenture.jnais.ws.redefines.B03Abb;
import it.accenture.jnais.ws.redefines.B03AcqExp;
import it.accenture.jnais.ws.redefines.B03AlqMargCSubrsh;
import it.accenture.jnais.ws.redefines.B03AlqMargRis;
import it.accenture.jnais.ws.redefines.B03AlqRetrT;
import it.accenture.jnais.ws.redefines.B03AntidurCalc365;
import it.accenture.jnais.ws.redefines.B03AntidurDtCalc;
import it.accenture.jnais.ws.redefines.B03AntidurRicorPrec;
import it.accenture.jnais.ws.redefines.B03CarAcqNonScon;
import it.accenture.jnais.ws.redefines.B03CarAcqPrecontato;
import it.accenture.jnais.ws.redefines.B03CarGest;
import it.accenture.jnais.ws.redefines.B03CarGestNonScon;
import it.accenture.jnais.ws.redefines.B03CarInc;
import it.accenture.jnais.ws.redefines.B03CarIncNonScon;
import it.accenture.jnais.ws.redefines.B03Carz;
import it.accenture.jnais.ws.redefines.B03CodAge;
import it.accenture.jnais.ws.redefines.B03CodCan;
import it.accenture.jnais.ws.redefines.B03CodPrdt;
import it.accenture.jnais.ws.redefines.B03CodSubage;
import it.accenture.jnais.ws.redefines.B03CoeffOpzCpt;
import it.accenture.jnais.ws.redefines.B03CoeffOpzRen;
import it.accenture.jnais.ws.redefines.B03CoeffRis1T;
import it.accenture.jnais.ws.redefines.B03CoeffRis2T;
import it.accenture.jnais.ws.redefines.B03CommisInter;
import it.accenture.jnais.ws.redefines.B03CptAsstoIniMor;
import it.accenture.jnais.ws.redefines.B03CptDtRidz;
import it.accenture.jnais.ws.redefines.B03CptDtStab;
import it.accenture.jnais.ws.redefines.B03CptRiasto;
import it.accenture.jnais.ws.redefines.B03CptRiastoEcc;
import it.accenture.jnais.ws.redefines.B03CptRshMor;
import it.accenture.jnais.ws.redefines.B03CSubrshT;
import it.accenture.jnais.ws.redefines.B03CumRiscpar;
import it.accenture.jnais.ws.redefines.B03Dir;
import it.accenture.jnais.ws.redefines.B03DirEmis;
import it.accenture.jnais.ws.redefines.B03DtDecorAdes;
import it.accenture.jnais.ws.redefines.B03DtEffCambStat;
import it.accenture.jnais.ws.redefines.B03DtEffRidz;
import it.accenture.jnais.ws.redefines.B03DtEffStab;
import it.accenture.jnais.ws.redefines.B03DtEmisCambStat;
import it.accenture.jnais.ws.redefines.B03DtEmisRidz;
import it.accenture.jnais.ws.redefines.B03DtEmisTrch;
import it.accenture.jnais.ws.redefines.B03DtIncUltPre;
import it.accenture.jnais.ws.redefines.B03DtIniValTar;
import it.accenture.jnais.ws.redefines.B03DtNasc1oAssto;
import it.accenture.jnais.ws.redefines.B03DtQtzEmis;
import it.accenture.jnais.ws.redefines.B03DtScadIntmd;
import it.accenture.jnais.ws.redefines.B03DtScadPagPre;
import it.accenture.jnais.ws.redefines.B03DtScadTrch;
import it.accenture.jnais.ws.redefines.B03DtUltPrePag;
import it.accenture.jnais.ws.redefines.B03DtUltRival;
import it.accenture.jnais.ws.redefines.B03Dur1oPerAa;
import it.accenture.jnais.ws.redefines.B03Dur1oPerGg;
import it.accenture.jnais.ws.redefines.B03Dur1oPerMm;
import it.accenture.jnais.ws.redefines.B03DurAa;
import it.accenture.jnais.ws.redefines.B03DurGarAa;
import it.accenture.jnais.ws.redefines.B03DurGarGg;
import it.accenture.jnais.ws.redefines.B03DurGarMm;
import it.accenture.jnais.ws.redefines.B03DurGg;
import it.accenture.jnais.ws.redefines.B03DurMm;
import it.accenture.jnais.ws.redefines.B03DurPagPre;
import it.accenture.jnais.ws.redefines.B03DurPagRen;
import it.accenture.jnais.ws.redefines.B03DurResDtCalc;
import it.accenture.jnais.ws.redefines.B03EtaAa1oAssto;
import it.accenture.jnais.ws.redefines.B03EtaMm1oAssto;
import it.accenture.jnais.ws.redefines.B03EtaRaggnDtCalc;
import it.accenture.jnais.ws.redefines.B03Fraz;
import it.accenture.jnais.ws.redefines.B03FrazDecrCpt;
import it.accenture.jnais.ws.redefines.B03FrazIniErogRen;
import it.accenture.jnais.ws.redefines.B03IdRichEstrazAgg;
import it.accenture.jnais.ws.redefines.B03ImpCarCasoMor;
import it.accenture.jnais.ws.redefines.B03IntrTecn;
import it.accenture.jnais.ws.redefines.B03MetRiscSpcl;
import it.accenture.jnais.ws.redefines.B03MinGartoT;
import it.accenture.jnais.ws.redefines.B03MinTrnutT;
import it.accenture.jnais.ws.redefines.B03NsQuo;
import it.accenture.jnais.ws.redefines.B03NumPrePatt;
import it.accenture.jnais.ws.redefines.B03OverComm;
import it.accenture.jnais.ws.redefines.B03PcCarAcq;
import it.accenture.jnais.ws.redefines.B03PcCarGest;
import it.accenture.jnais.ws.redefines.B03PcCarMor;
import it.accenture.jnais.ws.redefines.B03PreAnnualizRicor;
import it.accenture.jnais.ws.redefines.B03PreCont;
import it.accenture.jnais.ws.redefines.B03PreDovIni;
import it.accenture.jnais.ws.redefines.B03PreDovRivtoT;
import it.accenture.jnais.ws.redefines.B03PrePattuitoIni;
import it.accenture.jnais.ws.redefines.B03PrePpIni;
import it.accenture.jnais.ws.redefines.B03PrePpUlt;
import it.accenture.jnais.ws.redefines.B03PreRiasto;
import it.accenture.jnais.ws.redefines.B03PreRiastoEcc;
import it.accenture.jnais.ws.redefines.B03PreRshT;
import it.accenture.jnais.ws.redefines.B03ProvAcq;
import it.accenture.jnais.ws.redefines.B03ProvAcqRicor;
import it.accenture.jnais.ws.redefines.B03ProvInc;
import it.accenture.jnais.ws.redefines.B03PrstzAggIni;
import it.accenture.jnais.ws.redefines.B03PrstzAggUlt;
import it.accenture.jnais.ws.redefines.B03PrstzIni;
import it.accenture.jnais.ws.redefines.B03PrstzT;
import it.accenture.jnais.ws.redefines.B03QtzSpZCoupDtC;
import it.accenture.jnais.ws.redefines.B03QtzSpZCoupEmis;
import it.accenture.jnais.ws.redefines.B03QtzSpZOpzDtCa;
import it.accenture.jnais.ws.redefines.B03QtzSpZOpzEmis;
import it.accenture.jnais.ws.redefines.B03QtzTotDtCalc;
import it.accenture.jnais.ws.redefines.B03QtzTotDtUltBil;
import it.accenture.jnais.ws.redefines.B03QtzTotEmis;
import it.accenture.jnais.ws.redefines.B03Rappel;
import it.accenture.jnais.ws.redefines.B03RatRen;
import it.accenture.jnais.ws.redefines.B03RemunAss;
import it.accenture.jnais.ws.redefines.B03RisAcqT;
import it.accenture.jnais.ws.redefines.B03Riscpar;
import it.accenture.jnais.ws.redefines.B03RisMatChiuPrec;
import it.accenture.jnais.ws.redefines.B03RisPuraT;
import it.accenture.jnais.ws.redefines.B03RisRiasta;
import it.accenture.jnais.ws.redefines.B03RisRiastaEcc;
import it.accenture.jnais.ws.redefines.B03RisRistorniCap;
import it.accenture.jnais.ws.redefines.B03RisSpeT;
import it.accenture.jnais.ws.redefines.B03RisZilT;
import it.accenture.jnais.ws.redefines.B03TsMedio;
import it.accenture.jnais.ws.redefines.B03TsNetT;
import it.accenture.jnais.ws.redefines.B03TsPp;
import it.accenture.jnais.ws.redefines.B03TsRendtoSppr;
import it.accenture.jnais.ws.redefines.B03TsRendtoT;
import it.accenture.jnais.ws.redefines.B03TsStabPre;
import it.accenture.jnais.ws.redefines.B03TsTariDov;
import it.accenture.jnais.ws.redefines.B03TsTariScon;
import it.accenture.jnais.ws.redefines.B03UltRm;

/**Original name: BILA-TRCH-ESTR<br>
 * Variable: BILA-TRCH-ESTR from copybook IDBVB031<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class BilaTrchEstr {

    //==== PROPERTIES ====
    //Original name: B03-ID-BILA-TRCH-ESTR
    private int b03IdBilaTrchEstr = DefaultValues.INT_VAL;
    //Original name: B03-COD-COMP-ANIA
    private int b03CodCompAnia = DefaultValues.INT_VAL;
    //Original name: B03-ID-RICH-ESTRAZ-MAS
    private int b03IdRichEstrazMas = DefaultValues.INT_VAL;
    //Original name: B03-ID-RICH-ESTRAZ-AGG
    private B03IdRichEstrazAgg b03IdRichEstrazAgg = new B03IdRichEstrazAgg();
    //Original name: B03-FL-SIMULAZIONE
    private char b03FlSimulazione = DefaultValues.CHAR_VAL;
    //Original name: B03-DT-RIS
    private int b03DtRis = DefaultValues.INT_VAL;
    //Original name: B03-DT-PRODUZIONE
    private int b03DtProduzione = DefaultValues.INT_VAL;
    //Original name: B03-ID-POLI
    private int b03IdPoli = DefaultValues.INT_VAL;
    //Original name: B03-ID-ADES
    private int b03IdAdes = DefaultValues.INT_VAL;
    //Original name: B03-ID-GAR
    private int b03IdGar = DefaultValues.INT_VAL;
    //Original name: B03-ID-TRCH-DI-GAR
    private int b03IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: B03-TP-FRM-ASSVA
    private String b03TpFrmAssva = DefaultValues.stringVal(Len.B03_TP_FRM_ASSVA);
    //Original name: B03-TP-RAMO-BILA
    private String b03TpRamoBila = DefaultValues.stringVal(Len.B03_TP_RAMO_BILA);
    //Original name: B03-TP-CALC-RIS
    private String b03TpCalcRis = DefaultValues.stringVal(Len.B03_TP_CALC_RIS);
    //Original name: B03-COD-RAMO
    private String b03CodRamo = DefaultValues.stringVal(Len.B03_COD_RAMO);
    //Original name: B03-COD-TARI
    private String b03CodTari = DefaultValues.stringVal(Len.B03_COD_TARI);
    //Original name: B03-DT-INI-VAL-TAR
    private B03DtIniValTar b03DtIniValTar = new B03DtIniValTar();
    //Original name: B03-COD-PROD
    private String b03CodProd = DefaultValues.stringVal(Len.B03_COD_PROD);
    //Original name: B03-DT-INI-VLDT-PROD
    private int b03DtIniVldtProd = DefaultValues.INT_VAL;
    //Original name: B03-COD-TARI-ORGN
    private String b03CodTariOrgn = DefaultValues.stringVal(Len.B03_COD_TARI_ORGN);
    //Original name: B03-MIN-GARTO-T
    private B03MinGartoT b03MinGartoT = new B03MinGartoT();
    //Original name: B03-TP-TARI
    private String b03TpTari = DefaultValues.stringVal(Len.B03_TP_TARI);
    //Original name: B03-TP-PRE
    private char b03TpPre = DefaultValues.CHAR_VAL;
    //Original name: B03-TP-ADEG-PRE
    private char b03TpAdegPre = DefaultValues.CHAR_VAL;
    //Original name: B03-TP-RIVAL
    private String b03TpRival = DefaultValues.stringVal(Len.B03_TP_RIVAL);
    //Original name: B03-FL-DA-TRASF
    private char b03FlDaTrasf = DefaultValues.CHAR_VAL;
    //Original name: B03-FL-CAR-CONT
    private char b03FlCarCont = DefaultValues.CHAR_VAL;
    //Original name: B03-FL-PRE-DA-RIS
    private char b03FlPreDaRis = DefaultValues.CHAR_VAL;
    //Original name: B03-FL-PRE-AGG
    private char b03FlPreAgg = DefaultValues.CHAR_VAL;
    //Original name: B03-TP-TRCH
    private String b03TpTrch = DefaultValues.stringVal(Len.B03_TP_TRCH);
    //Original name: B03-TP-TST
    private String b03TpTst = DefaultValues.stringVal(Len.B03_TP_TST);
    //Original name: B03-COD-CONV
    private String b03CodConv = DefaultValues.stringVal(Len.B03_COD_CONV);
    //Original name: B03-DT-DECOR-POLI
    private int b03DtDecorPoli = DefaultValues.INT_VAL;
    //Original name: B03-DT-DECOR-ADES
    private B03DtDecorAdes b03DtDecorAdes = new B03DtDecorAdes();
    //Original name: B03-DT-DECOR-TRCH
    private int b03DtDecorTrch = DefaultValues.INT_VAL;
    //Original name: B03-DT-EMIS-POLI
    private int b03DtEmisPoli = DefaultValues.INT_VAL;
    //Original name: B03-DT-EMIS-TRCH
    private B03DtEmisTrch b03DtEmisTrch = new B03DtEmisTrch();
    //Original name: B03-DT-SCAD-TRCH
    private B03DtScadTrch b03DtScadTrch = new B03DtScadTrch();
    //Original name: B03-DT-SCAD-INTMD
    private B03DtScadIntmd b03DtScadIntmd = new B03DtScadIntmd();
    //Original name: B03-DT-SCAD-PAG-PRE
    private B03DtScadPagPre b03DtScadPagPre = new B03DtScadPagPre();
    //Original name: B03-DT-ULT-PRE-PAG
    private B03DtUltPrePag b03DtUltPrePag = new B03DtUltPrePag();
    //Original name: B03-DT-NASC-1O-ASSTO
    private B03DtNasc1oAssto b03DtNasc1oAssto = new B03DtNasc1oAssto();
    //Original name: B03-SEX-1O-ASSTO
    private char b03Sex1oAssto = DefaultValues.CHAR_VAL;
    //Original name: B03-ETA-AA-1O-ASSTO
    private B03EtaAa1oAssto b03EtaAa1oAssto = new B03EtaAa1oAssto();
    //Original name: B03-ETA-MM-1O-ASSTO
    private B03EtaMm1oAssto b03EtaMm1oAssto = new B03EtaMm1oAssto();
    //Original name: B03-ETA-RAGGN-DT-CALC
    private B03EtaRaggnDtCalc b03EtaRaggnDtCalc = new B03EtaRaggnDtCalc();
    //Original name: B03-DUR-AA
    private B03DurAa b03DurAa = new B03DurAa();
    //Original name: B03-DUR-MM
    private B03DurMm b03DurMm = new B03DurMm();
    //Original name: B03-DUR-GG
    private B03DurGg b03DurGg = new B03DurGg();
    //Original name: B03-DUR-1O-PER-AA
    private B03Dur1oPerAa b03Dur1oPerAa = new B03Dur1oPerAa();
    //Original name: B03-DUR-1O-PER-MM
    private B03Dur1oPerMm b03Dur1oPerMm = new B03Dur1oPerMm();
    //Original name: B03-DUR-1O-PER-GG
    private B03Dur1oPerGg b03Dur1oPerGg = new B03Dur1oPerGg();
    //Original name: B03-ANTIDUR-RICOR-PREC
    private B03AntidurRicorPrec b03AntidurRicorPrec = new B03AntidurRicorPrec();
    //Original name: B03-ANTIDUR-DT-CALC
    private B03AntidurDtCalc b03AntidurDtCalc = new B03AntidurDtCalc();
    //Original name: B03-DUR-RES-DT-CALC
    private B03DurResDtCalc b03DurResDtCalc = new B03DurResDtCalc();
    //Original name: B03-TP-STAT-BUS-POLI
    private String b03TpStatBusPoli = DefaultValues.stringVal(Len.B03_TP_STAT_BUS_POLI);
    //Original name: B03-TP-CAUS-POLI
    private String b03TpCausPoli = DefaultValues.stringVal(Len.B03_TP_CAUS_POLI);
    //Original name: B03-TP-STAT-BUS-ADES
    private String b03TpStatBusAdes = DefaultValues.stringVal(Len.B03_TP_STAT_BUS_ADES);
    //Original name: B03-TP-CAUS-ADES
    private String b03TpCausAdes = DefaultValues.stringVal(Len.B03_TP_CAUS_ADES);
    //Original name: B03-TP-STAT-BUS-TRCH
    private String b03TpStatBusTrch = DefaultValues.stringVal(Len.B03_TP_STAT_BUS_TRCH);
    //Original name: B03-TP-CAUS-TRCH
    private String b03TpCausTrch = DefaultValues.stringVal(Len.B03_TP_CAUS_TRCH);
    //Original name: B03-DT-EFF-CAMB-STAT
    private B03DtEffCambStat b03DtEffCambStat = new B03DtEffCambStat();
    //Original name: B03-DT-EMIS-CAMB-STAT
    private B03DtEmisCambStat b03DtEmisCambStat = new B03DtEmisCambStat();
    //Original name: B03-DT-EFF-STAB
    private B03DtEffStab b03DtEffStab = new B03DtEffStab();
    //Original name: B03-CPT-DT-STAB
    private B03CptDtStab b03CptDtStab = new B03CptDtStab();
    //Original name: B03-DT-EFF-RIDZ
    private B03DtEffRidz b03DtEffRidz = new B03DtEffRidz();
    //Original name: B03-DT-EMIS-RIDZ
    private B03DtEmisRidz b03DtEmisRidz = new B03DtEmisRidz();
    //Original name: B03-CPT-DT-RIDZ
    private B03CptDtRidz b03CptDtRidz = new B03CptDtRidz();
    //Original name: B03-FRAZ
    private B03Fraz b03Fraz = new B03Fraz();
    //Original name: B03-DUR-PAG-PRE
    private B03DurPagPre b03DurPagPre = new B03DurPagPre();
    //Original name: B03-NUM-PRE-PATT
    private B03NumPrePatt b03NumPrePatt = new B03NumPrePatt();
    //Original name: B03-FRAZ-INI-EROG-REN
    private B03FrazIniErogRen b03FrazIniErogRen = new B03FrazIniErogRen();
    //Original name: B03-AA-REN-CER
    private B03AaRenCer b03AaRenCer = new B03AaRenCer();
    //Original name: B03-RAT-REN
    private B03RatRen b03RatRen = new B03RatRen();
    //Original name: B03-COD-DIV
    private String b03CodDiv = DefaultValues.stringVal(Len.B03_COD_DIV);
    //Original name: B03-RISCPAR
    private B03Riscpar b03Riscpar = new B03Riscpar();
    //Original name: B03-CUM-RISCPAR
    private B03CumRiscpar b03CumRiscpar = new B03CumRiscpar();
    //Original name: B03-ULT-RM
    private B03UltRm b03UltRm = new B03UltRm();
    //Original name: B03-TS-RENDTO-T
    private B03TsRendtoT b03TsRendtoT = new B03TsRendtoT();
    //Original name: B03-ALQ-RETR-T
    private B03AlqRetrT b03AlqRetrT = new B03AlqRetrT();
    //Original name: B03-MIN-TRNUT-T
    private B03MinTrnutT b03MinTrnutT = new B03MinTrnutT();
    //Original name: B03-TS-NET-T
    private B03TsNetT b03TsNetT = new B03TsNetT();
    //Original name: B03-DT-ULT-RIVAL
    private B03DtUltRival b03DtUltRival = new B03DtUltRival();
    //Original name: B03-PRSTZ-INI
    private B03PrstzIni b03PrstzIni = new B03PrstzIni();
    //Original name: B03-PRSTZ-AGG-INI
    private B03PrstzAggIni b03PrstzAggIni = new B03PrstzAggIni();
    //Original name: B03-PRSTZ-AGG-ULT
    private B03PrstzAggUlt b03PrstzAggUlt = new B03PrstzAggUlt();
    //Original name: B03-RAPPEL
    private B03Rappel b03Rappel = new B03Rappel();
    //Original name: B03-PRE-PATTUITO-INI
    private B03PrePattuitoIni b03PrePattuitoIni = new B03PrePattuitoIni();
    //Original name: B03-PRE-DOV-INI
    private B03PreDovIni b03PreDovIni = new B03PreDovIni();
    //Original name: B03-PRE-DOV-RIVTO-T
    private B03PreDovRivtoT b03PreDovRivtoT = new B03PreDovRivtoT();
    //Original name: B03-PRE-ANNUALIZ-RICOR
    private B03PreAnnualizRicor b03PreAnnualizRicor = new B03PreAnnualizRicor();
    //Original name: B03-PRE-CONT
    private B03PreCont b03PreCont = new B03PreCont();
    //Original name: B03-PRE-PP-INI
    private B03PrePpIni b03PrePpIni = new B03PrePpIni();
    //Original name: B03-RIS-PURA-T
    private B03RisPuraT b03RisPuraT = new B03RisPuraT();
    //Original name: B03-PROV-ACQ
    private B03ProvAcq b03ProvAcq = new B03ProvAcq();
    //Original name: B03-PROV-ACQ-RICOR
    private B03ProvAcqRicor b03ProvAcqRicor = new B03ProvAcqRicor();
    //Original name: B03-PROV-INC
    private B03ProvInc b03ProvInc = new B03ProvInc();
    //Original name: B03-CAR-ACQ-NON-SCON
    private B03CarAcqNonScon b03CarAcqNonScon = new B03CarAcqNonScon();
    //Original name: B03-OVER-COMM
    private B03OverComm b03OverComm = new B03OverComm();
    //Original name: B03-CAR-ACQ-PRECONTATO
    private B03CarAcqPrecontato b03CarAcqPrecontato = new B03CarAcqPrecontato();
    //Original name: B03-RIS-ACQ-T
    private B03RisAcqT b03RisAcqT = new B03RisAcqT();
    //Original name: B03-RIS-ZIL-T
    private B03RisZilT b03RisZilT = new B03RisZilT();
    //Original name: B03-CAR-GEST-NON-SCON
    private B03CarGestNonScon b03CarGestNonScon = new B03CarGestNonScon();
    //Original name: B03-CAR-GEST
    private B03CarGest b03CarGest = new B03CarGest();
    //Original name: B03-RIS-SPE-T
    private B03RisSpeT b03RisSpeT = new B03RisSpeT();
    //Original name: B03-CAR-INC-NON-SCON
    private B03CarIncNonScon b03CarIncNonScon = new B03CarIncNonScon();
    //Original name: B03-CAR-INC
    private B03CarInc b03CarInc = new B03CarInc();
    //Original name: B03-RIS-RISTORNI-CAP
    private B03RisRistorniCap b03RisRistorniCap = new B03RisRistorniCap();
    //Original name: B03-INTR-TECN
    private B03IntrTecn b03IntrTecn = new B03IntrTecn();
    //Original name: B03-CPT-RSH-MOR
    private B03CptRshMor b03CptRshMor = new B03CptRshMor();
    //Original name: B03-C-SUBRSH-T
    private B03CSubrshT b03CSubrshT = new B03CSubrshT();
    //Original name: B03-PRE-RSH-T
    private B03PreRshT b03PreRshT = new B03PreRshT();
    //Original name: B03-ALQ-MARG-RIS
    private B03AlqMargRis b03AlqMargRis = new B03AlqMargRis();
    //Original name: B03-ALQ-MARG-C-SUBRSH
    private B03AlqMargCSubrsh b03AlqMargCSubrsh = new B03AlqMargCSubrsh();
    //Original name: B03-TS-RENDTO-SPPR
    private B03TsRendtoSppr b03TsRendtoSppr = new B03TsRendtoSppr();
    //Original name: B03-TP-IAS
    private String b03TpIas = DefaultValues.stringVal(Len.B03_TP_IAS);
    //Original name: B03-NS-QUO
    private B03NsQuo b03NsQuo = new B03NsQuo();
    //Original name: B03-TS-MEDIO
    private B03TsMedio b03TsMedio = new B03TsMedio();
    //Original name: B03-CPT-RIASTO
    private B03CptRiasto b03CptRiasto = new B03CptRiasto();
    //Original name: B03-PRE-RIASTO
    private B03PreRiasto b03PreRiasto = new B03PreRiasto();
    //Original name: B03-RIS-RIASTA
    private B03RisRiasta b03RisRiasta = new B03RisRiasta();
    //Original name: B03-CPT-RIASTO-ECC
    private B03CptRiastoEcc b03CptRiastoEcc = new B03CptRiastoEcc();
    //Original name: B03-PRE-RIASTO-ECC
    private B03PreRiastoEcc b03PreRiastoEcc = new B03PreRiastoEcc();
    //Original name: B03-RIS-RIASTA-ECC
    private B03RisRiastaEcc b03RisRiastaEcc = new B03RisRiastaEcc();
    //Original name: B03-COD-AGE
    private B03CodAge b03CodAge = new B03CodAge();
    //Original name: B03-COD-SUBAGE
    private B03CodSubage b03CodSubage = new B03CodSubage();
    //Original name: B03-COD-CAN
    private B03CodCan b03CodCan = new B03CodCan();
    //Original name: B03-IB-POLI
    private String b03IbPoli = DefaultValues.stringVal(Len.B03_IB_POLI);
    //Original name: B03-IB-ADES
    private String b03IbAdes = DefaultValues.stringVal(Len.B03_IB_ADES);
    //Original name: B03-IB-TRCH-DI-GAR
    private String b03IbTrchDiGar = DefaultValues.stringVal(Len.B03_IB_TRCH_DI_GAR);
    //Original name: B03-TP-PRSTZ
    private String b03TpPrstz = DefaultValues.stringVal(Len.B03_TP_PRSTZ);
    //Original name: B03-TP-TRASF
    private String b03TpTrasf = DefaultValues.stringVal(Len.B03_TP_TRASF);
    //Original name: B03-PP-INVRIO-TARI
    private char b03PpInvrioTari = DefaultValues.CHAR_VAL;
    //Original name: B03-COEFF-OPZ-REN
    private B03CoeffOpzRen b03CoeffOpzRen = new B03CoeffOpzRen();
    //Original name: B03-COEFF-OPZ-CPT
    private B03CoeffOpzCpt b03CoeffOpzCpt = new B03CoeffOpzCpt();
    //Original name: B03-DUR-PAG-REN
    private B03DurPagRen b03DurPagRen = new B03DurPagRen();
    //Original name: B03-VLT
    private String b03Vlt = DefaultValues.stringVal(Len.B03_VLT);
    //Original name: B03-RIS-MAT-CHIU-PREC
    private B03RisMatChiuPrec b03RisMatChiuPrec = new B03RisMatChiuPrec();
    //Original name: B03-COD-FND
    private String b03CodFnd = DefaultValues.stringVal(Len.B03_COD_FND);
    //Original name: B03-PRSTZ-T
    private B03PrstzT b03PrstzT = new B03PrstzT();
    //Original name: B03-TS-TARI-DOV
    private B03TsTariDov b03TsTariDov = new B03TsTariDov();
    //Original name: B03-TS-TARI-SCON
    private B03TsTariScon b03TsTariScon = new B03TsTariScon();
    //Original name: B03-TS-PP
    private B03TsPp b03TsPp = new B03TsPp();
    //Original name: B03-COEFF-RIS-1-T
    private B03CoeffRis1T b03CoeffRis1T = new B03CoeffRis1T();
    //Original name: B03-COEFF-RIS-2-T
    private B03CoeffRis2T b03CoeffRis2T = new B03CoeffRis2T();
    //Original name: B03-ABB
    private B03Abb b03Abb = new B03Abb();
    //Original name: B03-TP-COASS
    private String b03TpCoass = DefaultValues.stringVal(Len.B03_TP_COASS);
    //Original name: B03-TRAT-RIASS
    private String b03TratRiass = DefaultValues.stringVal(Len.B03_TRAT_RIASS);
    //Original name: B03-TRAT-RIASS-ECC
    private String b03TratRiassEcc = DefaultValues.stringVal(Len.B03_TRAT_RIASS_ECC);
    //Original name: B03-DS-OPER-SQL
    private char b03DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: B03-DS-VER
    private int b03DsVer = DefaultValues.INT_VAL;
    //Original name: B03-DS-TS-CPTZ
    private long b03DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: B03-DS-UTENTE
    private String b03DsUtente = DefaultValues.stringVal(Len.B03_DS_UTENTE);
    //Original name: B03-DS-STATO-ELAB
    private char b03DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: B03-TP-RGM-FISC
    private String b03TpRgmFisc = DefaultValues.stringVal(Len.B03_TP_RGM_FISC);
    //Original name: B03-DUR-GAR-AA
    private B03DurGarAa b03DurGarAa = new B03DurGarAa();
    //Original name: B03-DUR-GAR-MM
    private B03DurGarMm b03DurGarMm = new B03DurGarMm();
    //Original name: B03-DUR-GAR-GG
    private B03DurGarGg b03DurGarGg = new B03DurGarGg();
    //Original name: B03-ANTIDUR-CALC-365
    private B03AntidurCalc365 b03AntidurCalc365 = new B03AntidurCalc365();
    //Original name: B03-COD-FISC-CNTR
    private String b03CodFiscCntr = DefaultValues.stringVal(Len.B03_COD_FISC_CNTR);
    //Original name: B03-COD-FISC-ASSTO1
    private String b03CodFiscAssto1 = DefaultValues.stringVal(Len.B03_COD_FISC_ASSTO1);
    //Original name: B03-COD-FISC-ASSTO2
    private String b03CodFiscAssto2 = DefaultValues.stringVal(Len.B03_COD_FISC_ASSTO2);
    //Original name: B03-COD-FISC-ASSTO3
    private String b03CodFiscAssto3 = DefaultValues.stringVal(Len.B03_COD_FISC_ASSTO3);
    //Original name: B03-CAUS-SCON-LEN
    private short b03CausSconLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: B03-CAUS-SCON
    private String b03CausScon = DefaultValues.stringVal(Len.B03_CAUS_SCON);
    //Original name: B03-EMIT-TIT-OPZ-LEN
    private short b03EmitTitOpzLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: B03-EMIT-TIT-OPZ
    private String b03EmitTitOpz = DefaultValues.stringVal(Len.B03_EMIT_TIT_OPZ);
    //Original name: B03-QTZ-SP-Z-COUP-EMIS
    private B03QtzSpZCoupEmis b03QtzSpZCoupEmis = new B03QtzSpZCoupEmis();
    //Original name: B03-QTZ-SP-Z-OPZ-EMIS
    private B03QtzSpZOpzEmis b03QtzSpZOpzEmis = new B03QtzSpZOpzEmis();
    //Original name: B03-QTZ-SP-Z-COUP-DT-C
    private B03QtzSpZCoupDtC b03QtzSpZCoupDtC = new B03QtzSpZCoupDtC();
    //Original name: B03-QTZ-SP-Z-OPZ-DT-CA
    private B03QtzSpZOpzDtCa b03QtzSpZOpzDtCa = new B03QtzSpZOpzDtCa();
    //Original name: B03-QTZ-TOT-EMIS
    private B03QtzTotEmis b03QtzTotEmis = new B03QtzTotEmis();
    //Original name: B03-QTZ-TOT-DT-CALC
    private B03QtzTotDtCalc b03QtzTotDtCalc = new B03QtzTotDtCalc();
    //Original name: B03-QTZ-TOT-DT-ULT-BIL
    private B03QtzTotDtUltBil b03QtzTotDtUltBil = new B03QtzTotDtUltBil();
    //Original name: B03-DT-QTZ-EMIS
    private B03DtQtzEmis b03DtQtzEmis = new B03DtQtzEmis();
    //Original name: B03-PC-CAR-GEST
    private B03PcCarGest b03PcCarGest = new B03PcCarGest();
    //Original name: B03-PC-CAR-ACQ
    private B03PcCarAcq b03PcCarAcq = new B03PcCarAcq();
    //Original name: B03-IMP-CAR-CASO-MOR
    private B03ImpCarCasoMor b03ImpCarCasoMor = new B03ImpCarCasoMor();
    //Original name: B03-PC-CAR-MOR
    private B03PcCarMor b03PcCarMor = new B03PcCarMor();
    //Original name: B03-TP-VERS
    private char b03TpVers = DefaultValues.CHAR_VAL;
    //Original name: B03-FL-SWITCH
    private char b03FlSwitch = DefaultValues.CHAR_VAL;
    //Original name: B03-FL-IAS
    private char b03FlIas = DefaultValues.CHAR_VAL;
    //Original name: B03-DIR
    private B03Dir b03Dir = new B03Dir();
    //Original name: B03-TP-COP-CASO-MOR
    private String b03TpCopCasoMor = DefaultValues.stringVal(Len.B03_TP_COP_CASO_MOR);
    //Original name: B03-MET-RISC-SPCL
    private B03MetRiscSpcl b03MetRiscSpcl = new B03MetRiscSpcl();
    //Original name: B03-TP-STAT-INVST
    private String b03TpStatInvst = DefaultValues.stringVal(Len.B03_TP_STAT_INVST);
    //Original name: B03-COD-PRDT
    private B03CodPrdt b03CodPrdt = new B03CodPrdt();
    //Original name: B03-STAT-ASSTO-1
    private char b03StatAssto1 = DefaultValues.CHAR_VAL;
    //Original name: B03-STAT-ASSTO-2
    private char b03StatAssto2 = DefaultValues.CHAR_VAL;
    //Original name: B03-STAT-ASSTO-3
    private char b03StatAssto3 = DefaultValues.CHAR_VAL;
    //Original name: B03-CPT-ASSTO-INI-MOR
    private B03CptAsstoIniMor b03CptAsstoIniMor = new B03CptAsstoIniMor();
    //Original name: B03-TS-STAB-PRE
    private B03TsStabPre b03TsStabPre = new B03TsStabPre();
    //Original name: B03-DIR-EMIS
    private B03DirEmis b03DirEmis = new B03DirEmis();
    //Original name: B03-DT-INC-ULT-PRE
    private B03DtIncUltPre b03DtIncUltPre = new B03DtIncUltPre();
    //Original name: B03-STAT-TBGC-ASSTO-1
    private char b03StatTbgcAssto1 = DefaultValues.CHAR_VAL;
    //Original name: B03-STAT-TBGC-ASSTO-2
    private char b03StatTbgcAssto2 = DefaultValues.CHAR_VAL;
    //Original name: B03-STAT-TBGC-ASSTO-3
    private char b03StatTbgcAssto3 = DefaultValues.CHAR_VAL;
    //Original name: B03-FRAZ-DECR-CPT
    private B03FrazDecrCpt b03FrazDecrCpt = new B03FrazDecrCpt();
    //Original name: B03-PRE-PP-ULT
    private B03PrePpUlt b03PrePpUlt = new B03PrePpUlt();
    //Original name: B03-ACQ-EXP
    private B03AcqExp b03AcqExp = new B03AcqExp();
    //Original name: B03-REMUN-ASS
    private B03RemunAss b03RemunAss = new B03RemunAss();
    //Original name: B03-COMMIS-INTER
    private B03CommisInter b03CommisInter = new B03CommisInter();
    //Original name: B03-NUM-FINANZ
    private String b03NumFinanz = DefaultValues.stringVal(Len.B03_NUM_FINANZ);
    //Original name: B03-TP-ACC-COMM
    private String b03TpAccComm = DefaultValues.stringVal(Len.B03_TP_ACC_COMM);
    //Original name: B03-IB-ACC-COMM
    private String b03IbAccComm = DefaultValues.stringVal(Len.B03_IB_ACC_COMM);
    //Original name: B03-RAMO-BILA
    private String b03RamoBila = DefaultValues.stringVal(Len.B03_RAMO_BILA);
    //Original name: B03-CARZ
    private B03Carz b03Carz = new B03Carz();

    //==== METHODS ====
    public void setBilaTrchEstrFormatted(String data) {
        byte[] buffer = new byte[Len.BILA_TRCH_ESTR];
        MarshalByte.writeString(buffer, 1, data, Len.BILA_TRCH_ESTR);
        setBilaTrchEstrBytes(buffer, 1);
    }

    public String getBilaTrchEstrFormatted() {
        return MarshalByteExt.bufferToStr(getBilaTrchEstrBytes());
    }

    public byte[] getBilaTrchEstrBytes() {
        byte[] buffer = new byte[Len.BILA_TRCH_ESTR];
        return getBilaTrchEstrBytes(buffer, 1);
    }

    public void setBilaTrchEstrBytes(byte[] buffer, int offset) {
        int position = offset;
        b03IdBilaTrchEstr = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_ID_BILA_TRCH_ESTR, 0);
        position += Len.B03_ID_BILA_TRCH_ESTR;
        b03CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_COD_COMP_ANIA, 0);
        position += Len.B03_COD_COMP_ANIA;
        b03IdRichEstrazMas = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.B03_ID_RICH_ESTRAZ_MAS;
        b03IdRichEstrazAgg.setB03IdRichEstrazAggFromBuffer(buffer, position);
        position += B03IdRichEstrazAgg.Len.B03_ID_RICH_ESTRAZ_AGG;
        b03FlSimulazione = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03DtRis = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_DT_RIS, 0);
        position += Len.B03_DT_RIS;
        b03DtProduzione = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_DT_PRODUZIONE, 0);
        position += Len.B03_DT_PRODUZIONE;
        b03IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_ID_POLI, 0);
        position += Len.B03_ID_POLI;
        b03IdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_ID_ADES, 0);
        position += Len.B03_ID_ADES;
        b03IdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_ID_GAR, 0);
        position += Len.B03_ID_GAR;
        b03IdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_ID_TRCH_DI_GAR, 0);
        position += Len.B03_ID_TRCH_DI_GAR;
        b03TpFrmAssva = MarshalByte.readString(buffer, position, Len.B03_TP_FRM_ASSVA);
        position += Len.B03_TP_FRM_ASSVA;
        b03TpRamoBila = MarshalByte.readString(buffer, position, Len.B03_TP_RAMO_BILA);
        position += Len.B03_TP_RAMO_BILA;
        b03TpCalcRis = MarshalByte.readString(buffer, position, Len.B03_TP_CALC_RIS);
        position += Len.B03_TP_CALC_RIS;
        b03CodRamo = MarshalByte.readString(buffer, position, Len.B03_COD_RAMO);
        position += Len.B03_COD_RAMO;
        b03CodTari = MarshalByte.readString(buffer, position, Len.B03_COD_TARI);
        position += Len.B03_COD_TARI;
        b03DtIniValTar.setB03DtIniValTarFromBuffer(buffer, position);
        position += B03DtIniValTar.Len.B03_DT_INI_VAL_TAR;
        b03CodProd = MarshalByte.readString(buffer, position, Len.B03_COD_PROD);
        position += Len.B03_COD_PROD;
        b03DtIniVldtProd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_DT_INI_VLDT_PROD, 0);
        position += Len.B03_DT_INI_VLDT_PROD;
        b03CodTariOrgn = MarshalByte.readString(buffer, position, Len.B03_COD_TARI_ORGN);
        position += Len.B03_COD_TARI_ORGN;
        b03MinGartoT.setB03MinGartoTFromBuffer(buffer, position);
        position += B03MinGartoT.Len.B03_MIN_GARTO_T;
        b03TpTari = MarshalByte.readString(buffer, position, Len.B03_TP_TARI);
        position += Len.B03_TP_TARI;
        b03TpPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03TpAdegPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03TpRival = MarshalByte.readString(buffer, position, Len.B03_TP_RIVAL);
        position += Len.B03_TP_RIVAL;
        b03FlDaTrasf = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03FlCarCont = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03FlPreDaRis = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03FlPreAgg = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03TpTrch = MarshalByte.readString(buffer, position, Len.B03_TP_TRCH);
        position += Len.B03_TP_TRCH;
        b03TpTst = MarshalByte.readString(buffer, position, Len.B03_TP_TST);
        position += Len.B03_TP_TST;
        b03CodConv = MarshalByte.readString(buffer, position, Len.B03_COD_CONV);
        position += Len.B03_COD_CONV;
        b03DtDecorPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_DT_DECOR_POLI, 0);
        position += Len.B03_DT_DECOR_POLI;
        b03DtDecorAdes.setB03DtDecorAdesFromBuffer(buffer, position);
        position += B03DtDecorAdes.Len.B03_DT_DECOR_ADES;
        b03DtDecorTrch = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_DT_DECOR_TRCH, 0);
        position += Len.B03_DT_DECOR_TRCH;
        b03DtEmisPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_DT_EMIS_POLI, 0);
        position += Len.B03_DT_EMIS_POLI;
        b03DtEmisTrch.setB03DtEmisTrchFromBuffer(buffer, position);
        position += B03DtEmisTrch.Len.B03_DT_EMIS_TRCH;
        b03DtScadTrch.setB03DtScadTrchFromBuffer(buffer, position);
        position += B03DtScadTrch.Len.B03_DT_SCAD_TRCH;
        b03DtScadIntmd.setB03DtScadIntmdFromBuffer(buffer, position);
        position += B03DtScadIntmd.Len.B03_DT_SCAD_INTMD;
        b03DtScadPagPre.setB03DtScadPagPreFromBuffer(buffer, position);
        position += B03DtScadPagPre.Len.B03_DT_SCAD_PAG_PRE;
        b03DtUltPrePag.setB03DtUltPrePagFromBuffer(buffer, position);
        position += B03DtUltPrePag.Len.B03_DT_ULT_PRE_PAG;
        b03DtNasc1oAssto.setB03DtNasc1oAsstoFromBuffer(buffer, position);
        position += B03DtNasc1oAssto.Len.B03_DT_NASC1O_ASSTO;
        b03Sex1oAssto = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03EtaAa1oAssto.setB03EtaAa1oAsstoFromBuffer(buffer, position);
        position += B03EtaAa1oAssto.Len.B03_ETA_AA1O_ASSTO;
        b03EtaMm1oAssto.setB03EtaMm1oAsstoFromBuffer(buffer, position);
        position += B03EtaMm1oAssto.Len.B03_ETA_MM1O_ASSTO;
        b03EtaRaggnDtCalc.setB03EtaRaggnDtCalcFromBuffer(buffer, position);
        position += B03EtaRaggnDtCalc.Len.B03_ETA_RAGGN_DT_CALC;
        b03DurAa.setB03DurAaFromBuffer(buffer, position);
        position += B03DurAa.Len.B03_DUR_AA;
        b03DurMm.setB03DurMmFromBuffer(buffer, position);
        position += B03DurMm.Len.B03_DUR_MM;
        b03DurGg.setB03DurGgFromBuffer(buffer, position);
        position += B03DurGg.Len.B03_DUR_GG;
        b03Dur1oPerAa.setB03Dur1oPerAaFromBuffer(buffer, position);
        position += B03Dur1oPerAa.Len.B03_DUR1O_PER_AA;
        b03Dur1oPerMm.setB03Dur1oPerMmFromBuffer(buffer, position);
        position += B03Dur1oPerMm.Len.B03_DUR1O_PER_MM;
        b03Dur1oPerGg.setB03Dur1oPerGgFromBuffer(buffer, position);
        position += B03Dur1oPerGg.Len.B03_DUR1O_PER_GG;
        b03AntidurRicorPrec.setB03AntidurRicorPrecFromBuffer(buffer, position);
        position += B03AntidurRicorPrec.Len.B03_ANTIDUR_RICOR_PREC;
        b03AntidurDtCalc.setB03AntidurDtCalcFromBuffer(buffer, position);
        position += B03AntidurDtCalc.Len.B03_ANTIDUR_DT_CALC;
        b03DurResDtCalc.setB03DurResDtCalcFromBuffer(buffer, position);
        position += B03DurResDtCalc.Len.B03_DUR_RES_DT_CALC;
        b03TpStatBusPoli = MarshalByte.readString(buffer, position, Len.B03_TP_STAT_BUS_POLI);
        position += Len.B03_TP_STAT_BUS_POLI;
        b03TpCausPoli = MarshalByte.readString(buffer, position, Len.B03_TP_CAUS_POLI);
        position += Len.B03_TP_CAUS_POLI;
        b03TpStatBusAdes = MarshalByte.readString(buffer, position, Len.B03_TP_STAT_BUS_ADES);
        position += Len.B03_TP_STAT_BUS_ADES;
        b03TpCausAdes = MarshalByte.readString(buffer, position, Len.B03_TP_CAUS_ADES);
        position += Len.B03_TP_CAUS_ADES;
        b03TpStatBusTrch = MarshalByte.readString(buffer, position, Len.B03_TP_STAT_BUS_TRCH);
        position += Len.B03_TP_STAT_BUS_TRCH;
        b03TpCausTrch = MarshalByte.readString(buffer, position, Len.B03_TP_CAUS_TRCH);
        position += Len.B03_TP_CAUS_TRCH;
        b03DtEffCambStat.setB03DtEffCambStatFromBuffer(buffer, position);
        position += B03DtEffCambStat.Len.B03_DT_EFF_CAMB_STAT;
        b03DtEmisCambStat.setB03DtEmisCambStatFromBuffer(buffer, position);
        position += B03DtEmisCambStat.Len.B03_DT_EMIS_CAMB_STAT;
        b03DtEffStab.setB03DtEffStabFromBuffer(buffer, position);
        position += B03DtEffStab.Len.B03_DT_EFF_STAB;
        b03CptDtStab.setB03CptDtStabFromBuffer(buffer, position);
        position += B03CptDtStab.Len.B03_CPT_DT_STAB;
        b03DtEffRidz.setB03DtEffRidzFromBuffer(buffer, position);
        position += B03DtEffRidz.Len.B03_DT_EFF_RIDZ;
        b03DtEmisRidz.setB03DtEmisRidzFromBuffer(buffer, position);
        position += B03DtEmisRidz.Len.B03_DT_EMIS_RIDZ;
        b03CptDtRidz.setB03CptDtRidzFromBuffer(buffer, position);
        position += B03CptDtRidz.Len.B03_CPT_DT_RIDZ;
        b03Fraz.setB03FrazFromBuffer(buffer, position);
        position += B03Fraz.Len.B03_FRAZ;
        b03DurPagPre.setB03DurPagPreFromBuffer(buffer, position);
        position += B03DurPagPre.Len.B03_DUR_PAG_PRE;
        b03NumPrePatt.setB03NumPrePattFromBuffer(buffer, position);
        position += B03NumPrePatt.Len.B03_NUM_PRE_PATT;
        b03FrazIniErogRen.setB03FrazIniErogRenFromBuffer(buffer, position);
        position += B03FrazIniErogRen.Len.B03_FRAZ_INI_EROG_REN;
        b03AaRenCer.setB03AaRenCerFromBuffer(buffer, position);
        position += B03AaRenCer.Len.B03_AA_REN_CER;
        b03RatRen.setB03RatRenFromBuffer(buffer, position);
        position += B03RatRen.Len.B03_RAT_REN;
        b03CodDiv = MarshalByte.readString(buffer, position, Len.B03_COD_DIV);
        position += Len.B03_COD_DIV;
        b03Riscpar.setB03RiscparFromBuffer(buffer, position);
        position += B03Riscpar.Len.B03_RISCPAR;
        b03CumRiscpar.setB03CumRiscparFromBuffer(buffer, position);
        position += B03CumRiscpar.Len.B03_CUM_RISCPAR;
        b03UltRm.setB03UltRmFromBuffer(buffer, position);
        position += B03UltRm.Len.B03_ULT_RM;
        b03TsRendtoT.setB03TsRendtoTFromBuffer(buffer, position);
        position += B03TsRendtoT.Len.B03_TS_RENDTO_T;
        b03AlqRetrT.setB03AlqRetrTFromBuffer(buffer, position);
        position += B03AlqRetrT.Len.B03_ALQ_RETR_T;
        b03MinTrnutT.setB03MinTrnutTFromBuffer(buffer, position);
        position += B03MinTrnutT.Len.B03_MIN_TRNUT_T;
        b03TsNetT.setB03TsNetTFromBuffer(buffer, position);
        position += B03TsNetT.Len.B03_TS_NET_T;
        b03DtUltRival.setB03DtUltRivalFromBuffer(buffer, position);
        position += B03DtUltRival.Len.B03_DT_ULT_RIVAL;
        b03PrstzIni.setB03PrstzIniFromBuffer(buffer, position);
        position += B03PrstzIni.Len.B03_PRSTZ_INI;
        b03PrstzAggIni.setB03PrstzAggIniFromBuffer(buffer, position);
        position += B03PrstzAggIni.Len.B03_PRSTZ_AGG_INI;
        b03PrstzAggUlt.setB03PrstzAggUltFromBuffer(buffer, position);
        position += B03PrstzAggUlt.Len.B03_PRSTZ_AGG_ULT;
        b03Rappel.setB03RappelFromBuffer(buffer, position);
        position += B03Rappel.Len.B03_RAPPEL;
        b03PrePattuitoIni.setB03PrePattuitoIniFromBuffer(buffer, position);
        position += B03PrePattuitoIni.Len.B03_PRE_PATTUITO_INI;
        b03PreDovIni.setB03PreDovIniFromBuffer(buffer, position);
        position += B03PreDovIni.Len.B03_PRE_DOV_INI;
        b03PreDovRivtoT.setB03PreDovRivtoTFromBuffer(buffer, position);
        position += B03PreDovRivtoT.Len.B03_PRE_DOV_RIVTO_T;
        b03PreAnnualizRicor.setB03PreAnnualizRicorFromBuffer(buffer, position);
        position += B03PreAnnualizRicor.Len.B03_PRE_ANNUALIZ_RICOR;
        b03PreCont.setB03PreContFromBuffer(buffer, position);
        position += B03PreCont.Len.B03_PRE_CONT;
        b03PrePpIni.setB03PrePpIniFromBuffer(buffer, position);
        position += B03PrePpIni.Len.B03_PRE_PP_INI;
        b03RisPuraT.setB03RisPuraTFromBuffer(buffer, position);
        position += B03RisPuraT.Len.B03_RIS_PURA_T;
        b03ProvAcq.setB03ProvAcqFromBuffer(buffer, position);
        position += B03ProvAcq.Len.B03_PROV_ACQ;
        b03ProvAcqRicor.setB03ProvAcqRicorFromBuffer(buffer, position);
        position += B03ProvAcqRicor.Len.B03_PROV_ACQ_RICOR;
        b03ProvInc.setB03ProvIncFromBuffer(buffer, position);
        position += B03ProvInc.Len.B03_PROV_INC;
        b03CarAcqNonScon.setB03CarAcqNonSconFromBuffer(buffer, position);
        position += B03CarAcqNonScon.Len.B03_CAR_ACQ_NON_SCON;
        b03OverComm.setB03OverCommFromBuffer(buffer, position);
        position += B03OverComm.Len.B03_OVER_COMM;
        b03CarAcqPrecontato.setB03CarAcqPrecontatoFromBuffer(buffer, position);
        position += B03CarAcqPrecontato.Len.B03_CAR_ACQ_PRECONTATO;
        b03RisAcqT.setB03RisAcqTFromBuffer(buffer, position);
        position += B03RisAcqT.Len.B03_RIS_ACQ_T;
        b03RisZilT.setB03RisZilTFromBuffer(buffer, position);
        position += B03RisZilT.Len.B03_RIS_ZIL_T;
        b03CarGestNonScon.setB03CarGestNonSconFromBuffer(buffer, position);
        position += B03CarGestNonScon.Len.B03_CAR_GEST_NON_SCON;
        b03CarGest.setB03CarGestFromBuffer(buffer, position);
        position += B03CarGest.Len.B03_CAR_GEST;
        b03RisSpeT.setB03RisSpeTFromBuffer(buffer, position);
        position += B03RisSpeT.Len.B03_RIS_SPE_T;
        b03CarIncNonScon.setB03CarIncNonSconFromBuffer(buffer, position);
        position += B03CarIncNonScon.Len.B03_CAR_INC_NON_SCON;
        b03CarInc.setB03CarIncFromBuffer(buffer, position);
        position += B03CarInc.Len.B03_CAR_INC;
        b03RisRistorniCap.setB03RisRistorniCapFromBuffer(buffer, position);
        position += B03RisRistorniCap.Len.B03_RIS_RISTORNI_CAP;
        b03IntrTecn.setB03IntrTecnFromBuffer(buffer, position);
        position += B03IntrTecn.Len.B03_INTR_TECN;
        b03CptRshMor.setB03CptRshMorFromBuffer(buffer, position);
        position += B03CptRshMor.Len.B03_CPT_RSH_MOR;
        b03CSubrshT.setB03CSubrshTFromBuffer(buffer, position);
        position += B03CSubrshT.Len.B03_C_SUBRSH_T;
        b03PreRshT.setB03PreRshTFromBuffer(buffer, position);
        position += B03PreRshT.Len.B03_PRE_RSH_T;
        b03AlqMargRis.setB03AlqMargRisFromBuffer(buffer, position);
        position += B03AlqMargRis.Len.B03_ALQ_MARG_RIS;
        b03AlqMargCSubrsh.setB03AlqMargCSubrshFromBuffer(buffer, position);
        position += B03AlqMargCSubrsh.Len.B03_ALQ_MARG_C_SUBRSH;
        b03TsRendtoSppr.setB03TsRendtoSpprFromBuffer(buffer, position);
        position += B03TsRendtoSppr.Len.B03_TS_RENDTO_SPPR;
        b03TpIas = MarshalByte.readString(buffer, position, Len.B03_TP_IAS);
        position += Len.B03_TP_IAS;
        b03NsQuo.setB03NsQuoFromBuffer(buffer, position);
        position += B03NsQuo.Len.B03_NS_QUO;
        b03TsMedio.setB03TsMedioFromBuffer(buffer, position);
        position += B03TsMedio.Len.B03_TS_MEDIO;
        b03CptRiasto.setB03CptRiastoFromBuffer(buffer, position);
        position += B03CptRiasto.Len.B03_CPT_RIASTO;
        b03PreRiasto.setB03PreRiastoFromBuffer(buffer, position);
        position += B03PreRiasto.Len.B03_PRE_RIASTO;
        b03RisRiasta.setB03RisRiastaFromBuffer(buffer, position);
        position += B03RisRiasta.Len.B03_RIS_RIASTA;
        b03CptRiastoEcc.setB03CptRiastoEccFromBuffer(buffer, position);
        position += B03CptRiastoEcc.Len.B03_CPT_RIASTO_ECC;
        b03PreRiastoEcc.setB03PreRiastoEccFromBuffer(buffer, position);
        position += B03PreRiastoEcc.Len.B03_PRE_RIASTO_ECC;
        b03RisRiastaEcc.setB03RisRiastaEccFromBuffer(buffer, position);
        position += B03RisRiastaEcc.Len.B03_RIS_RIASTA_ECC;
        b03CodAge.setB03CodAgeFromBuffer(buffer, position);
        position += B03CodAge.Len.B03_COD_AGE;
        b03CodSubage.setB03CodSubageFromBuffer(buffer, position);
        position += B03CodSubage.Len.B03_COD_SUBAGE;
        b03CodCan.setB03CodCanFromBuffer(buffer, position);
        position += B03CodCan.Len.B03_COD_CAN;
        b03IbPoli = MarshalByte.readString(buffer, position, Len.B03_IB_POLI);
        position += Len.B03_IB_POLI;
        b03IbAdes = MarshalByte.readString(buffer, position, Len.B03_IB_ADES);
        position += Len.B03_IB_ADES;
        b03IbTrchDiGar = MarshalByte.readString(buffer, position, Len.B03_IB_TRCH_DI_GAR);
        position += Len.B03_IB_TRCH_DI_GAR;
        b03TpPrstz = MarshalByte.readString(buffer, position, Len.B03_TP_PRSTZ);
        position += Len.B03_TP_PRSTZ;
        b03TpTrasf = MarshalByte.readString(buffer, position, Len.B03_TP_TRASF);
        position += Len.B03_TP_TRASF;
        b03PpInvrioTari = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03CoeffOpzRen.setB03CoeffOpzRenFromBuffer(buffer, position);
        position += B03CoeffOpzRen.Len.B03_COEFF_OPZ_REN;
        b03CoeffOpzCpt.setB03CoeffOpzCptFromBuffer(buffer, position);
        position += B03CoeffOpzCpt.Len.B03_COEFF_OPZ_CPT;
        b03DurPagRen.setB03DurPagRenFromBuffer(buffer, position);
        position += B03DurPagRen.Len.B03_DUR_PAG_REN;
        b03Vlt = MarshalByte.readString(buffer, position, Len.B03_VLT);
        position += Len.B03_VLT;
        b03RisMatChiuPrec.setB03RisMatChiuPrecFromBuffer(buffer, position);
        position += B03RisMatChiuPrec.Len.B03_RIS_MAT_CHIU_PREC;
        b03CodFnd = MarshalByte.readString(buffer, position, Len.B03_COD_FND);
        position += Len.B03_COD_FND;
        b03PrstzT.setB03PrstzTFromBuffer(buffer, position);
        position += B03PrstzT.Len.B03_PRSTZ_T;
        b03TsTariDov.setB03TsTariDovFromBuffer(buffer, position);
        position += B03TsTariDov.Len.B03_TS_TARI_DOV;
        b03TsTariScon.setB03TsTariSconFromBuffer(buffer, position);
        position += B03TsTariScon.Len.B03_TS_TARI_SCON;
        b03TsPp.setB03TsPpFromBuffer(buffer, position);
        position += B03TsPp.Len.B03_TS_PP;
        b03CoeffRis1T.setB03CoeffRis1TFromBuffer(buffer, position);
        position += B03CoeffRis1T.Len.B03_COEFF_RIS1_T;
        b03CoeffRis2T.setB03CoeffRis2TFromBuffer(buffer, position);
        position += B03CoeffRis2T.Len.B03_COEFF_RIS2_T;
        b03Abb.setB03AbbFromBuffer(buffer, position);
        position += B03Abb.Len.B03_ABB;
        b03TpCoass = MarshalByte.readString(buffer, position, Len.B03_TP_COASS);
        position += Len.B03_TP_COASS;
        b03TratRiass = MarshalByte.readString(buffer, position, Len.B03_TRAT_RIASS);
        position += Len.B03_TRAT_RIASS;
        b03TratRiassEcc = MarshalByte.readString(buffer, position, Len.B03_TRAT_RIASS_ECC);
        position += Len.B03_TRAT_RIASS_ECC;
        b03DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B03_DS_VER, 0);
        position += Len.B03_DS_VER;
        b03DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.B03_DS_TS_CPTZ, 0);
        position += Len.B03_DS_TS_CPTZ;
        b03DsUtente = MarshalByte.readString(buffer, position, Len.B03_DS_UTENTE);
        position += Len.B03_DS_UTENTE;
        b03DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03TpRgmFisc = MarshalByte.readString(buffer, position, Len.B03_TP_RGM_FISC);
        position += Len.B03_TP_RGM_FISC;
        b03DurGarAa.setB03DurGarAaFromBuffer(buffer, position);
        position += B03DurGarAa.Len.B03_DUR_GAR_AA;
        b03DurGarMm.setB03DurGarMmFromBuffer(buffer, position);
        position += B03DurGarMm.Len.B03_DUR_GAR_MM;
        b03DurGarGg.setB03DurGarGgFromBuffer(buffer, position);
        position += B03DurGarGg.Len.B03_DUR_GAR_GG;
        b03AntidurCalc365.setB03AntidurCalc365FromBuffer(buffer, position);
        position += B03AntidurCalc365.Len.B03_ANTIDUR_CALC365;
        b03CodFiscCntr = MarshalByte.readString(buffer, position, Len.B03_COD_FISC_CNTR);
        position += Len.B03_COD_FISC_CNTR;
        b03CodFiscAssto1 = MarshalByte.readString(buffer, position, Len.B03_COD_FISC_ASSTO1);
        position += Len.B03_COD_FISC_ASSTO1;
        b03CodFiscAssto2 = MarshalByte.readString(buffer, position, Len.B03_COD_FISC_ASSTO2);
        position += Len.B03_COD_FISC_ASSTO2;
        b03CodFiscAssto3 = MarshalByte.readString(buffer, position, Len.B03_COD_FISC_ASSTO3);
        position += Len.B03_COD_FISC_ASSTO3;
        setB03CausSconVcharBytes(buffer, position);
        position += Len.B03_CAUS_SCON_VCHAR;
        setB03EmitTitOpzVcharBytes(buffer, position);
        position += Len.B03_EMIT_TIT_OPZ_VCHAR;
        b03QtzSpZCoupEmis.setB03QtzSpZCoupEmisFromBuffer(buffer, position);
        position += B03QtzSpZCoupEmis.Len.B03_QTZ_SP_Z_COUP_EMIS;
        b03QtzSpZOpzEmis.setB03QtzSpZOpzEmisFromBuffer(buffer, position);
        position += B03QtzSpZOpzEmis.Len.B03_QTZ_SP_Z_OPZ_EMIS;
        b03QtzSpZCoupDtC.setB03QtzSpZCoupDtCFromBuffer(buffer, position);
        position += B03QtzSpZCoupDtC.Len.B03_QTZ_SP_Z_COUP_DT_C;
        b03QtzSpZOpzDtCa.setB03QtzSpZOpzDtCaFromBuffer(buffer, position);
        position += B03QtzSpZOpzDtCa.Len.B03_QTZ_SP_Z_OPZ_DT_CA;
        b03QtzTotEmis.setB03QtzTotEmisFromBuffer(buffer, position);
        position += B03QtzTotEmis.Len.B03_QTZ_TOT_EMIS;
        b03QtzTotDtCalc.setB03QtzTotDtCalcFromBuffer(buffer, position);
        position += B03QtzTotDtCalc.Len.B03_QTZ_TOT_DT_CALC;
        b03QtzTotDtUltBil.setB03QtzTotDtUltBilFromBuffer(buffer, position);
        position += B03QtzTotDtUltBil.Len.B03_QTZ_TOT_DT_ULT_BIL;
        b03DtQtzEmis.setB03DtQtzEmisFromBuffer(buffer, position);
        position += B03DtQtzEmis.Len.B03_DT_QTZ_EMIS;
        b03PcCarGest.setB03PcCarGestFromBuffer(buffer, position);
        position += B03PcCarGest.Len.B03_PC_CAR_GEST;
        b03PcCarAcq.setB03PcCarAcqFromBuffer(buffer, position);
        position += B03PcCarAcq.Len.B03_PC_CAR_ACQ;
        b03ImpCarCasoMor.setB03ImpCarCasoMorFromBuffer(buffer, position);
        position += B03ImpCarCasoMor.Len.B03_IMP_CAR_CASO_MOR;
        b03PcCarMor.setB03PcCarMorFromBuffer(buffer, position);
        position += B03PcCarMor.Len.B03_PC_CAR_MOR;
        b03TpVers = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03FlSwitch = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03FlIas = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03Dir.setB03DirFromBuffer(buffer, position);
        position += B03Dir.Len.B03_DIR;
        b03TpCopCasoMor = MarshalByte.readString(buffer, position, Len.B03_TP_COP_CASO_MOR);
        position += Len.B03_TP_COP_CASO_MOR;
        b03MetRiscSpcl.setB03MetRiscSpclFromBuffer(buffer, position);
        position += B03MetRiscSpcl.Len.B03_MET_RISC_SPCL;
        b03TpStatInvst = MarshalByte.readString(buffer, position, Len.B03_TP_STAT_INVST);
        position += Len.B03_TP_STAT_INVST;
        b03CodPrdt.setB03CodPrdtFromBuffer(buffer, position);
        position += B03CodPrdt.Len.B03_COD_PRDT;
        b03StatAssto1 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03StatAssto2 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03StatAssto3 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03CptAsstoIniMor.setB03CptAsstoIniMorFromBuffer(buffer, position);
        position += B03CptAsstoIniMor.Len.B03_CPT_ASSTO_INI_MOR;
        b03TsStabPre.setB03TsStabPreFromBuffer(buffer, position);
        position += B03TsStabPre.Len.B03_TS_STAB_PRE;
        b03DirEmis.setB03DirEmisFromBuffer(buffer, position);
        position += B03DirEmis.Len.B03_DIR_EMIS;
        b03DtIncUltPre.setB03DtIncUltPreFromBuffer(buffer, position);
        position += B03DtIncUltPre.Len.B03_DT_INC_ULT_PRE;
        b03StatTbgcAssto1 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03StatTbgcAssto2 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03StatTbgcAssto3 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b03FrazDecrCpt.setB03FrazDecrCptFromBuffer(buffer, position);
        position += B03FrazDecrCpt.Len.B03_FRAZ_DECR_CPT;
        b03PrePpUlt.setB03PrePpUltFromBuffer(buffer, position);
        position += B03PrePpUlt.Len.B03_PRE_PP_ULT;
        b03AcqExp.setB03AcqExpFromBuffer(buffer, position);
        position += B03AcqExp.Len.B03_ACQ_EXP;
        b03RemunAss.setB03RemunAssFromBuffer(buffer, position);
        position += B03RemunAss.Len.B03_REMUN_ASS;
        b03CommisInter.setB03CommisInterFromBuffer(buffer, position);
        position += B03CommisInter.Len.B03_COMMIS_INTER;
        b03NumFinanz = MarshalByte.readString(buffer, position, Len.B03_NUM_FINANZ);
        position += Len.B03_NUM_FINANZ;
        b03TpAccComm = MarshalByte.readString(buffer, position, Len.B03_TP_ACC_COMM);
        position += Len.B03_TP_ACC_COMM;
        b03IbAccComm = MarshalByte.readString(buffer, position, Len.B03_IB_ACC_COMM);
        position += Len.B03_IB_ACC_COMM;
        b03RamoBila = MarshalByte.readString(buffer, position, Len.B03_RAMO_BILA);
        position += Len.B03_RAMO_BILA;
        b03Carz.setB03CarzFromBuffer(buffer, position);
    }

    public byte[] getBilaTrchEstrBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, b03IdBilaTrchEstr, Len.Int.B03_ID_BILA_TRCH_ESTR, 0);
        position += Len.B03_ID_BILA_TRCH_ESTR;
        MarshalByte.writeIntAsPacked(buffer, position, b03CodCompAnia, Len.Int.B03_COD_COMP_ANIA, 0);
        position += Len.B03_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, b03IdRichEstrazMas, Len.Int.B03_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.B03_ID_RICH_ESTRAZ_MAS;
        b03IdRichEstrazAgg.getB03IdRichEstrazAggAsBuffer(buffer, position);
        position += B03IdRichEstrazAgg.Len.B03_ID_RICH_ESTRAZ_AGG;
        MarshalByte.writeChar(buffer, position, b03FlSimulazione);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, b03DtRis, Len.Int.B03_DT_RIS, 0);
        position += Len.B03_DT_RIS;
        MarshalByte.writeIntAsPacked(buffer, position, b03DtProduzione, Len.Int.B03_DT_PRODUZIONE, 0);
        position += Len.B03_DT_PRODUZIONE;
        MarshalByte.writeIntAsPacked(buffer, position, b03IdPoli, Len.Int.B03_ID_POLI, 0);
        position += Len.B03_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, b03IdAdes, Len.Int.B03_ID_ADES, 0);
        position += Len.B03_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, b03IdGar, Len.Int.B03_ID_GAR, 0);
        position += Len.B03_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, b03IdTrchDiGar, Len.Int.B03_ID_TRCH_DI_GAR, 0);
        position += Len.B03_ID_TRCH_DI_GAR;
        MarshalByte.writeString(buffer, position, b03TpFrmAssva, Len.B03_TP_FRM_ASSVA);
        position += Len.B03_TP_FRM_ASSVA;
        MarshalByte.writeString(buffer, position, b03TpRamoBila, Len.B03_TP_RAMO_BILA);
        position += Len.B03_TP_RAMO_BILA;
        MarshalByte.writeString(buffer, position, b03TpCalcRis, Len.B03_TP_CALC_RIS);
        position += Len.B03_TP_CALC_RIS;
        MarshalByte.writeString(buffer, position, b03CodRamo, Len.B03_COD_RAMO);
        position += Len.B03_COD_RAMO;
        MarshalByte.writeString(buffer, position, b03CodTari, Len.B03_COD_TARI);
        position += Len.B03_COD_TARI;
        b03DtIniValTar.getB03DtIniValTarAsBuffer(buffer, position);
        position += B03DtIniValTar.Len.B03_DT_INI_VAL_TAR;
        MarshalByte.writeString(buffer, position, b03CodProd, Len.B03_COD_PROD);
        position += Len.B03_COD_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, b03DtIniVldtProd, Len.Int.B03_DT_INI_VLDT_PROD, 0);
        position += Len.B03_DT_INI_VLDT_PROD;
        MarshalByte.writeString(buffer, position, b03CodTariOrgn, Len.B03_COD_TARI_ORGN);
        position += Len.B03_COD_TARI_ORGN;
        b03MinGartoT.getB03MinGartoTAsBuffer(buffer, position);
        position += B03MinGartoT.Len.B03_MIN_GARTO_T;
        MarshalByte.writeString(buffer, position, b03TpTari, Len.B03_TP_TARI);
        position += Len.B03_TP_TARI;
        MarshalByte.writeChar(buffer, position, b03TpPre);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, b03TpAdegPre);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, b03TpRival, Len.B03_TP_RIVAL);
        position += Len.B03_TP_RIVAL;
        MarshalByte.writeChar(buffer, position, b03FlDaTrasf);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, b03FlCarCont);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, b03FlPreDaRis);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, b03FlPreAgg);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, b03TpTrch, Len.B03_TP_TRCH);
        position += Len.B03_TP_TRCH;
        MarshalByte.writeString(buffer, position, b03TpTst, Len.B03_TP_TST);
        position += Len.B03_TP_TST;
        MarshalByte.writeString(buffer, position, b03CodConv, Len.B03_COD_CONV);
        position += Len.B03_COD_CONV;
        MarshalByte.writeIntAsPacked(buffer, position, b03DtDecorPoli, Len.Int.B03_DT_DECOR_POLI, 0);
        position += Len.B03_DT_DECOR_POLI;
        b03DtDecorAdes.getB03DtDecorAdesAsBuffer(buffer, position);
        position += B03DtDecorAdes.Len.B03_DT_DECOR_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, b03DtDecorTrch, Len.Int.B03_DT_DECOR_TRCH, 0);
        position += Len.B03_DT_DECOR_TRCH;
        MarshalByte.writeIntAsPacked(buffer, position, b03DtEmisPoli, Len.Int.B03_DT_EMIS_POLI, 0);
        position += Len.B03_DT_EMIS_POLI;
        b03DtEmisTrch.getB03DtEmisTrchAsBuffer(buffer, position);
        position += B03DtEmisTrch.Len.B03_DT_EMIS_TRCH;
        b03DtScadTrch.getB03DtScadTrchAsBuffer(buffer, position);
        position += B03DtScadTrch.Len.B03_DT_SCAD_TRCH;
        b03DtScadIntmd.getB03DtScadIntmdAsBuffer(buffer, position);
        position += B03DtScadIntmd.Len.B03_DT_SCAD_INTMD;
        b03DtScadPagPre.getB03DtScadPagPreAsBuffer(buffer, position);
        position += B03DtScadPagPre.Len.B03_DT_SCAD_PAG_PRE;
        b03DtUltPrePag.getB03DtUltPrePagAsBuffer(buffer, position);
        position += B03DtUltPrePag.Len.B03_DT_ULT_PRE_PAG;
        b03DtNasc1oAssto.getB03DtNasc1oAsstoAsBuffer(buffer, position);
        position += B03DtNasc1oAssto.Len.B03_DT_NASC1O_ASSTO;
        MarshalByte.writeChar(buffer, position, b03Sex1oAssto);
        position += Types.CHAR_SIZE;
        b03EtaAa1oAssto.getB03EtaAa1oAsstoAsBuffer(buffer, position);
        position += B03EtaAa1oAssto.Len.B03_ETA_AA1O_ASSTO;
        b03EtaMm1oAssto.getB03EtaMm1oAsstoAsBuffer(buffer, position);
        position += B03EtaMm1oAssto.Len.B03_ETA_MM1O_ASSTO;
        b03EtaRaggnDtCalc.getB03EtaRaggnDtCalcAsBuffer(buffer, position);
        position += B03EtaRaggnDtCalc.Len.B03_ETA_RAGGN_DT_CALC;
        b03DurAa.getB03DurAaAsBuffer(buffer, position);
        position += B03DurAa.Len.B03_DUR_AA;
        b03DurMm.getB03DurMmAsBuffer(buffer, position);
        position += B03DurMm.Len.B03_DUR_MM;
        b03DurGg.getB03DurGgAsBuffer(buffer, position);
        position += B03DurGg.Len.B03_DUR_GG;
        b03Dur1oPerAa.getB03Dur1oPerAaAsBuffer(buffer, position);
        position += B03Dur1oPerAa.Len.B03_DUR1O_PER_AA;
        b03Dur1oPerMm.getB03Dur1oPerMmAsBuffer(buffer, position);
        position += B03Dur1oPerMm.Len.B03_DUR1O_PER_MM;
        b03Dur1oPerGg.getB03Dur1oPerGgAsBuffer(buffer, position);
        position += B03Dur1oPerGg.Len.B03_DUR1O_PER_GG;
        b03AntidurRicorPrec.getB03AntidurRicorPrecAsBuffer(buffer, position);
        position += B03AntidurRicorPrec.Len.B03_ANTIDUR_RICOR_PREC;
        b03AntidurDtCalc.getB03AntidurDtCalcAsBuffer(buffer, position);
        position += B03AntidurDtCalc.Len.B03_ANTIDUR_DT_CALC;
        b03DurResDtCalc.getB03DurResDtCalcAsBuffer(buffer, position);
        position += B03DurResDtCalc.Len.B03_DUR_RES_DT_CALC;
        MarshalByte.writeString(buffer, position, b03TpStatBusPoli, Len.B03_TP_STAT_BUS_POLI);
        position += Len.B03_TP_STAT_BUS_POLI;
        MarshalByte.writeString(buffer, position, b03TpCausPoli, Len.B03_TP_CAUS_POLI);
        position += Len.B03_TP_CAUS_POLI;
        MarshalByte.writeString(buffer, position, b03TpStatBusAdes, Len.B03_TP_STAT_BUS_ADES);
        position += Len.B03_TP_STAT_BUS_ADES;
        MarshalByte.writeString(buffer, position, b03TpCausAdes, Len.B03_TP_CAUS_ADES);
        position += Len.B03_TP_CAUS_ADES;
        MarshalByte.writeString(buffer, position, b03TpStatBusTrch, Len.B03_TP_STAT_BUS_TRCH);
        position += Len.B03_TP_STAT_BUS_TRCH;
        MarshalByte.writeString(buffer, position, b03TpCausTrch, Len.B03_TP_CAUS_TRCH);
        position += Len.B03_TP_CAUS_TRCH;
        b03DtEffCambStat.getB03DtEffCambStatAsBuffer(buffer, position);
        position += B03DtEffCambStat.Len.B03_DT_EFF_CAMB_STAT;
        b03DtEmisCambStat.getB03DtEmisCambStatAsBuffer(buffer, position);
        position += B03DtEmisCambStat.Len.B03_DT_EMIS_CAMB_STAT;
        b03DtEffStab.getB03DtEffStabAsBuffer(buffer, position);
        position += B03DtEffStab.Len.B03_DT_EFF_STAB;
        b03CptDtStab.getB03CptDtStabAsBuffer(buffer, position);
        position += B03CptDtStab.Len.B03_CPT_DT_STAB;
        b03DtEffRidz.getB03DtEffRidzAsBuffer(buffer, position);
        position += B03DtEffRidz.Len.B03_DT_EFF_RIDZ;
        b03DtEmisRidz.getB03DtEmisRidzAsBuffer(buffer, position);
        position += B03DtEmisRidz.Len.B03_DT_EMIS_RIDZ;
        b03CptDtRidz.getB03CptDtRidzAsBuffer(buffer, position);
        position += B03CptDtRidz.Len.B03_CPT_DT_RIDZ;
        b03Fraz.getB03FrazAsBuffer(buffer, position);
        position += B03Fraz.Len.B03_FRAZ;
        b03DurPagPre.getB03DurPagPreAsBuffer(buffer, position);
        position += B03DurPagPre.Len.B03_DUR_PAG_PRE;
        b03NumPrePatt.getB03NumPrePattAsBuffer(buffer, position);
        position += B03NumPrePatt.Len.B03_NUM_PRE_PATT;
        b03FrazIniErogRen.getB03FrazIniErogRenAsBuffer(buffer, position);
        position += B03FrazIniErogRen.Len.B03_FRAZ_INI_EROG_REN;
        b03AaRenCer.getB03AaRenCerAsBuffer(buffer, position);
        position += B03AaRenCer.Len.B03_AA_REN_CER;
        b03RatRen.getB03RatRenAsBuffer(buffer, position);
        position += B03RatRen.Len.B03_RAT_REN;
        MarshalByte.writeString(buffer, position, b03CodDiv, Len.B03_COD_DIV);
        position += Len.B03_COD_DIV;
        b03Riscpar.getB03RiscparAsBuffer(buffer, position);
        position += B03Riscpar.Len.B03_RISCPAR;
        b03CumRiscpar.getB03CumRiscparAsBuffer(buffer, position);
        position += B03CumRiscpar.Len.B03_CUM_RISCPAR;
        b03UltRm.getB03UltRmAsBuffer(buffer, position);
        position += B03UltRm.Len.B03_ULT_RM;
        b03TsRendtoT.getB03TsRendtoTAsBuffer(buffer, position);
        position += B03TsRendtoT.Len.B03_TS_RENDTO_T;
        b03AlqRetrT.getB03AlqRetrTAsBuffer(buffer, position);
        position += B03AlqRetrT.Len.B03_ALQ_RETR_T;
        b03MinTrnutT.getB03MinTrnutTAsBuffer(buffer, position);
        position += B03MinTrnutT.Len.B03_MIN_TRNUT_T;
        b03TsNetT.getB03TsNetTAsBuffer(buffer, position);
        position += B03TsNetT.Len.B03_TS_NET_T;
        b03DtUltRival.getB03DtUltRivalAsBuffer(buffer, position);
        position += B03DtUltRival.Len.B03_DT_ULT_RIVAL;
        b03PrstzIni.getB03PrstzIniAsBuffer(buffer, position);
        position += B03PrstzIni.Len.B03_PRSTZ_INI;
        b03PrstzAggIni.getB03PrstzAggIniAsBuffer(buffer, position);
        position += B03PrstzAggIni.Len.B03_PRSTZ_AGG_INI;
        b03PrstzAggUlt.getB03PrstzAggUltAsBuffer(buffer, position);
        position += B03PrstzAggUlt.Len.B03_PRSTZ_AGG_ULT;
        b03Rappel.getB03RappelAsBuffer(buffer, position);
        position += B03Rappel.Len.B03_RAPPEL;
        b03PrePattuitoIni.getB03PrePattuitoIniAsBuffer(buffer, position);
        position += B03PrePattuitoIni.Len.B03_PRE_PATTUITO_INI;
        b03PreDovIni.getB03PreDovIniAsBuffer(buffer, position);
        position += B03PreDovIni.Len.B03_PRE_DOV_INI;
        b03PreDovRivtoT.getB03PreDovRivtoTAsBuffer(buffer, position);
        position += B03PreDovRivtoT.Len.B03_PRE_DOV_RIVTO_T;
        b03PreAnnualizRicor.getB03PreAnnualizRicorAsBuffer(buffer, position);
        position += B03PreAnnualizRicor.Len.B03_PRE_ANNUALIZ_RICOR;
        b03PreCont.getB03PreContAsBuffer(buffer, position);
        position += B03PreCont.Len.B03_PRE_CONT;
        b03PrePpIni.getB03PrePpIniAsBuffer(buffer, position);
        position += B03PrePpIni.Len.B03_PRE_PP_INI;
        b03RisPuraT.getB03RisPuraTAsBuffer(buffer, position);
        position += B03RisPuraT.Len.B03_RIS_PURA_T;
        b03ProvAcq.getB03ProvAcqAsBuffer(buffer, position);
        position += B03ProvAcq.Len.B03_PROV_ACQ;
        b03ProvAcqRicor.getB03ProvAcqRicorAsBuffer(buffer, position);
        position += B03ProvAcqRicor.Len.B03_PROV_ACQ_RICOR;
        b03ProvInc.getB03ProvIncAsBuffer(buffer, position);
        position += B03ProvInc.Len.B03_PROV_INC;
        b03CarAcqNonScon.getB03CarAcqNonSconAsBuffer(buffer, position);
        position += B03CarAcqNonScon.Len.B03_CAR_ACQ_NON_SCON;
        b03OverComm.getB03OverCommAsBuffer(buffer, position);
        position += B03OverComm.Len.B03_OVER_COMM;
        b03CarAcqPrecontato.getB03CarAcqPrecontatoAsBuffer(buffer, position);
        position += B03CarAcqPrecontato.Len.B03_CAR_ACQ_PRECONTATO;
        b03RisAcqT.getB03RisAcqTAsBuffer(buffer, position);
        position += B03RisAcqT.Len.B03_RIS_ACQ_T;
        b03RisZilT.getB03RisZilTAsBuffer(buffer, position);
        position += B03RisZilT.Len.B03_RIS_ZIL_T;
        b03CarGestNonScon.getB03CarGestNonSconAsBuffer(buffer, position);
        position += B03CarGestNonScon.Len.B03_CAR_GEST_NON_SCON;
        b03CarGest.getB03CarGestAsBuffer(buffer, position);
        position += B03CarGest.Len.B03_CAR_GEST;
        b03RisSpeT.getB03RisSpeTAsBuffer(buffer, position);
        position += B03RisSpeT.Len.B03_RIS_SPE_T;
        b03CarIncNonScon.getB03CarIncNonSconAsBuffer(buffer, position);
        position += B03CarIncNonScon.Len.B03_CAR_INC_NON_SCON;
        b03CarInc.getB03CarIncAsBuffer(buffer, position);
        position += B03CarInc.Len.B03_CAR_INC;
        b03RisRistorniCap.getB03RisRistorniCapAsBuffer(buffer, position);
        position += B03RisRistorniCap.Len.B03_RIS_RISTORNI_CAP;
        b03IntrTecn.getB03IntrTecnAsBuffer(buffer, position);
        position += B03IntrTecn.Len.B03_INTR_TECN;
        b03CptRshMor.getB03CptRshMorAsBuffer(buffer, position);
        position += B03CptRshMor.Len.B03_CPT_RSH_MOR;
        b03CSubrshT.getB03CSubrshTAsBuffer(buffer, position);
        position += B03CSubrshT.Len.B03_C_SUBRSH_T;
        b03PreRshT.getB03PreRshTAsBuffer(buffer, position);
        position += B03PreRshT.Len.B03_PRE_RSH_T;
        b03AlqMargRis.getB03AlqMargRisAsBuffer(buffer, position);
        position += B03AlqMargRis.Len.B03_ALQ_MARG_RIS;
        b03AlqMargCSubrsh.getB03AlqMargCSubrshAsBuffer(buffer, position);
        position += B03AlqMargCSubrsh.Len.B03_ALQ_MARG_C_SUBRSH;
        b03TsRendtoSppr.getB03TsRendtoSpprAsBuffer(buffer, position);
        position += B03TsRendtoSppr.Len.B03_TS_RENDTO_SPPR;
        MarshalByte.writeString(buffer, position, b03TpIas, Len.B03_TP_IAS);
        position += Len.B03_TP_IAS;
        b03NsQuo.getB03NsQuoAsBuffer(buffer, position);
        position += B03NsQuo.Len.B03_NS_QUO;
        b03TsMedio.getB03TsMedioAsBuffer(buffer, position);
        position += B03TsMedio.Len.B03_TS_MEDIO;
        b03CptRiasto.getB03CptRiastoAsBuffer(buffer, position);
        position += B03CptRiasto.Len.B03_CPT_RIASTO;
        b03PreRiasto.getB03PreRiastoAsBuffer(buffer, position);
        position += B03PreRiasto.Len.B03_PRE_RIASTO;
        b03RisRiasta.getB03RisRiastaAsBuffer(buffer, position);
        position += B03RisRiasta.Len.B03_RIS_RIASTA;
        b03CptRiastoEcc.getB03CptRiastoEccAsBuffer(buffer, position);
        position += B03CptRiastoEcc.Len.B03_CPT_RIASTO_ECC;
        b03PreRiastoEcc.getB03PreRiastoEccAsBuffer(buffer, position);
        position += B03PreRiastoEcc.Len.B03_PRE_RIASTO_ECC;
        b03RisRiastaEcc.getB03RisRiastaEccAsBuffer(buffer, position);
        position += B03RisRiastaEcc.Len.B03_RIS_RIASTA_ECC;
        b03CodAge.getB03CodAgeAsBuffer(buffer, position);
        position += B03CodAge.Len.B03_COD_AGE;
        b03CodSubage.getB03CodSubageAsBuffer(buffer, position);
        position += B03CodSubage.Len.B03_COD_SUBAGE;
        b03CodCan.getB03CodCanAsBuffer(buffer, position);
        position += B03CodCan.Len.B03_COD_CAN;
        MarshalByte.writeString(buffer, position, b03IbPoli, Len.B03_IB_POLI);
        position += Len.B03_IB_POLI;
        MarshalByte.writeString(buffer, position, b03IbAdes, Len.B03_IB_ADES);
        position += Len.B03_IB_ADES;
        MarshalByte.writeString(buffer, position, b03IbTrchDiGar, Len.B03_IB_TRCH_DI_GAR);
        position += Len.B03_IB_TRCH_DI_GAR;
        MarshalByte.writeString(buffer, position, b03TpPrstz, Len.B03_TP_PRSTZ);
        position += Len.B03_TP_PRSTZ;
        MarshalByte.writeString(buffer, position, b03TpTrasf, Len.B03_TP_TRASF);
        position += Len.B03_TP_TRASF;
        MarshalByte.writeChar(buffer, position, b03PpInvrioTari);
        position += Types.CHAR_SIZE;
        b03CoeffOpzRen.getB03CoeffOpzRenAsBuffer(buffer, position);
        position += B03CoeffOpzRen.Len.B03_COEFF_OPZ_REN;
        b03CoeffOpzCpt.getB03CoeffOpzCptAsBuffer(buffer, position);
        position += B03CoeffOpzCpt.Len.B03_COEFF_OPZ_CPT;
        b03DurPagRen.getB03DurPagRenAsBuffer(buffer, position);
        position += B03DurPagRen.Len.B03_DUR_PAG_REN;
        MarshalByte.writeString(buffer, position, b03Vlt, Len.B03_VLT);
        position += Len.B03_VLT;
        b03RisMatChiuPrec.getB03RisMatChiuPrecAsBuffer(buffer, position);
        position += B03RisMatChiuPrec.Len.B03_RIS_MAT_CHIU_PREC;
        MarshalByte.writeString(buffer, position, b03CodFnd, Len.B03_COD_FND);
        position += Len.B03_COD_FND;
        b03PrstzT.getB03PrstzTAsBuffer(buffer, position);
        position += B03PrstzT.Len.B03_PRSTZ_T;
        b03TsTariDov.getB03TsTariDovAsBuffer(buffer, position);
        position += B03TsTariDov.Len.B03_TS_TARI_DOV;
        b03TsTariScon.getB03TsTariSconAsBuffer(buffer, position);
        position += B03TsTariScon.Len.B03_TS_TARI_SCON;
        b03TsPp.getB03TsPpAsBuffer(buffer, position);
        position += B03TsPp.Len.B03_TS_PP;
        b03CoeffRis1T.getB03CoeffRis1TAsBuffer(buffer, position);
        position += B03CoeffRis1T.Len.B03_COEFF_RIS1_T;
        b03CoeffRis2T.getB03CoeffRis2TAsBuffer(buffer, position);
        position += B03CoeffRis2T.Len.B03_COEFF_RIS2_T;
        b03Abb.getB03AbbAsBuffer(buffer, position);
        position += B03Abb.Len.B03_ABB;
        MarshalByte.writeString(buffer, position, b03TpCoass, Len.B03_TP_COASS);
        position += Len.B03_TP_COASS;
        MarshalByte.writeString(buffer, position, b03TratRiass, Len.B03_TRAT_RIASS);
        position += Len.B03_TRAT_RIASS;
        MarshalByte.writeString(buffer, position, b03TratRiassEcc, Len.B03_TRAT_RIASS_ECC);
        position += Len.B03_TRAT_RIASS_ECC;
        MarshalByte.writeChar(buffer, position, b03DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, b03DsVer, Len.Int.B03_DS_VER, 0);
        position += Len.B03_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, b03DsTsCptz, Len.Int.B03_DS_TS_CPTZ, 0);
        position += Len.B03_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, b03DsUtente, Len.B03_DS_UTENTE);
        position += Len.B03_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, b03DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, b03TpRgmFisc, Len.B03_TP_RGM_FISC);
        position += Len.B03_TP_RGM_FISC;
        b03DurGarAa.getB03DurGarAaAsBuffer(buffer, position);
        position += B03DurGarAa.Len.B03_DUR_GAR_AA;
        b03DurGarMm.getB03DurGarMmAsBuffer(buffer, position);
        position += B03DurGarMm.Len.B03_DUR_GAR_MM;
        b03DurGarGg.getB03DurGarGgAsBuffer(buffer, position);
        position += B03DurGarGg.Len.B03_DUR_GAR_GG;
        b03AntidurCalc365.getB03AntidurCalc365AsBuffer(buffer, position);
        position += B03AntidurCalc365.Len.B03_ANTIDUR_CALC365;
        MarshalByte.writeString(buffer, position, b03CodFiscCntr, Len.B03_COD_FISC_CNTR);
        position += Len.B03_COD_FISC_CNTR;
        MarshalByte.writeString(buffer, position, b03CodFiscAssto1, Len.B03_COD_FISC_ASSTO1);
        position += Len.B03_COD_FISC_ASSTO1;
        MarshalByte.writeString(buffer, position, b03CodFiscAssto2, Len.B03_COD_FISC_ASSTO2);
        position += Len.B03_COD_FISC_ASSTO2;
        MarshalByte.writeString(buffer, position, b03CodFiscAssto3, Len.B03_COD_FISC_ASSTO3);
        position += Len.B03_COD_FISC_ASSTO3;
        getB03CausSconVcharBytes(buffer, position);
        position += Len.B03_CAUS_SCON_VCHAR;
        getB03EmitTitOpzVcharBytes(buffer, position);
        position += Len.B03_EMIT_TIT_OPZ_VCHAR;
        b03QtzSpZCoupEmis.getB03QtzSpZCoupEmisAsBuffer(buffer, position);
        position += B03QtzSpZCoupEmis.Len.B03_QTZ_SP_Z_COUP_EMIS;
        b03QtzSpZOpzEmis.getB03QtzSpZOpzEmisAsBuffer(buffer, position);
        position += B03QtzSpZOpzEmis.Len.B03_QTZ_SP_Z_OPZ_EMIS;
        b03QtzSpZCoupDtC.getB03QtzSpZCoupDtCAsBuffer(buffer, position);
        position += B03QtzSpZCoupDtC.Len.B03_QTZ_SP_Z_COUP_DT_C;
        b03QtzSpZOpzDtCa.getB03QtzSpZOpzDtCaAsBuffer(buffer, position);
        position += B03QtzSpZOpzDtCa.Len.B03_QTZ_SP_Z_OPZ_DT_CA;
        b03QtzTotEmis.getB03QtzTotEmisAsBuffer(buffer, position);
        position += B03QtzTotEmis.Len.B03_QTZ_TOT_EMIS;
        b03QtzTotDtCalc.getB03QtzTotDtCalcAsBuffer(buffer, position);
        position += B03QtzTotDtCalc.Len.B03_QTZ_TOT_DT_CALC;
        b03QtzTotDtUltBil.getB03QtzTotDtUltBilAsBuffer(buffer, position);
        position += B03QtzTotDtUltBil.Len.B03_QTZ_TOT_DT_ULT_BIL;
        b03DtQtzEmis.getB03DtQtzEmisAsBuffer(buffer, position);
        position += B03DtQtzEmis.Len.B03_DT_QTZ_EMIS;
        b03PcCarGest.getB03PcCarGestAsBuffer(buffer, position);
        position += B03PcCarGest.Len.B03_PC_CAR_GEST;
        b03PcCarAcq.getB03PcCarAcqAsBuffer(buffer, position);
        position += B03PcCarAcq.Len.B03_PC_CAR_ACQ;
        b03ImpCarCasoMor.getB03ImpCarCasoMorAsBuffer(buffer, position);
        position += B03ImpCarCasoMor.Len.B03_IMP_CAR_CASO_MOR;
        b03PcCarMor.getB03PcCarMorAsBuffer(buffer, position);
        position += B03PcCarMor.Len.B03_PC_CAR_MOR;
        MarshalByte.writeChar(buffer, position, b03TpVers);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, b03FlSwitch);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, b03FlIas);
        position += Types.CHAR_SIZE;
        b03Dir.getB03DirAsBuffer(buffer, position);
        position += B03Dir.Len.B03_DIR;
        MarshalByte.writeString(buffer, position, b03TpCopCasoMor, Len.B03_TP_COP_CASO_MOR);
        position += Len.B03_TP_COP_CASO_MOR;
        b03MetRiscSpcl.getB03MetRiscSpclAsBuffer(buffer, position);
        position += B03MetRiscSpcl.Len.B03_MET_RISC_SPCL;
        MarshalByte.writeString(buffer, position, b03TpStatInvst, Len.B03_TP_STAT_INVST);
        position += Len.B03_TP_STAT_INVST;
        b03CodPrdt.getB03CodPrdtAsBuffer(buffer, position);
        position += B03CodPrdt.Len.B03_COD_PRDT;
        MarshalByte.writeChar(buffer, position, b03StatAssto1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, b03StatAssto2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, b03StatAssto3);
        position += Types.CHAR_SIZE;
        b03CptAsstoIniMor.getB03CptAsstoIniMorAsBuffer(buffer, position);
        position += B03CptAsstoIniMor.Len.B03_CPT_ASSTO_INI_MOR;
        b03TsStabPre.getB03TsStabPreAsBuffer(buffer, position);
        position += B03TsStabPre.Len.B03_TS_STAB_PRE;
        b03DirEmis.getB03DirEmisAsBuffer(buffer, position);
        position += B03DirEmis.Len.B03_DIR_EMIS;
        b03DtIncUltPre.getB03DtIncUltPreAsBuffer(buffer, position);
        position += B03DtIncUltPre.Len.B03_DT_INC_ULT_PRE;
        MarshalByte.writeChar(buffer, position, b03StatTbgcAssto1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, b03StatTbgcAssto2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, b03StatTbgcAssto3);
        position += Types.CHAR_SIZE;
        b03FrazDecrCpt.getB03FrazDecrCptAsBuffer(buffer, position);
        position += B03FrazDecrCpt.Len.B03_FRAZ_DECR_CPT;
        b03PrePpUlt.getB03PrePpUltAsBuffer(buffer, position);
        position += B03PrePpUlt.Len.B03_PRE_PP_ULT;
        b03AcqExp.getB03AcqExpAsBuffer(buffer, position);
        position += B03AcqExp.Len.B03_ACQ_EXP;
        b03RemunAss.getB03RemunAssAsBuffer(buffer, position);
        position += B03RemunAss.Len.B03_REMUN_ASS;
        b03CommisInter.getB03CommisInterAsBuffer(buffer, position);
        position += B03CommisInter.Len.B03_COMMIS_INTER;
        MarshalByte.writeString(buffer, position, b03NumFinanz, Len.B03_NUM_FINANZ);
        position += Len.B03_NUM_FINANZ;
        MarshalByte.writeString(buffer, position, b03TpAccComm, Len.B03_TP_ACC_COMM);
        position += Len.B03_TP_ACC_COMM;
        MarshalByte.writeString(buffer, position, b03IbAccComm, Len.B03_IB_ACC_COMM);
        position += Len.B03_IB_ACC_COMM;
        MarshalByte.writeString(buffer, position, b03RamoBila, Len.B03_RAMO_BILA);
        position += Len.B03_RAMO_BILA;
        b03Carz.getB03CarzAsBuffer(buffer, position);
        return buffer;
    }

    public void setB03IdBilaTrchEstr(int b03IdBilaTrchEstr) {
        this.b03IdBilaTrchEstr = b03IdBilaTrchEstr;
    }

    public int getB03IdBilaTrchEstr() {
        return this.b03IdBilaTrchEstr;
    }

    public void setB03CodCompAnia(int b03CodCompAnia) {
        this.b03CodCompAnia = b03CodCompAnia;
    }

    public int getB03CodCompAnia() {
        return this.b03CodCompAnia;
    }

    public void setB03IdRichEstrazMas(int b03IdRichEstrazMas) {
        this.b03IdRichEstrazMas = b03IdRichEstrazMas;
    }

    public int getB03IdRichEstrazMas() {
        return this.b03IdRichEstrazMas;
    }

    public void setB03FlSimulazione(char b03FlSimulazione) {
        this.b03FlSimulazione = b03FlSimulazione;
    }

    public char getB03FlSimulazione() {
        return this.b03FlSimulazione;
    }

    public void setB03DtRis(int b03DtRis) {
        this.b03DtRis = b03DtRis;
    }

    public int getB03DtRis() {
        return this.b03DtRis;
    }

    public void setB03DtProduzione(int b03DtProduzione) {
        this.b03DtProduzione = b03DtProduzione;
    }

    public int getB03DtProduzione() {
        return this.b03DtProduzione;
    }

    public void setB03IdPoli(int b03IdPoli) {
        this.b03IdPoli = b03IdPoli;
    }

    public int getB03IdPoli() {
        return this.b03IdPoli;
    }

    public void setB03IdAdes(int b03IdAdes) {
        this.b03IdAdes = b03IdAdes;
    }

    public int getB03IdAdes() {
        return this.b03IdAdes;
    }

    public void setB03IdGar(int b03IdGar) {
        this.b03IdGar = b03IdGar;
    }

    public int getB03IdGar() {
        return this.b03IdGar;
    }

    public void setB03IdTrchDiGar(int b03IdTrchDiGar) {
        this.b03IdTrchDiGar = b03IdTrchDiGar;
    }

    public int getB03IdTrchDiGar() {
        return this.b03IdTrchDiGar;
    }

    public void setB03TpFrmAssva(String b03TpFrmAssva) {
        this.b03TpFrmAssva = Functions.subString(b03TpFrmAssva, Len.B03_TP_FRM_ASSVA);
    }

    public String getB03TpFrmAssva() {
        return this.b03TpFrmAssva;
    }

    public void setB03TpRamoBila(String b03TpRamoBila) {
        this.b03TpRamoBila = Functions.subString(b03TpRamoBila, Len.B03_TP_RAMO_BILA);
    }

    public String getB03TpRamoBila() {
        return this.b03TpRamoBila;
    }

    public void setB03TpCalcRis(String b03TpCalcRis) {
        this.b03TpCalcRis = Functions.subString(b03TpCalcRis, Len.B03_TP_CALC_RIS);
    }

    public String getB03TpCalcRis() {
        return this.b03TpCalcRis;
    }

    public void setB03CodRamo(String b03CodRamo) {
        this.b03CodRamo = Functions.subString(b03CodRamo, Len.B03_COD_RAMO);
    }

    public String getB03CodRamo() {
        return this.b03CodRamo;
    }

    public void setB03CodTari(String b03CodTari) {
        this.b03CodTari = Functions.subString(b03CodTari, Len.B03_COD_TARI);
    }

    public String getB03CodTari() {
        return this.b03CodTari;
    }

    public void setB03CodProd(String b03CodProd) {
        this.b03CodProd = Functions.subString(b03CodProd, Len.B03_COD_PROD);
    }

    public String getB03CodProd() {
        return this.b03CodProd;
    }

    public void setB03DtIniVldtProd(int b03DtIniVldtProd) {
        this.b03DtIniVldtProd = b03DtIniVldtProd;
    }

    public int getB03DtIniVldtProd() {
        return this.b03DtIniVldtProd;
    }

    public void setB03CodTariOrgn(String b03CodTariOrgn) {
        this.b03CodTariOrgn = Functions.subString(b03CodTariOrgn, Len.B03_COD_TARI_ORGN);
    }

    public String getB03CodTariOrgn() {
        return this.b03CodTariOrgn;
    }

    public void setB03TpTari(String b03TpTari) {
        this.b03TpTari = Functions.subString(b03TpTari, Len.B03_TP_TARI);
    }

    public String getB03TpTari() {
        return this.b03TpTari;
    }

    public void setB03TpPre(char b03TpPre) {
        this.b03TpPre = b03TpPre;
    }

    public char getB03TpPre() {
        return this.b03TpPre;
    }

    public void setB03TpAdegPre(char b03TpAdegPre) {
        this.b03TpAdegPre = b03TpAdegPre;
    }

    public char getB03TpAdegPre() {
        return this.b03TpAdegPre;
    }

    public void setB03TpRival(String b03TpRival) {
        this.b03TpRival = Functions.subString(b03TpRival, Len.B03_TP_RIVAL);
    }

    public String getB03TpRival() {
        return this.b03TpRival;
    }

    public void setB03FlDaTrasf(char b03FlDaTrasf) {
        this.b03FlDaTrasf = b03FlDaTrasf;
    }

    public char getB03FlDaTrasf() {
        return this.b03FlDaTrasf;
    }

    public void setB03FlCarCont(char b03FlCarCont) {
        this.b03FlCarCont = b03FlCarCont;
    }

    public char getB03FlCarCont() {
        return this.b03FlCarCont;
    }

    public void setB03FlPreDaRis(char b03FlPreDaRis) {
        this.b03FlPreDaRis = b03FlPreDaRis;
    }

    public char getB03FlPreDaRis() {
        return this.b03FlPreDaRis;
    }

    public void setB03FlPreAgg(char b03FlPreAgg) {
        this.b03FlPreAgg = b03FlPreAgg;
    }

    public char getB03FlPreAgg() {
        return this.b03FlPreAgg;
    }

    public void setB03TpTrch(String b03TpTrch) {
        this.b03TpTrch = Functions.subString(b03TpTrch, Len.B03_TP_TRCH);
    }

    public String getB03TpTrch() {
        return this.b03TpTrch;
    }

    public void setB03TpTst(String b03TpTst) {
        this.b03TpTst = Functions.subString(b03TpTst, Len.B03_TP_TST);
    }

    public String getB03TpTst() {
        return this.b03TpTst;
    }

    public void setB03CodConv(String b03CodConv) {
        this.b03CodConv = Functions.subString(b03CodConv, Len.B03_COD_CONV);
    }

    public String getB03CodConv() {
        return this.b03CodConv;
    }

    public void setB03DtDecorPoli(int b03DtDecorPoli) {
        this.b03DtDecorPoli = b03DtDecorPoli;
    }

    public int getB03DtDecorPoli() {
        return this.b03DtDecorPoli;
    }

    public void setB03DtDecorTrch(int b03DtDecorTrch) {
        this.b03DtDecorTrch = b03DtDecorTrch;
    }

    public int getB03DtDecorTrch() {
        return this.b03DtDecorTrch;
    }

    public void setB03DtEmisPoli(int b03DtEmisPoli) {
        this.b03DtEmisPoli = b03DtEmisPoli;
    }

    public int getB03DtEmisPoli() {
        return this.b03DtEmisPoli;
    }

    public void setB03Sex1oAssto(char b03Sex1oAssto) {
        this.b03Sex1oAssto = b03Sex1oAssto;
    }

    public char getB03Sex1oAssto() {
        return this.b03Sex1oAssto;
    }

    public void setB03TpStatBusPoli(String b03TpStatBusPoli) {
        this.b03TpStatBusPoli = Functions.subString(b03TpStatBusPoli, Len.B03_TP_STAT_BUS_POLI);
    }

    public String getB03TpStatBusPoli() {
        return this.b03TpStatBusPoli;
    }

    public void setB03TpCausPoli(String b03TpCausPoli) {
        this.b03TpCausPoli = Functions.subString(b03TpCausPoli, Len.B03_TP_CAUS_POLI);
    }

    public String getB03TpCausPoli() {
        return this.b03TpCausPoli;
    }

    public void setB03TpStatBusAdes(String b03TpStatBusAdes) {
        this.b03TpStatBusAdes = Functions.subString(b03TpStatBusAdes, Len.B03_TP_STAT_BUS_ADES);
    }

    public String getB03TpStatBusAdes() {
        return this.b03TpStatBusAdes;
    }

    public void setB03TpCausAdes(String b03TpCausAdes) {
        this.b03TpCausAdes = Functions.subString(b03TpCausAdes, Len.B03_TP_CAUS_ADES);
    }

    public String getB03TpCausAdes() {
        return this.b03TpCausAdes;
    }

    public void setB03TpStatBusTrch(String b03TpStatBusTrch) {
        this.b03TpStatBusTrch = Functions.subString(b03TpStatBusTrch, Len.B03_TP_STAT_BUS_TRCH);
    }

    public String getB03TpStatBusTrch() {
        return this.b03TpStatBusTrch;
    }

    public void setB03TpCausTrch(String b03TpCausTrch) {
        this.b03TpCausTrch = Functions.subString(b03TpCausTrch, Len.B03_TP_CAUS_TRCH);
    }

    public String getB03TpCausTrch() {
        return this.b03TpCausTrch;
    }

    public void setB03CodDiv(String b03CodDiv) {
        this.b03CodDiv = Functions.subString(b03CodDiv, Len.B03_COD_DIV);
    }

    public String getB03CodDiv() {
        return this.b03CodDiv;
    }

    public void setB03TpIas(String b03TpIas) {
        this.b03TpIas = Functions.subString(b03TpIas, Len.B03_TP_IAS);
    }

    public String getB03TpIas() {
        return this.b03TpIas;
    }

    public void setB03IbPoli(String b03IbPoli) {
        this.b03IbPoli = Functions.subString(b03IbPoli, Len.B03_IB_POLI);
    }

    public String getB03IbPoli() {
        return this.b03IbPoli;
    }

    public void setB03IbAdes(String b03IbAdes) {
        this.b03IbAdes = Functions.subString(b03IbAdes, Len.B03_IB_ADES);
    }

    public String getB03IbAdes() {
        return this.b03IbAdes;
    }

    public void setB03IbTrchDiGar(String b03IbTrchDiGar) {
        this.b03IbTrchDiGar = Functions.subString(b03IbTrchDiGar, Len.B03_IB_TRCH_DI_GAR);
    }

    public String getB03IbTrchDiGar() {
        return this.b03IbTrchDiGar;
    }

    public void setB03TpPrstz(String b03TpPrstz) {
        this.b03TpPrstz = Functions.subString(b03TpPrstz, Len.B03_TP_PRSTZ);
    }

    public String getB03TpPrstz() {
        return this.b03TpPrstz;
    }

    public void setB03TpTrasf(String b03TpTrasf) {
        this.b03TpTrasf = Functions.subString(b03TpTrasf, Len.B03_TP_TRASF);
    }

    public String getB03TpTrasf() {
        return this.b03TpTrasf;
    }

    public void setB03PpInvrioTari(char b03PpInvrioTari) {
        this.b03PpInvrioTari = b03PpInvrioTari;
    }

    public char getB03PpInvrioTari() {
        return this.b03PpInvrioTari;
    }

    public void setB03Vlt(String b03Vlt) {
        this.b03Vlt = Functions.subString(b03Vlt, Len.B03_VLT);
    }

    public String getB03Vlt() {
        return this.b03Vlt;
    }

    public void setB03CodFnd(String b03CodFnd) {
        this.b03CodFnd = Functions.subString(b03CodFnd, Len.B03_COD_FND);
    }

    public String getB03CodFnd() {
        return this.b03CodFnd;
    }

    public void setB03TpCoass(String b03TpCoass) {
        this.b03TpCoass = Functions.subString(b03TpCoass, Len.B03_TP_COASS);
    }

    public String getB03TpCoass() {
        return this.b03TpCoass;
    }

    public void setB03TratRiass(String b03TratRiass) {
        this.b03TratRiass = Functions.subString(b03TratRiass, Len.B03_TRAT_RIASS);
    }

    public String getB03TratRiass() {
        return this.b03TratRiass;
    }

    public void setB03TratRiassEcc(String b03TratRiassEcc) {
        this.b03TratRiassEcc = Functions.subString(b03TratRiassEcc, Len.B03_TRAT_RIASS_ECC);
    }

    public String getB03TratRiassEcc() {
        return this.b03TratRiassEcc;
    }

    public void setB03DsOperSql(char b03DsOperSql) {
        this.b03DsOperSql = b03DsOperSql;
    }

    public char getB03DsOperSql() {
        return this.b03DsOperSql;
    }

    public void setB03DsVer(int b03DsVer) {
        this.b03DsVer = b03DsVer;
    }

    public int getB03DsVer() {
        return this.b03DsVer;
    }

    public void setB03DsTsCptz(long b03DsTsCptz) {
        this.b03DsTsCptz = b03DsTsCptz;
    }

    public long getB03DsTsCptz() {
        return this.b03DsTsCptz;
    }

    public void setB03DsUtente(String b03DsUtente) {
        this.b03DsUtente = Functions.subString(b03DsUtente, Len.B03_DS_UTENTE);
    }

    public String getB03DsUtente() {
        return this.b03DsUtente;
    }

    public void setB03DsStatoElab(char b03DsStatoElab) {
        this.b03DsStatoElab = b03DsStatoElab;
    }

    public char getB03DsStatoElab() {
        return this.b03DsStatoElab;
    }

    public void setB03TpRgmFisc(String b03TpRgmFisc) {
        this.b03TpRgmFisc = Functions.subString(b03TpRgmFisc, Len.B03_TP_RGM_FISC);
    }

    public String getB03TpRgmFisc() {
        return this.b03TpRgmFisc;
    }

    public void setB03CodFiscCntr(String b03CodFiscCntr) {
        this.b03CodFiscCntr = Functions.subString(b03CodFiscCntr, Len.B03_COD_FISC_CNTR);
    }

    public String getB03CodFiscCntr() {
        return this.b03CodFiscCntr;
    }

    public void setB03CodFiscAssto1(String b03CodFiscAssto1) {
        this.b03CodFiscAssto1 = Functions.subString(b03CodFiscAssto1, Len.B03_COD_FISC_ASSTO1);
    }

    public String getB03CodFiscAssto1() {
        return this.b03CodFiscAssto1;
    }

    public void setB03CodFiscAssto2(String b03CodFiscAssto2) {
        this.b03CodFiscAssto2 = Functions.subString(b03CodFiscAssto2, Len.B03_COD_FISC_ASSTO2);
    }

    public String getB03CodFiscAssto2() {
        return this.b03CodFiscAssto2;
    }

    public void setB03CodFiscAssto3(String b03CodFiscAssto3) {
        this.b03CodFiscAssto3 = Functions.subString(b03CodFiscAssto3, Len.B03_COD_FISC_ASSTO3);
    }

    public String getB03CodFiscAssto3() {
        return this.b03CodFiscAssto3;
    }

    public void setB03CausSconVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        b03CausSconLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        b03CausScon = MarshalByte.readString(buffer, position, Len.B03_CAUS_SCON);
    }

    public byte[] getB03CausSconVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, b03CausSconLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, b03CausScon, Len.B03_CAUS_SCON);
        return buffer;
    }

    public void setB03CausSconLen(short b03CausSconLen) {
        this.b03CausSconLen = b03CausSconLen;
    }

    public short getB03CausSconLen() {
        return this.b03CausSconLen;
    }

    public void setB03CausScon(String b03CausScon) {
        this.b03CausScon = Functions.subString(b03CausScon, Len.B03_CAUS_SCON);
    }

    public String getB03CausScon() {
        return this.b03CausScon;
    }

    public void setB03EmitTitOpzVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        b03EmitTitOpzLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        b03EmitTitOpz = MarshalByte.readString(buffer, position, Len.B03_EMIT_TIT_OPZ);
    }

    public byte[] getB03EmitTitOpzVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, b03EmitTitOpzLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, b03EmitTitOpz, Len.B03_EMIT_TIT_OPZ);
        return buffer;
    }

    public void setB03EmitTitOpzLen(short b03EmitTitOpzLen) {
        this.b03EmitTitOpzLen = b03EmitTitOpzLen;
    }

    public short getB03EmitTitOpzLen() {
        return this.b03EmitTitOpzLen;
    }

    public void setB03EmitTitOpz(String b03EmitTitOpz) {
        this.b03EmitTitOpz = Functions.subString(b03EmitTitOpz, Len.B03_EMIT_TIT_OPZ);
    }

    public String getB03EmitTitOpz() {
        return this.b03EmitTitOpz;
    }

    public void setB03TpVers(char b03TpVers) {
        this.b03TpVers = b03TpVers;
    }

    public char getB03TpVers() {
        return this.b03TpVers;
    }

    public void setB03FlSwitch(char b03FlSwitch) {
        this.b03FlSwitch = b03FlSwitch;
    }

    public char getB03FlSwitch() {
        return this.b03FlSwitch;
    }

    public void setB03FlIas(char b03FlIas) {
        this.b03FlIas = b03FlIas;
    }

    public char getB03FlIas() {
        return this.b03FlIas;
    }

    public void setB03TpCopCasoMor(String b03TpCopCasoMor) {
        this.b03TpCopCasoMor = Functions.subString(b03TpCopCasoMor, Len.B03_TP_COP_CASO_MOR);
    }

    public String getB03TpCopCasoMor() {
        return this.b03TpCopCasoMor;
    }

    public void setB03TpStatInvst(String b03TpStatInvst) {
        this.b03TpStatInvst = Functions.subString(b03TpStatInvst, Len.B03_TP_STAT_INVST);
    }

    public String getB03TpStatInvst() {
        return this.b03TpStatInvst;
    }

    public void setB03StatAssto1(char b03StatAssto1) {
        this.b03StatAssto1 = b03StatAssto1;
    }

    public char getB03StatAssto1() {
        return this.b03StatAssto1;
    }

    public void setB03StatAssto2(char b03StatAssto2) {
        this.b03StatAssto2 = b03StatAssto2;
    }

    public char getB03StatAssto2() {
        return this.b03StatAssto2;
    }

    public void setB03StatAssto3(char b03StatAssto3) {
        this.b03StatAssto3 = b03StatAssto3;
    }

    public char getB03StatAssto3() {
        return this.b03StatAssto3;
    }

    public void setB03StatTbgcAssto1(char b03StatTbgcAssto1) {
        this.b03StatTbgcAssto1 = b03StatTbgcAssto1;
    }

    public char getB03StatTbgcAssto1() {
        return this.b03StatTbgcAssto1;
    }

    public void setB03StatTbgcAssto2(char b03StatTbgcAssto2) {
        this.b03StatTbgcAssto2 = b03StatTbgcAssto2;
    }

    public char getB03StatTbgcAssto2() {
        return this.b03StatTbgcAssto2;
    }

    public void setB03StatTbgcAssto3(char b03StatTbgcAssto3) {
        this.b03StatTbgcAssto3 = b03StatTbgcAssto3;
    }

    public char getB03StatTbgcAssto3() {
        return this.b03StatTbgcAssto3;
    }

    public void setB03NumFinanz(String b03NumFinanz) {
        this.b03NumFinanz = Functions.subString(b03NumFinanz, Len.B03_NUM_FINANZ);
    }

    public String getB03NumFinanz() {
        return this.b03NumFinanz;
    }

    public void setB03TpAccComm(String b03TpAccComm) {
        this.b03TpAccComm = Functions.subString(b03TpAccComm, Len.B03_TP_ACC_COMM);
    }

    public String getB03TpAccComm() {
        return this.b03TpAccComm;
    }

    public void setB03IbAccComm(String b03IbAccComm) {
        this.b03IbAccComm = Functions.subString(b03IbAccComm, Len.B03_IB_ACC_COMM);
    }

    public String getB03IbAccComm() {
        return this.b03IbAccComm;
    }

    public void setB03RamoBila(String b03RamoBila) {
        this.b03RamoBila = Functions.subString(b03RamoBila, Len.B03_RAMO_BILA);
    }

    public String getB03RamoBila() {
        return this.b03RamoBila;
    }

    public B03AaRenCer getB03AaRenCer() {
        return b03AaRenCer;
    }

    public B03Abb getB03Abb() {
        return b03Abb;
    }

    public B03AcqExp getB03AcqExp() {
        return b03AcqExp;
    }

    public B03AlqMargCSubrsh getB03AlqMargCSubrsh() {
        return b03AlqMargCSubrsh;
    }

    public B03AlqMargRis getB03AlqMargRis() {
        return b03AlqMargRis;
    }

    public B03AlqRetrT getB03AlqRetrT() {
        return b03AlqRetrT;
    }

    public B03AntidurCalc365 getB03AntidurCalc365() {
        return b03AntidurCalc365;
    }

    public B03AntidurDtCalc getB03AntidurDtCalc() {
        return b03AntidurDtCalc;
    }

    public B03AntidurRicorPrec getB03AntidurRicorPrec() {
        return b03AntidurRicorPrec;
    }

    public B03CSubrshT getB03CSubrshT() {
        return b03CSubrshT;
    }

    public B03CarAcqNonScon getB03CarAcqNonScon() {
        return b03CarAcqNonScon;
    }

    public B03CarAcqPrecontato getB03CarAcqPrecontato() {
        return b03CarAcqPrecontato;
    }

    public B03CarGest getB03CarGest() {
        return b03CarGest;
    }

    public B03CarGestNonScon getB03CarGestNonScon() {
        return b03CarGestNonScon;
    }

    public B03CarInc getB03CarInc() {
        return b03CarInc;
    }

    public B03CarIncNonScon getB03CarIncNonScon() {
        return b03CarIncNonScon;
    }

    public B03Carz getB03Carz() {
        return b03Carz;
    }

    public B03CodAge getB03CodAge() {
        return b03CodAge;
    }

    public B03CodCan getB03CodCan() {
        return b03CodCan;
    }

    public B03CodPrdt getB03CodPrdt() {
        return b03CodPrdt;
    }

    public B03CodSubage getB03CodSubage() {
        return b03CodSubage;
    }

    public B03CoeffOpzCpt getB03CoeffOpzCpt() {
        return b03CoeffOpzCpt;
    }

    public B03CoeffOpzRen getB03CoeffOpzRen() {
        return b03CoeffOpzRen;
    }

    public B03CoeffRis1T getB03CoeffRis1T() {
        return b03CoeffRis1T;
    }

    public B03CoeffRis2T getB03CoeffRis2T() {
        return b03CoeffRis2T;
    }

    public B03CommisInter getB03CommisInter() {
        return b03CommisInter;
    }

    public B03CptAsstoIniMor getB03CptAsstoIniMor() {
        return b03CptAsstoIniMor;
    }

    public B03CptDtRidz getB03CptDtRidz() {
        return b03CptDtRidz;
    }

    public B03CptDtStab getB03CptDtStab() {
        return b03CptDtStab;
    }

    public B03CptRiasto getB03CptRiasto() {
        return b03CptRiasto;
    }

    public B03CptRiastoEcc getB03CptRiastoEcc() {
        return b03CptRiastoEcc;
    }

    public B03CptRshMor getB03CptRshMor() {
        return b03CptRshMor;
    }

    public B03CumRiscpar getB03CumRiscpar() {
        return b03CumRiscpar;
    }

    public B03Dir getB03Dir() {
        return b03Dir;
    }

    public B03DirEmis getB03DirEmis() {
        return b03DirEmis;
    }

    public B03DtDecorAdes getB03DtDecorAdes() {
        return b03DtDecorAdes;
    }

    public B03DtEffCambStat getB03DtEffCambStat() {
        return b03DtEffCambStat;
    }

    public B03DtEffRidz getB03DtEffRidz() {
        return b03DtEffRidz;
    }

    public B03DtEffStab getB03DtEffStab() {
        return b03DtEffStab;
    }

    public B03DtEmisCambStat getB03DtEmisCambStat() {
        return b03DtEmisCambStat;
    }

    public B03DtEmisRidz getB03DtEmisRidz() {
        return b03DtEmisRidz;
    }

    public B03DtEmisTrch getB03DtEmisTrch() {
        return b03DtEmisTrch;
    }

    public B03DtIncUltPre getB03DtIncUltPre() {
        return b03DtIncUltPre;
    }

    public B03DtIniValTar getB03DtIniValTar() {
        return b03DtIniValTar;
    }

    public B03DtNasc1oAssto getB03DtNasc1oAssto() {
        return b03DtNasc1oAssto;
    }

    public B03DtQtzEmis getB03DtQtzEmis() {
        return b03DtQtzEmis;
    }

    public B03DtScadIntmd getB03DtScadIntmd() {
        return b03DtScadIntmd;
    }

    public B03DtScadPagPre getB03DtScadPagPre() {
        return b03DtScadPagPre;
    }

    public B03DtScadTrch getB03DtScadTrch() {
        return b03DtScadTrch;
    }

    public B03DtUltPrePag getB03DtUltPrePag() {
        return b03DtUltPrePag;
    }

    public B03DtUltRival getB03DtUltRival() {
        return b03DtUltRival;
    }

    public B03Dur1oPerAa getB03Dur1oPerAa() {
        return b03Dur1oPerAa;
    }

    public B03Dur1oPerGg getB03Dur1oPerGg() {
        return b03Dur1oPerGg;
    }

    public B03Dur1oPerMm getB03Dur1oPerMm() {
        return b03Dur1oPerMm;
    }

    public B03DurAa getB03DurAa() {
        return b03DurAa;
    }

    public B03DurGarAa getB03DurGarAa() {
        return b03DurGarAa;
    }

    public B03DurGarGg getB03DurGarGg() {
        return b03DurGarGg;
    }

    public B03DurGarMm getB03DurGarMm() {
        return b03DurGarMm;
    }

    public B03DurGg getB03DurGg() {
        return b03DurGg;
    }

    public B03DurMm getB03DurMm() {
        return b03DurMm;
    }

    public B03DurPagPre getB03DurPagPre() {
        return b03DurPagPre;
    }

    public B03DurPagRen getB03DurPagRen() {
        return b03DurPagRen;
    }

    public B03DurResDtCalc getB03DurResDtCalc() {
        return b03DurResDtCalc;
    }

    public B03EtaAa1oAssto getB03EtaAa1oAssto() {
        return b03EtaAa1oAssto;
    }

    public B03EtaMm1oAssto getB03EtaMm1oAssto() {
        return b03EtaMm1oAssto;
    }

    public B03EtaRaggnDtCalc getB03EtaRaggnDtCalc() {
        return b03EtaRaggnDtCalc;
    }

    public B03Fraz getB03Fraz() {
        return b03Fraz;
    }

    public B03FrazDecrCpt getB03FrazDecrCpt() {
        return b03FrazDecrCpt;
    }

    public B03FrazIniErogRen getB03FrazIniErogRen() {
        return b03FrazIniErogRen;
    }

    public B03IdRichEstrazAgg getB03IdRichEstrazAgg() {
        return b03IdRichEstrazAgg;
    }

    public B03ImpCarCasoMor getB03ImpCarCasoMor() {
        return b03ImpCarCasoMor;
    }

    public B03IntrTecn getB03IntrTecn() {
        return b03IntrTecn;
    }

    public B03MetRiscSpcl getB03MetRiscSpcl() {
        return b03MetRiscSpcl;
    }

    public B03MinGartoT getB03MinGartoT() {
        return b03MinGartoT;
    }

    public B03MinTrnutT getB03MinTrnutT() {
        return b03MinTrnutT;
    }

    public B03NsQuo getB03NsQuo() {
        return b03NsQuo;
    }

    public B03NumPrePatt getB03NumPrePatt() {
        return b03NumPrePatt;
    }

    public B03OverComm getB03OverComm() {
        return b03OverComm;
    }

    public B03PcCarAcq getB03PcCarAcq() {
        return b03PcCarAcq;
    }

    public B03PcCarGest getB03PcCarGest() {
        return b03PcCarGest;
    }

    public B03PcCarMor getB03PcCarMor() {
        return b03PcCarMor;
    }

    public B03PreAnnualizRicor getB03PreAnnualizRicor() {
        return b03PreAnnualizRicor;
    }

    public B03PreCont getB03PreCont() {
        return b03PreCont;
    }

    public B03PreDovIni getB03PreDovIni() {
        return b03PreDovIni;
    }

    public B03PreDovRivtoT getB03PreDovRivtoT() {
        return b03PreDovRivtoT;
    }

    public B03PrePattuitoIni getB03PrePattuitoIni() {
        return b03PrePattuitoIni;
    }

    public B03PrePpIni getB03PrePpIni() {
        return b03PrePpIni;
    }

    public B03PrePpUlt getB03PrePpUlt() {
        return b03PrePpUlt;
    }

    public B03PreRiasto getB03PreRiasto() {
        return b03PreRiasto;
    }

    public B03PreRiastoEcc getB03PreRiastoEcc() {
        return b03PreRiastoEcc;
    }

    public B03PreRshT getB03PreRshT() {
        return b03PreRshT;
    }

    public B03ProvAcq getB03ProvAcq() {
        return b03ProvAcq;
    }

    public B03ProvAcqRicor getB03ProvAcqRicor() {
        return b03ProvAcqRicor;
    }

    public B03ProvInc getB03ProvInc() {
        return b03ProvInc;
    }

    public B03PrstzAggIni getB03PrstzAggIni() {
        return b03PrstzAggIni;
    }

    public B03PrstzAggUlt getB03PrstzAggUlt() {
        return b03PrstzAggUlt;
    }

    public B03PrstzIni getB03PrstzIni() {
        return b03PrstzIni;
    }

    public B03PrstzT getB03PrstzT() {
        return b03PrstzT;
    }

    public B03QtzSpZCoupDtC getB03QtzSpZCoupDtC() {
        return b03QtzSpZCoupDtC;
    }

    public B03QtzSpZCoupEmis getB03QtzSpZCoupEmis() {
        return b03QtzSpZCoupEmis;
    }

    public B03QtzSpZOpzDtCa getB03QtzSpZOpzDtCa() {
        return b03QtzSpZOpzDtCa;
    }

    public B03QtzSpZOpzEmis getB03QtzSpZOpzEmis() {
        return b03QtzSpZOpzEmis;
    }

    public B03QtzTotDtCalc getB03QtzTotDtCalc() {
        return b03QtzTotDtCalc;
    }

    public B03QtzTotDtUltBil getB03QtzTotDtUltBil() {
        return b03QtzTotDtUltBil;
    }

    public B03QtzTotEmis getB03QtzTotEmis() {
        return b03QtzTotEmis;
    }

    public B03Rappel getB03Rappel() {
        return b03Rappel;
    }

    public B03RatRen getB03RatRen() {
        return b03RatRen;
    }

    public B03RemunAss getB03RemunAss() {
        return b03RemunAss;
    }

    public B03RisAcqT getB03RisAcqT() {
        return b03RisAcqT;
    }

    public B03RisMatChiuPrec getB03RisMatChiuPrec() {
        return b03RisMatChiuPrec;
    }

    public B03RisPuraT getB03RisPuraT() {
        return b03RisPuraT;
    }

    public B03RisRiasta getB03RisRiasta() {
        return b03RisRiasta;
    }

    public B03RisRiastaEcc getB03RisRiastaEcc() {
        return b03RisRiastaEcc;
    }

    public B03RisRistorniCap getB03RisRistorniCap() {
        return b03RisRistorniCap;
    }

    public B03RisSpeT getB03RisSpeT() {
        return b03RisSpeT;
    }

    public B03RisZilT getB03RisZilT() {
        return b03RisZilT;
    }

    public B03Riscpar getB03Riscpar() {
        return b03Riscpar;
    }

    public B03TsMedio getB03TsMedio() {
        return b03TsMedio;
    }

    public B03TsNetT getB03TsNetT() {
        return b03TsNetT;
    }

    public B03TsPp getB03TsPp() {
        return b03TsPp;
    }

    public B03TsRendtoSppr getB03TsRendtoSppr() {
        return b03TsRendtoSppr;
    }

    public B03TsRendtoT getB03TsRendtoT() {
        return b03TsRendtoT;
    }

    public B03TsStabPre getB03TsStabPre() {
        return b03TsStabPre;
    }

    public B03TsTariDov getB03TsTariDov() {
        return b03TsTariDov;
    }

    public B03TsTariScon getB03TsTariScon() {
        return b03TsTariScon;
    }

    public B03UltRm getB03UltRm() {
        return b03UltRm;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_TP_FRM_ASSVA = 2;
        public static final int B03_TP_RAMO_BILA = 2;
        public static final int B03_TP_CALC_RIS = 2;
        public static final int B03_COD_RAMO = 12;
        public static final int B03_COD_TARI = 12;
        public static final int B03_COD_PROD = 12;
        public static final int B03_COD_TARI_ORGN = 12;
        public static final int B03_TP_TARI = 2;
        public static final int B03_TP_RIVAL = 2;
        public static final int B03_TP_TRCH = 2;
        public static final int B03_TP_TST = 2;
        public static final int B03_COD_CONV = 12;
        public static final int B03_TP_STAT_BUS_POLI = 2;
        public static final int B03_TP_CAUS_POLI = 2;
        public static final int B03_TP_STAT_BUS_ADES = 2;
        public static final int B03_TP_CAUS_ADES = 2;
        public static final int B03_TP_STAT_BUS_TRCH = 2;
        public static final int B03_TP_CAUS_TRCH = 2;
        public static final int B03_COD_DIV = 20;
        public static final int B03_TP_IAS = 2;
        public static final int B03_IB_POLI = 40;
        public static final int B03_IB_ADES = 40;
        public static final int B03_IB_TRCH_DI_GAR = 40;
        public static final int B03_TP_PRSTZ = 2;
        public static final int B03_TP_TRASF = 2;
        public static final int B03_VLT = 3;
        public static final int B03_COD_FND = 12;
        public static final int B03_TP_COASS = 2;
        public static final int B03_TRAT_RIASS = 12;
        public static final int B03_TRAT_RIASS_ECC = 12;
        public static final int B03_DS_UTENTE = 20;
        public static final int B03_TP_RGM_FISC = 2;
        public static final int B03_COD_FISC_CNTR = 16;
        public static final int B03_COD_FISC_ASSTO1 = 16;
        public static final int B03_COD_FISC_ASSTO2 = 16;
        public static final int B03_COD_FISC_ASSTO3 = 16;
        public static final int B03_CAUS_SCON = 100;
        public static final int B03_EMIT_TIT_OPZ = 100;
        public static final int B03_TP_COP_CASO_MOR = 2;
        public static final int B03_TP_STAT_INVST = 2;
        public static final int B03_NUM_FINANZ = 40;
        public static final int B03_TP_ACC_COMM = 2;
        public static final int B03_IB_ACC_COMM = 40;
        public static final int B03_RAMO_BILA = 12;
        public static final int B03_ID_BILA_TRCH_ESTR = 5;
        public static final int B03_COD_COMP_ANIA = 3;
        public static final int B03_ID_RICH_ESTRAZ_MAS = 5;
        public static final int B03_FL_SIMULAZIONE = 1;
        public static final int B03_DT_RIS = 5;
        public static final int B03_DT_PRODUZIONE = 5;
        public static final int B03_ID_POLI = 5;
        public static final int B03_ID_ADES = 5;
        public static final int B03_ID_GAR = 5;
        public static final int B03_ID_TRCH_DI_GAR = 5;
        public static final int B03_DT_INI_VLDT_PROD = 5;
        public static final int B03_TP_PRE = 1;
        public static final int B03_TP_ADEG_PRE = 1;
        public static final int B03_FL_DA_TRASF = 1;
        public static final int B03_FL_CAR_CONT = 1;
        public static final int B03_FL_PRE_DA_RIS = 1;
        public static final int B03_FL_PRE_AGG = 1;
        public static final int B03_DT_DECOR_POLI = 5;
        public static final int B03_DT_DECOR_TRCH = 5;
        public static final int B03_DT_EMIS_POLI = 5;
        public static final int B03_SEX1O_ASSTO = 1;
        public static final int B03_PP_INVRIO_TARI = 1;
        public static final int B03_DS_OPER_SQL = 1;
        public static final int B03_DS_VER = 5;
        public static final int B03_DS_TS_CPTZ = 10;
        public static final int B03_DS_STATO_ELAB = 1;
        public static final int B03_CAUS_SCON_LEN = 2;
        public static final int B03_CAUS_SCON_VCHAR = B03_CAUS_SCON_LEN + B03_CAUS_SCON;
        public static final int B03_EMIT_TIT_OPZ_LEN = 2;
        public static final int B03_EMIT_TIT_OPZ_VCHAR = B03_EMIT_TIT_OPZ_LEN + B03_EMIT_TIT_OPZ;
        public static final int B03_TP_VERS = 1;
        public static final int B03_FL_SWITCH = 1;
        public static final int B03_FL_IAS = 1;
        public static final int B03_STAT_ASSTO1 = 1;
        public static final int B03_STAT_ASSTO2 = 1;
        public static final int B03_STAT_ASSTO3 = 1;
        public static final int B03_STAT_TBGC_ASSTO1 = 1;
        public static final int B03_STAT_TBGC_ASSTO2 = 1;
        public static final int B03_STAT_TBGC_ASSTO3 = 1;
        public static final int BILA_TRCH_ESTR = B03_ID_BILA_TRCH_ESTR + B03_COD_COMP_ANIA + B03_ID_RICH_ESTRAZ_MAS + B03IdRichEstrazAgg.Len.B03_ID_RICH_ESTRAZ_AGG + B03_FL_SIMULAZIONE + B03_DT_RIS + B03_DT_PRODUZIONE + B03_ID_POLI + B03_ID_ADES + B03_ID_GAR + B03_ID_TRCH_DI_GAR + B03_TP_FRM_ASSVA + B03_TP_RAMO_BILA + B03_TP_CALC_RIS + B03_COD_RAMO + B03_COD_TARI + B03DtIniValTar.Len.B03_DT_INI_VAL_TAR + B03_COD_PROD + B03_DT_INI_VLDT_PROD + B03_COD_TARI_ORGN + B03MinGartoT.Len.B03_MIN_GARTO_T + B03_TP_TARI + B03_TP_PRE + B03_TP_ADEG_PRE + B03_TP_RIVAL + B03_FL_DA_TRASF + B03_FL_CAR_CONT + B03_FL_PRE_DA_RIS + B03_FL_PRE_AGG + B03_TP_TRCH + B03_TP_TST + B03_COD_CONV + B03_DT_DECOR_POLI + B03DtDecorAdes.Len.B03_DT_DECOR_ADES + B03_DT_DECOR_TRCH + B03_DT_EMIS_POLI + B03DtEmisTrch.Len.B03_DT_EMIS_TRCH + B03DtScadTrch.Len.B03_DT_SCAD_TRCH + B03DtScadIntmd.Len.B03_DT_SCAD_INTMD + B03DtScadPagPre.Len.B03_DT_SCAD_PAG_PRE + B03DtUltPrePag.Len.B03_DT_ULT_PRE_PAG + B03DtNasc1oAssto.Len.B03_DT_NASC1O_ASSTO + B03_SEX1O_ASSTO + B03EtaAa1oAssto.Len.B03_ETA_AA1O_ASSTO + B03EtaMm1oAssto.Len.B03_ETA_MM1O_ASSTO + B03EtaRaggnDtCalc.Len.B03_ETA_RAGGN_DT_CALC + B03DurAa.Len.B03_DUR_AA + B03DurMm.Len.B03_DUR_MM + B03DurGg.Len.B03_DUR_GG + B03Dur1oPerAa.Len.B03_DUR1O_PER_AA + B03Dur1oPerMm.Len.B03_DUR1O_PER_MM + B03Dur1oPerGg.Len.B03_DUR1O_PER_GG + B03AntidurRicorPrec.Len.B03_ANTIDUR_RICOR_PREC + B03AntidurDtCalc.Len.B03_ANTIDUR_DT_CALC + B03DurResDtCalc.Len.B03_DUR_RES_DT_CALC + B03_TP_STAT_BUS_POLI + B03_TP_CAUS_POLI + B03_TP_STAT_BUS_ADES + B03_TP_CAUS_ADES + B03_TP_STAT_BUS_TRCH + B03_TP_CAUS_TRCH + B03DtEffCambStat.Len.B03_DT_EFF_CAMB_STAT + B03DtEmisCambStat.Len.B03_DT_EMIS_CAMB_STAT + B03DtEffStab.Len.B03_DT_EFF_STAB + B03CptDtStab.Len.B03_CPT_DT_STAB + B03DtEffRidz.Len.B03_DT_EFF_RIDZ + B03DtEmisRidz.Len.B03_DT_EMIS_RIDZ + B03CptDtRidz.Len.B03_CPT_DT_RIDZ + B03Fraz.Len.B03_FRAZ + B03DurPagPre.Len.B03_DUR_PAG_PRE + B03NumPrePatt.Len.B03_NUM_PRE_PATT + B03FrazIniErogRen.Len.B03_FRAZ_INI_EROG_REN + B03AaRenCer.Len.B03_AA_REN_CER + B03RatRen.Len.B03_RAT_REN + B03_COD_DIV + B03Riscpar.Len.B03_RISCPAR + B03CumRiscpar.Len.B03_CUM_RISCPAR + B03UltRm.Len.B03_ULT_RM + B03TsRendtoT.Len.B03_TS_RENDTO_T + B03AlqRetrT.Len.B03_ALQ_RETR_T + B03MinTrnutT.Len.B03_MIN_TRNUT_T + B03TsNetT.Len.B03_TS_NET_T + B03DtUltRival.Len.B03_DT_ULT_RIVAL + B03PrstzIni.Len.B03_PRSTZ_INI + B03PrstzAggIni.Len.B03_PRSTZ_AGG_INI + B03PrstzAggUlt.Len.B03_PRSTZ_AGG_ULT + B03Rappel.Len.B03_RAPPEL + B03PrePattuitoIni.Len.B03_PRE_PATTUITO_INI + B03PreDovIni.Len.B03_PRE_DOV_INI + B03PreDovRivtoT.Len.B03_PRE_DOV_RIVTO_T + B03PreAnnualizRicor.Len.B03_PRE_ANNUALIZ_RICOR + B03PreCont.Len.B03_PRE_CONT + B03PrePpIni.Len.B03_PRE_PP_INI + B03RisPuraT.Len.B03_RIS_PURA_T + B03ProvAcq.Len.B03_PROV_ACQ + B03ProvAcqRicor.Len.B03_PROV_ACQ_RICOR + B03ProvInc.Len.B03_PROV_INC + B03CarAcqNonScon.Len.B03_CAR_ACQ_NON_SCON + B03OverComm.Len.B03_OVER_COMM + B03CarAcqPrecontato.Len.B03_CAR_ACQ_PRECONTATO + B03RisAcqT.Len.B03_RIS_ACQ_T + B03RisZilT.Len.B03_RIS_ZIL_T + B03CarGestNonScon.Len.B03_CAR_GEST_NON_SCON + B03CarGest.Len.B03_CAR_GEST + B03RisSpeT.Len.B03_RIS_SPE_T + B03CarIncNonScon.Len.B03_CAR_INC_NON_SCON + B03CarInc.Len.B03_CAR_INC + B03RisRistorniCap.Len.B03_RIS_RISTORNI_CAP + B03IntrTecn.Len.B03_INTR_TECN + B03CptRshMor.Len.B03_CPT_RSH_MOR + B03CSubrshT.Len.B03_C_SUBRSH_T + B03PreRshT.Len.B03_PRE_RSH_T + B03AlqMargRis.Len.B03_ALQ_MARG_RIS + B03AlqMargCSubrsh.Len.B03_ALQ_MARG_C_SUBRSH + B03TsRendtoSppr.Len.B03_TS_RENDTO_SPPR + B03_TP_IAS + B03NsQuo.Len.B03_NS_QUO + B03TsMedio.Len.B03_TS_MEDIO + B03CptRiasto.Len.B03_CPT_RIASTO + B03PreRiasto.Len.B03_PRE_RIASTO + B03RisRiasta.Len.B03_RIS_RIASTA + B03CptRiastoEcc.Len.B03_CPT_RIASTO_ECC + B03PreRiastoEcc.Len.B03_PRE_RIASTO_ECC + B03RisRiastaEcc.Len.B03_RIS_RIASTA_ECC + B03CodAge.Len.B03_COD_AGE + B03CodSubage.Len.B03_COD_SUBAGE + B03CodCan.Len.B03_COD_CAN + B03_IB_POLI + B03_IB_ADES + B03_IB_TRCH_DI_GAR + B03_TP_PRSTZ + B03_TP_TRASF + B03_PP_INVRIO_TARI + B03CoeffOpzRen.Len.B03_COEFF_OPZ_REN + B03CoeffOpzCpt.Len.B03_COEFF_OPZ_CPT + B03DurPagRen.Len.B03_DUR_PAG_REN + B03_VLT + B03RisMatChiuPrec.Len.B03_RIS_MAT_CHIU_PREC + B03_COD_FND + B03PrstzT.Len.B03_PRSTZ_T + B03TsTariDov.Len.B03_TS_TARI_DOV + B03TsTariScon.Len.B03_TS_TARI_SCON + B03TsPp.Len.B03_TS_PP + B03CoeffRis1T.Len.B03_COEFF_RIS1_T + B03CoeffRis2T.Len.B03_COEFF_RIS2_T + B03Abb.Len.B03_ABB + B03_TP_COASS + B03_TRAT_RIASS + B03_TRAT_RIASS_ECC + B03_DS_OPER_SQL + B03_DS_VER + B03_DS_TS_CPTZ + B03_DS_UTENTE + B03_DS_STATO_ELAB + B03_TP_RGM_FISC + B03DurGarAa.Len.B03_DUR_GAR_AA + B03DurGarMm.Len.B03_DUR_GAR_MM + B03DurGarGg.Len.B03_DUR_GAR_GG + B03AntidurCalc365.Len.B03_ANTIDUR_CALC365 + B03_COD_FISC_CNTR + B03_COD_FISC_ASSTO1 + B03_COD_FISC_ASSTO2 + B03_COD_FISC_ASSTO3 + B03_CAUS_SCON_VCHAR + B03_EMIT_TIT_OPZ_VCHAR + B03QtzSpZCoupEmis.Len.B03_QTZ_SP_Z_COUP_EMIS + B03QtzSpZOpzEmis.Len.B03_QTZ_SP_Z_OPZ_EMIS + B03QtzSpZCoupDtC.Len.B03_QTZ_SP_Z_COUP_DT_C + B03QtzSpZOpzDtCa.Len.B03_QTZ_SP_Z_OPZ_DT_CA + B03QtzTotEmis.Len.B03_QTZ_TOT_EMIS + B03QtzTotDtCalc.Len.B03_QTZ_TOT_DT_CALC + B03QtzTotDtUltBil.Len.B03_QTZ_TOT_DT_ULT_BIL + B03DtQtzEmis.Len.B03_DT_QTZ_EMIS + B03PcCarGest.Len.B03_PC_CAR_GEST + B03PcCarAcq.Len.B03_PC_CAR_ACQ + B03ImpCarCasoMor.Len.B03_IMP_CAR_CASO_MOR + B03PcCarMor.Len.B03_PC_CAR_MOR + B03_TP_VERS + B03_FL_SWITCH + B03_FL_IAS + B03Dir.Len.B03_DIR + B03_TP_COP_CASO_MOR + B03MetRiscSpcl.Len.B03_MET_RISC_SPCL + B03_TP_STAT_INVST + B03CodPrdt.Len.B03_COD_PRDT + B03_STAT_ASSTO1 + B03_STAT_ASSTO2 + B03_STAT_ASSTO3 + B03CptAsstoIniMor.Len.B03_CPT_ASSTO_INI_MOR + B03TsStabPre.Len.B03_TS_STAB_PRE + B03DirEmis.Len.B03_DIR_EMIS + B03DtIncUltPre.Len.B03_DT_INC_ULT_PRE + B03_STAT_TBGC_ASSTO1 + B03_STAT_TBGC_ASSTO2 + B03_STAT_TBGC_ASSTO3 + B03FrazDecrCpt.Len.B03_FRAZ_DECR_CPT + B03PrePpUlt.Len.B03_PRE_PP_ULT + B03AcqExp.Len.B03_ACQ_EXP + B03RemunAss.Len.B03_REMUN_ASS + B03CommisInter.Len.B03_COMMIS_INTER + B03_NUM_FINANZ + B03_TP_ACC_COMM + B03_IB_ACC_COMM + B03_RAMO_BILA + B03Carz.Len.B03_CARZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ID_BILA_TRCH_ESTR = 9;
            public static final int B03_COD_COMP_ANIA = 5;
            public static final int B03_ID_RICH_ESTRAZ_MAS = 9;
            public static final int B03_DT_RIS = 8;
            public static final int B03_DT_PRODUZIONE = 8;
            public static final int B03_ID_POLI = 9;
            public static final int B03_ID_ADES = 9;
            public static final int B03_ID_GAR = 9;
            public static final int B03_ID_TRCH_DI_GAR = 9;
            public static final int B03_DT_INI_VLDT_PROD = 8;
            public static final int B03_DT_DECOR_POLI = 8;
            public static final int B03_DT_DECOR_TRCH = 8;
            public static final int B03_DT_EMIS_POLI = 8;
            public static final int B03_DS_VER = 9;
            public static final int B03_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
