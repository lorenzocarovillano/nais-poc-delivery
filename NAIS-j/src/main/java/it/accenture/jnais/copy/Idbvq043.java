package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVQ043<br>
 * Copybook: IDBVQ043 from copybook IDBVQ043<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvq043 {

    //==== PROPERTIES ====
    //Original name: Q04-DT-INI-EFF-DB
    private String q04DtIniEffDb = DefaultValues.stringVal(Len.Q04_DT_INI_EFF_DB);
    //Original name: Q04-DT-END-EFF-DB
    private String q04DtEndEffDb = DefaultValues.stringVal(Len.Q04_DT_END_EFF_DB);

    //==== METHODS ====
    public void setQ04DtIniEffDb(String q04DtIniEffDb) {
        this.q04DtIniEffDb = Functions.subString(q04DtIniEffDb, Len.Q04_DT_INI_EFF_DB);
    }

    public String getQ04DtIniEffDb() {
        return this.q04DtIniEffDb;
    }

    public void setQ04DtEndEffDb(String q04DtEndEffDb) {
        this.q04DtEndEffDb = Functions.subString(q04DtEndEffDb, Len.Q04_DT_END_EFF_DB);
    }

    public String getQ04DtEndEffDb() {
        return this.q04DtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int Q04_DT_INI_EFF_DB = 10;
        public static final int Q04_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
