package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.AddressTrovato;
import it.accenture.jnais.ws.enums.CachePiena;
import it.accenture.jnais.ws.enums.TipoAddress;

/**Original name: IABC0010<br>
 * Variable: IABC0010 from copybook IABC0010<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Iabc0010 {

    //==== PROPERTIES ====
    /**Original name: IX-CACHE<br>
	 * <pre>*****************************************************************
	 *  STRUTTURA PER GESTIONE CACHE COMP_STR_DATO / ANAG_DATO
	 * *****************************************************************
	 * *****************************************************************
	 *  STRUTTURA PER GESTIONE CACHE
	 * *****************************************************************</pre>*/
    private long ixCache = DefaultValues.BIN_LONG_VAL;
    //Original name: TIPO-ADDRESS
    private TipoAddress tipoAddress = new TipoAddress();
    //Original name: ADDRESS-TROVATO
    private AddressTrovato addressTrovato = new AddressTrovato();
    //Original name: TROVATO-DATO-IN-CACHE
    private AddressTrovato trovatoDatoInCache = new AddressTrovato();
    //Original name: CACHE-PIENA
    private CachePiena cachePiena = new CachePiena();

    //==== METHODS ====
    public void setIxCache(long ixCache) {
        this.ixCache = ixCache;
    }

    public long getIxCache() {
        return this.ixCache;
    }

    public AddressTrovato getAddressTrovato() {
        return addressTrovato;
    }

    public CachePiena getCachePiena() {
        return cachePiena;
    }

    public TipoAddress getTipoAddress() {
        return tipoAddress;
    }

    public AddressTrovato getTrovatoDatoInCache() {
        return trovatoDatoInCache;
    }
}
