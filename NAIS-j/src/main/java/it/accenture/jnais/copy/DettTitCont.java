package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.DtcAcqExp;
import it.accenture.jnais.ws.redefines.DtcCarAcq;
import it.accenture.jnais.ws.redefines.DtcCarGest;
import it.accenture.jnais.ws.redefines.DtcCarIas;
import it.accenture.jnais.ws.redefines.DtcCarInc;
import it.accenture.jnais.ws.redefines.DtcCnbtAntirac;
import it.accenture.jnais.ws.redefines.DtcCommisInter;
import it.accenture.jnais.ws.redefines.DtcDir;
import it.accenture.jnais.ws.redefines.DtcDtEndCop;
import it.accenture.jnais.ws.redefines.DtcDtEsiTit;
import it.accenture.jnais.ws.redefines.DtcDtIniCop;
import it.accenture.jnais.ws.redefines.DtcFrqMovi;
import it.accenture.jnais.ws.redefines.DtcIdMoviChiu;
import it.accenture.jnais.ws.redefines.DtcImpAder;
import it.accenture.jnais.ws.redefines.DtcImpAz;
import it.accenture.jnais.ws.redefines.DtcImpTfr;
import it.accenture.jnais.ws.redefines.DtcImpTfrStrc;
import it.accenture.jnais.ws.redefines.DtcImpTrasfe;
import it.accenture.jnais.ws.redefines.DtcImpVolo;
import it.accenture.jnais.ws.redefines.DtcIntrFraz;
import it.accenture.jnais.ws.redefines.DtcIntrMora;
import it.accenture.jnais.ws.redefines.DtcIntrRetdt;
import it.accenture.jnais.ws.redefines.DtcIntrRiat;
import it.accenture.jnais.ws.redefines.DtcManfeeAntic;
import it.accenture.jnais.ws.redefines.DtcManfeeRec;
import it.accenture.jnais.ws.redefines.DtcManfeeRicor;
import it.accenture.jnais.ws.redefines.DtcNumGgRitardoPag;
import it.accenture.jnais.ws.redefines.DtcNumGgRival;
import it.accenture.jnais.ws.redefines.DtcPreNet;
import it.accenture.jnais.ws.redefines.DtcPrePpIas;
import it.accenture.jnais.ws.redefines.DtcPreSoloRsh;
import it.accenture.jnais.ws.redefines.DtcPreTot;
import it.accenture.jnais.ws.redefines.DtcProvAcq1aa;
import it.accenture.jnais.ws.redefines.DtcProvAcq2aa;
import it.accenture.jnais.ws.redefines.DtcProvDaRec;
import it.accenture.jnais.ws.redefines.DtcProvInc;
import it.accenture.jnais.ws.redefines.DtcProvRicor;
import it.accenture.jnais.ws.redefines.DtcRemunAss;
import it.accenture.jnais.ws.redefines.DtcSoprAlt;
import it.accenture.jnais.ws.redefines.DtcSoprProf;
import it.accenture.jnais.ws.redefines.DtcSoprSan;
import it.accenture.jnais.ws.redefines.DtcSoprSpo;
import it.accenture.jnais.ws.redefines.DtcSoprTec;
import it.accenture.jnais.ws.redefines.DtcSpeAge;
import it.accenture.jnais.ws.redefines.DtcSpeMed;
import it.accenture.jnais.ws.redefines.DtcTax;
import it.accenture.jnais.ws.redefines.DtcTotIntrPrest;

/**Original name: DETT-TIT-CONT<br>
 * Variable: DETT-TIT-CONT from copybook IDBVDTC1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DettTitCont {

    //==== PROPERTIES ====
    //Original name: DTC-ID-DETT-TIT-CONT
    private int dtcIdDettTitCont = DefaultValues.INT_VAL;
    //Original name: DTC-ID-TIT-CONT
    private int dtcIdTitCont = DefaultValues.INT_VAL;
    //Original name: DTC-ID-OGG
    private int dtcIdOgg = DefaultValues.INT_VAL;
    //Original name: DTC-TP-OGG
    private String dtcTpOgg = DefaultValues.stringVal(Len.DTC_TP_OGG);
    //Original name: DTC-ID-MOVI-CRZ
    private int dtcIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: DTC-ID-MOVI-CHIU
    private DtcIdMoviChiu dtcIdMoviChiu = new DtcIdMoviChiu();
    //Original name: DTC-DT-INI-EFF
    private int dtcDtIniEff = DefaultValues.INT_VAL;
    //Original name: DTC-DT-END-EFF
    private int dtcDtEndEff = DefaultValues.INT_VAL;
    //Original name: DTC-COD-COMP-ANIA
    private int dtcCodCompAnia = DefaultValues.INT_VAL;
    //Original name: DTC-DT-INI-COP
    private DtcDtIniCop dtcDtIniCop = new DtcDtIniCop();
    //Original name: DTC-DT-END-COP
    private DtcDtEndCop dtcDtEndCop = new DtcDtEndCop();
    //Original name: DTC-PRE-NET
    private DtcPreNet dtcPreNet = new DtcPreNet();
    //Original name: DTC-INTR-FRAZ
    private DtcIntrFraz dtcIntrFraz = new DtcIntrFraz();
    //Original name: DTC-INTR-MORA
    private DtcIntrMora dtcIntrMora = new DtcIntrMora();
    //Original name: DTC-INTR-RETDT
    private DtcIntrRetdt dtcIntrRetdt = new DtcIntrRetdt();
    //Original name: DTC-INTR-RIAT
    private DtcIntrRiat dtcIntrRiat = new DtcIntrRiat();
    //Original name: DTC-DIR
    private DtcDir dtcDir = new DtcDir();
    //Original name: DTC-SPE-MED
    private DtcSpeMed dtcSpeMed = new DtcSpeMed();
    //Original name: DTC-TAX
    private DtcTax dtcTax = new DtcTax();
    //Original name: DTC-SOPR-SAN
    private DtcSoprSan dtcSoprSan = new DtcSoprSan();
    //Original name: DTC-SOPR-SPO
    private DtcSoprSpo dtcSoprSpo = new DtcSoprSpo();
    //Original name: DTC-SOPR-TEC
    private DtcSoprTec dtcSoprTec = new DtcSoprTec();
    //Original name: DTC-SOPR-PROF
    private DtcSoprProf dtcSoprProf = new DtcSoprProf();
    //Original name: DTC-SOPR-ALT
    private DtcSoprAlt dtcSoprAlt = new DtcSoprAlt();
    //Original name: DTC-PRE-TOT
    private DtcPreTot dtcPreTot = new DtcPreTot();
    //Original name: DTC-PRE-PP-IAS
    private DtcPrePpIas dtcPrePpIas = new DtcPrePpIas();
    //Original name: DTC-PRE-SOLO-RSH
    private DtcPreSoloRsh dtcPreSoloRsh = new DtcPreSoloRsh();
    //Original name: DTC-CAR-ACQ
    private DtcCarAcq dtcCarAcq = new DtcCarAcq();
    //Original name: DTC-CAR-GEST
    private DtcCarGest dtcCarGest = new DtcCarGest();
    //Original name: DTC-CAR-INC
    private DtcCarInc dtcCarInc = new DtcCarInc();
    //Original name: DTC-PROV-ACQ-1AA
    private DtcProvAcq1aa dtcProvAcq1aa = new DtcProvAcq1aa();
    //Original name: DTC-PROV-ACQ-2AA
    private DtcProvAcq2aa dtcProvAcq2aa = new DtcProvAcq2aa();
    //Original name: DTC-PROV-RICOR
    private DtcProvRicor dtcProvRicor = new DtcProvRicor();
    //Original name: DTC-PROV-INC
    private DtcProvInc dtcProvInc = new DtcProvInc();
    //Original name: DTC-PROV-DA-REC
    private DtcProvDaRec dtcProvDaRec = new DtcProvDaRec();
    //Original name: DTC-COD-DVS
    private String dtcCodDvs = DefaultValues.stringVal(Len.DTC_COD_DVS);
    //Original name: DTC-FRQ-MOVI
    private DtcFrqMovi dtcFrqMovi = new DtcFrqMovi();
    //Original name: DTC-TP-RGM-FISC
    private String dtcTpRgmFisc = DefaultValues.stringVal(Len.DTC_TP_RGM_FISC);
    //Original name: DTC-COD-TARI
    private String dtcCodTari = DefaultValues.stringVal(Len.DTC_COD_TARI);
    //Original name: DTC-TP-STAT-TIT
    private String dtcTpStatTit = DefaultValues.stringVal(Len.DTC_TP_STAT_TIT);
    //Original name: DTC-IMP-AZ
    private DtcImpAz dtcImpAz = new DtcImpAz();
    //Original name: DTC-IMP-ADER
    private DtcImpAder dtcImpAder = new DtcImpAder();
    //Original name: DTC-IMP-TFR
    private DtcImpTfr dtcImpTfr = new DtcImpTfr();
    //Original name: DTC-IMP-VOLO
    private DtcImpVolo dtcImpVolo = new DtcImpVolo();
    //Original name: DTC-MANFEE-ANTIC
    private DtcManfeeAntic dtcManfeeAntic = new DtcManfeeAntic();
    //Original name: DTC-MANFEE-RICOR
    private DtcManfeeRicor dtcManfeeRicor = new DtcManfeeRicor();
    //Original name: DTC-MANFEE-REC
    private DtcManfeeRec dtcManfeeRec = new DtcManfeeRec();
    //Original name: DTC-DT-ESI-TIT
    private DtcDtEsiTit dtcDtEsiTit = new DtcDtEsiTit();
    //Original name: DTC-SPE-AGE
    private DtcSpeAge dtcSpeAge = new DtcSpeAge();
    //Original name: DTC-CAR-IAS
    private DtcCarIas dtcCarIas = new DtcCarIas();
    //Original name: DTC-TOT-INTR-PREST
    private DtcTotIntrPrest dtcTotIntrPrest = new DtcTotIntrPrest();
    //Original name: DTC-DS-RIGA
    private long dtcDsRiga = DefaultValues.LONG_VAL;
    //Original name: DTC-DS-OPER-SQL
    private char dtcDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: DTC-DS-VER
    private int dtcDsVer = DefaultValues.INT_VAL;
    //Original name: DTC-DS-TS-INI-CPTZ
    private long dtcDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: DTC-DS-TS-END-CPTZ
    private long dtcDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: DTC-DS-UTENTE
    private String dtcDsUtente = DefaultValues.stringVal(Len.DTC_DS_UTENTE);
    //Original name: DTC-DS-STATO-ELAB
    private char dtcDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: DTC-IMP-TRASFE
    private DtcImpTrasfe dtcImpTrasfe = new DtcImpTrasfe();
    //Original name: DTC-IMP-TFR-STRC
    private DtcImpTfrStrc dtcImpTfrStrc = new DtcImpTfrStrc();
    //Original name: DTC-NUM-GG-RITARDO-PAG
    private DtcNumGgRitardoPag dtcNumGgRitardoPag = new DtcNumGgRitardoPag();
    //Original name: DTC-NUM-GG-RIVAL
    private DtcNumGgRival dtcNumGgRival = new DtcNumGgRival();
    //Original name: DTC-ACQ-EXP
    private DtcAcqExp dtcAcqExp = new DtcAcqExp();
    //Original name: DTC-REMUN-ASS
    private DtcRemunAss dtcRemunAss = new DtcRemunAss();
    //Original name: DTC-COMMIS-INTER
    private DtcCommisInter dtcCommisInter = new DtcCommisInter();
    //Original name: DTC-CNBT-ANTIRAC
    private DtcCnbtAntirac dtcCnbtAntirac = new DtcCnbtAntirac();

    //==== METHODS ====
    public void setDettTitContFormatted(String data) {
        byte[] buffer = new byte[Len.DETT_TIT_CONT];
        MarshalByte.writeString(buffer, 1, data, Len.DETT_TIT_CONT);
        setDettTitContBytes(buffer, 1);
    }

    public String getDettTitContFormatted() {
        return MarshalByteExt.bufferToStr(getDettTitContBytes());
    }

    public byte[] getDettTitContBytes() {
        byte[] buffer = new byte[Len.DETT_TIT_CONT];
        return getDettTitContBytes(buffer, 1);
    }

    public void setDettTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        dtcIdDettTitCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTC_ID_DETT_TIT_CONT, 0);
        position += Len.DTC_ID_DETT_TIT_CONT;
        dtcIdTitCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTC_ID_TIT_CONT, 0);
        position += Len.DTC_ID_TIT_CONT;
        dtcIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTC_ID_OGG, 0);
        position += Len.DTC_ID_OGG;
        dtcTpOgg = MarshalByte.readString(buffer, position, Len.DTC_TP_OGG);
        position += Len.DTC_TP_OGG;
        dtcIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTC_ID_MOVI_CRZ, 0);
        position += Len.DTC_ID_MOVI_CRZ;
        dtcIdMoviChiu.setDtcIdMoviChiuFromBuffer(buffer, position);
        position += DtcIdMoviChiu.Len.DTC_ID_MOVI_CHIU;
        dtcDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTC_DT_INI_EFF, 0);
        position += Len.DTC_DT_INI_EFF;
        dtcDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTC_DT_END_EFF, 0);
        position += Len.DTC_DT_END_EFF;
        dtcCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTC_COD_COMP_ANIA, 0);
        position += Len.DTC_COD_COMP_ANIA;
        dtcDtIniCop.setDtcDtIniCopFromBuffer(buffer, position);
        position += DtcDtIniCop.Len.DTC_DT_INI_COP;
        dtcDtEndCop.setDtcDtEndCopFromBuffer(buffer, position);
        position += DtcDtEndCop.Len.DTC_DT_END_COP;
        dtcPreNet.setDtcPreNetFromBuffer(buffer, position);
        position += DtcPreNet.Len.DTC_PRE_NET;
        dtcIntrFraz.setDtcIntrFrazFromBuffer(buffer, position);
        position += DtcIntrFraz.Len.DTC_INTR_FRAZ;
        dtcIntrMora.setDtcIntrMoraFromBuffer(buffer, position);
        position += DtcIntrMora.Len.DTC_INTR_MORA;
        dtcIntrRetdt.setDtcIntrRetdtFromBuffer(buffer, position);
        position += DtcIntrRetdt.Len.DTC_INTR_RETDT;
        dtcIntrRiat.setDtcIntrRiatFromBuffer(buffer, position);
        position += DtcIntrRiat.Len.DTC_INTR_RIAT;
        dtcDir.setDtcDirFromBuffer(buffer, position);
        position += DtcDir.Len.DTC_DIR;
        dtcSpeMed.setDtcSpeMedFromBuffer(buffer, position);
        position += DtcSpeMed.Len.DTC_SPE_MED;
        dtcTax.setDtcTaxFromBuffer(buffer, position);
        position += DtcTax.Len.DTC_TAX;
        dtcSoprSan.setDtcSoprSanFromBuffer(buffer, position);
        position += DtcSoprSan.Len.DTC_SOPR_SAN;
        dtcSoprSpo.setDtcSoprSpoFromBuffer(buffer, position);
        position += DtcSoprSpo.Len.DTC_SOPR_SPO;
        dtcSoprTec.setDtcSoprTecFromBuffer(buffer, position);
        position += DtcSoprTec.Len.DTC_SOPR_TEC;
        dtcSoprProf.setDtcSoprProfFromBuffer(buffer, position);
        position += DtcSoprProf.Len.DTC_SOPR_PROF;
        dtcSoprAlt.setDtcSoprAltFromBuffer(buffer, position);
        position += DtcSoprAlt.Len.DTC_SOPR_ALT;
        dtcPreTot.setDtcPreTotFromBuffer(buffer, position);
        position += DtcPreTot.Len.DTC_PRE_TOT;
        dtcPrePpIas.setDtcPrePpIasFromBuffer(buffer, position);
        position += DtcPrePpIas.Len.DTC_PRE_PP_IAS;
        dtcPreSoloRsh.setDtcPreSoloRshFromBuffer(buffer, position);
        position += DtcPreSoloRsh.Len.DTC_PRE_SOLO_RSH;
        dtcCarAcq.setDtcCarAcqFromBuffer(buffer, position);
        position += DtcCarAcq.Len.DTC_CAR_ACQ;
        dtcCarGest.setDtcCarGestFromBuffer(buffer, position);
        position += DtcCarGest.Len.DTC_CAR_GEST;
        dtcCarInc.setDtcCarIncFromBuffer(buffer, position);
        position += DtcCarInc.Len.DTC_CAR_INC;
        dtcProvAcq1aa.setDtcProvAcq1aaFromBuffer(buffer, position);
        position += DtcProvAcq1aa.Len.DTC_PROV_ACQ1AA;
        dtcProvAcq2aa.setDtcProvAcq2aaFromBuffer(buffer, position);
        position += DtcProvAcq2aa.Len.DTC_PROV_ACQ2AA;
        dtcProvRicor.setDtcProvRicorFromBuffer(buffer, position);
        position += DtcProvRicor.Len.DTC_PROV_RICOR;
        dtcProvInc.setDtcProvIncFromBuffer(buffer, position);
        position += DtcProvInc.Len.DTC_PROV_INC;
        dtcProvDaRec.setDtcProvDaRecFromBuffer(buffer, position);
        position += DtcProvDaRec.Len.DTC_PROV_DA_REC;
        dtcCodDvs = MarshalByte.readString(buffer, position, Len.DTC_COD_DVS);
        position += Len.DTC_COD_DVS;
        dtcFrqMovi.setDtcFrqMoviFromBuffer(buffer, position);
        position += DtcFrqMovi.Len.DTC_FRQ_MOVI;
        dtcTpRgmFisc = MarshalByte.readString(buffer, position, Len.DTC_TP_RGM_FISC);
        position += Len.DTC_TP_RGM_FISC;
        dtcCodTari = MarshalByte.readString(buffer, position, Len.DTC_COD_TARI);
        position += Len.DTC_COD_TARI;
        dtcTpStatTit = MarshalByte.readString(buffer, position, Len.DTC_TP_STAT_TIT);
        position += Len.DTC_TP_STAT_TIT;
        dtcImpAz.setDtcImpAzFromBuffer(buffer, position);
        position += DtcImpAz.Len.DTC_IMP_AZ;
        dtcImpAder.setDtcImpAderFromBuffer(buffer, position);
        position += DtcImpAder.Len.DTC_IMP_ADER;
        dtcImpTfr.setDtcImpTfrFromBuffer(buffer, position);
        position += DtcImpTfr.Len.DTC_IMP_TFR;
        dtcImpVolo.setDtcImpVoloFromBuffer(buffer, position);
        position += DtcImpVolo.Len.DTC_IMP_VOLO;
        dtcManfeeAntic.setDtcManfeeAnticFromBuffer(buffer, position);
        position += DtcManfeeAntic.Len.DTC_MANFEE_ANTIC;
        dtcManfeeRicor.setDtcManfeeRicorFromBuffer(buffer, position);
        position += DtcManfeeRicor.Len.DTC_MANFEE_RICOR;
        dtcManfeeRec.setDtcManfeeRecFromBuffer(buffer, position);
        position += DtcManfeeRec.Len.DTC_MANFEE_REC;
        dtcDtEsiTit.setDtcDtEsiTitFromBuffer(buffer, position);
        position += DtcDtEsiTit.Len.DTC_DT_ESI_TIT;
        dtcSpeAge.setDtcSpeAgeFromBuffer(buffer, position);
        position += DtcSpeAge.Len.DTC_SPE_AGE;
        dtcCarIas.setDtcCarIasFromBuffer(buffer, position);
        position += DtcCarIas.Len.DTC_CAR_IAS;
        dtcTotIntrPrest.setDtcTotIntrPrestFromBuffer(buffer, position);
        position += DtcTotIntrPrest.Len.DTC_TOT_INTR_PREST;
        dtcDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DTC_DS_RIGA, 0);
        position += Len.DTC_DS_RIGA;
        dtcDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dtcDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DTC_DS_VER, 0);
        position += Len.DTC_DS_VER;
        dtcDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DTC_DS_TS_INI_CPTZ, 0);
        position += Len.DTC_DS_TS_INI_CPTZ;
        dtcDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DTC_DS_TS_END_CPTZ, 0);
        position += Len.DTC_DS_TS_END_CPTZ;
        dtcDsUtente = MarshalByte.readString(buffer, position, Len.DTC_DS_UTENTE);
        position += Len.DTC_DS_UTENTE;
        dtcDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dtcImpTrasfe.setDtcImpTrasfeFromBuffer(buffer, position);
        position += DtcImpTrasfe.Len.DTC_IMP_TRASFE;
        dtcImpTfrStrc.setDtcImpTfrStrcFromBuffer(buffer, position);
        position += DtcImpTfrStrc.Len.DTC_IMP_TFR_STRC;
        dtcNumGgRitardoPag.setDtcNumGgRitardoPagFromBuffer(buffer, position);
        position += DtcNumGgRitardoPag.Len.DTC_NUM_GG_RITARDO_PAG;
        dtcNumGgRival.setDtcNumGgRivalFromBuffer(buffer, position);
        position += DtcNumGgRival.Len.DTC_NUM_GG_RIVAL;
        dtcAcqExp.setDtcAcqExpFromBuffer(buffer, position);
        position += DtcAcqExp.Len.DTC_ACQ_EXP;
        dtcRemunAss.setDtcRemunAssFromBuffer(buffer, position);
        position += DtcRemunAss.Len.DTC_REMUN_ASS;
        dtcCommisInter.setDtcCommisInterFromBuffer(buffer, position);
        position += DtcCommisInter.Len.DTC_COMMIS_INTER;
        dtcCnbtAntirac.setDtcCnbtAntiracFromBuffer(buffer, position);
    }

    public byte[] getDettTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, dtcIdDettTitCont, Len.Int.DTC_ID_DETT_TIT_CONT, 0);
        position += Len.DTC_ID_DETT_TIT_CONT;
        MarshalByte.writeIntAsPacked(buffer, position, dtcIdTitCont, Len.Int.DTC_ID_TIT_CONT, 0);
        position += Len.DTC_ID_TIT_CONT;
        MarshalByte.writeIntAsPacked(buffer, position, dtcIdOgg, Len.Int.DTC_ID_OGG, 0);
        position += Len.DTC_ID_OGG;
        MarshalByte.writeString(buffer, position, dtcTpOgg, Len.DTC_TP_OGG);
        position += Len.DTC_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, dtcIdMoviCrz, Len.Int.DTC_ID_MOVI_CRZ, 0);
        position += Len.DTC_ID_MOVI_CRZ;
        dtcIdMoviChiu.getDtcIdMoviChiuAsBuffer(buffer, position);
        position += DtcIdMoviChiu.Len.DTC_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dtcDtIniEff, Len.Int.DTC_DT_INI_EFF, 0);
        position += Len.DTC_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtcDtEndEff, Len.Int.DTC_DT_END_EFF, 0);
        position += Len.DTC_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtcCodCompAnia, Len.Int.DTC_COD_COMP_ANIA, 0);
        position += Len.DTC_COD_COMP_ANIA;
        dtcDtIniCop.getDtcDtIniCopAsBuffer(buffer, position);
        position += DtcDtIniCop.Len.DTC_DT_INI_COP;
        dtcDtEndCop.getDtcDtEndCopAsBuffer(buffer, position);
        position += DtcDtEndCop.Len.DTC_DT_END_COP;
        dtcPreNet.getDtcPreNetAsBuffer(buffer, position);
        position += DtcPreNet.Len.DTC_PRE_NET;
        dtcIntrFraz.getDtcIntrFrazAsBuffer(buffer, position);
        position += DtcIntrFraz.Len.DTC_INTR_FRAZ;
        dtcIntrMora.getDtcIntrMoraAsBuffer(buffer, position);
        position += DtcIntrMora.Len.DTC_INTR_MORA;
        dtcIntrRetdt.getDtcIntrRetdtAsBuffer(buffer, position);
        position += DtcIntrRetdt.Len.DTC_INTR_RETDT;
        dtcIntrRiat.getDtcIntrRiatAsBuffer(buffer, position);
        position += DtcIntrRiat.Len.DTC_INTR_RIAT;
        dtcDir.getDtcDirAsBuffer(buffer, position);
        position += DtcDir.Len.DTC_DIR;
        dtcSpeMed.getDtcSpeMedAsBuffer(buffer, position);
        position += DtcSpeMed.Len.DTC_SPE_MED;
        dtcTax.getDtcTaxAsBuffer(buffer, position);
        position += DtcTax.Len.DTC_TAX;
        dtcSoprSan.getDtcSoprSanAsBuffer(buffer, position);
        position += DtcSoprSan.Len.DTC_SOPR_SAN;
        dtcSoprSpo.getDtcSoprSpoAsBuffer(buffer, position);
        position += DtcSoprSpo.Len.DTC_SOPR_SPO;
        dtcSoprTec.getDtcSoprTecAsBuffer(buffer, position);
        position += DtcSoprTec.Len.DTC_SOPR_TEC;
        dtcSoprProf.getDtcSoprProfAsBuffer(buffer, position);
        position += DtcSoprProf.Len.DTC_SOPR_PROF;
        dtcSoprAlt.getDtcSoprAltAsBuffer(buffer, position);
        position += DtcSoprAlt.Len.DTC_SOPR_ALT;
        dtcPreTot.getDtcPreTotAsBuffer(buffer, position);
        position += DtcPreTot.Len.DTC_PRE_TOT;
        dtcPrePpIas.getDtcPrePpIasAsBuffer(buffer, position);
        position += DtcPrePpIas.Len.DTC_PRE_PP_IAS;
        dtcPreSoloRsh.getDtcPreSoloRshAsBuffer(buffer, position);
        position += DtcPreSoloRsh.Len.DTC_PRE_SOLO_RSH;
        dtcCarAcq.getDtcCarAcqAsBuffer(buffer, position);
        position += DtcCarAcq.Len.DTC_CAR_ACQ;
        dtcCarGest.getDtcCarGestAsBuffer(buffer, position);
        position += DtcCarGest.Len.DTC_CAR_GEST;
        dtcCarInc.getDtcCarIncAsBuffer(buffer, position);
        position += DtcCarInc.Len.DTC_CAR_INC;
        dtcProvAcq1aa.getDtcProvAcq1aaAsBuffer(buffer, position);
        position += DtcProvAcq1aa.Len.DTC_PROV_ACQ1AA;
        dtcProvAcq2aa.getDtcProvAcq2aaAsBuffer(buffer, position);
        position += DtcProvAcq2aa.Len.DTC_PROV_ACQ2AA;
        dtcProvRicor.getDtcProvRicorAsBuffer(buffer, position);
        position += DtcProvRicor.Len.DTC_PROV_RICOR;
        dtcProvInc.getDtcProvIncAsBuffer(buffer, position);
        position += DtcProvInc.Len.DTC_PROV_INC;
        dtcProvDaRec.getDtcProvDaRecAsBuffer(buffer, position);
        position += DtcProvDaRec.Len.DTC_PROV_DA_REC;
        MarshalByte.writeString(buffer, position, dtcCodDvs, Len.DTC_COD_DVS);
        position += Len.DTC_COD_DVS;
        dtcFrqMovi.getDtcFrqMoviAsBuffer(buffer, position);
        position += DtcFrqMovi.Len.DTC_FRQ_MOVI;
        MarshalByte.writeString(buffer, position, dtcTpRgmFisc, Len.DTC_TP_RGM_FISC);
        position += Len.DTC_TP_RGM_FISC;
        MarshalByte.writeString(buffer, position, dtcCodTari, Len.DTC_COD_TARI);
        position += Len.DTC_COD_TARI;
        MarshalByte.writeString(buffer, position, dtcTpStatTit, Len.DTC_TP_STAT_TIT);
        position += Len.DTC_TP_STAT_TIT;
        dtcImpAz.getDtcImpAzAsBuffer(buffer, position);
        position += DtcImpAz.Len.DTC_IMP_AZ;
        dtcImpAder.getDtcImpAderAsBuffer(buffer, position);
        position += DtcImpAder.Len.DTC_IMP_ADER;
        dtcImpTfr.getDtcImpTfrAsBuffer(buffer, position);
        position += DtcImpTfr.Len.DTC_IMP_TFR;
        dtcImpVolo.getDtcImpVoloAsBuffer(buffer, position);
        position += DtcImpVolo.Len.DTC_IMP_VOLO;
        dtcManfeeAntic.getDtcManfeeAnticAsBuffer(buffer, position);
        position += DtcManfeeAntic.Len.DTC_MANFEE_ANTIC;
        dtcManfeeRicor.getDtcManfeeRicorAsBuffer(buffer, position);
        position += DtcManfeeRicor.Len.DTC_MANFEE_RICOR;
        dtcManfeeRec.getDtcManfeeRecAsBuffer(buffer, position);
        position += DtcManfeeRec.Len.DTC_MANFEE_REC;
        dtcDtEsiTit.getDtcDtEsiTitAsBuffer(buffer, position);
        position += DtcDtEsiTit.Len.DTC_DT_ESI_TIT;
        dtcSpeAge.getDtcSpeAgeAsBuffer(buffer, position);
        position += DtcSpeAge.Len.DTC_SPE_AGE;
        dtcCarIas.getDtcCarIasAsBuffer(buffer, position);
        position += DtcCarIas.Len.DTC_CAR_IAS;
        dtcTotIntrPrest.getDtcTotIntrPrestAsBuffer(buffer, position);
        position += DtcTotIntrPrest.Len.DTC_TOT_INTR_PREST;
        MarshalByte.writeLongAsPacked(buffer, position, dtcDsRiga, Len.Int.DTC_DS_RIGA, 0);
        position += Len.DTC_DS_RIGA;
        MarshalByte.writeChar(buffer, position, dtcDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dtcDsVer, Len.Int.DTC_DS_VER, 0);
        position += Len.DTC_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dtcDsTsIniCptz, Len.Int.DTC_DS_TS_INI_CPTZ, 0);
        position += Len.DTC_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dtcDsTsEndCptz, Len.Int.DTC_DS_TS_END_CPTZ, 0);
        position += Len.DTC_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dtcDsUtente, Len.DTC_DS_UTENTE);
        position += Len.DTC_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dtcDsStatoElab);
        position += Types.CHAR_SIZE;
        dtcImpTrasfe.getDtcImpTrasfeAsBuffer(buffer, position);
        position += DtcImpTrasfe.Len.DTC_IMP_TRASFE;
        dtcImpTfrStrc.getDtcImpTfrStrcAsBuffer(buffer, position);
        position += DtcImpTfrStrc.Len.DTC_IMP_TFR_STRC;
        dtcNumGgRitardoPag.getDtcNumGgRitardoPagAsBuffer(buffer, position);
        position += DtcNumGgRitardoPag.Len.DTC_NUM_GG_RITARDO_PAG;
        dtcNumGgRival.getDtcNumGgRivalAsBuffer(buffer, position);
        position += DtcNumGgRival.Len.DTC_NUM_GG_RIVAL;
        dtcAcqExp.getDtcAcqExpAsBuffer(buffer, position);
        position += DtcAcqExp.Len.DTC_ACQ_EXP;
        dtcRemunAss.getDtcRemunAssAsBuffer(buffer, position);
        position += DtcRemunAss.Len.DTC_REMUN_ASS;
        dtcCommisInter.getDtcCommisInterAsBuffer(buffer, position);
        position += DtcCommisInter.Len.DTC_COMMIS_INTER;
        dtcCnbtAntirac.getDtcCnbtAntiracAsBuffer(buffer, position);
        return buffer;
    }

    public void setDtcIdDettTitCont(int dtcIdDettTitCont) {
        this.dtcIdDettTitCont = dtcIdDettTitCont;
    }

    public int getDtcIdDettTitCont() {
        return this.dtcIdDettTitCont;
    }

    public void setDtcIdTitCont(int dtcIdTitCont) {
        this.dtcIdTitCont = dtcIdTitCont;
    }

    public int getDtcIdTitCont() {
        return this.dtcIdTitCont;
    }

    public void setDtcIdOgg(int dtcIdOgg) {
        this.dtcIdOgg = dtcIdOgg;
    }

    public int getDtcIdOgg() {
        return this.dtcIdOgg;
    }

    public void setDtcTpOgg(String dtcTpOgg) {
        this.dtcTpOgg = Functions.subString(dtcTpOgg, Len.DTC_TP_OGG);
    }

    public String getDtcTpOgg() {
        return this.dtcTpOgg;
    }

    public void setDtcIdMoviCrz(int dtcIdMoviCrz) {
        this.dtcIdMoviCrz = dtcIdMoviCrz;
    }

    public int getDtcIdMoviCrz() {
        return this.dtcIdMoviCrz;
    }

    public void setDtcDtIniEff(int dtcDtIniEff) {
        this.dtcDtIniEff = dtcDtIniEff;
    }

    public int getDtcDtIniEff() {
        return this.dtcDtIniEff;
    }

    public void setDtcDtEndEff(int dtcDtEndEff) {
        this.dtcDtEndEff = dtcDtEndEff;
    }

    public int getDtcDtEndEff() {
        return this.dtcDtEndEff;
    }

    public void setDtcCodCompAnia(int dtcCodCompAnia) {
        this.dtcCodCompAnia = dtcCodCompAnia;
    }

    public int getDtcCodCompAnia() {
        return this.dtcCodCompAnia;
    }

    public void setDtcCodDvs(String dtcCodDvs) {
        this.dtcCodDvs = Functions.subString(dtcCodDvs, Len.DTC_COD_DVS);
    }

    public String getDtcCodDvs() {
        return this.dtcCodDvs;
    }

    public String getDtcCodDvsFormatted() {
        return Functions.padBlanks(getDtcCodDvs(), Len.DTC_COD_DVS);
    }

    public void setDtcTpRgmFisc(String dtcTpRgmFisc) {
        this.dtcTpRgmFisc = Functions.subString(dtcTpRgmFisc, Len.DTC_TP_RGM_FISC);
    }

    public String getDtcTpRgmFisc() {
        return this.dtcTpRgmFisc;
    }

    public void setDtcCodTari(String dtcCodTari) {
        this.dtcCodTari = Functions.subString(dtcCodTari, Len.DTC_COD_TARI);
    }

    public String getDtcCodTari() {
        return this.dtcCodTari;
    }

    public String getDtcCodTariFormatted() {
        return Functions.padBlanks(getDtcCodTari(), Len.DTC_COD_TARI);
    }

    public void setDtcTpStatTit(String dtcTpStatTit) {
        this.dtcTpStatTit = Functions.subString(dtcTpStatTit, Len.DTC_TP_STAT_TIT);
    }

    public String getDtcTpStatTit() {
        return this.dtcTpStatTit;
    }

    public void setDtcDsRiga(long dtcDsRiga) {
        this.dtcDsRiga = dtcDsRiga;
    }

    public long getDtcDsRiga() {
        return this.dtcDsRiga;
    }

    public void setDtcDsOperSql(char dtcDsOperSql) {
        this.dtcDsOperSql = dtcDsOperSql;
    }

    public char getDtcDsOperSql() {
        return this.dtcDsOperSql;
    }

    public void setDtcDsVer(int dtcDsVer) {
        this.dtcDsVer = dtcDsVer;
    }

    public int getDtcDsVer() {
        return this.dtcDsVer;
    }

    public void setDtcDsTsIniCptz(long dtcDsTsIniCptz) {
        this.dtcDsTsIniCptz = dtcDsTsIniCptz;
    }

    public long getDtcDsTsIniCptz() {
        return this.dtcDsTsIniCptz;
    }

    public void setDtcDsTsEndCptz(long dtcDsTsEndCptz) {
        this.dtcDsTsEndCptz = dtcDsTsEndCptz;
    }

    public long getDtcDsTsEndCptz() {
        return this.dtcDsTsEndCptz;
    }

    public void setDtcDsUtente(String dtcDsUtente) {
        this.dtcDsUtente = Functions.subString(dtcDsUtente, Len.DTC_DS_UTENTE);
    }

    public String getDtcDsUtente() {
        return this.dtcDsUtente;
    }

    public void setDtcDsStatoElab(char dtcDsStatoElab) {
        this.dtcDsStatoElab = dtcDsStatoElab;
    }

    public char getDtcDsStatoElab() {
        return this.dtcDsStatoElab;
    }

    public DtcAcqExp getDtcAcqExp() {
        return dtcAcqExp;
    }

    public DtcCarAcq getDtcCarAcq() {
        return dtcCarAcq;
    }

    public DtcCarGest getDtcCarGest() {
        return dtcCarGest;
    }

    public DtcCarIas getDtcCarIas() {
        return dtcCarIas;
    }

    public DtcCarInc getDtcCarInc() {
        return dtcCarInc;
    }

    public DtcCnbtAntirac getDtcCnbtAntirac() {
        return dtcCnbtAntirac;
    }

    public DtcCommisInter getDtcCommisInter() {
        return dtcCommisInter;
    }

    public DtcDir getDtcDir() {
        return dtcDir;
    }

    public DtcDtEndCop getDtcDtEndCop() {
        return dtcDtEndCop;
    }

    public DtcDtEsiTit getDtcDtEsiTit() {
        return dtcDtEsiTit;
    }

    public DtcDtIniCop getDtcDtIniCop() {
        return dtcDtIniCop;
    }

    public DtcFrqMovi getDtcFrqMovi() {
        return dtcFrqMovi;
    }

    public DtcIdMoviChiu getDtcIdMoviChiu() {
        return dtcIdMoviChiu;
    }

    public DtcImpAder getDtcImpAder() {
        return dtcImpAder;
    }

    public DtcImpAz getDtcImpAz() {
        return dtcImpAz;
    }

    public DtcImpTfr getDtcImpTfr() {
        return dtcImpTfr;
    }

    public DtcImpTfrStrc getDtcImpTfrStrc() {
        return dtcImpTfrStrc;
    }

    public DtcImpTrasfe getDtcImpTrasfe() {
        return dtcImpTrasfe;
    }

    public DtcImpVolo getDtcImpVolo() {
        return dtcImpVolo;
    }

    public DtcIntrFraz getDtcIntrFraz() {
        return dtcIntrFraz;
    }

    public DtcIntrMora getDtcIntrMora() {
        return dtcIntrMora;
    }

    public DtcIntrRetdt getDtcIntrRetdt() {
        return dtcIntrRetdt;
    }

    public DtcIntrRiat getDtcIntrRiat() {
        return dtcIntrRiat;
    }

    public DtcManfeeAntic getDtcManfeeAntic() {
        return dtcManfeeAntic;
    }

    public DtcManfeeRec getDtcManfeeRec() {
        return dtcManfeeRec;
    }

    public DtcManfeeRicor getDtcManfeeRicor() {
        return dtcManfeeRicor;
    }

    public DtcNumGgRitardoPag getDtcNumGgRitardoPag() {
        return dtcNumGgRitardoPag;
    }

    public DtcNumGgRival getDtcNumGgRival() {
        return dtcNumGgRival;
    }

    public DtcPreNet getDtcPreNet() {
        return dtcPreNet;
    }

    public DtcPrePpIas getDtcPrePpIas() {
        return dtcPrePpIas;
    }

    public DtcPreSoloRsh getDtcPreSoloRsh() {
        return dtcPreSoloRsh;
    }

    public DtcPreTot getDtcPreTot() {
        return dtcPreTot;
    }

    public DtcProvAcq1aa getDtcProvAcq1aa() {
        return dtcProvAcq1aa;
    }

    public DtcProvAcq2aa getDtcProvAcq2aa() {
        return dtcProvAcq2aa;
    }

    public DtcProvDaRec getDtcProvDaRec() {
        return dtcProvDaRec;
    }

    public DtcProvInc getDtcProvInc() {
        return dtcProvInc;
    }

    public DtcProvRicor getDtcProvRicor() {
        return dtcProvRicor;
    }

    public DtcRemunAss getDtcRemunAss() {
        return dtcRemunAss;
    }

    public DtcSoprAlt getDtcSoprAlt() {
        return dtcSoprAlt;
    }

    public DtcSoprProf getDtcSoprProf() {
        return dtcSoprProf;
    }

    public DtcSoprSan getDtcSoprSan() {
        return dtcSoprSan;
    }

    public DtcSoprSpo getDtcSoprSpo() {
        return dtcSoprSpo;
    }

    public DtcSoprTec getDtcSoprTec() {
        return dtcSoprTec;
    }

    public DtcSpeAge getDtcSpeAge() {
        return dtcSpeAge;
    }

    public DtcSpeMed getDtcSpeMed() {
        return dtcSpeMed;
    }

    public DtcTax getDtcTax() {
        return dtcTax;
    }

    public DtcTotIntrPrest getDtcTotIntrPrest() {
        return dtcTotIntrPrest;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_TP_OGG = 2;
        public static final int DTC_COD_DVS = 20;
        public static final int DTC_TP_RGM_FISC = 2;
        public static final int DTC_COD_TARI = 12;
        public static final int DTC_TP_STAT_TIT = 2;
        public static final int DTC_DS_UTENTE = 20;
        public static final int DTC_ID_DETT_TIT_CONT = 5;
        public static final int DTC_ID_TIT_CONT = 5;
        public static final int DTC_ID_OGG = 5;
        public static final int DTC_ID_MOVI_CRZ = 5;
        public static final int DTC_DT_INI_EFF = 5;
        public static final int DTC_DT_END_EFF = 5;
        public static final int DTC_COD_COMP_ANIA = 3;
        public static final int DTC_DS_RIGA = 6;
        public static final int DTC_DS_OPER_SQL = 1;
        public static final int DTC_DS_VER = 5;
        public static final int DTC_DS_TS_INI_CPTZ = 10;
        public static final int DTC_DS_TS_END_CPTZ = 10;
        public static final int DTC_DS_STATO_ELAB = 1;
        public static final int DETT_TIT_CONT = DTC_ID_DETT_TIT_CONT + DTC_ID_TIT_CONT + DTC_ID_OGG + DTC_TP_OGG + DTC_ID_MOVI_CRZ + DtcIdMoviChiu.Len.DTC_ID_MOVI_CHIU + DTC_DT_INI_EFF + DTC_DT_END_EFF + DTC_COD_COMP_ANIA + DtcDtIniCop.Len.DTC_DT_INI_COP + DtcDtEndCop.Len.DTC_DT_END_COP + DtcPreNet.Len.DTC_PRE_NET + DtcIntrFraz.Len.DTC_INTR_FRAZ + DtcIntrMora.Len.DTC_INTR_MORA + DtcIntrRetdt.Len.DTC_INTR_RETDT + DtcIntrRiat.Len.DTC_INTR_RIAT + DtcDir.Len.DTC_DIR + DtcSpeMed.Len.DTC_SPE_MED + DtcTax.Len.DTC_TAX + DtcSoprSan.Len.DTC_SOPR_SAN + DtcSoprSpo.Len.DTC_SOPR_SPO + DtcSoprTec.Len.DTC_SOPR_TEC + DtcSoprProf.Len.DTC_SOPR_PROF + DtcSoprAlt.Len.DTC_SOPR_ALT + DtcPreTot.Len.DTC_PRE_TOT + DtcPrePpIas.Len.DTC_PRE_PP_IAS + DtcPreSoloRsh.Len.DTC_PRE_SOLO_RSH + DtcCarAcq.Len.DTC_CAR_ACQ + DtcCarGest.Len.DTC_CAR_GEST + DtcCarInc.Len.DTC_CAR_INC + DtcProvAcq1aa.Len.DTC_PROV_ACQ1AA + DtcProvAcq2aa.Len.DTC_PROV_ACQ2AA + DtcProvRicor.Len.DTC_PROV_RICOR + DtcProvInc.Len.DTC_PROV_INC + DtcProvDaRec.Len.DTC_PROV_DA_REC + DTC_COD_DVS + DtcFrqMovi.Len.DTC_FRQ_MOVI + DTC_TP_RGM_FISC + DTC_COD_TARI + DTC_TP_STAT_TIT + DtcImpAz.Len.DTC_IMP_AZ + DtcImpAder.Len.DTC_IMP_ADER + DtcImpTfr.Len.DTC_IMP_TFR + DtcImpVolo.Len.DTC_IMP_VOLO + DtcManfeeAntic.Len.DTC_MANFEE_ANTIC + DtcManfeeRicor.Len.DTC_MANFEE_RICOR + DtcManfeeRec.Len.DTC_MANFEE_REC + DtcDtEsiTit.Len.DTC_DT_ESI_TIT + DtcSpeAge.Len.DTC_SPE_AGE + DtcCarIas.Len.DTC_CAR_IAS + DtcTotIntrPrest.Len.DTC_TOT_INTR_PREST + DTC_DS_RIGA + DTC_DS_OPER_SQL + DTC_DS_VER + DTC_DS_TS_INI_CPTZ + DTC_DS_TS_END_CPTZ + DTC_DS_UTENTE + DTC_DS_STATO_ELAB + DtcImpTrasfe.Len.DTC_IMP_TRASFE + DtcImpTfrStrc.Len.DTC_IMP_TFR_STRC + DtcNumGgRitardoPag.Len.DTC_NUM_GG_RITARDO_PAG + DtcNumGgRival.Len.DTC_NUM_GG_RIVAL + DtcAcqExp.Len.DTC_ACQ_EXP + DtcRemunAss.Len.DTC_REMUN_ASS + DtcCommisInter.Len.DTC_COMMIS_INTER + DtcCnbtAntirac.Len.DTC_CNBT_ANTIRAC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_ID_DETT_TIT_CONT = 9;
            public static final int DTC_ID_TIT_CONT = 9;
            public static final int DTC_ID_OGG = 9;
            public static final int DTC_ID_MOVI_CRZ = 9;
            public static final int DTC_DT_INI_EFF = 8;
            public static final int DTC_DT_END_EFF = 8;
            public static final int DTC_COD_COMP_ANIA = 5;
            public static final int DTC_DS_RIGA = 10;
            public static final int DTC_DS_VER = 9;
            public static final int DTC_DS_TS_INI_CPTZ = 18;
            public static final int DTC_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
