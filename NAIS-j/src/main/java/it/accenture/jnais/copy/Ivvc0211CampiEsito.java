package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0211-CAMPI-ESITO<br>
 * Variable: IVVC0211-CAMPI-ESITO from copybook IVVC0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ivvc0211CampiEsito {

    //==== PROPERTIES ====
    //Original name: IVVC0211-DESCRIZ-ERR
    private String descrizErr = DefaultValues.stringVal(Len.DESCRIZ_ERR);
    //Original name: IVVC0211-COD-SERVIZIO-BE
    private String codServizioBe = DefaultValues.stringVal(Len.COD_SERVIZIO_BE);
    //Original name: IVVC0211-NOME-TABELLA
    private String nomeTabella = DefaultValues.stringVal(Len.NOME_TABELLA);
    //Original name: IVVC0211-KEY-TABELLA
    private String keyTabella = DefaultValues.stringVal(Len.KEY_TABELLA);

    //==== METHODS ====
    public void setCampiEsitoBytes(byte[] buffer) {
        setCampiEsitoBytes(buffer, 1);
    }

    public void setCampiEsitoBytes(byte[] buffer, int offset) {
        int position = offset;
        descrizErr = MarshalByte.readString(buffer, position, Len.DESCRIZ_ERR);
        position += Len.DESCRIZ_ERR;
        codServizioBe = MarshalByte.readString(buffer, position, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        nomeTabella = MarshalByte.readString(buffer, position, Len.NOME_TABELLA);
        position += Len.NOME_TABELLA;
        keyTabella = MarshalByte.readString(buffer, position, Len.KEY_TABELLA);
    }

    public byte[] getCampiEsitoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, descrizErr, Len.DESCRIZ_ERR);
        position += Len.DESCRIZ_ERR;
        MarshalByte.writeString(buffer, position, codServizioBe, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        MarshalByte.writeString(buffer, position, nomeTabella, Len.NOME_TABELLA);
        position += Len.NOME_TABELLA;
        MarshalByte.writeString(buffer, position, keyTabella, Len.KEY_TABELLA);
        return buffer;
    }

    public void setDescrizErr(String descrizErr) {
        this.descrizErr = Functions.subString(descrizErr, Len.DESCRIZ_ERR);
    }

    public String getDescrizErr() {
        return this.descrizErr;
    }

    public String getDescrizErrFormatted() {
        return Functions.padBlanks(getDescrizErr(), Len.DESCRIZ_ERR);
    }

    public void setCodServizioBe(String codServizioBe) {
        this.codServizioBe = Functions.subString(codServizioBe, Len.COD_SERVIZIO_BE);
    }

    public String getCodServizioBe() {
        return this.codServizioBe;
    }

    public void setNomeTabella(String nomeTabella) {
        this.nomeTabella = Functions.subString(nomeTabella, Len.NOME_TABELLA);
    }

    public String getNomeTabella() {
        return this.nomeTabella;
    }

    public String getS211NomeTabellaFormatted() {
        return Functions.padBlanks(getNomeTabella(), Len.NOME_TABELLA);
    }

    public void setKeyTabella(String keyTabella) {
        this.keyTabella = Functions.subString(keyTabella, Len.KEY_TABELLA);
    }

    public String getKeyTabella() {
        return this.keyTabella;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESCRIZ_ERR = 300;
        public static final int COD_SERVIZIO_BE = 8;
        public static final int NOME_TABELLA = 18;
        public static final int KEY_TABELLA = 20;
        public static final int CAMPI_ESITO = DESCRIZ_ERR + COD_SERVIZIO_BE + NOME_TABELLA + KEY_TABELLA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
