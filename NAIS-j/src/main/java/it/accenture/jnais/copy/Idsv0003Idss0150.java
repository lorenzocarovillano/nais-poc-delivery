package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.Idsv0003FormatoDataDb;
import it.accenture.jnais.ws.enums.Idsv0003Operazione;
import it.accenture.jnais.ws.enums.Idsv0003ReturnCode;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;

/**Original name: IDSV0003<br>
 * Variable: IDSV0003 from copybook IDSV0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Idsv0003Idss0150 {

    //==== PROPERTIES ====
    //Original name: IDSV0003-FORMATO-DATA-DB
    private Idsv0003FormatoDataDb formatoDataDb = new Idsv0003FormatoDataDb();
    /**Original name: IDSV0003-OPERAZIONE<br>
	 * <pre> -- CAMPI OPERAZIONE
	 *     COPY IDSV0008.
	 *     inizio COPY IDSV0008.</pre>*/
    private Idsv0003Operazione operazione = new Idsv0003Operazione();
    /**Original name: IDSV0003-RETURN-CODE<br>
	 * <pre> --   CAMPI ERRORI
	 *        COPY IDSV0006 REPLACING ==IDSV0003== BY ==IDSV0003==.
	 *      inizio COPY IDSV0006.
	 *  -- RETURN-CODE
	 *      COPY IDSV0004.
	 *      inizio COPY IDSV0004.</pre>*/
    private Idsv0003ReturnCode returnCode = new Idsv0003ReturnCode();
    /**Original name: IDSV0003-SQLCODE<br>
	 * <pre>   fine COPY IDSV0004</pre>*/
    private Idsv0003Sqlcode sqlcode = new Idsv0003Sqlcode();
    //Original name: IDSV0003-CAMPI-ESITO
    private Idsv0003CampiEsito campiEsito = new Idsv0003CampiEsito();

    //==== METHODS ====
    public Idsv0003CampiEsito getCampiEsito() {
        return campiEsito;
    }

    public Idsv0003FormatoDataDb getFormatoDataDb() {
        return formatoDataDb;
    }

    public Idsv0003Operazione getOperazione() {
        return operazione;
    }

    public Idsv0003ReturnCode getReturnCode() {
        return returnCode;
    }

    public Idsv0003Sqlcode getSqlcode() {
        return sqlcode;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_MAIN_BATCH = 8;
        public static final int TIPO_MOVIMENTO = 5;
        public static final int SESSIONE = 20;
        public static final int USER_NAME = 20;
        public static final int ID_MOVI_ANNULLATO = 9;
        public static final int BUFFER_WHERE_COND = 300;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
