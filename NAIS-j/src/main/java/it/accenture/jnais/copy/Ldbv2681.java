package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV2681<br>
 * Variable: LDBV2681 from copybook LDBV2681<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv2681 {

    //==== PROPERTIES ====
    //Original name: LDBV2681-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV2681-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV2681-TP-MOVI-01
    private int tpMovi01 = DefaultValues.INT_VAL;
    //Original name: LDBV2681-TP-MOVI-02
    private int tpMovi02 = DefaultValues.INT_VAL;
    //Original name: LDBV2681-TP-MOVI-03
    private int tpMovi03 = DefaultValues.INT_VAL;
    //Original name: LDBV2681-TP-MOVI-04
    private int tpMovi04 = DefaultValues.INT_VAL;
    //Original name: LDBV2681-TP-MOVI-05
    private int tpMovi05 = DefaultValues.INT_VAL;
    //Original name: LDBV2681-TP-MOVI-06
    private int tpMovi06 = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbv2681Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV2681];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV2681);
        setLdbv2681Bytes(buffer, 1);
    }

    public String getLdbv2681Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv2681Bytes());
    }

    public byte[] getLdbv2681Bytes() {
        byte[] buffer = new byte[Len.LDBV2681];
        return getLdbv2681Bytes(buffer, 1);
    }

    public void setLdbv2681Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpMovi01 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI01, 0);
        position += Len.TP_MOVI01;
        tpMovi02 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI02, 0);
        position += Len.TP_MOVI02;
        tpMovi03 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI03, 0);
        position += Len.TP_MOVI03;
        tpMovi04 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI04, 0);
        position += Len.TP_MOVI04;
        tpMovi05 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI05, 0);
        position += Len.TP_MOVI05;
        tpMovi06 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI06, 0);
    }

    public byte[] getLdbv2681Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi01, Len.Int.TP_MOVI01, 0);
        position += Len.TP_MOVI01;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi02, Len.Int.TP_MOVI02, 0);
        position += Len.TP_MOVI02;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi03, Len.Int.TP_MOVI03, 0);
        position += Len.TP_MOVI03;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi04, Len.Int.TP_MOVI04, 0);
        position += Len.TP_MOVI04;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi05, Len.Int.TP_MOVI05, 0);
        position += Len.TP_MOVI05;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi06, Len.Int.TP_MOVI06, 0);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpMovi01(int tpMovi01) {
        this.tpMovi01 = tpMovi01;
    }

    public int getTpMovi01() {
        return this.tpMovi01;
    }

    public void setTpMovi02(int tpMovi02) {
        this.tpMovi02 = tpMovi02;
    }

    public int getTpMovi02() {
        return this.tpMovi02;
    }

    public void setTpMovi03(int tpMovi03) {
        this.tpMovi03 = tpMovi03;
    }

    public int getTpMovi03() {
        return this.tpMovi03;
    }

    public void setTpMovi04(int tpMovi04) {
        this.tpMovi04 = tpMovi04;
    }

    public int getTpMovi04() {
        return this.tpMovi04;
    }

    public void setTpMovi05(int tpMovi05) {
        this.tpMovi05 = tpMovi05;
    }

    public int getTpMovi05() {
        return this.tpMovi05;
    }

    public void setTpMovi06(int tpMovi06) {
        this.tpMovi06 = tpMovi06;
    }

    public int getTpMovi06() {
        return this.tpMovi06;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int ID_OGG = 5;
        public static final int TP_MOVI01 = 3;
        public static final int TP_MOVI02 = 3;
        public static final int TP_MOVI03 = 3;
        public static final int TP_MOVI04 = 3;
        public static final int TP_MOVI05 = 3;
        public static final int TP_MOVI06 = 3;
        public static final int LDBV2681 = ID_OGG + TP_OGG + TP_MOVI01 + TP_MOVI02 + TP_MOVI03 + TP_MOVI04 + TP_MOVI05 + TP_MOVI06;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int TP_MOVI01 = 5;
            public static final int TP_MOVI02 = 5;
            public static final int TP_MOVI03 = 5;
            public static final int TP_MOVI04 = 5;
            public static final int TP_MOVI05 = 5;
            public static final int TP_MOVI06 = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
