package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: BUS-SERV-OK-WARN-REPORT<br>
 * Variable: BUS-SERV-OK-WARN-REPORT from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class BusServOkWarnReport {

    //==== PROPERTIES ====
    //Original name: FILLER-BUS-SERV-OK-WARN-REPORT
    private String flr1 = "            DI CUI WARNING";
    //Original name: FILLER-BUS-SERV-OK-WARN-REPORT-1
    private String flr2 = " :";
    //Original name: REP-BUS-SERV-WARNING
    private String repBusServWarning = "000000000";

    //==== METHODS ====
    public String getBusServOkWarnReportFormatted() {
        return MarshalByteExt.bufferToStr(getBusServOkWarnReportBytes());
    }

    public byte[] getBusServOkWarnReportBytes() {
        byte[] buffer = new byte[Len.BUS_SERV_OK_WARN_REPORT];
        return getBusServOkWarnReportBytes(buffer, 1);
    }

    public byte[] getBusServOkWarnReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, repBusServWarning, Len.REP_BUS_SERV_WARNING);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public void setRepBusServWarning(long repBusServWarning) {
        this.repBusServWarning = PicFormatter.display("Z(8)9").format(repBusServWarning).toString();
    }

    public long getRepBusServWarning() {
        return PicParser.display("Z(8)9").parseLong(this.repBusServWarning);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int REP_BUS_SERV_WARNING = 9;
        public static final int FLR1 = 50;
        public static final int FLR2 = 4;
        public static final int BUS_SERV_OK_WARN_REPORT = REP_BUS_SERV_WARNING + FLR1 + FLR2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
