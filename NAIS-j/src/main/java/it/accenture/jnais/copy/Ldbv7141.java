package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV7141<br>
 * Variable: LDBV7141 from copybook LDBV7141<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv7141 {

    //==== PROPERTIES ====
    //Original name: LDBV7141-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV7141-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV7141-TP-STA-TIT1
    private String tpStaTit1 = DefaultValues.stringVal(Len.TP_STA_TIT1);
    //Original name: LDBV7141-TP-STA-TIT2
    private String tpStaTit2 = DefaultValues.stringVal(Len.TP_STA_TIT2);
    //Original name: LDBV7141-TP-TIT
    private String tpTit = DefaultValues.stringVal(Len.TP_TIT);
    //Original name: LDBV7141-DT-DA
    private int dtDa = DefaultValues.INT_VAL;
    //Original name: LDBV7141-DT-DA-DB
    private String dtDaDb = "";
    //Original name: LDBV7141-DT-A
    private int dtA = DefaultValues.INT_VAL;
    //Original name: LDBV7141-DT-A-DB
    private String dtADb = "";

    //==== METHODS ====
    public void setLdbv7141Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV7141];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV7141);
        setLdbv7141Bytes(buffer, 1);
    }

    public String getLdbv7141Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv7141Bytes());
    }

    public byte[] getLdbv7141Bytes() {
        byte[] buffer = new byte[Len.LDBV7141];
        return getLdbv7141Bytes(buffer, 1);
    }

    public void setLdbv7141Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpStaTit1 = MarshalByte.readString(buffer, position, Len.TP_STA_TIT1);
        position += Len.TP_STA_TIT1;
        tpStaTit2 = MarshalByte.readString(buffer, position, Len.TP_STA_TIT2);
        position += Len.TP_STA_TIT2;
        tpTit = MarshalByte.readString(buffer, position, Len.TP_TIT);
        position += Len.TP_TIT;
        dtDa = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DA, 0);
        position += Len.DT_DA;
        dtDaDb = MarshalByte.readString(buffer, position, Len.DT_DA_DB);
        position += Len.DT_DA_DB;
        dtA = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_A, 0);
        position += Len.DT_A;
        dtADb = MarshalByte.readString(buffer, position, Len.DT_A_DB);
    }

    public byte[] getLdbv7141Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tpStaTit1, Len.TP_STA_TIT1);
        position += Len.TP_STA_TIT1;
        MarshalByte.writeString(buffer, position, tpStaTit2, Len.TP_STA_TIT2);
        position += Len.TP_STA_TIT2;
        MarshalByte.writeString(buffer, position, tpTit, Len.TP_TIT);
        position += Len.TP_TIT;
        MarshalByte.writeIntAsPacked(buffer, position, dtDa, Len.Int.DT_DA, 0);
        position += Len.DT_DA;
        MarshalByte.writeString(buffer, position, dtDaDb, Len.DT_DA_DB);
        position += Len.DT_DA_DB;
        MarshalByte.writeIntAsPacked(buffer, position, dtA, Len.Int.DT_A, 0);
        position += Len.DT_A;
        MarshalByte.writeString(buffer, position, dtADb, Len.DT_A_DB);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpStaTit1(String tpStaTit1) {
        this.tpStaTit1 = Functions.subString(tpStaTit1, Len.TP_STA_TIT1);
    }

    public String getTpStaTit1() {
        return this.tpStaTit1;
    }

    public void setTpStaTit2(String tpStaTit2) {
        this.tpStaTit2 = Functions.subString(tpStaTit2, Len.TP_STA_TIT2);
    }

    public String getTpStaTit2() {
        return this.tpStaTit2;
    }

    public void setTpTit(String tpTit) {
        this.tpTit = Functions.subString(tpTit, Len.TP_TIT);
    }

    public String getTpTit() {
        return this.tpTit;
    }

    public void setDtDa(int dtDa) {
        this.dtDa = dtDa;
    }

    public int getDtDa() {
        return this.dtDa;
    }

    public void setDtDaDb(String dtDaDb) {
        this.dtDaDb = Functions.subString(dtDaDb, Len.DT_DA_DB);
    }

    public String getDtDaDb() {
        return this.dtDaDb;
    }

    public void setDtA(int dtA) {
        this.dtA = dtA;
    }

    public int getDtA() {
        return this.dtA;
    }

    public void setDtADb(String dtADb) {
        this.dtADb = Functions.subString(dtADb, Len.DT_A_DB);
    }

    public String getDtADb() {
        return this.dtADb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int TP_STA_TIT1 = 2;
        public static final int TP_STA_TIT2 = 2;
        public static final int TP_TIT = 2;
        public static final int ID_OGG = 5;
        public static final int DT_DA = 5;
        public static final int DT_DA_DB = 10;
        public static final int DT_A = 5;
        public static final int DT_A_DB = 10;
        public static final int LDBV7141 = ID_OGG + TP_OGG + TP_STA_TIT1 + TP_STA_TIT2 + TP_TIT + DT_DA + DT_DA_DB + DT_A + DT_A_DB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int DT_DA = 8;
            public static final int DT_A = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
