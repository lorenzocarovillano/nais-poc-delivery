package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVF111-INPUT<br>
 * Variable: LDBVF111-INPUT from copybook LDBVF111<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbvf111Input {

    //==== PROPERTIES ====
    //Original name: LDBVF111-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBVF111-TP-STAT-TIT1
    private String tpStatTit1 = DefaultValues.stringVal(Len.TP_STAT_TIT1);
    //Original name: LDBVF111-TP-STAT-TIT2
    private String tpStatTit2 = DefaultValues.stringVal(Len.TP_STAT_TIT2);
    //Original name: LDBVF111-TP-STAT-TIT3
    private String tpStatTit3 = DefaultValues.stringVal(Len.TP_STAT_TIT3);
    //Original name: LDBVF111-TP-STAT-TIT4
    private String tpStatTit4 = DefaultValues.stringVal(Len.TP_STAT_TIT4);
    //Original name: LDBVF111-TP-STAT-TIT5
    private String tpStatTit5 = DefaultValues.stringVal(Len.TP_STAT_TIT5);
    //Original name: LDBVF111-DT-DECOR-TRCH
    private int dtDecorTrch = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbvf981Formatted(String data) {
        byte[] buffer = new byte[Len.INPUT];
        MarshalByte.writeString(buffer, 1, data, Len.INPUT);
        setInputBytes(buffer, 1);
    }

    public String getLdbvf981Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvf981Bytes());
    }

    public byte[] getLdbvf981Bytes() {
        byte[] buffer = new byte[Len.INPUT];
        return getInputBytes(buffer, 1);
    }

    public void setInputBytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpStatTit1 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT1);
        position += Len.TP_STAT_TIT1;
        tpStatTit2 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT2);
        position += Len.TP_STAT_TIT2;
        tpStatTit3 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT3);
        position += Len.TP_STAT_TIT3;
        tpStatTit4 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT4);
        position += Len.TP_STAT_TIT4;
        tpStatTit5 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT5);
        position += Len.TP_STAT_TIT5;
        dtDecorTrch = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DECOR_TRCH, 0);
    }

    public byte[] getInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpStatTit1, Len.TP_STAT_TIT1);
        position += Len.TP_STAT_TIT1;
        MarshalByte.writeString(buffer, position, tpStatTit2, Len.TP_STAT_TIT2);
        position += Len.TP_STAT_TIT2;
        MarshalByte.writeString(buffer, position, tpStatTit3, Len.TP_STAT_TIT3);
        position += Len.TP_STAT_TIT3;
        MarshalByte.writeString(buffer, position, tpStatTit4, Len.TP_STAT_TIT4);
        position += Len.TP_STAT_TIT4;
        MarshalByte.writeString(buffer, position, tpStatTit5, Len.TP_STAT_TIT5);
        position += Len.TP_STAT_TIT5;
        MarshalByte.writeIntAsPacked(buffer, position, dtDecorTrch, Len.Int.DT_DECOR_TRCH, 0);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpStatTit1(String tpStatTit1) {
        this.tpStatTit1 = Functions.subString(tpStatTit1, Len.TP_STAT_TIT1);
    }

    public String getTpStatTit1() {
        return this.tpStatTit1;
    }

    public void setTpStatTit2(String tpStatTit2) {
        this.tpStatTit2 = Functions.subString(tpStatTit2, Len.TP_STAT_TIT2);
    }

    public String getTpStatTit2() {
        return this.tpStatTit2;
    }

    public void setTpStatTit3(String tpStatTit3) {
        this.tpStatTit3 = Functions.subString(tpStatTit3, Len.TP_STAT_TIT3);
    }

    public String getTpStatTit3() {
        return this.tpStatTit3;
    }

    public void setTpStatTit4(String tpStatTit4) {
        this.tpStatTit4 = Functions.subString(tpStatTit4, Len.TP_STAT_TIT4);
    }

    public String getTpStatTit4() {
        return this.tpStatTit4;
    }

    public void setTpStatTit5(String tpStatTit5) {
        this.tpStatTit5 = Functions.subString(tpStatTit5, Len.TP_STAT_TIT5);
    }

    public String getTpStatTit5() {
        return this.tpStatTit5;
    }

    public void setDtDecorTrch(int dtDecorTrch) {
        this.dtDecorTrch = dtDecorTrch;
    }

    public int getDtDecorTrch() {
        return this.dtDecorTrch;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG = 5;
        public static final int TP_STAT_TIT1 = 2;
        public static final int TP_STAT_TIT2 = 2;
        public static final int TP_STAT_TIT3 = 2;
        public static final int TP_STAT_TIT4 = 2;
        public static final int TP_STAT_TIT5 = 2;
        public static final int DT_DECOR_TRCH = 5;
        public static final int INPUT = ID_OGG + TP_STAT_TIT1 + TP_STAT_TIT2 + TP_STAT_TIT3 + TP_STAT_TIT4 + TP_STAT_TIT5 + DT_DECOR_TRCH;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int DT_DECOR_TRCH = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
