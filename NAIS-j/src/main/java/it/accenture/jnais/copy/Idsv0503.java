package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Idsv0503AlfaTrovato;
import it.accenture.jnais.ws.enums.Idsv0503DatoInputTrovato;
import it.accenture.jnais.ws.enums.Idsv0503DecTrovato;
import it.accenture.jnais.ws.enums.Idsv0503FineStringa;
import it.accenture.jnais.ws.enums.Idsv0503IntTrovato;
import it.accenture.jnais.ws.enums.Idsv0503PosSegno;
import it.accenture.jnais.ws.enums.Idsv0503ReturnCode;
import it.accenture.jnais.ws.enums.Idsv0503SegnoPostiv;
import it.accenture.jnais.ws.enums.Idsv0503TipoSegno;
import it.accenture.jnais.ws.occurs.Idsv0503TabVariabili;
import it.accenture.jnais.ws.redefines.Idsv0503CampoDecimali;
import it.accenture.jnais.ws.redefines.Idsv0503CampoInteri;
import it.accenture.jnais.ws.redefines.Idsv0503StructNum;

/**Original name: IDSV0503<br>
 * Variable: IDSV0503 from copybook IDSV0503<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv0503 {

    //==== PROPERTIES ====
    public static final int IDSV0503_TAB_VARIABILI_MAXOCCURS = 100;
    //Original name: IDSV0503-ELE-VARIABILI-MAX
    private short idsv0503EleVariabiliMax = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-TAB-VARIABILI
    private Idsv0503TabVariabili[] idsv0503TabVariabili = new Idsv0503TabVariabili[IDSV0503_TAB_VARIABILI_MAXOCCURS];
    //Original name: IDSV0503-TIPO-FORMATO
    private Idsv0503TipoFormato idsv0503TipoFormato = new Idsv0503TipoFormato();
    //Original name: IDSV0503-TIPO-SEGNO
    private Idsv0503TipoSegno idsv0503TipoSegno = new Idsv0503TipoSegno();
    //Original name: IDSV0503-DATO-INPUT-TROVATO
    private Idsv0503DatoInputTrovato idsv0503DatoInputTrovato = new Idsv0503DatoInputTrovato();
    //Original name: IDSV0503-FINE-STRINGA
    private Idsv0503FineStringa idsv0503FineStringa = new Idsv0503FineStringa();
    //Original name: IDSV0503-ALFA-TROVATO
    private Idsv0503AlfaTrovato idsv0503AlfaTrovato = new Idsv0503AlfaTrovato();
    //Original name: IDSV0503-DEC-TROVATO
    private Idsv0503DecTrovato idsv0503DecTrovato = new Idsv0503DecTrovato();
    //Original name: IDSV0503-INT-TROVATO
    private Idsv0503IntTrovato idsv0503IntTrovato = new Idsv0503IntTrovato();
    //Original name: IDSV0503-SEGNO-POSTIV
    private Idsv0503SegnoPostiv idsv0503SegnoPostiv = new Idsv0503SegnoPostiv();
    //Original name: IDSV0503-POS-SEGNO
    private Idsv0503PosSegno idsv0503PosSegno = new Idsv0503PosSegno();
    /**Original name: IDSV0503-IND-VAR<br>
	 * <pre>**************************************************************
	 *  INDICI
	 * **************************************************************</pre>*/
    private short idsv0503IndVar = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-IND-STRINGA
    private short idsv0503IndStringa = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-IND-ALFA
    private String idsv0503IndAlfa = DefaultValues.stringVal(Len.IDSV0503_IND_ALFA);
    //Original name: IDSV0503-IND-INT
    private short idsv0503IndInt = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-IND-DEC
    private short idsv0503IndDec = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-IND-OUT
    private short idsv0503IndOut = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-IND-RICERCA
    private short idsv0503IndRicerca = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-POSIZ-MEDIA
    private String idsv0503PosizMedia = DefaultValues.stringVal(Len.IDSV0503_POSIZ_MEDIA);
    //Original name: IDSV0503-INTERVALLO
    private String idsv0503Intervallo = DefaultValues.stringVal(Len.IDSV0503_INTERVALLO);
    /**Original name: IDSV0503-LIMITE-INT-DEC<br>
	 * <pre>**************************************************************
	 *  LIMITI
	 * **************************************************************</pre>*/
    private String idsv0503LimiteIntDec = "18";
    //Original name: IDSV0503-LIMITE-STRINGA
    private String idsv0503LimiteStringa = "0060";
    //Original name: IDSV0503-LIMITE-STRINGA-OUTPUT
    private String idsv0503LimiteStringaOutput = "4000";
    //Original name: IDSV0503-LIMITE-ARRAY-INPUT
    private short idsv0503LimiteArrayInput = ((short)60);
    //Original name: IDSV0503-LIMITE-DECIMALI
    private String idsv0503LimiteDecimali = "00";
    //Original name: IDSV0503-LIMITE-INTERI
    private String idsv0503LimiteInteri = "00";
    //Original name: IDSV0503-LIM-INT-IMP
    private String idsv0503LimIntImp = "11";
    //Original name: IDSV0503-LIM-DEC-IMP
    private String idsv0503LimDecImp = "12";
    //Original name: IDSV0503-LIM-INT-PERC
    private String idsv0503LimIntPerc = "09";
    //Original name: IDSV0503-LIM-DEC-PERC
    private String idsv0503LimDecPerc = "10";
    //Original name: IDSV0503-DECIMALI-ESPOSTI-IMP
    private short idsv0503DecimaliEspostiImp = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-DECIMALI-ESPOSTI-PERC
    private short idsv0503DecimaliEspostiPerc = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-DECIMALI-ESPOSTI-TASS
    private short idsv0503DecimaliEspostiTass = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-SEPARATORE-VALORE
    private char idsv0503SeparatoreValore = DefaultValues.CHAR_VAL;
    //Original name: IDSV0503-SEPARATORE-VARIABILE
    private char idsv0503SeparatoreVariabile = DefaultValues.CHAR_VAL;
    //Original name: IDSV0503-SIMBOLO-DECIMALE
    private char idsv0503SimboloDecimale = DefaultValues.CHAR_VAL;
    //Original name: IDSV0503-SPAZIO-RICHIESTO
    private String idsv0503SpazioRichiesto = DefaultValues.stringVal(Len.IDSV0503_SPAZIO_RICHIESTO);
    //Original name: IDSV0503-SPAZIO-RESTANTE
    private String idsv0503SpazioRestante = DefaultValues.stringVal(Len.IDSV0503_SPAZIO_RESTANTE);
    //Original name: IDSV0503-POSIZIONE
    private short idsv0503Posizione = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-POSIZIONE-TOT
    private short idsv0503PosizioneTot = DefaultValues.SHORT_VAL;
    //Original name: IDSV0503-CHAR-FINE-STRINGA
    private String idsv0503CharFineStringa = "";
    //Original name: IDSV0503-LUNG-CHAR-FINE-STR
    private short idsv0503LungCharFineStr = ((short)2);
    /**Original name: IDSV0503-TP-DATO-APP<br>
	 * <pre>**************************************************************
	 *  COMODO
	 * **************************************************************</pre>*/
    private char idsv0503TpDatoApp = DefaultValues.CHAR_VAL;
    //Original name: IDSV0503-STRUCT-NUM
    private Idsv0503StructNum idsv0503StructNum = new Idsv0503StructNum();
    //Original name: IDSV0503-CAMPO-INTERI
    private Idsv0503CampoInteri idsv0503CampoInteri = new Idsv0503CampoInteri();
    //Original name: IDSV0503-CAMPO-DECIMALI
    private Idsv0503CampoDecimali idsv0503CampoDecimali = new Idsv0503CampoDecimali();
    //Original name: IDSV0503-CAMPO-ALFA
    private String idsv0503CampoAlfa = DefaultValues.stringVal(Len.IDSV0503_CAMPO_ALFA);
    //Original name: IDSV0503-CAMPO-APPOGGIO
    private String idsv0503CampoAppoggio = DefaultValues.stringVal(Len.IDSV0503_CAMPO_APPOGGIO);
    /**Original name: IDSV0503-LEN-STRINGA-TOT<br>
	 * <pre>**************************************************************
	 *   OUTPUT
	 * **************************************************************</pre>*/
    private short idsv0503LenStringaTot = DefaultValues.BIN_SHORT_VAL;
    //Original name: IDSV0503-STRINGA-TOT
    private String idsv0503StringaTot = DefaultValues.stringVal(Len.IDSV0503_STRINGA_TOT);
    //Original name: IDSV0503-RETURN-CODE
    private Idsv0503ReturnCode idsv0503ReturnCode = new Idsv0503ReturnCode();
    //Original name: IDSV0503-DESCRIZ-ERR
    private String idsv0503DescrizErr = DefaultValues.stringVal(Len.IDSV0503_DESCRIZ_ERR);

    //==== CONSTRUCTORS ====
    public Idsv0503() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int idsv0503TabVariabiliIdx = 1; idsv0503TabVariabiliIdx <= IDSV0503_TAB_VARIABILI_MAXOCCURS; idsv0503TabVariabiliIdx++) {
            idsv0503TabVariabili[idsv0503TabVariabiliIdx - 1] = new Idsv0503TabVariabili();
        }
    }

    public void setIdsv0503InputBytes(byte[] buffer) {
        setIdsv0503InputBytes(buffer, 1);
    }

    public void setIdsv0503InputBytes(byte[] buffer, int offset) {
        int position = offset;
        idsv0503EleVariabiliMax = MarshalByte.readPackedAsShort(buffer, position, Len.Int.IDSV0503_ELE_VARIABILI_MAX, 0);
        position += Len.IDSV0503_ELE_VARIABILI_MAX;
        for (int idx = 1; idx <= IDSV0503_TAB_VARIABILI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                idsv0503TabVariabili[idx - 1].setIdsv0503TabVariabiliBytes(buffer, position);
                position += Idsv0503TabVariabili.Len.IDSV0503_TAB_VARIABILI;
            }
            else {
                idsv0503TabVariabili[idx - 1].initIdsv0503TabVariabiliSpaces();
                position += Idsv0503TabVariabili.Len.IDSV0503_TAB_VARIABILI;
            }
        }
    }

    public void setIdsv0503EleVariabiliMax(short idsv0503EleVariabiliMax) {
        this.idsv0503EleVariabiliMax = idsv0503EleVariabiliMax;
    }

    public short getIdsv0503EleVariabiliMax() {
        return this.idsv0503EleVariabiliMax;
    }

    public void setIdsv0503IndVar(short idsv0503IndVar) {
        this.idsv0503IndVar = idsv0503IndVar;
    }

    public short getIdsv0503IndVar() {
        return this.idsv0503IndVar;
    }

    public void setIdsv0503IndStringa(short idsv0503IndStringa) {
        this.idsv0503IndStringa = idsv0503IndStringa;
    }

    public short getIdsv0503IndStringa() {
        return this.idsv0503IndStringa;
    }

    public void setIdsv0503IndAlfa(short idsv0503IndAlfa) {
        this.idsv0503IndAlfa = NumericDisplay.asString(idsv0503IndAlfa, Len.IDSV0503_IND_ALFA);
    }

    public short getIdsv0503IndAlfa() {
        return NumericDisplay.asShort(this.idsv0503IndAlfa);
    }

    public String getIdsv0503IndAlfaFormatted() {
        return this.idsv0503IndAlfa;
    }

    public void setIdsv0503IndInt(short idsv0503IndInt) {
        this.idsv0503IndInt = idsv0503IndInt;
    }

    public short getIdsv0503IndInt() {
        return this.idsv0503IndInt;
    }

    public void setIdsv0503IndDec(short idsv0503IndDec) {
        this.idsv0503IndDec = idsv0503IndDec;
    }

    public short getIdsv0503IndDec() {
        return this.idsv0503IndDec;
    }

    public void setIdsv0503IndOut(short idsv0503IndOut) {
        this.idsv0503IndOut = idsv0503IndOut;
    }

    public short getIdsv0503IndOut() {
        return this.idsv0503IndOut;
    }

    public void setIdsv0503IndRicerca(short idsv0503IndRicerca) {
        this.idsv0503IndRicerca = idsv0503IndRicerca;
    }

    public short getIdsv0503IndRicerca() {
        return this.idsv0503IndRicerca;
    }

    public void setIdsv0503PosizMediaFormatted(String idsv0503PosizMedia) {
        this.idsv0503PosizMedia = Trunc.toUnsignedNumeric(idsv0503PosizMedia, Len.IDSV0503_POSIZ_MEDIA);
    }

    public short getIdsv0503PosizMedia() {
        return NumericDisplay.asShort(this.idsv0503PosizMedia);
    }

    public void setIdsv0503IntervalloFormatted(String idsv0503Intervallo) {
        this.idsv0503Intervallo = Trunc.toUnsignedNumeric(idsv0503Intervallo, Len.IDSV0503_INTERVALLO);
    }

    public short getIdsv0503Intervallo() {
        return NumericDisplay.asShort(this.idsv0503Intervallo);
    }

    public short getIdsv0503LimiteIntDec() {
        return NumericDisplay.asShort(this.idsv0503LimiteIntDec);
    }

    public String getIdsv0503LimiteIntDecFormatted() {
        return this.idsv0503LimiteIntDec;
    }

    public short getIdsv0503LimiteStringa() {
        return NumericDisplay.asShort(this.idsv0503LimiteStringa);
    }

    public String getIdsv0503LimiteStringaFormatted() {
        return this.idsv0503LimiteStringa;
    }

    public String getIdsv0503LimiteStringaOutputFormatted() {
        return this.idsv0503LimiteStringaOutput;
    }

    public short getIdsv0503LimiteArrayInput() {
        return this.idsv0503LimiteArrayInput;
    }

    public void setIdsv0503LimiteDecimaliFormatted(String idsv0503LimiteDecimali) {
        this.idsv0503LimiteDecimali = Trunc.toUnsignedNumeric(idsv0503LimiteDecimali, Len.IDSV0503_LIMITE_DECIMALI);
    }

    public short getIdsv0503LimiteDecimali() {
        return NumericDisplay.asShort(this.idsv0503LimiteDecimali);
    }

    public void setIdsv0503LimiteInteriFormatted(String idsv0503LimiteInteri) {
        this.idsv0503LimiteInteri = Trunc.toUnsignedNumeric(idsv0503LimiteInteri, Len.IDSV0503_LIMITE_INTERI);
    }

    public short getIdsv0503LimiteInteri() {
        return NumericDisplay.asShort(this.idsv0503LimiteInteri);
    }

    public String getIdsv0503LimIntImpFormatted() {
        return this.idsv0503LimIntImp;
    }

    public String getIdsv0503LimDecImpFormatted() {
        return this.idsv0503LimDecImp;
    }

    public String getIdsv0503LimIntPercFormatted() {
        return this.idsv0503LimIntPerc;
    }

    public String getIdsv0503LimDecPercFormatted() {
        return this.idsv0503LimDecPerc;
    }

    public void setIdsv0503DecimaliEspostiImp(short idsv0503DecimaliEspostiImp) {
        this.idsv0503DecimaliEspostiImp = idsv0503DecimaliEspostiImp;
    }

    public short getIdsv0503DecimaliEspostiImp() {
        return this.idsv0503DecimaliEspostiImp;
    }

    public void setIdsv0503DecimaliEspostiPerc(short idsv0503DecimaliEspostiPerc) {
        this.idsv0503DecimaliEspostiPerc = idsv0503DecimaliEspostiPerc;
    }

    public short getIdsv0503DecimaliEspostiPerc() {
        return this.idsv0503DecimaliEspostiPerc;
    }

    public void setIdsv0503DecimaliEspostiTass(short idsv0503DecimaliEspostiTass) {
        this.idsv0503DecimaliEspostiTass = idsv0503DecimaliEspostiTass;
    }

    public short getIdsv0503DecimaliEspostiTass() {
        return this.idsv0503DecimaliEspostiTass;
    }

    public void setIdsv0503SeparatoreValore(char idsv0503SeparatoreValore) {
        this.idsv0503SeparatoreValore = idsv0503SeparatoreValore;
    }

    public void setIdsv0503SeparatoreValoreFormatted(String idsv0503SeparatoreValore) {
        setIdsv0503SeparatoreValore(Functions.charAt(idsv0503SeparatoreValore, Types.CHAR_SIZE));
    }

    public char getIdsv0503SeparatoreValore() {
        return this.idsv0503SeparatoreValore;
    }

    public void setIdsv0503SeparatoreVariabile(char idsv0503SeparatoreVariabile) {
        this.idsv0503SeparatoreVariabile = idsv0503SeparatoreVariabile;
    }

    public void setIdsv0503SeparatoreVariabileFormatted(String idsv0503SeparatoreVariabile) {
        setIdsv0503SeparatoreVariabile(Functions.charAt(idsv0503SeparatoreVariabile, Types.CHAR_SIZE));
    }

    public char getIdsv0503SeparatoreVariabile() {
        return this.idsv0503SeparatoreVariabile;
    }

    public void setIdsv0503SimboloDecimale(char idsv0503SimboloDecimale) {
        this.idsv0503SimboloDecimale = idsv0503SimboloDecimale;
    }

    public void setIdsv0503SimboloDecimaleFormatted(String idsv0503SimboloDecimale) {
        setIdsv0503SimboloDecimale(Functions.charAt(idsv0503SimboloDecimale, Types.CHAR_SIZE));
    }

    public char getIdsv0503SimboloDecimale() {
        return this.idsv0503SimboloDecimale;
    }

    public void setIdsv0503SpazioRichiestoFormatted(String idsv0503SpazioRichiesto) {
        this.idsv0503SpazioRichiesto = Trunc.toUnsignedNumeric(idsv0503SpazioRichiesto, Len.IDSV0503_SPAZIO_RICHIESTO);
    }

    public short getIdsv0503SpazioRichiesto() {
        return NumericDisplay.asShort(this.idsv0503SpazioRichiesto);
    }

    public void setIdsv0503SpazioRestante(short idsv0503SpazioRestante) {
        this.idsv0503SpazioRestante = NumericDisplay.asString(idsv0503SpazioRestante, Len.IDSV0503_SPAZIO_RESTANTE);
    }

    public void setIdsv0503SpazioRestanteFormatted(String idsv0503SpazioRestante) {
        this.idsv0503SpazioRestante = Trunc.toUnsignedNumeric(idsv0503SpazioRestante, Len.IDSV0503_SPAZIO_RESTANTE);
    }

    public short getIdsv0503SpazioRestante() {
        return NumericDisplay.asShort(this.idsv0503SpazioRestante);
    }

    public void setIdsv0503Posizione(short idsv0503Posizione) {
        this.idsv0503Posizione = idsv0503Posizione;
    }

    public short getIdsv0503Posizione() {
        return this.idsv0503Posizione;
    }

    public void setIdsv0503PosizioneTot(short idsv0503PosizioneTot) {
        this.idsv0503PosizioneTot = idsv0503PosizioneTot;
    }

    public short getIdsv0503PosizioneTot() {
        return this.idsv0503PosizioneTot;
    }

    public String getIdsv0503CharFineStringa() {
        return this.idsv0503CharFineStringa;
    }

    public short getIdsv0503LungCharFineStr() {
        return this.idsv0503LungCharFineStr;
    }

    public void setIdsv0503TpDatoApp(char idsv0503TpDatoApp) {
        this.idsv0503TpDatoApp = idsv0503TpDatoApp;
    }

    public char getIdsv0503TpDatoApp() {
        return this.idsv0503TpDatoApp;
    }

    public void setIdsv0503CampoAlfa(String idsv0503CampoAlfa) {
        this.idsv0503CampoAlfa = Functions.subString(idsv0503CampoAlfa, Len.IDSV0503_CAMPO_ALFA);
    }

    public String getIdsv0503CampoAlfa() {
        return this.idsv0503CampoAlfa;
    }

    public String getIdsv0503CampoAlfaFormatted() {
        return Functions.padBlanks(getIdsv0503CampoAlfa(), Len.IDSV0503_CAMPO_ALFA);
    }

    public void setIdsv0503CampoAppoggio(String idsv0503CampoAppoggio) {
        this.idsv0503CampoAppoggio = Functions.subString(idsv0503CampoAppoggio, Len.IDSV0503_CAMPO_APPOGGIO);
    }

    public void setIdsv0503CampoAppoggioSubstring(String replacement, int start, int length) {
        idsv0503CampoAppoggio = Functions.setSubstring(idsv0503CampoAppoggio, replacement, start, length);
    }

    public String getIdsv0503CampoAppoggio() {
        return this.idsv0503CampoAppoggio;
    }

    public String getIdsv0503CampoAppoggioFormatted() {
        return Functions.padBlanks(getIdsv0503CampoAppoggio(), Len.IDSV0503_CAMPO_APPOGGIO);
    }

    public void setIdsv0503LenStringaTot(short idsv0503LenStringaTot) {
        this.idsv0503LenStringaTot = idsv0503LenStringaTot;
    }

    public short getIdsv0503LenStringaTot() {
        return this.idsv0503LenStringaTot;
    }

    public void setIdsv0503StringaTot(String idsv0503StringaTot) {
        this.idsv0503StringaTot = Functions.subString(idsv0503StringaTot, Len.IDSV0503_STRINGA_TOT);
    }

    public void setIdsv0503StringaTotSubstring(String replacement, int start, int length) {
        idsv0503StringaTot = Functions.setSubstring(idsv0503StringaTot, replacement, start, length);
    }

    public String getIdsv0503StringaTot() {
        return this.idsv0503StringaTot;
    }

    public void setIdsv0503DescrizErr(String idsv0503DescrizErr) {
        this.idsv0503DescrizErr = Functions.subString(idsv0503DescrizErr, Len.IDSV0503_DESCRIZ_ERR);
    }

    public String getIdsv0503DescrizErr() {
        return this.idsv0503DescrizErr;
    }

    public Idsv0503AlfaTrovato getIdsv0503AlfaTrovato() {
        return idsv0503AlfaTrovato;
    }

    public Idsv0503CampoDecimali getIdsv0503CampoDecimali() {
        return idsv0503CampoDecimali;
    }

    public Idsv0503CampoInteri getIdsv0503CampoInteri() {
        return idsv0503CampoInteri;
    }

    public Idsv0503DatoInputTrovato getIdsv0503DatoInputTrovato() {
        return idsv0503DatoInputTrovato;
    }

    public Idsv0503DecTrovato getIdsv0503DecTrovato() {
        return idsv0503DecTrovato;
    }

    public Idsv0503FineStringa getIdsv0503FineStringa() {
        return idsv0503FineStringa;
    }

    public Idsv0503IntTrovato getIdsv0503IntTrovato() {
        return idsv0503IntTrovato;
    }

    public Idsv0503PosSegno getIdsv0503PosSegno() {
        return idsv0503PosSegno;
    }

    public Idsv0503ReturnCode getIdsv0503ReturnCode() {
        return idsv0503ReturnCode;
    }

    public Idsv0503SegnoPostiv getIdsv0503SegnoPostiv() {
        return idsv0503SegnoPostiv;
    }

    public Idsv0503StructNum getIdsv0503StructNum() {
        return idsv0503StructNum;
    }

    public Idsv0503TabVariabili getIdsv0503TabVariabili(int idx) {
        return idsv0503TabVariabili[idx - 1];
    }

    public Idsv0503TipoFormato getIdsv0503TipoFormato() {
        return idsv0503TipoFormato;
    }

    public Idsv0503TipoSegno getIdsv0503TipoSegno() {
        return idsv0503TipoSegno;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSV0503_IND_ALFA = 4;
        public static final int IDSV0503_IND_OUT = 4;
        public static final int IDSV0503_POSIZ_MEDIA = 4;
        public static final int IDSV0503_POSIZ_SUCC = 4;
        public static final int IDSV0503_PARTE_INTERA = 4;
        public static final int IDSV0503_PARTE_DECIMALE = 4;
        public static final int IDSV0503_INTERVALLO = 4;
        public static final int IDSV0503_DECIMALI_ESPOSTI_IMP = 1;
        public static final int IDSV0503_DECIMALI_ESPOSTI_PERC = 1;
        public static final int IDSV0503_DECIMALI_ESPOSTI_TASS = 1;
        public static final int IDSV0503_SPAZIO_RICHIESTO = 4;
        public static final int IDSV0503_SPAZIO_RESTANTE = 4;
        public static final int IDSV0503_POSIZIONE = 4;
        public static final int IDSV0503_POSIZIONE_TOT = 4;
        public static final int IDSV0503_POSIZ_FINALE = 4;
        public static final int IDSV0503_CAMPO_ALFA = 65;
        public static final int IDSV0503_CAMPO_APPOGGIO = 150;
        public static final int IDSV0503_STRINGA_TOT = 4000;
        public static final int IDSV0503_DESCRIZ_ERR = 300;
        public static final int IDSV0503_ELE_VARIABILI_MAX = 3;
        public static final int IDSV0503_LIMITE_INTERI = 2;
        public static final int IDSV0503_LIMITE_DECIMALI = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IDSV0503_ELE_VARIABILI_MAX = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
