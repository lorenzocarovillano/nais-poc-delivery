package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDBVBBS2<br>
 * Copybook: IDBVBBS2 from copybook IDBVBBS2<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvbbs2 {

    //==== PROPERTIES ====
    //Original name: IND-BBS-DES
    private short bbsDes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BBS-FLAG-TO-EXECUTE
    private short bbsFlagToExecute = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setBbsDes(short bbsDes) {
        this.bbsDes = bbsDes;
    }

    public short getBbsDes() {
        return this.bbsDes;
    }

    public void setBbsFlagToExecute(short bbsFlagToExecute) {
        this.bbsFlagToExecute = bbsFlagToExecute;
    }

    public short getBbsFlagToExecute() {
        return this.bbsFlagToExecute;
    }
}
