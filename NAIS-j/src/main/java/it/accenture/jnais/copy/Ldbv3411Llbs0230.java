package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV3411<br>
 * Variable: LDBV3411 from copybook LDBV3411<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv3411Llbs0230 {

    //==== PROPERTIES ====
    //Original name: LDBV3411-ID-POLI
    private int ldbv3411IdPoli = DefaultValues.INT_VAL;
    //Original name: LDBV3411-ID-ADES
    private int ldbv3411IdAdes = DefaultValues.INT_VAL;
    //Original name: LDBV3411-TP-OGG
    private String ldbv3411TpOgg = DefaultValues.stringVal(Len.LDBV3411_TP_OGG);
    //Original name: LDBV3411-TP-STAT-BUS-1
    private String ldbv3411TpStatBus1 = DefaultValues.stringVal(Len.LDBV3411_TP_STAT_BUS1);
    //Original name: LDBV3411-TP-STAT-BUS-2
    private String ldbv3411TpStatBus2 = DefaultValues.stringVal(Len.LDBV3411_TP_STAT_BUS2);
    //Original name: LDBV3411-GAR-OUTPUT
    private Ldbv3401GarOutput ldbv3411GarOutput = new Ldbv3401GarOutput();
    //Original name: L3411-TP-STAT-BUS
    private String l3411TpStatBus = DefaultValues.stringVal(Len.L3411_TP_STAT_BUS);
    //Original name: L3411-TP-CAUS
    private String l3411TpCaus = DefaultValues.stringVal(Len.L3411_TP_CAUS);

    //==== METHODS ====
    public void setLdbv3411Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV3411];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV3411);
        setLdbv3411Bytes(buffer, 1);
    }

    public String getLdbv3411Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv3411Bytes());
    }

    public byte[] getLdbv3411Bytes() {
        byte[] buffer = new byte[Len.LDBV3411];
        return getLdbv3411Bytes(buffer, 1);
    }

    public void setLdbv3411Bytes(byte[] buffer, int offset) {
        int position = offset;
        setLdbv3411DatiInputBytes(buffer, position);
        position += Len.LDBV3411_DATI_INPUT;
        ldbv3411GarOutput.setLdbv3401GarOutputBytes(buffer, position);
        position += Ldbv3401GarOutput.Len.LDBV3401_GAR_OUTPUT;
        setLdbv3411StbOutputBytes(buffer, position);
    }

    public byte[] getLdbv3411Bytes(byte[] buffer, int offset) {
        int position = offset;
        getLdbv3411DatiInputBytes(buffer, position);
        position += Len.LDBV3411_DATI_INPUT;
        ldbv3411GarOutput.getLdbv3401GarOutputBytes(buffer, position);
        position += Ldbv3401GarOutput.Len.LDBV3401_GAR_OUTPUT;
        getLdbv3411StbOutputBytes(buffer, position);
        return buffer;
    }

    public void setLdbv3411DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv3411IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV3411_ID_POLI, 0);
        position += Len.LDBV3411_ID_POLI;
        ldbv3411IdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV3411_ID_ADES, 0);
        position += Len.LDBV3411_ID_ADES;
        ldbv3411TpOgg = MarshalByte.readString(buffer, position, Len.LDBV3411_TP_OGG);
        position += Len.LDBV3411_TP_OGG;
        ldbv3411TpStatBus1 = MarshalByte.readString(buffer, position, Len.LDBV3411_TP_STAT_BUS1);
        position += Len.LDBV3411_TP_STAT_BUS1;
        ldbv3411TpStatBus2 = MarshalByte.readString(buffer, position, Len.LDBV3411_TP_STAT_BUS2);
    }

    public byte[] getLdbv3411DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv3411IdPoli, Len.Int.LDBV3411_ID_POLI, 0);
        position += Len.LDBV3411_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv3411IdAdes, Len.Int.LDBV3411_ID_ADES, 0);
        position += Len.LDBV3411_ID_ADES;
        MarshalByte.writeString(buffer, position, ldbv3411TpOgg, Len.LDBV3411_TP_OGG);
        position += Len.LDBV3411_TP_OGG;
        MarshalByte.writeString(buffer, position, ldbv3411TpStatBus1, Len.LDBV3411_TP_STAT_BUS1);
        position += Len.LDBV3411_TP_STAT_BUS1;
        MarshalByte.writeString(buffer, position, ldbv3411TpStatBus2, Len.LDBV3411_TP_STAT_BUS2);
        return buffer;
    }

    public void setLdbv3411IdPoli(int ldbv3411IdPoli) {
        this.ldbv3411IdPoli = ldbv3411IdPoli;
    }

    public int getLdbv3411IdPoli() {
        return this.ldbv3411IdPoli;
    }

    public void setLdbv3411IdAdes(int ldbv3411IdAdes) {
        this.ldbv3411IdAdes = ldbv3411IdAdes;
    }

    public int getLdbv3411IdAdes() {
        return this.ldbv3411IdAdes;
    }

    public void setLdbv3411TpOgg(String ldbv3411TpOgg) {
        this.ldbv3411TpOgg = Functions.subString(ldbv3411TpOgg, Len.LDBV3411_TP_OGG);
    }

    public String getLdbv3411TpOgg() {
        return this.ldbv3411TpOgg;
    }

    public void setLdbv3411TpStatBus1(String ldbv3411TpStatBus1) {
        this.ldbv3411TpStatBus1 = Functions.subString(ldbv3411TpStatBus1, Len.LDBV3411_TP_STAT_BUS1);
    }

    public String getLdbv3411TpStatBus1() {
        return this.ldbv3411TpStatBus1;
    }

    public void setLdbv3411TpStatBus2(String ldbv3411TpStatBus2) {
        this.ldbv3411TpStatBus2 = Functions.subString(ldbv3411TpStatBus2, Len.LDBV3411_TP_STAT_BUS2);
    }

    public String getLdbv3411TpStatBus2() {
        return this.ldbv3411TpStatBus2;
    }

    public void setLdbv3411StbOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        l3411TpStatBus = MarshalByte.readString(buffer, position, Len.L3411_TP_STAT_BUS);
        position += Len.L3411_TP_STAT_BUS;
        l3411TpCaus = MarshalByte.readString(buffer, position, Len.L3411_TP_CAUS);
    }

    public byte[] getLdbv3411StbOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, l3411TpStatBus, Len.L3411_TP_STAT_BUS);
        position += Len.L3411_TP_STAT_BUS;
        MarshalByte.writeString(buffer, position, l3411TpCaus, Len.L3411_TP_CAUS);
        return buffer;
    }

    public void setL3411TpStatBus(String l3411TpStatBus) {
        this.l3411TpStatBus = Functions.subString(l3411TpStatBus, Len.L3411_TP_STAT_BUS);
    }

    public String getL3411TpStatBus() {
        return this.l3411TpStatBus;
    }

    public void setL3411TpCaus(String l3411TpCaus) {
        this.l3411TpCaus = Functions.subString(l3411TpCaus, Len.L3411_TP_CAUS);
    }

    public String getL3411TpCaus() {
        return this.l3411TpCaus;
    }

    public Ldbv3401GarOutput getLdbv3411GarOutput() {
        return ldbv3411GarOutput;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV3411_TP_OGG = 2;
        public static final int LDBV3411_TP_STAT_BUS1 = 2;
        public static final int LDBV3411_TP_STAT_BUS2 = 2;
        public static final int L3411_TP_STAT_BUS = 2;
        public static final int L3411_TP_CAUS = 2;
        public static final int LDBV3411_ID_POLI = 5;
        public static final int LDBV3411_ID_ADES = 5;
        public static final int LDBV3411_DATI_INPUT = LDBV3411_ID_POLI + LDBV3411_ID_ADES + LDBV3411_TP_OGG + LDBV3411_TP_STAT_BUS1 + LDBV3411_TP_STAT_BUS2;
        public static final int LDBV3411_STB_OUTPUT = L3411_TP_STAT_BUS + L3411_TP_CAUS;
        public static final int LDBV3411 = LDBV3411_DATI_INPUT + Ldbv3401GarOutput.Len.LDBV3401_GAR_OUTPUT + LDBV3411_STB_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV3411_ID_POLI = 9;
            public static final int LDBV3411_ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
