package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: CNBT-NDED<br>
 * Variable: CNBT-NDED from copybook IDBVCND1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CnbtNded {

    //==== PROPERTIES ====
    //Original name: CND-ID-CNBT-NDED
    private int idCnbtNded = DefaultValues.INT_VAL;
    //Original name: CND-ID-D-FISC-ADES
    private int idDFiscAdes = DefaultValues.INT_VAL;
    //Original name: CND-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: CND-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: CND-ID-MOVI-CHIU
    private int idMoviChiu = DefaultValues.INT_VAL;
    //Original name: CND-DT-INI-EFF
    private int dtIniEff = DefaultValues.INT_VAL;
    //Original name: CND-DT-END-EFF
    private int dtEndEff = DefaultValues.INT_VAL;
    //Original name: CND-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: CND-DT-INI-PER
    private int dtIniPer = DefaultValues.INT_VAL;
    //Original name: CND-DT-END-PER
    private int dtEndPer = DefaultValues.INT_VAL;
    //Original name: CND-IMP-CNBT-NDED
    private AfDecimal impCnbtNded = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: CND-IMP-MAX-CNBT-NDED
    private AfDecimal impMaxCnbtNded = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: CND-DS-RIGA
    private long dsRiga = DefaultValues.LONG_VAL;
    //Original name: CND-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: CND-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: CND-DS-TS-INI-CPTZ
    private long dsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: CND-DS-TS-END-CPTZ
    private long dsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: CND-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: CND-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setCnbtNdedFormatted(String data) {
        byte[] buffer = new byte[Len.CNBT_NDED];
        MarshalByte.writeString(buffer, 1, data, Len.CNBT_NDED);
        setCnbtNdedBytes(buffer, 1);
    }

    public void setCnbtNdedBytes(byte[] buffer, int offset) {
        int position = offset;
        idCnbtNded = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_CNBT_NDED, 0);
        position += Len.ID_CNBT_NDED;
        idDFiscAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_D_FISC_ADES, 0);
        position += Len.ID_D_FISC_ADES;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        idMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        idMoviChiu = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        dtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        dtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        dtIniPer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_PER, 0);
        position += Len.DT_INI_PER;
        dtEndPer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_PER, 0);
        position += Len.DT_END_PER;
        impCnbtNded.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_CNBT_NDED, Len.Fract.IMP_CNBT_NDED));
        position += Len.IMP_CNBT_NDED;
        impMaxCnbtNded.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_MAX_CNBT_NDED, Len.Fract.IMP_MAX_CNBT_NDED));
        position += Len.IMP_MAX_CNBT_NDED;
        dsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        dsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public void setIdCnbtNded(int idCnbtNded) {
        this.idCnbtNded = idCnbtNded;
    }

    public int getIdCnbtNded() {
        return this.idCnbtNded;
    }

    public void setIdDFiscAdes(int idDFiscAdes) {
        this.idDFiscAdes = idDFiscAdes;
    }

    public int getIdDFiscAdes() {
        return this.idDFiscAdes;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setIdMoviChiu(int idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public int getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtIniEff(int dtIniEff) {
        this.dtIniEff = dtIniEff;
    }

    public int getDtIniEff() {
        return this.dtIniEff;
    }

    public void setDtEndEff(int dtEndEff) {
        this.dtEndEff = dtEndEff;
    }

    public int getDtEndEff() {
        return this.dtEndEff;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setDtIniPer(int dtIniPer) {
        this.dtIniPer = dtIniPer;
    }

    public int getDtIniPer() {
        return this.dtIniPer;
    }

    public void setDtEndPer(int dtEndPer) {
        this.dtEndPer = dtEndPer;
    }

    public int getDtEndPer() {
        return this.dtEndPer;
    }

    public void setImpCnbtNded(AfDecimal impCnbtNded) {
        this.impCnbtNded.assign(impCnbtNded);
    }

    public AfDecimal getImpCnbtNded() {
        return this.impCnbtNded.copy();
    }

    public void setImpMaxCnbtNded(AfDecimal impMaxCnbtNded) {
        this.impMaxCnbtNded.assign(impMaxCnbtNded);
    }

    public AfDecimal getImpMaxCnbtNded() {
        return this.impMaxCnbtNded.copy();
    }

    public void setDsRiga(long dsRiga) {
        this.dsRiga = dsRiga;
    }

    public long getDsRiga() {
        return this.dsRiga;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dsTsIniCptz = dsTsIniCptz;
    }

    public long getDsTsIniCptz() {
        return this.dsTsIniCptz;
    }

    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dsTsEndCptz = dsTsEndCptz;
    }

    public long getDsTsEndCptz() {
        return this.dsTsEndCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DS_UTENTE = 20;
        public static final int ID_CNBT_NDED = 5;
        public static final int ID_D_FISC_ADES = 5;
        public static final int ID_ADES = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int DT_INI_PER = 5;
        public static final int DT_END_PER = 5;
        public static final int IMP_CNBT_NDED = 8;
        public static final int IMP_MAX_CNBT_NDED = 8;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_STATO_ELAB = 1;
        public static final int CNBT_NDED = ID_CNBT_NDED + ID_D_FISC_ADES + ID_ADES + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + DT_INI_PER + DT_END_PER + IMP_CNBT_NDED + IMP_MAX_CNBT_NDED + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_CNBT_NDED = 9;
            public static final int ID_D_FISC_ADES = 9;
            public static final int ID_ADES = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_INI_PER = 8;
            public static final int DT_END_PER = 8;
            public static final int IMP_CNBT_NDED = 12;
            public static final int IMP_MAX_CNBT_NDED = 12;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMP_CNBT_NDED = 3;
            public static final int IMP_MAX_CNBT_NDED = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
