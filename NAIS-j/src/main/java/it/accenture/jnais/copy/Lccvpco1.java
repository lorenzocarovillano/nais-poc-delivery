package it.accenture.jnais.copy;

import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVPCO1<br>
 * Copybook: LCCVPCO1 from copybook LCCVPCO1<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvpco1 {

    //==== PROPERTIES ====
    /**Original name: WPCO-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA PARAM_COMP
	 *    ALIAS PCO
	 *    ULTIMO AGG. 13 NOV 2018
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WPCO-DATI
    private WpcoDati dati = new WpcoDati();

    //==== METHODS ====
    public WpcoDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }
}
