package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: BUS-SERV-ESEG-REPORT<br>
 * Variable: BUS-SERV-ESEG-REPORT from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class BusServEsegReport {

    //==== PROPERTIES ====
    //Original name: FILLER-BUS-SERV-ESEG-REPORT
    private String flr1 = "BUSINESS SERVICES ESEGUITI";
    //Original name: FILLER-BUS-SERV-ESEG-REPORT-1
    private String flr2 = " :";
    //Original name: REP-BUS-SERV-ESEGUITI
    private String repBusServEseguiti = "000000000";

    //==== METHODS ====
    public String getBusServEsegReportFormatted() {
        return MarshalByteExt.bufferToStr(getBusServEsegReportBytes());
    }

    public byte[] getBusServEsegReportBytes() {
        byte[] buffer = new byte[Len.BUS_SERV_ESEG_REPORT];
        return getBusServEsegReportBytes(buffer, 1);
    }

    public byte[] getBusServEsegReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, repBusServEseguiti, Len.REP_BUS_SERV_ESEGUITI);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public void setRepBusServEseguiti(long repBusServEseguiti) {
        this.repBusServEseguiti = PicFormatter.display("Z(8)9").format(repBusServEseguiti).toString();
    }

    public long getRepBusServEseguiti() {
        return PicParser.display("Z(8)9").parseLong(this.repBusServEseguiti);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int REP_BUS_SERV_ESEGUITI = 9;
        public static final int FLR1 = 50;
        public static final int FLR2 = 4;
        public static final int BUS_SERV_ESEG_REPORT = REP_BUS_SERV_ESEGUITI + FLR1 + FLR2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
