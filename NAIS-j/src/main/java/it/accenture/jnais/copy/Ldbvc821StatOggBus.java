package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVC821-STAT-OGG-BUS<br>
 * Variable: LDBVC821-STAT-OGG-BUS from copybook LDBVC821<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbvc821StatOggBus {

    //==== PROPERTIES ====
    //Original name: LC821-STB-ID-STAT-OGG-BUS
    private int idStatOggBus = DefaultValues.INT_VAL;
    //Original name: LC821-STB-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LC821-STB-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LC821-STB-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: LC821-STB-ID-MOVI-CHIU
    private int idMoviChiu = DefaultValues.INT_VAL;
    //Original name: LC821-STB-DT-INI-EFF
    private int dtIniEff = DefaultValues.INT_VAL;
    //Original name: LC821-STB-DT-END-EFF
    private int dtEndEff = DefaultValues.INT_VAL;
    //Original name: LC821-STB-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: LC821-STB-TP-STAT-BUS
    private String tpStatBus = DefaultValues.stringVal(Len.TP_STAT_BUS);
    //Original name: LC821-STB-TP-CAUS
    private String tpCaus = DefaultValues.stringVal(Len.TP_CAUS);
    //Original name: LC821-STB-DS-RIGA
    private long dsRiga = DefaultValues.LONG_VAL;
    //Original name: LC821-STB-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: LC821-STB-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: LC821-STB-DS-TS-INI-CPTZ
    private long dsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: LC821-STB-DS-TS-END-CPTZ
    private long dsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: LC821-STB-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: LC821-STB-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setLdbvc821StatOggBusBytes(byte[] buffer) {
        setLdbvc821StatOggBusBytes(buffer, 1);
    }

    public byte[] getLdbvc821StatOggBusBytes() {
        byte[] buffer = new byte[Len.LDBVC821_STAT_OGG_BUS];
        return getLdbvc821StatOggBusBytes(buffer, 1);
    }

    public void setLdbvc821StatOggBusBytes(byte[] buffer, int offset) {
        int position = offset;
        idStatOggBus = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_STAT_OGG_BUS, 0);
        position += Len.ID_STAT_OGG_BUS;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        idMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        idMoviChiu = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        dtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        dtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        tpStatBus = MarshalByte.readString(buffer, position, Len.TP_STAT_BUS);
        position += Len.TP_STAT_BUS;
        tpCaus = MarshalByte.readString(buffer, position, Len.TP_CAUS);
        position += Len.TP_CAUS;
        dsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        dsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getLdbvc821StatOggBusBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idStatOggBus, Len.Int.ID_STAT_OGG_BUS, 0);
        position += Len.ID_STAT_OGG_BUS;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviCrz, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviChiu, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniEff, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtEndEff, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, tpStatBus, Len.TP_STAT_BUS);
        position += Len.TP_STAT_BUS;
        MarshalByte.writeString(buffer, position, tpCaus, Len.TP_CAUS);
        position += Len.TP_CAUS;
        MarshalByte.writeLongAsPacked(buffer, position, dsRiga, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        return buffer;
    }

    public void setIdStatOggBus(int idStatOggBus) {
        this.idStatOggBus = idStatOggBus;
    }

    public int getIdStatOggBus() {
        return this.idStatOggBus;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setIdMoviChiu(int idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public int getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtIniEff(int dtIniEff) {
        this.dtIniEff = dtIniEff;
    }

    public int getDtIniEff() {
        return this.dtIniEff;
    }

    public void setDtEndEff(int dtEndEff) {
        this.dtEndEff = dtEndEff;
    }

    public int getDtEndEff() {
        return this.dtEndEff;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setTpStatBus(String tpStatBus) {
        this.tpStatBus = Functions.subString(tpStatBus, Len.TP_STAT_BUS);
    }

    public String getTpStatBus() {
        return this.tpStatBus;
    }

    public void setTpCaus(String tpCaus) {
        this.tpCaus = Functions.subString(tpCaus, Len.TP_CAUS);
    }

    public String getTpCaus() {
        return this.tpCaus;
    }

    public void setDsRiga(long dsRiga) {
        this.dsRiga = dsRiga;
    }

    public long getDsRiga() {
        return this.dsRiga;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dsTsIniCptz = dsTsIniCptz;
    }

    public long getDsTsIniCptz() {
        return this.dsTsIniCptz;
    }

    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dsTsEndCptz = dsTsEndCptz;
    }

    public long getDsTsEndCptz() {
        return this.dsTsEndCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_STAT_OGG_BUS = 5;
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int TP_STAT_BUS = 2;
        public static final int TP_CAUS = 2;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int LDBVC821_STAT_OGG_BUS = ID_STAT_OGG_BUS + ID_OGG + TP_OGG + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + TP_STAT_BUS + TP_CAUS + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_STAT_OGG_BUS = 9;
            public static final int ID_OGG = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
