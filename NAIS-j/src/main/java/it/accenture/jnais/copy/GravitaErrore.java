package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.commons.data.to.IGravitaErrore;

/**Original name: GRAVITA-ERRORE<br>
 * Variable: GRAVITA-ERRORE from copybook IDBVGER1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class GravitaErrore implements IGravitaErrore {

    //==== PROPERTIES ====
    //Original name: GER-ID-GRAVITA-ERRORE
    private int idGravitaErrore = DefaultValues.INT_VAL;
    //Original name: GER-COD-COMPAGNIA-ANIA
    private int codCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: GER-LIVELLO-GRAVITA
    private short livelloGravita = DefaultValues.SHORT_VAL;
    //Original name: GER-TIPO-TRATT-FE
    private char tipoTrattFe = DefaultValues.CHAR_VAL;
    //Original name: GER-COD-ERRORE
    private int codErrore = DefaultValues.BIN_INT_VAL;

    //==== METHODS ====
    @Override
    public void setIdGravitaErrore(int idGravitaErrore) {
        this.idGravitaErrore = idGravitaErrore;
    }

    @Override
    public int getIdGravitaErrore() {
        return this.idGravitaErrore;
    }

    public void setCodCompagniaAnia(int codCompagniaAnia) {
        this.codCompagniaAnia = codCompagniaAnia;
    }

    public int getCodCompagniaAnia() {
        return this.codCompagniaAnia;
    }

    @Override
    public void setLivelloGravita(short livelloGravita) {
        this.livelloGravita = livelloGravita;
    }

    @Override
    public short getLivelloGravita() {
        return this.livelloGravita;
    }

    @Override
    public void setTipoTrattFe(char tipoTrattFe) {
        this.tipoTrattFe = tipoTrattFe;
    }

    @Override
    public char getTipoTrattFe() {
        return this.tipoTrattFe;
    }

    public void setCodErrore(int codErrore) {
        this.codErrore = codErrore;
    }

    public int getCodErrore() {
        return this.codErrore;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_CONV_MAIN_BTC = 12;
        public static final int COD_PAGINA_SERV_BE = 12;
        public static final int KEY_FILTRO2 = 20;
        public static final int KEY_FILTRO1 = 20;
        public static final int KEY_FILTRO3 = 20;
        public static final int KEY_FILTRO4 = 20;
        public static final int KEY_FILTRO5 = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
