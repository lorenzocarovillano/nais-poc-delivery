package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-D-COLL<br>
 * Variable: IND-D-COLL from copybook IDBVDCO2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndDColl {

    //==== PROPERTIES ====
    //Original name: IND-DCO-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-IMP-ARROT-PRE
    private short impArrotPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-PC-SCON
    private short pcScon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-FL-ADES-SING
    private short flAdesSing = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-TP-IMP
    private short tpImp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-FL-RICL-PRE-DA-CPT
    private short flRiclPreDaCpt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-TP-ADES
    private short tpAdes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-DT-ULT-RINN-TAC
    private short dtUltRinnTac = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-IMP-SCON
    private short impScon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-FRAZ-DFLT
    private short frazDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-ETA-SCAD-MASC-DFLT
    private short etaScadMascDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-ETA-SCAD-FEMM-DFLT
    private short etaScadFemmDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-TP-DFLT-DUR
    private short tpDfltDur = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-DUR-AA-ADES-DFLT
    private short durAaAdesDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-DUR-MM-ADES-DFLT
    private short durMmAdesDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-DUR-GG-ADES-DFLT
    private short durGgAdesDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-DT-SCAD-ADES-DFLT
    private short dtScadAdesDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-COD-FND-DFLT
    private short codFndDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-TP-DUR
    private short tpDur = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-TP-CALC-DUR
    private short tpCalcDur = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-FL-NO-ADERENTI
    private short flNoAderenti = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-FL-DISTINTA-CNBTVA
    private short flDistintaCnbtva = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-FL-CNBT-AUTES
    private short flCnbtAutes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-FL-QTZ-POST-EMIS
    private short flQtzPostEmis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DCO-FL-COMNZ-FND-IS
    private short flComnzFndIs = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setImpArrotPre(short impArrotPre) {
        this.impArrotPre = impArrotPre;
    }

    public short getImpArrotPre() {
        return this.impArrotPre;
    }

    public void setPcScon(short pcScon) {
        this.pcScon = pcScon;
    }

    public short getPcScon() {
        return this.pcScon;
    }

    public void setFlAdesSing(short flAdesSing) {
        this.flAdesSing = flAdesSing;
    }

    public short getFlAdesSing() {
        return this.flAdesSing;
    }

    public void setTpImp(short tpImp) {
        this.tpImp = tpImp;
    }

    public short getTpImp() {
        return this.tpImp;
    }

    public void setFlRiclPreDaCpt(short flRiclPreDaCpt) {
        this.flRiclPreDaCpt = flRiclPreDaCpt;
    }

    public short getFlRiclPreDaCpt() {
        return this.flRiclPreDaCpt;
    }

    public void setTpAdes(short tpAdes) {
        this.tpAdes = tpAdes;
    }

    public short getTpAdes() {
        return this.tpAdes;
    }

    public void setDtUltRinnTac(short dtUltRinnTac) {
        this.dtUltRinnTac = dtUltRinnTac;
    }

    public short getDtUltRinnTac() {
        return this.dtUltRinnTac;
    }

    public void setImpScon(short impScon) {
        this.impScon = impScon;
    }

    public short getImpScon() {
        return this.impScon;
    }

    public void setFrazDflt(short frazDflt) {
        this.frazDflt = frazDflt;
    }

    public short getFrazDflt() {
        return this.frazDflt;
    }

    public void setEtaScadMascDflt(short etaScadMascDflt) {
        this.etaScadMascDflt = etaScadMascDflt;
    }

    public short getEtaScadMascDflt() {
        return this.etaScadMascDflt;
    }

    public void setEtaScadFemmDflt(short etaScadFemmDflt) {
        this.etaScadFemmDflt = etaScadFemmDflt;
    }

    public short getEtaScadFemmDflt() {
        return this.etaScadFemmDflt;
    }

    public void setTpDfltDur(short tpDfltDur) {
        this.tpDfltDur = tpDfltDur;
    }

    public short getTpDfltDur() {
        return this.tpDfltDur;
    }

    public void setDurAaAdesDflt(short durAaAdesDflt) {
        this.durAaAdesDflt = durAaAdesDflt;
    }

    public short getDurAaAdesDflt() {
        return this.durAaAdesDflt;
    }

    public void setDurMmAdesDflt(short durMmAdesDflt) {
        this.durMmAdesDflt = durMmAdesDflt;
    }

    public short getDurMmAdesDflt() {
        return this.durMmAdesDflt;
    }

    public void setDurGgAdesDflt(short durGgAdesDflt) {
        this.durGgAdesDflt = durGgAdesDflt;
    }

    public short getDurGgAdesDflt() {
        return this.durGgAdesDflt;
    }

    public void setDtScadAdesDflt(short dtScadAdesDflt) {
        this.dtScadAdesDflt = dtScadAdesDflt;
    }

    public short getDtScadAdesDflt() {
        return this.dtScadAdesDflt;
    }

    public void setCodFndDflt(short codFndDflt) {
        this.codFndDflt = codFndDflt;
    }

    public short getCodFndDflt() {
        return this.codFndDflt;
    }

    public void setTpDur(short tpDur) {
        this.tpDur = tpDur;
    }

    public short getTpDur() {
        return this.tpDur;
    }

    public void setTpCalcDur(short tpCalcDur) {
        this.tpCalcDur = tpCalcDur;
    }

    public short getTpCalcDur() {
        return this.tpCalcDur;
    }

    public void setFlNoAderenti(short flNoAderenti) {
        this.flNoAderenti = flNoAderenti;
    }

    public short getFlNoAderenti() {
        return this.flNoAderenti;
    }

    public void setFlDistintaCnbtva(short flDistintaCnbtva) {
        this.flDistintaCnbtva = flDistintaCnbtva;
    }

    public short getFlDistintaCnbtva() {
        return this.flDistintaCnbtva;
    }

    public void setFlCnbtAutes(short flCnbtAutes) {
        this.flCnbtAutes = flCnbtAutes;
    }

    public short getFlCnbtAutes() {
        return this.flCnbtAutes;
    }

    public void setFlQtzPostEmis(short flQtzPostEmis) {
        this.flQtzPostEmis = flQtzPostEmis;
    }

    public short getFlQtzPostEmis() {
        return this.flQtzPostEmis;
    }

    public void setFlComnzFndIs(short flComnzFndIs) {
        this.flComnzFndIs = flComnzFndIs;
    }

    public short getFlComnzFndIs() {
        return this.flComnzFndIs;
    }
}
