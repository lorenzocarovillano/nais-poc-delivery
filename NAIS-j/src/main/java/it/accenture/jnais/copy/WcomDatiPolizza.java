package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.WcomLivelloGenTit;

/**Original name: WCOM-DATI-POLIZZA<br>
 * Variable: WCOM-DATI-POLIZZA from copybook LCCC0261<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WcomDatiPolizza {

    //==== PROPERTIES ====
    //Original name: WCOM-ID-POLIZZA
    private int idPolizza = DefaultValues.INT_VAL;
    //Original name: WCOM-POL-IB-OGG
    private String polIbOgg = DefaultValues.stringVal(Len.POL_IB_OGG);
    //Original name: WCOM-LIVELLO-GEN-TIT
    private WcomLivelloGenTit livelloGenTit = new WcomLivelloGenTit();

    //==== METHODS ====
    public void setDatiPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        idPolizza = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLIZZA, 0);
        position += Len.ID_POLIZZA;
        polIbOgg = MarshalByte.readString(buffer, position, Len.POL_IB_OGG);
        position += Len.POL_IB_OGG;
        livelloGenTit.setLivelloGenTit(MarshalByte.readString(buffer, position, WcomLivelloGenTit.Len.LIVELLO_GEN_TIT));
    }

    public byte[] getDatiPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPolizza, Len.Int.ID_POLIZZA, 0);
        position += Len.ID_POLIZZA;
        MarshalByte.writeString(buffer, position, polIbOgg, Len.POL_IB_OGG);
        position += Len.POL_IB_OGG;
        MarshalByte.writeString(buffer, position, livelloGenTit.getLivelloGenTit(), WcomLivelloGenTit.Len.LIVELLO_GEN_TIT);
        return buffer;
    }

    public void setIdPolizza(int idPolizza) {
        this.idPolizza = idPolizza;
    }

    public int getIdPolizza() {
        return this.idPolizza;
    }

    public void setPolIbOgg(String polIbOgg) {
        this.polIbOgg = Functions.subString(polIbOgg, Len.POL_IB_OGG);
    }

    public String getPolIbOgg() {
        return this.polIbOgg;
    }

    public WcomLivelloGenTit getLivelloGenTit() {
        return livelloGenTit;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POLIZZA = 5;
        public static final int POL_IB_OGG = 40;
        public static final int DATI_POLIZZA = ID_POLIZZA + POL_IB_OGG + WcomLivelloGenTit.Len.LIVELLO_GEN_TIT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLIZZA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
