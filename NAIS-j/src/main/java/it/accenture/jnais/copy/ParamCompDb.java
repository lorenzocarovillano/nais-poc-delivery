package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: PARAM-COMP-DB<br>
 * Variable: PARAM-COMP-DB from copybook IDBVPCO3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ParamCompDb {

    //==== PROPERTIES ====
    //Original name: PCO-DT-CONT-DB
    private String dtContDb = DefaultValues.stringVal(Len.DT_CONT_DB);
    //Original name: PCO-DT-ULT-RIVAL-IN-DB
    private String dtUltRivalInDb = DefaultValues.stringVal(Len.DT_ULT_RIVAL_IN_DB);
    //Original name: PCO-DT-ULT-QTZO-IN-DB
    private String dtUltQtzoInDb = DefaultValues.stringVal(Len.DT_ULT_QTZO_IN_DB);
    //Original name: PCO-DT-ULT-RICL-RIASS-DB
    private String dtUltRiclRiassDb = DefaultValues.stringVal(Len.DT_ULT_RICL_RIASS_DB);
    //Original name: PCO-DT-ULT-TABUL-RIASS-DB
    private String dtUltTabulRiassDb = DefaultValues.stringVal(Len.DT_ULT_TABUL_RIASS_DB);
    //Original name: PCO-DT-ULT-BOLL-EMES-DB
    private String dtUltBollEmesDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_EMES_DB);
    //Original name: PCO-DT-ULT-BOLL-STOR-DB
    private String dtUltBollStorDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_STOR_DB);
    //Original name: PCO-DT-ULT-BOLL-LIQ-DB
    private String dtUltBollLiqDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_LIQ_DB);
    //Original name: PCO-DT-ULT-BOLL-RIAT-DB
    private String dtUltBollRiatDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_RIAT_DB);
    //Original name: PCO-DT-ULTELRISCPAR-PR-DB
    private String dtUltelriscparPrDb = DefaultValues.stringVal(Len.DT_ULTELRISCPAR_PR_DB);
    //Original name: PCO-DT-ULTC-IS-IN-DB
    private String dtUltcIsInDb = DefaultValues.stringVal(Len.DT_ULTC_IS_IN_DB);
    //Original name: PCO-DT-ULT-RICL-PRE-DB
    private String dtUltRiclPreDb = DefaultValues.stringVal(Len.DT_ULT_RICL_PRE_DB);
    //Original name: PCO-DT-ULTC-MARSOL-DB
    private String dtUltcMarsolDb = DefaultValues.stringVal(Len.DT_ULTC_MARSOL_DB);
    //Original name: PCO-DT-ULTC-RB-IN-DB
    private String dtUltcRbInDb = DefaultValues.stringVal(Len.DT_ULTC_RB_IN_DB);
    //Original name: PCO-DT-ULTGZ-TRCH-E-IN-DB
    private String dtUltgzTrchEInDb = DefaultValues.stringVal(Len.DT_ULTGZ_TRCH_E_IN_DB);
    //Original name: PCO-DT-ULT-BOLL-SNDEN-DB
    private String dtUltBollSndenDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_SNDEN_DB);
    //Original name: PCO-DT-ULT-BOLL-SNDNLQ-DB
    private String dtUltBollSndnlqDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_SNDNLQ_DB);
    //Original name: PCO-DT-ULTSC-ELAB-IN-DB
    private String dtUltscElabInDb = DefaultValues.stringVal(Len.DT_ULTSC_ELAB_IN_DB);
    //Original name: PCO-DT-ULTSC-OPZ-IN-DB
    private String dtUltscOpzInDb = DefaultValues.stringVal(Len.DT_ULTSC_OPZ_IN_DB);
    //Original name: PCO-DT-ULTC-BNSRIC-IN-DB
    private String dtUltcBnsricInDb = DefaultValues.stringVal(Len.DT_ULTC_BNSRIC_IN_DB);
    //Original name: PCO-DT-ULTC-BNSFDT-IN-DB
    private String dtUltcBnsfdtInDb = DefaultValues.stringVal(Len.DT_ULTC_BNSFDT_IN_DB);
    //Original name: PCO-DT-ULT-RINN-GARAC-DB
    private String dtUltRinnGaracDb = DefaultValues.stringVal(Len.DT_ULT_RINN_GARAC_DB);
    //Original name: PCO-DT-ULTGZ-CED-DB
    private String dtUltgzCedDb = DefaultValues.stringVal(Len.DT_ULTGZ_CED_DB);
    //Original name: PCO-DT-ULT-ELAB-PRLCOS-DB
    private String dtUltElabPrlcosDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_PRLCOS_DB);
    //Original name: PCO-DT-ULT-RINN-COLL-DB
    private String dtUltRinnCollDb = DefaultValues.stringVal(Len.DT_ULT_RINN_COLL_DB);
    //Original name: PCO-DT-ULT-RIVAL-CL-DB
    private String dtUltRivalClDb = DefaultValues.stringVal(Len.DT_ULT_RIVAL_CL_DB);
    //Original name: PCO-DT-ULT-QTZO-CL-DB
    private String dtUltQtzoClDb = DefaultValues.stringVal(Len.DT_ULT_QTZO_CL_DB);
    //Original name: PCO-DT-ULTC-BNSRIC-CL-DB
    private String dtUltcBnsricClDb = DefaultValues.stringVal(Len.DT_ULTC_BNSRIC_CL_DB);
    //Original name: PCO-DT-ULTC-BNSFDT-CL-DB
    private String dtUltcBnsfdtClDb = DefaultValues.stringVal(Len.DT_ULTC_BNSFDT_CL_DB);
    //Original name: PCO-DT-ULTC-IS-CL-DB
    private String dtUltcIsClDb = DefaultValues.stringVal(Len.DT_ULTC_IS_CL_DB);
    //Original name: PCO-DT-ULTC-RB-CL-DB
    private String dtUltcRbClDb = DefaultValues.stringVal(Len.DT_ULTC_RB_CL_DB);
    //Original name: PCO-DT-ULTGZ-TRCH-E-CL-DB
    private String dtUltgzTrchEClDb = DefaultValues.stringVal(Len.DT_ULTGZ_TRCH_E_CL_DB);
    //Original name: PCO-DT-ULTSC-ELAB-CL-DB
    private String dtUltscElabClDb = DefaultValues.stringVal(Len.DT_ULTSC_ELAB_CL_DB);
    //Original name: PCO-DT-ULTSC-OPZ-CL-DB
    private String dtUltscOpzClDb = DefaultValues.stringVal(Len.DT_ULTSC_OPZ_CL_DB);
    //Original name: PCO-STST-X-REGIONE-DB
    private String ststXRegioneDb = DefaultValues.stringVal(Len.STST_X_REGIONE_DB);
    //Original name: PCO-DT-ULTGZ-CED-COLL-DB
    private String dtUltgzCedCollDb = DefaultValues.stringVal(Len.DT_ULTGZ_CED_COLL_DB);
    //Original name: PCO-DT-ULT-EC-RIV-COLL-DB
    private String dtUltEcRivCollDb = DefaultValues.stringVal(Len.DT_ULT_EC_RIV_COLL_DB);
    //Original name: PCO-DT-ULT-EC-RIV-IND-DB
    private String dtUltEcRivIndDb = DefaultValues.stringVal(Len.DT_ULT_EC_RIV_IND_DB);
    //Original name: PCO-DT-ULT-EC-IL-COLL-DB
    private String dtUltEcIlCollDb = DefaultValues.stringVal(Len.DT_ULT_EC_IL_COLL_DB);
    //Original name: PCO-DT-ULT-EC-IL-IND-DB
    private String dtUltEcIlIndDb = DefaultValues.stringVal(Len.DT_ULT_EC_IL_IND_DB);
    //Original name: PCO-DT-ULT-EC-UL-COLL-DB
    private String dtUltEcUlCollDb = DefaultValues.stringVal(Len.DT_ULT_EC_UL_COLL_DB);
    //Original name: PCO-DT-ULT-EC-UL-IND-DB
    private String dtUltEcUlIndDb = DefaultValues.stringVal(Len.DT_ULT_EC_UL_IND_DB);
    //Original name: PCO-DT-ULT-BOLL-PERF-C-DB
    private String dtUltBollPerfCDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_PERF_C_DB);
    //Original name: PCO-DT-ULT-BOLL-RSP-IN-DB
    private String dtUltBollRspInDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_RSP_IN_DB);
    //Original name: PCO-DT-ULT-BOLL-RSP-CL-DB
    private String dtUltBollRspClDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_RSP_CL_DB);
    //Original name: PCO-DT-ULT-BOLL-EMES-I-DB
    private String dtUltBollEmesIDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_EMES_I_DB);
    //Original name: PCO-DT-ULT-BOLL-STOR-I-DB
    private String dtUltBollStorIDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_STOR_I_DB);
    //Original name: PCO-DT-ULT-BOLL-RIAT-I-DB
    private String dtUltBollRiatIDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_RIAT_I_DB);
    //Original name: PCO-DT-ULT-BOLL-SD-I-DB
    private String dtUltBollSdIDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_SD_I_DB);
    //Original name: PCO-DT-ULT-BOLL-SDNL-I-DB
    private String dtUltBollSdnlIDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_SDNL_I_DB);
    //Original name: PCO-DT-ULT-BOLL-PERF-I-DB
    private String dtUltBollPerfIDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_PERF_I_DB);
    //Original name: PCO-DT-RICL-RIRIAS-COM-DB
    private String dtRiclRiriasComDb = DefaultValues.stringVal(Len.DT_RICL_RIRIAS_COM_DB);
    //Original name: PCO-DT-ULT-ELAB-AT92-C-DB
    private String dtUltElabAt92CDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_AT92_C_DB);
    //Original name: PCO-DT-ULT-ELAB-AT92-I-DB
    private String dtUltElabAt92IDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_AT92_I_DB);
    //Original name: PCO-DT-ULT-ELAB-AT93-C-DB
    private String dtUltElabAt93CDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_AT93_C_DB);
    //Original name: PCO-DT-ULT-ELAB-AT93-I-DB
    private String dtUltElabAt93IDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_AT93_I_DB);
    //Original name: PCO-DT-ULT-ELAB-SPE-IN-DB
    private String dtUltElabSpeInDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_SPE_IN_DB);
    //Original name: PCO-DT-ULT-ELAB-PR-CON-DB
    private String dtUltElabPrConDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_PR_CON_DB);
    //Original name: PCO-DT-ULT-BOLL-RP-CL-DB
    private String dtUltBollRpClDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_RP_CL_DB);
    //Original name: PCO-DT-ULT-BOLL-RP-IN-DB
    private String dtUltBollRpInDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_RP_IN_DB);
    //Original name: PCO-DT-ULT-BOLL-PRE-I-DB
    private String dtUltBollPreIDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_PRE_I_DB);
    //Original name: PCO-DT-ULT-BOLL-PRE-C-DB
    private String dtUltBollPreCDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_PRE_C_DB);
    //Original name: PCO-DT-ULTC-PILDI-MM-C-DB
    private String dtUltcPildiMmCDb = DefaultValues.stringVal(Len.DT_ULTC_PILDI_MM_C_DB);
    //Original name: PCO-DT-ULTC-PILDI-AA-C-DB
    private String dtUltcPildiAaCDb = DefaultValues.stringVal(Len.DT_ULTC_PILDI_AA_C_DB);
    //Original name: PCO-DT-ULTC-PILDI-MM-I-DB
    private String dtUltcPildiMmIDb = DefaultValues.stringVal(Len.DT_ULTC_PILDI_MM_I_DB);
    //Original name: PCO-DT-ULTC-PILDI-TR-I-DB
    private String dtUltcPildiTrIDb = DefaultValues.stringVal(Len.DT_ULTC_PILDI_TR_I_DB);
    //Original name: PCO-DT-ULTC-PILDI-AA-I-DB
    private String dtUltcPildiAaIDb = DefaultValues.stringVal(Len.DT_ULTC_PILDI_AA_I_DB);
    //Original name: PCO-DT-ULT-BOLL-QUIE-C-DB
    private String dtUltBollQuieCDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_QUIE_C_DB);
    //Original name: PCO-DT-ULT-BOLL-QUIE-I-DB
    private String dtUltBollQuieIDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_QUIE_I_DB);
    //Original name: PCO-DT-ULT-BOLL-COTR-I-DB
    private String dtUltBollCotrIDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_COTR_I_DB);
    //Original name: PCO-DT-ULT-BOLL-COTR-C-DB
    private String dtUltBollCotrCDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_COTR_C_DB);
    //Original name: PCO-DT-ULT-BOLL-CORI-C-DB
    private String dtUltBollCoriCDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_CORI_C_DB);
    //Original name: PCO-DT-ULT-BOLL-CORI-I-DB
    private String dtUltBollCoriIDb = DefaultValues.stringVal(Len.DT_ULT_BOLL_CORI_I_DB);
    //Original name: PCO-DT-ULT-AGG-EROG-RE-DB
    private String dtUltAggErogReDb = DefaultValues.stringVal(Len.DT_ULT_AGG_EROG_RE_DB);
    //Original name: PCO-DT-ULT-ESTRAZ-FUG-DB
    private String dtUltEstrazFugDb = DefaultValues.stringVal(Len.DT_ULT_ESTRAZ_FUG_DB);
    //Original name: PCO-DT-ULT-ELAB-PR-AUT-DB
    private String dtUltElabPrAutDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_PR_AUT_DB);
    //Original name: PCO-DT-ULT-ELAB-COMMEF-DB
    private String dtUltElabCommefDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_COMMEF_DB);
    //Original name: PCO-DT-ULT-ELAB-LIQMEF-DB
    private String dtUltElabLiqmefDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_LIQMEF_DB);
    //Original name: PCO-DT-RIAT-RIASS-RSH-DB
    private String dtRiatRiassRshDb = DefaultValues.stringVal(Len.DT_RIAT_RIASS_RSH_DB);
    //Original name: PCO-DT-RIAT-RIASS-COMM-DB
    private String dtRiatRiassCommDb = DefaultValues.stringVal(Len.DT_RIAT_RIASS_COMM_DB);
    //Original name: PCO-DT-ULT-RINN-TAC-DB
    private String dtUltRinnTacDb = DefaultValues.stringVal(Len.DT_ULT_RINN_TAC_DB);
    //Original name: PCO-DT-ULT-EC-TCM-IND-DB
    private String dtUltEcTcmIndDb = DefaultValues.stringVal(Len.DT_ULT_EC_TCM_IND_DB);
    //Original name: PCO-DT-ULT-EC-TCM-COLL-DB
    private String dtUltEcTcmCollDb = DefaultValues.stringVal(Len.DT_ULT_EC_TCM_COLL_DB);
    //Original name: PCO-DT-ULT-EC-MRM-IND-DB
    private String dtUltEcMrmIndDb = DefaultValues.stringVal(Len.DT_ULT_EC_MRM_IND_DB);
    //Original name: PCO-DT-ULT-EC-MRM-COLL-DB
    private String dtUltEcMrmCollDb = DefaultValues.stringVal(Len.DT_ULT_EC_MRM_COLL_DB);
    //Original name: PCO-DT-ULT-ELAB-REDPRO-DB
    private String dtUltElabRedproDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_REDPRO_DB);
    //Original name: PCO-DT-ULT-ELAB-TAKE-P-DB
    private String dtUltElabTakePDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_TAKE_P_DB);
    //Original name: PCO-DT-ULT-ELAB-PASPAS-DB
    private String dtUltElabPaspasDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_PASPAS_DB);
    //Original name: PCO-DT-ULT-ESTR-DEC-CO-DB
    private String dtUltEstrDecCoDb = DefaultValues.stringVal(Len.DT_ULT_ESTR_DEC_CO_DB);
    //Original name: PCO-DT-ULT-ELAB-COS-AT-DB
    private String dtUltElabCosAtDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_COS_AT_DB);
    //Original name: PCO-DT-ULT-ELAB-COS-ST-DB
    private String dtUltElabCosStDb = DefaultValues.stringVal(Len.DT_ULT_ELAB_COS_ST_DB);
    //Original name: PCO-DT-ESTR-ASS-MIN70A-DB
    private String dtEstrAssMin70aDb = DefaultValues.stringVal(Len.DT_ESTR_ASS_MIN70A_DB);
    //Original name: PCO-DT-ESTR-ASS-MAG70A-DB
    private String dtEstrAssMag70aDb = DefaultValues.stringVal(Len.DT_ESTR_ASS_MAG70A_DB);

    //==== METHODS ====
    public void setDtContDb(String dtContDb) {
        this.dtContDb = Functions.subString(dtContDb, Len.DT_CONT_DB);
    }

    public String getDtContDb() {
        return this.dtContDb;
    }

    public void setDtUltRivalInDb(String dtUltRivalInDb) {
        this.dtUltRivalInDb = Functions.subString(dtUltRivalInDb, Len.DT_ULT_RIVAL_IN_DB);
    }

    public String getDtUltRivalInDb() {
        return this.dtUltRivalInDb;
    }

    public void setDtUltQtzoInDb(String dtUltQtzoInDb) {
        this.dtUltQtzoInDb = Functions.subString(dtUltQtzoInDb, Len.DT_ULT_QTZO_IN_DB);
    }

    public String getDtUltQtzoInDb() {
        return this.dtUltQtzoInDb;
    }

    public void setDtUltRiclRiassDb(String dtUltRiclRiassDb) {
        this.dtUltRiclRiassDb = Functions.subString(dtUltRiclRiassDb, Len.DT_ULT_RICL_RIASS_DB);
    }

    public String getDtUltRiclRiassDb() {
        return this.dtUltRiclRiassDb;
    }

    public void setDtUltTabulRiassDb(String dtUltTabulRiassDb) {
        this.dtUltTabulRiassDb = Functions.subString(dtUltTabulRiassDb, Len.DT_ULT_TABUL_RIASS_DB);
    }

    public String getDtUltTabulRiassDb() {
        return this.dtUltTabulRiassDb;
    }

    public void setDtUltBollEmesDb(String dtUltBollEmesDb) {
        this.dtUltBollEmesDb = Functions.subString(dtUltBollEmesDb, Len.DT_ULT_BOLL_EMES_DB);
    }

    public String getDtUltBollEmesDb() {
        return this.dtUltBollEmesDb;
    }

    public void setDtUltBollStorDb(String dtUltBollStorDb) {
        this.dtUltBollStorDb = Functions.subString(dtUltBollStorDb, Len.DT_ULT_BOLL_STOR_DB);
    }

    public String getDtUltBollStorDb() {
        return this.dtUltBollStorDb;
    }

    public void setDtUltBollLiqDb(String dtUltBollLiqDb) {
        this.dtUltBollLiqDb = Functions.subString(dtUltBollLiqDb, Len.DT_ULT_BOLL_LIQ_DB);
    }

    public String getDtUltBollLiqDb() {
        return this.dtUltBollLiqDb;
    }

    public void setDtUltBollRiatDb(String dtUltBollRiatDb) {
        this.dtUltBollRiatDb = Functions.subString(dtUltBollRiatDb, Len.DT_ULT_BOLL_RIAT_DB);
    }

    public String getDtUltBollRiatDb() {
        return this.dtUltBollRiatDb;
    }

    public void setDtUltelriscparPrDb(String dtUltelriscparPrDb) {
        this.dtUltelriscparPrDb = Functions.subString(dtUltelriscparPrDb, Len.DT_ULTELRISCPAR_PR_DB);
    }

    public String getDtUltelriscparPrDb() {
        return this.dtUltelriscparPrDb;
    }

    public void setDtUltcIsInDb(String dtUltcIsInDb) {
        this.dtUltcIsInDb = Functions.subString(dtUltcIsInDb, Len.DT_ULTC_IS_IN_DB);
    }

    public String getDtUltcIsInDb() {
        return this.dtUltcIsInDb;
    }

    public void setDtUltRiclPreDb(String dtUltRiclPreDb) {
        this.dtUltRiclPreDb = Functions.subString(dtUltRiclPreDb, Len.DT_ULT_RICL_PRE_DB);
    }

    public String getDtUltRiclPreDb() {
        return this.dtUltRiclPreDb;
    }

    public void setDtUltcMarsolDb(String dtUltcMarsolDb) {
        this.dtUltcMarsolDb = Functions.subString(dtUltcMarsolDb, Len.DT_ULTC_MARSOL_DB);
    }

    public String getDtUltcMarsolDb() {
        return this.dtUltcMarsolDb;
    }

    public void setDtUltcRbInDb(String dtUltcRbInDb) {
        this.dtUltcRbInDb = Functions.subString(dtUltcRbInDb, Len.DT_ULTC_RB_IN_DB);
    }

    public String getDtUltcRbInDb() {
        return this.dtUltcRbInDb;
    }

    public void setDtUltgzTrchEInDb(String dtUltgzTrchEInDb) {
        this.dtUltgzTrchEInDb = Functions.subString(dtUltgzTrchEInDb, Len.DT_ULTGZ_TRCH_E_IN_DB);
    }

    public String getDtUltgzTrchEInDb() {
        return this.dtUltgzTrchEInDb;
    }

    public void setDtUltBollSndenDb(String dtUltBollSndenDb) {
        this.dtUltBollSndenDb = Functions.subString(dtUltBollSndenDb, Len.DT_ULT_BOLL_SNDEN_DB);
    }

    public String getDtUltBollSndenDb() {
        return this.dtUltBollSndenDb;
    }

    public void setDtUltBollSndnlqDb(String dtUltBollSndnlqDb) {
        this.dtUltBollSndnlqDb = Functions.subString(dtUltBollSndnlqDb, Len.DT_ULT_BOLL_SNDNLQ_DB);
    }

    public String getDtUltBollSndnlqDb() {
        return this.dtUltBollSndnlqDb;
    }

    public void setDtUltscElabInDb(String dtUltscElabInDb) {
        this.dtUltscElabInDb = Functions.subString(dtUltscElabInDb, Len.DT_ULTSC_ELAB_IN_DB);
    }

    public String getDtUltscElabInDb() {
        return this.dtUltscElabInDb;
    }

    public void setDtUltscOpzInDb(String dtUltscOpzInDb) {
        this.dtUltscOpzInDb = Functions.subString(dtUltscOpzInDb, Len.DT_ULTSC_OPZ_IN_DB);
    }

    public String getDtUltscOpzInDb() {
        return this.dtUltscOpzInDb;
    }

    public void setDtUltcBnsricInDb(String dtUltcBnsricInDb) {
        this.dtUltcBnsricInDb = Functions.subString(dtUltcBnsricInDb, Len.DT_ULTC_BNSRIC_IN_DB);
    }

    public String getDtUltcBnsricInDb() {
        return this.dtUltcBnsricInDb;
    }

    public void setDtUltcBnsfdtInDb(String dtUltcBnsfdtInDb) {
        this.dtUltcBnsfdtInDb = Functions.subString(dtUltcBnsfdtInDb, Len.DT_ULTC_BNSFDT_IN_DB);
    }

    public String getDtUltcBnsfdtInDb() {
        return this.dtUltcBnsfdtInDb;
    }

    public void setDtUltRinnGaracDb(String dtUltRinnGaracDb) {
        this.dtUltRinnGaracDb = Functions.subString(dtUltRinnGaracDb, Len.DT_ULT_RINN_GARAC_DB);
    }

    public String getDtUltRinnGaracDb() {
        return this.dtUltRinnGaracDb;
    }

    public void setDtUltgzCedDb(String dtUltgzCedDb) {
        this.dtUltgzCedDb = Functions.subString(dtUltgzCedDb, Len.DT_ULTGZ_CED_DB);
    }

    public String getDtUltgzCedDb() {
        return this.dtUltgzCedDb;
    }

    public void setDtUltElabPrlcosDb(String dtUltElabPrlcosDb) {
        this.dtUltElabPrlcosDb = Functions.subString(dtUltElabPrlcosDb, Len.DT_ULT_ELAB_PRLCOS_DB);
    }

    public String getDtUltElabPrlcosDb() {
        return this.dtUltElabPrlcosDb;
    }

    public void setDtUltRinnCollDb(String dtUltRinnCollDb) {
        this.dtUltRinnCollDb = Functions.subString(dtUltRinnCollDb, Len.DT_ULT_RINN_COLL_DB);
    }

    public String getDtUltRinnCollDb() {
        return this.dtUltRinnCollDb;
    }

    public void setDtUltRivalClDb(String dtUltRivalClDb) {
        this.dtUltRivalClDb = Functions.subString(dtUltRivalClDb, Len.DT_ULT_RIVAL_CL_DB);
    }

    public String getDtUltRivalClDb() {
        return this.dtUltRivalClDb;
    }

    public void setDtUltQtzoClDb(String dtUltQtzoClDb) {
        this.dtUltQtzoClDb = Functions.subString(dtUltQtzoClDb, Len.DT_ULT_QTZO_CL_DB);
    }

    public String getDtUltQtzoClDb() {
        return this.dtUltQtzoClDb;
    }

    public void setDtUltcBnsricClDb(String dtUltcBnsricClDb) {
        this.dtUltcBnsricClDb = Functions.subString(dtUltcBnsricClDb, Len.DT_ULTC_BNSRIC_CL_DB);
    }

    public String getDtUltcBnsricClDb() {
        return this.dtUltcBnsricClDb;
    }

    public void setDtUltcBnsfdtClDb(String dtUltcBnsfdtClDb) {
        this.dtUltcBnsfdtClDb = Functions.subString(dtUltcBnsfdtClDb, Len.DT_ULTC_BNSFDT_CL_DB);
    }

    public String getDtUltcBnsfdtClDb() {
        return this.dtUltcBnsfdtClDb;
    }

    public void setDtUltcIsClDb(String dtUltcIsClDb) {
        this.dtUltcIsClDb = Functions.subString(dtUltcIsClDb, Len.DT_ULTC_IS_CL_DB);
    }

    public String getDtUltcIsClDb() {
        return this.dtUltcIsClDb;
    }

    public void setDtUltcRbClDb(String dtUltcRbClDb) {
        this.dtUltcRbClDb = Functions.subString(dtUltcRbClDb, Len.DT_ULTC_RB_CL_DB);
    }

    public String getDtUltcRbClDb() {
        return this.dtUltcRbClDb;
    }

    public void setDtUltgzTrchEClDb(String dtUltgzTrchEClDb) {
        this.dtUltgzTrchEClDb = Functions.subString(dtUltgzTrchEClDb, Len.DT_ULTGZ_TRCH_E_CL_DB);
    }

    public String getDtUltgzTrchEClDb() {
        return this.dtUltgzTrchEClDb;
    }

    public void setDtUltscElabClDb(String dtUltscElabClDb) {
        this.dtUltscElabClDb = Functions.subString(dtUltscElabClDb, Len.DT_ULTSC_ELAB_CL_DB);
    }

    public String getDtUltscElabClDb() {
        return this.dtUltscElabClDb;
    }

    public void setDtUltscOpzClDb(String dtUltscOpzClDb) {
        this.dtUltscOpzClDb = Functions.subString(dtUltscOpzClDb, Len.DT_ULTSC_OPZ_CL_DB);
    }

    public String getDtUltscOpzClDb() {
        return this.dtUltscOpzClDb;
    }

    public void setStstXRegioneDb(String ststXRegioneDb) {
        this.ststXRegioneDb = Functions.subString(ststXRegioneDb, Len.STST_X_REGIONE_DB);
    }

    public String getStstXRegioneDb() {
        return this.ststXRegioneDb;
    }

    public void setDtUltgzCedCollDb(String dtUltgzCedCollDb) {
        this.dtUltgzCedCollDb = Functions.subString(dtUltgzCedCollDb, Len.DT_ULTGZ_CED_COLL_DB);
    }

    public String getDtUltgzCedCollDb() {
        return this.dtUltgzCedCollDb;
    }

    public void setDtUltEcRivCollDb(String dtUltEcRivCollDb) {
        this.dtUltEcRivCollDb = Functions.subString(dtUltEcRivCollDb, Len.DT_ULT_EC_RIV_COLL_DB);
    }

    public String getDtUltEcRivCollDb() {
        return this.dtUltEcRivCollDb;
    }

    public void setDtUltEcRivIndDb(String dtUltEcRivIndDb) {
        this.dtUltEcRivIndDb = Functions.subString(dtUltEcRivIndDb, Len.DT_ULT_EC_RIV_IND_DB);
    }

    public String getDtUltEcRivIndDb() {
        return this.dtUltEcRivIndDb;
    }

    public void setDtUltEcIlCollDb(String dtUltEcIlCollDb) {
        this.dtUltEcIlCollDb = Functions.subString(dtUltEcIlCollDb, Len.DT_ULT_EC_IL_COLL_DB);
    }

    public String getDtUltEcIlCollDb() {
        return this.dtUltEcIlCollDb;
    }

    public void setDtUltEcIlIndDb(String dtUltEcIlIndDb) {
        this.dtUltEcIlIndDb = Functions.subString(dtUltEcIlIndDb, Len.DT_ULT_EC_IL_IND_DB);
    }

    public String getDtUltEcIlIndDb() {
        return this.dtUltEcIlIndDb;
    }

    public void setDtUltEcUlCollDb(String dtUltEcUlCollDb) {
        this.dtUltEcUlCollDb = Functions.subString(dtUltEcUlCollDb, Len.DT_ULT_EC_UL_COLL_DB);
    }

    public String getDtUltEcUlCollDb() {
        return this.dtUltEcUlCollDb;
    }

    public void setDtUltEcUlIndDb(String dtUltEcUlIndDb) {
        this.dtUltEcUlIndDb = Functions.subString(dtUltEcUlIndDb, Len.DT_ULT_EC_UL_IND_DB);
    }

    public String getDtUltEcUlIndDb() {
        return this.dtUltEcUlIndDb;
    }

    public void setDtUltBollPerfCDb(String dtUltBollPerfCDb) {
        this.dtUltBollPerfCDb = Functions.subString(dtUltBollPerfCDb, Len.DT_ULT_BOLL_PERF_C_DB);
    }

    public String getDtUltBollPerfCDb() {
        return this.dtUltBollPerfCDb;
    }

    public void setDtUltBollRspInDb(String dtUltBollRspInDb) {
        this.dtUltBollRspInDb = Functions.subString(dtUltBollRspInDb, Len.DT_ULT_BOLL_RSP_IN_DB);
    }

    public String getDtUltBollRspInDb() {
        return this.dtUltBollRspInDb;
    }

    public void setDtUltBollRspClDb(String dtUltBollRspClDb) {
        this.dtUltBollRspClDb = Functions.subString(dtUltBollRspClDb, Len.DT_ULT_BOLL_RSP_CL_DB);
    }

    public String getDtUltBollRspClDb() {
        return this.dtUltBollRspClDb;
    }

    public void setDtUltBollEmesIDb(String dtUltBollEmesIDb) {
        this.dtUltBollEmesIDb = Functions.subString(dtUltBollEmesIDb, Len.DT_ULT_BOLL_EMES_I_DB);
    }

    public String getDtUltBollEmesIDb() {
        return this.dtUltBollEmesIDb;
    }

    public void setDtUltBollStorIDb(String dtUltBollStorIDb) {
        this.dtUltBollStorIDb = Functions.subString(dtUltBollStorIDb, Len.DT_ULT_BOLL_STOR_I_DB);
    }

    public String getDtUltBollStorIDb() {
        return this.dtUltBollStorIDb;
    }

    public void setDtUltBollRiatIDb(String dtUltBollRiatIDb) {
        this.dtUltBollRiatIDb = Functions.subString(dtUltBollRiatIDb, Len.DT_ULT_BOLL_RIAT_I_DB);
    }

    public String getDtUltBollRiatIDb() {
        return this.dtUltBollRiatIDb;
    }

    public void setDtUltBollSdIDb(String dtUltBollSdIDb) {
        this.dtUltBollSdIDb = Functions.subString(dtUltBollSdIDb, Len.DT_ULT_BOLL_SD_I_DB);
    }

    public String getDtUltBollSdIDb() {
        return this.dtUltBollSdIDb;
    }

    public void setDtUltBollSdnlIDb(String dtUltBollSdnlIDb) {
        this.dtUltBollSdnlIDb = Functions.subString(dtUltBollSdnlIDb, Len.DT_ULT_BOLL_SDNL_I_DB);
    }

    public String getDtUltBollSdnlIDb() {
        return this.dtUltBollSdnlIDb;
    }

    public void setDtUltBollPerfIDb(String dtUltBollPerfIDb) {
        this.dtUltBollPerfIDb = Functions.subString(dtUltBollPerfIDb, Len.DT_ULT_BOLL_PERF_I_DB);
    }

    public String getDtUltBollPerfIDb() {
        return this.dtUltBollPerfIDb;
    }

    public void setDtRiclRiriasComDb(String dtRiclRiriasComDb) {
        this.dtRiclRiriasComDb = Functions.subString(dtRiclRiriasComDb, Len.DT_RICL_RIRIAS_COM_DB);
    }

    public String getDtRiclRiriasComDb() {
        return this.dtRiclRiriasComDb;
    }

    public void setDtUltElabAt92CDb(String dtUltElabAt92CDb) {
        this.dtUltElabAt92CDb = Functions.subString(dtUltElabAt92CDb, Len.DT_ULT_ELAB_AT92_C_DB);
    }

    public String getDtUltElabAt92CDb() {
        return this.dtUltElabAt92CDb;
    }

    public void setDtUltElabAt92IDb(String dtUltElabAt92IDb) {
        this.dtUltElabAt92IDb = Functions.subString(dtUltElabAt92IDb, Len.DT_ULT_ELAB_AT92_I_DB);
    }

    public String getDtUltElabAt92IDb() {
        return this.dtUltElabAt92IDb;
    }

    public void setDtUltElabAt93CDb(String dtUltElabAt93CDb) {
        this.dtUltElabAt93CDb = Functions.subString(dtUltElabAt93CDb, Len.DT_ULT_ELAB_AT93_C_DB);
    }

    public String getDtUltElabAt93CDb() {
        return this.dtUltElabAt93CDb;
    }

    public void setDtUltElabAt93IDb(String dtUltElabAt93IDb) {
        this.dtUltElabAt93IDb = Functions.subString(dtUltElabAt93IDb, Len.DT_ULT_ELAB_AT93_I_DB);
    }

    public String getDtUltElabAt93IDb() {
        return this.dtUltElabAt93IDb;
    }

    public void setDtUltElabSpeInDb(String dtUltElabSpeInDb) {
        this.dtUltElabSpeInDb = Functions.subString(dtUltElabSpeInDb, Len.DT_ULT_ELAB_SPE_IN_DB);
    }

    public String getDtUltElabSpeInDb() {
        return this.dtUltElabSpeInDb;
    }

    public void setDtUltElabPrConDb(String dtUltElabPrConDb) {
        this.dtUltElabPrConDb = Functions.subString(dtUltElabPrConDb, Len.DT_ULT_ELAB_PR_CON_DB);
    }

    public String getDtUltElabPrConDb() {
        return this.dtUltElabPrConDb;
    }

    public void setDtUltBollRpClDb(String dtUltBollRpClDb) {
        this.dtUltBollRpClDb = Functions.subString(dtUltBollRpClDb, Len.DT_ULT_BOLL_RP_CL_DB);
    }

    public String getDtUltBollRpClDb() {
        return this.dtUltBollRpClDb;
    }

    public void setDtUltBollRpInDb(String dtUltBollRpInDb) {
        this.dtUltBollRpInDb = Functions.subString(dtUltBollRpInDb, Len.DT_ULT_BOLL_RP_IN_DB);
    }

    public String getDtUltBollRpInDb() {
        return this.dtUltBollRpInDb;
    }

    public void setDtUltBollPreIDb(String dtUltBollPreIDb) {
        this.dtUltBollPreIDb = Functions.subString(dtUltBollPreIDb, Len.DT_ULT_BOLL_PRE_I_DB);
    }

    public String getDtUltBollPreIDb() {
        return this.dtUltBollPreIDb;
    }

    public void setDtUltBollPreCDb(String dtUltBollPreCDb) {
        this.dtUltBollPreCDb = Functions.subString(dtUltBollPreCDb, Len.DT_ULT_BOLL_PRE_C_DB);
    }

    public String getDtUltBollPreCDb() {
        return this.dtUltBollPreCDb;
    }

    public void setDtUltcPildiMmCDb(String dtUltcPildiMmCDb) {
        this.dtUltcPildiMmCDb = Functions.subString(dtUltcPildiMmCDb, Len.DT_ULTC_PILDI_MM_C_DB);
    }

    public String getDtUltcPildiMmCDb() {
        return this.dtUltcPildiMmCDb;
    }

    public void setDtUltcPildiAaCDb(String dtUltcPildiAaCDb) {
        this.dtUltcPildiAaCDb = Functions.subString(dtUltcPildiAaCDb, Len.DT_ULTC_PILDI_AA_C_DB);
    }

    public String getDtUltcPildiAaCDb() {
        return this.dtUltcPildiAaCDb;
    }

    public void setDtUltcPildiMmIDb(String dtUltcPildiMmIDb) {
        this.dtUltcPildiMmIDb = Functions.subString(dtUltcPildiMmIDb, Len.DT_ULTC_PILDI_MM_I_DB);
    }

    public String getDtUltcPildiMmIDb() {
        return this.dtUltcPildiMmIDb;
    }

    public void setDtUltcPildiTrIDb(String dtUltcPildiTrIDb) {
        this.dtUltcPildiTrIDb = Functions.subString(dtUltcPildiTrIDb, Len.DT_ULTC_PILDI_TR_I_DB);
    }

    public String getDtUltcPildiTrIDb() {
        return this.dtUltcPildiTrIDb;
    }

    public void setDtUltcPildiAaIDb(String dtUltcPildiAaIDb) {
        this.dtUltcPildiAaIDb = Functions.subString(dtUltcPildiAaIDb, Len.DT_ULTC_PILDI_AA_I_DB);
    }

    public String getDtUltcPildiAaIDb() {
        return this.dtUltcPildiAaIDb;
    }

    public void setDtUltBollQuieCDb(String dtUltBollQuieCDb) {
        this.dtUltBollQuieCDb = Functions.subString(dtUltBollQuieCDb, Len.DT_ULT_BOLL_QUIE_C_DB);
    }

    public String getDtUltBollQuieCDb() {
        return this.dtUltBollQuieCDb;
    }

    public void setDtUltBollQuieIDb(String dtUltBollQuieIDb) {
        this.dtUltBollQuieIDb = Functions.subString(dtUltBollQuieIDb, Len.DT_ULT_BOLL_QUIE_I_DB);
    }

    public String getDtUltBollQuieIDb() {
        return this.dtUltBollQuieIDb;
    }

    public void setDtUltBollCotrIDb(String dtUltBollCotrIDb) {
        this.dtUltBollCotrIDb = Functions.subString(dtUltBollCotrIDb, Len.DT_ULT_BOLL_COTR_I_DB);
    }

    public String getDtUltBollCotrIDb() {
        return this.dtUltBollCotrIDb;
    }

    public void setDtUltBollCotrCDb(String dtUltBollCotrCDb) {
        this.dtUltBollCotrCDb = Functions.subString(dtUltBollCotrCDb, Len.DT_ULT_BOLL_COTR_C_DB);
    }

    public String getDtUltBollCotrCDb() {
        return this.dtUltBollCotrCDb;
    }

    public void setDtUltBollCoriCDb(String dtUltBollCoriCDb) {
        this.dtUltBollCoriCDb = Functions.subString(dtUltBollCoriCDb, Len.DT_ULT_BOLL_CORI_C_DB);
    }

    public String getDtUltBollCoriCDb() {
        return this.dtUltBollCoriCDb;
    }

    public void setDtUltBollCoriIDb(String dtUltBollCoriIDb) {
        this.dtUltBollCoriIDb = Functions.subString(dtUltBollCoriIDb, Len.DT_ULT_BOLL_CORI_I_DB);
    }

    public String getDtUltBollCoriIDb() {
        return this.dtUltBollCoriIDb;
    }

    public void setDtUltAggErogReDb(String dtUltAggErogReDb) {
        this.dtUltAggErogReDb = Functions.subString(dtUltAggErogReDb, Len.DT_ULT_AGG_EROG_RE_DB);
    }

    public String getDtUltAggErogReDb() {
        return this.dtUltAggErogReDb;
    }

    public void setDtUltEstrazFugDb(String dtUltEstrazFugDb) {
        this.dtUltEstrazFugDb = Functions.subString(dtUltEstrazFugDb, Len.DT_ULT_ESTRAZ_FUG_DB);
    }

    public String getDtUltEstrazFugDb() {
        return this.dtUltEstrazFugDb;
    }

    public void setDtUltElabPrAutDb(String dtUltElabPrAutDb) {
        this.dtUltElabPrAutDb = Functions.subString(dtUltElabPrAutDb, Len.DT_ULT_ELAB_PR_AUT_DB);
    }

    public String getDtUltElabPrAutDb() {
        return this.dtUltElabPrAutDb;
    }

    public void setDtUltElabCommefDb(String dtUltElabCommefDb) {
        this.dtUltElabCommefDb = Functions.subString(dtUltElabCommefDb, Len.DT_ULT_ELAB_COMMEF_DB);
    }

    public String getDtUltElabCommefDb() {
        return this.dtUltElabCommefDb;
    }

    public void setDtUltElabLiqmefDb(String dtUltElabLiqmefDb) {
        this.dtUltElabLiqmefDb = Functions.subString(dtUltElabLiqmefDb, Len.DT_ULT_ELAB_LIQMEF_DB);
    }

    public String getDtUltElabLiqmefDb() {
        return this.dtUltElabLiqmefDb;
    }

    public void setDtRiatRiassRshDb(String dtRiatRiassRshDb) {
        this.dtRiatRiassRshDb = Functions.subString(dtRiatRiassRshDb, Len.DT_RIAT_RIASS_RSH_DB);
    }

    public String getDtRiatRiassRshDb() {
        return this.dtRiatRiassRshDb;
    }

    public void setDtRiatRiassCommDb(String dtRiatRiassCommDb) {
        this.dtRiatRiassCommDb = Functions.subString(dtRiatRiassCommDb, Len.DT_RIAT_RIASS_COMM_DB);
    }

    public String getDtRiatRiassCommDb() {
        return this.dtRiatRiassCommDb;
    }

    public void setDtUltRinnTacDb(String dtUltRinnTacDb) {
        this.dtUltRinnTacDb = Functions.subString(dtUltRinnTacDb, Len.DT_ULT_RINN_TAC_DB);
    }

    public String getDtUltRinnTacDb() {
        return this.dtUltRinnTacDb;
    }

    public void setDtUltEcTcmIndDb(String dtUltEcTcmIndDb) {
        this.dtUltEcTcmIndDb = Functions.subString(dtUltEcTcmIndDb, Len.DT_ULT_EC_TCM_IND_DB);
    }

    public String getDtUltEcTcmIndDb() {
        return this.dtUltEcTcmIndDb;
    }

    public void setDtUltEcTcmCollDb(String dtUltEcTcmCollDb) {
        this.dtUltEcTcmCollDb = Functions.subString(dtUltEcTcmCollDb, Len.DT_ULT_EC_TCM_COLL_DB);
    }

    public String getDtUltEcTcmCollDb() {
        return this.dtUltEcTcmCollDb;
    }

    public void setDtUltEcMrmIndDb(String dtUltEcMrmIndDb) {
        this.dtUltEcMrmIndDb = Functions.subString(dtUltEcMrmIndDb, Len.DT_ULT_EC_MRM_IND_DB);
    }

    public String getDtUltEcMrmIndDb() {
        return this.dtUltEcMrmIndDb;
    }

    public void setDtUltEcMrmCollDb(String dtUltEcMrmCollDb) {
        this.dtUltEcMrmCollDb = Functions.subString(dtUltEcMrmCollDb, Len.DT_ULT_EC_MRM_COLL_DB);
    }

    public String getDtUltEcMrmCollDb() {
        return this.dtUltEcMrmCollDb;
    }

    public void setDtUltElabRedproDb(String dtUltElabRedproDb) {
        this.dtUltElabRedproDb = Functions.subString(dtUltElabRedproDb, Len.DT_ULT_ELAB_REDPRO_DB);
    }

    public String getDtUltElabRedproDb() {
        return this.dtUltElabRedproDb;
    }

    public void setDtUltElabTakePDb(String dtUltElabTakePDb) {
        this.dtUltElabTakePDb = Functions.subString(dtUltElabTakePDb, Len.DT_ULT_ELAB_TAKE_P_DB);
    }

    public String getDtUltElabTakePDb() {
        return this.dtUltElabTakePDb;
    }

    public void setDtUltElabPaspasDb(String dtUltElabPaspasDb) {
        this.dtUltElabPaspasDb = Functions.subString(dtUltElabPaspasDb, Len.DT_ULT_ELAB_PASPAS_DB);
    }

    public String getDtUltElabPaspasDb() {
        return this.dtUltElabPaspasDb;
    }

    public void setDtUltEstrDecCoDb(String dtUltEstrDecCoDb) {
        this.dtUltEstrDecCoDb = Functions.subString(dtUltEstrDecCoDb, Len.DT_ULT_ESTR_DEC_CO_DB);
    }

    public String getDtUltEstrDecCoDb() {
        return this.dtUltEstrDecCoDb;
    }

    public void setDtUltElabCosAtDb(String dtUltElabCosAtDb) {
        this.dtUltElabCosAtDb = Functions.subString(dtUltElabCosAtDb, Len.DT_ULT_ELAB_COS_AT_DB);
    }

    public String getDtUltElabCosAtDb() {
        return this.dtUltElabCosAtDb;
    }

    public void setDtUltElabCosStDb(String dtUltElabCosStDb) {
        this.dtUltElabCosStDb = Functions.subString(dtUltElabCosStDb, Len.DT_ULT_ELAB_COS_ST_DB);
    }

    public String getDtUltElabCosStDb() {
        return this.dtUltElabCosStDb;
    }

    public void setDtEstrAssMin70aDb(String dtEstrAssMin70aDb) {
        this.dtEstrAssMin70aDb = Functions.subString(dtEstrAssMin70aDb, Len.DT_ESTR_ASS_MIN70A_DB);
    }

    public String getDtEstrAssMin70aDb() {
        return this.dtEstrAssMin70aDb;
    }

    public void setDtEstrAssMag70aDb(String dtEstrAssMag70aDb) {
        this.dtEstrAssMag70aDb = Functions.subString(dtEstrAssMag70aDb, Len.DT_ESTR_ASS_MAG70A_DB);
    }

    public String getDtEstrAssMag70aDb() {
        return this.dtEstrAssMag70aDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_CONT_DB = 10;
        public static final int DT_ULT_RIVAL_IN_DB = 10;
        public static final int DT_ULT_QTZO_IN_DB = 10;
        public static final int DT_ULT_RICL_RIASS_DB = 10;
        public static final int DT_ULT_TABUL_RIASS_DB = 10;
        public static final int DT_ULT_BOLL_EMES_DB = 10;
        public static final int DT_ULT_BOLL_STOR_DB = 10;
        public static final int DT_ULT_BOLL_LIQ_DB = 10;
        public static final int DT_ULT_BOLL_RIAT_DB = 10;
        public static final int DT_ULTELRISCPAR_PR_DB = 10;
        public static final int DT_ULTC_IS_IN_DB = 10;
        public static final int DT_ULT_RICL_PRE_DB = 10;
        public static final int DT_ULTC_MARSOL_DB = 10;
        public static final int DT_ULTC_RB_IN_DB = 10;
        public static final int DT_ULTGZ_TRCH_E_IN_DB = 10;
        public static final int DT_ULT_BOLL_SNDEN_DB = 10;
        public static final int DT_ULT_BOLL_SNDNLQ_DB = 10;
        public static final int DT_ULTSC_ELAB_IN_DB = 10;
        public static final int DT_ULTSC_OPZ_IN_DB = 10;
        public static final int DT_ULTC_BNSRIC_IN_DB = 10;
        public static final int DT_ULTC_BNSFDT_IN_DB = 10;
        public static final int DT_ULT_RINN_GARAC_DB = 10;
        public static final int DT_ULTGZ_CED_DB = 10;
        public static final int DT_ULT_ELAB_PRLCOS_DB = 10;
        public static final int DT_ULT_RINN_COLL_DB = 10;
        public static final int DT_ULT_RIVAL_CL_DB = 10;
        public static final int DT_ULT_QTZO_CL_DB = 10;
        public static final int DT_ULTC_BNSRIC_CL_DB = 10;
        public static final int DT_ULTC_BNSFDT_CL_DB = 10;
        public static final int DT_ULTC_IS_CL_DB = 10;
        public static final int DT_ULTC_RB_CL_DB = 10;
        public static final int DT_ULTGZ_TRCH_E_CL_DB = 10;
        public static final int DT_ULTSC_ELAB_CL_DB = 10;
        public static final int DT_ULTSC_OPZ_CL_DB = 10;
        public static final int STST_X_REGIONE_DB = 10;
        public static final int DT_ULTGZ_CED_COLL_DB = 10;
        public static final int DT_ULT_EC_RIV_COLL_DB = 10;
        public static final int DT_ULT_EC_RIV_IND_DB = 10;
        public static final int DT_ULT_EC_IL_COLL_DB = 10;
        public static final int DT_ULT_EC_IL_IND_DB = 10;
        public static final int DT_ULT_EC_UL_COLL_DB = 10;
        public static final int DT_ULT_EC_UL_IND_DB = 10;
        public static final int DT_ULT_BOLL_PERF_C_DB = 10;
        public static final int DT_ULT_BOLL_RSP_IN_DB = 10;
        public static final int DT_ULT_BOLL_RSP_CL_DB = 10;
        public static final int DT_ULT_BOLL_EMES_I_DB = 10;
        public static final int DT_ULT_BOLL_STOR_I_DB = 10;
        public static final int DT_ULT_BOLL_RIAT_I_DB = 10;
        public static final int DT_ULT_BOLL_SD_I_DB = 10;
        public static final int DT_ULT_BOLL_SDNL_I_DB = 10;
        public static final int DT_ULT_BOLL_PERF_I_DB = 10;
        public static final int DT_RICL_RIRIAS_COM_DB = 10;
        public static final int DT_ULT_ELAB_AT92_C_DB = 10;
        public static final int DT_ULT_ELAB_AT92_I_DB = 10;
        public static final int DT_ULT_ELAB_AT93_C_DB = 10;
        public static final int DT_ULT_ELAB_AT93_I_DB = 10;
        public static final int DT_ULT_ELAB_SPE_IN_DB = 10;
        public static final int DT_ULT_ELAB_PR_CON_DB = 10;
        public static final int DT_ULT_BOLL_RP_CL_DB = 10;
        public static final int DT_ULT_BOLL_RP_IN_DB = 10;
        public static final int DT_ULT_BOLL_PRE_I_DB = 10;
        public static final int DT_ULT_BOLL_PRE_C_DB = 10;
        public static final int DT_ULTC_PILDI_MM_C_DB = 10;
        public static final int DT_ULTC_PILDI_AA_C_DB = 10;
        public static final int DT_ULTC_PILDI_MM_I_DB = 10;
        public static final int DT_ULTC_PILDI_TR_I_DB = 10;
        public static final int DT_ULTC_PILDI_AA_I_DB = 10;
        public static final int DT_ULT_BOLL_QUIE_C_DB = 10;
        public static final int DT_ULT_BOLL_QUIE_I_DB = 10;
        public static final int DT_ULT_BOLL_COTR_I_DB = 10;
        public static final int DT_ULT_BOLL_COTR_C_DB = 10;
        public static final int DT_ULT_BOLL_CORI_C_DB = 10;
        public static final int DT_ULT_BOLL_CORI_I_DB = 10;
        public static final int DT_ULT_AGG_EROG_RE_DB = 10;
        public static final int DT_ULT_ESTRAZ_FUG_DB = 10;
        public static final int DT_ULT_ELAB_PR_AUT_DB = 10;
        public static final int DT_ULT_ELAB_COMMEF_DB = 10;
        public static final int DT_ULT_ELAB_LIQMEF_DB = 10;
        public static final int DT_RIAT_RIASS_RSH_DB = 10;
        public static final int DT_RIAT_RIASS_COMM_DB = 10;
        public static final int DT_ULT_RINN_TAC_DB = 10;
        public static final int DT_ULT_EC_TCM_IND_DB = 10;
        public static final int DT_ULT_EC_TCM_COLL_DB = 10;
        public static final int DT_ULT_EC_MRM_IND_DB = 10;
        public static final int DT_ULT_EC_MRM_COLL_DB = 10;
        public static final int DT_ULT_ELAB_REDPRO_DB = 10;
        public static final int DT_ULT_ELAB_TAKE_P_DB = 10;
        public static final int DT_ULT_ELAB_PASPAS_DB = 10;
        public static final int DT_ULT_ESTR_DEC_CO_DB = 10;
        public static final int DT_ULT_ELAB_COS_AT_DB = 10;
        public static final int DT_ULT_ELAB_COS_ST_DB = 10;
        public static final int DT_ESTR_ASS_MIN70A_DB = 10;
        public static final int DT_ESTR_ASS_MAG70A_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
