package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV2171<br>
 * Variable: LDBV2171 from copybook LDBV2171<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv2171 {

    //==== PROPERTIES ====
    //Original name: LDBV2171-ID-RAPP-ANA
    private int idRappAna = DefaultValues.INT_VAL;
    //Original name: LDBV2171-ID-GAR
    private int idGar = DefaultValues.INT_VAL;
    //Original name: LDBV2171-COD-DOM
    private String codDom = DefaultValues.stringVal(Len.COD_DOM);

    //==== METHODS ====
    public String getLdbv2171Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv2171Bytes());
    }

    public byte[] getLdbv2171Bytes() {
        byte[] buffer = new byte[Len.LDBV2171];
        return getLdbv2171Bytes(buffer, 1);
    }

    public byte[] getLdbv2171Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idRappAna, Len.Int.ID_RAPP_ANA, 0);
        position += Len.ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, idGar, Len.Int.ID_GAR, 0);
        position += Len.ID_GAR;
        MarshalByte.writeString(buffer, position, codDom, Len.COD_DOM);
        return buffer;
    }

    public void setIdRappAna(int idRappAna) {
        this.idRappAna = idRappAna;
    }

    public int getIdRappAna() {
        return this.idRappAna;
    }

    public void setIdGar(int idGar) {
        this.idGar = idGar;
    }

    public int getIdGar() {
        return this.idGar;
    }

    public void setCodDom(String codDom) {
        this.codDom = Functions.subString(codDom, Len.COD_DOM);
    }

    public String getCodDom() {
        return this.codDom;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_DOM = 20;
        public static final int ID_RAPP_ANA = 5;
        public static final int ID_GAR = 5;
        public static final int LDBV2171 = ID_RAPP_ANA + ID_GAR + COD_DOM;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_RAPP_ANA = 9;
            public static final int ID_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
