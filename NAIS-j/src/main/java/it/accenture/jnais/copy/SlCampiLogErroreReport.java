package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;

/**Original name: SL-CAMPI-LOG-ERRORE-REPORT<br>
 * Variable: SL-CAMPI-LOG-ERRORE-REPORT from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SlCampiLogErroreReport {

    //==== PROPERTIES ====
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT
    private String flr1 = "";
    //Original name: REP-LIV-GRAVITA-SL
    private char livGravitaSl = '-';
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-1
    private String flr2 = "";
    //Original name: REP-OGG-BUSINESS-SL
    private String oggBusinessSl = LiteralGenerator.create("-", Len.OGG_BUSINESS_SL);
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-2
    private String flr3 = "";
    //Original name: REP-TP-OGG-BUSINESS-SL
    private String tpOggBusinessSl = "--";
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-3
    private String flr4 = "";
    //Original name: REP-IB-OGG-POLI-SL
    private String ibOggPoliSl = LiteralGenerator.create("-", Len.IB_OGG_POLI_SL);
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-4
    private char flr5 = Types.SPACE_CHAR;
    //Original name: REP-IB-OGG-ADEI-SL
    private String ibOggAdeiSl = LiteralGenerator.create("-", Len.IB_OGG_ADEI_SL);
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-5
    private char flr6 = Types.SPACE_CHAR;
    //Original name: REP-ID-POLI-SL
    private String idPoliSl = "---------";
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-6
    private char flr7 = Types.SPACE_CHAR;
    //Original name: REP-ID-ADES-SL
    private String idAdesSl = "---------";
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-7
    private char flr8 = Types.SPACE_CHAR;
    //Original name: REP-DT-EFF-BUSINESS-SL
    private String dtEffBusinessSl = "----------";
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-8
    private char flr9 = Types.SPACE_CHAR;
    //Original name: REP-DESC-ERRORE-SL
    private String descErroreSl = LiteralGenerator.create("-", Len.DESC_ERRORE_SL);
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-9
    private char flr10 = Types.SPACE_CHAR;
    //Original name: REP-COD-MAIN-BATCH-SL
    private String codMainBatchSl = "--------";
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-10
    private char flr11 = Types.SPACE_CHAR;
    //Original name: REP-SERVIZIO-BE-SL
    private String servizioBeSl = "--------";
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-11
    private char flr12 = Types.SPACE_CHAR;
    //Original name: REP-LABEL-ERR-SL
    private String labelErrSl = LiteralGenerator.create("-", Len.LABEL_ERR_SL);
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-12
    private String flr13 = "";
    //Original name: REP-OPER-TABELLA-SL
    private String operTabellaSl = "--";
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-13
    private String flr14 = "";
    //Original name: REP-NOME-TABELLA-SL
    private String nomeTabellaSl = LiteralGenerator.create("-", Len.NOME_TABELLA_SL);
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-14
    private char flr15 = Types.SPACE_CHAR;
    //Original name: REP-STATUS-TABELLA-SL
    private String statusTabellaSl = "----------";
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-15
    private char flr16 = Types.SPACE_CHAR;
    //Original name: REP-TMST-ERRORE-SL
    private String tmstErroreSl = LiteralGenerator.create("-", Len.TMST_ERRORE_SL);
    //Original name: FILLER-SL-CAMPI-LOG-ERRORE-REPORT-16
    private char flr17 = Types.SPACE_CHAR;
    //Original name: REP-KEY-TABELLA-SL
    private String keyTabellaSl = LiteralGenerator.create("-", Len.KEY_TABELLA_SL);

    //==== METHODS ====
    public String getSlCampiLogErroreReportFormatted() {
        return MarshalByteExt.bufferToStr(getSlCampiLogErroreReportBytes());
    }

    public byte[] getSlCampiLogErroreReportBytes() {
        byte[] buffer = new byte[Len.SL_CAMPI_LOG_ERRORE_REPORT];
        return getSlCampiLogErroreReportBytes(buffer, 1);
    }

    public byte[] getSlCampiLogErroreReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeChar(buffer, position, livGravitaSl);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, oggBusinessSl, Len.OGG_BUSINESS_SL);
        position += Len.OGG_BUSINESS_SL;
        MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, tpOggBusinessSl, Len.TP_OGG_BUSINESS_SL);
        position += Len.TP_OGG_BUSINESS_SL;
        MarshalByte.writeString(buffer, position, flr4, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, ibOggPoliSl, Len.IB_OGG_POLI_SL);
        position += Len.IB_OGG_POLI_SL;
        MarshalByte.writeChar(buffer, position, flr5);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ibOggAdeiSl, Len.IB_OGG_ADEI_SL);
        position += Len.IB_OGG_ADEI_SL;
        MarshalByte.writeChar(buffer, position, flr6);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, idPoliSl, Len.ID_POLI_SL);
        position += Len.ID_POLI_SL;
        MarshalByte.writeChar(buffer, position, flr7);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, idAdesSl, Len.ID_ADES_SL);
        position += Len.ID_ADES_SL;
        MarshalByte.writeChar(buffer, position, flr8);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dtEffBusinessSl, Len.DT_EFF_BUSINESS_SL);
        position += Len.DT_EFF_BUSINESS_SL;
        MarshalByte.writeChar(buffer, position, flr9);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, descErroreSl, Len.DESC_ERRORE_SL);
        position += Len.DESC_ERRORE_SL;
        MarshalByte.writeChar(buffer, position, flr10);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codMainBatchSl, Len.COD_MAIN_BATCH_SL);
        position += Len.COD_MAIN_BATCH_SL;
        MarshalByte.writeChar(buffer, position, flr11);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, servizioBeSl, Len.SERVIZIO_BE_SL);
        position += Len.SERVIZIO_BE_SL;
        MarshalByte.writeChar(buffer, position, flr12);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, labelErrSl, Len.LABEL_ERR_SL);
        position += Len.LABEL_ERR_SL;
        MarshalByte.writeString(buffer, position, flr13, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, operTabellaSl, Len.OPER_TABELLA_SL);
        position += Len.OPER_TABELLA_SL;
        MarshalByte.writeString(buffer, position, flr14, Len.FLR14);
        position += Len.FLR14;
        MarshalByte.writeString(buffer, position, nomeTabellaSl, Len.NOME_TABELLA_SL);
        position += Len.NOME_TABELLA_SL;
        MarshalByte.writeChar(buffer, position, flr15);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, statusTabellaSl, Len.STATUS_TABELLA_SL);
        position += Len.STATUS_TABELLA_SL;
        MarshalByte.writeChar(buffer, position, flr16);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tmstErroreSl, Len.TMST_ERRORE_SL);
        position += Len.TMST_ERRORE_SL;
        MarshalByte.writeChar(buffer, position, flr17);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, keyTabellaSl, Len.KEY_TABELLA_SL);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public char getLivGravitaSl() {
        return this.livGravitaSl;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public String getOggBusinessSl() {
        return this.oggBusinessSl;
    }

    public String getFlr3() {
        return this.flr3;
    }

    public String getTpOggBusinessSl() {
        return this.tpOggBusinessSl;
    }

    public String getFlr4() {
        return this.flr4;
    }

    public String getIbOggPoliSl() {
        return this.ibOggPoliSl;
    }

    public char getFlr5() {
        return this.flr5;
    }

    public String getIbOggAdeiSl() {
        return this.ibOggAdeiSl;
    }

    public char getFlr6() {
        return this.flr6;
    }

    public String getIdPoliSl() {
        return this.idPoliSl;
    }

    public char getFlr7() {
        return this.flr7;
    }

    public String getIdAdesSl() {
        return this.idAdesSl;
    }

    public char getFlr8() {
        return this.flr8;
    }

    public String getDtEffBusinessSl() {
        return this.dtEffBusinessSl;
    }

    public char getFlr9() {
        return this.flr9;
    }

    public String getDescErroreSl() {
        return this.descErroreSl;
    }

    public char getFlr10() {
        return this.flr10;
    }

    public String getCodMainBatchSl() {
        return this.codMainBatchSl;
    }

    public char getFlr11() {
        return this.flr11;
    }

    public String getServizioBeSl() {
        return this.servizioBeSl;
    }

    public char getFlr12() {
        return this.flr12;
    }

    public String getLabelErrSl() {
        return this.labelErrSl;
    }

    public String getFlr13() {
        return this.flr13;
    }

    public String getOperTabellaSl() {
        return this.operTabellaSl;
    }

    public String getFlr14() {
        return this.flr14;
    }

    public String getNomeTabellaSl() {
        return this.nomeTabellaSl;
    }

    public char getFlr15() {
        return this.flr15;
    }

    public String getStatusTabellaSl() {
        return this.statusTabellaSl;
    }

    public char getFlr16() {
        return this.flr16;
    }

    public String getTmstErroreSl() {
        return this.tmstErroreSl;
    }

    public char getFlr17() {
        return this.flr17;
    }

    public String getKeyTabellaSl() {
        return this.keyTabellaSl;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OGG_BUSINESS_SL = 40;
        public static final int TP_OGG_BUSINESS_SL = 2;
        public static final int IB_OGG_POLI_SL = 40;
        public static final int IB_OGG_ADEI_SL = 40;
        public static final int ID_POLI_SL = 9;
        public static final int ID_ADES_SL = 9;
        public static final int DT_EFF_BUSINESS_SL = 10;
        public static final int DESC_ERRORE_SL = 100;
        public static final int COD_MAIN_BATCH_SL = 8;
        public static final int SERVIZIO_BE_SL = 8;
        public static final int LABEL_ERR_SL = 30;
        public static final int OPER_TABELLA_SL = 2;
        public static final int NOME_TABELLA_SL = 18;
        public static final int STATUS_TABELLA_SL = 10;
        public static final int TMST_ERRORE_SL = 26;
        public static final int KEY_TABELLA_SL = 100;
        public static final int FLR1 = 3;
        public static final int LIV_GRAVITA_SL = 1;
        public static final int FLR2 = 5;
        public static final int FLR3 = 4;
        public static final int FLR5 = 1;
        public static final int FLR14 = 6;
        public static final int SL_CAMPI_LOG_ERRORE_REPORT = LIV_GRAVITA_SL + OGG_BUSINESS_SL + TP_OGG_BUSINESS_SL + IB_OGG_POLI_SL + IB_OGG_ADEI_SL + ID_POLI_SL + ID_ADES_SL + DT_EFF_BUSINESS_SL + DESC_ERRORE_SL + COD_MAIN_BATCH_SL + SERVIZIO_BE_SL + LABEL_ERR_SL + OPER_TABELLA_SL + NOME_TABELLA_SL + STATUS_TABELLA_SL + TMST_ERRORE_SL + KEY_TABELLA_SL + FLR14 + 2 * FLR1 + FLR2 + 2 * FLR3 + 11 * FLR5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
