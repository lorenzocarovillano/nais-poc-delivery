package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Idsv0016TipoAnno;
import it.accenture.jnais.ws.redefines.Idsv0016StrGiorniAnnual;

/**Original name: IDSV0016<br>
 * Variable: IDSV0016 from copybook IDSV0016<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv0016 {

    //==== PROPERTIES ====
    /**Original name: IDSV0016-RISULTATO<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONTROLLO DATA FORMALE
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *  INIZIO STATEMENTS DA UTILIZZARE PRETTAMENTE CON IDSP0016
	 * *****************************************************************</pre>*/
    private short idsv0016Risultato = DefaultValues.SHORT_VAL;
    //Original name: IDSV0016-RESTO
    private short idsv0016Resto = DefaultValues.SHORT_VAL;
    //Original name: IDSV0016-CONST-FEBBRAIO
    private short idsv0016ConstFebbraio = ((short)2);
    //Original name: IDSV0016-DESC-ERRORE
    private String idsv0016DescErrore = DefaultValues.stringVal(Len.IDSV0016_DESC_ERRORE);
    //Original name: IDSV0016-DATA
    private Idsv0016Data idsv0016Data = new Idsv0016Data();
    //Original name: IDSV0016-TIPO-ANNO
    private Idsv0016TipoAnno idsv0016TipoAnno = new Idsv0016TipoAnno();
    //Original name: IDSV0016-STR-GIORNI-ANNUAL
    private Idsv0016StrGiorniAnnual idsv0016StrGiorniAnnual = new Idsv0016StrGiorniAnnual();

    //==== METHODS ====
    public void setIdsv0016Risultato(short idsv0016Risultato) {
        this.idsv0016Risultato = idsv0016Risultato;
    }

    public short getIdsv0016Risultato() {
        return this.idsv0016Risultato;
    }

    public void setIdsv0016Resto(short idsv0016Resto) {
        this.idsv0016Resto = idsv0016Resto;
    }

    public short getIdsv0016Resto() {
        return this.idsv0016Resto;
    }

    public short getIdsv0016ConstFebbraio() {
        return this.idsv0016ConstFebbraio;
    }

    public void setIdsv0016DescErrore(String idsv0016DescErrore) {
        this.idsv0016DescErrore = Functions.subString(idsv0016DescErrore, Len.IDSV0016_DESC_ERRORE);
    }

    public String getIdsv0016DescErrore() {
        return this.idsv0016DescErrore;
    }

    public Idsv0016Data getIdsv0016Data() {
        return idsv0016Data;
    }

    public Idsv0016StrGiorniAnnual getIdsv0016StrGiorniAnnual() {
        return idsv0016StrGiorniAnnual;
    }

    public Idsv0016TipoAnno getIdsv0016TipoAnno() {
        return idsv0016TipoAnno;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSV0016_DESC_ERRORE = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
