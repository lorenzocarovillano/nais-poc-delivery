package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-EST-POLI-CPI-PR<br>
 * Variable: IND-EST-POLI-CPI-PR from copybook IDBVP672<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndEstPoliCpiPr {

    //==== PROPERTIES ====
    //Original name: IND-P67-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-CPT-FIN
    private short cptFin = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-NUM-TST-FIN
    private short numTstFin = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-TS-FINANZ
    private short tsFinanz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-DUR-MM-FINANZ
    private short durMmFinanz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-DT-END-FINANZ
    private short dtEndFinanz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-AMM-1A-RAT
    private short amm1aRat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-VAL-RISC-BENE
    private short valRiscBene = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-AMM-RAT-END
    private short ammRatEnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-TS-CRE-RAT-FINANZ
    private short tsCreRatFinanz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-IMP-FIN-REVOLVING
    private short impFinRevolving = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-IMP-UTIL-C-REV
    private short impUtilCRev = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-IMP-RAT-REVOLVING
    private short impRatRevolving = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-DT-1O-UTLZ-C-REV
    private short dt1oUtlzCRev = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-IMP-ASSTO
    private short impAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-PRE-VERS
    private short preVers = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-DT-SCAD-COP
    private short dtScadCop = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-GG-DEL-MM-SCAD-RAT
    private short ggDelMmScadRat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-DT-SCAD-1A-RAT
    private short dtScad1aRat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-DT-EROG-FINANZ
    private short dtErogFinanz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-DT-STIPULA-FINANZ
    private short dtStipulaFinanz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-MM-PREAMM
    private short mmPreamm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-IMP-DEB-RES
    private short impDebRes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-IMP-RAT-FINANZ
    private short impRatFinanz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-IMP-CANONE-ANTIC
    private short impCanoneAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-PER-RAT-FINANZ
    private short perRatFinanz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-TP-MOD-PAG-RAT
    private short tpModPagRat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-TP-FINANZ-ER
    private short tpFinanzEr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-CAT-FINANZ-ER
    private short catFinanzEr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-VAL-RISC-END-LEAS
    private short valRiscEndLeas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-DT-EST-FINANZ
    private short dtEstFinanz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-DT-MAN-COP
    private short dtManCop = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-NUM-FINANZ
    private short numFinanz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P67-TP-MOD-ACQS
    private short tpModAcqs = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setCptFin(short cptFin) {
        this.cptFin = cptFin;
    }

    public short getCptFin() {
        return this.cptFin;
    }

    public void setNumTstFin(short numTstFin) {
        this.numTstFin = numTstFin;
    }

    public short getNumTstFin() {
        return this.numTstFin;
    }

    public void setTsFinanz(short tsFinanz) {
        this.tsFinanz = tsFinanz;
    }

    public short getTsFinanz() {
        return this.tsFinanz;
    }

    public void setDurMmFinanz(short durMmFinanz) {
        this.durMmFinanz = durMmFinanz;
    }

    public short getDurMmFinanz() {
        return this.durMmFinanz;
    }

    public void setDtEndFinanz(short dtEndFinanz) {
        this.dtEndFinanz = dtEndFinanz;
    }

    public short getDtEndFinanz() {
        return this.dtEndFinanz;
    }

    public void setAmm1aRat(short amm1aRat) {
        this.amm1aRat = amm1aRat;
    }

    public short getAmm1aRat() {
        return this.amm1aRat;
    }

    public void setValRiscBene(short valRiscBene) {
        this.valRiscBene = valRiscBene;
    }

    public short getValRiscBene() {
        return this.valRiscBene;
    }

    public void setAmmRatEnd(short ammRatEnd) {
        this.ammRatEnd = ammRatEnd;
    }

    public short getAmmRatEnd() {
        return this.ammRatEnd;
    }

    public void setTsCreRatFinanz(short tsCreRatFinanz) {
        this.tsCreRatFinanz = tsCreRatFinanz;
    }

    public short getTsCreRatFinanz() {
        return this.tsCreRatFinanz;
    }

    public void setImpFinRevolving(short impFinRevolving) {
        this.impFinRevolving = impFinRevolving;
    }

    public short getImpFinRevolving() {
        return this.impFinRevolving;
    }

    public void setImpUtilCRev(short impUtilCRev) {
        this.impUtilCRev = impUtilCRev;
    }

    public short getImpUtilCRev() {
        return this.impUtilCRev;
    }

    public void setImpRatRevolving(short impRatRevolving) {
        this.impRatRevolving = impRatRevolving;
    }

    public short getImpRatRevolving() {
        return this.impRatRevolving;
    }

    public void setDt1oUtlzCRev(short dt1oUtlzCRev) {
        this.dt1oUtlzCRev = dt1oUtlzCRev;
    }

    public short getDt1oUtlzCRev() {
        return this.dt1oUtlzCRev;
    }

    public void setImpAssto(short impAssto) {
        this.impAssto = impAssto;
    }

    public short getImpAssto() {
        return this.impAssto;
    }

    public void setPreVers(short preVers) {
        this.preVers = preVers;
    }

    public short getPreVers() {
        return this.preVers;
    }

    public void setDtScadCop(short dtScadCop) {
        this.dtScadCop = dtScadCop;
    }

    public short getDtScadCop() {
        return this.dtScadCop;
    }

    public void setGgDelMmScadRat(short ggDelMmScadRat) {
        this.ggDelMmScadRat = ggDelMmScadRat;
    }

    public short getGgDelMmScadRat() {
        return this.ggDelMmScadRat;
    }

    public void setDtScad1aRat(short dtScad1aRat) {
        this.dtScad1aRat = dtScad1aRat;
    }

    public short getDtScad1aRat() {
        return this.dtScad1aRat;
    }

    public void setDtErogFinanz(short dtErogFinanz) {
        this.dtErogFinanz = dtErogFinanz;
    }

    public short getDtErogFinanz() {
        return this.dtErogFinanz;
    }

    public void setDtStipulaFinanz(short dtStipulaFinanz) {
        this.dtStipulaFinanz = dtStipulaFinanz;
    }

    public short getDtStipulaFinanz() {
        return this.dtStipulaFinanz;
    }

    public void setMmPreamm(short mmPreamm) {
        this.mmPreamm = mmPreamm;
    }

    public short getMmPreamm() {
        return this.mmPreamm;
    }

    public void setImpDebRes(short impDebRes) {
        this.impDebRes = impDebRes;
    }

    public short getImpDebRes() {
        return this.impDebRes;
    }

    public void setImpRatFinanz(short impRatFinanz) {
        this.impRatFinanz = impRatFinanz;
    }

    public short getImpRatFinanz() {
        return this.impRatFinanz;
    }

    public void setImpCanoneAntic(short impCanoneAntic) {
        this.impCanoneAntic = impCanoneAntic;
    }

    public short getImpCanoneAntic() {
        return this.impCanoneAntic;
    }

    public void setPerRatFinanz(short perRatFinanz) {
        this.perRatFinanz = perRatFinanz;
    }

    public short getPerRatFinanz() {
        return this.perRatFinanz;
    }

    public void setTpModPagRat(short tpModPagRat) {
        this.tpModPagRat = tpModPagRat;
    }

    public short getTpModPagRat() {
        return this.tpModPagRat;
    }

    public void setTpFinanzEr(short tpFinanzEr) {
        this.tpFinanzEr = tpFinanzEr;
    }

    public short getTpFinanzEr() {
        return this.tpFinanzEr;
    }

    public void setCatFinanzEr(short catFinanzEr) {
        this.catFinanzEr = catFinanzEr;
    }

    public short getCatFinanzEr() {
        return this.catFinanzEr;
    }

    public void setValRiscEndLeas(short valRiscEndLeas) {
        this.valRiscEndLeas = valRiscEndLeas;
    }

    public short getValRiscEndLeas() {
        return this.valRiscEndLeas;
    }

    public void setDtEstFinanz(short dtEstFinanz) {
        this.dtEstFinanz = dtEstFinanz;
    }

    public short getDtEstFinanz() {
        return this.dtEstFinanz;
    }

    public void setDtManCop(short dtManCop) {
        this.dtManCop = dtManCop;
    }

    public short getDtManCop() {
        return this.dtManCop;
    }

    public void setNumFinanz(short numFinanz) {
        this.numFinanz = numFinanz;
    }

    public short getNumFinanz() {
        return this.numFinanz;
    }

    public void setTpModAcqs(short tpModAcqs) {
        this.tpModAcqs = tpModAcqs;
    }

    public short getTpModAcqs() {
        return this.tpModAcqs;
    }
}
