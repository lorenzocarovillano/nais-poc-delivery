package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: D-ATT-SERV-VAL-DB<br>
 * Variable: D-ATT-SERV-VAL-DB from copybook IDBVP893<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DAttServValDb {

    //==== PROPERTIES ====
    //Original name: P89-DT-INI-EFF-DB
    private String iniEffDb = DefaultValues.stringVal(Len.INI_EFF_DB);
    //Original name: P89-DT-END-EFF-DB
    private String endEffDb = DefaultValues.stringVal(Len.END_EFF_DB);
    //Original name: P89-DT-INI-CNTRL-FND-DB
    private String iniCntrlFndDb = DefaultValues.stringVal(Len.INI_CNTRL_FND_DB);

    //==== METHODS ====
    public void setIniEffDb(String iniEffDb) {
        this.iniEffDb = Functions.subString(iniEffDb, Len.INI_EFF_DB);
    }

    public String getIniEffDb() {
        return this.iniEffDb;
    }

    public void setEndEffDb(String endEffDb) {
        this.endEffDb = Functions.subString(endEffDb, Len.END_EFF_DB);
    }

    public String getEndEffDb() {
        return this.endEffDb;
    }

    public void setIniCntrlFndDb(String iniCntrlFndDb) {
        this.iniCntrlFndDb = Functions.subString(iniCntrlFndDb, Len.INI_CNTRL_FND_DB);
    }

    public String getIniCntrlFndDb() {
        return this.iniCntrlFndDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INI_EFF_DB = 10;
        public static final int END_EFF_DB = 10;
        public static final int INI_CNTRL_FND_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
