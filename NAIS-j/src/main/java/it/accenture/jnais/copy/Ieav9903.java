package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.WsFlagRicDerogaErr;

/**Original name: IEAV9903<br>
 * Variable: IEAV9903 from copybook IEAV9903<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ieav9903 {

    //==== PROPERTIES ====
    //Original name: ISPC0001-AREA-ERRORI
    private Ispc0001AreaErrori ispc0001AreaErrori = new Ispc0001AreaErrori();
    //Original name: ISPC0001-STEP-ELAB
    private char ispc0001StepElab = DefaultValues.CHAR_VAL;
    //Original name: WS-FLAG-RIC-DEROGA-ERR
    private WsFlagRicDerogaErr wsFlagRicDerogaErr = new WsFlagRicDerogaErr();
    //Original name: IX-ERR-GENERIC
    private int ixErrGeneric = 0;
    //Original name: IX-ERR-DEROG
    private int ixErrDerog = 0;

    //==== METHODS ====
    public void setIspc0001StepElab(char ispc0001StepElab) {
        this.ispc0001StepElab = ispc0001StepElab;
    }

    public void setIspc0001StepElabFormatted(String ispc0001StepElab) {
        setIspc0001StepElab(Functions.charAt(ispc0001StepElab, Types.CHAR_SIZE));
    }

    public char getIspc0001StepElab() {
        return this.ispc0001StepElab;
    }

    public void setIxErrGeneric(int ixErrGeneric) {
        this.ixErrGeneric = ixErrGeneric;
    }

    public int getIxErrGeneric() {
        return this.ixErrGeneric;
    }

    public void setIxErrDerog(int ixErrDerog) {
        this.ixErrDerog = ixErrDerog;
    }

    public int getIxErrDerog() {
        return this.ixErrDerog;
    }

    public Ispc0001AreaErrori getIspc0001AreaErrori() {
        return ispc0001AreaErrori;
    }

    public WsFlagRicDerogaErr getWsFlagRicDerogaErr() {
        return wsFlagRicDerogaErr;
    }
}
