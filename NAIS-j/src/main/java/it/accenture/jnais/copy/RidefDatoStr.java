package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.commons.data.to.IRidefDatoStr;

/**Original name: RIDEF-DATO-STR<br>
 * Variable: RIDEF-DATO-STR from copybook IDBVRDS1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class RidefDatoStr implements IRidefDatoStr {

    //==== PROPERTIES ====
    //Original name: RDS-COD-COMPAGNIA-ANIA
    private int codCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: RDS-TIPO-MOVIMENTO
    private int tipoMovimento = DefaultValues.INT_VAL;
    //Original name: RDS-COD-DATO
    private String codDato = DefaultValues.stringVal(Len.COD_DATO);
    //Original name: RDS-COD-STR-DATO
    private String codStrDato = DefaultValues.stringVal(Len.COD_STR_DATO);

    //==== METHODS ====
    public void setCodCompagniaAnia(int codCompagniaAnia) {
        this.codCompagniaAnia = codCompagniaAnia;
    }

    public int getCodCompagniaAnia() {
        return this.codCompagniaAnia;
    }

    public void setTipoMovimento(int tipoMovimento) {
        this.tipoMovimento = tipoMovimento;
    }

    public int getTipoMovimento() {
        return this.tipoMovimento;
    }

    public void setCodDato(String codDato) {
        this.codDato = Functions.subString(codDato, Len.COD_DATO);
    }

    public String getCodDato() {
        return this.codDato;
    }

    public String getCodStrDato() {
        return this.codStrDato;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_DATO = 30;
        public static final int COD_STR_DATO = 30;
        public static final int DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
