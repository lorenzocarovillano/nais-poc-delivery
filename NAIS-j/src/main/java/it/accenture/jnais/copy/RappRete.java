package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.RreCodAcqsCntrt;
import it.accenture.jnais.ws.redefines.RreCodCan;
import it.accenture.jnais.ws.redefines.RreCodPntReteEnd;
import it.accenture.jnais.ws.redefines.RreCodPntReteIni;
import it.accenture.jnais.ws.redefines.RreCodPntReteIniC;
import it.accenture.jnais.ws.redefines.RreIdMoviChiu;
import it.accenture.jnais.ws.redefines.RreIdOgg;
import it.accenture.jnais.ws.redefines.RreTpAcqsCntrt;

/**Original name: RAPP-RETE<br>
 * Variable: RAPP-RETE from copybook IDBVRRE1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class RappRete {

    //==== PROPERTIES ====
    //Original name: RRE-ID-RAPP-RETE
    private int rreIdRappRete = DefaultValues.INT_VAL;
    //Original name: RRE-ID-OGG
    private RreIdOgg rreIdOgg = new RreIdOgg();
    //Original name: RRE-TP-OGG
    private String rreTpOgg = DefaultValues.stringVal(Len.RRE_TP_OGG);
    //Original name: RRE-ID-MOVI-CRZ
    private int rreIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: RRE-ID-MOVI-CHIU
    private RreIdMoviChiu rreIdMoviChiu = new RreIdMoviChiu();
    //Original name: RRE-DT-INI-EFF
    private int rreDtIniEff = DefaultValues.INT_VAL;
    //Original name: RRE-DT-END-EFF
    private int rreDtEndEff = DefaultValues.INT_VAL;
    //Original name: RRE-COD-COMP-ANIA
    private int rreCodCompAnia = DefaultValues.INT_VAL;
    //Original name: RRE-TP-RETE
    private String rreTpRete = DefaultValues.stringVal(Len.RRE_TP_RETE);
    //Original name: RRE-TP-ACQS-CNTRT
    private RreTpAcqsCntrt rreTpAcqsCntrt = new RreTpAcqsCntrt();
    //Original name: RRE-COD-ACQS-CNTRT
    private RreCodAcqsCntrt rreCodAcqsCntrt = new RreCodAcqsCntrt();
    //Original name: RRE-COD-PNT-RETE-INI
    private RreCodPntReteIni rreCodPntReteIni = new RreCodPntReteIni();
    //Original name: RRE-COD-PNT-RETE-END
    private RreCodPntReteEnd rreCodPntReteEnd = new RreCodPntReteEnd();
    //Original name: RRE-FL-PNT-RETE-1RIO
    private char rreFlPntRete1rio = DefaultValues.CHAR_VAL;
    //Original name: RRE-COD-CAN
    private RreCodCan rreCodCan = new RreCodCan();
    //Original name: RRE-DS-RIGA
    private long rreDsRiga = DefaultValues.LONG_VAL;
    //Original name: RRE-DS-OPER-SQL
    private char rreDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: RRE-DS-VER
    private int rreDsVer = DefaultValues.INT_VAL;
    //Original name: RRE-DS-TS-INI-CPTZ
    private long rreDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: RRE-DS-TS-END-CPTZ
    private long rreDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: RRE-DS-UTENTE
    private String rreDsUtente = DefaultValues.stringVal(Len.RRE_DS_UTENTE);
    //Original name: RRE-DS-STATO-ELAB
    private char rreDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: RRE-COD-PNT-RETE-INI-C
    private RreCodPntReteIniC rreCodPntReteIniC = new RreCodPntReteIniC();
    //Original name: RRE-MATR-OPRT
    private String rreMatrOprt = DefaultValues.stringVal(Len.RRE_MATR_OPRT);

    //==== METHODS ====
    public void setRappReteFormatted(String data) {
        byte[] buffer = new byte[Len.RAPP_RETE];
        MarshalByte.writeString(buffer, 1, data, Len.RAPP_RETE);
        setRappReteBytes(buffer, 1);
    }

    public String getRappReteFormatted() {
        return MarshalByteExt.bufferToStr(getRappReteBytes());
    }

    public byte[] getRappReteBytes() {
        byte[] buffer = new byte[Len.RAPP_RETE];
        return getRappReteBytes(buffer, 1);
    }

    public void setRappReteBytes(byte[] buffer, int offset) {
        int position = offset;
        rreIdRappRete = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RRE_ID_RAPP_RETE, 0);
        position += Len.RRE_ID_RAPP_RETE;
        rreIdOgg.setRreIdOggFromBuffer(buffer, position);
        position += RreIdOgg.Len.RRE_ID_OGG;
        rreTpOgg = MarshalByte.readString(buffer, position, Len.RRE_TP_OGG);
        position += Len.RRE_TP_OGG;
        rreIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RRE_ID_MOVI_CRZ, 0);
        position += Len.RRE_ID_MOVI_CRZ;
        rreIdMoviChiu.setRreIdMoviChiuFromBuffer(buffer, position);
        position += RreIdMoviChiu.Len.RRE_ID_MOVI_CHIU;
        rreDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RRE_DT_INI_EFF, 0);
        position += Len.RRE_DT_INI_EFF;
        rreDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RRE_DT_END_EFF, 0);
        position += Len.RRE_DT_END_EFF;
        rreCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RRE_COD_COMP_ANIA, 0);
        position += Len.RRE_COD_COMP_ANIA;
        rreTpRete = MarshalByte.readString(buffer, position, Len.RRE_TP_RETE);
        position += Len.RRE_TP_RETE;
        rreTpAcqsCntrt.setRreTpAcqsCntrtFromBuffer(buffer, position);
        position += RreTpAcqsCntrt.Len.RRE_TP_ACQS_CNTRT;
        rreCodAcqsCntrt.setRreCodAcqsCntrtFromBuffer(buffer, position);
        position += RreCodAcqsCntrt.Len.RRE_COD_ACQS_CNTRT;
        rreCodPntReteIni.setRreCodPntReteIniFromBuffer(buffer, position);
        position += RreCodPntReteIni.Len.RRE_COD_PNT_RETE_INI;
        rreCodPntReteEnd.setRreCodPntReteEndFromBuffer(buffer, position);
        position += RreCodPntReteEnd.Len.RRE_COD_PNT_RETE_END;
        rreFlPntRete1rio = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rreCodCan.setRreCodCanFromBuffer(buffer, position);
        position += RreCodCan.Len.RRE_COD_CAN;
        rreDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RRE_DS_RIGA, 0);
        position += Len.RRE_DS_RIGA;
        rreDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rreDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RRE_DS_VER, 0);
        position += Len.RRE_DS_VER;
        rreDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RRE_DS_TS_INI_CPTZ, 0);
        position += Len.RRE_DS_TS_INI_CPTZ;
        rreDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RRE_DS_TS_END_CPTZ, 0);
        position += Len.RRE_DS_TS_END_CPTZ;
        rreDsUtente = MarshalByte.readString(buffer, position, Len.RRE_DS_UTENTE);
        position += Len.RRE_DS_UTENTE;
        rreDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rreCodPntReteIniC.setRreCodPntReteIniCFromBuffer(buffer, position);
        position += RreCodPntReteIniC.Len.RRE_COD_PNT_RETE_INI_C;
        rreMatrOprt = MarshalByte.readString(buffer, position, Len.RRE_MATR_OPRT);
    }

    public byte[] getRappReteBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, rreIdRappRete, Len.Int.RRE_ID_RAPP_RETE, 0);
        position += Len.RRE_ID_RAPP_RETE;
        rreIdOgg.getRreIdOggAsBuffer(buffer, position);
        position += RreIdOgg.Len.RRE_ID_OGG;
        MarshalByte.writeString(buffer, position, rreTpOgg, Len.RRE_TP_OGG);
        position += Len.RRE_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, rreIdMoviCrz, Len.Int.RRE_ID_MOVI_CRZ, 0);
        position += Len.RRE_ID_MOVI_CRZ;
        rreIdMoviChiu.getRreIdMoviChiuAsBuffer(buffer, position);
        position += RreIdMoviChiu.Len.RRE_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, rreDtIniEff, Len.Int.RRE_DT_INI_EFF, 0);
        position += Len.RRE_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, rreDtEndEff, Len.Int.RRE_DT_END_EFF, 0);
        position += Len.RRE_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, rreCodCompAnia, Len.Int.RRE_COD_COMP_ANIA, 0);
        position += Len.RRE_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, rreTpRete, Len.RRE_TP_RETE);
        position += Len.RRE_TP_RETE;
        rreTpAcqsCntrt.getRreTpAcqsCntrtAsBuffer(buffer, position);
        position += RreTpAcqsCntrt.Len.RRE_TP_ACQS_CNTRT;
        rreCodAcqsCntrt.getRreCodAcqsCntrtAsBuffer(buffer, position);
        position += RreCodAcqsCntrt.Len.RRE_COD_ACQS_CNTRT;
        rreCodPntReteIni.getRreCodPntReteIniAsBuffer(buffer, position);
        position += RreCodPntReteIni.Len.RRE_COD_PNT_RETE_INI;
        rreCodPntReteEnd.getRreCodPntReteEndAsBuffer(buffer, position);
        position += RreCodPntReteEnd.Len.RRE_COD_PNT_RETE_END;
        MarshalByte.writeChar(buffer, position, rreFlPntRete1rio);
        position += Types.CHAR_SIZE;
        rreCodCan.getRreCodCanAsBuffer(buffer, position);
        position += RreCodCan.Len.RRE_COD_CAN;
        MarshalByte.writeLongAsPacked(buffer, position, rreDsRiga, Len.Int.RRE_DS_RIGA, 0);
        position += Len.RRE_DS_RIGA;
        MarshalByte.writeChar(buffer, position, rreDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, rreDsVer, Len.Int.RRE_DS_VER, 0);
        position += Len.RRE_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, rreDsTsIniCptz, Len.Int.RRE_DS_TS_INI_CPTZ, 0);
        position += Len.RRE_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, rreDsTsEndCptz, Len.Int.RRE_DS_TS_END_CPTZ, 0);
        position += Len.RRE_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, rreDsUtente, Len.RRE_DS_UTENTE);
        position += Len.RRE_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, rreDsStatoElab);
        position += Types.CHAR_SIZE;
        rreCodPntReteIniC.getRreCodPntReteIniCAsBuffer(buffer, position);
        position += RreCodPntReteIniC.Len.RRE_COD_PNT_RETE_INI_C;
        MarshalByte.writeString(buffer, position, rreMatrOprt, Len.RRE_MATR_OPRT);
        return buffer;
    }

    public void setRreIdRappRete(int rreIdRappRete) {
        this.rreIdRappRete = rreIdRappRete;
    }

    public int getRreIdRappRete() {
        return this.rreIdRappRete;
    }

    public void setRreTpOgg(String rreTpOgg) {
        this.rreTpOgg = Functions.subString(rreTpOgg, Len.RRE_TP_OGG);
    }

    public String getRreTpOgg() {
        return this.rreTpOgg;
    }

    public void setRreIdMoviCrz(int rreIdMoviCrz) {
        this.rreIdMoviCrz = rreIdMoviCrz;
    }

    public int getRreIdMoviCrz() {
        return this.rreIdMoviCrz;
    }

    public void setRreDtIniEff(int rreDtIniEff) {
        this.rreDtIniEff = rreDtIniEff;
    }

    public int getRreDtIniEff() {
        return this.rreDtIniEff;
    }

    public void setRreDtEndEff(int rreDtEndEff) {
        this.rreDtEndEff = rreDtEndEff;
    }

    public int getRreDtEndEff() {
        return this.rreDtEndEff;
    }

    public void setRreCodCompAnia(int rreCodCompAnia) {
        this.rreCodCompAnia = rreCodCompAnia;
    }

    public int getRreCodCompAnia() {
        return this.rreCodCompAnia;
    }

    public void setRreTpRete(String rreTpRete) {
        this.rreTpRete = Functions.subString(rreTpRete, Len.RRE_TP_RETE);
    }

    public String getRreTpRete() {
        return this.rreTpRete;
    }

    public void setRreFlPntRete1rio(char rreFlPntRete1rio) {
        this.rreFlPntRete1rio = rreFlPntRete1rio;
    }

    public char getRreFlPntRete1rio() {
        return this.rreFlPntRete1rio;
    }

    public void setRreDsRiga(long rreDsRiga) {
        this.rreDsRiga = rreDsRiga;
    }

    public long getRreDsRiga() {
        return this.rreDsRiga;
    }

    public void setRreDsOperSql(char rreDsOperSql) {
        this.rreDsOperSql = rreDsOperSql;
    }

    public char getRreDsOperSql() {
        return this.rreDsOperSql;
    }

    public void setRreDsVer(int rreDsVer) {
        this.rreDsVer = rreDsVer;
    }

    public int getRreDsVer() {
        return this.rreDsVer;
    }

    public void setRreDsTsIniCptz(long rreDsTsIniCptz) {
        this.rreDsTsIniCptz = rreDsTsIniCptz;
    }

    public long getRreDsTsIniCptz() {
        return this.rreDsTsIniCptz;
    }

    public void setRreDsTsEndCptz(long rreDsTsEndCptz) {
        this.rreDsTsEndCptz = rreDsTsEndCptz;
    }

    public long getRreDsTsEndCptz() {
        return this.rreDsTsEndCptz;
    }

    public void setRreDsUtente(String rreDsUtente) {
        this.rreDsUtente = Functions.subString(rreDsUtente, Len.RRE_DS_UTENTE);
    }

    public String getRreDsUtente() {
        return this.rreDsUtente;
    }

    public void setRreDsStatoElab(char rreDsStatoElab) {
        this.rreDsStatoElab = rreDsStatoElab;
    }

    public char getRreDsStatoElab() {
        return this.rreDsStatoElab;
    }

    public void setRreMatrOprt(String rreMatrOprt) {
        this.rreMatrOprt = Functions.subString(rreMatrOprt, Len.RRE_MATR_OPRT);
    }

    public String getRreMatrOprt() {
        return this.rreMatrOprt;
    }

    public String getRreMatrOprtFormatted() {
        return Functions.padBlanks(getRreMatrOprt(), Len.RRE_MATR_OPRT);
    }

    public RreCodAcqsCntrt getRreCodAcqsCntrt() {
        return rreCodAcqsCntrt;
    }

    public RreCodCan getRreCodCan() {
        return rreCodCan;
    }

    public RreCodPntReteEnd getRreCodPntReteEnd() {
        return rreCodPntReteEnd;
    }

    public RreCodPntReteIni getRreCodPntReteIni() {
        return rreCodPntReteIni;
    }

    public RreCodPntReteIniC getRreCodPntReteIniC() {
        return rreCodPntReteIniC;
    }

    public RreIdMoviChiu getRreIdMoviChiu() {
        return rreIdMoviChiu;
    }

    public RreIdOgg getRreIdOgg() {
        return rreIdOgg;
    }

    public RreTpAcqsCntrt getRreTpAcqsCntrt() {
        return rreTpAcqsCntrt;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RRE_TP_OGG = 2;
        public static final int RRE_TP_RETE = 2;
        public static final int RRE_DS_UTENTE = 20;
        public static final int RRE_MATR_OPRT = 20;
        public static final int RRE_ID_RAPP_RETE = 5;
        public static final int RRE_ID_MOVI_CRZ = 5;
        public static final int RRE_DT_INI_EFF = 5;
        public static final int RRE_DT_END_EFF = 5;
        public static final int RRE_COD_COMP_ANIA = 3;
        public static final int RRE_FL_PNT_RETE1RIO = 1;
        public static final int RRE_DS_RIGA = 6;
        public static final int RRE_DS_OPER_SQL = 1;
        public static final int RRE_DS_VER = 5;
        public static final int RRE_DS_TS_INI_CPTZ = 10;
        public static final int RRE_DS_TS_END_CPTZ = 10;
        public static final int RRE_DS_STATO_ELAB = 1;
        public static final int RAPP_RETE = RRE_ID_RAPP_RETE + RreIdOgg.Len.RRE_ID_OGG + RRE_TP_OGG + RRE_ID_MOVI_CRZ + RreIdMoviChiu.Len.RRE_ID_MOVI_CHIU + RRE_DT_INI_EFF + RRE_DT_END_EFF + RRE_COD_COMP_ANIA + RRE_TP_RETE + RreTpAcqsCntrt.Len.RRE_TP_ACQS_CNTRT + RreCodAcqsCntrt.Len.RRE_COD_ACQS_CNTRT + RreCodPntReteIni.Len.RRE_COD_PNT_RETE_INI + RreCodPntReteEnd.Len.RRE_COD_PNT_RETE_END + RRE_FL_PNT_RETE1RIO + RreCodCan.Len.RRE_COD_CAN + RRE_DS_RIGA + RRE_DS_OPER_SQL + RRE_DS_VER + RRE_DS_TS_INI_CPTZ + RRE_DS_TS_END_CPTZ + RRE_DS_UTENTE + RRE_DS_STATO_ELAB + RreCodPntReteIniC.Len.RRE_COD_PNT_RETE_INI_C + RRE_MATR_OPRT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RRE_ID_RAPP_RETE = 9;
            public static final int RRE_ID_MOVI_CRZ = 9;
            public static final int RRE_DT_INI_EFF = 8;
            public static final int RRE_DT_END_EFF = 8;
            public static final int RRE_COD_COMP_ANIA = 5;
            public static final int RRE_DS_RIGA = 10;
            public static final int RRE_DS_VER = 9;
            public static final int RRE_DS_TS_INI_CPTZ = 18;
            public static final int RRE_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
