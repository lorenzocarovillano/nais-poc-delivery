package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: AREA-LDBV1131<br>
 * Variable: AREA-LDBV1131 from copybook LDBV1131<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AreaLdbv1131 {

    //==== PROPERTIES ====
    //Original name: LDBV1131-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV1131-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV1131-COD-PARAM
    private String codParam = DefaultValues.stringVal(Len.COD_PARAM);

    //==== METHODS ====
    public void setAreaLdbv1131Formatted(String data) {
        byte[] buffer = new byte[Len.AREA_LDBV1131];
        MarshalByte.writeString(buffer, 1, data, Len.AREA_LDBV1131);
        setAreaLdbv1131Bytes(buffer, 1);
    }

    public String getAreaLdbv1131Formatted() {
        return MarshalByteExt.bufferToStr(getAreaLdbv1131Bytes());
    }

    public byte[] getAreaLdbv1131Bytes() {
        byte[] buffer = new byte[Len.AREA_LDBV1131];
        return getAreaLdbv1131Bytes(buffer, 1);
    }

    public void setAreaLdbv1131Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        codParam = MarshalByte.readString(buffer, position, Len.COD_PARAM);
    }

    public byte[] getAreaLdbv1131Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, codParam, Len.COD_PARAM);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setCodParam(String codParam) {
        this.codParam = Functions.subString(codParam, Len.COD_PARAM);
    }

    public String getCodParam() {
        return this.codParam;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int COD_PARAM = 20;
        public static final int ID_OGG = 5;
        public static final int AREA_LDBV1131 = ID_OGG + TP_OGG + COD_PARAM;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
