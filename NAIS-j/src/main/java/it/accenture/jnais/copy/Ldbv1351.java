package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Ldbv1351LivelloOperazioni;
import it.accenture.jnais.ws.redefines.Ldbv1351StatBusTab;
import it.accenture.jnais.ws.redefines.Ldbv1351TpCausTab;

/**Original name: LDBV1351<br>
 * Variable: LDBV1351 from copybook LDBV1351<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv1351 {

    //==== PROPERTIES ====
    //Original name: LDBV1351-LIVELLO-OPERAZIONI
    private Ldbv1351LivelloOperazioni ldbv1351LivelloOperazioni = new Ldbv1351LivelloOperazioni();
    //Original name: LDBV1351-OPER-LOG-STAT-BUS
    private String ldbv1351OperLogStatBus = DefaultValues.stringVal(Len.LDBV1351_OPER_LOG_STAT_BUS);
    //Original name: LDBV1351-STAT-BUS-TAB
    private Ldbv1351StatBusTab ldbv1351StatBusTab = new Ldbv1351StatBusTab();
    //Original name: LDBV1351-OPER-LOG-CAUS
    private String ldbv1351OperLogCaus = DefaultValues.stringVal(Len.LDBV1351_OPER_LOG_CAUS);
    //Original name: LDBV1351-TP-CAUS-TAB
    private Ldbv1351TpCausTab ldbv1351TpCausTab = new Ldbv1351TpCausTab();

    //==== METHODS ====
    public void setLdbv1351Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV1351];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV1351);
        setLdbv1351Bytes(buffer, 1);
    }

    public String getLdbv1351Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv1351Bytes());
    }

    public byte[] getLdbv1351Bytes() {
        byte[] buffer = new byte[Len.LDBV1351];
        return getLdbv1351Bytes(buffer, 1);
    }

    public void setLdbv1351Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv1351LivelloOperazioni.setLdbv1351LivelloOperazioni(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        ldbv1351OperLogStatBus = MarshalByte.readString(buffer, position, Len.LDBV1351_OPER_LOG_STAT_BUS);
        position += Len.LDBV1351_OPER_LOG_STAT_BUS;
        ldbv1351StatBusTab.setLdbv1351StatBusTabBytes(buffer, position);
        position += Ldbv1351StatBusTab.Len.LDBV1351_STAT_BUS_TAB;
        ldbv1351OperLogCaus = MarshalByte.readString(buffer, position, Len.LDBV1351_OPER_LOG_CAUS);
        position += Len.LDBV1351_OPER_LOG_CAUS;
        ldbv1351TpCausTab.setLdbv1351TpCausTabBytes(buffer, position);
    }

    public byte[] getLdbv1351Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, ldbv1351LivelloOperazioni.getLdbv1351LivelloOperazioni());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ldbv1351OperLogStatBus, Len.LDBV1351_OPER_LOG_STAT_BUS);
        position += Len.LDBV1351_OPER_LOG_STAT_BUS;
        ldbv1351StatBusTab.getLdbv1351StatBusTabBytes(buffer, position);
        position += Ldbv1351StatBusTab.Len.LDBV1351_STAT_BUS_TAB;
        MarshalByte.writeString(buffer, position, ldbv1351OperLogCaus, Len.LDBV1351_OPER_LOG_CAUS);
        position += Len.LDBV1351_OPER_LOG_CAUS;
        ldbv1351TpCausTab.getLdbv1351TpCausTabBytes(buffer, position);
        return buffer;
    }

    public void setLdbv1351OperLogStatBus(String ldbv1351OperLogStatBus) {
        this.ldbv1351OperLogStatBus = Functions.subString(ldbv1351OperLogStatBus, Len.LDBV1351_OPER_LOG_STAT_BUS);
    }

    public String getLdbv1351OperLogStatBus() {
        return this.ldbv1351OperLogStatBus;
    }

    public void setLdbv1351OperLogCaus(String ldbv1351OperLogCaus) {
        this.ldbv1351OperLogCaus = Functions.subString(ldbv1351OperLogCaus, Len.LDBV1351_OPER_LOG_CAUS);
    }

    public String getLdbv1351OperLogCaus() {
        return this.ldbv1351OperLogCaus;
    }

    public Ldbv1351LivelloOperazioni getLdbv1351LivelloOperazioni() {
        return ldbv1351LivelloOperazioni;
    }

    public Ldbv1351StatBusTab getLdbv1351StatBusTab() {
        return ldbv1351StatBusTab;
    }

    public Ldbv1351TpCausTab getLdbv1351TpCausTab() {
        return ldbv1351TpCausTab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV1351_OPER_LOG_STAT_BUS = 3;
        public static final int LDBV1351_OPER_LOG_CAUS = 3;
        public static final int LDBV1351 = Ldbv1351LivelloOperazioni.Len.LDBV1351_LIVELLO_OPERAZIONI + LDBV1351_OPER_LOG_STAT_BUS + Ldbv1351StatBusTab.Len.LDBV1351_STAT_BUS_TAB + LDBV1351_OPER_LOG_CAUS + Ldbv1351TpCausTab.Len.LDBV1351_TP_CAUS_TAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
