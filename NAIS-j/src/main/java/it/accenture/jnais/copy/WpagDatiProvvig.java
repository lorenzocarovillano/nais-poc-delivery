package it.accenture.jnais.copy;

import it.accenture.jnais.ws.redefines.WpagAliqAcq;
import it.accenture.jnais.ws.redefines.WpagAliqCommisInt;
import it.accenture.jnais.ws.redefines.WpagAliqIncas;
import it.accenture.jnais.ws.redefines.WpagAliqRenAss;
import it.accenture.jnais.ws.redefines.WpagAliqRicor;
import it.accenture.jnais.ws.redefines.WpagImpAcq;
import it.accenture.jnais.ws.redefines.WpagImpCommisInt;
import it.accenture.jnais.ws.redefines.WpagImpIncas;
import it.accenture.jnais.ws.redefines.WpagImpRenAss;
import it.accenture.jnais.ws.redefines.WpagImpRicor;
import it.accenture.jnais.ws.redefines.WpagPrvAcq1oAnno;
import it.accenture.jnais.ws.redefines.WpagPrvAcq2oAnno;
import it.accenture.jnais.ws.redefines.WpagPrvCommisInt;
import it.accenture.jnais.ws.redefines.WpagPrvIncas;
import it.accenture.jnais.ws.redefines.WpagPrvRenAss;
import it.accenture.jnais.ws.redefines.WpagPrvRicor;

/**Original name: WPAG-DATI-PROVVIG<br>
 * Variable: WPAG-DATI-PROVVIG from copybook LVEC0268<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WpagDatiProvvig {

    //==== PROPERTIES ====
    //Original name: WPAG-PRV-ACQ-1O-ANNO
    private WpagPrvAcq1oAnno wpagPrvAcq1oAnno = new WpagPrvAcq1oAnno();
    //Original name: WPAG-PRV-ACQ-2O-ANNO
    private WpagPrvAcq2oAnno wpagPrvAcq2oAnno = new WpagPrvAcq2oAnno();
    //Original name: WPAG-PRV-RICOR
    private WpagPrvRicor wpagPrvRicor = new WpagPrvRicor();
    //Original name: WPAG-PRV-INCAS
    private WpagPrvIncas wpagPrvIncas = new WpagPrvIncas();
    //Original name: WPAG-PRV-REN-ASS
    private WpagPrvRenAss wpagPrvRenAss = new WpagPrvRenAss();
    //Original name: WPAG-PRV-COMMIS-INT
    private WpagPrvCommisInt wpagPrvCommisInt = new WpagPrvCommisInt();
    //Original name: WPAG-ALIQ-INCAS
    private WpagAliqIncas wpagAliqIncas = new WpagAliqIncas();
    //Original name: WPAG-ALIQ-ACQ
    private WpagAliqAcq wpagAliqAcq = new WpagAliqAcq();
    //Original name: WPAG-ALIQ-RICOR
    private WpagAliqRicor wpagAliqRicor = new WpagAliqRicor();
    //Original name: WPAG-ALIQ-REN-ASS
    private WpagAliqRenAss wpagAliqRenAss = new WpagAliqRenAss();
    //Original name: WPAG-ALIQ-COMMIS-INT
    private WpagAliqCommisInt wpagAliqCommisInt = new WpagAliqCommisInt();
    //Original name: WPAG-IMP-INCAS
    private WpagImpIncas wpagImpIncas = new WpagImpIncas();
    //Original name: WPAG-IMP-ACQ
    private WpagImpAcq wpagImpAcq = new WpagImpAcq();
    //Original name: WPAG-IMP-RICOR
    private WpagImpRicor wpagImpRicor = new WpagImpRicor();
    //Original name: WPAG-IMP-REN-ASS
    private WpagImpRenAss wpagImpRenAss = new WpagImpRenAss();
    //Original name: WPAG-IMP-COMMIS-INT
    private WpagImpCommisInt wpagImpCommisInt = new WpagImpCommisInt();

    //==== METHODS ====
    public void setWpagDatiProvvigBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagPrvAcq1oAnno.setWpagPrvAcq1oAnnoFromBuffer(buffer, position);
        position += WpagPrvAcq1oAnno.Len.WPAG_PRV_ACQ1O_ANNO;
        wpagPrvAcq2oAnno.setWpagPrvAcq2oAnnoFromBuffer(buffer, position);
        position += WpagPrvAcq2oAnno.Len.WPAG_PRV_ACQ2O_ANNO;
        wpagPrvRicor.setWpagPrvRicorFromBuffer(buffer, position);
        position += WpagPrvRicor.Len.WPAG_PRV_RICOR;
        wpagPrvIncas.setWpagPrvIncasFromBuffer(buffer, position);
        position += WpagPrvIncas.Len.WPAG_PRV_INCAS;
        wpagPrvRenAss.setWpagPrvRenAssFromBuffer(buffer, position);
        position += WpagPrvRenAss.Len.WPAG_PRV_REN_ASS;
        wpagPrvCommisInt.setWpagPrvCommisIntFromBuffer(buffer, position);
        position += WpagPrvCommisInt.Len.WPAG_PRV_COMMIS_INT;
        wpagAliqIncas.setWpagAliqIncasFromBuffer(buffer, position);
        position += WpagAliqIncas.Len.WPAG_ALIQ_INCAS;
        wpagAliqAcq.setWpagAliqAcqFromBuffer(buffer, position);
        position += WpagAliqAcq.Len.WPAG_ALIQ_ACQ;
        wpagAliqRicor.setWpagAliqRicorFromBuffer(buffer, position);
        position += WpagAliqRicor.Len.WPAG_ALIQ_RICOR;
        wpagAliqRenAss.setWpagAliqRenAssFromBuffer(buffer, position);
        position += WpagAliqRenAss.Len.WPAG_ALIQ_REN_ASS;
        wpagAliqCommisInt.setWpagAliqCommisIntFromBuffer(buffer, position);
        position += WpagAliqCommisInt.Len.WPAG_ALIQ_COMMIS_INT;
        wpagImpIncas.setWpagImpIncasFromBuffer(buffer, position);
        position += WpagImpIncas.Len.WPAG_IMP_INCAS;
        wpagImpAcq.setWpagImpAcqFromBuffer(buffer, position);
        position += WpagImpAcq.Len.WPAG_IMP_ACQ;
        wpagImpRicor.setWpagImpRicorFromBuffer(buffer, position);
        position += WpagImpRicor.Len.WPAG_IMP_RICOR;
        wpagImpRenAss.setWpagImpRenAssFromBuffer(buffer, position);
        position += WpagImpRenAss.Len.WPAG_IMP_REN_ASS;
        wpagImpCommisInt.setWpagImpCommisIntFromBuffer(buffer, position);
    }

    public byte[] getWpagDatiProvvigBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagPrvAcq1oAnno.getWpagPrvAcq1oAnnoAsBuffer(buffer, position);
        position += WpagPrvAcq1oAnno.Len.WPAG_PRV_ACQ1O_ANNO;
        wpagPrvAcq2oAnno.getWpagPrvAcq2oAnnoAsBuffer(buffer, position);
        position += WpagPrvAcq2oAnno.Len.WPAG_PRV_ACQ2O_ANNO;
        wpagPrvRicor.getWpagPrvRicorAsBuffer(buffer, position);
        position += WpagPrvRicor.Len.WPAG_PRV_RICOR;
        wpagPrvIncas.getWpagPrvIncasAsBuffer(buffer, position);
        position += WpagPrvIncas.Len.WPAG_PRV_INCAS;
        wpagPrvRenAss.getWpagPrvRenAssAsBuffer(buffer, position);
        position += WpagPrvRenAss.Len.WPAG_PRV_REN_ASS;
        wpagPrvCommisInt.getWpagPrvCommisIntAsBuffer(buffer, position);
        position += WpagPrvCommisInt.Len.WPAG_PRV_COMMIS_INT;
        wpagAliqIncas.getWpagAliqIncasAsBuffer(buffer, position);
        position += WpagAliqIncas.Len.WPAG_ALIQ_INCAS;
        wpagAliqAcq.getWpagAliqAcqAsBuffer(buffer, position);
        position += WpagAliqAcq.Len.WPAG_ALIQ_ACQ;
        wpagAliqRicor.getWpagAliqRicorAsBuffer(buffer, position);
        position += WpagAliqRicor.Len.WPAG_ALIQ_RICOR;
        wpagAliqRenAss.getWpagAliqRenAssAsBuffer(buffer, position);
        position += WpagAliqRenAss.Len.WPAG_ALIQ_REN_ASS;
        wpagAliqCommisInt.getWpagAliqCommisIntAsBuffer(buffer, position);
        position += WpagAliqCommisInt.Len.WPAG_ALIQ_COMMIS_INT;
        wpagImpIncas.getWpagImpIncasAsBuffer(buffer, position);
        position += WpagImpIncas.Len.WPAG_IMP_INCAS;
        wpagImpAcq.getWpagImpAcqAsBuffer(buffer, position);
        position += WpagImpAcq.Len.WPAG_IMP_ACQ;
        wpagImpRicor.getWpagImpRicorAsBuffer(buffer, position);
        position += WpagImpRicor.Len.WPAG_IMP_RICOR;
        wpagImpRenAss.getWpagImpRenAssAsBuffer(buffer, position);
        position += WpagImpRenAss.Len.WPAG_IMP_REN_ASS;
        wpagImpCommisInt.getWpagImpCommisIntAsBuffer(buffer, position);
        return buffer;
    }

    public void initWpagDatiProvvigSpaces() {
        wpagPrvAcq1oAnno.initWpagPrvAcq1oAnnoSpaces();
        wpagPrvAcq2oAnno.initWpagPrvAcq2oAnnoSpaces();
        wpagPrvRicor.initWpagPrvRicorSpaces();
        wpagPrvIncas.initWpagPrvIncasSpaces();
        wpagPrvRenAss.initWpagPrvRenAssSpaces();
        wpagPrvCommisInt.initWpagPrvCommisIntSpaces();
        wpagAliqIncas.initWpagAliqIncasSpaces();
        wpagAliqAcq.initWpagAliqAcqSpaces();
        wpagAliqRicor.initWpagAliqRicorSpaces();
        wpagAliqRenAss.initWpagAliqRenAssSpaces();
        wpagAliqCommisInt.initWpagAliqCommisIntSpaces();
        wpagImpIncas.initWpagImpIncasSpaces();
        wpagImpAcq.initWpagImpAcqSpaces();
        wpagImpRicor.initWpagImpRicorSpaces();
        wpagImpRenAss.initWpagImpRenAssSpaces();
        wpagImpCommisInt.initWpagImpCommisIntSpaces();
    }

    public WpagAliqAcq getWpagAliqAcq() {
        return wpagAliqAcq;
    }

    public WpagAliqCommisInt getWpagAliqCommisInt() {
        return wpagAliqCommisInt;
    }

    public WpagAliqIncas getWpagAliqIncas() {
        return wpagAliqIncas;
    }

    public WpagAliqRenAss getWpagAliqRenAss() {
        return wpagAliqRenAss;
    }

    public WpagAliqRicor getWpagAliqRicor() {
        return wpagAliqRicor;
    }

    public WpagImpAcq getWpagImpAcq() {
        return wpagImpAcq;
    }

    public WpagImpCommisInt getWpagImpCommisInt() {
        return wpagImpCommisInt;
    }

    public WpagImpIncas getWpagImpIncas() {
        return wpagImpIncas;
    }

    public WpagImpRenAss getWpagImpRenAss() {
        return wpagImpRenAss;
    }

    public WpagImpRicor getWpagImpRicor() {
        return wpagImpRicor;
    }

    public WpagPrvAcq1oAnno getWpagPrvAcq1oAnno() {
        return wpagPrvAcq1oAnno;
    }

    public WpagPrvAcq2oAnno getWpagPrvAcq2oAnno() {
        return wpagPrvAcq2oAnno;
    }

    public WpagPrvCommisInt getWpagPrvCommisInt() {
        return wpagPrvCommisInt;
    }

    public WpagPrvIncas getWpagPrvIncas() {
        return wpagPrvIncas;
    }

    public WpagPrvRenAss getWpagPrvRenAss() {
        return wpagPrvRenAss;
    }

    public WpagPrvRicor getWpagPrvRicor() {
        return wpagPrvRicor;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_DATI_PROVVIG = WpagPrvAcq1oAnno.Len.WPAG_PRV_ACQ1O_ANNO + WpagPrvAcq2oAnno.Len.WPAG_PRV_ACQ2O_ANNO + WpagPrvRicor.Len.WPAG_PRV_RICOR + WpagPrvIncas.Len.WPAG_PRV_INCAS + WpagPrvRenAss.Len.WPAG_PRV_REN_ASS + WpagPrvCommisInt.Len.WPAG_PRV_COMMIS_INT + WpagAliqIncas.Len.WPAG_ALIQ_INCAS + WpagAliqAcq.Len.WPAG_ALIQ_ACQ + WpagAliqRicor.Len.WPAG_ALIQ_RICOR + WpagAliqRenAss.Len.WPAG_ALIQ_REN_ASS + WpagAliqCommisInt.Len.WPAG_ALIQ_COMMIS_INT + WpagImpIncas.Len.WPAG_IMP_INCAS + WpagImpAcq.Len.WPAG_IMP_ACQ + WpagImpRicor.Len.WPAG_IMP_RICOR + WpagImpRenAss.Len.WPAG_IMP_REN_ASS + WpagImpCommisInt.Len.WPAG_IMP_COMMIS_INT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
