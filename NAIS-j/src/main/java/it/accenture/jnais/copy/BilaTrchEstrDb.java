package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: BILA-TRCH-ESTR-DB<br>
 * Variable: BILA-TRCH-ESTR-DB from copybook IDBVB033<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class BilaTrchEstrDb {

    //==== PROPERTIES ====
    //Original name: B03-DT-RIS-DB
    private String risDb = DefaultValues.stringVal(Len.RIS_DB);
    //Original name: B03-DT-PRODUZIONE-DB
    private String produzioneDb = DefaultValues.stringVal(Len.PRODUZIONE_DB);
    //Original name: B03-DT-INI-VAL-TAR-DB
    private String iniValTarDb = DefaultValues.stringVal(Len.INI_VAL_TAR_DB);
    //Original name: B03-DT-INI-VLDT-PROD-DB
    private String iniVldtProdDb = DefaultValues.stringVal(Len.INI_VLDT_PROD_DB);
    //Original name: B03-DT-DECOR-POLI-DB
    private String decorPoliDb = DefaultValues.stringVal(Len.DECOR_POLI_DB);
    //Original name: B03-DT-DECOR-ADES-DB
    private String decorAdesDb = DefaultValues.stringVal(Len.DECOR_ADES_DB);
    //Original name: B03-DT-DECOR-TRCH-DB
    private String decorTrchDb = DefaultValues.stringVal(Len.DECOR_TRCH_DB);
    //Original name: B03-DT-EMIS-POLI-DB
    private String emisPoliDb = DefaultValues.stringVal(Len.EMIS_POLI_DB);
    //Original name: B03-DT-EMIS-TRCH-DB
    private String emisTrchDb = DefaultValues.stringVal(Len.EMIS_TRCH_DB);
    //Original name: B03-DT-SCAD-TRCH-DB
    private String scadTrchDb = DefaultValues.stringVal(Len.SCAD_TRCH_DB);
    //Original name: B03-DT-SCAD-INTMD-DB
    private String scadIntmdDb = DefaultValues.stringVal(Len.SCAD_INTMD_DB);
    //Original name: B03-DT-SCAD-PAG-PRE-DB
    private String scadPagPreDb = DefaultValues.stringVal(Len.SCAD_PAG_PRE_DB);
    //Original name: B03-DT-ULT-PRE-PAG-DB
    private String ultPrePagDb = DefaultValues.stringVal(Len.ULT_PRE_PAG_DB);
    //Original name: B03-DT-NASC-1O-ASSTO-DB
    private String nasc1oAsstoDb = DefaultValues.stringVal(Len.NASC1O_ASSTO_DB);
    //Original name: B03-DT-EFF-CAMB-STAT-DB
    private String effCambStatDb = DefaultValues.stringVal(Len.EFF_CAMB_STAT_DB);
    //Original name: B03-DT-EMIS-CAMB-STAT-DB
    private String emisCambStatDb = DefaultValues.stringVal(Len.EMIS_CAMB_STAT_DB);
    //Original name: B03-DT-EFF-STAB-DB
    private String effStabDb = DefaultValues.stringVal(Len.EFF_STAB_DB);
    //Original name: B03-DT-EFF-RIDZ-DB
    private String effRidzDb = DefaultValues.stringVal(Len.EFF_RIDZ_DB);
    //Original name: B03-DT-EMIS-RIDZ-DB
    private String emisRidzDb = DefaultValues.stringVal(Len.EMIS_RIDZ_DB);
    //Original name: B03-DT-ULT-RIVAL-DB
    private String ultRivalDb = DefaultValues.stringVal(Len.ULT_RIVAL_DB);
    //Original name: B03-DT-QTZ-EMIS-DB
    private String qtzEmisDb = DefaultValues.stringVal(Len.QTZ_EMIS_DB);
    //Original name: B03-DT-INC-ULT-PRE-DB
    private String incUltPreDb = DefaultValues.stringVal(Len.INC_ULT_PRE_DB);

    //==== METHODS ====
    public void setRisDb(String risDb) {
        this.risDb = Functions.subString(risDb, Len.RIS_DB);
    }

    public String getRisDb() {
        return this.risDb;
    }

    public void setProduzioneDb(String produzioneDb) {
        this.produzioneDb = Functions.subString(produzioneDb, Len.PRODUZIONE_DB);
    }

    public String getProduzioneDb() {
        return this.produzioneDb;
    }

    public void setIniValTarDb(String iniValTarDb) {
        this.iniValTarDb = Functions.subString(iniValTarDb, Len.INI_VAL_TAR_DB);
    }

    public String getIniValTarDb() {
        return this.iniValTarDb;
    }

    public void setIniVldtProdDb(String iniVldtProdDb) {
        this.iniVldtProdDb = Functions.subString(iniVldtProdDb, Len.INI_VLDT_PROD_DB);
    }

    public String getIniVldtProdDb() {
        return this.iniVldtProdDb;
    }

    public void setDecorPoliDb(String decorPoliDb) {
        this.decorPoliDb = Functions.subString(decorPoliDb, Len.DECOR_POLI_DB);
    }

    public String getDecorPoliDb() {
        return this.decorPoliDb;
    }

    public void setDecorAdesDb(String decorAdesDb) {
        this.decorAdesDb = Functions.subString(decorAdesDb, Len.DECOR_ADES_DB);
    }

    public String getDecorAdesDb() {
        return this.decorAdesDb;
    }

    public void setDecorTrchDb(String decorTrchDb) {
        this.decorTrchDb = Functions.subString(decorTrchDb, Len.DECOR_TRCH_DB);
    }

    public String getDecorTrchDb() {
        return this.decorTrchDb;
    }

    public void setEmisPoliDb(String emisPoliDb) {
        this.emisPoliDb = Functions.subString(emisPoliDb, Len.EMIS_POLI_DB);
    }

    public String getEmisPoliDb() {
        return this.emisPoliDb;
    }

    public void setEmisTrchDb(String emisTrchDb) {
        this.emisTrchDb = Functions.subString(emisTrchDb, Len.EMIS_TRCH_DB);
    }

    public String getEmisTrchDb() {
        return this.emisTrchDb;
    }

    public void setScadTrchDb(String scadTrchDb) {
        this.scadTrchDb = Functions.subString(scadTrchDb, Len.SCAD_TRCH_DB);
    }

    public String getScadTrchDb() {
        return this.scadTrchDb;
    }

    public void setScadIntmdDb(String scadIntmdDb) {
        this.scadIntmdDb = Functions.subString(scadIntmdDb, Len.SCAD_INTMD_DB);
    }

    public String getScadIntmdDb() {
        return this.scadIntmdDb;
    }

    public void setScadPagPreDb(String scadPagPreDb) {
        this.scadPagPreDb = Functions.subString(scadPagPreDb, Len.SCAD_PAG_PRE_DB);
    }

    public String getScadPagPreDb() {
        return this.scadPagPreDb;
    }

    public void setUltPrePagDb(String ultPrePagDb) {
        this.ultPrePagDb = Functions.subString(ultPrePagDb, Len.ULT_PRE_PAG_DB);
    }

    public String getUltPrePagDb() {
        return this.ultPrePagDb;
    }

    public void setNasc1oAsstoDb(String nasc1oAsstoDb) {
        this.nasc1oAsstoDb = Functions.subString(nasc1oAsstoDb, Len.NASC1O_ASSTO_DB);
    }

    public String getNasc1oAsstoDb() {
        return this.nasc1oAsstoDb;
    }

    public void setEffCambStatDb(String effCambStatDb) {
        this.effCambStatDb = Functions.subString(effCambStatDb, Len.EFF_CAMB_STAT_DB);
    }

    public String getEffCambStatDb() {
        return this.effCambStatDb;
    }

    public void setEmisCambStatDb(String emisCambStatDb) {
        this.emisCambStatDb = Functions.subString(emisCambStatDb, Len.EMIS_CAMB_STAT_DB);
    }

    public String getEmisCambStatDb() {
        return this.emisCambStatDb;
    }

    public void setEffStabDb(String effStabDb) {
        this.effStabDb = Functions.subString(effStabDb, Len.EFF_STAB_DB);
    }

    public String getEffStabDb() {
        return this.effStabDb;
    }

    public void setEffRidzDb(String effRidzDb) {
        this.effRidzDb = Functions.subString(effRidzDb, Len.EFF_RIDZ_DB);
    }

    public String getEffRidzDb() {
        return this.effRidzDb;
    }

    public void setEmisRidzDb(String emisRidzDb) {
        this.emisRidzDb = Functions.subString(emisRidzDb, Len.EMIS_RIDZ_DB);
    }

    public String getEmisRidzDb() {
        return this.emisRidzDb;
    }

    public void setUltRivalDb(String ultRivalDb) {
        this.ultRivalDb = Functions.subString(ultRivalDb, Len.ULT_RIVAL_DB);
    }

    public String getUltRivalDb() {
        return this.ultRivalDb;
    }

    public void setQtzEmisDb(String qtzEmisDb) {
        this.qtzEmisDb = Functions.subString(qtzEmisDb, Len.QTZ_EMIS_DB);
    }

    public String getQtzEmisDb() {
        return this.qtzEmisDb;
    }

    public void setIncUltPreDb(String incUltPreDb) {
        this.incUltPreDb = Functions.subString(incUltPreDb, Len.INC_ULT_PRE_DB);
    }

    public String getIncUltPreDb() {
        return this.incUltPreDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RIS_DB = 10;
        public static final int PRODUZIONE_DB = 10;
        public static final int INI_VAL_TAR_DB = 10;
        public static final int INI_VLDT_PROD_DB = 10;
        public static final int DECOR_POLI_DB = 10;
        public static final int DECOR_ADES_DB = 10;
        public static final int DECOR_TRCH_DB = 10;
        public static final int EMIS_POLI_DB = 10;
        public static final int EMIS_TRCH_DB = 10;
        public static final int SCAD_TRCH_DB = 10;
        public static final int SCAD_INTMD_DB = 10;
        public static final int SCAD_PAG_PRE_DB = 10;
        public static final int ULT_PRE_PAG_DB = 10;
        public static final int NASC1O_ASSTO_DB = 10;
        public static final int EFF_CAMB_STAT_DB = 10;
        public static final int EMIS_CAMB_STAT_DB = 10;
        public static final int EFF_STAB_DB = 10;
        public static final int EFF_RIDZ_DB = 10;
        public static final int EMIS_RIDZ_DB = 10;
        public static final int ULT_RIVAL_DB = 10;
        public static final int QTZ_EMIS_DB = 10;
        public static final int INC_ULT_PRE_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
