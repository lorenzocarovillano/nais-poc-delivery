package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Loac0002FlDeroga;
import it.accenture.jnais.ws.redefines.Loac0002TabFunzDeroga;

/**Original name: LOAC0002-AREA-GEST-DEROGHE<br>
 * Variable: LOAC0002-AREA-GEST-DEROGHE from copybook LOAC0002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Loac0002AreaGestDeroghe {

    //==== PROPERTIES ====
    //Original name: LOAC0002-IND-R
    private short loac0002IndR = DefaultValues.SHORT_VAL;
    //Original name: LOAC0002-IND-C
    private short loac0002IndC = DefaultValues.SHORT_VAL;
    //Original name: LOAC0002-TP-MOVI
    private int loac0002TpMovi = DefaultValues.INT_VAL;
    //Original name: LOAC0002-FL-DEROGA
    private Loac0002FlDeroga loac0002FlDeroga = new Loac0002FlDeroga();
    //Original name: LOAC0002-ELE-MAX-FUNZ
    private short loac0002EleMaxFunz = ((short)1);
    //Original name: LOAC0002-TAB-FUNZ-DEROGA
    private Loac0002TabFunzDeroga loac0002TabFunzDeroga = new Loac0002TabFunzDeroga();

    //==== METHODS ====
    public void setLoac0002IndR(short loac0002IndR) {
        this.loac0002IndR = loac0002IndR;
    }

    public short getLoac0002IndR() {
        return this.loac0002IndR;
    }

    public void setLoac0002IndC(short loac0002IndC) {
        this.loac0002IndC = loac0002IndC;
    }

    public short getLoac0002IndC() {
        return this.loac0002IndC;
    }

    public void setLoac0002TpMovi(int loac0002TpMovi) {
        this.loac0002TpMovi = loac0002TpMovi;
    }

    public int getLoac0002TpMovi() {
        return this.loac0002TpMovi;
    }

    public short getLoac0002EleMaxFunz() {
        return this.loac0002EleMaxFunz;
    }

    public Loac0002FlDeroga getLoac0002FlDeroga() {
        return loac0002FlDeroga;
    }

    public Loac0002TabFunzDeroga getLoac0002TabFunzDeroga() {
        return loac0002TabFunzDeroga;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LOAC0002_TP_MOVI = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
