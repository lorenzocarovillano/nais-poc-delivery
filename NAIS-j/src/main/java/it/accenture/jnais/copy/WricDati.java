package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WricIdBatch;
import it.accenture.jnais.ws.redefines.WricIdJob;
import it.accenture.jnais.ws.redefines.WricIdOgg;
import it.accenture.jnais.ws.redefines.WricIdRichCollg;
import it.accenture.jnais.ws.redefines.WricTsEffEsecRich;

/**Original name: WRIC-DATI<br>
 * Variable: WRIC-DATI from copybook LCCVRIC1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WricDati {

    //==== PROPERTIES ====
    //Original name: WRIC-ID-RICH
    private int wricIdRich = DefaultValues.INT_VAL;
    //Original name: WRIC-COD-COMP-ANIA
    private int wricCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WRIC-TP-RICH
    private String wricTpRich = DefaultValues.stringVal(Len.WRIC_TP_RICH);
    //Original name: WRIC-COD-MACROFUNCT
    private String wricCodMacrofunct = DefaultValues.stringVal(Len.WRIC_COD_MACROFUNCT);
    //Original name: WRIC-TP-MOVI
    private int wricTpMovi = DefaultValues.INT_VAL;
    //Original name: WRIC-IB-RICH
    private String wricIbRich = DefaultValues.stringVal(Len.WRIC_IB_RICH);
    //Original name: WRIC-DT-EFF
    private int wricDtEff = DefaultValues.INT_VAL;
    //Original name: WRIC-DT-RGSTRZ-RICH
    private int wricDtRgstrzRich = DefaultValues.INT_VAL;
    //Original name: WRIC-DT-PERV-RICH
    private int wricDtPervRich = DefaultValues.INT_VAL;
    //Original name: WRIC-DT-ESEC-RICH
    private int wricDtEsecRich = DefaultValues.INT_VAL;
    //Original name: WRIC-TS-EFF-ESEC-RICH
    private WricTsEffEsecRich wricTsEffEsecRich = new WricTsEffEsecRich();
    //Original name: WRIC-ID-OGG
    private WricIdOgg wricIdOgg = new WricIdOgg();
    //Original name: WRIC-TP-OGG
    private String wricTpOgg = DefaultValues.stringVal(Len.WRIC_TP_OGG);
    //Original name: WRIC-IB-POLI
    private String wricIbPoli = DefaultValues.stringVal(Len.WRIC_IB_POLI);
    //Original name: WRIC-IB-ADES
    private String wricIbAdes = DefaultValues.stringVal(Len.WRIC_IB_ADES);
    //Original name: WRIC-IB-GAR
    private String wricIbGar = DefaultValues.stringVal(Len.WRIC_IB_GAR);
    //Original name: WRIC-IB-TRCH-DI-GAR
    private String wricIbTrchDiGar = DefaultValues.stringVal(Len.WRIC_IB_TRCH_DI_GAR);
    //Original name: WRIC-ID-BATCH
    private WricIdBatch wricIdBatch = new WricIdBatch();
    //Original name: WRIC-ID-JOB
    private WricIdJob wricIdJob = new WricIdJob();
    //Original name: WRIC-FL-SIMULAZIONE
    private char wricFlSimulazione = DefaultValues.CHAR_VAL;
    //Original name: WRIC-KEY-ORDINAMENTO
    private String wricKeyOrdinamento = DefaultValues.stringVal(Len.WRIC_KEY_ORDINAMENTO);
    //Original name: WRIC-ID-RICH-COLLG
    private WricIdRichCollg wricIdRichCollg = new WricIdRichCollg();
    //Original name: WRIC-TP-RAMO-BILA
    private String wricTpRamoBila = DefaultValues.stringVal(Len.WRIC_TP_RAMO_BILA);
    //Original name: WRIC-TP-FRM-ASSVA
    private String wricTpFrmAssva = DefaultValues.stringVal(Len.WRIC_TP_FRM_ASSVA);
    //Original name: WRIC-TP-CALC-RIS
    private String wricTpCalcRis = DefaultValues.stringVal(Len.WRIC_TP_CALC_RIS);
    //Original name: WRIC-DS-OPER-SQL
    private char wricDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WRIC-DS-VER
    private int wricDsVer = DefaultValues.INT_VAL;
    //Original name: WRIC-DS-TS-CPTZ
    private long wricDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WRIC-DS-UTENTE
    private String wricDsUtente = DefaultValues.stringVal(Len.WRIC_DS_UTENTE);
    //Original name: WRIC-DS-STATO-ELAB
    private char wricDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WRIC-RAMO-BILA
    private String wricRamoBila = DefaultValues.stringVal(Len.WRIC_RAMO_BILA);

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wricIdRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIC_ID_RICH, 0);
        position += Len.WRIC_ID_RICH;
        wricCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIC_COD_COMP_ANIA, 0);
        position += Len.WRIC_COD_COMP_ANIA;
        wricTpRich = MarshalByte.readString(buffer, position, Len.WRIC_TP_RICH);
        position += Len.WRIC_TP_RICH;
        wricCodMacrofunct = MarshalByte.readString(buffer, position, Len.WRIC_COD_MACROFUNCT);
        position += Len.WRIC_COD_MACROFUNCT;
        wricTpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIC_TP_MOVI, 0);
        position += Len.WRIC_TP_MOVI;
        wricIbRich = MarshalByte.readString(buffer, position, Len.WRIC_IB_RICH);
        position += Len.WRIC_IB_RICH;
        wricDtEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIC_DT_EFF, 0);
        position += Len.WRIC_DT_EFF;
        wricDtRgstrzRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIC_DT_RGSTRZ_RICH, 0);
        position += Len.WRIC_DT_RGSTRZ_RICH;
        wricDtPervRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIC_DT_PERV_RICH, 0);
        position += Len.WRIC_DT_PERV_RICH;
        wricDtEsecRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIC_DT_ESEC_RICH, 0);
        position += Len.WRIC_DT_ESEC_RICH;
        wricTsEffEsecRich.setWricTsEffEsecRichFromBuffer(buffer, position);
        position += WricTsEffEsecRich.Len.WRIC_TS_EFF_ESEC_RICH;
        wricIdOgg.setWricIdOggFromBuffer(buffer, position);
        position += WricIdOgg.Len.WRIC_ID_OGG;
        wricTpOgg = MarshalByte.readString(buffer, position, Len.WRIC_TP_OGG);
        position += Len.WRIC_TP_OGG;
        wricIbPoli = MarshalByte.readString(buffer, position, Len.WRIC_IB_POLI);
        position += Len.WRIC_IB_POLI;
        wricIbAdes = MarshalByte.readString(buffer, position, Len.WRIC_IB_ADES);
        position += Len.WRIC_IB_ADES;
        wricIbGar = MarshalByte.readString(buffer, position, Len.WRIC_IB_GAR);
        position += Len.WRIC_IB_GAR;
        wricIbTrchDiGar = MarshalByte.readString(buffer, position, Len.WRIC_IB_TRCH_DI_GAR);
        position += Len.WRIC_IB_TRCH_DI_GAR;
        wricIdBatch.setWricIdBatchFromBuffer(buffer, position);
        position += WricIdBatch.Len.WRIC_ID_BATCH;
        wricIdJob.setWricIdJobFromBuffer(buffer, position);
        position += WricIdJob.Len.WRIC_ID_JOB;
        wricFlSimulazione = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wricKeyOrdinamento = MarshalByte.readString(buffer, position, Len.WRIC_KEY_ORDINAMENTO);
        position += Len.WRIC_KEY_ORDINAMENTO;
        wricIdRichCollg.setWricIdRichCollgFromBuffer(buffer, position);
        position += WricIdRichCollg.Len.WRIC_ID_RICH_COLLG;
        wricTpRamoBila = MarshalByte.readString(buffer, position, Len.WRIC_TP_RAMO_BILA);
        position += Len.WRIC_TP_RAMO_BILA;
        wricTpFrmAssva = MarshalByte.readString(buffer, position, Len.WRIC_TP_FRM_ASSVA);
        position += Len.WRIC_TP_FRM_ASSVA;
        wricTpCalcRis = MarshalByte.readString(buffer, position, Len.WRIC_TP_CALC_RIS);
        position += Len.WRIC_TP_CALC_RIS;
        wricDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wricDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIC_DS_VER, 0);
        position += Len.WRIC_DS_VER;
        wricDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRIC_DS_TS_CPTZ, 0);
        position += Len.WRIC_DS_TS_CPTZ;
        wricDsUtente = MarshalByte.readString(buffer, position, Len.WRIC_DS_UTENTE);
        position += Len.WRIC_DS_UTENTE;
        wricDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wricRamoBila = MarshalByte.readString(buffer, position, Len.WRIC_RAMO_BILA);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wricIdRich, Len.Int.WRIC_ID_RICH, 0);
        position += Len.WRIC_ID_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, wricCodCompAnia, Len.Int.WRIC_COD_COMP_ANIA, 0);
        position += Len.WRIC_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wricTpRich, Len.WRIC_TP_RICH);
        position += Len.WRIC_TP_RICH;
        MarshalByte.writeString(buffer, position, wricCodMacrofunct, Len.WRIC_COD_MACROFUNCT);
        position += Len.WRIC_COD_MACROFUNCT;
        MarshalByte.writeIntAsPacked(buffer, position, wricTpMovi, Len.Int.WRIC_TP_MOVI, 0);
        position += Len.WRIC_TP_MOVI;
        MarshalByte.writeString(buffer, position, wricIbRich, Len.WRIC_IB_RICH);
        position += Len.WRIC_IB_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, wricDtEff, Len.Int.WRIC_DT_EFF, 0);
        position += Len.WRIC_DT_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wricDtRgstrzRich, Len.Int.WRIC_DT_RGSTRZ_RICH, 0);
        position += Len.WRIC_DT_RGSTRZ_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, wricDtPervRich, Len.Int.WRIC_DT_PERV_RICH, 0);
        position += Len.WRIC_DT_PERV_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, wricDtEsecRich, Len.Int.WRIC_DT_ESEC_RICH, 0);
        position += Len.WRIC_DT_ESEC_RICH;
        wricTsEffEsecRich.getWricTsEffEsecRichAsBuffer(buffer, position);
        position += WricTsEffEsecRich.Len.WRIC_TS_EFF_ESEC_RICH;
        wricIdOgg.getWricIdOggAsBuffer(buffer, position);
        position += WricIdOgg.Len.WRIC_ID_OGG;
        MarshalByte.writeString(buffer, position, wricTpOgg, Len.WRIC_TP_OGG);
        position += Len.WRIC_TP_OGG;
        MarshalByte.writeString(buffer, position, wricIbPoli, Len.WRIC_IB_POLI);
        position += Len.WRIC_IB_POLI;
        MarshalByte.writeString(buffer, position, wricIbAdes, Len.WRIC_IB_ADES);
        position += Len.WRIC_IB_ADES;
        MarshalByte.writeString(buffer, position, wricIbGar, Len.WRIC_IB_GAR);
        position += Len.WRIC_IB_GAR;
        MarshalByte.writeString(buffer, position, wricIbTrchDiGar, Len.WRIC_IB_TRCH_DI_GAR);
        position += Len.WRIC_IB_TRCH_DI_GAR;
        wricIdBatch.getWricIdBatchAsBuffer(buffer, position);
        position += WricIdBatch.Len.WRIC_ID_BATCH;
        wricIdJob.getWricIdJobAsBuffer(buffer, position);
        position += WricIdJob.Len.WRIC_ID_JOB;
        MarshalByte.writeChar(buffer, position, wricFlSimulazione);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wricKeyOrdinamento, Len.WRIC_KEY_ORDINAMENTO);
        position += Len.WRIC_KEY_ORDINAMENTO;
        wricIdRichCollg.getWricIdRichCollgAsBuffer(buffer, position);
        position += WricIdRichCollg.Len.WRIC_ID_RICH_COLLG;
        MarshalByte.writeString(buffer, position, wricTpRamoBila, Len.WRIC_TP_RAMO_BILA);
        position += Len.WRIC_TP_RAMO_BILA;
        MarshalByte.writeString(buffer, position, wricTpFrmAssva, Len.WRIC_TP_FRM_ASSVA);
        position += Len.WRIC_TP_FRM_ASSVA;
        MarshalByte.writeString(buffer, position, wricTpCalcRis, Len.WRIC_TP_CALC_RIS);
        position += Len.WRIC_TP_CALC_RIS;
        MarshalByte.writeChar(buffer, position, wricDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wricDsVer, Len.Int.WRIC_DS_VER, 0);
        position += Len.WRIC_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wricDsTsCptz, Len.Int.WRIC_DS_TS_CPTZ, 0);
        position += Len.WRIC_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wricDsUtente, Len.WRIC_DS_UTENTE);
        position += Len.WRIC_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wricDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wricRamoBila, Len.WRIC_RAMO_BILA);
        return buffer;
    }

    public void setWricIdRich(int wricIdRich) {
        this.wricIdRich = wricIdRich;
    }

    public int getWricIdRich() {
        return this.wricIdRich;
    }

    public void setWricCodCompAnia(int wricCodCompAnia) {
        this.wricCodCompAnia = wricCodCompAnia;
    }

    public int getWricCodCompAnia() {
        return this.wricCodCompAnia;
    }

    public void setWricTpRich(String wricTpRich) {
        this.wricTpRich = Functions.subString(wricTpRich, Len.WRIC_TP_RICH);
    }

    public String getWricTpRich() {
        return this.wricTpRich;
    }

    public void setWricCodMacrofunct(String wricCodMacrofunct) {
        this.wricCodMacrofunct = Functions.subString(wricCodMacrofunct, Len.WRIC_COD_MACROFUNCT);
    }

    public String getWricCodMacrofunct() {
        return this.wricCodMacrofunct;
    }

    public void setWricTpMovi(int wricTpMovi) {
        this.wricTpMovi = wricTpMovi;
    }

    public int getWricTpMovi() {
        return this.wricTpMovi;
    }

    public void setWricIbRich(String wricIbRich) {
        this.wricIbRich = Functions.subString(wricIbRich, Len.WRIC_IB_RICH);
    }

    public String getWricIbRich() {
        return this.wricIbRich;
    }

    public void setWricDtEff(int wricDtEff) {
        this.wricDtEff = wricDtEff;
    }

    public int getWricDtEff() {
        return this.wricDtEff;
    }

    public void setWricDtRgstrzRich(int wricDtRgstrzRich) {
        this.wricDtRgstrzRich = wricDtRgstrzRich;
    }

    public int getWricDtRgstrzRich() {
        return this.wricDtRgstrzRich;
    }

    public void setWricDtPervRich(int wricDtPervRich) {
        this.wricDtPervRich = wricDtPervRich;
    }

    public int getWricDtPervRich() {
        return this.wricDtPervRich;
    }

    public void setWricDtEsecRich(int wricDtEsecRich) {
        this.wricDtEsecRich = wricDtEsecRich;
    }

    public int getWricDtEsecRich() {
        return this.wricDtEsecRich;
    }

    public void setWricTpOgg(String wricTpOgg) {
        this.wricTpOgg = Functions.subString(wricTpOgg, Len.WRIC_TP_OGG);
    }

    public String getWricTpOgg() {
        return this.wricTpOgg;
    }

    public String getWricTpOggFormatted() {
        return Functions.padBlanks(getWricTpOgg(), Len.WRIC_TP_OGG);
    }

    public void setWricIbPoli(String wricIbPoli) {
        this.wricIbPoli = Functions.subString(wricIbPoli, Len.WRIC_IB_POLI);
    }

    public String getWricIbPoli() {
        return this.wricIbPoli;
    }

    public void setWricIbAdes(String wricIbAdes) {
        this.wricIbAdes = Functions.subString(wricIbAdes, Len.WRIC_IB_ADES);
    }

    public String getWricIbAdes() {
        return this.wricIbAdes;
    }

    public void setWricIbGar(String wricIbGar) {
        this.wricIbGar = Functions.subString(wricIbGar, Len.WRIC_IB_GAR);
    }

    public String getWricIbGar() {
        return this.wricIbGar;
    }

    public void setWricIbTrchDiGar(String wricIbTrchDiGar) {
        this.wricIbTrchDiGar = Functions.subString(wricIbTrchDiGar, Len.WRIC_IB_TRCH_DI_GAR);
    }

    public String getWricIbTrchDiGar() {
        return this.wricIbTrchDiGar;
    }

    public void setWricFlSimulazione(char wricFlSimulazione) {
        this.wricFlSimulazione = wricFlSimulazione;
    }

    public char getWricFlSimulazione() {
        return this.wricFlSimulazione;
    }

    public void setWricKeyOrdinamento(String wricKeyOrdinamento) {
        this.wricKeyOrdinamento = Functions.subString(wricKeyOrdinamento, Len.WRIC_KEY_ORDINAMENTO);
    }

    public String getWricKeyOrdinamento() {
        return this.wricKeyOrdinamento;
    }

    public void setWricTpRamoBila(String wricTpRamoBila) {
        this.wricTpRamoBila = Functions.subString(wricTpRamoBila, Len.WRIC_TP_RAMO_BILA);
    }

    public String getWricTpRamoBila() {
        return this.wricTpRamoBila;
    }

    public String getWricTpRamoBilaFormatted() {
        return Functions.padBlanks(getWricTpRamoBila(), Len.WRIC_TP_RAMO_BILA);
    }

    public void setWricTpFrmAssva(String wricTpFrmAssva) {
        this.wricTpFrmAssva = Functions.subString(wricTpFrmAssva, Len.WRIC_TP_FRM_ASSVA);
    }

    public String getWricTpFrmAssva() {
        return this.wricTpFrmAssva;
    }

    public String getWricTpFrmAssvaFormatted() {
        return Functions.padBlanks(getWricTpFrmAssva(), Len.WRIC_TP_FRM_ASSVA);
    }

    public void setWricTpCalcRis(String wricTpCalcRis) {
        this.wricTpCalcRis = Functions.subString(wricTpCalcRis, Len.WRIC_TP_CALC_RIS);
    }

    public String getWricTpCalcRis() {
        return this.wricTpCalcRis;
    }

    public String getWricTpCalcRisFormatted() {
        return Functions.padBlanks(getWricTpCalcRis(), Len.WRIC_TP_CALC_RIS);
    }

    public void setWricDsOperSql(char wricDsOperSql) {
        this.wricDsOperSql = wricDsOperSql;
    }

    public char getWricDsOperSql() {
        return this.wricDsOperSql;
    }

    public void setWricDsVer(int wricDsVer) {
        this.wricDsVer = wricDsVer;
    }

    public int getWricDsVer() {
        return this.wricDsVer;
    }

    public void setWricDsTsCptz(long wricDsTsCptz) {
        this.wricDsTsCptz = wricDsTsCptz;
    }

    public long getWricDsTsCptz() {
        return this.wricDsTsCptz;
    }

    public void setWricDsUtente(String wricDsUtente) {
        this.wricDsUtente = Functions.subString(wricDsUtente, Len.WRIC_DS_UTENTE);
    }

    public String getWricDsUtente() {
        return this.wricDsUtente;
    }

    public void setWricDsStatoElab(char wricDsStatoElab) {
        this.wricDsStatoElab = wricDsStatoElab;
    }

    public char getWricDsStatoElab() {
        return this.wricDsStatoElab;
    }

    public void setWricRamoBila(String wricRamoBila) {
        this.wricRamoBila = Functions.subString(wricRamoBila, Len.WRIC_RAMO_BILA);
    }

    public String getWricRamoBila() {
        return this.wricRamoBila;
    }

    public String getWricRamoBilaFormatted() {
        return Functions.padBlanks(getWricRamoBila(), Len.WRIC_RAMO_BILA);
    }

    public WricIdBatch getWricIdBatch() {
        return wricIdBatch;
    }

    public WricIdJob getWricIdJob() {
        return wricIdJob;
    }

    public WricIdOgg getWricIdOgg() {
        return wricIdOgg;
    }

    public WricIdRichCollg getWricIdRichCollg() {
        return wricIdRichCollg;
    }

    public WricTsEffEsecRich getWricTsEffEsecRich() {
        return wricTsEffEsecRich;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIC_ID_RICH = 5;
        public static final int WRIC_COD_COMP_ANIA = 3;
        public static final int WRIC_TP_RICH = 2;
        public static final int WRIC_COD_MACROFUNCT = 2;
        public static final int WRIC_TP_MOVI = 3;
        public static final int WRIC_IB_RICH = 40;
        public static final int WRIC_DT_EFF = 5;
        public static final int WRIC_DT_RGSTRZ_RICH = 5;
        public static final int WRIC_DT_PERV_RICH = 5;
        public static final int WRIC_DT_ESEC_RICH = 5;
        public static final int WRIC_TP_OGG = 2;
        public static final int WRIC_IB_POLI = 40;
        public static final int WRIC_IB_ADES = 40;
        public static final int WRIC_IB_GAR = 40;
        public static final int WRIC_IB_TRCH_DI_GAR = 40;
        public static final int WRIC_FL_SIMULAZIONE = 1;
        public static final int WRIC_KEY_ORDINAMENTO = 300;
        public static final int WRIC_TP_RAMO_BILA = 2;
        public static final int WRIC_TP_FRM_ASSVA = 2;
        public static final int WRIC_TP_CALC_RIS = 2;
        public static final int WRIC_DS_OPER_SQL = 1;
        public static final int WRIC_DS_VER = 5;
        public static final int WRIC_DS_TS_CPTZ = 10;
        public static final int WRIC_DS_UTENTE = 20;
        public static final int WRIC_DS_STATO_ELAB = 1;
        public static final int WRIC_RAMO_BILA = 12;
        public static final int DATI = WRIC_ID_RICH + WRIC_COD_COMP_ANIA + WRIC_TP_RICH + WRIC_COD_MACROFUNCT + WRIC_TP_MOVI + WRIC_IB_RICH + WRIC_DT_EFF + WRIC_DT_RGSTRZ_RICH + WRIC_DT_PERV_RICH + WRIC_DT_ESEC_RICH + WricTsEffEsecRich.Len.WRIC_TS_EFF_ESEC_RICH + WricIdOgg.Len.WRIC_ID_OGG + WRIC_TP_OGG + WRIC_IB_POLI + WRIC_IB_ADES + WRIC_IB_GAR + WRIC_IB_TRCH_DI_GAR + WricIdBatch.Len.WRIC_ID_BATCH + WricIdJob.Len.WRIC_ID_JOB + WRIC_FL_SIMULAZIONE + WRIC_KEY_ORDINAMENTO + WricIdRichCollg.Len.WRIC_ID_RICH_COLLG + WRIC_TP_RAMO_BILA + WRIC_TP_FRM_ASSVA + WRIC_TP_CALC_RIS + WRIC_DS_OPER_SQL + WRIC_DS_VER + WRIC_DS_TS_CPTZ + WRIC_DS_UTENTE + WRIC_DS_STATO_ELAB + WRIC_RAMO_BILA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIC_ID_RICH = 9;
            public static final int WRIC_COD_COMP_ANIA = 5;
            public static final int WRIC_TP_MOVI = 5;
            public static final int WRIC_DT_EFF = 8;
            public static final int WRIC_DT_RGSTRZ_RICH = 8;
            public static final int WRIC_DT_PERV_RICH = 8;
            public static final int WRIC_DT_ESEC_RICH = 8;
            public static final int WRIC_DS_VER = 9;
            public static final int WRIC_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
