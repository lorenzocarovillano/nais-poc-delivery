package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0001-LOG-ERRORE<br>
 * Variable: IDSV0001-LOG-ERRORE from copybook IDSV0001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv0001LogErrore {

    //==== PROPERTIES ====
    //Original name: IDSV0001-COD-SERVIZIO-BE
    private String codServizioBe = DefaultValues.stringVal(Len.COD_SERVIZIO_BE);
    //Original name: IDSV0001-LABEL-ERR
    private String labelErr = DefaultValues.stringVal(Len.LABEL_ERR);
    //Original name: IDSV0001-DESC-ERRORE-ESTESA
    private String descErroreEstesa = DefaultValues.stringVal(Len.DESC_ERRORE_ESTESA);
    //Original name: IDSV0001-OPER-TABELLA
    private String operTabella = DefaultValues.stringVal(Len.OPER_TABELLA);
    //Original name: IDSV0001-NOME-TABELLA
    private String nomeTabella = DefaultValues.stringVal(Len.NOME_TABELLA);
    //Original name: IDSV0001-STATUS-TABELLA
    private String statusTabella = DefaultValues.stringVal(Len.STATUS_TABELLA);
    //Original name: IDSV0001-KEY-TABELLA
    private String keyTabella = DefaultValues.stringVal(Len.KEY_TABELLA);

    //==== METHODS ====
    public void setLogErroreBytes(byte[] buffer) {
        setLogErroreBytes(buffer, 1);
    }

    public byte[] getIwfo0051LogErroreBytes() {
        byte[] buffer = new byte[Len.LOG_ERRORE];
        return getLogErroreBytes(buffer, 1);
    }

    public void setLogErroreBytes(byte[] buffer, int offset) {
        int position = offset;
        codServizioBe = MarshalByte.readString(buffer, position, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        labelErr = MarshalByte.readString(buffer, position, Len.LABEL_ERR);
        position += Len.LABEL_ERR;
        descErroreEstesa = MarshalByte.readString(buffer, position, Len.DESC_ERRORE_ESTESA);
        position += Len.DESC_ERRORE_ESTESA;
        operTabella = MarshalByte.readString(buffer, position, Len.OPER_TABELLA);
        position += Len.OPER_TABELLA;
        nomeTabella = MarshalByte.readString(buffer, position, Len.NOME_TABELLA);
        position += Len.NOME_TABELLA;
        statusTabella = MarshalByte.readString(buffer, position, Len.STATUS_TABELLA);
        position += Len.STATUS_TABELLA;
        keyTabella = MarshalByte.readString(buffer, position, Len.KEY_TABELLA);
    }

    public byte[] getLogErroreBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codServizioBe, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        MarshalByte.writeString(buffer, position, labelErr, Len.LABEL_ERR);
        position += Len.LABEL_ERR;
        MarshalByte.writeString(buffer, position, descErroreEstesa, Len.DESC_ERRORE_ESTESA);
        position += Len.DESC_ERRORE_ESTESA;
        MarshalByte.writeString(buffer, position, operTabella, Len.OPER_TABELLA);
        position += Len.OPER_TABELLA;
        MarshalByte.writeString(buffer, position, nomeTabella, Len.NOME_TABELLA);
        position += Len.NOME_TABELLA;
        MarshalByte.writeString(buffer, position, statusTabella, Len.STATUS_TABELLA);
        position += Len.STATUS_TABELLA;
        MarshalByte.writeString(buffer, position, keyTabella, Len.KEY_TABELLA);
        return buffer;
    }

    public void initIwfo0051LogErroreHighValues() {
        codServizioBe = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.COD_SERVIZIO_BE);
        labelErr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LABEL_ERR);
        descErroreEstesa = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.DESC_ERRORE_ESTESA);
        operTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.OPER_TABELLA);
        nomeTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NOME_TABELLA);
        statusTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.STATUS_TABELLA);
        keyTabella = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.KEY_TABELLA);
    }

    public void initLogErroreSpaces() {
        codServizioBe = "";
        labelErr = "";
        descErroreEstesa = "";
        operTabella = "";
        nomeTabella = "";
        statusTabella = "";
        keyTabella = "";
    }

    public void setCodServizioBe(String codServizioBe) {
        this.codServizioBe = Functions.subString(codServizioBe, Len.COD_SERVIZIO_BE);
    }

    public String getCodServizioBe() {
        return this.codServizioBe;
    }

    public String getCodServizioBeFormatted() {
        return Functions.padBlanks(getCodServizioBe(), Len.COD_SERVIZIO_BE);
    }

    public void setLabelErr(String labelErr) {
        this.labelErr = Functions.subString(labelErr, Len.LABEL_ERR);
    }

    public String getLabelErr() {
        return this.labelErr;
    }

    public void setDescErroreEstesa(String descErroreEstesa) {
        this.descErroreEstesa = Functions.subString(descErroreEstesa, Len.DESC_ERRORE_ESTESA);
    }

    public String getDescErroreEstesa() {
        return this.descErroreEstesa;
    }

    public String getDescErroreEstesaFormatted() {
        return Functions.padBlanks(getDescErroreEstesa(), Len.DESC_ERRORE_ESTESA);
    }

    public void setOperTabella(String operTabella) {
        this.operTabella = Functions.subString(operTabella, Len.OPER_TABELLA);
    }

    public String getOperTabella() {
        return this.operTabella;
    }

    public String getOperTabellaFormatted() {
        return Functions.padBlanks(getOperTabella(), Len.OPER_TABELLA);
    }

    public void setNomeTabella(String nomeTabella) {
        this.nomeTabella = Functions.subString(nomeTabella, Len.NOME_TABELLA);
    }

    public String getNomeTabella() {
        return this.nomeTabella;
    }

    public String getNomeTabellaFormatted() {
        return Functions.padBlanks(getNomeTabella(), Len.NOME_TABELLA);
    }

    public void setStatusTabella(String statusTabella) {
        this.statusTabella = Functions.subString(statusTabella, Len.STATUS_TABELLA);
    }

    public String getStatusTabella() {
        return this.statusTabella;
    }

    public String getStatusTabellaFormatted() {
        return Functions.padBlanks(getStatusTabella(), Len.STATUS_TABELLA);
    }

    public void setKeyTabella(String keyTabella) {
        this.keyTabella = Functions.subString(keyTabella, Len.KEY_TABELLA);
    }

    public String getKeyTabella() {
        return this.keyTabella;
    }

    public String getKeyTabellaFormatted() {
        return Functions.padBlanks(getKeyTabella(), Len.KEY_TABELLA);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_SERVIZIO_BE = 8;
        public static final int LABEL_ERR = 30;
        public static final int DESC_ERRORE_ESTESA = 200;
        public static final int OPER_TABELLA = 2;
        public static final int NOME_TABELLA = 18;
        public static final int STATUS_TABELLA = 10;
        public static final int KEY_TABELLA = 20;
        public static final int LOG_ERRORE = COD_SERVIZIO_BE + LABEL_ERR + DESC_ERRORE_ESTESA + OPER_TABELLA + NOME_TABELLA + STATUS_TABELLA + KEY_TABELLA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
