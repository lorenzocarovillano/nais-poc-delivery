package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.ws.occurs.Ivvc0222Fnd;

/**Original name: IVVC0222<br>
 * Variable: IVVC0222 from copybook IVVC0222<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0222 implements ICopyable<Ivvc0222> {

    //==== PROPERTIES ====
    public static final int FND_MAXOCCURS = 100;
    //Original name: IVVC0222-ID-TRANCHE
    private int idTranche = DefaultValues.INT_VAL;
    //Original name: IVVC0222-TP-TRCH
    private String tpTrch = DefaultValues.stringVal(Len.TP_TRCH);
    //Original name: IVVC0222-ELE-FND-MAX
    private short eleFndMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: IVVC0222-FND
    private Ivvc0222Fnd[] fnd = new Ivvc0222Fnd[FND_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ivvc0222() {
        init();
    }

    public Ivvc0222(Ivvc0222 ivvc0222) {
        this();
        this.idTranche = ivvc0222.idTranche;
        this.tpTrch = ivvc0222.tpTrch;
        this.eleFndMax = ivvc0222.eleFndMax;
        for (int fndIdx = 1; fndIdx <= Ivvc0222.FND_MAXOCCURS; fndIdx++) {
            this.fnd[fndIdx - 1] = ivvc0222.fnd[fndIdx - 1].copy();
        }
    }

    //==== METHODS ====
    public void init() {
        for (int fndIdx = 1; fndIdx <= FND_MAXOCCURS; fndIdx++) {
            fnd[fndIdx - 1] = new Ivvc0222Fnd();
        }
    }

    public void initIvvc0222Spaces() {
        idTranche = Types.INVALID_INT_VAL;
        tpTrch = "";
        eleFndMax = Types.INVALID_BINARY_SHORT_VAL;
        for (int idx = 1; idx <= FND_MAXOCCURS; idx++) {
            fnd[idx - 1].initFndSpaces();
        }
    }

    public void setIdTranche(int idTranche) {
        this.idTranche = idTranche;
    }

    public int getIdTranche() {
        return this.idTranche;
    }

    public void setTpTrch(String tpTrch) {
        this.tpTrch = Functions.subString(tpTrch, Len.TP_TRCH);
    }

    public String getTpTrch() {
        return this.tpTrch;
    }

    public void setEleFndMax(short eleFndMax) {
        this.eleFndMax = eleFndMax;
    }

    public short getEleFndMax() {
        return this.eleFndMax;
    }

    public Ivvc0222Fnd getFnd(int idx) {
        return fnd[idx - 1];
    }

    public Ivvc0222 copy() {
        return new Ivvc0222(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_TRANCHE = 5;
        public static final int TP_TRCH = 2;
        public static final int ELE_FND_MAX = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_TRANCHE = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
