package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVDFL3<br>
 * Copybook: IDBVDFL3 from copybook IDBVDFL3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvdfl3 {

    //==== PROPERTIES ====
    //Original name: DFL-DT-INI-EFF-DB
    private String flDtIniEffDb = DefaultValues.stringVal(Len.FL_DT_INI_EFF_DB);
    //Original name: DFL-DT-END-EFF-DB
    private String flDtEndEffDb = DefaultValues.stringVal(Len.FL_DT_END_EFF_DB);

    //==== METHODS ====
    public void setFlDtIniEffDb(String flDtIniEffDb) {
        this.flDtIniEffDb = Functions.subString(flDtIniEffDb, Len.FL_DT_INI_EFF_DB);
    }

    public String getFlDtIniEffDb() {
        return this.flDtIniEffDb;
    }

    public void setFlDtEndEffDb(String flDtEndEffDb) {
        this.flDtEndEffDb = Functions.subString(flDtEndEffDb, Len.FL_DT_END_EFF_DB);
    }

    public String getFlDtEndEffDb() {
        return this.flDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_DT_INI_EFF_DB = 10;
        public static final int FL_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
