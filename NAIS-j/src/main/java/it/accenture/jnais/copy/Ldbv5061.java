package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: LDBV5061<br>
 * Variable: LDBV5061 from copybook LDBV5061<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv5061 {

    //==== PROPERTIES ====
    //Original name: LDBV5061-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LDBV5061-TP-MOVI-01
    private int tpMovi01 = DefaultValues.INT_VAL;
    //Original name: LDBV5061-TP-MOVI-02
    private int tpMovi02 = DefaultValues.INT_VAL;
    //Original name: LDBV5061-TP-MOVI-03
    private int tpMovi03 = DefaultValues.INT_VAL;
    //Original name: LDBV5061-TP-MOVI-04
    private int tpMovi04 = DefaultValues.INT_VAL;
    //Original name: LDBV5061-TP-MOVI-05
    private int tpMovi05 = DefaultValues.INT_VAL;
    //Original name: LDBV5061-TP-MOVI-06
    private int tpMovi06 = DefaultValues.INT_VAL;
    //Original name: LDBV5061-TP-MOVI-07
    private int tpMovi07 = DefaultValues.INT_VAL;
    //Original name: LDBV5061-TP-MOVI-08
    private int tpMovi08 = DefaultValues.INT_VAL;
    //Original name: LDBV5061-TP-MOVI-09
    private int tpMovi09 = DefaultValues.INT_VAL;
    //Original name: LDBV5061-TP-MOVI-10
    private int tpMovi10 = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbv5061Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV5061];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV5061);
        setLdbv5061Bytes(buffer, 1);
    }

    public String getLdbv5061Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv5061Bytes());
    }

    public byte[] getLdbv5061Bytes() {
        byte[] buffer = new byte[Len.LDBV5061];
        return getLdbv5061Bytes(buffer, 1);
    }

    public void setLdbv5061Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        tpMovi01 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI01, 0);
        position += Len.TP_MOVI01;
        tpMovi02 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI02, 0);
        position += Len.TP_MOVI02;
        tpMovi03 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI03, 0);
        position += Len.TP_MOVI03;
        tpMovi04 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI04, 0);
        position += Len.TP_MOVI04;
        tpMovi05 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI05, 0);
        position += Len.TP_MOVI05;
        tpMovi06 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI06, 0);
        position += Len.TP_MOVI06;
        tpMovi07 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI07, 0);
        position += Len.TP_MOVI07;
        tpMovi08 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI08, 0);
        position += Len.TP_MOVI08;
        tpMovi09 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI09, 0);
        position += Len.TP_MOVI09;
        tpMovi10 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI10, 0);
    }

    public byte[] getLdbv5061Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi01, Len.Int.TP_MOVI01, 0);
        position += Len.TP_MOVI01;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi02, Len.Int.TP_MOVI02, 0);
        position += Len.TP_MOVI02;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi03, Len.Int.TP_MOVI03, 0);
        position += Len.TP_MOVI03;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi04, Len.Int.TP_MOVI04, 0);
        position += Len.TP_MOVI04;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi05, Len.Int.TP_MOVI05, 0);
        position += Len.TP_MOVI05;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi06, Len.Int.TP_MOVI06, 0);
        position += Len.TP_MOVI06;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi07, Len.Int.TP_MOVI07, 0);
        position += Len.TP_MOVI07;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi08, Len.Int.TP_MOVI08, 0);
        position += Len.TP_MOVI08;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi09, Len.Int.TP_MOVI09, 0);
        position += Len.TP_MOVI09;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi10, Len.Int.TP_MOVI10, 0);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setTpMovi01(int tpMovi01) {
        this.tpMovi01 = tpMovi01;
    }

    public int getTpMovi01() {
        return this.tpMovi01;
    }

    public void setTpMovi02(int tpMovi02) {
        this.tpMovi02 = tpMovi02;
    }

    public int getTpMovi02() {
        return this.tpMovi02;
    }

    public void setTpMovi03(int tpMovi03) {
        this.tpMovi03 = tpMovi03;
    }

    public int getTpMovi03() {
        return this.tpMovi03;
    }

    public void setTpMovi04(int tpMovi04) {
        this.tpMovi04 = tpMovi04;
    }

    public int getTpMovi04() {
        return this.tpMovi04;
    }

    public void setTpMovi05(int tpMovi05) {
        this.tpMovi05 = tpMovi05;
    }

    public int getTpMovi05() {
        return this.tpMovi05;
    }

    public void setTpMovi06(int tpMovi06) {
        this.tpMovi06 = tpMovi06;
    }

    public int getTpMovi06() {
        return this.tpMovi06;
    }

    public void setTpMovi07(int tpMovi07) {
        this.tpMovi07 = tpMovi07;
    }

    public int getTpMovi07() {
        return this.tpMovi07;
    }

    public void setTpMovi08(int tpMovi08) {
        this.tpMovi08 = tpMovi08;
    }

    public int getTpMovi08() {
        return this.tpMovi08;
    }

    public void setTpMovi09(int tpMovi09) {
        this.tpMovi09 = tpMovi09;
    }

    public int getTpMovi09() {
        return this.tpMovi09;
    }

    public void setTpMovi10(int tpMovi10) {
        this.tpMovi10 = tpMovi10;
    }

    public int getTpMovi10() {
        return this.tpMovi10;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POLI = 5;
        public static final int TP_MOVI01 = 3;
        public static final int TP_MOVI02 = 3;
        public static final int TP_MOVI03 = 3;
        public static final int TP_MOVI04 = 3;
        public static final int TP_MOVI05 = 3;
        public static final int TP_MOVI06 = 3;
        public static final int TP_MOVI07 = 3;
        public static final int TP_MOVI08 = 3;
        public static final int TP_MOVI09 = 3;
        public static final int TP_MOVI10 = 3;
        public static final int LDBV5061 = ID_POLI + TP_MOVI01 + TP_MOVI02 + TP_MOVI03 + TP_MOVI04 + TP_MOVI05 + TP_MOVI06 + TP_MOVI07 + TP_MOVI08 + TP_MOVI09 + TP_MOVI10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int TP_MOVI01 = 5;
            public static final int TP_MOVI02 = 5;
            public static final int TP_MOVI03 = 5;
            public static final int TP_MOVI04 = 5;
            public static final int TP_MOVI05 = 5;
            public static final int TP_MOVI06 = 5;
            public static final int TP_MOVI07 = 5;
            public static final int TP_MOVI08 = 5;
            public static final int TP_MOVI09 = 5;
            public static final int TP_MOVI10 = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
