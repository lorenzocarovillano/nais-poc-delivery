package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: DATA-ELABORAZIONE-REPORT<br>
 * Variable: DATA-ELABORAZIONE-REPORT from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DataElaborazioneReport {

    //==== PROPERTIES ====
    //Original name: FILLER-DATA-ELABORAZIONE-REPORT
    private String flr1 = "* DATA ELABORAZIONE    :";
    //Original name: REP-GIORNO
    private String giorno = "";
    //Original name: FILLER-DATA-ELABORAZIONE-REPORT-1
    private String flr2 = " /";
    //Original name: REP-MESE
    private String mese = "";
    //Original name: FILLER-DATA-ELABORAZIONE-REPORT-2
    private String flr3 = " /";
    //Original name: REP-ANNO
    private String anno = "";

    //==== METHODS ====
    public String getDataElaborazioneReportFormatted() {
        return MarshalByteExt.bufferToStr(getDataElaborazioneReportBytes());
    }

    public byte[] getDataElaborazioneReportBytes() {
        byte[] buffer = new byte[Len.DATA_ELABORAZIONE_REPORT];
        return getDataElaborazioneReportBytes(buffer, 1);
    }

    public byte[] getDataElaborazioneReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, giorno, Len.GIORNO);
        position += Len.GIORNO;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, mese, Len.MESE);
        position += Len.MESE;
        MarshalByte.writeString(buffer, position, flr3, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, anno, Len.ANNO);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public void setGiorno(String giorno) {
        this.giorno = Functions.subString(giorno, Len.GIORNO);
    }

    public String getGiorno() {
        return this.giorno;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public void setMese(String mese) {
        this.mese = Functions.subString(mese, Len.MESE);
    }

    public String getMese() {
        return this.mese;
    }

    public String getFlr3() {
        return this.flr3;
    }

    public void setAnno(String anno) {
        this.anno = Functions.subString(anno, Len.ANNO);
    }

    public String getAnno() {
        return this.anno;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 26;
        public static final int GIORNO = 2;
        public static final int FLR2 = 3;
        public static final int MESE = 2;
        public static final int ANNO = 4;
        public static final int DATA_ELABORAZIONE_REPORT = GIORNO + MESE + ANNO + FLR1 + 2 * FLR2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
