package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: TRCH-DI-GAR-DB<br>
 * Variable: TRCH-DI-GAR-DB from copybook IDBVTGA3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TrchDiGarDb {

    //==== PROPERTIES ====
    //Original name: TGA-DT-INI-EFF-DB
    private String iniEffDb = DefaultValues.stringVal(Len.INI_EFF_DB);
    //Original name: TGA-DT-END-EFF-DB
    private String endEffDb = DefaultValues.stringVal(Len.END_EFF_DB);
    //Original name: TGA-DT-DECOR-DB
    private String decorDb = DefaultValues.stringVal(Len.DECOR_DB);
    //Original name: TGA-DT-SCAD-DB
    private String scadDb = DefaultValues.stringVal(Len.SCAD_DB);
    //Original name: TGA-DT-EMIS-DB
    private String emisDb = DefaultValues.stringVal(Len.EMIS_DB);
    //Original name: TGA-DT-EFF-STAB-DB
    private String effStabDb = DefaultValues.stringVal(Len.EFF_STAB_DB);
    //Original name: TGA-DT-VLDT-PROD-DB
    private String vldtProdDb = DefaultValues.stringVal(Len.VLDT_PROD_DB);
    //Original name: TGA-DT-INI-VAL-TAR-DB
    private String iniValTarDb = DefaultValues.stringVal(Len.INI_VAL_TAR_DB);
    //Original name: TGA-DT-ULT-ADEG-PRE-PR-DB
    private String ultAdegPrePrDb = DefaultValues.stringVal(Len.ULT_ADEG_PRE_PR_DB);

    //==== METHODS ====
    public void setIniEffDb(String iniEffDb) {
        this.iniEffDb = Functions.subString(iniEffDb, Len.INI_EFF_DB);
    }

    public String getIniEffDb() {
        return this.iniEffDb;
    }

    public void setEndEffDb(String endEffDb) {
        this.endEffDb = Functions.subString(endEffDb, Len.END_EFF_DB);
    }

    public String getEndEffDb() {
        return this.endEffDb;
    }

    public void setDecorDb(String decorDb) {
        this.decorDb = Functions.subString(decorDb, Len.DECOR_DB);
    }

    public String getDecorDb() {
        return this.decorDb;
    }

    public void setScadDb(String scadDb) {
        this.scadDb = Functions.subString(scadDb, Len.SCAD_DB);
    }

    public String getScadDb() {
        return this.scadDb;
    }

    public void setEmisDb(String emisDb) {
        this.emisDb = Functions.subString(emisDb, Len.EMIS_DB);
    }

    public String getEmisDb() {
        return this.emisDb;
    }

    public void setEffStabDb(String effStabDb) {
        this.effStabDb = Functions.subString(effStabDb, Len.EFF_STAB_DB);
    }

    public String getEffStabDb() {
        return this.effStabDb;
    }

    public void setVldtProdDb(String vldtProdDb) {
        this.vldtProdDb = Functions.subString(vldtProdDb, Len.VLDT_PROD_DB);
    }

    public String getVldtProdDb() {
        return this.vldtProdDb;
    }

    public void setIniValTarDb(String iniValTarDb) {
        this.iniValTarDb = Functions.subString(iniValTarDb, Len.INI_VAL_TAR_DB);
    }

    public String getIniValTarDb() {
        return this.iniValTarDb;
    }

    public void setUltAdegPrePrDb(String ultAdegPrePrDb) {
        this.ultAdegPrePrDb = Functions.subString(ultAdegPrePrDb, Len.ULT_ADEG_PRE_PR_DB);
    }

    public String getUltAdegPrePrDb() {
        return this.ultAdegPrePrDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INI_EFF_DB = 10;
        public static final int END_EFF_DB = 10;
        public static final int DECOR_DB = 10;
        public static final int SCAD_DB = 10;
        public static final int EMIS_DB = 10;
        public static final int EFF_STAB_DB = 10;
        public static final int VLDT_PROD_DB = 10;
        public static final int INI_VAL_TAR_DB = 10;
        public static final int ULT_ADEG_PRE_PR_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
