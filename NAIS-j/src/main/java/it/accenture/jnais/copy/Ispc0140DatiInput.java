package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Ispc0140RicalcQuiet;
import it.accenture.jnais.ws.occurs.Ispc0140SchedaP;
import it.accenture.jnais.ws.occurs.Ispc0140SchedaT;

/**Original name: ISPC0140-DATI-INPUT<br>
 * Variable: ISPC0140-DATI-INPUT from copybook ISPC0140<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ispc0140DatiInput {

    //==== PROPERTIES ====
    public static final int SCHEDA_P_MAXOCCURS = 1;
    public static final int SCHEDA_T_MAXOCCURS = 20;
    //Original name: ISPC0140-COD-COMPAGNIA
    private String codCompagnia = DefaultValues.stringVal(Len.COD_COMPAGNIA);
    //Original name: ISPC0140-COD-PRODOTTO
    private String codProdotto = DefaultValues.stringVal(Len.COD_PRODOTTO);
    //Original name: ISPC0140-COD-CONVENZIONE
    private String codConvenzione = DefaultValues.stringVal(Len.COD_CONVENZIONE);
    //Original name: ISPC0140-DATA-INIZ-VALID-CONV
    private String dataInizValidConv = DefaultValues.stringVal(Len.DATA_INIZ_VALID_CONV);
    //Original name: ISPC0140-FUNZIONALITA
    private String funzionalita = DefaultValues.stringVal(Len.FUNZIONALITA);
    //Original name: ISPC0140-DATA-RIFERIMENTO
    private String dataRiferimento = DefaultValues.stringVal(Len.DATA_RIFERIMENTO);
    //Original name: ISPC0140-LIVELLO-UTENTE
    private String livelloUtente = DefaultValues.stringVal(Len.LIVELLO_UTENTE);
    //Original name: ISPC0140-SESSION-ID
    private String sessionId = DefaultValues.stringVal(Len.SESSION_ID);
    //Original name: ISPC0140-DATA-DECORR-POLIZZA
    private String dataDecorrPolizza = DefaultValues.stringVal(Len.DATA_DECORR_POLIZZA);
    //Original name: ISPC0140-MODALITA-CALCOLO
    private char modalitaCalcolo = DefaultValues.CHAR_VAL;
    //Original name: ISPC0140-RICALC-QUIET
    private Ispc0140RicalcQuiet ricalcQuiet = new Ispc0140RicalcQuiet();
    //Original name: ISPC0140-DEE
    private String dee = DefaultValues.stringVal(Len.DEE);
    /**Original name: ISPC0140-ELE-MAX-SCHEDA-P<br>
	 * <pre>----------------------------------------------------------------*
	 *    SCHEDE PRODOTTO
	 * ----------------------------------------------------------------*</pre>*/
    private String eleMaxSchedaP = DefaultValues.stringVal(Len.ELE_MAX_SCHEDA_P);
    //Original name: ISPC0140-SCHEDA-P
    private Ispc0140SchedaP[] schedaP = new Ispc0140SchedaP[SCHEDA_P_MAXOCCURS];
    /**Original name: ISPC0140-ELE-MAX-SCHEDA-T<br>
	 * <pre>----------------------------------------------------------------*
	 *    SCHEDE TRANCHE
	 * ----------------------------------------------------------------*</pre>*/
    private String eleMaxSchedaT = DefaultValues.stringVal(Len.ELE_MAX_SCHEDA_T);
    //Original name: ISPC0140-SCHEDA-T
    private Ispc0140SchedaT[] schedaT = new Ispc0140SchedaT[SCHEDA_T_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ispc0140DatiInput() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int schedaPIdx = 1; schedaPIdx <= SCHEDA_P_MAXOCCURS; schedaPIdx++) {
            schedaP[schedaPIdx - 1] = new Ispc0140SchedaP();
        }
        for (int schedaTIdx = 1; schedaTIdx <= SCHEDA_T_MAXOCCURS; schedaTIdx++) {
            schedaT[schedaTIdx - 1] = new Ispc0140SchedaT();
        }
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        codCompagnia = MarshalByte.readFixedString(buffer, position, Len.COD_COMPAGNIA);
        position += Len.COD_COMPAGNIA;
        codProdotto = MarshalByte.readString(buffer, position, Len.COD_PRODOTTO);
        position += Len.COD_PRODOTTO;
        codConvenzione = MarshalByte.readString(buffer, position, Len.COD_CONVENZIONE);
        position += Len.COD_CONVENZIONE;
        dataInizValidConv = MarshalByte.readString(buffer, position, Len.DATA_INIZ_VALID_CONV);
        position += Len.DATA_INIZ_VALID_CONV;
        funzionalita = MarshalByte.readFixedString(buffer, position, Len.FUNZIONALITA);
        position += Len.FUNZIONALITA;
        dataRiferimento = MarshalByte.readString(buffer, position, Len.DATA_RIFERIMENTO);
        position += Len.DATA_RIFERIMENTO;
        livelloUtente = MarshalByte.readFixedString(buffer, position, Len.LIVELLO_UTENTE);
        position += Len.LIVELLO_UTENTE;
        sessionId = MarshalByte.readString(buffer, position, Len.SESSION_ID);
        position += Len.SESSION_ID;
        dataDecorrPolizza = MarshalByte.readString(buffer, position, Len.DATA_DECORR_POLIZZA);
        position += Len.DATA_DECORR_POLIZZA;
        modalitaCalcolo = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ricalcQuiet.setRicalcQuiet(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        dee = MarshalByte.readString(buffer, position, Len.DEE);
        position += Len.DEE;
        eleMaxSchedaP = MarshalByte.readFixedString(buffer, position, Len.ELE_MAX_SCHEDA_P);
        position += Len.ELE_MAX_SCHEDA_P;
        for (int idx = 1; idx <= SCHEDA_P_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                schedaP[idx - 1].setSchedaPBytes(buffer, position);
                position += Ispc0140SchedaP.Len.SCHEDA_P;
            }
            else {
                schedaP[idx - 1].initSchedaPSpaces();
                position += Ispc0140SchedaP.Len.SCHEDA_P;
            }
        }
        eleMaxSchedaT = MarshalByte.readFixedString(buffer, position, Len.ELE_MAX_SCHEDA_T);
        position += Len.ELE_MAX_SCHEDA_T;
        for (int idx = 1; idx <= SCHEDA_T_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                schedaT[idx - 1].setSchedaTBytes(buffer, position);
                position += Ispc0140SchedaT.Len.SCHEDA_T;
            }
            else {
                schedaT[idx - 1].initSchedaTSpaces();
                position += Ispc0140SchedaT.Len.SCHEDA_T;
            }
        }
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codCompagnia, Len.COD_COMPAGNIA);
        position += Len.COD_COMPAGNIA;
        MarshalByte.writeString(buffer, position, codProdotto, Len.COD_PRODOTTO);
        position += Len.COD_PRODOTTO;
        MarshalByte.writeString(buffer, position, codConvenzione, Len.COD_CONVENZIONE);
        position += Len.COD_CONVENZIONE;
        MarshalByte.writeString(buffer, position, dataInizValidConv, Len.DATA_INIZ_VALID_CONV);
        position += Len.DATA_INIZ_VALID_CONV;
        MarshalByte.writeString(buffer, position, funzionalita, Len.FUNZIONALITA);
        position += Len.FUNZIONALITA;
        MarshalByte.writeString(buffer, position, dataRiferimento, Len.DATA_RIFERIMENTO);
        position += Len.DATA_RIFERIMENTO;
        MarshalByte.writeString(buffer, position, livelloUtente, Len.LIVELLO_UTENTE);
        position += Len.LIVELLO_UTENTE;
        MarshalByte.writeString(buffer, position, sessionId, Len.SESSION_ID);
        position += Len.SESSION_ID;
        MarshalByte.writeString(buffer, position, dataDecorrPolizza, Len.DATA_DECORR_POLIZZA);
        position += Len.DATA_DECORR_POLIZZA;
        MarshalByte.writeChar(buffer, position, modalitaCalcolo);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, ricalcQuiet.getRicalcQuiet());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dee, Len.DEE);
        position += Len.DEE;
        MarshalByte.writeString(buffer, position, eleMaxSchedaP, Len.ELE_MAX_SCHEDA_P);
        position += Len.ELE_MAX_SCHEDA_P;
        for (int idx = 1; idx <= SCHEDA_P_MAXOCCURS; idx++) {
            schedaP[idx - 1].getSchedaPBytes(buffer, position);
            position += Ispc0140SchedaP.Len.SCHEDA_P;
        }
        MarshalByte.writeString(buffer, position, eleMaxSchedaT, Len.ELE_MAX_SCHEDA_T);
        position += Len.ELE_MAX_SCHEDA_T;
        for (int idx = 1; idx <= SCHEDA_T_MAXOCCURS; idx++) {
            schedaT[idx - 1].getSchedaTBytes(buffer, position);
            position += Ispc0140SchedaT.Len.SCHEDA_T;
        }
        return buffer;
    }

    public void setIspc0140CodCompagniaFormatted(String ispc0140CodCompagnia) {
        this.codCompagnia = Trunc.toUnsignedNumeric(ispc0140CodCompagnia, Len.COD_COMPAGNIA);
    }

    public int getIspc0140CodCompagnia() {
        return NumericDisplay.asInt(this.codCompagnia);
    }

    public void setCodProdotto(String codProdotto) {
        this.codProdotto = Functions.subString(codProdotto, Len.COD_PRODOTTO);
    }

    public String getCodProdotto() {
        return this.codProdotto;
    }

    public void setCodConvenzione(String codConvenzione) {
        this.codConvenzione = Functions.subString(codConvenzione, Len.COD_CONVENZIONE);
    }

    public String getCodConvenzione() {
        return this.codConvenzione;
    }

    public void setDataInizValidConv(String dataInizValidConv) {
        this.dataInizValidConv = Functions.subString(dataInizValidConv, Len.DATA_INIZ_VALID_CONV);
    }

    public String getDataInizValidConv() {
        return this.dataInizValidConv;
    }

    public void setIspc0140FunzionalitaFormatted(String ispc0140Funzionalita) {
        this.funzionalita = Trunc.toUnsignedNumeric(ispc0140Funzionalita, Len.FUNZIONALITA);
    }

    public int getIspc0140Funzionalita() {
        return NumericDisplay.asInt(this.funzionalita);
    }

    public void setDataRiferimento(String dataRiferimento) {
        this.dataRiferimento = Functions.subString(dataRiferimento, Len.DATA_RIFERIMENTO);
    }

    public String getDataRiferimento() {
        return this.dataRiferimento;
    }

    public void setIspc0140LivelloUtente(short ispc0140LivelloUtente) {
        this.livelloUtente = NumericDisplay.asString(ispc0140LivelloUtente, Len.LIVELLO_UTENTE);
    }

    public short getIspc0140LivelloUtente() {
        return NumericDisplay.asShort(this.livelloUtente);
    }

    public void setSessionId(String sessionId) {
        this.sessionId = Functions.subString(sessionId, Len.SESSION_ID);
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setDataDecorrPolizza(String dataDecorrPolizza) {
        this.dataDecorrPolizza = Functions.subString(dataDecorrPolizza, Len.DATA_DECORR_POLIZZA);
    }

    public String getDataDecorrPolizza() {
        return this.dataDecorrPolizza;
    }

    public void setModalitaCalcolo(char modalitaCalcolo) {
        this.modalitaCalcolo = modalitaCalcolo;
    }

    public void setIspc0140ModalitaCalcoloFormatted(String ispc0140ModalitaCalcolo) {
        setModalitaCalcolo(Functions.charAt(ispc0140ModalitaCalcolo, Types.CHAR_SIZE));
    }

    public char getModalitaCalcolo() {
        return this.modalitaCalcolo;
    }

    public void setDee(String dee) {
        this.dee = Functions.subString(dee, Len.DEE);
    }

    public String getDee() {
        return this.dee;
    }

    public void setIspc0140EleMaxSchedaP(short ispc0140EleMaxSchedaP) {
        this.eleMaxSchedaP = NumericDisplay.asString(ispc0140EleMaxSchedaP, Len.ELE_MAX_SCHEDA_P);
    }

    public short getIspc0140EleMaxSchedaP() {
        return NumericDisplay.asShort(this.eleMaxSchedaP);
    }

    public void setIspc0140EleMaxSchedaT(short ispc0140EleMaxSchedaT) {
        this.eleMaxSchedaT = NumericDisplay.asString(ispc0140EleMaxSchedaT, Len.ELE_MAX_SCHEDA_T);
    }

    public short getIspc0140EleMaxSchedaT() {
        return NumericDisplay.asShort(this.eleMaxSchedaT);
    }

    public Ispc0140SchedaP getSchedaP(int idx) {
        return schedaP[idx - 1];
    }

    public Ispc0140SchedaT getSchedaT(int idx) {
        return schedaT[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_COMPAGNIA = 5;
        public static final int COD_PRODOTTO = 12;
        public static final int COD_CONVENZIONE = 12;
        public static final int DATA_INIZ_VALID_CONV = 8;
        public static final int FUNZIONALITA = 5;
        public static final int DATA_RIFERIMENTO = 8;
        public static final int LIVELLO_UTENTE = 2;
        public static final int SESSION_ID = 20;
        public static final int DATA_DECORR_POLIZZA = 8;
        public static final int MODALITA_CALCOLO = 1;
        public static final int DEE = 8;
        public static final int ELE_MAX_SCHEDA_P = 4;
        public static final int ELE_MAX_SCHEDA_T = 4;
        public static final int DATI_INPUT = COD_COMPAGNIA + COD_PRODOTTO + COD_CONVENZIONE + DATA_INIZ_VALID_CONV + FUNZIONALITA + DATA_RIFERIMENTO + LIVELLO_UTENTE + SESSION_ID + DATA_DECORR_POLIZZA + MODALITA_CALCOLO + Ispc0140RicalcQuiet.Len.RICALC_QUIET + DEE + ELE_MAX_SCHEDA_P + Ispc0140DatiInput.SCHEDA_P_MAXOCCURS * Ispc0140SchedaP.Len.SCHEDA_P + ELE_MAX_SCHEDA_T + Ispc0140DatiInput.SCHEDA_T_MAXOCCURS * Ispc0140SchedaT.Len.SCHEDA_T;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
