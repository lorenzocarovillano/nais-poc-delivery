package it.accenture.jnais.copy;

/**Original name: TIPO-FORMATO<br>
 * Variable: TIPO-FORMATO from copybook IDSV0501<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TipoFormato {

    //==== PROPERTIES ====
    //Original name: IMPORTO
    private char importo = 'I';
    //Original name: NUMERICO
    private char numerico = 'N';
    //Original name: MILLESIMI
    private char millesimi = 'M';
    //Original name: PERCENTUALE
    private char percentuale = 'P';
    //Original name: DT
    private char dt = 'D';
    //Original name: STRINGA
    private char stringa = 'S';
    //Original name: TASSO
    private char tasso = 'T';
    /**Original name: LISTA-IMPORTO<br>
	 * <pre> SIR 20310 GESTIONE LISTE OMOGENEE</pre>*/
    private char listaImporto = 'R';
    //Original name: LISTA-NUMERICO
    private char listaNumerico = 'H';
    //Original name: LISTA-MILLESIMI
    private char listaMillesimi = 'Z';
    //Original name: LISTA-PERCENTUALE
    private char listaPercentuale = 'O';
    //Original name: LISTA-DT
    private char listaDt = 'C';
    //Original name: LISTA-STRINGA
    private char listaStringa = 'L';
    //Original name: LISTA-TASSO
    private char listaTasso = 'T';

    //==== METHODS ====
    public char getImporto() {
        return this.importo;
    }

    public char getNumerico() {
        return this.numerico;
    }

    public char getMillesimi() {
        return this.millesimi;
    }

    public char getPercentuale() {
        return this.percentuale;
    }

    public char getDt() {
        return this.dt;
    }

    public char getStringa() {
        return this.stringa;
    }

    public char getTasso() {
        return this.tasso;
    }

    public char getListaImporto() {
        return this.listaImporto;
    }

    public char getListaNumerico() {
        return this.listaNumerico;
    }

    public char getListaMillesimi() {
        return this.listaMillesimi;
    }

    public char getListaPercentuale() {
        return this.listaPercentuale;
    }

    public char getListaDt() {
        return this.listaDt;
    }

    public char getListaStringa() {
        return this.listaStringa;
    }

    public char getListaTasso() {
        return this.listaTasso;
    }
}
