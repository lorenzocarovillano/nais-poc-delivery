package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpvtAlqCommisInter;
import it.accenture.jnais.ws.redefines.WpvtAlqProvAcq;
import it.accenture.jnais.ws.redefines.WpvtAlqProvInc;
import it.accenture.jnais.ws.redefines.WpvtAlqProvRicor;
import it.accenture.jnais.ws.redefines.WpvtAlqRemunAss;
import it.accenture.jnais.ws.redefines.WpvtCommisInter;
import it.accenture.jnais.ws.redefines.WpvtIdGar;
import it.accenture.jnais.ws.redefines.WpvtIdMoviChiu;
import it.accenture.jnais.ws.redefines.WpvtProv1aaAcq;
import it.accenture.jnais.ws.redefines.WpvtProv2aaAcq;
import it.accenture.jnais.ws.redefines.WpvtProvInc;
import it.accenture.jnais.ws.redefines.WpvtProvRicor;
import it.accenture.jnais.ws.redefines.WpvtRemunAss;

/**Original name: WPVT-DATI<br>
 * Variable: WPVT-DATI from copybook LCCVPVT1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WpvtDati {

    //==== PROPERTIES ====
    //Original name: WPVT-ID-PROV-DI-GAR
    private int wpvtIdProvDiGar = DefaultValues.INT_VAL;
    //Original name: WPVT-ID-GAR
    private WpvtIdGar wpvtIdGar = new WpvtIdGar();
    //Original name: WPVT-ID-MOVI-CRZ
    private int wpvtIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WPVT-ID-MOVI-CHIU
    private WpvtIdMoviChiu wpvtIdMoviChiu = new WpvtIdMoviChiu();
    //Original name: WPVT-DT-INI-EFF
    private int wpvtDtIniEff = DefaultValues.INT_VAL;
    //Original name: WPVT-DT-END-EFF
    private int wpvtDtEndEff = DefaultValues.INT_VAL;
    //Original name: WPVT-COD-COMP-ANIA
    private int wpvtCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WPVT-TP-PROV
    private String wpvtTpProv = DefaultValues.stringVal(Len.WPVT_TP_PROV);
    //Original name: WPVT-PROV-1AA-ACQ
    private WpvtProv1aaAcq wpvtProv1aaAcq = new WpvtProv1aaAcq();
    //Original name: WPVT-PROV-2AA-ACQ
    private WpvtProv2aaAcq wpvtProv2aaAcq = new WpvtProv2aaAcq();
    //Original name: WPVT-PROV-RICOR
    private WpvtProvRicor wpvtProvRicor = new WpvtProvRicor();
    //Original name: WPVT-PROV-INC
    private WpvtProvInc wpvtProvInc = new WpvtProvInc();
    //Original name: WPVT-ALQ-PROV-ACQ
    private WpvtAlqProvAcq wpvtAlqProvAcq = new WpvtAlqProvAcq();
    //Original name: WPVT-ALQ-PROV-INC
    private WpvtAlqProvInc wpvtAlqProvInc = new WpvtAlqProvInc();
    //Original name: WPVT-ALQ-PROV-RICOR
    private WpvtAlqProvRicor wpvtAlqProvRicor = new WpvtAlqProvRicor();
    //Original name: WPVT-FL-STOR-PROV-ACQ
    private char wpvtFlStorProvAcq = DefaultValues.CHAR_VAL;
    //Original name: WPVT-FL-REC-PROV-STORN
    private char wpvtFlRecProvStorn = DefaultValues.CHAR_VAL;
    //Original name: WPVT-FL-PROV-FORZ
    private char wpvtFlProvForz = DefaultValues.CHAR_VAL;
    //Original name: WPVT-TP-CALC-PROV
    private char wpvtTpCalcProv = DefaultValues.CHAR_VAL;
    //Original name: WPVT-DS-RIGA
    private long wpvtDsRiga = DefaultValues.LONG_VAL;
    //Original name: WPVT-DS-OPER-SQL
    private char wpvtDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WPVT-DS-VER
    private int wpvtDsVer = DefaultValues.INT_VAL;
    //Original name: WPVT-DS-TS-INI-CPTZ
    private long wpvtDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WPVT-DS-TS-END-CPTZ
    private long wpvtDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WPVT-DS-UTENTE
    private String wpvtDsUtente = DefaultValues.stringVal(Len.WPVT_DS_UTENTE);
    //Original name: WPVT-DS-STATO-ELAB
    private char wpvtDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WPVT-REMUN-ASS
    private WpvtRemunAss wpvtRemunAss = new WpvtRemunAss();
    //Original name: WPVT-COMMIS-INTER
    private WpvtCommisInter wpvtCommisInter = new WpvtCommisInter();
    //Original name: WPVT-ALQ-REMUN-ASS
    private WpvtAlqRemunAss wpvtAlqRemunAss = new WpvtAlqRemunAss();
    //Original name: WPVT-ALQ-COMMIS-INTER
    private WpvtAlqCommisInter wpvtAlqCommisInter = new WpvtAlqCommisInter();

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wpvtIdProvDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPVT_ID_PROV_DI_GAR, 0);
        position += Len.WPVT_ID_PROV_DI_GAR;
        wpvtIdGar.setWpvtIdGarFromBuffer(buffer, position);
        position += WpvtIdGar.Len.WPVT_ID_GAR;
        wpvtIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPVT_ID_MOVI_CRZ, 0);
        position += Len.WPVT_ID_MOVI_CRZ;
        wpvtIdMoviChiu.setWpvtIdMoviChiuFromBuffer(buffer, position);
        position += WpvtIdMoviChiu.Len.WPVT_ID_MOVI_CHIU;
        wpvtDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPVT_DT_INI_EFF, 0);
        position += Len.WPVT_DT_INI_EFF;
        wpvtDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPVT_DT_END_EFF, 0);
        position += Len.WPVT_DT_END_EFF;
        wpvtCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPVT_COD_COMP_ANIA, 0);
        position += Len.WPVT_COD_COMP_ANIA;
        wpvtTpProv = MarshalByte.readString(buffer, position, Len.WPVT_TP_PROV);
        position += Len.WPVT_TP_PROV;
        wpvtProv1aaAcq.setWpvtProv1aaAcqFromBuffer(buffer, position);
        position += WpvtProv1aaAcq.Len.WPVT_PROV1AA_ACQ;
        wpvtProv2aaAcq.setWpvtProv2aaAcqFromBuffer(buffer, position);
        position += WpvtProv2aaAcq.Len.WPVT_PROV2AA_ACQ;
        wpvtProvRicor.setWpvtProvRicorFromBuffer(buffer, position);
        position += WpvtProvRicor.Len.WPVT_PROV_RICOR;
        wpvtProvInc.setWpvtProvIncFromBuffer(buffer, position);
        position += WpvtProvInc.Len.WPVT_PROV_INC;
        wpvtAlqProvAcq.setWpvtAlqProvAcqFromBuffer(buffer, position);
        position += WpvtAlqProvAcq.Len.WPVT_ALQ_PROV_ACQ;
        wpvtAlqProvInc.setWpvtAlqProvIncFromBuffer(buffer, position);
        position += WpvtAlqProvInc.Len.WPVT_ALQ_PROV_INC;
        wpvtAlqProvRicor.setWpvtAlqProvRicorFromBuffer(buffer, position);
        position += WpvtAlqProvRicor.Len.WPVT_ALQ_PROV_RICOR;
        wpvtFlStorProvAcq = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpvtFlRecProvStorn = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpvtFlProvForz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpvtTpCalcProv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpvtDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPVT_DS_RIGA, 0);
        position += Len.WPVT_DS_RIGA;
        wpvtDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpvtDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPVT_DS_VER, 0);
        position += Len.WPVT_DS_VER;
        wpvtDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPVT_DS_TS_INI_CPTZ, 0);
        position += Len.WPVT_DS_TS_INI_CPTZ;
        wpvtDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPVT_DS_TS_END_CPTZ, 0);
        position += Len.WPVT_DS_TS_END_CPTZ;
        wpvtDsUtente = MarshalByte.readString(buffer, position, Len.WPVT_DS_UTENTE);
        position += Len.WPVT_DS_UTENTE;
        wpvtDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpvtRemunAss.setWpvtRemunAssFromBuffer(buffer, position);
        position += WpvtRemunAss.Len.WPVT_REMUN_ASS;
        wpvtCommisInter.setWpvtCommisInterFromBuffer(buffer, position);
        position += WpvtCommisInter.Len.WPVT_COMMIS_INTER;
        wpvtAlqRemunAss.setWpvtAlqRemunAssFromBuffer(buffer, position);
        position += WpvtAlqRemunAss.Len.WPVT_ALQ_REMUN_ASS;
        wpvtAlqCommisInter.setWpvtAlqCommisInterFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wpvtIdProvDiGar, Len.Int.WPVT_ID_PROV_DI_GAR, 0);
        position += Len.WPVT_ID_PROV_DI_GAR;
        wpvtIdGar.getWpvtIdGarAsBuffer(buffer, position);
        position += WpvtIdGar.Len.WPVT_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, wpvtIdMoviCrz, Len.Int.WPVT_ID_MOVI_CRZ, 0);
        position += Len.WPVT_ID_MOVI_CRZ;
        wpvtIdMoviChiu.getWpvtIdMoviChiuAsBuffer(buffer, position);
        position += WpvtIdMoviChiu.Len.WPVT_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wpvtDtIniEff, Len.Int.WPVT_DT_INI_EFF, 0);
        position += Len.WPVT_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpvtDtEndEff, Len.Int.WPVT_DT_END_EFF, 0);
        position += Len.WPVT_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpvtCodCompAnia, Len.Int.WPVT_COD_COMP_ANIA, 0);
        position += Len.WPVT_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wpvtTpProv, Len.WPVT_TP_PROV);
        position += Len.WPVT_TP_PROV;
        wpvtProv1aaAcq.getWpvtProv1aaAcqAsBuffer(buffer, position);
        position += WpvtProv1aaAcq.Len.WPVT_PROV1AA_ACQ;
        wpvtProv2aaAcq.getWpvtProv2aaAcqAsBuffer(buffer, position);
        position += WpvtProv2aaAcq.Len.WPVT_PROV2AA_ACQ;
        wpvtProvRicor.getWpvtProvRicorAsBuffer(buffer, position);
        position += WpvtProvRicor.Len.WPVT_PROV_RICOR;
        wpvtProvInc.getWpvtProvIncAsBuffer(buffer, position);
        position += WpvtProvInc.Len.WPVT_PROV_INC;
        wpvtAlqProvAcq.getWpvtAlqProvAcqAsBuffer(buffer, position);
        position += WpvtAlqProvAcq.Len.WPVT_ALQ_PROV_ACQ;
        wpvtAlqProvInc.getWpvtAlqProvIncAsBuffer(buffer, position);
        position += WpvtAlqProvInc.Len.WPVT_ALQ_PROV_INC;
        wpvtAlqProvRicor.getWpvtAlqProvRicorAsBuffer(buffer, position);
        position += WpvtAlqProvRicor.Len.WPVT_ALQ_PROV_RICOR;
        MarshalByte.writeChar(buffer, position, wpvtFlStorProvAcq);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpvtFlRecProvStorn);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpvtFlProvForz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpvtTpCalcProv);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, wpvtDsRiga, Len.Int.WPVT_DS_RIGA, 0);
        position += Len.WPVT_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wpvtDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wpvtDsVer, Len.Int.WPVT_DS_VER, 0);
        position += Len.WPVT_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wpvtDsTsIniCptz, Len.Int.WPVT_DS_TS_INI_CPTZ, 0);
        position += Len.WPVT_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wpvtDsTsEndCptz, Len.Int.WPVT_DS_TS_END_CPTZ, 0);
        position += Len.WPVT_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wpvtDsUtente, Len.WPVT_DS_UTENTE);
        position += Len.WPVT_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wpvtDsStatoElab);
        position += Types.CHAR_SIZE;
        wpvtRemunAss.getWpvtRemunAssAsBuffer(buffer, position);
        position += WpvtRemunAss.Len.WPVT_REMUN_ASS;
        wpvtCommisInter.getWpvtCommisInterAsBuffer(buffer, position);
        position += WpvtCommisInter.Len.WPVT_COMMIS_INTER;
        wpvtAlqRemunAss.getWpvtAlqRemunAssAsBuffer(buffer, position);
        position += WpvtAlqRemunAss.Len.WPVT_ALQ_REMUN_ASS;
        wpvtAlqCommisInter.getWpvtAlqCommisInterAsBuffer(buffer, position);
        return buffer;
    }

    public void initDatiSpaces() {
        wpvtIdProvDiGar = Types.INVALID_INT_VAL;
        wpvtIdGar.initWpvtIdGarSpaces();
        wpvtIdMoviCrz = Types.INVALID_INT_VAL;
        wpvtIdMoviChiu.initWpvtIdMoviChiuSpaces();
        wpvtDtIniEff = Types.INVALID_INT_VAL;
        wpvtDtEndEff = Types.INVALID_INT_VAL;
        wpvtCodCompAnia = Types.INVALID_INT_VAL;
        wpvtTpProv = "";
        wpvtProv1aaAcq.initWpvtProv1aaAcqSpaces();
        wpvtProv2aaAcq.initWpvtProv2aaAcqSpaces();
        wpvtProvRicor.initWpvtProvRicorSpaces();
        wpvtProvInc.initWpvtProvIncSpaces();
        wpvtAlqProvAcq.initWpvtAlqProvAcqSpaces();
        wpvtAlqProvInc.initWpvtAlqProvIncSpaces();
        wpvtAlqProvRicor.initWpvtAlqProvRicorSpaces();
        wpvtFlStorProvAcq = Types.SPACE_CHAR;
        wpvtFlRecProvStorn = Types.SPACE_CHAR;
        wpvtFlProvForz = Types.SPACE_CHAR;
        wpvtTpCalcProv = Types.SPACE_CHAR;
        wpvtDsRiga = Types.INVALID_LONG_VAL;
        wpvtDsOperSql = Types.SPACE_CHAR;
        wpvtDsVer = Types.INVALID_INT_VAL;
        wpvtDsTsIniCptz = Types.INVALID_LONG_VAL;
        wpvtDsTsEndCptz = Types.INVALID_LONG_VAL;
        wpvtDsUtente = "";
        wpvtDsStatoElab = Types.SPACE_CHAR;
        wpvtRemunAss.initWpvtRemunAssSpaces();
        wpvtCommisInter.initWpvtCommisInterSpaces();
        wpvtAlqRemunAss.initWpvtAlqRemunAssSpaces();
        wpvtAlqCommisInter.initWpvtAlqCommisInterSpaces();
    }

    public void setWpvtIdProvDiGar(int wpvtIdProvDiGar) {
        this.wpvtIdProvDiGar = wpvtIdProvDiGar;
    }

    public int getWpvtIdProvDiGar() {
        return this.wpvtIdProvDiGar;
    }

    public void setWpvtIdMoviCrz(int wpvtIdMoviCrz) {
        this.wpvtIdMoviCrz = wpvtIdMoviCrz;
    }

    public int getWpvtIdMoviCrz() {
        return this.wpvtIdMoviCrz;
    }

    public void setWpvtDtIniEff(int wpvtDtIniEff) {
        this.wpvtDtIniEff = wpvtDtIniEff;
    }

    public int getWpvtDtIniEff() {
        return this.wpvtDtIniEff;
    }

    public void setWpvtDtEndEff(int wpvtDtEndEff) {
        this.wpvtDtEndEff = wpvtDtEndEff;
    }

    public int getWpvtDtEndEff() {
        return this.wpvtDtEndEff;
    }

    public void setWpvtCodCompAnia(int wpvtCodCompAnia) {
        this.wpvtCodCompAnia = wpvtCodCompAnia;
    }

    public int getWpvtCodCompAnia() {
        return this.wpvtCodCompAnia;
    }

    public void setWpvtTpProv(String wpvtTpProv) {
        this.wpvtTpProv = Functions.subString(wpvtTpProv, Len.WPVT_TP_PROV);
    }

    public String getWpvtTpProv() {
        return this.wpvtTpProv;
    }

    public void setWpvtFlStorProvAcq(char wpvtFlStorProvAcq) {
        this.wpvtFlStorProvAcq = wpvtFlStorProvAcq;
    }

    public char getWpvtFlStorProvAcq() {
        return this.wpvtFlStorProvAcq;
    }

    public void setWpvtFlRecProvStorn(char wpvtFlRecProvStorn) {
        this.wpvtFlRecProvStorn = wpvtFlRecProvStorn;
    }

    public char getWpvtFlRecProvStorn() {
        return this.wpvtFlRecProvStorn;
    }

    public void setWpvtFlProvForz(char wpvtFlProvForz) {
        this.wpvtFlProvForz = wpvtFlProvForz;
    }

    public char getWpvtFlProvForz() {
        return this.wpvtFlProvForz;
    }

    public void setWpvtTpCalcProv(char wpvtTpCalcProv) {
        this.wpvtTpCalcProv = wpvtTpCalcProv;
    }

    public char getWpvtTpCalcProv() {
        return this.wpvtTpCalcProv;
    }

    public void setWpvtDsRiga(long wpvtDsRiga) {
        this.wpvtDsRiga = wpvtDsRiga;
    }

    public long getWpvtDsRiga() {
        return this.wpvtDsRiga;
    }

    public void setWpvtDsOperSql(char wpvtDsOperSql) {
        this.wpvtDsOperSql = wpvtDsOperSql;
    }

    public char getWpvtDsOperSql() {
        return this.wpvtDsOperSql;
    }

    public void setWpvtDsVer(int wpvtDsVer) {
        this.wpvtDsVer = wpvtDsVer;
    }

    public int getWpvtDsVer() {
        return this.wpvtDsVer;
    }

    public void setWpvtDsTsIniCptz(long wpvtDsTsIniCptz) {
        this.wpvtDsTsIniCptz = wpvtDsTsIniCptz;
    }

    public long getWpvtDsTsIniCptz() {
        return this.wpvtDsTsIniCptz;
    }

    public void setWpvtDsTsEndCptz(long wpvtDsTsEndCptz) {
        this.wpvtDsTsEndCptz = wpvtDsTsEndCptz;
    }

    public long getWpvtDsTsEndCptz() {
        return this.wpvtDsTsEndCptz;
    }

    public void setWpvtDsUtente(String wpvtDsUtente) {
        this.wpvtDsUtente = Functions.subString(wpvtDsUtente, Len.WPVT_DS_UTENTE);
    }

    public String getWpvtDsUtente() {
        return this.wpvtDsUtente;
    }

    public void setWpvtDsStatoElab(char wpvtDsStatoElab) {
        this.wpvtDsStatoElab = wpvtDsStatoElab;
    }

    public char getWpvtDsStatoElab() {
        return this.wpvtDsStatoElab;
    }

    public WpvtAlqCommisInter getWpvtAlqCommisInter() {
        return wpvtAlqCommisInter;
    }

    public WpvtAlqProvAcq getWpvtAlqProvAcq() {
        return wpvtAlqProvAcq;
    }

    public WpvtAlqProvInc getWpvtAlqProvInc() {
        return wpvtAlqProvInc;
    }

    public WpvtAlqProvRicor getWpvtAlqProvRicor() {
        return wpvtAlqProvRicor;
    }

    public WpvtAlqRemunAss getWpvtAlqRemunAss() {
        return wpvtAlqRemunAss;
    }

    public WpvtCommisInter getWpvtCommisInter() {
        return wpvtCommisInter;
    }

    public WpvtIdGar getWpvtIdGar() {
        return wpvtIdGar;
    }

    public WpvtIdMoviChiu getWpvtIdMoviChiu() {
        return wpvtIdMoviChiu;
    }

    public WpvtProv1aaAcq getWpvtProv1aaAcq() {
        return wpvtProv1aaAcq;
    }

    public WpvtProv2aaAcq getWpvtProv2aaAcq() {
        return wpvtProv2aaAcq;
    }

    public WpvtProvInc getWpvtProvInc() {
        return wpvtProvInc;
    }

    public WpvtProvRicor getWpvtProvRicor() {
        return wpvtProvRicor;
    }

    public WpvtRemunAss getWpvtRemunAss() {
        return wpvtRemunAss;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_ID_PROV_DI_GAR = 5;
        public static final int WPVT_ID_MOVI_CRZ = 5;
        public static final int WPVT_DT_INI_EFF = 5;
        public static final int WPVT_DT_END_EFF = 5;
        public static final int WPVT_COD_COMP_ANIA = 3;
        public static final int WPVT_TP_PROV = 2;
        public static final int WPVT_FL_STOR_PROV_ACQ = 1;
        public static final int WPVT_FL_REC_PROV_STORN = 1;
        public static final int WPVT_FL_PROV_FORZ = 1;
        public static final int WPVT_TP_CALC_PROV = 1;
        public static final int WPVT_DS_RIGA = 6;
        public static final int WPVT_DS_OPER_SQL = 1;
        public static final int WPVT_DS_VER = 5;
        public static final int WPVT_DS_TS_INI_CPTZ = 10;
        public static final int WPVT_DS_TS_END_CPTZ = 10;
        public static final int WPVT_DS_UTENTE = 20;
        public static final int WPVT_DS_STATO_ELAB = 1;
        public static final int DATI = WPVT_ID_PROV_DI_GAR + WpvtIdGar.Len.WPVT_ID_GAR + WPVT_ID_MOVI_CRZ + WpvtIdMoviChiu.Len.WPVT_ID_MOVI_CHIU + WPVT_DT_INI_EFF + WPVT_DT_END_EFF + WPVT_COD_COMP_ANIA + WPVT_TP_PROV + WpvtProv1aaAcq.Len.WPVT_PROV1AA_ACQ + WpvtProv2aaAcq.Len.WPVT_PROV2AA_ACQ + WpvtProvRicor.Len.WPVT_PROV_RICOR + WpvtProvInc.Len.WPVT_PROV_INC + WpvtAlqProvAcq.Len.WPVT_ALQ_PROV_ACQ + WpvtAlqProvInc.Len.WPVT_ALQ_PROV_INC + WpvtAlqProvRicor.Len.WPVT_ALQ_PROV_RICOR + WPVT_FL_STOR_PROV_ACQ + WPVT_FL_REC_PROV_STORN + WPVT_FL_PROV_FORZ + WPVT_TP_CALC_PROV + WPVT_DS_RIGA + WPVT_DS_OPER_SQL + WPVT_DS_VER + WPVT_DS_TS_INI_CPTZ + WPVT_DS_TS_END_CPTZ + WPVT_DS_UTENTE + WPVT_DS_STATO_ELAB + WpvtRemunAss.Len.WPVT_REMUN_ASS + WpvtCommisInter.Len.WPVT_COMMIS_INTER + WpvtAlqRemunAss.Len.WPVT_ALQ_REMUN_ASS + WpvtAlqCommisInter.Len.WPVT_ALQ_COMMIS_INTER;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_ID_PROV_DI_GAR = 9;
            public static final int WPVT_ID_MOVI_CRZ = 9;
            public static final int WPVT_DT_INI_EFF = 8;
            public static final int WPVT_DT_END_EFF = 8;
            public static final int WPVT_COD_COMP_ANIA = 5;
            public static final int WPVT_DS_RIGA = 10;
            public static final int WPVT_DS_VER = 9;
            public static final int WPVT_DS_TS_INI_CPTZ = 18;
            public static final int WPVT_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
