package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Ispc0040FlagRischioComune;
import it.accenture.jnais.ws.enums.Ispc0040TipoPolizza;
import it.accenture.jnais.ws.occurs.Ispc0040DifferimentoProroga;
import it.accenture.jnais.ws.occurs.Ispc0040Frazionamento;
import it.accenture.jnais.ws.occurs.Ispc0040ImpSconto;
import it.accenture.jnais.ws.occurs.Ispc0040ModalitaDurata;
import it.accenture.jnais.ws.occurs.Ispc0040ModCalcDur;
import it.accenture.jnais.ws.occurs.Ispc0040TipoAdesione;
import it.accenture.jnais.ws.occurs.Ispc0040TipoTrasformazione;

/**Original name: ISPC0040-DATI-OUTPUT1<br>
 * Variable: ISPC0040-DATI-OUTPUT1 from copybook ISPC0040<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ispc0040DatiOutput1 {

    //==== PROPERTIES ====
    public static final int TIPO_TRASFORMAZIONE_MAXOCCURS = 3;
    public static final int TIPO_ADESIONE_MAXOCCURS = 3;
    public static final int MOD_CALC_DUR_MAXOCCURS = 3;
    public static final int DIFFERIMENTO_PROROGA_MAXOCCURS = 2;
    public static final int MODALITA_DURATA_MAXOCCURS = 3;
    public static final int FRAZIONAMENTO_MAXOCCURS = 7;
    public static final int IMP_SCONTO_MAXOCCURS = 2;
    //Original name: ISPC0040-PRODOTTO
    private String prodotto = DefaultValues.stringVal(Len.PRODOTTO);
    //Original name: ISPC0040-DATA-VERSIONE-PROD
    private String dataVersioneProd = DefaultValues.stringVal(Len.DATA_VERSIONE_PROD);
    //Original name: ISPC0040-TIPO-VERSIONE-PROD
    private String tipoVersioneProd = DefaultValues.stringVal(Len.TIPO_VERSIONE_PROD);
    //Original name: ISPC0040-CODICE-DIVISA
    private String codiceDivisa = DefaultValues.stringVal(Len.CODICE_DIVISA);
    //Original name: ISPC0040-DESCR-DIVISA
    private String descrDivisa = DefaultValues.stringVal(Len.DESCR_DIVISA);
    /**Original name: ISPC0040-TIPO-POLIZZA<br>
	 * <pre>--       Tipo Polizza</pre>*/
    private Ispc0040TipoPolizza tipoPolizza = new Ispc0040TipoPolizza();
    //Original name: ISPC0040-DESC-POLIZZA
    private String descPolizza = DefaultValues.stringVal(Len.DESC_POLIZZA);
    /**Original name: ISPC0040-TP-TRASF-NUM-ELE<br>
	 * <pre>--       Tipo Trasformazione</pre>*/
    private String tpTrasfNumEle = DefaultValues.stringVal(Len.TP_TRASF_NUM_ELE);
    //Original name: ISPC0040-TIPO-TRASFORMAZIONE
    private Ispc0040TipoTrasformazione[] tipoTrasformazione = new Ispc0040TipoTrasformazione[TIPO_TRASFORMAZIONE_MAXOCCURS];
    /**Original name: ISPC0040-TP-ADE-NUM-ELE<br>
	 * <pre>--       Tipo Adesione</pre>*/
    private String tpAdeNumEle = DefaultValues.stringVal(Len.TP_ADE_NUM_ELE);
    //Original name: ISPC0040-TIPO-ADESIONE
    private Ispc0040TipoAdesione[] tipoAdesione = new Ispc0040TipoAdesione[TIPO_ADESIONE_MAXOCCURS];
    /**Original name: ISPC0040-NUM-ELE-MOD-CAL-DUR<br>
	 * <pre>--       Modalita di calcolo</pre>*/
    private String numEleModCalDur = DefaultValues.stringVal(Len.NUM_ELE_MOD_CAL_DUR);
    //Original name: ISPC0040-MOD-CALC-DUR
    private Ispc0040ModCalcDur[] modCalcDur = new Ispc0040ModCalcDur[MOD_CALC_DUR_MAXOCCURS];
    /**Original name: ISPC0040-FLAG-RISCHIO-COMUNE<br>
	 * <pre>--       Rischio Comune</pre>*/
    private Ispc0040FlagRischioComune flagRischioComune = new Ispc0040FlagRischioComune();
    /**Original name: ISPC0040-NUM-ELE-DIFF-PRO<br>
	 * <pre>--       Differimento Proroga</pre>*/
    private String numEleDiffPro = DefaultValues.stringVal(Len.NUM_ELE_DIFF_PRO);
    //Original name: ISPC0040-DIFFERIMENTO-PROROGA
    private Ispc0040DifferimentoProroga[] differimentoProroga = new Ispc0040DifferimentoProroga[DIFFERIMENTO_PROROGA_MAXOCCURS];
    //Original name: ISPC0040-ANNI-DIFFER-PROR
    private String anniDifferPror = DefaultValues.stringVal(Len.ANNI_DIFFER_PROR);
    //Original name: ISPC0040-TRATT-AA-DIFF-PROR
    private char trattAaDiffPror = DefaultValues.CHAR_VAL;
    /**Original name: ISPC0040-NUM-ELE-MOD-DUR<br>
	 * <pre>--       Modalita durata Adesione</pre>*/
    private String numEleModDur = DefaultValues.stringVal(Len.NUM_ELE_MOD_DUR);
    //Original name: ISPC0040-MODALITA-DURATA
    private Ispc0040ModalitaDurata[] modalitaDurata = new Ispc0040ModalitaDurata[MODALITA_DURATA_MAXOCCURS];
    /**Original name: ISPC0040-ETA-SCAD-MASCHI-DFLT<br>
	 * <pre>--       Dati Default Prodotto</pre>*/
    private String etaScadMaschiDflt = DefaultValues.stringVal(Len.ETA_SCAD_MASCHI_DFLT);
    //Original name: ISPC0040-TRATT-ETA-SCAD-MASCHI
    private char trattEtaScadMaschi = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-ETA-SCAD-FEMMINE-DFLT
    private String etaScadFemmineDflt = DefaultValues.stringVal(Len.ETA_SCAD_FEMMINE_DFLT);
    //Original name: ISPC0040-TRATT-ETA-SCAD-FEMM
    private char trattEtaScadFemm = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-DATA-SCADENZA-DFLT
    private String dataScadenzaDflt = DefaultValues.stringVal(Len.DATA_SCADENZA_DFLT);
    //Original name: ISPC0040-TRATT-DATA-SCAD-FEMM
    private char trattDataScadFemm = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-DURATA-ANNI-DEFAULT
    private String durataAnniDefault = DefaultValues.stringVal(Len.DURATA_ANNI_DEFAULT);
    //Original name: ISPC0040-TRATT-DURATA-ANNI
    private char trattDurataAnni = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-DURATA-MESI-DEFAULT
    private String durataMesiDefault = DefaultValues.stringVal(Len.DURATA_MESI_DEFAULT);
    //Original name: ISPC0040-TRATT-DURATA-MESI
    private char trattDurataMesi = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-DURATA-GIORNI-DEFAULT
    private String durataGiorniDefault = DefaultValues.stringVal(Len.DURATA_GIORNI_DEFAULT);
    //Original name: ISPC0040-TRATT-DURATA-GIORNI
    private char trattDurataGiorni = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-IND-MAX-FRAZ
    private String indMaxFraz = DefaultValues.stringVal(Len.IND_MAX_FRAZ);
    //Original name: ISPC0040-FRAZIONAMENTO
    private Ispc0040Frazionamento[] frazionamento = new Ispc0040Frazionamento[FRAZIONAMENTO_MAXOCCURS];
    //Original name: ISPC0040-TRATTAMENTO-FRAZ
    private char trattamentoFraz = DefaultValues.CHAR_VAL;
    /**Original name: ISPC0040-IND-MAX-IMP-SCO<br>
	 * <pre>--       Sconto</pre>*/
    private String indMaxImpSco = DefaultValues.stringVal(Len.IND_MAX_IMP_SCO);
    //Original name: ISPC0040-IMP-SCONTO
    private Ispc0040ImpSconto[] impSconto = new Ispc0040ImpSconto[IMP_SCONTO_MAXOCCURS];
    //Original name: ISPC0040-TRATTAMENTO-IMP-SCO
    private char trattamentoImpSco = DefaultValues.CHAR_VAL;
    /**Original name: ISPC0040-IND-MAX-FONDI<br>
	 * <pre>--       Lista Fondi</pre>*/
    private String indMaxFondi = DefaultValues.stringVal(Len.IND_MAX_FONDI);

    //==== CONSTRUCTORS ====
    public Ispc0040DatiOutput1() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tipoTrasformazioneIdx = 1; tipoTrasformazioneIdx <= TIPO_TRASFORMAZIONE_MAXOCCURS; tipoTrasformazioneIdx++) {
            tipoTrasformazione[tipoTrasformazioneIdx - 1] = new Ispc0040TipoTrasformazione();
        }
        for (int tipoAdesioneIdx = 1; tipoAdesioneIdx <= TIPO_ADESIONE_MAXOCCURS; tipoAdesioneIdx++) {
            tipoAdesione[tipoAdesioneIdx - 1] = new Ispc0040TipoAdesione();
        }
        for (int modCalcDurIdx = 1; modCalcDurIdx <= MOD_CALC_DUR_MAXOCCURS; modCalcDurIdx++) {
            modCalcDur[modCalcDurIdx - 1] = new Ispc0040ModCalcDur();
        }
        for (int differimentoProrogaIdx = 1; differimentoProrogaIdx <= DIFFERIMENTO_PROROGA_MAXOCCURS; differimentoProrogaIdx++) {
            differimentoProroga[differimentoProrogaIdx - 1] = new Ispc0040DifferimentoProroga();
        }
        for (int modalitaDurataIdx = 1; modalitaDurataIdx <= MODALITA_DURATA_MAXOCCURS; modalitaDurataIdx++) {
            modalitaDurata[modalitaDurataIdx - 1] = new Ispc0040ModalitaDurata();
        }
        for (int frazionamentoIdx = 1; frazionamentoIdx <= FRAZIONAMENTO_MAXOCCURS; frazionamentoIdx++) {
            frazionamento[frazionamentoIdx - 1] = new Ispc0040Frazionamento();
        }
        for (int impScontoIdx = 1; impScontoIdx <= IMP_SCONTO_MAXOCCURS; impScontoIdx++) {
            impSconto[impScontoIdx - 1] = new Ispc0040ImpSconto();
        }
    }

    public void setIspc0040DatiOutput1Bytes(byte[] buffer, int offset) {
        int position = offset;
        prodotto = MarshalByte.readString(buffer, position, Len.PRODOTTO);
        position += Len.PRODOTTO;
        dataVersioneProd = MarshalByte.readString(buffer, position, Len.DATA_VERSIONE_PROD);
        position += Len.DATA_VERSIONE_PROD;
        tipoVersioneProd = MarshalByte.readString(buffer, position, Len.TIPO_VERSIONE_PROD);
        position += Len.TIPO_VERSIONE_PROD;
        codiceDivisa = MarshalByte.readString(buffer, position, Len.CODICE_DIVISA);
        position += Len.CODICE_DIVISA;
        descrDivisa = MarshalByte.readString(buffer, position, Len.DESCR_DIVISA);
        position += Len.DESCR_DIVISA;
        tipoPolizza.setTipoPolizza(MarshalByte.readString(buffer, position, Ispc0040TipoPolizza.Len.TIPO_POLIZZA));
        position += Ispc0040TipoPolizza.Len.TIPO_POLIZZA;
        descPolizza = MarshalByte.readString(buffer, position, Len.DESC_POLIZZA);
        position += Len.DESC_POLIZZA;
        tpTrasfNumEle = MarshalByte.readFixedString(buffer, position, Len.TP_TRASF_NUM_ELE);
        position += Len.TP_TRASF_NUM_ELE;
        for (int idx = 1; idx <= TIPO_TRASFORMAZIONE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tipoTrasformazione[idx - 1].setTipoTrasformazioneBytes(buffer, position);
                position += Ispc0040TipoTrasformazione.Len.TIPO_TRASFORMAZIONE;
            }
            else {
                tipoTrasformazione[idx - 1].initTipoTrasformazioneSpaces();
                position += Ispc0040TipoTrasformazione.Len.TIPO_TRASFORMAZIONE;
            }
        }
        tpAdeNumEle = MarshalByte.readFixedString(buffer, position, Len.TP_ADE_NUM_ELE);
        position += Len.TP_ADE_NUM_ELE;
        for (int idx = 1; idx <= TIPO_ADESIONE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tipoAdesione[idx - 1].setTipoAdesioneBytes(buffer, position);
                position += Ispc0040TipoAdesione.Len.TIPO_ADESIONE;
            }
            else {
                tipoAdesione[idx - 1].initTipoAdesioneSpaces();
                position += Ispc0040TipoAdesione.Len.TIPO_ADESIONE;
            }
        }
        numEleModCalDur = MarshalByte.readFixedString(buffer, position, Len.NUM_ELE_MOD_CAL_DUR);
        position += Len.NUM_ELE_MOD_CAL_DUR;
        for (int idx = 1; idx <= MOD_CALC_DUR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                modCalcDur[idx - 1].setModCalcDurBytes(buffer, position);
                position += Ispc0040ModCalcDur.Len.MOD_CALC_DUR;
            }
            else {
                modCalcDur[idx - 1].initModCalcDurSpaces();
                position += Ispc0040ModCalcDur.Len.MOD_CALC_DUR;
            }
        }
        flagRischioComune.setFlagRischioComune(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        numEleDiffPro = MarshalByte.readFixedString(buffer, position, Len.NUM_ELE_DIFF_PRO);
        position += Len.NUM_ELE_DIFF_PRO;
        for (int idx = 1; idx <= DIFFERIMENTO_PROROGA_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                differimentoProroga[idx - 1].setDifferimentoProrogaBytes(buffer, position);
                position += Ispc0040DifferimentoProroga.Len.DIFFERIMENTO_PROROGA;
            }
            else {
                differimentoProroga[idx - 1].initDifferimentoProrogaSpaces();
                position += Ispc0040DifferimentoProroga.Len.DIFFERIMENTO_PROROGA;
            }
        }
        anniDifferPror = MarshalByte.readFixedString(buffer, position, Len.ANNI_DIFFER_PROR);
        position += Len.ANNI_DIFFER_PROR;
        trattAaDiffPror = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        numEleModDur = MarshalByte.readFixedString(buffer, position, Len.NUM_ELE_MOD_DUR);
        position += Len.NUM_ELE_MOD_DUR;
        for (int idx = 1; idx <= MODALITA_DURATA_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                modalitaDurata[idx - 1].setModalitaDurataBytes(buffer, position);
                position += Ispc0040ModalitaDurata.Len.MODALITA_DURATA;
            }
            else {
                modalitaDurata[idx - 1].initModalitaDurataSpaces();
                position += Ispc0040ModalitaDurata.Len.MODALITA_DURATA;
            }
        }
        etaScadMaschiDflt = MarshalByte.readFixedString(buffer, position, Len.ETA_SCAD_MASCHI_DFLT);
        position += Len.ETA_SCAD_MASCHI_DFLT;
        trattEtaScadMaschi = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        etaScadFemmineDflt = MarshalByte.readFixedString(buffer, position, Len.ETA_SCAD_FEMMINE_DFLT);
        position += Len.ETA_SCAD_FEMMINE_DFLT;
        trattEtaScadFemm = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dataScadenzaDflt = MarshalByte.readString(buffer, position, Len.DATA_SCADENZA_DFLT);
        position += Len.DATA_SCADENZA_DFLT;
        trattDataScadFemm = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        durataAnniDefault = MarshalByte.readFixedString(buffer, position, Len.DURATA_ANNI_DEFAULT);
        position += Len.DURATA_ANNI_DEFAULT;
        trattDurataAnni = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        durataMesiDefault = MarshalByte.readFixedString(buffer, position, Len.DURATA_MESI_DEFAULT);
        position += Len.DURATA_MESI_DEFAULT;
        trattDurataMesi = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        durataGiorniDefault = MarshalByte.readFixedString(buffer, position, Len.DURATA_GIORNI_DEFAULT);
        position += Len.DURATA_GIORNI_DEFAULT;
        trattDurataGiorni = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        indMaxFraz = MarshalByte.readFixedString(buffer, position, Len.IND_MAX_FRAZ);
        position += Len.IND_MAX_FRAZ;
        for (int idx = 1; idx <= FRAZIONAMENTO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                frazionamento[idx - 1].setFrazionamentoBytes(buffer, position);
                position += Ispc0040Frazionamento.Len.FRAZIONAMENTO;
            }
            else {
                frazionamento[idx - 1].initFrazionamentoSpaces();
                position += Ispc0040Frazionamento.Len.FRAZIONAMENTO;
            }
        }
        trattamentoFraz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        indMaxImpSco = MarshalByte.readFixedString(buffer, position, Len.IND_MAX_IMP_SCO);
        position += Len.IND_MAX_IMP_SCO;
        for (int idx = 1; idx <= IMP_SCONTO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                impSconto[idx - 1].setImpScontoBytes(buffer, position);
                position += Ispc0040ImpSconto.Len.IMP_SCONTO;
            }
            else {
                impSconto[idx - 1].initImpScontoSpaces();
                position += Ispc0040ImpSconto.Len.IMP_SCONTO;
            }
        }
        trattamentoImpSco = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        indMaxFondi = MarshalByte.readFixedString(buffer, position, Len.IND_MAX_FONDI);
    }

    public byte[] getIspc0040DatiOutput1Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, prodotto, Len.PRODOTTO);
        position += Len.PRODOTTO;
        MarshalByte.writeString(buffer, position, dataVersioneProd, Len.DATA_VERSIONE_PROD);
        position += Len.DATA_VERSIONE_PROD;
        MarshalByte.writeString(buffer, position, tipoVersioneProd, Len.TIPO_VERSIONE_PROD);
        position += Len.TIPO_VERSIONE_PROD;
        MarshalByte.writeString(buffer, position, codiceDivisa, Len.CODICE_DIVISA);
        position += Len.CODICE_DIVISA;
        MarshalByte.writeString(buffer, position, descrDivisa, Len.DESCR_DIVISA);
        position += Len.DESCR_DIVISA;
        MarshalByte.writeString(buffer, position, tipoPolizza.getTipoPolizza(), Ispc0040TipoPolizza.Len.TIPO_POLIZZA);
        position += Ispc0040TipoPolizza.Len.TIPO_POLIZZA;
        MarshalByte.writeString(buffer, position, descPolizza, Len.DESC_POLIZZA);
        position += Len.DESC_POLIZZA;
        MarshalByte.writeString(buffer, position, tpTrasfNumEle, Len.TP_TRASF_NUM_ELE);
        position += Len.TP_TRASF_NUM_ELE;
        for (int idx = 1; idx <= TIPO_TRASFORMAZIONE_MAXOCCURS; idx++) {
            tipoTrasformazione[idx - 1].getTipoTrasformazioneBytes(buffer, position);
            position += Ispc0040TipoTrasformazione.Len.TIPO_TRASFORMAZIONE;
        }
        MarshalByte.writeString(buffer, position, tpAdeNumEle, Len.TP_ADE_NUM_ELE);
        position += Len.TP_ADE_NUM_ELE;
        for (int idx = 1; idx <= TIPO_ADESIONE_MAXOCCURS; idx++) {
            tipoAdesione[idx - 1].getTipoAdesioneBytes(buffer, position);
            position += Ispc0040TipoAdesione.Len.TIPO_ADESIONE;
        }
        MarshalByte.writeString(buffer, position, numEleModCalDur, Len.NUM_ELE_MOD_CAL_DUR);
        position += Len.NUM_ELE_MOD_CAL_DUR;
        for (int idx = 1; idx <= MOD_CALC_DUR_MAXOCCURS; idx++) {
            modCalcDur[idx - 1].getModCalcDurBytes(buffer, position);
            position += Ispc0040ModCalcDur.Len.MOD_CALC_DUR;
        }
        MarshalByte.writeChar(buffer, position, flagRischioComune.getFlagRischioComune());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, numEleDiffPro, Len.NUM_ELE_DIFF_PRO);
        position += Len.NUM_ELE_DIFF_PRO;
        for (int idx = 1; idx <= DIFFERIMENTO_PROROGA_MAXOCCURS; idx++) {
            differimentoProroga[idx - 1].getDifferimentoProrogaBytes(buffer, position);
            position += Ispc0040DifferimentoProroga.Len.DIFFERIMENTO_PROROGA;
        }
        MarshalByte.writeString(buffer, position, anniDifferPror, Len.ANNI_DIFFER_PROR);
        position += Len.ANNI_DIFFER_PROR;
        MarshalByte.writeChar(buffer, position, trattAaDiffPror);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, numEleModDur, Len.NUM_ELE_MOD_DUR);
        position += Len.NUM_ELE_MOD_DUR;
        for (int idx = 1; idx <= MODALITA_DURATA_MAXOCCURS; idx++) {
            modalitaDurata[idx - 1].getModalitaDurataBytes(buffer, position);
            position += Ispc0040ModalitaDurata.Len.MODALITA_DURATA;
        }
        MarshalByte.writeString(buffer, position, etaScadMaschiDflt, Len.ETA_SCAD_MASCHI_DFLT);
        position += Len.ETA_SCAD_MASCHI_DFLT;
        MarshalByte.writeChar(buffer, position, trattEtaScadMaschi);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, etaScadFemmineDflt, Len.ETA_SCAD_FEMMINE_DFLT);
        position += Len.ETA_SCAD_FEMMINE_DFLT;
        MarshalByte.writeChar(buffer, position, trattEtaScadFemm);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dataScadenzaDflt, Len.DATA_SCADENZA_DFLT);
        position += Len.DATA_SCADENZA_DFLT;
        MarshalByte.writeChar(buffer, position, trattDataScadFemm);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, durataAnniDefault, Len.DURATA_ANNI_DEFAULT);
        position += Len.DURATA_ANNI_DEFAULT;
        MarshalByte.writeChar(buffer, position, trattDurataAnni);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, durataMesiDefault, Len.DURATA_MESI_DEFAULT);
        position += Len.DURATA_MESI_DEFAULT;
        MarshalByte.writeChar(buffer, position, trattDurataMesi);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, durataGiorniDefault, Len.DURATA_GIORNI_DEFAULT);
        position += Len.DURATA_GIORNI_DEFAULT;
        MarshalByte.writeChar(buffer, position, trattDurataGiorni);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, indMaxFraz, Len.IND_MAX_FRAZ);
        position += Len.IND_MAX_FRAZ;
        for (int idx = 1; idx <= FRAZIONAMENTO_MAXOCCURS; idx++) {
            frazionamento[idx - 1].getFrazionamentoBytes(buffer, position);
            position += Ispc0040Frazionamento.Len.FRAZIONAMENTO;
        }
        MarshalByte.writeChar(buffer, position, trattamentoFraz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, indMaxImpSco, Len.IND_MAX_IMP_SCO);
        position += Len.IND_MAX_IMP_SCO;
        for (int idx = 1; idx <= IMP_SCONTO_MAXOCCURS; idx++) {
            impSconto[idx - 1].getImpScontoBytes(buffer, position);
            position += Ispc0040ImpSconto.Len.IMP_SCONTO;
        }
        MarshalByte.writeChar(buffer, position, trattamentoImpSco);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, indMaxFondi, Len.IND_MAX_FONDI);
        return buffer;
    }

    public void setProdotto(String prodotto) {
        this.prodotto = Functions.subString(prodotto, Len.PRODOTTO);
    }

    public String getProdotto() {
        return this.prodotto;
    }

    public void setDataVersioneProd(String dataVersioneProd) {
        this.dataVersioneProd = Functions.subString(dataVersioneProd, Len.DATA_VERSIONE_PROD);
    }

    public String getDataVersioneProd() {
        return this.dataVersioneProd;
    }

    public String getIspc0040DataVersioneProdFormatted() {
        return Functions.padBlanks(getDataVersioneProd(), Len.DATA_VERSIONE_PROD);
    }

    public void setTipoVersioneProd(String tipoVersioneProd) {
        this.tipoVersioneProd = Functions.subString(tipoVersioneProd, Len.TIPO_VERSIONE_PROD);
    }

    public String getTipoVersioneProd() {
        return this.tipoVersioneProd;
    }

    public void setCodiceDivisa(String codiceDivisa) {
        this.codiceDivisa = Functions.subString(codiceDivisa, Len.CODICE_DIVISA);
    }

    public String getCodiceDivisa() {
        return this.codiceDivisa;
    }

    public void setDescrDivisa(String descrDivisa) {
        this.descrDivisa = Functions.subString(descrDivisa, Len.DESCR_DIVISA);
    }

    public String getDescrDivisa() {
        return this.descrDivisa;
    }

    public void setDescPolizza(String descPolizza) {
        this.descPolizza = Functions.subString(descPolizza, Len.DESC_POLIZZA);
    }

    public String getDescPolizza() {
        return this.descPolizza;
    }

    public void setIspc0040TpTrasfNumEleFormatted(String ispc0040TpTrasfNumEle) {
        this.tpTrasfNumEle = Trunc.toUnsignedNumeric(ispc0040TpTrasfNumEle, Len.TP_TRASF_NUM_ELE);
    }

    public short getIspc0040TpTrasfNumEle() {
        return NumericDisplay.asShort(this.tpTrasfNumEle);
    }

    public void setIspc0040TpAdeNumEleFormatted(String ispc0040TpAdeNumEle) {
        this.tpAdeNumEle = Trunc.toUnsignedNumeric(ispc0040TpAdeNumEle, Len.TP_ADE_NUM_ELE);
    }

    public short getIspc0040TpAdeNumEle() {
        return NumericDisplay.asShort(this.tpAdeNumEle);
    }

    public void setIspc0040NumEleModCalDurFormatted(String ispc0040NumEleModCalDur) {
        this.numEleModCalDur = Trunc.toUnsignedNumeric(ispc0040NumEleModCalDur, Len.NUM_ELE_MOD_CAL_DUR);
    }

    public short getIspc0040NumEleModCalDur() {
        return NumericDisplay.asShort(this.numEleModCalDur);
    }

    public void setIspc0040NumEleDiffProFormatted(String ispc0040NumEleDiffPro) {
        this.numEleDiffPro = Trunc.toUnsignedNumeric(ispc0040NumEleDiffPro, Len.NUM_ELE_DIFF_PRO);
    }

    public short getIspc0040NumEleDiffPro() {
        return NumericDisplay.asShort(this.numEleDiffPro);
    }

    public void setIspc0040AnniDifferProrFormatted(String ispc0040AnniDifferPror) {
        this.anniDifferPror = Trunc.toUnsignedNumeric(ispc0040AnniDifferPror, Len.ANNI_DIFFER_PROR);
    }

    public short getIspc0040AnniDifferPror() {
        return NumericDisplay.asShort(this.anniDifferPror);
    }

    public void setTrattAaDiffPror(char trattAaDiffPror) {
        this.trattAaDiffPror = trattAaDiffPror;
    }

    public char getTrattAaDiffPror() {
        return this.trattAaDiffPror;
    }

    public void setIspc0040NumEleModDurFormatted(String ispc0040NumEleModDur) {
        this.numEleModDur = Trunc.toUnsignedNumeric(ispc0040NumEleModDur, Len.NUM_ELE_MOD_DUR);
    }

    public short getIspc0040NumEleModDur() {
        return NumericDisplay.asShort(this.numEleModDur);
    }

    public void setIspc0040EtaScadMaschiDfltFormatted(String ispc0040EtaScadMaschiDflt) {
        this.etaScadMaschiDflt = Trunc.toUnsignedNumeric(ispc0040EtaScadMaschiDflt, Len.ETA_SCAD_MASCHI_DFLT);
    }

    public int getIspc0040EtaScadMaschiDflt() {
        return NumericDisplay.asInt(this.etaScadMaschiDflt);
    }

    public void setTrattEtaScadMaschi(char trattEtaScadMaschi) {
        this.trattEtaScadMaschi = trattEtaScadMaschi;
    }

    public char getTrattEtaScadMaschi() {
        return this.trattEtaScadMaschi;
    }

    public void setIspc0040EtaScadFemmineDfltFormatted(String ispc0040EtaScadFemmineDflt) {
        this.etaScadFemmineDflt = Trunc.toUnsignedNumeric(ispc0040EtaScadFemmineDflt, Len.ETA_SCAD_FEMMINE_DFLT);
    }

    public int getIspc0040EtaScadFemmineDflt() {
        return NumericDisplay.asInt(this.etaScadFemmineDflt);
    }

    public void setTrattEtaScadFemm(char trattEtaScadFemm) {
        this.trattEtaScadFemm = trattEtaScadFemm;
    }

    public char getTrattEtaScadFemm() {
        return this.trattEtaScadFemm;
    }

    public void setDataScadenzaDflt(String dataScadenzaDflt) {
        this.dataScadenzaDflt = Functions.subString(dataScadenzaDflt, Len.DATA_SCADENZA_DFLT);
    }

    public String getDataScadenzaDflt() {
        return this.dataScadenzaDflt;
    }

    public void setTrattDataScadFemm(char trattDataScadFemm) {
        this.trattDataScadFemm = trattDataScadFemm;
    }

    public char getTrattDataScadFemm() {
        return this.trattDataScadFemm;
    }

    public void setIspc0040DurataAnniDefaultFormatted(String ispc0040DurataAnniDefault) {
        this.durataAnniDefault = Trunc.toUnsignedNumeric(ispc0040DurataAnniDefault, Len.DURATA_ANNI_DEFAULT);
    }

    public int getIspc0040DurataAnniDefault() {
        return NumericDisplay.asInt(this.durataAnniDefault);
    }

    public void setTrattDurataAnni(char trattDurataAnni) {
        this.trattDurataAnni = trattDurataAnni;
    }

    public char getTrattDurataAnni() {
        return this.trattDurataAnni;
    }

    public void setIspc0040DurataMesiDefaultFormatted(String ispc0040DurataMesiDefault) {
        this.durataMesiDefault = Trunc.toUnsignedNumeric(ispc0040DurataMesiDefault, Len.DURATA_MESI_DEFAULT);
    }

    public int getIspc0040DurataMesiDefault() {
        return NumericDisplay.asInt(this.durataMesiDefault);
    }

    public void setTrattDurataMesi(char trattDurataMesi) {
        this.trattDurataMesi = trattDurataMesi;
    }

    public char getTrattDurataMesi() {
        return this.trattDurataMesi;
    }

    public void setIspc0040DurataGiorniDefaultFormatted(String ispc0040DurataGiorniDefault) {
        this.durataGiorniDefault = Trunc.toUnsignedNumeric(ispc0040DurataGiorniDefault, Len.DURATA_GIORNI_DEFAULT);
    }

    public int getIspc0040DurataGiorniDefault() {
        return NumericDisplay.asInt(this.durataGiorniDefault);
    }

    public void setTrattDurataGiorni(char trattDurataGiorni) {
        this.trattDurataGiorni = trattDurataGiorni;
    }

    public char getTrattDurataGiorni() {
        return this.trattDurataGiorni;
    }

    public void setIspc0040IndMaxFrazFormatted(String ispc0040IndMaxFraz) {
        this.indMaxFraz = Trunc.toUnsignedNumeric(ispc0040IndMaxFraz, Len.IND_MAX_FRAZ);
    }

    public short getIspc0040IndMaxFraz() {
        return NumericDisplay.asShort(this.indMaxFraz);
    }

    public void setTrattamentoFraz(char trattamentoFraz) {
        this.trattamentoFraz = trattamentoFraz;
    }

    public char getTrattamentoFraz() {
        return this.trattamentoFraz;
    }

    public void setIspc0040IndMaxImpScoFormatted(String ispc0040IndMaxImpSco) {
        this.indMaxImpSco = Trunc.toUnsignedNumeric(ispc0040IndMaxImpSco, Len.IND_MAX_IMP_SCO);
    }

    public short getIspc0040IndMaxImpSco() {
        return NumericDisplay.asShort(this.indMaxImpSco);
    }

    public void setTrattamentoImpSco(char trattamentoImpSco) {
        this.trattamentoImpSco = trattamentoImpSco;
    }

    public char getTrattamentoImpSco() {
        return this.trattamentoImpSco;
    }

    public void setIspc0040IndMaxFondiFormatted(String ispc0040IndMaxFondi) {
        this.indMaxFondi = Trunc.toUnsignedNumeric(ispc0040IndMaxFondi, Len.IND_MAX_FONDI);
    }

    public short getIspc0040IndMaxFondi() {
        return NumericDisplay.asShort(this.indMaxFondi);
    }

    public Ispc0040DifferimentoProroga getDifferimentoProroga(int idx) {
        return differimentoProroga[idx - 1];
    }

    public Ispc0040FlagRischioComune getFlagRischioComune() {
        return flagRischioComune;
    }

    public Ispc0040Frazionamento getFrazionamento(int idx) {
        return frazionamento[idx - 1];
    }

    public Ispc0040ImpSconto getImpSconto(int idx) {
        return impSconto[idx - 1];
    }

    public Ispc0040ModCalcDur getModCalcDur(int idx) {
        return modCalcDur[idx - 1];
    }

    public Ispc0040ModalitaDurata getModalitaDurata(int idx) {
        return modalitaDurata[idx - 1];
    }

    public Ispc0040TipoAdesione getTipoAdesione(int idx) {
        return tipoAdesione[idx - 1];
    }

    public Ispc0040TipoPolizza getTipoPolizza() {
        return tipoPolizza;
    }

    public Ispc0040TipoTrasformazione getTipoTrasformazione(int idx) {
        return tipoTrasformazione[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PRODOTTO = 12;
        public static final int DATA_VERSIONE_PROD = 8;
        public static final int TIPO_VERSIONE_PROD = 2;
        public static final int CODICE_DIVISA = 6;
        public static final int DESCR_DIVISA = 30;
        public static final int DESC_POLIZZA = 30;
        public static final int TP_TRASF_NUM_ELE = 3;
        public static final int TP_ADE_NUM_ELE = 3;
        public static final int NUM_ELE_MOD_CAL_DUR = 3;
        public static final int NUM_ELE_DIFF_PRO = 3;
        public static final int ANNI_DIFFER_PROR = 2;
        public static final int TRATT_AA_DIFF_PROR = 1;
        public static final int NUM_ELE_MOD_DUR = 3;
        public static final int ETA_SCAD_MASCHI_DFLT = 5;
        public static final int TRATT_ETA_SCAD_MASCHI = 1;
        public static final int ETA_SCAD_FEMMINE_DFLT = 5;
        public static final int TRATT_ETA_SCAD_FEMM = 1;
        public static final int DATA_SCADENZA_DFLT = 8;
        public static final int TRATT_DATA_SCAD_FEMM = 1;
        public static final int DURATA_ANNI_DEFAULT = 5;
        public static final int TRATT_DURATA_ANNI = 1;
        public static final int DURATA_MESI_DEFAULT = 5;
        public static final int TRATT_DURATA_MESI = 1;
        public static final int DURATA_GIORNI_DEFAULT = 5;
        public static final int TRATT_DURATA_GIORNI = 1;
        public static final int IND_MAX_FRAZ = 3;
        public static final int TRATTAMENTO_FRAZ = 1;
        public static final int IND_MAX_IMP_SCO = 3;
        public static final int TRATTAMENTO_IMP_SCO = 1;
        public static final int IND_MAX_FONDI = 3;
        public static final int ISPC0040_DATI_OUTPUT1 = PRODOTTO + DATA_VERSIONE_PROD + TIPO_VERSIONE_PROD + CODICE_DIVISA + DESCR_DIVISA + Ispc0040TipoPolizza.Len.TIPO_POLIZZA + DESC_POLIZZA + TP_TRASF_NUM_ELE + Ispc0040DatiOutput1.TIPO_TRASFORMAZIONE_MAXOCCURS * Ispc0040TipoTrasformazione.Len.TIPO_TRASFORMAZIONE + TP_ADE_NUM_ELE + Ispc0040DatiOutput1.TIPO_ADESIONE_MAXOCCURS * Ispc0040TipoAdesione.Len.TIPO_ADESIONE + NUM_ELE_MOD_CAL_DUR + Ispc0040DatiOutput1.MOD_CALC_DUR_MAXOCCURS * Ispc0040ModCalcDur.Len.MOD_CALC_DUR + Ispc0040FlagRischioComune.Len.FLAG_RISCHIO_COMUNE + NUM_ELE_DIFF_PRO + Ispc0040DatiOutput1.DIFFERIMENTO_PROROGA_MAXOCCURS * Ispc0040DifferimentoProroga.Len.DIFFERIMENTO_PROROGA + ANNI_DIFFER_PROR + TRATT_AA_DIFF_PROR + NUM_ELE_MOD_DUR + Ispc0040DatiOutput1.MODALITA_DURATA_MAXOCCURS * Ispc0040ModalitaDurata.Len.MODALITA_DURATA + ETA_SCAD_MASCHI_DFLT + TRATT_ETA_SCAD_MASCHI + ETA_SCAD_FEMMINE_DFLT + TRATT_ETA_SCAD_FEMM + DATA_SCADENZA_DFLT + TRATT_DATA_SCAD_FEMM + DURATA_ANNI_DEFAULT + TRATT_DURATA_ANNI + DURATA_MESI_DEFAULT + TRATT_DURATA_MESI + DURATA_GIORNI_DEFAULT + TRATT_DURATA_GIORNI + IND_MAX_FRAZ + Ispc0040DatiOutput1.FRAZIONAMENTO_MAXOCCURS * Ispc0040Frazionamento.Len.FRAZIONAMENTO + TRATTAMENTO_FRAZ + IND_MAX_IMP_SCO + Ispc0040DatiOutput1.IMP_SCONTO_MAXOCCURS * Ispc0040ImpSconto.Len.IMP_SCONTO + TRATTAMENTO_IMP_SCO + IND_MAX_FONDI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
