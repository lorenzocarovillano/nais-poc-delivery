package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WB01IdRichEstrazAgg;
import it.accenture.jnais.ws.redefines.WB01NumQuoDtCalc;
import it.accenture.jnais.ws.redefines.WB01NumQuoIni;
import it.accenture.jnais.ws.redefines.WB01PcInvst;
import it.accenture.jnais.ws.redefines.WB01ValQuoDtCalc;
import it.accenture.jnais.ws.redefines.WB01ValQuoIni;
import it.accenture.jnais.ws.redefines.WB01ValQuoT;

/**Original name: W-B01-REC-FISSO<br>
 * Variable: W-B01-REC-FISSO from copybook LCCVB018<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WB01RecFisso {

    //==== PROPERTIES ====
    //Original name: W-B01-ID-BILA-FND-ESTR
    private int wB01IdBilaFndEstr = DefaultValues.INT_VAL;
    //Original name: W-B01-ID-BILA-TRCH-ESTR
    private int wB01IdBilaTrchEstr = DefaultValues.INT_VAL;
    //Original name: W-B01-COD-COMP-ANIA
    private int wB01CodCompAnia = DefaultValues.INT_VAL;
    //Original name: W-B01-ID-RICH-ESTRAZ-MAS
    private int wB01IdRichEstrazMas = DefaultValues.INT_VAL;
    //Original name: W-B01-ID-RICH-ESTRAZ-AGG-IND
    private char wB01IdRichEstrazAggInd = DefaultValues.CHAR_VAL;
    //Original name: W-B01-ID-RICH-ESTRAZ-AGG
    private WB01IdRichEstrazAgg wB01IdRichEstrazAgg = new WB01IdRichEstrazAgg();
    //Original name: W-B01-DT-RIS
    private String wB01DtRis = DefaultValues.stringVal(Len.W_B01_DT_RIS);
    //Original name: W-B01-ID-POLI
    private int wB01IdPoli = DefaultValues.INT_VAL;
    //Original name: W-B01-ID-ADES
    private int wB01IdAdes = DefaultValues.INT_VAL;
    //Original name: W-B01-ID-GAR
    private int wB01IdGar = DefaultValues.INT_VAL;
    //Original name: W-B01-ID-TRCH-DI-GAR
    private int wB01IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: W-B01-COD-FND
    private String wB01CodFnd = DefaultValues.stringVal(Len.W_B01_COD_FND);
    //Original name: W-B01-DT-QTZ-INI-IND
    private char wB01DtQtzIniInd = DefaultValues.CHAR_VAL;
    //Original name: W-B01-DT-QTZ-INI
    private String wB01DtQtzIni = DefaultValues.stringVal(Len.W_B01_DT_QTZ_INI);
    //Original name: W-B01-NUM-QUO-INI-IND
    private char wB01NumQuoIniInd = DefaultValues.CHAR_VAL;
    //Original name: W-B01-NUM-QUO-INI
    private WB01NumQuoIni wB01NumQuoIni = new WB01NumQuoIni();
    //Original name: W-B01-NUM-QUO-DT-CALC-IND
    private char wB01NumQuoDtCalcInd = DefaultValues.CHAR_VAL;
    //Original name: W-B01-NUM-QUO-DT-CALC
    private WB01NumQuoDtCalc wB01NumQuoDtCalc = new WB01NumQuoDtCalc();
    //Original name: W-B01-VAL-QUO-INI-IND
    private char wB01ValQuoIniInd = DefaultValues.CHAR_VAL;
    //Original name: W-B01-VAL-QUO-INI
    private WB01ValQuoIni wB01ValQuoIni = new WB01ValQuoIni();
    //Original name: W-B01-DT-VALZZ-QUO-DT-CA-IND
    private char wB01DtValzzQuoDtCaInd = DefaultValues.CHAR_VAL;
    //Original name: W-B01-DT-VALZZ-QUO-DT-CA
    private String wB01DtValzzQuoDtCa = DefaultValues.stringVal(Len.W_B01_DT_VALZZ_QUO_DT_CA);
    //Original name: W-B01-VAL-QUO-DT-CALC-IND
    private char wB01ValQuoDtCalcInd = DefaultValues.CHAR_VAL;
    //Original name: W-B01-VAL-QUO-DT-CALC
    private WB01ValQuoDtCalc wB01ValQuoDtCalc = new WB01ValQuoDtCalc();
    //Original name: W-B01-VAL-QUO-T-IND
    private char wB01ValQuoTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B01-VAL-QUO-T
    private WB01ValQuoT wB01ValQuoT = new WB01ValQuoT();
    //Original name: W-B01-PC-INVST-IND
    private char wB01PcInvstInd = DefaultValues.CHAR_VAL;
    //Original name: W-B01-PC-INVST
    private WB01PcInvst wB01PcInvst = new WB01PcInvst();
    //Original name: W-B01-DS-OPER-SQL
    private char wB01DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: W-B01-DS-VER
    private int wB01DsVer = DefaultValues.INT_VAL;
    //Original name: W-B01-DS-TS-CPTZ
    private long wB01DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: W-B01-DS-UTENTE
    private String wB01DsUtente = DefaultValues.stringVal(Len.W_B01_DS_UTENTE);
    //Original name: W-B01-DS-STATO-ELAB
    private char wB01DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public byte[] getRecFissoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wB01IdBilaFndEstr, Len.Int.W_B01_ID_BILA_FND_ESTR, 0);
        position += Len.W_B01_ID_BILA_FND_ESTR;
        MarshalByte.writeIntAsPacked(buffer, position, wB01IdBilaTrchEstr, Len.Int.W_B01_ID_BILA_TRCH_ESTR, 0);
        position += Len.W_B01_ID_BILA_TRCH_ESTR;
        MarshalByte.writeIntAsPacked(buffer, position, wB01CodCompAnia, Len.Int.W_B01_COD_COMP_ANIA, 0);
        position += Len.W_B01_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wB01IdRichEstrazMas, Len.Int.W_B01_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.W_B01_ID_RICH_ESTRAZ_MAS;
        MarshalByte.writeChar(buffer, position, wB01IdRichEstrazAggInd);
        position += Types.CHAR_SIZE;
        wB01IdRichEstrazAgg.getwB01IdRichEstrazAggAsBuffer(buffer, position);
        position += WB01IdRichEstrazAgg.Len.W_B01_ID_RICH_ESTRAZ_AGG;
        MarshalByte.writeString(buffer, position, wB01DtRis, Len.W_B01_DT_RIS);
        position += Len.W_B01_DT_RIS;
        MarshalByte.writeIntAsPacked(buffer, position, wB01IdPoli, Len.Int.W_B01_ID_POLI, 0);
        position += Len.W_B01_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wB01IdAdes, Len.Int.W_B01_ID_ADES, 0);
        position += Len.W_B01_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wB01IdGar, Len.Int.W_B01_ID_GAR, 0);
        position += Len.W_B01_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, wB01IdTrchDiGar, Len.Int.W_B01_ID_TRCH_DI_GAR, 0);
        position += Len.W_B01_ID_TRCH_DI_GAR;
        MarshalByte.writeString(buffer, position, wB01CodFnd, Len.W_B01_COD_FND);
        position += Len.W_B01_COD_FND;
        MarshalByte.writeChar(buffer, position, wB01DtQtzIniInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB01DtQtzIni, Len.W_B01_DT_QTZ_INI);
        position += Len.W_B01_DT_QTZ_INI;
        MarshalByte.writeChar(buffer, position, wB01NumQuoIniInd);
        position += Types.CHAR_SIZE;
        wB01NumQuoIni.getwB01NumQuoIniAsBuffer(buffer, position);
        position += WB01NumQuoIni.Len.W_B01_NUM_QUO_INI;
        MarshalByte.writeChar(buffer, position, wB01NumQuoDtCalcInd);
        position += Types.CHAR_SIZE;
        wB01NumQuoDtCalc.getwB01NumQuoDtCalcAsBuffer(buffer, position);
        position += WB01NumQuoDtCalc.Len.W_B01_NUM_QUO_DT_CALC;
        MarshalByte.writeChar(buffer, position, wB01ValQuoIniInd);
        position += Types.CHAR_SIZE;
        wB01ValQuoIni.getwB01ValQuoIniAsBuffer(buffer, position);
        position += WB01ValQuoIni.Len.W_B01_VAL_QUO_INI;
        MarshalByte.writeChar(buffer, position, wB01DtValzzQuoDtCaInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB01DtValzzQuoDtCa, Len.W_B01_DT_VALZZ_QUO_DT_CA);
        position += Len.W_B01_DT_VALZZ_QUO_DT_CA;
        MarshalByte.writeChar(buffer, position, wB01ValQuoDtCalcInd);
        position += Types.CHAR_SIZE;
        wB01ValQuoDtCalc.getwB01ValQuoDtCalcAsBuffer(buffer, position);
        position += WB01ValQuoDtCalc.Len.W_B01_VAL_QUO_DT_CALC;
        MarshalByte.writeChar(buffer, position, wB01ValQuoTInd);
        position += Types.CHAR_SIZE;
        wB01ValQuoT.getwB01ValQuoTAsBuffer(buffer, position);
        position += WB01ValQuoT.Len.W_B01_VAL_QUO_T;
        MarshalByte.writeChar(buffer, position, wB01PcInvstInd);
        position += Types.CHAR_SIZE;
        wB01PcInvst.getwB01PcInvstAsBuffer(buffer, position);
        position += WB01PcInvst.Len.W_B01_PC_INVST;
        MarshalByte.writeChar(buffer, position, wB01DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wB01DsVer, Len.Int.W_B01_DS_VER, 0);
        position += Len.W_B01_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wB01DsTsCptz, Len.Int.W_B01_DS_TS_CPTZ, 0);
        position += Len.W_B01_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wB01DsUtente, Len.W_B01_DS_UTENTE);
        position += Len.W_B01_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wB01DsStatoElab);
        return buffer;
    }

    public void setwB01IdBilaFndEstr(int wB01IdBilaFndEstr) {
        this.wB01IdBilaFndEstr = wB01IdBilaFndEstr;
    }

    public int getwB01IdBilaFndEstr() {
        return this.wB01IdBilaFndEstr;
    }

    public void setwB01IdBilaTrchEstr(int wB01IdBilaTrchEstr) {
        this.wB01IdBilaTrchEstr = wB01IdBilaTrchEstr;
    }

    public int getwB01IdBilaTrchEstr() {
        return this.wB01IdBilaTrchEstr;
    }

    public void setwB01CodCompAnia(int wB01CodCompAnia) {
        this.wB01CodCompAnia = wB01CodCompAnia;
    }

    public int getwB01CodCompAnia() {
        return this.wB01CodCompAnia;
    }

    public void setwB01IdRichEstrazMas(int wB01IdRichEstrazMas) {
        this.wB01IdRichEstrazMas = wB01IdRichEstrazMas;
    }

    public int getwB01IdRichEstrazMas() {
        return this.wB01IdRichEstrazMas;
    }

    public void setwB01IdRichEstrazAggInd(char wB01IdRichEstrazAggInd) {
        this.wB01IdRichEstrazAggInd = wB01IdRichEstrazAggInd;
    }

    public void setwB01IdRichEstrazAggIndFormatted(String wB01IdRichEstrazAggInd) {
        setwB01IdRichEstrazAggInd(Functions.charAt(wB01IdRichEstrazAggInd, Types.CHAR_SIZE));
    }

    public char getwB01IdRichEstrazAggInd() {
        return this.wB01IdRichEstrazAggInd;
    }

    public void setwB01DtRis(String wB01DtRis) {
        this.wB01DtRis = Functions.subString(wB01DtRis, Len.W_B01_DT_RIS);
    }

    public String getwB01DtRis() {
        return this.wB01DtRis;
    }

    public void setwB01IdPoli(int wB01IdPoli) {
        this.wB01IdPoli = wB01IdPoli;
    }

    public int getwB01IdPoli() {
        return this.wB01IdPoli;
    }

    public void setwB01IdAdes(int wB01IdAdes) {
        this.wB01IdAdes = wB01IdAdes;
    }

    public int getwB01IdAdes() {
        return this.wB01IdAdes;
    }

    public void setwB01IdGar(int wB01IdGar) {
        this.wB01IdGar = wB01IdGar;
    }

    public int getwB01IdGar() {
        return this.wB01IdGar;
    }

    public void setwB01IdTrchDiGar(int wB01IdTrchDiGar) {
        this.wB01IdTrchDiGar = wB01IdTrchDiGar;
    }

    public int getwB01IdTrchDiGar() {
        return this.wB01IdTrchDiGar;
    }

    public void setwB01CodFnd(String wB01CodFnd) {
        this.wB01CodFnd = Functions.subString(wB01CodFnd, Len.W_B01_COD_FND);
    }

    public String getwB01CodFnd() {
        return this.wB01CodFnd;
    }

    public void setwB01DtQtzIniInd(char wB01DtQtzIniInd) {
        this.wB01DtQtzIniInd = wB01DtQtzIniInd;
    }

    public void setwB01DtQtzIniIndFormatted(String wB01DtQtzIniInd) {
        setwB01DtQtzIniInd(Functions.charAt(wB01DtQtzIniInd, Types.CHAR_SIZE));
    }

    public char getwB01DtQtzIniInd() {
        return this.wB01DtQtzIniInd;
    }

    public void setwB01DtQtzIni(String wB01DtQtzIni) {
        this.wB01DtQtzIni = Functions.subString(wB01DtQtzIni, Len.W_B01_DT_QTZ_INI);
    }

    public String getwB01DtQtzIni() {
        return this.wB01DtQtzIni;
    }

    public void setwB01NumQuoIniInd(char wB01NumQuoIniInd) {
        this.wB01NumQuoIniInd = wB01NumQuoIniInd;
    }

    public void setwB01NumQuoIniIndFormatted(String wB01NumQuoIniInd) {
        setwB01NumQuoIniInd(Functions.charAt(wB01NumQuoIniInd, Types.CHAR_SIZE));
    }

    public char getwB01NumQuoIniInd() {
        return this.wB01NumQuoIniInd;
    }

    public void setwB01NumQuoDtCalcInd(char wB01NumQuoDtCalcInd) {
        this.wB01NumQuoDtCalcInd = wB01NumQuoDtCalcInd;
    }

    public void setwB01NumQuoDtCalcIndFormatted(String wB01NumQuoDtCalcInd) {
        setwB01NumQuoDtCalcInd(Functions.charAt(wB01NumQuoDtCalcInd, Types.CHAR_SIZE));
    }

    public char getwB01NumQuoDtCalcInd() {
        return this.wB01NumQuoDtCalcInd;
    }

    public void setwB01ValQuoIniInd(char wB01ValQuoIniInd) {
        this.wB01ValQuoIniInd = wB01ValQuoIniInd;
    }

    public void setwB01ValQuoIniIndFormatted(String wB01ValQuoIniInd) {
        setwB01ValQuoIniInd(Functions.charAt(wB01ValQuoIniInd, Types.CHAR_SIZE));
    }

    public char getwB01ValQuoIniInd() {
        return this.wB01ValQuoIniInd;
    }

    public void setwB01DtValzzQuoDtCaInd(char wB01DtValzzQuoDtCaInd) {
        this.wB01DtValzzQuoDtCaInd = wB01DtValzzQuoDtCaInd;
    }

    public void setwB01DtValzzQuoDtCaIndFormatted(String wB01DtValzzQuoDtCaInd) {
        setwB01DtValzzQuoDtCaInd(Functions.charAt(wB01DtValzzQuoDtCaInd, Types.CHAR_SIZE));
    }

    public char getwB01DtValzzQuoDtCaInd() {
        return this.wB01DtValzzQuoDtCaInd;
    }

    public void setwB01DtValzzQuoDtCa(String wB01DtValzzQuoDtCa) {
        this.wB01DtValzzQuoDtCa = Functions.subString(wB01DtValzzQuoDtCa, Len.W_B01_DT_VALZZ_QUO_DT_CA);
    }

    public String getwB01DtValzzQuoDtCa() {
        return this.wB01DtValzzQuoDtCa;
    }

    public void setwB01ValQuoDtCalcInd(char wB01ValQuoDtCalcInd) {
        this.wB01ValQuoDtCalcInd = wB01ValQuoDtCalcInd;
    }

    public void setwB01ValQuoDtCalcIndFormatted(String wB01ValQuoDtCalcInd) {
        setwB01ValQuoDtCalcInd(Functions.charAt(wB01ValQuoDtCalcInd, Types.CHAR_SIZE));
    }

    public char getwB01ValQuoDtCalcInd() {
        return this.wB01ValQuoDtCalcInd;
    }

    public void setwB01ValQuoTInd(char wB01ValQuoTInd) {
        this.wB01ValQuoTInd = wB01ValQuoTInd;
    }

    public void setwB01ValQuoTIndFormatted(String wB01ValQuoTInd) {
        setwB01ValQuoTInd(Functions.charAt(wB01ValQuoTInd, Types.CHAR_SIZE));
    }

    public char getwB01ValQuoTInd() {
        return this.wB01ValQuoTInd;
    }

    public void setwB01PcInvstInd(char wB01PcInvstInd) {
        this.wB01PcInvstInd = wB01PcInvstInd;
    }

    public void setwB01PcInvstIndFormatted(String wB01PcInvstInd) {
        setwB01PcInvstInd(Functions.charAt(wB01PcInvstInd, Types.CHAR_SIZE));
    }

    public char getwB01PcInvstInd() {
        return this.wB01PcInvstInd;
    }

    public void setwB01DsOperSql(char wB01DsOperSql) {
        this.wB01DsOperSql = wB01DsOperSql;
    }

    public void setwB01DsOperSqlFormatted(String wB01DsOperSql) {
        setwB01DsOperSql(Functions.charAt(wB01DsOperSql, Types.CHAR_SIZE));
    }

    public char getwB01DsOperSql() {
        return this.wB01DsOperSql;
    }

    public void setwB01DsVer(int wB01DsVer) {
        this.wB01DsVer = wB01DsVer;
    }

    public int getwB01DsVer() {
        return this.wB01DsVer;
    }

    public void setwB01DsTsCptz(long wB01DsTsCptz) {
        this.wB01DsTsCptz = wB01DsTsCptz;
    }

    public long getwB01DsTsCptz() {
        return this.wB01DsTsCptz;
    }

    public void setwB01DsUtente(String wB01DsUtente) {
        this.wB01DsUtente = Functions.subString(wB01DsUtente, Len.W_B01_DS_UTENTE);
    }

    public String getwB01DsUtente() {
        return this.wB01DsUtente;
    }

    public void setwB01DsStatoElab(char wB01DsStatoElab) {
        this.wB01DsStatoElab = wB01DsStatoElab;
    }

    public void setwB01DsStatoElabFormatted(String wB01DsStatoElab) {
        setwB01DsStatoElab(Functions.charAt(wB01DsStatoElab, Types.CHAR_SIZE));
    }

    public char getwB01DsStatoElab() {
        return this.wB01DsStatoElab;
    }

    public WB01IdRichEstrazAgg getwB01IdRichEstrazAgg() {
        return wB01IdRichEstrazAgg;
    }

    public WB01NumQuoDtCalc getwB01NumQuoDtCalc() {
        return wB01NumQuoDtCalc;
    }

    public WB01NumQuoIni getwB01NumQuoIni() {
        return wB01NumQuoIni;
    }

    public WB01PcInvst getwB01PcInvst() {
        return wB01PcInvst;
    }

    public WB01ValQuoDtCalc getwB01ValQuoDtCalc() {
        return wB01ValQuoDtCalc;
    }

    public WB01ValQuoIni getwB01ValQuoIni() {
        return wB01ValQuoIni;
    }

    public WB01ValQuoT getwB01ValQuoT() {
        return wB01ValQuoT;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B01_DT_RIS = 10;
        public static final int W_B01_COD_FND = 12;
        public static final int W_B01_DT_QTZ_INI = 10;
        public static final int W_B01_DT_VALZZ_QUO_DT_CA = 10;
        public static final int W_B01_DS_UTENTE = 20;
        public static final int W_B01_ID_BILA_FND_ESTR = 5;
        public static final int W_B01_ID_BILA_TRCH_ESTR = 5;
        public static final int W_B01_COD_COMP_ANIA = 3;
        public static final int W_B01_ID_RICH_ESTRAZ_MAS = 5;
        public static final int W_B01_ID_RICH_ESTRAZ_AGG_IND = 1;
        public static final int W_B01_ID_POLI = 5;
        public static final int W_B01_ID_ADES = 5;
        public static final int W_B01_ID_GAR = 5;
        public static final int W_B01_ID_TRCH_DI_GAR = 5;
        public static final int W_B01_DT_QTZ_INI_IND = 1;
        public static final int W_B01_NUM_QUO_INI_IND = 1;
        public static final int W_B01_NUM_QUO_DT_CALC_IND = 1;
        public static final int W_B01_VAL_QUO_INI_IND = 1;
        public static final int W_B01_DT_VALZZ_QUO_DT_CA_IND = 1;
        public static final int W_B01_VAL_QUO_DT_CALC_IND = 1;
        public static final int W_B01_VAL_QUO_T_IND = 1;
        public static final int W_B01_PC_INVST_IND = 1;
        public static final int W_B01_DS_OPER_SQL = 1;
        public static final int W_B01_DS_VER = 5;
        public static final int W_B01_DS_TS_CPTZ = 10;
        public static final int W_B01_DS_STATO_ELAB = 1;
        public static final int REC_FISSO = W_B01_ID_BILA_FND_ESTR + W_B01_ID_BILA_TRCH_ESTR + W_B01_COD_COMP_ANIA + W_B01_ID_RICH_ESTRAZ_MAS + W_B01_ID_RICH_ESTRAZ_AGG_IND + WB01IdRichEstrazAgg.Len.W_B01_ID_RICH_ESTRAZ_AGG + W_B01_DT_RIS + W_B01_ID_POLI + W_B01_ID_ADES + W_B01_ID_GAR + W_B01_ID_TRCH_DI_GAR + W_B01_COD_FND + W_B01_DT_QTZ_INI_IND + W_B01_DT_QTZ_INI + W_B01_NUM_QUO_INI_IND + WB01NumQuoIni.Len.W_B01_NUM_QUO_INI + W_B01_NUM_QUO_DT_CALC_IND + WB01NumQuoDtCalc.Len.W_B01_NUM_QUO_DT_CALC + W_B01_VAL_QUO_INI_IND + WB01ValQuoIni.Len.W_B01_VAL_QUO_INI + W_B01_DT_VALZZ_QUO_DT_CA_IND + W_B01_DT_VALZZ_QUO_DT_CA + W_B01_VAL_QUO_DT_CALC_IND + WB01ValQuoDtCalc.Len.W_B01_VAL_QUO_DT_CALC + W_B01_VAL_QUO_T_IND + WB01ValQuoT.Len.W_B01_VAL_QUO_T + W_B01_PC_INVST_IND + WB01PcInvst.Len.W_B01_PC_INVST + W_B01_DS_OPER_SQL + W_B01_DS_VER + W_B01_DS_TS_CPTZ + W_B01_DS_UTENTE + W_B01_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B01_ID_BILA_FND_ESTR = 9;
            public static final int W_B01_ID_BILA_TRCH_ESTR = 9;
            public static final int W_B01_COD_COMP_ANIA = 5;
            public static final int W_B01_ID_RICH_ESTRAZ_MAS = 9;
            public static final int W_B01_ID_POLI = 9;
            public static final int W_B01_ID_ADES = 9;
            public static final int W_B01_ID_GAR = 9;
            public static final int W_B01_ID_TRCH_DI_GAR = 9;
            public static final int W_B01_DS_VER = 9;
            public static final int W_B01_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
