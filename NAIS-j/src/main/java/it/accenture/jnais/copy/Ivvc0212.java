package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import it.accenture.jnais.ws.redefines.WcntTabVar;

/**Original name: IVVC0212<br>
 * Copybook: IVVC0212 from copybook IVVC0212<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Ivvc0212 {

    //==== PROPERTIES ====
    //Original name: WCNT-ELE-VAR-CONT-MAX
    private short wcntEleVarContMax = DefaultValues.SHORT_VAL;
    //Original name: WCNT-TAB-VAR
    private WcntTabVar wcntTabVar = new WcntTabVar();

    //==== METHODS ====
    public String getWcntAreaVariabiliContFormatted() {
        return MarshalByteExt.bufferToStr(getWcntAreaVariabiliContBytes());
    }

    /**Original name: WCNT-AREA-VARIABILI-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA INPUT VARIABILI
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA
	 *    LUNG.
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getWcntAreaVariabiliContBytes() {
        byte[] buffer = new byte[Len.WCNT_AREA_VARIABILI_CONT];
        return getWcntAreaVariabiliContBytes(buffer, 1);
    }

    public void setWcntAreaVariabiliContBytes(byte[] buffer, int offset) {
        int position = offset;
        wcntEleVarContMax = MarshalByte.readPackedAsShort(buffer, position, Len.Int.WCNT_ELE_VAR_CONT_MAX, 0);
        position += Len.WCNT_ELE_VAR_CONT_MAX;
        wcntTabVar.setWcntTabVarBytes(buffer, position);
    }

    public byte[] getWcntAreaVariabiliContBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeShortAsPacked(buffer, position, wcntEleVarContMax, Len.Int.WCNT_ELE_VAR_CONT_MAX, 0);
        position += Len.WCNT_ELE_VAR_CONT_MAX;
        wcntTabVar.getWcntTabVarBytes(buffer, position);
        return buffer;
    }

    public void setWcntEleVarContMax(short wcntEleVarContMax) {
        this.wcntEleVarContMax = wcntEleVarContMax;
    }

    public short getWcntEleVarContMax() {
        return this.wcntEleVarContMax;
    }

    public WcntTabVar getWcntTabVar() {
        return wcntTabVar;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WCNT_ELE_VAR_CONT_MAX = 3;
        public static final int WCNT_AREA_VARIABILI_CONT = WCNT_ELE_VAR_CONT_MAX + WcntTabVar.Len.WCNT_TAB_VAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WCNT_ELE_VAR_CONT_MAX = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
