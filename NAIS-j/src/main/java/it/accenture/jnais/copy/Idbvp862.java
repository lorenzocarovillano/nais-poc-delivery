package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDBVP862<br>
 * Copybook: IDBVP862 from copybook IDBVP862<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvp862 {

    //==== PROPERTIES ====
    //Original name: IND-P86-ID-MOVI-CHIU
    private short p86IdMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P86-DESC-LIB-MOT-LIQ
    private short p86DescLibMotLiq = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setP86IdMoviChiu(short p86IdMoviChiu) {
        this.p86IdMoviChiu = p86IdMoviChiu;
    }

    public short getP86IdMoviChiu() {
        return this.p86IdMoviChiu;
    }

    public void setP86DescLibMotLiq(short p86DescLibMotLiq) {
        this.p86DescLibMotLiq = p86DescLibMotLiq;
    }

    public short getP86DescLibMotLiq() {
        return this.p86DescLibMotLiq;
    }
}
