package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: LDBVF971<br>
 * Variable: LDBVF971 from copybook LDBVF971<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbvf971 {

    //==== PROPERTIES ====
    //Original name: LDBVF971-TP-MOVI-1
    private int ldbvf971TpMovi1 = DefaultValues.INT_VAL;
    //Original name: LDBVF971-TP-MOVI-2
    private int ldbvf971TpMovi2 = DefaultValues.INT_VAL;
    //Original name: LDBVF971-TP-MOVI-3
    private int ldbvf971TpMovi3 = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbvf971Formatted(String data) {
        byte[] buffer = new byte[Len.LDBVF971];
        MarshalByte.writeString(buffer, 1, data, Len.LDBVF971);
        setLdbvf971Bytes(buffer, 1);
    }

    public String getLdbvf971Formatted() {
        return getLdbvf971InputFormatted();
    }

    public void setLdbvf971Bytes(byte[] buffer, int offset) {
        int position = offset;
        setLdbvf971InputBytes(buffer, position);
    }

    public String getLdbvf971InputFormatted() {
        return MarshalByteExt.bufferToStr(getLdbvf971InputBytes());
    }

    /**Original name: LDBVF971-INPUT<br>*/
    public byte[] getLdbvf971InputBytes() {
        byte[] buffer = new byte[Len.LDBVF971_INPUT];
        return getLdbvf971InputBytes(buffer, 1);
    }

    public void setLdbvf971InputBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbvf971TpMovi1 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBVF971_TP_MOVI1, 0);
        position += Len.LDBVF971_TP_MOVI1;
        ldbvf971TpMovi2 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBVF971_TP_MOVI2, 0);
        position += Len.LDBVF971_TP_MOVI2;
        ldbvf971TpMovi3 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBVF971_TP_MOVI3, 0);
    }

    public byte[] getLdbvf971InputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbvf971TpMovi1, Len.Int.LDBVF971_TP_MOVI1, 0);
        position += Len.LDBVF971_TP_MOVI1;
        MarshalByte.writeIntAsPacked(buffer, position, ldbvf971TpMovi2, Len.Int.LDBVF971_TP_MOVI2, 0);
        position += Len.LDBVF971_TP_MOVI2;
        MarshalByte.writeIntAsPacked(buffer, position, ldbvf971TpMovi3, Len.Int.LDBVF971_TP_MOVI3, 0);
        return buffer;
    }

    public void setLdbvf971TpMovi1(int ldbvf971TpMovi1) {
        this.ldbvf971TpMovi1 = ldbvf971TpMovi1;
    }

    public int getLdbvf971TpMovi1() {
        return this.ldbvf971TpMovi1;
    }

    public void setLdbvf971TpMovi2(int ldbvf971TpMovi2) {
        this.ldbvf971TpMovi2 = ldbvf971TpMovi2;
    }

    public int getLdbvf971TpMovi2() {
        return this.ldbvf971TpMovi2;
    }

    public void setLdbvf971TpMovi3(int ldbvf971TpMovi3) {
        this.ldbvf971TpMovi3 = ldbvf971TpMovi3;
    }

    public int getLdbvf971TpMovi3() {
        return this.ldbvf971TpMovi3;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBVF971_TP_MOVI1 = 3;
        public static final int LDBVF971_TP_MOVI2 = 3;
        public static final int LDBVF971_TP_MOVI3 = 3;
        public static final int LDBVF971_INPUT = LDBVF971_TP_MOVI1 + LDBVF971_TP_MOVI2 + LDBVF971_TP_MOVI3;
        public static final int LDBVF971 = LDBVF971_INPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBVF971_TP_MOVI1 = 5;
            public static final int LDBVF971_TP_MOVI2 = 5;
            public static final int LDBVF971_TP_MOVI3 = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
