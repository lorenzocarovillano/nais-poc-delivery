package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABV0006-REPORT<br>
 * Variable: IABV0006-REPORT from copybook IABV0006<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Iabv0006Report {

    //==== PROPERTIES ====
    //Original name: IABV0006-OGG-BUSINESS
    private String oggBusiness = DefaultValues.stringVal(Len.OGG_BUSINESS);
    //Original name: IABV0006-TP-OGG-BUSINESS
    private String tpOggBusiness = DefaultValues.stringVal(Len.TP_OGG_BUSINESS);
    //Original name: IABV0006-IB-OGG-POLI
    private String ibOggPoli = DefaultValues.stringVal(Len.IB_OGG_POLI);
    //Original name: IABV0006-IB-OGG-ADES
    private String ibOggAdes = DefaultValues.stringVal(Len.IB_OGG_ADES);
    //Original name: IABV0006-ID-POLI
    private String idPoli = DefaultValues.stringVal(Len.ID_POLI);
    //Original name: IABV0006-ID-ADES
    private String idAdes = DefaultValues.stringVal(Len.ID_ADES);
    //Original name: IABV0006-DT-EFF-BUSINESS
    private String dtEffBusiness = DefaultValues.stringVal(Len.DT_EFF_BUSINESS);

    //==== METHODS ====
    public void setReportBytes(byte[] buffer, int offset) {
        int position = offset;
        oggBusiness = MarshalByte.readString(buffer, position, Len.OGG_BUSINESS);
        position += Len.OGG_BUSINESS;
        tpOggBusiness = MarshalByte.readString(buffer, position, Len.TP_OGG_BUSINESS);
        position += Len.TP_OGG_BUSINESS;
        ibOggPoli = MarshalByte.readString(buffer, position, Len.IB_OGG_POLI);
        position += Len.IB_OGG_POLI;
        ibOggAdes = MarshalByte.readString(buffer, position, Len.IB_OGG_ADES);
        position += Len.IB_OGG_ADES;
        idPoli = MarshalByte.readString(buffer, position, Len.ID_POLI);
        position += Len.ID_POLI;
        idAdes = MarshalByte.readString(buffer, position, Len.ID_ADES);
        position += Len.ID_ADES;
        dtEffBusiness = MarshalByte.readString(buffer, position, Len.DT_EFF_BUSINESS);
    }

    public byte[] getReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, oggBusiness, Len.OGG_BUSINESS);
        position += Len.OGG_BUSINESS;
        MarshalByte.writeString(buffer, position, tpOggBusiness, Len.TP_OGG_BUSINESS);
        position += Len.TP_OGG_BUSINESS;
        MarshalByte.writeString(buffer, position, ibOggPoli, Len.IB_OGG_POLI);
        position += Len.IB_OGG_POLI;
        MarshalByte.writeString(buffer, position, ibOggAdes, Len.IB_OGG_ADES);
        position += Len.IB_OGG_ADES;
        MarshalByte.writeString(buffer, position, idPoli, Len.ID_POLI);
        position += Len.ID_POLI;
        MarshalByte.writeString(buffer, position, idAdes, Len.ID_ADES);
        position += Len.ID_ADES;
        MarshalByte.writeString(buffer, position, dtEffBusiness, Len.DT_EFF_BUSINESS);
        return buffer;
    }

    public void setOggBusiness(String oggBusiness) {
        this.oggBusiness = Functions.subString(oggBusiness, Len.OGG_BUSINESS);
    }

    public String getOggBusiness() {
        return this.oggBusiness;
    }

    public void setTpOggBusiness(String tpOggBusiness) {
        this.tpOggBusiness = Functions.subString(tpOggBusiness, Len.TP_OGG_BUSINESS);
    }

    public String getTpOggBusiness() {
        return this.tpOggBusiness;
    }

    public void setIbOggPoli(String ibOggPoli) {
        this.ibOggPoli = Functions.subString(ibOggPoli, Len.IB_OGG_POLI);
    }

    public String getIbOggPoli() {
        return this.ibOggPoli;
    }

    public void setIbOggAdes(String ibOggAdes) {
        this.ibOggAdes = Functions.subString(ibOggAdes, Len.IB_OGG_ADES);
    }

    public String getIbOggAdes() {
        return this.ibOggAdes;
    }

    public void setIdPoli(String idPoli) {
        this.idPoli = Functions.subString(idPoli, Len.ID_POLI);
    }

    public String getIdPoli() {
        return this.idPoli;
    }

    public void setIdAdes(String idAdes) {
        this.idAdes = Functions.subString(idAdes, Len.ID_ADES);
    }

    public String getIdAdes() {
        return this.idAdes;
    }

    public void setDtEffBusiness(String dtEffBusiness) {
        this.dtEffBusiness = Functions.subString(dtEffBusiness, Len.DT_EFF_BUSINESS);
    }

    public String getDtEffBusiness() {
        return this.dtEffBusiness;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OGG_BUSINESS = 40;
        public static final int TP_OGG_BUSINESS = 2;
        public static final int IB_OGG_POLI = 40;
        public static final int IB_OGG_ADES = 40;
        public static final int ID_POLI = 9;
        public static final int ID_ADES = 9;
        public static final int DT_EFF_BUSINESS = 10;
        public static final int REPORT = OGG_BUSINESS + TP_OGG_BUSINESS + IB_OGG_POLI + IB_OGG_ADES + ID_POLI + ID_ADES + DT_EFF_BUSINESS;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
