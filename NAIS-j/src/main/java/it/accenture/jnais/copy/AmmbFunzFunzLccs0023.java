package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: AMMB-FUNZ-FUNZ<br>
 * Variable: AMMB-FUNZ-FUNZ from copybook IDBVL051<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AmmbFunzFunzLccs0023 {

    //==== PROPERTIES ====
    //Original name: L05-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: L05-TP-MOVI-ESEC
    private int tpMoviEsec = DefaultValues.INT_VAL;
    //Original name: L05-TP-MOVI-RIFTO
    private int tpMoviRifto = DefaultValues.INT_VAL;
    //Original name: L05-GRAV-FUNZ-FUNZ
    private String gravFunzFunz = DefaultValues.stringVal(Len.GRAV_FUNZ_FUNZ);
    //Original name: L05-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: L05-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: L05-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: L05-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: L05-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: L05-COD-BLOCCO
    private String codBlocco = DefaultValues.stringVal(Len.COD_BLOCCO);
    //Original name: L05-SRVZ-VER-ANN
    private String srvzVerAnn = DefaultValues.stringVal(Len.SRVZ_VER_ANN);
    //Original name: L05-WHERE-CONDITION-LEN
    private short whereConditionLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: L05-WHERE-CONDITION
    private String whereCondition = DefaultValues.stringVal(Len.WHERE_CONDITION);
    //Original name: L05-FL-POLI-IFP
    private char flPoliIfp = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setAmmbFunzFunzFormatted(String data) {
        byte[] buffer = new byte[Len.AMMB_FUNZ_FUNZ];
        MarshalByte.writeString(buffer, 1, data, Len.AMMB_FUNZ_FUNZ);
        setAmmbFunzFunzBytes(buffer, 1);
    }

    public String getAmmbFunzFunzFormatted() {
        return MarshalByteExt.bufferToStr(getAmmbFunzFunzBytes());
    }

    public byte[] getAmmbFunzFunzBytes() {
        byte[] buffer = new byte[Len.AMMB_FUNZ_FUNZ];
        return getAmmbFunzFunzBytes(buffer, 1);
    }

    public void setAmmbFunzFunzBytes(byte[] buffer, int offset) {
        int position = offset;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        tpMoviEsec = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI_ESEC, 0);
        position += Len.TP_MOVI_ESEC;
        tpMoviRifto = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI_RIFTO, 0);
        position += Len.TP_MOVI_RIFTO;
        gravFunzFunz = MarshalByte.readString(buffer, position, Len.GRAV_FUNZ_FUNZ);
        position += Len.GRAV_FUNZ_FUNZ;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        codBlocco = MarshalByte.readString(buffer, position, Len.COD_BLOCCO);
        position += Len.COD_BLOCCO;
        srvzVerAnn = MarshalByte.readString(buffer, position, Len.SRVZ_VER_ANN);
        position += Len.SRVZ_VER_ANN;
        setWhereConditionVcharBytes(buffer, position);
        position += Len.WHERE_CONDITION_VCHAR;
        flPoliIfp = MarshalByte.readChar(buffer, position);
    }

    public byte[] getAmmbFunzFunzBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, tpMoviEsec, Len.Int.TP_MOVI_ESEC, 0);
        position += Len.TP_MOVI_ESEC;
        MarshalByte.writeIntAsPacked(buffer, position, tpMoviRifto, Len.Int.TP_MOVI_RIFTO, 0);
        position += Len.TP_MOVI_RIFTO;
        MarshalByte.writeString(buffer, position, gravFunzFunz, Len.GRAV_FUNZ_FUNZ);
        position += Len.GRAV_FUNZ_FUNZ;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsCptz, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codBlocco, Len.COD_BLOCCO);
        position += Len.COD_BLOCCO;
        MarshalByte.writeString(buffer, position, srvzVerAnn, Len.SRVZ_VER_ANN);
        position += Len.SRVZ_VER_ANN;
        getWhereConditionVcharBytes(buffer, position);
        position += Len.WHERE_CONDITION_VCHAR;
        MarshalByte.writeChar(buffer, position, flPoliIfp);
        return buffer;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setTpMoviEsec(int tpMoviEsec) {
        this.tpMoviEsec = tpMoviEsec;
    }

    public int getTpMoviEsec() {
        return this.tpMoviEsec;
    }

    public void setTpMoviRifto(int tpMoviRifto) {
        this.tpMoviRifto = tpMoviRifto;
    }

    public int getTpMoviRifto() {
        return this.tpMoviRifto;
    }

    public void setGravFunzFunz(String gravFunzFunz) {
        this.gravFunzFunz = Functions.subString(gravFunzFunz, Len.GRAV_FUNZ_FUNZ);
    }

    public String getGravFunzFunz() {
        return this.gravFunzFunz;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setCodBlocco(String codBlocco) {
        this.codBlocco = Functions.subString(codBlocco, Len.COD_BLOCCO);
    }

    public String getCodBlocco() {
        return this.codBlocco;
    }

    public void setSrvzVerAnn(String srvzVerAnn) {
        this.srvzVerAnn = Functions.subString(srvzVerAnn, Len.SRVZ_VER_ANN);
    }

    public String getSrvzVerAnn() {
        return this.srvzVerAnn;
    }

    public void setWhereConditionVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        whereConditionLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        whereCondition = MarshalByte.readString(buffer, position, Len.WHERE_CONDITION);
    }

    public byte[] getWhereConditionVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, whereConditionLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, whereCondition, Len.WHERE_CONDITION);
        return buffer;
    }

    public void setWhereConditionLen(short whereConditionLen) {
        this.whereConditionLen = whereConditionLen;
    }

    public short getWhereConditionLen() {
        return this.whereConditionLen;
    }

    public void setWhereCondition(String whereCondition) {
        this.whereCondition = Functions.subString(whereCondition, Len.WHERE_CONDITION);
    }

    public String getWhereCondition() {
        return this.whereCondition;
    }

    public void setFlPoliIfp(char flPoliIfp) {
        this.flPoliIfp = flPoliIfp;
    }

    public void setFlPoliIfpFormatted(String flPoliIfp) {
        setFlPoliIfp(Functions.charAt(flPoliIfp, Types.CHAR_SIZE));
    }

    public char getFlPoliIfp() {
        return this.flPoliIfp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GRAV_FUNZ_FUNZ = 2;
        public static final int DS_UTENTE = 20;
        public static final int COD_BLOCCO = 5;
        public static final int SRVZ_VER_ANN = 8;
        public static final int WHERE_CONDITION = 300;
        public static final int COD_COMP_ANIA = 3;
        public static final int TP_MOVI_ESEC = 3;
        public static final int TP_MOVI_RIFTO = 3;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_CPTZ = 10;
        public static final int DS_STATO_ELAB = 1;
        public static final int WHERE_CONDITION_LEN = 2;
        public static final int WHERE_CONDITION_VCHAR = WHERE_CONDITION_LEN + WHERE_CONDITION;
        public static final int FL_POLI_IFP = 1;
        public static final int AMMB_FUNZ_FUNZ = COD_COMP_ANIA + TP_MOVI_ESEC + TP_MOVI_RIFTO + GRAV_FUNZ_FUNZ + DS_OPER_SQL + DS_VER + DS_TS_CPTZ + DS_UTENTE + DS_STATO_ELAB + COD_BLOCCO + SRVZ_VER_ANN + WHERE_CONDITION_VCHAR + FL_POLI_IFP;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_COMP_ANIA = 5;
            public static final int TP_MOVI_ESEC = 5;
            public static final int TP_MOVI_RIFTO = 5;
            public static final int DS_VER = 9;
            public static final int DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
