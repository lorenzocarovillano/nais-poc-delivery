package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: WRDF-DATI<br>
 * Variable: WRDF-DATI from copybook LCCVRDF1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WrdfDati implements ICopyable<WrdfDati> {

    //==== PROPERTIES ====
    //Original name: WRDF-ID-RICH-DIS-FND
    private int idRichDisFnd = DefaultValues.INT_VAL;
    //Original name: WRDF-ID-MOVI-FINRIO
    private int idMoviFinrio = DefaultValues.INT_VAL;
    //Original name: WRDF-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: WRDF-ID-MOVI-CHIU
    private int idMoviChiu = DefaultValues.INT_VAL;
    //Original name: WRDF-DT-INI-EFF
    private int dtIniEff = DefaultValues.INT_VAL;
    //Original name: WRDF-DT-END-EFF
    private int dtEndEff = DefaultValues.INT_VAL;
    //Original name: WRDF-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: WRDF-COD-FND
    private String codFnd = DefaultValues.stringVal(Len.COD_FND);
    //Original name: WRDF-NUM-QUO
    private AfDecimal numQuo = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: WRDF-PC
    private AfDecimal pc = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: WRDF-IMP-MOVTO
    private AfDecimal impMovto = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WRDF-DT-DIS
    private int dtDis = DefaultValues.INT_VAL;
    //Original name: WRDF-COD-TARI
    private String codTari = DefaultValues.stringVal(Len.COD_TARI);
    //Original name: WRDF-TP-STAT
    private String tpStat = DefaultValues.stringVal(Len.TP_STAT);
    //Original name: WRDF-TP-MOD-DIS
    private String tpModDis = DefaultValues.stringVal(Len.TP_MOD_DIS);
    //Original name: WRDF-COD-DIV
    private String codDiv = DefaultValues.stringVal(Len.COD_DIV);
    //Original name: WRDF-DT-CAMBIO-VLT
    private int dtCambioVlt = DefaultValues.INT_VAL;
    //Original name: WRDF-TP-FND
    private char tpFnd = DefaultValues.CHAR_VAL;
    //Original name: WRDF-DS-RIGA
    private long dsRiga = DefaultValues.LONG_VAL;
    //Original name: WRDF-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WRDF-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: WRDF-DS-TS-INI-CPTZ
    private long dsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WRDF-DS-TS-END-CPTZ
    private long dsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WRDF-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: WRDF-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WRDF-DT-DIS-CALC
    private int dtDisCalc = DefaultValues.INT_VAL;
    //Original name: WRDF-FL-CALC-DIS
    private char flCalcDis = DefaultValues.CHAR_VAL;
    //Original name: WRDF-COMMIS-GEST
    private AfDecimal commisGest = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);
    //Original name: WRDF-NUM-QUO-CDG-FNZ
    private AfDecimal numQuoCdgFnz = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: WRDF-NUM-QUO-CDGTOT-FNZ
    private AfDecimal numQuoCdgtotFnz = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: WRDF-COS-RUN-ASSVA-IDC
    private AfDecimal cosRunAssvaIdc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WRDF-FL-SWM-BP2S
    private char flSwmBp2s = DefaultValues.CHAR_VAL;

    //==== CONSTRUCTORS ====
    public WrdfDati() {
    }

    public WrdfDati(WrdfDati dati) {
        this();
        this.idRichDisFnd = dati.idRichDisFnd;
        this.idMoviFinrio = dati.idMoviFinrio;
        this.idMoviCrz = dati.idMoviCrz;
        this.idMoviChiu = dati.idMoviChiu;
        this.dtIniEff = dati.dtIniEff;
        this.dtEndEff = dati.dtEndEff;
        this.codCompAnia = dati.codCompAnia;
        this.codFnd = dati.codFnd;
        this.numQuo.assign(dati.numQuo);
        this.pc.assign(dati.pc);
        this.impMovto.assign(dati.impMovto);
        this.dtDis = dati.dtDis;
        this.codTari = dati.codTari;
        this.tpStat = dati.tpStat;
        this.tpModDis = dati.tpModDis;
        this.codDiv = dati.codDiv;
        this.dtCambioVlt = dati.dtCambioVlt;
        this.tpFnd = dati.tpFnd;
        this.dsRiga = dati.dsRiga;
        this.dsOperSql = dati.dsOperSql;
        this.dsVer = dati.dsVer;
        this.dsTsIniCptz = dati.dsTsIniCptz;
        this.dsTsEndCptz = dati.dsTsEndCptz;
        this.dsUtente = dati.dsUtente;
        this.dsStatoElab = dati.dsStatoElab;
        this.dtDisCalc = dati.dtDisCalc;
        this.flCalcDis = dati.flCalcDis;
        this.commisGest.assign(dati.commisGest);
        this.numQuoCdgFnz.assign(dati.numQuoCdgFnz);
        this.numQuoCdgtotFnz.assign(dati.numQuoCdgtotFnz);
        this.cosRunAssvaIdc.assign(dati.cosRunAssvaIdc);
        this.flSwmBp2s = dati.flSwmBp2s;
    }

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        idRichDisFnd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_RICH_DIS_FND, 0);
        position += Len.ID_RICH_DIS_FND;
        idMoviFinrio = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_FINRIO, 0);
        position += Len.ID_MOVI_FINRIO;
        idMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        idMoviChiu = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        dtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        dtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        codFnd = MarshalByte.readString(buffer, position, Len.COD_FND);
        position += Len.COD_FND;
        numQuo.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.NUM_QUO, Len.Fract.NUM_QUO));
        position += Len.NUM_QUO;
        pc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC, Len.Fract.PC));
        position += Len.PC;
        impMovto.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_MOVTO, Len.Fract.IMP_MOVTO));
        position += Len.IMP_MOVTO;
        dtDis = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DIS, 0);
        position += Len.DT_DIS;
        codTari = MarshalByte.readString(buffer, position, Len.COD_TARI);
        position += Len.COD_TARI;
        tpStat = MarshalByte.readString(buffer, position, Len.TP_STAT);
        position += Len.TP_STAT;
        tpModDis = MarshalByte.readString(buffer, position, Len.TP_MOD_DIS);
        position += Len.TP_MOD_DIS;
        codDiv = MarshalByte.readString(buffer, position, Len.COD_DIV);
        position += Len.COD_DIV;
        dtCambioVlt = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_CAMBIO_VLT, 0);
        position += Len.DT_CAMBIO_VLT;
        tpFnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        dsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dtDisCalc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DIS_CALC, 0);
        position += Len.DT_DIS_CALC;
        flCalcDis = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        commisGest.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.COMMIS_GEST, Len.Fract.COMMIS_GEST));
        position += Len.COMMIS_GEST;
        numQuoCdgFnz.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.NUM_QUO_CDG_FNZ, Len.Fract.NUM_QUO_CDG_FNZ));
        position += Len.NUM_QUO_CDG_FNZ;
        numQuoCdgtotFnz.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.NUM_QUO_CDGTOT_FNZ, Len.Fract.NUM_QUO_CDGTOT_FNZ));
        position += Len.NUM_QUO_CDGTOT_FNZ;
        cosRunAssvaIdc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.COS_RUN_ASSVA_IDC, Len.Fract.COS_RUN_ASSVA_IDC));
        position += Len.COS_RUN_ASSVA_IDC;
        flSwmBp2s = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idRichDisFnd, Len.Int.ID_RICH_DIS_FND, 0);
        position += Len.ID_RICH_DIS_FND;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviFinrio, Len.Int.ID_MOVI_FINRIO, 0);
        position += Len.ID_MOVI_FINRIO;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviCrz, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviChiu, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniEff, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtEndEff, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, codFnd, Len.COD_FND);
        position += Len.COD_FND;
        MarshalByte.writeDecimalAsPacked(buffer, position, numQuo.copy());
        position += Len.NUM_QUO;
        MarshalByte.writeDecimalAsPacked(buffer, position, pc.copy());
        position += Len.PC;
        MarshalByte.writeDecimalAsPacked(buffer, position, impMovto.copy());
        position += Len.IMP_MOVTO;
        MarshalByte.writeIntAsPacked(buffer, position, dtDis, Len.Int.DT_DIS, 0);
        position += Len.DT_DIS;
        MarshalByte.writeString(buffer, position, codTari, Len.COD_TARI);
        position += Len.COD_TARI;
        MarshalByte.writeString(buffer, position, tpStat, Len.TP_STAT);
        position += Len.TP_STAT;
        MarshalByte.writeString(buffer, position, tpModDis, Len.TP_MOD_DIS);
        position += Len.TP_MOD_DIS;
        MarshalByte.writeString(buffer, position, codDiv, Len.COD_DIV);
        position += Len.COD_DIV;
        MarshalByte.writeIntAsPacked(buffer, position, dtCambioVlt, Len.Int.DT_CAMBIO_VLT, 0);
        position += Len.DT_CAMBIO_VLT;
        MarshalByte.writeChar(buffer, position, tpFnd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, dsRiga, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dtDisCalc, Len.Int.DT_DIS_CALC, 0);
        position += Len.DT_DIS_CALC;
        MarshalByte.writeChar(buffer, position, flCalcDis);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, commisGest.copy());
        position += Len.COMMIS_GEST;
        MarshalByte.writeDecimalAsPacked(buffer, position, numQuoCdgFnz.copy());
        position += Len.NUM_QUO_CDG_FNZ;
        MarshalByte.writeDecimalAsPacked(buffer, position, numQuoCdgtotFnz.copy());
        position += Len.NUM_QUO_CDGTOT_FNZ;
        MarshalByte.writeDecimalAsPacked(buffer, position, cosRunAssvaIdc.copy());
        position += Len.COS_RUN_ASSVA_IDC;
        MarshalByte.writeChar(buffer, position, flSwmBp2s);
        return buffer;
    }

    public void initDatiSpaces() {
        idRichDisFnd = Types.INVALID_INT_VAL;
        idMoviFinrio = Types.INVALID_INT_VAL;
        idMoviCrz = Types.INVALID_INT_VAL;
        idMoviChiu = Types.INVALID_INT_VAL;
        dtIniEff = Types.INVALID_INT_VAL;
        dtEndEff = Types.INVALID_INT_VAL;
        codCompAnia = Types.INVALID_INT_VAL;
        codFnd = "";
        numQuo.setNaN();
        pc.setNaN();
        impMovto.setNaN();
        dtDis = Types.INVALID_INT_VAL;
        codTari = "";
        tpStat = "";
        tpModDis = "";
        codDiv = "";
        dtCambioVlt = Types.INVALID_INT_VAL;
        tpFnd = Types.SPACE_CHAR;
        dsRiga = Types.INVALID_LONG_VAL;
        dsOperSql = Types.SPACE_CHAR;
        dsVer = Types.INVALID_INT_VAL;
        dsTsIniCptz = Types.INVALID_LONG_VAL;
        dsTsEndCptz = Types.INVALID_LONG_VAL;
        dsUtente = "";
        dsStatoElab = Types.SPACE_CHAR;
        dtDisCalc = Types.INVALID_INT_VAL;
        flCalcDis = Types.SPACE_CHAR;
        commisGest.setNaN();
        numQuoCdgFnz.setNaN();
        numQuoCdgtotFnz.setNaN();
        cosRunAssvaIdc.setNaN();
        flSwmBp2s = Types.SPACE_CHAR;
    }

    public void setIdRichDisFnd(int idRichDisFnd) {
        this.idRichDisFnd = idRichDisFnd;
    }

    public int getIdRichDisFnd() {
        return this.idRichDisFnd;
    }

    public void setIdMoviFinrio(int idMoviFinrio) {
        this.idMoviFinrio = idMoviFinrio;
    }

    public int getIdMoviFinrio() {
        return this.idMoviFinrio;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setIdMoviChiu(int idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public int getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtIniEff(int dtIniEff) {
        this.dtIniEff = dtIniEff;
    }

    public int getDtIniEff() {
        return this.dtIniEff;
    }

    public void setDtEndEff(int dtEndEff) {
        this.dtEndEff = dtEndEff;
    }

    public int getDtEndEff() {
        return this.dtEndEff;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setCodFnd(String codFnd) {
        this.codFnd = Functions.subString(codFnd, Len.COD_FND);
    }

    public String getCodFnd() {
        return this.codFnd;
    }

    public void setNumQuo(AfDecimal numQuo) {
        this.numQuo.assign(numQuo);
    }

    public AfDecimal getNumQuo() {
        return this.numQuo.copy();
    }

    public void setPc(AfDecimal pc) {
        this.pc.assign(pc);
    }

    public AfDecimal getPc() {
        return this.pc.copy();
    }

    public void setImpMovto(AfDecimal impMovto) {
        this.impMovto.assign(impMovto);
    }

    public AfDecimal getImpMovto() {
        return this.impMovto.copy();
    }

    public void setDtDis(int dtDis) {
        this.dtDis = dtDis;
    }

    public int getDtDis() {
        return this.dtDis;
    }

    public void setCodTari(String codTari) {
        this.codTari = Functions.subString(codTari, Len.COD_TARI);
    }

    public String getCodTari() {
        return this.codTari;
    }

    public void setTpStat(String tpStat) {
        this.tpStat = Functions.subString(tpStat, Len.TP_STAT);
    }

    public String getTpStat() {
        return this.tpStat;
    }

    public void setTpModDis(String tpModDis) {
        this.tpModDis = Functions.subString(tpModDis, Len.TP_MOD_DIS);
    }

    public String getTpModDis() {
        return this.tpModDis;
    }

    public void setCodDiv(String codDiv) {
        this.codDiv = Functions.subString(codDiv, Len.COD_DIV);
    }

    public String getCodDiv() {
        return this.codDiv;
    }

    public void setDtCambioVlt(int dtCambioVlt) {
        this.dtCambioVlt = dtCambioVlt;
    }

    public int getDtCambioVlt() {
        return this.dtCambioVlt;
    }

    public void setTpFnd(char tpFnd) {
        this.tpFnd = tpFnd;
    }

    public char getTpFnd() {
        return this.tpFnd;
    }

    public void setDsRiga(long dsRiga) {
        this.dsRiga = dsRiga;
    }

    public long getDsRiga() {
        return this.dsRiga;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dsTsIniCptz = dsTsIniCptz;
    }

    public long getDsTsIniCptz() {
        return this.dsTsIniCptz;
    }

    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dsTsEndCptz = dsTsEndCptz;
    }

    public long getDsTsEndCptz() {
        return this.dsTsEndCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setDtDisCalc(int dtDisCalc) {
        this.dtDisCalc = dtDisCalc;
    }

    public int getDtDisCalc() {
        return this.dtDisCalc;
    }

    public void setFlCalcDis(char flCalcDis) {
        this.flCalcDis = flCalcDis;
    }

    public char getFlCalcDis() {
        return this.flCalcDis;
    }

    public void setCommisGest(AfDecimal commisGest) {
        this.commisGest.assign(commisGest);
    }

    public AfDecimal getCommisGest() {
        return this.commisGest.copy();
    }

    public void setNumQuoCdgFnz(AfDecimal numQuoCdgFnz) {
        this.numQuoCdgFnz.assign(numQuoCdgFnz);
    }

    public AfDecimal getNumQuoCdgFnz() {
        return this.numQuoCdgFnz.copy();
    }

    public void setNumQuoCdgtotFnz(AfDecimal numQuoCdgtotFnz) {
        this.numQuoCdgtotFnz.assign(numQuoCdgtotFnz);
    }

    public AfDecimal getNumQuoCdgtotFnz() {
        return this.numQuoCdgtotFnz.copy();
    }

    public void setCosRunAssvaIdc(AfDecimal cosRunAssvaIdc) {
        this.cosRunAssvaIdc.assign(cosRunAssvaIdc);
    }

    public AfDecimal getCosRunAssvaIdc() {
        return this.cosRunAssvaIdc.copy();
    }

    public void setFlSwmBp2s(char flSwmBp2s) {
        this.flSwmBp2s = flSwmBp2s;
    }

    public char getFlSwmBp2s() {
        return this.flSwmBp2s;
    }

    public WrdfDati copy() {
        return new WrdfDati(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_RICH_DIS_FND = 5;
        public static final int ID_MOVI_FINRIO = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int COD_FND = 20;
        public static final int NUM_QUO = 7;
        public static final int PC = 4;
        public static final int IMP_MOVTO = 8;
        public static final int DT_DIS = 5;
        public static final int COD_TARI = 12;
        public static final int TP_STAT = 2;
        public static final int TP_MOD_DIS = 2;
        public static final int COD_DIV = 20;
        public static final int DT_CAMBIO_VLT = 5;
        public static final int TP_FND = 1;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int DT_DIS_CALC = 5;
        public static final int FL_CALC_DIS = 1;
        public static final int COMMIS_GEST = 10;
        public static final int NUM_QUO_CDG_FNZ = 7;
        public static final int NUM_QUO_CDGTOT_FNZ = 7;
        public static final int COS_RUN_ASSVA_IDC = 8;
        public static final int FL_SWM_BP2S = 1;
        public static final int DATI = ID_RICH_DIS_FND + ID_MOVI_FINRIO + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + COD_FND + NUM_QUO + PC + IMP_MOVTO + DT_DIS + COD_TARI + TP_STAT + TP_MOD_DIS + COD_DIV + DT_CAMBIO_VLT + TP_FND + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + DT_DIS_CALC + FL_CALC_DIS + COMMIS_GEST + NUM_QUO_CDG_FNZ + NUM_QUO_CDGTOT_FNZ + COS_RUN_ASSVA_IDC + FL_SWM_BP2S;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_RICH_DIS_FND = 9;
            public static final int ID_MOVI_FINRIO = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int NUM_QUO = 7;
            public static final int PC = 3;
            public static final int IMP_MOVTO = 12;
            public static final int DT_DIS = 8;
            public static final int DT_CAMBIO_VLT = 8;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int DT_DIS_CALC = 8;
            public static final int COMMIS_GEST = 11;
            public static final int NUM_QUO_CDG_FNZ = 7;
            public static final int NUM_QUO_CDGTOT_FNZ = 7;
            public static final int COS_RUN_ASSVA_IDC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int NUM_QUO = 5;
            public static final int PC = 3;
            public static final int IMP_MOVTO = 3;
            public static final int COMMIS_GEST = 7;
            public static final int NUM_QUO_CDG_FNZ = 5;
            public static final int NUM_QUO_CDGTOT_FNZ = 5;
            public static final int COS_RUN_ASSVA_IDC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
