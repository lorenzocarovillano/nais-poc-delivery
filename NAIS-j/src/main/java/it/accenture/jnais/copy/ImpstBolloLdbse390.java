package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.P58IdMoviChiu;
import it.accenture.jnais.ws.redefines.P58ImpstBolloDettV;
import it.accenture.jnais.ws.redefines.P58ImpstBolloTotV;

/**Original name: IMPST-BOLLO<br>
 * Variable: IMPST-BOLLO from copybook IDBVP581<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ImpstBolloLdbse390 {

    //==== PROPERTIES ====
    //Original name: P58-COD-FISC
    private String p58CodFisc = DefaultValues.stringVal(Len.P58_COD_FISC);
    //Original name: P58-COD-PART-IVA
    private String p58CodPartIva = DefaultValues.stringVal(Len.P58_COD_PART_IVA);
    //Original name: P58-ID-MOVI-CHIU
    private P58IdMoviChiu p58IdMoviChiu = new P58IdMoviChiu();
    //Original name: P58-IMPST-BOLLO-DETT-V
    private P58ImpstBolloDettV p58ImpstBolloDettV = new P58ImpstBolloDettV();
    //Original name: P58-IMPST-BOLLO-TOT-V
    private P58ImpstBolloTotV p58ImpstBolloTotV = new P58ImpstBolloTotV();

    //==== METHODS ====
    public void setP58CodFisc(String p58CodFisc) {
        this.p58CodFisc = Functions.subString(p58CodFisc, Len.P58_COD_FISC);
    }

    public String getP58CodFisc() {
        return this.p58CodFisc;
    }

    public void setP58CodPartIva(String p58CodPartIva) {
        this.p58CodPartIva = Functions.subString(p58CodPartIva, Len.P58_COD_PART_IVA);
    }

    public String getP58CodPartIva() {
        return this.p58CodPartIva;
    }

    public P58IdMoviChiu getP58IdMoviChiu() {
        return p58IdMoviChiu;
    }

    public P58ImpstBolloDettV getP58ImpstBolloDettV() {
        return p58ImpstBolloDettV;
    }

    public P58ImpstBolloTotV getP58ImpstBolloTotV() {
        return p58ImpstBolloTotV;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P58_IB_POLI = 40;
        public static final int P58_COD_FISC = 16;
        public static final int P58_COD_PART_IVA = 11;
        public static final int P58_TP_CAUS_BOLLO = 2;
        public static final int P58_DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
