package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LCCVB018<br>
 * Copybook: LCCVB018 from copybook LCCVB018<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvb018 {

    //==== PROPERTIES ====
    //Original name: W-B01-LEN-TOT
    private short lenTot = DefaultValues.BIN_SHORT_VAL;
    //Original name: W-B01-REC-FISSO
    private WB01RecFisso recFisso = new WB01RecFisso();

    //==== METHODS ====
    public void setLenTot(short lenTot) {
        this.lenTot = lenTot;
    }

    public short getLenTot() {
        return this.lenTot;
    }

    public WB01RecFisso getRecFisso() {
        return recFisso;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LEN_TOT = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
