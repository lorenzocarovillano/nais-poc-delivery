package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVQUE3<br>
 * Copybook: IDBVQUE3 from copybook IDBVQUE3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvque3 {

    //==== PROPERTIES ====
    //Original name: QUE-DT-INI-EFF-DB
    private String queDtIniEffDb = DefaultValues.stringVal(Len.QUE_DT_INI_EFF_DB);
    //Original name: QUE-DT-END-EFF-DB
    private String queDtEndEffDb = DefaultValues.stringVal(Len.QUE_DT_END_EFF_DB);

    //==== METHODS ====
    public void setQueDtIniEffDb(String queDtIniEffDb) {
        this.queDtIniEffDb = Functions.subString(queDtIniEffDb, Len.QUE_DT_INI_EFF_DB);
    }

    public String getQueDtIniEffDb() {
        return this.queDtIniEffDb;
    }

    public void setQueDtEndEffDb(String queDtEndEffDb) {
        this.queDtEndEffDb = Functions.subString(queDtEndEffDb, Len.QUE_DT_END_EFF_DB);
    }

    public String getQueDtEndEffDb() {
        return this.queDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int QUE_DT_INI_EFF_DB = 10;
        public static final int QUE_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
