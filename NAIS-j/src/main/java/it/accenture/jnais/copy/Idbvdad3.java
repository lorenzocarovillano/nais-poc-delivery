package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVDAD3<br>
 * Copybook: IDBVDAD3 from copybook IDBVDAD3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvdad3 {

    //==== PROPERTIES ====
    //Original name: DAD-DT-DECOR-DFLT-DB
    private String dadDtDecorDfltDb = DefaultValues.stringVal(Len.DAD_DT_DECOR_DFLT_DB);
    //Original name: DAD-DT-SCAD-ADES-DFLT-DB
    private String dadDtScadAdesDfltDb = DefaultValues.stringVal(Len.DAD_DT_SCAD_ADES_DFLT_DB);

    //==== METHODS ====
    public void setDadDtDecorDfltDb(String dadDtDecorDfltDb) {
        this.dadDtDecorDfltDb = Functions.subString(dadDtDecorDfltDb, Len.DAD_DT_DECOR_DFLT_DB);
    }

    public String getDadDtDecorDfltDb() {
        return this.dadDtDecorDfltDb;
    }

    public void setDadDtScadAdesDfltDb(String dadDtScadAdesDfltDb) {
        this.dadDtScadAdesDfltDb = Functions.subString(dadDtScadAdesDfltDb, Len.DAD_DT_SCAD_ADES_DFLT_DB);
    }

    public String getDadDtScadAdesDfltDb() {
        return this.dadDtScadAdesDfltDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_DT_DECOR_DFLT_DB = 10;
        public static final int DAD_DT_SCAD_ADES_DFLT_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
