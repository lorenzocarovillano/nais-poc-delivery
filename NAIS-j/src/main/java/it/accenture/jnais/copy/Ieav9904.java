package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import it.accenture.jnais.ws.occurs.Ieav9904EleErrori;

/**Original name: IEAV9904<br>
 * Variable: IEAV9904 from copybook IEAV9904<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ieav9904 {

    //==== PROPERTIES ====
    public static final int ELE_ERRORI_MAXOCCURS = 500;
    //Original name: IEAV9904-LIMITE-MAX
    private short limiteMax = ((short)500);
    //Original name: IEAV9904-MAX-ELE-ERRORI
    private short maxEleErrori = DefaultValues.BIN_SHORT_VAL;
    //Original name: IEAV9904-ELE-ERRORI
    private LazyArrayCopy<Ieav9904EleErrori> eleErrori = new LazyArrayCopy<Ieav9904EleErrori>(new Ieav9904EleErrori(), 1, ELE_ERRORI_MAXOCCURS);

    //==== METHODS ====
    public short getLimiteMax() {
        return this.limiteMax;
    }

    public void setMaxEleErrori(short maxEleErrori) {
        this.maxEleErrori = maxEleErrori;
    }

    public short getMaxEleErrori() {
        return this.maxEleErrori;
    }

    public Ieav9904EleErrori getEleErrori(int idx) {
        return eleErrori.get(idx - 1);
    }
}
