package it.accenture.jnais.copy;

import it.accenture.jnais.ws.enums.Iabv0008AccessoBatch;
import it.accenture.jnais.ws.enums.Iabv0008FormatoDataDb;
import it.accenture.jnais.ws.enums.Iabv0008GestioneRipartenza;
import it.accenture.jnais.ws.enums.Iabv0008LengthDataGates;
import it.accenture.jnais.ws.enums.Iabv0008LivelloGravitaLog;
import it.accenture.jnais.ws.enums.Iabv0008TemporaryTable;
import it.accenture.jnais.ws.enums.Iabv0008TipoOrderBy;
import it.accenture.jnais.ws.enums.Iabv0008TrattamentoCommit;
import it.accenture.jnais.ws.enums.Iabv0008TrattamentoStoricita;
import it.accenture.jnais.ws.enums.WkSimulazione;

/**Original name: IABV0008<br>
 * Variable: IABV0008 from copybook IABV0008<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Iabv0008 {

    //==== PROPERTIES ====
    /**Original name: IABV0008-FLAG-STOR-ESECUZ<br>
	 * <pre>*****************************************************************
	 *  campi di parametrizzazione ambiente
	 * *****************************************************************
	 * -- parametri di skeda</pre>*/
    private boolean flagStorEsecuz = false;
    //Original name: IABV0008-FLAG-SIMULAZIONE
    private WkSimulazione flagSimulazione = new WkSimulazione();
    //Original name: IABV0008-TRATTAMENTO-COMMIT
    private Iabv0008TrattamentoCommit trattamentoCommit = new Iabv0008TrattamentoCommit();
    //Original name: IABV0008-GESTIONE-RIPARTENZA
    private Iabv0008GestioneRipartenza gestioneRipartenza = new Iabv0008GestioneRipartenza();
    //Original name: IABV0008-TIPO-ORDER-BY
    private Iabv0008TipoOrderBy tipoOrderBy = new Iabv0008TipoOrderBy();
    //Original name: IABV0008-LIVELLO-GRAVITA-LOG
    private Iabv0008LivelloGravitaLog livelloGravitaLog = new Iabv0008LivelloGravitaLog();
    //Original name: IABV0008-TEMPORARY-TABLE
    private Iabv0008TemporaryTable temporaryTable = new Iabv0008TemporaryTable();
    //Original name: IABV0008-LENGTH-DATA-GATES
    private Iabv0008LengthDataGates lengthDataGates = new Iabv0008LengthDataGates();
    //Original name: IABV0008-APPEND-DATA-GATES
    private boolean appendDataGates = false;
    //Original name: IABV0008-TRATTAMENTO-STORICITA
    private Iabv0008TrattamentoStoricita trattamentoStoricita = new Iabv0008TrattamentoStoricita();
    //Original name: IABV0008-SCRITTURA-LOG-ERRORE
    private boolean scritturaLogErrore = false;
    //Original name: IABV0008-FORMATO-DATA-DB
    private Iabv0008FormatoDataDb formatoDataDb = new Iabv0008FormatoDataDb();
    /**Original name: IABV0008-FLAG-CONNESSIONE-DB<br>
	 * <pre>--     (Default UNIX 'ISO' - FORMATO DATE : 2008-12-31 )
	 * -- parametri per eventuale connessione a DataBase</pre>*/
    private boolean flagConnessioneDb = true;
    /**Original name: IABV0008-DATABASE-NAME<br>
	 * <pre>--
	 *  Default da definire :
	 *  X DataBase  - SVI / TST / COL / DEM
	 *  X User      - db2inst1
	 *  X PassWord  - db2inst1
	 * --</pre>*/
    private String databaseName = "SVI";
    //Original name: IABV0008-USER
    private String user = "db2inst1";
    //Original name: IABV0008-PASSWORD
    private String password = "db2inst1";
    /**Original name: IABV0008-ACCESSO-BATCH<br>
	 * <pre>-- modalità di accesso alla tabella BTC_BATCH</pre>*/
    private Iabv0008AccessoBatch accessoBatch = new Iabv0008AccessoBatch();

    //==== METHODS ====
    public boolean isFlagStorEsecuz() {
        return this.flagStorEsecuz;
    }

    public boolean isAppendDataGates() {
        return this.appendDataGates;
    }

    public boolean isScritturaLogErrore() {
        return this.scritturaLogErrore;
    }

    public boolean isFlagConnessioneDb() {
        return this.flagConnessioneDb;
    }

    public String getDatabaseName() {
        return this.databaseName;
    }

    public String getUser() {
        return this.user;
    }

    public String getPassword() {
        return this.password;
    }

    public Iabv0008AccessoBatch getAccessoBatch() {
        return accessoBatch;
    }

    public WkSimulazione getFlagSimulazione() {
        return flagSimulazione;
    }

    public Iabv0008FormatoDataDb getFormatoDataDb() {
        return formatoDataDb;
    }

    public Iabv0008GestioneRipartenza getGestioneRipartenza() {
        return gestioneRipartenza;
    }

    public Iabv0008LengthDataGates getLengthDataGates() {
        return lengthDataGates;
    }

    public Iabv0008LivelloGravitaLog getLivelloGravitaLog() {
        return livelloGravitaLog;
    }

    public Iabv0008TemporaryTable getTemporaryTable() {
        return temporaryTable;
    }

    public Iabv0008TipoOrderBy getTipoOrderBy() {
        return tipoOrderBy;
    }

    public Iabv0008TrattamentoCommit getTrattamentoCommit() {
        return trattamentoCommit;
    }

    public Iabv0008TrattamentoStoricita getTrattamentoStoricita() {
        return trattamentoStoricita;
    }
}
