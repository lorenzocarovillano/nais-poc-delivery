package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpagDtValQuote;

/**Original name: WPAG-DATI-ALTRI<br>
 * Variable: WPAG-DATI-ALTRI from copybook LVEC0268<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WpagDatiAltri {

    //==== PROPERTIES ====
    //Original name: WPAG-TP-IAS
    private String wpagTpIas = DefaultValues.stringVal(Len.WPAG_TP_IAS);
    //Original name: WPAG-DT-VAL-QUOTE
    private WpagDtValQuote wpagDtValQuote = new WpagDtValQuote();
    //Original name: WPAG-FL-FRAZ-PROVV
    private char wpagFlFrazProvv = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setWpagDatiAltriBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagTpIas = MarshalByte.readString(buffer, position, Len.WPAG_TP_IAS);
        position += Len.WPAG_TP_IAS;
        wpagDtValQuote.setWpagDtValQuoteFromBuffer(buffer, position);
        position += WpagDtValQuote.Len.WPAG_DT_VAL_QUOTE;
        wpagFlFrazProvv = MarshalByte.readChar(buffer, position);
    }

    public byte[] getWpagDatiAltriBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wpagTpIas, Len.WPAG_TP_IAS);
        position += Len.WPAG_TP_IAS;
        wpagDtValQuote.getWpagDtValQuoteAsBuffer(buffer, position);
        position += WpagDtValQuote.Len.WPAG_DT_VAL_QUOTE;
        MarshalByte.writeChar(buffer, position, wpagFlFrazProvv);
        return buffer;
    }

    public void initWpagDatiAltriSpaces() {
        wpagTpIas = "";
        wpagDtValQuote.initWpagDtValQuoteSpaces();
        wpagFlFrazProvv = Types.SPACE_CHAR;
    }

    public void setWpagTpIas(String wpagTpIas) {
        this.wpagTpIas = Functions.subString(wpagTpIas, Len.WPAG_TP_IAS);
    }

    public String getWpagTpIas() {
        return this.wpagTpIas;
    }

    public void setWpagFlFrazProvv(char wpagFlFrazProvv) {
        this.wpagFlFrazProvv = wpagFlFrazProvv;
    }

    public char getWpagFlFrazProvv() {
        return this.wpagFlFrazProvv;
    }

    public WpagDtValQuote getWpagDtValQuote() {
        return wpagDtValQuote;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TP_IAS = 2;
        public static final int WPAG_FL_FRAZ_PROVV = 1;
        public static final int WPAG_DATI_ALTRI = WPAG_TP_IAS + WpagDtValQuote.Len.WPAG_DT_VAL_QUOTE + WPAG_FL_FRAZ_PROVV;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
