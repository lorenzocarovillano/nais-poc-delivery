package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: VALORI-CAMPI-LOG-ERRORE-REPORT<br>
 * Variable: VALORI-CAMPI-LOG-ERRORE-REPORT from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ValoriCampiLogErroreReport {

    //==== PROPERTIES ====
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT
    private String flr1 = "";
    //Original name: REP-LIV-GRAVITA
    private String livGravita = "0";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-1
    private String flr2 = "";
    //Original name: REP-OGG-BUSINESS
    private String oggBusiness = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-2
    private String flr3 = "";
    //Original name: REP-TP-OGG-BUSINESS
    private String tpOggBusiness = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-3
    private String flr4 = "";
    //Original name: REP-IB-OGG-POLI
    private String ibOggPoli = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-4
    private char flr5 = Types.SPACE_CHAR;
    //Original name: REP-IB-OGG-ADES
    private String ibOggAdes = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-5
    private char flr6 = Types.SPACE_CHAR;
    //Original name: REP-ID-POLI
    private String idPoli = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-6
    private char flr7 = Types.SPACE_CHAR;
    //Original name: REP-ID-ADES
    private String idAdes = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-7
    private char flr8 = Types.SPACE_CHAR;
    //Original name: REP-DT-EFF-BUSINESS
    private String dtEffBusiness = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-8
    private char flr9 = Types.SPACE_CHAR;
    //Original name: REP-DESC-ERRORE
    private String descErrore = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-9
    private char flr10 = Types.SPACE_CHAR;
    //Original name: REP-COD-MAIN-BATCH
    private String codMainBatch = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-10
    private char flr11 = Types.SPACE_CHAR;
    //Original name: REP-SERVIZIO-BE
    private String servizioBe = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-11
    private char flr12 = Types.SPACE_CHAR;
    //Original name: REP-LABEL-ERR
    private String labelErr = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-12
    private String flr13 = "";
    //Original name: REP-OPER-TABELLA
    private String operTabella = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-13
    private String flr14 = "";
    //Original name: REP-NOME-TABELLA
    private String nomeTabella = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-14
    private char flr15 = Types.SPACE_CHAR;
    //Original name: REP-STATUS-TABELLA
    private String statusTabella = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-15
    private char flr16 = Types.SPACE_CHAR;
    //Original name: REP-TMST-ERRORE
    private String tmstErrore = "";
    //Original name: FILLER-VALORI-CAMPI-LOG-ERRORE-REPORT-16
    private char flr17 = Types.SPACE_CHAR;
    //Original name: REP-KEY-TABELLA
    private String keyTabella = "";

    //==== METHODS ====
    public String getValoriCampiLogErroreReportFormatted() {
        return MarshalByteExt.bufferToStr(getValoriCampiLogErroreReportBytes());
    }

    public byte[] getValoriCampiLogErroreReportBytes() {
        byte[] buffer = new byte[Len.VALORI_CAMPI_LOG_ERRORE_REPORT];
        return getValoriCampiLogErroreReportBytes(buffer, 1);
    }

    public byte[] getValoriCampiLogErroreReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, livGravita, Len.LIV_GRAVITA);
        position += Len.LIV_GRAVITA;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, oggBusiness, Len.OGG_BUSINESS);
        position += Len.OGG_BUSINESS;
        MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, tpOggBusiness, Len.TP_OGG_BUSINESS);
        position += Len.TP_OGG_BUSINESS;
        MarshalByte.writeString(buffer, position, flr4, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, ibOggPoli, Len.IB_OGG_POLI);
        position += Len.IB_OGG_POLI;
        MarshalByte.writeChar(buffer, position, flr5);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ibOggAdes, Len.IB_OGG_ADES);
        position += Len.IB_OGG_ADES;
        MarshalByte.writeChar(buffer, position, flr6);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, idPoli, Len.ID_POLI);
        position += Len.ID_POLI;
        MarshalByte.writeChar(buffer, position, flr7);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, idAdes, Len.ID_ADES);
        position += Len.ID_ADES;
        MarshalByte.writeChar(buffer, position, flr8);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dtEffBusiness, Len.DT_EFF_BUSINESS);
        position += Len.DT_EFF_BUSINESS;
        MarshalByte.writeChar(buffer, position, flr9);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, descErrore, Len.DESC_ERRORE);
        position += Len.DESC_ERRORE;
        MarshalByte.writeChar(buffer, position, flr10);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codMainBatch, Len.COD_MAIN_BATCH);
        position += Len.COD_MAIN_BATCH;
        MarshalByte.writeChar(buffer, position, flr11);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, servizioBe, Len.SERVIZIO_BE);
        position += Len.SERVIZIO_BE;
        MarshalByte.writeChar(buffer, position, flr12);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, labelErr, Len.LABEL_ERR);
        position += Len.LABEL_ERR;
        MarshalByte.writeString(buffer, position, flr13, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, operTabella, Len.OPER_TABELLA);
        position += Len.OPER_TABELLA;
        MarshalByte.writeString(buffer, position, flr14, Len.FLR14);
        position += Len.FLR14;
        MarshalByte.writeString(buffer, position, nomeTabella, Len.NOME_TABELLA);
        position += Len.NOME_TABELLA;
        MarshalByte.writeChar(buffer, position, flr15);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, statusTabella, Len.STATUS_TABELLA);
        position += Len.STATUS_TABELLA;
        MarshalByte.writeChar(buffer, position, flr16);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tmstErrore, Len.TMST_ERRORE);
        position += Len.TMST_ERRORE;
        MarshalByte.writeChar(buffer, position, flr17);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, keyTabella, Len.KEY_TABELLA);
        return buffer;
    }

    public void initValoriCampiLogErroreReportSpaces() {
        flr1 = "";
        livGravita = "";
        flr2 = "";
        oggBusiness = "";
        flr3 = "";
        tpOggBusiness = "";
        flr4 = "";
        ibOggPoli = "";
        flr5 = Types.SPACE_CHAR;
        ibOggAdes = "";
        flr6 = Types.SPACE_CHAR;
        idPoli = "";
        flr7 = Types.SPACE_CHAR;
        idAdes = "";
        flr8 = Types.SPACE_CHAR;
        dtEffBusiness = "";
        flr9 = Types.SPACE_CHAR;
        descErrore = "";
        flr10 = Types.SPACE_CHAR;
        codMainBatch = "";
        flr11 = Types.SPACE_CHAR;
        servizioBe = "";
        flr12 = Types.SPACE_CHAR;
        labelErr = "";
        flr13 = "";
        operTabella = "";
        flr14 = "";
        nomeTabella = "";
        flr15 = Types.SPACE_CHAR;
        statusTabella = "";
        flr16 = Types.SPACE_CHAR;
        tmstErrore = "";
        flr17 = Types.SPACE_CHAR;
        keyTabella = "";
    }

    public String getFlr1() {
        return this.flr1;
    }

    public void setLivGravita(short livGravita) {
        this.livGravita = NumericDisplay.asString(livGravita, Len.LIV_GRAVITA);
    }

    public void setLivGravitaFormatted(String livGravita) {
        this.livGravita = Trunc.toUnsignedNumeric(livGravita, Len.LIV_GRAVITA);
    }

    public short getLivGravita() {
        return NumericDisplay.asShort(this.livGravita);
    }

    public String getFlr2() {
        return this.flr2;
    }

    public void setOggBusiness(String oggBusiness) {
        this.oggBusiness = Functions.subString(oggBusiness, Len.OGG_BUSINESS);
    }

    public String getOggBusiness() {
        return this.oggBusiness;
    }

    public String getFlr3() {
        return this.flr3;
    }

    public void setTpOggBusiness(String tpOggBusiness) {
        this.tpOggBusiness = Functions.subString(tpOggBusiness, Len.TP_OGG_BUSINESS);
    }

    public String getTpOggBusiness() {
        return this.tpOggBusiness;
    }

    public String getFlr4() {
        return this.flr4;
    }

    public void setIbOggPoli(String ibOggPoli) {
        this.ibOggPoli = Functions.subString(ibOggPoli, Len.IB_OGG_POLI);
    }

    public String getIbOggPoli() {
        return this.ibOggPoli;
    }

    public char getFlr5() {
        return this.flr5;
    }

    public void setIbOggAdes(String ibOggAdes) {
        this.ibOggAdes = Functions.subString(ibOggAdes, Len.IB_OGG_ADES);
    }

    public String getIbOggAdes() {
        return this.ibOggAdes;
    }

    public char getFlr6() {
        return this.flr6;
    }

    public void setIdPoli(String idPoli) {
        this.idPoli = Functions.subString(idPoli, Len.ID_POLI);
    }

    public String getIdPoli() {
        return this.idPoli;
    }

    public char getFlr7() {
        return this.flr7;
    }

    public void setIdAdes(String idAdes) {
        this.idAdes = Functions.subString(idAdes, Len.ID_ADES);
    }

    public String getIdAdes() {
        return this.idAdes;
    }

    public char getFlr8() {
        return this.flr8;
    }

    public void setDtEffBusiness(String dtEffBusiness) {
        this.dtEffBusiness = Functions.subString(dtEffBusiness, Len.DT_EFF_BUSINESS);
    }

    public String getDtEffBusiness() {
        return this.dtEffBusiness;
    }

    public char getFlr9() {
        return this.flr9;
    }

    public void setDescErrore(String descErrore) {
        this.descErrore = Functions.subString(descErrore, Len.DESC_ERRORE);
    }

    public String getDescErrore() {
        return this.descErrore;
    }

    public char getFlr10() {
        return this.flr10;
    }

    public void setCodMainBatch(String codMainBatch) {
        this.codMainBatch = Functions.subString(codMainBatch, Len.COD_MAIN_BATCH);
    }

    public String getCodMainBatch() {
        return this.codMainBatch;
    }

    public char getFlr11() {
        return this.flr11;
    }

    public void setServizioBe(String servizioBe) {
        this.servizioBe = Functions.subString(servizioBe, Len.SERVIZIO_BE);
    }

    public String getServizioBe() {
        return this.servizioBe;
    }

    public char getFlr12() {
        return this.flr12;
    }

    public void setLabelErr(String labelErr) {
        this.labelErr = Functions.subString(labelErr, Len.LABEL_ERR);
    }

    public String getLabelErr() {
        return this.labelErr;
    }

    public String getFlr13() {
        return this.flr13;
    }

    public void setOperTabella(String operTabella) {
        this.operTabella = Functions.subString(operTabella, Len.OPER_TABELLA);
    }

    public String getOperTabella() {
        return this.operTabella;
    }

    public String getFlr14() {
        return this.flr14;
    }

    public void setNomeTabella(String nomeTabella) {
        this.nomeTabella = Functions.subString(nomeTabella, Len.NOME_TABELLA);
    }

    public String getNomeTabella() {
        return this.nomeTabella;
    }

    public char getFlr15() {
        return this.flr15;
    }

    public void setStatusTabella(String statusTabella) {
        this.statusTabella = Functions.subString(statusTabella, Len.STATUS_TABELLA);
    }

    public String getStatusTabella() {
        return this.statusTabella;
    }

    public char getFlr16() {
        return this.flr16;
    }

    public void setTmstErrore(String tmstErrore) {
        this.tmstErrore = Functions.subString(tmstErrore, Len.TMST_ERRORE);
    }

    public String getTmstErrore() {
        return this.tmstErrore;
    }

    public char getFlr17() {
        return this.flr17;
    }

    public void setKeyTabella(String keyTabella) {
        this.keyTabella = Functions.subString(keyTabella, Len.KEY_TABELLA);
    }

    public String getKeyTabella() {
        return this.keyTabella;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LIV_GRAVITA = 1;
        public static final int OGG_BUSINESS = 40;
        public static final int TP_OGG_BUSINESS = 2;
        public static final int IB_OGG_POLI = 40;
        public static final int IB_OGG_ADES = 40;
        public static final int ID_POLI = 9;
        public static final int ID_ADES = 9;
        public static final int DT_EFF_BUSINESS = 10;
        public static final int DESC_ERRORE = 100;
        public static final int COD_MAIN_BATCH = 8;
        public static final int SERVIZIO_BE = 8;
        public static final int LABEL_ERR = 30;
        public static final int OPER_TABELLA = 2;
        public static final int NOME_TABELLA = 18;
        public static final int STATUS_TABELLA = 10;
        public static final int TMST_ERRORE = 26;
        public static final int KEY_TABELLA = 100;
        public static final int FLR1 = 3;
        public static final int FLR2 = 5;
        public static final int FLR3 = 4;
        public static final int FLR5 = 1;
        public static final int FLR14 = 6;
        public static final int VALORI_CAMPI_LOG_ERRORE_REPORT = LIV_GRAVITA + OGG_BUSINESS + TP_OGG_BUSINESS + IB_OGG_POLI + IB_OGG_ADES + ID_POLI + ID_ADES + DT_EFF_BUSINESS + DESC_ERRORE + COD_MAIN_BATCH + SERVIZIO_BE + LABEL_ERR + OPER_TABELLA + NOME_TABELLA + STATUS_TABELLA + TMST_ERRORE + KEY_TABELLA + FLR14 + 2 * FLR1 + FLR2 + 2 * FLR3 + 11 * FLR5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
