package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LCCC0019-CAMPI-ESITO<br>
 * Variable: LCCC0019-CAMPI-ESITO from copybook LCCC0019<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Lccc0019CampiEsito {

    //==== PROPERTIES ====
    //Original name: LCCC0019-DESCRIZ-ERR-DB2
    private String descrizErrDb2 = DefaultValues.stringVal(Len.DESCRIZ_ERR_DB2);
    //Original name: LCCC0019-COD-SERVIZIO-BE
    private String codServizioBe = DefaultValues.stringVal(Len.COD_SERVIZIO_BE);
    //Original name: LCCC0019-LABEL-ERR
    private String labelErr = DefaultValues.stringVal(Len.LABEL_ERR);
    //Original name: LCCC0019-COD-ERRORE
    private String codErrore = DefaultValues.stringVal(Len.COD_ERRORE);
    //Original name: LCCC0019-PARAMETRI-ERR
    private String parametriErr = DefaultValues.stringVal(Len.PARAMETRI_ERR);
    //Original name: LCCC0019-NOME-TABELLA
    private String nomeTabella = DefaultValues.stringVal(Len.NOME_TABELLA);
    //Original name: LCCC0019-KEY-TABELLA
    private String keyTabella = DefaultValues.stringVal(Len.KEY_TABELLA);

    //==== METHODS ====
    public void setCampiEsitoBytes(byte[] buffer, int offset) {
        int position = offset;
        descrizErrDb2 = MarshalByte.readString(buffer, position, Len.DESCRIZ_ERR_DB2);
        position += Len.DESCRIZ_ERR_DB2;
        codServizioBe = MarshalByte.readString(buffer, position, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        labelErr = MarshalByte.readString(buffer, position, Len.LABEL_ERR);
        position += Len.LABEL_ERR;
        codErrore = MarshalByte.readFixedString(buffer, position, Len.COD_ERRORE);
        position += Len.COD_ERRORE;
        parametriErr = MarshalByte.readString(buffer, position, Len.PARAMETRI_ERR);
        position += Len.PARAMETRI_ERR;
        nomeTabella = MarshalByte.readString(buffer, position, Len.NOME_TABELLA);
        position += Len.NOME_TABELLA;
        keyTabella = MarshalByte.readString(buffer, position, Len.KEY_TABELLA);
    }

    public byte[] getCampiEsitoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, descrizErrDb2, Len.DESCRIZ_ERR_DB2);
        position += Len.DESCRIZ_ERR_DB2;
        MarshalByte.writeString(buffer, position, codServizioBe, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        MarshalByte.writeString(buffer, position, labelErr, Len.LABEL_ERR);
        position += Len.LABEL_ERR;
        MarshalByte.writeString(buffer, position, codErrore, Len.COD_ERRORE);
        position += Len.COD_ERRORE;
        MarshalByte.writeString(buffer, position, parametriErr, Len.PARAMETRI_ERR);
        position += Len.PARAMETRI_ERR;
        MarshalByte.writeString(buffer, position, nomeTabella, Len.NOME_TABELLA);
        position += Len.NOME_TABELLA;
        MarshalByte.writeString(buffer, position, keyTabella, Len.KEY_TABELLA);
        return buffer;
    }

    public void setDescrizErrDb2(String descrizErrDb2) {
        this.descrizErrDb2 = Functions.subString(descrizErrDb2, Len.DESCRIZ_ERR_DB2);
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setCodServizioBe(String codServizioBe) {
        this.codServizioBe = Functions.subString(codServizioBe, Len.COD_SERVIZIO_BE);
    }

    public String getCodServizioBe() {
        return this.codServizioBe;
    }

    public void setLabelErr(String labelErr) {
        this.labelErr = Functions.subString(labelErr, Len.LABEL_ERR);
    }

    public String getLabelErr() {
        return this.labelErr;
    }

    public void setCodErroreFormatted(String codErrore) {
        this.codErrore = Trunc.toUnsignedNumeric(codErrore, Len.COD_ERRORE);
    }

    public int getCodErrore() {
        return NumericDisplay.asInt(this.codErrore);
    }

    public void setParametriErr(String parametriErr) {
        this.parametriErr = Functions.subString(parametriErr, Len.PARAMETRI_ERR);
    }

    public String getParametriErr() {
        return this.parametriErr;
    }

    public void setNomeTabella(String nomeTabella) {
        this.nomeTabella = Functions.subString(nomeTabella, Len.NOME_TABELLA);
    }

    public String getNomeTabella() {
        return this.nomeTabella;
    }

    public void setKeyTabella(String keyTabella) {
        this.keyTabella = Functions.subString(keyTabella, Len.KEY_TABELLA);
    }

    public String getKeyTabella() {
        return this.keyTabella;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESCRIZ_ERR_DB2 = 300;
        public static final int COD_SERVIZIO_BE = 8;
        public static final int LABEL_ERR = 30;
        public static final int COD_ERRORE = 6;
        public static final int PARAMETRI_ERR = 100;
        public static final int NOME_TABELLA = 18;
        public static final int KEY_TABELLA = 20;
        public static final int CAMPI_ESITO = DESCRIZ_ERR_DB2 + COD_SERVIZIO_BE + LABEL_ERR + COD_ERRORE + PARAMETRI_ERR + NOME_TABELLA + KEY_TABELLA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
