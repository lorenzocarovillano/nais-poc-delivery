package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LVVC0000-DATI-INPUT-2<br>
 * Variable: LVVC0000-DATI-INPUT-2 from copybook LVVC0000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Lvvc0000DatiInput2 {

    //==== PROPERTIES ====
    //Original name: LVVC0000-ANNI-INPUT-2
    private String anniInput2 = DefaultValues.stringVal(Len.ANNI_INPUT2);
    //Original name: LVVC0000-MESI-INPUT-2
    private String mesiInput2 = DefaultValues.stringVal(Len.MESI_INPUT2);
    //Original name: LVVC0000-GIORNI-INPUT-2
    private String giorniInput2 = DefaultValues.stringVal(Len.GIORNI_INPUT2);

    //==== METHODS ====
    public void setDatiInput2Bytes(byte[] buffer, int offset) {
        int position = offset;
        anniInput2 = MarshalByte.readFixedString(buffer, position, Len.ANNI_INPUT2);
        position += Len.ANNI_INPUT2;
        mesiInput2 = MarshalByte.readFixedString(buffer, position, Len.MESI_INPUT2);
        position += Len.MESI_INPUT2;
        giorniInput2 = MarshalByte.readFixedString(buffer, position, Len.GIORNI_INPUT2);
    }

    public byte[] getDatiInput2Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, anniInput2, Len.ANNI_INPUT2);
        position += Len.ANNI_INPUT2;
        MarshalByte.writeString(buffer, position, mesiInput2, Len.MESI_INPUT2);
        position += Len.MESI_INPUT2;
        MarshalByte.writeString(buffer, position, giorniInput2, Len.GIORNI_INPUT2);
        return buffer;
    }

    public void setLvvc0000AnniInput2Formatted(String lvvc0000AnniInput2) {
        this.anniInput2 = Trunc.toUnsignedNumeric(lvvc0000AnniInput2, Len.ANNI_INPUT2);
    }

    public int getLvvc0000AnniInput2() {
        return NumericDisplay.asInt(this.anniInput2);
    }

    public void setLvvc0000MesiInput2Formatted(String lvvc0000MesiInput2) {
        this.mesiInput2 = Trunc.toUnsignedNumeric(lvvc0000MesiInput2, Len.MESI_INPUT2);
    }

    public int getLvvc0000MesiInput2() {
        return NumericDisplay.asInt(this.mesiInput2);
    }

    public void setLvvc0000GiorniInput2Formatted(String lvvc0000GiorniInput2) {
        this.giorniInput2 = Trunc.toUnsignedNumeric(lvvc0000GiorniInput2, Len.GIORNI_INPUT2);
    }

    public int getLvvc0000GiorniInput2() {
        return NumericDisplay.asInt(this.giorniInput2);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ANNI_INPUT2 = 5;
        public static final int MESI_INPUT2 = 5;
        public static final int GIORNI_INPUT2 = 5;
        public static final int DATI_INPUT2 = ANNI_INPUT2 + MESI_INPUT2 + GIORNI_INPUT2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
