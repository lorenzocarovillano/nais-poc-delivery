package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV1291<br>
 * Variable: LDBV1291 from copybook LDBV1291<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv1291 {

    //==== PROPERTIES ====
    //Original name: LDBV1291-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV1291-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV1291-TP-1
    private String tp1 = DefaultValues.stringVal(Len.TP1);
    //Original name: LDBV1291-TP-2
    private String tp2 = DefaultValues.stringVal(Len.TP2);
    //Original name: LDBV1291-TP-3
    private String tp3 = DefaultValues.stringVal(Len.TP3);
    //Original name: LDBV1291-TP-4
    private String tp4 = DefaultValues.stringVal(Len.TP4);
    //Original name: LDBV1291-TP-5
    private String tp5 = DefaultValues.stringVal(Len.TP5);
    //Original name: LDBV1291-TP-6
    private String tp6 = DefaultValues.stringVal(Len.TP6);
    //Original name: LDBV1291-TP-7
    private String tp7 = DefaultValues.stringVal(Len.TP7);
    //Original name: LDBV1291-TP-8
    private String tp8 = DefaultValues.stringVal(Len.TP8);
    //Original name: LDBV1291-TP-9
    private String tp9 = DefaultValues.stringVal(Len.TP9);
    //Original name: LDBV1291-TP-10
    private String tp10 = DefaultValues.stringVal(Len.TP10);
    //Original name: LDBV1291-TP-11
    private String tp11 = DefaultValues.stringVal(Len.TP11);

    //==== METHODS ====
    public void setLdbv1291Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV1291];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV1291);
        setLdbv1291Bytes(buffer, 1);
    }

    public String getLdbv1291Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv1291Bytes());
    }

    public byte[] getLdbv1291Bytes() {
        byte[] buffer = new byte[Len.LDBV1291];
        return getLdbv1291Bytes(buffer, 1);
    }

    public void setLdbv1291Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tp1 = MarshalByte.readString(buffer, position, Len.TP1);
        position += Len.TP1;
        tp2 = MarshalByte.readString(buffer, position, Len.TP2);
        position += Len.TP2;
        tp3 = MarshalByte.readString(buffer, position, Len.TP3);
        position += Len.TP3;
        tp4 = MarshalByte.readString(buffer, position, Len.TP4);
        position += Len.TP4;
        tp5 = MarshalByte.readString(buffer, position, Len.TP5);
        position += Len.TP5;
        tp6 = MarshalByte.readString(buffer, position, Len.TP6);
        position += Len.TP6;
        tp7 = MarshalByte.readString(buffer, position, Len.TP7);
        position += Len.TP7;
        tp8 = MarshalByte.readString(buffer, position, Len.TP8);
        position += Len.TP8;
        tp9 = MarshalByte.readString(buffer, position, Len.TP9);
        position += Len.TP9;
        tp10 = MarshalByte.readString(buffer, position, Len.TP10);
        position += Len.TP10;
        tp11 = MarshalByte.readString(buffer, position, Len.TP11);
    }

    public byte[] getLdbv1291Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tp1, Len.TP1);
        position += Len.TP1;
        MarshalByte.writeString(buffer, position, tp2, Len.TP2);
        position += Len.TP2;
        MarshalByte.writeString(buffer, position, tp3, Len.TP3);
        position += Len.TP3;
        MarshalByte.writeString(buffer, position, tp4, Len.TP4);
        position += Len.TP4;
        MarshalByte.writeString(buffer, position, tp5, Len.TP5);
        position += Len.TP5;
        MarshalByte.writeString(buffer, position, tp6, Len.TP6);
        position += Len.TP6;
        MarshalByte.writeString(buffer, position, tp7, Len.TP7);
        position += Len.TP7;
        MarshalByte.writeString(buffer, position, tp8, Len.TP8);
        position += Len.TP8;
        MarshalByte.writeString(buffer, position, tp9, Len.TP9);
        position += Len.TP9;
        MarshalByte.writeString(buffer, position, tp10, Len.TP10);
        position += Len.TP10;
        MarshalByte.writeString(buffer, position, tp11, Len.TP11);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTp1(String tp1) {
        this.tp1 = Functions.subString(tp1, Len.TP1);
    }

    public String getTp1() {
        return this.tp1;
    }

    public void setTp2(String tp2) {
        this.tp2 = Functions.subString(tp2, Len.TP2);
    }

    public String getTp2() {
        return this.tp2;
    }

    public void setTp3(String tp3) {
        this.tp3 = Functions.subString(tp3, Len.TP3);
    }

    public String getTp3() {
        return this.tp3;
    }

    public void setTp4(String tp4) {
        this.tp4 = Functions.subString(tp4, Len.TP4);
    }

    public String getTp4() {
        return this.tp4;
    }

    public void setTp5(String tp5) {
        this.tp5 = Functions.subString(tp5, Len.TP5);
    }

    public String getTp5() {
        return this.tp5;
    }

    public void setTp6(String tp6) {
        this.tp6 = Functions.subString(tp6, Len.TP6);
    }

    public String getTp6() {
        return this.tp6;
    }

    public void setTp7(String tp7) {
        this.tp7 = Functions.subString(tp7, Len.TP7);
    }

    public String getTp7() {
        return this.tp7;
    }

    public void setTp8(String tp8) {
        this.tp8 = Functions.subString(tp8, Len.TP8);
    }

    public String getTp8() {
        return this.tp8;
    }

    public void setTp9(String tp9) {
        this.tp9 = Functions.subString(tp9, Len.TP9);
    }

    public String getTp9() {
        return this.tp9;
    }

    public void setTp10(String tp10) {
        this.tp10 = Functions.subString(tp10, Len.TP10);
    }

    public String getTp10() {
        return this.tp10;
    }

    public void setTp11(String tp11) {
        this.tp11 = Functions.subString(tp11, Len.TP11);
    }

    public String getTp11() {
        return this.tp11;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int TP1 = 2;
        public static final int TP2 = 2;
        public static final int TP3 = 2;
        public static final int TP4 = 2;
        public static final int TP5 = 2;
        public static final int TP6 = 2;
        public static final int TP7 = 2;
        public static final int TP8 = 2;
        public static final int TP9 = 2;
        public static final int TP10 = 2;
        public static final int TP11 = 2;
        public static final int ID_OGG = 5;
        public static final int LDBV1291 = ID_OGG + TP_OGG + TP1 + TP2 + TP3 + TP4 + TP5 + TP6 + TP7 + TP8 + TP9 + TP10 + TP11;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
