package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV9091-DATI-INPUT<br>
 * Variable: LDBV9091-DATI-INPUT from copybook LDBV9091<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbv9091DatiInput {

    //==== PROPERTIES ====
    //Original name: LDBV9091-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LDBV9091-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LDBV9091-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV9091-TP-STAT-BUS-1
    private String tpStatBus1 = DefaultValues.stringVal(Len.TP_STAT_BUS1);
    //Original name: LDBV9091-TP-STAT-BUS-2
    private String tpStatBus2 = DefaultValues.stringVal(Len.TP_STAT_BUS2);
    //Original name: LDBV9091-RAMO1
    private String ramo1 = DefaultValues.stringVal(Len.RAMO1);
    //Original name: LDBV9091-RAMO2
    private String ramo2 = DefaultValues.stringVal(Len.RAMO2);
    //Original name: LDBV9091-RAMO3
    private String ramo3 = DefaultValues.stringVal(Len.RAMO3);
    //Original name: LDBV9091-RAMO4
    private String ramo4 = DefaultValues.stringVal(Len.RAMO4);
    //Original name: LDBV9091-RAMO5
    private String ramo5 = DefaultValues.stringVal(Len.RAMO5);
    //Original name: LDBV9091-RAMO6
    private String ramo6 = DefaultValues.stringVal(Len.RAMO6);
    //Original name: LDBV9091-TP-INVST1
    private short tpInvst1 = DefaultValues.SHORT_VAL;
    //Original name: LDBV9091-TP-INVST2
    private short tpInvst2 = DefaultValues.SHORT_VAL;
    //Original name: LDBV9091-TP-INVST3
    private short tpInvst3 = DefaultValues.SHORT_VAL;
    //Original name: LDBV9091-TP-INVST4
    private short tpInvst4 = DefaultValues.SHORT_VAL;
    //Original name: LDBV9091-TP-INVST5
    private short tpInvst5 = DefaultValues.SHORT_VAL;
    //Original name: LDBV9091-TP-INVST6
    private short tpInvst6 = DefaultValues.SHORT_VAL;

    //==== METHODS ====
    public void setLdbv9091DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpStatBus1 = MarshalByte.readString(buffer, position, Len.TP_STAT_BUS1);
        position += Len.TP_STAT_BUS1;
        tpStatBus2 = MarshalByte.readString(buffer, position, Len.TP_STAT_BUS2);
        position += Len.TP_STAT_BUS2;
        ramo1 = MarshalByte.readString(buffer, position, Len.RAMO1);
        position += Len.RAMO1;
        ramo2 = MarshalByte.readString(buffer, position, Len.RAMO2);
        position += Len.RAMO2;
        ramo3 = MarshalByte.readString(buffer, position, Len.RAMO3);
        position += Len.RAMO3;
        ramo4 = MarshalByte.readString(buffer, position, Len.RAMO4);
        position += Len.RAMO4;
        ramo5 = MarshalByte.readString(buffer, position, Len.RAMO5);
        position += Len.RAMO5;
        ramo6 = MarshalByte.readString(buffer, position, Len.RAMO6);
        position += Len.RAMO6;
        tpInvst1 = MarshalByte.readPackedAsShort(buffer, position, Len.Int.TP_INVST1, 0);
        position += Len.TP_INVST1;
        tpInvst2 = MarshalByte.readPackedAsShort(buffer, position, Len.Int.TP_INVST2, 0);
        position += Len.TP_INVST2;
        tpInvst3 = MarshalByte.readPackedAsShort(buffer, position, Len.Int.TP_INVST3, 0);
        position += Len.TP_INVST3;
        tpInvst4 = MarshalByte.readPackedAsShort(buffer, position, Len.Int.TP_INVST4, 0);
        position += Len.TP_INVST4;
        tpInvst5 = MarshalByte.readPackedAsShort(buffer, position, Len.Int.TP_INVST5, 0);
        position += Len.TP_INVST5;
        tpInvst6 = MarshalByte.readPackedAsShort(buffer, position, Len.Int.TP_INVST6, 0);
    }

    public byte[] getLdbv9091DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tpStatBus1, Len.TP_STAT_BUS1);
        position += Len.TP_STAT_BUS1;
        MarshalByte.writeString(buffer, position, tpStatBus2, Len.TP_STAT_BUS2);
        position += Len.TP_STAT_BUS2;
        MarshalByte.writeString(buffer, position, ramo1, Len.RAMO1);
        position += Len.RAMO1;
        MarshalByte.writeString(buffer, position, ramo2, Len.RAMO2);
        position += Len.RAMO2;
        MarshalByte.writeString(buffer, position, ramo3, Len.RAMO3);
        position += Len.RAMO3;
        MarshalByte.writeString(buffer, position, ramo4, Len.RAMO4);
        position += Len.RAMO4;
        MarshalByte.writeString(buffer, position, ramo5, Len.RAMO5);
        position += Len.RAMO5;
        MarshalByte.writeString(buffer, position, ramo6, Len.RAMO6);
        position += Len.RAMO6;
        MarshalByte.writeShortAsPacked(buffer, position, tpInvst1, Len.Int.TP_INVST1, 0);
        position += Len.TP_INVST1;
        MarshalByte.writeShortAsPacked(buffer, position, tpInvst2, Len.Int.TP_INVST2, 0);
        position += Len.TP_INVST2;
        MarshalByte.writeShortAsPacked(buffer, position, tpInvst3, Len.Int.TP_INVST3, 0);
        position += Len.TP_INVST3;
        MarshalByte.writeShortAsPacked(buffer, position, tpInvst4, Len.Int.TP_INVST4, 0);
        position += Len.TP_INVST4;
        MarshalByte.writeShortAsPacked(buffer, position, tpInvst5, Len.Int.TP_INVST5, 0);
        position += Len.TP_INVST5;
        MarshalByte.writeShortAsPacked(buffer, position, tpInvst6, Len.Int.TP_INVST6, 0);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpStatBus1(String tpStatBus1) {
        this.tpStatBus1 = Functions.subString(tpStatBus1, Len.TP_STAT_BUS1);
    }

    public String getTpStatBus1() {
        return this.tpStatBus1;
    }

    public void setTpStatBus2(String tpStatBus2) {
        this.tpStatBus2 = Functions.subString(tpStatBus2, Len.TP_STAT_BUS2);
    }

    public String getTpStatBus2() {
        return this.tpStatBus2;
    }

    public void setRamo1(String ramo1) {
        this.ramo1 = Functions.subString(ramo1, Len.RAMO1);
    }

    public String getRamo1() {
        return this.ramo1;
    }

    public void setRamo2(String ramo2) {
        this.ramo2 = Functions.subString(ramo2, Len.RAMO2);
    }

    public String getRamo2() {
        return this.ramo2;
    }

    public void setRamo3(String ramo3) {
        this.ramo3 = Functions.subString(ramo3, Len.RAMO3);
    }

    public String getRamo3() {
        return this.ramo3;
    }

    public void setRamo4(String ramo4) {
        this.ramo4 = Functions.subString(ramo4, Len.RAMO4);
    }

    public String getRamo4() {
        return this.ramo4;
    }

    public void setRamo5(String ramo5) {
        this.ramo5 = Functions.subString(ramo5, Len.RAMO5);
    }

    public String getRamo5() {
        return this.ramo5;
    }

    public void setRamo6(String ramo6) {
        this.ramo6 = Functions.subString(ramo6, Len.RAMO6);
    }

    public String getRamo6() {
        return this.ramo6;
    }

    public void setTpInvst1(short tpInvst1) {
        this.tpInvst1 = tpInvst1;
    }

    public short getTpInvst1() {
        return this.tpInvst1;
    }

    public void setTpInvst2(short tpInvst2) {
        this.tpInvst2 = tpInvst2;
    }

    public short getTpInvst2() {
        return this.tpInvst2;
    }

    public void setTpInvst3(short tpInvst3) {
        this.tpInvst3 = tpInvst3;
    }

    public short getTpInvst3() {
        return this.tpInvst3;
    }

    public void setTpInvst4(short tpInvst4) {
        this.tpInvst4 = tpInvst4;
    }

    public short getTpInvst4() {
        return this.tpInvst4;
    }

    public void setTpInvst5(short tpInvst5) {
        this.tpInvst5 = tpInvst5;
    }

    public short getTpInvst5() {
        return this.tpInvst5;
    }

    public void setTpInvst6(short tpInvst6) {
        this.tpInvst6 = tpInvst6;
    }

    public short getTpInvst6() {
        return this.tpInvst6;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POLI = 5;
        public static final int ID_ADES = 5;
        public static final int TP_OGG = 2;
        public static final int TP_STAT_BUS1 = 2;
        public static final int TP_STAT_BUS2 = 2;
        public static final int RAMO1 = 2;
        public static final int RAMO2 = 2;
        public static final int RAMO3 = 2;
        public static final int RAMO4 = 2;
        public static final int RAMO5 = 2;
        public static final int RAMO6 = 2;
        public static final int TP_INVST1 = 2;
        public static final int TP_INVST2 = 2;
        public static final int TP_INVST3 = 2;
        public static final int TP_INVST4 = 2;
        public static final int TP_INVST5 = 2;
        public static final int TP_INVST6 = 2;
        public static final int LDBV9091_DATI_INPUT = ID_POLI + ID_ADES + TP_OGG + TP_STAT_BUS1 + TP_STAT_BUS2 + RAMO1 + RAMO2 + RAMO3 + RAMO4 + RAMO5 + RAMO6 + TP_INVST1 + TP_INVST2 + TP_INVST3 + TP_INVST4 + TP_INVST5 + TP_INVST6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int ID_ADES = 9;
            public static final int TP_INVST1 = 2;
            public static final int TP_INVST2 = 2;
            public static final int TP_INVST3 = 2;
            public static final int TP_INVST4 = 2;
            public static final int TP_INVST5 = 2;
            public static final int TP_INVST6 = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
