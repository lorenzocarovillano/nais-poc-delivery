package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVBJE3<br>
 * Copybook: IDBVBJE3 from copybook IDBVBJE3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvbje3 {

    //==== PROPERTIES ====
    //Original name: BJE-DT-START-DB
    private String bjeDtStartDb = DefaultValues.stringVal(Len.BJE_DT_START_DB);
    //Original name: BJE-DT-END-DB
    private String bjeDtEndDb = DefaultValues.stringVal(Len.BJE_DT_END_DB);

    //==== METHODS ====
    public void setBjeDtStartDb(String bjeDtStartDb) {
        this.bjeDtStartDb = Functions.subString(bjeDtStartDb, Len.BJE_DT_START_DB);
    }

    public String getBjeDtStartDb() {
        return this.bjeDtStartDb;
    }

    public void setBjeDtEndDb(String bjeDtEndDb) {
        this.bjeDtEndDb = Functions.subString(bjeDtEndDb, Len.BJE_DT_END_DB);
    }

    public String getBjeDtEndDb() {
        return this.bjeDtEndDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BJE_DT_START_DB = 26;
        public static final int BJE_DT_END_DB = 26;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
