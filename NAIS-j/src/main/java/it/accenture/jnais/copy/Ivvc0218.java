package it.accenture.jnais.copy;

/**Original name: IVVC0218<br>
 * Variable: IVVC0218 from copybook IVVC0218<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0218 {

    //==== PROPERTIES ====
    /**Original name: C161-ALIAS-ADES<br>
	 * <pre>----------------------------------------------------------------*
	 *    ALIAS INPUT VARIABILI
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA
	 *    LUNG.
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 * ASSET
	 * *****************************************************************
	 * --  ALIAS ADESIONE</pre>*/
    private String aliasAdes = "ADE";
    /**Original name: C161-ALIAS-POLI<br>
	 * <pre>--  ALIAS POLIZZA</pre>*/
    private String aliasPoli = "POL";
    /**Original name: C161-ALIAS-RAPP-RETE<br>
	 * <pre>--  ALIAS RAPPORTO RETE</pre>*/
    private String aliasRappRete = "RRE";

    //==== METHODS ====
    public String getAliasAdes() {
        return this.aliasAdes;
    }

    public String getAliasPoli() {
        return this.aliasPoli;
    }

    public String getAliasRappRete() {
        return this.aliasRappRete;
    }
}
