package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCONT-LIQ-DB<br>
 * Variable: TCONT-LIQ-DB from copybook IDBVTCL3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TcontLiqDb {

    //==== PROPERTIES ====
    //Original name: TCL-DT-INI-EFF-DB
    private String iniEffDb = DefaultValues.stringVal(Len.INI_EFF_DB);
    //Original name: TCL-DT-END-EFF-DB
    private String endEffDb = DefaultValues.stringVal(Len.END_EFF_DB);
    //Original name: TCL-DT-VLT-DB
    private String vltDb = DefaultValues.stringVal(Len.VLT_DB);

    //==== METHODS ====
    public void setIniEffDb(String iniEffDb) {
        this.iniEffDb = Functions.subString(iniEffDb, Len.INI_EFF_DB);
    }

    public String getIniEffDb() {
        return this.iniEffDb;
    }

    public void setEndEffDb(String endEffDb) {
        this.endEffDb = Functions.subString(endEffDb, Len.END_EFF_DB);
    }

    public String getEndEffDb() {
        return this.endEffDb;
    }

    public void setVltDb(String vltDb) {
        this.vltDb = Functions.subString(vltDb, Len.VLT_DB);
    }

    public String getVltDb() {
        return this.vltDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INI_EFF_DB = 10;
        public static final int END_EFF_DB = 10;
        public static final int VLT_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
