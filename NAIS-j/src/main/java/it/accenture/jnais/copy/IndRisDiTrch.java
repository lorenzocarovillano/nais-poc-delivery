package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-RIS-DI-TRCH<br>
 * Variable: IND-RIS-DI-TRCH from copybook IDBVRST2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndRisDiTrch {

    //==== PROPERTIES ====
    //Original name: IND-RST-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-TP-CALC-RIS
    private short tpCalcRis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-ULT-RM
    private short ultRm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-DT-CALC
    private short dtCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-DT-ELAB
    private short dtElab = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-BILA
    private short risBila = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-MAT
    private short risMat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-INCR-X-RIVAL
    private short incrXRival = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RPTO-PRE
    private short rptoPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-FRAZ-PRE-PP
    private short frazPrePp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-TOT
    private short risTot = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-SPE
    private short risSpe = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-ABB
    private short risAbb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-BNSFDT
    private short risBnsfdt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-SOPR
    private short risSopr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-INTEG-BAS-TEC
    private short risIntegBasTec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-INTEG-DECR-TS
    private short risIntegDecrTs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-GAR-CASO-MOR
    private short risGarCasoMor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-ZIL
    private short risZil = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-FAIVL
    private short risFaivl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-COS-AMMTZ
    private short risCosAmmtz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-SPE-FAIVL
    private short risSpeFaivl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-PREST-FAIVL
    private short risPrestFaivl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-COMPON-ASSVA
    private short risComponAssva = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-ULT-COEFF-RIS
    private short ultCoeffRis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-ULT-COEFF-AGG-RIS
    private short ultCoeffAggRis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-ACQ
    private short risAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-UTI
    private short risUti = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-MAT-EFF
    private short risMatEff = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-RISTORNI-CAP
    private short risRistorniCap = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-TRM-BNS
    private short risTrmBns = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-BNSRIC
    private short risBnsric = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-COD-FND
    private short codFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-MIN-GARTO
    private short risMinGarto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-RSH-DFLT
    private short risRshDflt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RST-RIS-MOVI-NON-INVES
    private short risMoviNonInves = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setTpCalcRis(short tpCalcRis) {
        this.tpCalcRis = tpCalcRis;
    }

    public short getTpCalcRis() {
        return this.tpCalcRis;
    }

    public void setUltRm(short ultRm) {
        this.ultRm = ultRm;
    }

    public short getUltRm() {
        return this.ultRm;
    }

    public void setDtCalc(short dtCalc) {
        this.dtCalc = dtCalc;
    }

    public short getDtCalc() {
        return this.dtCalc;
    }

    public void setDtElab(short dtElab) {
        this.dtElab = dtElab;
    }

    public short getDtElab() {
        return this.dtElab;
    }

    public void setRisBila(short risBila) {
        this.risBila = risBila;
    }

    public short getRisBila() {
        return this.risBila;
    }

    public void setRisMat(short risMat) {
        this.risMat = risMat;
    }

    public short getRisMat() {
        return this.risMat;
    }

    public void setIncrXRival(short incrXRival) {
        this.incrXRival = incrXRival;
    }

    public short getIncrXRival() {
        return this.incrXRival;
    }

    public void setRptoPre(short rptoPre) {
        this.rptoPre = rptoPre;
    }

    public short getRptoPre() {
        return this.rptoPre;
    }

    public void setFrazPrePp(short frazPrePp) {
        this.frazPrePp = frazPrePp;
    }

    public short getFrazPrePp() {
        return this.frazPrePp;
    }

    public void setRisTot(short risTot) {
        this.risTot = risTot;
    }

    public short getRisTot() {
        return this.risTot;
    }

    public void setRisSpe(short risSpe) {
        this.risSpe = risSpe;
    }

    public short getRisSpe() {
        return this.risSpe;
    }

    public void setRisAbb(short risAbb) {
        this.risAbb = risAbb;
    }

    public short getRisAbb() {
        return this.risAbb;
    }

    public void setRisBnsfdt(short risBnsfdt) {
        this.risBnsfdt = risBnsfdt;
    }

    public short getRisBnsfdt() {
        return this.risBnsfdt;
    }

    public void setRisSopr(short risSopr) {
        this.risSopr = risSopr;
    }

    public short getRisSopr() {
        return this.risSopr;
    }

    public void setRisIntegBasTec(short risIntegBasTec) {
        this.risIntegBasTec = risIntegBasTec;
    }

    public short getRisIntegBasTec() {
        return this.risIntegBasTec;
    }

    public void setRisIntegDecrTs(short risIntegDecrTs) {
        this.risIntegDecrTs = risIntegDecrTs;
    }

    public short getRisIntegDecrTs() {
        return this.risIntegDecrTs;
    }

    public void setRisGarCasoMor(short risGarCasoMor) {
        this.risGarCasoMor = risGarCasoMor;
    }

    public short getRisGarCasoMor() {
        return this.risGarCasoMor;
    }

    public void setRisZil(short risZil) {
        this.risZil = risZil;
    }

    public short getRisZil() {
        return this.risZil;
    }

    public void setRisFaivl(short risFaivl) {
        this.risFaivl = risFaivl;
    }

    public short getRisFaivl() {
        return this.risFaivl;
    }

    public void setRisCosAmmtz(short risCosAmmtz) {
        this.risCosAmmtz = risCosAmmtz;
    }

    public short getRisCosAmmtz() {
        return this.risCosAmmtz;
    }

    public void setRisSpeFaivl(short risSpeFaivl) {
        this.risSpeFaivl = risSpeFaivl;
    }

    public short getRisSpeFaivl() {
        return this.risSpeFaivl;
    }

    public void setRisPrestFaivl(short risPrestFaivl) {
        this.risPrestFaivl = risPrestFaivl;
    }

    public short getRisPrestFaivl() {
        return this.risPrestFaivl;
    }

    public void setRisComponAssva(short risComponAssva) {
        this.risComponAssva = risComponAssva;
    }

    public short getRisComponAssva() {
        return this.risComponAssva;
    }

    public void setUltCoeffRis(short ultCoeffRis) {
        this.ultCoeffRis = ultCoeffRis;
    }

    public short getUltCoeffRis() {
        return this.ultCoeffRis;
    }

    public void setUltCoeffAggRis(short ultCoeffAggRis) {
        this.ultCoeffAggRis = ultCoeffAggRis;
    }

    public short getUltCoeffAggRis() {
        return this.ultCoeffAggRis;
    }

    public void setRisAcq(short risAcq) {
        this.risAcq = risAcq;
    }

    public short getRisAcq() {
        return this.risAcq;
    }

    public void setRisUti(short risUti) {
        this.risUti = risUti;
    }

    public short getRisUti() {
        return this.risUti;
    }

    public void setRisMatEff(short risMatEff) {
        this.risMatEff = risMatEff;
    }

    public short getRisMatEff() {
        return this.risMatEff;
    }

    public void setRisRistorniCap(short risRistorniCap) {
        this.risRistorniCap = risRistorniCap;
    }

    public short getRisRistorniCap() {
        return this.risRistorniCap;
    }

    public void setRisTrmBns(short risTrmBns) {
        this.risTrmBns = risTrmBns;
    }

    public short getRisTrmBns() {
        return this.risTrmBns;
    }

    public void setRisBnsric(short risBnsric) {
        this.risBnsric = risBnsric;
    }

    public short getRisBnsric() {
        return this.risBnsric;
    }

    public void setCodFnd(short codFnd) {
        this.codFnd = codFnd;
    }

    public short getCodFnd() {
        return this.codFnd;
    }

    public void setRisMinGarto(short risMinGarto) {
        this.risMinGarto = risMinGarto;
    }

    public short getRisMinGarto() {
        return this.risMinGarto;
    }

    public void setRisRshDflt(short risRshDflt) {
        this.risRshDflt = risRshDflt;
    }

    public short getRisRshDflt() {
        return this.risRshDflt;
    }

    public void setRisMoviNonInves(short risMoviNonInves) {
        this.risMoviNonInves = risMoviNonInves;
    }

    public short getRisMoviNonInves() {
        return this.risMoviNonInves;
    }
}
