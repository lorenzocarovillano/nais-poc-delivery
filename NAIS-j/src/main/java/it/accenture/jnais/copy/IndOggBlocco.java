package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-OGG-BLOCCO<br>
 * Variable: IND-OGG-BLOCCO from copybook IDBVL112<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndOggBlocco {

    //==== PROPERTIES ====
    //Original name: IND-L11-ID-OGG-1RIO
    private short idOgg1rio = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L11-TP-OGG-1RIO
    private short tpOgg1rio = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L11-ID-RICH
    private short idRich = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdOgg1rio(short idOgg1rio) {
        this.idOgg1rio = idOgg1rio;
    }

    public short getIdOgg1rio() {
        return this.idOgg1rio;
    }

    public void setTpOgg1rio(short tpOgg1rio) {
        this.tpOgg1rio = tpOgg1rio;
    }

    public short getTpOgg1rio() {
        return this.tpOgg1rio;
    }

    public void setIdRich(short idRich) {
        this.idRich = idRich;
    }

    public short getIdRich() {
        return this.idRich;
    }
}
