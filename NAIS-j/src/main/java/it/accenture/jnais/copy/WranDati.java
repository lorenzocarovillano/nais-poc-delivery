package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WranDtDeces;
import it.accenture.jnais.ws.redefines.WranDtDeliberaCda;
import it.accenture.jnais.ws.redefines.WranDtNasc;
import it.accenture.jnais.ws.redefines.WranIdMoviChiu;
import it.accenture.jnais.ws.redefines.WranIdRappAnaCollg;
import it.accenture.jnais.ws.redefines.WranPcNelRapp;

/**Original name: WRAN-DATI<br>
 * Variable: WRAN-DATI from copybook LCCVRAN1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WranDati {

    //==== PROPERTIES ====
    //Original name: WRAN-ID-RAPP-ANA
    private int wranIdRappAna = DefaultValues.INT_VAL;
    //Original name: WRAN-ID-RAPP-ANA-COLLG
    private WranIdRappAnaCollg wranIdRappAnaCollg = new WranIdRappAnaCollg();
    //Original name: WRAN-ID-OGG
    private int wranIdOgg = DefaultValues.INT_VAL;
    //Original name: WRAN-TP-OGG
    private String wranTpOgg = DefaultValues.stringVal(Len.WRAN_TP_OGG);
    //Original name: WRAN-ID-MOVI-CRZ
    private int wranIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WRAN-ID-MOVI-CHIU
    private WranIdMoviChiu wranIdMoviChiu = new WranIdMoviChiu();
    //Original name: WRAN-DT-INI-EFF
    private int wranDtIniEff = DefaultValues.INT_VAL;
    //Original name: WRAN-DT-END-EFF
    private int wranDtEndEff = DefaultValues.INT_VAL;
    //Original name: WRAN-COD-COMP-ANIA
    private int wranCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WRAN-COD-SOGG
    private String wranCodSogg = DefaultValues.stringVal(Len.WRAN_COD_SOGG);
    //Original name: WRAN-TP-RAPP-ANA
    private String wranTpRappAna = DefaultValues.stringVal(Len.WRAN_TP_RAPP_ANA);
    //Original name: WRAN-TP-PERS
    private char wranTpPers = DefaultValues.CHAR_VAL;
    //Original name: WRAN-SEX
    private char wranSex = DefaultValues.CHAR_VAL;
    //Original name: WRAN-DT-NASC
    private WranDtNasc wranDtNasc = new WranDtNasc();
    //Original name: WRAN-FL-ESTAS
    private char wranFlEstas = DefaultValues.CHAR_VAL;
    //Original name: WRAN-INDIR-1
    private String wranIndir1 = DefaultValues.stringVal(Len.WRAN_INDIR1);
    //Original name: WRAN-INDIR-2
    private String wranIndir2 = DefaultValues.stringVal(Len.WRAN_INDIR2);
    //Original name: WRAN-INDIR-3
    private String wranIndir3 = DefaultValues.stringVal(Len.WRAN_INDIR3);
    //Original name: WRAN-TP-UTLZ-INDIR-1
    private String wranTpUtlzIndir1 = DefaultValues.stringVal(Len.WRAN_TP_UTLZ_INDIR1);
    //Original name: WRAN-TP-UTLZ-INDIR-2
    private String wranTpUtlzIndir2 = DefaultValues.stringVal(Len.WRAN_TP_UTLZ_INDIR2);
    //Original name: WRAN-TP-UTLZ-INDIR-3
    private String wranTpUtlzIndir3 = DefaultValues.stringVal(Len.WRAN_TP_UTLZ_INDIR3);
    //Original name: WRAN-ESTR-CNT-CORR-ACCR
    private String wranEstrCntCorrAccr = DefaultValues.stringVal(Len.WRAN_ESTR_CNT_CORR_ACCR);
    //Original name: WRAN-ESTR-CNT-CORR-ADD
    private String wranEstrCntCorrAdd = DefaultValues.stringVal(Len.WRAN_ESTR_CNT_CORR_ADD);
    //Original name: WRAN-ESTR-DOCTO
    private String wranEstrDocto = DefaultValues.stringVal(Len.WRAN_ESTR_DOCTO);
    //Original name: WRAN-PC-NEL-RAPP
    private WranPcNelRapp wranPcNelRapp = new WranPcNelRapp();
    //Original name: WRAN-TP-MEZ-PAG-ADD
    private String wranTpMezPagAdd = DefaultValues.stringVal(Len.WRAN_TP_MEZ_PAG_ADD);
    //Original name: WRAN-TP-MEZ-PAG-ACCR
    private String wranTpMezPagAccr = DefaultValues.stringVal(Len.WRAN_TP_MEZ_PAG_ACCR);
    //Original name: WRAN-COD-MATR
    private String wranCodMatr = DefaultValues.stringVal(Len.WRAN_COD_MATR);
    //Original name: WRAN-TP-ADEGZ
    private String wranTpAdegz = DefaultValues.stringVal(Len.WRAN_TP_ADEGZ);
    //Original name: WRAN-FL-TST-RSH
    private char wranFlTstRsh = DefaultValues.CHAR_VAL;
    //Original name: WRAN-COD-AZ
    private String wranCodAz = DefaultValues.stringVal(Len.WRAN_COD_AZ);
    //Original name: WRAN-IND-PRINC
    private String wranIndPrinc = DefaultValues.stringVal(Len.WRAN_IND_PRINC);
    //Original name: WRAN-DT-DELIBERA-CDA
    private WranDtDeliberaCda wranDtDeliberaCda = new WranDtDeliberaCda();
    //Original name: WRAN-DLG-AL-RISC
    private char wranDlgAlRisc = DefaultValues.CHAR_VAL;
    //Original name: WRAN-LEGALE-RAPPR-PRINC
    private char wranLegaleRapprPrinc = DefaultValues.CHAR_VAL;
    //Original name: WRAN-TP-LEGALE-RAPPR
    private String wranTpLegaleRappr = DefaultValues.stringVal(Len.WRAN_TP_LEGALE_RAPPR);
    //Original name: WRAN-TP-IND-PRINC
    private String wranTpIndPrinc = DefaultValues.stringVal(Len.WRAN_TP_IND_PRINC);
    //Original name: WRAN-TP-STAT-RID
    private String wranTpStatRid = DefaultValues.stringVal(Len.WRAN_TP_STAT_RID);
    //Original name: WRAN-NOME-INT-RID
    private String wranNomeIntRid = DefaultValues.stringVal(Len.WRAN_NOME_INT_RID);
    //Original name: WRAN-COGN-INT-RID
    private String wranCognIntRid = DefaultValues.stringVal(Len.WRAN_COGN_INT_RID);
    //Original name: WRAN-COGN-INT-TRATT
    private String wranCognIntTratt = DefaultValues.stringVal(Len.WRAN_COGN_INT_TRATT);
    //Original name: WRAN-NOME-INT-TRATT
    private String wranNomeIntTratt = DefaultValues.stringVal(Len.WRAN_NOME_INT_TRATT);
    //Original name: WRAN-CF-INT-RID
    private String wranCfIntRid = DefaultValues.stringVal(Len.WRAN_CF_INT_RID);
    //Original name: WRAN-FL-COINC-DIP-CNTR
    private char wranFlCoincDipCntr = DefaultValues.CHAR_VAL;
    //Original name: WRAN-FL-COINC-INT-CNTR
    private char wranFlCoincIntCntr = DefaultValues.CHAR_VAL;
    //Original name: WRAN-DT-DECES
    private WranDtDeces wranDtDeces = new WranDtDeces();
    //Original name: WRAN-FL-FUMATORE
    private char wranFlFumatore = DefaultValues.CHAR_VAL;
    //Original name: WRAN-DS-RIGA
    private long wranDsRiga = DefaultValues.LONG_VAL;
    //Original name: WRAN-DS-OPER-SQL
    private char wranDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WRAN-DS-VER
    private int wranDsVer = DefaultValues.INT_VAL;
    //Original name: WRAN-DS-TS-INI-CPTZ
    private long wranDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WRAN-DS-TS-END-CPTZ
    private long wranDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WRAN-DS-UTENTE
    private String wranDsUtente = DefaultValues.stringVal(Len.WRAN_DS_UTENTE);
    //Original name: WRAN-DS-STATO-ELAB
    private char wranDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WRAN-FL-LAV-DIP
    private char wranFlLavDip = DefaultValues.CHAR_VAL;
    //Original name: WRAN-TP-VARZ-PAGAT
    private String wranTpVarzPagat = DefaultValues.stringVal(Len.WRAN_TP_VARZ_PAGAT);
    //Original name: WRAN-COD-RID
    private String wranCodRid = DefaultValues.stringVal(Len.WRAN_COD_RID);
    //Original name: WRAN-TP-CAUS-RID
    private String wranTpCausRid = DefaultValues.stringVal(Len.WRAN_TP_CAUS_RID);
    //Original name: WRAN-IND-MASSA-CORP
    private String wranIndMassaCorp = DefaultValues.stringVal(Len.WRAN_IND_MASSA_CORP);
    //Original name: WRAN-CAT-RSH-PROF
    private String wranCatRshProf = DefaultValues.stringVal(Len.WRAN_CAT_RSH_PROF);

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wranIdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRAN_ID_RAPP_ANA, 0);
        position += Len.WRAN_ID_RAPP_ANA;
        wranIdRappAnaCollg.setWranIdRappAnaCollgFromBuffer(buffer, position);
        position += WranIdRappAnaCollg.Len.WRAN_ID_RAPP_ANA_COLLG;
        wranIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRAN_ID_OGG, 0);
        position += Len.WRAN_ID_OGG;
        wranTpOgg = MarshalByte.readString(buffer, position, Len.WRAN_TP_OGG);
        position += Len.WRAN_TP_OGG;
        wranIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRAN_ID_MOVI_CRZ, 0);
        position += Len.WRAN_ID_MOVI_CRZ;
        wranIdMoviChiu.setWranIdMoviChiuFromBuffer(buffer, position);
        position += WranIdMoviChiu.Len.WRAN_ID_MOVI_CHIU;
        wranDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRAN_DT_INI_EFF, 0);
        position += Len.WRAN_DT_INI_EFF;
        wranDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRAN_DT_END_EFF, 0);
        position += Len.WRAN_DT_END_EFF;
        wranCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRAN_COD_COMP_ANIA, 0);
        position += Len.WRAN_COD_COMP_ANIA;
        wranCodSogg = MarshalByte.readString(buffer, position, Len.WRAN_COD_SOGG);
        position += Len.WRAN_COD_SOGG;
        wranTpRappAna = MarshalByte.readString(buffer, position, Len.WRAN_TP_RAPP_ANA);
        position += Len.WRAN_TP_RAPP_ANA;
        wranTpPers = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranSex = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranDtNasc.setWranDtNascFromBuffer(buffer, position);
        position += WranDtNasc.Len.WRAN_DT_NASC;
        wranFlEstas = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranIndir1 = MarshalByte.readString(buffer, position, Len.WRAN_INDIR1);
        position += Len.WRAN_INDIR1;
        wranIndir2 = MarshalByte.readString(buffer, position, Len.WRAN_INDIR2);
        position += Len.WRAN_INDIR2;
        wranIndir3 = MarshalByte.readString(buffer, position, Len.WRAN_INDIR3);
        position += Len.WRAN_INDIR3;
        wranTpUtlzIndir1 = MarshalByte.readString(buffer, position, Len.WRAN_TP_UTLZ_INDIR1);
        position += Len.WRAN_TP_UTLZ_INDIR1;
        wranTpUtlzIndir2 = MarshalByte.readString(buffer, position, Len.WRAN_TP_UTLZ_INDIR2);
        position += Len.WRAN_TP_UTLZ_INDIR2;
        wranTpUtlzIndir3 = MarshalByte.readString(buffer, position, Len.WRAN_TP_UTLZ_INDIR3);
        position += Len.WRAN_TP_UTLZ_INDIR3;
        wranEstrCntCorrAccr = MarshalByte.readString(buffer, position, Len.WRAN_ESTR_CNT_CORR_ACCR);
        position += Len.WRAN_ESTR_CNT_CORR_ACCR;
        wranEstrCntCorrAdd = MarshalByte.readString(buffer, position, Len.WRAN_ESTR_CNT_CORR_ADD);
        position += Len.WRAN_ESTR_CNT_CORR_ADD;
        wranEstrDocto = MarshalByte.readString(buffer, position, Len.WRAN_ESTR_DOCTO);
        position += Len.WRAN_ESTR_DOCTO;
        wranPcNelRapp.setWranPcNelRappFromBuffer(buffer, position);
        position += WranPcNelRapp.Len.WRAN_PC_NEL_RAPP;
        wranTpMezPagAdd = MarshalByte.readString(buffer, position, Len.WRAN_TP_MEZ_PAG_ADD);
        position += Len.WRAN_TP_MEZ_PAG_ADD;
        wranTpMezPagAccr = MarshalByte.readString(buffer, position, Len.WRAN_TP_MEZ_PAG_ACCR);
        position += Len.WRAN_TP_MEZ_PAG_ACCR;
        wranCodMatr = MarshalByte.readString(buffer, position, Len.WRAN_COD_MATR);
        position += Len.WRAN_COD_MATR;
        wranTpAdegz = MarshalByte.readString(buffer, position, Len.WRAN_TP_ADEGZ);
        position += Len.WRAN_TP_ADEGZ;
        wranFlTstRsh = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranCodAz = MarshalByte.readString(buffer, position, Len.WRAN_COD_AZ);
        position += Len.WRAN_COD_AZ;
        wranIndPrinc = MarshalByte.readString(buffer, position, Len.WRAN_IND_PRINC);
        position += Len.WRAN_IND_PRINC;
        wranDtDeliberaCda.setWranDtDeliberaCdaFromBuffer(buffer, position);
        position += WranDtDeliberaCda.Len.WRAN_DT_DELIBERA_CDA;
        wranDlgAlRisc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranLegaleRapprPrinc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranTpLegaleRappr = MarshalByte.readString(buffer, position, Len.WRAN_TP_LEGALE_RAPPR);
        position += Len.WRAN_TP_LEGALE_RAPPR;
        wranTpIndPrinc = MarshalByte.readString(buffer, position, Len.WRAN_TP_IND_PRINC);
        position += Len.WRAN_TP_IND_PRINC;
        wranTpStatRid = MarshalByte.readString(buffer, position, Len.WRAN_TP_STAT_RID);
        position += Len.WRAN_TP_STAT_RID;
        wranNomeIntRid = MarshalByte.readString(buffer, position, Len.WRAN_NOME_INT_RID);
        position += Len.WRAN_NOME_INT_RID;
        wranCognIntRid = MarshalByte.readString(buffer, position, Len.WRAN_COGN_INT_RID);
        position += Len.WRAN_COGN_INT_RID;
        wranCognIntTratt = MarshalByte.readString(buffer, position, Len.WRAN_COGN_INT_TRATT);
        position += Len.WRAN_COGN_INT_TRATT;
        wranNomeIntTratt = MarshalByte.readString(buffer, position, Len.WRAN_NOME_INT_TRATT);
        position += Len.WRAN_NOME_INT_TRATT;
        wranCfIntRid = MarshalByte.readString(buffer, position, Len.WRAN_CF_INT_RID);
        position += Len.WRAN_CF_INT_RID;
        wranFlCoincDipCntr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranFlCoincIntCntr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranDtDeces.setWranDtDecesFromBuffer(buffer, position);
        position += WranDtDeces.Len.WRAN_DT_DECES;
        wranFlFumatore = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRAN_DS_RIGA, 0);
        position += Len.WRAN_DS_RIGA;
        wranDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRAN_DS_VER, 0);
        position += Len.WRAN_DS_VER;
        wranDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRAN_DS_TS_INI_CPTZ, 0);
        position += Len.WRAN_DS_TS_INI_CPTZ;
        wranDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRAN_DS_TS_END_CPTZ, 0);
        position += Len.WRAN_DS_TS_END_CPTZ;
        wranDsUtente = MarshalByte.readString(buffer, position, Len.WRAN_DS_UTENTE);
        position += Len.WRAN_DS_UTENTE;
        wranDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranFlLavDip = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wranTpVarzPagat = MarshalByte.readString(buffer, position, Len.WRAN_TP_VARZ_PAGAT);
        position += Len.WRAN_TP_VARZ_PAGAT;
        wranCodRid = MarshalByte.readString(buffer, position, Len.WRAN_COD_RID);
        position += Len.WRAN_COD_RID;
        wranTpCausRid = MarshalByte.readString(buffer, position, Len.WRAN_TP_CAUS_RID);
        position += Len.WRAN_TP_CAUS_RID;
        wranIndMassaCorp = MarshalByte.readString(buffer, position, Len.WRAN_IND_MASSA_CORP);
        position += Len.WRAN_IND_MASSA_CORP;
        wranCatRshProf = MarshalByte.readString(buffer, position, Len.WRAN_CAT_RSH_PROF);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wranIdRappAna, Len.Int.WRAN_ID_RAPP_ANA, 0);
        position += Len.WRAN_ID_RAPP_ANA;
        wranIdRappAnaCollg.getWranIdRappAnaCollgAsBuffer(buffer, position);
        position += WranIdRappAnaCollg.Len.WRAN_ID_RAPP_ANA_COLLG;
        MarshalByte.writeIntAsPacked(buffer, position, wranIdOgg, Len.Int.WRAN_ID_OGG, 0);
        position += Len.WRAN_ID_OGG;
        MarshalByte.writeString(buffer, position, wranTpOgg, Len.WRAN_TP_OGG);
        position += Len.WRAN_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, wranIdMoviCrz, Len.Int.WRAN_ID_MOVI_CRZ, 0);
        position += Len.WRAN_ID_MOVI_CRZ;
        wranIdMoviChiu.getWranIdMoviChiuAsBuffer(buffer, position);
        position += WranIdMoviChiu.Len.WRAN_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wranDtIniEff, Len.Int.WRAN_DT_INI_EFF, 0);
        position += Len.WRAN_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wranDtEndEff, Len.Int.WRAN_DT_END_EFF, 0);
        position += Len.WRAN_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wranCodCompAnia, Len.Int.WRAN_COD_COMP_ANIA, 0);
        position += Len.WRAN_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wranCodSogg, Len.WRAN_COD_SOGG);
        position += Len.WRAN_COD_SOGG;
        MarshalByte.writeString(buffer, position, wranTpRappAna, Len.WRAN_TP_RAPP_ANA);
        position += Len.WRAN_TP_RAPP_ANA;
        MarshalByte.writeChar(buffer, position, wranTpPers);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wranSex);
        position += Types.CHAR_SIZE;
        wranDtNasc.getWranDtNascAsBuffer(buffer, position);
        position += WranDtNasc.Len.WRAN_DT_NASC;
        MarshalByte.writeChar(buffer, position, wranFlEstas);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wranIndir1, Len.WRAN_INDIR1);
        position += Len.WRAN_INDIR1;
        MarshalByte.writeString(buffer, position, wranIndir2, Len.WRAN_INDIR2);
        position += Len.WRAN_INDIR2;
        MarshalByte.writeString(buffer, position, wranIndir3, Len.WRAN_INDIR3);
        position += Len.WRAN_INDIR3;
        MarshalByte.writeString(buffer, position, wranTpUtlzIndir1, Len.WRAN_TP_UTLZ_INDIR1);
        position += Len.WRAN_TP_UTLZ_INDIR1;
        MarshalByte.writeString(buffer, position, wranTpUtlzIndir2, Len.WRAN_TP_UTLZ_INDIR2);
        position += Len.WRAN_TP_UTLZ_INDIR2;
        MarshalByte.writeString(buffer, position, wranTpUtlzIndir3, Len.WRAN_TP_UTLZ_INDIR3);
        position += Len.WRAN_TP_UTLZ_INDIR3;
        MarshalByte.writeString(buffer, position, wranEstrCntCorrAccr, Len.WRAN_ESTR_CNT_CORR_ACCR);
        position += Len.WRAN_ESTR_CNT_CORR_ACCR;
        MarshalByte.writeString(buffer, position, wranEstrCntCorrAdd, Len.WRAN_ESTR_CNT_CORR_ADD);
        position += Len.WRAN_ESTR_CNT_CORR_ADD;
        MarshalByte.writeString(buffer, position, wranEstrDocto, Len.WRAN_ESTR_DOCTO);
        position += Len.WRAN_ESTR_DOCTO;
        wranPcNelRapp.getWranPcNelRappAsBuffer(buffer, position);
        position += WranPcNelRapp.Len.WRAN_PC_NEL_RAPP;
        MarshalByte.writeString(buffer, position, wranTpMezPagAdd, Len.WRAN_TP_MEZ_PAG_ADD);
        position += Len.WRAN_TP_MEZ_PAG_ADD;
        MarshalByte.writeString(buffer, position, wranTpMezPagAccr, Len.WRAN_TP_MEZ_PAG_ACCR);
        position += Len.WRAN_TP_MEZ_PAG_ACCR;
        MarshalByte.writeString(buffer, position, wranCodMatr, Len.WRAN_COD_MATR);
        position += Len.WRAN_COD_MATR;
        MarshalByte.writeString(buffer, position, wranTpAdegz, Len.WRAN_TP_ADEGZ);
        position += Len.WRAN_TP_ADEGZ;
        MarshalByte.writeChar(buffer, position, wranFlTstRsh);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wranCodAz, Len.WRAN_COD_AZ);
        position += Len.WRAN_COD_AZ;
        MarshalByte.writeString(buffer, position, wranIndPrinc, Len.WRAN_IND_PRINC);
        position += Len.WRAN_IND_PRINC;
        wranDtDeliberaCda.getWranDtDeliberaCdaAsBuffer(buffer, position);
        position += WranDtDeliberaCda.Len.WRAN_DT_DELIBERA_CDA;
        MarshalByte.writeChar(buffer, position, wranDlgAlRisc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wranLegaleRapprPrinc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wranTpLegaleRappr, Len.WRAN_TP_LEGALE_RAPPR);
        position += Len.WRAN_TP_LEGALE_RAPPR;
        MarshalByte.writeString(buffer, position, wranTpIndPrinc, Len.WRAN_TP_IND_PRINC);
        position += Len.WRAN_TP_IND_PRINC;
        MarshalByte.writeString(buffer, position, wranTpStatRid, Len.WRAN_TP_STAT_RID);
        position += Len.WRAN_TP_STAT_RID;
        MarshalByte.writeString(buffer, position, wranNomeIntRid, Len.WRAN_NOME_INT_RID);
        position += Len.WRAN_NOME_INT_RID;
        MarshalByte.writeString(buffer, position, wranCognIntRid, Len.WRAN_COGN_INT_RID);
        position += Len.WRAN_COGN_INT_RID;
        MarshalByte.writeString(buffer, position, wranCognIntTratt, Len.WRAN_COGN_INT_TRATT);
        position += Len.WRAN_COGN_INT_TRATT;
        MarshalByte.writeString(buffer, position, wranNomeIntTratt, Len.WRAN_NOME_INT_TRATT);
        position += Len.WRAN_NOME_INT_TRATT;
        MarshalByte.writeString(buffer, position, wranCfIntRid, Len.WRAN_CF_INT_RID);
        position += Len.WRAN_CF_INT_RID;
        MarshalByte.writeChar(buffer, position, wranFlCoincDipCntr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wranFlCoincIntCntr);
        position += Types.CHAR_SIZE;
        wranDtDeces.getWranDtDecesAsBuffer(buffer, position);
        position += WranDtDeces.Len.WRAN_DT_DECES;
        MarshalByte.writeChar(buffer, position, wranFlFumatore);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, wranDsRiga, Len.Int.WRAN_DS_RIGA, 0);
        position += Len.WRAN_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wranDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wranDsVer, Len.Int.WRAN_DS_VER, 0);
        position += Len.WRAN_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wranDsTsIniCptz, Len.Int.WRAN_DS_TS_INI_CPTZ, 0);
        position += Len.WRAN_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wranDsTsEndCptz, Len.Int.WRAN_DS_TS_END_CPTZ, 0);
        position += Len.WRAN_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wranDsUtente, Len.WRAN_DS_UTENTE);
        position += Len.WRAN_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wranDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wranFlLavDip);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wranTpVarzPagat, Len.WRAN_TP_VARZ_PAGAT);
        position += Len.WRAN_TP_VARZ_PAGAT;
        MarshalByte.writeString(buffer, position, wranCodRid, Len.WRAN_COD_RID);
        position += Len.WRAN_COD_RID;
        MarshalByte.writeString(buffer, position, wranTpCausRid, Len.WRAN_TP_CAUS_RID);
        position += Len.WRAN_TP_CAUS_RID;
        MarshalByte.writeString(buffer, position, wranIndMassaCorp, Len.WRAN_IND_MASSA_CORP);
        position += Len.WRAN_IND_MASSA_CORP;
        MarshalByte.writeString(buffer, position, wranCatRshProf, Len.WRAN_CAT_RSH_PROF);
        return buffer;
    }

    public void initDatiSpaces() {
        wranIdRappAna = Types.INVALID_INT_VAL;
        wranIdRappAnaCollg.initWranIdRappAnaCollgSpaces();
        wranIdOgg = Types.INVALID_INT_VAL;
        wranTpOgg = "";
        wranIdMoviCrz = Types.INVALID_INT_VAL;
        wranIdMoviChiu.initWranIdMoviChiuSpaces();
        wranDtIniEff = Types.INVALID_INT_VAL;
        wranDtEndEff = Types.INVALID_INT_VAL;
        wranCodCompAnia = Types.INVALID_INT_VAL;
        wranCodSogg = "";
        wranTpRappAna = "";
        wranTpPers = Types.SPACE_CHAR;
        wranSex = Types.SPACE_CHAR;
        wranDtNasc.initWranDtNascSpaces();
        wranFlEstas = Types.SPACE_CHAR;
        wranIndir1 = "";
        wranIndir2 = "";
        wranIndir3 = "";
        wranTpUtlzIndir1 = "";
        wranTpUtlzIndir2 = "";
        wranTpUtlzIndir3 = "";
        wranEstrCntCorrAccr = "";
        wranEstrCntCorrAdd = "";
        wranEstrDocto = "";
        wranPcNelRapp.initWranPcNelRappSpaces();
        wranTpMezPagAdd = "";
        wranTpMezPagAccr = "";
        wranCodMatr = "";
        wranTpAdegz = "";
        wranFlTstRsh = Types.SPACE_CHAR;
        wranCodAz = "";
        wranIndPrinc = "";
        wranDtDeliberaCda.initWranDtDeliberaCdaSpaces();
        wranDlgAlRisc = Types.SPACE_CHAR;
        wranLegaleRapprPrinc = Types.SPACE_CHAR;
        wranTpLegaleRappr = "";
        wranTpIndPrinc = "";
        wranTpStatRid = "";
        wranNomeIntRid = "";
        wranCognIntRid = "";
        wranCognIntTratt = "";
        wranNomeIntTratt = "";
        wranCfIntRid = "";
        wranFlCoincDipCntr = Types.SPACE_CHAR;
        wranFlCoincIntCntr = Types.SPACE_CHAR;
        wranDtDeces.initWranDtDecesSpaces();
        wranFlFumatore = Types.SPACE_CHAR;
        wranDsRiga = Types.INVALID_LONG_VAL;
        wranDsOperSql = Types.SPACE_CHAR;
        wranDsVer = Types.INVALID_INT_VAL;
        wranDsTsIniCptz = Types.INVALID_LONG_VAL;
        wranDsTsEndCptz = Types.INVALID_LONG_VAL;
        wranDsUtente = "";
        wranDsStatoElab = Types.SPACE_CHAR;
        wranFlLavDip = Types.SPACE_CHAR;
        wranTpVarzPagat = "";
        wranCodRid = "";
        wranTpCausRid = "";
        wranIndMassaCorp = "";
        wranCatRshProf = "";
    }

    public void setWranIdRappAna(int wranIdRappAna) {
        this.wranIdRappAna = wranIdRappAna;
    }

    public int getWranIdRappAna() {
        return this.wranIdRappAna;
    }

    public void setWranIdOgg(int wranIdOgg) {
        this.wranIdOgg = wranIdOgg;
    }

    public int getWranIdOgg() {
        return this.wranIdOgg;
    }

    public void setWranTpOgg(String wranTpOgg) {
        this.wranTpOgg = Functions.subString(wranTpOgg, Len.WRAN_TP_OGG);
    }

    public String getWranTpOgg() {
        return this.wranTpOgg;
    }

    public void setWranIdMoviCrz(int wranIdMoviCrz) {
        this.wranIdMoviCrz = wranIdMoviCrz;
    }

    public int getWranIdMoviCrz() {
        return this.wranIdMoviCrz;
    }

    public void setWranDtIniEff(int wranDtIniEff) {
        this.wranDtIniEff = wranDtIniEff;
    }

    public int getWranDtIniEff() {
        return this.wranDtIniEff;
    }

    public void setWranDtEndEff(int wranDtEndEff) {
        this.wranDtEndEff = wranDtEndEff;
    }

    public int getWranDtEndEff() {
        return this.wranDtEndEff;
    }

    public void setWranCodCompAnia(int wranCodCompAnia) {
        this.wranCodCompAnia = wranCodCompAnia;
    }

    public int getWranCodCompAnia() {
        return this.wranCodCompAnia;
    }

    public void setWranCodSogg(String wranCodSogg) {
        this.wranCodSogg = Functions.subString(wranCodSogg, Len.WRAN_COD_SOGG);
    }

    public String getWranCodSogg() {
        return this.wranCodSogg;
    }

    public String getDranCodSoggFormatted() {
        return Functions.padBlanks(getWranCodSogg(), Len.WRAN_COD_SOGG);
    }

    public void setWranTpRappAna(String wranTpRappAna) {
        this.wranTpRappAna = Functions.subString(wranTpRappAna, Len.WRAN_TP_RAPP_ANA);
    }

    public String getWranTpRappAna() {
        return this.wranTpRappAna;
    }

    public void setWranTpPers(char wranTpPers) {
        this.wranTpPers = wranTpPers;
    }

    public char getWranTpPers() {
        return this.wranTpPers;
    }

    public void setWranSex(char wranSex) {
        this.wranSex = wranSex;
    }

    public char getWranSex() {
        return this.wranSex;
    }

    public void setWranFlEstas(char wranFlEstas) {
        this.wranFlEstas = wranFlEstas;
    }

    public char getWranFlEstas() {
        return this.wranFlEstas;
    }

    public void setWranIndir1(String wranIndir1) {
        this.wranIndir1 = Functions.subString(wranIndir1, Len.WRAN_INDIR1);
    }

    public String getWranIndir1() {
        return this.wranIndir1;
    }

    public void setWranIndir2(String wranIndir2) {
        this.wranIndir2 = Functions.subString(wranIndir2, Len.WRAN_INDIR2);
    }

    public String getWranIndir2() {
        return this.wranIndir2;
    }

    public void setWranIndir3(String wranIndir3) {
        this.wranIndir3 = Functions.subString(wranIndir3, Len.WRAN_INDIR3);
    }

    public String getWranIndir3() {
        return this.wranIndir3;
    }

    public void setWranTpUtlzIndir1(String wranTpUtlzIndir1) {
        this.wranTpUtlzIndir1 = Functions.subString(wranTpUtlzIndir1, Len.WRAN_TP_UTLZ_INDIR1);
    }

    public String getWranTpUtlzIndir1() {
        return this.wranTpUtlzIndir1;
    }

    public void setWranTpUtlzIndir2(String wranTpUtlzIndir2) {
        this.wranTpUtlzIndir2 = Functions.subString(wranTpUtlzIndir2, Len.WRAN_TP_UTLZ_INDIR2);
    }

    public String getWranTpUtlzIndir2() {
        return this.wranTpUtlzIndir2;
    }

    public void setWranTpUtlzIndir3(String wranTpUtlzIndir3) {
        this.wranTpUtlzIndir3 = Functions.subString(wranTpUtlzIndir3, Len.WRAN_TP_UTLZ_INDIR3);
    }

    public String getWranTpUtlzIndir3() {
        return this.wranTpUtlzIndir3;
    }

    public void setWranEstrCntCorrAccr(String wranEstrCntCorrAccr) {
        this.wranEstrCntCorrAccr = Functions.subString(wranEstrCntCorrAccr, Len.WRAN_ESTR_CNT_CORR_ACCR);
    }

    public String getWranEstrCntCorrAccr() {
        return this.wranEstrCntCorrAccr;
    }

    public void setWranEstrCntCorrAdd(String wranEstrCntCorrAdd) {
        this.wranEstrCntCorrAdd = Functions.subString(wranEstrCntCorrAdd, Len.WRAN_ESTR_CNT_CORR_ADD);
    }

    public String getWranEstrCntCorrAdd() {
        return this.wranEstrCntCorrAdd;
    }

    public void setWranEstrDocto(String wranEstrDocto) {
        this.wranEstrDocto = Functions.subString(wranEstrDocto, Len.WRAN_ESTR_DOCTO);
    }

    public String getWranEstrDocto() {
        return this.wranEstrDocto;
    }

    public void setWranTpMezPagAdd(String wranTpMezPagAdd) {
        this.wranTpMezPagAdd = Functions.subString(wranTpMezPagAdd, Len.WRAN_TP_MEZ_PAG_ADD);
    }

    public String getWranTpMezPagAdd() {
        return this.wranTpMezPagAdd;
    }

    public void setWranTpMezPagAccr(String wranTpMezPagAccr) {
        this.wranTpMezPagAccr = Functions.subString(wranTpMezPagAccr, Len.WRAN_TP_MEZ_PAG_ACCR);
    }

    public String getWranTpMezPagAccr() {
        return this.wranTpMezPagAccr;
    }

    public void setWranCodMatr(String wranCodMatr) {
        this.wranCodMatr = Functions.subString(wranCodMatr, Len.WRAN_COD_MATR);
    }

    public String getWranCodMatr() {
        return this.wranCodMatr;
    }

    public void setWranTpAdegz(String wranTpAdegz) {
        this.wranTpAdegz = Functions.subString(wranTpAdegz, Len.WRAN_TP_ADEGZ);
    }

    public String getWranTpAdegz() {
        return this.wranTpAdegz;
    }

    public void setWranFlTstRsh(char wranFlTstRsh) {
        this.wranFlTstRsh = wranFlTstRsh;
    }

    public char getWranFlTstRsh() {
        return this.wranFlTstRsh;
    }

    public void setWranCodAz(String wranCodAz) {
        this.wranCodAz = Functions.subString(wranCodAz, Len.WRAN_COD_AZ);
    }

    public String getWranCodAz() {
        return this.wranCodAz;
    }

    public void setWranIndPrinc(String wranIndPrinc) {
        this.wranIndPrinc = Functions.subString(wranIndPrinc, Len.WRAN_IND_PRINC);
    }

    public String getWranIndPrinc() {
        return this.wranIndPrinc;
    }

    public void setWranDlgAlRisc(char wranDlgAlRisc) {
        this.wranDlgAlRisc = wranDlgAlRisc;
    }

    public char getWranDlgAlRisc() {
        return this.wranDlgAlRisc;
    }

    public void setWranLegaleRapprPrinc(char wranLegaleRapprPrinc) {
        this.wranLegaleRapprPrinc = wranLegaleRapprPrinc;
    }

    public char getWranLegaleRapprPrinc() {
        return this.wranLegaleRapprPrinc;
    }

    public void setWranTpLegaleRappr(String wranTpLegaleRappr) {
        this.wranTpLegaleRappr = Functions.subString(wranTpLegaleRappr, Len.WRAN_TP_LEGALE_RAPPR);
    }

    public String getWranTpLegaleRappr() {
        return this.wranTpLegaleRappr;
    }

    public void setWranTpIndPrinc(String wranTpIndPrinc) {
        this.wranTpIndPrinc = Functions.subString(wranTpIndPrinc, Len.WRAN_TP_IND_PRINC);
    }

    public String getWranTpIndPrinc() {
        return this.wranTpIndPrinc;
    }

    public void setWranTpStatRid(String wranTpStatRid) {
        this.wranTpStatRid = Functions.subString(wranTpStatRid, Len.WRAN_TP_STAT_RID);
    }

    public String getWranTpStatRid() {
        return this.wranTpStatRid;
    }

    public void setWranNomeIntRid(String wranNomeIntRid) {
        this.wranNomeIntRid = Functions.subString(wranNomeIntRid, Len.WRAN_NOME_INT_RID);
    }

    public String getWranNomeIntRid() {
        return this.wranNomeIntRid;
    }

    public void setWranCognIntRid(String wranCognIntRid) {
        this.wranCognIntRid = Functions.subString(wranCognIntRid, Len.WRAN_COGN_INT_RID);
    }

    public String getWranCognIntRid() {
        return this.wranCognIntRid;
    }

    public void setWranCognIntTratt(String wranCognIntTratt) {
        this.wranCognIntTratt = Functions.subString(wranCognIntTratt, Len.WRAN_COGN_INT_TRATT);
    }

    public String getWranCognIntTratt() {
        return this.wranCognIntTratt;
    }

    public void setWranNomeIntTratt(String wranNomeIntTratt) {
        this.wranNomeIntTratt = Functions.subString(wranNomeIntTratt, Len.WRAN_NOME_INT_TRATT);
    }

    public String getWranNomeIntTratt() {
        return this.wranNomeIntTratt;
    }

    public void setWranCfIntRid(String wranCfIntRid) {
        this.wranCfIntRid = Functions.subString(wranCfIntRid, Len.WRAN_CF_INT_RID);
    }

    public String getWranCfIntRid() {
        return this.wranCfIntRid;
    }

    public void setWranFlCoincDipCntr(char wranFlCoincDipCntr) {
        this.wranFlCoincDipCntr = wranFlCoincDipCntr;
    }

    public char getWranFlCoincDipCntr() {
        return this.wranFlCoincDipCntr;
    }

    public void setWranFlCoincIntCntr(char wranFlCoincIntCntr) {
        this.wranFlCoincIntCntr = wranFlCoincIntCntr;
    }

    public char getWranFlCoincIntCntr() {
        return this.wranFlCoincIntCntr;
    }

    public void setWranFlFumatore(char wranFlFumatore) {
        this.wranFlFumatore = wranFlFumatore;
    }

    public char getWranFlFumatore() {
        return this.wranFlFumatore;
    }

    public void setWranDsRiga(long wranDsRiga) {
        this.wranDsRiga = wranDsRiga;
    }

    public long getWranDsRiga() {
        return this.wranDsRiga;
    }

    public void setWranDsOperSql(char wranDsOperSql) {
        this.wranDsOperSql = wranDsOperSql;
    }

    public char getWranDsOperSql() {
        return this.wranDsOperSql;
    }

    public void setWranDsVer(int wranDsVer) {
        this.wranDsVer = wranDsVer;
    }

    public int getWranDsVer() {
        return this.wranDsVer;
    }

    public void setWranDsTsIniCptz(long wranDsTsIniCptz) {
        this.wranDsTsIniCptz = wranDsTsIniCptz;
    }

    public long getWranDsTsIniCptz() {
        return this.wranDsTsIniCptz;
    }

    public void setWranDsTsEndCptz(long wranDsTsEndCptz) {
        this.wranDsTsEndCptz = wranDsTsEndCptz;
    }

    public long getWranDsTsEndCptz() {
        return this.wranDsTsEndCptz;
    }

    public void setWranDsUtente(String wranDsUtente) {
        this.wranDsUtente = Functions.subString(wranDsUtente, Len.WRAN_DS_UTENTE);
    }

    public String getWranDsUtente() {
        return this.wranDsUtente;
    }

    public void setWranDsStatoElab(char wranDsStatoElab) {
        this.wranDsStatoElab = wranDsStatoElab;
    }

    public char getWranDsStatoElab() {
        return this.wranDsStatoElab;
    }

    public void setWranFlLavDip(char wranFlLavDip) {
        this.wranFlLavDip = wranFlLavDip;
    }

    public char getWranFlLavDip() {
        return this.wranFlLavDip;
    }

    public void setWranTpVarzPagat(String wranTpVarzPagat) {
        this.wranTpVarzPagat = Functions.subString(wranTpVarzPagat, Len.WRAN_TP_VARZ_PAGAT);
    }

    public String getWranTpVarzPagat() {
        return this.wranTpVarzPagat;
    }

    public void setWranCodRid(String wranCodRid) {
        this.wranCodRid = Functions.subString(wranCodRid, Len.WRAN_COD_RID);
    }

    public String getWranCodRid() {
        return this.wranCodRid;
    }

    public void setWranTpCausRid(String wranTpCausRid) {
        this.wranTpCausRid = Functions.subString(wranTpCausRid, Len.WRAN_TP_CAUS_RID);
    }

    public String getWranTpCausRid() {
        return this.wranTpCausRid;
    }

    public void setWranIndMassaCorp(String wranIndMassaCorp) {
        this.wranIndMassaCorp = Functions.subString(wranIndMassaCorp, Len.WRAN_IND_MASSA_CORP);
    }

    public String getWranIndMassaCorp() {
        return this.wranIndMassaCorp;
    }

    public void setWranCatRshProf(String wranCatRshProf) {
        this.wranCatRshProf = Functions.subString(wranCatRshProf, Len.WRAN_CAT_RSH_PROF);
    }

    public String getWranCatRshProf() {
        return this.wranCatRshProf;
    }

    public WranDtDeces getWranDtDeces() {
        return wranDtDeces;
    }

    public WranDtDeliberaCda getWranDtDeliberaCda() {
        return wranDtDeliberaCda;
    }

    public WranDtNasc getWranDtNasc() {
        return wranDtNasc;
    }

    public WranIdMoviChiu getWranIdMoviChiu() {
        return wranIdMoviChiu;
    }

    public WranIdRappAnaCollg getWranIdRappAnaCollg() {
        return wranIdRappAnaCollg;
    }

    public WranPcNelRapp getWranPcNelRapp() {
        return wranPcNelRapp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRAN_ID_RAPP_ANA = 5;
        public static final int WRAN_ID_OGG = 5;
        public static final int WRAN_TP_OGG = 2;
        public static final int WRAN_ID_MOVI_CRZ = 5;
        public static final int WRAN_DT_INI_EFF = 5;
        public static final int WRAN_DT_END_EFF = 5;
        public static final int WRAN_COD_COMP_ANIA = 3;
        public static final int WRAN_COD_SOGG = 20;
        public static final int WRAN_TP_RAPP_ANA = 2;
        public static final int WRAN_TP_PERS = 1;
        public static final int WRAN_SEX = 1;
        public static final int WRAN_FL_ESTAS = 1;
        public static final int WRAN_INDIR1 = 20;
        public static final int WRAN_INDIR2 = 20;
        public static final int WRAN_INDIR3 = 20;
        public static final int WRAN_TP_UTLZ_INDIR1 = 2;
        public static final int WRAN_TP_UTLZ_INDIR2 = 2;
        public static final int WRAN_TP_UTLZ_INDIR3 = 2;
        public static final int WRAN_ESTR_CNT_CORR_ACCR = 20;
        public static final int WRAN_ESTR_CNT_CORR_ADD = 20;
        public static final int WRAN_ESTR_DOCTO = 20;
        public static final int WRAN_TP_MEZ_PAG_ADD = 2;
        public static final int WRAN_TP_MEZ_PAG_ACCR = 2;
        public static final int WRAN_COD_MATR = 20;
        public static final int WRAN_TP_ADEGZ = 2;
        public static final int WRAN_FL_TST_RSH = 1;
        public static final int WRAN_COD_AZ = 30;
        public static final int WRAN_IND_PRINC = 2;
        public static final int WRAN_DLG_AL_RISC = 1;
        public static final int WRAN_LEGALE_RAPPR_PRINC = 1;
        public static final int WRAN_TP_LEGALE_RAPPR = 2;
        public static final int WRAN_TP_IND_PRINC = 2;
        public static final int WRAN_TP_STAT_RID = 2;
        public static final int WRAN_NOME_INT_RID = 100;
        public static final int WRAN_COGN_INT_RID = 100;
        public static final int WRAN_COGN_INT_TRATT = 100;
        public static final int WRAN_NOME_INT_TRATT = 100;
        public static final int WRAN_CF_INT_RID = 16;
        public static final int WRAN_FL_COINC_DIP_CNTR = 1;
        public static final int WRAN_FL_COINC_INT_CNTR = 1;
        public static final int WRAN_FL_FUMATORE = 1;
        public static final int WRAN_DS_RIGA = 6;
        public static final int WRAN_DS_OPER_SQL = 1;
        public static final int WRAN_DS_VER = 5;
        public static final int WRAN_DS_TS_INI_CPTZ = 10;
        public static final int WRAN_DS_TS_END_CPTZ = 10;
        public static final int WRAN_DS_UTENTE = 20;
        public static final int WRAN_DS_STATO_ELAB = 1;
        public static final int WRAN_FL_LAV_DIP = 1;
        public static final int WRAN_TP_VARZ_PAGAT = 2;
        public static final int WRAN_COD_RID = 11;
        public static final int WRAN_TP_CAUS_RID = 2;
        public static final int WRAN_IND_MASSA_CORP = 2;
        public static final int WRAN_CAT_RSH_PROF = 2;
        public static final int DATI = WRAN_ID_RAPP_ANA + WranIdRappAnaCollg.Len.WRAN_ID_RAPP_ANA_COLLG + WRAN_ID_OGG + WRAN_TP_OGG + WRAN_ID_MOVI_CRZ + WranIdMoviChiu.Len.WRAN_ID_MOVI_CHIU + WRAN_DT_INI_EFF + WRAN_DT_END_EFF + WRAN_COD_COMP_ANIA + WRAN_COD_SOGG + WRAN_TP_RAPP_ANA + WRAN_TP_PERS + WRAN_SEX + WranDtNasc.Len.WRAN_DT_NASC + WRAN_FL_ESTAS + WRAN_INDIR1 + WRAN_INDIR2 + WRAN_INDIR3 + WRAN_TP_UTLZ_INDIR1 + WRAN_TP_UTLZ_INDIR2 + WRAN_TP_UTLZ_INDIR3 + WRAN_ESTR_CNT_CORR_ACCR + WRAN_ESTR_CNT_CORR_ADD + WRAN_ESTR_DOCTO + WranPcNelRapp.Len.WRAN_PC_NEL_RAPP + WRAN_TP_MEZ_PAG_ADD + WRAN_TP_MEZ_PAG_ACCR + WRAN_COD_MATR + WRAN_TP_ADEGZ + WRAN_FL_TST_RSH + WRAN_COD_AZ + WRAN_IND_PRINC + WranDtDeliberaCda.Len.WRAN_DT_DELIBERA_CDA + WRAN_DLG_AL_RISC + WRAN_LEGALE_RAPPR_PRINC + WRAN_TP_LEGALE_RAPPR + WRAN_TP_IND_PRINC + WRAN_TP_STAT_RID + WRAN_NOME_INT_RID + WRAN_COGN_INT_RID + WRAN_COGN_INT_TRATT + WRAN_NOME_INT_TRATT + WRAN_CF_INT_RID + WRAN_FL_COINC_DIP_CNTR + WRAN_FL_COINC_INT_CNTR + WranDtDeces.Len.WRAN_DT_DECES + WRAN_FL_FUMATORE + WRAN_DS_RIGA + WRAN_DS_OPER_SQL + WRAN_DS_VER + WRAN_DS_TS_INI_CPTZ + WRAN_DS_TS_END_CPTZ + WRAN_DS_UTENTE + WRAN_DS_STATO_ELAB + WRAN_FL_LAV_DIP + WRAN_TP_VARZ_PAGAT + WRAN_COD_RID + WRAN_TP_CAUS_RID + WRAN_IND_MASSA_CORP + WRAN_CAT_RSH_PROF;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRAN_ID_RAPP_ANA = 9;
            public static final int WRAN_ID_OGG = 9;
            public static final int WRAN_ID_MOVI_CRZ = 9;
            public static final int WRAN_DT_INI_EFF = 8;
            public static final int WRAN_DT_END_EFF = 8;
            public static final int WRAN_COD_COMP_ANIA = 5;
            public static final int WRAN_DS_RIGA = 10;
            public static final int WRAN_DS_VER = 9;
            public static final int WRAN_DS_TS_INI_CPTZ = 18;
            public static final int WRAN_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
