package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV5141-TP-TRCH<br>
 * Variable: LDBV5141-TP-TRCH from copybook LDBV5141<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbv5141TpTrch {

    //==== PROPERTIES ====
    //Original name: LDBV5141-TP-TRCH-1
    private String trch1 = DefaultValues.stringVal(Len.TRCH1);
    //Original name: LDBV5141-TP-TRCH-2
    private String trch2 = DefaultValues.stringVal(Len.TRCH2);
    //Original name: LDBV5141-TP-TRCH-3
    private String trch3 = DefaultValues.stringVal(Len.TRCH3);
    //Original name: LDBV5141-TP-TRCH-4
    private String trch4 = DefaultValues.stringVal(Len.TRCH4);
    //Original name: LDBV5141-TP-TRCH-5
    private String trch5 = DefaultValues.stringVal(Len.TRCH5);
    //Original name: LDBV5141-TP-TRCH-6
    private String trch6 = DefaultValues.stringVal(Len.TRCH6);
    //Original name: LDBV5141-TP-TRCH-7
    private String trch7 = DefaultValues.stringVal(Len.TRCH7);
    //Original name: LDBV5141-TP-TRCH-8
    private String trch8 = DefaultValues.stringVal(Len.TRCH8);
    //Original name: LDBV5141-TP-TRCH-9
    private String trch9 = DefaultValues.stringVal(Len.TRCH9);
    //Original name: LDBV5141-TP-TRCH-10
    private String trch10 = DefaultValues.stringVal(Len.TRCH10);
    //Original name: LDBV5141-TP-TRCH-11
    private String trch11 = DefaultValues.stringVal(Len.TRCH11);
    //Original name: LDBV5141-TP-TRCH-12
    private String trch12 = DefaultValues.stringVal(Len.TRCH12);
    //Original name: LDBV5141-TP-TRCH-13
    private String trch13 = DefaultValues.stringVal(Len.TRCH13);
    //Original name: LDBV5141-TP-TRCH-14
    private String trch14 = DefaultValues.stringVal(Len.TRCH14);
    //Original name: LDBV5141-TP-TRCH-15
    private String trch15 = DefaultValues.stringVal(Len.TRCH15);
    //Original name: LDBV5141-TP-TRCH-16
    private String trch16 = DefaultValues.stringVal(Len.TRCH16);

    //==== METHODS ====
    public void setTpTrchBytes(byte[] buffer) {
        setTpTrchBytes(buffer, 1);
    }

    public void setTpTrchBytes(byte[] buffer, int offset) {
        int position = offset;
        trch1 = MarshalByte.readString(buffer, position, Len.TRCH1);
        position += Len.TRCH1;
        trch2 = MarshalByte.readString(buffer, position, Len.TRCH2);
        position += Len.TRCH2;
        trch3 = MarshalByte.readString(buffer, position, Len.TRCH3);
        position += Len.TRCH3;
        trch4 = MarshalByte.readString(buffer, position, Len.TRCH4);
        position += Len.TRCH4;
        trch5 = MarshalByte.readString(buffer, position, Len.TRCH5);
        position += Len.TRCH5;
        trch6 = MarshalByte.readString(buffer, position, Len.TRCH6);
        position += Len.TRCH6;
        trch7 = MarshalByte.readString(buffer, position, Len.TRCH7);
        position += Len.TRCH7;
        trch8 = MarshalByte.readString(buffer, position, Len.TRCH8);
        position += Len.TRCH8;
        trch9 = MarshalByte.readString(buffer, position, Len.TRCH9);
        position += Len.TRCH9;
        trch10 = MarshalByte.readString(buffer, position, Len.TRCH10);
        position += Len.TRCH10;
        trch11 = MarshalByte.readString(buffer, position, Len.TRCH11);
        position += Len.TRCH11;
        trch12 = MarshalByte.readString(buffer, position, Len.TRCH12);
        position += Len.TRCH12;
        trch13 = MarshalByte.readString(buffer, position, Len.TRCH13);
        position += Len.TRCH13;
        trch14 = MarshalByte.readString(buffer, position, Len.TRCH14);
        position += Len.TRCH14;
        trch15 = MarshalByte.readString(buffer, position, Len.TRCH15);
        position += Len.TRCH15;
        trch16 = MarshalByte.readString(buffer, position, Len.TRCH16);
    }

    public byte[] getTpTrchBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, trch1, Len.TRCH1);
        position += Len.TRCH1;
        MarshalByte.writeString(buffer, position, trch2, Len.TRCH2);
        position += Len.TRCH2;
        MarshalByte.writeString(buffer, position, trch3, Len.TRCH3);
        position += Len.TRCH3;
        MarshalByte.writeString(buffer, position, trch4, Len.TRCH4);
        position += Len.TRCH4;
        MarshalByte.writeString(buffer, position, trch5, Len.TRCH5);
        position += Len.TRCH5;
        MarshalByte.writeString(buffer, position, trch6, Len.TRCH6);
        position += Len.TRCH6;
        MarshalByte.writeString(buffer, position, trch7, Len.TRCH7);
        position += Len.TRCH7;
        MarshalByte.writeString(buffer, position, trch8, Len.TRCH8);
        position += Len.TRCH8;
        MarshalByte.writeString(buffer, position, trch9, Len.TRCH9);
        position += Len.TRCH9;
        MarshalByte.writeString(buffer, position, trch10, Len.TRCH10);
        position += Len.TRCH10;
        MarshalByte.writeString(buffer, position, trch11, Len.TRCH11);
        position += Len.TRCH11;
        MarshalByte.writeString(buffer, position, trch12, Len.TRCH12);
        position += Len.TRCH12;
        MarshalByte.writeString(buffer, position, trch13, Len.TRCH13);
        position += Len.TRCH13;
        MarshalByte.writeString(buffer, position, trch14, Len.TRCH14);
        position += Len.TRCH14;
        MarshalByte.writeString(buffer, position, trch15, Len.TRCH15);
        position += Len.TRCH15;
        MarshalByte.writeString(buffer, position, trch16, Len.TRCH16);
        return buffer;
    }

    public void setTrch1(String trch1) {
        this.trch1 = Functions.subString(trch1, Len.TRCH1);
    }

    public String getTrch1() {
        return this.trch1;
    }

    public void setTrch2(String trch2) {
        this.trch2 = Functions.subString(trch2, Len.TRCH2);
    }

    public String getTrch2() {
        return this.trch2;
    }

    public void setTrch3(String trch3) {
        this.trch3 = Functions.subString(trch3, Len.TRCH3);
    }

    public String getTrch3() {
        return this.trch3;
    }

    public void setTrch4(String trch4) {
        this.trch4 = Functions.subString(trch4, Len.TRCH4);
    }

    public String getTrch4() {
        return this.trch4;
    }

    public void setTrch5(String trch5) {
        this.trch5 = Functions.subString(trch5, Len.TRCH5);
    }

    public String getTrch5() {
        return this.trch5;
    }

    public void setTrch6(String trch6) {
        this.trch6 = Functions.subString(trch6, Len.TRCH6);
    }

    public String getTrch6() {
        return this.trch6;
    }

    public void setTrch7(String trch7) {
        this.trch7 = Functions.subString(trch7, Len.TRCH7);
    }

    public String getTrch7() {
        return this.trch7;
    }

    public void setTrch8(String trch8) {
        this.trch8 = Functions.subString(trch8, Len.TRCH8);
    }

    public String getTrch8() {
        return this.trch8;
    }

    public void setTrch9(String trch9) {
        this.trch9 = Functions.subString(trch9, Len.TRCH9);
    }

    public String getTrch9() {
        return this.trch9;
    }

    public void setTrch10(String trch10) {
        this.trch10 = Functions.subString(trch10, Len.TRCH10);
    }

    public String getTrch10() {
        return this.trch10;
    }

    public void setTrch11(String trch11) {
        this.trch11 = Functions.subString(trch11, Len.TRCH11);
    }

    public String getTrch11() {
        return this.trch11;
    }

    public void setTrch12(String trch12) {
        this.trch12 = Functions.subString(trch12, Len.TRCH12);
    }

    public String getTrch12() {
        return this.trch12;
    }

    public void setTrch13(String trch13) {
        this.trch13 = Functions.subString(trch13, Len.TRCH13);
    }

    public String getTrch13() {
        return this.trch13;
    }

    public void setTrch14(String trch14) {
        this.trch14 = Functions.subString(trch14, Len.TRCH14);
    }

    public String getTrch14() {
        return this.trch14;
    }

    public void setTrch15(String trch15) {
        this.trch15 = Functions.subString(trch15, Len.TRCH15);
    }

    public String getTrch15() {
        return this.trch15;
    }

    public void setTrch16(String trch16) {
        this.trch16 = Functions.subString(trch16, Len.TRCH16);
    }

    public String getTrch16() {
        return this.trch16;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TRCH1 = 2;
        public static final int TRCH2 = 2;
        public static final int TRCH3 = 2;
        public static final int TRCH4 = 2;
        public static final int TRCH5 = 2;
        public static final int TRCH6 = 2;
        public static final int TRCH7 = 2;
        public static final int TRCH8 = 2;
        public static final int TRCH9 = 2;
        public static final int TRCH10 = 2;
        public static final int TRCH11 = 2;
        public static final int TRCH12 = 2;
        public static final int TRCH13 = 2;
        public static final int TRCH14 = 2;
        public static final int TRCH15 = 2;
        public static final int TRCH16 = 2;
        public static final int TP_TRCH = TRCH1 + TRCH2 + TRCH3 + TRCH4 + TRCH5 + TRCH6 + TRCH7 + TRCH8 + TRCH9 + TRCH10 + TRCH11 + TRCH12 + TRCH13 + TRCH14 + TRCH15 + TRCH16;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
