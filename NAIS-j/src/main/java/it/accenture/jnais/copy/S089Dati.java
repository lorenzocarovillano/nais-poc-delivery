package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.S089AddizComun;
import it.accenture.jnais.ws.redefines.S089AddizRegion;
import it.accenture.jnais.ws.redefines.S089BnsNonGoduto;
import it.accenture.jnais.ws.redefines.S089CnbtInpstfm;
import it.accenture.jnais.ws.redefines.S089ComponTaxRimb;
import it.accenture.jnais.ws.redefines.S089CosTunnelUscita;
import it.accenture.jnais.ws.redefines.S089DtDen;
import it.accenture.jnais.ws.redefines.S089DtEndIstr;
import it.accenture.jnais.ws.redefines.S089DtLiq;
import it.accenture.jnais.ws.redefines.S089DtMor;
import it.accenture.jnais.ws.redefines.S089DtPervDen;
import it.accenture.jnais.ws.redefines.S089DtRich;
import it.accenture.jnais.ws.redefines.S089DtVlt;
import it.accenture.jnais.ws.redefines.S089IdMoviChiu;
import it.accenture.jnais.ws.redefines.S089ImpbAddizComun;
import it.accenture.jnais.ws.redefines.S089ImpbAddizRegion;
import it.accenture.jnais.ws.redefines.S089ImpbBolloDettC;
import it.accenture.jnais.ws.redefines.S089ImpbCnbtInpstfm;
import it.accenture.jnais.ws.redefines.S089ImpbImpst252;
import it.accenture.jnais.ws.redefines.S089ImpbImpstPrvr;
import it.accenture.jnais.ws.redefines.S089ImpbIntrSuPrest;
import it.accenture.jnais.ws.redefines.S089ImpbIrpef;
import it.accenture.jnais.ws.redefines.S089ImpbIs;
import it.accenture.jnais.ws.redefines.S089ImpbIs1382011;
import it.accenture.jnais.ws.redefines.S089ImpbIs662014;
import it.accenture.jnais.ws.redefines.S089ImpbTaxSep;
import it.accenture.jnais.ws.redefines.S089ImpbVis1382011;
import it.accenture.jnais.ws.redefines.S089ImpbVis662014;
import it.accenture.jnais.ws.redefines.S089ImpDirDaRimb;
import it.accenture.jnais.ws.redefines.S089ImpDirLiq;
import it.accenture.jnais.ws.redefines.S089ImpExcontr;
import it.accenture.jnais.ws.redefines.S089ImpIntrRitPag;
import it.accenture.jnais.ws.redefines.S089ImpLrdCalcCp;
import it.accenture.jnais.ws.redefines.S089ImpLrdDaRimb;
import it.accenture.jnais.ws.redefines.S089ImpLrdLiqtoRilt;
import it.accenture.jnais.ws.redefines.S089ImpOnerLiq;
import it.accenture.jnais.ws.redefines.S089ImpPnl;
import it.accenture.jnais.ws.redefines.S089ImpRenK1;
import it.accenture.jnais.ws.redefines.S089ImpRenK2;
import it.accenture.jnais.ws.redefines.S089ImpRenK3;
import it.accenture.jnais.ws.redefines.S089Impst252;
import it.accenture.jnais.ws.redefines.S089ImpstApplRilt;
import it.accenture.jnais.ws.redefines.S089ImpstBolloDettC;
import it.accenture.jnais.ws.redefines.S089ImpstBolloTotAa;
import it.accenture.jnais.ws.redefines.S089ImpstBolloTotSw;
import it.accenture.jnais.ws.redefines.S089ImpstBolloTotV;
import it.accenture.jnais.ws.redefines.S089ImpstDaRimb;
import it.accenture.jnais.ws.redefines.S089ImpstIrpef;
import it.accenture.jnais.ws.redefines.S089ImpstPrvr;
import it.accenture.jnais.ws.redefines.S089ImpstSost1382011;
import it.accenture.jnais.ws.redefines.S089ImpstSost662014;
import it.accenture.jnais.ws.redefines.S089ImpstVis1382011;
import it.accenture.jnais.ws.redefines.S089ImpstVis662014;
import it.accenture.jnais.ws.redefines.S089MontDal2007;
import it.accenture.jnais.ws.redefines.S089MontEnd2000;
import it.accenture.jnais.ws.redefines.S089MontEnd2006;
import it.accenture.jnais.ws.redefines.S089PcAbbTitStat;
import it.accenture.jnais.ws.redefines.S089PcAbbTs662014;
import it.accenture.jnais.ws.redefines.S089PcRen;
import it.accenture.jnais.ws.redefines.S089PcRenK1;
import it.accenture.jnais.ws.redefines.S089PcRenK2;
import it.accenture.jnais.ws.redefines.S089PcRenK3;
import it.accenture.jnais.ws.redefines.S089PcRiscParz;
import it.accenture.jnais.ws.redefines.S089RisMat;
import it.accenture.jnais.ws.redefines.S089RisSpe;
import it.accenture.jnais.ws.redefines.S089SpeRcs;
import it.accenture.jnais.ws.redefines.S089TaxSep;
import it.accenture.jnais.ws.redefines.S089TotIasMggSin;
import it.accenture.jnais.ws.redefines.S089TotIasOnerPrvnt;
import it.accenture.jnais.ws.redefines.S089TotIasPnl;
import it.accenture.jnais.ws.redefines.S089TotIasRstDpst;
import it.accenture.jnais.ws.redefines.S089TotImpbAcc;
import it.accenture.jnais.ws.redefines.S089TotImpbTfr;
import it.accenture.jnais.ws.redefines.S089TotImpbVis;
import it.accenture.jnais.ws.redefines.S089TotImpIntrPrest;
import it.accenture.jnais.ws.redefines.S089TotImpIs;
import it.accenture.jnais.ws.redefines.S089TotImpLrdLiqto;
import it.accenture.jnais.ws.redefines.S089TotImpNetLiqto;
import it.accenture.jnais.ws.redefines.S089TotImpPrest;
import it.accenture.jnais.ws.redefines.S089TotImpRimb;
import it.accenture.jnais.ws.redefines.S089TotImpRitAcc;
import it.accenture.jnais.ws.redefines.S089TotImpRitTfr;
import it.accenture.jnais.ws.redefines.S089TotImpRitVis;
import it.accenture.jnais.ws.redefines.S089TotImpUti;
import it.accenture.jnais.ws.redefines.S089TpMetRisc;

/**Original name: S089-DATI<br>
 * Variable: S089-DATI from copybook LCCVLQU1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class S089Dati {

    //==== PROPERTIES ====
    //Original name: S089-ID-LIQ
    private int s089IdLiq = DefaultValues.INT_VAL;
    //Original name: S089-ID-OGG
    private int s089IdOgg = DefaultValues.INT_VAL;
    //Original name: S089-TP-OGG
    private String s089TpOgg = DefaultValues.stringVal(Len.S089_TP_OGG);
    //Original name: S089-ID-MOVI-CRZ
    private int s089IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: S089-ID-MOVI-CHIU
    private S089IdMoviChiu s089IdMoviChiu = new S089IdMoviChiu();
    //Original name: S089-DT-INI-EFF
    private int s089DtIniEff = DefaultValues.INT_VAL;
    //Original name: S089-DT-END-EFF
    private int s089DtEndEff = DefaultValues.INT_VAL;
    //Original name: S089-COD-COMP-ANIA
    private int s089CodCompAnia = DefaultValues.INT_VAL;
    //Original name: S089-IB-OGG
    private String s089IbOgg = DefaultValues.stringVal(Len.S089_IB_OGG);
    //Original name: S089-TP-LIQ
    private String s089TpLiq = DefaultValues.stringVal(Len.S089_TP_LIQ);
    //Original name: S089-DESC-CAU-EVE-SIN
    private String s089DescCauEveSin = DefaultValues.stringVal(Len.S089_DESC_CAU_EVE_SIN);
    //Original name: S089-COD-CAU-SIN
    private String s089CodCauSin = DefaultValues.stringVal(Len.S089_COD_CAU_SIN);
    //Original name: S089-COD-SIN-CATSTRF
    private String s089CodSinCatstrf = DefaultValues.stringVal(Len.S089_COD_SIN_CATSTRF);
    //Original name: S089-DT-MOR
    private S089DtMor s089DtMor = new S089DtMor();
    //Original name: S089-DT-DEN
    private S089DtDen s089DtDen = new S089DtDen();
    //Original name: S089-DT-PERV-DEN
    private S089DtPervDen s089DtPervDen = new S089DtPervDen();
    //Original name: S089-DT-RICH
    private S089DtRich s089DtRich = new S089DtRich();
    //Original name: S089-TP-SIN
    private String s089TpSin = DefaultValues.stringVal(Len.S089_TP_SIN);
    //Original name: S089-TP-RISC
    private String s089TpRisc = DefaultValues.stringVal(Len.S089_TP_RISC);
    //Original name: S089-TP-MET-RISC
    private S089TpMetRisc s089TpMetRisc = new S089TpMetRisc();
    //Original name: S089-DT-LIQ
    private S089DtLiq s089DtLiq = new S089DtLiq();
    //Original name: S089-COD-DVS
    private String s089CodDvs = DefaultValues.stringVal(Len.S089_COD_DVS);
    //Original name: S089-TOT-IMP-LRD-LIQTO
    private S089TotImpLrdLiqto s089TotImpLrdLiqto = new S089TotImpLrdLiqto();
    //Original name: S089-TOT-IMP-PREST
    private S089TotImpPrest s089TotImpPrest = new S089TotImpPrest();
    //Original name: S089-TOT-IMP-INTR-PREST
    private S089TotImpIntrPrest s089TotImpIntrPrest = new S089TotImpIntrPrest();
    //Original name: S089-TOT-IMP-UTI
    private S089TotImpUti s089TotImpUti = new S089TotImpUti();
    //Original name: S089-TOT-IMP-RIT-TFR
    private S089TotImpRitTfr s089TotImpRitTfr = new S089TotImpRitTfr();
    //Original name: S089-TOT-IMP-RIT-ACC
    private S089TotImpRitAcc s089TotImpRitAcc = new S089TotImpRitAcc();
    //Original name: S089-TOT-IMP-RIT-VIS
    private S089TotImpRitVis s089TotImpRitVis = new S089TotImpRitVis();
    //Original name: S089-TOT-IMPB-TFR
    private S089TotImpbTfr s089TotImpbTfr = new S089TotImpbTfr();
    //Original name: S089-TOT-IMPB-ACC
    private S089TotImpbAcc s089TotImpbAcc = new S089TotImpbAcc();
    //Original name: S089-TOT-IMPB-VIS
    private S089TotImpbVis s089TotImpbVis = new S089TotImpbVis();
    //Original name: S089-TOT-IMP-RIMB
    private S089TotImpRimb s089TotImpRimb = new S089TotImpRimb();
    //Original name: S089-IMPB-IMPST-PRVR
    private S089ImpbImpstPrvr s089ImpbImpstPrvr = new S089ImpbImpstPrvr();
    //Original name: S089-IMPST-PRVR
    private S089ImpstPrvr s089ImpstPrvr = new S089ImpstPrvr();
    //Original name: S089-IMPB-IMPST-252
    private S089ImpbImpst252 s089ImpbImpst252 = new S089ImpbImpst252();
    //Original name: S089-IMPST-252
    private S089Impst252 s089Impst252 = new S089Impst252();
    //Original name: S089-TOT-IMP-IS
    private S089TotImpIs s089TotImpIs = new S089TotImpIs();
    //Original name: S089-IMP-DIR-LIQ
    private S089ImpDirLiq s089ImpDirLiq = new S089ImpDirLiq();
    //Original name: S089-TOT-IMP-NET-LIQTO
    private S089TotImpNetLiqto s089TotImpNetLiqto = new S089TotImpNetLiqto();
    //Original name: S089-MONT-END2000
    private S089MontEnd2000 s089MontEnd2000 = new S089MontEnd2000();
    //Original name: S089-MONT-END2006
    private S089MontEnd2006 s089MontEnd2006 = new S089MontEnd2006();
    //Original name: S089-PC-REN
    private S089PcRen s089PcRen = new S089PcRen();
    //Original name: S089-IMP-PNL
    private S089ImpPnl s089ImpPnl = new S089ImpPnl();
    //Original name: S089-IMPB-IRPEF
    private S089ImpbIrpef s089ImpbIrpef = new S089ImpbIrpef();
    //Original name: S089-IMPST-IRPEF
    private S089ImpstIrpef s089ImpstIrpef = new S089ImpstIrpef();
    //Original name: S089-DT-VLT
    private S089DtVlt s089DtVlt = new S089DtVlt();
    //Original name: S089-DT-END-ISTR
    private S089DtEndIstr s089DtEndIstr = new S089DtEndIstr();
    //Original name: S089-TP-RIMB
    private String s089TpRimb = DefaultValues.stringVal(Len.S089_TP_RIMB);
    //Original name: S089-SPE-RCS
    private S089SpeRcs s089SpeRcs = new S089SpeRcs();
    //Original name: S089-IB-LIQ
    private String s089IbLiq = DefaultValues.stringVal(Len.S089_IB_LIQ);
    //Original name: S089-TOT-IAS-ONER-PRVNT
    private S089TotIasOnerPrvnt s089TotIasOnerPrvnt = new S089TotIasOnerPrvnt();
    //Original name: S089-TOT-IAS-MGG-SIN
    private S089TotIasMggSin s089TotIasMggSin = new S089TotIasMggSin();
    //Original name: S089-TOT-IAS-RST-DPST
    private S089TotIasRstDpst s089TotIasRstDpst = new S089TotIasRstDpst();
    //Original name: S089-IMP-ONER-LIQ
    private S089ImpOnerLiq s089ImpOnerLiq = new S089ImpOnerLiq();
    //Original name: S089-COMPON-TAX-RIMB
    private S089ComponTaxRimb s089ComponTaxRimb = new S089ComponTaxRimb();
    //Original name: S089-TP-MEZ-PAG
    private String s089TpMezPag = DefaultValues.stringVal(Len.S089_TP_MEZ_PAG);
    //Original name: S089-IMP-EXCONTR
    private S089ImpExcontr s089ImpExcontr = new S089ImpExcontr();
    //Original name: S089-IMP-INTR-RIT-PAG
    private S089ImpIntrRitPag s089ImpIntrRitPag = new S089ImpIntrRitPag();
    //Original name: S089-BNS-NON-GODUTO
    private S089BnsNonGoduto s089BnsNonGoduto = new S089BnsNonGoduto();
    //Original name: S089-CNBT-INPSTFM
    private S089CnbtInpstfm s089CnbtInpstfm = new S089CnbtInpstfm();
    //Original name: S089-IMPST-DA-RIMB
    private S089ImpstDaRimb s089ImpstDaRimb = new S089ImpstDaRimb();
    //Original name: S089-IMPB-IS
    private S089ImpbIs s089ImpbIs = new S089ImpbIs();
    //Original name: S089-TAX-SEP
    private S089TaxSep s089TaxSep = new S089TaxSep();
    //Original name: S089-IMPB-TAX-SEP
    private S089ImpbTaxSep s089ImpbTaxSep = new S089ImpbTaxSep();
    //Original name: S089-IMPB-INTR-SU-PREST
    private S089ImpbIntrSuPrest s089ImpbIntrSuPrest = new S089ImpbIntrSuPrest();
    //Original name: S089-ADDIZ-COMUN
    private S089AddizComun s089AddizComun = new S089AddizComun();
    //Original name: S089-IMPB-ADDIZ-COMUN
    private S089ImpbAddizComun s089ImpbAddizComun = new S089ImpbAddizComun();
    //Original name: S089-ADDIZ-REGION
    private S089AddizRegion s089AddizRegion = new S089AddizRegion();
    //Original name: S089-IMPB-ADDIZ-REGION
    private S089ImpbAddizRegion s089ImpbAddizRegion = new S089ImpbAddizRegion();
    //Original name: S089-MONT-DAL2007
    private S089MontDal2007 s089MontDal2007 = new S089MontDal2007();
    //Original name: S089-IMPB-CNBT-INPSTFM
    private S089ImpbCnbtInpstfm s089ImpbCnbtInpstfm = new S089ImpbCnbtInpstfm();
    //Original name: S089-IMP-LRD-DA-RIMB
    private S089ImpLrdDaRimb s089ImpLrdDaRimb = new S089ImpLrdDaRimb();
    //Original name: S089-IMP-DIR-DA-RIMB
    private S089ImpDirDaRimb s089ImpDirDaRimb = new S089ImpDirDaRimb();
    //Original name: S089-RIS-MAT
    private S089RisMat s089RisMat = new S089RisMat();
    //Original name: S089-RIS-SPE
    private S089RisSpe s089RisSpe = new S089RisSpe();
    //Original name: S089-DS-RIGA
    private long s089DsRiga = DefaultValues.LONG_VAL;
    //Original name: S089-DS-OPER-SQL
    private char s089DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: S089-DS-VER
    private int s089DsVer = DefaultValues.INT_VAL;
    //Original name: S089-DS-TS-INI-CPTZ
    private long s089DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: S089-DS-TS-END-CPTZ
    private long s089DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: S089-DS-UTENTE
    private String s089DsUtente = DefaultValues.stringVal(Len.S089_DS_UTENTE);
    //Original name: S089-DS-STATO-ELAB
    private char s089DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: S089-TOT-IAS-PNL
    private S089TotIasPnl s089TotIasPnl = new S089TotIasPnl();
    //Original name: S089-FL-EVE-GARTO
    private char s089FlEveGarto = DefaultValues.CHAR_VAL;
    //Original name: S089-IMP-REN-K1
    private S089ImpRenK1 s089ImpRenK1 = new S089ImpRenK1();
    //Original name: S089-IMP-REN-K2
    private S089ImpRenK2 s089ImpRenK2 = new S089ImpRenK2();
    //Original name: S089-IMP-REN-K3
    private S089ImpRenK3 s089ImpRenK3 = new S089ImpRenK3();
    //Original name: S089-PC-REN-K1
    private S089PcRenK1 s089PcRenK1 = new S089PcRenK1();
    //Original name: S089-PC-REN-K2
    private S089PcRenK2 s089PcRenK2 = new S089PcRenK2();
    //Original name: S089-PC-REN-K3
    private S089PcRenK3 s089PcRenK3 = new S089PcRenK3();
    //Original name: S089-TP-CAUS-ANTIC
    private String s089TpCausAntic = DefaultValues.stringVal(Len.S089_TP_CAUS_ANTIC);
    //Original name: S089-IMP-LRD-LIQTO-RILT
    private S089ImpLrdLiqtoRilt s089ImpLrdLiqtoRilt = new S089ImpLrdLiqtoRilt();
    //Original name: S089-IMPST-APPL-RILT
    private S089ImpstApplRilt s089ImpstApplRilt = new S089ImpstApplRilt();
    //Original name: S089-PC-RISC-PARZ
    private S089PcRiscParz s089PcRiscParz = new S089PcRiscParz();
    //Original name: S089-IMPST-BOLLO-TOT-V
    private S089ImpstBolloTotV s089ImpstBolloTotV = new S089ImpstBolloTotV();
    //Original name: S089-IMPST-BOLLO-DETT-C
    private S089ImpstBolloDettC s089ImpstBolloDettC = new S089ImpstBolloDettC();
    //Original name: S089-IMPST-BOLLO-TOT-SW
    private S089ImpstBolloTotSw s089ImpstBolloTotSw = new S089ImpstBolloTotSw();
    //Original name: S089-IMPST-BOLLO-TOT-AA
    private S089ImpstBolloTotAa s089ImpstBolloTotAa = new S089ImpstBolloTotAa();
    //Original name: S089-IMPB-VIS-1382011
    private S089ImpbVis1382011 s089ImpbVis1382011 = new S089ImpbVis1382011();
    //Original name: S089-IMPST-VIS-1382011
    private S089ImpstVis1382011 s089ImpstVis1382011 = new S089ImpstVis1382011();
    //Original name: S089-IMPB-IS-1382011
    private S089ImpbIs1382011 s089ImpbIs1382011 = new S089ImpbIs1382011();
    //Original name: S089-IMPST-SOST-1382011
    private S089ImpstSost1382011 s089ImpstSost1382011 = new S089ImpstSost1382011();
    //Original name: S089-PC-ABB-TIT-STAT
    private S089PcAbbTitStat s089PcAbbTitStat = new S089PcAbbTitStat();
    //Original name: S089-IMPB-BOLLO-DETT-C
    private S089ImpbBolloDettC s089ImpbBolloDettC = new S089ImpbBolloDettC();
    //Original name: S089-FL-PRE-COMP
    private char s089FlPreComp = DefaultValues.CHAR_VAL;
    //Original name: S089-IMPB-VIS-662014
    private S089ImpbVis662014 s089ImpbVis662014 = new S089ImpbVis662014();
    //Original name: S089-IMPST-VIS-662014
    private S089ImpstVis662014 s089ImpstVis662014 = new S089ImpstVis662014();
    //Original name: S089-IMPB-IS-662014
    private S089ImpbIs662014 s089ImpbIs662014 = new S089ImpbIs662014();
    //Original name: S089-IMPST-SOST-662014
    private S089ImpstSost662014 s089ImpstSost662014 = new S089ImpstSost662014();
    //Original name: S089-PC-ABB-TS-662014
    private S089PcAbbTs662014 s089PcAbbTs662014 = new S089PcAbbTs662014();
    //Original name: S089-IMP-LRD-CALC-CP
    private S089ImpLrdCalcCp s089ImpLrdCalcCp = new S089ImpLrdCalcCp();
    //Original name: S089-COS-TUNNEL-USCITA
    private S089CosTunnelUscita s089CosTunnelUscita = new S089CosTunnelUscita();

    //==== METHODS ====
    public String getWlquDatiFormatted() {
        return MarshalByteExt.bufferToStr(getWlquDatiBytes());
    }

    public byte[] getWlquDatiBytes() {
        byte[] buffer = new byte[Len.WLQU_DATI];
        return getWlquDatiBytes(buffer, 1);
    }

    public void setWlquDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        s089IdLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WLQU_ID_LIQ, 0);
        position += Len.WLQU_ID_LIQ;
        s089IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WLQU_ID_OGG, 0);
        position += Len.WLQU_ID_OGG;
        s089TpOgg = MarshalByte.readString(buffer, position, Len.S089_TP_OGG);
        position += Len.S089_TP_OGG;
        s089IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WLQU_ID_MOVI_CRZ, 0);
        position += Len.WLQU_ID_MOVI_CRZ;
        s089IdMoviChiu.setWlquIdMoviChiuFromBuffer(buffer, position);
        position += S089IdMoviChiu.Len.S089_ID_MOVI_CHIU;
        s089DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WLQU_DT_INI_EFF, 0);
        position += Len.WLQU_DT_INI_EFF;
        s089DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WLQU_DT_END_EFF, 0);
        position += Len.WLQU_DT_END_EFF;
        s089CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WLQU_COD_COMP_ANIA, 0);
        position += Len.WLQU_COD_COMP_ANIA;
        s089IbOgg = MarshalByte.readString(buffer, position, Len.S089_IB_OGG);
        position += Len.S089_IB_OGG;
        s089TpLiq = MarshalByte.readString(buffer, position, Len.S089_TP_LIQ);
        position += Len.S089_TP_LIQ;
        s089DescCauEveSin = MarshalByte.readString(buffer, position, Len.S089_DESC_CAU_EVE_SIN);
        position += Len.S089_DESC_CAU_EVE_SIN;
        s089CodCauSin = MarshalByte.readString(buffer, position, Len.S089_COD_CAU_SIN);
        position += Len.S089_COD_CAU_SIN;
        s089CodSinCatstrf = MarshalByte.readString(buffer, position, Len.S089_COD_SIN_CATSTRF);
        position += Len.S089_COD_SIN_CATSTRF;
        s089DtMor.setWlquDtMorFromBuffer(buffer, position);
        position += S089DtMor.Len.S089_DT_MOR;
        s089DtDen.setWlquDtDenFromBuffer(buffer, position);
        position += S089DtDen.Len.S089_DT_DEN;
        s089DtPervDen.setWlquDtPervDenFromBuffer(buffer, position);
        position += S089DtPervDen.Len.S089_DT_PERV_DEN;
        s089DtRich.setWlquDtRichFromBuffer(buffer, position);
        position += S089DtRich.Len.S089_DT_RICH;
        s089TpSin = MarshalByte.readString(buffer, position, Len.S089_TP_SIN);
        position += Len.S089_TP_SIN;
        s089TpRisc = MarshalByte.readString(buffer, position, Len.S089_TP_RISC);
        position += Len.S089_TP_RISC;
        s089TpMetRisc.setWlquTpMetRiscFromBuffer(buffer, position);
        position += S089TpMetRisc.Len.S089_TP_MET_RISC;
        s089DtLiq.setWlquDtLiqFromBuffer(buffer, position);
        position += S089DtLiq.Len.S089_DT_LIQ;
        s089CodDvs = MarshalByte.readString(buffer, position, Len.S089_COD_DVS);
        position += Len.S089_COD_DVS;
        s089TotImpLrdLiqto.setWlquTotImpLrdLiqtoFromBuffer(buffer, position);
        position += S089TotImpLrdLiqto.Len.S089_TOT_IMP_LRD_LIQTO;
        s089TotImpPrest.setWlquTotImpPrestFromBuffer(buffer, position);
        position += S089TotImpPrest.Len.S089_TOT_IMP_PREST;
        s089TotImpIntrPrest.setWlquTotImpIntrPrestFromBuffer(buffer, position);
        position += S089TotImpIntrPrest.Len.S089_TOT_IMP_INTR_PREST;
        s089TotImpUti.setWlquTotImpUtiFromBuffer(buffer, position);
        position += S089TotImpUti.Len.S089_TOT_IMP_UTI;
        s089TotImpRitTfr.setWlquTotImpRitTfrFromBuffer(buffer, position);
        position += S089TotImpRitTfr.Len.S089_TOT_IMP_RIT_TFR;
        s089TotImpRitAcc.setWlquTotImpRitAccFromBuffer(buffer, position);
        position += S089TotImpRitAcc.Len.S089_TOT_IMP_RIT_ACC;
        s089TotImpRitVis.setWlquTotImpRitVisFromBuffer(buffer, position);
        position += S089TotImpRitVis.Len.S089_TOT_IMP_RIT_VIS;
        s089TotImpbTfr.setWlquTotImpbTfrFromBuffer(buffer, position);
        position += S089TotImpbTfr.Len.S089_TOT_IMPB_TFR;
        s089TotImpbAcc.setWlquTotImpbAccFromBuffer(buffer, position);
        position += S089TotImpbAcc.Len.S089_TOT_IMPB_ACC;
        s089TotImpbVis.setWlquTotImpbVisFromBuffer(buffer, position);
        position += S089TotImpbVis.Len.S089_TOT_IMPB_VIS;
        s089TotImpRimb.setWlquTotImpRimbFromBuffer(buffer, position);
        position += S089TotImpRimb.Len.S089_TOT_IMP_RIMB;
        s089ImpbImpstPrvr.setWlquImpbImpstPrvrFromBuffer(buffer, position);
        position += S089ImpbImpstPrvr.Len.S089_IMPB_IMPST_PRVR;
        s089ImpstPrvr.setWlquImpstPrvrFromBuffer(buffer, position);
        position += S089ImpstPrvr.Len.S089_IMPST_PRVR;
        s089ImpbImpst252.setWlquImpbImpst252FromBuffer(buffer, position);
        position += S089ImpbImpst252.Len.S089_IMPB_IMPST252;
        s089Impst252.setWlquImpst252FromBuffer(buffer, position);
        position += S089Impst252.Len.S089_IMPST252;
        s089TotImpIs.setWlquTotImpIsFromBuffer(buffer, position);
        position += S089TotImpIs.Len.S089_TOT_IMP_IS;
        s089ImpDirLiq.setWlquImpDirLiqFromBuffer(buffer, position);
        position += S089ImpDirLiq.Len.S089_IMP_DIR_LIQ;
        s089TotImpNetLiqto.setWlquTotImpNetLiqtoFromBuffer(buffer, position);
        position += S089TotImpNetLiqto.Len.S089_TOT_IMP_NET_LIQTO;
        s089MontEnd2000.setWlquMontEnd2000FromBuffer(buffer, position);
        position += S089MontEnd2000.Len.S089_MONT_END2000;
        s089MontEnd2006.setWlquMontEnd2006FromBuffer(buffer, position);
        position += S089MontEnd2006.Len.S089_MONT_END2006;
        s089PcRen.setWlquPcRenFromBuffer(buffer, position);
        position += S089PcRen.Len.S089_PC_REN;
        s089ImpPnl.setWlquImpPnlFromBuffer(buffer, position);
        position += S089ImpPnl.Len.S089_IMP_PNL;
        s089ImpbIrpef.setWlquImpbIrpefFromBuffer(buffer, position);
        position += S089ImpbIrpef.Len.S089_IMPB_IRPEF;
        s089ImpstIrpef.setWlquImpstIrpefFromBuffer(buffer, position);
        position += S089ImpstIrpef.Len.S089_IMPST_IRPEF;
        s089DtVlt.setWlquDtVltFromBuffer(buffer, position);
        position += S089DtVlt.Len.S089_DT_VLT;
        s089DtEndIstr.setWlquDtEndIstrFromBuffer(buffer, position);
        position += S089DtEndIstr.Len.S089_DT_END_ISTR;
        s089TpRimb = MarshalByte.readString(buffer, position, Len.S089_TP_RIMB);
        position += Len.S089_TP_RIMB;
        s089SpeRcs.setWlquSpeRcsFromBuffer(buffer, position);
        position += S089SpeRcs.Len.S089_SPE_RCS;
        s089IbLiq = MarshalByte.readString(buffer, position, Len.S089_IB_LIQ);
        position += Len.S089_IB_LIQ;
        s089TotIasOnerPrvnt.setWlquTotIasOnerPrvntFromBuffer(buffer, position);
        position += S089TotIasOnerPrvnt.Len.S089_TOT_IAS_ONER_PRVNT;
        s089TotIasMggSin.setWlquTotIasMggSinFromBuffer(buffer, position);
        position += S089TotIasMggSin.Len.S089_TOT_IAS_MGG_SIN;
        s089TotIasRstDpst.setWlquTotIasRstDpstFromBuffer(buffer, position);
        position += S089TotIasRstDpst.Len.S089_TOT_IAS_RST_DPST;
        s089ImpOnerLiq.setWlquImpOnerLiqFromBuffer(buffer, position);
        position += S089ImpOnerLiq.Len.S089_IMP_ONER_LIQ;
        s089ComponTaxRimb.setWlquComponTaxRimbFromBuffer(buffer, position);
        position += S089ComponTaxRimb.Len.S089_COMPON_TAX_RIMB;
        s089TpMezPag = MarshalByte.readString(buffer, position, Len.S089_TP_MEZ_PAG);
        position += Len.S089_TP_MEZ_PAG;
        s089ImpExcontr.setWlquImpExcontrFromBuffer(buffer, position);
        position += S089ImpExcontr.Len.S089_IMP_EXCONTR;
        s089ImpIntrRitPag.setWlquImpIntrRitPagFromBuffer(buffer, position);
        position += S089ImpIntrRitPag.Len.S089_IMP_INTR_RIT_PAG;
        s089BnsNonGoduto.setWlquBnsNonGodutoFromBuffer(buffer, position);
        position += S089BnsNonGoduto.Len.S089_BNS_NON_GODUTO;
        s089CnbtInpstfm.setWlquCnbtInpstfmFromBuffer(buffer, position);
        position += S089CnbtInpstfm.Len.S089_CNBT_INPSTFM;
        s089ImpstDaRimb.setWlquImpstDaRimbFromBuffer(buffer, position);
        position += S089ImpstDaRimb.Len.S089_IMPST_DA_RIMB;
        s089ImpbIs.setWlquImpbIsFromBuffer(buffer, position);
        position += S089ImpbIs.Len.S089_IMPB_IS;
        s089TaxSep.setWlquTaxSepFromBuffer(buffer, position);
        position += S089TaxSep.Len.S089_TAX_SEP;
        s089ImpbTaxSep.setWlquImpbTaxSepFromBuffer(buffer, position);
        position += S089ImpbTaxSep.Len.S089_IMPB_TAX_SEP;
        s089ImpbIntrSuPrest.setWlquImpbIntrSuPrestFromBuffer(buffer, position);
        position += S089ImpbIntrSuPrest.Len.S089_IMPB_INTR_SU_PREST;
        s089AddizComun.setWlquAddizComunFromBuffer(buffer, position);
        position += S089AddizComun.Len.S089_ADDIZ_COMUN;
        s089ImpbAddizComun.setWlquImpbAddizComunFromBuffer(buffer, position);
        position += S089ImpbAddizComun.Len.S089_IMPB_ADDIZ_COMUN;
        s089AddizRegion.setWlquAddizRegionFromBuffer(buffer, position);
        position += S089AddizRegion.Len.S089_ADDIZ_REGION;
        s089ImpbAddizRegion.setWlquImpbAddizRegionFromBuffer(buffer, position);
        position += S089ImpbAddizRegion.Len.S089_IMPB_ADDIZ_REGION;
        s089MontDal2007.setWlquMontDal2007FromBuffer(buffer, position);
        position += S089MontDal2007.Len.S089_MONT_DAL2007;
        s089ImpbCnbtInpstfm.setWlquImpbCnbtInpstfmFromBuffer(buffer, position);
        position += S089ImpbCnbtInpstfm.Len.S089_IMPB_CNBT_INPSTFM;
        s089ImpLrdDaRimb.setWlquImpLrdDaRimbFromBuffer(buffer, position);
        position += S089ImpLrdDaRimb.Len.S089_IMP_LRD_DA_RIMB;
        s089ImpDirDaRimb.setWlquImpDirDaRimbFromBuffer(buffer, position);
        position += S089ImpDirDaRimb.Len.S089_IMP_DIR_DA_RIMB;
        s089RisMat.setWlquRisMatFromBuffer(buffer, position);
        position += S089RisMat.Len.S089_RIS_MAT;
        s089RisSpe.setWlquRisSpeFromBuffer(buffer, position);
        position += S089RisSpe.Len.S089_RIS_SPE;
        s089DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WLQU_DS_RIGA, 0);
        position += Len.WLQU_DS_RIGA;
        s089DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        s089DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WLQU_DS_VER, 0);
        position += Len.WLQU_DS_VER;
        s089DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WLQU_DS_TS_INI_CPTZ, 0);
        position += Len.WLQU_DS_TS_INI_CPTZ;
        s089DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WLQU_DS_TS_END_CPTZ, 0);
        position += Len.WLQU_DS_TS_END_CPTZ;
        s089DsUtente = MarshalByte.readString(buffer, position, Len.S089_DS_UTENTE);
        position += Len.S089_DS_UTENTE;
        s089DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        s089TotIasPnl.setWlquTotIasPnlFromBuffer(buffer, position);
        position += S089TotIasPnl.Len.S089_TOT_IAS_PNL;
        s089FlEveGarto = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        s089ImpRenK1.setWlquImpRenK1FromBuffer(buffer, position);
        position += S089ImpRenK1.Len.S089_IMP_REN_K1;
        s089ImpRenK2.setWlquImpRenK2FromBuffer(buffer, position);
        position += S089ImpRenK2.Len.S089_IMP_REN_K2;
        s089ImpRenK3.setWlquImpRenK3FromBuffer(buffer, position);
        position += S089ImpRenK3.Len.S089_IMP_REN_K3;
        s089PcRenK1.setWlquPcRenK1FromBuffer(buffer, position);
        position += S089PcRenK1.Len.S089_PC_REN_K1;
        s089PcRenK2.setWlquPcRenK2FromBuffer(buffer, position);
        position += S089PcRenK2.Len.S089_PC_REN_K2;
        s089PcRenK3.setWlquPcRenK3FromBuffer(buffer, position);
        position += S089PcRenK3.Len.S089_PC_REN_K3;
        s089TpCausAntic = MarshalByte.readString(buffer, position, Len.S089_TP_CAUS_ANTIC);
        position += Len.S089_TP_CAUS_ANTIC;
        s089ImpLrdLiqtoRilt.setWlquImpLrdLiqtoRiltFromBuffer(buffer, position);
        position += S089ImpLrdLiqtoRilt.Len.S089_IMP_LRD_LIQTO_RILT;
        s089ImpstApplRilt.setWlquImpstApplRiltFromBuffer(buffer, position);
        position += S089ImpstApplRilt.Len.S089_IMPST_APPL_RILT;
        s089PcRiscParz.setWlquPcRiscParzFromBuffer(buffer, position);
        position += S089PcRiscParz.Len.S089_PC_RISC_PARZ;
        s089ImpstBolloTotV.setWlquImpstBolloTotVFromBuffer(buffer, position);
        position += S089ImpstBolloTotV.Len.S089_IMPST_BOLLO_TOT_V;
        s089ImpstBolloDettC.setWlquImpstBolloDettCFromBuffer(buffer, position);
        position += S089ImpstBolloDettC.Len.S089_IMPST_BOLLO_DETT_C;
        s089ImpstBolloTotSw.setWlquImpstBolloTotSwFromBuffer(buffer, position);
        position += S089ImpstBolloTotSw.Len.S089_IMPST_BOLLO_TOT_SW;
        s089ImpstBolloTotAa.setWlquImpstBolloTotAaFromBuffer(buffer, position);
        position += S089ImpstBolloTotAa.Len.S089_IMPST_BOLLO_TOT_AA;
        s089ImpbVis1382011.setWlquImpbVis1382011FromBuffer(buffer, position);
        position += S089ImpbVis1382011.Len.S089_IMPB_VIS1382011;
        s089ImpstVis1382011.setWlquImpstVis1382011FromBuffer(buffer, position);
        position += S089ImpstVis1382011.Len.S089_IMPST_VIS1382011;
        s089ImpbIs1382011.setWlquImpbIs1382011FromBuffer(buffer, position);
        position += S089ImpbIs1382011.Len.S089_IMPB_IS1382011;
        s089ImpstSost1382011.setWlquImpstSost1382011FromBuffer(buffer, position);
        position += S089ImpstSost1382011.Len.S089_IMPST_SOST1382011;
        s089PcAbbTitStat.setWlquPcAbbTitStatFromBuffer(buffer, position);
        position += S089PcAbbTitStat.Len.S089_PC_ABB_TIT_STAT;
        s089ImpbBolloDettC.setWlquImpbBolloDettCFromBuffer(buffer, position);
        position += S089ImpbBolloDettC.Len.S089_IMPB_BOLLO_DETT_C;
        s089FlPreComp = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        s089ImpbVis662014.setWlquImpbVis662014FromBuffer(buffer, position);
        position += S089ImpbVis662014.Len.S089_IMPB_VIS662014;
        s089ImpstVis662014.setWlquImpstVis662014FromBuffer(buffer, position);
        position += S089ImpstVis662014.Len.S089_IMPST_VIS662014;
        s089ImpbIs662014.setWlquImpbIs662014FromBuffer(buffer, position);
        position += S089ImpbIs662014.Len.S089_IMPB_IS662014;
        s089ImpstSost662014.setWlquImpstSost662014FromBuffer(buffer, position);
        position += S089ImpstSost662014.Len.S089_IMPST_SOST662014;
        s089PcAbbTs662014.setWlquPcAbbTs662014FromBuffer(buffer, position);
        position += S089PcAbbTs662014.Len.S089_PC_ABB_TS662014;
        s089ImpLrdCalcCp.setWlquImpLrdCalcCpFromBuffer(buffer, position);
        position += S089ImpLrdCalcCp.Len.S089_IMP_LRD_CALC_CP;
        s089CosTunnelUscita.setWlquCosTunnelUscitaFromBuffer(buffer, position);
    }

    public byte[] getWlquDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, s089IdLiq, Len.Int.WLQU_ID_LIQ, 0);
        position += Len.WLQU_ID_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, s089IdOgg, Len.Int.WLQU_ID_OGG, 0);
        position += Len.WLQU_ID_OGG;
        MarshalByte.writeString(buffer, position, s089TpOgg, Len.S089_TP_OGG);
        position += Len.S089_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, s089IdMoviCrz, Len.Int.WLQU_ID_MOVI_CRZ, 0);
        position += Len.WLQU_ID_MOVI_CRZ;
        s089IdMoviChiu.getWlquIdMoviChiuAsBuffer(buffer, position);
        position += S089IdMoviChiu.Len.S089_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, s089DtIniEff, Len.Int.WLQU_DT_INI_EFF, 0);
        position += Len.WLQU_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, s089DtEndEff, Len.Int.WLQU_DT_END_EFF, 0);
        position += Len.WLQU_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, s089CodCompAnia, Len.Int.WLQU_COD_COMP_ANIA, 0);
        position += Len.WLQU_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, s089IbOgg, Len.S089_IB_OGG);
        position += Len.S089_IB_OGG;
        MarshalByte.writeString(buffer, position, s089TpLiq, Len.S089_TP_LIQ);
        position += Len.S089_TP_LIQ;
        MarshalByte.writeString(buffer, position, s089DescCauEveSin, Len.S089_DESC_CAU_EVE_SIN);
        position += Len.S089_DESC_CAU_EVE_SIN;
        MarshalByte.writeString(buffer, position, s089CodCauSin, Len.S089_COD_CAU_SIN);
        position += Len.S089_COD_CAU_SIN;
        MarshalByte.writeString(buffer, position, s089CodSinCatstrf, Len.S089_COD_SIN_CATSTRF);
        position += Len.S089_COD_SIN_CATSTRF;
        s089DtMor.getWlquDtMorAsBuffer(buffer, position);
        position += S089DtMor.Len.S089_DT_MOR;
        s089DtDen.getWlquDtDenAsBuffer(buffer, position);
        position += S089DtDen.Len.S089_DT_DEN;
        s089DtPervDen.getWlquDtPervDenAsBuffer(buffer, position);
        position += S089DtPervDen.Len.S089_DT_PERV_DEN;
        s089DtRich.getWlquDtRichAsBuffer(buffer, position);
        position += S089DtRich.Len.S089_DT_RICH;
        MarshalByte.writeString(buffer, position, s089TpSin, Len.S089_TP_SIN);
        position += Len.S089_TP_SIN;
        MarshalByte.writeString(buffer, position, s089TpRisc, Len.S089_TP_RISC);
        position += Len.S089_TP_RISC;
        s089TpMetRisc.getWlquTpMetRiscAsBuffer(buffer, position);
        position += S089TpMetRisc.Len.S089_TP_MET_RISC;
        s089DtLiq.getWlquDtLiqAsBuffer(buffer, position);
        position += S089DtLiq.Len.S089_DT_LIQ;
        MarshalByte.writeString(buffer, position, s089CodDvs, Len.S089_COD_DVS);
        position += Len.S089_COD_DVS;
        s089TotImpLrdLiqto.getWlquTotImpLrdLiqtoAsBuffer(buffer, position);
        position += S089TotImpLrdLiqto.Len.S089_TOT_IMP_LRD_LIQTO;
        s089TotImpPrest.getWlquTotImpPrestAsBuffer(buffer, position);
        position += S089TotImpPrest.Len.S089_TOT_IMP_PREST;
        s089TotImpIntrPrest.getWlquTotImpIntrPrestAsBuffer(buffer, position);
        position += S089TotImpIntrPrest.Len.S089_TOT_IMP_INTR_PREST;
        s089TotImpUti.getWlquTotImpUtiAsBuffer(buffer, position);
        position += S089TotImpUti.Len.S089_TOT_IMP_UTI;
        s089TotImpRitTfr.getWlquTotImpRitTfrAsBuffer(buffer, position);
        position += S089TotImpRitTfr.Len.S089_TOT_IMP_RIT_TFR;
        s089TotImpRitAcc.getWlquTotImpRitAccAsBuffer(buffer, position);
        position += S089TotImpRitAcc.Len.S089_TOT_IMP_RIT_ACC;
        s089TotImpRitVis.getWlquTotImpRitVisAsBuffer(buffer, position);
        position += S089TotImpRitVis.Len.S089_TOT_IMP_RIT_VIS;
        s089TotImpbTfr.getWlquTotImpbTfrAsBuffer(buffer, position);
        position += S089TotImpbTfr.Len.S089_TOT_IMPB_TFR;
        s089TotImpbAcc.getWlquTotImpbAccAsBuffer(buffer, position);
        position += S089TotImpbAcc.Len.S089_TOT_IMPB_ACC;
        s089TotImpbVis.getWlquTotImpbVisAsBuffer(buffer, position);
        position += S089TotImpbVis.Len.S089_TOT_IMPB_VIS;
        s089TotImpRimb.getWlquTotImpRimbAsBuffer(buffer, position);
        position += S089TotImpRimb.Len.S089_TOT_IMP_RIMB;
        s089ImpbImpstPrvr.getWlquImpbImpstPrvrAsBuffer(buffer, position);
        position += S089ImpbImpstPrvr.Len.S089_IMPB_IMPST_PRVR;
        s089ImpstPrvr.getWlquImpstPrvrAsBuffer(buffer, position);
        position += S089ImpstPrvr.Len.S089_IMPST_PRVR;
        s089ImpbImpst252.getWlquImpbImpst252AsBuffer(buffer, position);
        position += S089ImpbImpst252.Len.S089_IMPB_IMPST252;
        s089Impst252.getWlquImpst252AsBuffer(buffer, position);
        position += S089Impst252.Len.S089_IMPST252;
        s089TotImpIs.getWlquTotImpIsAsBuffer(buffer, position);
        position += S089TotImpIs.Len.S089_TOT_IMP_IS;
        s089ImpDirLiq.getWlquImpDirLiqAsBuffer(buffer, position);
        position += S089ImpDirLiq.Len.S089_IMP_DIR_LIQ;
        s089TotImpNetLiqto.getWlquTotImpNetLiqtoAsBuffer(buffer, position);
        position += S089TotImpNetLiqto.Len.S089_TOT_IMP_NET_LIQTO;
        s089MontEnd2000.getWlquMontEnd2000AsBuffer(buffer, position);
        position += S089MontEnd2000.Len.S089_MONT_END2000;
        s089MontEnd2006.getWlquMontEnd2006AsBuffer(buffer, position);
        position += S089MontEnd2006.Len.S089_MONT_END2006;
        s089PcRen.getWlquPcRenAsBuffer(buffer, position);
        position += S089PcRen.Len.S089_PC_REN;
        s089ImpPnl.getWlquImpPnlAsBuffer(buffer, position);
        position += S089ImpPnl.Len.S089_IMP_PNL;
        s089ImpbIrpef.getWlquImpbIrpefAsBuffer(buffer, position);
        position += S089ImpbIrpef.Len.S089_IMPB_IRPEF;
        s089ImpstIrpef.getWlquImpstIrpefAsBuffer(buffer, position);
        position += S089ImpstIrpef.Len.S089_IMPST_IRPEF;
        s089DtVlt.getWlquDtVltAsBuffer(buffer, position);
        position += S089DtVlt.Len.S089_DT_VLT;
        s089DtEndIstr.getWlquDtEndIstrAsBuffer(buffer, position);
        position += S089DtEndIstr.Len.S089_DT_END_ISTR;
        MarshalByte.writeString(buffer, position, s089TpRimb, Len.S089_TP_RIMB);
        position += Len.S089_TP_RIMB;
        s089SpeRcs.getWlquSpeRcsAsBuffer(buffer, position);
        position += S089SpeRcs.Len.S089_SPE_RCS;
        MarshalByte.writeString(buffer, position, s089IbLiq, Len.S089_IB_LIQ);
        position += Len.S089_IB_LIQ;
        s089TotIasOnerPrvnt.getWlquTotIasOnerPrvntAsBuffer(buffer, position);
        position += S089TotIasOnerPrvnt.Len.S089_TOT_IAS_ONER_PRVNT;
        s089TotIasMggSin.getWlquTotIasMggSinAsBuffer(buffer, position);
        position += S089TotIasMggSin.Len.S089_TOT_IAS_MGG_SIN;
        s089TotIasRstDpst.getWlquTotIasRstDpstAsBuffer(buffer, position);
        position += S089TotIasRstDpst.Len.S089_TOT_IAS_RST_DPST;
        s089ImpOnerLiq.getWlquImpOnerLiqAsBuffer(buffer, position);
        position += S089ImpOnerLiq.Len.S089_IMP_ONER_LIQ;
        s089ComponTaxRimb.getWlquComponTaxRimbAsBuffer(buffer, position);
        position += S089ComponTaxRimb.Len.S089_COMPON_TAX_RIMB;
        MarshalByte.writeString(buffer, position, s089TpMezPag, Len.S089_TP_MEZ_PAG);
        position += Len.S089_TP_MEZ_PAG;
        s089ImpExcontr.getWlquImpExcontrAsBuffer(buffer, position);
        position += S089ImpExcontr.Len.S089_IMP_EXCONTR;
        s089ImpIntrRitPag.getWlquImpIntrRitPagAsBuffer(buffer, position);
        position += S089ImpIntrRitPag.Len.S089_IMP_INTR_RIT_PAG;
        s089BnsNonGoduto.getWlquBnsNonGodutoAsBuffer(buffer, position);
        position += S089BnsNonGoduto.Len.S089_BNS_NON_GODUTO;
        s089CnbtInpstfm.getWlquCnbtInpstfmAsBuffer(buffer, position);
        position += S089CnbtInpstfm.Len.S089_CNBT_INPSTFM;
        s089ImpstDaRimb.getWlquImpstDaRimbAsBuffer(buffer, position);
        position += S089ImpstDaRimb.Len.S089_IMPST_DA_RIMB;
        s089ImpbIs.getWlquImpbIsAsBuffer(buffer, position);
        position += S089ImpbIs.Len.S089_IMPB_IS;
        s089TaxSep.getWlquTaxSepAsBuffer(buffer, position);
        position += S089TaxSep.Len.S089_TAX_SEP;
        s089ImpbTaxSep.getWlquImpbTaxSepAsBuffer(buffer, position);
        position += S089ImpbTaxSep.Len.S089_IMPB_TAX_SEP;
        s089ImpbIntrSuPrest.getWlquImpbIntrSuPrestAsBuffer(buffer, position);
        position += S089ImpbIntrSuPrest.Len.S089_IMPB_INTR_SU_PREST;
        s089AddizComun.getWlquAddizComunAsBuffer(buffer, position);
        position += S089AddizComun.Len.S089_ADDIZ_COMUN;
        s089ImpbAddizComun.getWlquImpbAddizComunAsBuffer(buffer, position);
        position += S089ImpbAddizComun.Len.S089_IMPB_ADDIZ_COMUN;
        s089AddizRegion.getWlquAddizRegionAsBuffer(buffer, position);
        position += S089AddizRegion.Len.S089_ADDIZ_REGION;
        s089ImpbAddizRegion.getWlquImpbAddizRegionAsBuffer(buffer, position);
        position += S089ImpbAddizRegion.Len.S089_IMPB_ADDIZ_REGION;
        s089MontDal2007.getWlquMontDal2007AsBuffer(buffer, position);
        position += S089MontDal2007.Len.S089_MONT_DAL2007;
        s089ImpbCnbtInpstfm.getWlquImpbCnbtInpstfmAsBuffer(buffer, position);
        position += S089ImpbCnbtInpstfm.Len.S089_IMPB_CNBT_INPSTFM;
        s089ImpLrdDaRimb.getWlquImpLrdDaRimbAsBuffer(buffer, position);
        position += S089ImpLrdDaRimb.Len.S089_IMP_LRD_DA_RIMB;
        s089ImpDirDaRimb.getWlquImpDirDaRimbAsBuffer(buffer, position);
        position += S089ImpDirDaRimb.Len.S089_IMP_DIR_DA_RIMB;
        s089RisMat.getWlquRisMatAsBuffer(buffer, position);
        position += S089RisMat.Len.S089_RIS_MAT;
        s089RisSpe.getWlquRisSpeAsBuffer(buffer, position);
        position += S089RisSpe.Len.S089_RIS_SPE;
        MarshalByte.writeLongAsPacked(buffer, position, s089DsRiga, Len.Int.WLQU_DS_RIGA, 0);
        position += Len.WLQU_DS_RIGA;
        MarshalByte.writeChar(buffer, position, s089DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, s089DsVer, Len.Int.WLQU_DS_VER, 0);
        position += Len.WLQU_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, s089DsTsIniCptz, Len.Int.WLQU_DS_TS_INI_CPTZ, 0);
        position += Len.WLQU_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, s089DsTsEndCptz, Len.Int.WLQU_DS_TS_END_CPTZ, 0);
        position += Len.WLQU_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, s089DsUtente, Len.S089_DS_UTENTE);
        position += Len.S089_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, s089DsStatoElab);
        position += Types.CHAR_SIZE;
        s089TotIasPnl.getWlquTotIasPnlAsBuffer(buffer, position);
        position += S089TotIasPnl.Len.S089_TOT_IAS_PNL;
        MarshalByte.writeChar(buffer, position, s089FlEveGarto);
        position += Types.CHAR_SIZE;
        s089ImpRenK1.getWlquImpRenK1AsBuffer(buffer, position);
        position += S089ImpRenK1.Len.S089_IMP_REN_K1;
        s089ImpRenK2.getWlquImpRenK2AsBuffer(buffer, position);
        position += S089ImpRenK2.Len.S089_IMP_REN_K2;
        s089ImpRenK3.getWlquImpRenK3AsBuffer(buffer, position);
        position += S089ImpRenK3.Len.S089_IMP_REN_K3;
        s089PcRenK1.getWlquPcRenK1AsBuffer(buffer, position);
        position += S089PcRenK1.Len.S089_PC_REN_K1;
        s089PcRenK2.getWlquPcRenK2AsBuffer(buffer, position);
        position += S089PcRenK2.Len.S089_PC_REN_K2;
        s089PcRenK3.getWlquPcRenK3AsBuffer(buffer, position);
        position += S089PcRenK3.Len.S089_PC_REN_K3;
        MarshalByte.writeString(buffer, position, s089TpCausAntic, Len.S089_TP_CAUS_ANTIC);
        position += Len.S089_TP_CAUS_ANTIC;
        s089ImpLrdLiqtoRilt.getWlquImpLrdLiqtoRiltAsBuffer(buffer, position);
        position += S089ImpLrdLiqtoRilt.Len.S089_IMP_LRD_LIQTO_RILT;
        s089ImpstApplRilt.getWlquImpstApplRiltAsBuffer(buffer, position);
        position += S089ImpstApplRilt.Len.S089_IMPST_APPL_RILT;
        s089PcRiscParz.getWlquPcRiscParzAsBuffer(buffer, position);
        position += S089PcRiscParz.Len.S089_PC_RISC_PARZ;
        s089ImpstBolloTotV.getWlquImpstBolloTotVAsBuffer(buffer, position);
        position += S089ImpstBolloTotV.Len.S089_IMPST_BOLLO_TOT_V;
        s089ImpstBolloDettC.getWlquImpstBolloDettCAsBuffer(buffer, position);
        position += S089ImpstBolloDettC.Len.S089_IMPST_BOLLO_DETT_C;
        s089ImpstBolloTotSw.getWlquImpstBolloTotSwAsBuffer(buffer, position);
        position += S089ImpstBolloTotSw.Len.S089_IMPST_BOLLO_TOT_SW;
        s089ImpstBolloTotAa.getWlquImpstBolloTotAaAsBuffer(buffer, position);
        position += S089ImpstBolloTotAa.Len.S089_IMPST_BOLLO_TOT_AA;
        s089ImpbVis1382011.getWlquImpbVis1382011AsBuffer(buffer, position);
        position += S089ImpbVis1382011.Len.S089_IMPB_VIS1382011;
        s089ImpstVis1382011.getWlquImpstVis1382011AsBuffer(buffer, position);
        position += S089ImpstVis1382011.Len.S089_IMPST_VIS1382011;
        s089ImpbIs1382011.getWlquImpbIs1382011AsBuffer(buffer, position);
        position += S089ImpbIs1382011.Len.S089_IMPB_IS1382011;
        s089ImpstSost1382011.getWlquImpstSost1382011AsBuffer(buffer, position);
        position += S089ImpstSost1382011.Len.S089_IMPST_SOST1382011;
        s089PcAbbTitStat.getWlquPcAbbTitStatAsBuffer(buffer, position);
        position += S089PcAbbTitStat.Len.S089_PC_ABB_TIT_STAT;
        s089ImpbBolloDettC.getWlquImpbBolloDettCAsBuffer(buffer, position);
        position += S089ImpbBolloDettC.Len.S089_IMPB_BOLLO_DETT_C;
        MarshalByte.writeChar(buffer, position, s089FlPreComp);
        position += Types.CHAR_SIZE;
        s089ImpbVis662014.getWlquImpbVis662014AsBuffer(buffer, position);
        position += S089ImpbVis662014.Len.S089_IMPB_VIS662014;
        s089ImpstVis662014.getWlquImpstVis662014AsBuffer(buffer, position);
        position += S089ImpstVis662014.Len.S089_IMPST_VIS662014;
        s089ImpbIs662014.getWlquImpbIs662014AsBuffer(buffer, position);
        position += S089ImpbIs662014.Len.S089_IMPB_IS662014;
        s089ImpstSost662014.getWlquImpstSost662014AsBuffer(buffer, position);
        position += S089ImpstSost662014.Len.S089_IMPST_SOST662014;
        s089PcAbbTs662014.getWlquPcAbbTs662014AsBuffer(buffer, position);
        position += S089PcAbbTs662014.Len.S089_PC_ABB_TS662014;
        s089ImpLrdCalcCp.getWlquImpLrdCalcCpAsBuffer(buffer, position);
        position += S089ImpLrdCalcCp.Len.S089_IMP_LRD_CALC_CP;
        s089CosTunnelUscita.getWlquCosTunnelUscitaAsBuffer(buffer, position);
        return buffer;
    }

    public void initWlquDatiSpaces() {
        s089IdLiq = Types.INVALID_INT_VAL;
        s089IdOgg = Types.INVALID_INT_VAL;
        s089TpOgg = "";
        s089IdMoviCrz = Types.INVALID_INT_VAL;
        s089IdMoviChiu.initWlquIdMoviChiuSpaces();
        s089DtIniEff = Types.INVALID_INT_VAL;
        s089DtEndEff = Types.INVALID_INT_VAL;
        s089CodCompAnia = Types.INVALID_INT_VAL;
        s089IbOgg = "";
        s089TpLiq = "";
        s089DescCauEveSin = "";
        s089CodCauSin = "";
        s089CodSinCatstrf = "";
        s089DtMor.initWlquDtMorSpaces();
        s089DtDen.initWlquDtDenSpaces();
        s089DtPervDen.initWlquDtPervDenSpaces();
        s089DtRich.initWlquDtRichSpaces();
        s089TpSin = "";
        s089TpRisc = "";
        s089TpMetRisc.initWlquTpMetRiscSpaces();
        s089DtLiq.initWlquDtLiqSpaces();
        s089CodDvs = "";
        s089TotImpLrdLiqto.initWlquTotImpLrdLiqtoSpaces();
        s089TotImpPrest.initWlquTotImpPrestSpaces();
        s089TotImpIntrPrest.initWlquTotImpIntrPrestSpaces();
        s089TotImpUti.initWlquTotImpUtiSpaces();
        s089TotImpRitTfr.initWlquTotImpRitTfrSpaces();
        s089TotImpRitAcc.initWlquTotImpRitAccSpaces();
        s089TotImpRitVis.initWlquTotImpRitVisSpaces();
        s089TotImpbTfr.initWlquTotImpbTfrSpaces();
        s089TotImpbAcc.initWlquTotImpbAccSpaces();
        s089TotImpbVis.initWlquTotImpbVisSpaces();
        s089TotImpRimb.initWlquTotImpRimbSpaces();
        s089ImpbImpstPrvr.initWlquImpbImpstPrvrSpaces();
        s089ImpstPrvr.initWlquImpstPrvrSpaces();
        s089ImpbImpst252.initWlquImpbImpst252Spaces();
        s089Impst252.initWlquImpst252Spaces();
        s089TotImpIs.initWlquTotImpIsSpaces();
        s089ImpDirLiq.initWlquImpDirLiqSpaces();
        s089TotImpNetLiqto.initWlquTotImpNetLiqtoSpaces();
        s089MontEnd2000.initWlquMontEnd2000Spaces();
        s089MontEnd2006.initWlquMontEnd2006Spaces();
        s089PcRen.initWlquPcRenSpaces();
        s089ImpPnl.initWlquImpPnlSpaces();
        s089ImpbIrpef.initWlquImpbIrpefSpaces();
        s089ImpstIrpef.initWlquImpstIrpefSpaces();
        s089DtVlt.initWlquDtVltSpaces();
        s089DtEndIstr.initWlquDtEndIstrSpaces();
        s089TpRimb = "";
        s089SpeRcs.initWlquSpeRcsSpaces();
        s089IbLiq = "";
        s089TotIasOnerPrvnt.initWlquTotIasOnerPrvntSpaces();
        s089TotIasMggSin.initWlquTotIasMggSinSpaces();
        s089TotIasRstDpst.initWlquTotIasRstDpstSpaces();
        s089ImpOnerLiq.initWlquImpOnerLiqSpaces();
        s089ComponTaxRimb.initWlquComponTaxRimbSpaces();
        s089TpMezPag = "";
        s089ImpExcontr.initWlquImpExcontrSpaces();
        s089ImpIntrRitPag.initWlquImpIntrRitPagSpaces();
        s089BnsNonGoduto.initWlquBnsNonGodutoSpaces();
        s089CnbtInpstfm.initWlquCnbtInpstfmSpaces();
        s089ImpstDaRimb.initWlquImpstDaRimbSpaces();
        s089ImpbIs.initWlquImpbIsSpaces();
        s089TaxSep.initWlquTaxSepSpaces();
        s089ImpbTaxSep.initWlquImpbTaxSepSpaces();
        s089ImpbIntrSuPrest.initWlquImpbIntrSuPrestSpaces();
        s089AddizComun.initWlquAddizComunSpaces();
        s089ImpbAddizComun.initWlquImpbAddizComunSpaces();
        s089AddizRegion.initWlquAddizRegionSpaces();
        s089ImpbAddizRegion.initWlquImpbAddizRegionSpaces();
        s089MontDal2007.initWlquMontDal2007Spaces();
        s089ImpbCnbtInpstfm.initWlquImpbCnbtInpstfmSpaces();
        s089ImpLrdDaRimb.initWlquImpLrdDaRimbSpaces();
        s089ImpDirDaRimb.initWlquImpDirDaRimbSpaces();
        s089RisMat.initWlquRisMatSpaces();
        s089RisSpe.initWlquRisSpeSpaces();
        s089DsRiga = Types.INVALID_LONG_VAL;
        s089DsOperSql = Types.SPACE_CHAR;
        s089DsVer = Types.INVALID_INT_VAL;
        s089DsTsIniCptz = Types.INVALID_LONG_VAL;
        s089DsTsEndCptz = Types.INVALID_LONG_VAL;
        s089DsUtente = "";
        s089DsStatoElab = Types.SPACE_CHAR;
        s089TotIasPnl.initWlquTotIasPnlSpaces();
        s089FlEveGarto = Types.SPACE_CHAR;
        s089ImpRenK1.initWlquImpRenK1Spaces();
        s089ImpRenK2.initWlquImpRenK2Spaces();
        s089ImpRenK3.initWlquImpRenK3Spaces();
        s089PcRenK1.initWlquPcRenK1Spaces();
        s089PcRenK2.initWlquPcRenK2Spaces();
        s089PcRenK3.initWlquPcRenK3Spaces();
        s089TpCausAntic = "";
        s089ImpLrdLiqtoRilt.initWlquImpLrdLiqtoRiltSpaces();
        s089ImpstApplRilt.initWlquImpstApplRiltSpaces();
        s089PcRiscParz.initWlquPcRiscParzSpaces();
        s089ImpstBolloTotV.initWlquImpstBolloTotVSpaces();
        s089ImpstBolloDettC.initWlquImpstBolloDettCSpaces();
        s089ImpstBolloTotSw.initWlquImpstBolloTotSwSpaces();
        s089ImpstBolloTotAa.initWlquImpstBolloTotAaSpaces();
        s089ImpbVis1382011.initWlquImpbVis1382011Spaces();
        s089ImpstVis1382011.initWlquImpstVis1382011Spaces();
        s089ImpbIs1382011.initWlquImpbIs1382011Spaces();
        s089ImpstSost1382011.initWlquImpstSost1382011Spaces();
        s089PcAbbTitStat.initWlquPcAbbTitStatSpaces();
        s089ImpbBolloDettC.initWlquImpbBolloDettCSpaces();
        s089FlPreComp = Types.SPACE_CHAR;
        s089ImpbVis662014.initWlquImpbVis662014Spaces();
        s089ImpstVis662014.initWlquImpstVis662014Spaces();
        s089ImpbIs662014.initWlquImpbIs662014Spaces();
        s089ImpstSost662014.initWlquImpstSost662014Spaces();
        s089PcAbbTs662014.initWlquPcAbbTs662014Spaces();
        s089ImpLrdCalcCp.initWlquImpLrdCalcCpSpaces();
        s089CosTunnelUscita.initWlquCosTunnelUscitaSpaces();
    }

    public void setWlquIdLiq(int wlquIdLiq) {
        this.s089IdLiq = wlquIdLiq;
    }

    public int getWlquIdLiq() {
        return this.s089IdLiq;
    }

    public void setWlquIdOgg(int wlquIdOgg) {
        this.s089IdOgg = wlquIdOgg;
    }

    public int getWlquIdOgg() {
        return this.s089IdOgg;
    }

    public void setWlquTpOgg(String wlquTpOgg) {
        this.s089TpOgg = Functions.subString(wlquTpOgg, Len.S089_TP_OGG);
    }

    public String getWlquTpOgg() {
        return this.s089TpOgg;
    }

    public void setWlquIdMoviCrz(int wlquIdMoviCrz) {
        this.s089IdMoviCrz = wlquIdMoviCrz;
    }

    public int getWlquIdMoviCrz() {
        return this.s089IdMoviCrz;
    }

    public void setWlquDtIniEff(int wlquDtIniEff) {
        this.s089DtIniEff = wlquDtIniEff;
    }

    public int getWlquDtIniEff() {
        return this.s089DtIniEff;
    }

    public void setWlquDtEndEff(int wlquDtEndEff) {
        this.s089DtEndEff = wlquDtEndEff;
    }

    public int getWlquDtEndEff() {
        return this.s089DtEndEff;
    }

    public void setWlquCodCompAnia(int wlquCodCompAnia) {
        this.s089CodCompAnia = wlquCodCompAnia;
    }

    public int getWlquCodCompAnia() {
        return this.s089CodCompAnia;
    }

    public void setWlquIbOgg(String wlquIbOgg) {
        this.s089IbOgg = Functions.subString(wlquIbOgg, Len.S089_IB_OGG);
    }

    public String getWlquIbOgg() {
        return this.s089IbOgg;
    }

    public void setWlquTpLiq(String wlquTpLiq) {
        this.s089TpLiq = Functions.subString(wlquTpLiq, Len.S089_TP_LIQ);
    }

    public String getWlquTpLiq() {
        return this.s089TpLiq;
    }

    public void setWlquDescCauEveSin(String wlquDescCauEveSin) {
        this.s089DescCauEveSin = Functions.subString(wlquDescCauEveSin, Len.S089_DESC_CAU_EVE_SIN);
    }

    public String getWlquDescCauEveSin() {
        return this.s089DescCauEveSin;
    }

    public void setWlquCodCauSin(String wlquCodCauSin) {
        this.s089CodCauSin = Functions.subString(wlquCodCauSin, Len.S089_COD_CAU_SIN);
    }

    public String getWlquCodCauSin() {
        return this.s089CodCauSin;
    }

    public void setWlquCodSinCatstrf(String wlquCodSinCatstrf) {
        this.s089CodSinCatstrf = Functions.subString(wlquCodSinCatstrf, Len.S089_COD_SIN_CATSTRF);
    }

    public String getWlquCodSinCatstrf() {
        return this.s089CodSinCatstrf;
    }

    public void setWlquTpSin(String wlquTpSin) {
        this.s089TpSin = Functions.subString(wlquTpSin, Len.S089_TP_SIN);
    }

    public String getWlquTpSin() {
        return this.s089TpSin;
    }

    public void setWlquTpRisc(String wlquTpRisc) {
        this.s089TpRisc = Functions.subString(wlquTpRisc, Len.S089_TP_RISC);
    }

    public String getWlquTpRisc() {
        return this.s089TpRisc;
    }

    public void setWlquCodDvs(String wlquCodDvs) {
        this.s089CodDvs = Functions.subString(wlquCodDvs, Len.S089_COD_DVS);
    }

    public String getWlquCodDvs() {
        return this.s089CodDvs;
    }

    public void setWlquTpRimb(String wlquTpRimb) {
        this.s089TpRimb = Functions.subString(wlquTpRimb, Len.S089_TP_RIMB);
    }

    public String getWlquTpRimb() {
        return this.s089TpRimb;
    }

    public void setWlquIbLiq(String wlquIbLiq) {
        this.s089IbLiq = Functions.subString(wlquIbLiq, Len.S089_IB_LIQ);
    }

    public String getWlquIbLiq() {
        return this.s089IbLiq;
    }

    public void setWlquTpMezPag(String wlquTpMezPag) {
        this.s089TpMezPag = Functions.subString(wlquTpMezPag, Len.S089_TP_MEZ_PAG);
    }

    public String getWlquTpMezPag() {
        return this.s089TpMezPag;
    }

    public void setWlquDsRiga(long wlquDsRiga) {
        this.s089DsRiga = wlquDsRiga;
    }

    public long getWlquDsRiga() {
        return this.s089DsRiga;
    }

    public void setWlquDsOperSql(char wlquDsOperSql) {
        this.s089DsOperSql = wlquDsOperSql;
    }

    public char getWlquDsOperSql() {
        return this.s089DsOperSql;
    }

    public void setWlquDsVer(int wlquDsVer) {
        this.s089DsVer = wlquDsVer;
    }

    public int getWlquDsVer() {
        return this.s089DsVer;
    }

    public void setWlquDsTsIniCptz(long wlquDsTsIniCptz) {
        this.s089DsTsIniCptz = wlquDsTsIniCptz;
    }

    public long getWlquDsTsIniCptz() {
        return this.s089DsTsIniCptz;
    }

    public void setWlquDsTsEndCptz(long wlquDsTsEndCptz) {
        this.s089DsTsEndCptz = wlquDsTsEndCptz;
    }

    public long getWlquDsTsEndCptz() {
        return this.s089DsTsEndCptz;
    }

    public void setWlquDsUtente(String wlquDsUtente) {
        this.s089DsUtente = Functions.subString(wlquDsUtente, Len.S089_DS_UTENTE);
    }

    public String getWlquDsUtente() {
        return this.s089DsUtente;
    }

    public void setWlquDsStatoElab(char wlquDsStatoElab) {
        this.s089DsStatoElab = wlquDsStatoElab;
    }

    public char getWlquDsStatoElab() {
        return this.s089DsStatoElab;
    }

    public void setWlquFlEveGarto(char wlquFlEveGarto) {
        this.s089FlEveGarto = wlquFlEveGarto;
    }

    public char getWlquFlEveGarto() {
        return this.s089FlEveGarto;
    }

    public void setWlquTpCausAntic(String wlquTpCausAntic) {
        this.s089TpCausAntic = Functions.subString(wlquTpCausAntic, Len.S089_TP_CAUS_ANTIC);
    }

    public String getWlquTpCausAntic() {
        return this.s089TpCausAntic;
    }

    public void setWlquFlPreComp(char wlquFlPreComp) {
        this.s089FlPreComp = wlquFlPreComp;
    }

    public char getWlquFlPreComp() {
        return this.s089FlPreComp;
    }

    public S089AddizComun getS089AddizComun() {
        return s089AddizComun;
    }

    public S089AddizRegion getS089AddizRegion() {
        return s089AddizRegion;
    }

    public S089BnsNonGoduto getS089BnsNonGoduto() {
        return s089BnsNonGoduto;
    }

    public S089CnbtInpstfm getS089CnbtInpstfm() {
        return s089CnbtInpstfm;
    }

    public S089ComponTaxRimb getS089ComponTaxRimb() {
        return s089ComponTaxRimb;
    }

    public S089CosTunnelUscita getS089CosTunnelUscita() {
        return s089CosTunnelUscita;
    }

    public S089DtDen getS089DtDen() {
        return s089DtDen;
    }

    public S089DtEndIstr getS089DtEndIstr() {
        return s089DtEndIstr;
    }

    public S089DtLiq getS089DtLiq() {
        return s089DtLiq;
    }

    public S089DtMor getS089DtMor() {
        return s089DtMor;
    }

    public S089DtPervDen getS089DtPervDen() {
        return s089DtPervDen;
    }

    public S089DtRich getS089DtRich() {
        return s089DtRich;
    }

    public S089DtVlt getS089DtVlt() {
        return s089DtVlt;
    }

    public S089IdMoviChiu getS089IdMoviChiu() {
        return s089IdMoviChiu;
    }

    public S089ImpDirDaRimb getS089ImpDirDaRimb() {
        return s089ImpDirDaRimb;
    }

    public S089ImpDirLiq getS089ImpDirLiq() {
        return s089ImpDirLiq;
    }

    public S089ImpExcontr getS089ImpExcontr() {
        return s089ImpExcontr;
    }

    public S089ImpIntrRitPag getS089ImpIntrRitPag() {
        return s089ImpIntrRitPag;
    }

    public S089ImpLrdCalcCp getS089ImpLrdCalcCp() {
        return s089ImpLrdCalcCp;
    }

    public S089ImpLrdDaRimb getS089ImpLrdDaRimb() {
        return s089ImpLrdDaRimb;
    }

    public S089ImpLrdLiqtoRilt getS089ImpLrdLiqtoRilt() {
        return s089ImpLrdLiqtoRilt;
    }

    public S089ImpOnerLiq getS089ImpOnerLiq() {
        return s089ImpOnerLiq;
    }

    public S089ImpPnl getS089ImpPnl() {
        return s089ImpPnl;
    }

    public S089ImpRenK1 getS089ImpRenK1() {
        return s089ImpRenK1;
    }

    public S089ImpRenK2 getS089ImpRenK2() {
        return s089ImpRenK2;
    }

    public S089ImpRenK3 getS089ImpRenK3() {
        return s089ImpRenK3;
    }

    public S089ImpbAddizComun getS089ImpbAddizComun() {
        return s089ImpbAddizComun;
    }

    public S089ImpbAddizRegion getS089ImpbAddizRegion() {
        return s089ImpbAddizRegion;
    }

    public S089ImpbBolloDettC getS089ImpbBolloDettC() {
        return s089ImpbBolloDettC;
    }

    public S089ImpbCnbtInpstfm getS089ImpbCnbtInpstfm() {
        return s089ImpbCnbtInpstfm;
    }

    public S089ImpbImpst252 getS089ImpbImpst252() {
        return s089ImpbImpst252;
    }

    public S089ImpbImpstPrvr getS089ImpbImpstPrvr() {
        return s089ImpbImpstPrvr;
    }

    public S089ImpbIntrSuPrest getS089ImpbIntrSuPrest() {
        return s089ImpbIntrSuPrest;
    }

    public S089ImpbIrpef getS089ImpbIrpef() {
        return s089ImpbIrpef;
    }

    public S089ImpbIs1382011 getS089ImpbIs1382011() {
        return s089ImpbIs1382011;
    }

    public S089ImpbIs662014 getS089ImpbIs662014() {
        return s089ImpbIs662014;
    }

    public S089ImpbIs getS089ImpbIs() {
        return s089ImpbIs;
    }

    public S089ImpbTaxSep getS089ImpbTaxSep() {
        return s089ImpbTaxSep;
    }

    public S089ImpbVis1382011 getS089ImpbVis1382011() {
        return s089ImpbVis1382011;
    }

    public S089ImpbVis662014 getS089ImpbVis662014() {
        return s089ImpbVis662014;
    }

    public S089Impst252 getS089Impst252() {
        return s089Impst252;
    }

    public S089ImpstApplRilt getS089ImpstApplRilt() {
        return s089ImpstApplRilt;
    }

    public S089ImpstBolloDettC getS089ImpstBolloDettC() {
        return s089ImpstBolloDettC;
    }

    public S089ImpstBolloTotAa getS089ImpstBolloTotAa() {
        return s089ImpstBolloTotAa;
    }

    public S089ImpstBolloTotSw getS089ImpstBolloTotSw() {
        return s089ImpstBolloTotSw;
    }

    public S089ImpstBolloTotV getS089ImpstBolloTotV() {
        return s089ImpstBolloTotV;
    }

    public S089ImpstDaRimb getS089ImpstDaRimb() {
        return s089ImpstDaRimb;
    }

    public S089ImpstIrpef getS089ImpstIrpef() {
        return s089ImpstIrpef;
    }

    public S089ImpstPrvr getS089ImpstPrvr() {
        return s089ImpstPrvr;
    }

    public S089ImpstSost1382011 getS089ImpstSost1382011() {
        return s089ImpstSost1382011;
    }

    public S089ImpstSost662014 getS089ImpstSost662014() {
        return s089ImpstSost662014;
    }

    public S089ImpstVis1382011 getS089ImpstVis1382011() {
        return s089ImpstVis1382011;
    }

    public S089ImpstVis662014 getS089ImpstVis662014() {
        return s089ImpstVis662014;
    }

    public S089MontDal2007 getS089MontDal2007() {
        return s089MontDal2007;
    }

    public S089MontEnd2000 getS089MontEnd2000() {
        return s089MontEnd2000;
    }

    public S089MontEnd2006 getS089MontEnd2006() {
        return s089MontEnd2006;
    }

    public S089PcAbbTitStat getS089PcAbbTitStat() {
        return s089PcAbbTitStat;
    }

    public S089PcAbbTs662014 getS089PcAbbTs662014() {
        return s089PcAbbTs662014;
    }

    public S089PcRen getS089PcRen() {
        return s089PcRen;
    }

    public S089PcRenK1 getS089PcRenK1() {
        return s089PcRenK1;
    }

    public S089PcRenK2 getS089PcRenK2() {
        return s089PcRenK2;
    }

    public S089PcRenK3 getS089PcRenK3() {
        return s089PcRenK3;
    }

    public S089PcRiscParz getS089PcRiscParz() {
        return s089PcRiscParz;
    }

    public S089RisMat getS089RisMat() {
        return s089RisMat;
    }

    public S089RisSpe getS089RisSpe() {
        return s089RisSpe;
    }

    public S089SpeRcs getS089SpeRcs() {
        return s089SpeRcs;
    }

    public S089TaxSep getS089TaxSep() {
        return s089TaxSep;
    }

    public S089TotIasMggSin getS089TotIasMggSin() {
        return s089TotIasMggSin;
    }

    public S089TotIasOnerPrvnt getS089TotIasOnerPrvnt() {
        return s089TotIasOnerPrvnt;
    }

    public S089TotIasPnl getS089TotIasPnl() {
        return s089TotIasPnl;
    }

    public S089TotIasRstDpst getS089TotIasRstDpst() {
        return s089TotIasRstDpst;
    }

    public S089TotImpIntrPrest getS089TotImpIntrPrest() {
        return s089TotImpIntrPrest;
    }

    public S089TotImpIs getS089TotImpIs() {
        return s089TotImpIs;
    }

    public S089TotImpLrdLiqto getS089TotImpLrdLiqto() {
        return s089TotImpLrdLiqto;
    }

    public S089TotImpNetLiqto getS089TotImpNetLiqto() {
        return s089TotImpNetLiqto;
    }

    public S089TotImpPrest getS089TotImpPrest() {
        return s089TotImpPrest;
    }

    public S089TotImpRimb getS089TotImpRimb() {
        return s089TotImpRimb;
    }

    public S089TotImpRitAcc getS089TotImpRitAcc() {
        return s089TotImpRitAcc;
    }

    public S089TotImpRitTfr getS089TotImpRitTfr() {
        return s089TotImpRitTfr;
    }

    public S089TotImpRitVis getS089TotImpRitVis() {
        return s089TotImpRitVis;
    }

    public S089TotImpUti getS089TotImpUti() {
        return s089TotImpUti;
    }

    public S089TotImpbAcc getS089TotImpbAcc() {
        return s089TotImpbAcc;
    }

    public S089TotImpbTfr getS089TotImpbTfr() {
        return s089TotImpbTfr;
    }

    public S089TotImpbVis getS089TotImpbVis() {
        return s089TotImpbVis;
    }

    public S089TpMetRisc getS089TpMetRisc() {
        return s089TpMetRisc;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TP_OGG = 2;
        public static final int S089_IB_OGG = 40;
        public static final int S089_TP_LIQ = 2;
        public static final int S089_DESC_CAU_EVE_SIN = 100;
        public static final int S089_COD_CAU_SIN = 20;
        public static final int S089_COD_SIN_CATSTRF = 20;
        public static final int S089_TP_SIN = 2;
        public static final int S089_TP_RISC = 2;
        public static final int S089_COD_DVS = 20;
        public static final int S089_TP_RIMB = 2;
        public static final int S089_IB_LIQ = 40;
        public static final int S089_TP_MEZ_PAG = 2;
        public static final int S089_DS_UTENTE = 20;
        public static final int S089_TP_CAUS_ANTIC = 2;
        public static final int WLQU_ID_LIQ = 5;
        public static final int WLQU_ID_OGG = 5;
        public static final int WLQU_ID_MOVI_CRZ = 5;
        public static final int WLQU_DT_INI_EFF = 5;
        public static final int WLQU_DT_END_EFF = 5;
        public static final int WLQU_COD_COMP_ANIA = 3;
        public static final int WLQU_DS_RIGA = 6;
        public static final int WLQU_DS_OPER_SQL = 1;
        public static final int WLQU_DS_VER = 5;
        public static final int WLQU_DS_TS_INI_CPTZ = 10;
        public static final int WLQU_DS_TS_END_CPTZ = 10;
        public static final int WLQU_DS_STATO_ELAB = 1;
        public static final int WLQU_FL_EVE_GARTO = 1;
        public static final int WLQU_FL_PRE_COMP = 1;
        public static final int WLQU_DATI = WLQU_ID_LIQ + WLQU_ID_OGG + S089_TP_OGG + WLQU_ID_MOVI_CRZ + S089IdMoviChiu.Len.S089_ID_MOVI_CHIU + WLQU_DT_INI_EFF + WLQU_DT_END_EFF + WLQU_COD_COMP_ANIA + S089_IB_OGG + S089_TP_LIQ + S089_DESC_CAU_EVE_SIN + S089_COD_CAU_SIN + S089_COD_SIN_CATSTRF + S089DtMor.Len.S089_DT_MOR + S089DtDen.Len.S089_DT_DEN + S089DtPervDen.Len.S089_DT_PERV_DEN + S089DtRich.Len.S089_DT_RICH + S089_TP_SIN + S089_TP_RISC + S089TpMetRisc.Len.S089_TP_MET_RISC + S089DtLiq.Len.S089_DT_LIQ + S089_COD_DVS + S089TotImpLrdLiqto.Len.S089_TOT_IMP_LRD_LIQTO + S089TotImpPrest.Len.S089_TOT_IMP_PREST + S089TotImpIntrPrest.Len.S089_TOT_IMP_INTR_PREST + S089TotImpUti.Len.S089_TOT_IMP_UTI + S089TotImpRitTfr.Len.S089_TOT_IMP_RIT_TFR + S089TotImpRitAcc.Len.S089_TOT_IMP_RIT_ACC + S089TotImpRitVis.Len.S089_TOT_IMP_RIT_VIS + S089TotImpbTfr.Len.S089_TOT_IMPB_TFR + S089TotImpbAcc.Len.S089_TOT_IMPB_ACC + S089TotImpbVis.Len.S089_TOT_IMPB_VIS + S089TotImpRimb.Len.S089_TOT_IMP_RIMB + S089ImpbImpstPrvr.Len.S089_IMPB_IMPST_PRVR + S089ImpstPrvr.Len.S089_IMPST_PRVR + S089ImpbImpst252.Len.S089_IMPB_IMPST252 + S089Impst252.Len.S089_IMPST252 + S089TotImpIs.Len.S089_TOT_IMP_IS + S089ImpDirLiq.Len.S089_IMP_DIR_LIQ + S089TotImpNetLiqto.Len.S089_TOT_IMP_NET_LIQTO + S089MontEnd2000.Len.S089_MONT_END2000 + S089MontEnd2006.Len.S089_MONT_END2006 + S089PcRen.Len.S089_PC_REN + S089ImpPnl.Len.S089_IMP_PNL + S089ImpbIrpef.Len.S089_IMPB_IRPEF + S089ImpstIrpef.Len.S089_IMPST_IRPEF + S089DtVlt.Len.S089_DT_VLT + S089DtEndIstr.Len.S089_DT_END_ISTR + S089_TP_RIMB + S089SpeRcs.Len.S089_SPE_RCS + S089_IB_LIQ + S089TotIasOnerPrvnt.Len.S089_TOT_IAS_ONER_PRVNT + S089TotIasMggSin.Len.S089_TOT_IAS_MGG_SIN + S089TotIasRstDpst.Len.S089_TOT_IAS_RST_DPST + S089ImpOnerLiq.Len.S089_IMP_ONER_LIQ + S089ComponTaxRimb.Len.S089_COMPON_TAX_RIMB + S089_TP_MEZ_PAG + S089ImpExcontr.Len.S089_IMP_EXCONTR + S089ImpIntrRitPag.Len.S089_IMP_INTR_RIT_PAG + S089BnsNonGoduto.Len.S089_BNS_NON_GODUTO + S089CnbtInpstfm.Len.S089_CNBT_INPSTFM + S089ImpstDaRimb.Len.S089_IMPST_DA_RIMB + S089ImpbIs.Len.S089_IMPB_IS + S089TaxSep.Len.S089_TAX_SEP + S089ImpbTaxSep.Len.S089_IMPB_TAX_SEP + S089ImpbIntrSuPrest.Len.S089_IMPB_INTR_SU_PREST + S089AddizComun.Len.S089_ADDIZ_COMUN + S089ImpbAddizComun.Len.S089_IMPB_ADDIZ_COMUN + S089AddizRegion.Len.S089_ADDIZ_REGION + S089ImpbAddizRegion.Len.S089_IMPB_ADDIZ_REGION + S089MontDal2007.Len.S089_MONT_DAL2007 + S089ImpbCnbtInpstfm.Len.S089_IMPB_CNBT_INPSTFM + S089ImpLrdDaRimb.Len.S089_IMP_LRD_DA_RIMB + S089ImpDirDaRimb.Len.S089_IMP_DIR_DA_RIMB + S089RisMat.Len.S089_RIS_MAT + S089RisSpe.Len.S089_RIS_SPE + WLQU_DS_RIGA + WLQU_DS_OPER_SQL + WLQU_DS_VER + WLQU_DS_TS_INI_CPTZ + WLQU_DS_TS_END_CPTZ + S089_DS_UTENTE + WLQU_DS_STATO_ELAB + S089TotIasPnl.Len.S089_TOT_IAS_PNL + WLQU_FL_EVE_GARTO + S089ImpRenK1.Len.S089_IMP_REN_K1 + S089ImpRenK2.Len.S089_IMP_REN_K2 + S089ImpRenK3.Len.S089_IMP_REN_K3 + S089PcRenK1.Len.S089_PC_REN_K1 + S089PcRenK2.Len.S089_PC_REN_K2 + S089PcRenK3.Len.S089_PC_REN_K3 + S089_TP_CAUS_ANTIC + S089ImpLrdLiqtoRilt.Len.S089_IMP_LRD_LIQTO_RILT + S089ImpstApplRilt.Len.S089_IMPST_APPL_RILT + S089PcRiscParz.Len.S089_PC_RISC_PARZ + S089ImpstBolloTotV.Len.S089_IMPST_BOLLO_TOT_V + S089ImpstBolloDettC.Len.S089_IMPST_BOLLO_DETT_C + S089ImpstBolloTotSw.Len.S089_IMPST_BOLLO_TOT_SW + S089ImpstBolloTotAa.Len.S089_IMPST_BOLLO_TOT_AA + S089ImpbVis1382011.Len.S089_IMPB_VIS1382011 + S089ImpstVis1382011.Len.S089_IMPST_VIS1382011 + S089ImpbIs1382011.Len.S089_IMPB_IS1382011 + S089ImpstSost1382011.Len.S089_IMPST_SOST1382011 + S089PcAbbTitStat.Len.S089_PC_ABB_TIT_STAT + S089ImpbBolloDettC.Len.S089_IMPB_BOLLO_DETT_C + WLQU_FL_PRE_COMP + S089ImpbVis662014.Len.S089_IMPB_VIS662014 + S089ImpstVis662014.Len.S089_IMPST_VIS662014 + S089ImpbIs662014.Len.S089_IMPB_IS662014 + S089ImpstSost662014.Len.S089_IMPST_SOST662014 + S089PcAbbTs662014.Len.S089_PC_ABB_TS662014 + S089ImpLrdCalcCp.Len.S089_IMP_LRD_CALC_CP + S089CosTunnelUscita.Len.S089_COS_TUNNEL_USCITA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_ID_LIQ = 9;
            public static final int WLQU_ID_OGG = 9;
            public static final int WLQU_ID_MOVI_CRZ = 9;
            public static final int WLQU_DT_INI_EFF = 8;
            public static final int WLQU_DT_END_EFF = 8;
            public static final int WLQU_COD_COMP_ANIA = 5;
            public static final int WLQU_DS_RIGA = 10;
            public static final int WLQU_DS_VER = 9;
            public static final int WLQU_DS_TS_INI_CPTZ = 18;
            public static final int WLQU_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
