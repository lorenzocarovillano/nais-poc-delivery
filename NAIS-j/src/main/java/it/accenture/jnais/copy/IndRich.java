package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-RICH<br>
 * Variable: IND-RICH from copybook IDBVRIC2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndRich {

    //==== PROPERTIES ====
    //Original name: IND-RIC-IB-RICH
    private short ibRich = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-TS-EFF-ESEC-RICH
    private short tsEffEsecRich = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-ID-OGG
    private short idOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-TP-OGG
    private short tpOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-IB-POLI
    private short ibPoli = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-IB-ADES
    private short ibAdes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-IB-GAR
    private short ibGar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-IB-TRCH-DI-GAR
    private short ibTrchDiGar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-ID-BATCH
    private short idBatch = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-ID-JOB
    private short idJob = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-FL-SIMULAZIONE
    private short flSimulazione = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-KEY-ORDINAMENTO
    private short keyOrdinamento = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-ID-RICH-COLLG
    private short idRichCollg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-TP-RAMO-BILA
    private short tpRamoBila = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-TP-FRM-ASSVA
    private short tpFrmAssva = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-TP-CALC-RIS
    private short tpCalcRis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIC-RAMO-BILA
    private short ramoBila = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIbRich(short ibRich) {
        this.ibRich = ibRich;
    }

    public short getIbRich() {
        return this.ibRich;
    }

    public void setTsEffEsecRich(short tsEffEsecRich) {
        this.tsEffEsecRich = tsEffEsecRich;
    }

    public short getTsEffEsecRich() {
        return this.tsEffEsecRich;
    }

    public void setIdOgg(short idOgg) {
        this.idOgg = idOgg;
    }

    public short getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(short tpOgg) {
        this.tpOgg = tpOgg;
    }

    public short getTpOgg() {
        return this.tpOgg;
    }

    public void setIbPoli(short ibPoli) {
        this.ibPoli = ibPoli;
    }

    public short getIbPoli() {
        return this.ibPoli;
    }

    public void setIbAdes(short ibAdes) {
        this.ibAdes = ibAdes;
    }

    public short getIbAdes() {
        return this.ibAdes;
    }

    public void setIbGar(short ibGar) {
        this.ibGar = ibGar;
    }

    public short getIbGar() {
        return this.ibGar;
    }

    public void setIbTrchDiGar(short ibTrchDiGar) {
        this.ibTrchDiGar = ibTrchDiGar;
    }

    public short getIbTrchDiGar() {
        return this.ibTrchDiGar;
    }

    public void setIdBatch(short idBatch) {
        this.idBatch = idBatch;
    }

    public short getIdBatch() {
        return this.idBatch;
    }

    public void setIdJob(short idJob) {
        this.idJob = idJob;
    }

    public short getIdJob() {
        return this.idJob;
    }

    public void setFlSimulazione(short flSimulazione) {
        this.flSimulazione = flSimulazione;
    }

    public short getFlSimulazione() {
        return this.flSimulazione;
    }

    public void setKeyOrdinamento(short keyOrdinamento) {
        this.keyOrdinamento = keyOrdinamento;
    }

    public short getKeyOrdinamento() {
        return this.keyOrdinamento;
    }

    public void setIdRichCollg(short idRichCollg) {
        this.idRichCollg = idRichCollg;
    }

    public short getIdRichCollg() {
        return this.idRichCollg;
    }

    public void setTpRamoBila(short tpRamoBila) {
        this.tpRamoBila = tpRamoBila;
    }

    public short getTpRamoBila() {
        return this.tpRamoBila;
    }

    public void setTpFrmAssva(short tpFrmAssva) {
        this.tpFrmAssva = tpFrmAssva;
    }

    public short getTpFrmAssva() {
        return this.tpFrmAssva;
    }

    public void setTpCalcRis(short tpCalcRis) {
        this.tpCalcRis = tpCalcRis;
    }

    public short getTpCalcRis() {
        return this.tpCalcRis;
    }

    public void setRamoBila(short ramoBila) {
        this.ramoBila = ramoBila;
    }

    public short getRamoBila() {
        return this.ramoBila;
    }
}
