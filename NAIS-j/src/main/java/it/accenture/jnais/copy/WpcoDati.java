package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpcoAaUti;
import it.accenture.jnais.ws.redefines.WpcoArrotPre;
import it.accenture.jnais.ws.redefines.WpcoDtCont;
import it.accenture.jnais.ws.redefines.WpcoDtEstrAssMag70a;
import it.accenture.jnais.ws.redefines.WpcoDtEstrAssMin70a;
import it.accenture.jnais.ws.redefines.WpcoDtRiatRiassComm;
import it.accenture.jnais.ws.redefines.WpcoDtRiatRiassRsh;
import it.accenture.jnais.ws.redefines.WpcoDtRiclRiriasCom;
import it.accenture.jnais.ws.redefines.WpcoDtUltAggErogRe;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollCoriC;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollCoriI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollCotrC;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollCotrI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollEmes;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollEmesI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollLiq;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollPerfC;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollPerfI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollPreC;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollPreI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollQuieC;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollQuieI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRiat;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRiatI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRpCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRpIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRspCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollRspIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollSdI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollSdnlI;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollSnden;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollSndnlq;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollStor;
import it.accenture.jnais.ws.redefines.WpcoDtUltBollStorI;
import it.accenture.jnais.ws.redefines.WpcoDtUltcBnsfdtCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltcBnsfdtIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltcBnsricCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltcBnsricIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltcIsCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltcIsIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltcMarsol;
import it.accenture.jnais.ws.redefines.WpcoDtUltcPildiAaC;
import it.accenture.jnais.ws.redefines.WpcoDtUltcPildiAaI;
import it.accenture.jnais.ws.redefines.WpcoDtUltcPildiMmC;
import it.accenture.jnais.ws.redefines.WpcoDtUltcPildiMmI;
import it.accenture.jnais.ws.redefines.WpcoDtUltcPildiTrI;
import it.accenture.jnais.ws.redefines.WpcoDtUltcRbCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltcRbIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcIlColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcIlInd;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcMrmColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcMrmInd;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcRivColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcRivInd;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcTcmColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcTcmInd;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcUlColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltEcUlInd;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabAt92C;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabAt92I;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabAt93C;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabAt93I;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabCommef;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabCosAt;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabCosSt;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabLiqmef;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabPaspas;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabPrAut;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabPrCon;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabPrlcos;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabRedpro;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabSpeIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltElabTakeP;
import it.accenture.jnais.ws.redefines.WpcoDtUltelriscparPr;
import it.accenture.jnais.ws.redefines.WpcoDtUltEstrazFug;
import it.accenture.jnais.ws.redefines.WpcoDtUltEstrDecCo;
import it.accenture.jnais.ws.redefines.WpcoDtUltgzCed;
import it.accenture.jnais.ws.redefines.WpcoDtUltgzCedColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltgzTrchECl;
import it.accenture.jnais.ws.redefines.WpcoDtUltgzTrchEIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltQtzoCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltQtzoIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltRiclPre;
import it.accenture.jnais.ws.redefines.WpcoDtUltRiclRiass;
import it.accenture.jnais.ws.redefines.WpcoDtUltRinnColl;
import it.accenture.jnais.ws.redefines.WpcoDtUltRinnGarac;
import it.accenture.jnais.ws.redefines.WpcoDtUltRinnTac;
import it.accenture.jnais.ws.redefines.WpcoDtUltRivalCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltRivalIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltscElabCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltscElabIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltscOpzCl;
import it.accenture.jnais.ws.redefines.WpcoDtUltscOpzIn;
import it.accenture.jnais.ws.redefines.WpcoDtUltTabulRiass;
import it.accenture.jnais.ws.redefines.WpcoFrqCostiAtt;
import it.accenture.jnais.ws.redefines.WpcoFrqCostiStornati;
import it.accenture.jnais.ws.redefines.WpcoGgIntrRitPag;
import it.accenture.jnais.ws.redefines.WpcoGgMaxRecProv;
import it.accenture.jnais.ws.redefines.WpcoImpAssSociale;
import it.accenture.jnais.ws.redefines.WpcoLimVltr;
import it.accenture.jnais.ws.redefines.WpcoLmCSubrshConIn;
import it.accenture.jnais.ws.redefines.WpcoLmRisConInt;
import it.accenture.jnais.ws.redefines.WpcoNumGgArrIntrPr;
import it.accenture.jnais.ws.redefines.WpcoNumMmCalcMora;
import it.accenture.jnais.ws.redefines.WpcoPcCSubrshMarsol;
import it.accenture.jnais.ws.redefines.WpcoPcGarNoriskMars;
import it.accenture.jnais.ws.redefines.WpcoPcProv1aaAcq;
import it.accenture.jnais.ws.redefines.WpcoPcRidImp1382011;
import it.accenture.jnais.ws.redefines.WpcoPcRidImp662014;
import it.accenture.jnais.ws.redefines.WpcoPcRmMarsol;
import it.accenture.jnais.ws.redefines.WpcoSoglAmlPrePer;
import it.accenture.jnais.ws.redefines.WpcoSoglAmlPreSavR;
import it.accenture.jnais.ws.redefines.WpcoSoglAmlPreUni;
import it.accenture.jnais.ws.redefines.WpcoStstXRegione;

/**Original name: WPCO-DATI<br>
 * Variable: WPCO-DATI from copybook LCCVPCO1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WpcoDati {

    //==== PROPERTIES ====
    //Original name: WPCO-COD-COMP-ANIA
    private int wpcoCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WPCO-COD-TRAT-CIRT
    private String wpcoCodTratCirt = DefaultValues.stringVal(Len.WPCO_COD_TRAT_CIRT);
    //Original name: WPCO-LIM-VLTR
    private WpcoLimVltr wpcoLimVltr = new WpcoLimVltr();
    //Original name: WPCO-TP-RAT-PERF
    private char wpcoTpRatPerf = DefaultValues.CHAR_VAL;
    //Original name: WPCO-TP-LIV-GENZ-TIT
    private String wpcoTpLivGenzTit = DefaultValues.stringVal(Len.WPCO_TP_LIV_GENZ_TIT);
    //Original name: WPCO-ARROT-PRE
    private WpcoArrotPre wpcoArrotPre = new WpcoArrotPre();
    //Original name: WPCO-DT-CONT
    private WpcoDtCont wpcoDtCont = new WpcoDtCont();
    //Original name: WPCO-DT-ULT-RIVAL-IN
    private WpcoDtUltRivalIn wpcoDtUltRivalIn = new WpcoDtUltRivalIn();
    //Original name: WPCO-DT-ULT-QTZO-IN
    private WpcoDtUltQtzoIn wpcoDtUltQtzoIn = new WpcoDtUltQtzoIn();
    //Original name: WPCO-DT-ULT-RICL-RIASS
    private WpcoDtUltRiclRiass wpcoDtUltRiclRiass = new WpcoDtUltRiclRiass();
    //Original name: WPCO-DT-ULT-TABUL-RIASS
    private WpcoDtUltTabulRiass wpcoDtUltTabulRiass = new WpcoDtUltTabulRiass();
    //Original name: WPCO-DT-ULT-BOLL-EMES
    private WpcoDtUltBollEmes wpcoDtUltBollEmes = new WpcoDtUltBollEmes();
    //Original name: WPCO-DT-ULT-BOLL-STOR
    private WpcoDtUltBollStor wpcoDtUltBollStor = new WpcoDtUltBollStor();
    //Original name: WPCO-DT-ULT-BOLL-LIQ
    private WpcoDtUltBollLiq wpcoDtUltBollLiq = new WpcoDtUltBollLiq();
    //Original name: WPCO-DT-ULT-BOLL-RIAT
    private WpcoDtUltBollRiat wpcoDtUltBollRiat = new WpcoDtUltBollRiat();
    //Original name: WPCO-DT-ULTELRISCPAR-PR
    private WpcoDtUltelriscparPr wpcoDtUltelriscparPr = new WpcoDtUltelriscparPr();
    //Original name: WPCO-DT-ULTC-IS-IN
    private WpcoDtUltcIsIn wpcoDtUltcIsIn = new WpcoDtUltcIsIn();
    //Original name: WPCO-DT-ULT-RICL-PRE
    private WpcoDtUltRiclPre wpcoDtUltRiclPre = new WpcoDtUltRiclPre();
    //Original name: WPCO-DT-ULTC-MARSOL
    private WpcoDtUltcMarsol wpcoDtUltcMarsol = new WpcoDtUltcMarsol();
    //Original name: WPCO-DT-ULTC-RB-IN
    private WpcoDtUltcRbIn wpcoDtUltcRbIn = new WpcoDtUltcRbIn();
    //Original name: WPCO-PC-PROV-1AA-ACQ
    private WpcoPcProv1aaAcq wpcoPcProv1aaAcq = new WpcoPcProv1aaAcq();
    //Original name: WPCO-MOD-INTR-PREST
    private String wpcoModIntrPrest = DefaultValues.stringVal(Len.WPCO_MOD_INTR_PREST);
    //Original name: WPCO-GG-MAX-REC-PROV
    private WpcoGgMaxRecProv wpcoGgMaxRecProv = new WpcoGgMaxRecProv();
    //Original name: WPCO-DT-ULTGZ-TRCH-E-IN
    private WpcoDtUltgzTrchEIn wpcoDtUltgzTrchEIn = new WpcoDtUltgzTrchEIn();
    //Original name: WPCO-DT-ULT-BOLL-SNDEN
    private WpcoDtUltBollSnden wpcoDtUltBollSnden = new WpcoDtUltBollSnden();
    //Original name: WPCO-DT-ULT-BOLL-SNDNLQ
    private WpcoDtUltBollSndnlq wpcoDtUltBollSndnlq = new WpcoDtUltBollSndnlq();
    //Original name: WPCO-DT-ULTSC-ELAB-IN
    private WpcoDtUltscElabIn wpcoDtUltscElabIn = new WpcoDtUltscElabIn();
    //Original name: WPCO-DT-ULTSC-OPZ-IN
    private WpcoDtUltscOpzIn wpcoDtUltscOpzIn = new WpcoDtUltscOpzIn();
    //Original name: WPCO-DT-ULTC-BNSRIC-IN
    private WpcoDtUltcBnsricIn wpcoDtUltcBnsricIn = new WpcoDtUltcBnsricIn();
    //Original name: WPCO-DT-ULTC-BNSFDT-IN
    private WpcoDtUltcBnsfdtIn wpcoDtUltcBnsfdtIn = new WpcoDtUltcBnsfdtIn();
    //Original name: WPCO-DT-ULT-RINN-GARAC
    private WpcoDtUltRinnGarac wpcoDtUltRinnGarac = new WpcoDtUltRinnGarac();
    //Original name: WPCO-DT-ULTGZ-CED
    private WpcoDtUltgzCed wpcoDtUltgzCed = new WpcoDtUltgzCed();
    //Original name: WPCO-DT-ULT-ELAB-PRLCOS
    private WpcoDtUltElabPrlcos wpcoDtUltElabPrlcos = new WpcoDtUltElabPrlcos();
    //Original name: WPCO-DT-ULT-RINN-COLL
    private WpcoDtUltRinnColl wpcoDtUltRinnColl = new WpcoDtUltRinnColl();
    //Original name: WPCO-FL-RVC-PERF
    private char wpcoFlRvcPerf = DefaultValues.CHAR_VAL;
    //Original name: WPCO-FL-RCS-POLI-NOPERF
    private char wpcoFlRcsPoliNoperf = DefaultValues.CHAR_VAL;
    //Original name: WPCO-FL-GEST-PLUSV
    private char wpcoFlGestPlusv = DefaultValues.CHAR_VAL;
    //Original name: WPCO-DT-ULT-RIVAL-CL
    private WpcoDtUltRivalCl wpcoDtUltRivalCl = new WpcoDtUltRivalCl();
    //Original name: WPCO-DT-ULT-QTZO-CL
    private WpcoDtUltQtzoCl wpcoDtUltQtzoCl = new WpcoDtUltQtzoCl();
    //Original name: WPCO-DT-ULTC-BNSRIC-CL
    private WpcoDtUltcBnsricCl wpcoDtUltcBnsricCl = new WpcoDtUltcBnsricCl();
    //Original name: WPCO-DT-ULTC-BNSFDT-CL
    private WpcoDtUltcBnsfdtCl wpcoDtUltcBnsfdtCl = new WpcoDtUltcBnsfdtCl();
    //Original name: WPCO-DT-ULTC-IS-CL
    private WpcoDtUltcIsCl wpcoDtUltcIsCl = new WpcoDtUltcIsCl();
    //Original name: WPCO-DT-ULTC-RB-CL
    private WpcoDtUltcRbCl wpcoDtUltcRbCl = new WpcoDtUltcRbCl();
    //Original name: WPCO-DT-ULTGZ-TRCH-E-CL
    private WpcoDtUltgzTrchECl wpcoDtUltgzTrchECl = new WpcoDtUltgzTrchECl();
    //Original name: WPCO-DT-ULTSC-ELAB-CL
    private WpcoDtUltscElabCl wpcoDtUltscElabCl = new WpcoDtUltscElabCl();
    //Original name: WPCO-DT-ULTSC-OPZ-CL
    private WpcoDtUltscOpzCl wpcoDtUltscOpzCl = new WpcoDtUltscOpzCl();
    //Original name: WPCO-STST-X-REGIONE
    private WpcoStstXRegione wpcoStstXRegione = new WpcoStstXRegione();
    //Original name: WPCO-DT-ULTGZ-CED-COLL
    private WpcoDtUltgzCedColl wpcoDtUltgzCedColl = new WpcoDtUltgzCedColl();
    //Original name: WPCO-TP-MOD-RIVAL
    private String wpcoTpModRival = DefaultValues.stringVal(Len.WPCO_TP_MOD_RIVAL);
    //Original name: WPCO-NUM-MM-CALC-MORA
    private WpcoNumMmCalcMora wpcoNumMmCalcMora = new WpcoNumMmCalcMora();
    //Original name: WPCO-DT-ULT-EC-RIV-COLL
    private WpcoDtUltEcRivColl wpcoDtUltEcRivColl = new WpcoDtUltEcRivColl();
    //Original name: WPCO-DT-ULT-EC-RIV-IND
    private WpcoDtUltEcRivInd wpcoDtUltEcRivInd = new WpcoDtUltEcRivInd();
    //Original name: WPCO-DT-ULT-EC-IL-COLL
    private WpcoDtUltEcIlColl wpcoDtUltEcIlColl = new WpcoDtUltEcIlColl();
    //Original name: WPCO-DT-ULT-EC-IL-IND
    private WpcoDtUltEcIlInd wpcoDtUltEcIlInd = new WpcoDtUltEcIlInd();
    //Original name: WPCO-DT-ULT-EC-UL-COLL
    private WpcoDtUltEcUlColl wpcoDtUltEcUlColl = new WpcoDtUltEcUlColl();
    //Original name: WPCO-DT-ULT-EC-UL-IND
    private WpcoDtUltEcUlInd wpcoDtUltEcUlInd = new WpcoDtUltEcUlInd();
    //Original name: WPCO-AA-UTI
    private WpcoAaUti wpcoAaUti = new WpcoAaUti();
    //Original name: WPCO-CALC-RSH-COMUN
    private char wpcoCalcRshComun = DefaultValues.CHAR_VAL;
    //Original name: WPCO-FL-LIV-DEBUG
    private short wpcoFlLivDebug = DefaultValues.SHORT_VAL;
    //Original name: WPCO-DT-ULT-BOLL-PERF-C
    private WpcoDtUltBollPerfC wpcoDtUltBollPerfC = new WpcoDtUltBollPerfC();
    //Original name: WPCO-DT-ULT-BOLL-RSP-IN
    private WpcoDtUltBollRspIn wpcoDtUltBollRspIn = new WpcoDtUltBollRspIn();
    //Original name: WPCO-DT-ULT-BOLL-RSP-CL
    private WpcoDtUltBollRspCl wpcoDtUltBollRspCl = new WpcoDtUltBollRspCl();
    //Original name: WPCO-DT-ULT-BOLL-EMES-I
    private WpcoDtUltBollEmesI wpcoDtUltBollEmesI = new WpcoDtUltBollEmesI();
    //Original name: WPCO-DT-ULT-BOLL-STOR-I
    private WpcoDtUltBollStorI wpcoDtUltBollStorI = new WpcoDtUltBollStorI();
    //Original name: WPCO-DT-ULT-BOLL-RIAT-I
    private WpcoDtUltBollRiatI wpcoDtUltBollRiatI = new WpcoDtUltBollRiatI();
    //Original name: WPCO-DT-ULT-BOLL-SD-I
    private WpcoDtUltBollSdI wpcoDtUltBollSdI = new WpcoDtUltBollSdI();
    //Original name: WPCO-DT-ULT-BOLL-SDNL-I
    private WpcoDtUltBollSdnlI wpcoDtUltBollSdnlI = new WpcoDtUltBollSdnlI();
    //Original name: WPCO-DT-ULT-BOLL-PERF-I
    private WpcoDtUltBollPerfI wpcoDtUltBollPerfI = new WpcoDtUltBollPerfI();
    //Original name: WPCO-DT-RICL-RIRIAS-COM
    private WpcoDtRiclRiriasCom wpcoDtRiclRiriasCom = new WpcoDtRiclRiriasCom();
    //Original name: WPCO-DT-ULT-ELAB-AT92-C
    private WpcoDtUltElabAt92C wpcoDtUltElabAt92C = new WpcoDtUltElabAt92C();
    //Original name: WPCO-DT-ULT-ELAB-AT92-I
    private WpcoDtUltElabAt92I wpcoDtUltElabAt92I = new WpcoDtUltElabAt92I();
    //Original name: WPCO-DT-ULT-ELAB-AT93-C
    private WpcoDtUltElabAt93C wpcoDtUltElabAt93C = new WpcoDtUltElabAt93C();
    //Original name: WPCO-DT-ULT-ELAB-AT93-I
    private WpcoDtUltElabAt93I wpcoDtUltElabAt93I = new WpcoDtUltElabAt93I();
    //Original name: WPCO-DT-ULT-ELAB-SPE-IN
    private WpcoDtUltElabSpeIn wpcoDtUltElabSpeIn = new WpcoDtUltElabSpeIn();
    //Original name: WPCO-DT-ULT-ELAB-PR-CON
    private WpcoDtUltElabPrCon wpcoDtUltElabPrCon = new WpcoDtUltElabPrCon();
    //Original name: WPCO-DT-ULT-BOLL-RP-CL
    private WpcoDtUltBollRpCl wpcoDtUltBollRpCl = new WpcoDtUltBollRpCl();
    //Original name: WPCO-DT-ULT-BOLL-RP-IN
    private WpcoDtUltBollRpIn wpcoDtUltBollRpIn = new WpcoDtUltBollRpIn();
    //Original name: WPCO-DT-ULT-BOLL-PRE-I
    private WpcoDtUltBollPreI wpcoDtUltBollPreI = new WpcoDtUltBollPreI();
    //Original name: WPCO-DT-ULT-BOLL-PRE-C
    private WpcoDtUltBollPreC wpcoDtUltBollPreC = new WpcoDtUltBollPreC();
    //Original name: WPCO-DT-ULTC-PILDI-MM-C
    private WpcoDtUltcPildiMmC wpcoDtUltcPildiMmC = new WpcoDtUltcPildiMmC();
    //Original name: WPCO-DT-ULTC-PILDI-AA-C
    private WpcoDtUltcPildiAaC wpcoDtUltcPildiAaC = new WpcoDtUltcPildiAaC();
    //Original name: WPCO-DT-ULTC-PILDI-MM-I
    private WpcoDtUltcPildiMmI wpcoDtUltcPildiMmI = new WpcoDtUltcPildiMmI();
    //Original name: WPCO-DT-ULTC-PILDI-TR-I
    private WpcoDtUltcPildiTrI wpcoDtUltcPildiTrI = new WpcoDtUltcPildiTrI();
    //Original name: WPCO-DT-ULTC-PILDI-AA-I
    private WpcoDtUltcPildiAaI wpcoDtUltcPildiAaI = new WpcoDtUltcPildiAaI();
    //Original name: WPCO-DT-ULT-BOLL-QUIE-C
    private WpcoDtUltBollQuieC wpcoDtUltBollQuieC = new WpcoDtUltBollQuieC();
    //Original name: WPCO-DT-ULT-BOLL-QUIE-I
    private WpcoDtUltBollQuieI wpcoDtUltBollQuieI = new WpcoDtUltBollQuieI();
    //Original name: WPCO-DT-ULT-BOLL-COTR-I
    private WpcoDtUltBollCotrI wpcoDtUltBollCotrI = new WpcoDtUltBollCotrI();
    //Original name: WPCO-DT-ULT-BOLL-COTR-C
    private WpcoDtUltBollCotrC wpcoDtUltBollCotrC = new WpcoDtUltBollCotrC();
    //Original name: WPCO-DT-ULT-BOLL-CORI-C
    private WpcoDtUltBollCoriC wpcoDtUltBollCoriC = new WpcoDtUltBollCoriC();
    //Original name: WPCO-DT-ULT-BOLL-CORI-I
    private WpcoDtUltBollCoriI wpcoDtUltBollCoriI = new WpcoDtUltBollCoriI();
    //Original name: WPCO-DS-OPER-SQL
    private char wpcoDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WPCO-DS-VER
    private int wpcoDsVer = DefaultValues.INT_VAL;
    //Original name: WPCO-DS-TS-CPTZ
    private long wpcoDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WPCO-DS-UTENTE
    private String wpcoDsUtente = DefaultValues.stringVal(Len.WPCO_DS_UTENTE);
    //Original name: WPCO-DS-STATO-ELAB
    private char wpcoDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WPCO-TP-VALZZ-DT-VLT
    private String wpcoTpValzzDtVlt = DefaultValues.stringVal(Len.WPCO_TP_VALZZ_DT_VLT);
    //Original name: WPCO-FL-FRAZ-PROV-ACQ
    private char wpcoFlFrazProvAcq = DefaultValues.CHAR_VAL;
    //Original name: WPCO-DT-ULT-AGG-EROG-RE
    private WpcoDtUltAggErogRe wpcoDtUltAggErogRe = new WpcoDtUltAggErogRe();
    //Original name: WPCO-PC-RM-MARSOL
    private WpcoPcRmMarsol wpcoPcRmMarsol = new WpcoPcRmMarsol();
    //Original name: WPCO-PC-C-SUBRSH-MARSOL
    private WpcoPcCSubrshMarsol wpcoPcCSubrshMarsol = new WpcoPcCSubrshMarsol();
    //Original name: WPCO-COD-COMP-ISVAP
    private String wpcoCodCompIsvap = DefaultValues.stringVal(Len.WPCO_COD_COMP_ISVAP);
    //Original name: WPCO-LM-RIS-CON-INT
    private WpcoLmRisConInt wpcoLmRisConInt = new WpcoLmRisConInt();
    //Original name: WPCO-LM-C-SUBRSH-CON-IN
    private WpcoLmCSubrshConIn wpcoLmCSubrshConIn = new WpcoLmCSubrshConIn();
    //Original name: WPCO-PC-GAR-NORISK-MARS
    private WpcoPcGarNoriskMars wpcoPcGarNoriskMars = new WpcoPcGarNoriskMars();
    //Original name: WPCO-CRZ-1A-RAT-INTR-PR
    private char wpcoCrz1aRatIntrPr = DefaultValues.CHAR_VAL;
    //Original name: WPCO-NUM-GG-ARR-INTR-PR
    private WpcoNumGgArrIntrPr wpcoNumGgArrIntrPr = new WpcoNumGgArrIntrPr();
    //Original name: WPCO-FL-VISUAL-VINPG
    private char wpcoFlVisualVinpg = DefaultValues.CHAR_VAL;
    //Original name: WPCO-DT-ULT-ESTRAZ-FUG
    private WpcoDtUltEstrazFug wpcoDtUltEstrazFug = new WpcoDtUltEstrazFug();
    //Original name: WPCO-DT-ULT-ELAB-PR-AUT
    private WpcoDtUltElabPrAut wpcoDtUltElabPrAut = new WpcoDtUltElabPrAut();
    //Original name: WPCO-DT-ULT-ELAB-COMMEF
    private WpcoDtUltElabCommef wpcoDtUltElabCommef = new WpcoDtUltElabCommef();
    //Original name: WPCO-DT-ULT-ELAB-LIQMEF
    private WpcoDtUltElabLiqmef wpcoDtUltElabLiqmef = new WpcoDtUltElabLiqmef();
    //Original name: WPCO-COD-FISC-MEF
    private String wpcoCodFiscMef = DefaultValues.stringVal(Len.WPCO_COD_FISC_MEF);
    //Original name: WPCO-IMP-ASS-SOCIALE
    private WpcoImpAssSociale wpcoImpAssSociale = new WpcoImpAssSociale();
    //Original name: WPCO-MOD-COMNZ-INVST-SW
    private char wpcoModComnzInvstSw = DefaultValues.CHAR_VAL;
    //Original name: WPCO-DT-RIAT-RIASS-RSH
    private WpcoDtRiatRiassRsh wpcoDtRiatRiassRsh = new WpcoDtRiatRiassRsh();
    //Original name: WPCO-DT-RIAT-RIASS-COMM
    private WpcoDtRiatRiassComm wpcoDtRiatRiassComm = new WpcoDtRiatRiassComm();
    //Original name: WPCO-GG-INTR-RIT-PAG
    private WpcoGgIntrRitPag wpcoGgIntrRitPag = new WpcoGgIntrRitPag();
    //Original name: WPCO-DT-ULT-RINN-TAC
    private WpcoDtUltRinnTac wpcoDtUltRinnTac = new WpcoDtUltRinnTac();
    //Original name: WPCO-DESC-COMP-LEN
    private short wpcoDescCompLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPCO-DESC-COMP
    private String wpcoDescComp = DefaultValues.stringVal(Len.WPCO_DESC_COMP);
    //Original name: WPCO-DT-ULT-EC-TCM-IND
    private WpcoDtUltEcTcmInd wpcoDtUltEcTcmInd = new WpcoDtUltEcTcmInd();
    //Original name: WPCO-DT-ULT-EC-TCM-COLL
    private WpcoDtUltEcTcmColl wpcoDtUltEcTcmColl = new WpcoDtUltEcTcmColl();
    //Original name: WPCO-DT-ULT-EC-MRM-IND
    private WpcoDtUltEcMrmInd wpcoDtUltEcMrmInd = new WpcoDtUltEcMrmInd();
    //Original name: WPCO-DT-ULT-EC-MRM-COLL
    private WpcoDtUltEcMrmColl wpcoDtUltEcMrmColl = new WpcoDtUltEcMrmColl();
    //Original name: WPCO-COD-COMP-LDAP
    private String wpcoCodCompLdap = DefaultValues.stringVal(Len.WPCO_COD_COMP_LDAP);
    //Original name: WPCO-PC-RID-IMP-1382011
    private WpcoPcRidImp1382011 wpcoPcRidImp1382011 = new WpcoPcRidImp1382011();
    //Original name: WPCO-PC-RID-IMP-662014
    private WpcoPcRidImp662014 wpcoPcRidImp662014 = new WpcoPcRidImp662014();
    //Original name: WPCO-SOGL-AML-PRE-UNI
    private WpcoSoglAmlPreUni wpcoSoglAmlPreUni = new WpcoSoglAmlPreUni();
    //Original name: WPCO-SOGL-AML-PRE-PER
    private WpcoSoglAmlPrePer wpcoSoglAmlPrePer = new WpcoSoglAmlPrePer();
    //Original name: WPCO-COD-SOGG-FTZ-ASSTO
    private String wpcoCodSoggFtzAssto = DefaultValues.stringVal(Len.WPCO_COD_SOGG_FTZ_ASSTO);
    //Original name: WPCO-DT-ULT-ELAB-REDPRO
    private WpcoDtUltElabRedpro wpcoDtUltElabRedpro = new WpcoDtUltElabRedpro();
    //Original name: WPCO-DT-ULT-ELAB-TAKE-P
    private WpcoDtUltElabTakeP wpcoDtUltElabTakeP = new WpcoDtUltElabTakeP();
    //Original name: WPCO-DT-ULT-ELAB-PASPAS
    private WpcoDtUltElabPaspas wpcoDtUltElabPaspas = new WpcoDtUltElabPaspas();
    //Original name: WPCO-SOGL-AML-PRE-SAV-R
    private WpcoSoglAmlPreSavR wpcoSoglAmlPreSavR = new WpcoSoglAmlPreSavR();
    //Original name: WPCO-DT-ULT-ESTR-DEC-CO
    private WpcoDtUltEstrDecCo wpcoDtUltEstrDecCo = new WpcoDtUltEstrDecCo();
    //Original name: WPCO-DT-ULT-ELAB-COS-AT
    private WpcoDtUltElabCosAt wpcoDtUltElabCosAt = new WpcoDtUltElabCosAt();
    //Original name: WPCO-FRQ-COSTI-ATT
    private WpcoFrqCostiAtt wpcoFrqCostiAtt = new WpcoFrqCostiAtt();
    //Original name: WPCO-DT-ULT-ELAB-COS-ST
    private WpcoDtUltElabCosSt wpcoDtUltElabCosSt = new WpcoDtUltElabCosSt();
    //Original name: WPCO-FRQ-COSTI-STORNATI
    private WpcoFrqCostiStornati wpcoFrqCostiStornati = new WpcoFrqCostiStornati();
    //Original name: WPCO-DT-ESTR-ASS-MIN70A
    private WpcoDtEstrAssMin70a wpcoDtEstrAssMin70a = new WpcoDtEstrAssMin70a();
    //Original name: WPCO-DT-ESTR-ASS-MAG70A
    private WpcoDtEstrAssMag70a wpcoDtEstrAssMag70a = new WpcoDtEstrAssMag70a();

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wpcoCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPCO_COD_COMP_ANIA, 0);
        position += Len.WPCO_COD_COMP_ANIA;
        wpcoCodTratCirt = MarshalByte.readString(buffer, position, Len.WPCO_COD_TRAT_CIRT);
        position += Len.WPCO_COD_TRAT_CIRT;
        wpcoLimVltr.setDpcoLimVltrFromBuffer(buffer, position);
        position += WpcoLimVltr.Len.WPCO_LIM_VLTR;
        wpcoTpRatPerf = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpcoTpLivGenzTit = MarshalByte.readString(buffer, position, Len.WPCO_TP_LIV_GENZ_TIT);
        position += Len.WPCO_TP_LIV_GENZ_TIT;
        wpcoArrotPre.setDpcoArrotPreFromBuffer(buffer, position);
        position += WpcoArrotPre.Len.WPCO_ARROT_PRE;
        wpcoDtCont.setDpcoDtContFromBuffer(buffer, position);
        position += WpcoDtCont.Len.WPCO_DT_CONT;
        wpcoDtUltRivalIn.setDpcoDtUltRivalInFromBuffer(buffer, position);
        position += WpcoDtUltRivalIn.Len.WPCO_DT_ULT_RIVAL_IN;
        wpcoDtUltQtzoIn.setDpcoDtUltQtzoInFromBuffer(buffer, position);
        position += WpcoDtUltQtzoIn.Len.WPCO_DT_ULT_QTZO_IN;
        wpcoDtUltRiclRiass.setDpcoDtUltRiclRiassFromBuffer(buffer, position);
        position += WpcoDtUltRiclRiass.Len.WPCO_DT_ULT_RICL_RIASS;
        wpcoDtUltTabulRiass.setDpcoDtUltTabulRiassFromBuffer(buffer, position);
        position += WpcoDtUltTabulRiass.Len.WPCO_DT_ULT_TABUL_RIASS;
        wpcoDtUltBollEmes.setDpcoDtUltBollEmesFromBuffer(buffer, position);
        position += WpcoDtUltBollEmes.Len.WPCO_DT_ULT_BOLL_EMES;
        wpcoDtUltBollStor.setDpcoDtUltBollStorFromBuffer(buffer, position);
        position += WpcoDtUltBollStor.Len.WPCO_DT_ULT_BOLL_STOR;
        wpcoDtUltBollLiq.setDpcoDtUltBollLiqFromBuffer(buffer, position);
        position += WpcoDtUltBollLiq.Len.WPCO_DT_ULT_BOLL_LIQ;
        wpcoDtUltBollRiat.setDpcoDtUltBollRiatFromBuffer(buffer, position);
        position += WpcoDtUltBollRiat.Len.WPCO_DT_ULT_BOLL_RIAT;
        wpcoDtUltelriscparPr.setDpcoDtUltelriscparPrFromBuffer(buffer, position);
        position += WpcoDtUltelriscparPr.Len.WPCO_DT_ULTELRISCPAR_PR;
        wpcoDtUltcIsIn.setDpcoDtUltcIsInFromBuffer(buffer, position);
        position += WpcoDtUltcIsIn.Len.WPCO_DT_ULTC_IS_IN;
        wpcoDtUltRiclPre.setDpcoDtUltRiclPreFromBuffer(buffer, position);
        position += WpcoDtUltRiclPre.Len.WPCO_DT_ULT_RICL_PRE;
        wpcoDtUltcMarsol.setDpcoDtUltcMarsolFromBuffer(buffer, position);
        position += WpcoDtUltcMarsol.Len.WPCO_DT_ULTC_MARSOL;
        wpcoDtUltcRbIn.setDpcoDtUltcRbInFromBuffer(buffer, position);
        position += WpcoDtUltcRbIn.Len.WPCO_DT_ULTC_RB_IN;
        wpcoPcProv1aaAcq.setDpcoPcProv1aaAcqFromBuffer(buffer, position);
        position += WpcoPcProv1aaAcq.Len.WPCO_PC_PROV1AA_ACQ;
        wpcoModIntrPrest = MarshalByte.readString(buffer, position, Len.WPCO_MOD_INTR_PREST);
        position += Len.WPCO_MOD_INTR_PREST;
        wpcoGgMaxRecProv.setDpcoGgMaxRecProvFromBuffer(buffer, position);
        position += WpcoGgMaxRecProv.Len.WPCO_GG_MAX_REC_PROV;
        wpcoDtUltgzTrchEIn.setDpcoDtUltgzTrchEInFromBuffer(buffer, position);
        position += WpcoDtUltgzTrchEIn.Len.WPCO_DT_ULTGZ_TRCH_E_IN;
        wpcoDtUltBollSnden.setDpcoDtUltBollSndenFromBuffer(buffer, position);
        position += WpcoDtUltBollSnden.Len.WPCO_DT_ULT_BOLL_SNDEN;
        wpcoDtUltBollSndnlq.setDpcoDtUltBollSndnlqFromBuffer(buffer, position);
        position += WpcoDtUltBollSndnlq.Len.WPCO_DT_ULT_BOLL_SNDNLQ;
        wpcoDtUltscElabIn.setDpcoDtUltscElabInFromBuffer(buffer, position);
        position += WpcoDtUltscElabIn.Len.WPCO_DT_ULTSC_ELAB_IN;
        wpcoDtUltscOpzIn.setDpcoDtUltscOpzInFromBuffer(buffer, position);
        position += WpcoDtUltscOpzIn.Len.WPCO_DT_ULTSC_OPZ_IN;
        wpcoDtUltcBnsricIn.setDpcoDtUltcBnsricInFromBuffer(buffer, position);
        position += WpcoDtUltcBnsricIn.Len.WPCO_DT_ULTC_BNSRIC_IN;
        wpcoDtUltcBnsfdtIn.setDpcoDtUltcBnsfdtInFromBuffer(buffer, position);
        position += WpcoDtUltcBnsfdtIn.Len.WPCO_DT_ULTC_BNSFDT_IN;
        wpcoDtUltRinnGarac.setDpcoDtUltRinnGaracFromBuffer(buffer, position);
        position += WpcoDtUltRinnGarac.Len.WPCO_DT_ULT_RINN_GARAC;
        wpcoDtUltgzCed.setDpcoDtUltgzCedFromBuffer(buffer, position);
        position += WpcoDtUltgzCed.Len.WPCO_DT_ULTGZ_CED;
        wpcoDtUltElabPrlcos.setDpcoDtUltElabPrlcosFromBuffer(buffer, position);
        position += WpcoDtUltElabPrlcos.Len.WPCO_DT_ULT_ELAB_PRLCOS;
        wpcoDtUltRinnColl.setDpcoDtUltRinnCollFromBuffer(buffer, position);
        position += WpcoDtUltRinnColl.Len.WPCO_DT_ULT_RINN_COLL;
        wpcoFlRvcPerf = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpcoFlRcsPoliNoperf = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpcoFlGestPlusv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpcoDtUltRivalCl.setDpcoDtUltRivalClFromBuffer(buffer, position);
        position += WpcoDtUltRivalCl.Len.WPCO_DT_ULT_RIVAL_CL;
        wpcoDtUltQtzoCl.setDpcoDtUltQtzoClFromBuffer(buffer, position);
        position += WpcoDtUltQtzoCl.Len.WPCO_DT_ULT_QTZO_CL;
        wpcoDtUltcBnsricCl.setDpcoDtUltcBnsricClFromBuffer(buffer, position);
        position += WpcoDtUltcBnsricCl.Len.WPCO_DT_ULTC_BNSRIC_CL;
        wpcoDtUltcBnsfdtCl.setDpcoDtUltcBnsfdtClFromBuffer(buffer, position);
        position += WpcoDtUltcBnsfdtCl.Len.WPCO_DT_ULTC_BNSFDT_CL;
        wpcoDtUltcIsCl.setDpcoDtUltcIsClFromBuffer(buffer, position);
        position += WpcoDtUltcIsCl.Len.WPCO_DT_ULTC_IS_CL;
        wpcoDtUltcRbCl.setDpcoDtUltcRbClFromBuffer(buffer, position);
        position += WpcoDtUltcRbCl.Len.WPCO_DT_ULTC_RB_CL;
        wpcoDtUltgzTrchECl.setDpcoDtUltgzTrchEClFromBuffer(buffer, position);
        position += WpcoDtUltgzTrchECl.Len.WPCO_DT_ULTGZ_TRCH_E_CL;
        wpcoDtUltscElabCl.setDpcoDtUltscElabClFromBuffer(buffer, position);
        position += WpcoDtUltscElabCl.Len.WPCO_DT_ULTSC_ELAB_CL;
        wpcoDtUltscOpzCl.setDpcoDtUltscOpzClFromBuffer(buffer, position);
        position += WpcoDtUltscOpzCl.Len.WPCO_DT_ULTSC_OPZ_CL;
        wpcoStstXRegione.setDpcoStstXRegioneFromBuffer(buffer, position);
        position += WpcoStstXRegione.Len.WPCO_STST_X_REGIONE;
        wpcoDtUltgzCedColl.setDpcoDtUltgzCedCollFromBuffer(buffer, position);
        position += WpcoDtUltgzCedColl.Len.WPCO_DT_ULTGZ_CED_COLL;
        wpcoTpModRival = MarshalByte.readString(buffer, position, Len.WPCO_TP_MOD_RIVAL);
        position += Len.WPCO_TP_MOD_RIVAL;
        wpcoNumMmCalcMora.setDpcoNumMmCalcMoraFromBuffer(buffer, position);
        position += WpcoNumMmCalcMora.Len.WPCO_NUM_MM_CALC_MORA;
        wpcoDtUltEcRivColl.setDpcoDtUltEcRivCollFromBuffer(buffer, position);
        position += WpcoDtUltEcRivColl.Len.WPCO_DT_ULT_EC_RIV_COLL;
        wpcoDtUltEcRivInd.setDpcoDtUltEcRivIndFromBuffer(buffer, position);
        position += WpcoDtUltEcRivInd.Len.WPCO_DT_ULT_EC_RIV_IND;
        wpcoDtUltEcIlColl.setDpcoDtUltEcIlCollFromBuffer(buffer, position);
        position += WpcoDtUltEcIlColl.Len.WPCO_DT_ULT_EC_IL_COLL;
        wpcoDtUltEcIlInd.setDpcoDtUltEcIlIndFromBuffer(buffer, position);
        position += WpcoDtUltEcIlInd.Len.WPCO_DT_ULT_EC_IL_IND;
        wpcoDtUltEcUlColl.setDpcoDtUltEcUlCollFromBuffer(buffer, position);
        position += WpcoDtUltEcUlColl.Len.WPCO_DT_ULT_EC_UL_COLL;
        wpcoDtUltEcUlInd.setDpcoDtUltEcUlIndFromBuffer(buffer, position);
        position += WpcoDtUltEcUlInd.Len.WPCO_DT_ULT_EC_UL_IND;
        wpcoAaUti.setDpcoAaUtiFromBuffer(buffer, position);
        position += WpcoAaUti.Len.WPCO_AA_UTI;
        wpcoCalcRshComun = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpcoFlLivDebug = MarshalByte.readPackedAsShort(buffer, position, Len.Int.WPCO_FL_LIV_DEBUG, 0);
        position += Len.WPCO_FL_LIV_DEBUG;
        wpcoDtUltBollPerfC.setDpcoDtUltBollPerfCFromBuffer(buffer, position);
        position += WpcoDtUltBollPerfC.Len.WPCO_DT_ULT_BOLL_PERF_C;
        wpcoDtUltBollRspIn.setDpcoDtUltBollRspInFromBuffer(buffer, position);
        position += WpcoDtUltBollRspIn.Len.WPCO_DT_ULT_BOLL_RSP_IN;
        wpcoDtUltBollRspCl.setDpcoDtUltBollRspClFromBuffer(buffer, position);
        position += WpcoDtUltBollRspCl.Len.WPCO_DT_ULT_BOLL_RSP_CL;
        wpcoDtUltBollEmesI.setDpcoDtUltBollEmesIFromBuffer(buffer, position);
        position += WpcoDtUltBollEmesI.Len.WPCO_DT_ULT_BOLL_EMES_I;
        wpcoDtUltBollStorI.setDpcoDtUltBollStorIFromBuffer(buffer, position);
        position += WpcoDtUltBollStorI.Len.WPCO_DT_ULT_BOLL_STOR_I;
        wpcoDtUltBollRiatI.setDpcoDtUltBollRiatIFromBuffer(buffer, position);
        position += WpcoDtUltBollRiatI.Len.WPCO_DT_ULT_BOLL_RIAT_I;
        wpcoDtUltBollSdI.setDpcoDtUltBollSdIFromBuffer(buffer, position);
        position += WpcoDtUltBollSdI.Len.WPCO_DT_ULT_BOLL_SD_I;
        wpcoDtUltBollSdnlI.setDpcoDtUltBollSdnlIFromBuffer(buffer, position);
        position += WpcoDtUltBollSdnlI.Len.WPCO_DT_ULT_BOLL_SDNL_I;
        wpcoDtUltBollPerfI.setDpcoDtUltBollPerfIFromBuffer(buffer, position);
        position += WpcoDtUltBollPerfI.Len.WPCO_DT_ULT_BOLL_PERF_I;
        wpcoDtRiclRiriasCom.setDpcoDtRiclRiriasComFromBuffer(buffer, position);
        position += WpcoDtRiclRiriasCom.Len.WPCO_DT_RICL_RIRIAS_COM;
        wpcoDtUltElabAt92C.setDpcoDtUltElabAt92CFromBuffer(buffer, position);
        position += WpcoDtUltElabAt92C.Len.WPCO_DT_ULT_ELAB_AT92_C;
        wpcoDtUltElabAt92I.setDpcoDtUltElabAt92IFromBuffer(buffer, position);
        position += WpcoDtUltElabAt92I.Len.WPCO_DT_ULT_ELAB_AT92_I;
        wpcoDtUltElabAt93C.setDpcoDtUltElabAt93CFromBuffer(buffer, position);
        position += WpcoDtUltElabAt93C.Len.WPCO_DT_ULT_ELAB_AT93_C;
        wpcoDtUltElabAt93I.setDpcoDtUltElabAt93IFromBuffer(buffer, position);
        position += WpcoDtUltElabAt93I.Len.WPCO_DT_ULT_ELAB_AT93_I;
        wpcoDtUltElabSpeIn.setDpcoDtUltElabSpeInFromBuffer(buffer, position);
        position += WpcoDtUltElabSpeIn.Len.WPCO_DT_ULT_ELAB_SPE_IN;
        wpcoDtUltElabPrCon.setDpcoDtUltElabPrConFromBuffer(buffer, position);
        position += WpcoDtUltElabPrCon.Len.WPCO_DT_ULT_ELAB_PR_CON;
        wpcoDtUltBollRpCl.setDpcoDtUltBollRpClFromBuffer(buffer, position);
        position += WpcoDtUltBollRpCl.Len.WPCO_DT_ULT_BOLL_RP_CL;
        wpcoDtUltBollRpIn.setDpcoDtUltBollRpInFromBuffer(buffer, position);
        position += WpcoDtUltBollRpIn.Len.WPCO_DT_ULT_BOLL_RP_IN;
        wpcoDtUltBollPreI.setDpcoDtUltBollPreIFromBuffer(buffer, position);
        position += WpcoDtUltBollPreI.Len.WPCO_DT_ULT_BOLL_PRE_I;
        wpcoDtUltBollPreC.setDpcoDtUltBollPreCFromBuffer(buffer, position);
        position += WpcoDtUltBollPreC.Len.WPCO_DT_ULT_BOLL_PRE_C;
        wpcoDtUltcPildiMmC.setDpcoDtUltcPildiMmCFromBuffer(buffer, position);
        position += WpcoDtUltcPildiMmC.Len.WPCO_DT_ULTC_PILDI_MM_C;
        wpcoDtUltcPildiAaC.setDpcoDtUltcPildiAaCFromBuffer(buffer, position);
        position += WpcoDtUltcPildiAaC.Len.WPCO_DT_ULTC_PILDI_AA_C;
        wpcoDtUltcPildiMmI.setDpcoDtUltcPildiMmIFromBuffer(buffer, position);
        position += WpcoDtUltcPildiMmI.Len.WPCO_DT_ULTC_PILDI_MM_I;
        wpcoDtUltcPildiTrI.setDpcoDtUltcPildiTrIFromBuffer(buffer, position);
        position += WpcoDtUltcPildiTrI.Len.WPCO_DT_ULTC_PILDI_TR_I;
        wpcoDtUltcPildiAaI.setDpcoDtUltcPildiAaIFromBuffer(buffer, position);
        position += WpcoDtUltcPildiAaI.Len.WPCO_DT_ULTC_PILDI_AA_I;
        wpcoDtUltBollQuieC.setDpcoDtUltBollQuieCFromBuffer(buffer, position);
        position += WpcoDtUltBollQuieC.Len.WPCO_DT_ULT_BOLL_QUIE_C;
        wpcoDtUltBollQuieI.setDpcoDtUltBollQuieIFromBuffer(buffer, position);
        position += WpcoDtUltBollQuieI.Len.WPCO_DT_ULT_BOLL_QUIE_I;
        wpcoDtUltBollCotrI.setDpcoDtUltBollCotrIFromBuffer(buffer, position);
        position += WpcoDtUltBollCotrI.Len.WPCO_DT_ULT_BOLL_COTR_I;
        wpcoDtUltBollCotrC.setDpcoDtUltBollCotrCFromBuffer(buffer, position);
        position += WpcoDtUltBollCotrC.Len.WPCO_DT_ULT_BOLL_COTR_C;
        wpcoDtUltBollCoriC.setDpcoDtUltBollCoriCFromBuffer(buffer, position);
        position += WpcoDtUltBollCoriC.Len.WPCO_DT_ULT_BOLL_CORI_C;
        wpcoDtUltBollCoriI.setDpcoDtUltBollCoriIFromBuffer(buffer, position);
        position += WpcoDtUltBollCoriI.Len.WPCO_DT_ULT_BOLL_CORI_I;
        wpcoDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpcoDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPCO_DS_VER, 0);
        position += Len.WPCO_DS_VER;
        wpcoDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPCO_DS_TS_CPTZ, 0);
        position += Len.WPCO_DS_TS_CPTZ;
        wpcoDsUtente = MarshalByte.readString(buffer, position, Len.WPCO_DS_UTENTE);
        position += Len.WPCO_DS_UTENTE;
        wpcoDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpcoTpValzzDtVlt = MarshalByte.readString(buffer, position, Len.WPCO_TP_VALZZ_DT_VLT);
        position += Len.WPCO_TP_VALZZ_DT_VLT;
        wpcoFlFrazProvAcq = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpcoDtUltAggErogRe.setDpcoDtUltAggErogReFromBuffer(buffer, position);
        position += WpcoDtUltAggErogRe.Len.WPCO_DT_ULT_AGG_EROG_RE;
        wpcoPcRmMarsol.setDpcoPcRmMarsolFromBuffer(buffer, position);
        position += WpcoPcRmMarsol.Len.WPCO_PC_RM_MARSOL;
        wpcoPcCSubrshMarsol.setDpcoPcCSubrshMarsolFromBuffer(buffer, position);
        position += WpcoPcCSubrshMarsol.Len.WPCO_PC_C_SUBRSH_MARSOL;
        wpcoCodCompIsvap = MarshalByte.readString(buffer, position, Len.WPCO_COD_COMP_ISVAP);
        position += Len.WPCO_COD_COMP_ISVAP;
        wpcoLmRisConInt.setDpcoLmRisConIntFromBuffer(buffer, position);
        position += WpcoLmRisConInt.Len.WPCO_LM_RIS_CON_INT;
        wpcoLmCSubrshConIn.setDpcoLmCSubrshConInFromBuffer(buffer, position);
        position += WpcoLmCSubrshConIn.Len.WPCO_LM_C_SUBRSH_CON_IN;
        wpcoPcGarNoriskMars.setDpcoPcGarNoriskMarsFromBuffer(buffer, position);
        position += WpcoPcGarNoriskMars.Len.WPCO_PC_GAR_NORISK_MARS;
        wpcoCrz1aRatIntrPr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpcoNumGgArrIntrPr.setDpcoNumGgArrIntrPrFromBuffer(buffer, position);
        position += WpcoNumGgArrIntrPr.Len.WPCO_NUM_GG_ARR_INTR_PR;
        wpcoFlVisualVinpg = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpcoDtUltEstrazFug.setDpcoDtUltEstrazFugFromBuffer(buffer, position);
        position += WpcoDtUltEstrazFug.Len.WPCO_DT_ULT_ESTRAZ_FUG;
        wpcoDtUltElabPrAut.setDpcoDtUltElabPrAutFromBuffer(buffer, position);
        position += WpcoDtUltElabPrAut.Len.WPCO_DT_ULT_ELAB_PR_AUT;
        wpcoDtUltElabCommef.setDpcoDtUltElabCommefFromBuffer(buffer, position);
        position += WpcoDtUltElabCommef.Len.WPCO_DT_ULT_ELAB_COMMEF;
        wpcoDtUltElabLiqmef.setDpcoDtUltElabLiqmefFromBuffer(buffer, position);
        position += WpcoDtUltElabLiqmef.Len.WPCO_DT_ULT_ELAB_LIQMEF;
        wpcoCodFiscMef = MarshalByte.readString(buffer, position, Len.WPCO_COD_FISC_MEF);
        position += Len.WPCO_COD_FISC_MEF;
        wpcoImpAssSociale.setDpcoImpAssSocialeFromBuffer(buffer, position);
        position += WpcoImpAssSociale.Len.WPCO_IMP_ASS_SOCIALE;
        wpcoModComnzInvstSw = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpcoDtRiatRiassRsh.setDpcoDtRiatRiassRshFromBuffer(buffer, position);
        position += WpcoDtRiatRiassRsh.Len.WPCO_DT_RIAT_RIASS_RSH;
        wpcoDtRiatRiassComm.setDpcoDtRiatRiassCommFromBuffer(buffer, position);
        position += WpcoDtRiatRiassComm.Len.WPCO_DT_RIAT_RIASS_COMM;
        wpcoGgIntrRitPag.setDpcoGgIntrRitPagFromBuffer(buffer, position);
        position += WpcoGgIntrRitPag.Len.WPCO_GG_INTR_RIT_PAG;
        wpcoDtUltRinnTac.setDpcoDtUltRinnTacFromBuffer(buffer, position);
        position += WpcoDtUltRinnTac.Len.WPCO_DT_ULT_RINN_TAC;
        setWpcoDescCompVcharBytes(buffer, position);
        position += Len.WPCO_DESC_COMP_VCHAR;
        wpcoDtUltEcTcmInd.setDpcoDtUltEcTcmIndFromBuffer(buffer, position);
        position += WpcoDtUltEcTcmInd.Len.WPCO_DT_ULT_EC_TCM_IND;
        wpcoDtUltEcTcmColl.setDpcoDtUltEcTcmCollFromBuffer(buffer, position);
        position += WpcoDtUltEcTcmColl.Len.WPCO_DT_ULT_EC_TCM_COLL;
        wpcoDtUltEcMrmInd.setDpcoDtUltEcMrmIndFromBuffer(buffer, position);
        position += WpcoDtUltEcMrmInd.Len.WPCO_DT_ULT_EC_MRM_IND;
        wpcoDtUltEcMrmColl.setDpcoDtUltEcMrmCollFromBuffer(buffer, position);
        position += WpcoDtUltEcMrmColl.Len.WPCO_DT_ULT_EC_MRM_COLL;
        wpcoCodCompLdap = MarshalByte.readString(buffer, position, Len.WPCO_COD_COMP_LDAP);
        position += Len.WPCO_COD_COMP_LDAP;
        wpcoPcRidImp1382011.setDpcoPcRidImp1382011FromBuffer(buffer, position);
        position += WpcoPcRidImp1382011.Len.WPCO_PC_RID_IMP1382011;
        wpcoPcRidImp662014.setDpcoPcRidImp662014FromBuffer(buffer, position);
        position += WpcoPcRidImp662014.Len.WPCO_PC_RID_IMP662014;
        wpcoSoglAmlPreUni.setDpcoSoglAmlPreUniFromBuffer(buffer, position);
        position += WpcoSoglAmlPreUni.Len.WPCO_SOGL_AML_PRE_UNI;
        wpcoSoglAmlPrePer.setDpcoSoglAmlPrePerFromBuffer(buffer, position);
        position += WpcoSoglAmlPrePer.Len.WPCO_SOGL_AML_PRE_PER;
        wpcoCodSoggFtzAssto = MarshalByte.readString(buffer, position, Len.WPCO_COD_SOGG_FTZ_ASSTO);
        position += Len.WPCO_COD_SOGG_FTZ_ASSTO;
        wpcoDtUltElabRedpro.setDpcoDtUltElabRedproFromBuffer(buffer, position);
        position += WpcoDtUltElabRedpro.Len.WPCO_DT_ULT_ELAB_REDPRO;
        wpcoDtUltElabTakeP.setDpcoDtUltElabTakePFromBuffer(buffer, position);
        position += WpcoDtUltElabTakeP.Len.WPCO_DT_ULT_ELAB_TAKE_P;
        wpcoDtUltElabPaspas.setDpcoDtUltElabPaspasFromBuffer(buffer, position);
        position += WpcoDtUltElabPaspas.Len.WPCO_DT_ULT_ELAB_PASPAS;
        wpcoSoglAmlPreSavR.setDpcoSoglAmlPreSavRFromBuffer(buffer, position);
        position += WpcoSoglAmlPreSavR.Len.WPCO_SOGL_AML_PRE_SAV_R;
        wpcoDtUltEstrDecCo.setDpcoDtUltEstrDecCoFromBuffer(buffer, position);
        position += WpcoDtUltEstrDecCo.Len.WPCO_DT_ULT_ESTR_DEC_CO;
        wpcoDtUltElabCosAt.setDpcoDtUltElabCosAtFromBuffer(buffer, position);
        position += WpcoDtUltElabCosAt.Len.WPCO_DT_ULT_ELAB_COS_AT;
        wpcoFrqCostiAtt.setDpcoFrqCostiAttFromBuffer(buffer, position);
        position += WpcoFrqCostiAtt.Len.WPCO_FRQ_COSTI_ATT;
        wpcoDtUltElabCosSt.setDpcoDtUltElabCosStFromBuffer(buffer, position);
        position += WpcoDtUltElabCosSt.Len.WPCO_DT_ULT_ELAB_COS_ST;
        wpcoFrqCostiStornati.setDpcoFrqCostiStornatiFromBuffer(buffer, position);
        position += WpcoFrqCostiStornati.Len.WPCO_FRQ_COSTI_STORNATI;
        wpcoDtEstrAssMin70a.setDpcoDtEstrAssMin70aFromBuffer(buffer, position);
        position += WpcoDtEstrAssMin70a.Len.WPCO_DT_ESTR_ASS_MIN70A;
        wpcoDtEstrAssMag70a.setDpcoDtEstrAssMag70aFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wpcoCodCompAnia, Len.Int.WPCO_COD_COMP_ANIA, 0);
        position += Len.WPCO_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wpcoCodTratCirt, Len.WPCO_COD_TRAT_CIRT);
        position += Len.WPCO_COD_TRAT_CIRT;
        wpcoLimVltr.getWpcoLimVltrAsBuffer(buffer, position);
        position += WpcoLimVltr.Len.WPCO_LIM_VLTR;
        MarshalByte.writeChar(buffer, position, wpcoTpRatPerf);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wpcoTpLivGenzTit, Len.WPCO_TP_LIV_GENZ_TIT);
        position += Len.WPCO_TP_LIV_GENZ_TIT;
        wpcoArrotPre.getWpcoArrotPreAsBuffer(buffer, position);
        position += WpcoArrotPre.Len.WPCO_ARROT_PRE;
        wpcoDtCont.getWpcoDtContAsBuffer(buffer, position);
        position += WpcoDtCont.Len.WPCO_DT_CONT;
        wpcoDtUltRivalIn.getWpcoDtUltRivalInAsBuffer(buffer, position);
        position += WpcoDtUltRivalIn.Len.WPCO_DT_ULT_RIVAL_IN;
        wpcoDtUltQtzoIn.getWpcoDtUltQtzoInAsBuffer(buffer, position);
        position += WpcoDtUltQtzoIn.Len.WPCO_DT_ULT_QTZO_IN;
        wpcoDtUltRiclRiass.getWpcoDtUltRiclRiassAsBuffer(buffer, position);
        position += WpcoDtUltRiclRiass.Len.WPCO_DT_ULT_RICL_RIASS;
        wpcoDtUltTabulRiass.getWpcoDtUltTabulRiassAsBuffer(buffer, position);
        position += WpcoDtUltTabulRiass.Len.WPCO_DT_ULT_TABUL_RIASS;
        wpcoDtUltBollEmes.getWpcoDtUltBollEmesAsBuffer(buffer, position);
        position += WpcoDtUltBollEmes.Len.WPCO_DT_ULT_BOLL_EMES;
        wpcoDtUltBollStor.getWpcoDtUltBollStorAsBuffer(buffer, position);
        position += WpcoDtUltBollStor.Len.WPCO_DT_ULT_BOLL_STOR;
        wpcoDtUltBollLiq.getWpcoDtUltBollLiqAsBuffer(buffer, position);
        position += WpcoDtUltBollLiq.Len.WPCO_DT_ULT_BOLL_LIQ;
        wpcoDtUltBollRiat.getWpcoDtUltBollRiatAsBuffer(buffer, position);
        position += WpcoDtUltBollRiat.Len.WPCO_DT_ULT_BOLL_RIAT;
        wpcoDtUltelriscparPr.getWpcoDtUltelriscparPrAsBuffer(buffer, position);
        position += WpcoDtUltelriscparPr.Len.WPCO_DT_ULTELRISCPAR_PR;
        wpcoDtUltcIsIn.getWpcoDtUltcIsInAsBuffer(buffer, position);
        position += WpcoDtUltcIsIn.Len.WPCO_DT_ULTC_IS_IN;
        wpcoDtUltRiclPre.getWpcoDtUltRiclPreAsBuffer(buffer, position);
        position += WpcoDtUltRiclPre.Len.WPCO_DT_ULT_RICL_PRE;
        wpcoDtUltcMarsol.getWpcoDtUltcMarsolAsBuffer(buffer, position);
        position += WpcoDtUltcMarsol.Len.WPCO_DT_ULTC_MARSOL;
        wpcoDtUltcRbIn.getWpcoDtUltcRbInAsBuffer(buffer, position);
        position += WpcoDtUltcRbIn.Len.WPCO_DT_ULTC_RB_IN;
        wpcoPcProv1aaAcq.getWpcoPcProv1aaAcqAsBuffer(buffer, position);
        position += WpcoPcProv1aaAcq.Len.WPCO_PC_PROV1AA_ACQ;
        MarshalByte.writeString(buffer, position, wpcoModIntrPrest, Len.WPCO_MOD_INTR_PREST);
        position += Len.WPCO_MOD_INTR_PREST;
        wpcoGgMaxRecProv.getWpcoGgMaxRecProvAsBuffer(buffer, position);
        position += WpcoGgMaxRecProv.Len.WPCO_GG_MAX_REC_PROV;
        wpcoDtUltgzTrchEIn.getWpcoDtUltgzTrchEInAsBuffer(buffer, position);
        position += WpcoDtUltgzTrchEIn.Len.WPCO_DT_ULTGZ_TRCH_E_IN;
        wpcoDtUltBollSnden.getWpcoDtUltBollSndenAsBuffer(buffer, position);
        position += WpcoDtUltBollSnden.Len.WPCO_DT_ULT_BOLL_SNDEN;
        wpcoDtUltBollSndnlq.getWpcoDtUltBollSndnlqAsBuffer(buffer, position);
        position += WpcoDtUltBollSndnlq.Len.WPCO_DT_ULT_BOLL_SNDNLQ;
        wpcoDtUltscElabIn.getWpcoDtUltscElabInAsBuffer(buffer, position);
        position += WpcoDtUltscElabIn.Len.WPCO_DT_ULTSC_ELAB_IN;
        wpcoDtUltscOpzIn.getWpcoDtUltscOpzInAsBuffer(buffer, position);
        position += WpcoDtUltscOpzIn.Len.WPCO_DT_ULTSC_OPZ_IN;
        wpcoDtUltcBnsricIn.getWpcoDtUltcBnsricInAsBuffer(buffer, position);
        position += WpcoDtUltcBnsricIn.Len.WPCO_DT_ULTC_BNSRIC_IN;
        wpcoDtUltcBnsfdtIn.getWpcoDtUltcBnsfdtInAsBuffer(buffer, position);
        position += WpcoDtUltcBnsfdtIn.Len.WPCO_DT_ULTC_BNSFDT_IN;
        wpcoDtUltRinnGarac.getWpcoDtUltRinnGaracAsBuffer(buffer, position);
        position += WpcoDtUltRinnGarac.Len.WPCO_DT_ULT_RINN_GARAC;
        wpcoDtUltgzCed.getWpcoDtUltgzCedAsBuffer(buffer, position);
        position += WpcoDtUltgzCed.Len.WPCO_DT_ULTGZ_CED;
        wpcoDtUltElabPrlcos.getWpcoDtUltElabPrlcosAsBuffer(buffer, position);
        position += WpcoDtUltElabPrlcos.Len.WPCO_DT_ULT_ELAB_PRLCOS;
        wpcoDtUltRinnColl.getWpcoDtUltRinnCollAsBuffer(buffer, position);
        position += WpcoDtUltRinnColl.Len.WPCO_DT_ULT_RINN_COLL;
        MarshalByte.writeChar(buffer, position, wpcoFlRvcPerf);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpcoFlRcsPoliNoperf);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpcoFlGestPlusv);
        position += Types.CHAR_SIZE;
        wpcoDtUltRivalCl.getWpcoDtUltRivalClAsBuffer(buffer, position);
        position += WpcoDtUltRivalCl.Len.WPCO_DT_ULT_RIVAL_CL;
        wpcoDtUltQtzoCl.getWpcoDtUltQtzoClAsBuffer(buffer, position);
        position += WpcoDtUltQtzoCl.Len.WPCO_DT_ULT_QTZO_CL;
        wpcoDtUltcBnsricCl.getWpcoDtUltcBnsricClAsBuffer(buffer, position);
        position += WpcoDtUltcBnsricCl.Len.WPCO_DT_ULTC_BNSRIC_CL;
        wpcoDtUltcBnsfdtCl.getWpcoDtUltcBnsfdtClAsBuffer(buffer, position);
        position += WpcoDtUltcBnsfdtCl.Len.WPCO_DT_ULTC_BNSFDT_CL;
        wpcoDtUltcIsCl.getWpcoDtUltcIsClAsBuffer(buffer, position);
        position += WpcoDtUltcIsCl.Len.WPCO_DT_ULTC_IS_CL;
        wpcoDtUltcRbCl.getWpcoDtUltcRbClAsBuffer(buffer, position);
        position += WpcoDtUltcRbCl.Len.WPCO_DT_ULTC_RB_CL;
        wpcoDtUltgzTrchECl.getWpcoDtUltgzTrchEClAsBuffer(buffer, position);
        position += WpcoDtUltgzTrchECl.Len.WPCO_DT_ULTGZ_TRCH_E_CL;
        wpcoDtUltscElabCl.getWpcoDtUltscElabClAsBuffer(buffer, position);
        position += WpcoDtUltscElabCl.Len.WPCO_DT_ULTSC_ELAB_CL;
        wpcoDtUltscOpzCl.getWpcoDtUltscOpzClAsBuffer(buffer, position);
        position += WpcoDtUltscOpzCl.Len.WPCO_DT_ULTSC_OPZ_CL;
        wpcoStstXRegione.getWpcoStstXRegioneAsBuffer(buffer, position);
        position += WpcoStstXRegione.Len.WPCO_STST_X_REGIONE;
        wpcoDtUltgzCedColl.getWpcoDtUltgzCedCollAsBuffer(buffer, position);
        position += WpcoDtUltgzCedColl.Len.WPCO_DT_ULTGZ_CED_COLL;
        MarshalByte.writeString(buffer, position, wpcoTpModRival, Len.WPCO_TP_MOD_RIVAL);
        position += Len.WPCO_TP_MOD_RIVAL;
        wpcoNumMmCalcMora.getWpcoNumMmCalcMoraAsBuffer(buffer, position);
        position += WpcoNumMmCalcMora.Len.WPCO_NUM_MM_CALC_MORA;
        wpcoDtUltEcRivColl.getWpcoDtUltEcRivCollAsBuffer(buffer, position);
        position += WpcoDtUltEcRivColl.Len.WPCO_DT_ULT_EC_RIV_COLL;
        wpcoDtUltEcRivInd.getWpcoDtUltEcRivIndAsBuffer(buffer, position);
        position += WpcoDtUltEcRivInd.Len.WPCO_DT_ULT_EC_RIV_IND;
        wpcoDtUltEcIlColl.getWpcoDtUltEcIlCollAsBuffer(buffer, position);
        position += WpcoDtUltEcIlColl.Len.WPCO_DT_ULT_EC_IL_COLL;
        wpcoDtUltEcIlInd.getWpcoDtUltEcIlIndAsBuffer(buffer, position);
        position += WpcoDtUltEcIlInd.Len.WPCO_DT_ULT_EC_IL_IND;
        wpcoDtUltEcUlColl.getWpcoDtUltEcUlCollAsBuffer(buffer, position);
        position += WpcoDtUltEcUlColl.Len.WPCO_DT_ULT_EC_UL_COLL;
        wpcoDtUltEcUlInd.getWpcoDtUltEcUlIndAsBuffer(buffer, position);
        position += WpcoDtUltEcUlInd.Len.WPCO_DT_ULT_EC_UL_IND;
        wpcoAaUti.getWpcoAaUtiAsBuffer(buffer, position);
        position += WpcoAaUti.Len.WPCO_AA_UTI;
        MarshalByte.writeChar(buffer, position, wpcoCalcRshComun);
        position += Types.CHAR_SIZE;
        MarshalByte.writeShortAsPacked(buffer, position, wpcoFlLivDebug, Len.Int.WPCO_FL_LIV_DEBUG, 0);
        position += Len.WPCO_FL_LIV_DEBUG;
        wpcoDtUltBollPerfC.getWpcoDtUltBollPerfCAsBuffer(buffer, position);
        position += WpcoDtUltBollPerfC.Len.WPCO_DT_ULT_BOLL_PERF_C;
        wpcoDtUltBollRspIn.getWpcoDtUltBollRspInAsBuffer(buffer, position);
        position += WpcoDtUltBollRspIn.Len.WPCO_DT_ULT_BOLL_RSP_IN;
        wpcoDtUltBollRspCl.getWpcoDtUltBollRspClAsBuffer(buffer, position);
        position += WpcoDtUltBollRspCl.Len.WPCO_DT_ULT_BOLL_RSP_CL;
        wpcoDtUltBollEmesI.getWpcoDtUltBollEmesIAsBuffer(buffer, position);
        position += WpcoDtUltBollEmesI.Len.WPCO_DT_ULT_BOLL_EMES_I;
        wpcoDtUltBollStorI.getWpcoDtUltBollStorIAsBuffer(buffer, position);
        position += WpcoDtUltBollStorI.Len.WPCO_DT_ULT_BOLL_STOR_I;
        wpcoDtUltBollRiatI.getWpcoDtUltBollRiatIAsBuffer(buffer, position);
        position += WpcoDtUltBollRiatI.Len.WPCO_DT_ULT_BOLL_RIAT_I;
        wpcoDtUltBollSdI.getWpcoDtUltBollSdIAsBuffer(buffer, position);
        position += WpcoDtUltBollSdI.Len.WPCO_DT_ULT_BOLL_SD_I;
        wpcoDtUltBollSdnlI.getWpcoDtUltBollSdnlIAsBuffer(buffer, position);
        position += WpcoDtUltBollSdnlI.Len.WPCO_DT_ULT_BOLL_SDNL_I;
        wpcoDtUltBollPerfI.getWpcoDtUltBollPerfIAsBuffer(buffer, position);
        position += WpcoDtUltBollPerfI.Len.WPCO_DT_ULT_BOLL_PERF_I;
        wpcoDtRiclRiriasCom.getWpcoDtRiclRiriasComAsBuffer(buffer, position);
        position += WpcoDtRiclRiriasCom.Len.WPCO_DT_RICL_RIRIAS_COM;
        wpcoDtUltElabAt92C.getWpcoDtUltElabAt92CAsBuffer(buffer, position);
        position += WpcoDtUltElabAt92C.Len.WPCO_DT_ULT_ELAB_AT92_C;
        wpcoDtUltElabAt92I.getWpcoDtUltElabAt92IAsBuffer(buffer, position);
        position += WpcoDtUltElabAt92I.Len.WPCO_DT_ULT_ELAB_AT92_I;
        wpcoDtUltElabAt93C.getWpcoDtUltElabAt93CAsBuffer(buffer, position);
        position += WpcoDtUltElabAt93C.Len.WPCO_DT_ULT_ELAB_AT93_C;
        wpcoDtUltElabAt93I.getWpcoDtUltElabAt93IAsBuffer(buffer, position);
        position += WpcoDtUltElabAt93I.Len.WPCO_DT_ULT_ELAB_AT93_I;
        wpcoDtUltElabSpeIn.getWpcoDtUltElabSpeInAsBuffer(buffer, position);
        position += WpcoDtUltElabSpeIn.Len.WPCO_DT_ULT_ELAB_SPE_IN;
        wpcoDtUltElabPrCon.getWpcoDtUltElabPrConAsBuffer(buffer, position);
        position += WpcoDtUltElabPrCon.Len.WPCO_DT_ULT_ELAB_PR_CON;
        wpcoDtUltBollRpCl.getWpcoDtUltBollRpClAsBuffer(buffer, position);
        position += WpcoDtUltBollRpCl.Len.WPCO_DT_ULT_BOLL_RP_CL;
        wpcoDtUltBollRpIn.getWpcoDtUltBollRpInAsBuffer(buffer, position);
        position += WpcoDtUltBollRpIn.Len.WPCO_DT_ULT_BOLL_RP_IN;
        wpcoDtUltBollPreI.getWpcoDtUltBollPreIAsBuffer(buffer, position);
        position += WpcoDtUltBollPreI.Len.WPCO_DT_ULT_BOLL_PRE_I;
        wpcoDtUltBollPreC.getWpcoDtUltBollPreCAsBuffer(buffer, position);
        position += WpcoDtUltBollPreC.Len.WPCO_DT_ULT_BOLL_PRE_C;
        wpcoDtUltcPildiMmC.getWpcoDtUltcPildiMmCAsBuffer(buffer, position);
        position += WpcoDtUltcPildiMmC.Len.WPCO_DT_ULTC_PILDI_MM_C;
        wpcoDtUltcPildiAaC.getWpcoDtUltcPildiAaCAsBuffer(buffer, position);
        position += WpcoDtUltcPildiAaC.Len.WPCO_DT_ULTC_PILDI_AA_C;
        wpcoDtUltcPildiMmI.getWpcoDtUltcPildiMmIAsBuffer(buffer, position);
        position += WpcoDtUltcPildiMmI.Len.WPCO_DT_ULTC_PILDI_MM_I;
        wpcoDtUltcPildiTrI.getWpcoDtUltcPildiTrIAsBuffer(buffer, position);
        position += WpcoDtUltcPildiTrI.Len.WPCO_DT_ULTC_PILDI_TR_I;
        wpcoDtUltcPildiAaI.getWpcoDtUltcPildiAaIAsBuffer(buffer, position);
        position += WpcoDtUltcPildiAaI.Len.WPCO_DT_ULTC_PILDI_AA_I;
        wpcoDtUltBollQuieC.getWpcoDtUltBollQuieCAsBuffer(buffer, position);
        position += WpcoDtUltBollQuieC.Len.WPCO_DT_ULT_BOLL_QUIE_C;
        wpcoDtUltBollQuieI.getWpcoDtUltBollQuieIAsBuffer(buffer, position);
        position += WpcoDtUltBollQuieI.Len.WPCO_DT_ULT_BOLL_QUIE_I;
        wpcoDtUltBollCotrI.getWpcoDtUltBollCotrIAsBuffer(buffer, position);
        position += WpcoDtUltBollCotrI.Len.WPCO_DT_ULT_BOLL_COTR_I;
        wpcoDtUltBollCotrC.getWpcoDtUltBollCotrCAsBuffer(buffer, position);
        position += WpcoDtUltBollCotrC.Len.WPCO_DT_ULT_BOLL_COTR_C;
        wpcoDtUltBollCoriC.getWpcoDtUltBollCoriCAsBuffer(buffer, position);
        position += WpcoDtUltBollCoriC.Len.WPCO_DT_ULT_BOLL_CORI_C;
        wpcoDtUltBollCoriI.getWpcoDtUltBollCoriIAsBuffer(buffer, position);
        position += WpcoDtUltBollCoriI.Len.WPCO_DT_ULT_BOLL_CORI_I;
        MarshalByte.writeChar(buffer, position, wpcoDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wpcoDsVer, Len.Int.WPCO_DS_VER, 0);
        position += Len.WPCO_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wpcoDsTsCptz, Len.Int.WPCO_DS_TS_CPTZ, 0);
        position += Len.WPCO_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wpcoDsUtente, Len.WPCO_DS_UTENTE);
        position += Len.WPCO_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wpcoDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wpcoTpValzzDtVlt, Len.WPCO_TP_VALZZ_DT_VLT);
        position += Len.WPCO_TP_VALZZ_DT_VLT;
        MarshalByte.writeChar(buffer, position, wpcoFlFrazProvAcq);
        position += Types.CHAR_SIZE;
        wpcoDtUltAggErogRe.getWpcoDtUltAggErogReAsBuffer(buffer, position);
        position += WpcoDtUltAggErogRe.Len.WPCO_DT_ULT_AGG_EROG_RE;
        wpcoPcRmMarsol.getWpcoPcRmMarsolAsBuffer(buffer, position);
        position += WpcoPcRmMarsol.Len.WPCO_PC_RM_MARSOL;
        wpcoPcCSubrshMarsol.getWpcoPcCSubrshMarsolAsBuffer(buffer, position);
        position += WpcoPcCSubrshMarsol.Len.WPCO_PC_C_SUBRSH_MARSOL;
        MarshalByte.writeString(buffer, position, wpcoCodCompIsvap, Len.WPCO_COD_COMP_ISVAP);
        position += Len.WPCO_COD_COMP_ISVAP;
        wpcoLmRisConInt.getWpcoLmRisConIntAsBuffer(buffer, position);
        position += WpcoLmRisConInt.Len.WPCO_LM_RIS_CON_INT;
        wpcoLmCSubrshConIn.getWpcoLmCSubrshConInAsBuffer(buffer, position);
        position += WpcoLmCSubrshConIn.Len.WPCO_LM_C_SUBRSH_CON_IN;
        wpcoPcGarNoriskMars.getWpcoPcGarNoriskMarsAsBuffer(buffer, position);
        position += WpcoPcGarNoriskMars.Len.WPCO_PC_GAR_NORISK_MARS;
        MarshalByte.writeChar(buffer, position, wpcoCrz1aRatIntrPr);
        position += Types.CHAR_SIZE;
        wpcoNumGgArrIntrPr.getWpcoNumGgArrIntrPrAsBuffer(buffer, position);
        position += WpcoNumGgArrIntrPr.Len.WPCO_NUM_GG_ARR_INTR_PR;
        MarshalByte.writeChar(buffer, position, wpcoFlVisualVinpg);
        position += Types.CHAR_SIZE;
        wpcoDtUltEstrazFug.getWpcoDtUltEstrazFugAsBuffer(buffer, position);
        position += WpcoDtUltEstrazFug.Len.WPCO_DT_ULT_ESTRAZ_FUG;
        wpcoDtUltElabPrAut.getWpcoDtUltElabPrAutAsBuffer(buffer, position);
        position += WpcoDtUltElabPrAut.Len.WPCO_DT_ULT_ELAB_PR_AUT;
        wpcoDtUltElabCommef.getWpcoDtUltElabCommefAsBuffer(buffer, position);
        position += WpcoDtUltElabCommef.Len.WPCO_DT_ULT_ELAB_COMMEF;
        wpcoDtUltElabLiqmef.getWpcoDtUltElabLiqmefAsBuffer(buffer, position);
        position += WpcoDtUltElabLiqmef.Len.WPCO_DT_ULT_ELAB_LIQMEF;
        MarshalByte.writeString(buffer, position, wpcoCodFiscMef, Len.WPCO_COD_FISC_MEF);
        position += Len.WPCO_COD_FISC_MEF;
        wpcoImpAssSociale.getWpcoImpAssSocialeAsBuffer(buffer, position);
        position += WpcoImpAssSociale.Len.WPCO_IMP_ASS_SOCIALE;
        MarshalByte.writeChar(buffer, position, wpcoModComnzInvstSw);
        position += Types.CHAR_SIZE;
        wpcoDtRiatRiassRsh.getWpcoDtRiatRiassRshAsBuffer(buffer, position);
        position += WpcoDtRiatRiassRsh.Len.WPCO_DT_RIAT_RIASS_RSH;
        wpcoDtRiatRiassComm.getWpcoDtRiatRiassCommAsBuffer(buffer, position);
        position += WpcoDtRiatRiassComm.Len.WPCO_DT_RIAT_RIASS_COMM;
        wpcoGgIntrRitPag.getWpcoGgIntrRitPagAsBuffer(buffer, position);
        position += WpcoGgIntrRitPag.Len.WPCO_GG_INTR_RIT_PAG;
        wpcoDtUltRinnTac.getWpcoDtUltRinnTacAsBuffer(buffer, position);
        position += WpcoDtUltRinnTac.Len.WPCO_DT_ULT_RINN_TAC;
        getWpcoDescCompVcharBytes(buffer, position);
        position += Len.WPCO_DESC_COMP_VCHAR;
        wpcoDtUltEcTcmInd.getWpcoDtUltEcTcmIndAsBuffer(buffer, position);
        position += WpcoDtUltEcTcmInd.Len.WPCO_DT_ULT_EC_TCM_IND;
        wpcoDtUltEcTcmColl.getWpcoDtUltEcTcmCollAsBuffer(buffer, position);
        position += WpcoDtUltEcTcmColl.Len.WPCO_DT_ULT_EC_TCM_COLL;
        wpcoDtUltEcMrmInd.getWpcoDtUltEcMrmIndAsBuffer(buffer, position);
        position += WpcoDtUltEcMrmInd.Len.WPCO_DT_ULT_EC_MRM_IND;
        wpcoDtUltEcMrmColl.getWpcoDtUltEcMrmCollAsBuffer(buffer, position);
        position += WpcoDtUltEcMrmColl.Len.WPCO_DT_ULT_EC_MRM_COLL;
        MarshalByte.writeString(buffer, position, wpcoCodCompLdap, Len.WPCO_COD_COMP_LDAP);
        position += Len.WPCO_COD_COMP_LDAP;
        wpcoPcRidImp1382011.getWpcoPcRidImp1382011AsBuffer(buffer, position);
        position += WpcoPcRidImp1382011.Len.WPCO_PC_RID_IMP1382011;
        wpcoPcRidImp662014.getWpcoPcRidImp662014AsBuffer(buffer, position);
        position += WpcoPcRidImp662014.Len.WPCO_PC_RID_IMP662014;
        wpcoSoglAmlPreUni.getWpcoSoglAmlPreUniAsBuffer(buffer, position);
        position += WpcoSoglAmlPreUni.Len.WPCO_SOGL_AML_PRE_UNI;
        wpcoSoglAmlPrePer.getWpcoSoglAmlPrePerAsBuffer(buffer, position);
        position += WpcoSoglAmlPrePer.Len.WPCO_SOGL_AML_PRE_PER;
        MarshalByte.writeString(buffer, position, wpcoCodSoggFtzAssto, Len.WPCO_COD_SOGG_FTZ_ASSTO);
        position += Len.WPCO_COD_SOGG_FTZ_ASSTO;
        wpcoDtUltElabRedpro.getWpcoDtUltElabRedproAsBuffer(buffer, position);
        position += WpcoDtUltElabRedpro.Len.WPCO_DT_ULT_ELAB_REDPRO;
        wpcoDtUltElabTakeP.getWpcoDtUltElabTakePAsBuffer(buffer, position);
        position += WpcoDtUltElabTakeP.Len.WPCO_DT_ULT_ELAB_TAKE_P;
        wpcoDtUltElabPaspas.getWpcoDtUltElabPaspasAsBuffer(buffer, position);
        position += WpcoDtUltElabPaspas.Len.WPCO_DT_ULT_ELAB_PASPAS;
        wpcoSoglAmlPreSavR.getWpcoSoglAmlPreSavRAsBuffer(buffer, position);
        position += WpcoSoglAmlPreSavR.Len.WPCO_SOGL_AML_PRE_SAV_R;
        wpcoDtUltEstrDecCo.getWpcoDtUltEstrDecCoAsBuffer(buffer, position);
        position += WpcoDtUltEstrDecCo.Len.WPCO_DT_ULT_ESTR_DEC_CO;
        wpcoDtUltElabCosAt.getWpcoDtUltElabCosAtAsBuffer(buffer, position);
        position += WpcoDtUltElabCosAt.Len.WPCO_DT_ULT_ELAB_COS_AT;
        wpcoFrqCostiAtt.getWpcoFrqCostiAttAsBuffer(buffer, position);
        position += WpcoFrqCostiAtt.Len.WPCO_FRQ_COSTI_ATT;
        wpcoDtUltElabCosSt.getWpcoDtUltElabCosStAsBuffer(buffer, position);
        position += WpcoDtUltElabCosSt.Len.WPCO_DT_ULT_ELAB_COS_ST;
        wpcoFrqCostiStornati.getWpcoFrqCostiStornatiAsBuffer(buffer, position);
        position += WpcoFrqCostiStornati.Len.WPCO_FRQ_COSTI_STORNATI;
        wpcoDtEstrAssMin70a.getWpcoDtEstrAssMin70aAsBuffer(buffer, position);
        position += WpcoDtEstrAssMin70a.Len.WPCO_DT_ESTR_ASS_MIN70A;
        wpcoDtEstrAssMag70a.getWpcoDtEstrAssMag70aAsBuffer(buffer, position);
        return buffer;
    }

    public void setWpcoCodCompAnia(int wpcoCodCompAnia) {
        this.wpcoCodCompAnia = wpcoCodCompAnia;
    }

    public int getWpcoCodCompAnia() {
        return this.wpcoCodCompAnia;
    }

    public void setWpcoCodTratCirt(String wpcoCodTratCirt) {
        this.wpcoCodTratCirt = Functions.subString(wpcoCodTratCirt, Len.WPCO_COD_TRAT_CIRT);
    }

    public String getWpcoCodTratCirt() {
        return this.wpcoCodTratCirt;
    }

    public String getWpcoCodTratCirtFormatted() {
        return Functions.padBlanks(getWpcoCodTratCirt(), Len.WPCO_COD_TRAT_CIRT);
    }

    public void setWpcoTpRatPerf(char wpcoTpRatPerf) {
        this.wpcoTpRatPerf = wpcoTpRatPerf;
    }

    public char getWpcoTpRatPerf() {
        return this.wpcoTpRatPerf;
    }

    public void setWpcoTpLivGenzTit(String wpcoTpLivGenzTit) {
        this.wpcoTpLivGenzTit = Functions.subString(wpcoTpLivGenzTit, Len.WPCO_TP_LIV_GENZ_TIT);
    }

    public String getWpcoTpLivGenzTit() {
        return this.wpcoTpLivGenzTit;
    }

    public void setWpcoModIntrPrest(String wpcoModIntrPrest) {
        this.wpcoModIntrPrest = Functions.subString(wpcoModIntrPrest, Len.WPCO_MOD_INTR_PREST);
    }

    public String getWpcoModIntrPrest() {
        return this.wpcoModIntrPrest;
    }

    public String getWpcoModIntrPrestFormatted() {
        return Functions.padBlanks(getWpcoModIntrPrest(), Len.WPCO_MOD_INTR_PREST);
    }

    public void setWpcoFlRvcPerf(char wpcoFlRvcPerf) {
        this.wpcoFlRvcPerf = wpcoFlRvcPerf;
    }

    public char getWpcoFlRvcPerf() {
        return this.wpcoFlRvcPerf;
    }

    public void setWpcoFlRcsPoliNoperf(char wpcoFlRcsPoliNoperf) {
        this.wpcoFlRcsPoliNoperf = wpcoFlRcsPoliNoperf;
    }

    public char getWpcoFlRcsPoliNoperf() {
        return this.wpcoFlRcsPoliNoperf;
    }

    public void setWpcoFlGestPlusv(char wpcoFlGestPlusv) {
        this.wpcoFlGestPlusv = wpcoFlGestPlusv;
    }

    public char getWpcoFlGestPlusv() {
        return this.wpcoFlGestPlusv;
    }

    public void setWpcoTpModRival(String wpcoTpModRival) {
        this.wpcoTpModRival = Functions.subString(wpcoTpModRival, Len.WPCO_TP_MOD_RIVAL);
    }

    public String getWpcoTpModRival() {
        return this.wpcoTpModRival;
    }

    public void setWpcoCalcRshComun(char wpcoCalcRshComun) {
        this.wpcoCalcRshComun = wpcoCalcRshComun;
    }

    public char getWpcoCalcRshComun() {
        return this.wpcoCalcRshComun;
    }

    public void setWpcoFlLivDebug(short wpcoFlLivDebug) {
        this.wpcoFlLivDebug = wpcoFlLivDebug;
    }

    public short getWpcoFlLivDebug() {
        return this.wpcoFlLivDebug;
    }

    public void setWpcoDsOperSql(char wpcoDsOperSql) {
        this.wpcoDsOperSql = wpcoDsOperSql;
    }

    public char getWpcoDsOperSql() {
        return this.wpcoDsOperSql;
    }

    public void setWpcoDsVer(int wpcoDsVer) {
        this.wpcoDsVer = wpcoDsVer;
    }

    public int getWpcoDsVer() {
        return this.wpcoDsVer;
    }

    public void setWpcoDsTsCptz(long wpcoDsTsCptz) {
        this.wpcoDsTsCptz = wpcoDsTsCptz;
    }

    public long getWpcoDsTsCptz() {
        return this.wpcoDsTsCptz;
    }

    public void setWpcoDsUtente(String wpcoDsUtente) {
        this.wpcoDsUtente = Functions.subString(wpcoDsUtente, Len.WPCO_DS_UTENTE);
    }

    public String getWpcoDsUtente() {
        return this.wpcoDsUtente;
    }

    public void setWpcoDsStatoElab(char wpcoDsStatoElab) {
        this.wpcoDsStatoElab = wpcoDsStatoElab;
    }

    public char getWpcoDsStatoElab() {
        return this.wpcoDsStatoElab;
    }

    public void setWpcoTpValzzDtVlt(String wpcoTpValzzDtVlt) {
        this.wpcoTpValzzDtVlt = Functions.subString(wpcoTpValzzDtVlt, Len.WPCO_TP_VALZZ_DT_VLT);
    }

    public String getWpcoTpValzzDtVlt() {
        return this.wpcoTpValzzDtVlt;
    }

    public String getWpcoTpValzzDtVltFormatted() {
        return Functions.padBlanks(getWpcoTpValzzDtVlt(), Len.WPCO_TP_VALZZ_DT_VLT);
    }

    public void setWpcoFlFrazProvAcq(char wpcoFlFrazProvAcq) {
        this.wpcoFlFrazProvAcq = wpcoFlFrazProvAcq;
    }

    public char getWpcoFlFrazProvAcq() {
        return this.wpcoFlFrazProvAcq;
    }

    public void setWpcoCodCompIsvap(String wpcoCodCompIsvap) {
        this.wpcoCodCompIsvap = Functions.subString(wpcoCodCompIsvap, Len.WPCO_COD_COMP_ISVAP);
    }

    public String getWpcoCodCompIsvap() {
        return this.wpcoCodCompIsvap;
    }

    public String getWpcoCodCompIsvapFormatted() {
        return Functions.padBlanks(getWpcoCodCompIsvap(), Len.WPCO_COD_COMP_ISVAP);
    }

    public void setWpcoCrz1aRatIntrPr(char wpcoCrz1aRatIntrPr) {
        this.wpcoCrz1aRatIntrPr = wpcoCrz1aRatIntrPr;
    }

    public char getWpcoCrz1aRatIntrPr() {
        return this.wpcoCrz1aRatIntrPr;
    }

    public void setWpcoFlVisualVinpg(char wpcoFlVisualVinpg) {
        this.wpcoFlVisualVinpg = wpcoFlVisualVinpg;
    }

    public char getWpcoFlVisualVinpg() {
        return this.wpcoFlVisualVinpg;
    }

    public void setWpcoCodFiscMef(String wpcoCodFiscMef) {
        this.wpcoCodFiscMef = Functions.subString(wpcoCodFiscMef, Len.WPCO_COD_FISC_MEF);
    }

    public String getWpcoCodFiscMef() {
        return this.wpcoCodFiscMef;
    }

    public String getWpcoCodFiscMefFormatted() {
        return Functions.padBlanks(getWpcoCodFiscMef(), Len.WPCO_COD_FISC_MEF);
    }

    public void setWpcoModComnzInvstSw(char wpcoModComnzInvstSw) {
        this.wpcoModComnzInvstSw = wpcoModComnzInvstSw;
    }

    public char getWpcoModComnzInvstSw() {
        return this.wpcoModComnzInvstSw;
    }

    public void setWpcoDescCompVcharBytes(byte[] buffer) {
        setWpcoDescCompVcharBytes(buffer, 1);
    }

    /**Original name: WPCO-DESC-COMP-VCHAR<br>*/
    public byte[] getWpcoDescCompVcharBytes() {
        byte[] buffer = new byte[Len.WPCO_DESC_COMP_VCHAR];
        return getWpcoDescCompVcharBytes(buffer, 1);
    }

    public void setWpcoDescCompVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        wpcoDescCompLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wpcoDescComp = MarshalByte.readString(buffer, position, Len.WPCO_DESC_COMP);
    }

    public byte[] getWpcoDescCompVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpcoDescCompLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, wpcoDescComp, Len.WPCO_DESC_COMP);
        return buffer;
    }

    public void setWpcoDescCompLen(short wpcoDescCompLen) {
        this.wpcoDescCompLen = wpcoDescCompLen;
    }

    public short getWpcoDescCompLen() {
        return this.wpcoDescCompLen;
    }

    public void setWpcoDescComp(String wpcoDescComp) {
        this.wpcoDescComp = Functions.subString(wpcoDescComp, Len.WPCO_DESC_COMP);
    }

    public String getWpcoDescComp() {
        return this.wpcoDescComp;
    }

    public void setWpcoCodCompLdap(String wpcoCodCompLdap) {
        this.wpcoCodCompLdap = Functions.subString(wpcoCodCompLdap, Len.WPCO_COD_COMP_LDAP);
    }

    public String getWpcoCodCompLdap() {
        return this.wpcoCodCompLdap;
    }

    public String getWpcoCodCompLdapFormatted() {
        return Functions.padBlanks(getWpcoCodCompLdap(), Len.WPCO_COD_COMP_LDAP);
    }

    public void setWpcoCodSoggFtzAssto(String wpcoCodSoggFtzAssto) {
        this.wpcoCodSoggFtzAssto = Functions.subString(wpcoCodSoggFtzAssto, Len.WPCO_COD_SOGG_FTZ_ASSTO);
    }

    public String getWpcoCodSoggFtzAssto() {
        return this.wpcoCodSoggFtzAssto;
    }

    public String getWpcoCodSoggFtzAsstoFormatted() {
        return Functions.padBlanks(getWpcoCodSoggFtzAssto(), Len.WPCO_COD_SOGG_FTZ_ASSTO);
    }

    public WpcoAaUti getWpcoAaUti() {
        return wpcoAaUti;
    }

    public WpcoArrotPre getWpcoArrotPre() {
        return wpcoArrotPre;
    }

    public WpcoDtCont getWpcoDtCont() {
        return wpcoDtCont;
    }

    public WpcoDtEstrAssMag70a getWpcoDtEstrAssMag70a() {
        return wpcoDtEstrAssMag70a;
    }

    public WpcoDtEstrAssMin70a getWpcoDtEstrAssMin70a() {
        return wpcoDtEstrAssMin70a;
    }

    public WpcoDtRiatRiassComm getWpcoDtRiatRiassComm() {
        return wpcoDtRiatRiassComm;
    }

    public WpcoDtRiatRiassRsh getWpcoDtRiatRiassRsh() {
        return wpcoDtRiatRiassRsh;
    }

    public WpcoDtRiclRiriasCom getWpcoDtRiclRiriasCom() {
        return wpcoDtRiclRiriasCom;
    }

    public WpcoDtUltAggErogRe getWpcoDtUltAggErogRe() {
        return wpcoDtUltAggErogRe;
    }

    public WpcoDtUltBollCoriC getWpcoDtUltBollCoriC() {
        return wpcoDtUltBollCoriC;
    }

    public WpcoDtUltBollCoriI getWpcoDtUltBollCoriI() {
        return wpcoDtUltBollCoriI;
    }

    public WpcoDtUltBollCotrC getWpcoDtUltBollCotrC() {
        return wpcoDtUltBollCotrC;
    }

    public WpcoDtUltBollCotrI getWpcoDtUltBollCotrI() {
        return wpcoDtUltBollCotrI;
    }

    public WpcoDtUltBollEmes getWpcoDtUltBollEmes() {
        return wpcoDtUltBollEmes;
    }

    public WpcoDtUltBollEmesI getWpcoDtUltBollEmesI() {
        return wpcoDtUltBollEmesI;
    }

    public WpcoDtUltBollLiq getWpcoDtUltBollLiq() {
        return wpcoDtUltBollLiq;
    }

    public WpcoDtUltBollPerfC getWpcoDtUltBollPerfC() {
        return wpcoDtUltBollPerfC;
    }

    public WpcoDtUltBollPerfI getWpcoDtUltBollPerfI() {
        return wpcoDtUltBollPerfI;
    }

    public WpcoDtUltBollPreC getWpcoDtUltBollPreC() {
        return wpcoDtUltBollPreC;
    }

    public WpcoDtUltBollPreI getWpcoDtUltBollPreI() {
        return wpcoDtUltBollPreI;
    }

    public WpcoDtUltBollQuieC getWpcoDtUltBollQuieC() {
        return wpcoDtUltBollQuieC;
    }

    public WpcoDtUltBollQuieI getWpcoDtUltBollQuieI() {
        return wpcoDtUltBollQuieI;
    }

    public WpcoDtUltBollRiat getWpcoDtUltBollRiat() {
        return wpcoDtUltBollRiat;
    }

    public WpcoDtUltBollRiatI getWpcoDtUltBollRiatI() {
        return wpcoDtUltBollRiatI;
    }

    public WpcoDtUltBollRpCl getWpcoDtUltBollRpCl() {
        return wpcoDtUltBollRpCl;
    }

    public WpcoDtUltBollRpIn getWpcoDtUltBollRpIn() {
        return wpcoDtUltBollRpIn;
    }

    public WpcoDtUltBollRspCl getWpcoDtUltBollRspCl() {
        return wpcoDtUltBollRspCl;
    }

    public WpcoDtUltBollRspIn getWpcoDtUltBollRspIn() {
        return wpcoDtUltBollRspIn;
    }

    public WpcoDtUltBollSdI getWpcoDtUltBollSdI() {
        return wpcoDtUltBollSdI;
    }

    public WpcoDtUltBollSdnlI getWpcoDtUltBollSdnlI() {
        return wpcoDtUltBollSdnlI;
    }

    public WpcoDtUltBollSnden getWpcoDtUltBollSnden() {
        return wpcoDtUltBollSnden;
    }

    public WpcoDtUltBollSndnlq getWpcoDtUltBollSndnlq() {
        return wpcoDtUltBollSndnlq;
    }

    public WpcoDtUltBollStor getWpcoDtUltBollStor() {
        return wpcoDtUltBollStor;
    }

    public WpcoDtUltBollStorI getWpcoDtUltBollStorI() {
        return wpcoDtUltBollStorI;
    }

    public WpcoDtUltEcIlColl getWpcoDtUltEcIlColl() {
        return wpcoDtUltEcIlColl;
    }

    public WpcoDtUltEcIlInd getWpcoDtUltEcIlInd() {
        return wpcoDtUltEcIlInd;
    }

    public WpcoDtUltEcMrmColl getWpcoDtUltEcMrmColl() {
        return wpcoDtUltEcMrmColl;
    }

    public WpcoDtUltEcMrmInd getWpcoDtUltEcMrmInd() {
        return wpcoDtUltEcMrmInd;
    }

    public WpcoDtUltEcRivColl getWpcoDtUltEcRivColl() {
        return wpcoDtUltEcRivColl;
    }

    public WpcoDtUltEcRivInd getWpcoDtUltEcRivInd() {
        return wpcoDtUltEcRivInd;
    }

    public WpcoDtUltEcTcmColl getWpcoDtUltEcTcmColl() {
        return wpcoDtUltEcTcmColl;
    }

    public WpcoDtUltEcTcmInd getWpcoDtUltEcTcmInd() {
        return wpcoDtUltEcTcmInd;
    }

    public WpcoDtUltEcUlColl getWpcoDtUltEcUlColl() {
        return wpcoDtUltEcUlColl;
    }

    public WpcoDtUltEcUlInd getWpcoDtUltEcUlInd() {
        return wpcoDtUltEcUlInd;
    }

    public WpcoDtUltElabAt92C getWpcoDtUltElabAt92C() {
        return wpcoDtUltElabAt92C;
    }

    public WpcoDtUltElabAt92I getWpcoDtUltElabAt92I() {
        return wpcoDtUltElabAt92I;
    }

    public WpcoDtUltElabAt93C getWpcoDtUltElabAt93C() {
        return wpcoDtUltElabAt93C;
    }

    public WpcoDtUltElabAt93I getWpcoDtUltElabAt93I() {
        return wpcoDtUltElabAt93I;
    }

    public WpcoDtUltElabCommef getWpcoDtUltElabCommef() {
        return wpcoDtUltElabCommef;
    }

    public WpcoDtUltElabCosAt getWpcoDtUltElabCosAt() {
        return wpcoDtUltElabCosAt;
    }

    public WpcoDtUltElabCosSt getWpcoDtUltElabCosSt() {
        return wpcoDtUltElabCosSt;
    }

    public WpcoDtUltElabLiqmef getWpcoDtUltElabLiqmef() {
        return wpcoDtUltElabLiqmef;
    }

    public WpcoDtUltElabPaspas getWpcoDtUltElabPaspas() {
        return wpcoDtUltElabPaspas;
    }

    public WpcoDtUltElabPrAut getWpcoDtUltElabPrAut() {
        return wpcoDtUltElabPrAut;
    }

    public WpcoDtUltElabPrCon getWpcoDtUltElabPrCon() {
        return wpcoDtUltElabPrCon;
    }

    public WpcoDtUltElabPrlcos getWpcoDtUltElabPrlcos() {
        return wpcoDtUltElabPrlcos;
    }

    public WpcoDtUltElabRedpro getWpcoDtUltElabRedpro() {
        return wpcoDtUltElabRedpro;
    }

    public WpcoDtUltElabSpeIn getWpcoDtUltElabSpeIn() {
        return wpcoDtUltElabSpeIn;
    }

    public WpcoDtUltElabTakeP getWpcoDtUltElabTakeP() {
        return wpcoDtUltElabTakeP;
    }

    public WpcoDtUltEstrDecCo getWpcoDtUltEstrDecCo() {
        return wpcoDtUltEstrDecCo;
    }

    public WpcoDtUltEstrazFug getWpcoDtUltEstrazFug() {
        return wpcoDtUltEstrazFug;
    }

    public WpcoDtUltQtzoCl getWpcoDtUltQtzoCl() {
        return wpcoDtUltQtzoCl;
    }

    public WpcoDtUltQtzoIn getWpcoDtUltQtzoIn() {
        return wpcoDtUltQtzoIn;
    }

    public WpcoDtUltRiclPre getWpcoDtUltRiclPre() {
        return wpcoDtUltRiclPre;
    }

    public WpcoDtUltRiclRiass getWpcoDtUltRiclRiass() {
        return wpcoDtUltRiclRiass;
    }

    public WpcoDtUltRinnColl getWpcoDtUltRinnColl() {
        return wpcoDtUltRinnColl;
    }

    public WpcoDtUltRinnGarac getWpcoDtUltRinnGarac() {
        return wpcoDtUltRinnGarac;
    }

    public WpcoDtUltRinnTac getWpcoDtUltRinnTac() {
        return wpcoDtUltRinnTac;
    }

    public WpcoDtUltRivalCl getWpcoDtUltRivalCl() {
        return wpcoDtUltRivalCl;
    }

    public WpcoDtUltRivalIn getWpcoDtUltRivalIn() {
        return wpcoDtUltRivalIn;
    }

    public WpcoDtUltTabulRiass getWpcoDtUltTabulRiass() {
        return wpcoDtUltTabulRiass;
    }

    public WpcoDtUltcBnsfdtCl getWpcoDtUltcBnsfdtCl() {
        return wpcoDtUltcBnsfdtCl;
    }

    public WpcoDtUltcBnsfdtIn getWpcoDtUltcBnsfdtIn() {
        return wpcoDtUltcBnsfdtIn;
    }

    public WpcoDtUltcBnsricCl getWpcoDtUltcBnsricCl() {
        return wpcoDtUltcBnsricCl;
    }

    public WpcoDtUltcBnsricIn getWpcoDtUltcBnsricIn() {
        return wpcoDtUltcBnsricIn;
    }

    public WpcoDtUltcIsCl getWpcoDtUltcIsCl() {
        return wpcoDtUltcIsCl;
    }

    public WpcoDtUltcIsIn getWpcoDtUltcIsIn() {
        return wpcoDtUltcIsIn;
    }

    public WpcoDtUltcMarsol getWpcoDtUltcMarsol() {
        return wpcoDtUltcMarsol;
    }

    public WpcoDtUltcPildiAaC getWpcoDtUltcPildiAaC() {
        return wpcoDtUltcPildiAaC;
    }

    public WpcoDtUltcPildiAaI getWpcoDtUltcPildiAaI() {
        return wpcoDtUltcPildiAaI;
    }

    public WpcoDtUltcPildiMmC getWpcoDtUltcPildiMmC() {
        return wpcoDtUltcPildiMmC;
    }

    public WpcoDtUltcPildiMmI getWpcoDtUltcPildiMmI() {
        return wpcoDtUltcPildiMmI;
    }

    public WpcoDtUltcPildiTrI getWpcoDtUltcPildiTrI() {
        return wpcoDtUltcPildiTrI;
    }

    public WpcoDtUltcRbCl getWpcoDtUltcRbCl() {
        return wpcoDtUltcRbCl;
    }

    public WpcoDtUltcRbIn getWpcoDtUltcRbIn() {
        return wpcoDtUltcRbIn;
    }

    public WpcoDtUltelriscparPr getWpcoDtUltelriscparPr() {
        return wpcoDtUltelriscparPr;
    }

    public WpcoDtUltgzCed getWpcoDtUltgzCed() {
        return wpcoDtUltgzCed;
    }

    public WpcoDtUltgzCedColl getWpcoDtUltgzCedColl() {
        return wpcoDtUltgzCedColl;
    }

    public WpcoDtUltgzTrchECl getWpcoDtUltgzTrchECl() {
        return wpcoDtUltgzTrchECl;
    }

    public WpcoDtUltgzTrchEIn getWpcoDtUltgzTrchEIn() {
        return wpcoDtUltgzTrchEIn;
    }

    public WpcoDtUltscElabCl getWpcoDtUltscElabCl() {
        return wpcoDtUltscElabCl;
    }

    public WpcoDtUltscElabIn getWpcoDtUltscElabIn() {
        return wpcoDtUltscElabIn;
    }

    public WpcoDtUltscOpzCl getWpcoDtUltscOpzCl() {
        return wpcoDtUltscOpzCl;
    }

    public WpcoDtUltscOpzIn getWpcoDtUltscOpzIn() {
        return wpcoDtUltscOpzIn;
    }

    public WpcoFrqCostiAtt getWpcoFrqCostiAtt() {
        return wpcoFrqCostiAtt;
    }

    public WpcoFrqCostiStornati getWpcoFrqCostiStornati() {
        return wpcoFrqCostiStornati;
    }

    public WpcoGgIntrRitPag getWpcoGgIntrRitPag() {
        return wpcoGgIntrRitPag;
    }

    public WpcoGgMaxRecProv getWpcoGgMaxRecProv() {
        return wpcoGgMaxRecProv;
    }

    public WpcoImpAssSociale getWpcoImpAssSociale() {
        return wpcoImpAssSociale;
    }

    public WpcoLimVltr getWpcoLimVltr() {
        return wpcoLimVltr;
    }

    public WpcoLmCSubrshConIn getWpcoLmCSubrshConIn() {
        return wpcoLmCSubrshConIn;
    }

    public WpcoLmRisConInt getWpcoLmRisConInt() {
        return wpcoLmRisConInt;
    }

    public WpcoNumGgArrIntrPr getWpcoNumGgArrIntrPr() {
        return wpcoNumGgArrIntrPr;
    }

    public WpcoNumMmCalcMora getWpcoNumMmCalcMora() {
        return wpcoNumMmCalcMora;
    }

    public WpcoPcCSubrshMarsol getWpcoPcCSubrshMarsol() {
        return wpcoPcCSubrshMarsol;
    }

    public WpcoPcGarNoriskMars getWpcoPcGarNoriskMars() {
        return wpcoPcGarNoriskMars;
    }

    public WpcoPcProv1aaAcq getWpcoPcProv1aaAcq() {
        return wpcoPcProv1aaAcq;
    }

    public WpcoPcRidImp1382011 getWpcoPcRidImp1382011() {
        return wpcoPcRidImp1382011;
    }

    public WpcoPcRidImp662014 getWpcoPcRidImp662014() {
        return wpcoPcRidImp662014;
    }

    public WpcoPcRmMarsol getWpcoPcRmMarsol() {
        return wpcoPcRmMarsol;
    }

    public WpcoSoglAmlPrePer getWpcoSoglAmlPrePer() {
        return wpcoSoglAmlPrePer;
    }

    public WpcoSoglAmlPreSavR getWpcoSoglAmlPreSavR() {
        return wpcoSoglAmlPreSavR;
    }

    public WpcoSoglAmlPreUni getWpcoSoglAmlPreUni() {
        return wpcoSoglAmlPreUni;
    }

    public WpcoStstXRegione getWpcoStstXRegione() {
        return wpcoStstXRegione;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_COD_TRAT_CIRT = 20;
        public static final int WPCO_TP_LIV_GENZ_TIT = 2;
        public static final int WPCO_MOD_INTR_PREST = 2;
        public static final int WPCO_TP_MOD_RIVAL = 2;
        public static final int WPCO_DS_UTENTE = 20;
        public static final int WPCO_TP_VALZZ_DT_VLT = 2;
        public static final int WPCO_COD_COMP_ISVAP = 5;
        public static final int WPCO_COD_FISC_MEF = 16;
        public static final int WPCO_DESC_COMP = 250;
        public static final int WPCO_COD_COMP_LDAP = 20;
        public static final int WPCO_COD_SOGG_FTZ_ASSTO = 20;
        public static final int WPCO_COD_COMP_ANIA = 3;
        public static final int WPCO_TP_RAT_PERF = 1;
        public static final int WPCO_FL_RVC_PERF = 1;
        public static final int WPCO_FL_RCS_POLI_NOPERF = 1;
        public static final int WPCO_FL_GEST_PLUSV = 1;
        public static final int WPCO_CALC_RSH_COMUN = 1;
        public static final int WPCO_FL_LIV_DEBUG = 1;
        public static final int WPCO_DS_OPER_SQL = 1;
        public static final int WPCO_DS_VER = 5;
        public static final int WPCO_DS_TS_CPTZ = 10;
        public static final int WPCO_DS_STATO_ELAB = 1;
        public static final int WPCO_FL_FRAZ_PROV_ACQ = 1;
        public static final int WPCO_CRZ1A_RAT_INTR_PR = 1;
        public static final int WPCO_FL_VISUAL_VINPG = 1;
        public static final int WPCO_MOD_COMNZ_INVST_SW = 1;
        public static final int WPCO_DESC_COMP_LEN = 2;
        public static final int WPCO_DESC_COMP_VCHAR = WPCO_DESC_COMP_LEN + WPCO_DESC_COMP;
        public static final int DATI = WPCO_COD_COMP_ANIA + WPCO_COD_TRAT_CIRT + WpcoLimVltr.Len.WPCO_LIM_VLTR + WPCO_TP_RAT_PERF + WPCO_TP_LIV_GENZ_TIT + WpcoArrotPre.Len.WPCO_ARROT_PRE + WpcoDtCont.Len.WPCO_DT_CONT + WpcoDtUltRivalIn.Len.WPCO_DT_ULT_RIVAL_IN + WpcoDtUltQtzoIn.Len.WPCO_DT_ULT_QTZO_IN + WpcoDtUltRiclRiass.Len.WPCO_DT_ULT_RICL_RIASS + WpcoDtUltTabulRiass.Len.WPCO_DT_ULT_TABUL_RIASS + WpcoDtUltBollEmes.Len.WPCO_DT_ULT_BOLL_EMES + WpcoDtUltBollStor.Len.WPCO_DT_ULT_BOLL_STOR + WpcoDtUltBollLiq.Len.WPCO_DT_ULT_BOLL_LIQ + WpcoDtUltBollRiat.Len.WPCO_DT_ULT_BOLL_RIAT + WpcoDtUltelriscparPr.Len.WPCO_DT_ULTELRISCPAR_PR + WpcoDtUltcIsIn.Len.WPCO_DT_ULTC_IS_IN + WpcoDtUltRiclPre.Len.WPCO_DT_ULT_RICL_PRE + WpcoDtUltcMarsol.Len.WPCO_DT_ULTC_MARSOL + WpcoDtUltcRbIn.Len.WPCO_DT_ULTC_RB_IN + WpcoPcProv1aaAcq.Len.WPCO_PC_PROV1AA_ACQ + WPCO_MOD_INTR_PREST + WpcoGgMaxRecProv.Len.WPCO_GG_MAX_REC_PROV + WpcoDtUltgzTrchEIn.Len.WPCO_DT_ULTGZ_TRCH_E_IN + WpcoDtUltBollSnden.Len.WPCO_DT_ULT_BOLL_SNDEN + WpcoDtUltBollSndnlq.Len.WPCO_DT_ULT_BOLL_SNDNLQ + WpcoDtUltscElabIn.Len.WPCO_DT_ULTSC_ELAB_IN + WpcoDtUltscOpzIn.Len.WPCO_DT_ULTSC_OPZ_IN + WpcoDtUltcBnsricIn.Len.WPCO_DT_ULTC_BNSRIC_IN + WpcoDtUltcBnsfdtIn.Len.WPCO_DT_ULTC_BNSFDT_IN + WpcoDtUltRinnGarac.Len.WPCO_DT_ULT_RINN_GARAC + WpcoDtUltgzCed.Len.WPCO_DT_ULTGZ_CED + WpcoDtUltElabPrlcos.Len.WPCO_DT_ULT_ELAB_PRLCOS + WpcoDtUltRinnColl.Len.WPCO_DT_ULT_RINN_COLL + WPCO_FL_RVC_PERF + WPCO_FL_RCS_POLI_NOPERF + WPCO_FL_GEST_PLUSV + WpcoDtUltRivalCl.Len.WPCO_DT_ULT_RIVAL_CL + WpcoDtUltQtzoCl.Len.WPCO_DT_ULT_QTZO_CL + WpcoDtUltcBnsricCl.Len.WPCO_DT_ULTC_BNSRIC_CL + WpcoDtUltcBnsfdtCl.Len.WPCO_DT_ULTC_BNSFDT_CL + WpcoDtUltcIsCl.Len.WPCO_DT_ULTC_IS_CL + WpcoDtUltcRbCl.Len.WPCO_DT_ULTC_RB_CL + WpcoDtUltgzTrchECl.Len.WPCO_DT_ULTGZ_TRCH_E_CL + WpcoDtUltscElabCl.Len.WPCO_DT_ULTSC_ELAB_CL + WpcoDtUltscOpzCl.Len.WPCO_DT_ULTSC_OPZ_CL + WpcoStstXRegione.Len.WPCO_STST_X_REGIONE + WpcoDtUltgzCedColl.Len.WPCO_DT_ULTGZ_CED_COLL + WPCO_TP_MOD_RIVAL + WpcoNumMmCalcMora.Len.WPCO_NUM_MM_CALC_MORA + WpcoDtUltEcRivColl.Len.WPCO_DT_ULT_EC_RIV_COLL + WpcoDtUltEcRivInd.Len.WPCO_DT_ULT_EC_RIV_IND + WpcoDtUltEcIlColl.Len.WPCO_DT_ULT_EC_IL_COLL + WpcoDtUltEcIlInd.Len.WPCO_DT_ULT_EC_IL_IND + WpcoDtUltEcUlColl.Len.WPCO_DT_ULT_EC_UL_COLL + WpcoDtUltEcUlInd.Len.WPCO_DT_ULT_EC_UL_IND + WpcoAaUti.Len.WPCO_AA_UTI + WPCO_CALC_RSH_COMUN + WPCO_FL_LIV_DEBUG + WpcoDtUltBollPerfC.Len.WPCO_DT_ULT_BOLL_PERF_C + WpcoDtUltBollRspIn.Len.WPCO_DT_ULT_BOLL_RSP_IN + WpcoDtUltBollRspCl.Len.WPCO_DT_ULT_BOLL_RSP_CL + WpcoDtUltBollEmesI.Len.WPCO_DT_ULT_BOLL_EMES_I + WpcoDtUltBollStorI.Len.WPCO_DT_ULT_BOLL_STOR_I + WpcoDtUltBollRiatI.Len.WPCO_DT_ULT_BOLL_RIAT_I + WpcoDtUltBollSdI.Len.WPCO_DT_ULT_BOLL_SD_I + WpcoDtUltBollSdnlI.Len.WPCO_DT_ULT_BOLL_SDNL_I + WpcoDtUltBollPerfI.Len.WPCO_DT_ULT_BOLL_PERF_I + WpcoDtRiclRiriasCom.Len.WPCO_DT_RICL_RIRIAS_COM + WpcoDtUltElabAt92C.Len.WPCO_DT_ULT_ELAB_AT92_C + WpcoDtUltElabAt92I.Len.WPCO_DT_ULT_ELAB_AT92_I + WpcoDtUltElabAt93C.Len.WPCO_DT_ULT_ELAB_AT93_C + WpcoDtUltElabAt93I.Len.WPCO_DT_ULT_ELAB_AT93_I + WpcoDtUltElabSpeIn.Len.WPCO_DT_ULT_ELAB_SPE_IN + WpcoDtUltElabPrCon.Len.WPCO_DT_ULT_ELAB_PR_CON + WpcoDtUltBollRpCl.Len.WPCO_DT_ULT_BOLL_RP_CL + WpcoDtUltBollRpIn.Len.WPCO_DT_ULT_BOLL_RP_IN + WpcoDtUltBollPreI.Len.WPCO_DT_ULT_BOLL_PRE_I + WpcoDtUltBollPreC.Len.WPCO_DT_ULT_BOLL_PRE_C + WpcoDtUltcPildiMmC.Len.WPCO_DT_ULTC_PILDI_MM_C + WpcoDtUltcPildiAaC.Len.WPCO_DT_ULTC_PILDI_AA_C + WpcoDtUltcPildiMmI.Len.WPCO_DT_ULTC_PILDI_MM_I + WpcoDtUltcPildiTrI.Len.WPCO_DT_ULTC_PILDI_TR_I + WpcoDtUltcPildiAaI.Len.WPCO_DT_ULTC_PILDI_AA_I + WpcoDtUltBollQuieC.Len.WPCO_DT_ULT_BOLL_QUIE_C + WpcoDtUltBollQuieI.Len.WPCO_DT_ULT_BOLL_QUIE_I + WpcoDtUltBollCotrI.Len.WPCO_DT_ULT_BOLL_COTR_I + WpcoDtUltBollCotrC.Len.WPCO_DT_ULT_BOLL_COTR_C + WpcoDtUltBollCoriC.Len.WPCO_DT_ULT_BOLL_CORI_C + WpcoDtUltBollCoriI.Len.WPCO_DT_ULT_BOLL_CORI_I + WPCO_DS_OPER_SQL + WPCO_DS_VER + WPCO_DS_TS_CPTZ + WPCO_DS_UTENTE + WPCO_DS_STATO_ELAB + WPCO_TP_VALZZ_DT_VLT + WPCO_FL_FRAZ_PROV_ACQ + WpcoDtUltAggErogRe.Len.WPCO_DT_ULT_AGG_EROG_RE + WpcoPcRmMarsol.Len.WPCO_PC_RM_MARSOL + WpcoPcCSubrshMarsol.Len.WPCO_PC_C_SUBRSH_MARSOL + WPCO_COD_COMP_ISVAP + WpcoLmRisConInt.Len.WPCO_LM_RIS_CON_INT + WpcoLmCSubrshConIn.Len.WPCO_LM_C_SUBRSH_CON_IN + WpcoPcGarNoriskMars.Len.WPCO_PC_GAR_NORISK_MARS + WPCO_CRZ1A_RAT_INTR_PR + WpcoNumGgArrIntrPr.Len.WPCO_NUM_GG_ARR_INTR_PR + WPCO_FL_VISUAL_VINPG + WpcoDtUltEstrazFug.Len.WPCO_DT_ULT_ESTRAZ_FUG + WpcoDtUltElabPrAut.Len.WPCO_DT_ULT_ELAB_PR_AUT + WpcoDtUltElabCommef.Len.WPCO_DT_ULT_ELAB_COMMEF + WpcoDtUltElabLiqmef.Len.WPCO_DT_ULT_ELAB_LIQMEF + WPCO_COD_FISC_MEF + WpcoImpAssSociale.Len.WPCO_IMP_ASS_SOCIALE + WPCO_MOD_COMNZ_INVST_SW + WpcoDtRiatRiassRsh.Len.WPCO_DT_RIAT_RIASS_RSH + WpcoDtRiatRiassComm.Len.WPCO_DT_RIAT_RIASS_COMM + WpcoGgIntrRitPag.Len.WPCO_GG_INTR_RIT_PAG + WpcoDtUltRinnTac.Len.WPCO_DT_ULT_RINN_TAC + WPCO_DESC_COMP_VCHAR + WpcoDtUltEcTcmInd.Len.WPCO_DT_ULT_EC_TCM_IND + WpcoDtUltEcTcmColl.Len.WPCO_DT_ULT_EC_TCM_COLL + WpcoDtUltEcMrmInd.Len.WPCO_DT_ULT_EC_MRM_IND + WpcoDtUltEcMrmColl.Len.WPCO_DT_ULT_EC_MRM_COLL + WPCO_COD_COMP_LDAP + WpcoPcRidImp1382011.Len.WPCO_PC_RID_IMP1382011 + WpcoPcRidImp662014.Len.WPCO_PC_RID_IMP662014 + WpcoSoglAmlPreUni.Len.WPCO_SOGL_AML_PRE_UNI + WpcoSoglAmlPrePer.Len.WPCO_SOGL_AML_PRE_PER + WPCO_COD_SOGG_FTZ_ASSTO + WpcoDtUltElabRedpro.Len.WPCO_DT_ULT_ELAB_REDPRO + WpcoDtUltElabTakeP.Len.WPCO_DT_ULT_ELAB_TAKE_P + WpcoDtUltElabPaspas.Len.WPCO_DT_ULT_ELAB_PASPAS + WpcoSoglAmlPreSavR.Len.WPCO_SOGL_AML_PRE_SAV_R + WpcoDtUltEstrDecCo.Len.WPCO_DT_ULT_ESTR_DEC_CO + WpcoDtUltElabCosAt.Len.WPCO_DT_ULT_ELAB_COS_AT + WpcoFrqCostiAtt.Len.WPCO_FRQ_COSTI_ATT + WpcoDtUltElabCosSt.Len.WPCO_DT_ULT_ELAB_COS_ST + WpcoFrqCostiStornati.Len.WPCO_FRQ_COSTI_STORNATI + WpcoDtEstrAssMin70a.Len.WPCO_DT_ESTR_ASS_MIN70A + WpcoDtEstrAssMag70a.Len.WPCO_DT_ESTR_ASS_MAG70A;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_COD_COMP_ANIA = 5;
            public static final int WPCO_FL_LIV_DEBUG = 1;
            public static final int WPCO_DS_VER = 9;
            public static final int WPCO_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
