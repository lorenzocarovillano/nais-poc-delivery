package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.GrzAaPagPreUni;
import it.accenture.jnais.ws.redefines.GrzAaStab;
import it.accenture.jnais.ws.redefines.GrzDtDecor;
import it.accenture.jnais.ws.redefines.GrzDtEndCarz;
import it.accenture.jnais.ws.redefines.GrzDtIniValTar;
import it.accenture.jnais.ws.redefines.GrzDtPresc;
import it.accenture.jnais.ws.redefines.GrzDtScad;
import it.accenture.jnais.ws.redefines.GrzDtVarzTpIas;
import it.accenture.jnais.ws.redefines.GrzDurAa;
import it.accenture.jnais.ws.redefines.GrzDurGg;
import it.accenture.jnais.ws.redefines.GrzDurMm;
import it.accenture.jnais.ws.redefines.GrzEtaAa1oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaAa2oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaAa3oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaAScad;
import it.accenture.jnais.ws.redefines.GrzEtaMm1oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaMm2oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaMm3oAssto;
import it.accenture.jnais.ws.redefines.GrzFrazDecrCpt;
import it.accenture.jnais.ws.redefines.GrzFrazIniErogRen;
import it.accenture.jnais.ws.redefines.GrzId1oAssto;
import it.accenture.jnais.ws.redefines.GrzId2oAssto;
import it.accenture.jnais.ws.redefines.GrzId3oAssto;
import it.accenture.jnais.ws.redefines.GrzIdAdes;
import it.accenture.jnais.ws.redefines.GrzIdMoviChiu;
import it.accenture.jnais.ws.redefines.GrzMm1oRat;
import it.accenture.jnais.ws.redefines.GrzMmPagPreUni;
import it.accenture.jnais.ws.redefines.GrzNumAaPagPre;
import it.accenture.jnais.ws.redefines.GrzPc1oRat;
import it.accenture.jnais.ws.redefines.GrzPcOpz;
import it.accenture.jnais.ws.redefines.GrzPcRevrsb;
import it.accenture.jnais.ws.redefines.GrzPcRipPre;
import it.accenture.jnais.ws.redefines.GrzTpGar;
import it.accenture.jnais.ws.redefines.GrzTpInvst;
import it.accenture.jnais.ws.redefines.GrzTsStabLimitata;

/**Original name: GAR<br>
 * Variable: GAR from copybook IDBVGRZ1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class GarIvvs0216 {

    //==== PROPERTIES ====
    //Original name: GRZ-ID-GAR
    private int grzIdGar = DefaultValues.INT_VAL;
    //Original name: GRZ-ID-ADES
    private GrzIdAdes grzIdAdes = new GrzIdAdes();
    //Original name: GRZ-ID-POLI
    private int grzIdPoli = DefaultValues.INT_VAL;
    //Original name: GRZ-ID-MOVI-CRZ
    private int grzIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: GRZ-ID-MOVI-CHIU
    private GrzIdMoviChiu grzIdMoviChiu = new GrzIdMoviChiu();
    //Original name: GRZ-DT-INI-EFF
    private int grzDtIniEff = DefaultValues.INT_VAL;
    //Original name: GRZ-DT-END-EFF
    private int grzDtEndEff = DefaultValues.INT_VAL;
    //Original name: GRZ-COD-COMP-ANIA
    private int grzCodCompAnia = DefaultValues.INT_VAL;
    //Original name: GRZ-IB-OGG
    private String grzIbOgg = DefaultValues.stringVal(Len.GRZ_IB_OGG);
    //Original name: GRZ-DT-DECOR
    private GrzDtDecor grzDtDecor = new GrzDtDecor();
    //Original name: GRZ-DT-SCAD
    private GrzDtScad grzDtScad = new GrzDtScad();
    //Original name: GRZ-COD-SEZ
    private String grzCodSez = DefaultValues.stringVal(Len.GRZ_COD_SEZ);
    //Original name: GRZ-COD-TARI
    private String grzCodTari = DefaultValues.stringVal(Len.GRZ_COD_TARI);
    //Original name: GRZ-RAMO-BILA
    private String grzRamoBila = DefaultValues.stringVal(Len.GRZ_RAMO_BILA);
    //Original name: GRZ-DT-INI-VAL-TAR
    private GrzDtIniValTar grzDtIniValTar = new GrzDtIniValTar();
    //Original name: GRZ-ID-1O-ASSTO
    private GrzId1oAssto grzId1oAssto = new GrzId1oAssto();
    //Original name: GRZ-ID-2O-ASSTO
    private GrzId2oAssto grzId2oAssto = new GrzId2oAssto();
    //Original name: GRZ-ID-3O-ASSTO
    private GrzId3oAssto grzId3oAssto = new GrzId3oAssto();
    //Original name: GRZ-TP-GAR
    private GrzTpGar grzTpGar = new GrzTpGar();
    //Original name: GRZ-TP-RSH
    private String grzTpRsh = DefaultValues.stringVal(Len.GRZ_TP_RSH);
    //Original name: GRZ-TP-INVST
    private GrzTpInvst grzTpInvst = new GrzTpInvst();
    //Original name: GRZ-MOD-PAG-GARCOL
    private String grzModPagGarcol = DefaultValues.stringVal(Len.GRZ_MOD_PAG_GARCOL);
    //Original name: GRZ-TP-PER-PRE
    private String grzTpPerPre = DefaultValues.stringVal(Len.GRZ_TP_PER_PRE);
    //Original name: GRZ-ETA-AA-1O-ASSTO
    private GrzEtaAa1oAssto grzEtaAa1oAssto = new GrzEtaAa1oAssto();
    //Original name: GRZ-ETA-MM-1O-ASSTO
    private GrzEtaMm1oAssto grzEtaMm1oAssto = new GrzEtaMm1oAssto();
    //Original name: GRZ-ETA-AA-2O-ASSTO
    private GrzEtaAa2oAssto grzEtaAa2oAssto = new GrzEtaAa2oAssto();
    //Original name: GRZ-ETA-MM-2O-ASSTO
    private GrzEtaMm2oAssto grzEtaMm2oAssto = new GrzEtaMm2oAssto();
    //Original name: GRZ-ETA-AA-3O-ASSTO
    private GrzEtaAa3oAssto grzEtaAa3oAssto = new GrzEtaAa3oAssto();
    //Original name: GRZ-ETA-MM-3O-ASSTO
    private GrzEtaMm3oAssto grzEtaMm3oAssto = new GrzEtaMm3oAssto();
    //Original name: GRZ-TP-EMIS-PUR
    private char grzTpEmisPur = DefaultValues.CHAR_VAL;
    //Original name: GRZ-ETA-A-SCAD
    private GrzEtaAScad grzEtaAScad = new GrzEtaAScad();
    //Original name: GRZ-TP-CALC-PRE-PRSTZ
    private String grzTpCalcPrePrstz = DefaultValues.stringVal(Len.GRZ_TP_CALC_PRE_PRSTZ);
    //Original name: GRZ-TP-PRE
    private char grzTpPre = DefaultValues.CHAR_VAL;
    //Original name: GRZ-TP-DUR
    private String grzTpDur = DefaultValues.stringVal(Len.GRZ_TP_DUR);
    //Original name: GRZ-DUR-AA
    private GrzDurAa grzDurAa = new GrzDurAa();
    //Original name: GRZ-DUR-MM
    private GrzDurMm grzDurMm = new GrzDurMm();
    //Original name: GRZ-DUR-GG
    private GrzDurGg grzDurGg = new GrzDurGg();
    //Original name: GRZ-NUM-AA-PAG-PRE
    private GrzNumAaPagPre grzNumAaPagPre = new GrzNumAaPagPre();
    //Original name: GRZ-AA-PAG-PRE-UNI
    private GrzAaPagPreUni grzAaPagPreUni = new GrzAaPagPreUni();
    //Original name: GRZ-MM-PAG-PRE-UNI
    private GrzMmPagPreUni grzMmPagPreUni = new GrzMmPagPreUni();
    //Original name: GRZ-FRAZ-INI-EROG-REN
    private GrzFrazIniErogRen grzFrazIniErogRen = new GrzFrazIniErogRen();
    //Original name: GRZ-MM-1O-RAT
    private GrzMm1oRat grzMm1oRat = new GrzMm1oRat();
    //Original name: GRZ-PC-1O-RAT
    private GrzPc1oRat grzPc1oRat = new GrzPc1oRat();
    //Original name: GRZ-TP-PRSTZ-ASSTA
    private String grzTpPrstzAssta = DefaultValues.stringVal(Len.GRZ_TP_PRSTZ_ASSTA);
    //Original name: GRZ-DT-END-CARZ
    private GrzDtEndCarz grzDtEndCarz = new GrzDtEndCarz();
    //Original name: GRZ-PC-RIP-PRE
    private GrzPcRipPre grzPcRipPre = new GrzPcRipPre();
    //Original name: GRZ-COD-FND
    private String grzCodFnd = DefaultValues.stringVal(Len.GRZ_COD_FND);
    //Original name: GRZ-AA-REN-CER
    private String grzAaRenCer = DefaultValues.stringVal(Len.GRZ_AA_REN_CER);
    //Original name: GRZ-PC-REVRSB
    private GrzPcRevrsb grzPcRevrsb = new GrzPcRevrsb();
    //Original name: GRZ-TP-PC-RIP
    private String grzTpPcRip = DefaultValues.stringVal(Len.GRZ_TP_PC_RIP);
    //Original name: GRZ-PC-OPZ
    private GrzPcOpz grzPcOpz = new GrzPcOpz();
    //Original name: GRZ-TP-IAS
    private String grzTpIas = DefaultValues.stringVal(Len.GRZ_TP_IAS);
    //Original name: GRZ-TP-STAB
    private String grzTpStab = DefaultValues.stringVal(Len.GRZ_TP_STAB);
    //Original name: GRZ-TP-ADEG-PRE
    private char grzTpAdegPre = DefaultValues.CHAR_VAL;
    //Original name: GRZ-DT-VARZ-TP-IAS
    private GrzDtVarzTpIas grzDtVarzTpIas = new GrzDtVarzTpIas();
    //Original name: GRZ-FRAZ-DECR-CPT
    private GrzFrazDecrCpt grzFrazDecrCpt = new GrzFrazDecrCpt();
    //Original name: GRZ-COD-TRAT-RIASS
    private String grzCodTratRiass = DefaultValues.stringVal(Len.GRZ_COD_TRAT_RIASS);
    //Original name: GRZ-TP-DT-EMIS-RIASS
    private String grzTpDtEmisRiass = DefaultValues.stringVal(Len.GRZ_TP_DT_EMIS_RIASS);
    //Original name: GRZ-TP-CESS-RIASS
    private String grzTpCessRiass = DefaultValues.stringVal(Len.GRZ_TP_CESS_RIASS);
    //Original name: GRZ-DS-RIGA
    private long grzDsRiga = DefaultValues.LONG_VAL;
    //Original name: GRZ-DS-OPER-SQL
    private char grzDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: GRZ-DS-VER
    private int grzDsVer = DefaultValues.INT_VAL;
    //Original name: GRZ-DS-TS-INI-CPTZ
    private long grzDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: GRZ-DS-TS-END-CPTZ
    private long grzDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: GRZ-DS-UTENTE
    private String grzDsUtente = DefaultValues.stringVal(Len.GRZ_DS_UTENTE);
    //Original name: GRZ-DS-STATO-ELAB
    private char grzDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: GRZ-AA-STAB
    private GrzAaStab grzAaStab = new GrzAaStab();
    //Original name: GRZ-TS-STAB-LIMITATA
    private GrzTsStabLimitata grzTsStabLimitata = new GrzTsStabLimitata();
    //Original name: GRZ-DT-PRESC
    private GrzDtPresc grzDtPresc = new GrzDtPresc();
    //Original name: GRZ-RSH-INVST
    private char grzRshInvst = DefaultValues.CHAR_VAL;
    //Original name: GRZ-TP-RAMO-BILA
    private String grzTpRamoBila = DefaultValues.stringVal(Len.GRZ_TP_RAMO_BILA);

    //==== METHODS ====
    public void setGarFormatted(String data) {
        byte[] buffer = new byte[Len.GAR];
        MarshalByte.writeString(buffer, 1, data, Len.GAR);
        setGarBytes(buffer, 1);
    }

    public String getGarFormatted() {
        return MarshalByteExt.bufferToStr(getGarBytes());
    }

    public byte[] getGarBytes() {
        byte[] buffer = new byte[Len.GAR];
        return getGarBytes(buffer, 1);
    }

    public void setGarBytes(byte[] buffer, int offset) {
        int position = offset;
        grzIdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRZ_ID_GAR, 0);
        position += Len.GRZ_ID_GAR;
        grzIdAdes.setGrzIdAdesFromBuffer(buffer, position);
        position += GrzIdAdes.Len.GRZ_ID_ADES;
        grzIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRZ_ID_POLI, 0);
        position += Len.GRZ_ID_POLI;
        grzIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRZ_ID_MOVI_CRZ, 0);
        position += Len.GRZ_ID_MOVI_CRZ;
        grzIdMoviChiu.setGrzIdMoviChiuFromBuffer(buffer, position);
        position += GrzIdMoviChiu.Len.GRZ_ID_MOVI_CHIU;
        grzDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRZ_DT_INI_EFF, 0);
        position += Len.GRZ_DT_INI_EFF;
        grzDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRZ_DT_END_EFF, 0);
        position += Len.GRZ_DT_END_EFF;
        grzCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRZ_COD_COMP_ANIA, 0);
        position += Len.GRZ_COD_COMP_ANIA;
        grzIbOgg = MarshalByte.readString(buffer, position, Len.GRZ_IB_OGG);
        position += Len.GRZ_IB_OGG;
        grzDtDecor.setGrzDtDecorFromBuffer(buffer, position);
        position += GrzDtDecor.Len.GRZ_DT_DECOR;
        grzDtScad.setGrzDtScadFromBuffer(buffer, position);
        position += GrzDtScad.Len.GRZ_DT_SCAD;
        grzCodSez = MarshalByte.readString(buffer, position, Len.GRZ_COD_SEZ);
        position += Len.GRZ_COD_SEZ;
        grzCodTari = MarshalByte.readString(buffer, position, Len.GRZ_COD_TARI);
        position += Len.GRZ_COD_TARI;
        grzRamoBila = MarshalByte.readString(buffer, position, Len.GRZ_RAMO_BILA);
        position += Len.GRZ_RAMO_BILA;
        grzDtIniValTar.setGrzDtIniValTarFromBuffer(buffer, position);
        position += GrzDtIniValTar.Len.GRZ_DT_INI_VAL_TAR;
        grzId1oAssto.setGrzId1oAsstoFromBuffer(buffer, position);
        position += GrzId1oAssto.Len.GRZ_ID1O_ASSTO;
        grzId2oAssto.setGrzId2oAsstoFromBuffer(buffer, position);
        position += GrzId2oAssto.Len.GRZ_ID2O_ASSTO;
        grzId3oAssto.setGrzId3oAsstoFromBuffer(buffer, position);
        position += GrzId3oAssto.Len.GRZ_ID3O_ASSTO;
        grzTpGar.setGrzTpGarFromBuffer(buffer, position);
        position += GrzTpGar.Len.GRZ_TP_GAR;
        grzTpRsh = MarshalByte.readString(buffer, position, Len.GRZ_TP_RSH);
        position += Len.GRZ_TP_RSH;
        grzTpInvst.setGrzTpInvstFromBuffer(buffer, position);
        position += GrzTpInvst.Len.GRZ_TP_INVST;
        grzModPagGarcol = MarshalByte.readString(buffer, position, Len.GRZ_MOD_PAG_GARCOL);
        position += Len.GRZ_MOD_PAG_GARCOL;
        grzTpPerPre = MarshalByte.readString(buffer, position, Len.GRZ_TP_PER_PRE);
        position += Len.GRZ_TP_PER_PRE;
        grzEtaAa1oAssto.setGrzEtaAa1oAsstoFromBuffer(buffer, position);
        position += GrzEtaAa1oAssto.Len.GRZ_ETA_AA1O_ASSTO;
        grzEtaMm1oAssto.setGrzEtaMm1oAsstoFromBuffer(buffer, position);
        position += GrzEtaMm1oAssto.Len.GRZ_ETA_MM1O_ASSTO;
        grzEtaAa2oAssto.setGrzEtaAa2oAsstoFromBuffer(buffer, position);
        position += GrzEtaAa2oAssto.Len.GRZ_ETA_AA2O_ASSTO;
        grzEtaMm2oAssto.setGrzEtaMm2oAsstoFromBuffer(buffer, position);
        position += GrzEtaMm2oAssto.Len.GRZ_ETA_MM2O_ASSTO;
        grzEtaAa3oAssto.setGrzEtaAa3oAsstoFromBuffer(buffer, position);
        position += GrzEtaAa3oAssto.Len.GRZ_ETA_AA3O_ASSTO;
        grzEtaMm3oAssto.setGrzEtaMm3oAsstoFromBuffer(buffer, position);
        position += GrzEtaMm3oAssto.Len.GRZ_ETA_MM3O_ASSTO;
        grzTpEmisPur = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        grzEtaAScad.setGrzEtaAScadFromBuffer(buffer, position);
        position += GrzEtaAScad.Len.GRZ_ETA_A_SCAD;
        grzTpCalcPrePrstz = MarshalByte.readString(buffer, position, Len.GRZ_TP_CALC_PRE_PRSTZ);
        position += Len.GRZ_TP_CALC_PRE_PRSTZ;
        grzTpPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        grzTpDur = MarshalByte.readString(buffer, position, Len.GRZ_TP_DUR);
        position += Len.GRZ_TP_DUR;
        grzDurAa.setGrzDurAaFromBuffer(buffer, position);
        position += GrzDurAa.Len.GRZ_DUR_AA;
        grzDurMm.setGrzDurMmFromBuffer(buffer, position);
        position += GrzDurMm.Len.GRZ_DUR_MM;
        grzDurGg.setGrzDurGgFromBuffer(buffer, position);
        position += GrzDurGg.Len.GRZ_DUR_GG;
        grzNumAaPagPre.setGrzNumAaPagPreFromBuffer(buffer, position);
        position += GrzNumAaPagPre.Len.GRZ_NUM_AA_PAG_PRE;
        grzAaPagPreUni.setGrzAaPagPreUniFromBuffer(buffer, position);
        position += GrzAaPagPreUni.Len.GRZ_AA_PAG_PRE_UNI;
        grzMmPagPreUni.setGrzMmPagPreUniFromBuffer(buffer, position);
        position += GrzMmPagPreUni.Len.GRZ_MM_PAG_PRE_UNI;
        grzFrazIniErogRen.setGrzFrazIniErogRenFromBuffer(buffer, position);
        position += GrzFrazIniErogRen.Len.GRZ_FRAZ_INI_EROG_REN;
        grzMm1oRat.setGrzMm1oRatFromBuffer(buffer, position);
        position += GrzMm1oRat.Len.GRZ_MM1O_RAT;
        grzPc1oRat.setGrzPc1oRatFromBuffer(buffer, position);
        position += GrzPc1oRat.Len.GRZ_PC1O_RAT;
        grzTpPrstzAssta = MarshalByte.readString(buffer, position, Len.GRZ_TP_PRSTZ_ASSTA);
        position += Len.GRZ_TP_PRSTZ_ASSTA;
        grzDtEndCarz.setGrzDtEndCarzFromBuffer(buffer, position);
        position += GrzDtEndCarz.Len.GRZ_DT_END_CARZ;
        grzPcRipPre.setGrzPcRipPreFromBuffer(buffer, position);
        position += GrzPcRipPre.Len.GRZ_PC_RIP_PRE;
        grzCodFnd = MarshalByte.readString(buffer, position, Len.GRZ_COD_FND);
        position += Len.GRZ_COD_FND;
        grzAaRenCer = MarshalByte.readString(buffer, position, Len.GRZ_AA_REN_CER);
        position += Len.GRZ_AA_REN_CER;
        grzPcRevrsb.setGrzPcRevrsbFromBuffer(buffer, position);
        position += GrzPcRevrsb.Len.GRZ_PC_REVRSB;
        grzTpPcRip = MarshalByte.readString(buffer, position, Len.GRZ_TP_PC_RIP);
        position += Len.GRZ_TP_PC_RIP;
        grzPcOpz.setGrzPcOpzFromBuffer(buffer, position);
        position += GrzPcOpz.Len.GRZ_PC_OPZ;
        grzTpIas = MarshalByte.readString(buffer, position, Len.GRZ_TP_IAS);
        position += Len.GRZ_TP_IAS;
        grzTpStab = MarshalByte.readString(buffer, position, Len.GRZ_TP_STAB);
        position += Len.GRZ_TP_STAB;
        grzTpAdegPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        grzDtVarzTpIas.setGrzDtVarzTpIasFromBuffer(buffer, position);
        position += GrzDtVarzTpIas.Len.GRZ_DT_VARZ_TP_IAS;
        grzFrazDecrCpt.setGrzFrazDecrCptFromBuffer(buffer, position);
        position += GrzFrazDecrCpt.Len.GRZ_FRAZ_DECR_CPT;
        grzCodTratRiass = MarshalByte.readString(buffer, position, Len.GRZ_COD_TRAT_RIASS);
        position += Len.GRZ_COD_TRAT_RIASS;
        grzTpDtEmisRiass = MarshalByte.readString(buffer, position, Len.GRZ_TP_DT_EMIS_RIASS);
        position += Len.GRZ_TP_DT_EMIS_RIASS;
        grzTpCessRiass = MarshalByte.readString(buffer, position, Len.GRZ_TP_CESS_RIASS);
        position += Len.GRZ_TP_CESS_RIASS;
        grzDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.GRZ_DS_RIGA, 0);
        position += Len.GRZ_DS_RIGA;
        grzDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        grzDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GRZ_DS_VER, 0);
        position += Len.GRZ_DS_VER;
        grzDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.GRZ_DS_TS_INI_CPTZ, 0);
        position += Len.GRZ_DS_TS_INI_CPTZ;
        grzDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.GRZ_DS_TS_END_CPTZ, 0);
        position += Len.GRZ_DS_TS_END_CPTZ;
        grzDsUtente = MarshalByte.readString(buffer, position, Len.GRZ_DS_UTENTE);
        position += Len.GRZ_DS_UTENTE;
        grzDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        grzAaStab.setGrzAaStabFromBuffer(buffer, position);
        position += GrzAaStab.Len.GRZ_AA_STAB;
        grzTsStabLimitata.setGrzTsStabLimitataFromBuffer(buffer, position);
        position += GrzTsStabLimitata.Len.GRZ_TS_STAB_LIMITATA;
        grzDtPresc.setGrzDtPrescFromBuffer(buffer, position);
        position += GrzDtPresc.Len.GRZ_DT_PRESC;
        grzRshInvst = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        grzTpRamoBila = MarshalByte.readString(buffer, position, Len.GRZ_TP_RAMO_BILA);
    }

    public byte[] getGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, grzIdGar, Len.Int.GRZ_ID_GAR, 0);
        position += Len.GRZ_ID_GAR;
        grzIdAdes.getGrzIdAdesAsBuffer(buffer, position);
        position += GrzIdAdes.Len.GRZ_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, grzIdPoli, Len.Int.GRZ_ID_POLI, 0);
        position += Len.GRZ_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, grzIdMoviCrz, Len.Int.GRZ_ID_MOVI_CRZ, 0);
        position += Len.GRZ_ID_MOVI_CRZ;
        grzIdMoviChiu.getGrzIdMoviChiuAsBuffer(buffer, position);
        position += GrzIdMoviChiu.Len.GRZ_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, grzDtIniEff, Len.Int.GRZ_DT_INI_EFF, 0);
        position += Len.GRZ_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, grzDtEndEff, Len.Int.GRZ_DT_END_EFF, 0);
        position += Len.GRZ_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, grzCodCompAnia, Len.Int.GRZ_COD_COMP_ANIA, 0);
        position += Len.GRZ_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, grzIbOgg, Len.GRZ_IB_OGG);
        position += Len.GRZ_IB_OGG;
        grzDtDecor.getGrzDtDecorAsBuffer(buffer, position);
        position += GrzDtDecor.Len.GRZ_DT_DECOR;
        grzDtScad.getGrzDtScadAsBuffer(buffer, position);
        position += GrzDtScad.Len.GRZ_DT_SCAD;
        MarshalByte.writeString(buffer, position, grzCodSez, Len.GRZ_COD_SEZ);
        position += Len.GRZ_COD_SEZ;
        MarshalByte.writeString(buffer, position, grzCodTari, Len.GRZ_COD_TARI);
        position += Len.GRZ_COD_TARI;
        MarshalByte.writeString(buffer, position, grzRamoBila, Len.GRZ_RAMO_BILA);
        position += Len.GRZ_RAMO_BILA;
        grzDtIniValTar.getGrzDtIniValTarAsBuffer(buffer, position);
        position += GrzDtIniValTar.Len.GRZ_DT_INI_VAL_TAR;
        grzId1oAssto.getGrzId1oAsstoAsBuffer(buffer, position);
        position += GrzId1oAssto.Len.GRZ_ID1O_ASSTO;
        grzId2oAssto.getGrzId2oAsstoAsBuffer(buffer, position);
        position += GrzId2oAssto.Len.GRZ_ID2O_ASSTO;
        grzId3oAssto.getGrzId3oAsstoAsBuffer(buffer, position);
        position += GrzId3oAssto.Len.GRZ_ID3O_ASSTO;
        grzTpGar.getGrzTpGarAsBuffer(buffer, position);
        position += GrzTpGar.Len.GRZ_TP_GAR;
        MarshalByte.writeString(buffer, position, grzTpRsh, Len.GRZ_TP_RSH);
        position += Len.GRZ_TP_RSH;
        grzTpInvst.getGrzTpInvstAsBuffer(buffer, position);
        position += GrzTpInvst.Len.GRZ_TP_INVST;
        MarshalByte.writeString(buffer, position, grzModPagGarcol, Len.GRZ_MOD_PAG_GARCOL);
        position += Len.GRZ_MOD_PAG_GARCOL;
        MarshalByte.writeString(buffer, position, grzTpPerPre, Len.GRZ_TP_PER_PRE);
        position += Len.GRZ_TP_PER_PRE;
        grzEtaAa1oAssto.getGrzEtaAa1oAsstoAsBuffer(buffer, position);
        position += GrzEtaAa1oAssto.Len.GRZ_ETA_AA1O_ASSTO;
        grzEtaMm1oAssto.getGrzEtaMm1oAsstoAsBuffer(buffer, position);
        position += GrzEtaMm1oAssto.Len.GRZ_ETA_MM1O_ASSTO;
        grzEtaAa2oAssto.getGrzEtaAa2oAsstoAsBuffer(buffer, position);
        position += GrzEtaAa2oAssto.Len.GRZ_ETA_AA2O_ASSTO;
        grzEtaMm2oAssto.getGrzEtaMm2oAsstoAsBuffer(buffer, position);
        position += GrzEtaMm2oAssto.Len.GRZ_ETA_MM2O_ASSTO;
        grzEtaAa3oAssto.getGrzEtaAa3oAsstoAsBuffer(buffer, position);
        position += GrzEtaAa3oAssto.Len.GRZ_ETA_AA3O_ASSTO;
        grzEtaMm3oAssto.getGrzEtaMm3oAsstoAsBuffer(buffer, position);
        position += GrzEtaMm3oAssto.Len.GRZ_ETA_MM3O_ASSTO;
        MarshalByte.writeChar(buffer, position, grzTpEmisPur);
        position += Types.CHAR_SIZE;
        grzEtaAScad.getGrzEtaAScadAsBuffer(buffer, position);
        position += GrzEtaAScad.Len.GRZ_ETA_A_SCAD;
        MarshalByte.writeString(buffer, position, grzTpCalcPrePrstz, Len.GRZ_TP_CALC_PRE_PRSTZ);
        position += Len.GRZ_TP_CALC_PRE_PRSTZ;
        MarshalByte.writeChar(buffer, position, grzTpPre);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, grzTpDur, Len.GRZ_TP_DUR);
        position += Len.GRZ_TP_DUR;
        grzDurAa.getGrzDurAaAsBuffer(buffer, position);
        position += GrzDurAa.Len.GRZ_DUR_AA;
        grzDurMm.getGrzDurMmAsBuffer(buffer, position);
        position += GrzDurMm.Len.GRZ_DUR_MM;
        grzDurGg.getGrzDurGgAsBuffer(buffer, position);
        position += GrzDurGg.Len.GRZ_DUR_GG;
        grzNumAaPagPre.getGrzNumAaPagPreAsBuffer(buffer, position);
        position += GrzNumAaPagPre.Len.GRZ_NUM_AA_PAG_PRE;
        grzAaPagPreUni.getGrzAaPagPreUniAsBuffer(buffer, position);
        position += GrzAaPagPreUni.Len.GRZ_AA_PAG_PRE_UNI;
        grzMmPagPreUni.getGrzMmPagPreUniAsBuffer(buffer, position);
        position += GrzMmPagPreUni.Len.GRZ_MM_PAG_PRE_UNI;
        grzFrazIniErogRen.getGrzFrazIniErogRenAsBuffer(buffer, position);
        position += GrzFrazIniErogRen.Len.GRZ_FRAZ_INI_EROG_REN;
        grzMm1oRat.getGrzMm1oRatAsBuffer(buffer, position);
        position += GrzMm1oRat.Len.GRZ_MM1O_RAT;
        grzPc1oRat.getGrzPc1oRatAsBuffer(buffer, position);
        position += GrzPc1oRat.Len.GRZ_PC1O_RAT;
        MarshalByte.writeString(buffer, position, grzTpPrstzAssta, Len.GRZ_TP_PRSTZ_ASSTA);
        position += Len.GRZ_TP_PRSTZ_ASSTA;
        grzDtEndCarz.getGrzDtEndCarzAsBuffer(buffer, position);
        position += GrzDtEndCarz.Len.GRZ_DT_END_CARZ;
        grzPcRipPre.getGrzPcRipPreAsBuffer(buffer, position);
        position += GrzPcRipPre.Len.GRZ_PC_RIP_PRE;
        MarshalByte.writeString(buffer, position, grzCodFnd, Len.GRZ_COD_FND);
        position += Len.GRZ_COD_FND;
        MarshalByte.writeString(buffer, position, grzAaRenCer, Len.GRZ_AA_REN_CER);
        position += Len.GRZ_AA_REN_CER;
        grzPcRevrsb.getGrzPcRevrsbAsBuffer(buffer, position);
        position += GrzPcRevrsb.Len.GRZ_PC_REVRSB;
        MarshalByte.writeString(buffer, position, grzTpPcRip, Len.GRZ_TP_PC_RIP);
        position += Len.GRZ_TP_PC_RIP;
        grzPcOpz.getGrzPcOpzAsBuffer(buffer, position);
        position += GrzPcOpz.Len.GRZ_PC_OPZ;
        MarshalByte.writeString(buffer, position, grzTpIas, Len.GRZ_TP_IAS);
        position += Len.GRZ_TP_IAS;
        MarshalByte.writeString(buffer, position, grzTpStab, Len.GRZ_TP_STAB);
        position += Len.GRZ_TP_STAB;
        MarshalByte.writeChar(buffer, position, grzTpAdegPre);
        position += Types.CHAR_SIZE;
        grzDtVarzTpIas.getGrzDtVarzTpIasAsBuffer(buffer, position);
        position += GrzDtVarzTpIas.Len.GRZ_DT_VARZ_TP_IAS;
        grzFrazDecrCpt.getGrzFrazDecrCptAsBuffer(buffer, position);
        position += GrzFrazDecrCpt.Len.GRZ_FRAZ_DECR_CPT;
        MarshalByte.writeString(buffer, position, grzCodTratRiass, Len.GRZ_COD_TRAT_RIASS);
        position += Len.GRZ_COD_TRAT_RIASS;
        MarshalByte.writeString(buffer, position, grzTpDtEmisRiass, Len.GRZ_TP_DT_EMIS_RIASS);
        position += Len.GRZ_TP_DT_EMIS_RIASS;
        MarshalByte.writeString(buffer, position, grzTpCessRiass, Len.GRZ_TP_CESS_RIASS);
        position += Len.GRZ_TP_CESS_RIASS;
        MarshalByte.writeLongAsPacked(buffer, position, grzDsRiga, Len.Int.GRZ_DS_RIGA, 0);
        position += Len.GRZ_DS_RIGA;
        MarshalByte.writeChar(buffer, position, grzDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, grzDsVer, Len.Int.GRZ_DS_VER, 0);
        position += Len.GRZ_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, grzDsTsIniCptz, Len.Int.GRZ_DS_TS_INI_CPTZ, 0);
        position += Len.GRZ_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, grzDsTsEndCptz, Len.Int.GRZ_DS_TS_END_CPTZ, 0);
        position += Len.GRZ_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, grzDsUtente, Len.GRZ_DS_UTENTE);
        position += Len.GRZ_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, grzDsStatoElab);
        position += Types.CHAR_SIZE;
        grzAaStab.getGrzAaStabAsBuffer(buffer, position);
        position += GrzAaStab.Len.GRZ_AA_STAB;
        grzTsStabLimitata.getGrzTsStabLimitataAsBuffer(buffer, position);
        position += GrzTsStabLimitata.Len.GRZ_TS_STAB_LIMITATA;
        grzDtPresc.getGrzDtPrescAsBuffer(buffer, position);
        position += GrzDtPresc.Len.GRZ_DT_PRESC;
        MarshalByte.writeChar(buffer, position, grzRshInvst);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, grzTpRamoBila, Len.GRZ_TP_RAMO_BILA);
        return buffer;
    }

    public void setGrzIdGar(int grzIdGar) {
        this.grzIdGar = grzIdGar;
    }

    public int getGrzIdGar() {
        return this.grzIdGar;
    }

    public void setGrzIdPoli(int grzIdPoli) {
        this.grzIdPoli = grzIdPoli;
    }

    public int getGrzIdPoli() {
        return this.grzIdPoli;
    }

    public void setGrzIdMoviCrz(int grzIdMoviCrz) {
        this.grzIdMoviCrz = grzIdMoviCrz;
    }

    public int getGrzIdMoviCrz() {
        return this.grzIdMoviCrz;
    }

    public void setGrzDtIniEff(int grzDtIniEff) {
        this.grzDtIniEff = grzDtIniEff;
    }

    public int getGrzDtIniEff() {
        return this.grzDtIniEff;
    }

    public void setGrzDtEndEff(int grzDtEndEff) {
        this.grzDtEndEff = grzDtEndEff;
    }

    public int getGrzDtEndEff() {
        return this.grzDtEndEff;
    }

    public void setGrzCodCompAnia(int grzCodCompAnia) {
        this.grzCodCompAnia = grzCodCompAnia;
    }

    public int getGrzCodCompAnia() {
        return this.grzCodCompAnia;
    }

    public void setGrzIbOgg(String grzIbOgg) {
        this.grzIbOgg = Functions.subString(grzIbOgg, Len.GRZ_IB_OGG);
    }

    public String getGrzIbOgg() {
        return this.grzIbOgg;
    }

    public void setGrzCodSez(String grzCodSez) {
        this.grzCodSez = Functions.subString(grzCodSez, Len.GRZ_COD_SEZ);
    }

    public String getGrzCodSez() {
        return this.grzCodSez;
    }

    public String getGrzCodSezFormatted() {
        return Functions.padBlanks(getGrzCodSez(), Len.GRZ_COD_SEZ);
    }

    public void setGrzCodTari(String grzCodTari) {
        this.grzCodTari = Functions.subString(grzCodTari, Len.GRZ_COD_TARI);
    }

    public String getGrzCodTari() {
        return this.grzCodTari;
    }

    public void setGrzRamoBila(String grzRamoBila) {
        this.grzRamoBila = Functions.subString(grzRamoBila, Len.GRZ_RAMO_BILA);
    }

    public String getGrzRamoBila() {
        return this.grzRamoBila;
    }

    public String getGrzRamoBilaFormatted() {
        return Functions.padBlanks(getGrzRamoBila(), Len.GRZ_RAMO_BILA);
    }

    public void setGrzTpRsh(String grzTpRsh) {
        this.grzTpRsh = Functions.subString(grzTpRsh, Len.GRZ_TP_RSH);
    }

    public String getGrzTpRsh() {
        return this.grzTpRsh;
    }

    public String getGrzTpRshFormatted() {
        return Functions.padBlanks(getGrzTpRsh(), Len.GRZ_TP_RSH);
    }

    public void setGrzModPagGarcol(String grzModPagGarcol) {
        this.grzModPagGarcol = Functions.subString(grzModPagGarcol, Len.GRZ_MOD_PAG_GARCOL);
    }

    public String getGrzModPagGarcol() {
        return this.grzModPagGarcol;
    }

    public String getGrzModPagGarcolFormatted() {
        return Functions.padBlanks(getGrzModPagGarcol(), Len.GRZ_MOD_PAG_GARCOL);
    }

    public void setGrzTpPerPre(String grzTpPerPre) {
        this.grzTpPerPre = Functions.subString(grzTpPerPre, Len.GRZ_TP_PER_PRE);
    }

    public String getGrzTpPerPre() {
        return this.grzTpPerPre;
    }

    public String getGrzTpPerPreFormatted() {
        return Functions.padBlanks(getGrzTpPerPre(), Len.GRZ_TP_PER_PRE);
    }

    public void setGrzTpEmisPur(char grzTpEmisPur) {
        this.grzTpEmisPur = grzTpEmisPur;
    }

    public char getGrzTpEmisPur() {
        return this.grzTpEmisPur;
    }

    public void setGrzTpCalcPrePrstz(String grzTpCalcPrePrstz) {
        this.grzTpCalcPrePrstz = Functions.subString(grzTpCalcPrePrstz, Len.GRZ_TP_CALC_PRE_PRSTZ);
    }

    public String getGrzTpCalcPrePrstz() {
        return this.grzTpCalcPrePrstz;
    }

    public String getGrzTpCalcPrePrstzFormatted() {
        return Functions.padBlanks(getGrzTpCalcPrePrstz(), Len.GRZ_TP_CALC_PRE_PRSTZ);
    }

    public void setGrzTpPre(char grzTpPre) {
        this.grzTpPre = grzTpPre;
    }

    public char getGrzTpPre() {
        return this.grzTpPre;
    }

    public void setGrzTpDur(String grzTpDur) {
        this.grzTpDur = Functions.subString(grzTpDur, Len.GRZ_TP_DUR);
    }

    public String getGrzTpDur() {
        return this.grzTpDur;
    }

    public String getGrzTpDurFormatted() {
        return Functions.padBlanks(getGrzTpDur(), Len.GRZ_TP_DUR);
    }

    public void setGrzTpPrstzAssta(String grzTpPrstzAssta) {
        this.grzTpPrstzAssta = Functions.subString(grzTpPrstzAssta, Len.GRZ_TP_PRSTZ_ASSTA);
    }

    public String getGrzTpPrstzAssta() {
        return this.grzTpPrstzAssta;
    }

    public String getGrzTpPrstzAsstaFormatted() {
        return Functions.padBlanks(getGrzTpPrstzAssta(), Len.GRZ_TP_PRSTZ_ASSTA);
    }

    public void setGrzCodFnd(String grzCodFnd) {
        this.grzCodFnd = Functions.subString(grzCodFnd, Len.GRZ_COD_FND);
    }

    public String getGrzCodFnd() {
        return this.grzCodFnd;
    }

    public String getGrzCodFndFormatted() {
        return Functions.padBlanks(getGrzCodFnd(), Len.GRZ_COD_FND);
    }

    public void setGrzAaRenCer(String grzAaRenCer) {
        this.grzAaRenCer = Functions.subString(grzAaRenCer, Len.GRZ_AA_REN_CER);
    }

    public String getGrzAaRenCer() {
        return this.grzAaRenCer;
    }

    public String getGrzAaRenCerFormatted() {
        return Functions.padBlanks(getGrzAaRenCer(), Len.GRZ_AA_REN_CER);
    }

    public void setGrzTpPcRip(String grzTpPcRip) {
        this.grzTpPcRip = Functions.subString(grzTpPcRip, Len.GRZ_TP_PC_RIP);
    }

    public String getGrzTpPcRip() {
        return this.grzTpPcRip;
    }

    public String getGrzTpPcRipFormatted() {
        return Functions.padBlanks(getGrzTpPcRip(), Len.GRZ_TP_PC_RIP);
    }

    public void setGrzTpIas(String grzTpIas) {
        this.grzTpIas = Functions.subString(grzTpIas, Len.GRZ_TP_IAS);
    }

    public String getGrzTpIas() {
        return this.grzTpIas;
    }

    public String getGrzTpIasFormatted() {
        return Functions.padBlanks(getGrzTpIas(), Len.GRZ_TP_IAS);
    }

    public void setGrzTpStab(String grzTpStab) {
        this.grzTpStab = Functions.subString(grzTpStab, Len.GRZ_TP_STAB);
    }

    public String getGrzTpStab() {
        return this.grzTpStab;
    }

    public String getGrzTpStabFormatted() {
        return Functions.padBlanks(getGrzTpStab(), Len.GRZ_TP_STAB);
    }

    public void setGrzTpAdegPre(char grzTpAdegPre) {
        this.grzTpAdegPre = grzTpAdegPre;
    }

    public char getGrzTpAdegPre() {
        return this.grzTpAdegPre;
    }

    public void setGrzCodTratRiass(String grzCodTratRiass) {
        this.grzCodTratRiass = Functions.subString(grzCodTratRiass, Len.GRZ_COD_TRAT_RIASS);
    }

    public String getGrzCodTratRiass() {
        return this.grzCodTratRiass;
    }

    public String getGrzCodTratRiassFormatted() {
        return Functions.padBlanks(getGrzCodTratRiass(), Len.GRZ_COD_TRAT_RIASS);
    }

    public void setGrzTpDtEmisRiass(String grzTpDtEmisRiass) {
        this.grzTpDtEmisRiass = Functions.subString(grzTpDtEmisRiass, Len.GRZ_TP_DT_EMIS_RIASS);
    }

    public String getGrzTpDtEmisRiass() {
        return this.grzTpDtEmisRiass;
    }

    public String getGrzTpDtEmisRiassFormatted() {
        return Functions.padBlanks(getGrzTpDtEmisRiass(), Len.GRZ_TP_DT_EMIS_RIASS);
    }

    public void setGrzTpCessRiass(String grzTpCessRiass) {
        this.grzTpCessRiass = Functions.subString(grzTpCessRiass, Len.GRZ_TP_CESS_RIASS);
    }

    public String getGrzTpCessRiass() {
        return this.grzTpCessRiass;
    }

    public String getGrzTpCessRiassFormatted() {
        return Functions.padBlanks(getGrzTpCessRiass(), Len.GRZ_TP_CESS_RIASS);
    }

    public void setGrzDsRiga(long grzDsRiga) {
        this.grzDsRiga = grzDsRiga;
    }

    public long getGrzDsRiga() {
        return this.grzDsRiga;
    }

    public void setGrzDsOperSql(char grzDsOperSql) {
        this.grzDsOperSql = grzDsOperSql;
    }

    public char getGrzDsOperSql() {
        return this.grzDsOperSql;
    }

    public void setGrzDsVer(int grzDsVer) {
        this.grzDsVer = grzDsVer;
    }

    public int getGrzDsVer() {
        return this.grzDsVer;
    }

    public void setGrzDsTsIniCptz(long grzDsTsIniCptz) {
        this.grzDsTsIniCptz = grzDsTsIniCptz;
    }

    public long getGrzDsTsIniCptz() {
        return this.grzDsTsIniCptz;
    }

    public void setGrzDsTsEndCptz(long grzDsTsEndCptz) {
        this.grzDsTsEndCptz = grzDsTsEndCptz;
    }

    public long getGrzDsTsEndCptz() {
        return this.grzDsTsEndCptz;
    }

    public void setGrzDsUtente(String grzDsUtente) {
        this.grzDsUtente = Functions.subString(grzDsUtente, Len.GRZ_DS_UTENTE);
    }

    public String getGrzDsUtente() {
        return this.grzDsUtente;
    }

    public void setGrzDsStatoElab(char grzDsStatoElab) {
        this.grzDsStatoElab = grzDsStatoElab;
    }

    public char getGrzDsStatoElab() {
        return this.grzDsStatoElab;
    }

    public void setGrzRshInvst(char grzRshInvst) {
        this.grzRshInvst = grzRshInvst;
    }

    public char getGrzRshInvst() {
        return this.grzRshInvst;
    }

    public void setGrzTpRamoBila(String grzTpRamoBila) {
        this.grzTpRamoBila = Functions.subString(grzTpRamoBila, Len.GRZ_TP_RAMO_BILA);
    }

    public String getGrzTpRamoBila() {
        return this.grzTpRamoBila;
    }

    public GrzAaPagPreUni getGrzAaPagPreUni() {
        return grzAaPagPreUni;
    }

    public GrzAaStab getGrzAaStab() {
        return grzAaStab;
    }

    public GrzDtDecor getGrzDtDecor() {
        return grzDtDecor;
    }

    public GrzDtEndCarz getGrzDtEndCarz() {
        return grzDtEndCarz;
    }

    public GrzDtIniValTar getGrzDtIniValTar() {
        return grzDtIniValTar;
    }

    public GrzDtPresc getGrzDtPresc() {
        return grzDtPresc;
    }

    public GrzDtScad getGrzDtScad() {
        return grzDtScad;
    }

    public GrzDtVarzTpIas getGrzDtVarzTpIas() {
        return grzDtVarzTpIas;
    }

    public GrzDurAa getGrzDurAa() {
        return grzDurAa;
    }

    public GrzDurGg getGrzDurGg() {
        return grzDurGg;
    }

    public GrzDurMm getGrzDurMm() {
        return grzDurMm;
    }

    public GrzEtaAScad getGrzEtaAScad() {
        return grzEtaAScad;
    }

    public GrzEtaAa1oAssto getGrzEtaAa1oAssto() {
        return grzEtaAa1oAssto;
    }

    public GrzEtaAa2oAssto getGrzEtaAa2oAssto() {
        return grzEtaAa2oAssto;
    }

    public GrzEtaAa3oAssto getGrzEtaAa3oAssto() {
        return grzEtaAa3oAssto;
    }

    public GrzEtaMm1oAssto getGrzEtaMm1oAssto() {
        return grzEtaMm1oAssto;
    }

    public GrzEtaMm2oAssto getGrzEtaMm2oAssto() {
        return grzEtaMm2oAssto;
    }

    public GrzEtaMm3oAssto getGrzEtaMm3oAssto() {
        return grzEtaMm3oAssto;
    }

    public GrzFrazDecrCpt getGrzFrazDecrCpt() {
        return grzFrazDecrCpt;
    }

    public GrzFrazIniErogRen getGrzFrazIniErogRen() {
        return grzFrazIniErogRen;
    }

    public GrzId1oAssto getGrzId1oAssto() {
        return grzId1oAssto;
    }

    public GrzId2oAssto getGrzId2oAssto() {
        return grzId2oAssto;
    }

    public GrzId3oAssto getGrzId3oAssto() {
        return grzId3oAssto;
    }

    public GrzIdAdes getGrzIdAdes() {
        return grzIdAdes;
    }

    public GrzIdMoviChiu getGrzIdMoviChiu() {
        return grzIdMoviChiu;
    }

    public GrzMm1oRat getGrzMm1oRat() {
        return grzMm1oRat;
    }

    public GrzMmPagPreUni getGrzMmPagPreUni() {
        return grzMmPagPreUni;
    }

    public GrzNumAaPagPre getGrzNumAaPagPre() {
        return grzNumAaPagPre;
    }

    public GrzPc1oRat getGrzPc1oRat() {
        return grzPc1oRat;
    }

    public GrzPcOpz getGrzPcOpz() {
        return grzPcOpz;
    }

    public GrzPcRevrsb getGrzPcRevrsb() {
        return grzPcRevrsb;
    }

    public GrzPcRipPre getGrzPcRipPre() {
        return grzPcRipPre;
    }

    public GrzTpGar getGrzTpGar() {
        return grzTpGar;
    }

    public GrzTpInvst getGrzTpInvst() {
        return grzTpInvst;
    }

    public GrzTsStabLimitata getGrzTsStabLimitata() {
        return grzTsStabLimitata;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_IB_OGG = 40;
        public static final int GRZ_COD_SEZ = 12;
        public static final int GRZ_COD_TARI = 12;
        public static final int GRZ_RAMO_BILA = 12;
        public static final int GRZ_TP_RSH = 2;
        public static final int GRZ_MOD_PAG_GARCOL = 2;
        public static final int GRZ_TP_PER_PRE = 2;
        public static final int GRZ_TP_CALC_PRE_PRSTZ = 2;
        public static final int GRZ_TP_DUR = 2;
        public static final int GRZ_TP_PRSTZ_ASSTA = 2;
        public static final int GRZ_COD_FND = 12;
        public static final int GRZ_AA_REN_CER = 18;
        public static final int GRZ_TP_PC_RIP = 2;
        public static final int GRZ_TP_IAS = 2;
        public static final int GRZ_TP_STAB = 2;
        public static final int GRZ_COD_TRAT_RIASS = 12;
        public static final int GRZ_TP_DT_EMIS_RIASS = 2;
        public static final int GRZ_TP_CESS_RIASS = 2;
        public static final int GRZ_DS_UTENTE = 20;
        public static final int GRZ_TP_RAMO_BILA = 2;
        public static final int GRZ_ID_GAR = 5;
        public static final int GRZ_ID_POLI = 5;
        public static final int GRZ_ID_MOVI_CRZ = 5;
        public static final int GRZ_DT_INI_EFF = 5;
        public static final int GRZ_DT_END_EFF = 5;
        public static final int GRZ_COD_COMP_ANIA = 3;
        public static final int GRZ_TP_EMIS_PUR = 1;
        public static final int GRZ_TP_PRE = 1;
        public static final int GRZ_TP_ADEG_PRE = 1;
        public static final int GRZ_DS_RIGA = 6;
        public static final int GRZ_DS_OPER_SQL = 1;
        public static final int GRZ_DS_VER = 5;
        public static final int GRZ_DS_TS_INI_CPTZ = 10;
        public static final int GRZ_DS_TS_END_CPTZ = 10;
        public static final int GRZ_DS_STATO_ELAB = 1;
        public static final int GRZ_RSH_INVST = 1;
        public static final int GAR = GRZ_ID_GAR + GrzIdAdes.Len.GRZ_ID_ADES + GRZ_ID_POLI + GRZ_ID_MOVI_CRZ + GrzIdMoviChiu.Len.GRZ_ID_MOVI_CHIU + GRZ_DT_INI_EFF + GRZ_DT_END_EFF + GRZ_COD_COMP_ANIA + GRZ_IB_OGG + GrzDtDecor.Len.GRZ_DT_DECOR + GrzDtScad.Len.GRZ_DT_SCAD + GRZ_COD_SEZ + GRZ_COD_TARI + GRZ_RAMO_BILA + GrzDtIniValTar.Len.GRZ_DT_INI_VAL_TAR + GrzId1oAssto.Len.GRZ_ID1O_ASSTO + GrzId2oAssto.Len.GRZ_ID2O_ASSTO + GrzId3oAssto.Len.GRZ_ID3O_ASSTO + GrzTpGar.Len.GRZ_TP_GAR + GRZ_TP_RSH + GrzTpInvst.Len.GRZ_TP_INVST + GRZ_MOD_PAG_GARCOL + GRZ_TP_PER_PRE + GrzEtaAa1oAssto.Len.GRZ_ETA_AA1O_ASSTO + GrzEtaMm1oAssto.Len.GRZ_ETA_MM1O_ASSTO + GrzEtaAa2oAssto.Len.GRZ_ETA_AA2O_ASSTO + GrzEtaMm2oAssto.Len.GRZ_ETA_MM2O_ASSTO + GrzEtaAa3oAssto.Len.GRZ_ETA_AA3O_ASSTO + GrzEtaMm3oAssto.Len.GRZ_ETA_MM3O_ASSTO + GRZ_TP_EMIS_PUR + GrzEtaAScad.Len.GRZ_ETA_A_SCAD + GRZ_TP_CALC_PRE_PRSTZ + GRZ_TP_PRE + GRZ_TP_DUR + GrzDurAa.Len.GRZ_DUR_AA + GrzDurMm.Len.GRZ_DUR_MM + GrzDurGg.Len.GRZ_DUR_GG + GrzNumAaPagPre.Len.GRZ_NUM_AA_PAG_PRE + GrzAaPagPreUni.Len.GRZ_AA_PAG_PRE_UNI + GrzMmPagPreUni.Len.GRZ_MM_PAG_PRE_UNI + GrzFrazIniErogRen.Len.GRZ_FRAZ_INI_EROG_REN + GrzMm1oRat.Len.GRZ_MM1O_RAT + GrzPc1oRat.Len.GRZ_PC1O_RAT + GRZ_TP_PRSTZ_ASSTA + GrzDtEndCarz.Len.GRZ_DT_END_CARZ + GrzPcRipPre.Len.GRZ_PC_RIP_PRE + GRZ_COD_FND + GRZ_AA_REN_CER + GrzPcRevrsb.Len.GRZ_PC_REVRSB + GRZ_TP_PC_RIP + GrzPcOpz.Len.GRZ_PC_OPZ + GRZ_TP_IAS + GRZ_TP_STAB + GRZ_TP_ADEG_PRE + GrzDtVarzTpIas.Len.GRZ_DT_VARZ_TP_IAS + GrzFrazDecrCpt.Len.GRZ_FRAZ_DECR_CPT + GRZ_COD_TRAT_RIASS + GRZ_TP_DT_EMIS_RIASS + GRZ_TP_CESS_RIASS + GRZ_DS_RIGA + GRZ_DS_OPER_SQL + GRZ_DS_VER + GRZ_DS_TS_INI_CPTZ + GRZ_DS_TS_END_CPTZ + GRZ_DS_UTENTE + GRZ_DS_STATO_ELAB + GrzAaStab.Len.GRZ_AA_STAB + GrzTsStabLimitata.Len.GRZ_TS_STAB_LIMITATA + GrzDtPresc.Len.GRZ_DT_PRESC + GRZ_RSH_INVST + GRZ_TP_RAMO_BILA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_ID_GAR = 9;
            public static final int GRZ_ID_POLI = 9;
            public static final int GRZ_ID_MOVI_CRZ = 9;
            public static final int GRZ_DT_INI_EFF = 8;
            public static final int GRZ_DT_END_EFF = 8;
            public static final int GRZ_COD_COMP_ANIA = 5;
            public static final int GRZ_DS_RIGA = 10;
            public static final int GRZ_DS_VER = 9;
            public static final int GRZ_DS_TS_INI_CPTZ = 18;
            public static final int GRZ_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
