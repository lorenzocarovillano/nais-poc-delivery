package it.accenture.jnais.copy;

/**Original name: WS-DESC-TP-MOVI<br>
 * Variable: WS-DESC-TP-MOVI from copybook LRGC0661<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDescTpMovi {

    //==== PROPERTIES ====
    //Original name: TP-VERS-PREMI
    private String versPremi = "Versamento premi";
    //Original name: TP-SW-IN-1
    private String swIn1 = "Switch in ingresso";
    //Original name: TP-SW-OUT-1
    private String swOut1 = "Switch in uscita";
    //Original name: TP-SW-IN-2
    private String swIn2 = "Switch in ingresso quick";
    //Original name: TP-SW-OUT-2
    private String swOut2 = "Switch in uscita quick";
    //Original name: TP-SW-IN-3
    private String swIn3 = "Switch in ingresso quick";
    //Original name: TP-SW-OUT-3
    private String swOut3 = "Switch in uscita quick";
    //Original name: TP-SW-IN-4
    private String swIn4 = "Switch in ingresso massivo";
    //Original name: TP-SW-OUT-4
    private String swOut4 = "Switch in uscita massivo";
    //Original name: TP-SW-IN-5
    private String swIn5 = "Switch in ingresso da ribilanciamento";
    //Original name: TP-SW-OUT-5
    private String swOut5 = "Switch in uscita da ribilanciamento";
    //Original name: TP-SW-IN-6
    private String swIn6 = "Switch in ingresso da chiusura fondi";
    //Original name: TP-SW-OUT-6
    private String swOut6 = "Switch in uscita da chiusura fondi";
    //Original name: TP-SW-IN-7
    private String swIn7 = "Ribilanciamento automatico in ingresso";
    //Original name: TP-SW-OUT-7
    private String swOut7 = "Ribilanciamento automatico in uscita";
    //Original name: TP-SW-IN-8
    private String swIn8 = "Ribilanciamento periodico in ingresso";
    //Original name: TP-SW-OUT-8
    private String swOut8 = "Ribilanciamento periodico in uscita";
    /**Original name: TP-CASO-MORTE<br>
	 * <pre>    03 TP-SW-IN                      PIC X(050) VALUE
	 *        'Switch in ingresso                                '.
	 *     03 TP-SW-OUT                     PIC X(050) VALUE
	 *        'Switch in uscita                                  '.</pre>*/
    private String casoMorte = "Costo caso morte";
    //Original name: TP-PAG-CEDOLA
    private String pagCedola = "Pagamento cedola";
    //Original name: TP-COMM-GEST
    private String commGest = "Commissioni di gestione";
    //Original name: TP-REBATE
    private String rebate = "Rebate";
    //Original name: TP-IMP-SOST
    private String impSost = "Calcolo imposta sostitutiva";
    //Original name: TP-IN-SW-PASSO-PASSO
    private String inSwPassoPasso = "Switch da Passo dopo Passo in ingresso";
    //Original name: TP-OUT-SW-PASSO-PASSO
    private String outSwPassoPasso = "Switch da Passo dopo Passo in uscita";
    //Original name: TP-IN-SW-STOP-LOSS
    private String inSwStopLoss = "Switch da Stop Loss in ingresso";
    //Original name: TP-IN-SW-LINEA
    private String inSwLinea = "Switch di Linea in ingresso";
    //Original name: TP-OUT-SW-STOP-LOSS
    private String outSwStopLoss = "Switch da Stop Loss in uscita";
    //Original name: TP-OUT-SW-LINEA
    private String outSwLinea = "Switch di Linea in uscita";
    /**Original name: TP-IN-SW-BATCH-PARZ-MASS<br>
	 * <pre>        'Switch batch massivo parziale  in ingresso       '.</pre>*/
    private String inSwBatchParzMass = "Switch batch massivo automatico in ingresso";
    //Original name: TP-OUT-SW-BATCH-PARZ-MASS
    private String outSwBatchParzMass = "Switch batch massivo automatico in uscita";
    //Original name: TP-COMP-NAV-NEG
    private String compNavNeg = "Disinvestimento per operazione straordinaria";
    //Original name: TP-COMP-NAV-POS
    private String compNavPos = "Investimento per operazione straordinaria";
    //Original name: TP-IN-SW-RIBIL
    private String inSwRibil = "Switch Ribilanciamento fnz in ingresso";
    //Original name: TP-IN-SW-GAP-EVENT
    private String inSwGapEvent = "Switch Gap Event FNZ   in ingresso";
    //Original name: TP-IN-SW-SUP-SOGLIA
    private String inSwSupSoglia = "Switch Superamento Soglia FNZ in ingresso";
    //Original name: TP-IN-SW-MASSIVO-FNZ
    private String inSwMassivoFnz = "Switch Massivo FNZ in ingresso";
    //Original name: TP-OUT-SW-RIBIL
    private String outSwRibil = "Switch Ribilanciamento fnz in uscita";
    //Original name: TP-OUT-SW-GAP-EVENT
    private String outSwGapEvent = "Switch Gap Event FNZ   in uscita";
    //Original name: TP-OUT-SW-SUP-SOGLIA
    private String outSwSupSoglia = "Switch Superamento Soglia FNZ in uscita";
    //Original name: TP-OUT-SW-MASSIVO-FNZ
    private String outSwMassivoFnz = "Switch Massivo FNZ in uscita";

    //==== METHODS ====
    public String getVersPremi() {
        return this.versPremi;
    }

    public String getSwIn1() {
        return this.swIn1;
    }

    public String getSwOut1() {
        return this.swOut1;
    }

    public String getSwIn2() {
        return this.swIn2;
    }

    public String getSwOut2() {
        return this.swOut2;
    }

    public String getSwIn3() {
        return this.swIn3;
    }

    public String getSwOut3() {
        return this.swOut3;
    }

    public String getSwIn4() {
        return this.swIn4;
    }

    public String getSwOut4() {
        return this.swOut4;
    }

    public String getSwIn5() {
        return this.swIn5;
    }

    public String getSwOut5() {
        return this.swOut5;
    }

    public String getSwIn6() {
        return this.swIn6;
    }

    public String getSwOut6() {
        return this.swOut6;
    }

    public String getSwIn7() {
        return this.swIn7;
    }

    public String getSwOut7() {
        return this.swOut7;
    }

    public String getSwIn8() {
        return this.swIn8;
    }

    public String getSwOut8() {
        return this.swOut8;
    }

    public String getCasoMorte() {
        return this.casoMorte;
    }

    public String getPagCedola() {
        return this.pagCedola;
    }

    public String getCommGest() {
        return this.commGest;
    }

    public String getRebate() {
        return this.rebate;
    }

    public String getImpSost() {
        return this.impSost;
    }

    public String getInSwPassoPasso() {
        return this.inSwPassoPasso;
    }

    public String getOutSwPassoPasso() {
        return this.outSwPassoPasso;
    }

    public String getInSwStopLoss() {
        return this.inSwStopLoss;
    }

    public String getInSwLinea() {
        return this.inSwLinea;
    }

    public String getOutSwStopLoss() {
        return this.outSwStopLoss;
    }

    public String getOutSwLinea() {
        return this.outSwLinea;
    }

    public String getInSwBatchParzMass() {
        return this.inSwBatchParzMass;
    }

    public String getOutSwBatchParzMass() {
        return this.outSwBatchParzMass;
    }

    public String getCompNavNeg() {
        return this.compNavNeg;
    }

    public String getCompNavPos() {
        return this.compNavPos;
    }

    public String getInSwRibil() {
        return this.inSwRibil;
    }

    public String getInSwGapEvent() {
        return this.inSwGapEvent;
    }

    public String getInSwSupSoglia() {
        return this.inSwSupSoglia;
    }

    public String getInSwMassivoFnz() {
        return this.inSwMassivoFnz;
    }

    public String getOutSwRibil() {
        return this.outSwRibil;
    }

    public String getOutSwGapEvent() {
        return this.outSwGapEvent;
    }

    public String getOutSwSupSoglia() {
        return this.outSwSupSoglia;
    }

    public String getOutSwMassivoFnz() {
        return this.outSwMassivoFnz;
    }
}
