package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WB03AaRenCerLlbs0230;
import it.accenture.jnais.ws.redefines.WB03AbbLlbs0230;
import it.accenture.jnais.ws.redefines.WB03AcqExpLlbs0230;
import it.accenture.jnais.ws.redefines.WB03AlqMargCSubrshLlbs0230;
import it.accenture.jnais.ws.redefines.WB03AlqMargRisLlbs0230;
import it.accenture.jnais.ws.redefines.WB03AlqRetrTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03AntidurCalc365Llbs0230;
import it.accenture.jnais.ws.redefines.WB03AntidurDtCalcLlbs0230;
import it.accenture.jnais.ws.redefines.WB03AntidurRicorPrecLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CarAcqNonSconLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CarAcqPrecontatoLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CarGestLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CarGestNonSconLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CarIncLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CarIncNonSconLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CarzLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CodAgeLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CodCanLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CodPrdtLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CodSubageLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CoeffOpzCptLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CoeffOpzRenLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CoeffRis1TLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CoeffRis2TLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CommisInterLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CptAsstoIniMorLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CptDtRidzLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CptDtStabLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CptRiastoEccLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CptRiastoLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CptRshMorLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CSubrshTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03CumRiscparLlbs0230;
import it.accenture.jnais.ws.redefines.WB03DirEmisLlbs0230;
import it.accenture.jnais.ws.redefines.WB03DirLlbs0230;
import it.accenture.jnais.ws.redefines.WB03Dur1oPerAaLlbs0230;
import it.accenture.jnais.ws.redefines.WB03Dur1oPerGgLlbs0230;
import it.accenture.jnais.ws.redefines.WB03Dur1oPerMmLlbs0230;
import it.accenture.jnais.ws.redefines.WB03DurAaLlbs0230;
import it.accenture.jnais.ws.redefines.WB03DurGarAaLlbs0230;
import it.accenture.jnais.ws.redefines.WB03DurGarGgLlbs0230;
import it.accenture.jnais.ws.redefines.WB03DurGarMmLlbs0230;
import it.accenture.jnais.ws.redefines.WB03DurGgLlbs0230;
import it.accenture.jnais.ws.redefines.WB03DurMmLlbs0230;
import it.accenture.jnais.ws.redefines.WB03DurPagPreLlbs0230;
import it.accenture.jnais.ws.redefines.WB03DurPagRenLlbs0230;
import it.accenture.jnais.ws.redefines.WB03DurResDtCalcLlbs0230;
import it.accenture.jnais.ws.redefines.WB03EtaAa1oAsstoLlbs0230;
import it.accenture.jnais.ws.redefines.WB03EtaMm1oAsstoLlbs0230;
import it.accenture.jnais.ws.redefines.WB03EtaRaggnDtCalcLlbs0230;
import it.accenture.jnais.ws.redefines.WB03FrazDecrCptLlbs0230;
import it.accenture.jnais.ws.redefines.WB03FrazIniErogRenLlbs0230;
import it.accenture.jnais.ws.redefines.WB03FrazLlbs0230;
import it.accenture.jnais.ws.redefines.WB03IdRichEstrazAggLlbs0230;
import it.accenture.jnais.ws.redefines.WB03ImpCarCasoMorLlbs0230;
import it.accenture.jnais.ws.redefines.WB03IntrTecnLlbs0230;
import it.accenture.jnais.ws.redefines.WB03MetRiscSpclLlbs0230;
import it.accenture.jnais.ws.redefines.WB03MinGartoTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03MinTrnutTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03NsQuoLlbs0230;
import it.accenture.jnais.ws.redefines.WB03NumPrePattLlbs0230;
import it.accenture.jnais.ws.redefines.WB03OverCommLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PcCarAcqLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PcCarMorLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PreAnnualizRicorLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PreContLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PreDovIniLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PreDovRivtoTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PrePattuitoIniLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PrePpIniLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PrePpUltLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PreRiastoEccLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PreRiastoLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PreRshTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03ProvAcqLlbs0230;
import it.accenture.jnais.ws.redefines.WB03ProvAcqRicorLlbs0230;
import it.accenture.jnais.ws.redefines.WB03ProvIncLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PrstzAggIniLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PrstzAggUltLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PrstzIniLlbs0230;
import it.accenture.jnais.ws.redefines.WB03PrstzTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03QtzSpZCoupDtCLlbs0230;
import it.accenture.jnais.ws.redefines.WB03QtzSpZCoupEmisLlbs0230;
import it.accenture.jnais.ws.redefines.WB03QtzSpZOpzDtCaLlbs0230;
import it.accenture.jnais.ws.redefines.WB03QtzSpZOpzEmisLlbs0230;
import it.accenture.jnais.ws.redefines.WB03QtzTotDtCalcLlbs0230;
import it.accenture.jnais.ws.redefines.WB03QtzTotDtUltBilLlbs0230;
import it.accenture.jnais.ws.redefines.WB03QtzTotEmisLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RappelLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RatRenLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RemunAssLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RisAcqTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RiscparLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RisMatChiuPrecLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RisPuraTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RisRiastaEccLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RisRiastaLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RisRistorniCapLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RisSpeTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03RisZilTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03TsMedioLlbs0230;
import it.accenture.jnais.ws.redefines.WB03TsNetTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03TsPpLlbs0230;
import it.accenture.jnais.ws.redefines.WB03TsRendtoSpprLlbs0230;
import it.accenture.jnais.ws.redefines.WB03TsRendtoTLlbs0230;
import it.accenture.jnais.ws.redefines.WB03TsStabPreLlbs0230;
import it.accenture.jnais.ws.redefines.WB03TsTariDovLlbs0230;
import it.accenture.jnais.ws.redefines.WB03TsTariSconLlbs0230;
import it.accenture.jnais.ws.redefines.WB03UltRmLlbs0230;

/**Original name: W-B03-REC-FISSO<br>
 * Variable: W-B03-REC-FISSO from copybook LCCVB038<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WB03RecFisso {

    //==== PROPERTIES ====
    //Original name: W-B03-ID-BILA-TRCH-ESTR
    private int wB03IdBilaTrchEstr = DefaultValues.INT_VAL;
    //Original name: W-B03-COD-COMP-ANIA
    private int wB03CodCompAnia = DefaultValues.INT_VAL;
    //Original name: W-B03-ID-RICH-ESTRAZ-MAS
    private int wB03IdRichEstrazMas = DefaultValues.INT_VAL;
    //Original name: W-B03-ID-RICH-ESTRAZ-AGG-IND
    private char wB03IdRichEstrazAggInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ID-RICH-ESTRAZ-AGG
    private WB03IdRichEstrazAggLlbs0230 wB03IdRichEstrazAgg = new WB03IdRichEstrazAggLlbs0230();
    //Original name: W-B03-FL-SIMULAZIONE-IND
    private char wB03FlSimulazioneInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-SIMULAZIONE
    private char wB03FlSimulazione = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-RIS
    private String wB03DtRis = DefaultValues.stringVal(Len.W_B03_DT_RIS);
    //Original name: W-B03-DT-PRODUZIONE
    private String wB03DtProduzione = DefaultValues.stringVal(Len.W_B03_DT_PRODUZIONE);
    //Original name: W-B03-ID-POLI
    private int wB03IdPoli = DefaultValues.INT_VAL;
    //Original name: W-B03-ID-ADES
    private int wB03IdAdes = DefaultValues.INT_VAL;
    //Original name: W-B03-ID-GAR
    private int wB03IdGar = DefaultValues.INT_VAL;
    //Original name: W-B03-ID-TRCH-DI-GAR
    private int wB03IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: W-B03-TP-FRM-ASSVA
    private String wB03TpFrmAssva = DefaultValues.stringVal(Len.W_B03_TP_FRM_ASSVA);
    //Original name: W-B03-TP-RAMO-BILA
    private String wB03TpRamoBila = DefaultValues.stringVal(Len.W_B03_TP_RAMO_BILA);
    //Original name: W-B03-TP-CALC-RIS
    private String wB03TpCalcRis = DefaultValues.stringVal(Len.W_B03_TP_CALC_RIS);
    //Original name: W-B03-COD-RAMO
    private String wB03CodRamo = DefaultValues.stringVal(Len.W_B03_COD_RAMO);
    //Original name: W-B03-COD-TARI
    private String wB03CodTari = DefaultValues.stringVal(Len.W_B03_COD_TARI);
    //Original name: W-B03-DT-INI-VAL-TAR-IND
    private char wB03DtIniValTarInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-INI-VAL-TAR
    private String wB03DtIniValTar = DefaultValues.stringVal(Len.W_B03_DT_INI_VAL_TAR);
    //Original name: W-B03-COD-PROD-IND
    private char wB03CodProdInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-PROD
    private String wB03CodProd = DefaultValues.stringVal(Len.W_B03_COD_PROD);
    //Original name: W-B03-DT-INI-VLDT-PROD
    private String wB03DtIniVldtProd = DefaultValues.stringVal(Len.W_B03_DT_INI_VLDT_PROD);
    //Original name: W-B03-COD-TARI-ORGN-IND
    private char wB03CodTariOrgnInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-TARI-ORGN
    private String wB03CodTariOrgn = DefaultValues.stringVal(Len.W_B03_COD_TARI_ORGN);
    //Original name: W-B03-MIN-GARTO-T-IND
    private char wB03MinGartoTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-MIN-GARTO-T
    private WB03MinGartoTLlbs0230 wB03MinGartoT = new WB03MinGartoTLlbs0230();
    //Original name: W-B03-TP-TARI-IND
    private char wB03TpTariInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-TARI
    private String wB03TpTari = DefaultValues.stringVal(Len.W_B03_TP_TARI);
    //Original name: W-B03-TP-PRE-IND
    private char wB03TpPreInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-PRE
    private char wB03TpPre = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-ADEG-PRE-IND
    private char wB03TpAdegPreInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-ADEG-PRE
    private char wB03TpAdegPre = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-RIVAL-IND
    private char wB03TpRivalInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-RIVAL
    private String wB03TpRival = DefaultValues.stringVal(Len.W_B03_TP_RIVAL);
    //Original name: W-B03-FL-DA-TRASF-IND
    private char wB03FlDaTrasfInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-DA-TRASF
    private char wB03FlDaTrasf = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-CAR-CONT-IND
    private char wB03FlCarContInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-CAR-CONT
    private char wB03FlCarCont = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-PRE-DA-RIS-IND
    private char wB03FlPreDaRisInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-PRE-DA-RIS
    private char wB03FlPreDaRis = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-PRE-AGG-IND
    private char wB03FlPreAggInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-PRE-AGG
    private char wB03FlPreAgg = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-TRCH-IND
    private char wB03TpTrchInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-TRCH
    private String wB03TpTrch = DefaultValues.stringVal(Len.W_B03_TP_TRCH);
    //Original name: W-B03-TP-TST-IND
    private char wB03TpTstInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-TST
    private String wB03TpTst = DefaultValues.stringVal(Len.W_B03_TP_TST);
    //Original name: W-B03-COD-CONV-IND
    private char wB03CodConvInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-CONV
    private String wB03CodConv = DefaultValues.stringVal(Len.W_B03_COD_CONV);
    //Original name: W-B03-DT-DECOR-POLI
    private String wB03DtDecorPoli = DefaultValues.stringVal(Len.W_B03_DT_DECOR_POLI);
    //Original name: W-B03-DT-DECOR-ADES-IND
    private char wB03DtDecorAdesInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-DECOR-ADES
    private String wB03DtDecorAdes = DefaultValues.stringVal(Len.W_B03_DT_DECOR_ADES);
    //Original name: W-B03-DT-DECOR-TRCH
    private String wB03DtDecorTrch = DefaultValues.stringVal(Len.W_B03_DT_DECOR_TRCH);
    //Original name: W-B03-DT-EMIS-POLI
    private String wB03DtEmisPoli = DefaultValues.stringVal(Len.W_B03_DT_EMIS_POLI);
    //Original name: W-B03-DT-EMIS-TRCH-IND
    private char wB03DtEmisTrchInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-EMIS-TRCH
    private String wB03DtEmisTrch = DefaultValues.stringVal(Len.W_B03_DT_EMIS_TRCH);
    //Original name: W-B03-DT-SCAD-TRCH-IND
    private char wB03DtScadTrchInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-SCAD-TRCH
    private String wB03DtScadTrch = DefaultValues.stringVal(Len.W_B03_DT_SCAD_TRCH);
    //Original name: W-B03-DT-SCAD-INTMD-IND
    private char wB03DtScadIntmdInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-SCAD-INTMD
    private String wB03DtScadIntmd = DefaultValues.stringVal(Len.W_B03_DT_SCAD_INTMD);
    //Original name: W-B03-DT-SCAD-PAG-PRE-IND
    private char wB03DtScadPagPreInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-SCAD-PAG-PRE
    private String wB03DtScadPagPre = DefaultValues.stringVal(Len.W_B03_DT_SCAD_PAG_PRE);
    //Original name: W-B03-DT-ULT-PRE-PAG-IND
    private char wB03DtUltPrePagInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-ULT-PRE-PAG
    private String wB03DtUltPrePag = DefaultValues.stringVal(Len.W_B03_DT_ULT_PRE_PAG);
    //Original name: W-B03-DT-NASC-1O-ASSTO-IND
    private char wB03DtNasc1oAsstoInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-NASC-1O-ASSTO
    private String wB03DtNasc1oAssto = DefaultValues.stringVal(Len.W_B03_DT_NASC1O_ASSTO);
    //Original name: W-B03-SEX-1O-ASSTO-IND
    private char wB03Sex1oAsstoInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-SEX-1O-ASSTO
    private char wB03Sex1oAssto = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ETA-AA-1O-ASSTO-IND
    private char wB03EtaAa1oAsstoInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ETA-AA-1O-ASSTO
    private WB03EtaAa1oAsstoLlbs0230 wB03EtaAa1oAssto = new WB03EtaAa1oAsstoLlbs0230();
    //Original name: W-B03-ETA-MM-1O-ASSTO-IND
    private char wB03EtaMm1oAsstoInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ETA-MM-1O-ASSTO
    private WB03EtaMm1oAsstoLlbs0230 wB03EtaMm1oAssto = new WB03EtaMm1oAsstoLlbs0230();
    //Original name: W-B03-ETA-RAGGN-DT-CALC-IND
    private char wB03EtaRaggnDtCalcInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ETA-RAGGN-DT-CALC
    private WB03EtaRaggnDtCalcLlbs0230 wB03EtaRaggnDtCalc = new WB03EtaRaggnDtCalcLlbs0230();
    //Original name: W-B03-DUR-AA-IND
    private char wB03DurAaInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-AA
    private WB03DurAaLlbs0230 wB03DurAa = new WB03DurAaLlbs0230();
    //Original name: W-B03-DUR-MM-IND
    private char wB03DurMmInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-MM
    private WB03DurMmLlbs0230 wB03DurMm = new WB03DurMmLlbs0230();
    //Original name: W-B03-DUR-GG-IND
    private char wB03DurGgInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-GG
    private WB03DurGgLlbs0230 wB03DurGg = new WB03DurGgLlbs0230();
    //Original name: W-B03-DUR-1O-PER-AA-IND
    private char wB03Dur1oPerAaInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-1O-PER-AA
    private WB03Dur1oPerAaLlbs0230 wB03Dur1oPerAa = new WB03Dur1oPerAaLlbs0230();
    //Original name: W-B03-DUR-1O-PER-MM-IND
    private char wB03Dur1oPerMmInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-1O-PER-MM
    private WB03Dur1oPerMmLlbs0230 wB03Dur1oPerMm = new WB03Dur1oPerMmLlbs0230();
    //Original name: W-B03-DUR-1O-PER-GG-IND
    private char wB03Dur1oPerGgInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-1O-PER-GG
    private WB03Dur1oPerGgLlbs0230 wB03Dur1oPerGg = new WB03Dur1oPerGgLlbs0230();
    //Original name: W-B03-ANTIDUR-RICOR-PREC-IND
    private char wB03AntidurRicorPrecInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ANTIDUR-RICOR-PREC
    private WB03AntidurRicorPrecLlbs0230 wB03AntidurRicorPrec = new WB03AntidurRicorPrecLlbs0230();
    //Original name: W-B03-ANTIDUR-DT-CALC-IND
    private char wB03AntidurDtCalcInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ANTIDUR-DT-CALC
    private WB03AntidurDtCalcLlbs0230 wB03AntidurDtCalc = new WB03AntidurDtCalcLlbs0230();
    //Original name: W-B03-DUR-RES-DT-CALC-IND
    private char wB03DurResDtCalcInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-RES-DT-CALC
    private WB03DurResDtCalcLlbs0230 wB03DurResDtCalc = new WB03DurResDtCalcLlbs0230();
    //Original name: W-B03-TP-STAT-BUS-POLI
    private String wB03TpStatBusPoli = DefaultValues.stringVal(Len.W_B03_TP_STAT_BUS_POLI);
    //Original name: W-B03-TP-CAUS-POLI
    private String wB03TpCausPoli = DefaultValues.stringVal(Len.W_B03_TP_CAUS_POLI);
    //Original name: W-B03-TP-STAT-BUS-ADES
    private String wB03TpStatBusAdes = DefaultValues.stringVal(Len.W_B03_TP_STAT_BUS_ADES);
    //Original name: W-B03-TP-CAUS-ADES
    private String wB03TpCausAdes = DefaultValues.stringVal(Len.W_B03_TP_CAUS_ADES);
    //Original name: W-B03-TP-STAT-BUS-TRCH
    private String wB03TpStatBusTrch = DefaultValues.stringVal(Len.W_B03_TP_STAT_BUS_TRCH);
    //Original name: W-B03-TP-CAUS-TRCH
    private String wB03TpCausTrch = DefaultValues.stringVal(Len.W_B03_TP_CAUS_TRCH);
    //Original name: W-B03-DT-EFF-CAMB-STAT-IND
    private char wB03DtEffCambStatInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-EFF-CAMB-STAT
    private String wB03DtEffCambStat = DefaultValues.stringVal(Len.W_B03_DT_EFF_CAMB_STAT);
    //Original name: W-B03-DT-EMIS-CAMB-STAT-IND
    private char wB03DtEmisCambStatInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-EMIS-CAMB-STAT
    private String wB03DtEmisCambStat = DefaultValues.stringVal(Len.W_B03_DT_EMIS_CAMB_STAT);
    //Original name: W-B03-DT-EFF-STAB-IND
    private char wB03DtEffStabInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-EFF-STAB
    private String wB03DtEffStab = DefaultValues.stringVal(Len.W_B03_DT_EFF_STAB);
    //Original name: W-B03-CPT-DT-STAB-IND
    private char wB03CptDtStabInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CPT-DT-STAB
    private WB03CptDtStabLlbs0230 wB03CptDtStab = new WB03CptDtStabLlbs0230();
    //Original name: W-B03-DT-EFF-RIDZ-IND
    private char wB03DtEffRidzInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-EFF-RIDZ
    private String wB03DtEffRidz = DefaultValues.stringVal(Len.W_B03_DT_EFF_RIDZ);
    //Original name: W-B03-DT-EMIS-RIDZ-IND
    private char wB03DtEmisRidzInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-EMIS-RIDZ
    private String wB03DtEmisRidz = DefaultValues.stringVal(Len.W_B03_DT_EMIS_RIDZ);
    //Original name: W-B03-CPT-DT-RIDZ-IND
    private char wB03CptDtRidzInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CPT-DT-RIDZ
    private WB03CptDtRidzLlbs0230 wB03CptDtRidz = new WB03CptDtRidzLlbs0230();
    //Original name: W-B03-FRAZ-IND
    private char wB03FrazInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FRAZ
    private WB03FrazLlbs0230 wB03Fraz = new WB03FrazLlbs0230();
    //Original name: W-B03-DUR-PAG-PRE-IND
    private char wB03DurPagPreInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-PAG-PRE
    private WB03DurPagPreLlbs0230 wB03DurPagPre = new WB03DurPagPreLlbs0230();
    //Original name: W-B03-NUM-PRE-PATT-IND
    private char wB03NumPrePattInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-NUM-PRE-PATT
    private WB03NumPrePattLlbs0230 wB03NumPrePatt = new WB03NumPrePattLlbs0230();
    //Original name: W-B03-FRAZ-INI-EROG-REN-IND
    private char wB03FrazIniErogRenInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FRAZ-INI-EROG-REN
    private WB03FrazIniErogRenLlbs0230 wB03FrazIniErogRen = new WB03FrazIniErogRenLlbs0230();
    //Original name: W-B03-AA-REN-CER-IND
    private char wB03AaRenCerInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-AA-REN-CER
    private WB03AaRenCerLlbs0230 wB03AaRenCer = new WB03AaRenCerLlbs0230();
    //Original name: W-B03-RAT-REN-IND
    private char wB03RatRenInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-RAT-REN
    private WB03RatRenLlbs0230 wB03RatRen = new WB03RatRenLlbs0230();
    //Original name: W-B03-COD-DIV-IND
    private char wB03CodDivInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-DIV
    private String wB03CodDiv = DefaultValues.stringVal(Len.W_B03_COD_DIV);
    //Original name: W-B03-RISCPAR-IND
    private char wB03RiscparInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-RISCPAR
    private WB03RiscparLlbs0230 wB03Riscpar = new WB03RiscparLlbs0230();
    //Original name: W-B03-CUM-RISCPAR-IND
    private char wB03CumRiscparInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CUM-RISCPAR
    private WB03CumRiscparLlbs0230 wB03CumRiscpar = new WB03CumRiscparLlbs0230();
    //Original name: W-B03-ULT-RM-IND
    private char wB03UltRmInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ULT-RM
    private WB03UltRmLlbs0230 wB03UltRm = new WB03UltRmLlbs0230();
    //Original name: W-B03-TS-RENDTO-T-IND
    private char wB03TsRendtoTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TS-RENDTO-T
    private WB03TsRendtoTLlbs0230 wB03TsRendtoT = new WB03TsRendtoTLlbs0230();
    //Original name: W-B03-ALQ-RETR-T-IND
    private char wB03AlqRetrTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ALQ-RETR-T
    private WB03AlqRetrTLlbs0230 wB03AlqRetrT = new WB03AlqRetrTLlbs0230();
    //Original name: W-B03-MIN-TRNUT-T-IND
    private char wB03MinTrnutTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-MIN-TRNUT-T
    private WB03MinTrnutTLlbs0230 wB03MinTrnutT = new WB03MinTrnutTLlbs0230();
    //Original name: W-B03-TS-NET-T-IND
    private char wB03TsNetTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TS-NET-T
    private WB03TsNetTLlbs0230 wB03TsNetT = new WB03TsNetTLlbs0230();
    //Original name: W-B03-DT-ULT-RIVAL-IND
    private char wB03DtUltRivalInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-ULT-RIVAL
    private String wB03DtUltRival = DefaultValues.stringVal(Len.W_B03_DT_ULT_RIVAL);
    //Original name: W-B03-PRSTZ-INI-IND
    private char wB03PrstzIniInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRSTZ-INI
    private WB03PrstzIniLlbs0230 wB03PrstzIni = new WB03PrstzIniLlbs0230();
    //Original name: W-B03-PRSTZ-AGG-INI-IND
    private char wB03PrstzAggIniInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRSTZ-AGG-INI
    private WB03PrstzAggIniLlbs0230 wB03PrstzAggIni = new WB03PrstzAggIniLlbs0230();
    //Original name: W-B03-PRSTZ-AGG-ULT-IND
    private char wB03PrstzAggUltInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRSTZ-AGG-ULT
    private WB03PrstzAggUltLlbs0230 wB03PrstzAggUlt = new WB03PrstzAggUltLlbs0230();
    //Original name: W-B03-RAPPEL-IND
    private char wB03RappelInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-RAPPEL
    private WB03RappelLlbs0230 wB03Rappel = new WB03RappelLlbs0230();
    //Original name: W-B03-PRE-PATTUITO-INI-IND
    private char wB03PrePattuitoIniInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRE-PATTUITO-INI
    private WB03PrePattuitoIniLlbs0230 wB03PrePattuitoIni = new WB03PrePattuitoIniLlbs0230();
    //Original name: W-B03-PRE-DOV-INI-IND
    private char wB03PreDovIniInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRE-DOV-INI
    private WB03PreDovIniLlbs0230 wB03PreDovIni = new WB03PreDovIniLlbs0230();
    //Original name: W-B03-PRE-DOV-RIVTO-T-IND
    private char wB03PreDovRivtoTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRE-DOV-RIVTO-T
    private WB03PreDovRivtoTLlbs0230 wB03PreDovRivtoT = new WB03PreDovRivtoTLlbs0230();
    //Original name: W-B03-PRE-ANNUALIZ-RICOR-IND
    private char wB03PreAnnualizRicorInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRE-ANNUALIZ-RICOR
    private WB03PreAnnualizRicorLlbs0230 wB03PreAnnualizRicor = new WB03PreAnnualizRicorLlbs0230();
    //Original name: W-B03-PRE-CONT-IND
    private char wB03PreContInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRE-CONT
    private WB03PreContLlbs0230 wB03PreCont = new WB03PreContLlbs0230();
    //Original name: W-B03-PRE-PP-INI-IND
    private char wB03PrePpIniInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRE-PP-INI
    private WB03PrePpIniLlbs0230 wB03PrePpIni = new WB03PrePpIniLlbs0230();
    //Original name: W-B03-RIS-PURA-T-IND
    private char wB03RisPuraTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-RIS-PURA-T
    private WB03RisPuraTLlbs0230 wB03RisPuraT = new WB03RisPuraTLlbs0230();
    //Original name: W-B03-PROV-ACQ-IND
    private char wB03ProvAcqInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PROV-ACQ
    private WB03ProvAcqLlbs0230 wB03ProvAcq = new WB03ProvAcqLlbs0230();
    //Original name: W-B03-PROV-ACQ-RICOR-IND
    private char wB03ProvAcqRicorInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PROV-ACQ-RICOR
    private WB03ProvAcqRicorLlbs0230 wB03ProvAcqRicor = new WB03ProvAcqRicorLlbs0230();
    //Original name: W-B03-PROV-INC-IND
    private char wB03ProvIncInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PROV-INC
    private WB03ProvIncLlbs0230 wB03ProvInc = new WB03ProvIncLlbs0230();
    //Original name: W-B03-CAR-ACQ-NON-SCON-IND
    private char wB03CarAcqNonSconInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CAR-ACQ-NON-SCON
    private WB03CarAcqNonSconLlbs0230 wB03CarAcqNonScon = new WB03CarAcqNonSconLlbs0230();
    //Original name: W-B03-OVER-COMM-IND
    private char wB03OverCommInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-OVER-COMM
    private WB03OverCommLlbs0230 wB03OverComm = new WB03OverCommLlbs0230();
    //Original name: W-B03-CAR-ACQ-PRECONTATO-IND
    private char wB03CarAcqPrecontatoInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CAR-ACQ-PRECONTATO
    private WB03CarAcqPrecontatoLlbs0230 wB03CarAcqPrecontato = new WB03CarAcqPrecontatoLlbs0230();
    //Original name: W-B03-RIS-ACQ-T-IND
    private char wB03RisAcqTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-RIS-ACQ-T
    private WB03RisAcqTLlbs0230 wB03RisAcqT = new WB03RisAcqTLlbs0230();
    //Original name: W-B03-RIS-ZIL-T-IND
    private char wB03RisZilTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-RIS-ZIL-T
    private WB03RisZilTLlbs0230 wB03RisZilT = new WB03RisZilTLlbs0230();
    //Original name: W-B03-CAR-GEST-NON-SCON-IND
    private char wB03CarGestNonSconInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CAR-GEST-NON-SCON
    private WB03CarGestNonSconLlbs0230 wB03CarGestNonScon = new WB03CarGestNonSconLlbs0230();
    //Original name: W-B03-CAR-GEST-IND
    private char wB03CarGestInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CAR-GEST
    private WB03CarGestLlbs0230 wB03CarGest = new WB03CarGestLlbs0230();
    //Original name: W-B03-RIS-SPE-T-IND
    private char wB03RisSpeTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-RIS-SPE-T
    private WB03RisSpeTLlbs0230 wB03RisSpeT = new WB03RisSpeTLlbs0230();
    //Original name: W-B03-CAR-INC-NON-SCON-IND
    private char wB03CarIncNonSconInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CAR-INC-NON-SCON
    private WB03CarIncNonSconLlbs0230 wB03CarIncNonScon = new WB03CarIncNonSconLlbs0230();
    //Original name: W-B03-CAR-INC-IND
    private char wB03CarIncInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CAR-INC
    private WB03CarIncLlbs0230 wB03CarInc = new WB03CarIncLlbs0230();
    //Original name: W-B03-RIS-RISTORNI-CAP-IND
    private char wB03RisRistorniCapInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-RIS-RISTORNI-CAP
    private WB03RisRistorniCapLlbs0230 wB03RisRistorniCap = new WB03RisRistorniCapLlbs0230();
    //Original name: W-B03-INTR-TECN-IND
    private char wB03IntrTecnInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-INTR-TECN
    private WB03IntrTecnLlbs0230 wB03IntrTecn = new WB03IntrTecnLlbs0230();
    //Original name: W-B03-CPT-RSH-MOR-IND
    private char wB03CptRshMorInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CPT-RSH-MOR
    private WB03CptRshMorLlbs0230 wB03CptRshMor = new WB03CptRshMorLlbs0230();
    //Original name: W-B03-C-SUBRSH-T-IND
    private char wB03CSubrshTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-C-SUBRSH-T
    private WB03CSubrshTLlbs0230 wB03CSubrshT = new WB03CSubrshTLlbs0230();
    //Original name: W-B03-PRE-RSH-T-IND
    private char wB03PreRshTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRE-RSH-T
    private WB03PreRshTLlbs0230 wB03PreRshT = new WB03PreRshTLlbs0230();
    //Original name: W-B03-ALQ-MARG-RIS-IND
    private char wB03AlqMargRisInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ALQ-MARG-RIS
    private WB03AlqMargRisLlbs0230 wB03AlqMargRis = new WB03AlqMargRisLlbs0230();
    //Original name: W-B03-ALQ-MARG-C-SUBRSH-IND
    private char wB03AlqMargCSubrshInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ALQ-MARG-C-SUBRSH
    private WB03AlqMargCSubrshLlbs0230 wB03AlqMargCSubrsh = new WB03AlqMargCSubrshLlbs0230();
    //Original name: W-B03-TS-RENDTO-SPPR-IND
    private char wB03TsRendtoSpprInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TS-RENDTO-SPPR
    private WB03TsRendtoSpprLlbs0230 wB03TsRendtoSppr = new WB03TsRendtoSpprLlbs0230();
    //Original name: W-B03-TP-IAS-IND
    private char wB03TpIasInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-IAS
    private String wB03TpIas = DefaultValues.stringVal(Len.W_B03_TP_IAS);
    //Original name: W-B03-NS-QUO-IND
    private char wB03NsQuoInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-NS-QUO
    private WB03NsQuoLlbs0230 wB03NsQuo = new WB03NsQuoLlbs0230();
    //Original name: W-B03-TS-MEDIO-IND
    private char wB03TsMedioInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TS-MEDIO
    private WB03TsMedioLlbs0230 wB03TsMedio = new WB03TsMedioLlbs0230();
    //Original name: W-B03-CPT-RIASTO-IND
    private char wB03CptRiastoInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CPT-RIASTO
    private WB03CptRiastoLlbs0230 wB03CptRiasto = new WB03CptRiastoLlbs0230();
    //Original name: W-B03-PRE-RIASTO-IND
    private char wB03PreRiastoInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRE-RIASTO
    private WB03PreRiastoLlbs0230 wB03PreRiasto = new WB03PreRiastoLlbs0230();
    //Original name: W-B03-RIS-RIASTA-IND
    private char wB03RisRiastaInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-RIS-RIASTA
    private WB03RisRiastaLlbs0230 wB03RisRiasta = new WB03RisRiastaLlbs0230();
    //Original name: W-B03-CPT-RIASTO-ECC-IND
    private char wB03CptRiastoEccInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CPT-RIASTO-ECC
    private WB03CptRiastoEccLlbs0230 wB03CptRiastoEcc = new WB03CptRiastoEccLlbs0230();
    //Original name: W-B03-PRE-RIASTO-ECC-IND
    private char wB03PreRiastoEccInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRE-RIASTO-ECC
    private WB03PreRiastoEccLlbs0230 wB03PreRiastoEcc = new WB03PreRiastoEccLlbs0230();
    //Original name: W-B03-RIS-RIASTA-ECC-IND
    private char wB03RisRiastaEccInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-RIS-RIASTA-ECC
    private WB03RisRiastaEccLlbs0230 wB03RisRiastaEcc = new WB03RisRiastaEccLlbs0230();
    //Original name: W-B03-COD-AGE-IND
    private char wB03CodAgeInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-AGE
    private WB03CodAgeLlbs0230 wB03CodAge = new WB03CodAgeLlbs0230();
    //Original name: W-B03-COD-SUBAGE-IND
    private char wB03CodSubageInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-SUBAGE
    private WB03CodSubageLlbs0230 wB03CodSubage = new WB03CodSubageLlbs0230();
    //Original name: W-B03-COD-CAN-IND
    private char wB03CodCanInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-CAN
    private WB03CodCanLlbs0230 wB03CodCan = new WB03CodCanLlbs0230();
    //Original name: W-B03-IB-POLI-IND
    private char wB03IbPoliInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-IB-POLI
    private String wB03IbPoli = DefaultValues.stringVal(Len.W_B03_IB_POLI);
    //Original name: W-B03-IB-ADES-IND
    private char wB03IbAdesInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-IB-ADES
    private String wB03IbAdes = DefaultValues.stringVal(Len.W_B03_IB_ADES);
    //Original name: W-B03-IB-TRCH-DI-GAR-IND
    private char wB03IbTrchDiGarInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-IB-TRCH-DI-GAR
    private String wB03IbTrchDiGar = DefaultValues.stringVal(Len.W_B03_IB_TRCH_DI_GAR);
    //Original name: W-B03-TP-PRSTZ-IND
    private char wB03TpPrstzInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-PRSTZ
    private String wB03TpPrstz = DefaultValues.stringVal(Len.W_B03_TP_PRSTZ);
    //Original name: W-B03-TP-TRASF-IND
    private char wB03TpTrasfInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-TRASF
    private String wB03TpTrasf = DefaultValues.stringVal(Len.W_B03_TP_TRASF);
    //Original name: W-B03-PP-INVRIO-TARI-IND
    private char wB03PpInvrioTariInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PP-INVRIO-TARI
    private char wB03PpInvrioTari = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COEFF-OPZ-REN-IND
    private char wB03CoeffOpzRenInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COEFF-OPZ-REN
    private WB03CoeffOpzRenLlbs0230 wB03CoeffOpzRen = new WB03CoeffOpzRenLlbs0230();
    //Original name: W-B03-COEFF-OPZ-CPT-IND
    private char wB03CoeffOpzCptInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COEFF-OPZ-CPT
    private WB03CoeffOpzCptLlbs0230 wB03CoeffOpzCpt = new WB03CoeffOpzCptLlbs0230();
    //Original name: W-B03-DUR-PAG-REN-IND
    private char wB03DurPagRenInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-PAG-REN
    private WB03DurPagRenLlbs0230 wB03DurPagRen = new WB03DurPagRenLlbs0230();
    //Original name: W-B03-VLT-IND
    private char wB03VltInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-VLT
    private String wB03Vlt = DefaultValues.stringVal(Len.W_B03_VLT);
    //Original name: W-B03-RIS-MAT-CHIU-PREC-IND
    private char wB03RisMatChiuPrecInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-RIS-MAT-CHIU-PREC
    private WB03RisMatChiuPrecLlbs0230 wB03RisMatChiuPrec = new WB03RisMatChiuPrecLlbs0230();
    //Original name: W-B03-COD-FND-IND
    private char wB03CodFndInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-FND
    private String wB03CodFnd = DefaultValues.stringVal(Len.W_B03_COD_FND);
    //Original name: W-B03-PRSTZ-T-IND
    private char wB03PrstzTInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRSTZ-T
    private WB03PrstzTLlbs0230 wB03PrstzT = new WB03PrstzTLlbs0230();
    //Original name: W-B03-TS-TARI-DOV-IND
    private char wB03TsTariDovInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TS-TARI-DOV
    private WB03TsTariDovLlbs0230 wB03TsTariDov = new WB03TsTariDovLlbs0230();
    //Original name: W-B03-TS-TARI-SCON-IND
    private char wB03TsTariSconInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TS-TARI-SCON
    private WB03TsTariSconLlbs0230 wB03TsTariScon = new WB03TsTariSconLlbs0230();
    //Original name: W-B03-TS-PP-IND
    private char wB03TsPpInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TS-PP
    private WB03TsPpLlbs0230 wB03TsPp = new WB03TsPpLlbs0230();
    //Original name: W-B03-COEFF-RIS-1-T-IND
    private char wB03CoeffRis1TInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COEFF-RIS-1-T
    private WB03CoeffRis1TLlbs0230 wB03CoeffRis1T = new WB03CoeffRis1TLlbs0230();
    //Original name: W-B03-COEFF-RIS-2-T-IND
    private char wB03CoeffRis2TInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COEFF-RIS-2-T
    private WB03CoeffRis2TLlbs0230 wB03CoeffRis2T = new WB03CoeffRis2TLlbs0230();
    //Original name: W-B03-ABB-IND
    private char wB03AbbInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ABB
    private WB03AbbLlbs0230 wB03Abb = new WB03AbbLlbs0230();
    //Original name: W-B03-TP-COASS-IND
    private char wB03TpCoassInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-COASS
    private String wB03TpCoass = DefaultValues.stringVal(Len.W_B03_TP_COASS);
    //Original name: W-B03-TRAT-RIASS-IND
    private char wB03TratRiassInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TRAT-RIASS
    private String wB03TratRiass = DefaultValues.stringVal(Len.W_B03_TRAT_RIASS);
    //Original name: W-B03-TRAT-RIASS-ECC-IND
    private char wB03TratRiassEccInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TRAT-RIASS-ECC
    private String wB03TratRiassEcc = DefaultValues.stringVal(Len.W_B03_TRAT_RIASS_ECC);
    //Original name: W-B03-DS-OPER-SQL
    private char wB03DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DS-VER
    private int wB03DsVer = DefaultValues.INT_VAL;
    //Original name: W-B03-DS-TS-CPTZ
    private long wB03DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: W-B03-DS-UTENTE
    private String wB03DsUtente = DefaultValues.stringVal(Len.W_B03_DS_UTENTE);
    //Original name: W-B03-DS-STATO-ELAB
    private char wB03DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-RGM-FISC-IND
    private char wB03TpRgmFiscInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-RGM-FISC
    private String wB03TpRgmFisc = DefaultValues.stringVal(Len.W_B03_TP_RGM_FISC);
    //Original name: W-B03-DUR-GAR-AA-IND
    private char wB03DurGarAaInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-GAR-AA
    private WB03DurGarAaLlbs0230 wB03DurGarAa = new WB03DurGarAaLlbs0230();
    //Original name: W-B03-DUR-GAR-MM-IND
    private char wB03DurGarMmInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-GAR-MM
    private WB03DurGarMmLlbs0230 wB03DurGarMm = new WB03DurGarMmLlbs0230();
    //Original name: W-B03-DUR-GAR-GG-IND
    private char wB03DurGarGgInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DUR-GAR-GG
    private WB03DurGarGgLlbs0230 wB03DurGarGg = new WB03DurGarGgLlbs0230();
    //Original name: W-B03-ANTIDUR-CALC-365-IND
    private char wB03AntidurCalc365Ind = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ANTIDUR-CALC-365
    private WB03AntidurCalc365Llbs0230 wB03AntidurCalc365 = new WB03AntidurCalc365Llbs0230();
    //Original name: W-B03-COD-FISC-CNTR-IND
    private char wB03CodFiscCntrInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-FISC-CNTR
    private String wB03CodFiscCntr = DefaultValues.stringVal(Len.W_B03_COD_FISC_CNTR);
    //Original name: W-B03-COD-FISC-ASSTO1-IND
    private char wB03CodFiscAssto1Ind = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-FISC-ASSTO1
    private String wB03CodFiscAssto1 = DefaultValues.stringVal(Len.W_B03_COD_FISC_ASSTO1);
    //Original name: W-B03-COD-FISC-ASSTO2-IND
    private char wB03CodFiscAssto2Ind = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-FISC-ASSTO2
    private String wB03CodFiscAssto2 = DefaultValues.stringVal(Len.W_B03_COD_FISC_ASSTO2);
    //Original name: W-B03-COD-FISC-ASSTO3-IND
    private char wB03CodFiscAssto3Ind = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-FISC-ASSTO3
    private String wB03CodFiscAssto3 = DefaultValues.stringVal(Len.W_B03_COD_FISC_ASSTO3);
    //Original name: W-B03-CAUS-SCON-IND
    private char wB03CausSconInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CAUS-SCON-LEN
    private short wB03CausSconLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: W-B03-CAUS-SCON
    private String wB03CausScon = DefaultValues.stringVal(Len.W_B03_CAUS_SCON);
    //Original name: W-B03-EMIT-TIT-OPZ-IND
    private char wB03EmitTitOpzInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-EMIT-TIT-OPZ-LEN
    private short wB03EmitTitOpzLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: W-B03-EMIT-TIT-OPZ
    private String wB03EmitTitOpz = DefaultValues.stringVal(Len.W_B03_EMIT_TIT_OPZ);
    //Original name: W-B03-QTZ-SP-Z-COUP-EMIS-IND
    private char wB03QtzSpZCoupEmisInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-QTZ-SP-Z-COUP-EMIS
    private WB03QtzSpZCoupEmisLlbs0230 wB03QtzSpZCoupEmis = new WB03QtzSpZCoupEmisLlbs0230();
    //Original name: W-B03-QTZ-SP-Z-OPZ-EMIS-IND
    private char wB03QtzSpZOpzEmisInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-QTZ-SP-Z-OPZ-EMIS
    private WB03QtzSpZOpzEmisLlbs0230 wB03QtzSpZOpzEmis = new WB03QtzSpZOpzEmisLlbs0230();
    //Original name: W-B03-QTZ-SP-Z-COUP-DT-C-IND
    private char wB03QtzSpZCoupDtCInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-QTZ-SP-Z-COUP-DT-C
    private WB03QtzSpZCoupDtCLlbs0230 wB03QtzSpZCoupDtC = new WB03QtzSpZCoupDtCLlbs0230();
    //Original name: W-B03-QTZ-SP-Z-OPZ-DT-CA-IND
    private char wB03QtzSpZOpzDtCaInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-QTZ-SP-Z-OPZ-DT-CA
    private WB03QtzSpZOpzDtCaLlbs0230 wB03QtzSpZOpzDtCa = new WB03QtzSpZOpzDtCaLlbs0230();
    //Original name: W-B03-QTZ-TOT-EMIS-IND
    private char wB03QtzTotEmisInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-QTZ-TOT-EMIS
    private WB03QtzTotEmisLlbs0230 wB03QtzTotEmis = new WB03QtzTotEmisLlbs0230();
    //Original name: W-B03-QTZ-TOT-DT-CALC-IND
    private char wB03QtzTotDtCalcInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-QTZ-TOT-DT-CALC
    private WB03QtzTotDtCalcLlbs0230 wB03QtzTotDtCalc = new WB03QtzTotDtCalcLlbs0230();
    //Original name: W-B03-QTZ-TOT-DT-ULT-BIL-IND
    private char wB03QtzTotDtUltBilInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-QTZ-TOT-DT-ULT-BIL
    private WB03QtzTotDtUltBilLlbs0230 wB03QtzTotDtUltBil = new WB03QtzTotDtUltBilLlbs0230();
    //Original name: W-B03-DT-QTZ-EMIS-IND
    private char wB03DtQtzEmisInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-QTZ-EMIS
    private String wB03DtQtzEmis = DefaultValues.stringVal(Len.W_B03_DT_QTZ_EMIS);
    //Original name: W-B03-PC-CAR-GEST-IND
    private char wB03PcCarGestInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PC-CAR-GEST
    private AfDecimal wB03PcCarGest = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: W-B03-PC-CAR-ACQ-IND
    private char wB03PcCarAcqInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PC-CAR-ACQ
    private WB03PcCarAcqLlbs0230 wB03PcCarAcq = new WB03PcCarAcqLlbs0230();
    //Original name: W-B03-IMP-CAR-CASO-MOR-IND
    private char wB03ImpCarCasoMorInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-IMP-CAR-CASO-MOR
    private WB03ImpCarCasoMorLlbs0230 wB03ImpCarCasoMor = new WB03ImpCarCasoMorLlbs0230();
    //Original name: W-B03-PC-CAR-MOR-IND
    private char wB03PcCarMorInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PC-CAR-MOR
    private WB03PcCarMorLlbs0230 wB03PcCarMor = new WB03PcCarMorLlbs0230();
    //Original name: W-B03-TP-VERS-IND
    private char wB03TpVersInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-VERS
    private char wB03TpVers = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-SWITCH-IND
    private char wB03FlSwitchInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-SWITCH
    private char wB03FlSwitch = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-IAS-IND
    private char wB03FlIasInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FL-IAS
    private char wB03FlIas = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DIR-IND
    private char wB03DirInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DIR
    private WB03DirLlbs0230 wB03Dir = new WB03DirLlbs0230();
    //Original name: W-B03-TP-COP-CASO-MOR-IND
    private char wB03TpCopCasoMorInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-COP-CASO-MOR
    private String wB03TpCopCasoMor = DefaultValues.stringVal(Len.W_B03_TP_COP_CASO_MOR);
    //Original name: W-B03-MET-RISC-SPCL-IND
    private char wB03MetRiscSpclInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-MET-RISC-SPCL
    private WB03MetRiscSpclLlbs0230 wB03MetRiscSpcl = new WB03MetRiscSpclLlbs0230();
    //Original name: W-B03-TP-STAT-INVST-IND
    private char wB03TpStatInvstInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-STAT-INVST
    private String wB03TpStatInvst = DefaultValues.stringVal(Len.W_B03_TP_STAT_INVST);
    //Original name: W-B03-COD-PRDT-IND
    private char wB03CodPrdtInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COD-PRDT
    private WB03CodPrdtLlbs0230 wB03CodPrdt = new WB03CodPrdtLlbs0230();
    //Original name: W-B03-STAT-ASSTO-1-IND
    private char wB03StatAssto1Ind = DefaultValues.CHAR_VAL;
    //Original name: W-B03-STAT-ASSTO-1
    private char wB03StatAssto1 = DefaultValues.CHAR_VAL;
    //Original name: W-B03-STAT-ASSTO-2-IND
    private char wB03StatAssto2Ind = DefaultValues.CHAR_VAL;
    //Original name: W-B03-STAT-ASSTO-2
    private char wB03StatAssto2 = DefaultValues.CHAR_VAL;
    //Original name: W-B03-STAT-ASSTO-3-IND
    private char wB03StatAssto3Ind = DefaultValues.CHAR_VAL;
    //Original name: W-B03-STAT-ASSTO-3
    private char wB03StatAssto3 = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CPT-ASSTO-INI-MOR-IND
    private char wB03CptAsstoIniMorInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CPT-ASSTO-INI-MOR
    private WB03CptAsstoIniMorLlbs0230 wB03CptAsstoIniMor = new WB03CptAsstoIniMorLlbs0230();
    //Original name: W-B03-TS-STAB-PRE-IND
    private char wB03TsStabPreInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TS-STAB-PRE
    private WB03TsStabPreLlbs0230 wB03TsStabPre = new WB03TsStabPreLlbs0230();
    //Original name: W-B03-DIR-EMIS-IND
    private char wB03DirEmisInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DIR-EMIS
    private WB03DirEmisLlbs0230 wB03DirEmis = new WB03DirEmisLlbs0230();
    //Original name: W-B03-DT-INC-ULT-PRE-IND
    private char wB03DtIncUltPreInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-DT-INC-ULT-PRE
    private String wB03DtIncUltPre = DefaultValues.stringVal(Len.W_B03_DT_INC_ULT_PRE);
    //Original name: W-B03-STAT-TBGC-ASSTO-1-IND
    private char wB03StatTbgcAssto1Ind = DefaultValues.CHAR_VAL;
    //Original name: W-B03-STAT-TBGC-ASSTO-1
    private char wB03StatTbgcAssto1 = DefaultValues.CHAR_VAL;
    //Original name: W-B03-STAT-TBGC-ASSTO-2-IND
    private char wB03StatTbgcAssto2Ind = DefaultValues.CHAR_VAL;
    //Original name: W-B03-STAT-TBGC-ASSTO-2
    private char wB03StatTbgcAssto2 = DefaultValues.CHAR_VAL;
    //Original name: W-B03-STAT-TBGC-ASSTO-3-IND
    private char wB03StatTbgcAssto3Ind = DefaultValues.CHAR_VAL;
    //Original name: W-B03-STAT-TBGC-ASSTO-3
    private char wB03StatTbgcAssto3 = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FRAZ-DECR-CPT-IND
    private char wB03FrazDecrCptInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-FRAZ-DECR-CPT
    private WB03FrazDecrCptLlbs0230 wB03FrazDecrCpt = new WB03FrazDecrCptLlbs0230();
    //Original name: W-B03-PRE-PP-ULT-IND
    private char wB03PrePpUltInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-PRE-PP-ULT
    private WB03PrePpUltLlbs0230 wB03PrePpUlt = new WB03PrePpUltLlbs0230();
    //Original name: W-B03-ACQ-EXP-IND
    private char wB03AcqExpInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-ACQ-EXP
    private WB03AcqExpLlbs0230 wB03AcqExp = new WB03AcqExpLlbs0230();
    //Original name: W-B03-REMUN-ASS-IND
    private char wB03RemunAssInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-REMUN-ASS
    private WB03RemunAssLlbs0230 wB03RemunAss = new WB03RemunAssLlbs0230();
    //Original name: W-B03-COMMIS-INTER-IND
    private char wB03CommisInterInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-COMMIS-INTER
    private WB03CommisInterLlbs0230 wB03CommisInter = new WB03CommisInterLlbs0230();
    //Original name: W-B03-NUM-FINANZ-IND
    private char wB03NumFinanzInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-NUM-FINANZ
    private String wB03NumFinanz = DefaultValues.stringVal(Len.W_B03_NUM_FINANZ);
    //Original name: W-B03-TP-ACC-COMM-IND
    private char wB03TpAccCommInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-TP-ACC-COMM
    private String wB03TpAccComm = DefaultValues.stringVal(Len.W_B03_TP_ACC_COMM);
    //Original name: W-B03-IB-ACC-COMM-IND
    private char wB03IbAccCommInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-IB-ACC-COMM
    private String wB03IbAccComm = DefaultValues.stringVal(Len.W_B03_IB_ACC_COMM);
    //Original name: W-B03-RAMO-BILA
    private String wB03RamoBila = DefaultValues.stringVal(Len.W_B03_RAMO_BILA);
    //Original name: W-B03-CARZ-IND
    private char wB03CarzInd = DefaultValues.CHAR_VAL;
    //Original name: W-B03-CARZ
    private WB03CarzLlbs0230 wB03Carz = new WB03CarzLlbs0230();

    //==== METHODS ====
    public byte[] getRecFissoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wB03IdBilaTrchEstr, Len.Int.W_B03_ID_BILA_TRCH_ESTR, 0);
        position += Len.W_B03_ID_BILA_TRCH_ESTR;
        MarshalByte.writeIntAsPacked(buffer, position, wB03CodCompAnia, Len.Int.W_B03_COD_COMP_ANIA, 0);
        position += Len.W_B03_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wB03IdRichEstrazMas, Len.Int.W_B03_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.W_B03_ID_RICH_ESTRAZ_MAS;
        MarshalByte.writeChar(buffer, position, wB03IdRichEstrazAggInd);
        position += Types.CHAR_SIZE;
        wB03IdRichEstrazAgg.getwB03IdRichEstrazAggAsBuffer(buffer, position);
        position += WB03IdRichEstrazAggLlbs0230.Len.W_B03_ID_RICH_ESTRAZ_AGG;
        MarshalByte.writeChar(buffer, position, wB03FlSimulazioneInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlSimulazione);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtRis, Len.W_B03_DT_RIS);
        position += Len.W_B03_DT_RIS;
        MarshalByte.writeString(buffer, position, wB03DtProduzione, Len.W_B03_DT_PRODUZIONE);
        position += Len.W_B03_DT_PRODUZIONE;
        MarshalByte.writeIntAsPacked(buffer, position, wB03IdPoli, Len.Int.W_B03_ID_POLI, 0);
        position += Len.W_B03_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wB03IdAdes, Len.Int.W_B03_ID_ADES, 0);
        position += Len.W_B03_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wB03IdGar, Len.Int.W_B03_ID_GAR, 0);
        position += Len.W_B03_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, wB03IdTrchDiGar, Len.Int.W_B03_ID_TRCH_DI_GAR, 0);
        position += Len.W_B03_ID_TRCH_DI_GAR;
        MarshalByte.writeString(buffer, position, wB03TpFrmAssva, Len.W_B03_TP_FRM_ASSVA);
        position += Len.W_B03_TP_FRM_ASSVA;
        MarshalByte.writeString(buffer, position, wB03TpRamoBila, Len.W_B03_TP_RAMO_BILA);
        position += Len.W_B03_TP_RAMO_BILA;
        MarshalByte.writeString(buffer, position, wB03TpCalcRis, Len.W_B03_TP_CALC_RIS);
        position += Len.W_B03_TP_CALC_RIS;
        MarshalByte.writeString(buffer, position, wB03CodRamo, Len.W_B03_COD_RAMO);
        position += Len.W_B03_COD_RAMO;
        MarshalByte.writeString(buffer, position, wB03CodTari, Len.W_B03_COD_TARI);
        position += Len.W_B03_COD_TARI;
        MarshalByte.writeChar(buffer, position, wB03DtIniValTarInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtIniValTar, Len.W_B03_DT_INI_VAL_TAR);
        position += Len.W_B03_DT_INI_VAL_TAR;
        MarshalByte.writeChar(buffer, position, wB03CodProdInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03CodProd, Len.W_B03_COD_PROD);
        position += Len.W_B03_COD_PROD;
        MarshalByte.writeString(buffer, position, wB03DtIniVldtProd, Len.W_B03_DT_INI_VLDT_PROD);
        position += Len.W_B03_DT_INI_VLDT_PROD;
        MarshalByte.writeChar(buffer, position, wB03CodTariOrgnInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03CodTariOrgn, Len.W_B03_COD_TARI_ORGN);
        position += Len.W_B03_COD_TARI_ORGN;
        MarshalByte.writeChar(buffer, position, wB03MinGartoTInd);
        position += Types.CHAR_SIZE;
        wB03MinGartoT.getwB03MinGartoTAsBuffer(buffer, position);
        position += WB03MinGartoTLlbs0230.Len.W_B03_MIN_GARTO_T;
        MarshalByte.writeChar(buffer, position, wB03TpTariInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpTari, Len.W_B03_TP_TARI);
        position += Len.W_B03_TP_TARI;
        MarshalByte.writeChar(buffer, position, wB03TpPreInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03TpPre);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03TpAdegPreInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03TpAdegPre);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03TpRivalInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpRival, Len.W_B03_TP_RIVAL);
        position += Len.W_B03_TP_RIVAL;
        MarshalByte.writeChar(buffer, position, wB03FlDaTrasfInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlDaTrasf);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlCarContInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlCarCont);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlPreDaRisInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlPreDaRis);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlPreAggInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlPreAgg);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03TpTrchInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpTrch, Len.W_B03_TP_TRCH);
        position += Len.W_B03_TP_TRCH;
        MarshalByte.writeChar(buffer, position, wB03TpTstInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpTst, Len.W_B03_TP_TST);
        position += Len.W_B03_TP_TST;
        MarshalByte.writeChar(buffer, position, wB03CodConvInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03CodConv, Len.W_B03_COD_CONV);
        position += Len.W_B03_COD_CONV;
        MarshalByte.writeString(buffer, position, wB03DtDecorPoli, Len.W_B03_DT_DECOR_POLI);
        position += Len.W_B03_DT_DECOR_POLI;
        MarshalByte.writeChar(buffer, position, wB03DtDecorAdesInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtDecorAdes, Len.W_B03_DT_DECOR_ADES);
        position += Len.W_B03_DT_DECOR_ADES;
        MarshalByte.writeString(buffer, position, wB03DtDecorTrch, Len.W_B03_DT_DECOR_TRCH);
        position += Len.W_B03_DT_DECOR_TRCH;
        MarshalByte.writeString(buffer, position, wB03DtEmisPoli, Len.W_B03_DT_EMIS_POLI);
        position += Len.W_B03_DT_EMIS_POLI;
        MarshalByte.writeChar(buffer, position, wB03DtEmisTrchInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtEmisTrch, Len.W_B03_DT_EMIS_TRCH);
        position += Len.W_B03_DT_EMIS_TRCH;
        MarshalByte.writeChar(buffer, position, wB03DtScadTrchInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtScadTrch, Len.W_B03_DT_SCAD_TRCH);
        position += Len.W_B03_DT_SCAD_TRCH;
        MarshalByte.writeChar(buffer, position, wB03DtScadIntmdInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtScadIntmd, Len.W_B03_DT_SCAD_INTMD);
        position += Len.W_B03_DT_SCAD_INTMD;
        MarshalByte.writeChar(buffer, position, wB03DtScadPagPreInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtScadPagPre, Len.W_B03_DT_SCAD_PAG_PRE);
        position += Len.W_B03_DT_SCAD_PAG_PRE;
        MarshalByte.writeChar(buffer, position, wB03DtUltPrePagInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtUltPrePag, Len.W_B03_DT_ULT_PRE_PAG);
        position += Len.W_B03_DT_ULT_PRE_PAG;
        MarshalByte.writeChar(buffer, position, wB03DtNasc1oAsstoInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtNasc1oAssto, Len.W_B03_DT_NASC1O_ASSTO);
        position += Len.W_B03_DT_NASC1O_ASSTO;
        MarshalByte.writeChar(buffer, position, wB03Sex1oAsstoInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03Sex1oAssto);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03EtaAa1oAsstoInd);
        position += Types.CHAR_SIZE;
        wB03EtaAa1oAssto.getwB03EtaAa1oAsstoAsBuffer(buffer, position);
        position += WB03EtaAa1oAsstoLlbs0230.Len.W_B03_ETA_AA1O_ASSTO;
        MarshalByte.writeChar(buffer, position, wB03EtaMm1oAsstoInd);
        position += Types.CHAR_SIZE;
        wB03EtaMm1oAssto.getwB03EtaMm1oAsstoAsBuffer(buffer, position);
        position += WB03EtaMm1oAsstoLlbs0230.Len.W_B03_ETA_MM1O_ASSTO;
        MarshalByte.writeChar(buffer, position, wB03EtaRaggnDtCalcInd);
        position += Types.CHAR_SIZE;
        wB03EtaRaggnDtCalc.getwB03EtaRaggnDtCalcAsBuffer(buffer, position);
        position += WB03EtaRaggnDtCalcLlbs0230.Len.W_B03_ETA_RAGGN_DT_CALC;
        MarshalByte.writeChar(buffer, position, wB03DurAaInd);
        position += Types.CHAR_SIZE;
        wB03DurAa.getwB03DurAaAsBuffer(buffer, position);
        position += WB03DurAaLlbs0230.Len.W_B03_DUR_AA;
        MarshalByte.writeChar(buffer, position, wB03DurMmInd);
        position += Types.CHAR_SIZE;
        wB03DurMm.getwB03DurMmAsBuffer(buffer, position);
        position += WB03DurMmLlbs0230.Len.W_B03_DUR_MM;
        MarshalByte.writeChar(buffer, position, wB03DurGgInd);
        position += Types.CHAR_SIZE;
        wB03DurGg.getwB03DurGgAsBuffer(buffer, position);
        position += WB03DurGgLlbs0230.Len.W_B03_DUR_GG;
        MarshalByte.writeChar(buffer, position, wB03Dur1oPerAaInd);
        position += Types.CHAR_SIZE;
        wB03Dur1oPerAa.getwB03Dur1oPerAaAsBuffer(buffer, position);
        position += WB03Dur1oPerAaLlbs0230.Len.W_B03_DUR1O_PER_AA;
        MarshalByte.writeChar(buffer, position, wB03Dur1oPerMmInd);
        position += Types.CHAR_SIZE;
        wB03Dur1oPerMm.getwB03Dur1oPerMmAsBuffer(buffer, position);
        position += WB03Dur1oPerMmLlbs0230.Len.W_B03_DUR1O_PER_MM;
        MarshalByte.writeChar(buffer, position, wB03Dur1oPerGgInd);
        position += Types.CHAR_SIZE;
        wB03Dur1oPerGg.getwB03Dur1oPerGgAsBuffer(buffer, position);
        position += WB03Dur1oPerGgLlbs0230.Len.W_B03_DUR1O_PER_GG;
        MarshalByte.writeChar(buffer, position, wB03AntidurRicorPrecInd);
        position += Types.CHAR_SIZE;
        wB03AntidurRicorPrec.getwB03AntidurRicorPrecAsBuffer(buffer, position);
        position += WB03AntidurRicorPrecLlbs0230.Len.W_B03_ANTIDUR_RICOR_PREC;
        MarshalByte.writeChar(buffer, position, wB03AntidurDtCalcInd);
        position += Types.CHAR_SIZE;
        wB03AntidurDtCalc.getwB03AntidurDtCalcAsBuffer(buffer, position);
        position += WB03AntidurDtCalcLlbs0230.Len.W_B03_ANTIDUR_DT_CALC;
        MarshalByte.writeChar(buffer, position, wB03DurResDtCalcInd);
        position += Types.CHAR_SIZE;
        wB03DurResDtCalc.getwB03DurResDtCalcAsBuffer(buffer, position);
        position += WB03DurResDtCalcLlbs0230.Len.W_B03_DUR_RES_DT_CALC;
        MarshalByte.writeString(buffer, position, wB03TpStatBusPoli, Len.W_B03_TP_STAT_BUS_POLI);
        position += Len.W_B03_TP_STAT_BUS_POLI;
        MarshalByte.writeString(buffer, position, wB03TpCausPoli, Len.W_B03_TP_CAUS_POLI);
        position += Len.W_B03_TP_CAUS_POLI;
        MarshalByte.writeString(buffer, position, wB03TpStatBusAdes, Len.W_B03_TP_STAT_BUS_ADES);
        position += Len.W_B03_TP_STAT_BUS_ADES;
        MarshalByte.writeString(buffer, position, wB03TpCausAdes, Len.W_B03_TP_CAUS_ADES);
        position += Len.W_B03_TP_CAUS_ADES;
        MarshalByte.writeString(buffer, position, wB03TpStatBusTrch, Len.W_B03_TP_STAT_BUS_TRCH);
        position += Len.W_B03_TP_STAT_BUS_TRCH;
        MarshalByte.writeString(buffer, position, wB03TpCausTrch, Len.W_B03_TP_CAUS_TRCH);
        position += Len.W_B03_TP_CAUS_TRCH;
        MarshalByte.writeChar(buffer, position, wB03DtEffCambStatInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtEffCambStat, Len.W_B03_DT_EFF_CAMB_STAT);
        position += Len.W_B03_DT_EFF_CAMB_STAT;
        MarshalByte.writeChar(buffer, position, wB03DtEmisCambStatInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtEmisCambStat, Len.W_B03_DT_EMIS_CAMB_STAT);
        position += Len.W_B03_DT_EMIS_CAMB_STAT;
        MarshalByte.writeChar(buffer, position, wB03DtEffStabInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtEffStab, Len.W_B03_DT_EFF_STAB);
        position += Len.W_B03_DT_EFF_STAB;
        MarshalByte.writeChar(buffer, position, wB03CptDtStabInd);
        position += Types.CHAR_SIZE;
        wB03CptDtStab.getwB03CptDtStabAsBuffer(buffer, position);
        position += WB03CptDtStabLlbs0230.Len.W_B03_CPT_DT_STAB;
        MarshalByte.writeChar(buffer, position, wB03DtEffRidzInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtEffRidz, Len.W_B03_DT_EFF_RIDZ);
        position += Len.W_B03_DT_EFF_RIDZ;
        MarshalByte.writeChar(buffer, position, wB03DtEmisRidzInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtEmisRidz, Len.W_B03_DT_EMIS_RIDZ);
        position += Len.W_B03_DT_EMIS_RIDZ;
        MarshalByte.writeChar(buffer, position, wB03CptDtRidzInd);
        position += Types.CHAR_SIZE;
        wB03CptDtRidz.getwB03CptDtRidzAsBuffer(buffer, position);
        position += WB03CptDtRidzLlbs0230.Len.W_B03_CPT_DT_RIDZ;
        MarshalByte.writeChar(buffer, position, wB03FrazInd);
        position += Types.CHAR_SIZE;
        wB03Fraz.getwB03FrazAsBuffer(buffer, position);
        position += WB03FrazLlbs0230.Len.W_B03_FRAZ;
        MarshalByte.writeChar(buffer, position, wB03DurPagPreInd);
        position += Types.CHAR_SIZE;
        wB03DurPagPre.getwB03DurPagPreAsBuffer(buffer, position);
        position += WB03DurPagPreLlbs0230.Len.W_B03_DUR_PAG_PRE;
        MarshalByte.writeChar(buffer, position, wB03NumPrePattInd);
        position += Types.CHAR_SIZE;
        wB03NumPrePatt.getwB03NumPrePattAsBuffer(buffer, position);
        position += WB03NumPrePattLlbs0230.Len.W_B03_NUM_PRE_PATT;
        MarshalByte.writeChar(buffer, position, wB03FrazIniErogRenInd);
        position += Types.CHAR_SIZE;
        wB03FrazIniErogRen.getwB03FrazIniErogRenAsBuffer(buffer, position);
        position += WB03FrazIniErogRenLlbs0230.Len.W_B03_FRAZ_INI_EROG_REN;
        MarshalByte.writeChar(buffer, position, wB03AaRenCerInd);
        position += Types.CHAR_SIZE;
        wB03AaRenCer.getwB03AaRenCerAsBuffer(buffer, position);
        position += WB03AaRenCerLlbs0230.Len.W_B03_AA_REN_CER;
        MarshalByte.writeChar(buffer, position, wB03RatRenInd);
        position += Types.CHAR_SIZE;
        wB03RatRen.getwB03RatRenAsBuffer(buffer, position);
        position += WB03RatRenLlbs0230.Len.W_B03_RAT_REN;
        MarshalByte.writeChar(buffer, position, wB03CodDivInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03CodDiv, Len.W_B03_COD_DIV);
        position += Len.W_B03_COD_DIV;
        MarshalByte.writeChar(buffer, position, wB03RiscparInd);
        position += Types.CHAR_SIZE;
        wB03Riscpar.getwB03RiscparAsBuffer(buffer, position);
        position += WB03RiscparLlbs0230.Len.W_B03_RISCPAR;
        MarshalByte.writeChar(buffer, position, wB03CumRiscparInd);
        position += Types.CHAR_SIZE;
        wB03CumRiscpar.getwB03CumRiscparAsBuffer(buffer, position);
        position += WB03CumRiscparLlbs0230.Len.W_B03_CUM_RISCPAR;
        MarshalByte.writeChar(buffer, position, wB03UltRmInd);
        position += Types.CHAR_SIZE;
        wB03UltRm.getwB03UltRmAsBuffer(buffer, position);
        position += WB03UltRmLlbs0230.Len.W_B03_ULT_RM;
        MarshalByte.writeChar(buffer, position, wB03TsRendtoTInd);
        position += Types.CHAR_SIZE;
        wB03TsRendtoT.getwB03TsRendtoTAsBuffer(buffer, position);
        position += WB03TsRendtoTLlbs0230.Len.W_B03_TS_RENDTO_T;
        MarshalByte.writeChar(buffer, position, wB03AlqRetrTInd);
        position += Types.CHAR_SIZE;
        wB03AlqRetrT.getwB03AlqRetrTAsBuffer(buffer, position);
        position += WB03AlqRetrTLlbs0230.Len.W_B03_ALQ_RETR_T;
        MarshalByte.writeChar(buffer, position, wB03MinTrnutTInd);
        position += Types.CHAR_SIZE;
        wB03MinTrnutT.getwB03MinTrnutTAsBuffer(buffer, position);
        position += WB03MinTrnutTLlbs0230.Len.W_B03_MIN_TRNUT_T;
        MarshalByte.writeChar(buffer, position, wB03TsNetTInd);
        position += Types.CHAR_SIZE;
        wB03TsNetT.getwB03TsNetTAsBuffer(buffer, position);
        position += WB03TsNetTLlbs0230.Len.W_B03_TS_NET_T;
        MarshalByte.writeChar(buffer, position, wB03DtUltRivalInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtUltRival, Len.W_B03_DT_ULT_RIVAL);
        position += Len.W_B03_DT_ULT_RIVAL;
        MarshalByte.writeChar(buffer, position, wB03PrstzIniInd);
        position += Types.CHAR_SIZE;
        wB03PrstzIni.getwB03PrstzIniAsBuffer(buffer, position);
        position += WB03PrstzIniLlbs0230.Len.W_B03_PRSTZ_INI;
        MarshalByte.writeChar(buffer, position, wB03PrstzAggIniInd);
        position += Types.CHAR_SIZE;
        wB03PrstzAggIni.getwB03PrstzAggIniAsBuffer(buffer, position);
        position += WB03PrstzAggIniLlbs0230.Len.W_B03_PRSTZ_AGG_INI;
        MarshalByte.writeChar(buffer, position, wB03PrstzAggUltInd);
        position += Types.CHAR_SIZE;
        wB03PrstzAggUlt.getwB03PrstzAggUltAsBuffer(buffer, position);
        position += WB03PrstzAggUltLlbs0230.Len.W_B03_PRSTZ_AGG_ULT;
        MarshalByte.writeChar(buffer, position, wB03RappelInd);
        position += Types.CHAR_SIZE;
        wB03Rappel.getwB03RappelAsBuffer(buffer, position);
        position += WB03RappelLlbs0230.Len.W_B03_RAPPEL;
        MarshalByte.writeChar(buffer, position, wB03PrePattuitoIniInd);
        position += Types.CHAR_SIZE;
        wB03PrePattuitoIni.getwB03PrePattuitoIniAsBuffer(buffer, position);
        position += WB03PrePattuitoIniLlbs0230.Len.W_B03_PRE_PATTUITO_INI;
        MarshalByte.writeChar(buffer, position, wB03PreDovIniInd);
        position += Types.CHAR_SIZE;
        wB03PreDovIni.getwB03PreDovIniAsBuffer(buffer, position);
        position += WB03PreDovIniLlbs0230.Len.W_B03_PRE_DOV_INI;
        MarshalByte.writeChar(buffer, position, wB03PreDovRivtoTInd);
        position += Types.CHAR_SIZE;
        wB03PreDovRivtoT.getwB03PreDovRivtoTAsBuffer(buffer, position);
        position += WB03PreDovRivtoTLlbs0230.Len.W_B03_PRE_DOV_RIVTO_T;
        MarshalByte.writeChar(buffer, position, wB03PreAnnualizRicorInd);
        position += Types.CHAR_SIZE;
        wB03PreAnnualizRicor.getwB03PreAnnualizRicorAsBuffer(buffer, position);
        position += WB03PreAnnualizRicorLlbs0230.Len.W_B03_PRE_ANNUALIZ_RICOR;
        MarshalByte.writeChar(buffer, position, wB03PreContInd);
        position += Types.CHAR_SIZE;
        wB03PreCont.getwB03PreContAsBuffer(buffer, position);
        position += WB03PreContLlbs0230.Len.W_B03_PRE_CONT;
        MarshalByte.writeChar(buffer, position, wB03PrePpIniInd);
        position += Types.CHAR_SIZE;
        wB03PrePpIni.getwB03PrePpIniAsBuffer(buffer, position);
        position += WB03PrePpIniLlbs0230.Len.W_B03_PRE_PP_INI;
        MarshalByte.writeChar(buffer, position, wB03RisPuraTInd);
        position += Types.CHAR_SIZE;
        wB03RisPuraT.getwB03RisPuraTAsBuffer(buffer, position);
        position += WB03RisPuraTLlbs0230.Len.W_B03_RIS_PURA_T;
        MarshalByte.writeChar(buffer, position, wB03ProvAcqInd);
        position += Types.CHAR_SIZE;
        wB03ProvAcq.getwB03ProvAcqAsBuffer(buffer, position);
        position += WB03ProvAcqLlbs0230.Len.W_B03_PROV_ACQ;
        MarshalByte.writeChar(buffer, position, wB03ProvAcqRicorInd);
        position += Types.CHAR_SIZE;
        wB03ProvAcqRicor.getwB03ProvAcqRicorAsBuffer(buffer, position);
        position += WB03ProvAcqRicorLlbs0230.Len.W_B03_PROV_ACQ_RICOR;
        MarshalByte.writeChar(buffer, position, wB03ProvIncInd);
        position += Types.CHAR_SIZE;
        wB03ProvInc.getwB03ProvIncAsBuffer(buffer, position);
        position += WB03ProvIncLlbs0230.Len.W_B03_PROV_INC;
        MarshalByte.writeChar(buffer, position, wB03CarAcqNonSconInd);
        position += Types.CHAR_SIZE;
        wB03CarAcqNonScon.getwB03CarAcqNonSconAsBuffer(buffer, position);
        position += WB03CarAcqNonSconLlbs0230.Len.W_B03_CAR_ACQ_NON_SCON;
        MarshalByte.writeChar(buffer, position, wB03OverCommInd);
        position += Types.CHAR_SIZE;
        wB03OverComm.getwB03OverCommAsBuffer(buffer, position);
        position += WB03OverCommLlbs0230.Len.W_B03_OVER_COMM;
        MarshalByte.writeChar(buffer, position, wB03CarAcqPrecontatoInd);
        position += Types.CHAR_SIZE;
        wB03CarAcqPrecontato.getwB03CarAcqPrecontatoAsBuffer(buffer, position);
        position += WB03CarAcqPrecontatoLlbs0230.Len.W_B03_CAR_ACQ_PRECONTATO;
        MarshalByte.writeChar(buffer, position, wB03RisAcqTInd);
        position += Types.CHAR_SIZE;
        wB03RisAcqT.getwB03RisAcqTAsBuffer(buffer, position);
        position += WB03RisAcqTLlbs0230.Len.W_B03_RIS_ACQ_T;
        MarshalByte.writeChar(buffer, position, wB03RisZilTInd);
        position += Types.CHAR_SIZE;
        wB03RisZilT.getwB03RisZilTAsBuffer(buffer, position);
        position += WB03RisZilTLlbs0230.Len.W_B03_RIS_ZIL_T;
        MarshalByte.writeChar(buffer, position, wB03CarGestNonSconInd);
        position += Types.CHAR_SIZE;
        wB03CarGestNonScon.getwB03CarGestNonSconAsBuffer(buffer, position);
        position += WB03CarGestNonSconLlbs0230.Len.W_B03_CAR_GEST_NON_SCON;
        MarshalByte.writeChar(buffer, position, wB03CarGestInd);
        position += Types.CHAR_SIZE;
        wB03CarGest.getwB03CarGestAsBuffer(buffer, position);
        position += WB03CarGestLlbs0230.Len.W_B03_CAR_GEST;
        MarshalByte.writeChar(buffer, position, wB03RisSpeTInd);
        position += Types.CHAR_SIZE;
        wB03RisSpeT.getwB03RisSpeTAsBuffer(buffer, position);
        position += WB03RisSpeTLlbs0230.Len.W_B03_RIS_SPE_T;
        MarshalByte.writeChar(buffer, position, wB03CarIncNonSconInd);
        position += Types.CHAR_SIZE;
        wB03CarIncNonScon.getwB03CarIncNonSconAsBuffer(buffer, position);
        position += WB03CarIncNonSconLlbs0230.Len.W_B03_CAR_INC_NON_SCON;
        MarshalByte.writeChar(buffer, position, wB03CarIncInd);
        position += Types.CHAR_SIZE;
        wB03CarInc.getwB03CarIncAsBuffer(buffer, position);
        position += WB03CarIncLlbs0230.Len.W_B03_CAR_INC;
        MarshalByte.writeChar(buffer, position, wB03RisRistorniCapInd);
        position += Types.CHAR_SIZE;
        wB03RisRistorniCap.getwB03RisRistorniCapAsBuffer(buffer, position);
        position += WB03RisRistorniCapLlbs0230.Len.W_B03_RIS_RISTORNI_CAP;
        MarshalByte.writeChar(buffer, position, wB03IntrTecnInd);
        position += Types.CHAR_SIZE;
        wB03IntrTecn.getwB03IntrTecnAsBuffer(buffer, position);
        position += WB03IntrTecnLlbs0230.Len.W_B03_INTR_TECN;
        MarshalByte.writeChar(buffer, position, wB03CptRshMorInd);
        position += Types.CHAR_SIZE;
        wB03CptRshMor.getwB03CptRshMorAsBuffer(buffer, position);
        position += WB03CptRshMorLlbs0230.Len.W_B03_CPT_RSH_MOR;
        MarshalByte.writeChar(buffer, position, wB03CSubrshTInd);
        position += Types.CHAR_SIZE;
        wB03CSubrshT.getwB03CSubrshTAsBuffer(buffer, position);
        position += WB03CSubrshTLlbs0230.Len.W_B03_C_SUBRSH_T;
        MarshalByte.writeChar(buffer, position, wB03PreRshTInd);
        position += Types.CHAR_SIZE;
        wB03PreRshT.getwB03PreRshTAsBuffer(buffer, position);
        position += WB03PreRshTLlbs0230.Len.W_B03_PRE_RSH_T;
        MarshalByte.writeChar(buffer, position, wB03AlqMargRisInd);
        position += Types.CHAR_SIZE;
        wB03AlqMargRis.getwB03AlqMargRisAsBuffer(buffer, position);
        position += WB03AlqMargRisLlbs0230.Len.W_B03_ALQ_MARG_RIS;
        MarshalByte.writeChar(buffer, position, wB03AlqMargCSubrshInd);
        position += Types.CHAR_SIZE;
        wB03AlqMargCSubrsh.getwB03AlqMargCSubrshAsBuffer(buffer, position);
        position += WB03AlqMargCSubrshLlbs0230.Len.W_B03_ALQ_MARG_C_SUBRSH;
        MarshalByte.writeChar(buffer, position, wB03TsRendtoSpprInd);
        position += Types.CHAR_SIZE;
        wB03TsRendtoSppr.getwB03TsRendtoSpprAsBuffer(buffer, position);
        position += WB03TsRendtoSpprLlbs0230.Len.W_B03_TS_RENDTO_SPPR;
        MarshalByte.writeChar(buffer, position, wB03TpIasInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpIas, Len.W_B03_TP_IAS);
        position += Len.W_B03_TP_IAS;
        MarshalByte.writeChar(buffer, position, wB03NsQuoInd);
        position += Types.CHAR_SIZE;
        wB03NsQuo.getwB03NsQuoAsBuffer(buffer, position);
        position += WB03NsQuoLlbs0230.Len.W_B03_NS_QUO;
        MarshalByte.writeChar(buffer, position, wB03TsMedioInd);
        position += Types.CHAR_SIZE;
        wB03TsMedio.getwB03TsMedioAsBuffer(buffer, position);
        position += WB03TsMedioLlbs0230.Len.W_B03_TS_MEDIO;
        MarshalByte.writeChar(buffer, position, wB03CptRiastoInd);
        position += Types.CHAR_SIZE;
        wB03CptRiasto.getwB03CptRiastoAsBuffer(buffer, position);
        position += WB03CptRiastoLlbs0230.Len.W_B03_CPT_RIASTO;
        MarshalByte.writeChar(buffer, position, wB03PreRiastoInd);
        position += Types.CHAR_SIZE;
        wB03PreRiasto.getwB03PreRiastoAsBuffer(buffer, position);
        position += WB03PreRiastoLlbs0230.Len.W_B03_PRE_RIASTO;
        MarshalByte.writeChar(buffer, position, wB03RisRiastaInd);
        position += Types.CHAR_SIZE;
        wB03RisRiasta.getwB03RisRiastaAsBuffer(buffer, position);
        position += WB03RisRiastaLlbs0230.Len.W_B03_RIS_RIASTA;
        MarshalByte.writeChar(buffer, position, wB03CptRiastoEccInd);
        position += Types.CHAR_SIZE;
        wB03CptRiastoEcc.getwB03CptRiastoEccAsBuffer(buffer, position);
        position += WB03CptRiastoEccLlbs0230.Len.W_B03_CPT_RIASTO_ECC;
        MarshalByte.writeChar(buffer, position, wB03PreRiastoEccInd);
        position += Types.CHAR_SIZE;
        wB03PreRiastoEcc.getwB03PreRiastoEccAsBuffer(buffer, position);
        position += WB03PreRiastoEccLlbs0230.Len.W_B03_PRE_RIASTO_ECC;
        MarshalByte.writeChar(buffer, position, wB03RisRiastaEccInd);
        position += Types.CHAR_SIZE;
        wB03RisRiastaEcc.getwB03RisRiastaEccAsBuffer(buffer, position);
        position += WB03RisRiastaEccLlbs0230.Len.W_B03_RIS_RIASTA_ECC;
        MarshalByte.writeChar(buffer, position, wB03CodAgeInd);
        position += Types.CHAR_SIZE;
        wB03CodAge.getwB03CodAgeAsBuffer(buffer, position);
        position += WB03CodAgeLlbs0230.Len.W_B03_COD_AGE;
        MarshalByte.writeChar(buffer, position, wB03CodSubageInd);
        position += Types.CHAR_SIZE;
        wB03CodSubage.getwB03CodSubageAsBuffer(buffer, position);
        position += WB03CodSubageLlbs0230.Len.W_B03_COD_SUBAGE;
        MarshalByte.writeChar(buffer, position, wB03CodCanInd);
        position += Types.CHAR_SIZE;
        wB03CodCan.getwB03CodCanAsBuffer(buffer, position);
        position += WB03CodCanLlbs0230.Len.W_B03_COD_CAN;
        MarshalByte.writeChar(buffer, position, wB03IbPoliInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03IbPoli, Len.W_B03_IB_POLI);
        position += Len.W_B03_IB_POLI;
        MarshalByte.writeChar(buffer, position, wB03IbAdesInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03IbAdes, Len.W_B03_IB_ADES);
        position += Len.W_B03_IB_ADES;
        MarshalByte.writeChar(buffer, position, wB03IbTrchDiGarInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03IbTrchDiGar, Len.W_B03_IB_TRCH_DI_GAR);
        position += Len.W_B03_IB_TRCH_DI_GAR;
        MarshalByte.writeChar(buffer, position, wB03TpPrstzInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpPrstz, Len.W_B03_TP_PRSTZ);
        position += Len.W_B03_TP_PRSTZ;
        MarshalByte.writeChar(buffer, position, wB03TpTrasfInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpTrasf, Len.W_B03_TP_TRASF);
        position += Len.W_B03_TP_TRASF;
        MarshalByte.writeChar(buffer, position, wB03PpInvrioTariInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03PpInvrioTari);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03CoeffOpzRenInd);
        position += Types.CHAR_SIZE;
        wB03CoeffOpzRen.getwB03CoeffOpzRenAsBuffer(buffer, position);
        position += WB03CoeffOpzRenLlbs0230.Len.W_B03_COEFF_OPZ_REN;
        MarshalByte.writeChar(buffer, position, wB03CoeffOpzCptInd);
        position += Types.CHAR_SIZE;
        wB03CoeffOpzCpt.getwB03CoeffOpzCptAsBuffer(buffer, position);
        position += WB03CoeffOpzCptLlbs0230.Len.W_B03_COEFF_OPZ_CPT;
        MarshalByte.writeChar(buffer, position, wB03DurPagRenInd);
        position += Types.CHAR_SIZE;
        wB03DurPagRen.getwB03DurPagRenAsBuffer(buffer, position);
        position += WB03DurPagRenLlbs0230.Len.W_B03_DUR_PAG_REN;
        MarshalByte.writeChar(buffer, position, wB03VltInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03Vlt, Len.W_B03_VLT);
        position += Len.W_B03_VLT;
        MarshalByte.writeChar(buffer, position, wB03RisMatChiuPrecInd);
        position += Types.CHAR_SIZE;
        wB03RisMatChiuPrec.getwB03RisMatChiuPrecAsBuffer(buffer, position);
        position += WB03RisMatChiuPrecLlbs0230.Len.W_B03_RIS_MAT_CHIU_PREC;
        MarshalByte.writeChar(buffer, position, wB03CodFndInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03CodFnd, Len.W_B03_COD_FND);
        position += Len.W_B03_COD_FND;
        MarshalByte.writeChar(buffer, position, wB03PrstzTInd);
        position += Types.CHAR_SIZE;
        wB03PrstzT.getwB03PrstzTAsBuffer(buffer, position);
        position += WB03PrstzTLlbs0230.Len.W_B03_PRSTZ_T;
        MarshalByte.writeChar(buffer, position, wB03TsTariDovInd);
        position += Types.CHAR_SIZE;
        wB03TsTariDov.getwB03TsTariDovAsBuffer(buffer, position);
        position += WB03TsTariDovLlbs0230.Len.W_B03_TS_TARI_DOV;
        MarshalByte.writeChar(buffer, position, wB03TsTariSconInd);
        position += Types.CHAR_SIZE;
        wB03TsTariScon.getwB03TsTariSconAsBuffer(buffer, position);
        position += WB03TsTariSconLlbs0230.Len.W_B03_TS_TARI_SCON;
        MarshalByte.writeChar(buffer, position, wB03TsPpInd);
        position += Types.CHAR_SIZE;
        wB03TsPp.getwB03TsPpAsBuffer(buffer, position);
        position += WB03TsPpLlbs0230.Len.W_B03_TS_PP;
        MarshalByte.writeChar(buffer, position, wB03CoeffRis1TInd);
        position += Types.CHAR_SIZE;
        wB03CoeffRis1T.getwB03CoeffRis1TAsBuffer(buffer, position);
        position += WB03CoeffRis1TLlbs0230.Len.W_B03_COEFF_RIS1_T;
        MarshalByte.writeChar(buffer, position, wB03CoeffRis2TInd);
        position += Types.CHAR_SIZE;
        wB03CoeffRis2T.getwB03CoeffRis2TAsBuffer(buffer, position);
        position += WB03CoeffRis2TLlbs0230.Len.W_B03_COEFF_RIS2_T;
        MarshalByte.writeChar(buffer, position, wB03AbbInd);
        position += Types.CHAR_SIZE;
        wB03Abb.getwB03AbbAsBuffer(buffer, position);
        position += WB03AbbLlbs0230.Len.W_B03_ABB;
        MarshalByte.writeChar(buffer, position, wB03TpCoassInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpCoass, Len.W_B03_TP_COASS);
        position += Len.W_B03_TP_COASS;
        MarshalByte.writeChar(buffer, position, wB03TratRiassInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TratRiass, Len.W_B03_TRAT_RIASS);
        position += Len.W_B03_TRAT_RIASS;
        MarshalByte.writeChar(buffer, position, wB03TratRiassEccInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TratRiassEcc, Len.W_B03_TRAT_RIASS_ECC);
        position += Len.W_B03_TRAT_RIASS_ECC;
        MarshalByte.writeChar(buffer, position, wB03DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wB03DsVer, Len.Int.W_B03_DS_VER, 0);
        position += Len.W_B03_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wB03DsTsCptz, Len.Int.W_B03_DS_TS_CPTZ, 0);
        position += Len.W_B03_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wB03DsUtente, Len.W_B03_DS_UTENTE);
        position += Len.W_B03_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wB03DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03TpRgmFiscInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpRgmFisc, Len.W_B03_TP_RGM_FISC);
        position += Len.W_B03_TP_RGM_FISC;
        MarshalByte.writeChar(buffer, position, wB03DurGarAaInd);
        position += Types.CHAR_SIZE;
        wB03DurGarAa.getwB03DurGarAaAsBuffer(buffer, position);
        position += WB03DurGarAaLlbs0230.Len.W_B03_DUR_GAR_AA;
        MarshalByte.writeChar(buffer, position, wB03DurGarMmInd);
        position += Types.CHAR_SIZE;
        wB03DurGarMm.getwB03DurGarMmAsBuffer(buffer, position);
        position += WB03DurGarMmLlbs0230.Len.W_B03_DUR_GAR_MM;
        MarshalByte.writeChar(buffer, position, wB03DurGarGgInd);
        position += Types.CHAR_SIZE;
        wB03DurGarGg.getwB03DurGarGgAsBuffer(buffer, position);
        position += WB03DurGarGgLlbs0230.Len.W_B03_DUR_GAR_GG;
        MarshalByte.writeChar(buffer, position, wB03AntidurCalc365Ind);
        position += Types.CHAR_SIZE;
        wB03AntidurCalc365.getwB03AntidurCalc365AsBuffer(buffer, position);
        position += WB03AntidurCalc365Llbs0230.Len.W_B03_ANTIDUR_CALC365;
        MarshalByte.writeChar(buffer, position, wB03CodFiscCntrInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03CodFiscCntr, Len.W_B03_COD_FISC_CNTR);
        position += Len.W_B03_COD_FISC_CNTR;
        MarshalByte.writeChar(buffer, position, wB03CodFiscAssto1Ind);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03CodFiscAssto1, Len.W_B03_COD_FISC_ASSTO1);
        position += Len.W_B03_COD_FISC_ASSTO1;
        MarshalByte.writeChar(buffer, position, wB03CodFiscAssto2Ind);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03CodFiscAssto2, Len.W_B03_COD_FISC_ASSTO2);
        position += Len.W_B03_COD_FISC_ASSTO2;
        MarshalByte.writeChar(buffer, position, wB03CodFiscAssto3Ind);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03CodFiscAssto3, Len.W_B03_COD_FISC_ASSTO3);
        position += Len.W_B03_COD_FISC_ASSTO3;
        MarshalByte.writeChar(buffer, position, wB03CausSconInd);
        position += Types.CHAR_SIZE;
        getwB03CausSconVcharBytes(buffer, position);
        position += Len.W_B03_CAUS_SCON_VCHAR;
        MarshalByte.writeChar(buffer, position, wB03EmitTitOpzInd);
        position += Types.CHAR_SIZE;
        getwB03EmitTitOpzVcharBytes(buffer, position);
        position += Len.W_B03_EMIT_TIT_OPZ_VCHAR;
        MarshalByte.writeChar(buffer, position, wB03QtzSpZCoupEmisInd);
        position += Types.CHAR_SIZE;
        wB03QtzSpZCoupEmis.getwB03QtzSpZCoupEmisAsBuffer(buffer, position);
        position += WB03QtzSpZCoupEmisLlbs0230.Len.W_B03_QTZ_SP_Z_COUP_EMIS;
        MarshalByte.writeChar(buffer, position, wB03QtzSpZOpzEmisInd);
        position += Types.CHAR_SIZE;
        wB03QtzSpZOpzEmis.getwB03QtzSpZOpzEmisAsBuffer(buffer, position);
        position += WB03QtzSpZOpzEmisLlbs0230.Len.W_B03_QTZ_SP_Z_OPZ_EMIS;
        MarshalByte.writeChar(buffer, position, wB03QtzSpZCoupDtCInd);
        position += Types.CHAR_SIZE;
        wB03QtzSpZCoupDtC.getwB03QtzSpZCoupDtCAsBuffer(buffer, position);
        position += WB03QtzSpZCoupDtCLlbs0230.Len.W_B03_QTZ_SP_Z_COUP_DT_C;
        MarshalByte.writeChar(buffer, position, wB03QtzSpZOpzDtCaInd);
        position += Types.CHAR_SIZE;
        wB03QtzSpZOpzDtCa.getwB03QtzSpZOpzDtCaAsBuffer(buffer, position);
        position += WB03QtzSpZOpzDtCaLlbs0230.Len.W_B03_QTZ_SP_Z_OPZ_DT_CA;
        MarshalByte.writeChar(buffer, position, wB03QtzTotEmisInd);
        position += Types.CHAR_SIZE;
        wB03QtzTotEmis.getwB03QtzTotEmisAsBuffer(buffer, position);
        position += WB03QtzTotEmisLlbs0230.Len.W_B03_QTZ_TOT_EMIS;
        MarshalByte.writeChar(buffer, position, wB03QtzTotDtCalcInd);
        position += Types.CHAR_SIZE;
        wB03QtzTotDtCalc.getwB03QtzTotDtCalcAsBuffer(buffer, position);
        position += WB03QtzTotDtCalcLlbs0230.Len.W_B03_QTZ_TOT_DT_CALC;
        MarshalByte.writeChar(buffer, position, wB03QtzTotDtUltBilInd);
        position += Types.CHAR_SIZE;
        wB03QtzTotDtUltBil.getwB03QtzTotDtUltBilAsBuffer(buffer, position);
        position += WB03QtzTotDtUltBilLlbs0230.Len.W_B03_QTZ_TOT_DT_ULT_BIL;
        MarshalByte.writeChar(buffer, position, wB03DtQtzEmisInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtQtzEmis, Len.W_B03_DT_QTZ_EMIS);
        position += Len.W_B03_DT_QTZ_EMIS;
        MarshalByte.writeChar(buffer, position, wB03PcCarGestInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, wB03PcCarGest.copy());
        position += Len.W_B03_PC_CAR_GEST;
        MarshalByte.writeChar(buffer, position, wB03PcCarAcqInd);
        position += Types.CHAR_SIZE;
        wB03PcCarAcq.getwB03PcCarAcqAsBuffer(buffer, position);
        position += WB03PcCarAcqLlbs0230.Len.W_B03_PC_CAR_ACQ;
        MarshalByte.writeChar(buffer, position, wB03ImpCarCasoMorInd);
        position += Types.CHAR_SIZE;
        wB03ImpCarCasoMor.getwB03ImpCarCasoMorAsBuffer(buffer, position);
        position += WB03ImpCarCasoMorLlbs0230.Len.W_B03_IMP_CAR_CASO_MOR;
        MarshalByte.writeChar(buffer, position, wB03PcCarMorInd);
        position += Types.CHAR_SIZE;
        wB03PcCarMor.getwB03PcCarMorAsBuffer(buffer, position);
        position += WB03PcCarMorLlbs0230.Len.W_B03_PC_CAR_MOR;
        MarshalByte.writeChar(buffer, position, wB03TpVersInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03TpVers);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlSwitchInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlSwitch);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlIasInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FlIas);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03DirInd);
        position += Types.CHAR_SIZE;
        wB03Dir.getwB03DirAsBuffer(buffer, position);
        position += WB03DirLlbs0230.Len.W_B03_DIR;
        MarshalByte.writeChar(buffer, position, wB03TpCopCasoMorInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpCopCasoMor, Len.W_B03_TP_COP_CASO_MOR);
        position += Len.W_B03_TP_COP_CASO_MOR;
        MarshalByte.writeChar(buffer, position, wB03MetRiscSpclInd);
        position += Types.CHAR_SIZE;
        wB03MetRiscSpcl.getwB03MetRiscSpclAsBuffer(buffer, position);
        position += WB03MetRiscSpclLlbs0230.Len.W_B03_MET_RISC_SPCL;
        MarshalByte.writeChar(buffer, position, wB03TpStatInvstInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpStatInvst, Len.W_B03_TP_STAT_INVST);
        position += Len.W_B03_TP_STAT_INVST;
        MarshalByte.writeChar(buffer, position, wB03CodPrdtInd);
        position += Types.CHAR_SIZE;
        wB03CodPrdt.getwB03CodPrdtAsBuffer(buffer, position);
        position += WB03CodPrdtLlbs0230.Len.W_B03_COD_PRDT;
        MarshalByte.writeChar(buffer, position, wB03StatAssto1Ind);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03StatAssto1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03StatAssto2Ind);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03StatAssto2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03StatAssto3Ind);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03StatAssto3);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03CptAsstoIniMorInd);
        position += Types.CHAR_SIZE;
        wB03CptAsstoIniMor.getwB03CptAsstoIniMorAsBuffer(buffer, position);
        position += WB03CptAsstoIniMorLlbs0230.Len.W_B03_CPT_ASSTO_INI_MOR;
        MarshalByte.writeChar(buffer, position, wB03TsStabPreInd);
        position += Types.CHAR_SIZE;
        wB03TsStabPre.getwB03TsStabPreAsBuffer(buffer, position);
        position += WB03TsStabPreLlbs0230.Len.W_B03_TS_STAB_PRE;
        MarshalByte.writeChar(buffer, position, wB03DirEmisInd);
        position += Types.CHAR_SIZE;
        wB03DirEmis.getwB03DirEmisAsBuffer(buffer, position);
        position += WB03DirEmisLlbs0230.Len.W_B03_DIR_EMIS;
        MarshalByte.writeChar(buffer, position, wB03DtIncUltPreInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03DtIncUltPre, Len.W_B03_DT_INC_ULT_PRE);
        position += Len.W_B03_DT_INC_ULT_PRE;
        MarshalByte.writeChar(buffer, position, wB03StatTbgcAssto1Ind);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03StatTbgcAssto1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03StatTbgcAssto2Ind);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03StatTbgcAssto2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03StatTbgcAssto3Ind);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03StatTbgcAssto3);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB03FrazDecrCptInd);
        position += Types.CHAR_SIZE;
        wB03FrazDecrCpt.getwB03FrazDecrCptAsBuffer(buffer, position);
        position += WB03FrazDecrCptLlbs0230.Len.W_B03_FRAZ_DECR_CPT;
        MarshalByte.writeChar(buffer, position, wB03PrePpUltInd);
        position += Types.CHAR_SIZE;
        wB03PrePpUlt.getwB03PrePpUltAsBuffer(buffer, position);
        position += WB03PrePpUltLlbs0230.Len.W_B03_PRE_PP_ULT;
        MarshalByte.writeChar(buffer, position, wB03AcqExpInd);
        position += Types.CHAR_SIZE;
        wB03AcqExp.getwB03AcqExpAsBuffer(buffer, position);
        position += WB03AcqExpLlbs0230.Len.W_B03_ACQ_EXP;
        MarshalByte.writeChar(buffer, position, wB03RemunAssInd);
        position += Types.CHAR_SIZE;
        wB03RemunAss.getwB03RemunAssAsBuffer(buffer, position);
        position += WB03RemunAssLlbs0230.Len.W_B03_REMUN_ASS;
        MarshalByte.writeChar(buffer, position, wB03CommisInterInd);
        position += Types.CHAR_SIZE;
        wB03CommisInter.getwB03CommisInterAsBuffer(buffer, position);
        position += WB03CommisInterLlbs0230.Len.W_B03_COMMIS_INTER;
        MarshalByte.writeChar(buffer, position, wB03NumFinanzInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03NumFinanz, Len.W_B03_NUM_FINANZ);
        position += Len.W_B03_NUM_FINANZ;
        MarshalByte.writeChar(buffer, position, wB03TpAccCommInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03TpAccComm, Len.W_B03_TP_ACC_COMM);
        position += Len.W_B03_TP_ACC_COMM;
        MarshalByte.writeChar(buffer, position, wB03IbAccCommInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB03IbAccComm, Len.W_B03_IB_ACC_COMM);
        position += Len.W_B03_IB_ACC_COMM;
        MarshalByte.writeString(buffer, position, wB03RamoBila, Len.W_B03_RAMO_BILA);
        position += Len.W_B03_RAMO_BILA;
        MarshalByte.writeChar(buffer, position, wB03CarzInd);
        position += Types.CHAR_SIZE;
        wB03Carz.getwB03CarzAsBuffer(buffer, position);
        return buffer;
    }

    public void setwB03IdBilaTrchEstr(int wB03IdBilaTrchEstr) {
        this.wB03IdBilaTrchEstr = wB03IdBilaTrchEstr;
    }

    public int getwB03IdBilaTrchEstr() {
        return this.wB03IdBilaTrchEstr;
    }

    public void setwB03CodCompAnia(int wB03CodCompAnia) {
        this.wB03CodCompAnia = wB03CodCompAnia;
    }

    public int getwB03CodCompAnia() {
        return this.wB03CodCompAnia;
    }

    public void setwB03IdRichEstrazMas(int wB03IdRichEstrazMas) {
        this.wB03IdRichEstrazMas = wB03IdRichEstrazMas;
    }

    public int getwB03IdRichEstrazMas() {
        return this.wB03IdRichEstrazMas;
    }

    public void setwB03IdRichEstrazAggInd(char wB03IdRichEstrazAggInd) {
        this.wB03IdRichEstrazAggInd = wB03IdRichEstrazAggInd;
    }

    public void setwB03IdRichEstrazAggIndFormatted(String wB03IdRichEstrazAggInd) {
        setwB03IdRichEstrazAggInd(Functions.charAt(wB03IdRichEstrazAggInd, Types.CHAR_SIZE));
    }

    public char getwB03IdRichEstrazAggInd() {
        return this.wB03IdRichEstrazAggInd;
    }

    public void setwB03FlSimulazioneInd(char wB03FlSimulazioneInd) {
        this.wB03FlSimulazioneInd = wB03FlSimulazioneInd;
    }

    public void setwB03FlSimulazioneIndFormatted(String wB03FlSimulazioneInd) {
        setwB03FlSimulazioneInd(Functions.charAt(wB03FlSimulazioneInd, Types.CHAR_SIZE));
    }

    public char getwB03FlSimulazioneInd() {
        return this.wB03FlSimulazioneInd;
    }

    public void setwB03FlSimulazione(char wB03FlSimulazione) {
        this.wB03FlSimulazione = wB03FlSimulazione;
    }

    public char getwB03FlSimulazione() {
        return this.wB03FlSimulazione;
    }

    public void setwB03DtRis(String wB03DtRis) {
        this.wB03DtRis = Functions.subString(wB03DtRis, Len.W_B03_DT_RIS);
    }

    public String getwB03DtRis() {
        return this.wB03DtRis;
    }

    public void setwB03DtProduzione(String wB03DtProduzione) {
        this.wB03DtProduzione = Functions.subString(wB03DtProduzione, Len.W_B03_DT_PRODUZIONE);
    }

    public String getwB03DtProduzione() {
        return this.wB03DtProduzione;
    }

    public void setwB03IdPoli(int wB03IdPoli) {
        this.wB03IdPoli = wB03IdPoli;
    }

    public int getwB03IdPoli() {
        return this.wB03IdPoli;
    }

    public void setwB03IdAdes(int wB03IdAdes) {
        this.wB03IdAdes = wB03IdAdes;
    }

    public int getwB03IdAdes() {
        return this.wB03IdAdes;
    }

    public void setwB03IdGar(int wB03IdGar) {
        this.wB03IdGar = wB03IdGar;
    }

    public int getwB03IdGar() {
        return this.wB03IdGar;
    }

    public void setwB03IdTrchDiGar(int wB03IdTrchDiGar) {
        this.wB03IdTrchDiGar = wB03IdTrchDiGar;
    }

    public int getwB03IdTrchDiGar() {
        return this.wB03IdTrchDiGar;
    }

    public void setwB03TpFrmAssva(String wB03TpFrmAssva) {
        this.wB03TpFrmAssva = Functions.subString(wB03TpFrmAssva, Len.W_B03_TP_FRM_ASSVA);
    }

    public String getwB03TpFrmAssva() {
        return this.wB03TpFrmAssva;
    }

    public void setwB03TpRamoBila(String wB03TpRamoBila) {
        this.wB03TpRamoBila = Functions.subString(wB03TpRamoBila, Len.W_B03_TP_RAMO_BILA);
    }

    public String getwB03TpRamoBila() {
        return this.wB03TpRamoBila;
    }

    public void setwB03TpCalcRis(String wB03TpCalcRis) {
        this.wB03TpCalcRis = Functions.subString(wB03TpCalcRis, Len.W_B03_TP_CALC_RIS);
    }

    public String getwB03TpCalcRis() {
        return this.wB03TpCalcRis;
    }

    public void setwB03CodRamo(String wB03CodRamo) {
        this.wB03CodRamo = Functions.subString(wB03CodRamo, Len.W_B03_COD_RAMO);
    }

    public String getwB03CodRamo() {
        return this.wB03CodRamo;
    }

    public void setwB03CodTari(String wB03CodTari) {
        this.wB03CodTari = Functions.subString(wB03CodTari, Len.W_B03_COD_TARI);
    }

    public String getwB03CodTari() {
        return this.wB03CodTari;
    }

    public void setwB03DtIniValTarInd(char wB03DtIniValTarInd) {
        this.wB03DtIniValTarInd = wB03DtIniValTarInd;
    }

    public void setwB03DtIniValTarIndFormatted(String wB03DtIniValTarInd) {
        setwB03DtIniValTarInd(Functions.charAt(wB03DtIniValTarInd, Types.CHAR_SIZE));
    }

    public char getwB03DtIniValTarInd() {
        return this.wB03DtIniValTarInd;
    }

    public void setwB03DtIniValTar(String wB03DtIniValTar) {
        this.wB03DtIniValTar = Functions.subString(wB03DtIniValTar, Len.W_B03_DT_INI_VAL_TAR);
    }

    public String getwB03DtIniValTar() {
        return this.wB03DtIniValTar;
    }

    public void setwB03CodProdInd(char wB03CodProdInd) {
        this.wB03CodProdInd = wB03CodProdInd;
    }

    public void setwB03CodProdIndFormatted(String wB03CodProdInd) {
        setwB03CodProdInd(Functions.charAt(wB03CodProdInd, Types.CHAR_SIZE));
    }

    public char getwB03CodProdInd() {
        return this.wB03CodProdInd;
    }

    public void setwB03CodProd(String wB03CodProd) {
        this.wB03CodProd = Functions.subString(wB03CodProd, Len.W_B03_COD_PROD);
    }

    public String getwB03CodProd() {
        return this.wB03CodProd;
    }

    public void setwB03DtIniVldtProd(String wB03DtIniVldtProd) {
        this.wB03DtIniVldtProd = Functions.subString(wB03DtIniVldtProd, Len.W_B03_DT_INI_VLDT_PROD);
    }

    public String getwB03DtIniVldtProd() {
        return this.wB03DtIniVldtProd;
    }

    public void setwB03CodTariOrgnInd(char wB03CodTariOrgnInd) {
        this.wB03CodTariOrgnInd = wB03CodTariOrgnInd;
    }

    public void setwB03CodTariOrgnIndFormatted(String wB03CodTariOrgnInd) {
        setwB03CodTariOrgnInd(Functions.charAt(wB03CodTariOrgnInd, Types.CHAR_SIZE));
    }

    public char getwB03CodTariOrgnInd() {
        return this.wB03CodTariOrgnInd;
    }

    public void setwB03CodTariOrgn(String wB03CodTariOrgn) {
        this.wB03CodTariOrgn = Functions.subString(wB03CodTariOrgn, Len.W_B03_COD_TARI_ORGN);
    }

    public String getwB03CodTariOrgn() {
        return this.wB03CodTariOrgn;
    }

    public void setwB03MinGartoTInd(char wB03MinGartoTInd) {
        this.wB03MinGartoTInd = wB03MinGartoTInd;
    }

    public void setwB03MinGartoTIndFormatted(String wB03MinGartoTInd) {
        setwB03MinGartoTInd(Functions.charAt(wB03MinGartoTInd, Types.CHAR_SIZE));
    }

    public char getwB03MinGartoTInd() {
        return this.wB03MinGartoTInd;
    }

    public void setwB03TpTariInd(char wB03TpTariInd) {
        this.wB03TpTariInd = wB03TpTariInd;
    }

    public void setwB03TpTariIndFormatted(String wB03TpTariInd) {
        setwB03TpTariInd(Functions.charAt(wB03TpTariInd, Types.CHAR_SIZE));
    }

    public char getwB03TpTariInd() {
        return this.wB03TpTariInd;
    }

    public void setwB03TpTari(String wB03TpTari) {
        this.wB03TpTari = Functions.subString(wB03TpTari, Len.W_B03_TP_TARI);
    }

    public String getwB03TpTari() {
        return this.wB03TpTari;
    }

    public void setwB03TpPreInd(char wB03TpPreInd) {
        this.wB03TpPreInd = wB03TpPreInd;
    }

    public void setwB03TpPreIndFormatted(String wB03TpPreInd) {
        setwB03TpPreInd(Functions.charAt(wB03TpPreInd, Types.CHAR_SIZE));
    }

    public char getwB03TpPreInd() {
        return this.wB03TpPreInd;
    }

    public void setwB03TpPre(char wB03TpPre) {
        this.wB03TpPre = wB03TpPre;
    }

    public char getwB03TpPre() {
        return this.wB03TpPre;
    }

    public void setwB03TpAdegPreInd(char wB03TpAdegPreInd) {
        this.wB03TpAdegPreInd = wB03TpAdegPreInd;
    }

    public void setwB03TpAdegPreIndFormatted(String wB03TpAdegPreInd) {
        setwB03TpAdegPreInd(Functions.charAt(wB03TpAdegPreInd, Types.CHAR_SIZE));
    }

    public char getwB03TpAdegPreInd() {
        return this.wB03TpAdegPreInd;
    }

    public void setwB03TpAdegPre(char wB03TpAdegPre) {
        this.wB03TpAdegPre = wB03TpAdegPre;
    }

    public char getwB03TpAdegPre() {
        return this.wB03TpAdegPre;
    }

    public void setwB03TpRivalInd(char wB03TpRivalInd) {
        this.wB03TpRivalInd = wB03TpRivalInd;
    }

    public void setwB03TpRivalIndFormatted(String wB03TpRivalInd) {
        setwB03TpRivalInd(Functions.charAt(wB03TpRivalInd, Types.CHAR_SIZE));
    }

    public char getwB03TpRivalInd() {
        return this.wB03TpRivalInd;
    }

    public void setwB03TpRival(String wB03TpRival) {
        this.wB03TpRival = Functions.subString(wB03TpRival, Len.W_B03_TP_RIVAL);
    }

    public String getwB03TpRival() {
        return this.wB03TpRival;
    }

    public void setwB03FlDaTrasfInd(char wB03FlDaTrasfInd) {
        this.wB03FlDaTrasfInd = wB03FlDaTrasfInd;
    }

    public void setwB03FlDaTrasfIndFormatted(String wB03FlDaTrasfInd) {
        setwB03FlDaTrasfInd(Functions.charAt(wB03FlDaTrasfInd, Types.CHAR_SIZE));
    }

    public char getwB03FlDaTrasfInd() {
        return this.wB03FlDaTrasfInd;
    }

    public void setwB03FlDaTrasf(char wB03FlDaTrasf) {
        this.wB03FlDaTrasf = wB03FlDaTrasf;
    }

    public char getwB03FlDaTrasf() {
        return this.wB03FlDaTrasf;
    }

    public void setwB03FlCarContInd(char wB03FlCarContInd) {
        this.wB03FlCarContInd = wB03FlCarContInd;
    }

    public void setwB03FlCarContIndFormatted(String wB03FlCarContInd) {
        setwB03FlCarContInd(Functions.charAt(wB03FlCarContInd, Types.CHAR_SIZE));
    }

    public char getwB03FlCarContInd() {
        return this.wB03FlCarContInd;
    }

    public void setwB03FlCarCont(char wB03FlCarCont) {
        this.wB03FlCarCont = wB03FlCarCont;
    }

    public char getwB03FlCarCont() {
        return this.wB03FlCarCont;
    }

    public void setwB03FlPreDaRisInd(char wB03FlPreDaRisInd) {
        this.wB03FlPreDaRisInd = wB03FlPreDaRisInd;
    }

    public void setwB03FlPreDaRisIndFormatted(String wB03FlPreDaRisInd) {
        setwB03FlPreDaRisInd(Functions.charAt(wB03FlPreDaRisInd, Types.CHAR_SIZE));
    }

    public char getwB03FlPreDaRisInd() {
        return this.wB03FlPreDaRisInd;
    }

    public void setwB03FlPreDaRis(char wB03FlPreDaRis) {
        this.wB03FlPreDaRis = wB03FlPreDaRis;
    }

    public char getwB03FlPreDaRis() {
        return this.wB03FlPreDaRis;
    }

    public void setwB03FlPreAggInd(char wB03FlPreAggInd) {
        this.wB03FlPreAggInd = wB03FlPreAggInd;
    }

    public void setwB03FlPreAggIndFormatted(String wB03FlPreAggInd) {
        setwB03FlPreAggInd(Functions.charAt(wB03FlPreAggInd, Types.CHAR_SIZE));
    }

    public char getwB03FlPreAggInd() {
        return this.wB03FlPreAggInd;
    }

    public void setwB03FlPreAgg(char wB03FlPreAgg) {
        this.wB03FlPreAgg = wB03FlPreAgg;
    }

    public char getwB03FlPreAgg() {
        return this.wB03FlPreAgg;
    }

    public void setwB03TpTrchInd(char wB03TpTrchInd) {
        this.wB03TpTrchInd = wB03TpTrchInd;
    }

    public void setwB03TpTrchIndFormatted(String wB03TpTrchInd) {
        setwB03TpTrchInd(Functions.charAt(wB03TpTrchInd, Types.CHAR_SIZE));
    }

    public char getwB03TpTrchInd() {
        return this.wB03TpTrchInd;
    }

    public void setwB03TpTrch(String wB03TpTrch) {
        this.wB03TpTrch = Functions.subString(wB03TpTrch, Len.W_B03_TP_TRCH);
    }

    public String getwB03TpTrch() {
        return this.wB03TpTrch;
    }

    public void setwB03TpTstInd(char wB03TpTstInd) {
        this.wB03TpTstInd = wB03TpTstInd;
    }

    public void setwB03TpTstIndFormatted(String wB03TpTstInd) {
        setwB03TpTstInd(Functions.charAt(wB03TpTstInd, Types.CHAR_SIZE));
    }

    public char getwB03TpTstInd() {
        return this.wB03TpTstInd;
    }

    public void setwB03TpTst(String wB03TpTst) {
        this.wB03TpTst = Functions.subString(wB03TpTst, Len.W_B03_TP_TST);
    }

    public String getwB03TpTst() {
        return this.wB03TpTst;
    }

    public void setwB03CodConvInd(char wB03CodConvInd) {
        this.wB03CodConvInd = wB03CodConvInd;
    }

    public void setwB03CodConvIndFormatted(String wB03CodConvInd) {
        setwB03CodConvInd(Functions.charAt(wB03CodConvInd, Types.CHAR_SIZE));
    }

    public char getwB03CodConvInd() {
        return this.wB03CodConvInd;
    }

    public void setwB03CodConv(String wB03CodConv) {
        this.wB03CodConv = Functions.subString(wB03CodConv, Len.W_B03_COD_CONV);
    }

    public String getwB03CodConv() {
        return this.wB03CodConv;
    }

    public void setwB03DtDecorPoli(String wB03DtDecorPoli) {
        this.wB03DtDecorPoli = Functions.subString(wB03DtDecorPoli, Len.W_B03_DT_DECOR_POLI);
    }

    public String getwB03DtDecorPoli() {
        return this.wB03DtDecorPoli;
    }

    public void setwB03DtDecorAdesInd(char wB03DtDecorAdesInd) {
        this.wB03DtDecorAdesInd = wB03DtDecorAdesInd;
    }

    public void setwB03DtDecorAdesIndFormatted(String wB03DtDecorAdesInd) {
        setwB03DtDecorAdesInd(Functions.charAt(wB03DtDecorAdesInd, Types.CHAR_SIZE));
    }

    public char getwB03DtDecorAdesInd() {
        return this.wB03DtDecorAdesInd;
    }

    public void setwB03DtDecorAdes(String wB03DtDecorAdes) {
        this.wB03DtDecorAdes = Functions.subString(wB03DtDecorAdes, Len.W_B03_DT_DECOR_ADES);
    }

    public String getwB03DtDecorAdes() {
        return this.wB03DtDecorAdes;
    }

    public void setwB03DtDecorTrch(String wB03DtDecorTrch) {
        this.wB03DtDecorTrch = Functions.subString(wB03DtDecorTrch, Len.W_B03_DT_DECOR_TRCH);
    }

    public String getwB03DtDecorTrch() {
        return this.wB03DtDecorTrch;
    }

    public void setwB03DtEmisPoli(String wB03DtEmisPoli) {
        this.wB03DtEmisPoli = Functions.subString(wB03DtEmisPoli, Len.W_B03_DT_EMIS_POLI);
    }

    public String getwB03DtEmisPoli() {
        return this.wB03DtEmisPoli;
    }

    public void setwB03DtEmisTrchInd(char wB03DtEmisTrchInd) {
        this.wB03DtEmisTrchInd = wB03DtEmisTrchInd;
    }

    public void setwB03DtEmisTrchIndFormatted(String wB03DtEmisTrchInd) {
        setwB03DtEmisTrchInd(Functions.charAt(wB03DtEmisTrchInd, Types.CHAR_SIZE));
    }

    public char getwB03DtEmisTrchInd() {
        return this.wB03DtEmisTrchInd;
    }

    public void setwB03DtEmisTrch(String wB03DtEmisTrch) {
        this.wB03DtEmisTrch = Functions.subString(wB03DtEmisTrch, Len.W_B03_DT_EMIS_TRCH);
    }

    public String getwB03DtEmisTrch() {
        return this.wB03DtEmisTrch;
    }

    public void setwB03DtScadTrchInd(char wB03DtScadTrchInd) {
        this.wB03DtScadTrchInd = wB03DtScadTrchInd;
    }

    public void setwB03DtScadTrchIndFormatted(String wB03DtScadTrchInd) {
        setwB03DtScadTrchInd(Functions.charAt(wB03DtScadTrchInd, Types.CHAR_SIZE));
    }

    public char getwB03DtScadTrchInd() {
        return this.wB03DtScadTrchInd;
    }

    public void setwB03DtScadTrch(String wB03DtScadTrch) {
        this.wB03DtScadTrch = Functions.subString(wB03DtScadTrch, Len.W_B03_DT_SCAD_TRCH);
    }

    public String getwB03DtScadTrch() {
        return this.wB03DtScadTrch;
    }

    public void setwB03DtScadIntmdInd(char wB03DtScadIntmdInd) {
        this.wB03DtScadIntmdInd = wB03DtScadIntmdInd;
    }

    public void setwB03DtScadIntmdIndFormatted(String wB03DtScadIntmdInd) {
        setwB03DtScadIntmdInd(Functions.charAt(wB03DtScadIntmdInd, Types.CHAR_SIZE));
    }

    public char getwB03DtScadIntmdInd() {
        return this.wB03DtScadIntmdInd;
    }

    public void setwB03DtScadIntmd(String wB03DtScadIntmd) {
        this.wB03DtScadIntmd = Functions.subString(wB03DtScadIntmd, Len.W_B03_DT_SCAD_INTMD);
    }

    public String getwB03DtScadIntmd() {
        return this.wB03DtScadIntmd;
    }

    public void setwB03DtScadPagPreInd(char wB03DtScadPagPreInd) {
        this.wB03DtScadPagPreInd = wB03DtScadPagPreInd;
    }

    public void setwB03DtScadPagPreIndFormatted(String wB03DtScadPagPreInd) {
        setwB03DtScadPagPreInd(Functions.charAt(wB03DtScadPagPreInd, Types.CHAR_SIZE));
    }

    public char getwB03DtScadPagPreInd() {
        return this.wB03DtScadPagPreInd;
    }

    public void setwB03DtScadPagPre(String wB03DtScadPagPre) {
        this.wB03DtScadPagPre = Functions.subString(wB03DtScadPagPre, Len.W_B03_DT_SCAD_PAG_PRE);
    }

    public String getwB03DtScadPagPre() {
        return this.wB03DtScadPagPre;
    }

    public void setwB03DtUltPrePagInd(char wB03DtUltPrePagInd) {
        this.wB03DtUltPrePagInd = wB03DtUltPrePagInd;
    }

    public void setwB03DtUltPrePagIndFormatted(String wB03DtUltPrePagInd) {
        setwB03DtUltPrePagInd(Functions.charAt(wB03DtUltPrePagInd, Types.CHAR_SIZE));
    }

    public char getwB03DtUltPrePagInd() {
        return this.wB03DtUltPrePagInd;
    }

    public void setwB03DtUltPrePag(String wB03DtUltPrePag) {
        this.wB03DtUltPrePag = Functions.subString(wB03DtUltPrePag, Len.W_B03_DT_ULT_PRE_PAG);
    }

    public String getwB03DtUltPrePag() {
        return this.wB03DtUltPrePag;
    }

    public void setwB03DtNasc1oAsstoInd(char wB03DtNasc1oAsstoInd) {
        this.wB03DtNasc1oAsstoInd = wB03DtNasc1oAsstoInd;
    }

    public void setwB03DtNasc1oAsstoIndFormatted(String wB03DtNasc1oAsstoInd) {
        setwB03DtNasc1oAsstoInd(Functions.charAt(wB03DtNasc1oAsstoInd, Types.CHAR_SIZE));
    }

    public char getwB03DtNasc1oAsstoInd() {
        return this.wB03DtNasc1oAsstoInd;
    }

    public void setwB03DtNasc1oAssto(String wB03DtNasc1oAssto) {
        this.wB03DtNasc1oAssto = Functions.subString(wB03DtNasc1oAssto, Len.W_B03_DT_NASC1O_ASSTO);
    }

    public String getwB03DtNasc1oAssto() {
        return this.wB03DtNasc1oAssto;
    }

    public void setwB03Sex1oAsstoInd(char wB03Sex1oAsstoInd) {
        this.wB03Sex1oAsstoInd = wB03Sex1oAsstoInd;
    }

    public void setwB03Sex1oAsstoIndFormatted(String wB03Sex1oAsstoInd) {
        setwB03Sex1oAsstoInd(Functions.charAt(wB03Sex1oAsstoInd, Types.CHAR_SIZE));
    }

    public char getwB03Sex1oAsstoInd() {
        return this.wB03Sex1oAsstoInd;
    }

    public void setwB03Sex1oAssto(char wB03Sex1oAssto) {
        this.wB03Sex1oAssto = wB03Sex1oAssto;
    }

    public char getwB03Sex1oAssto() {
        return this.wB03Sex1oAssto;
    }

    public void setwB03EtaAa1oAsstoInd(char wB03EtaAa1oAsstoInd) {
        this.wB03EtaAa1oAsstoInd = wB03EtaAa1oAsstoInd;
    }

    public void setwB03EtaAa1oAsstoIndFormatted(String wB03EtaAa1oAsstoInd) {
        setwB03EtaAa1oAsstoInd(Functions.charAt(wB03EtaAa1oAsstoInd, Types.CHAR_SIZE));
    }

    public char getwB03EtaAa1oAsstoInd() {
        return this.wB03EtaAa1oAsstoInd;
    }

    public void setwB03EtaMm1oAsstoInd(char wB03EtaMm1oAsstoInd) {
        this.wB03EtaMm1oAsstoInd = wB03EtaMm1oAsstoInd;
    }

    public void setwB03EtaMm1oAsstoIndFormatted(String wB03EtaMm1oAsstoInd) {
        setwB03EtaMm1oAsstoInd(Functions.charAt(wB03EtaMm1oAsstoInd, Types.CHAR_SIZE));
    }

    public char getwB03EtaMm1oAsstoInd() {
        return this.wB03EtaMm1oAsstoInd;
    }

    public void setwB03EtaRaggnDtCalcInd(char wB03EtaRaggnDtCalcInd) {
        this.wB03EtaRaggnDtCalcInd = wB03EtaRaggnDtCalcInd;
    }

    public void setwB03EtaRaggnDtCalcIndFormatted(String wB03EtaRaggnDtCalcInd) {
        setwB03EtaRaggnDtCalcInd(Functions.charAt(wB03EtaRaggnDtCalcInd, Types.CHAR_SIZE));
    }

    public char getwB03EtaRaggnDtCalcInd() {
        return this.wB03EtaRaggnDtCalcInd;
    }

    public void setwB03DurAaInd(char wB03DurAaInd) {
        this.wB03DurAaInd = wB03DurAaInd;
    }

    public void setwB03DurAaIndFormatted(String wB03DurAaInd) {
        setwB03DurAaInd(Functions.charAt(wB03DurAaInd, Types.CHAR_SIZE));
    }

    public char getwB03DurAaInd() {
        return this.wB03DurAaInd;
    }

    public void setwB03DurMmInd(char wB03DurMmInd) {
        this.wB03DurMmInd = wB03DurMmInd;
    }

    public void setwB03DurMmIndFormatted(String wB03DurMmInd) {
        setwB03DurMmInd(Functions.charAt(wB03DurMmInd, Types.CHAR_SIZE));
    }

    public char getwB03DurMmInd() {
        return this.wB03DurMmInd;
    }

    public void setwB03DurGgInd(char wB03DurGgInd) {
        this.wB03DurGgInd = wB03DurGgInd;
    }

    public void setwB03DurGgIndFormatted(String wB03DurGgInd) {
        setwB03DurGgInd(Functions.charAt(wB03DurGgInd, Types.CHAR_SIZE));
    }

    public char getwB03DurGgInd() {
        return this.wB03DurGgInd;
    }

    public void setwB03Dur1oPerAaInd(char wB03Dur1oPerAaInd) {
        this.wB03Dur1oPerAaInd = wB03Dur1oPerAaInd;
    }

    public void setwB03Dur1oPerAaIndFormatted(String wB03Dur1oPerAaInd) {
        setwB03Dur1oPerAaInd(Functions.charAt(wB03Dur1oPerAaInd, Types.CHAR_SIZE));
    }

    public char getwB03Dur1oPerAaInd() {
        return this.wB03Dur1oPerAaInd;
    }

    public void setwB03Dur1oPerMmInd(char wB03Dur1oPerMmInd) {
        this.wB03Dur1oPerMmInd = wB03Dur1oPerMmInd;
    }

    public void setwB03Dur1oPerMmIndFormatted(String wB03Dur1oPerMmInd) {
        setwB03Dur1oPerMmInd(Functions.charAt(wB03Dur1oPerMmInd, Types.CHAR_SIZE));
    }

    public char getwB03Dur1oPerMmInd() {
        return this.wB03Dur1oPerMmInd;
    }

    public void setwB03Dur1oPerGgInd(char wB03Dur1oPerGgInd) {
        this.wB03Dur1oPerGgInd = wB03Dur1oPerGgInd;
    }

    public void setwB03Dur1oPerGgIndFormatted(String wB03Dur1oPerGgInd) {
        setwB03Dur1oPerGgInd(Functions.charAt(wB03Dur1oPerGgInd, Types.CHAR_SIZE));
    }

    public char getwB03Dur1oPerGgInd() {
        return this.wB03Dur1oPerGgInd;
    }

    public void setwB03AntidurRicorPrecInd(char wB03AntidurRicorPrecInd) {
        this.wB03AntidurRicorPrecInd = wB03AntidurRicorPrecInd;
    }

    public void setwB03AntidurRicorPrecIndFormatted(String wB03AntidurRicorPrecInd) {
        setwB03AntidurRicorPrecInd(Functions.charAt(wB03AntidurRicorPrecInd, Types.CHAR_SIZE));
    }

    public char getwB03AntidurRicorPrecInd() {
        return this.wB03AntidurRicorPrecInd;
    }

    public void setwB03AntidurDtCalcInd(char wB03AntidurDtCalcInd) {
        this.wB03AntidurDtCalcInd = wB03AntidurDtCalcInd;
    }

    public void setwB03AntidurDtCalcIndFormatted(String wB03AntidurDtCalcInd) {
        setwB03AntidurDtCalcInd(Functions.charAt(wB03AntidurDtCalcInd, Types.CHAR_SIZE));
    }

    public char getwB03AntidurDtCalcInd() {
        return this.wB03AntidurDtCalcInd;
    }

    public void setwB03DurResDtCalcInd(char wB03DurResDtCalcInd) {
        this.wB03DurResDtCalcInd = wB03DurResDtCalcInd;
    }

    public void setwB03DurResDtCalcIndFormatted(String wB03DurResDtCalcInd) {
        setwB03DurResDtCalcInd(Functions.charAt(wB03DurResDtCalcInd, Types.CHAR_SIZE));
    }

    public char getwB03DurResDtCalcInd() {
        return this.wB03DurResDtCalcInd;
    }

    public void setwB03TpStatBusPoli(String wB03TpStatBusPoli) {
        this.wB03TpStatBusPoli = Functions.subString(wB03TpStatBusPoli, Len.W_B03_TP_STAT_BUS_POLI);
    }

    public String getwB03TpStatBusPoli() {
        return this.wB03TpStatBusPoli;
    }

    public void setwB03TpCausPoli(String wB03TpCausPoli) {
        this.wB03TpCausPoli = Functions.subString(wB03TpCausPoli, Len.W_B03_TP_CAUS_POLI);
    }

    public String getwB03TpCausPoli() {
        return this.wB03TpCausPoli;
    }

    public void setwB03TpStatBusAdes(String wB03TpStatBusAdes) {
        this.wB03TpStatBusAdes = Functions.subString(wB03TpStatBusAdes, Len.W_B03_TP_STAT_BUS_ADES);
    }

    public String getwB03TpStatBusAdes() {
        return this.wB03TpStatBusAdes;
    }

    public void setwB03TpCausAdes(String wB03TpCausAdes) {
        this.wB03TpCausAdes = Functions.subString(wB03TpCausAdes, Len.W_B03_TP_CAUS_ADES);
    }

    public String getwB03TpCausAdes() {
        return this.wB03TpCausAdes;
    }

    public void setwB03TpStatBusTrch(String wB03TpStatBusTrch) {
        this.wB03TpStatBusTrch = Functions.subString(wB03TpStatBusTrch, Len.W_B03_TP_STAT_BUS_TRCH);
    }

    public String getwB03TpStatBusTrch() {
        return this.wB03TpStatBusTrch;
    }

    public void setwB03TpCausTrch(String wB03TpCausTrch) {
        this.wB03TpCausTrch = Functions.subString(wB03TpCausTrch, Len.W_B03_TP_CAUS_TRCH);
    }

    public String getwB03TpCausTrch() {
        return this.wB03TpCausTrch;
    }

    public void setwB03DtEffCambStatInd(char wB03DtEffCambStatInd) {
        this.wB03DtEffCambStatInd = wB03DtEffCambStatInd;
    }

    public void setwB03DtEffCambStatIndFormatted(String wB03DtEffCambStatInd) {
        setwB03DtEffCambStatInd(Functions.charAt(wB03DtEffCambStatInd, Types.CHAR_SIZE));
    }

    public char getwB03DtEffCambStatInd() {
        return this.wB03DtEffCambStatInd;
    }

    public void setwB03DtEffCambStat(String wB03DtEffCambStat) {
        this.wB03DtEffCambStat = Functions.subString(wB03DtEffCambStat, Len.W_B03_DT_EFF_CAMB_STAT);
    }

    public String getwB03DtEffCambStat() {
        return this.wB03DtEffCambStat;
    }

    public void setwB03DtEmisCambStatInd(char wB03DtEmisCambStatInd) {
        this.wB03DtEmisCambStatInd = wB03DtEmisCambStatInd;
    }

    public void setwB03DtEmisCambStatIndFormatted(String wB03DtEmisCambStatInd) {
        setwB03DtEmisCambStatInd(Functions.charAt(wB03DtEmisCambStatInd, Types.CHAR_SIZE));
    }

    public char getwB03DtEmisCambStatInd() {
        return this.wB03DtEmisCambStatInd;
    }

    public void setwB03DtEmisCambStat(String wB03DtEmisCambStat) {
        this.wB03DtEmisCambStat = Functions.subString(wB03DtEmisCambStat, Len.W_B03_DT_EMIS_CAMB_STAT);
    }

    public String getwB03DtEmisCambStat() {
        return this.wB03DtEmisCambStat;
    }

    public void setwB03DtEffStabInd(char wB03DtEffStabInd) {
        this.wB03DtEffStabInd = wB03DtEffStabInd;
    }

    public void setwB03DtEffStabIndFormatted(String wB03DtEffStabInd) {
        setwB03DtEffStabInd(Functions.charAt(wB03DtEffStabInd, Types.CHAR_SIZE));
    }

    public char getwB03DtEffStabInd() {
        return this.wB03DtEffStabInd;
    }

    public void setwB03DtEffStab(String wB03DtEffStab) {
        this.wB03DtEffStab = Functions.subString(wB03DtEffStab, Len.W_B03_DT_EFF_STAB);
    }

    public String getwB03DtEffStab() {
        return this.wB03DtEffStab;
    }

    public void setwB03CptDtStabInd(char wB03CptDtStabInd) {
        this.wB03CptDtStabInd = wB03CptDtStabInd;
    }

    public void setwB03CptDtStabIndFormatted(String wB03CptDtStabInd) {
        setwB03CptDtStabInd(Functions.charAt(wB03CptDtStabInd, Types.CHAR_SIZE));
    }

    public char getwB03CptDtStabInd() {
        return this.wB03CptDtStabInd;
    }

    public void setwB03DtEffRidzInd(char wB03DtEffRidzInd) {
        this.wB03DtEffRidzInd = wB03DtEffRidzInd;
    }

    public void setwB03DtEffRidzIndFormatted(String wB03DtEffRidzInd) {
        setwB03DtEffRidzInd(Functions.charAt(wB03DtEffRidzInd, Types.CHAR_SIZE));
    }

    public char getwB03DtEffRidzInd() {
        return this.wB03DtEffRidzInd;
    }

    public void setwB03DtEffRidz(String wB03DtEffRidz) {
        this.wB03DtEffRidz = Functions.subString(wB03DtEffRidz, Len.W_B03_DT_EFF_RIDZ);
    }

    public String getwB03DtEffRidz() {
        return this.wB03DtEffRidz;
    }

    public void setwB03DtEmisRidzInd(char wB03DtEmisRidzInd) {
        this.wB03DtEmisRidzInd = wB03DtEmisRidzInd;
    }

    public void setwB03DtEmisRidzIndFormatted(String wB03DtEmisRidzInd) {
        setwB03DtEmisRidzInd(Functions.charAt(wB03DtEmisRidzInd, Types.CHAR_SIZE));
    }

    public char getwB03DtEmisRidzInd() {
        return this.wB03DtEmisRidzInd;
    }

    public void setwB03DtEmisRidz(String wB03DtEmisRidz) {
        this.wB03DtEmisRidz = Functions.subString(wB03DtEmisRidz, Len.W_B03_DT_EMIS_RIDZ);
    }

    public String getwB03DtEmisRidz() {
        return this.wB03DtEmisRidz;
    }

    public void setwB03CptDtRidzInd(char wB03CptDtRidzInd) {
        this.wB03CptDtRidzInd = wB03CptDtRidzInd;
    }

    public void setwB03CptDtRidzIndFormatted(String wB03CptDtRidzInd) {
        setwB03CptDtRidzInd(Functions.charAt(wB03CptDtRidzInd, Types.CHAR_SIZE));
    }

    public char getwB03CptDtRidzInd() {
        return this.wB03CptDtRidzInd;
    }

    public void setwB03FrazInd(char wB03FrazInd) {
        this.wB03FrazInd = wB03FrazInd;
    }

    public void setwB03FrazIndFormatted(String wB03FrazInd) {
        setwB03FrazInd(Functions.charAt(wB03FrazInd, Types.CHAR_SIZE));
    }

    public char getwB03FrazInd() {
        return this.wB03FrazInd;
    }

    public void setwB03DurPagPreInd(char wB03DurPagPreInd) {
        this.wB03DurPagPreInd = wB03DurPagPreInd;
    }

    public void setwB03DurPagPreIndFormatted(String wB03DurPagPreInd) {
        setwB03DurPagPreInd(Functions.charAt(wB03DurPagPreInd, Types.CHAR_SIZE));
    }

    public char getwB03DurPagPreInd() {
        return this.wB03DurPagPreInd;
    }

    public void setwB03NumPrePattInd(char wB03NumPrePattInd) {
        this.wB03NumPrePattInd = wB03NumPrePattInd;
    }

    public void setwB03NumPrePattIndFormatted(String wB03NumPrePattInd) {
        setwB03NumPrePattInd(Functions.charAt(wB03NumPrePattInd, Types.CHAR_SIZE));
    }

    public char getwB03NumPrePattInd() {
        return this.wB03NumPrePattInd;
    }

    public void setwB03FrazIniErogRenInd(char wB03FrazIniErogRenInd) {
        this.wB03FrazIniErogRenInd = wB03FrazIniErogRenInd;
    }

    public void setwB03FrazIniErogRenIndFormatted(String wB03FrazIniErogRenInd) {
        setwB03FrazIniErogRenInd(Functions.charAt(wB03FrazIniErogRenInd, Types.CHAR_SIZE));
    }

    public char getwB03FrazIniErogRenInd() {
        return this.wB03FrazIniErogRenInd;
    }

    public void setwB03AaRenCerInd(char wB03AaRenCerInd) {
        this.wB03AaRenCerInd = wB03AaRenCerInd;
    }

    public void setwB03AaRenCerIndFormatted(String wB03AaRenCerInd) {
        setwB03AaRenCerInd(Functions.charAt(wB03AaRenCerInd, Types.CHAR_SIZE));
    }

    public char getwB03AaRenCerInd() {
        return this.wB03AaRenCerInd;
    }

    public void setwB03RatRenInd(char wB03RatRenInd) {
        this.wB03RatRenInd = wB03RatRenInd;
    }

    public void setwB03RatRenIndFormatted(String wB03RatRenInd) {
        setwB03RatRenInd(Functions.charAt(wB03RatRenInd, Types.CHAR_SIZE));
    }

    public char getwB03RatRenInd() {
        return this.wB03RatRenInd;
    }

    public void setwB03CodDivInd(char wB03CodDivInd) {
        this.wB03CodDivInd = wB03CodDivInd;
    }

    public void setwB03CodDivIndFormatted(String wB03CodDivInd) {
        setwB03CodDivInd(Functions.charAt(wB03CodDivInd, Types.CHAR_SIZE));
    }

    public char getwB03CodDivInd() {
        return this.wB03CodDivInd;
    }

    public void setwB03CodDiv(String wB03CodDiv) {
        this.wB03CodDiv = Functions.subString(wB03CodDiv, Len.W_B03_COD_DIV);
    }

    public String getwB03CodDiv() {
        return this.wB03CodDiv;
    }

    public void setwB03RiscparInd(char wB03RiscparInd) {
        this.wB03RiscparInd = wB03RiscparInd;
    }

    public void setwB03RiscparIndFormatted(String wB03RiscparInd) {
        setwB03RiscparInd(Functions.charAt(wB03RiscparInd, Types.CHAR_SIZE));
    }

    public char getwB03RiscparInd() {
        return this.wB03RiscparInd;
    }

    public void setwB03CumRiscparInd(char wB03CumRiscparInd) {
        this.wB03CumRiscparInd = wB03CumRiscparInd;
    }

    public void setwB03CumRiscparIndFormatted(String wB03CumRiscparInd) {
        setwB03CumRiscparInd(Functions.charAt(wB03CumRiscparInd, Types.CHAR_SIZE));
    }

    public char getwB03CumRiscparInd() {
        return this.wB03CumRiscparInd;
    }

    public void setwB03UltRmInd(char wB03UltRmInd) {
        this.wB03UltRmInd = wB03UltRmInd;
    }

    public void setwB03UltRmIndFormatted(String wB03UltRmInd) {
        setwB03UltRmInd(Functions.charAt(wB03UltRmInd, Types.CHAR_SIZE));
    }

    public char getwB03UltRmInd() {
        return this.wB03UltRmInd;
    }

    public void setwB03TsRendtoTInd(char wB03TsRendtoTInd) {
        this.wB03TsRendtoTInd = wB03TsRendtoTInd;
    }

    public void setwB03TsRendtoTIndFormatted(String wB03TsRendtoTInd) {
        setwB03TsRendtoTInd(Functions.charAt(wB03TsRendtoTInd, Types.CHAR_SIZE));
    }

    public char getwB03TsRendtoTInd() {
        return this.wB03TsRendtoTInd;
    }

    public void setwB03AlqRetrTInd(char wB03AlqRetrTInd) {
        this.wB03AlqRetrTInd = wB03AlqRetrTInd;
    }

    public void setwB03AlqRetrTIndFormatted(String wB03AlqRetrTInd) {
        setwB03AlqRetrTInd(Functions.charAt(wB03AlqRetrTInd, Types.CHAR_SIZE));
    }

    public char getwB03AlqRetrTInd() {
        return this.wB03AlqRetrTInd;
    }

    public void setwB03MinTrnutTInd(char wB03MinTrnutTInd) {
        this.wB03MinTrnutTInd = wB03MinTrnutTInd;
    }

    public void setwB03MinTrnutTIndFormatted(String wB03MinTrnutTInd) {
        setwB03MinTrnutTInd(Functions.charAt(wB03MinTrnutTInd, Types.CHAR_SIZE));
    }

    public char getwB03MinTrnutTInd() {
        return this.wB03MinTrnutTInd;
    }

    public void setwB03TsNetTInd(char wB03TsNetTInd) {
        this.wB03TsNetTInd = wB03TsNetTInd;
    }

    public void setwB03TsNetTIndFormatted(String wB03TsNetTInd) {
        setwB03TsNetTInd(Functions.charAt(wB03TsNetTInd, Types.CHAR_SIZE));
    }

    public char getwB03TsNetTInd() {
        return this.wB03TsNetTInd;
    }

    public void setwB03DtUltRivalInd(char wB03DtUltRivalInd) {
        this.wB03DtUltRivalInd = wB03DtUltRivalInd;
    }

    public void setwB03DtUltRivalIndFormatted(String wB03DtUltRivalInd) {
        setwB03DtUltRivalInd(Functions.charAt(wB03DtUltRivalInd, Types.CHAR_SIZE));
    }

    public char getwB03DtUltRivalInd() {
        return this.wB03DtUltRivalInd;
    }

    public void setwB03DtUltRival(String wB03DtUltRival) {
        this.wB03DtUltRival = Functions.subString(wB03DtUltRival, Len.W_B03_DT_ULT_RIVAL);
    }

    public String getwB03DtUltRival() {
        return this.wB03DtUltRival;
    }

    public void setwB03PrstzIniInd(char wB03PrstzIniInd) {
        this.wB03PrstzIniInd = wB03PrstzIniInd;
    }

    public void setwB03PrstzIniIndFormatted(String wB03PrstzIniInd) {
        setwB03PrstzIniInd(Functions.charAt(wB03PrstzIniInd, Types.CHAR_SIZE));
    }

    public char getwB03PrstzIniInd() {
        return this.wB03PrstzIniInd;
    }

    public void setwB03PrstzAggIniInd(char wB03PrstzAggIniInd) {
        this.wB03PrstzAggIniInd = wB03PrstzAggIniInd;
    }

    public void setwB03PrstzAggIniIndFormatted(String wB03PrstzAggIniInd) {
        setwB03PrstzAggIniInd(Functions.charAt(wB03PrstzAggIniInd, Types.CHAR_SIZE));
    }

    public char getwB03PrstzAggIniInd() {
        return this.wB03PrstzAggIniInd;
    }

    public void setwB03PrstzAggUltInd(char wB03PrstzAggUltInd) {
        this.wB03PrstzAggUltInd = wB03PrstzAggUltInd;
    }

    public void setwB03PrstzAggUltIndFormatted(String wB03PrstzAggUltInd) {
        setwB03PrstzAggUltInd(Functions.charAt(wB03PrstzAggUltInd, Types.CHAR_SIZE));
    }

    public char getwB03PrstzAggUltInd() {
        return this.wB03PrstzAggUltInd;
    }

    public void setwB03RappelInd(char wB03RappelInd) {
        this.wB03RappelInd = wB03RappelInd;
    }

    public void setwB03RappelIndFormatted(String wB03RappelInd) {
        setwB03RappelInd(Functions.charAt(wB03RappelInd, Types.CHAR_SIZE));
    }

    public char getwB03RappelInd() {
        return this.wB03RappelInd;
    }

    public void setwB03PrePattuitoIniInd(char wB03PrePattuitoIniInd) {
        this.wB03PrePattuitoIniInd = wB03PrePattuitoIniInd;
    }

    public void setwB03PrePattuitoIniIndFormatted(String wB03PrePattuitoIniInd) {
        setwB03PrePattuitoIniInd(Functions.charAt(wB03PrePattuitoIniInd, Types.CHAR_SIZE));
    }

    public char getwB03PrePattuitoIniInd() {
        return this.wB03PrePattuitoIniInd;
    }

    public void setwB03PreDovIniInd(char wB03PreDovIniInd) {
        this.wB03PreDovIniInd = wB03PreDovIniInd;
    }

    public void setwB03PreDovIniIndFormatted(String wB03PreDovIniInd) {
        setwB03PreDovIniInd(Functions.charAt(wB03PreDovIniInd, Types.CHAR_SIZE));
    }

    public char getwB03PreDovIniInd() {
        return this.wB03PreDovIniInd;
    }

    public void setwB03PreDovRivtoTInd(char wB03PreDovRivtoTInd) {
        this.wB03PreDovRivtoTInd = wB03PreDovRivtoTInd;
    }

    public void setwB03PreDovRivtoTIndFormatted(String wB03PreDovRivtoTInd) {
        setwB03PreDovRivtoTInd(Functions.charAt(wB03PreDovRivtoTInd, Types.CHAR_SIZE));
    }

    public char getwB03PreDovRivtoTInd() {
        return this.wB03PreDovRivtoTInd;
    }

    public void setwB03PreAnnualizRicorInd(char wB03PreAnnualizRicorInd) {
        this.wB03PreAnnualizRicorInd = wB03PreAnnualizRicorInd;
    }

    public void setwB03PreAnnualizRicorIndFormatted(String wB03PreAnnualizRicorInd) {
        setwB03PreAnnualizRicorInd(Functions.charAt(wB03PreAnnualizRicorInd, Types.CHAR_SIZE));
    }

    public char getwB03PreAnnualizRicorInd() {
        return this.wB03PreAnnualizRicorInd;
    }

    public void setwB03PreContInd(char wB03PreContInd) {
        this.wB03PreContInd = wB03PreContInd;
    }

    public void setwB03PreContIndFormatted(String wB03PreContInd) {
        setwB03PreContInd(Functions.charAt(wB03PreContInd, Types.CHAR_SIZE));
    }

    public char getwB03PreContInd() {
        return this.wB03PreContInd;
    }

    public void setwB03PrePpIniInd(char wB03PrePpIniInd) {
        this.wB03PrePpIniInd = wB03PrePpIniInd;
    }

    public void setwB03PrePpIniIndFormatted(String wB03PrePpIniInd) {
        setwB03PrePpIniInd(Functions.charAt(wB03PrePpIniInd, Types.CHAR_SIZE));
    }

    public char getwB03PrePpIniInd() {
        return this.wB03PrePpIniInd;
    }

    public void setwB03RisPuraTInd(char wB03RisPuraTInd) {
        this.wB03RisPuraTInd = wB03RisPuraTInd;
    }

    public void setwB03RisPuraTIndFormatted(String wB03RisPuraTInd) {
        setwB03RisPuraTInd(Functions.charAt(wB03RisPuraTInd, Types.CHAR_SIZE));
    }

    public char getwB03RisPuraTInd() {
        return this.wB03RisPuraTInd;
    }

    public void setwB03ProvAcqInd(char wB03ProvAcqInd) {
        this.wB03ProvAcqInd = wB03ProvAcqInd;
    }

    public void setwB03ProvAcqIndFormatted(String wB03ProvAcqInd) {
        setwB03ProvAcqInd(Functions.charAt(wB03ProvAcqInd, Types.CHAR_SIZE));
    }

    public char getwB03ProvAcqInd() {
        return this.wB03ProvAcqInd;
    }

    public void setwB03ProvAcqRicorInd(char wB03ProvAcqRicorInd) {
        this.wB03ProvAcqRicorInd = wB03ProvAcqRicorInd;
    }

    public void setwB03ProvAcqRicorIndFormatted(String wB03ProvAcqRicorInd) {
        setwB03ProvAcqRicorInd(Functions.charAt(wB03ProvAcqRicorInd, Types.CHAR_SIZE));
    }

    public char getwB03ProvAcqRicorInd() {
        return this.wB03ProvAcqRicorInd;
    }

    public void setwB03ProvIncInd(char wB03ProvIncInd) {
        this.wB03ProvIncInd = wB03ProvIncInd;
    }

    public void setwB03ProvIncIndFormatted(String wB03ProvIncInd) {
        setwB03ProvIncInd(Functions.charAt(wB03ProvIncInd, Types.CHAR_SIZE));
    }

    public char getwB03ProvIncInd() {
        return this.wB03ProvIncInd;
    }

    public void setwB03CarAcqNonSconInd(char wB03CarAcqNonSconInd) {
        this.wB03CarAcqNonSconInd = wB03CarAcqNonSconInd;
    }

    public void setwB03CarAcqNonSconIndFormatted(String wB03CarAcqNonSconInd) {
        setwB03CarAcqNonSconInd(Functions.charAt(wB03CarAcqNonSconInd, Types.CHAR_SIZE));
    }

    public char getwB03CarAcqNonSconInd() {
        return this.wB03CarAcqNonSconInd;
    }

    public void setwB03OverCommInd(char wB03OverCommInd) {
        this.wB03OverCommInd = wB03OverCommInd;
    }

    public void setwB03OverCommIndFormatted(String wB03OverCommInd) {
        setwB03OverCommInd(Functions.charAt(wB03OverCommInd, Types.CHAR_SIZE));
    }

    public char getwB03OverCommInd() {
        return this.wB03OverCommInd;
    }

    public void setwB03CarAcqPrecontatoInd(char wB03CarAcqPrecontatoInd) {
        this.wB03CarAcqPrecontatoInd = wB03CarAcqPrecontatoInd;
    }

    public void setwB03CarAcqPrecontatoIndFormatted(String wB03CarAcqPrecontatoInd) {
        setwB03CarAcqPrecontatoInd(Functions.charAt(wB03CarAcqPrecontatoInd, Types.CHAR_SIZE));
    }

    public char getwB03CarAcqPrecontatoInd() {
        return this.wB03CarAcqPrecontatoInd;
    }

    public void setwB03RisAcqTInd(char wB03RisAcqTInd) {
        this.wB03RisAcqTInd = wB03RisAcqTInd;
    }

    public void setwB03RisAcqTIndFormatted(String wB03RisAcqTInd) {
        setwB03RisAcqTInd(Functions.charAt(wB03RisAcqTInd, Types.CHAR_SIZE));
    }

    public char getwB03RisAcqTInd() {
        return this.wB03RisAcqTInd;
    }

    public void setwB03RisZilTInd(char wB03RisZilTInd) {
        this.wB03RisZilTInd = wB03RisZilTInd;
    }

    public void setwB03RisZilTIndFormatted(String wB03RisZilTInd) {
        setwB03RisZilTInd(Functions.charAt(wB03RisZilTInd, Types.CHAR_SIZE));
    }

    public char getwB03RisZilTInd() {
        return this.wB03RisZilTInd;
    }

    public void setwB03CarGestNonSconInd(char wB03CarGestNonSconInd) {
        this.wB03CarGestNonSconInd = wB03CarGestNonSconInd;
    }

    public void setwB03CarGestNonSconIndFormatted(String wB03CarGestNonSconInd) {
        setwB03CarGestNonSconInd(Functions.charAt(wB03CarGestNonSconInd, Types.CHAR_SIZE));
    }

    public char getwB03CarGestNonSconInd() {
        return this.wB03CarGestNonSconInd;
    }

    public void setwB03CarGestInd(char wB03CarGestInd) {
        this.wB03CarGestInd = wB03CarGestInd;
    }

    public void setwB03CarGestIndFormatted(String wB03CarGestInd) {
        setwB03CarGestInd(Functions.charAt(wB03CarGestInd, Types.CHAR_SIZE));
    }

    public char getwB03CarGestInd() {
        return this.wB03CarGestInd;
    }

    public void setwB03RisSpeTInd(char wB03RisSpeTInd) {
        this.wB03RisSpeTInd = wB03RisSpeTInd;
    }

    public void setwB03RisSpeTIndFormatted(String wB03RisSpeTInd) {
        setwB03RisSpeTInd(Functions.charAt(wB03RisSpeTInd, Types.CHAR_SIZE));
    }

    public char getwB03RisSpeTInd() {
        return this.wB03RisSpeTInd;
    }

    public void setwB03CarIncNonSconInd(char wB03CarIncNonSconInd) {
        this.wB03CarIncNonSconInd = wB03CarIncNonSconInd;
    }

    public void setwB03CarIncNonSconIndFormatted(String wB03CarIncNonSconInd) {
        setwB03CarIncNonSconInd(Functions.charAt(wB03CarIncNonSconInd, Types.CHAR_SIZE));
    }

    public char getwB03CarIncNonSconInd() {
        return this.wB03CarIncNonSconInd;
    }

    public void setwB03CarIncInd(char wB03CarIncInd) {
        this.wB03CarIncInd = wB03CarIncInd;
    }

    public void setwB03CarIncIndFormatted(String wB03CarIncInd) {
        setwB03CarIncInd(Functions.charAt(wB03CarIncInd, Types.CHAR_SIZE));
    }

    public char getwB03CarIncInd() {
        return this.wB03CarIncInd;
    }

    public void setwB03RisRistorniCapInd(char wB03RisRistorniCapInd) {
        this.wB03RisRistorniCapInd = wB03RisRistorniCapInd;
    }

    public void setwB03RisRistorniCapIndFormatted(String wB03RisRistorniCapInd) {
        setwB03RisRistorniCapInd(Functions.charAt(wB03RisRistorniCapInd, Types.CHAR_SIZE));
    }

    public char getwB03RisRistorniCapInd() {
        return this.wB03RisRistorniCapInd;
    }

    public void setwB03IntrTecnInd(char wB03IntrTecnInd) {
        this.wB03IntrTecnInd = wB03IntrTecnInd;
    }

    public void setwB03IntrTecnIndFormatted(String wB03IntrTecnInd) {
        setwB03IntrTecnInd(Functions.charAt(wB03IntrTecnInd, Types.CHAR_SIZE));
    }

    public char getwB03IntrTecnInd() {
        return this.wB03IntrTecnInd;
    }

    public void setwB03CptRshMorInd(char wB03CptRshMorInd) {
        this.wB03CptRshMorInd = wB03CptRshMorInd;
    }

    public void setwB03CptRshMorIndFormatted(String wB03CptRshMorInd) {
        setwB03CptRshMorInd(Functions.charAt(wB03CptRshMorInd, Types.CHAR_SIZE));
    }

    public char getwB03CptRshMorInd() {
        return this.wB03CptRshMorInd;
    }

    public void setwB03CSubrshTInd(char wB03CSubrshTInd) {
        this.wB03CSubrshTInd = wB03CSubrshTInd;
    }

    public void setwB03CSubrshTIndFormatted(String wB03CSubrshTInd) {
        setwB03CSubrshTInd(Functions.charAt(wB03CSubrshTInd, Types.CHAR_SIZE));
    }

    public char getwB03CSubrshTInd() {
        return this.wB03CSubrshTInd;
    }

    public void setwB03PreRshTInd(char wB03PreRshTInd) {
        this.wB03PreRshTInd = wB03PreRshTInd;
    }

    public void setwB03PreRshTIndFormatted(String wB03PreRshTInd) {
        setwB03PreRshTInd(Functions.charAt(wB03PreRshTInd, Types.CHAR_SIZE));
    }

    public char getwB03PreRshTInd() {
        return this.wB03PreRshTInd;
    }

    public void setwB03AlqMargRisInd(char wB03AlqMargRisInd) {
        this.wB03AlqMargRisInd = wB03AlqMargRisInd;
    }

    public void setwB03AlqMargRisIndFormatted(String wB03AlqMargRisInd) {
        setwB03AlqMargRisInd(Functions.charAt(wB03AlqMargRisInd, Types.CHAR_SIZE));
    }

    public char getwB03AlqMargRisInd() {
        return this.wB03AlqMargRisInd;
    }

    public void setwB03AlqMargCSubrshInd(char wB03AlqMargCSubrshInd) {
        this.wB03AlqMargCSubrshInd = wB03AlqMargCSubrshInd;
    }

    public void setwB03AlqMargCSubrshIndFormatted(String wB03AlqMargCSubrshInd) {
        setwB03AlqMargCSubrshInd(Functions.charAt(wB03AlqMargCSubrshInd, Types.CHAR_SIZE));
    }

    public char getwB03AlqMargCSubrshInd() {
        return this.wB03AlqMargCSubrshInd;
    }

    public void setwB03TsRendtoSpprInd(char wB03TsRendtoSpprInd) {
        this.wB03TsRendtoSpprInd = wB03TsRendtoSpprInd;
    }

    public void setwB03TsRendtoSpprIndFormatted(String wB03TsRendtoSpprInd) {
        setwB03TsRendtoSpprInd(Functions.charAt(wB03TsRendtoSpprInd, Types.CHAR_SIZE));
    }

    public char getwB03TsRendtoSpprInd() {
        return this.wB03TsRendtoSpprInd;
    }

    public void setwB03TpIasInd(char wB03TpIasInd) {
        this.wB03TpIasInd = wB03TpIasInd;
    }

    public void setwB03TpIasIndFormatted(String wB03TpIasInd) {
        setwB03TpIasInd(Functions.charAt(wB03TpIasInd, Types.CHAR_SIZE));
    }

    public char getwB03TpIasInd() {
        return this.wB03TpIasInd;
    }

    public void setwB03TpIas(String wB03TpIas) {
        this.wB03TpIas = Functions.subString(wB03TpIas, Len.W_B03_TP_IAS);
    }

    public String getwB03TpIas() {
        return this.wB03TpIas;
    }

    public void setwB03NsQuoInd(char wB03NsQuoInd) {
        this.wB03NsQuoInd = wB03NsQuoInd;
    }

    public void setwB03NsQuoIndFormatted(String wB03NsQuoInd) {
        setwB03NsQuoInd(Functions.charAt(wB03NsQuoInd, Types.CHAR_SIZE));
    }

    public char getwB03NsQuoInd() {
        return this.wB03NsQuoInd;
    }

    public void setwB03TsMedioInd(char wB03TsMedioInd) {
        this.wB03TsMedioInd = wB03TsMedioInd;
    }

    public void setwB03TsMedioIndFormatted(String wB03TsMedioInd) {
        setwB03TsMedioInd(Functions.charAt(wB03TsMedioInd, Types.CHAR_SIZE));
    }

    public char getwB03TsMedioInd() {
        return this.wB03TsMedioInd;
    }

    public void setwB03CptRiastoInd(char wB03CptRiastoInd) {
        this.wB03CptRiastoInd = wB03CptRiastoInd;
    }

    public void setwB03CptRiastoIndFormatted(String wB03CptRiastoInd) {
        setwB03CptRiastoInd(Functions.charAt(wB03CptRiastoInd, Types.CHAR_SIZE));
    }

    public char getwB03CptRiastoInd() {
        return this.wB03CptRiastoInd;
    }

    public void setwB03PreRiastoInd(char wB03PreRiastoInd) {
        this.wB03PreRiastoInd = wB03PreRiastoInd;
    }

    public void setwB03PreRiastoIndFormatted(String wB03PreRiastoInd) {
        setwB03PreRiastoInd(Functions.charAt(wB03PreRiastoInd, Types.CHAR_SIZE));
    }

    public char getwB03PreRiastoInd() {
        return this.wB03PreRiastoInd;
    }

    public void setwB03RisRiastaInd(char wB03RisRiastaInd) {
        this.wB03RisRiastaInd = wB03RisRiastaInd;
    }

    public void setwB03RisRiastaIndFormatted(String wB03RisRiastaInd) {
        setwB03RisRiastaInd(Functions.charAt(wB03RisRiastaInd, Types.CHAR_SIZE));
    }

    public char getwB03RisRiastaInd() {
        return this.wB03RisRiastaInd;
    }

    public void setwB03CptRiastoEccInd(char wB03CptRiastoEccInd) {
        this.wB03CptRiastoEccInd = wB03CptRiastoEccInd;
    }

    public void setwB03CptRiastoEccIndFormatted(String wB03CptRiastoEccInd) {
        setwB03CptRiastoEccInd(Functions.charAt(wB03CptRiastoEccInd, Types.CHAR_SIZE));
    }

    public char getwB03CptRiastoEccInd() {
        return this.wB03CptRiastoEccInd;
    }

    public void setwB03PreRiastoEccInd(char wB03PreRiastoEccInd) {
        this.wB03PreRiastoEccInd = wB03PreRiastoEccInd;
    }

    public void setwB03PreRiastoEccIndFormatted(String wB03PreRiastoEccInd) {
        setwB03PreRiastoEccInd(Functions.charAt(wB03PreRiastoEccInd, Types.CHAR_SIZE));
    }

    public char getwB03PreRiastoEccInd() {
        return this.wB03PreRiastoEccInd;
    }

    public void setwB03RisRiastaEccInd(char wB03RisRiastaEccInd) {
        this.wB03RisRiastaEccInd = wB03RisRiastaEccInd;
    }

    public void setwB03RisRiastaEccIndFormatted(String wB03RisRiastaEccInd) {
        setwB03RisRiastaEccInd(Functions.charAt(wB03RisRiastaEccInd, Types.CHAR_SIZE));
    }

    public char getwB03RisRiastaEccInd() {
        return this.wB03RisRiastaEccInd;
    }

    public void setwB03CodAgeInd(char wB03CodAgeInd) {
        this.wB03CodAgeInd = wB03CodAgeInd;
    }

    public void setwB03CodAgeIndFormatted(String wB03CodAgeInd) {
        setwB03CodAgeInd(Functions.charAt(wB03CodAgeInd, Types.CHAR_SIZE));
    }

    public char getwB03CodAgeInd() {
        return this.wB03CodAgeInd;
    }

    public void setwB03CodSubageInd(char wB03CodSubageInd) {
        this.wB03CodSubageInd = wB03CodSubageInd;
    }

    public void setwB03CodSubageIndFormatted(String wB03CodSubageInd) {
        setwB03CodSubageInd(Functions.charAt(wB03CodSubageInd, Types.CHAR_SIZE));
    }

    public char getwB03CodSubageInd() {
        return this.wB03CodSubageInd;
    }

    public void setwB03CodCanInd(char wB03CodCanInd) {
        this.wB03CodCanInd = wB03CodCanInd;
    }

    public void setwB03CodCanIndFormatted(String wB03CodCanInd) {
        setwB03CodCanInd(Functions.charAt(wB03CodCanInd, Types.CHAR_SIZE));
    }

    public char getwB03CodCanInd() {
        return this.wB03CodCanInd;
    }

    public void setwB03IbPoliInd(char wB03IbPoliInd) {
        this.wB03IbPoliInd = wB03IbPoliInd;
    }

    public void setwB03IbPoliIndFormatted(String wB03IbPoliInd) {
        setwB03IbPoliInd(Functions.charAt(wB03IbPoliInd, Types.CHAR_SIZE));
    }

    public char getwB03IbPoliInd() {
        return this.wB03IbPoliInd;
    }

    public void setwB03IbPoli(String wB03IbPoli) {
        this.wB03IbPoli = Functions.subString(wB03IbPoli, Len.W_B03_IB_POLI);
    }

    public String getwB03IbPoli() {
        return this.wB03IbPoli;
    }

    public void setwB03IbAdesInd(char wB03IbAdesInd) {
        this.wB03IbAdesInd = wB03IbAdesInd;
    }

    public void setwB03IbAdesIndFormatted(String wB03IbAdesInd) {
        setwB03IbAdesInd(Functions.charAt(wB03IbAdesInd, Types.CHAR_SIZE));
    }

    public char getwB03IbAdesInd() {
        return this.wB03IbAdesInd;
    }

    public void setwB03IbAdes(String wB03IbAdes) {
        this.wB03IbAdes = Functions.subString(wB03IbAdes, Len.W_B03_IB_ADES);
    }

    public String getwB03IbAdes() {
        return this.wB03IbAdes;
    }

    public void setwB03IbTrchDiGarInd(char wB03IbTrchDiGarInd) {
        this.wB03IbTrchDiGarInd = wB03IbTrchDiGarInd;
    }

    public void setwB03IbTrchDiGarIndFormatted(String wB03IbTrchDiGarInd) {
        setwB03IbTrchDiGarInd(Functions.charAt(wB03IbTrchDiGarInd, Types.CHAR_SIZE));
    }

    public char getwB03IbTrchDiGarInd() {
        return this.wB03IbTrchDiGarInd;
    }

    public void setwB03IbTrchDiGar(String wB03IbTrchDiGar) {
        this.wB03IbTrchDiGar = Functions.subString(wB03IbTrchDiGar, Len.W_B03_IB_TRCH_DI_GAR);
    }

    public String getwB03IbTrchDiGar() {
        return this.wB03IbTrchDiGar;
    }

    public void setwB03TpPrstzInd(char wB03TpPrstzInd) {
        this.wB03TpPrstzInd = wB03TpPrstzInd;
    }

    public void setwB03TpPrstzIndFormatted(String wB03TpPrstzInd) {
        setwB03TpPrstzInd(Functions.charAt(wB03TpPrstzInd, Types.CHAR_SIZE));
    }

    public char getwB03TpPrstzInd() {
        return this.wB03TpPrstzInd;
    }

    public void setwB03TpPrstz(String wB03TpPrstz) {
        this.wB03TpPrstz = Functions.subString(wB03TpPrstz, Len.W_B03_TP_PRSTZ);
    }

    public String getwB03TpPrstz() {
        return this.wB03TpPrstz;
    }

    public void setwB03TpTrasfInd(char wB03TpTrasfInd) {
        this.wB03TpTrasfInd = wB03TpTrasfInd;
    }

    public void setwB03TpTrasfIndFormatted(String wB03TpTrasfInd) {
        setwB03TpTrasfInd(Functions.charAt(wB03TpTrasfInd, Types.CHAR_SIZE));
    }

    public char getwB03TpTrasfInd() {
        return this.wB03TpTrasfInd;
    }

    public void setwB03TpTrasf(String wB03TpTrasf) {
        this.wB03TpTrasf = Functions.subString(wB03TpTrasf, Len.W_B03_TP_TRASF);
    }

    public String getwB03TpTrasf() {
        return this.wB03TpTrasf;
    }

    public void setwB03PpInvrioTariInd(char wB03PpInvrioTariInd) {
        this.wB03PpInvrioTariInd = wB03PpInvrioTariInd;
    }

    public void setwB03PpInvrioTariIndFormatted(String wB03PpInvrioTariInd) {
        setwB03PpInvrioTariInd(Functions.charAt(wB03PpInvrioTariInd, Types.CHAR_SIZE));
    }

    public char getwB03PpInvrioTariInd() {
        return this.wB03PpInvrioTariInd;
    }

    public void setwB03PpInvrioTari(char wB03PpInvrioTari) {
        this.wB03PpInvrioTari = wB03PpInvrioTari;
    }

    public char getwB03PpInvrioTari() {
        return this.wB03PpInvrioTari;
    }

    public void setwB03CoeffOpzRenInd(char wB03CoeffOpzRenInd) {
        this.wB03CoeffOpzRenInd = wB03CoeffOpzRenInd;
    }

    public void setwB03CoeffOpzRenIndFormatted(String wB03CoeffOpzRenInd) {
        setwB03CoeffOpzRenInd(Functions.charAt(wB03CoeffOpzRenInd, Types.CHAR_SIZE));
    }

    public char getwB03CoeffOpzRenInd() {
        return this.wB03CoeffOpzRenInd;
    }

    public void setwB03CoeffOpzCptInd(char wB03CoeffOpzCptInd) {
        this.wB03CoeffOpzCptInd = wB03CoeffOpzCptInd;
    }

    public void setwB03CoeffOpzCptIndFormatted(String wB03CoeffOpzCptInd) {
        setwB03CoeffOpzCptInd(Functions.charAt(wB03CoeffOpzCptInd, Types.CHAR_SIZE));
    }

    public char getwB03CoeffOpzCptInd() {
        return this.wB03CoeffOpzCptInd;
    }

    public void setwB03DurPagRenInd(char wB03DurPagRenInd) {
        this.wB03DurPagRenInd = wB03DurPagRenInd;
    }

    public void setwB03DurPagRenIndFormatted(String wB03DurPagRenInd) {
        setwB03DurPagRenInd(Functions.charAt(wB03DurPagRenInd, Types.CHAR_SIZE));
    }

    public char getwB03DurPagRenInd() {
        return this.wB03DurPagRenInd;
    }

    public void setwB03VltInd(char wB03VltInd) {
        this.wB03VltInd = wB03VltInd;
    }

    public void setwB03VltIndFormatted(String wB03VltInd) {
        setwB03VltInd(Functions.charAt(wB03VltInd, Types.CHAR_SIZE));
    }

    public char getwB03VltInd() {
        return this.wB03VltInd;
    }

    public void setwB03Vlt(String wB03Vlt) {
        this.wB03Vlt = Functions.subString(wB03Vlt, Len.W_B03_VLT);
    }

    public String getwB03Vlt() {
        return this.wB03Vlt;
    }

    public void setwB03RisMatChiuPrecInd(char wB03RisMatChiuPrecInd) {
        this.wB03RisMatChiuPrecInd = wB03RisMatChiuPrecInd;
    }

    public void setwB03RisMatChiuPrecIndFormatted(String wB03RisMatChiuPrecInd) {
        setwB03RisMatChiuPrecInd(Functions.charAt(wB03RisMatChiuPrecInd, Types.CHAR_SIZE));
    }

    public char getwB03RisMatChiuPrecInd() {
        return this.wB03RisMatChiuPrecInd;
    }

    public void setwB03CodFndInd(char wB03CodFndInd) {
        this.wB03CodFndInd = wB03CodFndInd;
    }

    public void setwB03CodFndIndFormatted(String wB03CodFndInd) {
        setwB03CodFndInd(Functions.charAt(wB03CodFndInd, Types.CHAR_SIZE));
    }

    public char getwB03CodFndInd() {
        return this.wB03CodFndInd;
    }

    public void setwB03CodFnd(String wB03CodFnd) {
        this.wB03CodFnd = Functions.subString(wB03CodFnd, Len.W_B03_COD_FND);
    }

    public String getwB03CodFnd() {
        return this.wB03CodFnd;
    }

    public void setwB03PrstzTInd(char wB03PrstzTInd) {
        this.wB03PrstzTInd = wB03PrstzTInd;
    }

    public void setwB03PrstzTIndFormatted(String wB03PrstzTInd) {
        setwB03PrstzTInd(Functions.charAt(wB03PrstzTInd, Types.CHAR_SIZE));
    }

    public char getwB03PrstzTInd() {
        return this.wB03PrstzTInd;
    }

    public void setwB03TsTariDovInd(char wB03TsTariDovInd) {
        this.wB03TsTariDovInd = wB03TsTariDovInd;
    }

    public void setwB03TsTariDovIndFormatted(String wB03TsTariDovInd) {
        setwB03TsTariDovInd(Functions.charAt(wB03TsTariDovInd, Types.CHAR_SIZE));
    }

    public char getwB03TsTariDovInd() {
        return this.wB03TsTariDovInd;
    }

    public void setwB03TsTariSconInd(char wB03TsTariSconInd) {
        this.wB03TsTariSconInd = wB03TsTariSconInd;
    }

    public void setwB03TsTariSconIndFormatted(String wB03TsTariSconInd) {
        setwB03TsTariSconInd(Functions.charAt(wB03TsTariSconInd, Types.CHAR_SIZE));
    }

    public char getwB03TsTariSconInd() {
        return this.wB03TsTariSconInd;
    }

    public void setwB03TsPpInd(char wB03TsPpInd) {
        this.wB03TsPpInd = wB03TsPpInd;
    }

    public void setwB03TsPpIndFormatted(String wB03TsPpInd) {
        setwB03TsPpInd(Functions.charAt(wB03TsPpInd, Types.CHAR_SIZE));
    }

    public char getwB03TsPpInd() {
        return this.wB03TsPpInd;
    }

    public void setwB03CoeffRis1TInd(char wB03CoeffRis1TInd) {
        this.wB03CoeffRis1TInd = wB03CoeffRis1TInd;
    }

    public void setwB03CoeffRis1TIndFormatted(String wB03CoeffRis1TInd) {
        setwB03CoeffRis1TInd(Functions.charAt(wB03CoeffRis1TInd, Types.CHAR_SIZE));
    }

    public char getwB03CoeffRis1TInd() {
        return this.wB03CoeffRis1TInd;
    }

    public void setwB03CoeffRis2TInd(char wB03CoeffRis2TInd) {
        this.wB03CoeffRis2TInd = wB03CoeffRis2TInd;
    }

    public void setwB03CoeffRis2TIndFormatted(String wB03CoeffRis2TInd) {
        setwB03CoeffRis2TInd(Functions.charAt(wB03CoeffRis2TInd, Types.CHAR_SIZE));
    }

    public char getwB03CoeffRis2TInd() {
        return this.wB03CoeffRis2TInd;
    }

    public void setwB03AbbInd(char wB03AbbInd) {
        this.wB03AbbInd = wB03AbbInd;
    }

    public void setwB03AbbIndFormatted(String wB03AbbInd) {
        setwB03AbbInd(Functions.charAt(wB03AbbInd, Types.CHAR_SIZE));
    }

    public char getwB03AbbInd() {
        return this.wB03AbbInd;
    }

    public void setwB03TpCoassInd(char wB03TpCoassInd) {
        this.wB03TpCoassInd = wB03TpCoassInd;
    }

    public void setwB03TpCoassIndFormatted(String wB03TpCoassInd) {
        setwB03TpCoassInd(Functions.charAt(wB03TpCoassInd, Types.CHAR_SIZE));
    }

    public char getwB03TpCoassInd() {
        return this.wB03TpCoassInd;
    }

    public void setwB03TpCoass(String wB03TpCoass) {
        this.wB03TpCoass = Functions.subString(wB03TpCoass, Len.W_B03_TP_COASS);
    }

    public String getwB03TpCoass() {
        return this.wB03TpCoass;
    }

    public void setwB03TratRiassInd(char wB03TratRiassInd) {
        this.wB03TratRiassInd = wB03TratRiassInd;
    }

    public void setwB03TratRiassIndFormatted(String wB03TratRiassInd) {
        setwB03TratRiassInd(Functions.charAt(wB03TratRiassInd, Types.CHAR_SIZE));
    }

    public char getwB03TratRiassInd() {
        return this.wB03TratRiassInd;
    }

    public void setwB03TratRiass(String wB03TratRiass) {
        this.wB03TratRiass = Functions.subString(wB03TratRiass, Len.W_B03_TRAT_RIASS);
    }

    public String getwB03TratRiass() {
        return this.wB03TratRiass;
    }

    public void setwB03TratRiassEccInd(char wB03TratRiassEccInd) {
        this.wB03TratRiassEccInd = wB03TratRiassEccInd;
    }

    public void setwB03TratRiassEccIndFormatted(String wB03TratRiassEccInd) {
        setwB03TratRiassEccInd(Functions.charAt(wB03TratRiassEccInd, Types.CHAR_SIZE));
    }

    public char getwB03TratRiassEccInd() {
        return this.wB03TratRiassEccInd;
    }

    public void setwB03TratRiassEcc(String wB03TratRiassEcc) {
        this.wB03TratRiassEcc = Functions.subString(wB03TratRiassEcc, Len.W_B03_TRAT_RIASS_ECC);
    }

    public String getwB03TratRiassEcc() {
        return this.wB03TratRiassEcc;
    }

    public void setwB03DsOperSql(char wB03DsOperSql) {
        this.wB03DsOperSql = wB03DsOperSql;
    }

    public void setwB03DsOperSqlFormatted(String wB03DsOperSql) {
        setwB03DsOperSql(Functions.charAt(wB03DsOperSql, Types.CHAR_SIZE));
    }

    public char getwB03DsOperSql() {
        return this.wB03DsOperSql;
    }

    public void setwB03DsVer(int wB03DsVer) {
        this.wB03DsVer = wB03DsVer;
    }

    public int getwB03DsVer() {
        return this.wB03DsVer;
    }

    public void setwB03DsTsCptz(long wB03DsTsCptz) {
        this.wB03DsTsCptz = wB03DsTsCptz;
    }

    public long getwB03DsTsCptz() {
        return this.wB03DsTsCptz;
    }

    public void setwB03DsUtente(String wB03DsUtente) {
        this.wB03DsUtente = Functions.subString(wB03DsUtente, Len.W_B03_DS_UTENTE);
    }

    public String getwB03DsUtente() {
        return this.wB03DsUtente;
    }

    public void setwB03DsStatoElab(char wB03DsStatoElab) {
        this.wB03DsStatoElab = wB03DsStatoElab;
    }

    public void setwB03DsStatoElabFormatted(String wB03DsStatoElab) {
        setwB03DsStatoElab(Functions.charAt(wB03DsStatoElab, Types.CHAR_SIZE));
    }

    public char getwB03DsStatoElab() {
        return this.wB03DsStatoElab;
    }

    public void setwB03TpRgmFiscInd(char wB03TpRgmFiscInd) {
        this.wB03TpRgmFiscInd = wB03TpRgmFiscInd;
    }

    public void setwB03TpRgmFiscIndFormatted(String wB03TpRgmFiscInd) {
        setwB03TpRgmFiscInd(Functions.charAt(wB03TpRgmFiscInd, Types.CHAR_SIZE));
    }

    public char getwB03TpRgmFiscInd() {
        return this.wB03TpRgmFiscInd;
    }

    public void setwB03TpRgmFisc(String wB03TpRgmFisc) {
        this.wB03TpRgmFisc = Functions.subString(wB03TpRgmFisc, Len.W_B03_TP_RGM_FISC);
    }

    public String getwB03TpRgmFisc() {
        return this.wB03TpRgmFisc;
    }

    public void setwB03DurGarAaInd(char wB03DurGarAaInd) {
        this.wB03DurGarAaInd = wB03DurGarAaInd;
    }

    public void setwB03DurGarAaIndFormatted(String wB03DurGarAaInd) {
        setwB03DurGarAaInd(Functions.charAt(wB03DurGarAaInd, Types.CHAR_SIZE));
    }

    public char getwB03DurGarAaInd() {
        return this.wB03DurGarAaInd;
    }

    public void setwB03DurGarMmInd(char wB03DurGarMmInd) {
        this.wB03DurGarMmInd = wB03DurGarMmInd;
    }

    public void setwB03DurGarMmIndFormatted(String wB03DurGarMmInd) {
        setwB03DurGarMmInd(Functions.charAt(wB03DurGarMmInd, Types.CHAR_SIZE));
    }

    public char getwB03DurGarMmInd() {
        return this.wB03DurGarMmInd;
    }

    public void setwB03DurGarGgInd(char wB03DurGarGgInd) {
        this.wB03DurGarGgInd = wB03DurGarGgInd;
    }

    public void setwB03DurGarGgIndFormatted(String wB03DurGarGgInd) {
        setwB03DurGarGgInd(Functions.charAt(wB03DurGarGgInd, Types.CHAR_SIZE));
    }

    public char getwB03DurGarGgInd() {
        return this.wB03DurGarGgInd;
    }

    public void setwB03AntidurCalc365Ind(char wB03AntidurCalc365Ind) {
        this.wB03AntidurCalc365Ind = wB03AntidurCalc365Ind;
    }

    public void setwB03AntidurCalc365IndFormatted(String wB03AntidurCalc365Ind) {
        setwB03AntidurCalc365Ind(Functions.charAt(wB03AntidurCalc365Ind, Types.CHAR_SIZE));
    }

    public char getwB03AntidurCalc365Ind() {
        return this.wB03AntidurCalc365Ind;
    }

    public void setwB03CodFiscCntrInd(char wB03CodFiscCntrInd) {
        this.wB03CodFiscCntrInd = wB03CodFiscCntrInd;
    }

    public void setwB03CodFiscCntrIndFormatted(String wB03CodFiscCntrInd) {
        setwB03CodFiscCntrInd(Functions.charAt(wB03CodFiscCntrInd, Types.CHAR_SIZE));
    }

    public char getwB03CodFiscCntrInd() {
        return this.wB03CodFiscCntrInd;
    }

    public void setwB03CodFiscCntr(String wB03CodFiscCntr) {
        this.wB03CodFiscCntr = Functions.subString(wB03CodFiscCntr, Len.W_B03_COD_FISC_CNTR);
    }

    public String getwB03CodFiscCntr() {
        return this.wB03CodFiscCntr;
    }

    public void setwB03CodFiscAssto1Ind(char wB03CodFiscAssto1Ind) {
        this.wB03CodFiscAssto1Ind = wB03CodFiscAssto1Ind;
    }

    public void setwB03CodFiscAssto1IndFormatted(String wB03CodFiscAssto1Ind) {
        setwB03CodFiscAssto1Ind(Functions.charAt(wB03CodFiscAssto1Ind, Types.CHAR_SIZE));
    }

    public char getwB03CodFiscAssto1Ind() {
        return this.wB03CodFiscAssto1Ind;
    }

    public void setwB03CodFiscAssto1(String wB03CodFiscAssto1) {
        this.wB03CodFiscAssto1 = Functions.subString(wB03CodFiscAssto1, Len.W_B03_COD_FISC_ASSTO1);
    }

    public String getwB03CodFiscAssto1() {
        return this.wB03CodFiscAssto1;
    }

    public void setwB03CodFiscAssto2Ind(char wB03CodFiscAssto2Ind) {
        this.wB03CodFiscAssto2Ind = wB03CodFiscAssto2Ind;
    }

    public void setwB03CodFiscAssto2IndFormatted(String wB03CodFiscAssto2Ind) {
        setwB03CodFiscAssto2Ind(Functions.charAt(wB03CodFiscAssto2Ind, Types.CHAR_SIZE));
    }

    public char getwB03CodFiscAssto2Ind() {
        return this.wB03CodFiscAssto2Ind;
    }

    public void setwB03CodFiscAssto2(String wB03CodFiscAssto2) {
        this.wB03CodFiscAssto2 = Functions.subString(wB03CodFiscAssto2, Len.W_B03_COD_FISC_ASSTO2);
    }

    public String getwB03CodFiscAssto2() {
        return this.wB03CodFiscAssto2;
    }

    public void setwB03CodFiscAssto3Ind(char wB03CodFiscAssto3Ind) {
        this.wB03CodFiscAssto3Ind = wB03CodFiscAssto3Ind;
    }

    public void setwB03CodFiscAssto3IndFormatted(String wB03CodFiscAssto3Ind) {
        setwB03CodFiscAssto3Ind(Functions.charAt(wB03CodFiscAssto3Ind, Types.CHAR_SIZE));
    }

    public char getwB03CodFiscAssto3Ind() {
        return this.wB03CodFiscAssto3Ind;
    }

    public void setwB03CodFiscAssto3(String wB03CodFiscAssto3) {
        this.wB03CodFiscAssto3 = Functions.subString(wB03CodFiscAssto3, Len.W_B03_COD_FISC_ASSTO3);
    }

    public String getwB03CodFiscAssto3() {
        return this.wB03CodFiscAssto3;
    }

    public void setwB03CausSconInd(char wB03CausSconInd) {
        this.wB03CausSconInd = wB03CausSconInd;
    }

    public void setwB03CausSconIndFormatted(String wB03CausSconInd) {
        setwB03CausSconInd(Functions.charAt(wB03CausSconInd, Types.CHAR_SIZE));
    }

    public char getwB03CausSconInd() {
        return this.wB03CausSconInd;
    }

    public byte[] getwB03CausSconVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wB03CausSconLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, wB03CausScon, Len.W_B03_CAUS_SCON);
        return buffer;
    }

    public void setwB03CausSconLen(short wB03CausSconLen) {
        this.wB03CausSconLen = wB03CausSconLen;
    }

    public short getwB03CausSconLen() {
        return this.wB03CausSconLen;
    }

    public void setwB03CausScon(String wB03CausScon) {
        this.wB03CausScon = Functions.subString(wB03CausScon, Len.W_B03_CAUS_SCON);
    }

    public String getwB03CausScon() {
        return this.wB03CausScon;
    }

    public void setwB03EmitTitOpzInd(char wB03EmitTitOpzInd) {
        this.wB03EmitTitOpzInd = wB03EmitTitOpzInd;
    }

    public void setwB03EmitTitOpzIndFormatted(String wB03EmitTitOpzInd) {
        setwB03EmitTitOpzInd(Functions.charAt(wB03EmitTitOpzInd, Types.CHAR_SIZE));
    }

    public char getwB03EmitTitOpzInd() {
        return this.wB03EmitTitOpzInd;
    }

    public byte[] getwB03EmitTitOpzVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wB03EmitTitOpzLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, wB03EmitTitOpz, Len.W_B03_EMIT_TIT_OPZ);
        return buffer;
    }

    public void setwB03EmitTitOpzLen(short wB03EmitTitOpzLen) {
        this.wB03EmitTitOpzLen = wB03EmitTitOpzLen;
    }

    public short getwB03EmitTitOpzLen() {
        return this.wB03EmitTitOpzLen;
    }

    public void setwB03EmitTitOpz(String wB03EmitTitOpz) {
        this.wB03EmitTitOpz = Functions.subString(wB03EmitTitOpz, Len.W_B03_EMIT_TIT_OPZ);
    }

    public String getwB03EmitTitOpz() {
        return this.wB03EmitTitOpz;
    }

    public void setwB03QtzSpZCoupEmisInd(char wB03QtzSpZCoupEmisInd) {
        this.wB03QtzSpZCoupEmisInd = wB03QtzSpZCoupEmisInd;
    }

    public void setwB03QtzSpZCoupEmisIndFormatted(String wB03QtzSpZCoupEmisInd) {
        setwB03QtzSpZCoupEmisInd(Functions.charAt(wB03QtzSpZCoupEmisInd, Types.CHAR_SIZE));
    }

    public char getwB03QtzSpZCoupEmisInd() {
        return this.wB03QtzSpZCoupEmisInd;
    }

    public void setwB03QtzSpZOpzEmisInd(char wB03QtzSpZOpzEmisInd) {
        this.wB03QtzSpZOpzEmisInd = wB03QtzSpZOpzEmisInd;
    }

    public void setwB03QtzSpZOpzEmisIndFormatted(String wB03QtzSpZOpzEmisInd) {
        setwB03QtzSpZOpzEmisInd(Functions.charAt(wB03QtzSpZOpzEmisInd, Types.CHAR_SIZE));
    }

    public char getwB03QtzSpZOpzEmisInd() {
        return this.wB03QtzSpZOpzEmisInd;
    }

    public void setwB03QtzSpZCoupDtCInd(char wB03QtzSpZCoupDtCInd) {
        this.wB03QtzSpZCoupDtCInd = wB03QtzSpZCoupDtCInd;
    }

    public void setwB03QtzSpZCoupDtCIndFormatted(String wB03QtzSpZCoupDtCInd) {
        setwB03QtzSpZCoupDtCInd(Functions.charAt(wB03QtzSpZCoupDtCInd, Types.CHAR_SIZE));
    }

    public char getwB03QtzSpZCoupDtCInd() {
        return this.wB03QtzSpZCoupDtCInd;
    }

    public void setwB03QtzSpZOpzDtCaInd(char wB03QtzSpZOpzDtCaInd) {
        this.wB03QtzSpZOpzDtCaInd = wB03QtzSpZOpzDtCaInd;
    }

    public void setwB03QtzSpZOpzDtCaIndFormatted(String wB03QtzSpZOpzDtCaInd) {
        setwB03QtzSpZOpzDtCaInd(Functions.charAt(wB03QtzSpZOpzDtCaInd, Types.CHAR_SIZE));
    }

    public char getwB03QtzSpZOpzDtCaInd() {
        return this.wB03QtzSpZOpzDtCaInd;
    }

    public void setwB03QtzTotEmisInd(char wB03QtzTotEmisInd) {
        this.wB03QtzTotEmisInd = wB03QtzTotEmisInd;
    }

    public void setwB03QtzTotEmisIndFormatted(String wB03QtzTotEmisInd) {
        setwB03QtzTotEmisInd(Functions.charAt(wB03QtzTotEmisInd, Types.CHAR_SIZE));
    }

    public char getwB03QtzTotEmisInd() {
        return this.wB03QtzTotEmisInd;
    }

    public void setwB03QtzTotDtCalcInd(char wB03QtzTotDtCalcInd) {
        this.wB03QtzTotDtCalcInd = wB03QtzTotDtCalcInd;
    }

    public void setwB03QtzTotDtCalcIndFormatted(String wB03QtzTotDtCalcInd) {
        setwB03QtzTotDtCalcInd(Functions.charAt(wB03QtzTotDtCalcInd, Types.CHAR_SIZE));
    }

    public char getwB03QtzTotDtCalcInd() {
        return this.wB03QtzTotDtCalcInd;
    }

    public void setwB03QtzTotDtUltBilInd(char wB03QtzTotDtUltBilInd) {
        this.wB03QtzTotDtUltBilInd = wB03QtzTotDtUltBilInd;
    }

    public void setwB03QtzTotDtUltBilIndFormatted(String wB03QtzTotDtUltBilInd) {
        setwB03QtzTotDtUltBilInd(Functions.charAt(wB03QtzTotDtUltBilInd, Types.CHAR_SIZE));
    }

    public char getwB03QtzTotDtUltBilInd() {
        return this.wB03QtzTotDtUltBilInd;
    }

    public void setwB03DtQtzEmisInd(char wB03DtQtzEmisInd) {
        this.wB03DtQtzEmisInd = wB03DtQtzEmisInd;
    }

    public void setwB03DtQtzEmisIndFormatted(String wB03DtQtzEmisInd) {
        setwB03DtQtzEmisInd(Functions.charAt(wB03DtQtzEmisInd, Types.CHAR_SIZE));
    }

    public char getwB03DtQtzEmisInd() {
        return this.wB03DtQtzEmisInd;
    }

    public void setwB03DtQtzEmis(String wB03DtQtzEmis) {
        this.wB03DtQtzEmis = Functions.subString(wB03DtQtzEmis, Len.W_B03_DT_QTZ_EMIS);
    }

    public String getwB03DtQtzEmis() {
        return this.wB03DtQtzEmis;
    }

    public void setwB03PcCarGestInd(char wB03PcCarGestInd) {
        this.wB03PcCarGestInd = wB03PcCarGestInd;
    }

    public char getwB03PcCarGestInd() {
        return this.wB03PcCarGestInd;
    }

    public void setwB03PcCarGest(AfDecimal wB03PcCarGest) {
        this.wB03PcCarGest.assign(wB03PcCarGest);
    }

    public AfDecimal getwB03PcCarGest() {
        return this.wB03PcCarGest.copy();
    }

    public void setwB03PcCarAcqInd(char wB03PcCarAcqInd) {
        this.wB03PcCarAcqInd = wB03PcCarAcqInd;
    }

    public void setwB03PcCarAcqIndFormatted(String wB03PcCarAcqInd) {
        setwB03PcCarAcqInd(Functions.charAt(wB03PcCarAcqInd, Types.CHAR_SIZE));
    }

    public char getwB03PcCarAcqInd() {
        return this.wB03PcCarAcqInd;
    }

    public void setwB03ImpCarCasoMorInd(char wB03ImpCarCasoMorInd) {
        this.wB03ImpCarCasoMorInd = wB03ImpCarCasoMorInd;
    }

    public void setwB03ImpCarCasoMorIndFormatted(String wB03ImpCarCasoMorInd) {
        setwB03ImpCarCasoMorInd(Functions.charAt(wB03ImpCarCasoMorInd, Types.CHAR_SIZE));
    }

    public char getwB03ImpCarCasoMorInd() {
        return this.wB03ImpCarCasoMorInd;
    }

    public void setwB03PcCarMorInd(char wB03PcCarMorInd) {
        this.wB03PcCarMorInd = wB03PcCarMorInd;
    }

    public void setwB03PcCarMorIndFormatted(String wB03PcCarMorInd) {
        setwB03PcCarMorInd(Functions.charAt(wB03PcCarMorInd, Types.CHAR_SIZE));
    }

    public char getwB03PcCarMorInd() {
        return this.wB03PcCarMorInd;
    }

    public void setwB03TpVersInd(char wB03TpVersInd) {
        this.wB03TpVersInd = wB03TpVersInd;
    }

    public void setwB03TpVersIndFormatted(String wB03TpVersInd) {
        setwB03TpVersInd(Functions.charAt(wB03TpVersInd, Types.CHAR_SIZE));
    }

    public char getwB03TpVersInd() {
        return this.wB03TpVersInd;
    }

    public void setwB03TpVers(char wB03TpVers) {
        this.wB03TpVers = wB03TpVers;
    }

    public char getwB03TpVers() {
        return this.wB03TpVers;
    }

    public void setwB03FlSwitchInd(char wB03FlSwitchInd) {
        this.wB03FlSwitchInd = wB03FlSwitchInd;
    }

    public void setwB03FlSwitchIndFormatted(String wB03FlSwitchInd) {
        setwB03FlSwitchInd(Functions.charAt(wB03FlSwitchInd, Types.CHAR_SIZE));
    }

    public char getwB03FlSwitchInd() {
        return this.wB03FlSwitchInd;
    }

    public void setwB03FlSwitch(char wB03FlSwitch) {
        this.wB03FlSwitch = wB03FlSwitch;
    }

    public char getwB03FlSwitch() {
        return this.wB03FlSwitch;
    }

    public void setwB03FlIasInd(char wB03FlIasInd) {
        this.wB03FlIasInd = wB03FlIasInd;
    }

    public void setwB03FlIasIndFormatted(String wB03FlIasInd) {
        setwB03FlIasInd(Functions.charAt(wB03FlIasInd, Types.CHAR_SIZE));
    }

    public char getwB03FlIasInd() {
        return this.wB03FlIasInd;
    }

    public void setwB03FlIas(char wB03FlIas) {
        this.wB03FlIas = wB03FlIas;
    }

    public char getwB03FlIas() {
        return this.wB03FlIas;
    }

    public void setwB03DirInd(char wB03DirInd) {
        this.wB03DirInd = wB03DirInd;
    }

    public void setwB03DirIndFormatted(String wB03DirInd) {
        setwB03DirInd(Functions.charAt(wB03DirInd, Types.CHAR_SIZE));
    }

    public char getwB03DirInd() {
        return this.wB03DirInd;
    }

    public void setwB03TpCopCasoMorInd(char wB03TpCopCasoMorInd) {
        this.wB03TpCopCasoMorInd = wB03TpCopCasoMorInd;
    }

    public void setwB03TpCopCasoMorIndFormatted(String wB03TpCopCasoMorInd) {
        setwB03TpCopCasoMorInd(Functions.charAt(wB03TpCopCasoMorInd, Types.CHAR_SIZE));
    }

    public char getwB03TpCopCasoMorInd() {
        return this.wB03TpCopCasoMorInd;
    }

    public void setwB03TpCopCasoMor(String wB03TpCopCasoMor) {
        this.wB03TpCopCasoMor = Functions.subString(wB03TpCopCasoMor, Len.W_B03_TP_COP_CASO_MOR);
    }

    public String getwB03TpCopCasoMor() {
        return this.wB03TpCopCasoMor;
    }

    public void setwB03MetRiscSpclInd(char wB03MetRiscSpclInd) {
        this.wB03MetRiscSpclInd = wB03MetRiscSpclInd;
    }

    public void setwB03MetRiscSpclIndFormatted(String wB03MetRiscSpclInd) {
        setwB03MetRiscSpclInd(Functions.charAt(wB03MetRiscSpclInd, Types.CHAR_SIZE));
    }

    public char getwB03MetRiscSpclInd() {
        return this.wB03MetRiscSpclInd;
    }

    public void setwB03TpStatInvstInd(char wB03TpStatInvstInd) {
        this.wB03TpStatInvstInd = wB03TpStatInvstInd;
    }

    public void setwB03TpStatInvstIndFormatted(String wB03TpStatInvstInd) {
        setwB03TpStatInvstInd(Functions.charAt(wB03TpStatInvstInd, Types.CHAR_SIZE));
    }

    public char getwB03TpStatInvstInd() {
        return this.wB03TpStatInvstInd;
    }

    public void setwB03TpStatInvst(String wB03TpStatInvst) {
        this.wB03TpStatInvst = Functions.subString(wB03TpStatInvst, Len.W_B03_TP_STAT_INVST);
    }

    public String getwB03TpStatInvst() {
        return this.wB03TpStatInvst;
    }

    public void setwB03CodPrdtInd(char wB03CodPrdtInd) {
        this.wB03CodPrdtInd = wB03CodPrdtInd;
    }

    public void setwB03CodPrdtIndFormatted(String wB03CodPrdtInd) {
        setwB03CodPrdtInd(Functions.charAt(wB03CodPrdtInd, Types.CHAR_SIZE));
    }

    public char getwB03CodPrdtInd() {
        return this.wB03CodPrdtInd;
    }

    public void setwB03StatAssto1Ind(char wB03StatAssto1Ind) {
        this.wB03StatAssto1Ind = wB03StatAssto1Ind;
    }

    public void setwB03StatAssto1IndFormatted(String wB03StatAssto1Ind) {
        setwB03StatAssto1Ind(Functions.charAt(wB03StatAssto1Ind, Types.CHAR_SIZE));
    }

    public char getwB03StatAssto1Ind() {
        return this.wB03StatAssto1Ind;
    }

    public void setwB03StatAssto1(char wB03StatAssto1) {
        this.wB03StatAssto1 = wB03StatAssto1;
    }

    public char getwB03StatAssto1() {
        return this.wB03StatAssto1;
    }

    public void setwB03StatAssto2Ind(char wB03StatAssto2Ind) {
        this.wB03StatAssto2Ind = wB03StatAssto2Ind;
    }

    public void setwB03StatAssto2IndFormatted(String wB03StatAssto2Ind) {
        setwB03StatAssto2Ind(Functions.charAt(wB03StatAssto2Ind, Types.CHAR_SIZE));
    }

    public char getwB03StatAssto2Ind() {
        return this.wB03StatAssto2Ind;
    }

    public void setwB03StatAssto2(char wB03StatAssto2) {
        this.wB03StatAssto2 = wB03StatAssto2;
    }

    public char getwB03StatAssto2() {
        return this.wB03StatAssto2;
    }

    public void setwB03StatAssto3Ind(char wB03StatAssto3Ind) {
        this.wB03StatAssto3Ind = wB03StatAssto3Ind;
    }

    public void setwB03StatAssto3IndFormatted(String wB03StatAssto3Ind) {
        setwB03StatAssto3Ind(Functions.charAt(wB03StatAssto3Ind, Types.CHAR_SIZE));
    }

    public char getwB03StatAssto3Ind() {
        return this.wB03StatAssto3Ind;
    }

    public void setwB03StatAssto3(char wB03StatAssto3) {
        this.wB03StatAssto3 = wB03StatAssto3;
    }

    public char getwB03StatAssto3() {
        return this.wB03StatAssto3;
    }

    public void setwB03CptAsstoIniMorInd(char wB03CptAsstoIniMorInd) {
        this.wB03CptAsstoIniMorInd = wB03CptAsstoIniMorInd;
    }

    public void setwB03CptAsstoIniMorIndFormatted(String wB03CptAsstoIniMorInd) {
        setwB03CptAsstoIniMorInd(Functions.charAt(wB03CptAsstoIniMorInd, Types.CHAR_SIZE));
    }

    public char getwB03CptAsstoIniMorInd() {
        return this.wB03CptAsstoIniMorInd;
    }

    public void setwB03TsStabPreInd(char wB03TsStabPreInd) {
        this.wB03TsStabPreInd = wB03TsStabPreInd;
    }

    public void setwB03TsStabPreIndFormatted(String wB03TsStabPreInd) {
        setwB03TsStabPreInd(Functions.charAt(wB03TsStabPreInd, Types.CHAR_SIZE));
    }

    public char getwB03TsStabPreInd() {
        return this.wB03TsStabPreInd;
    }

    public void setwB03DirEmisInd(char wB03DirEmisInd) {
        this.wB03DirEmisInd = wB03DirEmisInd;
    }

    public void setwB03DirEmisIndFormatted(String wB03DirEmisInd) {
        setwB03DirEmisInd(Functions.charAt(wB03DirEmisInd, Types.CHAR_SIZE));
    }

    public char getwB03DirEmisInd() {
        return this.wB03DirEmisInd;
    }

    public void setwB03DtIncUltPreInd(char wB03DtIncUltPreInd) {
        this.wB03DtIncUltPreInd = wB03DtIncUltPreInd;
    }

    public void setwB03DtIncUltPreIndFormatted(String wB03DtIncUltPreInd) {
        setwB03DtIncUltPreInd(Functions.charAt(wB03DtIncUltPreInd, Types.CHAR_SIZE));
    }

    public char getwB03DtIncUltPreInd() {
        return this.wB03DtIncUltPreInd;
    }

    public void setwB03DtIncUltPre(String wB03DtIncUltPre) {
        this.wB03DtIncUltPre = Functions.subString(wB03DtIncUltPre, Len.W_B03_DT_INC_ULT_PRE);
    }

    public String getwB03DtIncUltPre() {
        return this.wB03DtIncUltPre;
    }

    public void setwB03StatTbgcAssto1Ind(char wB03StatTbgcAssto1Ind) {
        this.wB03StatTbgcAssto1Ind = wB03StatTbgcAssto1Ind;
    }

    public void setwB03StatTbgcAssto1IndFormatted(String wB03StatTbgcAssto1Ind) {
        setwB03StatTbgcAssto1Ind(Functions.charAt(wB03StatTbgcAssto1Ind, Types.CHAR_SIZE));
    }

    public char getwB03StatTbgcAssto1Ind() {
        return this.wB03StatTbgcAssto1Ind;
    }

    public void setwB03StatTbgcAssto1(char wB03StatTbgcAssto1) {
        this.wB03StatTbgcAssto1 = wB03StatTbgcAssto1;
    }

    public char getwB03StatTbgcAssto1() {
        return this.wB03StatTbgcAssto1;
    }

    public void setwB03StatTbgcAssto2Ind(char wB03StatTbgcAssto2Ind) {
        this.wB03StatTbgcAssto2Ind = wB03StatTbgcAssto2Ind;
    }

    public void setwB03StatTbgcAssto2IndFormatted(String wB03StatTbgcAssto2Ind) {
        setwB03StatTbgcAssto2Ind(Functions.charAt(wB03StatTbgcAssto2Ind, Types.CHAR_SIZE));
    }

    public char getwB03StatTbgcAssto2Ind() {
        return this.wB03StatTbgcAssto2Ind;
    }

    public void setwB03StatTbgcAssto2(char wB03StatTbgcAssto2) {
        this.wB03StatTbgcAssto2 = wB03StatTbgcAssto2;
    }

    public char getwB03StatTbgcAssto2() {
        return this.wB03StatTbgcAssto2;
    }

    public void setwB03StatTbgcAssto3Ind(char wB03StatTbgcAssto3Ind) {
        this.wB03StatTbgcAssto3Ind = wB03StatTbgcAssto3Ind;
    }

    public void setwB03StatTbgcAssto3IndFormatted(String wB03StatTbgcAssto3Ind) {
        setwB03StatTbgcAssto3Ind(Functions.charAt(wB03StatTbgcAssto3Ind, Types.CHAR_SIZE));
    }

    public char getwB03StatTbgcAssto3Ind() {
        return this.wB03StatTbgcAssto3Ind;
    }

    public void setwB03StatTbgcAssto3(char wB03StatTbgcAssto3) {
        this.wB03StatTbgcAssto3 = wB03StatTbgcAssto3;
    }

    public char getwB03StatTbgcAssto3() {
        return this.wB03StatTbgcAssto3;
    }

    public void setwB03FrazDecrCptInd(char wB03FrazDecrCptInd) {
        this.wB03FrazDecrCptInd = wB03FrazDecrCptInd;
    }

    public void setwB03FrazDecrCptIndFormatted(String wB03FrazDecrCptInd) {
        setwB03FrazDecrCptInd(Functions.charAt(wB03FrazDecrCptInd, Types.CHAR_SIZE));
    }

    public char getwB03FrazDecrCptInd() {
        return this.wB03FrazDecrCptInd;
    }

    public void setwB03PrePpUltInd(char wB03PrePpUltInd) {
        this.wB03PrePpUltInd = wB03PrePpUltInd;
    }

    public void setwB03PrePpUltIndFormatted(String wB03PrePpUltInd) {
        setwB03PrePpUltInd(Functions.charAt(wB03PrePpUltInd, Types.CHAR_SIZE));
    }

    public char getwB03PrePpUltInd() {
        return this.wB03PrePpUltInd;
    }

    public void setwB03AcqExpInd(char wB03AcqExpInd) {
        this.wB03AcqExpInd = wB03AcqExpInd;
    }

    public void setwB03AcqExpIndFormatted(String wB03AcqExpInd) {
        setwB03AcqExpInd(Functions.charAt(wB03AcqExpInd, Types.CHAR_SIZE));
    }

    public char getwB03AcqExpInd() {
        return this.wB03AcqExpInd;
    }

    public void setwB03RemunAssInd(char wB03RemunAssInd) {
        this.wB03RemunAssInd = wB03RemunAssInd;
    }

    public void setwB03RemunAssIndFormatted(String wB03RemunAssInd) {
        setwB03RemunAssInd(Functions.charAt(wB03RemunAssInd, Types.CHAR_SIZE));
    }

    public char getwB03RemunAssInd() {
        return this.wB03RemunAssInd;
    }

    public void setwB03CommisInterInd(char wB03CommisInterInd) {
        this.wB03CommisInterInd = wB03CommisInterInd;
    }

    public void setwB03CommisInterIndFormatted(String wB03CommisInterInd) {
        setwB03CommisInterInd(Functions.charAt(wB03CommisInterInd, Types.CHAR_SIZE));
    }

    public char getwB03CommisInterInd() {
        return this.wB03CommisInterInd;
    }

    public void setwB03NumFinanzInd(char wB03NumFinanzInd) {
        this.wB03NumFinanzInd = wB03NumFinanzInd;
    }

    public void setwB03NumFinanzIndFormatted(String wB03NumFinanzInd) {
        setwB03NumFinanzInd(Functions.charAt(wB03NumFinanzInd, Types.CHAR_SIZE));
    }

    public char getwB03NumFinanzInd() {
        return this.wB03NumFinanzInd;
    }

    public void setwB03NumFinanz(String wB03NumFinanz) {
        this.wB03NumFinanz = Functions.subString(wB03NumFinanz, Len.W_B03_NUM_FINANZ);
    }

    public String getwB03NumFinanz() {
        return this.wB03NumFinanz;
    }

    public void setwB03TpAccCommInd(char wB03TpAccCommInd) {
        this.wB03TpAccCommInd = wB03TpAccCommInd;
    }

    public void setwB03TpAccCommIndFormatted(String wB03TpAccCommInd) {
        setwB03TpAccCommInd(Functions.charAt(wB03TpAccCommInd, Types.CHAR_SIZE));
    }

    public char getwB03TpAccCommInd() {
        return this.wB03TpAccCommInd;
    }

    public void setwB03TpAccComm(String wB03TpAccComm) {
        this.wB03TpAccComm = Functions.subString(wB03TpAccComm, Len.W_B03_TP_ACC_COMM);
    }

    public String getwB03TpAccComm() {
        return this.wB03TpAccComm;
    }

    public void setwB03IbAccCommInd(char wB03IbAccCommInd) {
        this.wB03IbAccCommInd = wB03IbAccCommInd;
    }

    public void setwB03IbAccCommIndFormatted(String wB03IbAccCommInd) {
        setwB03IbAccCommInd(Functions.charAt(wB03IbAccCommInd, Types.CHAR_SIZE));
    }

    public char getwB03IbAccCommInd() {
        return this.wB03IbAccCommInd;
    }

    public void setwB03IbAccComm(String wB03IbAccComm) {
        this.wB03IbAccComm = Functions.subString(wB03IbAccComm, Len.W_B03_IB_ACC_COMM);
    }

    public String getwB03IbAccComm() {
        return this.wB03IbAccComm;
    }

    public void setwB03RamoBila(String wB03RamoBila) {
        this.wB03RamoBila = Functions.subString(wB03RamoBila, Len.W_B03_RAMO_BILA);
    }

    public String getwB03RamoBila() {
        return this.wB03RamoBila;
    }

    public void setwB03CarzInd(char wB03CarzInd) {
        this.wB03CarzInd = wB03CarzInd;
    }

    public void setwB03CarzIndFormatted(String wB03CarzInd) {
        setwB03CarzInd(Functions.charAt(wB03CarzInd, Types.CHAR_SIZE));
    }

    public char getwB03CarzInd() {
        return this.wB03CarzInd;
    }

    public WB03AaRenCerLlbs0230 getwB03AaRenCer() {
        return wB03AaRenCer;
    }

    public WB03AbbLlbs0230 getwB03Abb() {
        return wB03Abb;
    }

    public WB03AcqExpLlbs0230 getwB03AcqExp() {
        return wB03AcqExp;
    }

    public WB03AlqMargCSubrshLlbs0230 getwB03AlqMargCSubrsh() {
        return wB03AlqMargCSubrsh;
    }

    public WB03AlqMargRisLlbs0230 getwB03AlqMargRis() {
        return wB03AlqMargRis;
    }

    public WB03AlqRetrTLlbs0230 getwB03AlqRetrT() {
        return wB03AlqRetrT;
    }

    public WB03AntidurCalc365Llbs0230 getwB03AntidurCalc365() {
        return wB03AntidurCalc365;
    }

    public WB03AntidurDtCalcLlbs0230 getwB03AntidurDtCalc() {
        return wB03AntidurDtCalc;
    }

    public WB03AntidurRicorPrecLlbs0230 getwB03AntidurRicorPrec() {
        return wB03AntidurRicorPrec;
    }

    public WB03CSubrshTLlbs0230 getwB03CSubrshT() {
        return wB03CSubrshT;
    }

    public WB03CarAcqNonSconLlbs0230 getwB03CarAcqNonScon() {
        return wB03CarAcqNonScon;
    }

    public WB03CarAcqPrecontatoLlbs0230 getwB03CarAcqPrecontato() {
        return wB03CarAcqPrecontato;
    }

    public WB03CarGestLlbs0230 getwB03CarGest() {
        return wB03CarGest;
    }

    public WB03CarGestNonSconLlbs0230 getwB03CarGestNonScon() {
        return wB03CarGestNonScon;
    }

    public WB03CarIncLlbs0230 getwB03CarInc() {
        return wB03CarInc;
    }

    public WB03CarIncNonSconLlbs0230 getwB03CarIncNonScon() {
        return wB03CarIncNonScon;
    }

    public WB03CarzLlbs0230 getwB03Carz() {
        return wB03Carz;
    }

    public WB03CodAgeLlbs0230 getwB03CodAge() {
        return wB03CodAge;
    }

    public WB03CodCanLlbs0230 getwB03CodCan() {
        return wB03CodCan;
    }

    public WB03CodPrdtLlbs0230 getwB03CodPrdt() {
        return wB03CodPrdt;
    }

    public WB03CodSubageLlbs0230 getwB03CodSubage() {
        return wB03CodSubage;
    }

    public WB03CoeffOpzCptLlbs0230 getwB03CoeffOpzCpt() {
        return wB03CoeffOpzCpt;
    }

    public WB03CoeffOpzRenLlbs0230 getwB03CoeffOpzRen() {
        return wB03CoeffOpzRen;
    }

    public WB03CoeffRis1TLlbs0230 getwB03CoeffRis1T() {
        return wB03CoeffRis1T;
    }

    public WB03CoeffRis2TLlbs0230 getwB03CoeffRis2T() {
        return wB03CoeffRis2T;
    }

    public WB03CommisInterLlbs0230 getwB03CommisInter() {
        return wB03CommisInter;
    }

    public WB03CptAsstoIniMorLlbs0230 getwB03CptAsstoIniMor() {
        return wB03CptAsstoIniMor;
    }

    public WB03CptDtRidzLlbs0230 getwB03CptDtRidz() {
        return wB03CptDtRidz;
    }

    public WB03CptDtStabLlbs0230 getwB03CptDtStab() {
        return wB03CptDtStab;
    }

    public WB03CptRiastoLlbs0230 getwB03CptRiasto() {
        return wB03CptRiasto;
    }

    public WB03CptRiastoEccLlbs0230 getwB03CptRiastoEcc() {
        return wB03CptRiastoEcc;
    }

    public WB03CptRshMorLlbs0230 getwB03CptRshMor() {
        return wB03CptRshMor;
    }

    public WB03CumRiscparLlbs0230 getwB03CumRiscpar() {
        return wB03CumRiscpar;
    }

    public WB03DirLlbs0230 getwB03Dir() {
        return wB03Dir;
    }

    public WB03DirEmisLlbs0230 getwB03DirEmis() {
        return wB03DirEmis;
    }

    public WB03Dur1oPerAaLlbs0230 getwB03Dur1oPerAa() {
        return wB03Dur1oPerAa;
    }

    public WB03Dur1oPerGgLlbs0230 getwB03Dur1oPerGg() {
        return wB03Dur1oPerGg;
    }

    public WB03Dur1oPerMmLlbs0230 getwB03Dur1oPerMm() {
        return wB03Dur1oPerMm;
    }

    public WB03DurAaLlbs0230 getwB03DurAa() {
        return wB03DurAa;
    }

    public WB03DurGarAaLlbs0230 getwB03DurGarAa() {
        return wB03DurGarAa;
    }

    public WB03DurGarGgLlbs0230 getwB03DurGarGg() {
        return wB03DurGarGg;
    }

    public WB03DurGarMmLlbs0230 getwB03DurGarMm() {
        return wB03DurGarMm;
    }

    public WB03DurGgLlbs0230 getwB03DurGg() {
        return wB03DurGg;
    }

    public WB03DurMmLlbs0230 getwB03DurMm() {
        return wB03DurMm;
    }

    public WB03DurPagPreLlbs0230 getwB03DurPagPre() {
        return wB03DurPagPre;
    }

    public WB03DurPagRenLlbs0230 getwB03DurPagRen() {
        return wB03DurPagRen;
    }

    public WB03DurResDtCalcLlbs0230 getwB03DurResDtCalc() {
        return wB03DurResDtCalc;
    }

    public WB03EtaAa1oAsstoLlbs0230 getwB03EtaAa1oAssto() {
        return wB03EtaAa1oAssto;
    }

    public WB03EtaMm1oAsstoLlbs0230 getwB03EtaMm1oAssto() {
        return wB03EtaMm1oAssto;
    }

    public WB03EtaRaggnDtCalcLlbs0230 getwB03EtaRaggnDtCalc() {
        return wB03EtaRaggnDtCalc;
    }

    public WB03FrazLlbs0230 getwB03Fraz() {
        return wB03Fraz;
    }

    public WB03FrazDecrCptLlbs0230 getwB03FrazDecrCpt() {
        return wB03FrazDecrCpt;
    }

    public WB03FrazIniErogRenLlbs0230 getwB03FrazIniErogRen() {
        return wB03FrazIniErogRen;
    }

    public WB03IdRichEstrazAggLlbs0230 getwB03IdRichEstrazAgg() {
        return wB03IdRichEstrazAgg;
    }

    public WB03ImpCarCasoMorLlbs0230 getwB03ImpCarCasoMor() {
        return wB03ImpCarCasoMor;
    }

    public WB03IntrTecnLlbs0230 getwB03IntrTecn() {
        return wB03IntrTecn;
    }

    public WB03MetRiscSpclLlbs0230 getwB03MetRiscSpcl() {
        return wB03MetRiscSpcl;
    }

    public WB03MinGartoTLlbs0230 getwB03MinGartoT() {
        return wB03MinGartoT;
    }

    public WB03MinTrnutTLlbs0230 getwB03MinTrnutT() {
        return wB03MinTrnutT;
    }

    public WB03NsQuoLlbs0230 getwB03NsQuo() {
        return wB03NsQuo;
    }

    public WB03NumPrePattLlbs0230 getwB03NumPrePatt() {
        return wB03NumPrePatt;
    }

    public WB03OverCommLlbs0230 getwB03OverComm() {
        return wB03OverComm;
    }

    public WB03PcCarAcqLlbs0230 getwB03PcCarAcq() {
        return wB03PcCarAcq;
    }

    public WB03PcCarMorLlbs0230 getwB03PcCarMor() {
        return wB03PcCarMor;
    }

    public WB03PreAnnualizRicorLlbs0230 getwB03PreAnnualizRicor() {
        return wB03PreAnnualizRicor;
    }

    public WB03PreContLlbs0230 getwB03PreCont() {
        return wB03PreCont;
    }

    public WB03PreDovIniLlbs0230 getwB03PreDovIni() {
        return wB03PreDovIni;
    }

    public WB03PreDovRivtoTLlbs0230 getwB03PreDovRivtoT() {
        return wB03PreDovRivtoT;
    }

    public WB03PrePattuitoIniLlbs0230 getwB03PrePattuitoIni() {
        return wB03PrePattuitoIni;
    }

    public WB03PrePpIniLlbs0230 getwB03PrePpIni() {
        return wB03PrePpIni;
    }

    public WB03PrePpUltLlbs0230 getwB03PrePpUlt() {
        return wB03PrePpUlt;
    }

    public WB03PreRiastoLlbs0230 getwB03PreRiasto() {
        return wB03PreRiasto;
    }

    public WB03PreRiastoEccLlbs0230 getwB03PreRiastoEcc() {
        return wB03PreRiastoEcc;
    }

    public WB03PreRshTLlbs0230 getwB03PreRshT() {
        return wB03PreRshT;
    }

    public WB03ProvAcqLlbs0230 getwB03ProvAcq() {
        return wB03ProvAcq;
    }

    public WB03ProvAcqRicorLlbs0230 getwB03ProvAcqRicor() {
        return wB03ProvAcqRicor;
    }

    public WB03ProvIncLlbs0230 getwB03ProvInc() {
        return wB03ProvInc;
    }

    public WB03PrstzAggIniLlbs0230 getwB03PrstzAggIni() {
        return wB03PrstzAggIni;
    }

    public WB03PrstzAggUltLlbs0230 getwB03PrstzAggUlt() {
        return wB03PrstzAggUlt;
    }

    public WB03PrstzIniLlbs0230 getwB03PrstzIni() {
        return wB03PrstzIni;
    }

    public WB03PrstzTLlbs0230 getwB03PrstzT() {
        return wB03PrstzT;
    }

    public WB03QtzSpZCoupDtCLlbs0230 getwB03QtzSpZCoupDtC() {
        return wB03QtzSpZCoupDtC;
    }

    public WB03QtzSpZCoupEmisLlbs0230 getwB03QtzSpZCoupEmis() {
        return wB03QtzSpZCoupEmis;
    }

    public WB03QtzSpZOpzDtCaLlbs0230 getwB03QtzSpZOpzDtCa() {
        return wB03QtzSpZOpzDtCa;
    }

    public WB03QtzSpZOpzEmisLlbs0230 getwB03QtzSpZOpzEmis() {
        return wB03QtzSpZOpzEmis;
    }

    public WB03QtzTotDtCalcLlbs0230 getwB03QtzTotDtCalc() {
        return wB03QtzTotDtCalc;
    }

    public WB03QtzTotDtUltBilLlbs0230 getwB03QtzTotDtUltBil() {
        return wB03QtzTotDtUltBil;
    }

    public WB03QtzTotEmisLlbs0230 getwB03QtzTotEmis() {
        return wB03QtzTotEmis;
    }

    public WB03RappelLlbs0230 getwB03Rappel() {
        return wB03Rappel;
    }

    public WB03RatRenLlbs0230 getwB03RatRen() {
        return wB03RatRen;
    }

    public WB03RemunAssLlbs0230 getwB03RemunAss() {
        return wB03RemunAss;
    }

    public WB03RisAcqTLlbs0230 getwB03RisAcqT() {
        return wB03RisAcqT;
    }

    public WB03RisMatChiuPrecLlbs0230 getwB03RisMatChiuPrec() {
        return wB03RisMatChiuPrec;
    }

    public WB03RisPuraTLlbs0230 getwB03RisPuraT() {
        return wB03RisPuraT;
    }

    public WB03RisRiastaLlbs0230 getwB03RisRiasta() {
        return wB03RisRiasta;
    }

    public WB03RisRiastaEccLlbs0230 getwB03RisRiastaEcc() {
        return wB03RisRiastaEcc;
    }

    public WB03RisRistorniCapLlbs0230 getwB03RisRistorniCap() {
        return wB03RisRistorniCap;
    }

    public WB03RisSpeTLlbs0230 getwB03RisSpeT() {
        return wB03RisSpeT;
    }

    public WB03RisZilTLlbs0230 getwB03RisZilT() {
        return wB03RisZilT;
    }

    public WB03RiscparLlbs0230 getwB03Riscpar() {
        return wB03Riscpar;
    }

    public WB03TsMedioLlbs0230 getwB03TsMedio() {
        return wB03TsMedio;
    }

    public WB03TsNetTLlbs0230 getwB03TsNetT() {
        return wB03TsNetT;
    }

    public WB03TsPpLlbs0230 getwB03TsPp() {
        return wB03TsPp;
    }

    public WB03TsRendtoSpprLlbs0230 getwB03TsRendtoSppr() {
        return wB03TsRendtoSppr;
    }

    public WB03TsRendtoTLlbs0230 getwB03TsRendtoT() {
        return wB03TsRendtoT;
    }

    public WB03TsStabPreLlbs0230 getwB03TsStabPre() {
        return wB03TsStabPre;
    }

    public WB03TsTariDovLlbs0230 getwB03TsTariDov() {
        return wB03TsTariDov;
    }

    public WB03TsTariSconLlbs0230 getwB03TsTariScon() {
        return wB03TsTariScon;
    }

    public WB03UltRmLlbs0230 getwB03UltRm() {
        return wB03UltRm;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DT_RIS = 10;
        public static final int W_B03_DT_PRODUZIONE = 10;
        public static final int W_B03_TP_FRM_ASSVA = 2;
        public static final int W_B03_TP_RAMO_BILA = 2;
        public static final int W_B03_TP_CALC_RIS = 2;
        public static final int W_B03_COD_RAMO = 12;
        public static final int W_B03_COD_TARI = 12;
        public static final int W_B03_DT_INI_VAL_TAR = 10;
        public static final int W_B03_COD_PROD = 12;
        public static final int W_B03_DT_INI_VLDT_PROD = 10;
        public static final int W_B03_COD_TARI_ORGN = 12;
        public static final int W_B03_TP_TARI = 2;
        public static final int W_B03_TP_RIVAL = 2;
        public static final int W_B03_TP_TRCH = 2;
        public static final int W_B03_TP_TST = 2;
        public static final int W_B03_COD_CONV = 12;
        public static final int W_B03_DT_DECOR_POLI = 10;
        public static final int W_B03_DT_DECOR_ADES = 10;
        public static final int W_B03_DT_DECOR_TRCH = 10;
        public static final int W_B03_DT_EMIS_POLI = 10;
        public static final int W_B03_DT_EMIS_TRCH = 10;
        public static final int W_B03_DT_SCAD_TRCH = 10;
        public static final int W_B03_DT_SCAD_INTMD = 10;
        public static final int W_B03_DT_SCAD_PAG_PRE = 10;
        public static final int W_B03_DT_ULT_PRE_PAG = 10;
        public static final int W_B03_DT_NASC1O_ASSTO = 10;
        public static final int W_B03_TP_STAT_BUS_POLI = 2;
        public static final int W_B03_TP_CAUS_POLI = 2;
        public static final int W_B03_TP_STAT_BUS_ADES = 2;
        public static final int W_B03_TP_CAUS_ADES = 2;
        public static final int W_B03_TP_STAT_BUS_TRCH = 2;
        public static final int W_B03_TP_CAUS_TRCH = 2;
        public static final int W_B03_DT_EFF_CAMB_STAT = 10;
        public static final int W_B03_DT_EMIS_CAMB_STAT = 10;
        public static final int W_B03_DT_EFF_STAB = 10;
        public static final int W_B03_DT_EFF_RIDZ = 10;
        public static final int W_B03_DT_EMIS_RIDZ = 10;
        public static final int W_B03_COD_DIV = 20;
        public static final int W_B03_DT_ULT_RIVAL = 10;
        public static final int W_B03_TP_IAS = 2;
        public static final int W_B03_IB_POLI = 40;
        public static final int W_B03_IB_ADES = 40;
        public static final int W_B03_IB_TRCH_DI_GAR = 40;
        public static final int W_B03_TP_PRSTZ = 2;
        public static final int W_B03_TP_TRASF = 2;
        public static final int W_B03_VLT = 3;
        public static final int W_B03_COD_FND = 12;
        public static final int W_B03_TP_COASS = 2;
        public static final int W_B03_TRAT_RIASS = 12;
        public static final int W_B03_TRAT_RIASS_ECC = 12;
        public static final int W_B03_DS_UTENTE = 20;
        public static final int W_B03_TP_RGM_FISC = 2;
        public static final int W_B03_COD_FISC_CNTR = 16;
        public static final int W_B03_COD_FISC_ASSTO1 = 16;
        public static final int W_B03_COD_FISC_ASSTO2 = 16;
        public static final int W_B03_COD_FISC_ASSTO3 = 16;
        public static final int W_B03_CAUS_SCON = 100;
        public static final int W_B03_EMIT_TIT_OPZ = 100;
        public static final int W_B03_DT_QTZ_EMIS = 10;
        public static final int W_B03_TP_COP_CASO_MOR = 2;
        public static final int W_B03_TP_STAT_INVST = 2;
        public static final int W_B03_DT_INC_ULT_PRE = 10;
        public static final int W_B03_NUM_FINANZ = 40;
        public static final int W_B03_TP_ACC_COMM = 2;
        public static final int W_B03_IB_ACC_COMM = 40;
        public static final int W_B03_RAMO_BILA = 12;
        public static final int W_B03_ID_BILA_TRCH_ESTR = 5;
        public static final int W_B03_COD_COMP_ANIA = 3;
        public static final int W_B03_ID_RICH_ESTRAZ_MAS = 5;
        public static final int W_B03_ID_RICH_ESTRAZ_AGG_IND = 1;
        public static final int W_B03_FL_SIMULAZIONE_IND = 1;
        public static final int W_B03_FL_SIMULAZIONE = 1;
        public static final int W_B03_ID_POLI = 5;
        public static final int W_B03_ID_ADES = 5;
        public static final int W_B03_ID_GAR = 5;
        public static final int W_B03_ID_TRCH_DI_GAR = 5;
        public static final int W_B03_DT_INI_VAL_TAR_IND = 1;
        public static final int W_B03_COD_PROD_IND = 1;
        public static final int W_B03_COD_TARI_ORGN_IND = 1;
        public static final int W_B03_MIN_GARTO_T_IND = 1;
        public static final int W_B03_TP_TARI_IND = 1;
        public static final int W_B03_TP_PRE_IND = 1;
        public static final int W_B03_TP_PRE = 1;
        public static final int W_B03_TP_ADEG_PRE_IND = 1;
        public static final int W_B03_TP_ADEG_PRE = 1;
        public static final int W_B03_TP_RIVAL_IND = 1;
        public static final int W_B03_FL_DA_TRASF_IND = 1;
        public static final int W_B03_FL_DA_TRASF = 1;
        public static final int W_B03_FL_CAR_CONT_IND = 1;
        public static final int W_B03_FL_CAR_CONT = 1;
        public static final int W_B03_FL_PRE_DA_RIS_IND = 1;
        public static final int W_B03_FL_PRE_DA_RIS = 1;
        public static final int W_B03_FL_PRE_AGG_IND = 1;
        public static final int W_B03_FL_PRE_AGG = 1;
        public static final int W_B03_TP_TRCH_IND = 1;
        public static final int W_B03_TP_TST_IND = 1;
        public static final int W_B03_COD_CONV_IND = 1;
        public static final int W_B03_DT_DECOR_ADES_IND = 1;
        public static final int W_B03_DT_EMIS_TRCH_IND = 1;
        public static final int W_B03_DT_SCAD_TRCH_IND = 1;
        public static final int W_B03_DT_SCAD_INTMD_IND = 1;
        public static final int W_B03_DT_SCAD_PAG_PRE_IND = 1;
        public static final int W_B03_DT_ULT_PRE_PAG_IND = 1;
        public static final int W_B03_DT_NASC1O_ASSTO_IND = 1;
        public static final int W_B03_SEX1O_ASSTO_IND = 1;
        public static final int W_B03_SEX1O_ASSTO = 1;
        public static final int W_B03_ETA_AA1O_ASSTO_IND = 1;
        public static final int W_B03_ETA_MM1O_ASSTO_IND = 1;
        public static final int W_B03_ETA_RAGGN_DT_CALC_IND = 1;
        public static final int W_B03_DUR_AA_IND = 1;
        public static final int W_B03_DUR_MM_IND = 1;
        public static final int W_B03_DUR_GG_IND = 1;
        public static final int W_B03_DUR1O_PER_AA_IND = 1;
        public static final int W_B03_DUR1O_PER_MM_IND = 1;
        public static final int W_B03_DUR1O_PER_GG_IND = 1;
        public static final int W_B03_ANTIDUR_RICOR_PREC_IND = 1;
        public static final int W_B03_ANTIDUR_DT_CALC_IND = 1;
        public static final int W_B03_DUR_RES_DT_CALC_IND = 1;
        public static final int W_B03_DT_EFF_CAMB_STAT_IND = 1;
        public static final int W_B03_DT_EMIS_CAMB_STAT_IND = 1;
        public static final int W_B03_DT_EFF_STAB_IND = 1;
        public static final int W_B03_CPT_DT_STAB_IND = 1;
        public static final int W_B03_DT_EFF_RIDZ_IND = 1;
        public static final int W_B03_DT_EMIS_RIDZ_IND = 1;
        public static final int W_B03_CPT_DT_RIDZ_IND = 1;
        public static final int W_B03_FRAZ_IND = 1;
        public static final int W_B03_DUR_PAG_PRE_IND = 1;
        public static final int W_B03_NUM_PRE_PATT_IND = 1;
        public static final int W_B03_FRAZ_INI_EROG_REN_IND = 1;
        public static final int W_B03_AA_REN_CER_IND = 1;
        public static final int W_B03_RAT_REN_IND = 1;
        public static final int W_B03_COD_DIV_IND = 1;
        public static final int W_B03_RISCPAR_IND = 1;
        public static final int W_B03_CUM_RISCPAR_IND = 1;
        public static final int W_B03_ULT_RM_IND = 1;
        public static final int W_B03_TS_RENDTO_T_IND = 1;
        public static final int W_B03_ALQ_RETR_T_IND = 1;
        public static final int W_B03_MIN_TRNUT_T_IND = 1;
        public static final int W_B03_TS_NET_T_IND = 1;
        public static final int W_B03_DT_ULT_RIVAL_IND = 1;
        public static final int W_B03_PRSTZ_INI_IND = 1;
        public static final int W_B03_PRSTZ_AGG_INI_IND = 1;
        public static final int W_B03_PRSTZ_AGG_ULT_IND = 1;
        public static final int W_B03_RAPPEL_IND = 1;
        public static final int W_B03_PRE_PATTUITO_INI_IND = 1;
        public static final int W_B03_PRE_DOV_INI_IND = 1;
        public static final int W_B03_PRE_DOV_RIVTO_T_IND = 1;
        public static final int W_B03_PRE_ANNUALIZ_RICOR_IND = 1;
        public static final int W_B03_PRE_CONT_IND = 1;
        public static final int W_B03_PRE_PP_INI_IND = 1;
        public static final int W_B03_RIS_PURA_T_IND = 1;
        public static final int W_B03_PROV_ACQ_IND = 1;
        public static final int W_B03_PROV_ACQ_RICOR_IND = 1;
        public static final int W_B03_PROV_INC_IND = 1;
        public static final int W_B03_CAR_ACQ_NON_SCON_IND = 1;
        public static final int W_B03_OVER_COMM_IND = 1;
        public static final int W_B03_CAR_ACQ_PRECONTATO_IND = 1;
        public static final int W_B03_RIS_ACQ_T_IND = 1;
        public static final int W_B03_RIS_ZIL_T_IND = 1;
        public static final int W_B03_CAR_GEST_NON_SCON_IND = 1;
        public static final int W_B03_CAR_GEST_IND = 1;
        public static final int W_B03_RIS_SPE_T_IND = 1;
        public static final int W_B03_CAR_INC_NON_SCON_IND = 1;
        public static final int W_B03_CAR_INC_IND = 1;
        public static final int W_B03_RIS_RISTORNI_CAP_IND = 1;
        public static final int W_B03_INTR_TECN_IND = 1;
        public static final int W_B03_CPT_RSH_MOR_IND = 1;
        public static final int W_B03_C_SUBRSH_T_IND = 1;
        public static final int W_B03_PRE_RSH_T_IND = 1;
        public static final int W_B03_ALQ_MARG_RIS_IND = 1;
        public static final int W_B03_ALQ_MARG_C_SUBRSH_IND = 1;
        public static final int W_B03_TS_RENDTO_SPPR_IND = 1;
        public static final int W_B03_TP_IAS_IND = 1;
        public static final int W_B03_NS_QUO_IND = 1;
        public static final int W_B03_TS_MEDIO_IND = 1;
        public static final int W_B03_CPT_RIASTO_IND = 1;
        public static final int W_B03_PRE_RIASTO_IND = 1;
        public static final int W_B03_RIS_RIASTA_IND = 1;
        public static final int W_B03_CPT_RIASTO_ECC_IND = 1;
        public static final int W_B03_PRE_RIASTO_ECC_IND = 1;
        public static final int W_B03_RIS_RIASTA_ECC_IND = 1;
        public static final int W_B03_COD_AGE_IND = 1;
        public static final int W_B03_COD_SUBAGE_IND = 1;
        public static final int W_B03_COD_CAN_IND = 1;
        public static final int W_B03_IB_POLI_IND = 1;
        public static final int W_B03_IB_ADES_IND = 1;
        public static final int W_B03_IB_TRCH_DI_GAR_IND = 1;
        public static final int W_B03_TP_PRSTZ_IND = 1;
        public static final int W_B03_TP_TRASF_IND = 1;
        public static final int W_B03_PP_INVRIO_TARI_IND = 1;
        public static final int W_B03_PP_INVRIO_TARI = 1;
        public static final int W_B03_COEFF_OPZ_REN_IND = 1;
        public static final int W_B03_COEFF_OPZ_CPT_IND = 1;
        public static final int W_B03_DUR_PAG_REN_IND = 1;
        public static final int W_B03_VLT_IND = 1;
        public static final int W_B03_RIS_MAT_CHIU_PREC_IND = 1;
        public static final int W_B03_COD_FND_IND = 1;
        public static final int W_B03_PRSTZ_T_IND = 1;
        public static final int W_B03_TS_TARI_DOV_IND = 1;
        public static final int W_B03_TS_TARI_SCON_IND = 1;
        public static final int W_B03_TS_PP_IND = 1;
        public static final int W_B03_COEFF_RIS1_T_IND = 1;
        public static final int W_B03_COEFF_RIS2_T_IND = 1;
        public static final int W_B03_ABB_IND = 1;
        public static final int W_B03_TP_COASS_IND = 1;
        public static final int W_B03_TRAT_RIASS_IND = 1;
        public static final int W_B03_TRAT_RIASS_ECC_IND = 1;
        public static final int W_B03_DS_OPER_SQL = 1;
        public static final int W_B03_DS_VER = 5;
        public static final int W_B03_DS_TS_CPTZ = 10;
        public static final int W_B03_DS_STATO_ELAB = 1;
        public static final int W_B03_TP_RGM_FISC_IND = 1;
        public static final int W_B03_DUR_GAR_AA_IND = 1;
        public static final int W_B03_DUR_GAR_MM_IND = 1;
        public static final int W_B03_DUR_GAR_GG_IND = 1;
        public static final int W_B03_ANTIDUR_CALC365_IND = 1;
        public static final int W_B03_COD_FISC_CNTR_IND = 1;
        public static final int W_B03_COD_FISC_ASSTO1_IND = 1;
        public static final int W_B03_COD_FISC_ASSTO2_IND = 1;
        public static final int W_B03_COD_FISC_ASSTO3_IND = 1;
        public static final int W_B03_CAUS_SCON_IND = 1;
        public static final int W_B03_CAUS_SCON_LEN = 2;
        public static final int W_B03_CAUS_SCON_VCHAR = W_B03_CAUS_SCON_LEN + W_B03_CAUS_SCON;
        public static final int W_B03_EMIT_TIT_OPZ_IND = 1;
        public static final int W_B03_EMIT_TIT_OPZ_LEN = 2;
        public static final int W_B03_EMIT_TIT_OPZ_VCHAR = W_B03_EMIT_TIT_OPZ_LEN + W_B03_EMIT_TIT_OPZ;
        public static final int W_B03_QTZ_SP_Z_COUP_EMIS_IND = 1;
        public static final int W_B03_QTZ_SP_Z_OPZ_EMIS_IND = 1;
        public static final int W_B03_QTZ_SP_Z_COUP_DT_C_IND = 1;
        public static final int W_B03_QTZ_SP_Z_OPZ_DT_CA_IND = 1;
        public static final int W_B03_QTZ_TOT_EMIS_IND = 1;
        public static final int W_B03_QTZ_TOT_DT_CALC_IND = 1;
        public static final int W_B03_QTZ_TOT_DT_ULT_BIL_IND = 1;
        public static final int W_B03_DT_QTZ_EMIS_IND = 1;
        public static final int W_B03_PC_CAR_GEST_IND = 1;
        public static final int W_B03_PC_CAR_GEST = 4;
        public static final int W_B03_PC_CAR_ACQ_IND = 1;
        public static final int W_B03_IMP_CAR_CASO_MOR_IND = 1;
        public static final int W_B03_PC_CAR_MOR_IND = 1;
        public static final int W_B03_TP_VERS_IND = 1;
        public static final int W_B03_TP_VERS = 1;
        public static final int W_B03_FL_SWITCH_IND = 1;
        public static final int W_B03_FL_SWITCH = 1;
        public static final int W_B03_FL_IAS_IND = 1;
        public static final int W_B03_FL_IAS = 1;
        public static final int W_B03_DIR_IND = 1;
        public static final int W_B03_TP_COP_CASO_MOR_IND = 1;
        public static final int W_B03_MET_RISC_SPCL_IND = 1;
        public static final int W_B03_TP_STAT_INVST_IND = 1;
        public static final int W_B03_COD_PRDT_IND = 1;
        public static final int W_B03_STAT_ASSTO1_IND = 1;
        public static final int W_B03_STAT_ASSTO1 = 1;
        public static final int W_B03_STAT_ASSTO2_IND = 1;
        public static final int W_B03_STAT_ASSTO2 = 1;
        public static final int W_B03_STAT_ASSTO3_IND = 1;
        public static final int W_B03_STAT_ASSTO3 = 1;
        public static final int W_B03_CPT_ASSTO_INI_MOR_IND = 1;
        public static final int W_B03_TS_STAB_PRE_IND = 1;
        public static final int W_B03_DIR_EMIS_IND = 1;
        public static final int W_B03_DT_INC_ULT_PRE_IND = 1;
        public static final int W_B03_STAT_TBGC_ASSTO1_IND = 1;
        public static final int W_B03_STAT_TBGC_ASSTO1 = 1;
        public static final int W_B03_STAT_TBGC_ASSTO2_IND = 1;
        public static final int W_B03_STAT_TBGC_ASSTO2 = 1;
        public static final int W_B03_STAT_TBGC_ASSTO3_IND = 1;
        public static final int W_B03_STAT_TBGC_ASSTO3 = 1;
        public static final int W_B03_FRAZ_DECR_CPT_IND = 1;
        public static final int W_B03_PRE_PP_ULT_IND = 1;
        public static final int W_B03_ACQ_EXP_IND = 1;
        public static final int W_B03_REMUN_ASS_IND = 1;
        public static final int W_B03_COMMIS_INTER_IND = 1;
        public static final int W_B03_NUM_FINANZ_IND = 1;
        public static final int W_B03_TP_ACC_COMM_IND = 1;
        public static final int W_B03_IB_ACC_COMM_IND = 1;
        public static final int W_B03_CARZ_IND = 1;
        public static final int REC_FISSO = W_B03_ID_BILA_TRCH_ESTR + W_B03_COD_COMP_ANIA + W_B03_ID_RICH_ESTRAZ_MAS + W_B03_ID_RICH_ESTRAZ_AGG_IND + WB03IdRichEstrazAggLlbs0230.Len.W_B03_ID_RICH_ESTRAZ_AGG + W_B03_FL_SIMULAZIONE_IND + W_B03_FL_SIMULAZIONE + W_B03_DT_RIS + W_B03_DT_PRODUZIONE + W_B03_ID_POLI + W_B03_ID_ADES + W_B03_ID_GAR + W_B03_ID_TRCH_DI_GAR + W_B03_TP_FRM_ASSVA + W_B03_TP_RAMO_BILA + W_B03_TP_CALC_RIS + W_B03_COD_RAMO + W_B03_COD_TARI + W_B03_DT_INI_VAL_TAR_IND + W_B03_DT_INI_VAL_TAR + W_B03_COD_PROD_IND + W_B03_COD_PROD + W_B03_DT_INI_VLDT_PROD + W_B03_COD_TARI_ORGN_IND + W_B03_COD_TARI_ORGN + W_B03_MIN_GARTO_T_IND + WB03MinGartoTLlbs0230.Len.W_B03_MIN_GARTO_T + W_B03_TP_TARI_IND + W_B03_TP_TARI + W_B03_TP_PRE_IND + W_B03_TP_PRE + W_B03_TP_ADEG_PRE_IND + W_B03_TP_ADEG_PRE + W_B03_TP_RIVAL_IND + W_B03_TP_RIVAL + W_B03_FL_DA_TRASF_IND + W_B03_FL_DA_TRASF + W_B03_FL_CAR_CONT_IND + W_B03_FL_CAR_CONT + W_B03_FL_PRE_DA_RIS_IND + W_B03_FL_PRE_DA_RIS + W_B03_FL_PRE_AGG_IND + W_B03_FL_PRE_AGG + W_B03_TP_TRCH_IND + W_B03_TP_TRCH + W_B03_TP_TST_IND + W_B03_TP_TST + W_B03_COD_CONV_IND + W_B03_COD_CONV + W_B03_DT_DECOR_POLI + W_B03_DT_DECOR_ADES_IND + W_B03_DT_DECOR_ADES + W_B03_DT_DECOR_TRCH + W_B03_DT_EMIS_POLI + W_B03_DT_EMIS_TRCH_IND + W_B03_DT_EMIS_TRCH + W_B03_DT_SCAD_TRCH_IND + W_B03_DT_SCAD_TRCH + W_B03_DT_SCAD_INTMD_IND + W_B03_DT_SCAD_INTMD + W_B03_DT_SCAD_PAG_PRE_IND + W_B03_DT_SCAD_PAG_PRE + W_B03_DT_ULT_PRE_PAG_IND + W_B03_DT_ULT_PRE_PAG + W_B03_DT_NASC1O_ASSTO_IND + W_B03_DT_NASC1O_ASSTO + W_B03_SEX1O_ASSTO_IND + W_B03_SEX1O_ASSTO + W_B03_ETA_AA1O_ASSTO_IND + WB03EtaAa1oAsstoLlbs0230.Len.W_B03_ETA_AA1O_ASSTO + W_B03_ETA_MM1O_ASSTO_IND + WB03EtaMm1oAsstoLlbs0230.Len.W_B03_ETA_MM1O_ASSTO + W_B03_ETA_RAGGN_DT_CALC_IND + WB03EtaRaggnDtCalcLlbs0230.Len.W_B03_ETA_RAGGN_DT_CALC + W_B03_DUR_AA_IND + WB03DurAaLlbs0230.Len.W_B03_DUR_AA + W_B03_DUR_MM_IND + WB03DurMmLlbs0230.Len.W_B03_DUR_MM + W_B03_DUR_GG_IND + WB03DurGgLlbs0230.Len.W_B03_DUR_GG + W_B03_DUR1O_PER_AA_IND + WB03Dur1oPerAaLlbs0230.Len.W_B03_DUR1O_PER_AA + W_B03_DUR1O_PER_MM_IND + WB03Dur1oPerMmLlbs0230.Len.W_B03_DUR1O_PER_MM + W_B03_DUR1O_PER_GG_IND + WB03Dur1oPerGgLlbs0230.Len.W_B03_DUR1O_PER_GG + W_B03_ANTIDUR_RICOR_PREC_IND + WB03AntidurRicorPrecLlbs0230.Len.W_B03_ANTIDUR_RICOR_PREC + W_B03_ANTIDUR_DT_CALC_IND + WB03AntidurDtCalcLlbs0230.Len.W_B03_ANTIDUR_DT_CALC + W_B03_DUR_RES_DT_CALC_IND + WB03DurResDtCalcLlbs0230.Len.W_B03_DUR_RES_DT_CALC + W_B03_TP_STAT_BUS_POLI + W_B03_TP_CAUS_POLI + W_B03_TP_STAT_BUS_ADES + W_B03_TP_CAUS_ADES + W_B03_TP_STAT_BUS_TRCH + W_B03_TP_CAUS_TRCH + W_B03_DT_EFF_CAMB_STAT_IND + W_B03_DT_EFF_CAMB_STAT + W_B03_DT_EMIS_CAMB_STAT_IND + W_B03_DT_EMIS_CAMB_STAT + W_B03_DT_EFF_STAB_IND + W_B03_DT_EFF_STAB + W_B03_CPT_DT_STAB_IND + WB03CptDtStabLlbs0230.Len.W_B03_CPT_DT_STAB + W_B03_DT_EFF_RIDZ_IND + W_B03_DT_EFF_RIDZ + W_B03_DT_EMIS_RIDZ_IND + W_B03_DT_EMIS_RIDZ + W_B03_CPT_DT_RIDZ_IND + WB03CptDtRidzLlbs0230.Len.W_B03_CPT_DT_RIDZ + W_B03_FRAZ_IND + WB03FrazLlbs0230.Len.W_B03_FRAZ + W_B03_DUR_PAG_PRE_IND + WB03DurPagPreLlbs0230.Len.W_B03_DUR_PAG_PRE + W_B03_NUM_PRE_PATT_IND + WB03NumPrePattLlbs0230.Len.W_B03_NUM_PRE_PATT + W_B03_FRAZ_INI_EROG_REN_IND + WB03FrazIniErogRenLlbs0230.Len.W_B03_FRAZ_INI_EROG_REN + W_B03_AA_REN_CER_IND + WB03AaRenCerLlbs0230.Len.W_B03_AA_REN_CER + W_B03_RAT_REN_IND + WB03RatRenLlbs0230.Len.W_B03_RAT_REN + W_B03_COD_DIV_IND + W_B03_COD_DIV + W_B03_RISCPAR_IND + WB03RiscparLlbs0230.Len.W_B03_RISCPAR + W_B03_CUM_RISCPAR_IND + WB03CumRiscparLlbs0230.Len.W_B03_CUM_RISCPAR + W_B03_ULT_RM_IND + WB03UltRmLlbs0230.Len.W_B03_ULT_RM + W_B03_TS_RENDTO_T_IND + WB03TsRendtoTLlbs0230.Len.W_B03_TS_RENDTO_T + W_B03_ALQ_RETR_T_IND + WB03AlqRetrTLlbs0230.Len.W_B03_ALQ_RETR_T + W_B03_MIN_TRNUT_T_IND + WB03MinTrnutTLlbs0230.Len.W_B03_MIN_TRNUT_T + W_B03_TS_NET_T_IND + WB03TsNetTLlbs0230.Len.W_B03_TS_NET_T + W_B03_DT_ULT_RIVAL_IND + W_B03_DT_ULT_RIVAL + W_B03_PRSTZ_INI_IND + WB03PrstzIniLlbs0230.Len.W_B03_PRSTZ_INI + W_B03_PRSTZ_AGG_INI_IND + WB03PrstzAggIniLlbs0230.Len.W_B03_PRSTZ_AGG_INI + W_B03_PRSTZ_AGG_ULT_IND + WB03PrstzAggUltLlbs0230.Len.W_B03_PRSTZ_AGG_ULT + W_B03_RAPPEL_IND + WB03RappelLlbs0230.Len.W_B03_RAPPEL + W_B03_PRE_PATTUITO_INI_IND + WB03PrePattuitoIniLlbs0230.Len.W_B03_PRE_PATTUITO_INI + W_B03_PRE_DOV_INI_IND + WB03PreDovIniLlbs0230.Len.W_B03_PRE_DOV_INI + W_B03_PRE_DOV_RIVTO_T_IND + WB03PreDovRivtoTLlbs0230.Len.W_B03_PRE_DOV_RIVTO_T + W_B03_PRE_ANNUALIZ_RICOR_IND + WB03PreAnnualizRicorLlbs0230.Len.W_B03_PRE_ANNUALIZ_RICOR + W_B03_PRE_CONT_IND + WB03PreContLlbs0230.Len.W_B03_PRE_CONT + W_B03_PRE_PP_INI_IND + WB03PrePpIniLlbs0230.Len.W_B03_PRE_PP_INI + W_B03_RIS_PURA_T_IND + WB03RisPuraTLlbs0230.Len.W_B03_RIS_PURA_T + W_B03_PROV_ACQ_IND + WB03ProvAcqLlbs0230.Len.W_B03_PROV_ACQ + W_B03_PROV_ACQ_RICOR_IND + WB03ProvAcqRicorLlbs0230.Len.W_B03_PROV_ACQ_RICOR + W_B03_PROV_INC_IND + WB03ProvIncLlbs0230.Len.W_B03_PROV_INC + W_B03_CAR_ACQ_NON_SCON_IND + WB03CarAcqNonSconLlbs0230.Len.W_B03_CAR_ACQ_NON_SCON + W_B03_OVER_COMM_IND + WB03OverCommLlbs0230.Len.W_B03_OVER_COMM + W_B03_CAR_ACQ_PRECONTATO_IND + WB03CarAcqPrecontatoLlbs0230.Len.W_B03_CAR_ACQ_PRECONTATO + W_B03_RIS_ACQ_T_IND + WB03RisAcqTLlbs0230.Len.W_B03_RIS_ACQ_T + W_B03_RIS_ZIL_T_IND + WB03RisZilTLlbs0230.Len.W_B03_RIS_ZIL_T + W_B03_CAR_GEST_NON_SCON_IND + WB03CarGestNonSconLlbs0230.Len.W_B03_CAR_GEST_NON_SCON + W_B03_CAR_GEST_IND + WB03CarGestLlbs0230.Len.W_B03_CAR_GEST + W_B03_RIS_SPE_T_IND + WB03RisSpeTLlbs0230.Len.W_B03_RIS_SPE_T + W_B03_CAR_INC_NON_SCON_IND + WB03CarIncNonSconLlbs0230.Len.W_B03_CAR_INC_NON_SCON + W_B03_CAR_INC_IND + WB03CarIncLlbs0230.Len.W_B03_CAR_INC + W_B03_RIS_RISTORNI_CAP_IND + WB03RisRistorniCapLlbs0230.Len.W_B03_RIS_RISTORNI_CAP + W_B03_INTR_TECN_IND + WB03IntrTecnLlbs0230.Len.W_B03_INTR_TECN + W_B03_CPT_RSH_MOR_IND + WB03CptRshMorLlbs0230.Len.W_B03_CPT_RSH_MOR + W_B03_C_SUBRSH_T_IND + WB03CSubrshTLlbs0230.Len.W_B03_C_SUBRSH_T + W_B03_PRE_RSH_T_IND + WB03PreRshTLlbs0230.Len.W_B03_PRE_RSH_T + W_B03_ALQ_MARG_RIS_IND + WB03AlqMargRisLlbs0230.Len.W_B03_ALQ_MARG_RIS + W_B03_ALQ_MARG_C_SUBRSH_IND + WB03AlqMargCSubrshLlbs0230.Len.W_B03_ALQ_MARG_C_SUBRSH + W_B03_TS_RENDTO_SPPR_IND + WB03TsRendtoSpprLlbs0230.Len.W_B03_TS_RENDTO_SPPR + W_B03_TP_IAS_IND + W_B03_TP_IAS + W_B03_NS_QUO_IND + WB03NsQuoLlbs0230.Len.W_B03_NS_QUO + W_B03_TS_MEDIO_IND + WB03TsMedioLlbs0230.Len.W_B03_TS_MEDIO + W_B03_CPT_RIASTO_IND + WB03CptRiastoLlbs0230.Len.W_B03_CPT_RIASTO + W_B03_PRE_RIASTO_IND + WB03PreRiastoLlbs0230.Len.W_B03_PRE_RIASTO + W_B03_RIS_RIASTA_IND + WB03RisRiastaLlbs0230.Len.W_B03_RIS_RIASTA + W_B03_CPT_RIASTO_ECC_IND + WB03CptRiastoEccLlbs0230.Len.W_B03_CPT_RIASTO_ECC + W_B03_PRE_RIASTO_ECC_IND + WB03PreRiastoEccLlbs0230.Len.W_B03_PRE_RIASTO_ECC + W_B03_RIS_RIASTA_ECC_IND + WB03RisRiastaEccLlbs0230.Len.W_B03_RIS_RIASTA_ECC + W_B03_COD_AGE_IND + WB03CodAgeLlbs0230.Len.W_B03_COD_AGE + W_B03_COD_SUBAGE_IND + WB03CodSubageLlbs0230.Len.W_B03_COD_SUBAGE + W_B03_COD_CAN_IND + WB03CodCanLlbs0230.Len.W_B03_COD_CAN + W_B03_IB_POLI_IND + W_B03_IB_POLI + W_B03_IB_ADES_IND + W_B03_IB_ADES + W_B03_IB_TRCH_DI_GAR_IND + W_B03_IB_TRCH_DI_GAR + W_B03_TP_PRSTZ_IND + W_B03_TP_PRSTZ + W_B03_TP_TRASF_IND + W_B03_TP_TRASF + W_B03_PP_INVRIO_TARI_IND + W_B03_PP_INVRIO_TARI + W_B03_COEFF_OPZ_REN_IND + WB03CoeffOpzRenLlbs0230.Len.W_B03_COEFF_OPZ_REN + W_B03_COEFF_OPZ_CPT_IND + WB03CoeffOpzCptLlbs0230.Len.W_B03_COEFF_OPZ_CPT + W_B03_DUR_PAG_REN_IND + WB03DurPagRenLlbs0230.Len.W_B03_DUR_PAG_REN + W_B03_VLT_IND + W_B03_VLT + W_B03_RIS_MAT_CHIU_PREC_IND + WB03RisMatChiuPrecLlbs0230.Len.W_B03_RIS_MAT_CHIU_PREC + W_B03_COD_FND_IND + W_B03_COD_FND + W_B03_PRSTZ_T_IND + WB03PrstzTLlbs0230.Len.W_B03_PRSTZ_T + W_B03_TS_TARI_DOV_IND + WB03TsTariDovLlbs0230.Len.W_B03_TS_TARI_DOV + W_B03_TS_TARI_SCON_IND + WB03TsTariSconLlbs0230.Len.W_B03_TS_TARI_SCON + W_B03_TS_PP_IND + WB03TsPpLlbs0230.Len.W_B03_TS_PP + W_B03_COEFF_RIS1_T_IND + WB03CoeffRis1TLlbs0230.Len.W_B03_COEFF_RIS1_T + W_B03_COEFF_RIS2_T_IND + WB03CoeffRis2TLlbs0230.Len.W_B03_COEFF_RIS2_T + W_B03_ABB_IND + WB03AbbLlbs0230.Len.W_B03_ABB + W_B03_TP_COASS_IND + W_B03_TP_COASS + W_B03_TRAT_RIASS_IND + W_B03_TRAT_RIASS + W_B03_TRAT_RIASS_ECC_IND + W_B03_TRAT_RIASS_ECC + W_B03_DS_OPER_SQL + W_B03_DS_VER + W_B03_DS_TS_CPTZ + W_B03_DS_UTENTE + W_B03_DS_STATO_ELAB + W_B03_TP_RGM_FISC_IND + W_B03_TP_RGM_FISC + W_B03_DUR_GAR_AA_IND + WB03DurGarAaLlbs0230.Len.W_B03_DUR_GAR_AA + W_B03_DUR_GAR_MM_IND + WB03DurGarMmLlbs0230.Len.W_B03_DUR_GAR_MM + W_B03_DUR_GAR_GG_IND + WB03DurGarGgLlbs0230.Len.W_B03_DUR_GAR_GG + W_B03_ANTIDUR_CALC365_IND + WB03AntidurCalc365Llbs0230.Len.W_B03_ANTIDUR_CALC365 + W_B03_COD_FISC_CNTR_IND + W_B03_COD_FISC_CNTR + W_B03_COD_FISC_ASSTO1_IND + W_B03_COD_FISC_ASSTO1 + W_B03_COD_FISC_ASSTO2_IND + W_B03_COD_FISC_ASSTO2 + W_B03_COD_FISC_ASSTO3_IND + W_B03_COD_FISC_ASSTO3 + W_B03_CAUS_SCON_IND + W_B03_CAUS_SCON_VCHAR + W_B03_EMIT_TIT_OPZ_IND + W_B03_EMIT_TIT_OPZ_VCHAR + W_B03_QTZ_SP_Z_COUP_EMIS_IND + WB03QtzSpZCoupEmisLlbs0230.Len.W_B03_QTZ_SP_Z_COUP_EMIS + W_B03_QTZ_SP_Z_OPZ_EMIS_IND + WB03QtzSpZOpzEmisLlbs0230.Len.W_B03_QTZ_SP_Z_OPZ_EMIS + W_B03_QTZ_SP_Z_COUP_DT_C_IND + WB03QtzSpZCoupDtCLlbs0230.Len.W_B03_QTZ_SP_Z_COUP_DT_C + W_B03_QTZ_SP_Z_OPZ_DT_CA_IND + WB03QtzSpZOpzDtCaLlbs0230.Len.W_B03_QTZ_SP_Z_OPZ_DT_CA + W_B03_QTZ_TOT_EMIS_IND + WB03QtzTotEmisLlbs0230.Len.W_B03_QTZ_TOT_EMIS + W_B03_QTZ_TOT_DT_CALC_IND + WB03QtzTotDtCalcLlbs0230.Len.W_B03_QTZ_TOT_DT_CALC + W_B03_QTZ_TOT_DT_ULT_BIL_IND + WB03QtzTotDtUltBilLlbs0230.Len.W_B03_QTZ_TOT_DT_ULT_BIL + W_B03_DT_QTZ_EMIS_IND + W_B03_DT_QTZ_EMIS + W_B03_PC_CAR_GEST_IND + W_B03_PC_CAR_GEST + W_B03_PC_CAR_ACQ_IND + WB03PcCarAcqLlbs0230.Len.W_B03_PC_CAR_ACQ + W_B03_IMP_CAR_CASO_MOR_IND + WB03ImpCarCasoMorLlbs0230.Len.W_B03_IMP_CAR_CASO_MOR + W_B03_PC_CAR_MOR_IND + WB03PcCarMorLlbs0230.Len.W_B03_PC_CAR_MOR + W_B03_TP_VERS_IND + W_B03_TP_VERS + W_B03_FL_SWITCH_IND + W_B03_FL_SWITCH + W_B03_FL_IAS_IND + W_B03_FL_IAS + W_B03_DIR_IND + WB03DirLlbs0230.Len.W_B03_DIR + W_B03_TP_COP_CASO_MOR_IND + W_B03_TP_COP_CASO_MOR + W_B03_MET_RISC_SPCL_IND + WB03MetRiscSpclLlbs0230.Len.W_B03_MET_RISC_SPCL + W_B03_TP_STAT_INVST_IND + W_B03_TP_STAT_INVST + W_B03_COD_PRDT_IND + WB03CodPrdtLlbs0230.Len.W_B03_COD_PRDT + W_B03_STAT_ASSTO1_IND + W_B03_STAT_ASSTO1 + W_B03_STAT_ASSTO2_IND + W_B03_STAT_ASSTO2 + W_B03_STAT_ASSTO3_IND + W_B03_STAT_ASSTO3 + W_B03_CPT_ASSTO_INI_MOR_IND + WB03CptAsstoIniMorLlbs0230.Len.W_B03_CPT_ASSTO_INI_MOR + W_B03_TS_STAB_PRE_IND + WB03TsStabPreLlbs0230.Len.W_B03_TS_STAB_PRE + W_B03_DIR_EMIS_IND + WB03DirEmisLlbs0230.Len.W_B03_DIR_EMIS + W_B03_DT_INC_ULT_PRE_IND + W_B03_DT_INC_ULT_PRE + W_B03_STAT_TBGC_ASSTO1_IND + W_B03_STAT_TBGC_ASSTO1 + W_B03_STAT_TBGC_ASSTO2_IND + W_B03_STAT_TBGC_ASSTO2 + W_B03_STAT_TBGC_ASSTO3_IND + W_B03_STAT_TBGC_ASSTO3 + W_B03_FRAZ_DECR_CPT_IND + WB03FrazDecrCptLlbs0230.Len.W_B03_FRAZ_DECR_CPT + W_B03_PRE_PP_ULT_IND + WB03PrePpUltLlbs0230.Len.W_B03_PRE_PP_ULT + W_B03_ACQ_EXP_IND + WB03AcqExpLlbs0230.Len.W_B03_ACQ_EXP + W_B03_REMUN_ASS_IND + WB03RemunAssLlbs0230.Len.W_B03_REMUN_ASS + W_B03_COMMIS_INTER_IND + WB03CommisInterLlbs0230.Len.W_B03_COMMIS_INTER + W_B03_NUM_FINANZ_IND + W_B03_NUM_FINANZ + W_B03_TP_ACC_COMM_IND + W_B03_TP_ACC_COMM + W_B03_IB_ACC_COMM_IND + W_B03_IB_ACC_COMM + W_B03_RAMO_BILA + W_B03_CARZ_IND + WB03CarzLlbs0230.Len.W_B03_CARZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_ID_BILA_TRCH_ESTR = 9;
            public static final int W_B03_COD_COMP_ANIA = 5;
            public static final int W_B03_ID_RICH_ESTRAZ_MAS = 9;
            public static final int W_B03_ID_POLI = 9;
            public static final int W_B03_ID_ADES = 9;
            public static final int W_B03_ID_GAR = 9;
            public static final int W_B03_ID_TRCH_DI_GAR = 9;
            public static final int W_B03_DS_VER = 9;
            public static final int W_B03_DS_TS_CPTZ = 18;
            public static final int W_B03_PC_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PC_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
