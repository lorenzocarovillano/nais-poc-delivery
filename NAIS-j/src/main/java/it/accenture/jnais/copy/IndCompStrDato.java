package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-COMP-STR-DATO<br>
 * Variable: IND-COMP-STR-DATO from copybook IDBVCSD2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndCompStrDato {

    //==== PROPERTIES ====
    //Original name: IND-CSD-COD-STR-DATO2
    private short codStrDato2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-COD-DATO
    private short codDato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-FLAG-KEY
    private short flagKey = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-FLAG-RETURN-CODE
    private short flagReturnCode = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-FLAG-CALL-USING
    private short flagCallUsing = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-FLAG-WHERE-COND
    private short flagWhereCond = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-FLAG-REDEFINES
    private short flagRedefines = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-TIPO-DATO
    private short tipoDato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-LUNGHEZZA-DATO
    private short lunghezzaDato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-PRECISIONE-DATO
    private short precisioneDato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-COD-DOMINIO
    private short codDominio = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-FORMATTAZIONE-DATO
    private short formattazioneDato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-SERVIZIO-CONVERS
    private short servizioConvers = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-AREA-CONV-STANDARD
    private short areaConvStandard = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-RICORRENZA
    private short ricorrenza = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-OBBLIGATORIETA
    private short obbligatorieta = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CSD-VALORE-DEFAULT
    private short valoreDefault = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setCodStrDato2(short codStrDato2) {
        this.codStrDato2 = codStrDato2;
    }

    public short getCodStrDato2() {
        return this.codStrDato2;
    }

    public void setCodDato(short codDato) {
        this.codDato = codDato;
    }

    public short getCodDato() {
        return this.codDato;
    }

    public void setFlagKey(short flagKey) {
        this.flagKey = flagKey;
    }

    public short getFlagKey() {
        return this.flagKey;
    }

    public void setFlagReturnCode(short flagReturnCode) {
        this.flagReturnCode = flagReturnCode;
    }

    public short getFlagReturnCode() {
        return this.flagReturnCode;
    }

    public void setFlagCallUsing(short flagCallUsing) {
        this.flagCallUsing = flagCallUsing;
    }

    public short getFlagCallUsing() {
        return this.flagCallUsing;
    }

    public void setFlagWhereCond(short flagWhereCond) {
        this.flagWhereCond = flagWhereCond;
    }

    public short getFlagWhereCond() {
        return this.flagWhereCond;
    }

    public void setFlagRedefines(short flagRedefines) {
        this.flagRedefines = flagRedefines;
    }

    public short getFlagRedefines() {
        return this.flagRedefines;
    }

    public void setTipoDato(short tipoDato) {
        this.tipoDato = tipoDato;
    }

    public short getTipoDato() {
        return this.tipoDato;
    }

    public void setLunghezzaDato(short lunghezzaDato) {
        this.lunghezzaDato = lunghezzaDato;
    }

    public short getLunghezzaDato() {
        return this.lunghezzaDato;
    }

    public void setPrecisioneDato(short precisioneDato) {
        this.precisioneDato = precisioneDato;
    }

    public short getPrecisioneDato() {
        return this.precisioneDato;
    }

    public void setCodDominio(short codDominio) {
        this.codDominio = codDominio;
    }

    public short getCodDominio() {
        return this.codDominio;
    }

    public void setFormattazioneDato(short formattazioneDato) {
        this.formattazioneDato = formattazioneDato;
    }

    public short getFormattazioneDato() {
        return this.formattazioneDato;
    }

    public void setServizioConvers(short servizioConvers) {
        this.servizioConvers = servizioConvers;
    }

    public short getServizioConvers() {
        return this.servizioConvers;
    }

    public void setAreaConvStandard(short areaConvStandard) {
        this.areaConvStandard = areaConvStandard;
    }

    public short getAreaConvStandard() {
        return this.areaConvStandard;
    }

    public void setRicorrenza(short ricorrenza) {
        this.ricorrenza = ricorrenza;
    }

    public short getRicorrenza() {
        return this.ricorrenza;
    }

    public void setObbligatorieta(short obbligatorieta) {
        this.obbligatorieta = obbligatorieta;
    }

    public short getObbligatorieta() {
        return this.obbligatorieta;
    }

    public void setValoreDefault(short valoreDefault) {
        this.valoreDefault = valoreDefault;
    }

    public short getValoreDefault() {
        return this.valoreDefault;
    }
}
