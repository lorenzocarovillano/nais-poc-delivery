package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import it.accenture.jnais.ws.occurs.WpagDatiTrancheLlbs0230;

/**Original name: LSTC0072<br>
 * Variable: LSTC0072 from copybook LSTC0072<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lstc0072 {

    //==== PROPERTIES ====
    public static final int DATI_TRANCHE_MAXOCCURS = 1250;
    //Original name: WPAG-TRATT-COD-FND
    private char trattCodFnd = DefaultValues.CHAR_VAL;
    //Original name: WPAG-TRATT-NUM-QUO
    private char trattNumQuo = DefaultValues.CHAR_VAL;
    //Original name: WPAG-TRATT-VAL-QUO
    private char trattValQuo = DefaultValues.CHAR_VAL;
    //Original name: WPAG-TRATT-IMP-DISINV
    private char trattImpDisinv = DefaultValues.CHAR_VAL;
    //Original name: WPAG-ELE-DETT-FONDI-MAX
    private short eleDettFondiMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPAG-DATI-TRANCHE
    private LazyArrayCopy<WpagDatiTrancheLlbs0230> datiTranche = new LazyArrayCopy<WpagDatiTrancheLlbs0230>(new WpagDatiTrancheLlbs0230(), 1, DATI_TRANCHE_MAXOCCURS);

    //==== METHODS ====
    public void setTrattCodFnd(char trattCodFnd) {
        this.trattCodFnd = trattCodFnd;
    }

    public char getTrattCodFnd() {
        return this.trattCodFnd;
    }

    public void setTrattNumQuo(char trattNumQuo) {
        this.trattNumQuo = trattNumQuo;
    }

    public char getTrattNumQuo() {
        return this.trattNumQuo;
    }

    public void setTrattValQuo(char trattValQuo) {
        this.trattValQuo = trattValQuo;
    }

    public char getTrattValQuo() {
        return this.trattValQuo;
    }

    public void setTrattImpDisinv(char trattImpDisinv) {
        this.trattImpDisinv = trattImpDisinv;
    }

    public char getTrattImpDisinv() {
        return this.trattImpDisinv;
    }

    public void setEleDettFondiMax(short eleDettFondiMax) {
        this.eleDettFondiMax = eleDettFondiMax;
    }

    public short getEleDettFondiMax() {
        return this.eleDettFondiMax;
    }

    public LazyArrayCopy<WpagDatiTrancheLlbs0230> getDatiTrancheObj() {
        return datiTranche;
    }
}
