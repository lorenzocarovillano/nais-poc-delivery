package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVP863<br>
 * Copybook: IDBVP863 from copybook IDBVP863<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvp863 {

    //==== PROPERTIES ====
    //Original name: P86-DT-INI-EFF-DB
    private String p86DtIniEffDb = DefaultValues.stringVal(Len.P86_DT_INI_EFF_DB);
    //Original name: P86-DT-END-EFF-DB
    private String p86DtEndEffDb = DefaultValues.stringVal(Len.P86_DT_END_EFF_DB);

    //==== METHODS ====
    public void setP86DtIniEffDb(String p86DtIniEffDb) {
        this.p86DtIniEffDb = Functions.subString(p86DtIniEffDb, Len.P86_DT_INI_EFF_DB);
    }

    public String getP86DtIniEffDb() {
        return this.p86DtIniEffDb;
    }

    public void setP86DtEndEffDb(String p86DtEndEffDb) {
        this.p86DtEndEffDb = Functions.subString(p86DtEndEffDb, Len.P86_DT_END_EFF_DB);
    }

    public String getP86DtEndEffDb() {
        return this.p86DtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P86_DT_INI_EFF_DB = 10;
        public static final int P86_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
