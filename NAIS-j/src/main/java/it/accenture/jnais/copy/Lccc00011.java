package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.WcomModificaDtdecor;
import it.accenture.jnais.ws.enums.WcomNavigabilita;
import it.accenture.jnais.ws.enums.WcomTipoOperazione;
import it.accenture.jnais.ws.enums.WcomTpVisualizPag;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.occurs.WcomTabMovAnnull;
import it.accenture.jnais.ws.occurs.WcomTabPlatfond;

/**Original name: LCCC0001<br>
 * Variable: LCCC0001 from copybook LCCC0001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccc00011 {

    //==== PROPERTIES ====
    public static final int TAB_MOV_ANNULL_MAXOCCURS = 10;
    public static final int TAB_PLATFOND_MAXOCCURS = 3;
    /**Original name: WCOM-TIPO-OPERAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *      COPY COMUNE DI INTERFACCIA  BACK-END --> FRONT-END
	 *      LUNGHEZZA COMPLESSIVA : 5000 BYTES
	 * ----------------------------------------------------------------*</pre>*/
    private WcomTipoOperazione tipoOperazione = new WcomTipoOperazione();
    //Original name: WCOM-NAVIGABILITA
    private WcomNavigabilita navigabilita = new WcomNavigabilita();
    //Original name: WCOM-TASTI-DA-ABILITARE
    private WcomTastiDaAbilitare tastiDaAbilitare = new WcomTastiDaAbilitare();
    //Original name: WCOM-DT-ULT-VERS-PROD
    private String dtUltVersProd = DefaultValues.stringVal(Len.DT_ULT_VERS_PROD);
    //Original name: WCOM-STATI
    private WcomStati stati = new WcomStati();
    //Original name: WCOM-DATI-DEROGHE
    private WcomDatiDeroghe datiDeroghe = new WcomDatiDeroghe();
    //Original name: WCOM-ELE-MOV-ANNULL-MAX
    private short eleMovAnnullMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WCOM-TAB-MOV-ANNULL
    private WcomTabMovAnnull[] tabMovAnnull = new WcomTabMovAnnull[TAB_MOV_ANNULL_MAXOCCURS];
    //Original name: WCOM-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: WCOM-FLAG-TARIFFA-RISCHIO
    private char flagTariffaRischio = DefaultValues.CHAR_VAL;
    //Original name: WCOM-ELE-MAX-PLATFOND
    private short eleMaxPlatfond = DefaultValues.BIN_SHORT_VAL;
    //Original name: WCOM-TAB-PLATFOND
    private WcomTabPlatfond[] tabPlatfond = new WcomTabPlatfond[TAB_PLATFOND_MAXOCCURS];
    //Original name: WCOM-MODIFICA-DTDECOR
    private WcomModificaDtdecor modificaDtdecor = new WcomModificaDtdecor();
    //Original name: WCOM-STATUS-DER
    private WpolStatus statusDer = new WpolStatus();
    //Original name: WCOM-ID-RICH-EST
    private int idRichEst = DefaultValues.INT_VAL;
    //Original name: WCOM-CODICE-INIZIATIVA
    private String codiceIniziativa = DefaultValues.stringVal(Len.CODICE_INIZIATIVA);
    //Original name: WCOM-TP-VISUALIZ-PAG
    private WcomTpVisualizPag tpVisualizPag = new WcomTpVisualizPag();
    //Original name: FILLER-WCOM-AREA-STATI
    private String flr1 = DefaultValues.stringVal(Len.FLR1);

    //==== CONSTRUCTORS ====
    public Lccc00011() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabMovAnnullIdx = 1; tabMovAnnullIdx <= TAB_MOV_ANNULL_MAXOCCURS; tabMovAnnullIdx++) {
            tabMovAnnull[tabMovAnnullIdx - 1] = new WcomTabMovAnnull();
        }
        for (int tabPlatfondIdx = 1; tabPlatfondIdx <= TAB_PLATFOND_MAXOCCURS; tabPlatfondIdx++) {
            tabPlatfond[tabPlatfondIdx - 1] = new WcomTabPlatfond();
        }
    }

    public void setDatiActuatorBytes(byte[] buffer, int offset) {
        int position = offset;
        dtUltVersProd = MarshalByte.readFixedString(buffer, position, Len.DT_ULT_VERS_PROD);
    }

    public byte[] getDatiActuatorBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, dtUltVersProd, Len.DT_ULT_VERS_PROD);
        return buffer;
    }

    public void setWcomDtUltVersProd(int wcomDtUltVersProd) {
        this.dtUltVersProd = NumericDisplay.asString(wcomDtUltVersProd, Len.DT_ULT_VERS_PROD);
    }

    public void setWcomDtUltVersProdFormatted(String wcomDtUltVersProd) {
        this.dtUltVersProd = Trunc.toUnsignedNumeric(wcomDtUltVersProd, Len.DT_ULT_VERS_PROD);
    }

    public int getWcomDtUltVersProd() {
        return NumericDisplay.asInt(this.dtUltVersProd);
    }

    public String getWcomDtUltVersProdFormatted() {
        return this.dtUltVersProd;
    }

    public void setMovimentiAnnullBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMovAnnullMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_MOV_ANNULL_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabMovAnnull[idx - 1].setTabMovAnnullBytes(buffer, position);
                position += WcomTabMovAnnull.Len.TAB_MOV_ANNULL;
            }
            else {
                tabMovAnnull[idx - 1].initTabMovAnnullSpaces();
                position += WcomTabMovAnnull.Len.TAB_MOV_ANNULL;
            }
        }
    }

    public byte[] getMovimentiAnnullBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleMovAnnullMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_MOV_ANNULL_MAXOCCURS; idx++) {
            tabMovAnnull[idx - 1].getTabMovAnnullBytes(buffer, position);
            position += WcomTabMovAnnull.Len.TAB_MOV_ANNULL;
        }
        return buffer;
    }

    public void setEleMovAnnullMax(short eleMovAnnullMax) {
        this.eleMovAnnullMax = eleMovAnnullMax;
    }

    public short getEleMovAnnullMax() {
        return this.eleMovAnnullMax;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setFlagTariffaRischio(char flagTariffaRischio) {
        this.flagTariffaRischio = flagTariffaRischio;
    }

    public char getFlagTariffaRischio() {
        return this.flagTariffaRischio;
    }

    public void setAreaPlatfondBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxPlatfond = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_PLATFOND_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabPlatfond[idx - 1].setTabPlatfondBytes(buffer, position);
                position += WcomTabPlatfond.Len.TAB_PLATFOND;
            }
            else {
                tabPlatfond[idx - 1].initTabPlatfondSpaces();
                position += WcomTabPlatfond.Len.TAB_PLATFOND;
            }
        }
    }

    public byte[] getAreaPlatfondBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleMaxPlatfond);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_PLATFOND_MAXOCCURS; idx++) {
            tabPlatfond[idx - 1].getTabPlatfondBytes(buffer, position);
            position += WcomTabPlatfond.Len.TAB_PLATFOND;
        }
        return buffer;
    }

    public void setEleMaxPlatfond(short eleMaxPlatfond) {
        this.eleMaxPlatfond = eleMaxPlatfond;
    }

    public short getEleMaxPlatfond() {
        return this.eleMaxPlatfond;
    }

    public void setIdRichEst(int idRichEst) {
        this.idRichEst = idRichEst;
    }

    public int getIdRichEst() {
        return this.idRichEst;
    }

    public void setCodiceIniziativa(String codiceIniziativa) {
        this.codiceIniziativa = Functions.subString(codiceIniziativa, Len.CODICE_INIZIATIVA);
    }

    public String getCodiceIniziativa() {
        return this.codiceIniziativa;
    }

    public void setFlr1(String flr1) {
        this.flr1 = Functions.subString(flr1, Len.FLR1);
    }

    public String getFlr1() {
        return this.flr1;
    }

    public WcomDatiDeroghe getDatiDeroghe() {
        return datiDeroghe;
    }

    public WcomModificaDtdecor getModificaDtdecor() {
        return modificaDtdecor;
    }

    public WcomNavigabilita getNavigabilita() {
        return navigabilita;
    }

    public WcomStati getStati() {
        return stati;
    }

    public WpolStatus getStatusDer() {
        return statusDer;
    }

    public WcomTabMovAnnull getTabMovAnnull(int idx) {
        return tabMovAnnull[idx - 1];
    }

    public WcomTabPlatfond getTabPlatfond(int idx) {
        return tabPlatfond[idx - 1];
    }

    public WcomTastiDaAbilitare getTastiDaAbilitare() {
        return tastiDaAbilitare;
    }

    public WcomTipoOperazione getTipoOperazione() {
        return tipoOperazione;
    }

    public WcomTpVisualizPag getTpVisualizPag() {
        return tpVisualizPag;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_ULT_VERS_PROD = 8;
        public static final int DATI_ACTUATOR = DT_ULT_VERS_PROD;
        public static final int ELE_MOV_ANNULL_MAX = 2;
        public static final int MOVIMENTI_ANNULL = ELE_MOV_ANNULL_MAX + Lccc00011.TAB_MOV_ANNULL_MAXOCCURS * WcomTabMovAnnull.Len.TAB_MOV_ANNULL;
        public static final int ID_MOVI_CRZ = 5;
        public static final int FLAG_TARIFFA_RISCHIO = 1;
        public static final int ELE_MAX_PLATFOND = 2;
        public static final int AREA_PLATFOND = ELE_MAX_PLATFOND + Lccc00011.TAB_PLATFOND_MAXOCCURS * WcomTabPlatfond.Len.TAB_PLATFOND;
        public static final int ID_RICH_EST = 5;
        public static final int CODICE_INIZIATIVA = 12;
        public static final int FLR1 = 135;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_RICH_EST = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
