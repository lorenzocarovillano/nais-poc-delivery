package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.WopzOpzioni;

/**Original name: IVVC0217<br>
 * Copybook: IVVC0217 from copybook IVVC0217<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Ivvc0217 {

    //==== PROPERTIES ====
    public static final int OPZIONI_MAXOCCURS = 10;
    /**Original name: WOPZ-ELE-MAX-OPZIONI<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA INPUT VARIABILI
	 *    PORTAFOGLIO VITA - PROCESSO GARANZIE OPZIONI
	 *    LUNG.
	 * ----------------------------------------------------------------*
	 *  03 WOPZ-AREA-OPZIONI.</pre>*/
    public String eleMaxOpzioni = DefaultValues.stringVal(Len.ELE_MAX_OPZIONI);
    //Original name: WOPZ-OPZIONI
    private WopzOpzioni[] opzioni = new WopzOpzioni[OPZIONI_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ivvc0217() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int opzioniIdx = 1; opzioniIdx <= OPZIONI_MAXOCCURS; opzioniIdx++) {
            opzioni[opzioniIdx - 1] = new WopzOpzioni();
        }
    }

    public void setEleMaxOpzioniFormatted(String eleMaxOpzioni) {
        this.eleMaxOpzioni = Trunc.toUnsignedNumeric(eleMaxOpzioni, Len.ELE_MAX_OPZIONI);
    }

    public short getEleMaxOpzioni() {
        return NumericDisplay.asShort(this.eleMaxOpzioni);
    }

    public WopzOpzioni getOpzioni(int idx) {
        return opzioni[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_MAX_OPZIONI = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
