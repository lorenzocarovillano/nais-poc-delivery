package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.AdaLunghezzaDato;
import it.accenture.jnais.ws.redefines.AdaPrecisioneDato;

/**Original name: ANAG-DATO<br>
 * Variable: ANAG-DATO from copybook IDBVADA1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AnagDatoLdbs1390 {

    //==== PROPERTIES ====
    //Original name: ADA-COD-COMPAGNIA-ANIA
    private int adaCodCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: ADA-COD-DATO
    private String adaCodDato = DefaultValues.stringVal(Len.ADA_COD_DATO);
    //Original name: ADA-DESC-DATO-LEN
    private short adaDescDatoLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: ADA-DESC-DATO
    private String adaDescDato = DefaultValues.stringVal(Len.ADA_DESC_DATO);
    //Original name: ADA-TIPO-DATO
    private String adaTipoDato = DefaultValues.stringVal(Len.ADA_TIPO_DATO);
    //Original name: ADA-LUNGHEZZA-DATO
    private AdaLunghezzaDato adaLunghezzaDato = new AdaLunghezzaDato();
    //Original name: ADA-PRECISIONE-DATO
    private AdaPrecisioneDato adaPrecisioneDato = new AdaPrecisioneDato();
    //Original name: ADA-COD-DOMINIO
    private String adaCodDominio = DefaultValues.stringVal(Len.ADA_COD_DOMINIO);
    //Original name: ADA-FORMATTAZIONE-DATO
    private String adaFormattazioneDato = DefaultValues.stringVal(Len.ADA_FORMATTAZIONE_DATO);
    //Original name: ADA-DS-UTENTE
    private String adaDsUtente = DefaultValues.stringVal(Len.ADA_DS_UTENTE);

    //==== METHODS ====
    public void setAdaCodCompagniaAnia(int adaCodCompagniaAnia) {
        this.adaCodCompagniaAnia = adaCodCompagniaAnia;
    }

    public int getAdaCodCompagniaAnia() {
        return this.adaCodCompagniaAnia;
    }

    public void setAdaCodDato(String adaCodDato) {
        this.adaCodDato = Functions.subString(adaCodDato, Len.ADA_COD_DATO);
    }

    public String getAdaCodDato() {
        return this.adaCodDato;
    }

    public void setAdaDescDatoLen(short adaDescDatoLen) {
        this.adaDescDatoLen = adaDescDatoLen;
    }

    public short getAdaDescDatoLen() {
        return this.adaDescDatoLen;
    }

    public void setAdaDescDato(String adaDescDato) {
        this.adaDescDato = Functions.subString(adaDescDato, Len.ADA_DESC_DATO);
    }

    public String getAdaDescDato() {
        return this.adaDescDato;
    }

    public void setAdaTipoDato(String adaTipoDato) {
        this.adaTipoDato = Functions.subString(adaTipoDato, Len.ADA_TIPO_DATO);
    }

    public String getAdaTipoDato() {
        return this.adaTipoDato;
    }

    public String getAdaTipoDatoFormatted() {
        return Functions.padBlanks(getAdaTipoDato(), Len.ADA_TIPO_DATO);
    }

    public void setAdaCodDominio(String adaCodDominio) {
        this.adaCodDominio = Functions.subString(adaCodDominio, Len.ADA_COD_DOMINIO);
    }

    public String getAdaCodDominio() {
        return this.adaCodDominio;
    }

    public void setAdaFormattazioneDato(String adaFormattazioneDato) {
        this.adaFormattazioneDato = Functions.subString(adaFormattazioneDato, Len.ADA_FORMATTAZIONE_DATO);
    }

    public String getAdaFormattazioneDato() {
        return this.adaFormattazioneDato;
    }

    public void setAdaDsUtente(String adaDsUtente) {
        this.adaDsUtente = Functions.subString(adaDsUtente, Len.ADA_DS_UTENTE);
    }

    public String getAdaDsUtente() {
        return this.adaDsUtente;
    }

    public AdaLunghezzaDato getAdaLunghezzaDato() {
        return adaLunghezzaDato;
    }

    public AdaPrecisioneDato getAdaPrecisioneDato() {
        return adaPrecisioneDato;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ADA_TIPO_DATO = 2;
        public static final int ADA_FORMATTAZIONE_DATO = 20;
        public static final int ADA_COD_DATO = 30;
        public static final int ADA_DESC_DATO = 250;
        public static final int ADA_COD_DOMINIO = 30;
        public static final int ADA_DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
