package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.WpreBatchProva;
import it.accenture.jnais.ws.enums.WpreFlBonus;
import it.accenture.jnais.ws.enums.WpreFlOnlUpdParamComp;
import it.accenture.jnais.ws.enums.WpreIndModRec;
import it.accenture.jnais.ws.enums.WpreIndRecProvv;
import it.accenture.jnais.ws.enums.WpreTpElaEstContoSel;
import it.accenture.jnais.ws.enums.WpreTpFrmAssva;
import it.accenture.jnais.ws.occurs.WpreTabAreaRiass;

/**Original name: WPRE-AREA-TAB-BATCH-PRES<br>
 * Variable: WPRE-AREA-TAB-BATCH-PRES from copybook LOAC0560<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WpreAreaTabBatchPres {

    //==== PROPERTIES ====
    public static final int TAB_AREA_RIASS_MAXOCCURS = 10;
    //Original name: WPRE-MOVI-BATCH
    private String moviBatch = DefaultValues.stringVal(Len.MOVI_BATCH);
    //Original name: WPRE-TP-ELA-EST-CONTO
    private WpreTpElaEstContoSel tpElaEstConto = new WpreTpElaEstContoSel();
    //Original name: WPRE-BATCH-PROVA
    private WpreBatchProva batchProva = new WpreBatchProva();
    //Original name: WPRE-TP-FRM-ASSVA
    private WpreTpFrmAssva tpFrmAssva = new WpreTpFrmAssva();
    //Original name: WPRE-FL-BONUS
    private WpreFlBonus flBonus = new WpreFlBonus();
    //Original name: WPRE-TP-RIASS
    private String tpRiass = DefaultValues.stringVal(Len.TP_RIASS);
    //Original name: WPRE-ELE-MAX-RIASS
    private short eleMaxRiass = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPRE-TAB-AREA-RIASS
    private WpreTabAreaRiass[] tabAreaRiass = new WpreTabAreaRiass[TAB_AREA_RIASS_MAXOCCURS];
    //Original name: WPRE-DT-COMPETENZA-DET
    private String dtCompetenzaDet = DefaultValues.stringVal(Len.DT_COMPETENZA_DET);
    //Original name: WPRE-IB-OGGETTO
    private String ibOggetto = DefaultValues.stringVal(Len.IB_OGGETTO);
    //Original name: WPRE-DT-ELAB-DA
    private String dtElabDa = DefaultValues.stringVal(Len.DT_ELAB_DA);
    //Original name: WPRE-DT-ELAB-A
    private String dtElabA = DefaultValues.stringVal(Len.DT_ELAB_A);
    //Original name: WPRE-FL-ONL-UPD-PARAM-COMP
    private WpreFlOnlUpdParamComp flOnlUpdParamComp = new WpreFlOnlUpdParamComp();
    //Original name: WPRE-RAMO
    private String ramo = DefaultValues.stringVal(Len.RAMO);
    //Original name: WPRE-CANALE-VENDITA
    private String canaleVendita = DefaultValues.stringVal(Len.CANALE_VENDITA);
    //Original name: WPRE-IND-REC-PROVV
    private WpreIndRecProvv indRecProvv = new WpreIndRecProvv();
    //Original name: WPRE-DATA-ATTIVAZIONE
    private String dataAttivazione = DefaultValues.stringVal(Len.DATA_ATTIVAZIONE);
    /**Original name: WPRE-IND-MOD-REC<br>
	 * <pre>      INDICATORE MODALITA RECUPERO. (COLLETTIVA, POSIZIONE).</pre>*/
    private WpreIndModRec indModRec = new WpreIndModRec();
    //Original name: WPRE-DATA-COMPETENZA
    private long dataCompetenza = DefaultValues.LONG_VAL;

    //==== CONSTRUCTORS ====
    public WpreAreaTabBatchPres() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabAreaRiassIdx = 1; tabAreaRiassIdx <= TAB_AREA_RIASS_MAXOCCURS; tabAreaRiassIdx++) {
            tabAreaRiass[tabAreaRiassIdx - 1] = new WpreTabAreaRiass();
        }
    }

    public void setAreaTabBatchPresFormatted(String data) {
        byte[] buffer = new byte[Len.AREA_TAB_BATCH_PRES];
        MarshalByte.writeString(buffer, 1, data, Len.AREA_TAB_BATCH_PRES);
        setAreaTabBatchPresBytes(buffer, 1);
    }

    public void setAreaTabBatchPresBytes(byte[] buffer, int offset) {
        int position = offset;
        moviBatch = MarshalByte.readFixedString(buffer, position, Len.MOVI_BATCH);
        position += Len.MOVI_BATCH;
        tpElaEstConto.setTpElaEstConto(MarshalByte.readString(buffer, position, WpreTpElaEstContoSel.Len.TP_ELA_EST_CONTO));
        position += WpreTpElaEstContoSel.Len.TP_ELA_EST_CONTO;
        batchProva.setBatchProva(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        tpFrmAssva.setTpFrmAssva(MarshalByte.readString(buffer, position, WpreTpFrmAssva.Len.TP_FRM_ASSVA));
        position += WpreTpFrmAssva.Len.TP_FRM_ASSVA;
        flBonus.setFlBonus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        tpRiass = MarshalByte.readString(buffer, position, Len.TP_RIASS);
        position += Len.TP_RIASS;
        eleMaxRiass = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_AREA_RIASS_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabAreaRiass[idx - 1].setTabAreaRiassBytes(buffer, position);
                position += WpreTabAreaRiass.Len.TAB_AREA_RIASS;
            }
            else {
                tabAreaRiass[idx - 1].initTabAreaRiassSpaces();
                position += WpreTabAreaRiass.Len.TAB_AREA_RIASS;
            }
        }
        dtCompetenzaDet = MarshalByte.readFixedString(buffer, position, Len.DT_COMPETENZA_DET);
        position += Len.DT_COMPETENZA_DET;
        ibOggetto = MarshalByte.readString(buffer, position, Len.IB_OGGETTO);
        position += Len.IB_OGGETTO;
        dtElabDa = MarshalByte.readFixedString(buffer, position, Len.DT_ELAB_DA);
        position += Len.DT_ELAB_DA;
        dtElabA = MarshalByte.readFixedString(buffer, position, Len.DT_ELAB_A);
        position += Len.DT_ELAB_A;
        flOnlUpdParamComp.setFlOnlUpdParamComp(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        ramo = MarshalByte.readString(buffer, position, Len.RAMO);
        position += Len.RAMO;
        canaleVendita = MarshalByte.readFixedString(buffer, position, Len.CANALE_VENDITA);
        position += Len.CANALE_VENDITA;
        indRecProvv.setIndRecProvv(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        dataAttivazione = MarshalByte.readFixedString(buffer, position, Len.DATA_ATTIVAZIONE);
        position += Len.DATA_ATTIVAZIONE;
        indModRec.setIndModRec(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        dataCompetenza = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DATA_COMPETENZA, 0);
    }

    public String getWpreMoviBatchFormatted() {
        return this.moviBatch;
    }

    public void setTpRiass(String tpRiass) {
        this.tpRiass = Functions.subString(tpRiass, Len.TP_RIASS);
    }

    public String getTpRiass() {
        return this.tpRiass;
    }

    public void setEleMaxRiass(short eleMaxRiass) {
        this.eleMaxRiass = eleMaxRiass;
    }

    public short getEleMaxRiass() {
        return this.eleMaxRiass;
    }

    public void setIbOggetto(String ibOggetto) {
        this.ibOggetto = Functions.subString(ibOggetto, Len.IB_OGGETTO);
    }

    public String getIbOggetto() {
        return this.ibOggetto;
    }

    public String getDtElabDaFormatted() {
        return this.dtElabDa;
    }

    public String getDtElabAFormatted() {
        return this.dtElabA;
    }

    public void setRamo(String ramo) {
        this.ramo = Functions.subString(ramo, Len.RAMO);
    }

    public String getRamo() {
        return this.ramo;
    }

    public void setDataCompetenza(long dataCompetenza) {
        this.dataCompetenza = dataCompetenza;
    }

    public long getDataCompetenza() {
        return this.dataCompetenza;
    }

    public WpreTpFrmAssva getTpFrmAssva() {
        return tpFrmAssva;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MOVI_BATCH = 5;
        public static final int TP_RIASS = 2;
        public static final int DT_COMPETENZA_DET = 8;
        public static final int IB_OGGETTO = 40;
        public static final int DT_ELAB_DA = 8;
        public static final int DT_ELAB_A = 8;
        public static final int RAMO = 12;
        public static final int CANALE_VENDITA = 5;
        public static final int DATA_ATTIVAZIONE = 8;
        public static final int ELE_MAX_RIASS = 2;
        public static final int DATA_COMPETENZA = 10;
        public static final int AREA_TAB_BATCH_PRES = MOVI_BATCH + WpreTpElaEstContoSel.Len.TP_ELA_EST_CONTO + WpreBatchProva.Len.BATCH_PROVA + WpreTpFrmAssva.Len.TP_FRM_ASSVA + WpreFlBonus.Len.FL_BONUS + TP_RIASS + ELE_MAX_RIASS + WpreAreaTabBatchPres.TAB_AREA_RIASS_MAXOCCURS * WpreTabAreaRiass.Len.TAB_AREA_RIASS + DT_COMPETENZA_DET + IB_OGGETTO + DT_ELAB_DA + DT_ELAB_A + WpreFlOnlUpdParamComp.Len.FL_ONL_UPD_PARAM_COMP + RAMO + CANALE_VENDITA + WpreIndRecProvv.Len.IND_REC_PROVV + DATA_ATTIVAZIONE + WpreIndModRec.Len.IND_MOD_REC + DATA_COMPETENZA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DATA_COMPETENZA = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
