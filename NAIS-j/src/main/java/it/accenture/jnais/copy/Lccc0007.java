package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WcomFlagOverflow;

/**Original name: LCCC0007<br>
 * Variable: LCCC0007 from copybook LCCC0007<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccc0007 {

    //==== PROPERTIES ====
    //Original name: WCOM-FLAG-OVERFLOW
    private WcomFlagOverflow flagOverflow = new WcomFlagOverflow();

    //==== METHODS ====
    public WcomFlagOverflow getFlagOverflow() {
        return flagOverflow;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGGETTO_PTF = 9;
        public static final int TIPO_OGGETTO_PTF = 2;
        public static final int ID_PTF = 9;
        public static final int IB_OGGETTO_PTF = 40;
        public static final int IB_OGGETTO_SEC_PTF = 40;
        public static final int DATA_INIZIO_EFFETTO_PTF = 8;
        public static final int DATA_FINE_EFFETTO_PTF = 8;
        public static final int NOME_QUERY_PTF = 8;
        public static final int DATI_WHERE_COND_PTF = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
