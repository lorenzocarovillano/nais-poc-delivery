package it.accenture.jnais.copy;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-DTC-MAX<br>
 * Variable: WK-DTC-MAX from copybook LCCVDTCZ<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkDtcMax {

    //==== PROPERTIES ====
    //Original name: WK-DTC-MAX-A
    private short a = ((short)100);

    //==== METHODS ====
    public short getA() {
        return this.a;
    }
}
