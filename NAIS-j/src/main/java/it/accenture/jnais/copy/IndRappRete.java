package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-RAPP-RETE<br>
 * Variable: IND-RAPP-RETE from copybook IDBVRRE2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndRappRete {

    //==== PROPERTIES ====
    //Original name: IND-RRE-ID-OGG
    private short idOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RRE-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RRE-TP-ACQS-CNTRT
    private short tpAcqsCntrt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RRE-COD-ACQS-CNTRT
    private short codAcqsCntrt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RRE-COD-PNT-RETE-INI
    private short codPntReteIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RRE-COD-PNT-RETE-END
    private short codPntReteEnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RRE-FL-PNT-RETE-1RIO
    private short flPntRete1rio = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RRE-COD-CAN
    private short codCan = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RRE-COD-PNT-RETE-INI-C
    private short codPntReteIniC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RRE-MATR-OPRT
    private short matrOprt = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idOgg = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idOgg;
    }

    public void setDtSin1oAssto(short dtSin1oAssto) {
        this.idMoviChiu = dtSin1oAssto;
    }

    public short getDtSin1oAssto() {
        return this.idMoviChiu;
    }

    public void setCauSin1oAssto(short cauSin1oAssto) {
        this.tpAcqsCntrt = cauSin1oAssto;
    }

    public short getCauSin1oAssto() {
        return this.tpAcqsCntrt;
    }

    public void setTpSin1oAssto(short tpSin1oAssto) {
        this.codAcqsCntrt = tpSin1oAssto;
    }

    public short getTpSin1oAssto() {
        return this.codAcqsCntrt;
    }

    public void setDtSin2oAssto(short dtSin2oAssto) {
        this.codPntReteIni = dtSin2oAssto;
    }

    public short getDtSin2oAssto() {
        return this.codPntReteIni;
    }

    public void setCauSin2oAssto(short cauSin2oAssto) {
        this.codPntReteEnd = cauSin2oAssto;
    }

    public short getCauSin2oAssto() {
        return this.codPntReteEnd;
    }

    public void setTpSin2oAssto(short tpSin2oAssto) {
        this.flPntRete1rio = tpSin2oAssto;
    }

    public short getTpSin2oAssto() {
        return this.flPntRete1rio;
    }

    public void setDtSin3oAssto(short dtSin3oAssto) {
        this.codCan = dtSin3oAssto;
    }

    public short getDtSin3oAssto() {
        return this.codCan;
    }

    public void setCauSin3oAssto(short cauSin3oAssto) {
        this.codPntReteIniC = cauSin3oAssto;
    }

    public short getCauSin3oAssto() {
        return this.codPntReteIniC;
    }

    public void setTpSin3oAssto(short tpSin3oAssto) {
        this.matrOprt = tpSin3oAssto;
    }

    public short getTpSin3oAssto() {
        return this.matrOprt;
    }
}
