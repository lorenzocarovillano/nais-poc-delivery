package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: GAR-DB<br>
 * Variable: GAR-DB from copybook IDBVGRZ3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class GarDb {

    //==== PROPERTIES ====
    //Original name: GRZ-DT-INI-EFF-DB
    private String iniEffDb = DefaultValues.stringVal(Len.INI_EFF_DB);
    //Original name: GRZ-DT-END-EFF-DB
    private String endEffDb = DefaultValues.stringVal(Len.END_EFF_DB);
    //Original name: GRZ-DT-DECOR-DB
    private String decorDb = DefaultValues.stringVal(Len.DECOR_DB);
    //Original name: GRZ-DT-SCAD-DB
    private String scadDb = DefaultValues.stringVal(Len.SCAD_DB);
    //Original name: GRZ-DT-INI-VAL-TAR-DB
    private String iniValTarDb = DefaultValues.stringVal(Len.INI_VAL_TAR_DB);
    //Original name: GRZ-DT-END-CARZ-DB
    private String endCarzDb = DefaultValues.stringVal(Len.END_CARZ_DB);
    //Original name: GRZ-DT-VARZ-TP-IAS-DB
    private String varzTpIasDb = DefaultValues.stringVal(Len.VARZ_TP_IAS_DB);
    //Original name: GRZ-DT-PRESC-DB
    private String prescDb = DefaultValues.stringVal(Len.PRESC_DB);

    //==== METHODS ====
    public void setIniEffDb(String iniEffDb) {
        this.iniEffDb = Functions.subString(iniEffDb, Len.INI_EFF_DB);
    }

    public String getIniEffDb() {
        return this.iniEffDb;
    }

    public void setEndEffDb(String endEffDb) {
        this.endEffDb = Functions.subString(endEffDb, Len.END_EFF_DB);
    }

    public String getEndEffDb() {
        return this.endEffDb;
    }

    public void setDecorDb(String decorDb) {
        this.decorDb = Functions.subString(decorDb, Len.DECOR_DB);
    }

    public String getDecorDb() {
        return this.decorDb;
    }

    public void setScadDb(String scadDb) {
        this.scadDb = Functions.subString(scadDb, Len.SCAD_DB);
    }

    public String getScadDb() {
        return this.scadDb;
    }

    public void setIniValTarDb(String iniValTarDb) {
        this.iniValTarDb = Functions.subString(iniValTarDb, Len.INI_VAL_TAR_DB);
    }

    public String getIniValTarDb() {
        return this.iniValTarDb;
    }

    public void setEndCarzDb(String endCarzDb) {
        this.endCarzDb = Functions.subString(endCarzDb, Len.END_CARZ_DB);
    }

    public String getEndCarzDb() {
        return this.endCarzDb;
    }

    public void setVarzTpIasDb(String varzTpIasDb) {
        this.varzTpIasDb = Functions.subString(varzTpIasDb, Len.VARZ_TP_IAS_DB);
    }

    public String getVarzTpIasDb() {
        return this.varzTpIasDb;
    }

    public void setPrescDb(String prescDb) {
        this.prescDb = Functions.subString(prescDb, Len.PRESC_DB);
    }

    public String getPrescDb() {
        return this.prescDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INI_EFF_DB = 10;
        public static final int END_EFF_DB = 10;
        public static final int DECOR_DB = 10;
        public static final int SCAD_DB = 10;
        public static final int INI_VAL_TAR_DB = 10;
        public static final int END_CARZ_DB = 10;
        public static final int VARZ_TP_IAS_DB = 10;
        public static final int PRESC_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
