package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: LDBV5991<br>
 * Variable: LDBV5991 from copybook LDBV5991<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv5991 {

    //==== PROPERTIES ====
    //Original name: LDBV5991-TP-MOV1
    private int mov1 = DefaultValues.INT_VAL;
    //Original name: LDBV5991-TP-MOV2
    private int mov2 = DefaultValues.INT_VAL;
    //Original name: LDBV5991-TP-MOV3
    private int mov3 = DefaultValues.INT_VAL;
    //Original name: LDBV5991-TP-MOV4
    private int mov4 = DefaultValues.INT_VAL;
    //Original name: LDBV5991-TP-MOV5
    private int mov5 = DefaultValues.INT_VAL;
    //Original name: LDBV5991-TP-MOV6
    private int mov6 = DefaultValues.INT_VAL;
    //Original name: LDBV5991-TP-MOV7
    private int mov7 = DefaultValues.INT_VAL;
    //Original name: LDBV5991-TP-MOV8
    private int mov8 = DefaultValues.INT_VAL;
    //Original name: LDBV5991-TP-MOV9
    private int mov9 = DefaultValues.INT_VAL;
    //Original name: LDBV5991-TP-MOV10
    private int mov10 = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbv5991Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV5991];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV5991);
        setLdbv5991Bytes(buffer, 1);
    }

    public String getLdbv5991Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv5991Bytes());
    }

    public byte[] getLdbv5991Bytes() {
        byte[] buffer = new byte[Len.LDBV5991];
        return getLdbv5991Bytes(buffer, 1);
    }

    public void setLdbv5991Bytes(byte[] buffer, int offset) {
        int position = offset;
        mov1 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV1, 0);
        position += Len.MOV1;
        mov2 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV2, 0);
        position += Len.MOV2;
        mov3 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV3, 0);
        position += Len.MOV3;
        mov4 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV4, 0);
        position += Len.MOV4;
        mov5 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV5, 0);
        position += Len.MOV5;
        mov6 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV6, 0);
        position += Len.MOV6;
        mov7 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV7, 0);
        position += Len.MOV7;
        mov8 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV8, 0);
        position += Len.MOV8;
        mov9 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV9, 0);
        position += Len.MOV9;
        mov10 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOV10, 0);
    }

    public byte[] getLdbv5991Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, mov1, Len.Int.MOV1, 0);
        position += Len.MOV1;
        MarshalByte.writeIntAsPacked(buffer, position, mov2, Len.Int.MOV2, 0);
        position += Len.MOV2;
        MarshalByte.writeIntAsPacked(buffer, position, mov3, Len.Int.MOV3, 0);
        position += Len.MOV3;
        MarshalByte.writeIntAsPacked(buffer, position, mov4, Len.Int.MOV4, 0);
        position += Len.MOV4;
        MarshalByte.writeIntAsPacked(buffer, position, mov5, Len.Int.MOV5, 0);
        position += Len.MOV5;
        MarshalByte.writeIntAsPacked(buffer, position, mov6, Len.Int.MOV6, 0);
        position += Len.MOV6;
        MarshalByte.writeIntAsPacked(buffer, position, mov7, Len.Int.MOV7, 0);
        position += Len.MOV7;
        MarshalByte.writeIntAsPacked(buffer, position, mov8, Len.Int.MOV8, 0);
        position += Len.MOV8;
        MarshalByte.writeIntAsPacked(buffer, position, mov9, Len.Int.MOV9, 0);
        position += Len.MOV9;
        MarshalByte.writeIntAsPacked(buffer, position, mov10, Len.Int.MOV10, 0);
        return buffer;
    }

    public void setMov1(int mov1) {
        this.mov1 = mov1;
    }

    public int getMov1() {
        return this.mov1;
    }

    public void setMov2(int mov2) {
        this.mov2 = mov2;
    }

    public int getMov2() {
        return this.mov2;
    }

    public void setMov3(int mov3) {
        this.mov3 = mov3;
    }

    public int getMov3() {
        return this.mov3;
    }

    public void setMov4(int mov4) {
        this.mov4 = mov4;
    }

    public int getMov4() {
        return this.mov4;
    }

    public void setMov5(int mov5) {
        this.mov5 = mov5;
    }

    public int getMov5() {
        return this.mov5;
    }

    public void setMov6(int mov6) {
        this.mov6 = mov6;
    }

    public int getMov6() {
        return this.mov6;
    }

    public void setMov7(int mov7) {
        this.mov7 = mov7;
    }

    public int getMov7() {
        return this.mov7;
    }

    public void setMov8(int mov8) {
        this.mov8 = mov8;
    }

    public int getMov8() {
        return this.mov8;
    }

    public void setMov9(int mov9) {
        this.mov9 = mov9;
    }

    public int getMov9() {
        return this.mov9;
    }

    public void setMov10(int mov10) {
        this.mov10 = mov10;
    }

    public int getMov10() {
        return this.mov10;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MOV1 = 3;
        public static final int MOV2 = 3;
        public static final int MOV3 = 3;
        public static final int MOV4 = 3;
        public static final int MOV5 = 3;
        public static final int MOV6 = 3;
        public static final int MOV7 = 3;
        public static final int MOV8 = 3;
        public static final int MOV9 = 3;
        public static final int MOV10 = 3;
        public static final int LDBV5991 = MOV1 + MOV2 + MOV3 + MOV4 + MOV5 + MOV6 + MOV7 + MOV8 + MOV9 + MOV10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MOV1 = 5;
            public static final int MOV2 = 5;
            public static final int MOV3 = 5;
            public static final int MOV4 = 5;
            public static final int MOV5 = 5;
            public static final int MOV6 = 5;
            public static final int MOV7 = 5;
            public static final int MOV8 = 5;
            public static final int MOV9 = 5;
            public static final int MOV10 = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
