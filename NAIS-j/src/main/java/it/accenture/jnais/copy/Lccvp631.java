package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LCCVP631<br>
 * Variable: LCCVP631 from copybook LCCVP631<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvp631 {

    //==== PROPERTIES ====
    //Original name: WP63-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WP63-DATI
    private Wp63Dati dati = new Wp63Dati();

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public Wp63Dati getDati() {
        return dati;
    }
}
