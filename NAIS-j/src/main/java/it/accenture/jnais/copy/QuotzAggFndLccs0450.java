package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: QUOTZ-AGG-FND<br>
 * Variable: QUOTZ-AGG-FND from copybook IDBVL411<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class QuotzAggFndLccs0450 {

    //==== PROPERTIES ====
    //Original name: L41-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: L41-TP-QTZ
    private String tpQtz = DefaultValues.stringVal(Len.TP_QTZ);
    //Original name: L41-COD-FND
    private String codFnd = DefaultValues.stringVal(Len.COD_FND);
    //Original name: L41-DT-QTZ
    private int dtQtz = DefaultValues.INT_VAL;
    //Original name: L41-TP-FND
    private char tpFnd = DefaultValues.CHAR_VAL;
    //Original name: L41-VAL-QUO
    private AfDecimal valQuo = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: L41-DESC-AGG-LEN
    private short descAggLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: L41-DESC-AGG
    private String descAgg = DefaultValues.stringVal(Len.DESC_AGG);
    //Original name: L41-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: L41-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: L41-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: L41-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: L41-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setQuotzAggFndFormatted(String data) {
        byte[] buffer = new byte[Len.QUOTZ_AGG_FND];
        MarshalByte.writeString(buffer, 1, data, Len.QUOTZ_AGG_FND);
        setQuotzAggFndBytes(buffer, 1);
    }

    public String getQuotzAggFndFormatted() {
        return MarshalByteExt.bufferToStr(getQuotzAggFndBytes());
    }

    public byte[] getQuotzAggFndBytes() {
        byte[] buffer = new byte[Len.QUOTZ_AGG_FND];
        return getQuotzAggFndBytes(buffer, 1);
    }

    public void setQuotzAggFndBytes(byte[] buffer, int offset) {
        int position = offset;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        tpQtz = MarshalByte.readString(buffer, position, Len.TP_QTZ);
        position += Len.TP_QTZ;
        codFnd = MarshalByte.readString(buffer, position, Len.COD_FND);
        position += Len.COD_FND;
        dtQtz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_QTZ, 0);
        position += Len.DT_QTZ;
        tpFnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        valQuo.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.VAL_QUO, Len.Fract.VAL_QUO));
        position += Len.VAL_QUO;
        setDescAggVcharBytes(buffer, position);
        position += Len.DESC_AGG_VCHAR;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getQuotzAggFndBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, tpQtz, Len.TP_QTZ);
        position += Len.TP_QTZ;
        MarshalByte.writeString(buffer, position, codFnd, Len.COD_FND);
        position += Len.COD_FND;
        MarshalByte.writeIntAsPacked(buffer, position, dtQtz, Len.Int.DT_QTZ, 0);
        position += Len.DT_QTZ;
        MarshalByte.writeChar(buffer, position, tpFnd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, valQuo.copy());
        position += Len.VAL_QUO;
        getDescAggVcharBytes(buffer, position);
        position += Len.DESC_AGG_VCHAR;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsCptz, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        return buffer;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setTpQtz(String tpQtz) {
        this.tpQtz = Functions.subString(tpQtz, Len.TP_QTZ);
    }

    public String getTpQtz() {
        return this.tpQtz;
    }

    public void setCodFnd(String codFnd) {
        this.codFnd = Functions.subString(codFnd, Len.COD_FND);
    }

    public String getCodFnd() {
        return this.codFnd;
    }

    public void setDtQtz(int dtQtz) {
        this.dtQtz = dtQtz;
    }

    public int getDtQtz() {
        return this.dtQtz;
    }

    public void setTpFnd(char tpFnd) {
        this.tpFnd = tpFnd;
    }

    public char getTpFnd() {
        return this.tpFnd;
    }

    public void setValQuo(AfDecimal valQuo) {
        this.valQuo.assign(valQuo);
    }

    public AfDecimal getValQuo() {
        return this.valQuo.copy();
    }

    public void setDescAggVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        descAggLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        descAgg = MarshalByte.readString(buffer, position, Len.DESC_AGG);
    }

    public byte[] getDescAggVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, descAggLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, descAgg, Len.DESC_AGG);
        return buffer;
    }

    public void setDescAggLen(short descAggLen) {
        this.descAggLen = descAggLen;
    }

    public short getDescAggLen() {
        return this.descAggLen;
    }

    public void setDescAgg(String descAgg) {
        this.descAgg = Functions.subString(descAgg, Len.DESC_AGG);
    }

    public String getDescAgg() {
        return this.descAgg;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_QTZ = 2;
        public static final int COD_FND = 12;
        public static final int DESC_AGG = 100;
        public static final int DS_UTENTE = 20;
        public static final int COD_COMP_ANIA = 3;
        public static final int DT_QTZ = 5;
        public static final int TP_FND = 1;
        public static final int VAL_QUO = 7;
        public static final int DESC_AGG_LEN = 2;
        public static final int DESC_AGG_VCHAR = DESC_AGG_LEN + DESC_AGG;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_CPTZ = 10;
        public static final int DS_STATO_ELAB = 1;
        public static final int QUOTZ_AGG_FND = COD_COMP_ANIA + TP_QTZ + COD_FND + DT_QTZ + TP_FND + VAL_QUO + DESC_AGG_VCHAR + DS_OPER_SQL + DS_VER + DS_TS_CPTZ + DS_UTENTE + DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_QTZ = 8;
            public static final int VAL_QUO = 7;
            public static final int DS_VER = 9;
            public static final int DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAL_QUO = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
