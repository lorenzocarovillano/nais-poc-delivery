package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.L11IdOgg1rio;
import it.accenture.jnais.ws.redefines.L11IdRich;

/**Original name: OGG-BLOCCO<br>
 * Variable: OGG-BLOCCO from copybook IDBVL111<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class OggBlocco {

    //==== PROPERTIES ====
    //Original name: L11-ID-OGG-BLOCCO
    private int l11IdOggBlocco = DefaultValues.INT_VAL;
    //Original name: L11-ID-OGG-1RIO
    private L11IdOgg1rio l11IdOgg1rio = new L11IdOgg1rio();
    //Original name: L11-TP-OGG-1RIO
    private String l11TpOgg1rio = DefaultValues.stringVal(Len.L11_TP_OGG1RIO);
    //Original name: L11-COD-COMP-ANIA
    private int l11CodCompAnia = DefaultValues.INT_VAL;
    //Original name: L11-TP-MOVI
    private int l11TpMovi = DefaultValues.INT_VAL;
    //Original name: L11-TP-OGG
    private String l11TpOgg = DefaultValues.stringVal(Len.L11_TP_OGG);
    //Original name: L11-ID-OGG
    private int l11IdOgg = DefaultValues.INT_VAL;
    //Original name: L11-DT-EFF
    private int l11DtEff = DefaultValues.INT_VAL;
    //Original name: L11-TP-STAT-BLOCCO
    private String l11TpStatBlocco = DefaultValues.stringVal(Len.L11_TP_STAT_BLOCCO);
    //Original name: L11-COD-BLOCCO
    private String l11CodBlocco = DefaultValues.stringVal(Len.L11_COD_BLOCCO);
    //Original name: L11-ID-RICH
    private L11IdRich l11IdRich = new L11IdRich();
    //Original name: L11-DS-OPER-SQL
    private char l11DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: L11-DS-VER
    private int l11DsVer = DefaultValues.INT_VAL;
    //Original name: L11-DS-TS-CPTZ
    private long l11DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: L11-DS-UTENTE
    private String l11DsUtente = DefaultValues.stringVal(Len.L11_DS_UTENTE);
    //Original name: L11-DS-STATO-ELAB
    private char l11DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setOggBloccoFormatted(String data) {
        byte[] buffer = new byte[Len.OGG_BLOCCO];
        MarshalByte.writeString(buffer, 1, data, Len.OGG_BLOCCO);
        setOggBloccoBytes(buffer, 1);
    }

    public String getOggBloccoFormatted() {
        return MarshalByteExt.bufferToStr(getOggBloccoBytes());
    }

    public byte[] getOggBloccoBytes() {
        byte[] buffer = new byte[Len.OGG_BLOCCO];
        return getOggBloccoBytes(buffer, 1);
    }

    public void setOggBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        l11IdOggBlocco = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L11_ID_OGG_BLOCCO, 0);
        position += Len.L11_ID_OGG_BLOCCO;
        l11IdOgg1rio.setL11IdOgg1rioFromBuffer(buffer, position);
        position += L11IdOgg1rio.Len.L11_ID_OGG1RIO;
        l11TpOgg1rio = MarshalByte.readString(buffer, position, Len.L11_TP_OGG1RIO);
        position += Len.L11_TP_OGG1RIO;
        l11CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L11_COD_COMP_ANIA, 0);
        position += Len.L11_COD_COMP_ANIA;
        l11TpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L11_TP_MOVI, 0);
        position += Len.L11_TP_MOVI;
        l11TpOgg = MarshalByte.readString(buffer, position, Len.L11_TP_OGG);
        position += Len.L11_TP_OGG;
        l11IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L11_ID_OGG, 0);
        position += Len.L11_ID_OGG;
        l11DtEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L11_DT_EFF, 0);
        position += Len.L11_DT_EFF;
        l11TpStatBlocco = MarshalByte.readString(buffer, position, Len.L11_TP_STAT_BLOCCO);
        position += Len.L11_TP_STAT_BLOCCO;
        l11CodBlocco = MarshalByte.readString(buffer, position, Len.L11_COD_BLOCCO);
        position += Len.L11_COD_BLOCCO;
        l11IdRich.setL11IdRichFromBuffer(buffer, position);
        position += L11IdRich.Len.L11_ID_RICH;
        l11DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l11DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L11_DS_VER, 0);
        position += Len.L11_DS_VER;
        l11DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L11_DS_TS_CPTZ, 0);
        position += Len.L11_DS_TS_CPTZ;
        l11DsUtente = MarshalByte.readString(buffer, position, Len.L11_DS_UTENTE);
        position += Len.L11_DS_UTENTE;
        l11DsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getOggBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, l11IdOggBlocco, Len.Int.L11_ID_OGG_BLOCCO, 0);
        position += Len.L11_ID_OGG_BLOCCO;
        l11IdOgg1rio.getL11IdOgg1rioAsBuffer(buffer, position);
        position += L11IdOgg1rio.Len.L11_ID_OGG1RIO;
        MarshalByte.writeString(buffer, position, l11TpOgg1rio, Len.L11_TP_OGG1RIO);
        position += Len.L11_TP_OGG1RIO;
        MarshalByte.writeIntAsPacked(buffer, position, l11CodCompAnia, Len.Int.L11_COD_COMP_ANIA, 0);
        position += Len.L11_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, l11TpMovi, Len.Int.L11_TP_MOVI, 0);
        position += Len.L11_TP_MOVI;
        MarshalByte.writeString(buffer, position, l11TpOgg, Len.L11_TP_OGG);
        position += Len.L11_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, l11IdOgg, Len.Int.L11_ID_OGG, 0);
        position += Len.L11_ID_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, l11DtEff, Len.Int.L11_DT_EFF, 0);
        position += Len.L11_DT_EFF;
        MarshalByte.writeString(buffer, position, l11TpStatBlocco, Len.L11_TP_STAT_BLOCCO);
        position += Len.L11_TP_STAT_BLOCCO;
        MarshalByte.writeString(buffer, position, l11CodBlocco, Len.L11_COD_BLOCCO);
        position += Len.L11_COD_BLOCCO;
        l11IdRich.getL11IdRichAsBuffer(buffer, position);
        position += L11IdRich.Len.L11_ID_RICH;
        MarshalByte.writeChar(buffer, position, l11DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, l11DsVer, Len.Int.L11_DS_VER, 0);
        position += Len.L11_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, l11DsTsCptz, Len.Int.L11_DS_TS_CPTZ, 0);
        position += Len.L11_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, l11DsUtente, Len.L11_DS_UTENTE);
        position += Len.L11_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, l11DsStatoElab);
        return buffer;
    }

    public void setL11IdOggBlocco(int l11IdOggBlocco) {
        this.l11IdOggBlocco = l11IdOggBlocco;
    }

    public int getL11IdOggBlocco() {
        return this.l11IdOggBlocco;
    }

    public void setL11TpOgg1rio(String l11TpOgg1rio) {
        this.l11TpOgg1rio = Functions.subString(l11TpOgg1rio, Len.L11_TP_OGG1RIO);
    }

    public String getL11TpOgg1rio() {
        return this.l11TpOgg1rio;
    }

    public void setL11CodCompAnia(int l11CodCompAnia) {
        this.l11CodCompAnia = l11CodCompAnia;
    }

    public int getL11CodCompAnia() {
        return this.l11CodCompAnia;
    }

    public void setL11TpMovi(int l11TpMovi) {
        this.l11TpMovi = l11TpMovi;
    }

    public int getL11TpMovi() {
        return this.l11TpMovi;
    }

    public void setL11TpOgg(String l11TpOgg) {
        this.l11TpOgg = Functions.subString(l11TpOgg, Len.L11_TP_OGG);
    }

    public String getL11TpOgg() {
        return this.l11TpOgg;
    }

    public void setL11IdOgg(int l11IdOgg) {
        this.l11IdOgg = l11IdOgg;
    }

    public int getL11IdOgg() {
        return this.l11IdOgg;
    }

    public void setL11DtEff(int l11DtEff) {
        this.l11DtEff = l11DtEff;
    }

    public int getL11DtEff() {
        return this.l11DtEff;
    }

    public void setL11TpStatBlocco(String l11TpStatBlocco) {
        this.l11TpStatBlocco = Functions.subString(l11TpStatBlocco, Len.L11_TP_STAT_BLOCCO);
    }

    public String getL11TpStatBlocco() {
        return this.l11TpStatBlocco;
    }

    public void setL11CodBlocco(String l11CodBlocco) {
        this.l11CodBlocco = Functions.subString(l11CodBlocco, Len.L11_COD_BLOCCO);
    }

    public String getL11CodBlocco() {
        return this.l11CodBlocco;
    }

    public void setL11DsOperSql(char l11DsOperSql) {
        this.l11DsOperSql = l11DsOperSql;
    }

    public char getL11DsOperSql() {
        return this.l11DsOperSql;
    }

    public void setL11DsVer(int l11DsVer) {
        this.l11DsVer = l11DsVer;
    }

    public int getL11DsVer() {
        return this.l11DsVer;
    }

    public void setL11DsTsCptz(long l11DsTsCptz) {
        this.l11DsTsCptz = l11DsTsCptz;
    }

    public long getL11DsTsCptz() {
        return this.l11DsTsCptz;
    }

    public void setL11DsUtente(String l11DsUtente) {
        this.l11DsUtente = Functions.subString(l11DsUtente, Len.L11_DS_UTENTE);
    }

    public String getL11DsUtente() {
        return this.l11DsUtente;
    }

    public void setL11DsStatoElab(char l11DsStatoElab) {
        this.l11DsStatoElab = l11DsStatoElab;
    }

    public char getL11DsStatoElab() {
        return this.l11DsStatoElab;
    }

    public L11IdOgg1rio getL11IdOgg1rio() {
        return l11IdOgg1rio;
    }

    public L11IdRich getL11IdRich() {
        return l11IdRich;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L11_TP_OGG1RIO = 2;
        public static final int L11_TP_OGG = 2;
        public static final int L11_TP_STAT_BLOCCO = 2;
        public static final int L11_COD_BLOCCO = 5;
        public static final int L11_DS_UTENTE = 20;
        public static final int L11_ID_OGG_BLOCCO = 5;
        public static final int L11_COD_COMP_ANIA = 3;
        public static final int L11_TP_MOVI = 3;
        public static final int L11_ID_OGG = 5;
        public static final int L11_DT_EFF = 5;
        public static final int L11_DS_OPER_SQL = 1;
        public static final int L11_DS_VER = 5;
        public static final int L11_DS_TS_CPTZ = 10;
        public static final int L11_DS_STATO_ELAB = 1;
        public static final int OGG_BLOCCO = L11_ID_OGG_BLOCCO + L11IdOgg1rio.Len.L11_ID_OGG1RIO + L11_TP_OGG1RIO + L11_COD_COMP_ANIA + L11_TP_MOVI + L11_TP_OGG + L11_ID_OGG + L11_DT_EFF + L11_TP_STAT_BLOCCO + L11_COD_BLOCCO + L11IdRich.Len.L11_ID_RICH + L11_DS_OPER_SQL + L11_DS_VER + L11_DS_TS_CPTZ + L11_DS_UTENTE + L11_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L11_ID_OGG_BLOCCO = 9;
            public static final int L11_COD_COMP_ANIA = 5;
            public static final int L11_TP_MOVI = 5;
            public static final int L11_ID_OGG = 9;
            public static final int L11_DT_EFF = 8;
            public static final int L11_DS_VER = 9;
            public static final int L11_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
