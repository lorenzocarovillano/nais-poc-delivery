package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.enums.Iabv0006CounterStd;
import it.accenture.jnais.ws.enums.Iabv0006GuideRowsRead;
import it.accenture.jnais.ws.occurs.Iabv0006EleErrori;

/**Original name: IABV0006-CUSTOM-COUNTERS<br>
 * Variable: IABV0006-CUSTOM-COUNTERS from copybook IABV0006<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Iabv0006CustomCounters {

    //==== PROPERTIES ====
    public static final int ELE_ERRORI_MAXOCCURS = 10;
    //Original name: IABV0006-COUNTER-STD
    private Iabv0006CounterStd counterStd = new Iabv0006CounterStd();
    //Original name: IABV0006-GUIDE-ROWS-READ
    private Iabv0006GuideRowsRead guideRowsRead = new Iabv0006GuideRowsRead();
    //Original name: IABV0006-LIM-CUSTOM-COUNT-MAX
    private short limCustomCountMax = ((short)10);
    //Original name: IABV0006-MAX-ELE-CUSTOM-COUNT
    private short maxEleCustomCount = DefaultValues.BIN_SHORT_VAL;
    //Original name: IABV0006-ELE-ERRORI
    private Iabv0006EleErrori[] eleErrori = new Iabv0006EleErrori[ELE_ERRORI_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Iabv0006CustomCounters() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int eleErroriIdx = 1; eleErroriIdx <= ELE_ERRORI_MAXOCCURS; eleErroriIdx++) {
            eleErrori[eleErroriIdx - 1] = new Iabv0006EleErrori();
        }
    }

    public void setCustomCountersBytes(byte[] buffer, int offset) {
        int position = offset;
        counterStd.setCounterStd(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        guideRowsRead.setGuideRowsRead(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        limCustomCountMax = MarshalByte.readShort(buffer, position, Len.LIM_CUSTOM_COUNT_MAX);
        position += Len.LIM_CUSTOM_COUNT_MAX;
        maxEleCustomCount = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= ELE_ERRORI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                eleErrori[idx - 1].setEleErroriBytes(buffer, position);
                position += Iabv0006EleErrori.Len.ELE_ERRORI;
            }
            else {
                eleErrori[idx - 1].initEleErroriSpaces();
                position += Iabv0006EleErrori.Len.ELE_ERRORI;
            }
        }
    }

    public byte[] getCustomCountersBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, counterStd.getCounterStd());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, guideRowsRead.getGuideRowsRead());
        position += Types.CHAR_SIZE;
        MarshalByte.writeShort(buffer, position, limCustomCountMax, Len.LIM_CUSTOM_COUNT_MAX);
        position += Len.LIM_CUSTOM_COUNT_MAX;
        MarshalByte.writeBinaryShort(buffer, position, maxEleCustomCount);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= ELE_ERRORI_MAXOCCURS; idx++) {
            eleErrori[idx - 1].getEleErroriBytes(buffer, position);
            position += Iabv0006EleErrori.Len.ELE_ERRORI;
        }
        return buffer;
    }

    public void setLimCustomCountMax(short limCustomCountMax) {
        this.limCustomCountMax = limCustomCountMax;
    }

    public short getLimCustomCountMax() {
        return this.limCustomCountMax;
    }

    public void setMaxEleCustomCount(short maxEleCustomCount) {
        this.maxEleCustomCount = maxEleCustomCount;
    }

    public short getMaxEleCustomCount() {
        return this.maxEleCustomCount;
    }

    public Iabv0006CounterStd getCounterStd() {
        return counterStd;
    }

    public Iabv0006EleErrori getEleErrori(int idx) {
        return eleErrori[idx - 1];
    }

    public Iabv0006GuideRowsRead getGuideRowsRead() {
        return guideRowsRead;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LIM_CUSTOM_COUNT_MAX = 4;
        public static final int MAX_ELE_CUSTOM_COUNT = 2;
        public static final int CUSTOM_COUNTERS = Iabv0006CounterStd.Len.COUNTER_STD + Iabv0006GuideRowsRead.Len.GUIDE_ROWS_READ + LIM_CUSTOM_COUNT_MAX + MAX_ELE_CUSTOM_COUNT + Iabv0006CustomCounters.ELE_ERRORI_MAXOCCURS * Iabv0006EleErrori.Len.ELE_ERRORI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
