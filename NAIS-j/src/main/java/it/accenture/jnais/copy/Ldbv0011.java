package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: LDBV0011<br>
 * Variable: LDBV0011 from copybook LDBV0011<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv0011 {

    //==== PROPERTIES ====
    //Original name: LDBV0011-ID-TIT-CONT
    private int titCont = DefaultValues.INT_VAL;
    //Original name: LDBV0011-ID-DETT-TIT-CONT
    private int dettTitCont = DefaultValues.INT_VAL;
    //Original name: LDBV0011-ID-POLI
    private int poli = DefaultValues.INT_VAL;
    //Original name: LDBV0011-ID-ADES
    private int ades = DefaultValues.INT_VAL;
    //Original name: LDBV0011-ID-TRCH-DI-GAR
    private int trchDiGar = DefaultValues.INT_VAL;
    //Original name: LDBV0011-ID-GAR
    private int gar = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbv0011Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV0011];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV0011);
        setLdbv0011Bytes(buffer, 1);
    }

    public String getLdbv0011Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv0011Bytes());
    }

    public byte[] getLdbv0011Bytes() {
        byte[] buffer = new byte[Len.LDBV0011];
        return getLdbv0011Bytes(buffer, 1);
    }

    public void setLdbv0011Bytes(byte[] buffer, int offset) {
        int position = offset;
        titCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TIT_CONT, 0);
        position += Len.TIT_CONT;
        dettTitCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DETT_TIT_CONT, 0);
        position += Len.DETT_TIT_CONT;
        poli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POLI, 0);
        position += Len.POLI;
        ades = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ADES, 0);
        position += Len.ADES;
        trchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TRCH_DI_GAR, 0);
        position += Len.TRCH_DI_GAR;
        gar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.GAR, 0);
    }

    public byte[] getLdbv0011Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, titCont, Len.Int.TIT_CONT, 0);
        position += Len.TIT_CONT;
        MarshalByte.writeIntAsPacked(buffer, position, dettTitCont, Len.Int.DETT_TIT_CONT, 0);
        position += Len.DETT_TIT_CONT;
        MarshalByte.writeIntAsPacked(buffer, position, poli, Len.Int.POLI, 0);
        position += Len.POLI;
        MarshalByte.writeIntAsPacked(buffer, position, ades, Len.Int.ADES, 0);
        position += Len.ADES;
        MarshalByte.writeIntAsPacked(buffer, position, trchDiGar, Len.Int.TRCH_DI_GAR, 0);
        position += Len.TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, gar, Len.Int.GAR, 0);
        return buffer;
    }

    public void setTitCont(int titCont) {
        this.titCont = titCont;
    }

    public int getTitCont() {
        return this.titCont;
    }

    public void setDettTitCont(int dettTitCont) {
        this.dettTitCont = dettTitCont;
    }

    public int getDettTitCont() {
        return this.dettTitCont;
    }

    public void setPoli(int poli) {
        this.poli = poli;
    }

    public int getPoli() {
        return this.poli;
    }

    public void setAdes(int ades) {
        this.ades = ades;
    }

    public int getAdes() {
        return this.ades;
    }

    public void setTrchDiGar(int trchDiGar) {
        this.trchDiGar = trchDiGar;
    }

    public int getTrchDiGar() {
        return this.trchDiGar;
    }

    public void setGar(int gar) {
        this.gar = gar;
    }

    public int getGar() {
        return this.gar;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_CONT = 5;
        public static final int DETT_TIT_CONT = 5;
        public static final int POLI = 5;
        public static final int ADES = 5;
        public static final int TRCH_DI_GAR = 5;
        public static final int GAR = 5;
        public static final int LDBV0011 = TIT_CONT + DETT_TIT_CONT + POLI + ADES + TRCH_DI_GAR + GAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_CONT = 9;
            public static final int DETT_TIT_CONT = 9;
            public static final int POLI = 9;
            public static final int ADES = 9;
            public static final int TRCH_DI_GAR = 9;
            public static final int GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
