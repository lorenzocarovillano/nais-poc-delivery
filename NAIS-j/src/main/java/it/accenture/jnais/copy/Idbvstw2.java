package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDBVSTW2<br>
 * Copybook: IDBVSTW2 from copybook IDBVSTW2<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvstw2 {

    //==== PROPERTIES ====
    //Original name: IND-STW-ID-MOVI-CHIU
    private short stwIdMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-STW-FL-STAT-END
    private short stwFlStatEnd = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setStwIdMoviChiu(short stwIdMoviChiu) {
        this.stwIdMoviChiu = stwIdMoviChiu;
    }

    public short getStwIdMoviChiu() {
        return this.stwIdMoviChiu;
    }

    public void setStwFlStatEnd(short stwFlStatEnd) {
        this.stwFlStatEnd = stwFlStatEnd;
    }

    public short getStwFlStatEnd() {
        return this.stwFlStatEnd;
    }
}
