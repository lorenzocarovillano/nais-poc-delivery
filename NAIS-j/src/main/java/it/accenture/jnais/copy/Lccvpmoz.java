package it.accenture.jnais.copy;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LCCVPMOZ<br>
 * Copybook: LCCVPMOZ from copybook LCCVPMOZ<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvpmoz {

    //==== PROPERTIES ====
    //Original name: WK-PMO-MAX-A
    private short maxA = ((short)50);
    //Original name: WK-PMO-MAX-B
    private short maxB = ((short)100);

    //==== METHODS ====
    public short getMaxA() {
        return this.maxA;
    }

    public short getMaxB() {
        return this.maxB;
    }
}
