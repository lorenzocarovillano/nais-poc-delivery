package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LCCVB038<br>
 * Copybook: LCCVB038 from copybook LCCVB038<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvb038 {

    //==== PROPERTIES ====
    //Original name: W-B03-LEN-TOT
    private short lenTot = DefaultValues.BIN_SHORT_VAL;
    //Original name: W-B03-REC-FISSO
    private WB03RecFisso recFisso = new WB03RecFisso();

    //==== METHODS ====
    public void setLenTot(short lenTot) {
        this.lenTot = lenTot;
    }

    public short getLenTot() {
        return this.lenTot;
    }

    public WB03RecFisso getRecFisso() {
        return recFisso;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LEN_TOT = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
