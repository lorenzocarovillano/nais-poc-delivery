package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVSPG3<br>
 * Copybook: IDBVSPG3 from copybook IDBVSPG3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvspg3 {

    //==== PROPERTIES ====
    //Original name: SPG-DT-INI-EFF-DB
    private String spgDtIniEffDb = DefaultValues.stringVal(Len.SPG_DT_INI_EFF_DB);
    //Original name: SPG-DT-END-EFF-DB
    private String spgDtEndEffDb = DefaultValues.stringVal(Len.SPG_DT_END_EFF_DB);

    //==== METHODS ====
    public void setSpgDtIniEffDb(String spgDtIniEffDb) {
        this.spgDtIniEffDb = Functions.subString(spgDtIniEffDb, Len.SPG_DT_INI_EFF_DB);
    }

    public String getSpgDtIniEffDb() {
        return this.spgDtIniEffDb;
    }

    public void setSpgDtEndEffDb(String spgDtEndEffDb) {
        this.spgDtEndEffDb = Functions.subString(spgDtEndEffDb, Len.SPG_DT_END_EFF_DB);
    }

    public String getSpgDtEndEffDb() {
        return this.spgDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int SPG_DT_INI_EFF_DB = 10;
        public static final int SPG_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
