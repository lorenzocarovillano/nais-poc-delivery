package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVTGA1<br>
 * Variable: LCCVTGA1 from copybook LCCVTGA1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvtga1Ivvs0216 implements ICopyable<Lccvtga1Ivvs0216> {

    //==== PROPERTIES ====
    /**Original name: W1TGA-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA TRCH_DI_GAR
	 *    ALIAS TGA
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: W1TGA-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: W1TGA-DATI
    private WtgaDati dati = new WtgaDati();

    //==== CONSTRUCTORS ====
    public Lccvtga1Ivvs0216() {
    }

    public Lccvtga1Ivvs0216(Lccvtga1Ivvs0216 lccvtga1) {
        this();
        this.status.setStatus(lccvtga1.status.getStatus());
        this.idPtf = lccvtga1.idPtf;
        this.dati = lccvtga1.dati.copy();
    }

    //==== METHODS ====
    public void initLccvtga1Spaces() {
        status.setStatus(Types.SPACE_CHAR);
        idPtf = Types.INVALID_INT_VAL;
        dati.initDatiSpaces();
    }

    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WtgaDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    public Lccvtga1Ivvs0216 copy() {
        return new Lccvtga1Ivvs0216(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
