package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV7851<br>
 * Variable: LDBV7851 from copybook LDBV7851<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv7851 {

    //==== PROPERTIES ====
    //Original name: LDBV7851-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV7851-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV7851-TP-MOVI-01
    private int tpMovi01 = DefaultValues.INT_VAL;
    //Original name: LDBV7851-TP-MOVI-02
    private int tpMovi02 = DefaultValues.INT_VAL;
    //Original name: LDBV7851-TP-MOVI-03
    private int tpMovi03 = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbv7851Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV7851];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV7851);
        setLdbv7851Bytes(buffer, 1);
    }

    public String getLdbv7851Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv7851Bytes());
    }

    public byte[] getLdbv7851Bytes() {
        byte[] buffer = new byte[Len.LDBV7851];
        return getLdbv7851Bytes(buffer, 1);
    }

    public void setLdbv7851Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpMovi01 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI01, 0);
        position += Len.TP_MOVI01;
        tpMovi02 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI02, 0);
        position += Len.TP_MOVI02;
        tpMovi03 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI03, 0);
    }

    public byte[] getLdbv7851Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi01, Len.Int.TP_MOVI01, 0);
        position += Len.TP_MOVI01;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi02, Len.Int.TP_MOVI02, 0);
        position += Len.TP_MOVI02;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi03, Len.Int.TP_MOVI03, 0);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpMovi01(int tpMovi01) {
        this.tpMovi01 = tpMovi01;
    }

    public int getTpMovi01() {
        return this.tpMovi01;
    }

    public void setTpMovi02(int tpMovi02) {
        this.tpMovi02 = tpMovi02;
    }

    public int getTpMovi02() {
        return this.tpMovi02;
    }

    public void setTpMovi03(int tpMovi03) {
        this.tpMovi03 = tpMovi03;
    }

    public int getTpMovi03() {
        return this.tpMovi03;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int ID_OGG = 5;
        public static final int TP_MOVI01 = 3;
        public static final int TP_MOVI02 = 3;
        public static final int TP_MOVI03 = 3;
        public static final int LDBV7851 = ID_OGG + TP_OGG + TP_MOVI01 + TP_MOVI02 + TP_MOVI03;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int TP_MOVI01 = 5;
            public static final int TP_MOVI02 = 5;
            public static final int TP_MOVI03 = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
