package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-LIQ<br>
 * Variable: IND-LIQ from copybook IDBVLQU2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndLiq {

    //==== PROPERTIES ====
    //Original name: IND-LQU-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IB-OGG
    private short ibOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-DESC-CAU-EVE-SIN
    private short descCauEveSin = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-COD-CAU-SIN
    private short codCauSin = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-COD-SIN-CATSTRF
    private short codSinCatstrf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-DT-MOR
    private short dtMor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-DT-DEN
    private short dtDen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-DT-PERV-DEN
    private short dtPervDen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-DT-RICH
    private short dtRich = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TP-SIN
    private short tpSin = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TP-RISC
    private short tpRisc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TP-MET-RISC
    private short tpMetRisc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-DT-LIQ
    private short dtLiq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-COD-DVS
    private short codDvs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMP-LRD-LIQTO
    private short totImpLrdLiqto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMP-PREST
    private short totImpPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMP-INTR-PREST
    private short totImpIntrPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMP-UTI
    private short totImpUti = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMP-RIT-TFR
    private short totImpRitTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMP-RIT-ACC
    private short totImpRitAcc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMP-RIT-VIS
    private short totImpRitVis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMPB-TFR
    private short totImpbTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMPB-ACC
    private short totImpbAcc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMPB-VIS
    private short totImpbVis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMP-RIMB
    private short totImpRimb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-IMPST-PRVR
    private short impbImpstPrvr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-PRVR
    private short impstPrvr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-IMPST-252
    private short impbImpst252 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-252
    private short impst252 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMP-IS
    private short totImpIs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-DIR-LIQ
    private short impDirLiq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IMP-NET-LIQTO
    private short totImpNetLiqto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-MONT-END2000
    private short montEnd2000 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-MONT-END2006
    private short montEnd2006 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-PC-REN
    private short pcRen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-PNL
    private short impPnl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-IRPEF
    private short impbIrpef = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-IRPEF
    private short impstIrpef = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-DT-VLT
    private short dtVlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-DT-END-ISTR
    private short dtEndIstr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-SPE-RCS
    private short speRcs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IB-LIQ
    private short ibLiq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IAS-ONER-PRVNT
    private short totIasOnerPrvnt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IAS-MGG-SIN
    private short totIasMggSin = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IAS-RST-DPST
    private short totIasRstDpst = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-ONER-LIQ
    private short impOnerLiq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-COMPON-TAX-RIMB
    private short componTaxRimb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TP-MEZ-PAG
    private short tpMezPag = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-EXCONTR
    private short impExcontr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-INTR-RIT-PAG
    private short impIntrRitPag = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-BNS-NON-GODUTO
    private short bnsNonGoduto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-CNBT-INPSTFM
    private short cnbtInpstfm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-DA-RIMB
    private short impstDaRimb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-IS
    private short impbIs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TAX-SEP
    private short taxSep = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-TAX-SEP
    private short impbTaxSep = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-INTR-SU-PREST
    private short impbIntrSuPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-ADDIZ-COMUN
    private short addizComun = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-ADDIZ-COMUN
    private short impbAddizComun = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-ADDIZ-REGION
    private short addizRegion = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-ADDIZ-REGION
    private short impbAddizRegion = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-MONT-DAL2007
    private short montDal2007 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-CNBT-INPSTFM
    private short impbCnbtInpstfm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-LRD-DA-RIMB
    private short impLrdDaRimb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-DIR-DA-RIMB
    private short impDirDaRimb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-RIS-MAT
    private short risMat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-RIS-SPE
    private short risSpe = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TOT-IAS-PNL
    private short totIasPnl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-FL-EVE-GARTO
    private short flEveGarto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-REN-K1
    private short impRenK1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-REN-K2
    private short impRenK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-REN-K3
    private short impRenK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-PC-REN-K1
    private short pcRenK1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-PC-REN-K2
    private short pcRenK2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-PC-REN-K3
    private short pcRenK3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-TP-CAUS-ANTIC
    private short tpCausAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-LRD-LIQTO-RILT
    private short impLrdLiqtoRilt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-APPL-RILT
    private short impstApplRilt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-PC-RISC-PARZ
    private short pcRiscParz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-BOLLO-TOT-V
    private short impstBolloTotV = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-BOLLO-DETT-C
    private short impstBolloDettC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-BOLLO-TOT-SW
    private short impstBolloTotSw = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-BOLLO-TOT-AA
    private short impstBolloTotAa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-VIS-1382011
    private short impbVis1382011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-VIS-1382011
    private short impstVis1382011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-IS-1382011
    private short impbIs1382011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-SOST-1382011
    private short impstSost1382011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-PC-ABB-TIT-STAT
    private short pcAbbTitStat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-BOLLO-DETT-C
    private short impbBolloDettC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-FL-PRE-COMP
    private short flPreComp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-VIS-662014
    private short impbVis662014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-VIS-662014
    private short impstVis662014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPB-IS-662014
    private short impbIs662014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMPST-SOST-662014
    private short impstSost662014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-PC-ABB-TS-662014
    private short pcAbbTs662014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-IMP-LRD-CALC-CP
    private short impLrdCalcCp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-LQU-COS-TUNNEL-USCITA
    private short cosTunnelUscita = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setIbOgg(short ibOgg) {
        this.ibOgg = ibOgg;
    }

    public short getIbOgg() {
        return this.ibOgg;
    }

    public void setDescCauEveSin(short descCauEveSin) {
        this.descCauEveSin = descCauEveSin;
    }

    public short getDescCauEveSin() {
        return this.descCauEveSin;
    }

    public void setCodCauSin(short codCauSin) {
        this.codCauSin = codCauSin;
    }

    public short getCodCauSin() {
        return this.codCauSin;
    }

    public void setCodSinCatstrf(short codSinCatstrf) {
        this.codSinCatstrf = codSinCatstrf;
    }

    public short getCodSinCatstrf() {
        return this.codSinCatstrf;
    }

    public void setDtMor(short dtMor) {
        this.dtMor = dtMor;
    }

    public short getDtMor() {
        return this.dtMor;
    }

    public void setDtDen(short dtDen) {
        this.dtDen = dtDen;
    }

    public short getDtDen() {
        return this.dtDen;
    }

    public void setDtPervDen(short dtPervDen) {
        this.dtPervDen = dtPervDen;
    }

    public short getDtPervDen() {
        return this.dtPervDen;
    }

    public void setDtRich(short dtRich) {
        this.dtRich = dtRich;
    }

    public short getDtRich() {
        return this.dtRich;
    }

    public void setTpSin(short tpSin) {
        this.tpSin = tpSin;
    }

    public short getTpSin() {
        return this.tpSin;
    }

    public void setTpRisc(short tpRisc) {
        this.tpRisc = tpRisc;
    }

    public short getTpRisc() {
        return this.tpRisc;
    }

    public void setTpMetRisc(short tpMetRisc) {
        this.tpMetRisc = tpMetRisc;
    }

    public short getTpMetRisc() {
        return this.tpMetRisc;
    }

    public void setDtLiq(short dtLiq) {
        this.dtLiq = dtLiq;
    }

    public short getDtLiq() {
        return this.dtLiq;
    }

    public void setCodDvs(short codDvs) {
        this.codDvs = codDvs;
    }

    public short getCodDvs() {
        return this.codDvs;
    }

    public void setTotImpLrdLiqto(short totImpLrdLiqto) {
        this.totImpLrdLiqto = totImpLrdLiqto;
    }

    public short getTotImpLrdLiqto() {
        return this.totImpLrdLiqto;
    }

    public void setTotImpPrest(short totImpPrest) {
        this.totImpPrest = totImpPrest;
    }

    public short getTotImpPrest() {
        return this.totImpPrest;
    }

    public void setTotImpIntrPrest(short totImpIntrPrest) {
        this.totImpIntrPrest = totImpIntrPrest;
    }

    public short getTotImpIntrPrest() {
        return this.totImpIntrPrest;
    }

    public void setTotImpUti(short totImpUti) {
        this.totImpUti = totImpUti;
    }

    public short getTotImpUti() {
        return this.totImpUti;
    }

    public void setTotImpRitTfr(short totImpRitTfr) {
        this.totImpRitTfr = totImpRitTfr;
    }

    public short getTotImpRitTfr() {
        return this.totImpRitTfr;
    }

    public void setTotImpRitAcc(short totImpRitAcc) {
        this.totImpRitAcc = totImpRitAcc;
    }

    public short getTotImpRitAcc() {
        return this.totImpRitAcc;
    }

    public void setTotImpRitVis(short totImpRitVis) {
        this.totImpRitVis = totImpRitVis;
    }

    public short getTotImpRitVis() {
        return this.totImpRitVis;
    }

    public void setTotImpbTfr(short totImpbTfr) {
        this.totImpbTfr = totImpbTfr;
    }

    public short getTotImpbTfr() {
        return this.totImpbTfr;
    }

    public void setTotImpbAcc(short totImpbAcc) {
        this.totImpbAcc = totImpbAcc;
    }

    public short getTotImpbAcc() {
        return this.totImpbAcc;
    }

    public void setTotImpbVis(short totImpbVis) {
        this.totImpbVis = totImpbVis;
    }

    public short getTotImpbVis() {
        return this.totImpbVis;
    }

    public void setTotImpRimb(short totImpRimb) {
        this.totImpRimb = totImpRimb;
    }

    public short getTotImpRimb() {
        return this.totImpRimb;
    }

    public void setImpbImpstPrvr(short impbImpstPrvr) {
        this.impbImpstPrvr = impbImpstPrvr;
    }

    public short getImpbImpstPrvr() {
        return this.impbImpstPrvr;
    }

    public void setImpstPrvr(short impstPrvr) {
        this.impstPrvr = impstPrvr;
    }

    public short getImpstPrvr() {
        return this.impstPrvr;
    }

    public void setImpbImpst252(short impbImpst252) {
        this.impbImpst252 = impbImpst252;
    }

    public short getImpbImpst252() {
        return this.impbImpst252;
    }

    public void setImpst252(short impst252) {
        this.impst252 = impst252;
    }

    public short getImpst252() {
        return this.impst252;
    }

    public void setTotImpIs(short totImpIs) {
        this.totImpIs = totImpIs;
    }

    public short getTotImpIs() {
        return this.totImpIs;
    }

    public void setImpDirLiq(short impDirLiq) {
        this.impDirLiq = impDirLiq;
    }

    public short getImpDirLiq() {
        return this.impDirLiq;
    }

    public void setTotImpNetLiqto(short totImpNetLiqto) {
        this.totImpNetLiqto = totImpNetLiqto;
    }

    public short getTotImpNetLiqto() {
        return this.totImpNetLiqto;
    }

    public void setMontEnd2000(short montEnd2000) {
        this.montEnd2000 = montEnd2000;
    }

    public short getMontEnd2000() {
        return this.montEnd2000;
    }

    public void setMontEnd2006(short montEnd2006) {
        this.montEnd2006 = montEnd2006;
    }

    public short getMontEnd2006() {
        return this.montEnd2006;
    }

    public void setPcRen(short pcRen) {
        this.pcRen = pcRen;
    }

    public short getPcRen() {
        return this.pcRen;
    }

    public void setImpPnl(short impPnl) {
        this.impPnl = impPnl;
    }

    public short getImpPnl() {
        return this.impPnl;
    }

    public void setImpbIrpef(short impbIrpef) {
        this.impbIrpef = impbIrpef;
    }

    public short getImpbIrpef() {
        return this.impbIrpef;
    }

    public void setImpstIrpef(short impstIrpef) {
        this.impstIrpef = impstIrpef;
    }

    public short getImpstIrpef() {
        return this.impstIrpef;
    }

    public void setDtVlt(short dtVlt) {
        this.dtVlt = dtVlt;
    }

    public short getDtVlt() {
        return this.dtVlt;
    }

    public void setDtEndIstr(short dtEndIstr) {
        this.dtEndIstr = dtEndIstr;
    }

    public short getDtEndIstr() {
        return this.dtEndIstr;
    }

    public void setSpeRcs(short speRcs) {
        this.speRcs = speRcs;
    }

    public short getSpeRcs() {
        return this.speRcs;
    }

    public void setIbLiq(short ibLiq) {
        this.ibLiq = ibLiq;
    }

    public short getIbLiq() {
        return this.ibLiq;
    }

    public void setTotIasOnerPrvnt(short totIasOnerPrvnt) {
        this.totIasOnerPrvnt = totIasOnerPrvnt;
    }

    public short getTotIasOnerPrvnt() {
        return this.totIasOnerPrvnt;
    }

    public void setTotIasMggSin(short totIasMggSin) {
        this.totIasMggSin = totIasMggSin;
    }

    public short getTotIasMggSin() {
        return this.totIasMggSin;
    }

    public void setTotIasRstDpst(short totIasRstDpst) {
        this.totIasRstDpst = totIasRstDpst;
    }

    public short getTotIasRstDpst() {
        return this.totIasRstDpst;
    }

    public void setImpOnerLiq(short impOnerLiq) {
        this.impOnerLiq = impOnerLiq;
    }

    public short getImpOnerLiq() {
        return this.impOnerLiq;
    }

    public void setComponTaxRimb(short componTaxRimb) {
        this.componTaxRimb = componTaxRimb;
    }

    public short getComponTaxRimb() {
        return this.componTaxRimb;
    }

    public void setTpMezPag(short tpMezPag) {
        this.tpMezPag = tpMezPag;
    }

    public short getTpMezPag() {
        return this.tpMezPag;
    }

    public void setImpExcontr(short impExcontr) {
        this.impExcontr = impExcontr;
    }

    public short getImpExcontr() {
        return this.impExcontr;
    }

    public void setImpIntrRitPag(short impIntrRitPag) {
        this.impIntrRitPag = impIntrRitPag;
    }

    public short getImpIntrRitPag() {
        return this.impIntrRitPag;
    }

    public void setBnsNonGoduto(short bnsNonGoduto) {
        this.bnsNonGoduto = bnsNonGoduto;
    }

    public short getBnsNonGoduto() {
        return this.bnsNonGoduto;
    }

    public void setCnbtInpstfm(short cnbtInpstfm) {
        this.cnbtInpstfm = cnbtInpstfm;
    }

    public short getCnbtInpstfm() {
        return this.cnbtInpstfm;
    }

    public void setImpstDaRimb(short impstDaRimb) {
        this.impstDaRimb = impstDaRimb;
    }

    public short getImpstDaRimb() {
        return this.impstDaRimb;
    }

    public void setImpbIs(short impbIs) {
        this.impbIs = impbIs;
    }

    public short getImpbIs() {
        return this.impbIs;
    }

    public void setTaxSep(short taxSep) {
        this.taxSep = taxSep;
    }

    public short getTaxSep() {
        return this.taxSep;
    }

    public void setImpbTaxSep(short impbTaxSep) {
        this.impbTaxSep = impbTaxSep;
    }

    public short getImpbTaxSep() {
        return this.impbTaxSep;
    }

    public void setImpbIntrSuPrest(short impbIntrSuPrest) {
        this.impbIntrSuPrest = impbIntrSuPrest;
    }

    public short getImpbIntrSuPrest() {
        return this.impbIntrSuPrest;
    }

    public void setAddizComun(short addizComun) {
        this.addizComun = addizComun;
    }

    public short getAddizComun() {
        return this.addizComun;
    }

    public void setImpbAddizComun(short impbAddizComun) {
        this.impbAddizComun = impbAddizComun;
    }

    public short getImpbAddizComun() {
        return this.impbAddizComun;
    }

    public void setAddizRegion(short addizRegion) {
        this.addizRegion = addizRegion;
    }

    public short getAddizRegion() {
        return this.addizRegion;
    }

    public void setImpbAddizRegion(short impbAddizRegion) {
        this.impbAddizRegion = impbAddizRegion;
    }

    public short getImpbAddizRegion() {
        return this.impbAddizRegion;
    }

    public void setMontDal2007(short montDal2007) {
        this.montDal2007 = montDal2007;
    }

    public short getMontDal2007() {
        return this.montDal2007;
    }

    public void setImpbCnbtInpstfm(short impbCnbtInpstfm) {
        this.impbCnbtInpstfm = impbCnbtInpstfm;
    }

    public short getImpbCnbtInpstfm() {
        return this.impbCnbtInpstfm;
    }

    public void setImpLrdDaRimb(short impLrdDaRimb) {
        this.impLrdDaRimb = impLrdDaRimb;
    }

    public short getImpLrdDaRimb() {
        return this.impLrdDaRimb;
    }

    public void setImpDirDaRimb(short impDirDaRimb) {
        this.impDirDaRimb = impDirDaRimb;
    }

    public short getImpDirDaRimb() {
        return this.impDirDaRimb;
    }

    public void setRisMat(short risMat) {
        this.risMat = risMat;
    }

    public short getRisMat() {
        return this.risMat;
    }

    public void setRisSpe(short risSpe) {
        this.risSpe = risSpe;
    }

    public short getRisSpe() {
        return this.risSpe;
    }

    public void setTotIasPnl(short totIasPnl) {
        this.totIasPnl = totIasPnl;
    }

    public short getTotIasPnl() {
        return this.totIasPnl;
    }

    public void setFlEveGarto(short flEveGarto) {
        this.flEveGarto = flEveGarto;
    }

    public short getFlEveGarto() {
        return this.flEveGarto;
    }

    public void setImpRenK1(short impRenK1) {
        this.impRenK1 = impRenK1;
    }

    public short getImpRenK1() {
        return this.impRenK1;
    }

    public void setImpRenK2(short impRenK2) {
        this.impRenK2 = impRenK2;
    }

    public short getImpRenK2() {
        return this.impRenK2;
    }

    public void setImpRenK3(short impRenK3) {
        this.impRenK3 = impRenK3;
    }

    public short getImpRenK3() {
        return this.impRenK3;
    }

    public void setPcRenK1(short pcRenK1) {
        this.pcRenK1 = pcRenK1;
    }

    public short getPcRenK1() {
        return this.pcRenK1;
    }

    public void setPcRenK2(short pcRenK2) {
        this.pcRenK2 = pcRenK2;
    }

    public short getPcRenK2() {
        return this.pcRenK2;
    }

    public void setPcRenK3(short pcRenK3) {
        this.pcRenK3 = pcRenK3;
    }

    public short getPcRenK3() {
        return this.pcRenK3;
    }

    public void setTpCausAntic(short tpCausAntic) {
        this.tpCausAntic = tpCausAntic;
    }

    public short getTpCausAntic() {
        return this.tpCausAntic;
    }

    public void setImpLrdLiqtoRilt(short impLrdLiqtoRilt) {
        this.impLrdLiqtoRilt = impLrdLiqtoRilt;
    }

    public short getImpLrdLiqtoRilt() {
        return this.impLrdLiqtoRilt;
    }

    public void setImpstApplRilt(short impstApplRilt) {
        this.impstApplRilt = impstApplRilt;
    }

    public short getImpstApplRilt() {
        return this.impstApplRilt;
    }

    public void setPcRiscParz(short pcRiscParz) {
        this.pcRiscParz = pcRiscParz;
    }

    public short getPcRiscParz() {
        return this.pcRiscParz;
    }

    public void setImpstBolloTotV(short impstBolloTotV) {
        this.impstBolloTotV = impstBolloTotV;
    }

    public short getImpstBolloTotV() {
        return this.impstBolloTotV;
    }

    public void setImpstBolloDettC(short impstBolloDettC) {
        this.impstBolloDettC = impstBolloDettC;
    }

    public short getImpstBolloDettC() {
        return this.impstBolloDettC;
    }

    public void setImpstBolloTotSw(short impstBolloTotSw) {
        this.impstBolloTotSw = impstBolloTotSw;
    }

    public short getImpstBolloTotSw() {
        return this.impstBolloTotSw;
    }

    public void setImpstBolloTotAa(short impstBolloTotAa) {
        this.impstBolloTotAa = impstBolloTotAa;
    }

    public short getImpstBolloTotAa() {
        return this.impstBolloTotAa;
    }

    public void setImpbVis1382011(short impbVis1382011) {
        this.impbVis1382011 = impbVis1382011;
    }

    public short getImpbVis1382011() {
        return this.impbVis1382011;
    }

    public void setImpstVis1382011(short impstVis1382011) {
        this.impstVis1382011 = impstVis1382011;
    }

    public short getImpstVis1382011() {
        return this.impstVis1382011;
    }

    public void setImpbIs1382011(short impbIs1382011) {
        this.impbIs1382011 = impbIs1382011;
    }

    public short getImpbIs1382011() {
        return this.impbIs1382011;
    }

    public void setImpstSost1382011(short impstSost1382011) {
        this.impstSost1382011 = impstSost1382011;
    }

    public short getImpstSost1382011() {
        return this.impstSost1382011;
    }

    public void setPcAbbTitStat(short pcAbbTitStat) {
        this.pcAbbTitStat = pcAbbTitStat;
    }

    public short getPcAbbTitStat() {
        return this.pcAbbTitStat;
    }

    public void setImpbBolloDettC(short impbBolloDettC) {
        this.impbBolloDettC = impbBolloDettC;
    }

    public short getImpbBolloDettC() {
        return this.impbBolloDettC;
    }

    public void setFlPreComp(short flPreComp) {
        this.flPreComp = flPreComp;
    }

    public short getFlPreComp() {
        return this.flPreComp;
    }

    public void setImpbVis662014(short impbVis662014) {
        this.impbVis662014 = impbVis662014;
    }

    public short getImpbVis662014() {
        return this.impbVis662014;
    }

    public void setImpstVis662014(short impstVis662014) {
        this.impstVis662014 = impstVis662014;
    }

    public short getImpstVis662014() {
        return this.impstVis662014;
    }

    public void setImpbIs662014(short impbIs662014) {
        this.impbIs662014 = impbIs662014;
    }

    public short getImpbIs662014() {
        return this.impbIs662014;
    }

    public void setImpstSost662014(short impstSost662014) {
        this.impstSost662014 = impstSost662014;
    }

    public short getImpstSost662014() {
        return this.impstSost662014;
    }

    public void setPcAbbTs662014(short pcAbbTs662014) {
        this.pcAbbTs662014 = pcAbbTs662014;
    }

    public short getPcAbbTs662014() {
        return this.pcAbbTs662014;
    }

    public void setImpLrdCalcCp(short impLrdCalcCp) {
        this.impLrdCalcCp = impLrdCalcCp;
    }

    public short getImpLrdCalcCp() {
        return this.impLrdCalcCp;
    }

    public void setCosTunnelUscita(short cosTunnelUscita) {
        this.cosTunnelUscita = cosTunnelUscita;
    }

    public short getCosTunnelUscita() {
        return this.cosTunnelUscita;
    }
}
