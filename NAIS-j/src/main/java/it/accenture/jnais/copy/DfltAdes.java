package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.DadDtDecorDflt;
import it.accenture.jnais.ws.redefines.DadDtScadAdesDflt;
import it.accenture.jnais.ws.redefines.DadDurAaAdesDflt;
import it.accenture.jnais.ws.redefines.DadDurGgAdesDflt;
import it.accenture.jnais.ws.redefines.DadDurMmAdesDflt;
import it.accenture.jnais.ws.redefines.DadEtaScadFemmDflt;
import it.accenture.jnais.ws.redefines.DadEtaScadMascDflt;
import it.accenture.jnais.ws.redefines.DadFrazDflt;
import it.accenture.jnais.ws.redefines.DadImpAder;
import it.accenture.jnais.ws.redefines.DadImpAz;
import it.accenture.jnais.ws.redefines.DadImpPreDflt;
import it.accenture.jnais.ws.redefines.DadImpProvIncDflt;
import it.accenture.jnais.ws.redefines.DadImpTfr;
import it.accenture.jnais.ws.redefines.DadImpVolo;
import it.accenture.jnais.ws.redefines.DadPcAder;
import it.accenture.jnais.ws.redefines.DadPcAz;
import it.accenture.jnais.ws.redefines.DadPcProvIncDflt;
import it.accenture.jnais.ws.redefines.DadPcTfr;
import it.accenture.jnais.ws.redefines.DadPcVolo;

/**Original name: DFLT-ADES<br>
 * Variable: DFLT-ADES from copybook IDBVDAD1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DfltAdes {

    //==== PROPERTIES ====
    //Original name: DAD-ID-DFLT-ADES
    private int dadIdDfltAdes = DefaultValues.INT_VAL;
    //Original name: DAD-ID-POLI
    private int dadIdPoli = DefaultValues.INT_VAL;
    //Original name: DAD-IB-POLI
    private String dadIbPoli = DefaultValues.stringVal(Len.DAD_IB_POLI);
    //Original name: DAD-IB-DFLT
    private String dadIbDflt = DefaultValues.stringVal(Len.DAD_IB_DFLT);
    //Original name: DAD-COD-GAR-1
    private String dadCodGar1 = DefaultValues.stringVal(Len.DAD_COD_GAR1);
    //Original name: DAD-COD-GAR-2
    private String dadCodGar2 = DefaultValues.stringVal(Len.DAD_COD_GAR2);
    //Original name: DAD-COD-GAR-3
    private String dadCodGar3 = DefaultValues.stringVal(Len.DAD_COD_GAR3);
    //Original name: DAD-COD-GAR-4
    private String dadCodGar4 = DefaultValues.stringVal(Len.DAD_COD_GAR4);
    //Original name: DAD-COD-GAR-5
    private String dadCodGar5 = DefaultValues.stringVal(Len.DAD_COD_GAR5);
    //Original name: DAD-COD-GAR-6
    private String dadCodGar6 = DefaultValues.stringVal(Len.DAD_COD_GAR6);
    //Original name: DAD-DT-DECOR-DFLT
    private DadDtDecorDflt dadDtDecorDflt = new DadDtDecorDflt();
    //Original name: DAD-ETA-SCAD-MASC-DFLT
    private DadEtaScadMascDflt dadEtaScadMascDflt = new DadEtaScadMascDflt();
    //Original name: DAD-ETA-SCAD-FEMM-DFLT
    private DadEtaScadFemmDflt dadEtaScadFemmDflt = new DadEtaScadFemmDflt();
    //Original name: DAD-DUR-AA-ADES-DFLT
    private DadDurAaAdesDflt dadDurAaAdesDflt = new DadDurAaAdesDflt();
    //Original name: DAD-DUR-MM-ADES-DFLT
    private DadDurMmAdesDflt dadDurMmAdesDflt = new DadDurMmAdesDflt();
    //Original name: DAD-DUR-GG-ADES-DFLT
    private DadDurGgAdesDflt dadDurGgAdesDflt = new DadDurGgAdesDflt();
    //Original name: DAD-DT-SCAD-ADES-DFLT
    private DadDtScadAdesDflt dadDtScadAdesDflt = new DadDtScadAdesDflt();
    //Original name: DAD-FRAZ-DFLT
    private DadFrazDflt dadFrazDflt = new DadFrazDflt();
    //Original name: DAD-PC-PROV-INC-DFLT
    private DadPcProvIncDflt dadPcProvIncDflt = new DadPcProvIncDflt();
    //Original name: DAD-IMP-PROV-INC-DFLT
    private DadImpProvIncDflt dadImpProvIncDflt = new DadImpProvIncDflt();
    //Original name: DAD-IMP-AZ
    private DadImpAz dadImpAz = new DadImpAz();
    //Original name: DAD-IMP-ADER
    private DadImpAder dadImpAder = new DadImpAder();
    //Original name: DAD-IMP-TFR
    private DadImpTfr dadImpTfr = new DadImpTfr();
    //Original name: DAD-IMP-VOLO
    private DadImpVolo dadImpVolo = new DadImpVolo();
    //Original name: DAD-PC-AZ
    private DadPcAz dadPcAz = new DadPcAz();
    //Original name: DAD-PC-ADER
    private DadPcAder dadPcAder = new DadPcAder();
    //Original name: DAD-PC-TFR
    private DadPcTfr dadPcTfr = new DadPcTfr();
    //Original name: DAD-PC-VOLO
    private DadPcVolo dadPcVolo = new DadPcVolo();
    //Original name: DAD-TP-FNT-CNBTVA
    private String dadTpFntCnbtva = DefaultValues.stringVal(Len.DAD_TP_FNT_CNBTVA);
    //Original name: DAD-IMP-PRE-DFLT
    private DadImpPreDflt dadImpPreDflt = new DadImpPreDflt();
    //Original name: DAD-COD-COMP-ANIA
    private int dadCodCompAnia = DefaultValues.INT_VAL;
    //Original name: DAD-TP-PRE
    private char dadTpPre = DefaultValues.CHAR_VAL;
    //Original name: DAD-DS-OPER-SQL
    private char dadDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: DAD-DS-VER
    private int dadDsVer = DefaultValues.INT_VAL;
    //Original name: DAD-DS-TS-CPTZ
    private long dadDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: DAD-DS-UTENTE
    private String dadDsUtente = DefaultValues.stringVal(Len.DAD_DS_UTENTE);
    //Original name: DAD-DS-STATO-ELAB
    private char dadDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setDfltAdesFormatted(String data) {
        byte[] buffer = new byte[Len.DFLT_ADES];
        MarshalByte.writeString(buffer, 1, data, Len.DFLT_ADES);
        setDfltAdesBytes(buffer, 1);
    }

    public void setDfltAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        dadIdDfltAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DAD_ID_DFLT_ADES, 0);
        position += Len.DAD_ID_DFLT_ADES;
        dadIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DAD_ID_POLI, 0);
        position += Len.DAD_ID_POLI;
        dadIbPoli = MarshalByte.readString(buffer, position, Len.DAD_IB_POLI);
        position += Len.DAD_IB_POLI;
        dadIbDflt = MarshalByte.readString(buffer, position, Len.DAD_IB_DFLT);
        position += Len.DAD_IB_DFLT;
        dadCodGar1 = MarshalByte.readString(buffer, position, Len.DAD_COD_GAR1);
        position += Len.DAD_COD_GAR1;
        dadCodGar2 = MarshalByte.readString(buffer, position, Len.DAD_COD_GAR2);
        position += Len.DAD_COD_GAR2;
        dadCodGar3 = MarshalByte.readString(buffer, position, Len.DAD_COD_GAR3);
        position += Len.DAD_COD_GAR3;
        dadCodGar4 = MarshalByte.readString(buffer, position, Len.DAD_COD_GAR4);
        position += Len.DAD_COD_GAR4;
        dadCodGar5 = MarshalByte.readString(buffer, position, Len.DAD_COD_GAR5);
        position += Len.DAD_COD_GAR5;
        dadCodGar6 = MarshalByte.readString(buffer, position, Len.DAD_COD_GAR6);
        position += Len.DAD_COD_GAR6;
        dadDtDecorDflt.setDadDtDecorDfltFromBuffer(buffer, position);
        position += DadDtDecorDflt.Len.DAD_DT_DECOR_DFLT;
        dadEtaScadMascDflt.setDadEtaScadMascDfltFromBuffer(buffer, position);
        position += DadEtaScadMascDflt.Len.DAD_ETA_SCAD_MASC_DFLT;
        dadEtaScadFemmDflt.setDadEtaScadFemmDfltFromBuffer(buffer, position);
        position += DadEtaScadFemmDflt.Len.DAD_ETA_SCAD_FEMM_DFLT;
        dadDurAaAdesDflt.setDadDurAaAdesDfltFromBuffer(buffer, position);
        position += DadDurAaAdesDflt.Len.DAD_DUR_AA_ADES_DFLT;
        dadDurMmAdesDflt.setDadDurMmAdesDfltFromBuffer(buffer, position);
        position += DadDurMmAdesDflt.Len.DAD_DUR_MM_ADES_DFLT;
        dadDurGgAdesDflt.setDadDurGgAdesDfltFromBuffer(buffer, position);
        position += DadDurGgAdesDflt.Len.DAD_DUR_GG_ADES_DFLT;
        dadDtScadAdesDflt.setDadDtScadAdesDfltFromBuffer(buffer, position);
        position += DadDtScadAdesDflt.Len.DAD_DT_SCAD_ADES_DFLT;
        dadFrazDflt.setDadFrazDfltFromBuffer(buffer, position);
        position += DadFrazDflt.Len.DAD_FRAZ_DFLT;
        dadPcProvIncDflt.setDadPcProvIncDfltFromBuffer(buffer, position);
        position += DadPcProvIncDflt.Len.DAD_PC_PROV_INC_DFLT;
        dadImpProvIncDflt.setDadImpProvIncDfltFromBuffer(buffer, position);
        position += DadImpProvIncDflt.Len.DAD_IMP_PROV_INC_DFLT;
        dadImpAz.setDadImpAzFromBuffer(buffer, position);
        position += DadImpAz.Len.DAD_IMP_AZ;
        dadImpAder.setDadImpAderFromBuffer(buffer, position);
        position += DadImpAder.Len.DAD_IMP_ADER;
        dadImpTfr.setDadImpTfrFromBuffer(buffer, position);
        position += DadImpTfr.Len.DAD_IMP_TFR;
        dadImpVolo.setDadImpVoloFromBuffer(buffer, position);
        position += DadImpVolo.Len.DAD_IMP_VOLO;
        dadPcAz.setDadPcAzFromBuffer(buffer, position);
        position += DadPcAz.Len.DAD_PC_AZ;
        dadPcAder.setDadPcAderFromBuffer(buffer, position);
        position += DadPcAder.Len.DAD_PC_ADER;
        dadPcTfr.setDadPcTfrFromBuffer(buffer, position);
        position += DadPcTfr.Len.DAD_PC_TFR;
        dadPcVolo.setDadPcVoloFromBuffer(buffer, position);
        position += DadPcVolo.Len.DAD_PC_VOLO;
        dadTpFntCnbtva = MarshalByte.readString(buffer, position, Len.DAD_TP_FNT_CNBTVA);
        position += Len.DAD_TP_FNT_CNBTVA;
        dadImpPreDflt.setDadImpPreDfltFromBuffer(buffer, position);
        position += DadImpPreDflt.Len.DAD_IMP_PRE_DFLT;
        dadCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DAD_COD_COMP_ANIA, 0);
        position += Len.DAD_COD_COMP_ANIA;
        dadTpPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dadDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dadDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DAD_DS_VER, 0);
        position += Len.DAD_DS_VER;
        dadDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DAD_DS_TS_CPTZ, 0);
        position += Len.DAD_DS_TS_CPTZ;
        dadDsUtente = MarshalByte.readString(buffer, position, Len.DAD_DS_UTENTE);
        position += Len.DAD_DS_UTENTE;
        dadDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public void setDadIdDfltAdes(int dadIdDfltAdes) {
        this.dadIdDfltAdes = dadIdDfltAdes;
    }

    public int getDadIdDfltAdes() {
        return this.dadIdDfltAdes;
    }

    public void setDadIdPoli(int dadIdPoli) {
        this.dadIdPoli = dadIdPoli;
    }

    public int getDadIdPoli() {
        return this.dadIdPoli;
    }

    public void setDadIbPoli(String dadIbPoli) {
        this.dadIbPoli = Functions.subString(dadIbPoli, Len.DAD_IB_POLI);
    }

    public String getDadIbPoli() {
        return this.dadIbPoli;
    }

    public void setDadIbDflt(String dadIbDflt) {
        this.dadIbDflt = Functions.subString(dadIbDflt, Len.DAD_IB_DFLT);
    }

    public String getDadIbDflt() {
        return this.dadIbDflt;
    }

    public void setDadCodGar1(String dadCodGar1) {
        this.dadCodGar1 = Functions.subString(dadCodGar1, Len.DAD_COD_GAR1);
    }

    public String getDadCodGar1() {
        return this.dadCodGar1;
    }

    public String getDadCodGar1Formatted() {
        return Functions.padBlanks(getDadCodGar1(), Len.DAD_COD_GAR1);
    }

    public void setDadCodGar2(String dadCodGar2) {
        this.dadCodGar2 = Functions.subString(dadCodGar2, Len.DAD_COD_GAR2);
    }

    public String getDadCodGar2() {
        return this.dadCodGar2;
    }

    public String getDadCodGar2Formatted() {
        return Functions.padBlanks(getDadCodGar2(), Len.DAD_COD_GAR2);
    }

    public void setDadCodGar3(String dadCodGar3) {
        this.dadCodGar3 = Functions.subString(dadCodGar3, Len.DAD_COD_GAR3);
    }

    public String getDadCodGar3() {
        return this.dadCodGar3;
    }

    public String getDadCodGar3Formatted() {
        return Functions.padBlanks(getDadCodGar3(), Len.DAD_COD_GAR3);
    }

    public void setDadCodGar4(String dadCodGar4) {
        this.dadCodGar4 = Functions.subString(dadCodGar4, Len.DAD_COD_GAR4);
    }

    public String getDadCodGar4() {
        return this.dadCodGar4;
    }

    public String getDadCodGar4Formatted() {
        return Functions.padBlanks(getDadCodGar4(), Len.DAD_COD_GAR4);
    }

    public void setDadCodGar5(String dadCodGar5) {
        this.dadCodGar5 = Functions.subString(dadCodGar5, Len.DAD_COD_GAR5);
    }

    public String getDadCodGar5() {
        return this.dadCodGar5;
    }

    public String getDadCodGar5Formatted() {
        return Functions.padBlanks(getDadCodGar5(), Len.DAD_COD_GAR5);
    }

    public void setDadCodGar6(String dadCodGar6) {
        this.dadCodGar6 = Functions.subString(dadCodGar6, Len.DAD_COD_GAR6);
    }

    public String getDadCodGar6() {
        return this.dadCodGar6;
    }

    public String getDadCodGar6Formatted() {
        return Functions.padBlanks(getDadCodGar6(), Len.DAD_COD_GAR6);
    }

    public void setDadTpFntCnbtva(String dadTpFntCnbtva) {
        this.dadTpFntCnbtva = Functions.subString(dadTpFntCnbtva, Len.DAD_TP_FNT_CNBTVA);
    }

    public String getDadTpFntCnbtva() {
        return this.dadTpFntCnbtva;
    }

    public String getDadTpFntCnbtvaFormatted() {
        return Functions.padBlanks(getDadTpFntCnbtva(), Len.DAD_TP_FNT_CNBTVA);
    }

    public void setDadCodCompAnia(int dadCodCompAnia) {
        this.dadCodCompAnia = dadCodCompAnia;
    }

    public int getDadCodCompAnia() {
        return this.dadCodCompAnia;
    }

    public void setDadTpPre(char dadTpPre) {
        this.dadTpPre = dadTpPre;
    }

    public char getDadTpPre() {
        return this.dadTpPre;
    }

    public void setDadDsOperSql(char dadDsOperSql) {
        this.dadDsOperSql = dadDsOperSql;
    }

    public char getDadDsOperSql() {
        return this.dadDsOperSql;
    }

    public void setDadDsVer(int dadDsVer) {
        this.dadDsVer = dadDsVer;
    }

    public int getDadDsVer() {
        return this.dadDsVer;
    }

    public void setDadDsTsCptz(long dadDsTsCptz) {
        this.dadDsTsCptz = dadDsTsCptz;
    }

    public long getDadDsTsCptz() {
        return this.dadDsTsCptz;
    }

    public void setDadDsUtente(String dadDsUtente) {
        this.dadDsUtente = Functions.subString(dadDsUtente, Len.DAD_DS_UTENTE);
    }

    public String getDadDsUtente() {
        return this.dadDsUtente;
    }

    public void setDadDsStatoElab(char dadDsStatoElab) {
        this.dadDsStatoElab = dadDsStatoElab;
    }

    public char getDadDsStatoElab() {
        return this.dadDsStatoElab;
    }

    public DadDtDecorDflt getDadDtDecorDflt() {
        return dadDtDecorDflt;
    }

    public DadDtScadAdesDflt getDadDtScadAdesDflt() {
        return dadDtScadAdesDflt;
    }

    public DadDurAaAdesDflt getDadDurAaAdesDflt() {
        return dadDurAaAdesDflt;
    }

    public DadDurGgAdesDflt getDadDurGgAdesDflt() {
        return dadDurGgAdesDflt;
    }

    public DadDurMmAdesDflt getDadDurMmAdesDflt() {
        return dadDurMmAdesDflt;
    }

    public DadEtaScadFemmDflt getDadEtaScadFemmDflt() {
        return dadEtaScadFemmDflt;
    }

    public DadEtaScadMascDflt getDadEtaScadMascDflt() {
        return dadEtaScadMascDflt;
    }

    public DadFrazDflt getDadFrazDflt() {
        return dadFrazDflt;
    }

    public DadImpAder getDadImpAder() {
        return dadImpAder;
    }

    public DadImpAz getDadImpAz() {
        return dadImpAz;
    }

    public DadImpPreDflt getDadImpPreDflt() {
        return dadImpPreDflt;
    }

    public DadImpProvIncDflt getDadImpProvIncDflt() {
        return dadImpProvIncDflt;
    }

    public DadImpTfr getDadImpTfr() {
        return dadImpTfr;
    }

    public DadImpVolo getDadImpVolo() {
        return dadImpVolo;
    }

    public DadPcAder getDadPcAder() {
        return dadPcAder;
    }

    public DadPcAz getDadPcAz() {
        return dadPcAz;
    }

    public DadPcProvIncDflt getDadPcProvIncDflt() {
        return dadPcProvIncDflt;
    }

    public DadPcTfr getDadPcTfr() {
        return dadPcTfr;
    }

    public DadPcVolo getDadPcVolo() {
        return dadPcVolo;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_IB_POLI = 40;
        public static final int DAD_IB_DFLT = 40;
        public static final int DAD_COD_GAR1 = 12;
        public static final int DAD_COD_GAR2 = 12;
        public static final int DAD_COD_GAR3 = 12;
        public static final int DAD_COD_GAR4 = 12;
        public static final int DAD_COD_GAR5 = 12;
        public static final int DAD_COD_GAR6 = 12;
        public static final int DAD_TP_FNT_CNBTVA = 2;
        public static final int DAD_DS_UTENTE = 20;
        public static final int DAD_ID_DFLT_ADES = 5;
        public static final int DAD_ID_POLI = 5;
        public static final int DAD_COD_COMP_ANIA = 3;
        public static final int DAD_TP_PRE = 1;
        public static final int DAD_DS_OPER_SQL = 1;
        public static final int DAD_DS_VER = 5;
        public static final int DAD_DS_TS_CPTZ = 10;
        public static final int DAD_DS_STATO_ELAB = 1;
        public static final int DFLT_ADES = DAD_ID_DFLT_ADES + DAD_ID_POLI + DAD_IB_POLI + DAD_IB_DFLT + DAD_COD_GAR1 + DAD_COD_GAR2 + DAD_COD_GAR3 + DAD_COD_GAR4 + DAD_COD_GAR5 + DAD_COD_GAR6 + DadDtDecorDflt.Len.DAD_DT_DECOR_DFLT + DadEtaScadMascDflt.Len.DAD_ETA_SCAD_MASC_DFLT + DadEtaScadFemmDflt.Len.DAD_ETA_SCAD_FEMM_DFLT + DadDurAaAdesDflt.Len.DAD_DUR_AA_ADES_DFLT + DadDurMmAdesDflt.Len.DAD_DUR_MM_ADES_DFLT + DadDurGgAdesDflt.Len.DAD_DUR_GG_ADES_DFLT + DadDtScadAdesDflt.Len.DAD_DT_SCAD_ADES_DFLT + DadFrazDflt.Len.DAD_FRAZ_DFLT + DadPcProvIncDflt.Len.DAD_PC_PROV_INC_DFLT + DadImpProvIncDflt.Len.DAD_IMP_PROV_INC_DFLT + DadImpAz.Len.DAD_IMP_AZ + DadImpAder.Len.DAD_IMP_ADER + DadImpTfr.Len.DAD_IMP_TFR + DadImpVolo.Len.DAD_IMP_VOLO + DadPcAz.Len.DAD_PC_AZ + DadPcAder.Len.DAD_PC_ADER + DadPcTfr.Len.DAD_PC_TFR + DadPcVolo.Len.DAD_PC_VOLO + DAD_TP_FNT_CNBTVA + DadImpPreDflt.Len.DAD_IMP_PRE_DFLT + DAD_COD_COMP_ANIA + DAD_TP_PRE + DAD_DS_OPER_SQL + DAD_DS_VER + DAD_DS_TS_CPTZ + DAD_DS_UTENTE + DAD_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_ID_DFLT_ADES = 9;
            public static final int DAD_ID_POLI = 9;
            public static final int DAD_COD_COMP_ANIA = 5;
            public static final int DAD_DS_VER = 9;
            public static final int DAD_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
