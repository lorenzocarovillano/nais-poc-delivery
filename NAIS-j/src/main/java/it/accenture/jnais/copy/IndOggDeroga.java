package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-OGG-DEROGA<br>
 * Variable: IND-OGG-DEROGA from copybook IDBVODE2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndOggDeroga {

    //==== PROPERTIES ====
    //Original name: IND-ODE-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ODE-IB-OGG
    private short ibOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ODE-COD-LIV-AUT-APPRT
    private short codLivAutApprt = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setIbOgg(short ibOgg) {
        this.ibOgg = ibOgg;
    }

    public short getIbOgg() {
        return this.ibOgg;
    }

    public void setCodLivAutApprt(short codLivAutApprt) {
        this.codLivAutApprt = codLivAutApprt;
    }

    public short getCodLivAutApprt() {
        return this.codLivAutApprt;
    }
}
