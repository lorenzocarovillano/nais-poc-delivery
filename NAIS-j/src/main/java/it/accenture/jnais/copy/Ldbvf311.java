package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: LDBVF311<br>
 * Variable: LDBVF311 from copybook LDBVF311<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbvf311 {

    //==== PROPERTIES ====
    //Original name: LDBVF311-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LDBVF311-MM
    private short mm = DefaultValues.SHORT_VAL;
    //Original name: LDBVF311-AA
    private short aa = DefaultValues.SHORT_VAL;

    //==== METHODS ====
    public void setLdbvf311Formatted(String data) {
        byte[] buffer = new byte[Len.LDBVF311];
        MarshalByte.writeString(buffer, 1, data, Len.LDBVF311);
        setLdbvf311Bytes(buffer, 1);
    }

    public String getLdbvf311Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvf311Bytes());
    }

    public byte[] getLdbvf311Bytes() {
        byte[] buffer = new byte[Len.LDBVF311];
        return getLdbvf311Bytes(buffer, 1);
    }

    public void setLdbvf311Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        mm = MarshalByte.readPackedAsShort(buffer, position, Len.Int.MM, 0);
        position += Len.MM;
        aa = MarshalByte.readPackedAsShort(buffer, position, Len.Int.AA, 0);
    }

    public byte[] getLdbvf311Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeShortAsPacked(buffer, position, mm, Len.Int.MM, 0);
        position += Len.MM;
        MarshalByte.writeShortAsPacked(buffer, position, aa, Len.Int.AA, 0);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setMm(short mm) {
        this.mm = mm;
    }

    public short getMm() {
        return this.mm;
    }

    public void setAa(short aa) {
        this.aa = aa;
    }

    public short getAa() {
        return this.aa;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POLI = 5;
        public static final int MM = 2;
        public static final int AA = 3;
        public static final int LDBVF311 = ID_POLI + MM + AA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int MM = 2;
            public static final int AA = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
