package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVE153<br>
 * Copybook: IDBVE153 from copybook IDBVE153<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbve153 {

    //==== PROPERTIES ====
    //Original name: E15-DT-INI-EFF-DB
    private String e15DtIniEffDb = DefaultValues.stringVal(Len.E15_DT_INI_EFF_DB);
    //Original name: E15-DT-END-EFF-DB
    private String e15DtEndEffDb = DefaultValues.stringVal(Len.E15_DT_END_EFF_DB);

    //==== METHODS ====
    public void setE15DtIniEffDb(String e15DtIniEffDb) {
        this.e15DtIniEffDb = Functions.subString(e15DtIniEffDb, Len.E15_DT_INI_EFF_DB);
    }

    public String getE15DtIniEffDb() {
        return this.e15DtIniEffDb;
    }

    public void setE15DtEndEffDb(String e15DtEndEffDb) {
        this.e15DtEndEffDb = Functions.subString(e15DtEndEffDb, Len.E15_DT_END_EFF_DB);
    }

    public String getE15DtEndEffDb() {
        return this.e15DtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int E15_DT_INI_EFF_DB = 10;
        public static final int E15_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
