package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;

/**Original name: W820-AREA-TESTATA<br>
 * Variable: W820-AREA-TESTATA from copybook LOAR0820<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class W820AreaTestata {

    //==== PROPERTIES ====
    //Original name: FILLER-W820-AREA-TESTATA
    private String flr1 = "DATA ELABORAZIONE";
    //Original name: FILLER-W820-AREA-TESTATA-1
    private char flr2 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-2
    private String flr3 = "CODICE COMPAGNIA ANIA";
    //Original name: FILLER-W820-AREA-TESTATA-3
    private char flr4 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-4
    private String flr5 = "CODICE AGENZIA";
    //Original name: FILLER-W820-AREA-TESTATA-5
    private char flr6 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-6
    private String flr7 = "RAMO GESTIONALE";
    //Original name: FILLER-W820-AREA-TESTATA-7
    private char flr8 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-8
    private String flr9 = "NUMERO POLIZZA";
    //Original name: FILLER-W820-AREA-TESTATA-9
    private char flr10 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-10
    private String flr11 = "CODICE TARIFFA";
    //Original name: FILLER-W820-AREA-TESTATA-11
    private char flr12 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-12
    private String flr13 = "DATA EMISSIONE";
    //Original name: FILLER-W820-AREA-TESTATA-13
    private char flr14 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-14
    private String flr15 = "DATA EFFETTO";
    //Original name: FILLER-W820-AREA-TESTATA-15
    private char flr16 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-16
    private String flr17 = "DATA RICORRENZA";
    //Original name: FILLER-W820-AREA-TESTATA-17
    private char flr18 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-18
    private String flr19 = "DATA PAGAMENTO";
    //Original name: FILLER-W820-AREA-TESTATA-19
    private char flr20 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-20
    private String flr21 = "PREMIO NETTO";
    //Original name: FILLER-W820-AREA-TESTATA-21
    private char flr22 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-22
    private String flr23 = "CAPITALE RIVALUTATO";
    //Original name: FILLER-W820-AREA-TESTATA-23
    private char flr24 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-24
    private String flr25 = "ULTIMO INCREMENTO";
    //Original name: FILLER-W820-AREA-TESTATA-25
    private char flr26 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-26
    private String flr27 = "RISERVA";
    //Original name: FILLER-W820-AREA-TESTATA-27
    private char flr28 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-28
    private String flr29 = "FLAG VERSAMENTI AGGIUNTIVI";
    //Original name: FILLER-W820-AREA-TESTATA-29
    private char flr30 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-30
    private String flr31 = "FLAG RISCATTI PARZIALI";
    //Original name: FILLER-W820-AREA-TESTATA-31
    private char flr32 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-32
    private String flr33 = "IMPORTO RISCATTATO";
    //Original name: FILLER-W820-AREA-TESTATA-33
    private char flr34 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-34
    private String flr35 = "PERCENTUALE REMUNERAZIONE";
    //Original name: FILLER-W820-AREA-TESTATA-35
    private char flr36 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-36
    private String flr37 = "IMPORTO REMUNERAZIONE";
    //Original name: FILLER-W820-AREA-TESTATA-37
    private char flr38 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-38
    private String flr39 = "TP-ELABORAZIONE";
    //Original name: FILLER-W820-AREA-TESTATA-39
    private char flr40 = ';';
    //Original name: FILLER-W820-AREA-TESTATA-40
    private String flr41 = "";

    //==== METHODS ====
    public String getTestataFormatted() {
        return MarshalByteExt.bufferToStr(getTestataBytes());
    }

    public byte[] getTestataBytes() {
        byte[] buffer = new byte[Len.TESTATA];
        return getTestataBytes(buffer, 1);
    }

    public byte[] getTestataBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeChar(buffer, position, flr2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeChar(buffer, position, flr4);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeChar(buffer, position, flr6);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
        position += Len.FLR7;
        MarshalByte.writeChar(buffer, position, flr8);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr9, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeChar(buffer, position, flr10);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr11, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeChar(buffer, position, flr12);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr13, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeChar(buffer, position, flr14);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr15, Len.FLR15);
        position += Len.FLR15;
        MarshalByte.writeChar(buffer, position, flr16);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr17, Len.FLR7);
        position += Len.FLR7;
        MarshalByte.writeChar(buffer, position, flr18);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr19, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeChar(buffer, position, flr20);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr21, Len.FLR15);
        position += Len.FLR15;
        MarshalByte.writeChar(buffer, position, flr22);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr23, Len.FLR23);
        position += Len.FLR23;
        MarshalByte.writeChar(buffer, position, flr24);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr25, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeChar(buffer, position, flr26);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr27, Len.FLR27);
        position += Len.FLR27;
        MarshalByte.writeChar(buffer, position, flr28);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr29, Len.FLR29);
        position += Len.FLR29;
        MarshalByte.writeChar(buffer, position, flr30);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr31, Len.FLR31);
        position += Len.FLR31;
        MarshalByte.writeChar(buffer, position, flr32);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr33, Len.FLR33);
        position += Len.FLR33;
        MarshalByte.writeChar(buffer, position, flr34);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr35, Len.FLR35);
        position += Len.FLR35;
        MarshalByte.writeChar(buffer, position, flr36);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr37, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeChar(buffer, position, flr38);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr39, Len.FLR7);
        position += Len.FLR7;
        MarshalByte.writeChar(buffer, position, flr40);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flr41, Len.FLR41);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public char getFlr2() {
        return this.flr2;
    }

    public String getFlr3() {
        return this.flr3;
    }

    public char getFlr4() {
        return this.flr4;
    }

    public String getFlr5() {
        return this.flr5;
    }

    public char getFlr6() {
        return this.flr6;
    }

    public String getFlr7() {
        return this.flr7;
    }

    public char getFlr8() {
        return this.flr8;
    }

    public String getFlr9() {
        return this.flr9;
    }

    public char getFlr10() {
        return this.flr10;
    }

    public String getFlr11() {
        return this.flr11;
    }

    public char getFlr12() {
        return this.flr12;
    }

    public String getFlr13() {
        return this.flr13;
    }

    public char getFlr14() {
        return this.flr14;
    }

    public String getFlr15() {
        return this.flr15;
    }

    public char getFlr16() {
        return this.flr16;
    }

    public String getFlr17() {
        return this.flr17;
    }

    public char getFlr18() {
        return this.flr18;
    }

    public String getFlr19() {
        return this.flr19;
    }

    public char getFlr20() {
        return this.flr20;
    }

    public String getFlr21() {
        return this.flr21;
    }

    public char getFlr22() {
        return this.flr22;
    }

    public String getFlr23() {
        return this.flr23;
    }

    public char getFlr24() {
        return this.flr24;
    }

    public String getFlr25() {
        return this.flr25;
    }

    public char getFlr26() {
        return this.flr26;
    }

    public String getFlr27() {
        return this.flr27;
    }

    public char getFlr28() {
        return this.flr28;
    }

    public String getFlr29() {
        return this.flr29;
    }

    public char getFlr30() {
        return this.flr30;
    }

    public String getFlr31() {
        return this.flr31;
    }

    public char getFlr32() {
        return this.flr32;
    }

    public String getFlr33() {
        return this.flr33;
    }

    public char getFlr34() {
        return this.flr34;
    }

    public String getFlr35() {
        return this.flr35;
    }

    public char getFlr36() {
        return this.flr36;
    }

    public String getFlr37() {
        return this.flr37;
    }

    public char getFlr38() {
        return this.flr38;
    }

    public String getFlr39() {
        return this.flr39;
    }

    public char getFlr40() {
        return this.flr40;
    }

    public String getFlr41() {
        return this.flr41;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 17;
        public static final int FLR2 = 1;
        public static final int FLR3 = 21;
        public static final int FLR5 = 14;
        public static final int FLR7 = 15;
        public static final int FLR15 = 12;
        public static final int FLR23 = 19;
        public static final int FLR27 = 8;
        public static final int FLR29 = 26;
        public static final int FLR31 = 22;
        public static final int FLR33 = 18;
        public static final int FLR35 = 25;
        public static final int FLR41 = 59;
        public static final int TESTATA = FLR35 + FLR33 + FLR23 + FLR31 + 2 * FLR1 + 20 * FLR2 + FLR41 + 2 * FLR3 + 5 * FLR5 + 3 * FLR7 + FLR29 + 2 * FLR15 + FLR27;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
