package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Wb03AaRenCer;
import it.accenture.jnais.ws.redefines.Wb03Abb;
import it.accenture.jnais.ws.redefines.Wb03AcqExp;
import it.accenture.jnais.ws.redefines.Wb03AlqMargCSubrsh;
import it.accenture.jnais.ws.redefines.Wb03AlqMargRis;
import it.accenture.jnais.ws.redefines.Wb03AlqRetrT;
import it.accenture.jnais.ws.redefines.Wb03AntidurCalc365;
import it.accenture.jnais.ws.redefines.Wb03AntidurDtCalc;
import it.accenture.jnais.ws.redefines.Wb03AntidurRicorPrec;
import it.accenture.jnais.ws.redefines.Wb03CarAcqNonScon;
import it.accenture.jnais.ws.redefines.Wb03CarAcqPrecontato;
import it.accenture.jnais.ws.redefines.Wb03CarGest;
import it.accenture.jnais.ws.redefines.Wb03CarGestNonScon;
import it.accenture.jnais.ws.redefines.Wb03CarInc;
import it.accenture.jnais.ws.redefines.Wb03CarIncNonScon;
import it.accenture.jnais.ws.redefines.Wb03Carz;
import it.accenture.jnais.ws.redefines.Wb03CodAge;
import it.accenture.jnais.ws.redefines.Wb03CodCan;
import it.accenture.jnais.ws.redefines.Wb03CodPrdt;
import it.accenture.jnais.ws.redefines.Wb03CodSubage;
import it.accenture.jnais.ws.redefines.Wb03CoeffOpzCpt;
import it.accenture.jnais.ws.redefines.Wb03CoeffOpzRen;
import it.accenture.jnais.ws.redefines.Wb03CoeffRis1T;
import it.accenture.jnais.ws.redefines.Wb03CoeffRis2T;
import it.accenture.jnais.ws.redefines.Wb03CommisInter;
import it.accenture.jnais.ws.redefines.Wb03CptAsstoIniMor;
import it.accenture.jnais.ws.redefines.Wb03CptDtRidz;
import it.accenture.jnais.ws.redefines.Wb03CptDtStab;
import it.accenture.jnais.ws.redefines.Wb03CptRiasto;
import it.accenture.jnais.ws.redefines.Wb03CptRiastoEcc;
import it.accenture.jnais.ws.redefines.Wb03CptRshMor;
import it.accenture.jnais.ws.redefines.Wb03CSubrshT;
import it.accenture.jnais.ws.redefines.Wb03CumRiscpar;
import it.accenture.jnais.ws.redefines.Wb03Dir;
import it.accenture.jnais.ws.redefines.Wb03DirEmis;
import it.accenture.jnais.ws.redefines.Wb03DtDecorAdes;
import it.accenture.jnais.ws.redefines.Wb03DtEffCambStat;
import it.accenture.jnais.ws.redefines.Wb03DtEffRidz;
import it.accenture.jnais.ws.redefines.Wb03DtEffStab;
import it.accenture.jnais.ws.redefines.Wb03DtEmisCambStat;
import it.accenture.jnais.ws.redefines.Wb03DtEmisRidz;
import it.accenture.jnais.ws.redefines.Wb03DtEmisTrch;
import it.accenture.jnais.ws.redefines.Wb03DtIncUltPre;
import it.accenture.jnais.ws.redefines.Wb03DtIniValTar;
import it.accenture.jnais.ws.redefines.Wb03DtNasc1oAssto;
import it.accenture.jnais.ws.redefines.Wb03DtQtzEmis;
import it.accenture.jnais.ws.redefines.Wb03DtScadIntmd;
import it.accenture.jnais.ws.redefines.Wb03DtScadPagPre;
import it.accenture.jnais.ws.redefines.Wb03DtScadTrch;
import it.accenture.jnais.ws.redefines.Wb03DtUltPrePag;
import it.accenture.jnais.ws.redefines.Wb03DtUltRival;
import it.accenture.jnais.ws.redefines.Wb03Dur1oPerAa;
import it.accenture.jnais.ws.redefines.Wb03Dur1oPerGg;
import it.accenture.jnais.ws.redefines.Wb03Dur1oPerMm;
import it.accenture.jnais.ws.redefines.Wb03DurAa;
import it.accenture.jnais.ws.redefines.Wb03DurGarAa;
import it.accenture.jnais.ws.redefines.Wb03DurGarGg;
import it.accenture.jnais.ws.redefines.Wb03DurGarMm;
import it.accenture.jnais.ws.redefines.Wb03DurGg;
import it.accenture.jnais.ws.redefines.Wb03DurMm;
import it.accenture.jnais.ws.redefines.Wb03DurPagPre;
import it.accenture.jnais.ws.redefines.Wb03DurPagRen;
import it.accenture.jnais.ws.redefines.Wb03DurResDtCalc;
import it.accenture.jnais.ws.redefines.Wb03EtaAa1oAssto;
import it.accenture.jnais.ws.redefines.Wb03EtaMm1oAssto;
import it.accenture.jnais.ws.redefines.Wb03EtaRaggnDtCalc;
import it.accenture.jnais.ws.redefines.Wb03Fraz;
import it.accenture.jnais.ws.redefines.Wb03FrazDecrCpt;
import it.accenture.jnais.ws.redefines.Wb03FrazIniErogRen;
import it.accenture.jnais.ws.redefines.Wb03IdRichEstrazAgg;
import it.accenture.jnais.ws.redefines.Wb03ImpCarCasoMor;
import it.accenture.jnais.ws.redefines.Wb03IntrTecn;
import it.accenture.jnais.ws.redefines.Wb03MetRiscSpcl;
import it.accenture.jnais.ws.redefines.Wb03MinGartoT;
import it.accenture.jnais.ws.redefines.Wb03MinTrnutT;
import it.accenture.jnais.ws.redefines.Wb03NsQuo;
import it.accenture.jnais.ws.redefines.Wb03NumPrePatt;
import it.accenture.jnais.ws.redefines.Wb03OverComm;
import it.accenture.jnais.ws.redefines.Wb03PcCarAcq;
import it.accenture.jnais.ws.redefines.Wb03PcCarGest;
import it.accenture.jnais.ws.redefines.Wb03PcCarMor;
import it.accenture.jnais.ws.redefines.Wb03PreAnnualizRicor;
import it.accenture.jnais.ws.redefines.Wb03PreCont;
import it.accenture.jnais.ws.redefines.Wb03PreDovIni;
import it.accenture.jnais.ws.redefines.Wb03PreDovRivtoT;
import it.accenture.jnais.ws.redefines.Wb03PrePattuitoIni;
import it.accenture.jnais.ws.redefines.Wb03PrePpIni;
import it.accenture.jnais.ws.redefines.Wb03PrePpUlt;
import it.accenture.jnais.ws.redefines.Wb03PreRiasto;
import it.accenture.jnais.ws.redefines.Wb03PreRiastoEcc;
import it.accenture.jnais.ws.redefines.Wb03PreRshT;
import it.accenture.jnais.ws.redefines.Wb03ProvAcq;
import it.accenture.jnais.ws.redefines.Wb03ProvAcqRicor;
import it.accenture.jnais.ws.redefines.Wb03ProvInc;
import it.accenture.jnais.ws.redefines.Wb03PrstzAggIni;
import it.accenture.jnais.ws.redefines.Wb03PrstzAggUlt;
import it.accenture.jnais.ws.redefines.Wb03PrstzIni;
import it.accenture.jnais.ws.redefines.Wb03PrstzT;
import it.accenture.jnais.ws.redefines.Wb03QtzSpZCoupDtC;
import it.accenture.jnais.ws.redefines.Wb03QtzSpZCoupEmis;
import it.accenture.jnais.ws.redefines.Wb03QtzSpZOpzDtCa;
import it.accenture.jnais.ws.redefines.Wb03QtzSpZOpzEmis;
import it.accenture.jnais.ws.redefines.Wb03QtzTotDtCalc;
import it.accenture.jnais.ws.redefines.Wb03QtzTotDtUltBil;
import it.accenture.jnais.ws.redefines.Wb03QtzTotEmis;
import it.accenture.jnais.ws.redefines.Wb03Rappel;
import it.accenture.jnais.ws.redefines.Wb03RatRen;
import it.accenture.jnais.ws.redefines.Wb03RemunAss;
import it.accenture.jnais.ws.redefines.Wb03RisAcqT;
import it.accenture.jnais.ws.redefines.Wb03Riscpar;
import it.accenture.jnais.ws.redefines.Wb03RisMatChiuPrec;
import it.accenture.jnais.ws.redefines.Wb03RisPuraT;
import it.accenture.jnais.ws.redefines.Wb03RisRiasta;
import it.accenture.jnais.ws.redefines.Wb03RisRiastaEcc;
import it.accenture.jnais.ws.redefines.Wb03RisRistorniCap;
import it.accenture.jnais.ws.redefines.Wb03RisSpeT;
import it.accenture.jnais.ws.redefines.Wb03RisZilT;
import it.accenture.jnais.ws.redefines.Wb03TsMedio;
import it.accenture.jnais.ws.redefines.Wb03TsNetT;
import it.accenture.jnais.ws.redefines.Wb03TsPp;
import it.accenture.jnais.ws.redefines.Wb03TsRendtoSppr;
import it.accenture.jnais.ws.redefines.Wb03TsRendtoT;
import it.accenture.jnais.ws.redefines.Wb03TsStabPre;
import it.accenture.jnais.ws.redefines.Wb03TsTariDov;
import it.accenture.jnais.ws.redefines.Wb03TsTariScon;
import it.accenture.jnais.ws.redefines.Wb03UltRm;

/**Original name: WB03-DATI<br>
 * Variable: WB03-DATI from copybook LCCVB031<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wb03Dati {

    //==== PROPERTIES ====
    //Original name: WB03-ID-BILA-TRCH-ESTR
    private int wb03IdBilaTrchEstr = DefaultValues.INT_VAL;
    //Original name: WB03-COD-COMP-ANIA
    private int wb03CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WB03-ID-RICH-ESTRAZ-MAS
    private int wb03IdRichEstrazMas = DefaultValues.INT_VAL;
    //Original name: WB03-ID-RICH-ESTRAZ-AGG
    private Wb03IdRichEstrazAgg wb03IdRichEstrazAgg = new Wb03IdRichEstrazAgg();
    //Original name: WB03-FL-SIMULAZIONE
    private char wb03FlSimulazione = DefaultValues.CHAR_VAL;
    //Original name: WB03-DT-RIS
    private int wb03DtRis = DefaultValues.INT_VAL;
    //Original name: WB03-DT-PRODUZIONE
    private int wb03DtProduzione = DefaultValues.INT_VAL;
    //Original name: WB03-ID-POLI
    private int wb03IdPoli = DefaultValues.INT_VAL;
    //Original name: WB03-ID-ADES
    private int wb03IdAdes = DefaultValues.INT_VAL;
    //Original name: WB03-ID-GAR
    private int wb03IdGar = DefaultValues.INT_VAL;
    //Original name: WB03-ID-TRCH-DI-GAR
    private int wb03IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: WB03-TP-FRM-ASSVA
    private String wb03TpFrmAssva = DefaultValues.stringVal(Len.WB03_TP_FRM_ASSVA);
    //Original name: WB03-TP-RAMO-BILA
    private String wb03TpRamoBila = DefaultValues.stringVal(Len.WB03_TP_RAMO_BILA);
    //Original name: WB03-TP-CALC-RIS
    private String wb03TpCalcRis = DefaultValues.stringVal(Len.WB03_TP_CALC_RIS);
    //Original name: WB03-COD-RAMO
    private String wb03CodRamo = DefaultValues.stringVal(Len.WB03_COD_RAMO);
    //Original name: WB03-COD-TARI
    private String wb03CodTari = DefaultValues.stringVal(Len.WB03_COD_TARI);
    //Original name: WB03-DT-INI-VAL-TAR
    private Wb03DtIniValTar wb03DtIniValTar = new Wb03DtIniValTar();
    //Original name: WB03-COD-PROD
    private String wb03CodProd = DefaultValues.stringVal(Len.WB03_COD_PROD);
    //Original name: WB03-DT-INI-VLDT-PROD
    private int wb03DtIniVldtProd = DefaultValues.INT_VAL;
    //Original name: WB03-COD-TARI-ORGN
    private String wb03CodTariOrgn = DefaultValues.stringVal(Len.WB03_COD_TARI_ORGN);
    //Original name: WB03-MIN-GARTO-T
    private Wb03MinGartoT wb03MinGartoT = new Wb03MinGartoT();
    //Original name: WB03-TP-TARI
    private String wb03TpTari = DefaultValues.stringVal(Len.WB03_TP_TARI);
    //Original name: WB03-TP-PRE
    private char wb03TpPre = DefaultValues.CHAR_VAL;
    //Original name: WB03-TP-ADEG-PRE
    private char wb03TpAdegPre = DefaultValues.CHAR_VAL;
    //Original name: WB03-TP-RIVAL
    private String wb03TpRival = DefaultValues.stringVal(Len.WB03_TP_RIVAL);
    //Original name: WB03-FL-DA-TRASF
    private char wb03FlDaTrasf = DefaultValues.CHAR_VAL;
    //Original name: WB03-FL-CAR-CONT
    private char wb03FlCarCont = DefaultValues.CHAR_VAL;
    //Original name: WB03-FL-PRE-DA-RIS
    private char wb03FlPreDaRis = DefaultValues.CHAR_VAL;
    //Original name: WB03-FL-PRE-AGG
    private char wb03FlPreAgg = DefaultValues.CHAR_VAL;
    //Original name: WB03-TP-TRCH
    private String wb03TpTrch = DefaultValues.stringVal(Len.WB03_TP_TRCH);
    //Original name: WB03-TP-TST
    private String wb03TpTst = DefaultValues.stringVal(Len.WB03_TP_TST);
    //Original name: WB03-COD-CONV
    private String wb03CodConv = DefaultValues.stringVal(Len.WB03_COD_CONV);
    //Original name: WB03-DT-DECOR-POLI
    private int wb03DtDecorPoli = DefaultValues.INT_VAL;
    //Original name: WB03-DT-DECOR-ADES
    private Wb03DtDecorAdes wb03DtDecorAdes = new Wb03DtDecorAdes();
    //Original name: WB03-DT-DECOR-TRCH
    private int wb03DtDecorTrch = DefaultValues.INT_VAL;
    //Original name: WB03-DT-EMIS-POLI
    private int wb03DtEmisPoli = DefaultValues.INT_VAL;
    //Original name: WB03-DT-EMIS-TRCH
    private Wb03DtEmisTrch wb03DtEmisTrch = new Wb03DtEmisTrch();
    //Original name: WB03-DT-SCAD-TRCH
    private Wb03DtScadTrch wb03DtScadTrch = new Wb03DtScadTrch();
    //Original name: WB03-DT-SCAD-INTMD
    private Wb03DtScadIntmd wb03DtScadIntmd = new Wb03DtScadIntmd();
    //Original name: WB03-DT-SCAD-PAG-PRE
    private Wb03DtScadPagPre wb03DtScadPagPre = new Wb03DtScadPagPre();
    //Original name: WB03-DT-ULT-PRE-PAG
    private Wb03DtUltPrePag wb03DtUltPrePag = new Wb03DtUltPrePag();
    //Original name: WB03-DT-NASC-1O-ASSTO
    private Wb03DtNasc1oAssto wb03DtNasc1oAssto = new Wb03DtNasc1oAssto();
    //Original name: WB03-SEX-1O-ASSTO
    private char wb03Sex1oAssto = DefaultValues.CHAR_VAL;
    //Original name: WB03-ETA-AA-1O-ASSTO
    private Wb03EtaAa1oAssto wb03EtaAa1oAssto = new Wb03EtaAa1oAssto();
    //Original name: WB03-ETA-MM-1O-ASSTO
    private Wb03EtaMm1oAssto wb03EtaMm1oAssto = new Wb03EtaMm1oAssto();
    //Original name: WB03-ETA-RAGGN-DT-CALC
    private Wb03EtaRaggnDtCalc wb03EtaRaggnDtCalc = new Wb03EtaRaggnDtCalc();
    //Original name: WB03-DUR-AA
    private Wb03DurAa wb03DurAa = new Wb03DurAa();
    //Original name: WB03-DUR-MM
    private Wb03DurMm wb03DurMm = new Wb03DurMm();
    //Original name: WB03-DUR-GG
    private Wb03DurGg wb03DurGg = new Wb03DurGg();
    //Original name: WB03-DUR-1O-PER-AA
    private Wb03Dur1oPerAa wb03Dur1oPerAa = new Wb03Dur1oPerAa();
    //Original name: WB03-DUR-1O-PER-MM
    private Wb03Dur1oPerMm wb03Dur1oPerMm = new Wb03Dur1oPerMm();
    //Original name: WB03-DUR-1O-PER-GG
    private Wb03Dur1oPerGg wb03Dur1oPerGg = new Wb03Dur1oPerGg();
    //Original name: WB03-ANTIDUR-RICOR-PREC
    private Wb03AntidurRicorPrec wb03AntidurRicorPrec = new Wb03AntidurRicorPrec();
    //Original name: WB03-ANTIDUR-DT-CALC
    private Wb03AntidurDtCalc wb03AntidurDtCalc = new Wb03AntidurDtCalc();
    //Original name: WB03-DUR-RES-DT-CALC
    private Wb03DurResDtCalc wb03DurResDtCalc = new Wb03DurResDtCalc();
    //Original name: WB03-TP-STAT-BUS-POLI
    private String wb03TpStatBusPoli = DefaultValues.stringVal(Len.WB03_TP_STAT_BUS_POLI);
    //Original name: WB03-TP-CAUS-POLI
    private String wb03TpCausPoli = DefaultValues.stringVal(Len.WB03_TP_CAUS_POLI);
    //Original name: WB03-TP-STAT-BUS-ADES
    private String wb03TpStatBusAdes = DefaultValues.stringVal(Len.WB03_TP_STAT_BUS_ADES);
    //Original name: WB03-TP-CAUS-ADES
    private String wb03TpCausAdes = DefaultValues.stringVal(Len.WB03_TP_CAUS_ADES);
    //Original name: WB03-TP-STAT-BUS-TRCH
    private String wb03TpStatBusTrch = DefaultValues.stringVal(Len.WB03_TP_STAT_BUS_TRCH);
    //Original name: WB03-TP-CAUS-TRCH
    private String wb03TpCausTrch = DefaultValues.stringVal(Len.WB03_TP_CAUS_TRCH);
    //Original name: WB03-DT-EFF-CAMB-STAT
    private Wb03DtEffCambStat wb03DtEffCambStat = new Wb03DtEffCambStat();
    //Original name: WB03-DT-EMIS-CAMB-STAT
    private Wb03DtEmisCambStat wb03DtEmisCambStat = new Wb03DtEmisCambStat();
    //Original name: WB03-DT-EFF-STAB
    private Wb03DtEffStab wb03DtEffStab = new Wb03DtEffStab();
    //Original name: WB03-CPT-DT-STAB
    private Wb03CptDtStab wb03CptDtStab = new Wb03CptDtStab();
    //Original name: WB03-DT-EFF-RIDZ
    private Wb03DtEffRidz wb03DtEffRidz = new Wb03DtEffRidz();
    //Original name: WB03-DT-EMIS-RIDZ
    private Wb03DtEmisRidz wb03DtEmisRidz = new Wb03DtEmisRidz();
    //Original name: WB03-CPT-DT-RIDZ
    private Wb03CptDtRidz wb03CptDtRidz = new Wb03CptDtRidz();
    //Original name: WB03-FRAZ
    private Wb03Fraz wb03Fraz = new Wb03Fraz();
    //Original name: WB03-DUR-PAG-PRE
    private Wb03DurPagPre wb03DurPagPre = new Wb03DurPagPre();
    //Original name: WB03-NUM-PRE-PATT
    private Wb03NumPrePatt wb03NumPrePatt = new Wb03NumPrePatt();
    //Original name: WB03-FRAZ-INI-EROG-REN
    private Wb03FrazIniErogRen wb03FrazIniErogRen = new Wb03FrazIniErogRen();
    //Original name: WB03-AA-REN-CER
    private Wb03AaRenCer wb03AaRenCer = new Wb03AaRenCer();
    //Original name: WB03-RAT-REN
    private Wb03RatRen wb03RatRen = new Wb03RatRen();
    //Original name: WB03-COD-DIV
    private String wb03CodDiv = DefaultValues.stringVal(Len.WB03_COD_DIV);
    //Original name: WB03-RISCPAR
    private Wb03Riscpar wb03Riscpar = new Wb03Riscpar();
    //Original name: WB03-CUM-RISCPAR
    private Wb03CumRiscpar wb03CumRiscpar = new Wb03CumRiscpar();
    //Original name: WB03-ULT-RM
    private Wb03UltRm wb03UltRm = new Wb03UltRm();
    //Original name: WB03-TS-RENDTO-T
    private Wb03TsRendtoT wb03TsRendtoT = new Wb03TsRendtoT();
    //Original name: WB03-ALQ-RETR-T
    private Wb03AlqRetrT wb03AlqRetrT = new Wb03AlqRetrT();
    //Original name: WB03-MIN-TRNUT-T
    private Wb03MinTrnutT wb03MinTrnutT = new Wb03MinTrnutT();
    //Original name: WB03-TS-NET-T
    private Wb03TsNetT wb03TsNetT = new Wb03TsNetT();
    //Original name: WB03-DT-ULT-RIVAL
    private Wb03DtUltRival wb03DtUltRival = new Wb03DtUltRival();
    //Original name: WB03-PRSTZ-INI
    private Wb03PrstzIni wb03PrstzIni = new Wb03PrstzIni();
    //Original name: WB03-PRSTZ-AGG-INI
    private Wb03PrstzAggIni wb03PrstzAggIni = new Wb03PrstzAggIni();
    //Original name: WB03-PRSTZ-AGG-ULT
    private Wb03PrstzAggUlt wb03PrstzAggUlt = new Wb03PrstzAggUlt();
    //Original name: WB03-RAPPEL
    private Wb03Rappel wb03Rappel = new Wb03Rappel();
    //Original name: WB03-PRE-PATTUITO-INI
    private Wb03PrePattuitoIni wb03PrePattuitoIni = new Wb03PrePattuitoIni();
    //Original name: WB03-PRE-DOV-INI
    private Wb03PreDovIni wb03PreDovIni = new Wb03PreDovIni();
    //Original name: WB03-PRE-DOV-RIVTO-T
    private Wb03PreDovRivtoT wb03PreDovRivtoT = new Wb03PreDovRivtoT();
    //Original name: WB03-PRE-ANNUALIZ-RICOR
    private Wb03PreAnnualizRicor wb03PreAnnualizRicor = new Wb03PreAnnualizRicor();
    //Original name: WB03-PRE-CONT
    private Wb03PreCont wb03PreCont = new Wb03PreCont();
    //Original name: WB03-PRE-PP-INI
    private Wb03PrePpIni wb03PrePpIni = new Wb03PrePpIni();
    //Original name: WB03-RIS-PURA-T
    private Wb03RisPuraT wb03RisPuraT = new Wb03RisPuraT();
    //Original name: WB03-PROV-ACQ
    private Wb03ProvAcq wb03ProvAcq = new Wb03ProvAcq();
    //Original name: WB03-PROV-ACQ-RICOR
    private Wb03ProvAcqRicor wb03ProvAcqRicor = new Wb03ProvAcqRicor();
    //Original name: WB03-PROV-INC
    private Wb03ProvInc wb03ProvInc = new Wb03ProvInc();
    //Original name: WB03-CAR-ACQ-NON-SCON
    private Wb03CarAcqNonScon wb03CarAcqNonScon = new Wb03CarAcqNonScon();
    //Original name: WB03-OVER-COMM
    private Wb03OverComm wb03OverComm = new Wb03OverComm();
    //Original name: WB03-CAR-ACQ-PRECONTATO
    private Wb03CarAcqPrecontato wb03CarAcqPrecontato = new Wb03CarAcqPrecontato();
    //Original name: WB03-RIS-ACQ-T
    private Wb03RisAcqT wb03RisAcqT = new Wb03RisAcqT();
    //Original name: WB03-RIS-ZIL-T
    private Wb03RisZilT wb03RisZilT = new Wb03RisZilT();
    //Original name: WB03-CAR-GEST-NON-SCON
    private Wb03CarGestNonScon wb03CarGestNonScon = new Wb03CarGestNonScon();
    //Original name: WB03-CAR-GEST
    private Wb03CarGest wb03CarGest = new Wb03CarGest();
    //Original name: WB03-RIS-SPE-T
    private Wb03RisSpeT wb03RisSpeT = new Wb03RisSpeT();
    //Original name: WB03-CAR-INC-NON-SCON
    private Wb03CarIncNonScon wb03CarIncNonScon = new Wb03CarIncNonScon();
    //Original name: WB03-CAR-INC
    private Wb03CarInc wb03CarInc = new Wb03CarInc();
    //Original name: WB03-RIS-RISTORNI-CAP
    private Wb03RisRistorniCap wb03RisRistorniCap = new Wb03RisRistorniCap();
    //Original name: WB03-INTR-TECN
    private Wb03IntrTecn wb03IntrTecn = new Wb03IntrTecn();
    //Original name: WB03-CPT-RSH-MOR
    private Wb03CptRshMor wb03CptRshMor = new Wb03CptRshMor();
    //Original name: WB03-C-SUBRSH-T
    private Wb03CSubrshT wb03CSubrshT = new Wb03CSubrshT();
    //Original name: WB03-PRE-RSH-T
    private Wb03PreRshT wb03PreRshT = new Wb03PreRshT();
    //Original name: WB03-ALQ-MARG-RIS
    private Wb03AlqMargRis wb03AlqMargRis = new Wb03AlqMargRis();
    //Original name: WB03-ALQ-MARG-C-SUBRSH
    private Wb03AlqMargCSubrsh wb03AlqMargCSubrsh = new Wb03AlqMargCSubrsh();
    //Original name: WB03-TS-RENDTO-SPPR
    private Wb03TsRendtoSppr wb03TsRendtoSppr = new Wb03TsRendtoSppr();
    //Original name: WB03-TP-IAS
    private String wb03TpIas = DefaultValues.stringVal(Len.WB03_TP_IAS);
    //Original name: WB03-NS-QUO
    private Wb03NsQuo wb03NsQuo = new Wb03NsQuo();
    //Original name: WB03-TS-MEDIO
    private Wb03TsMedio wb03TsMedio = new Wb03TsMedio();
    //Original name: WB03-CPT-RIASTO
    private Wb03CptRiasto wb03CptRiasto = new Wb03CptRiasto();
    //Original name: WB03-PRE-RIASTO
    private Wb03PreRiasto wb03PreRiasto = new Wb03PreRiasto();
    //Original name: WB03-RIS-RIASTA
    private Wb03RisRiasta wb03RisRiasta = new Wb03RisRiasta();
    //Original name: WB03-CPT-RIASTO-ECC
    private Wb03CptRiastoEcc wb03CptRiastoEcc = new Wb03CptRiastoEcc();
    //Original name: WB03-PRE-RIASTO-ECC
    private Wb03PreRiastoEcc wb03PreRiastoEcc = new Wb03PreRiastoEcc();
    //Original name: WB03-RIS-RIASTA-ECC
    private Wb03RisRiastaEcc wb03RisRiastaEcc = new Wb03RisRiastaEcc();
    //Original name: WB03-COD-AGE
    private Wb03CodAge wb03CodAge = new Wb03CodAge();
    //Original name: WB03-COD-SUBAGE
    private Wb03CodSubage wb03CodSubage = new Wb03CodSubage();
    //Original name: WB03-COD-CAN
    private Wb03CodCan wb03CodCan = new Wb03CodCan();
    //Original name: WB03-IB-POLI
    private String wb03IbPoli = DefaultValues.stringVal(Len.WB03_IB_POLI);
    //Original name: WB03-IB-ADES
    private String wb03IbAdes = DefaultValues.stringVal(Len.WB03_IB_ADES);
    //Original name: WB03-IB-TRCH-DI-GAR
    private String wb03IbTrchDiGar = DefaultValues.stringVal(Len.WB03_IB_TRCH_DI_GAR);
    //Original name: WB03-TP-PRSTZ
    private String wb03TpPrstz = DefaultValues.stringVal(Len.WB03_TP_PRSTZ);
    //Original name: WB03-TP-TRASF
    private String wb03TpTrasf = DefaultValues.stringVal(Len.WB03_TP_TRASF);
    //Original name: WB03-PP-INVRIO-TARI
    private char wb03PpInvrioTari = DefaultValues.CHAR_VAL;
    //Original name: WB03-COEFF-OPZ-REN
    private Wb03CoeffOpzRen wb03CoeffOpzRen = new Wb03CoeffOpzRen();
    //Original name: WB03-COEFF-OPZ-CPT
    private Wb03CoeffOpzCpt wb03CoeffOpzCpt = new Wb03CoeffOpzCpt();
    //Original name: WB03-DUR-PAG-REN
    private Wb03DurPagRen wb03DurPagRen = new Wb03DurPagRen();
    //Original name: WB03-VLT
    private String wb03Vlt = DefaultValues.stringVal(Len.WB03_VLT);
    //Original name: WB03-RIS-MAT-CHIU-PREC
    private Wb03RisMatChiuPrec wb03RisMatChiuPrec = new Wb03RisMatChiuPrec();
    //Original name: WB03-COD-FND
    private String wb03CodFnd = DefaultValues.stringVal(Len.WB03_COD_FND);
    //Original name: WB03-PRSTZ-T
    private Wb03PrstzT wb03PrstzT = new Wb03PrstzT();
    //Original name: WB03-TS-TARI-DOV
    private Wb03TsTariDov wb03TsTariDov = new Wb03TsTariDov();
    //Original name: WB03-TS-TARI-SCON
    private Wb03TsTariScon wb03TsTariScon = new Wb03TsTariScon();
    //Original name: WB03-TS-PP
    private Wb03TsPp wb03TsPp = new Wb03TsPp();
    //Original name: WB03-COEFF-RIS-1-T
    private Wb03CoeffRis1T wb03CoeffRis1T = new Wb03CoeffRis1T();
    //Original name: WB03-COEFF-RIS-2-T
    private Wb03CoeffRis2T wb03CoeffRis2T = new Wb03CoeffRis2T();
    //Original name: WB03-ABB
    private Wb03Abb wb03Abb = new Wb03Abb();
    //Original name: WB03-TP-COASS
    private String wb03TpCoass = DefaultValues.stringVal(Len.WB03_TP_COASS);
    //Original name: WB03-TRAT-RIASS
    private String wb03TratRiass = DefaultValues.stringVal(Len.WB03_TRAT_RIASS);
    //Original name: WB03-TRAT-RIASS-ECC
    private String wb03TratRiassEcc = DefaultValues.stringVal(Len.WB03_TRAT_RIASS_ECC);
    //Original name: WB03-DS-OPER-SQL
    private char wb03DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WB03-DS-VER
    private int wb03DsVer = DefaultValues.INT_VAL;
    //Original name: WB03-DS-TS-CPTZ
    private long wb03DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WB03-DS-UTENTE
    private String wb03DsUtente = DefaultValues.stringVal(Len.WB03_DS_UTENTE);
    //Original name: WB03-DS-STATO-ELAB
    private char wb03DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WB03-TP-RGM-FISC
    private String wb03TpRgmFisc = DefaultValues.stringVal(Len.WB03_TP_RGM_FISC);
    //Original name: WB03-DUR-GAR-AA
    private Wb03DurGarAa wb03DurGarAa = new Wb03DurGarAa();
    //Original name: WB03-DUR-GAR-MM
    private Wb03DurGarMm wb03DurGarMm = new Wb03DurGarMm();
    //Original name: WB03-DUR-GAR-GG
    private Wb03DurGarGg wb03DurGarGg = new Wb03DurGarGg();
    //Original name: WB03-ANTIDUR-CALC-365
    private Wb03AntidurCalc365 wb03AntidurCalc365 = new Wb03AntidurCalc365();
    //Original name: WB03-COD-FISC-CNTR
    private String wb03CodFiscCntr = DefaultValues.stringVal(Len.WB03_COD_FISC_CNTR);
    //Original name: WB03-COD-FISC-ASSTO1
    private String wb03CodFiscAssto1 = DefaultValues.stringVal(Len.WB03_COD_FISC_ASSTO1);
    //Original name: WB03-COD-FISC-ASSTO2
    private String wb03CodFiscAssto2 = DefaultValues.stringVal(Len.WB03_COD_FISC_ASSTO2);
    //Original name: WB03-COD-FISC-ASSTO3
    private String wb03CodFiscAssto3 = DefaultValues.stringVal(Len.WB03_COD_FISC_ASSTO3);
    //Original name: WB03-CAUS-SCON
    private String wb03CausScon = DefaultValues.stringVal(Len.WB03_CAUS_SCON);
    //Original name: WB03-EMIT-TIT-OPZ
    private String wb03EmitTitOpz = DefaultValues.stringVal(Len.WB03_EMIT_TIT_OPZ);
    //Original name: WB03-QTZ-SP-Z-COUP-EMIS
    private Wb03QtzSpZCoupEmis wb03QtzSpZCoupEmis = new Wb03QtzSpZCoupEmis();
    //Original name: WB03-QTZ-SP-Z-OPZ-EMIS
    private Wb03QtzSpZOpzEmis wb03QtzSpZOpzEmis = new Wb03QtzSpZOpzEmis();
    //Original name: WB03-QTZ-SP-Z-COUP-DT-C
    private Wb03QtzSpZCoupDtC wb03QtzSpZCoupDtC = new Wb03QtzSpZCoupDtC();
    //Original name: WB03-QTZ-SP-Z-OPZ-DT-CA
    private Wb03QtzSpZOpzDtCa wb03QtzSpZOpzDtCa = new Wb03QtzSpZOpzDtCa();
    //Original name: WB03-QTZ-TOT-EMIS
    private Wb03QtzTotEmis wb03QtzTotEmis = new Wb03QtzTotEmis();
    //Original name: WB03-QTZ-TOT-DT-CALC
    private Wb03QtzTotDtCalc wb03QtzTotDtCalc = new Wb03QtzTotDtCalc();
    //Original name: WB03-QTZ-TOT-DT-ULT-BIL
    private Wb03QtzTotDtUltBil wb03QtzTotDtUltBil = new Wb03QtzTotDtUltBil();
    //Original name: WB03-DT-QTZ-EMIS
    private Wb03DtQtzEmis wb03DtQtzEmis = new Wb03DtQtzEmis();
    //Original name: WB03-PC-CAR-GEST
    private Wb03PcCarGest wb03PcCarGest = new Wb03PcCarGest();
    //Original name: WB03-PC-CAR-ACQ
    private Wb03PcCarAcq wb03PcCarAcq = new Wb03PcCarAcq();
    //Original name: WB03-IMP-CAR-CASO-MOR
    private Wb03ImpCarCasoMor wb03ImpCarCasoMor = new Wb03ImpCarCasoMor();
    //Original name: WB03-PC-CAR-MOR
    private Wb03PcCarMor wb03PcCarMor = new Wb03PcCarMor();
    //Original name: WB03-TP-VERS
    private char wb03TpVers = DefaultValues.CHAR_VAL;
    //Original name: WB03-FL-SWITCH
    private char wb03FlSwitch = DefaultValues.CHAR_VAL;
    //Original name: WB03-FL-IAS
    private char wb03FlIas = DefaultValues.CHAR_VAL;
    //Original name: WB03-DIR
    private Wb03Dir wb03Dir = new Wb03Dir();
    //Original name: WB03-TP-COP-CASO-MOR
    private String wb03TpCopCasoMor = DefaultValues.stringVal(Len.WB03_TP_COP_CASO_MOR);
    //Original name: WB03-MET-RISC-SPCL
    private Wb03MetRiscSpcl wb03MetRiscSpcl = new Wb03MetRiscSpcl();
    //Original name: WB03-TP-STAT-INVST
    private String wb03TpStatInvst = DefaultValues.stringVal(Len.WB03_TP_STAT_INVST);
    //Original name: WB03-COD-PRDT
    private Wb03CodPrdt wb03CodPrdt = new Wb03CodPrdt();
    //Original name: WB03-STAT-ASSTO-1
    private char wb03StatAssto1 = DefaultValues.CHAR_VAL;
    //Original name: WB03-STAT-ASSTO-2
    private char wb03StatAssto2 = DefaultValues.CHAR_VAL;
    //Original name: WB03-STAT-ASSTO-3
    private char wb03StatAssto3 = DefaultValues.CHAR_VAL;
    //Original name: WB03-CPT-ASSTO-INI-MOR
    private Wb03CptAsstoIniMor wb03CptAsstoIniMor = new Wb03CptAsstoIniMor();
    //Original name: WB03-TS-STAB-PRE
    private Wb03TsStabPre wb03TsStabPre = new Wb03TsStabPre();
    //Original name: WB03-DIR-EMIS
    private Wb03DirEmis wb03DirEmis = new Wb03DirEmis();
    //Original name: WB03-DT-INC-ULT-PRE
    private Wb03DtIncUltPre wb03DtIncUltPre = new Wb03DtIncUltPre();
    //Original name: WB03-STAT-TBGC-ASSTO-1
    private char wb03StatTbgcAssto1 = DefaultValues.CHAR_VAL;
    //Original name: WB03-STAT-TBGC-ASSTO-2
    private char wb03StatTbgcAssto2 = DefaultValues.CHAR_VAL;
    //Original name: WB03-STAT-TBGC-ASSTO-3
    private char wb03StatTbgcAssto3 = DefaultValues.CHAR_VAL;
    //Original name: WB03-FRAZ-DECR-CPT
    private Wb03FrazDecrCpt wb03FrazDecrCpt = new Wb03FrazDecrCpt();
    //Original name: WB03-PRE-PP-ULT
    private Wb03PrePpUlt wb03PrePpUlt = new Wb03PrePpUlt();
    //Original name: WB03-ACQ-EXP
    private Wb03AcqExp wb03AcqExp = new Wb03AcqExp();
    //Original name: WB03-REMUN-ASS
    private Wb03RemunAss wb03RemunAss = new Wb03RemunAss();
    //Original name: WB03-COMMIS-INTER
    private Wb03CommisInter wb03CommisInter = new Wb03CommisInter();
    //Original name: WB03-NUM-FINANZ
    private String wb03NumFinanz = DefaultValues.stringVal(Len.WB03_NUM_FINANZ);
    //Original name: WB03-TP-ACC-COMM
    private String wb03TpAccComm = DefaultValues.stringVal(Len.WB03_TP_ACC_COMM);
    //Original name: WB03-IB-ACC-COMM
    private String wb03IbAccComm = DefaultValues.stringVal(Len.WB03_IB_ACC_COMM);
    //Original name: WB03-RAMO-BILA
    private String wb03RamoBila = DefaultValues.stringVal(Len.WB03_RAMO_BILA);
    //Original name: WB03-CARZ
    private Wb03Carz wb03Carz = new Wb03Carz();

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wb03IdBilaTrchEstr = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_ID_BILA_TRCH_ESTR, 0);
        position += Len.WB03_ID_BILA_TRCH_ESTR;
        wb03CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_COD_COMP_ANIA, 0);
        position += Len.WB03_COD_COMP_ANIA;
        wb03IdRichEstrazMas = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.WB03_ID_RICH_ESTRAZ_MAS;
        wb03IdRichEstrazAgg.setWb03IdRichEstrazAggFromBuffer(buffer, position);
        position += Wb03IdRichEstrazAgg.Len.WB03_ID_RICH_ESTRAZ_AGG;
        wb03FlSimulazione = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03DtRis = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_DT_RIS, 0);
        position += Len.WB03_DT_RIS;
        wb03DtProduzione = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_DT_PRODUZIONE, 0);
        position += Len.WB03_DT_PRODUZIONE;
        wb03IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_ID_POLI, 0);
        position += Len.WB03_ID_POLI;
        wb03IdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_ID_ADES, 0);
        position += Len.WB03_ID_ADES;
        wb03IdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_ID_GAR, 0);
        position += Len.WB03_ID_GAR;
        wb03IdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_ID_TRCH_DI_GAR, 0);
        position += Len.WB03_ID_TRCH_DI_GAR;
        wb03TpFrmAssva = MarshalByte.readString(buffer, position, Len.WB03_TP_FRM_ASSVA);
        position += Len.WB03_TP_FRM_ASSVA;
        wb03TpRamoBila = MarshalByte.readString(buffer, position, Len.WB03_TP_RAMO_BILA);
        position += Len.WB03_TP_RAMO_BILA;
        wb03TpCalcRis = MarshalByte.readString(buffer, position, Len.WB03_TP_CALC_RIS);
        position += Len.WB03_TP_CALC_RIS;
        wb03CodRamo = MarshalByte.readString(buffer, position, Len.WB03_COD_RAMO);
        position += Len.WB03_COD_RAMO;
        wb03CodTari = MarshalByte.readString(buffer, position, Len.WB03_COD_TARI);
        position += Len.WB03_COD_TARI;
        wb03DtIniValTar.setWb03DtIniValTarFromBuffer(buffer, position);
        position += Wb03DtIniValTar.Len.WB03_DT_INI_VAL_TAR;
        wb03CodProd = MarshalByte.readString(buffer, position, Len.WB03_COD_PROD);
        position += Len.WB03_COD_PROD;
        wb03DtIniVldtProd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_DT_INI_VLDT_PROD, 0);
        position += Len.WB03_DT_INI_VLDT_PROD;
        wb03CodTariOrgn = MarshalByte.readString(buffer, position, Len.WB03_COD_TARI_ORGN);
        position += Len.WB03_COD_TARI_ORGN;
        wb03MinGartoT.setWb03MinGartoTFromBuffer(buffer, position);
        position += Wb03MinGartoT.Len.WB03_MIN_GARTO_T;
        wb03TpTari = MarshalByte.readString(buffer, position, Len.WB03_TP_TARI);
        position += Len.WB03_TP_TARI;
        wb03TpPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03TpAdegPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03TpRival = MarshalByte.readString(buffer, position, Len.WB03_TP_RIVAL);
        position += Len.WB03_TP_RIVAL;
        wb03FlDaTrasf = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03FlCarCont = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03FlPreDaRis = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03FlPreAgg = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03TpTrch = MarshalByte.readString(buffer, position, Len.WB03_TP_TRCH);
        position += Len.WB03_TP_TRCH;
        wb03TpTst = MarshalByte.readString(buffer, position, Len.WB03_TP_TST);
        position += Len.WB03_TP_TST;
        wb03CodConv = MarshalByte.readString(buffer, position, Len.WB03_COD_CONV);
        position += Len.WB03_COD_CONV;
        wb03DtDecorPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_DT_DECOR_POLI, 0);
        position += Len.WB03_DT_DECOR_POLI;
        wb03DtDecorAdes.setWb03DtDecorAdesFromBuffer(buffer, position);
        position += Wb03DtDecorAdes.Len.WB03_DT_DECOR_ADES;
        wb03DtDecorTrch = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_DT_DECOR_TRCH, 0);
        position += Len.WB03_DT_DECOR_TRCH;
        wb03DtEmisPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_DT_EMIS_POLI, 0);
        position += Len.WB03_DT_EMIS_POLI;
        wb03DtEmisTrch.setWb03DtEmisTrchFromBuffer(buffer, position);
        position += Wb03DtEmisTrch.Len.WB03_DT_EMIS_TRCH;
        wb03DtScadTrch.setWb03DtScadTrchFromBuffer(buffer, position);
        position += Wb03DtScadTrch.Len.WB03_DT_SCAD_TRCH;
        wb03DtScadIntmd.setWb03DtScadIntmdFromBuffer(buffer, position);
        position += Wb03DtScadIntmd.Len.WB03_DT_SCAD_INTMD;
        wb03DtScadPagPre.setWb03DtScadPagPreFromBuffer(buffer, position);
        position += Wb03DtScadPagPre.Len.WB03_DT_SCAD_PAG_PRE;
        wb03DtUltPrePag.setWb03DtUltPrePagFromBuffer(buffer, position);
        position += Wb03DtUltPrePag.Len.WB03_DT_ULT_PRE_PAG;
        wb03DtNasc1oAssto.setWb03DtNasc1oAsstoFromBuffer(buffer, position);
        position += Wb03DtNasc1oAssto.Len.WB03_DT_NASC1O_ASSTO;
        wb03Sex1oAssto = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03EtaAa1oAssto.setWb03EtaAa1oAsstoFromBuffer(buffer, position);
        position += Wb03EtaAa1oAssto.Len.WB03_ETA_AA1O_ASSTO;
        wb03EtaMm1oAssto.setWb03EtaMm1oAsstoFromBuffer(buffer, position);
        position += Wb03EtaMm1oAssto.Len.WB03_ETA_MM1O_ASSTO;
        wb03EtaRaggnDtCalc.setWb03EtaRaggnDtCalcFromBuffer(buffer, position);
        position += Wb03EtaRaggnDtCalc.Len.WB03_ETA_RAGGN_DT_CALC;
        wb03DurAa.setWb03DurAaFromBuffer(buffer, position);
        position += Wb03DurAa.Len.WB03_DUR_AA;
        wb03DurMm.setWb03DurMmFromBuffer(buffer, position);
        position += Wb03DurMm.Len.WB03_DUR_MM;
        wb03DurGg.setWb03DurGgFromBuffer(buffer, position);
        position += Wb03DurGg.Len.WB03_DUR_GG;
        wb03Dur1oPerAa.setWb03Dur1oPerAaFromBuffer(buffer, position);
        position += Wb03Dur1oPerAa.Len.WB03_DUR1O_PER_AA;
        wb03Dur1oPerMm.setWb03Dur1oPerMmFromBuffer(buffer, position);
        position += Wb03Dur1oPerMm.Len.WB03_DUR1O_PER_MM;
        wb03Dur1oPerGg.setWb03Dur1oPerGgFromBuffer(buffer, position);
        position += Wb03Dur1oPerGg.Len.WB03_DUR1O_PER_GG;
        wb03AntidurRicorPrec.setWb03AntidurRicorPrecFromBuffer(buffer, position);
        position += Wb03AntidurRicorPrec.Len.WB03_ANTIDUR_RICOR_PREC;
        wb03AntidurDtCalc.setWb03AntidurDtCalcFromBuffer(buffer, position);
        position += Wb03AntidurDtCalc.Len.WB03_ANTIDUR_DT_CALC;
        wb03DurResDtCalc.setWb03DurResDtCalcFromBuffer(buffer, position);
        position += Wb03DurResDtCalc.Len.WB03_DUR_RES_DT_CALC;
        wb03TpStatBusPoli = MarshalByte.readString(buffer, position, Len.WB03_TP_STAT_BUS_POLI);
        position += Len.WB03_TP_STAT_BUS_POLI;
        wb03TpCausPoli = MarshalByte.readString(buffer, position, Len.WB03_TP_CAUS_POLI);
        position += Len.WB03_TP_CAUS_POLI;
        wb03TpStatBusAdes = MarshalByte.readString(buffer, position, Len.WB03_TP_STAT_BUS_ADES);
        position += Len.WB03_TP_STAT_BUS_ADES;
        wb03TpCausAdes = MarshalByte.readString(buffer, position, Len.WB03_TP_CAUS_ADES);
        position += Len.WB03_TP_CAUS_ADES;
        wb03TpStatBusTrch = MarshalByte.readString(buffer, position, Len.WB03_TP_STAT_BUS_TRCH);
        position += Len.WB03_TP_STAT_BUS_TRCH;
        wb03TpCausTrch = MarshalByte.readString(buffer, position, Len.WB03_TP_CAUS_TRCH);
        position += Len.WB03_TP_CAUS_TRCH;
        wb03DtEffCambStat.setWb03DtEffCambStatFromBuffer(buffer, position);
        position += Wb03DtEffCambStat.Len.WB03_DT_EFF_CAMB_STAT;
        wb03DtEmisCambStat.setWb03DtEmisCambStatFromBuffer(buffer, position);
        position += Wb03DtEmisCambStat.Len.WB03_DT_EMIS_CAMB_STAT;
        wb03DtEffStab.setWb03DtEffStabFromBuffer(buffer, position);
        position += Wb03DtEffStab.Len.WB03_DT_EFF_STAB;
        wb03CptDtStab.setWb03CptDtStabFromBuffer(buffer, position);
        position += Wb03CptDtStab.Len.WB03_CPT_DT_STAB;
        wb03DtEffRidz.setWb03DtEffRidzFromBuffer(buffer, position);
        position += Wb03DtEffRidz.Len.WB03_DT_EFF_RIDZ;
        wb03DtEmisRidz.setWb03DtEmisRidzFromBuffer(buffer, position);
        position += Wb03DtEmisRidz.Len.WB03_DT_EMIS_RIDZ;
        wb03CptDtRidz.setWb03CptDtRidzFromBuffer(buffer, position);
        position += Wb03CptDtRidz.Len.WB03_CPT_DT_RIDZ;
        wb03Fraz.setWb03FrazFromBuffer(buffer, position);
        position += Wb03Fraz.Len.WB03_FRAZ;
        wb03DurPagPre.setWb03DurPagPreFromBuffer(buffer, position);
        position += Wb03DurPagPre.Len.WB03_DUR_PAG_PRE;
        wb03NumPrePatt.setWb03NumPrePattFromBuffer(buffer, position);
        position += Wb03NumPrePatt.Len.WB03_NUM_PRE_PATT;
        wb03FrazIniErogRen.setWb03FrazIniErogRenFromBuffer(buffer, position);
        position += Wb03FrazIniErogRen.Len.WB03_FRAZ_INI_EROG_REN;
        wb03AaRenCer.setWb03AaRenCerFromBuffer(buffer, position);
        position += Wb03AaRenCer.Len.WB03_AA_REN_CER;
        wb03RatRen.setWb03RatRenFromBuffer(buffer, position);
        position += Wb03RatRen.Len.WB03_RAT_REN;
        wb03CodDiv = MarshalByte.readString(buffer, position, Len.WB03_COD_DIV);
        position += Len.WB03_COD_DIV;
        wb03Riscpar.setWb03RiscparFromBuffer(buffer, position);
        position += Wb03Riscpar.Len.WB03_RISCPAR;
        wb03CumRiscpar.setWb03CumRiscparFromBuffer(buffer, position);
        position += Wb03CumRiscpar.Len.WB03_CUM_RISCPAR;
        wb03UltRm.setWb03UltRmFromBuffer(buffer, position);
        position += Wb03UltRm.Len.WB03_ULT_RM;
        wb03TsRendtoT.setWb03TsRendtoTFromBuffer(buffer, position);
        position += Wb03TsRendtoT.Len.WB03_TS_RENDTO_T;
        wb03AlqRetrT.setWb03AlqRetrTFromBuffer(buffer, position);
        position += Wb03AlqRetrT.Len.WB03_ALQ_RETR_T;
        wb03MinTrnutT.setWb03MinTrnutTFromBuffer(buffer, position);
        position += Wb03MinTrnutT.Len.WB03_MIN_TRNUT_T;
        wb03TsNetT.setWb03TsNetTFromBuffer(buffer, position);
        position += Wb03TsNetT.Len.WB03_TS_NET_T;
        wb03DtUltRival.setWb03DtUltRivalFromBuffer(buffer, position);
        position += Wb03DtUltRival.Len.WB03_DT_ULT_RIVAL;
        wb03PrstzIni.setWb03PrstzIniFromBuffer(buffer, position);
        position += Wb03PrstzIni.Len.WB03_PRSTZ_INI;
        wb03PrstzAggIni.setWb03PrstzAggIniFromBuffer(buffer, position);
        position += Wb03PrstzAggIni.Len.WB03_PRSTZ_AGG_INI;
        wb03PrstzAggUlt.setWb03PrstzAggUltFromBuffer(buffer, position);
        position += Wb03PrstzAggUlt.Len.WB03_PRSTZ_AGG_ULT;
        wb03Rappel.setWb03RappelFromBuffer(buffer, position);
        position += Wb03Rappel.Len.WB03_RAPPEL;
        wb03PrePattuitoIni.setWb03PrePattuitoIniFromBuffer(buffer, position);
        position += Wb03PrePattuitoIni.Len.WB03_PRE_PATTUITO_INI;
        wb03PreDovIni.setWb03PreDovIniFromBuffer(buffer, position);
        position += Wb03PreDovIni.Len.WB03_PRE_DOV_INI;
        wb03PreDovRivtoT.setWb03PreDovRivtoTFromBuffer(buffer, position);
        position += Wb03PreDovRivtoT.Len.WB03_PRE_DOV_RIVTO_T;
        wb03PreAnnualizRicor.setWb03PreAnnualizRicorFromBuffer(buffer, position);
        position += Wb03PreAnnualizRicor.Len.WB03_PRE_ANNUALIZ_RICOR;
        wb03PreCont.setWb03PreContFromBuffer(buffer, position);
        position += Wb03PreCont.Len.WB03_PRE_CONT;
        wb03PrePpIni.setWb03PrePpIniFromBuffer(buffer, position);
        position += Wb03PrePpIni.Len.WB03_PRE_PP_INI;
        wb03RisPuraT.setWb03RisPuraTFromBuffer(buffer, position);
        position += Wb03RisPuraT.Len.WB03_RIS_PURA_T;
        wb03ProvAcq.setWb03ProvAcqFromBuffer(buffer, position);
        position += Wb03ProvAcq.Len.WB03_PROV_ACQ;
        wb03ProvAcqRicor.setWb03ProvAcqRicorFromBuffer(buffer, position);
        position += Wb03ProvAcqRicor.Len.WB03_PROV_ACQ_RICOR;
        wb03ProvInc.setWb03ProvIncFromBuffer(buffer, position);
        position += Wb03ProvInc.Len.WB03_PROV_INC;
        wb03CarAcqNonScon.setWb03CarAcqNonSconFromBuffer(buffer, position);
        position += Wb03CarAcqNonScon.Len.WB03_CAR_ACQ_NON_SCON;
        wb03OverComm.setWb03OverCommFromBuffer(buffer, position);
        position += Wb03OverComm.Len.WB03_OVER_COMM;
        wb03CarAcqPrecontato.setWb03CarAcqPrecontatoFromBuffer(buffer, position);
        position += Wb03CarAcqPrecontato.Len.WB03_CAR_ACQ_PRECONTATO;
        wb03RisAcqT.setWb03RisAcqTFromBuffer(buffer, position);
        position += Wb03RisAcqT.Len.WB03_RIS_ACQ_T;
        wb03RisZilT.setWb03RisZilTFromBuffer(buffer, position);
        position += Wb03RisZilT.Len.WB03_RIS_ZIL_T;
        wb03CarGestNonScon.setWb03CarGestNonSconFromBuffer(buffer, position);
        position += Wb03CarGestNonScon.Len.WB03_CAR_GEST_NON_SCON;
        wb03CarGest.setWb03CarGestFromBuffer(buffer, position);
        position += Wb03CarGest.Len.WB03_CAR_GEST;
        wb03RisSpeT.setWb03RisSpeTFromBuffer(buffer, position);
        position += Wb03RisSpeT.Len.WB03_RIS_SPE_T;
        wb03CarIncNonScon.setWb03CarIncNonSconFromBuffer(buffer, position);
        position += Wb03CarIncNonScon.Len.WB03_CAR_INC_NON_SCON;
        wb03CarInc.setWb03CarIncFromBuffer(buffer, position);
        position += Wb03CarInc.Len.WB03_CAR_INC;
        wb03RisRistorniCap.setWb03RisRistorniCapFromBuffer(buffer, position);
        position += Wb03RisRistorniCap.Len.WB03_RIS_RISTORNI_CAP;
        wb03IntrTecn.setWb03IntrTecnFromBuffer(buffer, position);
        position += Wb03IntrTecn.Len.WB03_INTR_TECN;
        wb03CptRshMor.setWb03CptRshMorFromBuffer(buffer, position);
        position += Wb03CptRshMor.Len.WB03_CPT_RSH_MOR;
        wb03CSubrshT.setWb03CSubrshTFromBuffer(buffer, position);
        position += Wb03CSubrshT.Len.WB03_C_SUBRSH_T;
        wb03PreRshT.setWb03PreRshTFromBuffer(buffer, position);
        position += Wb03PreRshT.Len.WB03_PRE_RSH_T;
        wb03AlqMargRis.setWb03AlqMargRisFromBuffer(buffer, position);
        position += Wb03AlqMargRis.Len.WB03_ALQ_MARG_RIS;
        wb03AlqMargCSubrsh.setWb03AlqMargCSubrshFromBuffer(buffer, position);
        position += Wb03AlqMargCSubrsh.Len.WB03_ALQ_MARG_C_SUBRSH;
        wb03TsRendtoSppr.setWb03TsRendtoSpprFromBuffer(buffer, position);
        position += Wb03TsRendtoSppr.Len.WB03_TS_RENDTO_SPPR;
        wb03TpIas = MarshalByte.readString(buffer, position, Len.WB03_TP_IAS);
        position += Len.WB03_TP_IAS;
        wb03NsQuo.setWb03NsQuoFromBuffer(buffer, position);
        position += Wb03NsQuo.Len.WB03_NS_QUO;
        wb03TsMedio.setWb03TsMedioFromBuffer(buffer, position);
        position += Wb03TsMedio.Len.WB03_TS_MEDIO;
        wb03CptRiasto.setWb03CptRiastoFromBuffer(buffer, position);
        position += Wb03CptRiasto.Len.WB03_CPT_RIASTO;
        wb03PreRiasto.setWb03PreRiastoFromBuffer(buffer, position);
        position += Wb03PreRiasto.Len.WB03_PRE_RIASTO;
        wb03RisRiasta.setWb03RisRiastaFromBuffer(buffer, position);
        position += Wb03RisRiasta.Len.WB03_RIS_RIASTA;
        wb03CptRiastoEcc.setWb03CptRiastoEccFromBuffer(buffer, position);
        position += Wb03CptRiastoEcc.Len.WB03_CPT_RIASTO_ECC;
        wb03PreRiastoEcc.setWb03PreRiastoEccFromBuffer(buffer, position);
        position += Wb03PreRiastoEcc.Len.WB03_PRE_RIASTO_ECC;
        wb03RisRiastaEcc.setWb03RisRiastaEccFromBuffer(buffer, position);
        position += Wb03RisRiastaEcc.Len.WB03_RIS_RIASTA_ECC;
        wb03CodAge.setWb03CodAgeFromBuffer(buffer, position);
        position += Wb03CodAge.Len.WB03_COD_AGE;
        wb03CodSubage.setWb03CodSubageFromBuffer(buffer, position);
        position += Wb03CodSubage.Len.WB03_COD_SUBAGE;
        wb03CodCan.setWb03CodCanFromBuffer(buffer, position);
        position += Wb03CodCan.Len.WB03_COD_CAN;
        wb03IbPoli = MarshalByte.readString(buffer, position, Len.WB03_IB_POLI);
        position += Len.WB03_IB_POLI;
        wb03IbAdes = MarshalByte.readString(buffer, position, Len.WB03_IB_ADES);
        position += Len.WB03_IB_ADES;
        wb03IbTrchDiGar = MarshalByte.readString(buffer, position, Len.WB03_IB_TRCH_DI_GAR);
        position += Len.WB03_IB_TRCH_DI_GAR;
        wb03TpPrstz = MarshalByte.readString(buffer, position, Len.WB03_TP_PRSTZ);
        position += Len.WB03_TP_PRSTZ;
        wb03TpTrasf = MarshalByte.readString(buffer, position, Len.WB03_TP_TRASF);
        position += Len.WB03_TP_TRASF;
        wb03PpInvrioTari = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03CoeffOpzRen.setWb03CoeffOpzRenFromBuffer(buffer, position);
        position += Wb03CoeffOpzRen.Len.WB03_COEFF_OPZ_REN;
        wb03CoeffOpzCpt.setWb03CoeffOpzCptFromBuffer(buffer, position);
        position += Wb03CoeffOpzCpt.Len.WB03_COEFF_OPZ_CPT;
        wb03DurPagRen.setWb03DurPagRenFromBuffer(buffer, position);
        position += Wb03DurPagRen.Len.WB03_DUR_PAG_REN;
        wb03Vlt = MarshalByte.readString(buffer, position, Len.WB03_VLT);
        position += Len.WB03_VLT;
        wb03RisMatChiuPrec.setWb03RisMatChiuPrecFromBuffer(buffer, position);
        position += Wb03RisMatChiuPrec.Len.WB03_RIS_MAT_CHIU_PREC;
        wb03CodFnd = MarshalByte.readString(buffer, position, Len.WB03_COD_FND);
        position += Len.WB03_COD_FND;
        wb03PrstzT.setWb03PrstzTFromBuffer(buffer, position);
        position += Wb03PrstzT.Len.WB03_PRSTZ_T;
        wb03TsTariDov.setWb03TsTariDovFromBuffer(buffer, position);
        position += Wb03TsTariDov.Len.WB03_TS_TARI_DOV;
        wb03TsTariScon.setWb03TsTariSconFromBuffer(buffer, position);
        position += Wb03TsTariScon.Len.WB03_TS_TARI_SCON;
        wb03TsPp.setWb03TsPpFromBuffer(buffer, position);
        position += Wb03TsPp.Len.WB03_TS_PP;
        wb03CoeffRis1T.setWb03CoeffRis1TFromBuffer(buffer, position);
        position += Wb03CoeffRis1T.Len.WB03_COEFF_RIS1_T;
        wb03CoeffRis2T.setWb03CoeffRis2TFromBuffer(buffer, position);
        position += Wb03CoeffRis2T.Len.WB03_COEFF_RIS2_T;
        wb03Abb.setWb03AbbFromBuffer(buffer, position);
        position += Wb03Abb.Len.WB03_ABB;
        wb03TpCoass = MarshalByte.readString(buffer, position, Len.WB03_TP_COASS);
        position += Len.WB03_TP_COASS;
        wb03TratRiass = MarshalByte.readString(buffer, position, Len.WB03_TRAT_RIASS);
        position += Len.WB03_TRAT_RIASS;
        wb03TratRiassEcc = MarshalByte.readString(buffer, position, Len.WB03_TRAT_RIASS_ECC);
        position += Len.WB03_TRAT_RIASS_ECC;
        wb03DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB03_DS_VER, 0);
        position += Len.WB03_DS_VER;
        wb03DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WB03_DS_TS_CPTZ, 0);
        position += Len.WB03_DS_TS_CPTZ;
        wb03DsUtente = MarshalByte.readString(buffer, position, Len.WB03_DS_UTENTE);
        position += Len.WB03_DS_UTENTE;
        wb03DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03TpRgmFisc = MarshalByte.readString(buffer, position, Len.WB03_TP_RGM_FISC);
        position += Len.WB03_TP_RGM_FISC;
        wb03DurGarAa.setWb03DurGarAaFromBuffer(buffer, position);
        position += Wb03DurGarAa.Len.WB03_DUR_GAR_AA;
        wb03DurGarMm.setWb03DurGarMmFromBuffer(buffer, position);
        position += Wb03DurGarMm.Len.WB03_DUR_GAR_MM;
        wb03DurGarGg.setWb03DurGarGgFromBuffer(buffer, position);
        position += Wb03DurGarGg.Len.WB03_DUR_GAR_GG;
        wb03AntidurCalc365.setWb03AntidurCalc365FromBuffer(buffer, position);
        position += Wb03AntidurCalc365.Len.WB03_ANTIDUR_CALC365;
        wb03CodFiscCntr = MarshalByte.readString(buffer, position, Len.WB03_COD_FISC_CNTR);
        position += Len.WB03_COD_FISC_CNTR;
        wb03CodFiscAssto1 = MarshalByte.readString(buffer, position, Len.WB03_COD_FISC_ASSTO1);
        position += Len.WB03_COD_FISC_ASSTO1;
        wb03CodFiscAssto2 = MarshalByte.readString(buffer, position, Len.WB03_COD_FISC_ASSTO2);
        position += Len.WB03_COD_FISC_ASSTO2;
        wb03CodFiscAssto3 = MarshalByte.readString(buffer, position, Len.WB03_COD_FISC_ASSTO3);
        position += Len.WB03_COD_FISC_ASSTO3;
        wb03CausScon = MarshalByte.readString(buffer, position, Len.WB03_CAUS_SCON);
        position += Len.WB03_CAUS_SCON;
        wb03EmitTitOpz = MarshalByte.readString(buffer, position, Len.WB03_EMIT_TIT_OPZ);
        position += Len.WB03_EMIT_TIT_OPZ;
        wb03QtzSpZCoupEmis.setWb03QtzSpZCoupEmisFromBuffer(buffer, position);
        position += Wb03QtzSpZCoupEmis.Len.WB03_QTZ_SP_Z_COUP_EMIS;
        wb03QtzSpZOpzEmis.setWb03QtzSpZOpzEmisFromBuffer(buffer, position);
        position += Wb03QtzSpZOpzEmis.Len.WB03_QTZ_SP_Z_OPZ_EMIS;
        wb03QtzSpZCoupDtC.setWb03QtzSpZCoupDtCFromBuffer(buffer, position);
        position += Wb03QtzSpZCoupDtC.Len.WB03_QTZ_SP_Z_COUP_DT_C;
        wb03QtzSpZOpzDtCa.setWb03QtzSpZOpzDtCaFromBuffer(buffer, position);
        position += Wb03QtzSpZOpzDtCa.Len.WB03_QTZ_SP_Z_OPZ_DT_CA;
        wb03QtzTotEmis.setWb03QtzTotEmisFromBuffer(buffer, position);
        position += Wb03QtzTotEmis.Len.WB03_QTZ_TOT_EMIS;
        wb03QtzTotDtCalc.setWb03QtzTotDtCalcFromBuffer(buffer, position);
        position += Wb03QtzTotDtCalc.Len.WB03_QTZ_TOT_DT_CALC;
        wb03QtzTotDtUltBil.setWb03QtzTotDtUltBilFromBuffer(buffer, position);
        position += Wb03QtzTotDtUltBil.Len.WB03_QTZ_TOT_DT_ULT_BIL;
        wb03DtQtzEmis.setWb03DtQtzEmisFromBuffer(buffer, position);
        position += Wb03DtQtzEmis.Len.WB03_DT_QTZ_EMIS;
        wb03PcCarGest.setWb03PcCarGestFromBuffer(buffer, position);
        position += Wb03PcCarGest.Len.WB03_PC_CAR_GEST;
        wb03PcCarAcq.setWb03PcCarAcqFromBuffer(buffer, position);
        position += Wb03PcCarAcq.Len.WB03_PC_CAR_ACQ;
        wb03ImpCarCasoMor.setWb03ImpCarCasoMorFromBuffer(buffer, position);
        position += Wb03ImpCarCasoMor.Len.WB03_IMP_CAR_CASO_MOR;
        wb03PcCarMor.setWb03PcCarMorFromBuffer(buffer, position);
        position += Wb03PcCarMor.Len.WB03_PC_CAR_MOR;
        wb03TpVers = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03FlSwitch = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03FlIas = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03Dir.setWb03DirFromBuffer(buffer, position);
        position += Wb03Dir.Len.WB03_DIR;
        wb03TpCopCasoMor = MarshalByte.readString(buffer, position, Len.WB03_TP_COP_CASO_MOR);
        position += Len.WB03_TP_COP_CASO_MOR;
        wb03MetRiscSpcl.setWb03MetRiscSpclFromBuffer(buffer, position);
        position += Wb03MetRiscSpcl.Len.WB03_MET_RISC_SPCL;
        wb03TpStatInvst = MarshalByte.readString(buffer, position, Len.WB03_TP_STAT_INVST);
        position += Len.WB03_TP_STAT_INVST;
        wb03CodPrdt.setWb03CodPrdtFromBuffer(buffer, position);
        position += Wb03CodPrdt.Len.WB03_COD_PRDT;
        wb03StatAssto1 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03StatAssto2 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03StatAssto3 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03CptAsstoIniMor.setWb03CptAsstoIniMorFromBuffer(buffer, position);
        position += Wb03CptAsstoIniMor.Len.WB03_CPT_ASSTO_INI_MOR;
        wb03TsStabPre.setWb03TsStabPreFromBuffer(buffer, position);
        position += Wb03TsStabPre.Len.WB03_TS_STAB_PRE;
        wb03DirEmis.setWb03DirEmisFromBuffer(buffer, position);
        position += Wb03DirEmis.Len.WB03_DIR_EMIS;
        wb03DtIncUltPre.setWb03DtIncUltPreFromBuffer(buffer, position);
        position += Wb03DtIncUltPre.Len.WB03_DT_INC_ULT_PRE;
        wb03StatTbgcAssto1 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03StatTbgcAssto2 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03StatTbgcAssto3 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb03FrazDecrCpt.setWb03FrazDecrCptFromBuffer(buffer, position);
        position += Wb03FrazDecrCpt.Len.WB03_FRAZ_DECR_CPT;
        wb03PrePpUlt.setWb03PrePpUltFromBuffer(buffer, position);
        position += Wb03PrePpUlt.Len.WB03_PRE_PP_ULT;
        wb03AcqExp.setWb03AcqExpFromBuffer(buffer, position);
        position += Wb03AcqExp.Len.WB03_ACQ_EXP;
        wb03RemunAss.setWb03RemunAssFromBuffer(buffer, position);
        position += Wb03RemunAss.Len.WB03_REMUN_ASS;
        wb03CommisInter.setWb03CommisInterFromBuffer(buffer, position);
        position += Wb03CommisInter.Len.WB03_COMMIS_INTER;
        wb03NumFinanz = MarshalByte.readString(buffer, position, Len.WB03_NUM_FINANZ);
        position += Len.WB03_NUM_FINANZ;
        wb03TpAccComm = MarshalByte.readString(buffer, position, Len.WB03_TP_ACC_COMM);
        position += Len.WB03_TP_ACC_COMM;
        wb03IbAccComm = MarshalByte.readString(buffer, position, Len.WB03_IB_ACC_COMM);
        position += Len.WB03_IB_ACC_COMM;
        wb03RamoBila = MarshalByte.readString(buffer, position, Len.WB03_RAMO_BILA);
        position += Len.WB03_RAMO_BILA;
        wb03Carz.setWb03CarzFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wb03IdBilaTrchEstr, Len.Int.WB03_ID_BILA_TRCH_ESTR, 0);
        position += Len.WB03_ID_BILA_TRCH_ESTR;
        MarshalByte.writeIntAsPacked(buffer, position, wb03CodCompAnia, Len.Int.WB03_COD_COMP_ANIA, 0);
        position += Len.WB03_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wb03IdRichEstrazMas, Len.Int.WB03_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.WB03_ID_RICH_ESTRAZ_MAS;
        wb03IdRichEstrazAgg.getWb03IdRichEstrazAggAsBuffer(buffer, position);
        position += Wb03IdRichEstrazAgg.Len.WB03_ID_RICH_ESTRAZ_AGG;
        MarshalByte.writeChar(buffer, position, wb03FlSimulazione);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wb03DtRis, Len.Int.WB03_DT_RIS, 0);
        position += Len.WB03_DT_RIS;
        MarshalByte.writeIntAsPacked(buffer, position, wb03DtProduzione, Len.Int.WB03_DT_PRODUZIONE, 0);
        position += Len.WB03_DT_PRODUZIONE;
        MarshalByte.writeIntAsPacked(buffer, position, wb03IdPoli, Len.Int.WB03_ID_POLI, 0);
        position += Len.WB03_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wb03IdAdes, Len.Int.WB03_ID_ADES, 0);
        position += Len.WB03_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wb03IdGar, Len.Int.WB03_ID_GAR, 0);
        position += Len.WB03_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, wb03IdTrchDiGar, Len.Int.WB03_ID_TRCH_DI_GAR, 0);
        position += Len.WB03_ID_TRCH_DI_GAR;
        MarshalByte.writeString(buffer, position, wb03TpFrmAssva, Len.WB03_TP_FRM_ASSVA);
        position += Len.WB03_TP_FRM_ASSVA;
        MarshalByte.writeString(buffer, position, wb03TpRamoBila, Len.WB03_TP_RAMO_BILA);
        position += Len.WB03_TP_RAMO_BILA;
        MarshalByte.writeString(buffer, position, wb03TpCalcRis, Len.WB03_TP_CALC_RIS);
        position += Len.WB03_TP_CALC_RIS;
        MarshalByte.writeString(buffer, position, wb03CodRamo, Len.WB03_COD_RAMO);
        position += Len.WB03_COD_RAMO;
        MarshalByte.writeString(buffer, position, wb03CodTari, Len.WB03_COD_TARI);
        position += Len.WB03_COD_TARI;
        wb03DtIniValTar.getWb03DtIniValTarAsBuffer(buffer, position);
        position += Wb03DtIniValTar.Len.WB03_DT_INI_VAL_TAR;
        MarshalByte.writeString(buffer, position, wb03CodProd, Len.WB03_COD_PROD);
        position += Len.WB03_COD_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, wb03DtIniVldtProd, Len.Int.WB03_DT_INI_VLDT_PROD, 0);
        position += Len.WB03_DT_INI_VLDT_PROD;
        MarshalByte.writeString(buffer, position, wb03CodTariOrgn, Len.WB03_COD_TARI_ORGN);
        position += Len.WB03_COD_TARI_ORGN;
        wb03MinGartoT.getWb03MinGartoTAsBuffer(buffer, position);
        position += Wb03MinGartoT.Len.WB03_MIN_GARTO_T;
        MarshalByte.writeString(buffer, position, wb03TpTari, Len.WB03_TP_TARI);
        position += Len.WB03_TP_TARI;
        MarshalByte.writeChar(buffer, position, wb03TpPre);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wb03TpAdegPre);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wb03TpRival, Len.WB03_TP_RIVAL);
        position += Len.WB03_TP_RIVAL;
        MarshalByte.writeChar(buffer, position, wb03FlDaTrasf);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wb03FlCarCont);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wb03FlPreDaRis);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wb03FlPreAgg);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wb03TpTrch, Len.WB03_TP_TRCH);
        position += Len.WB03_TP_TRCH;
        MarshalByte.writeString(buffer, position, wb03TpTst, Len.WB03_TP_TST);
        position += Len.WB03_TP_TST;
        MarshalByte.writeString(buffer, position, wb03CodConv, Len.WB03_COD_CONV);
        position += Len.WB03_COD_CONV;
        MarshalByte.writeIntAsPacked(buffer, position, wb03DtDecorPoli, Len.Int.WB03_DT_DECOR_POLI, 0);
        position += Len.WB03_DT_DECOR_POLI;
        wb03DtDecorAdes.getWb03DtDecorAdesAsBuffer(buffer, position);
        position += Wb03DtDecorAdes.Len.WB03_DT_DECOR_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wb03DtDecorTrch, Len.Int.WB03_DT_DECOR_TRCH, 0);
        position += Len.WB03_DT_DECOR_TRCH;
        MarshalByte.writeIntAsPacked(buffer, position, wb03DtEmisPoli, Len.Int.WB03_DT_EMIS_POLI, 0);
        position += Len.WB03_DT_EMIS_POLI;
        wb03DtEmisTrch.getWb03DtEmisTrchAsBuffer(buffer, position);
        position += Wb03DtEmisTrch.Len.WB03_DT_EMIS_TRCH;
        wb03DtScadTrch.getWb03DtScadTrchAsBuffer(buffer, position);
        position += Wb03DtScadTrch.Len.WB03_DT_SCAD_TRCH;
        wb03DtScadIntmd.getWb03DtScadIntmdAsBuffer(buffer, position);
        position += Wb03DtScadIntmd.Len.WB03_DT_SCAD_INTMD;
        wb03DtScadPagPre.getWb03DtScadPagPreAsBuffer(buffer, position);
        position += Wb03DtScadPagPre.Len.WB03_DT_SCAD_PAG_PRE;
        wb03DtUltPrePag.getWb03DtUltPrePagAsBuffer(buffer, position);
        position += Wb03DtUltPrePag.Len.WB03_DT_ULT_PRE_PAG;
        wb03DtNasc1oAssto.getWb03DtNasc1oAsstoAsBuffer(buffer, position);
        position += Wb03DtNasc1oAssto.Len.WB03_DT_NASC1O_ASSTO;
        MarshalByte.writeChar(buffer, position, wb03Sex1oAssto);
        position += Types.CHAR_SIZE;
        wb03EtaAa1oAssto.getWb03EtaAa1oAsstoAsBuffer(buffer, position);
        position += Wb03EtaAa1oAssto.Len.WB03_ETA_AA1O_ASSTO;
        wb03EtaMm1oAssto.getWb03EtaMm1oAsstoAsBuffer(buffer, position);
        position += Wb03EtaMm1oAssto.Len.WB03_ETA_MM1O_ASSTO;
        wb03EtaRaggnDtCalc.getWb03EtaRaggnDtCalcAsBuffer(buffer, position);
        position += Wb03EtaRaggnDtCalc.Len.WB03_ETA_RAGGN_DT_CALC;
        wb03DurAa.getWb03DurAaAsBuffer(buffer, position);
        position += Wb03DurAa.Len.WB03_DUR_AA;
        wb03DurMm.getWb03DurMmAsBuffer(buffer, position);
        position += Wb03DurMm.Len.WB03_DUR_MM;
        wb03DurGg.getWb03DurGgAsBuffer(buffer, position);
        position += Wb03DurGg.Len.WB03_DUR_GG;
        wb03Dur1oPerAa.getWb03Dur1oPerAaAsBuffer(buffer, position);
        position += Wb03Dur1oPerAa.Len.WB03_DUR1O_PER_AA;
        wb03Dur1oPerMm.getWb03Dur1oPerMmAsBuffer(buffer, position);
        position += Wb03Dur1oPerMm.Len.WB03_DUR1O_PER_MM;
        wb03Dur1oPerGg.getWb03Dur1oPerGgAsBuffer(buffer, position);
        position += Wb03Dur1oPerGg.Len.WB03_DUR1O_PER_GG;
        wb03AntidurRicorPrec.getWb03AntidurRicorPrecAsBuffer(buffer, position);
        position += Wb03AntidurRicorPrec.Len.WB03_ANTIDUR_RICOR_PREC;
        wb03AntidurDtCalc.getWb03AntidurDtCalcAsBuffer(buffer, position);
        position += Wb03AntidurDtCalc.Len.WB03_ANTIDUR_DT_CALC;
        wb03DurResDtCalc.getWb03DurResDtCalcAsBuffer(buffer, position);
        position += Wb03DurResDtCalc.Len.WB03_DUR_RES_DT_CALC;
        MarshalByte.writeString(buffer, position, wb03TpStatBusPoli, Len.WB03_TP_STAT_BUS_POLI);
        position += Len.WB03_TP_STAT_BUS_POLI;
        MarshalByte.writeString(buffer, position, wb03TpCausPoli, Len.WB03_TP_CAUS_POLI);
        position += Len.WB03_TP_CAUS_POLI;
        MarshalByte.writeString(buffer, position, wb03TpStatBusAdes, Len.WB03_TP_STAT_BUS_ADES);
        position += Len.WB03_TP_STAT_BUS_ADES;
        MarshalByte.writeString(buffer, position, wb03TpCausAdes, Len.WB03_TP_CAUS_ADES);
        position += Len.WB03_TP_CAUS_ADES;
        MarshalByte.writeString(buffer, position, wb03TpStatBusTrch, Len.WB03_TP_STAT_BUS_TRCH);
        position += Len.WB03_TP_STAT_BUS_TRCH;
        MarshalByte.writeString(buffer, position, wb03TpCausTrch, Len.WB03_TP_CAUS_TRCH);
        position += Len.WB03_TP_CAUS_TRCH;
        wb03DtEffCambStat.getWb03DtEffCambStatAsBuffer(buffer, position);
        position += Wb03DtEffCambStat.Len.WB03_DT_EFF_CAMB_STAT;
        wb03DtEmisCambStat.getWb03DtEmisCambStatAsBuffer(buffer, position);
        position += Wb03DtEmisCambStat.Len.WB03_DT_EMIS_CAMB_STAT;
        wb03DtEffStab.getWb03DtEffStabAsBuffer(buffer, position);
        position += Wb03DtEffStab.Len.WB03_DT_EFF_STAB;
        wb03CptDtStab.getWb03CptDtStabAsBuffer(buffer, position);
        position += Wb03CptDtStab.Len.WB03_CPT_DT_STAB;
        wb03DtEffRidz.getWb03DtEffRidzAsBuffer(buffer, position);
        position += Wb03DtEffRidz.Len.WB03_DT_EFF_RIDZ;
        wb03DtEmisRidz.getWb03DtEmisRidzAsBuffer(buffer, position);
        position += Wb03DtEmisRidz.Len.WB03_DT_EMIS_RIDZ;
        wb03CptDtRidz.getWb03CptDtRidzAsBuffer(buffer, position);
        position += Wb03CptDtRidz.Len.WB03_CPT_DT_RIDZ;
        wb03Fraz.getWb03FrazAsBuffer(buffer, position);
        position += Wb03Fraz.Len.WB03_FRAZ;
        wb03DurPagPre.getWb03DurPagPreAsBuffer(buffer, position);
        position += Wb03DurPagPre.Len.WB03_DUR_PAG_PRE;
        wb03NumPrePatt.getWb03NumPrePattAsBuffer(buffer, position);
        position += Wb03NumPrePatt.Len.WB03_NUM_PRE_PATT;
        wb03FrazIniErogRen.getWb03FrazIniErogRenAsBuffer(buffer, position);
        position += Wb03FrazIniErogRen.Len.WB03_FRAZ_INI_EROG_REN;
        wb03AaRenCer.getWb03AaRenCerAsBuffer(buffer, position);
        position += Wb03AaRenCer.Len.WB03_AA_REN_CER;
        wb03RatRen.getWb03RatRenAsBuffer(buffer, position);
        position += Wb03RatRen.Len.WB03_RAT_REN;
        MarshalByte.writeString(buffer, position, wb03CodDiv, Len.WB03_COD_DIV);
        position += Len.WB03_COD_DIV;
        wb03Riscpar.getWb03RiscparAsBuffer(buffer, position);
        position += Wb03Riscpar.Len.WB03_RISCPAR;
        wb03CumRiscpar.getWb03CumRiscparAsBuffer(buffer, position);
        position += Wb03CumRiscpar.Len.WB03_CUM_RISCPAR;
        wb03UltRm.getWb03UltRmAsBuffer(buffer, position);
        position += Wb03UltRm.Len.WB03_ULT_RM;
        wb03TsRendtoT.getWb03TsRendtoTAsBuffer(buffer, position);
        position += Wb03TsRendtoT.Len.WB03_TS_RENDTO_T;
        wb03AlqRetrT.getWb03AlqRetrTAsBuffer(buffer, position);
        position += Wb03AlqRetrT.Len.WB03_ALQ_RETR_T;
        wb03MinTrnutT.getWb03MinTrnutTAsBuffer(buffer, position);
        position += Wb03MinTrnutT.Len.WB03_MIN_TRNUT_T;
        wb03TsNetT.getWb03TsNetTAsBuffer(buffer, position);
        position += Wb03TsNetT.Len.WB03_TS_NET_T;
        wb03DtUltRival.getWb03DtUltRivalAsBuffer(buffer, position);
        position += Wb03DtUltRival.Len.WB03_DT_ULT_RIVAL;
        wb03PrstzIni.getWb03PrstzIniAsBuffer(buffer, position);
        position += Wb03PrstzIni.Len.WB03_PRSTZ_INI;
        wb03PrstzAggIni.getWb03PrstzAggIniAsBuffer(buffer, position);
        position += Wb03PrstzAggIni.Len.WB03_PRSTZ_AGG_INI;
        wb03PrstzAggUlt.getWb03PrstzAggUltAsBuffer(buffer, position);
        position += Wb03PrstzAggUlt.Len.WB03_PRSTZ_AGG_ULT;
        wb03Rappel.getWb03RappelAsBuffer(buffer, position);
        position += Wb03Rappel.Len.WB03_RAPPEL;
        wb03PrePattuitoIni.getWb03PrePattuitoIniAsBuffer(buffer, position);
        position += Wb03PrePattuitoIni.Len.WB03_PRE_PATTUITO_INI;
        wb03PreDovIni.getWb03PreDovIniAsBuffer(buffer, position);
        position += Wb03PreDovIni.Len.WB03_PRE_DOV_INI;
        wb03PreDovRivtoT.getWb03PreDovRivtoTAsBuffer(buffer, position);
        position += Wb03PreDovRivtoT.Len.WB03_PRE_DOV_RIVTO_T;
        wb03PreAnnualizRicor.getWb03PreAnnualizRicorAsBuffer(buffer, position);
        position += Wb03PreAnnualizRicor.Len.WB03_PRE_ANNUALIZ_RICOR;
        wb03PreCont.getWb03PreContAsBuffer(buffer, position);
        position += Wb03PreCont.Len.WB03_PRE_CONT;
        wb03PrePpIni.getWb03PrePpIniAsBuffer(buffer, position);
        position += Wb03PrePpIni.Len.WB03_PRE_PP_INI;
        wb03RisPuraT.getWb03RisPuraTAsBuffer(buffer, position);
        position += Wb03RisPuraT.Len.WB03_RIS_PURA_T;
        wb03ProvAcq.getWb03ProvAcqAsBuffer(buffer, position);
        position += Wb03ProvAcq.Len.WB03_PROV_ACQ;
        wb03ProvAcqRicor.getWb03ProvAcqRicorAsBuffer(buffer, position);
        position += Wb03ProvAcqRicor.Len.WB03_PROV_ACQ_RICOR;
        wb03ProvInc.getWb03ProvIncAsBuffer(buffer, position);
        position += Wb03ProvInc.Len.WB03_PROV_INC;
        wb03CarAcqNonScon.getWb03CarAcqNonSconAsBuffer(buffer, position);
        position += Wb03CarAcqNonScon.Len.WB03_CAR_ACQ_NON_SCON;
        wb03OverComm.getWb03OverCommAsBuffer(buffer, position);
        position += Wb03OverComm.Len.WB03_OVER_COMM;
        wb03CarAcqPrecontato.getWb03CarAcqPrecontatoAsBuffer(buffer, position);
        position += Wb03CarAcqPrecontato.Len.WB03_CAR_ACQ_PRECONTATO;
        wb03RisAcqT.getWb03RisAcqTAsBuffer(buffer, position);
        position += Wb03RisAcqT.Len.WB03_RIS_ACQ_T;
        wb03RisZilT.getWb03RisZilTAsBuffer(buffer, position);
        position += Wb03RisZilT.Len.WB03_RIS_ZIL_T;
        wb03CarGestNonScon.getWb03CarGestNonSconAsBuffer(buffer, position);
        position += Wb03CarGestNonScon.Len.WB03_CAR_GEST_NON_SCON;
        wb03CarGest.getWb03CarGestAsBuffer(buffer, position);
        position += Wb03CarGest.Len.WB03_CAR_GEST;
        wb03RisSpeT.getWb03RisSpeTAsBuffer(buffer, position);
        position += Wb03RisSpeT.Len.WB03_RIS_SPE_T;
        wb03CarIncNonScon.getWb03CarIncNonSconAsBuffer(buffer, position);
        position += Wb03CarIncNonScon.Len.WB03_CAR_INC_NON_SCON;
        wb03CarInc.getWb03CarIncAsBuffer(buffer, position);
        position += Wb03CarInc.Len.WB03_CAR_INC;
        wb03RisRistorniCap.getWb03RisRistorniCapAsBuffer(buffer, position);
        position += Wb03RisRistorniCap.Len.WB03_RIS_RISTORNI_CAP;
        wb03IntrTecn.getWb03IntrTecnAsBuffer(buffer, position);
        position += Wb03IntrTecn.Len.WB03_INTR_TECN;
        wb03CptRshMor.getWb03CptRshMorAsBuffer(buffer, position);
        position += Wb03CptRshMor.Len.WB03_CPT_RSH_MOR;
        wb03CSubrshT.getWb03CSubrshTAsBuffer(buffer, position);
        position += Wb03CSubrshT.Len.WB03_C_SUBRSH_T;
        wb03PreRshT.getWb03PreRshTAsBuffer(buffer, position);
        position += Wb03PreRshT.Len.WB03_PRE_RSH_T;
        wb03AlqMargRis.getWb03AlqMargRisAsBuffer(buffer, position);
        position += Wb03AlqMargRis.Len.WB03_ALQ_MARG_RIS;
        wb03AlqMargCSubrsh.getWb03AlqMargCSubrshAsBuffer(buffer, position);
        position += Wb03AlqMargCSubrsh.Len.WB03_ALQ_MARG_C_SUBRSH;
        wb03TsRendtoSppr.getWb03TsRendtoSpprAsBuffer(buffer, position);
        position += Wb03TsRendtoSppr.Len.WB03_TS_RENDTO_SPPR;
        MarshalByte.writeString(buffer, position, wb03TpIas, Len.WB03_TP_IAS);
        position += Len.WB03_TP_IAS;
        wb03NsQuo.getWb03NsQuoAsBuffer(buffer, position);
        position += Wb03NsQuo.Len.WB03_NS_QUO;
        wb03TsMedio.getWb03TsMedioAsBuffer(buffer, position);
        position += Wb03TsMedio.Len.WB03_TS_MEDIO;
        wb03CptRiasto.getWb03CptRiastoAsBuffer(buffer, position);
        position += Wb03CptRiasto.Len.WB03_CPT_RIASTO;
        wb03PreRiasto.getWb03PreRiastoAsBuffer(buffer, position);
        position += Wb03PreRiasto.Len.WB03_PRE_RIASTO;
        wb03RisRiasta.getWb03RisRiastaAsBuffer(buffer, position);
        position += Wb03RisRiasta.Len.WB03_RIS_RIASTA;
        wb03CptRiastoEcc.getWb03CptRiastoEccAsBuffer(buffer, position);
        position += Wb03CptRiastoEcc.Len.WB03_CPT_RIASTO_ECC;
        wb03PreRiastoEcc.getWb03PreRiastoEccAsBuffer(buffer, position);
        position += Wb03PreRiastoEcc.Len.WB03_PRE_RIASTO_ECC;
        wb03RisRiastaEcc.getWb03RisRiastaEccAsBuffer(buffer, position);
        position += Wb03RisRiastaEcc.Len.WB03_RIS_RIASTA_ECC;
        wb03CodAge.getWb03CodAgeAsBuffer(buffer, position);
        position += Wb03CodAge.Len.WB03_COD_AGE;
        wb03CodSubage.getWb03CodSubageAsBuffer(buffer, position);
        position += Wb03CodSubage.Len.WB03_COD_SUBAGE;
        wb03CodCan.getWb03CodCanAsBuffer(buffer, position);
        position += Wb03CodCan.Len.WB03_COD_CAN;
        MarshalByte.writeString(buffer, position, wb03IbPoli, Len.WB03_IB_POLI);
        position += Len.WB03_IB_POLI;
        MarshalByte.writeString(buffer, position, wb03IbAdes, Len.WB03_IB_ADES);
        position += Len.WB03_IB_ADES;
        MarshalByte.writeString(buffer, position, wb03IbTrchDiGar, Len.WB03_IB_TRCH_DI_GAR);
        position += Len.WB03_IB_TRCH_DI_GAR;
        MarshalByte.writeString(buffer, position, wb03TpPrstz, Len.WB03_TP_PRSTZ);
        position += Len.WB03_TP_PRSTZ;
        MarshalByte.writeString(buffer, position, wb03TpTrasf, Len.WB03_TP_TRASF);
        position += Len.WB03_TP_TRASF;
        MarshalByte.writeChar(buffer, position, wb03PpInvrioTari);
        position += Types.CHAR_SIZE;
        wb03CoeffOpzRen.getWb03CoeffOpzRenAsBuffer(buffer, position);
        position += Wb03CoeffOpzRen.Len.WB03_COEFF_OPZ_REN;
        wb03CoeffOpzCpt.getWb03CoeffOpzCptAsBuffer(buffer, position);
        position += Wb03CoeffOpzCpt.Len.WB03_COEFF_OPZ_CPT;
        wb03DurPagRen.getWb03DurPagRenAsBuffer(buffer, position);
        position += Wb03DurPagRen.Len.WB03_DUR_PAG_REN;
        MarshalByte.writeString(buffer, position, wb03Vlt, Len.WB03_VLT);
        position += Len.WB03_VLT;
        wb03RisMatChiuPrec.getWb03RisMatChiuPrecAsBuffer(buffer, position);
        position += Wb03RisMatChiuPrec.Len.WB03_RIS_MAT_CHIU_PREC;
        MarshalByte.writeString(buffer, position, wb03CodFnd, Len.WB03_COD_FND);
        position += Len.WB03_COD_FND;
        wb03PrstzT.getWb03PrstzTAsBuffer(buffer, position);
        position += Wb03PrstzT.Len.WB03_PRSTZ_T;
        wb03TsTariDov.getWb03TsTariDovAsBuffer(buffer, position);
        position += Wb03TsTariDov.Len.WB03_TS_TARI_DOV;
        wb03TsTariScon.getWb03TsTariSconAsBuffer(buffer, position);
        position += Wb03TsTariScon.Len.WB03_TS_TARI_SCON;
        wb03TsPp.getWb03TsPpAsBuffer(buffer, position);
        position += Wb03TsPp.Len.WB03_TS_PP;
        wb03CoeffRis1T.getWb03CoeffRis1TAsBuffer(buffer, position);
        position += Wb03CoeffRis1T.Len.WB03_COEFF_RIS1_T;
        wb03CoeffRis2T.getWb03CoeffRis2TAsBuffer(buffer, position);
        position += Wb03CoeffRis2T.Len.WB03_COEFF_RIS2_T;
        wb03Abb.getWb03AbbAsBuffer(buffer, position);
        position += Wb03Abb.Len.WB03_ABB;
        MarshalByte.writeString(buffer, position, wb03TpCoass, Len.WB03_TP_COASS);
        position += Len.WB03_TP_COASS;
        MarshalByte.writeString(buffer, position, wb03TratRiass, Len.WB03_TRAT_RIASS);
        position += Len.WB03_TRAT_RIASS;
        MarshalByte.writeString(buffer, position, wb03TratRiassEcc, Len.WB03_TRAT_RIASS_ECC);
        position += Len.WB03_TRAT_RIASS_ECC;
        MarshalByte.writeChar(buffer, position, wb03DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wb03DsVer, Len.Int.WB03_DS_VER, 0);
        position += Len.WB03_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wb03DsTsCptz, Len.Int.WB03_DS_TS_CPTZ, 0);
        position += Len.WB03_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wb03DsUtente, Len.WB03_DS_UTENTE);
        position += Len.WB03_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wb03DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wb03TpRgmFisc, Len.WB03_TP_RGM_FISC);
        position += Len.WB03_TP_RGM_FISC;
        wb03DurGarAa.getWb03DurGarAaAsBuffer(buffer, position);
        position += Wb03DurGarAa.Len.WB03_DUR_GAR_AA;
        wb03DurGarMm.getWb03DurGarMmAsBuffer(buffer, position);
        position += Wb03DurGarMm.Len.WB03_DUR_GAR_MM;
        wb03DurGarGg.getWb03DurGarGgAsBuffer(buffer, position);
        position += Wb03DurGarGg.Len.WB03_DUR_GAR_GG;
        wb03AntidurCalc365.getWb03AntidurCalc365AsBuffer(buffer, position);
        position += Wb03AntidurCalc365.Len.WB03_ANTIDUR_CALC365;
        MarshalByte.writeString(buffer, position, wb03CodFiscCntr, Len.WB03_COD_FISC_CNTR);
        position += Len.WB03_COD_FISC_CNTR;
        MarshalByte.writeString(buffer, position, wb03CodFiscAssto1, Len.WB03_COD_FISC_ASSTO1);
        position += Len.WB03_COD_FISC_ASSTO1;
        MarshalByte.writeString(buffer, position, wb03CodFiscAssto2, Len.WB03_COD_FISC_ASSTO2);
        position += Len.WB03_COD_FISC_ASSTO2;
        MarshalByte.writeString(buffer, position, wb03CodFiscAssto3, Len.WB03_COD_FISC_ASSTO3);
        position += Len.WB03_COD_FISC_ASSTO3;
        MarshalByte.writeString(buffer, position, wb03CausScon, Len.WB03_CAUS_SCON);
        position += Len.WB03_CAUS_SCON;
        MarshalByte.writeString(buffer, position, wb03EmitTitOpz, Len.WB03_EMIT_TIT_OPZ);
        position += Len.WB03_EMIT_TIT_OPZ;
        wb03QtzSpZCoupEmis.getWb03QtzSpZCoupEmisAsBuffer(buffer, position);
        position += Wb03QtzSpZCoupEmis.Len.WB03_QTZ_SP_Z_COUP_EMIS;
        wb03QtzSpZOpzEmis.getWb03QtzSpZOpzEmisAsBuffer(buffer, position);
        position += Wb03QtzSpZOpzEmis.Len.WB03_QTZ_SP_Z_OPZ_EMIS;
        wb03QtzSpZCoupDtC.getWb03QtzSpZCoupDtCAsBuffer(buffer, position);
        position += Wb03QtzSpZCoupDtC.Len.WB03_QTZ_SP_Z_COUP_DT_C;
        wb03QtzSpZOpzDtCa.getWb03QtzSpZOpzDtCaAsBuffer(buffer, position);
        position += Wb03QtzSpZOpzDtCa.Len.WB03_QTZ_SP_Z_OPZ_DT_CA;
        wb03QtzTotEmis.getWb03QtzTotEmisAsBuffer(buffer, position);
        position += Wb03QtzTotEmis.Len.WB03_QTZ_TOT_EMIS;
        wb03QtzTotDtCalc.getWb03QtzTotDtCalcAsBuffer(buffer, position);
        position += Wb03QtzTotDtCalc.Len.WB03_QTZ_TOT_DT_CALC;
        wb03QtzTotDtUltBil.getWb03QtzTotDtUltBilAsBuffer(buffer, position);
        position += Wb03QtzTotDtUltBil.Len.WB03_QTZ_TOT_DT_ULT_BIL;
        wb03DtQtzEmis.getWb03DtQtzEmisAsBuffer(buffer, position);
        position += Wb03DtQtzEmis.Len.WB03_DT_QTZ_EMIS;
        wb03PcCarGest.getWb03PcCarGestAsBuffer(buffer, position);
        position += Wb03PcCarGest.Len.WB03_PC_CAR_GEST;
        wb03PcCarAcq.getWb03PcCarAcqAsBuffer(buffer, position);
        position += Wb03PcCarAcq.Len.WB03_PC_CAR_ACQ;
        wb03ImpCarCasoMor.getWb03ImpCarCasoMorAsBuffer(buffer, position);
        position += Wb03ImpCarCasoMor.Len.WB03_IMP_CAR_CASO_MOR;
        wb03PcCarMor.getWb03PcCarMorAsBuffer(buffer, position);
        position += Wb03PcCarMor.Len.WB03_PC_CAR_MOR;
        MarshalByte.writeChar(buffer, position, wb03TpVers);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wb03FlSwitch);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wb03FlIas);
        position += Types.CHAR_SIZE;
        wb03Dir.getWb03DirAsBuffer(buffer, position);
        position += Wb03Dir.Len.WB03_DIR;
        MarshalByte.writeString(buffer, position, wb03TpCopCasoMor, Len.WB03_TP_COP_CASO_MOR);
        position += Len.WB03_TP_COP_CASO_MOR;
        wb03MetRiscSpcl.getWb03MetRiscSpclAsBuffer(buffer, position);
        position += Wb03MetRiscSpcl.Len.WB03_MET_RISC_SPCL;
        MarshalByte.writeString(buffer, position, wb03TpStatInvst, Len.WB03_TP_STAT_INVST);
        position += Len.WB03_TP_STAT_INVST;
        wb03CodPrdt.getWb03CodPrdtAsBuffer(buffer, position);
        position += Wb03CodPrdt.Len.WB03_COD_PRDT;
        MarshalByte.writeChar(buffer, position, wb03StatAssto1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wb03StatAssto2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wb03StatAssto3);
        position += Types.CHAR_SIZE;
        wb03CptAsstoIniMor.getWb03CptAsstoIniMorAsBuffer(buffer, position);
        position += Wb03CptAsstoIniMor.Len.WB03_CPT_ASSTO_INI_MOR;
        wb03TsStabPre.getWb03TsStabPreAsBuffer(buffer, position);
        position += Wb03TsStabPre.Len.WB03_TS_STAB_PRE;
        wb03DirEmis.getWb03DirEmisAsBuffer(buffer, position);
        position += Wb03DirEmis.Len.WB03_DIR_EMIS;
        wb03DtIncUltPre.getWb03DtIncUltPreAsBuffer(buffer, position);
        position += Wb03DtIncUltPre.Len.WB03_DT_INC_ULT_PRE;
        MarshalByte.writeChar(buffer, position, wb03StatTbgcAssto1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wb03StatTbgcAssto2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wb03StatTbgcAssto3);
        position += Types.CHAR_SIZE;
        wb03FrazDecrCpt.getWb03FrazDecrCptAsBuffer(buffer, position);
        position += Wb03FrazDecrCpt.Len.WB03_FRAZ_DECR_CPT;
        wb03PrePpUlt.getWb03PrePpUltAsBuffer(buffer, position);
        position += Wb03PrePpUlt.Len.WB03_PRE_PP_ULT;
        wb03AcqExp.getWb03AcqExpAsBuffer(buffer, position);
        position += Wb03AcqExp.Len.WB03_ACQ_EXP;
        wb03RemunAss.getWb03RemunAssAsBuffer(buffer, position);
        position += Wb03RemunAss.Len.WB03_REMUN_ASS;
        wb03CommisInter.getWb03CommisInterAsBuffer(buffer, position);
        position += Wb03CommisInter.Len.WB03_COMMIS_INTER;
        MarshalByte.writeString(buffer, position, wb03NumFinanz, Len.WB03_NUM_FINANZ);
        position += Len.WB03_NUM_FINANZ;
        MarshalByte.writeString(buffer, position, wb03TpAccComm, Len.WB03_TP_ACC_COMM);
        position += Len.WB03_TP_ACC_COMM;
        MarshalByte.writeString(buffer, position, wb03IbAccComm, Len.WB03_IB_ACC_COMM);
        position += Len.WB03_IB_ACC_COMM;
        MarshalByte.writeString(buffer, position, wb03RamoBila, Len.WB03_RAMO_BILA);
        position += Len.WB03_RAMO_BILA;
        wb03Carz.getWb03CarzAsBuffer(buffer, position);
        return buffer;
    }

    public void setWb03IdBilaTrchEstr(int wb03IdBilaTrchEstr) {
        this.wb03IdBilaTrchEstr = wb03IdBilaTrchEstr;
    }

    public int getWb03IdBilaTrchEstr() {
        return this.wb03IdBilaTrchEstr;
    }

    public void setWb03CodCompAnia(int wb03CodCompAnia) {
        this.wb03CodCompAnia = wb03CodCompAnia;
    }

    public int getWb03CodCompAnia() {
        return this.wb03CodCompAnia;
    }

    public void setWb03IdRichEstrazMas(int wb03IdRichEstrazMas) {
        this.wb03IdRichEstrazMas = wb03IdRichEstrazMas;
    }

    public int getWb03IdRichEstrazMas() {
        return this.wb03IdRichEstrazMas;
    }

    public void setWb03FlSimulazione(char wb03FlSimulazione) {
        this.wb03FlSimulazione = wb03FlSimulazione;
    }

    public char getWb03FlSimulazione() {
        return this.wb03FlSimulazione;
    }

    public void setWb03DtRis(int wb03DtRis) {
        this.wb03DtRis = wb03DtRis;
    }

    public int getWb03DtRis() {
        return this.wb03DtRis;
    }

    public void setWb03DtProduzione(int wb03DtProduzione) {
        this.wb03DtProduzione = wb03DtProduzione;
    }

    public int getWb03DtProduzione() {
        return this.wb03DtProduzione;
    }

    public void setWb03IdPoli(int wb03IdPoli) {
        this.wb03IdPoli = wb03IdPoli;
    }

    public int getWb03IdPoli() {
        return this.wb03IdPoli;
    }

    public void setWb03IdAdes(int wb03IdAdes) {
        this.wb03IdAdes = wb03IdAdes;
    }

    public int getWb03IdAdes() {
        return this.wb03IdAdes;
    }

    public void setWb03IdGar(int wb03IdGar) {
        this.wb03IdGar = wb03IdGar;
    }

    public int getWb03IdGar() {
        return this.wb03IdGar;
    }

    public void setWb03IdTrchDiGar(int wb03IdTrchDiGar) {
        this.wb03IdTrchDiGar = wb03IdTrchDiGar;
    }

    public int getWb03IdTrchDiGar() {
        return this.wb03IdTrchDiGar;
    }

    public void setWb03TpFrmAssva(String wb03TpFrmAssva) {
        this.wb03TpFrmAssva = Functions.subString(wb03TpFrmAssva, Len.WB03_TP_FRM_ASSVA);
    }

    public String getWb03TpFrmAssva() {
        return this.wb03TpFrmAssva;
    }

    public void setWb03TpRamoBila(String wb03TpRamoBila) {
        this.wb03TpRamoBila = Functions.subString(wb03TpRamoBila, Len.WB03_TP_RAMO_BILA);
    }

    public String getWb03TpRamoBila() {
        return this.wb03TpRamoBila;
    }

    public void setWb03TpCalcRis(String wb03TpCalcRis) {
        this.wb03TpCalcRis = Functions.subString(wb03TpCalcRis, Len.WB03_TP_CALC_RIS);
    }

    public String getWb03TpCalcRis() {
        return this.wb03TpCalcRis;
    }

    public void setWb03CodRamo(String wb03CodRamo) {
        this.wb03CodRamo = Functions.subString(wb03CodRamo, Len.WB03_COD_RAMO);
    }

    public String getWb03CodRamo() {
        return this.wb03CodRamo;
    }

    public void setWb03CodTari(String wb03CodTari) {
        this.wb03CodTari = Functions.subString(wb03CodTari, Len.WB03_COD_TARI);
    }

    public String getWb03CodTari() {
        return this.wb03CodTari;
    }

    public void setWb03CodProd(String wb03CodProd) {
        this.wb03CodProd = Functions.subString(wb03CodProd, Len.WB03_COD_PROD);
    }

    public String getWb03CodProd() {
        return this.wb03CodProd;
    }

    public String getWb03CodProdFormatted() {
        return Functions.padBlanks(getWb03CodProd(), Len.WB03_COD_PROD);
    }

    public void setWb03DtIniVldtProd(int wb03DtIniVldtProd) {
        this.wb03DtIniVldtProd = wb03DtIniVldtProd;
    }

    public int getWb03DtIniVldtProd() {
        return this.wb03DtIniVldtProd;
    }

    public void setWb03CodTariOrgn(String wb03CodTariOrgn) {
        this.wb03CodTariOrgn = Functions.subString(wb03CodTariOrgn, Len.WB03_COD_TARI_ORGN);
    }

    public String getWb03CodTariOrgn() {
        return this.wb03CodTariOrgn;
    }

    public String getWb03CodTariOrgnFormatted() {
        return Functions.padBlanks(getWb03CodTariOrgn(), Len.WB03_COD_TARI_ORGN);
    }

    public void setWb03TpTari(String wb03TpTari) {
        this.wb03TpTari = Functions.subString(wb03TpTari, Len.WB03_TP_TARI);
    }

    public String getWb03TpTari() {
        return this.wb03TpTari;
    }

    public String getWb03TpTariFormatted() {
        return Functions.padBlanks(getWb03TpTari(), Len.WB03_TP_TARI);
    }

    public void setWb03TpPre(char wb03TpPre) {
        this.wb03TpPre = wb03TpPre;
    }

    public void setWb03TpPreFormatted(String wb03TpPre) {
        setWb03TpPre(Functions.charAt(wb03TpPre, Types.CHAR_SIZE));
    }

    public char getWb03TpPre() {
        return this.wb03TpPre;
    }

    public void setWb03TpAdegPre(char wb03TpAdegPre) {
        this.wb03TpAdegPre = wb03TpAdegPre;
    }

    public char getWb03TpAdegPre() {
        return this.wb03TpAdegPre;
    }

    public void setWb03TpRival(String wb03TpRival) {
        this.wb03TpRival = Functions.subString(wb03TpRival, Len.WB03_TP_RIVAL);
    }

    public String getWb03TpRival() {
        return this.wb03TpRival;
    }

    public String getWb03TpRivalFormatted() {
        return Functions.padBlanks(getWb03TpRival(), Len.WB03_TP_RIVAL);
    }

    public void setWb03FlDaTrasf(char wb03FlDaTrasf) {
        this.wb03FlDaTrasf = wb03FlDaTrasf;
    }

    public void setWb03FlDaTrasfFormatted(String wb03FlDaTrasf) {
        setWb03FlDaTrasf(Functions.charAt(wb03FlDaTrasf, Types.CHAR_SIZE));
    }

    public char getWb03FlDaTrasf() {
        return this.wb03FlDaTrasf;
    }

    public void setWb03FlCarCont(char wb03FlCarCont) {
        this.wb03FlCarCont = wb03FlCarCont;
    }

    public char getWb03FlCarCont() {
        return this.wb03FlCarCont;
    }

    public void setWb03FlPreDaRis(char wb03FlPreDaRis) {
        this.wb03FlPreDaRis = wb03FlPreDaRis;
    }

    public char getWb03FlPreDaRis() {
        return this.wb03FlPreDaRis;
    }

    public void setWb03FlPreAgg(char wb03FlPreAgg) {
        this.wb03FlPreAgg = wb03FlPreAgg;
    }

    public void setWb03FlPreAggFormatted(String wb03FlPreAgg) {
        setWb03FlPreAgg(Functions.charAt(wb03FlPreAgg, Types.CHAR_SIZE));
    }

    public char getWb03FlPreAgg() {
        return this.wb03FlPreAgg;
    }

    public void setWb03TpTrch(String wb03TpTrch) {
        this.wb03TpTrch = Functions.subString(wb03TpTrch, Len.WB03_TP_TRCH);
    }

    public String getWb03TpTrch() {
        return this.wb03TpTrch;
    }

    public String getWb03TpTrchFormatted() {
        return Functions.padBlanks(getWb03TpTrch(), Len.WB03_TP_TRCH);
    }

    public void setWb03TpTst(String wb03TpTst) {
        this.wb03TpTst = Functions.subString(wb03TpTst, Len.WB03_TP_TST);
    }

    public String getWb03TpTst() {
        return this.wb03TpTst;
    }

    public String getWb03TpTstFormatted() {
        return Functions.padBlanks(getWb03TpTst(), Len.WB03_TP_TST);
    }

    public void setWb03CodConv(String wb03CodConv) {
        this.wb03CodConv = Functions.subString(wb03CodConv, Len.WB03_COD_CONV);
    }

    public String getWb03CodConv() {
        return this.wb03CodConv;
    }

    public String getWb03CodConvFormatted() {
        return Functions.padBlanks(getWb03CodConv(), Len.WB03_COD_CONV);
    }

    public void setWb03DtDecorPoli(int wb03DtDecorPoli) {
        this.wb03DtDecorPoli = wb03DtDecorPoli;
    }

    public int getWb03DtDecorPoli() {
        return this.wb03DtDecorPoli;
    }

    public void setWb03DtDecorTrch(int wb03DtDecorTrch) {
        this.wb03DtDecorTrch = wb03DtDecorTrch;
    }

    public int getWb03DtDecorTrch() {
        return this.wb03DtDecorTrch;
    }

    public void setWb03DtEmisPoli(int wb03DtEmisPoli) {
        this.wb03DtEmisPoli = wb03DtEmisPoli;
    }

    public int getWb03DtEmisPoli() {
        return this.wb03DtEmisPoli;
    }

    public void setWb03Sex1oAssto(char wb03Sex1oAssto) {
        this.wb03Sex1oAssto = wb03Sex1oAssto;
    }

    public char getWb03Sex1oAssto() {
        return this.wb03Sex1oAssto;
    }

    public void setWb03TpStatBusPoli(String wb03TpStatBusPoli) {
        this.wb03TpStatBusPoli = Functions.subString(wb03TpStatBusPoli, Len.WB03_TP_STAT_BUS_POLI);
    }

    public String getWb03TpStatBusPoli() {
        return this.wb03TpStatBusPoli;
    }

    public void setWb03TpCausPoli(String wb03TpCausPoli) {
        this.wb03TpCausPoli = Functions.subString(wb03TpCausPoli, Len.WB03_TP_CAUS_POLI);
    }

    public String getWb03TpCausPoli() {
        return this.wb03TpCausPoli;
    }

    public void setWb03TpStatBusAdes(String wb03TpStatBusAdes) {
        this.wb03TpStatBusAdes = Functions.subString(wb03TpStatBusAdes, Len.WB03_TP_STAT_BUS_ADES);
    }

    public String getWb03TpStatBusAdes() {
        return this.wb03TpStatBusAdes;
    }

    public void setWb03TpCausAdes(String wb03TpCausAdes) {
        this.wb03TpCausAdes = Functions.subString(wb03TpCausAdes, Len.WB03_TP_CAUS_ADES);
    }

    public String getWb03TpCausAdes() {
        return this.wb03TpCausAdes;
    }

    public void setWb03TpStatBusTrch(String wb03TpStatBusTrch) {
        this.wb03TpStatBusTrch = Functions.subString(wb03TpStatBusTrch, Len.WB03_TP_STAT_BUS_TRCH);
    }

    public String getWb03TpStatBusTrch() {
        return this.wb03TpStatBusTrch;
    }

    public void setWb03TpCausTrch(String wb03TpCausTrch) {
        this.wb03TpCausTrch = Functions.subString(wb03TpCausTrch, Len.WB03_TP_CAUS_TRCH);
    }

    public String getWb03TpCausTrch() {
        return this.wb03TpCausTrch;
    }

    public void setWb03CodDiv(String wb03CodDiv) {
        this.wb03CodDiv = Functions.subString(wb03CodDiv, Len.WB03_COD_DIV);
    }

    public String getWb03CodDiv() {
        return this.wb03CodDiv;
    }

    public String getWb03CodDivFormatted() {
        return Functions.padBlanks(getWb03CodDiv(), Len.WB03_COD_DIV);
    }

    public void setWb03TpIas(String wb03TpIas) {
        this.wb03TpIas = Functions.subString(wb03TpIas, Len.WB03_TP_IAS);
    }

    public String getWb03TpIas() {
        return this.wb03TpIas;
    }

    public String getWb03TpIasFormatted() {
        return Functions.padBlanks(getWb03TpIas(), Len.WB03_TP_IAS);
    }

    public void setWb03IbPoli(String wb03IbPoli) {
        this.wb03IbPoli = Functions.subString(wb03IbPoli, Len.WB03_IB_POLI);
    }

    public String getWb03IbPoli() {
        return this.wb03IbPoli;
    }

    public void setWb03IbAdes(String wb03IbAdes) {
        this.wb03IbAdes = Functions.subString(wb03IbAdes, Len.WB03_IB_ADES);
    }

    public String getWb03IbAdes() {
        return this.wb03IbAdes;
    }

    public void setWb03IbTrchDiGar(String wb03IbTrchDiGar) {
        this.wb03IbTrchDiGar = Functions.subString(wb03IbTrchDiGar, Len.WB03_IB_TRCH_DI_GAR);
    }

    public String getWb03IbTrchDiGar() {
        return this.wb03IbTrchDiGar;
    }

    public void setWb03TpPrstz(String wb03TpPrstz) {
        this.wb03TpPrstz = Functions.subString(wb03TpPrstz, Len.WB03_TP_PRSTZ);
    }

    public String getWb03TpPrstz() {
        return this.wb03TpPrstz;
    }

    public String getWb03TpPrstzFormatted() {
        return Functions.padBlanks(getWb03TpPrstz(), Len.WB03_TP_PRSTZ);
    }

    public void setWb03TpTrasf(String wb03TpTrasf) {
        this.wb03TpTrasf = Functions.subString(wb03TpTrasf, Len.WB03_TP_TRASF);
    }

    public String getWb03TpTrasf() {
        return this.wb03TpTrasf;
    }

    public String getWb03TpTrasfFormatted() {
        return Functions.padBlanks(getWb03TpTrasf(), Len.WB03_TP_TRASF);
    }

    public void setWb03PpInvrioTari(char wb03PpInvrioTari) {
        this.wb03PpInvrioTari = wb03PpInvrioTari;
    }

    public char getWb03PpInvrioTari() {
        return this.wb03PpInvrioTari;
    }

    public void setWb03Vlt(String wb03Vlt) {
        this.wb03Vlt = Functions.subString(wb03Vlt, Len.WB03_VLT);
    }

    public String getWb03Vlt() {
        return this.wb03Vlt;
    }

    public String getWb03VltFormatted() {
        return Functions.padBlanks(getWb03Vlt(), Len.WB03_VLT);
    }

    public void setWb03CodFnd(String wb03CodFnd) {
        this.wb03CodFnd = Functions.subString(wb03CodFnd, Len.WB03_COD_FND);
    }

    public String getWb03CodFnd() {
        return this.wb03CodFnd;
    }

    public String getWb03CodFndFormatted() {
        return Functions.padBlanks(getWb03CodFnd(), Len.WB03_COD_FND);
    }

    public void setWb03TpCoass(String wb03TpCoass) {
        this.wb03TpCoass = Functions.subString(wb03TpCoass, Len.WB03_TP_COASS);
    }

    public String getWb03TpCoass() {
        return this.wb03TpCoass;
    }

    public String getWb03TpCoassFormatted() {
        return Functions.padBlanks(getWb03TpCoass(), Len.WB03_TP_COASS);
    }

    public void setWb03TratRiass(String wb03TratRiass) {
        this.wb03TratRiass = Functions.subString(wb03TratRiass, Len.WB03_TRAT_RIASS);
    }

    public String getWb03TratRiass() {
        return this.wb03TratRiass;
    }

    public String getWb03TratRiassFormatted() {
        return Functions.padBlanks(getWb03TratRiass(), Len.WB03_TRAT_RIASS);
    }

    public void setWb03TratRiassEcc(String wb03TratRiassEcc) {
        this.wb03TratRiassEcc = Functions.subString(wb03TratRiassEcc, Len.WB03_TRAT_RIASS_ECC);
    }

    public String getWb03TratRiassEcc() {
        return this.wb03TratRiassEcc;
    }

    public String getWb03TratRiassEccFormatted() {
        return Functions.padBlanks(getWb03TratRiassEcc(), Len.WB03_TRAT_RIASS_ECC);
    }

    public void setWb03DsOperSql(char wb03DsOperSql) {
        this.wb03DsOperSql = wb03DsOperSql;
    }

    public char getWb03DsOperSql() {
        return this.wb03DsOperSql;
    }

    public void setWb03DsVer(int wb03DsVer) {
        this.wb03DsVer = wb03DsVer;
    }

    public int getWb03DsVer() {
        return this.wb03DsVer;
    }

    public void setWb03DsTsCptz(long wb03DsTsCptz) {
        this.wb03DsTsCptz = wb03DsTsCptz;
    }

    public long getWb03DsTsCptz() {
        return this.wb03DsTsCptz;
    }

    public void setWb03DsUtente(String wb03DsUtente) {
        this.wb03DsUtente = Functions.subString(wb03DsUtente, Len.WB03_DS_UTENTE);
    }

    public String getWb03DsUtente() {
        return this.wb03DsUtente;
    }

    public void setWb03DsStatoElab(char wb03DsStatoElab) {
        this.wb03DsStatoElab = wb03DsStatoElab;
    }

    public char getWb03DsStatoElab() {
        return this.wb03DsStatoElab;
    }

    public void setWb03TpRgmFisc(String wb03TpRgmFisc) {
        this.wb03TpRgmFisc = Functions.subString(wb03TpRgmFisc, Len.WB03_TP_RGM_FISC);
    }

    public String getWb03TpRgmFisc() {
        return this.wb03TpRgmFisc;
    }

    public String getWb03TpRgmFiscFormatted() {
        return Functions.padBlanks(getWb03TpRgmFisc(), Len.WB03_TP_RGM_FISC);
    }

    public void setWb03CodFiscCntr(String wb03CodFiscCntr) {
        this.wb03CodFiscCntr = Functions.subString(wb03CodFiscCntr, Len.WB03_COD_FISC_CNTR);
    }

    public String getWb03CodFiscCntr() {
        return this.wb03CodFiscCntr;
    }

    public String getWb03CodFiscCntrFormatted() {
        return Functions.padBlanks(getWb03CodFiscCntr(), Len.WB03_COD_FISC_CNTR);
    }

    public void setWb03CodFiscAssto1(String wb03CodFiscAssto1) {
        this.wb03CodFiscAssto1 = Functions.subString(wb03CodFiscAssto1, Len.WB03_COD_FISC_ASSTO1);
    }

    public String getWb03CodFiscAssto1() {
        return this.wb03CodFiscAssto1;
    }

    public String getWb03CodFiscAssto1Formatted() {
        return Functions.padBlanks(getWb03CodFiscAssto1(), Len.WB03_COD_FISC_ASSTO1);
    }

    public void setWb03CodFiscAssto2(String wb03CodFiscAssto2) {
        this.wb03CodFiscAssto2 = Functions.subString(wb03CodFiscAssto2, Len.WB03_COD_FISC_ASSTO2);
    }

    public String getWb03CodFiscAssto2() {
        return this.wb03CodFiscAssto2;
    }

    public String getWb03CodFiscAssto2Formatted() {
        return Functions.padBlanks(getWb03CodFiscAssto2(), Len.WB03_COD_FISC_ASSTO2);
    }

    public void setWb03CodFiscAssto3(String wb03CodFiscAssto3) {
        this.wb03CodFiscAssto3 = Functions.subString(wb03CodFiscAssto3, Len.WB03_COD_FISC_ASSTO3);
    }

    public String getWb03CodFiscAssto3() {
        return this.wb03CodFiscAssto3;
    }

    public String getWb03CodFiscAssto3Formatted() {
        return Functions.padBlanks(getWb03CodFiscAssto3(), Len.WB03_COD_FISC_ASSTO3);
    }

    public void setWb03CausScon(String wb03CausScon) {
        this.wb03CausScon = Functions.subString(wb03CausScon, Len.WB03_CAUS_SCON);
    }

    public String getWb03CausScon() {
        return this.wb03CausScon;
    }

    public void setWb03EmitTitOpz(String wb03EmitTitOpz) {
        this.wb03EmitTitOpz = Functions.subString(wb03EmitTitOpz, Len.WB03_EMIT_TIT_OPZ);
    }

    public String getWb03EmitTitOpz() {
        return this.wb03EmitTitOpz;
    }

    public void setWb03TpVers(char wb03TpVers) {
        this.wb03TpVers = wb03TpVers;
    }

    public char getWb03TpVers() {
        return this.wb03TpVers;
    }

    public void setWb03FlSwitch(char wb03FlSwitch) {
        this.wb03FlSwitch = wb03FlSwitch;
    }

    public char getWb03FlSwitch() {
        return this.wb03FlSwitch;
    }

    public void setWb03FlIas(char wb03FlIas) {
        this.wb03FlIas = wb03FlIas;
    }

    public char getWb03FlIas() {
        return this.wb03FlIas;
    }

    public void setWb03TpCopCasoMor(String wb03TpCopCasoMor) {
        this.wb03TpCopCasoMor = Functions.subString(wb03TpCopCasoMor, Len.WB03_TP_COP_CASO_MOR);
    }

    public String getWb03TpCopCasoMor() {
        return this.wb03TpCopCasoMor;
    }

    public String getWb03TpCopCasoMorFormatted() {
        return Functions.padBlanks(getWb03TpCopCasoMor(), Len.WB03_TP_COP_CASO_MOR);
    }

    public void setWb03TpStatInvst(String wb03TpStatInvst) {
        this.wb03TpStatInvst = Functions.subString(wb03TpStatInvst, Len.WB03_TP_STAT_INVST);
    }

    public String getWb03TpStatInvst() {
        return this.wb03TpStatInvst;
    }

    public String getWb03TpStatInvstFormatted() {
        return Functions.padBlanks(getWb03TpStatInvst(), Len.WB03_TP_STAT_INVST);
    }

    public void setWb03StatAssto1(char wb03StatAssto1) {
        this.wb03StatAssto1 = wb03StatAssto1;
    }

    public char getWb03StatAssto1() {
        return this.wb03StatAssto1;
    }

    public void setWb03StatAssto2(char wb03StatAssto2) {
        this.wb03StatAssto2 = wb03StatAssto2;
    }

    public char getWb03StatAssto2() {
        return this.wb03StatAssto2;
    }

    public void setWb03StatAssto3(char wb03StatAssto3) {
        this.wb03StatAssto3 = wb03StatAssto3;
    }

    public char getWb03StatAssto3() {
        return this.wb03StatAssto3;
    }

    public void setWb03StatTbgcAssto1(char wb03StatTbgcAssto1) {
        this.wb03StatTbgcAssto1 = wb03StatTbgcAssto1;
    }

    public void setWb03StatTbgcAssto1Formatted(String wb03StatTbgcAssto1) {
        setWb03StatTbgcAssto1(Functions.charAt(wb03StatTbgcAssto1, Types.CHAR_SIZE));
    }

    public char getWb03StatTbgcAssto1() {
        return this.wb03StatTbgcAssto1;
    }

    public void setWb03StatTbgcAssto2(char wb03StatTbgcAssto2) {
        this.wb03StatTbgcAssto2 = wb03StatTbgcAssto2;
    }

    public void setWb03StatTbgcAssto2Formatted(String wb03StatTbgcAssto2) {
        setWb03StatTbgcAssto2(Functions.charAt(wb03StatTbgcAssto2, Types.CHAR_SIZE));
    }

    public char getWb03StatTbgcAssto2() {
        return this.wb03StatTbgcAssto2;
    }

    public void setWb03StatTbgcAssto3(char wb03StatTbgcAssto3) {
        this.wb03StatTbgcAssto3 = wb03StatTbgcAssto3;
    }

    public void setWb03StatTbgcAssto3Formatted(String wb03StatTbgcAssto3) {
        setWb03StatTbgcAssto3(Functions.charAt(wb03StatTbgcAssto3, Types.CHAR_SIZE));
    }

    public char getWb03StatTbgcAssto3() {
        return this.wb03StatTbgcAssto3;
    }

    public void setWb03NumFinanz(String wb03NumFinanz) {
        this.wb03NumFinanz = Functions.subString(wb03NumFinanz, Len.WB03_NUM_FINANZ);
    }

    public String getWb03NumFinanz() {
        return this.wb03NumFinanz;
    }

    public void setWb03TpAccComm(String wb03TpAccComm) {
        this.wb03TpAccComm = Functions.subString(wb03TpAccComm, Len.WB03_TP_ACC_COMM);
    }

    public String getWb03TpAccComm() {
        return this.wb03TpAccComm;
    }

    public String getWb03TpAccCommFormatted() {
        return Functions.padBlanks(getWb03TpAccComm(), Len.WB03_TP_ACC_COMM);
    }

    public void setWb03IbAccComm(String wb03IbAccComm) {
        this.wb03IbAccComm = Functions.subString(wb03IbAccComm, Len.WB03_IB_ACC_COMM);
    }

    public String getWb03IbAccComm() {
        return this.wb03IbAccComm;
    }

    public void setWb03RamoBila(String wb03RamoBila) {
        this.wb03RamoBila = Functions.subString(wb03RamoBila, Len.WB03_RAMO_BILA);
    }

    public String getWb03RamoBila() {
        return this.wb03RamoBila;
    }

    public Wb03AaRenCer getWb03AaRenCer() {
        return wb03AaRenCer;
    }

    public Wb03Abb getWb03Abb() {
        return wb03Abb;
    }

    public Wb03AcqExp getWb03AcqExp() {
        return wb03AcqExp;
    }

    public Wb03AlqMargCSubrsh getWb03AlqMargCSubrsh() {
        return wb03AlqMargCSubrsh;
    }

    public Wb03AlqMargRis getWb03AlqMargRis() {
        return wb03AlqMargRis;
    }

    public Wb03AlqRetrT getWb03AlqRetrT() {
        return wb03AlqRetrT;
    }

    public Wb03AntidurCalc365 getWb03AntidurCalc365() {
        return wb03AntidurCalc365;
    }

    public Wb03AntidurDtCalc getWb03AntidurDtCalc() {
        return wb03AntidurDtCalc;
    }

    public Wb03AntidurRicorPrec getWb03AntidurRicorPrec() {
        return wb03AntidurRicorPrec;
    }

    public Wb03CSubrshT getWb03CSubrshT() {
        return wb03CSubrshT;
    }

    public Wb03CarAcqNonScon getWb03CarAcqNonScon() {
        return wb03CarAcqNonScon;
    }

    public Wb03CarAcqPrecontato getWb03CarAcqPrecontato() {
        return wb03CarAcqPrecontato;
    }

    public Wb03CarGest getWb03CarGest() {
        return wb03CarGest;
    }

    public Wb03CarGestNonScon getWb03CarGestNonScon() {
        return wb03CarGestNonScon;
    }

    public Wb03CarInc getWb03CarInc() {
        return wb03CarInc;
    }

    public Wb03CarIncNonScon getWb03CarIncNonScon() {
        return wb03CarIncNonScon;
    }

    public Wb03Carz getWb03Carz() {
        return wb03Carz;
    }

    public Wb03CodAge getWb03CodAge() {
        return wb03CodAge;
    }

    public Wb03CodCan getWb03CodCan() {
        return wb03CodCan;
    }

    public Wb03CodPrdt getWb03CodPrdt() {
        return wb03CodPrdt;
    }

    public Wb03CodSubage getWb03CodSubage() {
        return wb03CodSubage;
    }

    public Wb03CoeffOpzCpt getWb03CoeffOpzCpt() {
        return wb03CoeffOpzCpt;
    }

    public Wb03CoeffOpzRen getWb03CoeffOpzRen() {
        return wb03CoeffOpzRen;
    }

    public Wb03CoeffRis1T getWb03CoeffRis1T() {
        return wb03CoeffRis1T;
    }

    public Wb03CoeffRis2T getWb03CoeffRis2T() {
        return wb03CoeffRis2T;
    }

    public Wb03CommisInter getWb03CommisInter() {
        return wb03CommisInter;
    }

    public Wb03CptAsstoIniMor getWb03CptAsstoIniMor() {
        return wb03CptAsstoIniMor;
    }

    public Wb03CptDtRidz getWb03CptDtRidz() {
        return wb03CptDtRidz;
    }

    public Wb03CptDtStab getWb03CptDtStab() {
        return wb03CptDtStab;
    }

    public Wb03CptRiasto getWb03CptRiasto() {
        return wb03CptRiasto;
    }

    public Wb03CptRiastoEcc getWb03CptRiastoEcc() {
        return wb03CptRiastoEcc;
    }

    public Wb03CptRshMor getWb03CptRshMor() {
        return wb03CptRshMor;
    }

    public Wb03CumRiscpar getWb03CumRiscpar() {
        return wb03CumRiscpar;
    }

    public Wb03Dir getWb03Dir() {
        return wb03Dir;
    }

    public Wb03DirEmis getWb03DirEmis() {
        return wb03DirEmis;
    }

    public Wb03DtDecorAdes getWb03DtDecorAdes() {
        return wb03DtDecorAdes;
    }

    public Wb03DtEffCambStat getWb03DtEffCambStat() {
        return wb03DtEffCambStat;
    }

    public Wb03DtEffRidz getWb03DtEffRidz() {
        return wb03DtEffRidz;
    }

    public Wb03DtEffStab getWb03DtEffStab() {
        return wb03DtEffStab;
    }

    public Wb03DtEmisCambStat getWb03DtEmisCambStat() {
        return wb03DtEmisCambStat;
    }

    public Wb03DtEmisRidz getWb03DtEmisRidz() {
        return wb03DtEmisRidz;
    }

    public Wb03DtEmisTrch getWb03DtEmisTrch() {
        return wb03DtEmisTrch;
    }

    public Wb03DtIncUltPre getWb03DtIncUltPre() {
        return wb03DtIncUltPre;
    }

    public Wb03DtIniValTar getWb03DtIniValTar() {
        return wb03DtIniValTar;
    }

    public Wb03DtNasc1oAssto getWb03DtNasc1oAssto() {
        return wb03DtNasc1oAssto;
    }

    public Wb03DtQtzEmis getWb03DtQtzEmis() {
        return wb03DtQtzEmis;
    }

    public Wb03DtScadIntmd getWb03DtScadIntmd() {
        return wb03DtScadIntmd;
    }

    public Wb03DtScadPagPre getWb03DtScadPagPre() {
        return wb03DtScadPagPre;
    }

    public Wb03DtScadTrch getWb03DtScadTrch() {
        return wb03DtScadTrch;
    }

    public Wb03DtUltPrePag getWb03DtUltPrePag() {
        return wb03DtUltPrePag;
    }

    public Wb03DtUltRival getWb03DtUltRival() {
        return wb03DtUltRival;
    }

    public Wb03Dur1oPerAa getWb03Dur1oPerAa() {
        return wb03Dur1oPerAa;
    }

    public Wb03Dur1oPerGg getWb03Dur1oPerGg() {
        return wb03Dur1oPerGg;
    }

    public Wb03Dur1oPerMm getWb03Dur1oPerMm() {
        return wb03Dur1oPerMm;
    }

    public Wb03DurAa getWb03DurAa() {
        return wb03DurAa;
    }

    public Wb03DurGarAa getWb03DurGarAa() {
        return wb03DurGarAa;
    }

    public Wb03DurGarGg getWb03DurGarGg() {
        return wb03DurGarGg;
    }

    public Wb03DurGarMm getWb03DurGarMm() {
        return wb03DurGarMm;
    }

    public Wb03DurGg getWb03DurGg() {
        return wb03DurGg;
    }

    public Wb03DurMm getWb03DurMm() {
        return wb03DurMm;
    }

    public Wb03DurPagPre getWb03DurPagPre() {
        return wb03DurPagPre;
    }

    public Wb03DurPagRen getWb03DurPagRen() {
        return wb03DurPagRen;
    }

    public Wb03DurResDtCalc getWb03DurResDtCalc() {
        return wb03DurResDtCalc;
    }

    public Wb03EtaAa1oAssto getWb03EtaAa1oAssto() {
        return wb03EtaAa1oAssto;
    }

    public Wb03EtaMm1oAssto getWb03EtaMm1oAssto() {
        return wb03EtaMm1oAssto;
    }

    public Wb03EtaRaggnDtCalc getWb03EtaRaggnDtCalc() {
        return wb03EtaRaggnDtCalc;
    }

    public Wb03Fraz getWb03Fraz() {
        return wb03Fraz;
    }

    public Wb03FrazDecrCpt getWb03FrazDecrCpt() {
        return wb03FrazDecrCpt;
    }

    public Wb03FrazIniErogRen getWb03FrazIniErogRen() {
        return wb03FrazIniErogRen;
    }

    public Wb03IdRichEstrazAgg getWb03IdRichEstrazAgg() {
        return wb03IdRichEstrazAgg;
    }

    public Wb03ImpCarCasoMor getWb03ImpCarCasoMor() {
        return wb03ImpCarCasoMor;
    }

    public Wb03IntrTecn getWb03IntrTecn() {
        return wb03IntrTecn;
    }

    public Wb03MetRiscSpcl getWb03MetRiscSpcl() {
        return wb03MetRiscSpcl;
    }

    public Wb03MinGartoT getWb03MinGartoT() {
        return wb03MinGartoT;
    }

    public Wb03MinTrnutT getWb03MinTrnutT() {
        return wb03MinTrnutT;
    }

    public Wb03NsQuo getWb03NsQuo() {
        return wb03NsQuo;
    }

    public Wb03NumPrePatt getWb03NumPrePatt() {
        return wb03NumPrePatt;
    }

    public Wb03OverComm getWb03OverComm() {
        return wb03OverComm;
    }

    public Wb03PcCarAcq getWb03PcCarAcq() {
        return wb03PcCarAcq;
    }

    public Wb03PcCarGest getWb03PcCarGest() {
        return wb03PcCarGest;
    }

    public Wb03PcCarMor getWb03PcCarMor() {
        return wb03PcCarMor;
    }

    public Wb03PreAnnualizRicor getWb03PreAnnualizRicor() {
        return wb03PreAnnualizRicor;
    }

    public Wb03PreCont getWb03PreCont() {
        return wb03PreCont;
    }

    public Wb03PreDovIni getWb03PreDovIni() {
        return wb03PreDovIni;
    }

    public Wb03PreDovRivtoT getWb03PreDovRivtoT() {
        return wb03PreDovRivtoT;
    }

    public Wb03PrePattuitoIni getWb03PrePattuitoIni() {
        return wb03PrePattuitoIni;
    }

    public Wb03PrePpIni getWb03PrePpIni() {
        return wb03PrePpIni;
    }

    public Wb03PrePpUlt getWb03PrePpUlt() {
        return wb03PrePpUlt;
    }

    public Wb03PreRiasto getWb03PreRiasto() {
        return wb03PreRiasto;
    }

    public Wb03PreRiastoEcc getWb03PreRiastoEcc() {
        return wb03PreRiastoEcc;
    }

    public Wb03PreRshT getWb03PreRshT() {
        return wb03PreRshT;
    }

    public Wb03ProvAcq getWb03ProvAcq() {
        return wb03ProvAcq;
    }

    public Wb03ProvAcqRicor getWb03ProvAcqRicor() {
        return wb03ProvAcqRicor;
    }

    public Wb03ProvInc getWb03ProvInc() {
        return wb03ProvInc;
    }

    public Wb03PrstzAggIni getWb03PrstzAggIni() {
        return wb03PrstzAggIni;
    }

    public Wb03PrstzAggUlt getWb03PrstzAggUlt() {
        return wb03PrstzAggUlt;
    }

    public Wb03PrstzIni getWb03PrstzIni() {
        return wb03PrstzIni;
    }

    public Wb03PrstzT getWb03PrstzT() {
        return wb03PrstzT;
    }

    public Wb03QtzSpZCoupDtC getWb03QtzSpZCoupDtC() {
        return wb03QtzSpZCoupDtC;
    }

    public Wb03QtzSpZCoupEmis getWb03QtzSpZCoupEmis() {
        return wb03QtzSpZCoupEmis;
    }

    public Wb03QtzSpZOpzDtCa getWb03QtzSpZOpzDtCa() {
        return wb03QtzSpZOpzDtCa;
    }

    public Wb03QtzSpZOpzEmis getWb03QtzSpZOpzEmis() {
        return wb03QtzSpZOpzEmis;
    }

    public Wb03QtzTotDtCalc getWb03QtzTotDtCalc() {
        return wb03QtzTotDtCalc;
    }

    public Wb03QtzTotDtUltBil getWb03QtzTotDtUltBil() {
        return wb03QtzTotDtUltBil;
    }

    public Wb03QtzTotEmis getWb03QtzTotEmis() {
        return wb03QtzTotEmis;
    }

    public Wb03Rappel getWb03Rappel() {
        return wb03Rappel;
    }

    public Wb03RatRen getWb03RatRen() {
        return wb03RatRen;
    }

    public Wb03RemunAss getWb03RemunAss() {
        return wb03RemunAss;
    }

    public Wb03RisAcqT getWb03RisAcqT() {
        return wb03RisAcqT;
    }

    public Wb03RisMatChiuPrec getWb03RisMatChiuPrec() {
        return wb03RisMatChiuPrec;
    }

    public Wb03RisPuraT getWb03RisPuraT() {
        return wb03RisPuraT;
    }

    public Wb03RisRiasta getWb03RisRiasta() {
        return wb03RisRiasta;
    }

    public Wb03RisRiastaEcc getWb03RisRiastaEcc() {
        return wb03RisRiastaEcc;
    }

    public Wb03RisRistorniCap getWb03RisRistorniCap() {
        return wb03RisRistorniCap;
    }

    public Wb03RisSpeT getWb03RisSpeT() {
        return wb03RisSpeT;
    }

    public Wb03RisZilT getWb03RisZilT() {
        return wb03RisZilT;
    }

    public Wb03Riscpar getWb03Riscpar() {
        return wb03Riscpar;
    }

    public Wb03TsMedio getWb03TsMedio() {
        return wb03TsMedio;
    }

    public Wb03TsNetT getWb03TsNetT() {
        return wb03TsNetT;
    }

    public Wb03TsPp getWb03TsPp() {
        return wb03TsPp;
    }

    public Wb03TsRendtoSppr getWb03TsRendtoSppr() {
        return wb03TsRendtoSppr;
    }

    public Wb03TsRendtoT getWb03TsRendtoT() {
        return wb03TsRendtoT;
    }

    public Wb03TsStabPre getWb03TsStabPre() {
        return wb03TsStabPre;
    }

    public Wb03TsTariDov getWb03TsTariDov() {
        return wb03TsTariDov;
    }

    public Wb03TsTariScon getWb03TsTariScon() {
        return wb03TsTariScon;
    }

    public Wb03UltRm getWb03UltRm() {
        return wb03UltRm;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ID_BILA_TRCH_ESTR = 5;
        public static final int WB03_COD_COMP_ANIA = 3;
        public static final int WB03_ID_RICH_ESTRAZ_MAS = 5;
        public static final int WB03_FL_SIMULAZIONE = 1;
        public static final int WB03_DT_RIS = 5;
        public static final int WB03_DT_PRODUZIONE = 5;
        public static final int WB03_ID_POLI = 5;
        public static final int WB03_ID_ADES = 5;
        public static final int WB03_ID_GAR = 5;
        public static final int WB03_ID_TRCH_DI_GAR = 5;
        public static final int WB03_TP_FRM_ASSVA = 2;
        public static final int WB03_TP_RAMO_BILA = 2;
        public static final int WB03_TP_CALC_RIS = 2;
        public static final int WB03_COD_RAMO = 12;
        public static final int WB03_COD_TARI = 12;
        public static final int WB03_COD_PROD = 12;
        public static final int WB03_DT_INI_VLDT_PROD = 5;
        public static final int WB03_COD_TARI_ORGN = 12;
        public static final int WB03_TP_TARI = 2;
        public static final int WB03_TP_PRE = 1;
        public static final int WB03_TP_ADEG_PRE = 1;
        public static final int WB03_TP_RIVAL = 2;
        public static final int WB03_FL_DA_TRASF = 1;
        public static final int WB03_FL_CAR_CONT = 1;
        public static final int WB03_FL_PRE_DA_RIS = 1;
        public static final int WB03_FL_PRE_AGG = 1;
        public static final int WB03_TP_TRCH = 2;
        public static final int WB03_TP_TST = 2;
        public static final int WB03_COD_CONV = 12;
        public static final int WB03_DT_DECOR_POLI = 5;
        public static final int WB03_DT_DECOR_TRCH = 5;
        public static final int WB03_DT_EMIS_POLI = 5;
        public static final int WB03_SEX1O_ASSTO = 1;
        public static final int WB03_TP_STAT_BUS_POLI = 2;
        public static final int WB03_TP_CAUS_POLI = 2;
        public static final int WB03_TP_STAT_BUS_ADES = 2;
        public static final int WB03_TP_CAUS_ADES = 2;
        public static final int WB03_TP_STAT_BUS_TRCH = 2;
        public static final int WB03_TP_CAUS_TRCH = 2;
        public static final int WB03_COD_DIV = 20;
        public static final int WB03_TP_IAS = 2;
        public static final int WB03_IB_POLI = 40;
        public static final int WB03_IB_ADES = 40;
        public static final int WB03_IB_TRCH_DI_GAR = 40;
        public static final int WB03_TP_PRSTZ = 2;
        public static final int WB03_TP_TRASF = 2;
        public static final int WB03_PP_INVRIO_TARI = 1;
        public static final int WB03_VLT = 3;
        public static final int WB03_COD_FND = 12;
        public static final int WB03_TP_COASS = 2;
        public static final int WB03_TRAT_RIASS = 12;
        public static final int WB03_TRAT_RIASS_ECC = 12;
        public static final int WB03_DS_OPER_SQL = 1;
        public static final int WB03_DS_VER = 5;
        public static final int WB03_DS_TS_CPTZ = 10;
        public static final int WB03_DS_UTENTE = 20;
        public static final int WB03_DS_STATO_ELAB = 1;
        public static final int WB03_TP_RGM_FISC = 2;
        public static final int WB03_COD_FISC_CNTR = 16;
        public static final int WB03_COD_FISC_ASSTO1 = 16;
        public static final int WB03_COD_FISC_ASSTO2 = 16;
        public static final int WB03_COD_FISC_ASSTO3 = 16;
        public static final int WB03_CAUS_SCON = 100;
        public static final int WB03_EMIT_TIT_OPZ = 100;
        public static final int WB03_TP_VERS = 1;
        public static final int WB03_FL_SWITCH = 1;
        public static final int WB03_FL_IAS = 1;
        public static final int WB03_TP_COP_CASO_MOR = 2;
        public static final int WB03_TP_STAT_INVST = 2;
        public static final int WB03_STAT_ASSTO1 = 1;
        public static final int WB03_STAT_ASSTO2 = 1;
        public static final int WB03_STAT_ASSTO3 = 1;
        public static final int WB03_STAT_TBGC_ASSTO1 = 1;
        public static final int WB03_STAT_TBGC_ASSTO2 = 1;
        public static final int WB03_STAT_TBGC_ASSTO3 = 1;
        public static final int WB03_NUM_FINANZ = 40;
        public static final int WB03_TP_ACC_COMM = 2;
        public static final int WB03_IB_ACC_COMM = 40;
        public static final int WB03_RAMO_BILA = 12;
        public static final int DATI = WB03_ID_BILA_TRCH_ESTR + WB03_COD_COMP_ANIA + WB03_ID_RICH_ESTRAZ_MAS + Wb03IdRichEstrazAgg.Len.WB03_ID_RICH_ESTRAZ_AGG + WB03_FL_SIMULAZIONE + WB03_DT_RIS + WB03_DT_PRODUZIONE + WB03_ID_POLI + WB03_ID_ADES + WB03_ID_GAR + WB03_ID_TRCH_DI_GAR + WB03_TP_FRM_ASSVA + WB03_TP_RAMO_BILA + WB03_TP_CALC_RIS + WB03_COD_RAMO + WB03_COD_TARI + Wb03DtIniValTar.Len.WB03_DT_INI_VAL_TAR + WB03_COD_PROD + WB03_DT_INI_VLDT_PROD + WB03_COD_TARI_ORGN + Wb03MinGartoT.Len.WB03_MIN_GARTO_T + WB03_TP_TARI + WB03_TP_PRE + WB03_TP_ADEG_PRE + WB03_TP_RIVAL + WB03_FL_DA_TRASF + WB03_FL_CAR_CONT + WB03_FL_PRE_DA_RIS + WB03_FL_PRE_AGG + WB03_TP_TRCH + WB03_TP_TST + WB03_COD_CONV + WB03_DT_DECOR_POLI + Wb03DtDecorAdes.Len.WB03_DT_DECOR_ADES + WB03_DT_DECOR_TRCH + WB03_DT_EMIS_POLI + Wb03DtEmisTrch.Len.WB03_DT_EMIS_TRCH + Wb03DtScadTrch.Len.WB03_DT_SCAD_TRCH + Wb03DtScadIntmd.Len.WB03_DT_SCAD_INTMD + Wb03DtScadPagPre.Len.WB03_DT_SCAD_PAG_PRE + Wb03DtUltPrePag.Len.WB03_DT_ULT_PRE_PAG + Wb03DtNasc1oAssto.Len.WB03_DT_NASC1O_ASSTO + WB03_SEX1O_ASSTO + Wb03EtaAa1oAssto.Len.WB03_ETA_AA1O_ASSTO + Wb03EtaMm1oAssto.Len.WB03_ETA_MM1O_ASSTO + Wb03EtaRaggnDtCalc.Len.WB03_ETA_RAGGN_DT_CALC + Wb03DurAa.Len.WB03_DUR_AA + Wb03DurMm.Len.WB03_DUR_MM + Wb03DurGg.Len.WB03_DUR_GG + Wb03Dur1oPerAa.Len.WB03_DUR1O_PER_AA + Wb03Dur1oPerMm.Len.WB03_DUR1O_PER_MM + Wb03Dur1oPerGg.Len.WB03_DUR1O_PER_GG + Wb03AntidurRicorPrec.Len.WB03_ANTIDUR_RICOR_PREC + Wb03AntidurDtCalc.Len.WB03_ANTIDUR_DT_CALC + Wb03DurResDtCalc.Len.WB03_DUR_RES_DT_CALC + WB03_TP_STAT_BUS_POLI + WB03_TP_CAUS_POLI + WB03_TP_STAT_BUS_ADES + WB03_TP_CAUS_ADES + WB03_TP_STAT_BUS_TRCH + WB03_TP_CAUS_TRCH + Wb03DtEffCambStat.Len.WB03_DT_EFF_CAMB_STAT + Wb03DtEmisCambStat.Len.WB03_DT_EMIS_CAMB_STAT + Wb03DtEffStab.Len.WB03_DT_EFF_STAB + Wb03CptDtStab.Len.WB03_CPT_DT_STAB + Wb03DtEffRidz.Len.WB03_DT_EFF_RIDZ + Wb03DtEmisRidz.Len.WB03_DT_EMIS_RIDZ + Wb03CptDtRidz.Len.WB03_CPT_DT_RIDZ + Wb03Fraz.Len.WB03_FRAZ + Wb03DurPagPre.Len.WB03_DUR_PAG_PRE + Wb03NumPrePatt.Len.WB03_NUM_PRE_PATT + Wb03FrazIniErogRen.Len.WB03_FRAZ_INI_EROG_REN + Wb03AaRenCer.Len.WB03_AA_REN_CER + Wb03RatRen.Len.WB03_RAT_REN + WB03_COD_DIV + Wb03Riscpar.Len.WB03_RISCPAR + Wb03CumRiscpar.Len.WB03_CUM_RISCPAR + Wb03UltRm.Len.WB03_ULT_RM + Wb03TsRendtoT.Len.WB03_TS_RENDTO_T + Wb03AlqRetrT.Len.WB03_ALQ_RETR_T + Wb03MinTrnutT.Len.WB03_MIN_TRNUT_T + Wb03TsNetT.Len.WB03_TS_NET_T + Wb03DtUltRival.Len.WB03_DT_ULT_RIVAL + Wb03PrstzIni.Len.WB03_PRSTZ_INI + Wb03PrstzAggIni.Len.WB03_PRSTZ_AGG_INI + Wb03PrstzAggUlt.Len.WB03_PRSTZ_AGG_ULT + Wb03Rappel.Len.WB03_RAPPEL + Wb03PrePattuitoIni.Len.WB03_PRE_PATTUITO_INI + Wb03PreDovIni.Len.WB03_PRE_DOV_INI + Wb03PreDovRivtoT.Len.WB03_PRE_DOV_RIVTO_T + Wb03PreAnnualizRicor.Len.WB03_PRE_ANNUALIZ_RICOR + Wb03PreCont.Len.WB03_PRE_CONT + Wb03PrePpIni.Len.WB03_PRE_PP_INI + Wb03RisPuraT.Len.WB03_RIS_PURA_T + Wb03ProvAcq.Len.WB03_PROV_ACQ + Wb03ProvAcqRicor.Len.WB03_PROV_ACQ_RICOR + Wb03ProvInc.Len.WB03_PROV_INC + Wb03CarAcqNonScon.Len.WB03_CAR_ACQ_NON_SCON + Wb03OverComm.Len.WB03_OVER_COMM + Wb03CarAcqPrecontato.Len.WB03_CAR_ACQ_PRECONTATO + Wb03RisAcqT.Len.WB03_RIS_ACQ_T + Wb03RisZilT.Len.WB03_RIS_ZIL_T + Wb03CarGestNonScon.Len.WB03_CAR_GEST_NON_SCON + Wb03CarGest.Len.WB03_CAR_GEST + Wb03RisSpeT.Len.WB03_RIS_SPE_T + Wb03CarIncNonScon.Len.WB03_CAR_INC_NON_SCON + Wb03CarInc.Len.WB03_CAR_INC + Wb03RisRistorniCap.Len.WB03_RIS_RISTORNI_CAP + Wb03IntrTecn.Len.WB03_INTR_TECN + Wb03CptRshMor.Len.WB03_CPT_RSH_MOR + Wb03CSubrshT.Len.WB03_C_SUBRSH_T + Wb03PreRshT.Len.WB03_PRE_RSH_T + Wb03AlqMargRis.Len.WB03_ALQ_MARG_RIS + Wb03AlqMargCSubrsh.Len.WB03_ALQ_MARG_C_SUBRSH + Wb03TsRendtoSppr.Len.WB03_TS_RENDTO_SPPR + WB03_TP_IAS + Wb03NsQuo.Len.WB03_NS_QUO + Wb03TsMedio.Len.WB03_TS_MEDIO + Wb03CptRiasto.Len.WB03_CPT_RIASTO + Wb03PreRiasto.Len.WB03_PRE_RIASTO + Wb03RisRiasta.Len.WB03_RIS_RIASTA + Wb03CptRiastoEcc.Len.WB03_CPT_RIASTO_ECC + Wb03PreRiastoEcc.Len.WB03_PRE_RIASTO_ECC + Wb03RisRiastaEcc.Len.WB03_RIS_RIASTA_ECC + Wb03CodAge.Len.WB03_COD_AGE + Wb03CodSubage.Len.WB03_COD_SUBAGE + Wb03CodCan.Len.WB03_COD_CAN + WB03_IB_POLI + WB03_IB_ADES + WB03_IB_TRCH_DI_GAR + WB03_TP_PRSTZ + WB03_TP_TRASF + WB03_PP_INVRIO_TARI + Wb03CoeffOpzRen.Len.WB03_COEFF_OPZ_REN + Wb03CoeffOpzCpt.Len.WB03_COEFF_OPZ_CPT + Wb03DurPagRen.Len.WB03_DUR_PAG_REN + WB03_VLT + Wb03RisMatChiuPrec.Len.WB03_RIS_MAT_CHIU_PREC + WB03_COD_FND + Wb03PrstzT.Len.WB03_PRSTZ_T + Wb03TsTariDov.Len.WB03_TS_TARI_DOV + Wb03TsTariScon.Len.WB03_TS_TARI_SCON + Wb03TsPp.Len.WB03_TS_PP + Wb03CoeffRis1T.Len.WB03_COEFF_RIS1_T + Wb03CoeffRis2T.Len.WB03_COEFF_RIS2_T + Wb03Abb.Len.WB03_ABB + WB03_TP_COASS + WB03_TRAT_RIASS + WB03_TRAT_RIASS_ECC + WB03_DS_OPER_SQL + WB03_DS_VER + WB03_DS_TS_CPTZ + WB03_DS_UTENTE + WB03_DS_STATO_ELAB + WB03_TP_RGM_FISC + Wb03DurGarAa.Len.WB03_DUR_GAR_AA + Wb03DurGarMm.Len.WB03_DUR_GAR_MM + Wb03DurGarGg.Len.WB03_DUR_GAR_GG + Wb03AntidurCalc365.Len.WB03_ANTIDUR_CALC365 + WB03_COD_FISC_CNTR + WB03_COD_FISC_ASSTO1 + WB03_COD_FISC_ASSTO2 + WB03_COD_FISC_ASSTO3 + WB03_CAUS_SCON + WB03_EMIT_TIT_OPZ + Wb03QtzSpZCoupEmis.Len.WB03_QTZ_SP_Z_COUP_EMIS + Wb03QtzSpZOpzEmis.Len.WB03_QTZ_SP_Z_OPZ_EMIS + Wb03QtzSpZCoupDtC.Len.WB03_QTZ_SP_Z_COUP_DT_C + Wb03QtzSpZOpzDtCa.Len.WB03_QTZ_SP_Z_OPZ_DT_CA + Wb03QtzTotEmis.Len.WB03_QTZ_TOT_EMIS + Wb03QtzTotDtCalc.Len.WB03_QTZ_TOT_DT_CALC + Wb03QtzTotDtUltBil.Len.WB03_QTZ_TOT_DT_ULT_BIL + Wb03DtQtzEmis.Len.WB03_DT_QTZ_EMIS + Wb03PcCarGest.Len.WB03_PC_CAR_GEST + Wb03PcCarAcq.Len.WB03_PC_CAR_ACQ + Wb03ImpCarCasoMor.Len.WB03_IMP_CAR_CASO_MOR + Wb03PcCarMor.Len.WB03_PC_CAR_MOR + WB03_TP_VERS + WB03_FL_SWITCH + WB03_FL_IAS + Wb03Dir.Len.WB03_DIR + WB03_TP_COP_CASO_MOR + Wb03MetRiscSpcl.Len.WB03_MET_RISC_SPCL + WB03_TP_STAT_INVST + Wb03CodPrdt.Len.WB03_COD_PRDT + WB03_STAT_ASSTO1 + WB03_STAT_ASSTO2 + WB03_STAT_ASSTO3 + Wb03CptAsstoIniMor.Len.WB03_CPT_ASSTO_INI_MOR + Wb03TsStabPre.Len.WB03_TS_STAB_PRE + Wb03DirEmis.Len.WB03_DIR_EMIS + Wb03DtIncUltPre.Len.WB03_DT_INC_ULT_PRE + WB03_STAT_TBGC_ASSTO1 + WB03_STAT_TBGC_ASSTO2 + WB03_STAT_TBGC_ASSTO3 + Wb03FrazDecrCpt.Len.WB03_FRAZ_DECR_CPT + Wb03PrePpUlt.Len.WB03_PRE_PP_ULT + Wb03AcqExp.Len.WB03_ACQ_EXP + Wb03RemunAss.Len.WB03_REMUN_ASS + Wb03CommisInter.Len.WB03_COMMIS_INTER + WB03_NUM_FINANZ + WB03_TP_ACC_COMM + WB03_IB_ACC_COMM + WB03_RAMO_BILA + Wb03Carz.Len.WB03_CARZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ID_BILA_TRCH_ESTR = 9;
            public static final int WB03_COD_COMP_ANIA = 5;
            public static final int WB03_ID_RICH_ESTRAZ_MAS = 9;
            public static final int WB03_DT_RIS = 8;
            public static final int WB03_DT_PRODUZIONE = 8;
            public static final int WB03_ID_POLI = 9;
            public static final int WB03_ID_ADES = 9;
            public static final int WB03_ID_GAR = 9;
            public static final int WB03_ID_TRCH_DI_GAR = 9;
            public static final int WB03_DT_INI_VLDT_PROD = 8;
            public static final int WB03_DT_DECOR_POLI = 8;
            public static final int WB03_DT_DECOR_TRCH = 8;
            public static final int WB03_DT_EMIS_POLI = 8;
            public static final int WB03_DS_VER = 9;
            public static final int WB03_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
