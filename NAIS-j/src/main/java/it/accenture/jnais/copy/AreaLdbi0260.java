package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: AREA-LDBI0260<br>
 * Variable: AREA-LDBI0260 from copybook LDBI0260<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AreaLdbi0260 {

    //==== PROPERTIES ====
    //Original name: LDBI0260-FLAG-CUR
    private String flagCur = DefaultValues.stringVal(Len.FLAG_CUR);
    //Original name: LDBI0260-COD-MOVI-NAIS
    private int codMoviNais = DefaultValues.INT_VAL;
    //Original name: LDBI0260-OGG-NAIS
    private String oggNais = DefaultValues.stringVal(Len.OGG_NAIS);
    //Original name: LDBI0260-FRM-ASSVA
    private String frmAssva = DefaultValues.stringVal(Len.FRM_ASSVA);
    //Original name: LDBI0260-COD-MOVI-ACTUATOR
    private String codMoviActuator = DefaultValues.stringVal(Len.COD_MOVI_ACTUATOR);
    //Original name: LDBI0260-AMMISSIBILITA-MOVI
    private char ammissibilitaMovi = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setAreaLdbi0260Formatted(String data) {
        byte[] buffer = new byte[Len.AREA_LDBI0260];
        MarshalByte.writeString(buffer, 1, data, Len.AREA_LDBI0260);
        setAreaLdbi0260Bytes(buffer, 1);
    }

    public String getAreaLdbi0260Formatted() {
        return MarshalByteExt.bufferToStr(getAreaLdbi0260Bytes());
    }

    public byte[] getAreaLdbi0260Bytes() {
        byte[] buffer = new byte[Len.AREA_LDBI0260];
        return getAreaLdbi0260Bytes(buffer, 1);
    }

    public void setAreaLdbi0260Bytes(byte[] buffer, int offset) {
        int position = offset;
        flagCur = MarshalByte.readFixedString(buffer, position, Len.FLAG_CUR);
        position += Len.FLAG_CUR;
        codMoviNais = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_MOVI_NAIS, 0);
        position += Len.COD_MOVI_NAIS;
        oggNais = MarshalByte.readString(buffer, position, Len.OGG_NAIS);
        position += Len.OGG_NAIS;
        frmAssva = MarshalByte.readString(buffer, position, Len.FRM_ASSVA);
        position += Len.FRM_ASSVA;
        codMoviActuator = MarshalByte.readString(buffer, position, Len.COD_MOVI_ACTUATOR);
        position += Len.COD_MOVI_ACTUATOR;
        ammissibilitaMovi = MarshalByte.readChar(buffer, position);
    }

    public byte[] getAreaLdbi0260Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flagCur, Len.FLAG_CUR);
        position += Len.FLAG_CUR;
        MarshalByte.writeIntAsPacked(buffer, position, codMoviNais, Len.Int.COD_MOVI_NAIS, 0);
        position += Len.COD_MOVI_NAIS;
        MarshalByte.writeString(buffer, position, oggNais, Len.OGG_NAIS);
        position += Len.OGG_NAIS;
        MarshalByte.writeString(buffer, position, frmAssva, Len.FRM_ASSVA);
        position += Len.FRM_ASSVA;
        MarshalByte.writeString(buffer, position, codMoviActuator, Len.COD_MOVI_ACTUATOR);
        position += Len.COD_MOVI_ACTUATOR;
        MarshalByte.writeChar(buffer, position, ammissibilitaMovi);
        return buffer;
    }

    public short getFlagCur() {
        return NumericDisplay.asShort(this.flagCur);
    }

    public void setCodMoviNais(int codMoviNais) {
        this.codMoviNais = codMoviNais;
    }

    public int getCodMoviNais() {
        return this.codMoviNais;
    }

    public void setOggNais(String oggNais) {
        this.oggNais = Functions.subString(oggNais, Len.OGG_NAIS);
    }

    public String getOggNais() {
        return this.oggNais;
    }

    public void setFrmAssva(String frmAssva) {
        this.frmAssva = Functions.subString(frmAssva, Len.FRM_ASSVA);
    }

    public String getFrmAssva() {
        return this.frmAssva;
    }

    public void setCodMoviActuator(String codMoviActuator) {
        this.codMoviActuator = Functions.subString(codMoviActuator, Len.COD_MOVI_ACTUATOR);
    }

    public String getCodMoviActuator() {
        return this.codMoviActuator;
    }

    public void setAmmissibilitaMovi(char ammissibilitaMovi) {
        this.ammissibilitaMovi = ammissibilitaMovi;
    }

    public char getAmmissibilitaMovi() {
        return this.ammissibilitaMovi;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_CUR = 1;
        public static final int OGG_NAIS = 2;
        public static final int FRM_ASSVA = 2;
        public static final int COD_MOVI_ACTUATOR = 5;
        public static final int COD_MOVI_NAIS = 3;
        public static final int AMMISSIBILITA_MOVI = 1;
        public static final int AREA_LDBI0260 = FLAG_CUR + COD_MOVI_NAIS + OGG_NAIS + FRM_ASSVA + COD_MOVI_ACTUATOR + AMMISSIBILITA_MOVI;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_MOVI_NAIS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
