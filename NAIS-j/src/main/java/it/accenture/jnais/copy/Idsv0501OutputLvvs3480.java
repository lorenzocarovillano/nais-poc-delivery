package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.occurs.Idsv0501TabStringheTot;

/**Original name: IDSV0501-OUTPUT<br>
 * Variable: IDSV0501-OUTPUT from copybook IDSV0502<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Idsv0501OutputLvvs3480 {

    //==== PROPERTIES ====
    public static final int TAB_STRINGHE_TOT_MAXOCCURS = 5;
    //Original name: IDSV0501-TP-STRINGA
    private char tpStringa = DefaultValues.CHAR_VAL;
    //Original name: IDSV0501-MAX-TAB-STR
    private short maxTabStr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IDSV0501-TAB-STRINGHE-TOT
    private Idsv0501TabStringheTot[] tabStringheTot = new Idsv0501TabStringheTot[TAB_STRINGHE_TOT_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Idsv0501OutputLvvs3480() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabStringheTotIdx = 1; tabStringheTotIdx <= TAB_STRINGHE_TOT_MAXOCCURS; tabStringheTotIdx++) {
            tabStringheTot[tabStringheTotIdx - 1] = new Idsv0501TabStringheTot();
        }
    }

    public byte[] getIdsv0501OutputBytes() {
        byte[] buffer = new byte[Len.IDSV0501_OUTPUT];
        return getIdsv0501OutputBytes(buffer, 1);
    }

    public byte[] getIdsv0501OutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, tpStringa);
        position += Types.CHAR_SIZE;
        MarshalByte.writeBinaryShort(buffer, position, maxTabStr);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_STRINGHE_TOT_MAXOCCURS; idx++) {
            tabStringheTot[idx - 1].getIdsv0501TabStringheTotBytes(buffer, position);
            position += Idsv0501TabStringheTot.Len.IDSV0501_TAB_STRINGHE_TOT;
        }
        return buffer;
    }

    public void setTpStringa(char tpStringa) {
        this.tpStringa = tpStringa;
    }

    public char getTpStringa() {
        return this.tpStringa;
    }

    public void setMaxTabStr(short maxTabStr) {
        this.maxTabStr = maxTabStr;
    }

    public short getMaxTabStr() {
        return this.maxTabStr;
    }

    public Idsv0501TabStringheTot getTabStringheTot(int idx) {
        return tabStringheTot[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_STRINGA = 1;
        public static final int MAX_TAB_STR = 2;
        public static final int IDSV0501_OUTPUT = TP_STRINGA + MAX_TAB_STR + Idsv0501OutputLvvs3480.TAB_STRINGHE_TOT_MAXOCCURS * Idsv0501TabStringheTot.Len.IDSV0501_TAB_STRINGHE_TOT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
