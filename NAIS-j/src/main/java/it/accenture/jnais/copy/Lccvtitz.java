package it.accenture.jnais.copy;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LCCVTITZ<br>
 * Copybook: LCCVTITZ from copybook LCCVTITZ<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvtitz {

    //==== PROPERTIES ====
    //Original name: WK-TIT-MAX-A
    private short maxA = ((short)200);

    //==== METHODS ====
    public short getMaxA() {
        return this.maxA;
    }
}
