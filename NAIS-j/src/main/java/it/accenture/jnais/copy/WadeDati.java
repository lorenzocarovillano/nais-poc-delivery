package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WadeCumCnbtCap;
import it.accenture.jnais.ws.redefines.WadeDtDecor;
import it.accenture.jnais.ws.redefines.WadeDtDecorPrestBan;
import it.accenture.jnais.ws.redefines.WadeDtEffVarzStatT;
import it.accenture.jnais.ws.redefines.WadeDtNovaRgmFisc;
import it.accenture.jnais.ws.redefines.WadeDtPresc;
import it.accenture.jnais.ws.redefines.WadeDtScad;
import it.accenture.jnais.ws.redefines.WadeDtUltConsCnbt;
import it.accenture.jnais.ws.redefines.WadeDtVarzTpIas;
import it.accenture.jnais.ws.redefines.WadeDurAa;
import it.accenture.jnais.ws.redefines.WadeDurGg;
import it.accenture.jnais.ws.redefines.WadeDurMm;
import it.accenture.jnais.ws.redefines.WadeEtaAScad;
import it.accenture.jnais.ws.redefines.WadeIdMoviChiu;
import it.accenture.jnais.ws.redefines.WadeImpAder;
import it.accenture.jnais.ws.redefines.WadeImpAz;
import it.accenture.jnais.ws.redefines.WadeImpbVisDaRec;
import it.accenture.jnais.ws.redefines.WadeImpGarCnbt;
import it.accenture.jnais.ws.redefines.WadeImpRecRitAcc;
import it.accenture.jnais.ws.redefines.WadeImpRecRitVis;
import it.accenture.jnais.ws.redefines.WadeImpTfr;
import it.accenture.jnais.ws.redefines.WadeImpVolo;
import it.accenture.jnais.ws.redefines.WadeNumRatPian;
import it.accenture.jnais.ws.redefines.WadePcAder;
import it.accenture.jnais.ws.redefines.WadePcAz;
import it.accenture.jnais.ws.redefines.WadePcTfr;
import it.accenture.jnais.ws.redefines.WadePcVolo;
import it.accenture.jnais.ws.redefines.WadePreLrdInd;
import it.accenture.jnais.ws.redefines.WadePreNetInd;
import it.accenture.jnais.ws.redefines.WadePrstzIniInd;
import it.accenture.jnais.ws.redefines.WadeRatLrdInd;

/**Original name: WADE-DATI<br>
 * Variable: WADE-DATI from copybook LCCVADE1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WadeDati {

    //==== PROPERTIES ====
    //Original name: WADE-ID-ADES
    private int wadeIdAdes = DefaultValues.INT_VAL;
    //Original name: WADE-ID-POLI
    private int wadeIdPoli = DefaultValues.INT_VAL;
    //Original name: WADE-ID-MOVI-CRZ
    private int wadeIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WADE-ID-MOVI-CHIU
    private WadeIdMoviChiu wadeIdMoviChiu = new WadeIdMoviChiu();
    //Original name: WADE-DT-INI-EFF
    private int wadeDtIniEff = DefaultValues.INT_VAL;
    //Original name: WADE-DT-END-EFF
    private int wadeDtEndEff = DefaultValues.INT_VAL;
    //Original name: WADE-IB-PREV
    private String wadeIbPrev = DefaultValues.stringVal(Len.WADE_IB_PREV);
    //Original name: WADE-IB-OGG
    private String wadeIbOgg = DefaultValues.stringVal(Len.WADE_IB_OGG);
    //Original name: WADE-COD-COMP-ANIA
    private int wadeCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WADE-DT-DECOR
    private WadeDtDecor wadeDtDecor = new WadeDtDecor();
    //Original name: WADE-DT-SCAD
    private WadeDtScad wadeDtScad = new WadeDtScad();
    //Original name: WADE-ETA-A-SCAD
    private WadeEtaAScad wadeEtaAScad = new WadeEtaAScad();
    //Original name: WADE-DUR-AA
    private WadeDurAa wadeDurAa = new WadeDurAa();
    //Original name: WADE-DUR-MM
    private WadeDurMm wadeDurMm = new WadeDurMm();
    //Original name: WADE-DUR-GG
    private WadeDurGg wadeDurGg = new WadeDurGg();
    //Original name: WADE-TP-RGM-FISC
    private String wadeTpRgmFisc = DefaultValues.stringVal(Len.WADE_TP_RGM_FISC);
    //Original name: WADE-TP-RIAT
    private String wadeTpRiat = DefaultValues.stringVal(Len.WADE_TP_RIAT);
    //Original name: WADE-TP-MOD-PAG-TIT
    private String wadeTpModPagTit = DefaultValues.stringVal(Len.WADE_TP_MOD_PAG_TIT);
    //Original name: WADE-TP-IAS
    private String wadeTpIas = DefaultValues.stringVal(Len.WADE_TP_IAS);
    //Original name: WADE-DT-VARZ-TP-IAS
    private WadeDtVarzTpIas wadeDtVarzTpIas = new WadeDtVarzTpIas();
    //Original name: WADE-PRE-NET-IND
    private WadePreNetInd wadePreNetInd = new WadePreNetInd();
    //Original name: WADE-PRE-LRD-IND
    private WadePreLrdInd wadePreLrdInd = new WadePreLrdInd();
    //Original name: WADE-RAT-LRD-IND
    private WadeRatLrdInd wadeRatLrdInd = new WadeRatLrdInd();
    //Original name: WADE-PRSTZ-INI-IND
    private WadePrstzIniInd wadePrstzIniInd = new WadePrstzIniInd();
    //Original name: WADE-FL-COINC-ASSTO
    private char wadeFlCoincAssto = DefaultValues.CHAR_VAL;
    //Original name: WADE-IB-DFLT
    private String wadeIbDflt = DefaultValues.stringVal(Len.WADE_IB_DFLT);
    //Original name: WADE-MOD-CALC
    private String wadeModCalc = DefaultValues.stringVal(Len.WADE_MOD_CALC);
    //Original name: WADE-TP-FNT-CNBTVA
    private String wadeTpFntCnbtva = DefaultValues.stringVal(Len.WADE_TP_FNT_CNBTVA);
    //Original name: WADE-IMP-AZ
    private WadeImpAz wadeImpAz = new WadeImpAz();
    //Original name: WADE-IMP-ADER
    private WadeImpAder wadeImpAder = new WadeImpAder();
    //Original name: WADE-IMP-TFR
    private WadeImpTfr wadeImpTfr = new WadeImpTfr();
    //Original name: WADE-IMP-VOLO
    private WadeImpVolo wadeImpVolo = new WadeImpVolo();
    //Original name: WADE-PC-AZ
    private WadePcAz wadePcAz = new WadePcAz();
    //Original name: WADE-PC-ADER
    private WadePcAder wadePcAder = new WadePcAder();
    //Original name: WADE-PC-TFR
    private WadePcTfr wadePcTfr = new WadePcTfr();
    //Original name: WADE-PC-VOLO
    private WadePcVolo wadePcVolo = new WadePcVolo();
    //Original name: WADE-DT-NOVA-RGM-FISC
    private WadeDtNovaRgmFisc wadeDtNovaRgmFisc = new WadeDtNovaRgmFisc();
    //Original name: WADE-FL-ATTIV
    private char wadeFlAttiv = DefaultValues.CHAR_VAL;
    //Original name: WADE-IMP-REC-RIT-VIS
    private WadeImpRecRitVis wadeImpRecRitVis = new WadeImpRecRitVis();
    //Original name: WADE-IMP-REC-RIT-ACC
    private WadeImpRecRitAcc wadeImpRecRitAcc = new WadeImpRecRitAcc();
    //Original name: WADE-FL-VARZ-STAT-TBGC
    private char wadeFlVarzStatTbgc = DefaultValues.CHAR_VAL;
    //Original name: WADE-FL-PROVZA-MIGRAZ
    private char wadeFlProvzaMigraz = DefaultValues.CHAR_VAL;
    //Original name: WADE-IMPB-VIS-DA-REC
    private WadeImpbVisDaRec wadeImpbVisDaRec = new WadeImpbVisDaRec();
    //Original name: WADE-DT-DECOR-PREST-BAN
    private WadeDtDecorPrestBan wadeDtDecorPrestBan = new WadeDtDecorPrestBan();
    //Original name: WADE-DT-EFF-VARZ-STAT-T
    private WadeDtEffVarzStatT wadeDtEffVarzStatT = new WadeDtEffVarzStatT();
    //Original name: WADE-DS-RIGA
    private long wadeDsRiga = DefaultValues.LONG_VAL;
    //Original name: WADE-DS-OPER-SQL
    private char wadeDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WADE-DS-VER
    private int wadeDsVer = DefaultValues.INT_VAL;
    //Original name: WADE-DS-TS-INI-CPTZ
    private long wadeDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WADE-DS-TS-END-CPTZ
    private long wadeDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WADE-DS-UTENTE
    private String wadeDsUtente = DefaultValues.stringVal(Len.WADE_DS_UTENTE);
    //Original name: WADE-DS-STATO-ELAB
    private char wadeDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WADE-CUM-CNBT-CAP
    private WadeCumCnbtCap wadeCumCnbtCap = new WadeCumCnbtCap();
    //Original name: WADE-IMP-GAR-CNBT
    private WadeImpGarCnbt wadeImpGarCnbt = new WadeImpGarCnbt();
    //Original name: WADE-DT-ULT-CONS-CNBT
    private WadeDtUltConsCnbt wadeDtUltConsCnbt = new WadeDtUltConsCnbt();
    //Original name: WADE-IDEN-ISC-FND
    private String wadeIdenIscFnd = DefaultValues.stringVal(Len.WADE_IDEN_ISC_FND);
    //Original name: WADE-NUM-RAT-PIAN
    private WadeNumRatPian wadeNumRatPian = new WadeNumRatPian();
    //Original name: WADE-DT-PRESC
    private WadeDtPresc wadeDtPresc = new WadeDtPresc();
    //Original name: WADE-CONCS-PREST
    private char wadeConcsPrest = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wadeIdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WADE_ID_ADES, 0);
        position += Len.WADE_ID_ADES;
        wadeIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WADE_ID_POLI, 0);
        position += Len.WADE_ID_POLI;
        wadeIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WADE_ID_MOVI_CRZ, 0);
        position += Len.WADE_ID_MOVI_CRZ;
        wadeIdMoviChiu.setWadeIdMoviChiuFromBuffer(buffer, position);
        position += WadeIdMoviChiu.Len.WADE_ID_MOVI_CHIU;
        wadeDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WADE_DT_INI_EFF, 0);
        position += Len.WADE_DT_INI_EFF;
        wadeDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WADE_DT_END_EFF, 0);
        position += Len.WADE_DT_END_EFF;
        wadeIbPrev = MarshalByte.readString(buffer, position, Len.WADE_IB_PREV);
        position += Len.WADE_IB_PREV;
        wadeIbOgg = MarshalByte.readString(buffer, position, Len.WADE_IB_OGG);
        position += Len.WADE_IB_OGG;
        wadeCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WADE_COD_COMP_ANIA, 0);
        position += Len.WADE_COD_COMP_ANIA;
        wadeDtDecor.setWadeDtDecorFromBuffer(buffer, position);
        position += WadeDtDecor.Len.WADE_DT_DECOR;
        wadeDtScad.setWadeDtScadFromBuffer(buffer, position);
        position += WadeDtScad.Len.WADE_DT_SCAD;
        wadeEtaAScad.setWadeEtaAScadFromBuffer(buffer, position);
        position += WadeEtaAScad.Len.WADE_ETA_A_SCAD;
        wadeDurAa.setWadeDurAaFromBuffer(buffer, position);
        position += WadeDurAa.Len.WADE_DUR_AA;
        wadeDurMm.setWadeDurMmFromBuffer(buffer, position);
        position += WadeDurMm.Len.WADE_DUR_MM;
        wadeDurGg.setWadeDurGgFromBuffer(buffer, position);
        position += WadeDurGg.Len.WADE_DUR_GG;
        wadeTpRgmFisc = MarshalByte.readString(buffer, position, Len.WADE_TP_RGM_FISC);
        position += Len.WADE_TP_RGM_FISC;
        wadeTpRiat = MarshalByte.readString(buffer, position, Len.WADE_TP_RIAT);
        position += Len.WADE_TP_RIAT;
        wadeTpModPagTit = MarshalByte.readString(buffer, position, Len.WADE_TP_MOD_PAG_TIT);
        position += Len.WADE_TP_MOD_PAG_TIT;
        wadeTpIas = MarshalByte.readString(buffer, position, Len.WADE_TP_IAS);
        position += Len.WADE_TP_IAS;
        wadeDtVarzTpIas.setWadeDtVarzTpIasFromBuffer(buffer, position);
        position += WadeDtVarzTpIas.Len.WADE_DT_VARZ_TP_IAS;
        wadePreNetInd.setWadePreNetIndFromBuffer(buffer, position);
        position += WadePreNetInd.Len.WADE_PRE_NET_IND;
        wadePreLrdInd.setWadePreLrdIndFromBuffer(buffer, position);
        position += WadePreLrdInd.Len.WADE_PRE_LRD_IND;
        wadeRatLrdInd.setWadeRatLrdIndFromBuffer(buffer, position);
        position += WadeRatLrdInd.Len.WADE_RAT_LRD_IND;
        wadePrstzIniInd.setWadePrstzIniIndFromBuffer(buffer, position);
        position += WadePrstzIniInd.Len.WADE_PRSTZ_INI_IND;
        wadeFlCoincAssto = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wadeIbDflt = MarshalByte.readString(buffer, position, Len.WADE_IB_DFLT);
        position += Len.WADE_IB_DFLT;
        wadeModCalc = MarshalByte.readString(buffer, position, Len.WADE_MOD_CALC);
        position += Len.WADE_MOD_CALC;
        wadeTpFntCnbtva = MarshalByte.readString(buffer, position, Len.WADE_TP_FNT_CNBTVA);
        position += Len.WADE_TP_FNT_CNBTVA;
        wadeImpAz.setWadeImpAzFromBuffer(buffer, position);
        position += WadeImpAz.Len.WADE_IMP_AZ;
        wadeImpAder.setWadeImpAderFromBuffer(buffer, position);
        position += WadeImpAder.Len.WADE_IMP_ADER;
        wadeImpTfr.setWadeImpTfrFromBuffer(buffer, position);
        position += WadeImpTfr.Len.WADE_IMP_TFR;
        wadeImpVolo.setWadeImpVoloFromBuffer(buffer, position);
        position += WadeImpVolo.Len.WADE_IMP_VOLO;
        wadePcAz.setWadePcAzFromBuffer(buffer, position);
        position += WadePcAz.Len.WADE_PC_AZ;
        wadePcAder.setWadePcAderFromBuffer(buffer, position);
        position += WadePcAder.Len.WADE_PC_ADER;
        wadePcTfr.setWadePcTfrFromBuffer(buffer, position);
        position += WadePcTfr.Len.WADE_PC_TFR;
        wadePcVolo.setWadePcVoloFromBuffer(buffer, position);
        position += WadePcVolo.Len.WADE_PC_VOLO;
        wadeDtNovaRgmFisc.setWadeDtNovaRgmFiscFromBuffer(buffer, position);
        position += WadeDtNovaRgmFisc.Len.WADE_DT_NOVA_RGM_FISC;
        wadeFlAttiv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wadeImpRecRitVis.setWadeImpRecRitVisFromBuffer(buffer, position);
        position += WadeImpRecRitVis.Len.WADE_IMP_REC_RIT_VIS;
        wadeImpRecRitAcc.setWadeImpRecRitAccFromBuffer(buffer, position);
        position += WadeImpRecRitAcc.Len.WADE_IMP_REC_RIT_ACC;
        wadeFlVarzStatTbgc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wadeFlProvzaMigraz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wadeImpbVisDaRec.setWadeImpbVisDaRecFromBuffer(buffer, position);
        position += WadeImpbVisDaRec.Len.WADE_IMPB_VIS_DA_REC;
        wadeDtDecorPrestBan.setWadeDtDecorPrestBanFromBuffer(buffer, position);
        position += WadeDtDecorPrestBan.Len.WADE_DT_DECOR_PREST_BAN;
        wadeDtEffVarzStatT.setWadeDtEffVarzStatTFromBuffer(buffer, position);
        position += WadeDtEffVarzStatT.Len.WADE_DT_EFF_VARZ_STAT_T;
        wadeDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WADE_DS_RIGA, 0);
        position += Len.WADE_DS_RIGA;
        wadeDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wadeDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WADE_DS_VER, 0);
        position += Len.WADE_DS_VER;
        wadeDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WADE_DS_TS_INI_CPTZ, 0);
        position += Len.WADE_DS_TS_INI_CPTZ;
        wadeDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WADE_DS_TS_END_CPTZ, 0);
        position += Len.WADE_DS_TS_END_CPTZ;
        wadeDsUtente = MarshalByte.readString(buffer, position, Len.WADE_DS_UTENTE);
        position += Len.WADE_DS_UTENTE;
        wadeDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wadeCumCnbtCap.setWadeCumCnbtCapFromBuffer(buffer, position);
        position += WadeCumCnbtCap.Len.WADE_CUM_CNBT_CAP;
        wadeImpGarCnbt.setWadeImpGarCnbtFromBuffer(buffer, position);
        position += WadeImpGarCnbt.Len.WADE_IMP_GAR_CNBT;
        wadeDtUltConsCnbt.setWadeDtUltConsCnbtFromBuffer(buffer, position);
        position += WadeDtUltConsCnbt.Len.WADE_DT_ULT_CONS_CNBT;
        wadeIdenIscFnd = MarshalByte.readString(buffer, position, Len.WADE_IDEN_ISC_FND);
        position += Len.WADE_IDEN_ISC_FND;
        wadeNumRatPian.setWadeNumRatPianFromBuffer(buffer, position);
        position += WadeNumRatPian.Len.WADE_NUM_RAT_PIAN;
        wadeDtPresc.setWadeDtPrescFromBuffer(buffer, position);
        position += WadeDtPresc.Len.WADE_DT_PRESC;
        wadeConcsPrest = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wadeIdAdes, Len.Int.WADE_ID_ADES, 0);
        position += Len.WADE_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wadeIdPoli, Len.Int.WADE_ID_POLI, 0);
        position += Len.WADE_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wadeIdMoviCrz, Len.Int.WADE_ID_MOVI_CRZ, 0);
        position += Len.WADE_ID_MOVI_CRZ;
        wadeIdMoviChiu.getWadeIdMoviChiuAsBuffer(buffer, position);
        position += WadeIdMoviChiu.Len.WADE_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wadeDtIniEff, Len.Int.WADE_DT_INI_EFF, 0);
        position += Len.WADE_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wadeDtEndEff, Len.Int.WADE_DT_END_EFF, 0);
        position += Len.WADE_DT_END_EFF;
        MarshalByte.writeString(buffer, position, wadeIbPrev, Len.WADE_IB_PREV);
        position += Len.WADE_IB_PREV;
        MarshalByte.writeString(buffer, position, wadeIbOgg, Len.WADE_IB_OGG);
        position += Len.WADE_IB_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, wadeCodCompAnia, Len.Int.WADE_COD_COMP_ANIA, 0);
        position += Len.WADE_COD_COMP_ANIA;
        wadeDtDecor.getWadeDtDecorAsBuffer(buffer, position);
        position += WadeDtDecor.Len.WADE_DT_DECOR;
        wadeDtScad.getWadeDtScadAsBuffer(buffer, position);
        position += WadeDtScad.Len.WADE_DT_SCAD;
        wadeEtaAScad.getWadeEtaAScadAsBuffer(buffer, position);
        position += WadeEtaAScad.Len.WADE_ETA_A_SCAD;
        wadeDurAa.getWadeDurAaAsBuffer(buffer, position);
        position += WadeDurAa.Len.WADE_DUR_AA;
        wadeDurMm.getWadeDurMmAsBuffer(buffer, position);
        position += WadeDurMm.Len.WADE_DUR_MM;
        wadeDurGg.getWadeDurGgAsBuffer(buffer, position);
        position += WadeDurGg.Len.WADE_DUR_GG;
        MarshalByte.writeString(buffer, position, wadeTpRgmFisc, Len.WADE_TP_RGM_FISC);
        position += Len.WADE_TP_RGM_FISC;
        MarshalByte.writeString(buffer, position, wadeTpRiat, Len.WADE_TP_RIAT);
        position += Len.WADE_TP_RIAT;
        MarshalByte.writeString(buffer, position, wadeTpModPagTit, Len.WADE_TP_MOD_PAG_TIT);
        position += Len.WADE_TP_MOD_PAG_TIT;
        MarshalByte.writeString(buffer, position, wadeTpIas, Len.WADE_TP_IAS);
        position += Len.WADE_TP_IAS;
        wadeDtVarzTpIas.getWadeDtVarzTpIasAsBuffer(buffer, position);
        position += WadeDtVarzTpIas.Len.WADE_DT_VARZ_TP_IAS;
        wadePreNetInd.getWadePreNetIndAsBuffer(buffer, position);
        position += WadePreNetInd.Len.WADE_PRE_NET_IND;
        wadePreLrdInd.getWadePreLrdIndAsBuffer(buffer, position);
        position += WadePreLrdInd.Len.WADE_PRE_LRD_IND;
        wadeRatLrdInd.getWadeRatLrdIndAsBuffer(buffer, position);
        position += WadeRatLrdInd.Len.WADE_RAT_LRD_IND;
        wadePrstzIniInd.getWadePrstzIniIndAsBuffer(buffer, position);
        position += WadePrstzIniInd.Len.WADE_PRSTZ_INI_IND;
        MarshalByte.writeChar(buffer, position, wadeFlCoincAssto);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wadeIbDflt, Len.WADE_IB_DFLT);
        position += Len.WADE_IB_DFLT;
        MarshalByte.writeString(buffer, position, wadeModCalc, Len.WADE_MOD_CALC);
        position += Len.WADE_MOD_CALC;
        MarshalByte.writeString(buffer, position, wadeTpFntCnbtva, Len.WADE_TP_FNT_CNBTVA);
        position += Len.WADE_TP_FNT_CNBTVA;
        wadeImpAz.getWadeImpAzAsBuffer(buffer, position);
        position += WadeImpAz.Len.WADE_IMP_AZ;
        wadeImpAder.getWadeImpAderAsBuffer(buffer, position);
        position += WadeImpAder.Len.WADE_IMP_ADER;
        wadeImpTfr.getWadeImpTfrAsBuffer(buffer, position);
        position += WadeImpTfr.Len.WADE_IMP_TFR;
        wadeImpVolo.getWadeImpVoloAsBuffer(buffer, position);
        position += WadeImpVolo.Len.WADE_IMP_VOLO;
        wadePcAz.getWadePcAzAsBuffer(buffer, position);
        position += WadePcAz.Len.WADE_PC_AZ;
        wadePcAder.getWadePcAderAsBuffer(buffer, position);
        position += WadePcAder.Len.WADE_PC_ADER;
        wadePcTfr.getWadePcTfrAsBuffer(buffer, position);
        position += WadePcTfr.Len.WADE_PC_TFR;
        wadePcVolo.getWadePcVoloAsBuffer(buffer, position);
        position += WadePcVolo.Len.WADE_PC_VOLO;
        wadeDtNovaRgmFisc.getWadeDtNovaRgmFiscAsBuffer(buffer, position);
        position += WadeDtNovaRgmFisc.Len.WADE_DT_NOVA_RGM_FISC;
        MarshalByte.writeChar(buffer, position, wadeFlAttiv);
        position += Types.CHAR_SIZE;
        wadeImpRecRitVis.getWadeImpRecRitVisAsBuffer(buffer, position);
        position += WadeImpRecRitVis.Len.WADE_IMP_REC_RIT_VIS;
        wadeImpRecRitAcc.getWadeImpRecRitAccAsBuffer(buffer, position);
        position += WadeImpRecRitAcc.Len.WADE_IMP_REC_RIT_ACC;
        MarshalByte.writeChar(buffer, position, wadeFlVarzStatTbgc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wadeFlProvzaMigraz);
        position += Types.CHAR_SIZE;
        wadeImpbVisDaRec.getWadeImpbVisDaRecAsBuffer(buffer, position);
        position += WadeImpbVisDaRec.Len.WADE_IMPB_VIS_DA_REC;
        wadeDtDecorPrestBan.getWadeDtDecorPrestBanAsBuffer(buffer, position);
        position += WadeDtDecorPrestBan.Len.WADE_DT_DECOR_PREST_BAN;
        wadeDtEffVarzStatT.getWadeDtEffVarzStatTAsBuffer(buffer, position);
        position += WadeDtEffVarzStatT.Len.WADE_DT_EFF_VARZ_STAT_T;
        MarshalByte.writeLongAsPacked(buffer, position, wadeDsRiga, Len.Int.WADE_DS_RIGA, 0);
        position += Len.WADE_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wadeDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wadeDsVer, Len.Int.WADE_DS_VER, 0);
        position += Len.WADE_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wadeDsTsIniCptz, Len.Int.WADE_DS_TS_INI_CPTZ, 0);
        position += Len.WADE_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wadeDsTsEndCptz, Len.Int.WADE_DS_TS_END_CPTZ, 0);
        position += Len.WADE_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wadeDsUtente, Len.WADE_DS_UTENTE);
        position += Len.WADE_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wadeDsStatoElab);
        position += Types.CHAR_SIZE;
        wadeCumCnbtCap.getWadeCumCnbtCapAsBuffer(buffer, position);
        position += WadeCumCnbtCap.Len.WADE_CUM_CNBT_CAP;
        wadeImpGarCnbt.getWadeImpGarCnbtAsBuffer(buffer, position);
        position += WadeImpGarCnbt.Len.WADE_IMP_GAR_CNBT;
        wadeDtUltConsCnbt.getWadeDtUltConsCnbtAsBuffer(buffer, position);
        position += WadeDtUltConsCnbt.Len.WADE_DT_ULT_CONS_CNBT;
        MarshalByte.writeString(buffer, position, wadeIdenIscFnd, Len.WADE_IDEN_ISC_FND);
        position += Len.WADE_IDEN_ISC_FND;
        wadeNumRatPian.getWadeNumRatPianAsBuffer(buffer, position);
        position += WadeNumRatPian.Len.WADE_NUM_RAT_PIAN;
        wadeDtPresc.getWadeDtPrescAsBuffer(buffer, position);
        position += WadeDtPresc.Len.WADE_DT_PRESC;
        MarshalByte.writeChar(buffer, position, wadeConcsPrest);
        return buffer;
    }

    public void initDatiSpaces() {
        wadeIdAdes = Types.INVALID_INT_VAL;
        wadeIdPoli = Types.INVALID_INT_VAL;
        wadeIdMoviCrz = Types.INVALID_INT_VAL;
        wadeIdMoviChiu.initWadeIdMoviChiuSpaces();
        wadeDtIniEff = Types.INVALID_INT_VAL;
        wadeDtEndEff = Types.INVALID_INT_VAL;
        wadeIbPrev = "";
        wadeIbOgg = "";
        wadeCodCompAnia = Types.INVALID_INT_VAL;
        wadeDtDecor.initWadeDtDecorSpaces();
        wadeDtScad.initWadeDtScadSpaces();
        wadeEtaAScad.initWadeEtaAScadSpaces();
        wadeDurAa.initWadeDurAaSpaces();
        wadeDurMm.initWadeDurMmSpaces();
        wadeDurGg.initWadeDurGgSpaces();
        wadeTpRgmFisc = "";
        wadeTpRiat = "";
        wadeTpModPagTit = "";
        wadeTpIas = "";
        wadeDtVarzTpIas.initWadeDtVarzTpIasSpaces();
        wadePreNetInd.initWadePreNetIndSpaces();
        wadePreLrdInd.initWadePreLrdIndSpaces();
        wadeRatLrdInd.initWadeRatLrdIndSpaces();
        wadePrstzIniInd.initWadePrstzIniIndSpaces();
        wadeFlCoincAssto = Types.SPACE_CHAR;
        wadeIbDflt = "";
        wadeModCalc = "";
        wadeTpFntCnbtva = "";
        wadeImpAz.initWadeImpAzSpaces();
        wadeImpAder.initWadeImpAderSpaces();
        wadeImpTfr.initWadeImpTfrSpaces();
        wadeImpVolo.initWadeImpVoloSpaces();
        wadePcAz.initWadePcAzSpaces();
        wadePcAder.initWadePcAderSpaces();
        wadePcTfr.initWadePcTfrSpaces();
        wadePcVolo.initWadePcVoloSpaces();
        wadeDtNovaRgmFisc.initWadeDtNovaRgmFiscSpaces();
        wadeFlAttiv = Types.SPACE_CHAR;
        wadeImpRecRitVis.initWadeImpRecRitVisSpaces();
        wadeImpRecRitAcc.initWadeImpRecRitAccSpaces();
        wadeFlVarzStatTbgc = Types.SPACE_CHAR;
        wadeFlProvzaMigraz = Types.SPACE_CHAR;
        wadeImpbVisDaRec.initWadeImpbVisDaRecSpaces();
        wadeDtDecorPrestBan.initWadeDtDecorPrestBanSpaces();
        wadeDtEffVarzStatT.initWadeDtEffVarzStatTSpaces();
        wadeDsRiga = Types.INVALID_LONG_VAL;
        wadeDsOperSql = Types.SPACE_CHAR;
        wadeDsVer = Types.INVALID_INT_VAL;
        wadeDsTsIniCptz = Types.INVALID_LONG_VAL;
        wadeDsTsEndCptz = Types.INVALID_LONG_VAL;
        wadeDsUtente = "";
        wadeDsStatoElab = Types.SPACE_CHAR;
        wadeCumCnbtCap.initWadeCumCnbtCapSpaces();
        wadeImpGarCnbt.initWadeImpGarCnbtSpaces();
        wadeDtUltConsCnbt.initWadeDtUltConsCnbtSpaces();
        wadeIdenIscFnd = "";
        wadeNumRatPian.initWadeNumRatPianSpaces();
        wadeDtPresc.initWadeDtPrescSpaces();
        wadeConcsPrest = Types.SPACE_CHAR;
    }

    public void setWadeIdAdes(int wadeIdAdes) {
        this.wadeIdAdes = wadeIdAdes;
    }

    public int getWadeIdAdes() {
        return this.wadeIdAdes;
    }

    public void setWadeIdPoli(int wadeIdPoli) {
        this.wadeIdPoli = wadeIdPoli;
    }

    public int getWadeIdPoli() {
        return this.wadeIdPoli;
    }

    public void setWadeIdMoviCrz(int wadeIdMoviCrz) {
        this.wadeIdMoviCrz = wadeIdMoviCrz;
    }

    public int getWadeIdMoviCrz() {
        return this.wadeIdMoviCrz;
    }

    public void setWadeDtIniEff(int wadeDtIniEff) {
        this.wadeDtIniEff = wadeDtIniEff;
    }

    public int getWadeDtIniEff() {
        return this.wadeDtIniEff;
    }

    public void setWadeDtEndEff(int wadeDtEndEff) {
        this.wadeDtEndEff = wadeDtEndEff;
    }

    public int getWadeDtEndEff() {
        return this.wadeDtEndEff;
    }

    public void setWadeIbPrev(String wadeIbPrev) {
        this.wadeIbPrev = Functions.subString(wadeIbPrev, Len.WADE_IB_PREV);
    }

    public String getWadeIbPrev() {
        return this.wadeIbPrev;
    }

    public void setWadeIbOgg(String wadeIbOgg) {
        this.wadeIbOgg = Functions.subString(wadeIbOgg, Len.WADE_IB_OGG);
    }

    public String getWadeIbOgg() {
        return this.wadeIbOgg;
    }

    public void setWadeCodCompAnia(int wadeCodCompAnia) {
        this.wadeCodCompAnia = wadeCodCompAnia;
    }

    public int getWadeCodCompAnia() {
        return this.wadeCodCompAnia;
    }

    public void setWadeTpRgmFisc(String wadeTpRgmFisc) {
        this.wadeTpRgmFisc = Functions.subString(wadeTpRgmFisc, Len.WADE_TP_RGM_FISC);
    }

    public String getWadeTpRgmFisc() {
        return this.wadeTpRgmFisc;
    }

    public void setWadeTpRiat(String wadeTpRiat) {
        this.wadeTpRiat = Functions.subString(wadeTpRiat, Len.WADE_TP_RIAT);
    }

    public String getWadeTpRiat() {
        return this.wadeTpRiat;
    }

    public void setWadeTpModPagTit(String wadeTpModPagTit) {
        this.wadeTpModPagTit = Functions.subString(wadeTpModPagTit, Len.WADE_TP_MOD_PAG_TIT);
    }

    public String getWadeTpModPagTit() {
        return this.wadeTpModPagTit;
    }

    public void setWadeTpIas(String wadeTpIas) {
        this.wadeTpIas = Functions.subString(wadeTpIas, Len.WADE_TP_IAS);
    }

    public String getWadeTpIas() {
        return this.wadeTpIas;
    }

    public void setWadeFlCoincAssto(char wadeFlCoincAssto) {
        this.wadeFlCoincAssto = wadeFlCoincAssto;
    }

    public char getWadeFlCoincAssto() {
        return this.wadeFlCoincAssto;
    }

    public void setWadeIbDflt(String wadeIbDflt) {
        this.wadeIbDflt = Functions.subString(wadeIbDflt, Len.WADE_IB_DFLT);
    }

    public String getWadeIbDflt() {
        return this.wadeIbDflt;
    }

    public void setWadeModCalc(String wadeModCalc) {
        this.wadeModCalc = Functions.subString(wadeModCalc, Len.WADE_MOD_CALC);
    }

    public String getWadeModCalc() {
        return this.wadeModCalc;
    }

    public String getWadeModCalcFormatted() {
        return Functions.padBlanks(getWadeModCalc(), Len.WADE_MOD_CALC);
    }

    public void setWadeTpFntCnbtva(String wadeTpFntCnbtva) {
        this.wadeTpFntCnbtva = Functions.subString(wadeTpFntCnbtva, Len.WADE_TP_FNT_CNBTVA);
    }

    public String getWadeTpFntCnbtva() {
        return this.wadeTpFntCnbtva;
    }

    public void setWadeFlAttiv(char wadeFlAttiv) {
        this.wadeFlAttiv = wadeFlAttiv;
    }

    public char getWadeFlAttiv() {
        return this.wadeFlAttiv;
    }

    public void setWadeFlVarzStatTbgc(char wadeFlVarzStatTbgc) {
        this.wadeFlVarzStatTbgc = wadeFlVarzStatTbgc;
    }

    public char getWadeFlVarzStatTbgc() {
        return this.wadeFlVarzStatTbgc;
    }

    public void setWadeFlProvzaMigraz(char wadeFlProvzaMigraz) {
        this.wadeFlProvzaMigraz = wadeFlProvzaMigraz;
    }

    public char getWadeFlProvzaMigraz() {
        return this.wadeFlProvzaMigraz;
    }

    public void setWadeDsRiga(long wadeDsRiga) {
        this.wadeDsRiga = wadeDsRiga;
    }

    public long getWadeDsRiga() {
        return this.wadeDsRiga;
    }

    public void setWadeDsOperSql(char wadeDsOperSql) {
        this.wadeDsOperSql = wadeDsOperSql;
    }

    public char getWadeDsOperSql() {
        return this.wadeDsOperSql;
    }

    public void setWadeDsVer(int wadeDsVer) {
        this.wadeDsVer = wadeDsVer;
    }

    public int getWadeDsVer() {
        return this.wadeDsVer;
    }

    public void setWadeDsTsIniCptz(long wadeDsTsIniCptz) {
        this.wadeDsTsIniCptz = wadeDsTsIniCptz;
    }

    public long getWadeDsTsIniCptz() {
        return this.wadeDsTsIniCptz;
    }

    public void setWadeDsTsEndCptz(long wadeDsTsEndCptz) {
        this.wadeDsTsEndCptz = wadeDsTsEndCptz;
    }

    public long getWadeDsTsEndCptz() {
        return this.wadeDsTsEndCptz;
    }

    public void setWadeDsUtente(String wadeDsUtente) {
        this.wadeDsUtente = Functions.subString(wadeDsUtente, Len.WADE_DS_UTENTE);
    }

    public String getWadeDsUtente() {
        return this.wadeDsUtente;
    }

    public void setWadeDsStatoElab(char wadeDsStatoElab) {
        this.wadeDsStatoElab = wadeDsStatoElab;
    }

    public char getWadeDsStatoElab() {
        return this.wadeDsStatoElab;
    }

    public void setWadeIdenIscFnd(String wadeIdenIscFnd) {
        this.wadeIdenIscFnd = Functions.subString(wadeIdenIscFnd, Len.WADE_IDEN_ISC_FND);
    }

    public String getWadeIdenIscFnd() {
        return this.wadeIdenIscFnd;
    }

    public void setWadeConcsPrest(char wadeConcsPrest) {
        this.wadeConcsPrest = wadeConcsPrest;
    }

    public char getWadeConcsPrest() {
        return this.wadeConcsPrest;
    }

    public WadeCumCnbtCap getWadeCumCnbtCap() {
        return wadeCumCnbtCap;
    }

    public WadeDtDecor getWadeDtDecor() {
        return wadeDtDecor;
    }

    public WadeDtDecorPrestBan getWadeDtDecorPrestBan() {
        return wadeDtDecorPrestBan;
    }

    public WadeDtEffVarzStatT getWadeDtEffVarzStatT() {
        return wadeDtEffVarzStatT;
    }

    public WadeDtNovaRgmFisc getWadeDtNovaRgmFisc() {
        return wadeDtNovaRgmFisc;
    }

    public WadeDtPresc getWadeDtPresc() {
        return wadeDtPresc;
    }

    public WadeDtScad getWadeDtScad() {
        return wadeDtScad;
    }

    public WadeDtUltConsCnbt getWadeDtUltConsCnbt() {
        return wadeDtUltConsCnbt;
    }

    public WadeDtVarzTpIas getWadeDtVarzTpIas() {
        return wadeDtVarzTpIas;
    }

    public WadeDurAa getWadeDurAa() {
        return wadeDurAa;
    }

    public WadeDurGg getWadeDurGg() {
        return wadeDurGg;
    }

    public WadeDurMm getWadeDurMm() {
        return wadeDurMm;
    }

    public WadeEtaAScad getWadeEtaAScad() {
        return wadeEtaAScad;
    }

    public WadeIdMoviChiu getWadeIdMoviChiu() {
        return wadeIdMoviChiu;
    }

    public WadeImpAder getWadeImpAder() {
        return wadeImpAder;
    }

    public WadeImpAz getWadeImpAz() {
        return wadeImpAz;
    }

    public WadeImpGarCnbt getWadeImpGarCnbt() {
        return wadeImpGarCnbt;
    }

    public WadeImpRecRitAcc getWadeImpRecRitAcc() {
        return wadeImpRecRitAcc;
    }

    public WadeImpRecRitVis getWadeImpRecRitVis() {
        return wadeImpRecRitVis;
    }

    public WadeImpTfr getWadeImpTfr() {
        return wadeImpTfr;
    }

    public WadeImpVolo getWadeImpVolo() {
        return wadeImpVolo;
    }

    public WadeImpbVisDaRec getWadeImpbVisDaRec() {
        return wadeImpbVisDaRec;
    }

    public WadeNumRatPian getWadeNumRatPian() {
        return wadeNumRatPian;
    }

    public WadePcAder getWadePcAder() {
        return wadePcAder;
    }

    public WadePcAz getWadePcAz() {
        return wadePcAz;
    }

    public WadePcTfr getWadePcTfr() {
        return wadePcTfr;
    }

    public WadePcVolo getWadePcVolo() {
        return wadePcVolo;
    }

    public WadePreLrdInd getWadePreLrdInd() {
        return wadePreLrdInd;
    }

    public WadePreNetInd getWadePreNetInd() {
        return wadePreNetInd;
    }

    public WadePrstzIniInd getWadePrstzIniInd() {
        return wadePrstzIniInd;
    }

    public WadeRatLrdInd getWadeRatLrdInd() {
        return wadeRatLrdInd;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_IB_PREV = 40;
        public static final int WADE_IB_OGG = 40;
        public static final int WADE_TP_RGM_FISC = 2;
        public static final int WADE_TP_RIAT = 20;
        public static final int WADE_TP_MOD_PAG_TIT = 2;
        public static final int WADE_TP_IAS = 2;
        public static final int WADE_IB_DFLT = 40;
        public static final int WADE_MOD_CALC = 2;
        public static final int WADE_TP_FNT_CNBTVA = 2;
        public static final int WADE_DS_UTENTE = 20;
        public static final int WADE_IDEN_ISC_FND = 40;
        public static final int WADE_ID_ADES = 5;
        public static final int WADE_ID_POLI = 5;
        public static final int WADE_ID_MOVI_CRZ = 5;
        public static final int WADE_DT_INI_EFF = 5;
        public static final int WADE_DT_END_EFF = 5;
        public static final int WADE_COD_COMP_ANIA = 3;
        public static final int WADE_FL_COINC_ASSTO = 1;
        public static final int WADE_FL_ATTIV = 1;
        public static final int WADE_FL_VARZ_STAT_TBGC = 1;
        public static final int WADE_FL_PROVZA_MIGRAZ = 1;
        public static final int WADE_DS_RIGA = 6;
        public static final int WADE_DS_OPER_SQL = 1;
        public static final int WADE_DS_VER = 5;
        public static final int WADE_DS_TS_INI_CPTZ = 10;
        public static final int WADE_DS_TS_END_CPTZ = 10;
        public static final int WADE_DS_STATO_ELAB = 1;
        public static final int WADE_CONCS_PREST = 1;
        public static final int DATI = WADE_ID_ADES + WADE_ID_POLI + WADE_ID_MOVI_CRZ + WadeIdMoviChiu.Len.WADE_ID_MOVI_CHIU + WADE_DT_INI_EFF + WADE_DT_END_EFF + WADE_IB_PREV + WADE_IB_OGG + WADE_COD_COMP_ANIA + WadeDtDecor.Len.WADE_DT_DECOR + WadeDtScad.Len.WADE_DT_SCAD + WadeEtaAScad.Len.WADE_ETA_A_SCAD + WadeDurAa.Len.WADE_DUR_AA + WadeDurMm.Len.WADE_DUR_MM + WadeDurGg.Len.WADE_DUR_GG + WADE_TP_RGM_FISC + WADE_TP_RIAT + WADE_TP_MOD_PAG_TIT + WADE_TP_IAS + WadeDtVarzTpIas.Len.WADE_DT_VARZ_TP_IAS + WadePreNetInd.Len.WADE_PRE_NET_IND + WadePreLrdInd.Len.WADE_PRE_LRD_IND + WadeRatLrdInd.Len.WADE_RAT_LRD_IND + WadePrstzIniInd.Len.WADE_PRSTZ_INI_IND + WADE_FL_COINC_ASSTO + WADE_IB_DFLT + WADE_MOD_CALC + WADE_TP_FNT_CNBTVA + WadeImpAz.Len.WADE_IMP_AZ + WadeImpAder.Len.WADE_IMP_ADER + WadeImpTfr.Len.WADE_IMP_TFR + WadeImpVolo.Len.WADE_IMP_VOLO + WadePcAz.Len.WADE_PC_AZ + WadePcAder.Len.WADE_PC_ADER + WadePcTfr.Len.WADE_PC_TFR + WadePcVolo.Len.WADE_PC_VOLO + WadeDtNovaRgmFisc.Len.WADE_DT_NOVA_RGM_FISC + WADE_FL_ATTIV + WadeImpRecRitVis.Len.WADE_IMP_REC_RIT_VIS + WadeImpRecRitAcc.Len.WADE_IMP_REC_RIT_ACC + WADE_FL_VARZ_STAT_TBGC + WADE_FL_PROVZA_MIGRAZ + WadeImpbVisDaRec.Len.WADE_IMPB_VIS_DA_REC + WadeDtDecorPrestBan.Len.WADE_DT_DECOR_PREST_BAN + WadeDtEffVarzStatT.Len.WADE_DT_EFF_VARZ_STAT_T + WADE_DS_RIGA + WADE_DS_OPER_SQL + WADE_DS_VER + WADE_DS_TS_INI_CPTZ + WADE_DS_TS_END_CPTZ + WADE_DS_UTENTE + WADE_DS_STATO_ELAB + WadeCumCnbtCap.Len.WADE_CUM_CNBT_CAP + WadeImpGarCnbt.Len.WADE_IMP_GAR_CNBT + WadeDtUltConsCnbt.Len.WADE_DT_ULT_CONS_CNBT + WADE_IDEN_ISC_FND + WadeNumRatPian.Len.WADE_NUM_RAT_PIAN + WadeDtPresc.Len.WADE_DT_PRESC + WADE_CONCS_PREST;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_ID_ADES = 9;
            public static final int WADE_ID_POLI = 9;
            public static final int WADE_ID_MOVI_CRZ = 9;
            public static final int WADE_DT_INI_EFF = 8;
            public static final int WADE_DT_END_EFF = 8;
            public static final int WADE_COD_COMP_ANIA = 5;
            public static final int WADE_DS_RIGA = 10;
            public static final int WADE_DS_VER = 9;
            public static final int WADE_DS_TS_INI_CPTZ = 18;
            public static final int WADE_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
