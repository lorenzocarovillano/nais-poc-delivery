package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Ivvc0221FormaTecnica;
import it.accenture.jnais.ws.occurs.Ivvc0221TabDomanda;

/**Original name: IVVC0221-DETT-QUEST-WC<br>
 * Variable: IVVC0221-DETT-QUEST-WC from copybook IVVC0221<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0221DettQuestWc {

    //==== PROPERTIES ====
    public static final int TAB_DOMANDA_MAXOCCURS = 5;
    //Original name: IVVC0221-FORMA-TECNICA
    private Ivvc0221FormaTecnica formaTecnica = new Ivvc0221FormaTecnica();
    //Original name: IVVC0221-FILLER
    private char filler = DefaultValues.CHAR_VAL;
    //Original name: IVVC0221-COD-QUEST
    private String codQuest = DefaultValues.stringVal(Len.COD_QUEST);
    //Original name: IVVC0221-FILLER-1
    private char filler1 = DefaultValues.CHAR_VAL;
    //Original name: IVVC0221-MAX-DOMANDA
    private String maxDomanda = DefaultValues.stringVal(Len.MAX_DOMANDA);
    //Original name: IVVC0221-FILLER-2
    private char filler2 = DefaultValues.CHAR_VAL;
    //Original name: IVVC0221-TAB-DOMANDA
    private Ivvc0221TabDomanda[] tabDomanda = new Ivvc0221TabDomanda[TAB_DOMANDA_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ivvc0221DettQuestWc() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabDomandaIdx = 1; tabDomandaIdx <= TAB_DOMANDA_MAXOCCURS; tabDomandaIdx++) {
            tabDomanda[tabDomandaIdx - 1] = new Ivvc0221TabDomanda();
        }
    }

    public void setIvvc0221DettQuestWcFormatted(String data) {
        byte[] buffer = new byte[Len.IVVC0221_DETT_QUEST_WC];
        MarshalByte.writeString(buffer, 1, data, Len.IVVC0221_DETT_QUEST_WC);
        setIvvc0221DettQuestWcBytes(buffer, 1);
    }

    public void setIvvc0221DettQuestWcBytes(byte[] buffer, int offset) {
        int position = offset;
        formaTecnica.setFormaTecnica(MarshalByte.readString(buffer, position, Ivvc0221FormaTecnica.Len.FORMA_TECNICA));
        position += Ivvc0221FormaTecnica.Len.FORMA_TECNICA;
        filler = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        codQuest = MarshalByte.readString(buffer, position, Len.COD_QUEST);
        position += Len.COD_QUEST;
        filler1 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        maxDomanda = MarshalByte.readFixedString(buffer, position, Len.MAX_DOMANDA);
        position += Len.MAX_DOMANDA;
        filler2 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        for (int idx = 1; idx <= TAB_DOMANDA_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabDomanda[idx - 1].setTabDomandaBytes(buffer, position);
                position += Ivvc0221TabDomanda.Len.TAB_DOMANDA;
            }
            else {
                tabDomanda[idx - 1].initTabDomandaSpaces();
                position += Ivvc0221TabDomanda.Len.TAB_DOMANDA;
            }
        }
    }

    public void setFiller(char filler) {
        this.filler = filler;
    }

    public char getFiller() {
        return this.filler;
    }

    public void setCodQuest(String codQuest) {
        this.codQuest = Functions.subString(codQuest, Len.COD_QUEST);
    }

    public String getCodQuest() {
        return this.codQuest;
    }

    public void setFiller1(char filler1) {
        this.filler1 = filler1;
    }

    public char getFiller1() {
        return this.filler1;
    }

    public short getMaxDomanda() {
        return NumericDisplay.asShort(this.maxDomanda);
    }

    public void setFiller2(char filler2) {
        this.filler2 = filler2;
    }

    public char getFiller2() {
        return this.filler2;
    }

    public Ivvc0221FormaTecnica getFormaTecnica() {
        return formaTecnica;
    }

    public Ivvc0221TabDomanda getTabDomanda(int idx) {
        return tabDomanda[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_QUEST = 15;
        public static final int MAX_DOMANDA = 2;
        public static final int FILLER = 1;
        public static final int FILLER1 = 1;
        public static final int FILLER2 = 1;
        public static final int IVVC0221_DETT_QUEST_WC = Ivvc0221FormaTecnica.Len.FORMA_TECNICA + FILLER + COD_QUEST + FILLER1 + MAX_DOMANDA + FILLER2 + Ivvc0221DettQuestWc.TAB_DOMANDA_MAXOCCURS * Ivvc0221TabDomanda.Len.TAB_DOMANDA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
