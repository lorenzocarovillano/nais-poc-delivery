package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.TgaAbbAnnuUlt;
import it.accenture.jnais.ws.redefines.TgaAbbTotIni;
import it.accenture.jnais.ws.redefines.TgaAbbTotUlt;
import it.accenture.jnais.ws.redefines.TgaAcqExp;
import it.accenture.jnais.ws.redefines.TgaAlqCommisInter;
import it.accenture.jnais.ws.redefines.TgaAlqProvAcq;
import it.accenture.jnais.ws.redefines.TgaAlqProvInc;
import it.accenture.jnais.ws.redefines.TgaAlqProvRicor;
import it.accenture.jnais.ws.redefines.TgaAlqRemunAss;
import it.accenture.jnais.ws.redefines.TgaAlqScon;
import it.accenture.jnais.ws.redefines.TgaBnsGiaLiqto;
import it.accenture.jnais.ws.redefines.TgaCommisGest;
import it.accenture.jnais.ws.redefines.TgaCommisInter;
import it.accenture.jnais.ws.redefines.TgaCosRunAssva;
import it.accenture.jnais.ws.redefines.TgaCosRunAssvaIdc;
import it.accenture.jnais.ws.redefines.TgaCptInOpzRivto;
import it.accenture.jnais.ws.redefines.TgaCptMinScad;
import it.accenture.jnais.ws.redefines.TgaCptRshMor;
import it.accenture.jnais.ws.redefines.TgaDtEffStab;
import it.accenture.jnais.ws.redefines.TgaDtEmis;
import it.accenture.jnais.ws.redefines.TgaDtIniValTar;
import it.accenture.jnais.ws.redefines.TgaDtScad;
import it.accenture.jnais.ws.redefines.TgaDtUltAdegPrePr;
import it.accenture.jnais.ws.redefines.TgaDtVldtProd;
import it.accenture.jnais.ws.redefines.TgaDurAa;
import it.accenture.jnais.ws.redefines.TgaDurAbb;
import it.accenture.jnais.ws.redefines.TgaDurGg;
import it.accenture.jnais.ws.redefines.TgaDurMm;
import it.accenture.jnais.ws.redefines.TgaEtaAa1oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaAa2oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaAa3oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm1oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm2oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm3oAssto;
import it.accenture.jnais.ws.redefines.TgaIdMoviChiu;
import it.accenture.jnais.ws.redefines.TgaImpAder;
import it.accenture.jnais.ws.redefines.TgaImpAltSopr;
import it.accenture.jnais.ws.redefines.TgaImpAz;
import it.accenture.jnais.ws.redefines.TgaImpbCommisInter;
import it.accenture.jnais.ws.redefines.TgaImpBns;
import it.accenture.jnais.ws.redefines.TgaImpBnsAntic;
import it.accenture.jnais.ws.redefines.TgaImpbProvAcq;
import it.accenture.jnais.ws.redefines.TgaImpbProvInc;
import it.accenture.jnais.ws.redefines.TgaImpbProvRicor;
import it.accenture.jnais.ws.redefines.TgaImpbRemunAss;
import it.accenture.jnais.ws.redefines.TgaImpbVisEnd2000;
import it.accenture.jnais.ws.redefines.TgaImpCarAcq;
import it.accenture.jnais.ws.redefines.TgaImpCarGest;
import it.accenture.jnais.ws.redefines.TgaImpCarInc;
import it.accenture.jnais.ws.redefines.TgaImpScon;
import it.accenture.jnais.ws.redefines.TgaImpSoprProf;
import it.accenture.jnais.ws.redefines.TgaImpSoprSan;
import it.accenture.jnais.ws.redefines.TgaImpSoprSpo;
import it.accenture.jnais.ws.redefines.TgaImpSoprTec;
import it.accenture.jnais.ws.redefines.TgaImpTfr;
import it.accenture.jnais.ws.redefines.TgaImpTfrStrc;
import it.accenture.jnais.ws.redefines.TgaImpTrasfe;
import it.accenture.jnais.ws.redefines.TgaImpVolo;
import it.accenture.jnais.ws.redefines.TgaIncrPre;
import it.accenture.jnais.ws.redefines.TgaIncrPrstz;
import it.accenture.jnais.ws.redefines.TgaIntrMora;
import it.accenture.jnais.ws.redefines.TgaManfeeAntic;
import it.accenture.jnais.ws.redefines.TgaManfeeRicor;
import it.accenture.jnais.ws.redefines.TgaMatuEnd2000;
import it.accenture.jnais.ws.redefines.TgaMinGarto;
import it.accenture.jnais.ws.redefines.TgaMinTrnut;
import it.accenture.jnais.ws.redefines.TgaNumGgRival;
import it.accenture.jnais.ws.redefines.TgaOldTsTec;
import it.accenture.jnais.ws.redefines.TgaPcCommisGest;
import it.accenture.jnais.ws.redefines.TgaPcIntrRiat;
import it.accenture.jnais.ws.redefines.TgaPcRetr;
import it.accenture.jnais.ws.redefines.TgaPcRipPre;
import it.accenture.jnais.ws.redefines.TgaPreAttDiTrch;
import it.accenture.jnais.ws.redefines.TgaPreCasoMor;
import it.accenture.jnais.ws.redefines.TgaPreIniNet;
import it.accenture.jnais.ws.redefines.TgaPreInvrioIni;
import it.accenture.jnais.ws.redefines.TgaPreInvrioUlt;
import it.accenture.jnais.ws.redefines.TgaPreLrd;
import it.accenture.jnais.ws.redefines.TgaPrePattuito;
import it.accenture.jnais.ws.redefines.TgaPrePpIni;
import it.accenture.jnais.ws.redefines.TgaPrePpUlt;
import it.accenture.jnais.ws.redefines.TgaPreRivto;
import it.accenture.jnais.ws.redefines.TgaPreStab;
import it.accenture.jnais.ws.redefines.TgaPreTariIni;
import it.accenture.jnais.ws.redefines.TgaPreTariUlt;
import it.accenture.jnais.ws.redefines.TgaPreUniRivto;
import it.accenture.jnais.ws.redefines.TgaProv1aaAcq;
import it.accenture.jnais.ws.redefines.TgaProv2aaAcq;
import it.accenture.jnais.ws.redefines.TgaProvInc;
import it.accenture.jnais.ws.redefines.TgaProvRicor;
import it.accenture.jnais.ws.redefines.TgaPrstzAggIni;
import it.accenture.jnais.ws.redefines.TgaPrstzAggUlt;
import it.accenture.jnais.ws.redefines.TgaPrstzIni;
import it.accenture.jnais.ws.redefines.TgaPrstzIniNewfis;
import it.accenture.jnais.ws.redefines.TgaPrstzIniNforz;
import it.accenture.jnais.ws.redefines.TgaPrstzIniStab;
import it.accenture.jnais.ws.redefines.TgaPrstzRidIni;
import it.accenture.jnais.ws.redefines.TgaPrstzUlt;
import it.accenture.jnais.ws.redefines.TgaRatLrd;
import it.accenture.jnais.ws.redefines.TgaRemunAss;
import it.accenture.jnais.ws.redefines.TgaRendtoLrd;
import it.accenture.jnais.ws.redefines.TgaRendtoRetr;
import it.accenture.jnais.ws.redefines.TgaRenIniTsTec0;
import it.accenture.jnais.ws.redefines.TgaRisMat;
import it.accenture.jnais.ws.redefines.TgaTsRivalFis;
import it.accenture.jnais.ws.redefines.TgaTsRivalIndiciz;
import it.accenture.jnais.ws.redefines.TgaTsRivalNet;
import it.accenture.jnais.ws.redefines.TgaVisEnd2000;
import it.accenture.jnais.ws.redefines.TgaVisEnd2000Nforz;

/**Original name: TRCH-DI-GAR<br>
 * Variable: TRCH-DI-GAR from copybook IDBVTGA1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TrchDiGarIvvs0216 {

    //==== PROPERTIES ====
    //Original name: TGA-ID-TRCH-DI-GAR
    private int tgaIdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: TGA-ID-GAR
    private int tgaIdGar = DefaultValues.INT_VAL;
    //Original name: TGA-ID-ADES
    private int tgaIdAdes = DefaultValues.INT_VAL;
    //Original name: TGA-ID-POLI
    private int tgaIdPoli = DefaultValues.INT_VAL;
    //Original name: TGA-ID-MOVI-CRZ
    private int tgaIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: TGA-ID-MOVI-CHIU
    private TgaIdMoviChiu tgaIdMoviChiu = new TgaIdMoviChiu();
    //Original name: TGA-DT-INI-EFF
    private int tgaDtIniEff = DefaultValues.INT_VAL;
    //Original name: TGA-DT-END-EFF
    private int tgaDtEndEff = DefaultValues.INT_VAL;
    //Original name: TGA-COD-COMP-ANIA
    private int tgaCodCompAnia = DefaultValues.INT_VAL;
    //Original name: TGA-DT-DECOR
    private int tgaDtDecor = DefaultValues.INT_VAL;
    //Original name: TGA-DT-SCAD
    private TgaDtScad tgaDtScad = new TgaDtScad();
    //Original name: TGA-IB-OGG
    private String tgaIbOgg = DefaultValues.stringVal(Len.TGA_IB_OGG);
    //Original name: TGA-TP-RGM-FISC
    private String tgaTpRgmFisc = DefaultValues.stringVal(Len.TGA_TP_RGM_FISC);
    //Original name: TGA-DT-EMIS
    private TgaDtEmis tgaDtEmis = new TgaDtEmis();
    //Original name: TGA-TP-TRCH
    private String tgaTpTrch = DefaultValues.stringVal(Len.TGA_TP_TRCH);
    //Original name: TGA-DUR-AA
    private TgaDurAa tgaDurAa = new TgaDurAa();
    //Original name: TGA-DUR-MM
    private TgaDurMm tgaDurMm = new TgaDurMm();
    //Original name: TGA-DUR-GG
    private TgaDurGg tgaDurGg = new TgaDurGg();
    //Original name: TGA-PRE-CASO-MOR
    private TgaPreCasoMor tgaPreCasoMor = new TgaPreCasoMor();
    //Original name: TGA-PC-INTR-RIAT
    private TgaPcIntrRiat tgaPcIntrRiat = new TgaPcIntrRiat();
    //Original name: TGA-IMP-BNS-ANTIC
    private TgaImpBnsAntic tgaImpBnsAntic = new TgaImpBnsAntic();
    //Original name: TGA-PRE-INI-NET
    private TgaPreIniNet tgaPreIniNet = new TgaPreIniNet();
    //Original name: TGA-PRE-PP-INI
    private TgaPrePpIni tgaPrePpIni = new TgaPrePpIni();
    //Original name: TGA-PRE-PP-ULT
    private TgaPrePpUlt tgaPrePpUlt = new TgaPrePpUlt();
    //Original name: TGA-PRE-TARI-INI
    private TgaPreTariIni tgaPreTariIni = new TgaPreTariIni();
    //Original name: TGA-PRE-TARI-ULT
    private TgaPreTariUlt tgaPreTariUlt = new TgaPreTariUlt();
    //Original name: TGA-PRE-INVRIO-INI
    private TgaPreInvrioIni tgaPreInvrioIni = new TgaPreInvrioIni();
    //Original name: TGA-PRE-INVRIO-ULT
    private TgaPreInvrioUlt tgaPreInvrioUlt = new TgaPreInvrioUlt();
    //Original name: TGA-PRE-RIVTO
    private TgaPreRivto tgaPreRivto = new TgaPreRivto();
    //Original name: TGA-IMP-SOPR-PROF
    private TgaImpSoprProf tgaImpSoprProf = new TgaImpSoprProf();
    //Original name: TGA-IMP-SOPR-SAN
    private TgaImpSoprSan tgaImpSoprSan = new TgaImpSoprSan();
    //Original name: TGA-IMP-SOPR-SPO
    private TgaImpSoprSpo tgaImpSoprSpo = new TgaImpSoprSpo();
    //Original name: TGA-IMP-SOPR-TEC
    private TgaImpSoprTec tgaImpSoprTec = new TgaImpSoprTec();
    //Original name: TGA-IMP-ALT-SOPR
    private TgaImpAltSopr tgaImpAltSopr = new TgaImpAltSopr();
    //Original name: TGA-PRE-STAB
    private TgaPreStab tgaPreStab = new TgaPreStab();
    //Original name: TGA-DT-EFF-STAB
    private TgaDtEffStab tgaDtEffStab = new TgaDtEffStab();
    //Original name: TGA-TS-RIVAL-FIS
    private TgaTsRivalFis tgaTsRivalFis = new TgaTsRivalFis();
    //Original name: TGA-TS-RIVAL-INDICIZ
    private TgaTsRivalIndiciz tgaTsRivalIndiciz = new TgaTsRivalIndiciz();
    //Original name: TGA-OLD-TS-TEC
    private TgaOldTsTec tgaOldTsTec = new TgaOldTsTec();
    //Original name: TGA-RAT-LRD
    private TgaRatLrd tgaRatLrd = new TgaRatLrd();
    //Original name: TGA-PRE-LRD
    private TgaPreLrd tgaPreLrd = new TgaPreLrd();
    //Original name: TGA-PRSTZ-INI
    private TgaPrstzIni tgaPrstzIni = new TgaPrstzIni();
    //Original name: TGA-PRSTZ-ULT
    private TgaPrstzUlt tgaPrstzUlt = new TgaPrstzUlt();
    //Original name: TGA-CPT-IN-OPZ-RIVTO
    private TgaCptInOpzRivto tgaCptInOpzRivto = new TgaCptInOpzRivto();
    //Original name: TGA-PRSTZ-INI-STAB
    private TgaPrstzIniStab tgaPrstzIniStab = new TgaPrstzIniStab();
    //Original name: TGA-CPT-RSH-MOR
    private TgaCptRshMor tgaCptRshMor = new TgaCptRshMor();
    //Original name: TGA-PRSTZ-RID-INI
    private TgaPrstzRidIni tgaPrstzRidIni = new TgaPrstzRidIni();
    //Original name: TGA-FL-CAR-CONT
    private char tgaFlCarCont = DefaultValues.CHAR_VAL;
    //Original name: TGA-BNS-GIA-LIQTO
    private TgaBnsGiaLiqto tgaBnsGiaLiqto = new TgaBnsGiaLiqto();
    //Original name: TGA-IMP-BNS
    private TgaImpBns tgaImpBns = new TgaImpBns();
    //Original name: TGA-COD-DVS
    private String tgaCodDvs = DefaultValues.stringVal(Len.TGA_COD_DVS);
    //Original name: TGA-PRSTZ-INI-NEWFIS
    private TgaPrstzIniNewfis tgaPrstzIniNewfis = new TgaPrstzIniNewfis();
    //Original name: TGA-IMP-SCON
    private TgaImpScon tgaImpScon = new TgaImpScon();
    //Original name: TGA-ALQ-SCON
    private TgaAlqScon tgaAlqScon = new TgaAlqScon();
    //Original name: TGA-IMP-CAR-ACQ
    private TgaImpCarAcq tgaImpCarAcq = new TgaImpCarAcq();
    //Original name: TGA-IMP-CAR-INC
    private TgaImpCarInc tgaImpCarInc = new TgaImpCarInc();
    //Original name: TGA-IMP-CAR-GEST
    private TgaImpCarGest tgaImpCarGest = new TgaImpCarGest();
    //Original name: TGA-ETA-AA-1O-ASSTO
    private TgaEtaAa1oAssto tgaEtaAa1oAssto = new TgaEtaAa1oAssto();
    //Original name: TGA-ETA-MM-1O-ASSTO
    private TgaEtaMm1oAssto tgaEtaMm1oAssto = new TgaEtaMm1oAssto();
    //Original name: TGA-ETA-AA-2O-ASSTO
    private TgaEtaAa2oAssto tgaEtaAa2oAssto = new TgaEtaAa2oAssto();
    //Original name: TGA-ETA-MM-2O-ASSTO
    private TgaEtaMm2oAssto tgaEtaMm2oAssto = new TgaEtaMm2oAssto();
    //Original name: TGA-ETA-AA-3O-ASSTO
    private TgaEtaAa3oAssto tgaEtaAa3oAssto = new TgaEtaAa3oAssto();
    //Original name: TGA-ETA-MM-3O-ASSTO
    private TgaEtaMm3oAssto tgaEtaMm3oAssto = new TgaEtaMm3oAssto();
    //Original name: TGA-RENDTO-LRD
    private TgaRendtoLrd tgaRendtoLrd = new TgaRendtoLrd();
    //Original name: TGA-PC-RETR
    private TgaPcRetr tgaPcRetr = new TgaPcRetr();
    //Original name: TGA-RENDTO-RETR
    private TgaRendtoRetr tgaRendtoRetr = new TgaRendtoRetr();
    //Original name: TGA-MIN-GARTO
    private TgaMinGarto tgaMinGarto = new TgaMinGarto();
    //Original name: TGA-MIN-TRNUT
    private TgaMinTrnut tgaMinTrnut = new TgaMinTrnut();
    //Original name: TGA-PRE-ATT-DI-TRCH
    private TgaPreAttDiTrch tgaPreAttDiTrch = new TgaPreAttDiTrch();
    //Original name: TGA-MATU-END2000
    private TgaMatuEnd2000 tgaMatuEnd2000 = new TgaMatuEnd2000();
    //Original name: TGA-ABB-TOT-INI
    private TgaAbbTotIni tgaAbbTotIni = new TgaAbbTotIni();
    //Original name: TGA-ABB-TOT-ULT
    private TgaAbbTotUlt tgaAbbTotUlt = new TgaAbbTotUlt();
    //Original name: TGA-ABB-ANNU-ULT
    private TgaAbbAnnuUlt tgaAbbAnnuUlt = new TgaAbbAnnuUlt();
    //Original name: TGA-DUR-ABB
    private TgaDurAbb tgaDurAbb = new TgaDurAbb();
    //Original name: TGA-TP-ADEG-ABB
    private char tgaTpAdegAbb = DefaultValues.CHAR_VAL;
    //Original name: TGA-MOD-CALC
    private String tgaModCalc = DefaultValues.stringVal(Len.TGA_MOD_CALC);
    //Original name: TGA-IMP-AZ
    private TgaImpAz tgaImpAz = new TgaImpAz();
    //Original name: TGA-IMP-ADER
    private TgaImpAder tgaImpAder = new TgaImpAder();
    //Original name: TGA-IMP-TFR
    private TgaImpTfr tgaImpTfr = new TgaImpTfr();
    //Original name: TGA-IMP-VOLO
    private TgaImpVolo tgaImpVolo = new TgaImpVolo();
    //Original name: TGA-VIS-END2000
    private TgaVisEnd2000 tgaVisEnd2000 = new TgaVisEnd2000();
    //Original name: TGA-DT-VLDT-PROD
    private TgaDtVldtProd tgaDtVldtProd = new TgaDtVldtProd();
    //Original name: TGA-DT-INI-VAL-TAR
    private TgaDtIniValTar tgaDtIniValTar = new TgaDtIniValTar();
    //Original name: TGA-IMPB-VIS-END2000
    private TgaImpbVisEnd2000 tgaImpbVisEnd2000 = new TgaImpbVisEnd2000();
    //Original name: TGA-REN-INI-TS-TEC-0
    private TgaRenIniTsTec0 tgaRenIniTsTec0 = new TgaRenIniTsTec0();
    //Original name: TGA-PC-RIP-PRE
    private TgaPcRipPre tgaPcRipPre = new TgaPcRipPre();
    //Original name: TGA-FL-IMPORTI-FORZ
    private char tgaFlImportiForz = DefaultValues.CHAR_VAL;
    //Original name: TGA-PRSTZ-INI-NFORZ
    private TgaPrstzIniNforz tgaPrstzIniNforz = new TgaPrstzIniNforz();
    //Original name: TGA-VIS-END2000-NFORZ
    private TgaVisEnd2000Nforz tgaVisEnd2000Nforz = new TgaVisEnd2000Nforz();
    //Original name: TGA-INTR-MORA
    private TgaIntrMora tgaIntrMora = new TgaIntrMora();
    //Original name: TGA-MANFEE-ANTIC
    private TgaManfeeAntic tgaManfeeAntic = new TgaManfeeAntic();
    //Original name: TGA-MANFEE-RICOR
    private TgaManfeeRicor tgaManfeeRicor = new TgaManfeeRicor();
    //Original name: TGA-PRE-UNI-RIVTO
    private TgaPreUniRivto tgaPreUniRivto = new TgaPreUniRivto();
    //Original name: TGA-PROV-1AA-ACQ
    private TgaProv1aaAcq tgaProv1aaAcq = new TgaProv1aaAcq();
    //Original name: TGA-PROV-2AA-ACQ
    private TgaProv2aaAcq tgaProv2aaAcq = new TgaProv2aaAcq();
    //Original name: TGA-PROV-RICOR
    private TgaProvRicor tgaProvRicor = new TgaProvRicor();
    //Original name: TGA-PROV-INC
    private TgaProvInc tgaProvInc = new TgaProvInc();
    //Original name: TGA-ALQ-PROV-ACQ
    private TgaAlqProvAcq tgaAlqProvAcq = new TgaAlqProvAcq();
    //Original name: TGA-ALQ-PROV-INC
    private TgaAlqProvInc tgaAlqProvInc = new TgaAlqProvInc();
    //Original name: TGA-ALQ-PROV-RICOR
    private TgaAlqProvRicor tgaAlqProvRicor = new TgaAlqProvRicor();
    //Original name: TGA-IMPB-PROV-ACQ
    private TgaImpbProvAcq tgaImpbProvAcq = new TgaImpbProvAcq();
    //Original name: TGA-IMPB-PROV-INC
    private TgaImpbProvInc tgaImpbProvInc = new TgaImpbProvInc();
    //Original name: TGA-IMPB-PROV-RICOR
    private TgaImpbProvRicor tgaImpbProvRicor = new TgaImpbProvRicor();
    //Original name: TGA-FL-PROV-FORZ
    private char tgaFlProvForz = DefaultValues.CHAR_VAL;
    //Original name: TGA-PRSTZ-AGG-INI
    private TgaPrstzAggIni tgaPrstzAggIni = new TgaPrstzAggIni();
    //Original name: TGA-INCR-PRE
    private TgaIncrPre tgaIncrPre = new TgaIncrPre();
    //Original name: TGA-INCR-PRSTZ
    private TgaIncrPrstz tgaIncrPrstz = new TgaIncrPrstz();
    //Original name: TGA-DT-ULT-ADEG-PRE-PR
    private TgaDtUltAdegPrePr tgaDtUltAdegPrePr = new TgaDtUltAdegPrePr();
    //Original name: TGA-PRSTZ-AGG-ULT
    private TgaPrstzAggUlt tgaPrstzAggUlt = new TgaPrstzAggUlt();
    //Original name: TGA-TS-RIVAL-NET
    private TgaTsRivalNet tgaTsRivalNet = new TgaTsRivalNet();
    //Original name: TGA-PRE-PATTUITO
    private TgaPrePattuito tgaPrePattuito = new TgaPrePattuito();
    //Original name: TGA-TP-RIVAL
    private String tgaTpRival = DefaultValues.stringVal(Len.TGA_TP_RIVAL);
    //Original name: TGA-RIS-MAT
    private TgaRisMat tgaRisMat = new TgaRisMat();
    //Original name: TGA-CPT-MIN-SCAD
    private TgaCptMinScad tgaCptMinScad = new TgaCptMinScad();
    //Original name: TGA-COMMIS-GEST
    private TgaCommisGest tgaCommisGest = new TgaCommisGest();
    //Original name: TGA-TP-MANFEE-APPL
    private String tgaTpManfeeAppl = DefaultValues.stringVal(Len.TGA_TP_MANFEE_APPL);
    //Original name: TGA-DS-RIGA
    private long tgaDsRiga = DefaultValues.LONG_VAL;
    //Original name: TGA-DS-OPER-SQL
    private char tgaDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: TGA-DS-VER
    private int tgaDsVer = DefaultValues.INT_VAL;
    //Original name: TGA-DS-TS-INI-CPTZ
    private long tgaDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: TGA-DS-TS-END-CPTZ
    private long tgaDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: TGA-DS-UTENTE
    private String tgaDsUtente = DefaultValues.stringVal(Len.TGA_DS_UTENTE);
    //Original name: TGA-DS-STATO-ELAB
    private char tgaDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: TGA-PC-COMMIS-GEST
    private TgaPcCommisGest tgaPcCommisGest = new TgaPcCommisGest();
    //Original name: TGA-NUM-GG-RIVAL
    private TgaNumGgRival tgaNumGgRival = new TgaNumGgRival();
    //Original name: TGA-IMP-TRASFE
    private TgaImpTrasfe tgaImpTrasfe = new TgaImpTrasfe();
    //Original name: TGA-IMP-TFR-STRC
    private TgaImpTfrStrc tgaImpTfrStrc = new TgaImpTfrStrc();
    //Original name: TGA-ACQ-EXP
    private TgaAcqExp tgaAcqExp = new TgaAcqExp();
    //Original name: TGA-REMUN-ASS
    private TgaRemunAss tgaRemunAss = new TgaRemunAss();
    //Original name: TGA-COMMIS-INTER
    private TgaCommisInter tgaCommisInter = new TgaCommisInter();
    //Original name: TGA-ALQ-REMUN-ASS
    private TgaAlqRemunAss tgaAlqRemunAss = new TgaAlqRemunAss();
    //Original name: TGA-ALQ-COMMIS-INTER
    private TgaAlqCommisInter tgaAlqCommisInter = new TgaAlqCommisInter();
    //Original name: TGA-IMPB-REMUN-ASS
    private TgaImpbRemunAss tgaImpbRemunAss = new TgaImpbRemunAss();
    //Original name: TGA-IMPB-COMMIS-INTER
    private TgaImpbCommisInter tgaImpbCommisInter = new TgaImpbCommisInter();
    //Original name: TGA-COS-RUN-ASSVA
    private TgaCosRunAssva tgaCosRunAssva = new TgaCosRunAssva();
    //Original name: TGA-COS-RUN-ASSVA-IDC
    private TgaCosRunAssvaIdc tgaCosRunAssvaIdc = new TgaCosRunAssvaIdc();

    //==== METHODS ====
    public void setTrchDiGarFormatted(String data) {
        byte[] buffer = new byte[Len.TRCH_DI_GAR];
        MarshalByte.writeString(buffer, 1, data, Len.TRCH_DI_GAR);
        setTrchDiGarBytes(buffer, 1);
    }

    public String getTrchDiGarFormatted() {
        return MarshalByteExt.bufferToStr(getTrchDiGarBytes());
    }

    public byte[] getTrchDiGarBytes() {
        byte[] buffer = new byte[Len.TRCH_DI_GAR];
        return getTrchDiGarBytes(buffer, 1);
    }

    public void setTrchDiGarBytes(byte[] buffer, int offset) {
        int position = offset;
        tgaIdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TGA_ID_TRCH_DI_GAR, 0);
        position += Len.TGA_ID_TRCH_DI_GAR;
        tgaIdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TGA_ID_GAR, 0);
        position += Len.TGA_ID_GAR;
        tgaIdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TGA_ID_ADES, 0);
        position += Len.TGA_ID_ADES;
        tgaIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TGA_ID_POLI, 0);
        position += Len.TGA_ID_POLI;
        tgaIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TGA_ID_MOVI_CRZ, 0);
        position += Len.TGA_ID_MOVI_CRZ;
        tgaIdMoviChiu.setTgaIdMoviChiuFromBuffer(buffer, position);
        position += TgaIdMoviChiu.Len.TGA_ID_MOVI_CHIU;
        tgaDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TGA_DT_INI_EFF, 0);
        position += Len.TGA_DT_INI_EFF;
        tgaDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TGA_DT_END_EFF, 0);
        position += Len.TGA_DT_END_EFF;
        tgaCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TGA_COD_COMP_ANIA, 0);
        position += Len.TGA_COD_COMP_ANIA;
        tgaDtDecor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TGA_DT_DECOR, 0);
        position += Len.TGA_DT_DECOR;
        tgaDtScad.setTgaDtScadFromBuffer(buffer, position);
        position += TgaDtScad.Len.TGA_DT_SCAD;
        tgaIbOgg = MarshalByte.readString(buffer, position, Len.TGA_IB_OGG);
        position += Len.TGA_IB_OGG;
        tgaTpRgmFisc = MarshalByte.readString(buffer, position, Len.TGA_TP_RGM_FISC);
        position += Len.TGA_TP_RGM_FISC;
        tgaDtEmis.setTgaDtEmisFromBuffer(buffer, position);
        position += TgaDtEmis.Len.TGA_DT_EMIS;
        tgaTpTrch = MarshalByte.readString(buffer, position, Len.TGA_TP_TRCH);
        position += Len.TGA_TP_TRCH;
        tgaDurAa.setTgaDurAaFromBuffer(buffer, position);
        position += TgaDurAa.Len.TGA_DUR_AA;
        tgaDurMm.setTgaDurMmFromBuffer(buffer, position);
        position += TgaDurMm.Len.TGA_DUR_MM;
        tgaDurGg.setTgaDurGgFromBuffer(buffer, position);
        position += TgaDurGg.Len.TGA_DUR_GG;
        tgaPreCasoMor.setTgaPreCasoMorFromBuffer(buffer, position);
        position += TgaPreCasoMor.Len.TGA_PRE_CASO_MOR;
        tgaPcIntrRiat.setTgaPcIntrRiatFromBuffer(buffer, position);
        position += TgaPcIntrRiat.Len.TGA_PC_INTR_RIAT;
        tgaImpBnsAntic.setTgaImpBnsAnticFromBuffer(buffer, position);
        position += TgaImpBnsAntic.Len.TGA_IMP_BNS_ANTIC;
        tgaPreIniNet.setTgaPreIniNetFromBuffer(buffer, position);
        position += TgaPreIniNet.Len.TGA_PRE_INI_NET;
        tgaPrePpIni.setTgaPrePpIniFromBuffer(buffer, position);
        position += TgaPrePpIni.Len.TGA_PRE_PP_INI;
        tgaPrePpUlt.setTgaPrePpUltFromBuffer(buffer, position);
        position += TgaPrePpUlt.Len.TGA_PRE_PP_ULT;
        tgaPreTariIni.setTgaPreTariIniFromBuffer(buffer, position);
        position += TgaPreTariIni.Len.TGA_PRE_TARI_INI;
        tgaPreTariUlt.setTgaPreTariUltFromBuffer(buffer, position);
        position += TgaPreTariUlt.Len.TGA_PRE_TARI_ULT;
        tgaPreInvrioIni.setTgaPreInvrioIniFromBuffer(buffer, position);
        position += TgaPreInvrioIni.Len.TGA_PRE_INVRIO_INI;
        tgaPreInvrioUlt.setTgaPreInvrioUltFromBuffer(buffer, position);
        position += TgaPreInvrioUlt.Len.TGA_PRE_INVRIO_ULT;
        tgaPreRivto.setTgaPreRivtoFromBuffer(buffer, position);
        position += TgaPreRivto.Len.TGA_PRE_RIVTO;
        tgaImpSoprProf.setTgaImpSoprProfFromBuffer(buffer, position);
        position += TgaImpSoprProf.Len.TGA_IMP_SOPR_PROF;
        tgaImpSoprSan.setTgaImpSoprSanFromBuffer(buffer, position);
        position += TgaImpSoprSan.Len.TGA_IMP_SOPR_SAN;
        tgaImpSoprSpo.setTgaImpSoprSpoFromBuffer(buffer, position);
        position += TgaImpSoprSpo.Len.TGA_IMP_SOPR_SPO;
        tgaImpSoprTec.setTgaImpSoprTecFromBuffer(buffer, position);
        position += TgaImpSoprTec.Len.TGA_IMP_SOPR_TEC;
        tgaImpAltSopr.setTgaImpAltSoprFromBuffer(buffer, position);
        position += TgaImpAltSopr.Len.TGA_IMP_ALT_SOPR;
        tgaPreStab.setTgaPreStabFromBuffer(buffer, position);
        position += TgaPreStab.Len.TGA_PRE_STAB;
        tgaDtEffStab.setTgaDtEffStabFromBuffer(buffer, position);
        position += TgaDtEffStab.Len.TGA_DT_EFF_STAB;
        tgaTsRivalFis.setTgaTsRivalFisFromBuffer(buffer, position);
        position += TgaTsRivalFis.Len.TGA_TS_RIVAL_FIS;
        tgaTsRivalIndiciz.setTgaTsRivalIndicizFromBuffer(buffer, position);
        position += TgaTsRivalIndiciz.Len.TGA_TS_RIVAL_INDICIZ;
        tgaOldTsTec.setTgaOldTsTecFromBuffer(buffer, position);
        position += TgaOldTsTec.Len.TGA_OLD_TS_TEC;
        tgaRatLrd.setTgaRatLrdFromBuffer(buffer, position);
        position += TgaRatLrd.Len.TGA_RAT_LRD;
        tgaPreLrd.setTgaPreLrdFromBuffer(buffer, position);
        position += TgaPreLrd.Len.TGA_PRE_LRD;
        tgaPrstzIni.setTgaPrstzIniFromBuffer(buffer, position);
        position += TgaPrstzIni.Len.TGA_PRSTZ_INI;
        tgaPrstzUlt.setTgaPrstzUltFromBuffer(buffer, position);
        position += TgaPrstzUlt.Len.TGA_PRSTZ_ULT;
        tgaCptInOpzRivto.setTgaCptInOpzRivtoFromBuffer(buffer, position);
        position += TgaCptInOpzRivto.Len.TGA_CPT_IN_OPZ_RIVTO;
        tgaPrstzIniStab.setTgaPrstzIniStabFromBuffer(buffer, position);
        position += TgaPrstzIniStab.Len.TGA_PRSTZ_INI_STAB;
        tgaCptRshMor.setTgaCptRshMorFromBuffer(buffer, position);
        position += TgaCptRshMor.Len.TGA_CPT_RSH_MOR;
        tgaPrstzRidIni.setTgaPrstzRidIniFromBuffer(buffer, position);
        position += TgaPrstzRidIni.Len.TGA_PRSTZ_RID_INI;
        tgaFlCarCont = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tgaBnsGiaLiqto.setTgaBnsGiaLiqtoFromBuffer(buffer, position);
        position += TgaBnsGiaLiqto.Len.TGA_BNS_GIA_LIQTO;
        tgaImpBns.setTgaImpBnsFromBuffer(buffer, position);
        position += TgaImpBns.Len.TGA_IMP_BNS;
        tgaCodDvs = MarshalByte.readString(buffer, position, Len.TGA_COD_DVS);
        position += Len.TGA_COD_DVS;
        tgaPrstzIniNewfis.setTgaPrstzIniNewfisFromBuffer(buffer, position);
        position += TgaPrstzIniNewfis.Len.TGA_PRSTZ_INI_NEWFIS;
        tgaImpScon.setTgaImpSconFromBuffer(buffer, position);
        position += TgaImpScon.Len.TGA_IMP_SCON;
        tgaAlqScon.setTgaAlqSconFromBuffer(buffer, position);
        position += TgaAlqScon.Len.TGA_ALQ_SCON;
        tgaImpCarAcq.setTgaImpCarAcqFromBuffer(buffer, position);
        position += TgaImpCarAcq.Len.TGA_IMP_CAR_ACQ;
        tgaImpCarInc.setTgaImpCarIncFromBuffer(buffer, position);
        position += TgaImpCarInc.Len.TGA_IMP_CAR_INC;
        tgaImpCarGest.setTgaImpCarGestFromBuffer(buffer, position);
        position += TgaImpCarGest.Len.TGA_IMP_CAR_GEST;
        tgaEtaAa1oAssto.setTgaEtaAa1oAsstoFromBuffer(buffer, position);
        position += TgaEtaAa1oAssto.Len.TGA_ETA_AA1O_ASSTO;
        tgaEtaMm1oAssto.setTgaEtaMm1oAsstoFromBuffer(buffer, position);
        position += TgaEtaMm1oAssto.Len.TGA_ETA_MM1O_ASSTO;
        tgaEtaAa2oAssto.setTgaEtaAa2oAsstoFromBuffer(buffer, position);
        position += TgaEtaAa2oAssto.Len.TGA_ETA_AA2O_ASSTO;
        tgaEtaMm2oAssto.setTgaEtaMm2oAsstoFromBuffer(buffer, position);
        position += TgaEtaMm2oAssto.Len.TGA_ETA_MM2O_ASSTO;
        tgaEtaAa3oAssto.setTgaEtaAa3oAsstoFromBuffer(buffer, position);
        position += TgaEtaAa3oAssto.Len.TGA_ETA_AA3O_ASSTO;
        tgaEtaMm3oAssto.setTgaEtaMm3oAsstoFromBuffer(buffer, position);
        position += TgaEtaMm3oAssto.Len.TGA_ETA_MM3O_ASSTO;
        tgaRendtoLrd.setTgaRendtoLrdFromBuffer(buffer, position);
        position += TgaRendtoLrd.Len.TGA_RENDTO_LRD;
        tgaPcRetr.setTgaPcRetrFromBuffer(buffer, position);
        position += TgaPcRetr.Len.TGA_PC_RETR;
        tgaRendtoRetr.setTgaRendtoRetrFromBuffer(buffer, position);
        position += TgaRendtoRetr.Len.TGA_RENDTO_RETR;
        tgaMinGarto.setTgaMinGartoFromBuffer(buffer, position);
        position += TgaMinGarto.Len.TGA_MIN_GARTO;
        tgaMinTrnut.setTgaMinTrnutFromBuffer(buffer, position);
        position += TgaMinTrnut.Len.TGA_MIN_TRNUT;
        tgaPreAttDiTrch.setTgaPreAttDiTrchFromBuffer(buffer, position);
        position += TgaPreAttDiTrch.Len.TGA_PRE_ATT_DI_TRCH;
        tgaMatuEnd2000.setTgaMatuEnd2000FromBuffer(buffer, position);
        position += TgaMatuEnd2000.Len.TGA_MATU_END2000;
        tgaAbbTotIni.setTgaAbbTotIniFromBuffer(buffer, position);
        position += TgaAbbTotIni.Len.TGA_ABB_TOT_INI;
        tgaAbbTotUlt.setTgaAbbTotUltFromBuffer(buffer, position);
        position += TgaAbbTotUlt.Len.TGA_ABB_TOT_ULT;
        tgaAbbAnnuUlt.setTgaAbbAnnuUltFromBuffer(buffer, position);
        position += TgaAbbAnnuUlt.Len.TGA_ABB_ANNU_ULT;
        tgaDurAbb.setTgaDurAbbFromBuffer(buffer, position);
        position += TgaDurAbb.Len.TGA_DUR_ABB;
        tgaTpAdegAbb = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tgaModCalc = MarshalByte.readString(buffer, position, Len.TGA_MOD_CALC);
        position += Len.TGA_MOD_CALC;
        tgaImpAz.setTgaImpAzFromBuffer(buffer, position);
        position += TgaImpAz.Len.TGA_IMP_AZ;
        tgaImpAder.setTgaImpAderFromBuffer(buffer, position);
        position += TgaImpAder.Len.TGA_IMP_ADER;
        tgaImpTfr.setTgaImpTfrFromBuffer(buffer, position);
        position += TgaImpTfr.Len.TGA_IMP_TFR;
        tgaImpVolo.setTgaImpVoloFromBuffer(buffer, position);
        position += TgaImpVolo.Len.TGA_IMP_VOLO;
        tgaVisEnd2000.setTgaVisEnd2000FromBuffer(buffer, position);
        position += TgaVisEnd2000.Len.TGA_VIS_END2000;
        tgaDtVldtProd.setTgaDtVldtProdFromBuffer(buffer, position);
        position += TgaDtVldtProd.Len.TGA_DT_VLDT_PROD;
        tgaDtIniValTar.setTgaDtIniValTarFromBuffer(buffer, position);
        position += TgaDtIniValTar.Len.TGA_DT_INI_VAL_TAR;
        tgaImpbVisEnd2000.setTgaImpbVisEnd2000FromBuffer(buffer, position);
        position += TgaImpbVisEnd2000.Len.TGA_IMPB_VIS_END2000;
        tgaRenIniTsTec0.setTgaRenIniTsTec0FromBuffer(buffer, position);
        position += TgaRenIniTsTec0.Len.TGA_REN_INI_TS_TEC0;
        tgaPcRipPre.setTgaPcRipPreFromBuffer(buffer, position);
        position += TgaPcRipPre.Len.TGA_PC_RIP_PRE;
        tgaFlImportiForz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tgaPrstzIniNforz.setTgaPrstzIniNforzFromBuffer(buffer, position);
        position += TgaPrstzIniNforz.Len.TGA_PRSTZ_INI_NFORZ;
        tgaVisEnd2000Nforz.setTgaVisEnd2000NforzFromBuffer(buffer, position);
        position += TgaVisEnd2000Nforz.Len.TGA_VIS_END2000_NFORZ;
        tgaIntrMora.setTgaIntrMoraFromBuffer(buffer, position);
        position += TgaIntrMora.Len.TGA_INTR_MORA;
        tgaManfeeAntic.setTgaManfeeAnticFromBuffer(buffer, position);
        position += TgaManfeeAntic.Len.TGA_MANFEE_ANTIC;
        tgaManfeeRicor.setTgaManfeeRicorFromBuffer(buffer, position);
        position += TgaManfeeRicor.Len.TGA_MANFEE_RICOR;
        tgaPreUniRivto.setTgaPreUniRivtoFromBuffer(buffer, position);
        position += TgaPreUniRivto.Len.TGA_PRE_UNI_RIVTO;
        tgaProv1aaAcq.setTgaProv1aaAcqFromBuffer(buffer, position);
        position += TgaProv1aaAcq.Len.TGA_PROV1AA_ACQ;
        tgaProv2aaAcq.setTgaProv2aaAcqFromBuffer(buffer, position);
        position += TgaProv2aaAcq.Len.TGA_PROV2AA_ACQ;
        tgaProvRicor.setTgaProvRicorFromBuffer(buffer, position);
        position += TgaProvRicor.Len.TGA_PROV_RICOR;
        tgaProvInc.setTgaProvIncFromBuffer(buffer, position);
        position += TgaProvInc.Len.TGA_PROV_INC;
        tgaAlqProvAcq.setTgaAlqProvAcqFromBuffer(buffer, position);
        position += TgaAlqProvAcq.Len.TGA_ALQ_PROV_ACQ;
        tgaAlqProvInc.setTgaAlqProvIncFromBuffer(buffer, position);
        position += TgaAlqProvInc.Len.TGA_ALQ_PROV_INC;
        tgaAlqProvRicor.setTgaAlqProvRicorFromBuffer(buffer, position);
        position += TgaAlqProvRicor.Len.TGA_ALQ_PROV_RICOR;
        tgaImpbProvAcq.setTgaImpbProvAcqFromBuffer(buffer, position);
        position += TgaImpbProvAcq.Len.TGA_IMPB_PROV_ACQ;
        tgaImpbProvInc.setTgaImpbProvIncFromBuffer(buffer, position);
        position += TgaImpbProvInc.Len.TGA_IMPB_PROV_INC;
        tgaImpbProvRicor.setTgaImpbProvRicorFromBuffer(buffer, position);
        position += TgaImpbProvRicor.Len.TGA_IMPB_PROV_RICOR;
        tgaFlProvForz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tgaPrstzAggIni.setTgaPrstzAggIniFromBuffer(buffer, position);
        position += TgaPrstzAggIni.Len.TGA_PRSTZ_AGG_INI;
        tgaIncrPre.setTgaIncrPreFromBuffer(buffer, position);
        position += TgaIncrPre.Len.TGA_INCR_PRE;
        tgaIncrPrstz.setTgaIncrPrstzFromBuffer(buffer, position);
        position += TgaIncrPrstz.Len.TGA_INCR_PRSTZ;
        tgaDtUltAdegPrePr.setTgaDtUltAdegPrePrFromBuffer(buffer, position);
        position += TgaDtUltAdegPrePr.Len.TGA_DT_ULT_ADEG_PRE_PR;
        tgaPrstzAggUlt.setTgaPrstzAggUltFromBuffer(buffer, position);
        position += TgaPrstzAggUlt.Len.TGA_PRSTZ_AGG_ULT;
        tgaTsRivalNet.setTgaTsRivalNetFromBuffer(buffer, position);
        position += TgaTsRivalNet.Len.TGA_TS_RIVAL_NET;
        tgaPrePattuito.setTgaPrePattuitoFromBuffer(buffer, position);
        position += TgaPrePattuito.Len.TGA_PRE_PATTUITO;
        tgaTpRival = MarshalByte.readString(buffer, position, Len.TGA_TP_RIVAL);
        position += Len.TGA_TP_RIVAL;
        tgaRisMat.setTgaRisMatFromBuffer(buffer, position);
        position += TgaRisMat.Len.TGA_RIS_MAT;
        tgaCptMinScad.setTgaCptMinScadFromBuffer(buffer, position);
        position += TgaCptMinScad.Len.TGA_CPT_MIN_SCAD;
        tgaCommisGest.setTgaCommisGestFromBuffer(buffer, position);
        position += TgaCommisGest.Len.TGA_COMMIS_GEST;
        tgaTpManfeeAppl = MarshalByte.readString(buffer, position, Len.TGA_TP_MANFEE_APPL);
        position += Len.TGA_TP_MANFEE_APPL;
        tgaDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TGA_DS_RIGA, 0);
        position += Len.TGA_DS_RIGA;
        tgaDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tgaDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TGA_DS_VER, 0);
        position += Len.TGA_DS_VER;
        tgaDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TGA_DS_TS_INI_CPTZ, 0);
        position += Len.TGA_DS_TS_INI_CPTZ;
        tgaDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TGA_DS_TS_END_CPTZ, 0);
        position += Len.TGA_DS_TS_END_CPTZ;
        tgaDsUtente = MarshalByte.readString(buffer, position, Len.TGA_DS_UTENTE);
        position += Len.TGA_DS_UTENTE;
        tgaDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tgaPcCommisGest.setTgaPcCommisGestFromBuffer(buffer, position);
        position += TgaPcCommisGest.Len.TGA_PC_COMMIS_GEST;
        tgaNumGgRival.setTgaNumGgRivalFromBuffer(buffer, position);
        position += TgaNumGgRival.Len.TGA_NUM_GG_RIVAL;
        tgaImpTrasfe.setTgaImpTrasfeFromBuffer(buffer, position);
        position += TgaImpTrasfe.Len.TGA_IMP_TRASFE;
        tgaImpTfrStrc.setTgaImpTfrStrcFromBuffer(buffer, position);
        position += TgaImpTfrStrc.Len.TGA_IMP_TFR_STRC;
        tgaAcqExp.setTgaAcqExpFromBuffer(buffer, position);
        position += TgaAcqExp.Len.TGA_ACQ_EXP;
        tgaRemunAss.setTgaRemunAssFromBuffer(buffer, position);
        position += TgaRemunAss.Len.TGA_REMUN_ASS;
        tgaCommisInter.setTgaCommisInterFromBuffer(buffer, position);
        position += TgaCommisInter.Len.TGA_COMMIS_INTER;
        tgaAlqRemunAss.setTgaAlqRemunAssFromBuffer(buffer, position);
        position += TgaAlqRemunAss.Len.TGA_ALQ_REMUN_ASS;
        tgaAlqCommisInter.setTgaAlqCommisInterFromBuffer(buffer, position);
        position += TgaAlqCommisInter.Len.TGA_ALQ_COMMIS_INTER;
        tgaImpbRemunAss.setTgaImpbRemunAssFromBuffer(buffer, position);
        position += TgaImpbRemunAss.Len.TGA_IMPB_REMUN_ASS;
        tgaImpbCommisInter.setTgaImpbCommisInterFromBuffer(buffer, position);
        position += TgaImpbCommisInter.Len.TGA_IMPB_COMMIS_INTER;
        tgaCosRunAssva.setTgaCosRunAssvaFromBuffer(buffer, position);
        position += TgaCosRunAssva.Len.TGA_COS_RUN_ASSVA;
        tgaCosRunAssvaIdc.setTgaCosRunAssvaIdcFromBuffer(buffer, position);
    }

    public byte[] getTrchDiGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, tgaIdTrchDiGar, Len.Int.TGA_ID_TRCH_DI_GAR, 0);
        position += Len.TGA_ID_TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, tgaIdGar, Len.Int.TGA_ID_GAR, 0);
        position += Len.TGA_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, tgaIdAdes, Len.Int.TGA_ID_ADES, 0);
        position += Len.TGA_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, tgaIdPoli, Len.Int.TGA_ID_POLI, 0);
        position += Len.TGA_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, tgaIdMoviCrz, Len.Int.TGA_ID_MOVI_CRZ, 0);
        position += Len.TGA_ID_MOVI_CRZ;
        tgaIdMoviChiu.getTgaIdMoviChiuAsBuffer(buffer, position);
        position += TgaIdMoviChiu.Len.TGA_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, tgaDtIniEff, Len.Int.TGA_DT_INI_EFF, 0);
        position += Len.TGA_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, tgaDtEndEff, Len.Int.TGA_DT_END_EFF, 0);
        position += Len.TGA_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, tgaCodCompAnia, Len.Int.TGA_COD_COMP_ANIA, 0);
        position += Len.TGA_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, tgaDtDecor, Len.Int.TGA_DT_DECOR, 0);
        position += Len.TGA_DT_DECOR;
        tgaDtScad.getTgaDtScadAsBuffer(buffer, position);
        position += TgaDtScad.Len.TGA_DT_SCAD;
        MarshalByte.writeString(buffer, position, tgaIbOgg, Len.TGA_IB_OGG);
        position += Len.TGA_IB_OGG;
        MarshalByte.writeString(buffer, position, tgaTpRgmFisc, Len.TGA_TP_RGM_FISC);
        position += Len.TGA_TP_RGM_FISC;
        tgaDtEmis.getTgaDtEmisAsBuffer(buffer, position);
        position += TgaDtEmis.Len.TGA_DT_EMIS;
        MarshalByte.writeString(buffer, position, tgaTpTrch, Len.TGA_TP_TRCH);
        position += Len.TGA_TP_TRCH;
        tgaDurAa.getTgaDurAaAsBuffer(buffer, position);
        position += TgaDurAa.Len.TGA_DUR_AA;
        tgaDurMm.getTgaDurMmAsBuffer(buffer, position);
        position += TgaDurMm.Len.TGA_DUR_MM;
        tgaDurGg.getTgaDurGgAsBuffer(buffer, position);
        position += TgaDurGg.Len.TGA_DUR_GG;
        tgaPreCasoMor.getTgaPreCasoMorAsBuffer(buffer, position);
        position += TgaPreCasoMor.Len.TGA_PRE_CASO_MOR;
        tgaPcIntrRiat.getTgaPcIntrRiatAsBuffer(buffer, position);
        position += TgaPcIntrRiat.Len.TGA_PC_INTR_RIAT;
        tgaImpBnsAntic.getTgaImpBnsAnticAsBuffer(buffer, position);
        position += TgaImpBnsAntic.Len.TGA_IMP_BNS_ANTIC;
        tgaPreIniNet.getTgaPreIniNetAsBuffer(buffer, position);
        position += TgaPreIniNet.Len.TGA_PRE_INI_NET;
        tgaPrePpIni.getTgaPrePpIniAsBuffer(buffer, position);
        position += TgaPrePpIni.Len.TGA_PRE_PP_INI;
        tgaPrePpUlt.getTgaPrePpUltAsBuffer(buffer, position);
        position += TgaPrePpUlt.Len.TGA_PRE_PP_ULT;
        tgaPreTariIni.getTgaPreTariIniAsBuffer(buffer, position);
        position += TgaPreTariIni.Len.TGA_PRE_TARI_INI;
        tgaPreTariUlt.getTgaPreTariUltAsBuffer(buffer, position);
        position += TgaPreTariUlt.Len.TGA_PRE_TARI_ULT;
        tgaPreInvrioIni.getTgaPreInvrioIniAsBuffer(buffer, position);
        position += TgaPreInvrioIni.Len.TGA_PRE_INVRIO_INI;
        tgaPreInvrioUlt.getTgaPreInvrioUltAsBuffer(buffer, position);
        position += TgaPreInvrioUlt.Len.TGA_PRE_INVRIO_ULT;
        tgaPreRivto.getTgaPreRivtoAsBuffer(buffer, position);
        position += TgaPreRivto.Len.TGA_PRE_RIVTO;
        tgaImpSoprProf.getTgaImpSoprProfAsBuffer(buffer, position);
        position += TgaImpSoprProf.Len.TGA_IMP_SOPR_PROF;
        tgaImpSoprSan.getTgaImpSoprSanAsBuffer(buffer, position);
        position += TgaImpSoprSan.Len.TGA_IMP_SOPR_SAN;
        tgaImpSoprSpo.getTgaImpSoprSpoAsBuffer(buffer, position);
        position += TgaImpSoprSpo.Len.TGA_IMP_SOPR_SPO;
        tgaImpSoprTec.getTgaImpSoprTecAsBuffer(buffer, position);
        position += TgaImpSoprTec.Len.TGA_IMP_SOPR_TEC;
        tgaImpAltSopr.getTgaImpAltSoprAsBuffer(buffer, position);
        position += TgaImpAltSopr.Len.TGA_IMP_ALT_SOPR;
        tgaPreStab.getTgaPreStabAsBuffer(buffer, position);
        position += TgaPreStab.Len.TGA_PRE_STAB;
        tgaDtEffStab.getTgaDtEffStabAsBuffer(buffer, position);
        position += TgaDtEffStab.Len.TGA_DT_EFF_STAB;
        tgaTsRivalFis.getTgaTsRivalFisAsBuffer(buffer, position);
        position += TgaTsRivalFis.Len.TGA_TS_RIVAL_FIS;
        tgaTsRivalIndiciz.getTgaTsRivalIndicizAsBuffer(buffer, position);
        position += TgaTsRivalIndiciz.Len.TGA_TS_RIVAL_INDICIZ;
        tgaOldTsTec.getTgaOldTsTecAsBuffer(buffer, position);
        position += TgaOldTsTec.Len.TGA_OLD_TS_TEC;
        tgaRatLrd.getTgaRatLrdAsBuffer(buffer, position);
        position += TgaRatLrd.Len.TGA_RAT_LRD;
        tgaPreLrd.getTgaPreLrdAsBuffer(buffer, position);
        position += TgaPreLrd.Len.TGA_PRE_LRD;
        tgaPrstzIni.getTgaPrstzIniAsBuffer(buffer, position);
        position += TgaPrstzIni.Len.TGA_PRSTZ_INI;
        tgaPrstzUlt.getTgaPrstzUltAsBuffer(buffer, position);
        position += TgaPrstzUlt.Len.TGA_PRSTZ_ULT;
        tgaCptInOpzRivto.getTgaCptInOpzRivtoAsBuffer(buffer, position);
        position += TgaCptInOpzRivto.Len.TGA_CPT_IN_OPZ_RIVTO;
        tgaPrstzIniStab.getTgaPrstzIniStabAsBuffer(buffer, position);
        position += TgaPrstzIniStab.Len.TGA_PRSTZ_INI_STAB;
        tgaCptRshMor.getTgaCptRshMorAsBuffer(buffer, position);
        position += TgaCptRshMor.Len.TGA_CPT_RSH_MOR;
        tgaPrstzRidIni.getTgaPrstzRidIniAsBuffer(buffer, position);
        position += TgaPrstzRidIni.Len.TGA_PRSTZ_RID_INI;
        MarshalByte.writeChar(buffer, position, tgaFlCarCont);
        position += Types.CHAR_SIZE;
        tgaBnsGiaLiqto.getTgaBnsGiaLiqtoAsBuffer(buffer, position);
        position += TgaBnsGiaLiqto.Len.TGA_BNS_GIA_LIQTO;
        tgaImpBns.getTgaImpBnsAsBuffer(buffer, position);
        position += TgaImpBns.Len.TGA_IMP_BNS;
        MarshalByte.writeString(buffer, position, tgaCodDvs, Len.TGA_COD_DVS);
        position += Len.TGA_COD_DVS;
        tgaPrstzIniNewfis.getTgaPrstzIniNewfisAsBuffer(buffer, position);
        position += TgaPrstzIniNewfis.Len.TGA_PRSTZ_INI_NEWFIS;
        tgaImpScon.getTgaImpSconAsBuffer(buffer, position);
        position += TgaImpScon.Len.TGA_IMP_SCON;
        tgaAlqScon.getTgaAlqSconAsBuffer(buffer, position);
        position += TgaAlqScon.Len.TGA_ALQ_SCON;
        tgaImpCarAcq.getTgaImpCarAcqAsBuffer(buffer, position);
        position += TgaImpCarAcq.Len.TGA_IMP_CAR_ACQ;
        tgaImpCarInc.getTgaImpCarIncAsBuffer(buffer, position);
        position += TgaImpCarInc.Len.TGA_IMP_CAR_INC;
        tgaImpCarGest.getTgaImpCarGestAsBuffer(buffer, position);
        position += TgaImpCarGest.Len.TGA_IMP_CAR_GEST;
        tgaEtaAa1oAssto.getTgaEtaAa1oAsstoAsBuffer(buffer, position);
        position += TgaEtaAa1oAssto.Len.TGA_ETA_AA1O_ASSTO;
        tgaEtaMm1oAssto.getTgaEtaMm1oAsstoAsBuffer(buffer, position);
        position += TgaEtaMm1oAssto.Len.TGA_ETA_MM1O_ASSTO;
        tgaEtaAa2oAssto.getTgaEtaAa2oAsstoAsBuffer(buffer, position);
        position += TgaEtaAa2oAssto.Len.TGA_ETA_AA2O_ASSTO;
        tgaEtaMm2oAssto.getTgaEtaMm2oAsstoAsBuffer(buffer, position);
        position += TgaEtaMm2oAssto.Len.TGA_ETA_MM2O_ASSTO;
        tgaEtaAa3oAssto.getTgaEtaAa3oAsstoAsBuffer(buffer, position);
        position += TgaEtaAa3oAssto.Len.TGA_ETA_AA3O_ASSTO;
        tgaEtaMm3oAssto.getTgaEtaMm3oAsstoAsBuffer(buffer, position);
        position += TgaEtaMm3oAssto.Len.TGA_ETA_MM3O_ASSTO;
        tgaRendtoLrd.getTgaRendtoLrdAsBuffer(buffer, position);
        position += TgaRendtoLrd.Len.TGA_RENDTO_LRD;
        tgaPcRetr.getTgaPcRetrAsBuffer(buffer, position);
        position += TgaPcRetr.Len.TGA_PC_RETR;
        tgaRendtoRetr.getTgaRendtoRetrAsBuffer(buffer, position);
        position += TgaRendtoRetr.Len.TGA_RENDTO_RETR;
        tgaMinGarto.getTgaMinGartoAsBuffer(buffer, position);
        position += TgaMinGarto.Len.TGA_MIN_GARTO;
        tgaMinTrnut.getTgaMinTrnutAsBuffer(buffer, position);
        position += TgaMinTrnut.Len.TGA_MIN_TRNUT;
        tgaPreAttDiTrch.getTgaPreAttDiTrchAsBuffer(buffer, position);
        position += TgaPreAttDiTrch.Len.TGA_PRE_ATT_DI_TRCH;
        tgaMatuEnd2000.getTgaMatuEnd2000AsBuffer(buffer, position);
        position += TgaMatuEnd2000.Len.TGA_MATU_END2000;
        tgaAbbTotIni.getTgaAbbTotIniAsBuffer(buffer, position);
        position += TgaAbbTotIni.Len.TGA_ABB_TOT_INI;
        tgaAbbTotUlt.getTgaAbbTotUltAsBuffer(buffer, position);
        position += TgaAbbTotUlt.Len.TGA_ABB_TOT_ULT;
        tgaAbbAnnuUlt.getTgaAbbAnnuUltAsBuffer(buffer, position);
        position += TgaAbbAnnuUlt.Len.TGA_ABB_ANNU_ULT;
        tgaDurAbb.getTgaDurAbbAsBuffer(buffer, position);
        position += TgaDurAbb.Len.TGA_DUR_ABB;
        MarshalByte.writeChar(buffer, position, tgaTpAdegAbb);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tgaModCalc, Len.TGA_MOD_CALC);
        position += Len.TGA_MOD_CALC;
        tgaImpAz.getTgaImpAzAsBuffer(buffer, position);
        position += TgaImpAz.Len.TGA_IMP_AZ;
        tgaImpAder.getTgaImpAderAsBuffer(buffer, position);
        position += TgaImpAder.Len.TGA_IMP_ADER;
        tgaImpTfr.getTgaImpTfrAsBuffer(buffer, position);
        position += TgaImpTfr.Len.TGA_IMP_TFR;
        tgaImpVolo.getTgaImpVoloAsBuffer(buffer, position);
        position += TgaImpVolo.Len.TGA_IMP_VOLO;
        tgaVisEnd2000.getTgaVisEnd2000AsBuffer(buffer, position);
        position += TgaVisEnd2000.Len.TGA_VIS_END2000;
        tgaDtVldtProd.getTgaDtVldtProdAsBuffer(buffer, position);
        position += TgaDtVldtProd.Len.TGA_DT_VLDT_PROD;
        tgaDtIniValTar.getTgaDtIniValTarAsBuffer(buffer, position);
        position += TgaDtIniValTar.Len.TGA_DT_INI_VAL_TAR;
        tgaImpbVisEnd2000.getTgaImpbVisEnd2000AsBuffer(buffer, position);
        position += TgaImpbVisEnd2000.Len.TGA_IMPB_VIS_END2000;
        tgaRenIniTsTec0.getTgaRenIniTsTec0AsBuffer(buffer, position);
        position += TgaRenIniTsTec0.Len.TGA_REN_INI_TS_TEC0;
        tgaPcRipPre.getTgaPcRipPreAsBuffer(buffer, position);
        position += TgaPcRipPre.Len.TGA_PC_RIP_PRE;
        MarshalByte.writeChar(buffer, position, tgaFlImportiForz);
        position += Types.CHAR_SIZE;
        tgaPrstzIniNforz.getTgaPrstzIniNforzAsBuffer(buffer, position);
        position += TgaPrstzIniNforz.Len.TGA_PRSTZ_INI_NFORZ;
        tgaVisEnd2000Nforz.getTgaVisEnd2000NforzAsBuffer(buffer, position);
        position += TgaVisEnd2000Nforz.Len.TGA_VIS_END2000_NFORZ;
        tgaIntrMora.getTgaIntrMoraAsBuffer(buffer, position);
        position += TgaIntrMora.Len.TGA_INTR_MORA;
        tgaManfeeAntic.getTgaManfeeAnticAsBuffer(buffer, position);
        position += TgaManfeeAntic.Len.TGA_MANFEE_ANTIC;
        tgaManfeeRicor.getTgaManfeeRicorAsBuffer(buffer, position);
        position += TgaManfeeRicor.Len.TGA_MANFEE_RICOR;
        tgaPreUniRivto.getTgaPreUniRivtoAsBuffer(buffer, position);
        position += TgaPreUniRivto.Len.TGA_PRE_UNI_RIVTO;
        tgaProv1aaAcq.getTgaProv1aaAcqAsBuffer(buffer, position);
        position += TgaProv1aaAcq.Len.TGA_PROV1AA_ACQ;
        tgaProv2aaAcq.getTgaProv2aaAcqAsBuffer(buffer, position);
        position += TgaProv2aaAcq.Len.TGA_PROV2AA_ACQ;
        tgaProvRicor.getTgaProvRicorAsBuffer(buffer, position);
        position += TgaProvRicor.Len.TGA_PROV_RICOR;
        tgaProvInc.getTgaProvIncAsBuffer(buffer, position);
        position += TgaProvInc.Len.TGA_PROV_INC;
        tgaAlqProvAcq.getTgaAlqProvAcqAsBuffer(buffer, position);
        position += TgaAlqProvAcq.Len.TGA_ALQ_PROV_ACQ;
        tgaAlqProvInc.getTgaAlqProvIncAsBuffer(buffer, position);
        position += TgaAlqProvInc.Len.TGA_ALQ_PROV_INC;
        tgaAlqProvRicor.getTgaAlqProvRicorAsBuffer(buffer, position);
        position += TgaAlqProvRicor.Len.TGA_ALQ_PROV_RICOR;
        tgaImpbProvAcq.getTgaImpbProvAcqAsBuffer(buffer, position);
        position += TgaImpbProvAcq.Len.TGA_IMPB_PROV_ACQ;
        tgaImpbProvInc.getTgaImpbProvIncAsBuffer(buffer, position);
        position += TgaImpbProvInc.Len.TGA_IMPB_PROV_INC;
        tgaImpbProvRicor.getTgaImpbProvRicorAsBuffer(buffer, position);
        position += TgaImpbProvRicor.Len.TGA_IMPB_PROV_RICOR;
        MarshalByte.writeChar(buffer, position, tgaFlProvForz);
        position += Types.CHAR_SIZE;
        tgaPrstzAggIni.getTgaPrstzAggIniAsBuffer(buffer, position);
        position += TgaPrstzAggIni.Len.TGA_PRSTZ_AGG_INI;
        tgaIncrPre.getTgaIncrPreAsBuffer(buffer, position);
        position += TgaIncrPre.Len.TGA_INCR_PRE;
        tgaIncrPrstz.getTgaIncrPrstzAsBuffer(buffer, position);
        position += TgaIncrPrstz.Len.TGA_INCR_PRSTZ;
        tgaDtUltAdegPrePr.getTgaDtUltAdegPrePrAsBuffer(buffer, position);
        position += TgaDtUltAdegPrePr.Len.TGA_DT_ULT_ADEG_PRE_PR;
        tgaPrstzAggUlt.getTgaPrstzAggUltAsBuffer(buffer, position);
        position += TgaPrstzAggUlt.Len.TGA_PRSTZ_AGG_ULT;
        tgaTsRivalNet.getTgaTsRivalNetAsBuffer(buffer, position);
        position += TgaTsRivalNet.Len.TGA_TS_RIVAL_NET;
        tgaPrePattuito.getTgaPrePattuitoAsBuffer(buffer, position);
        position += TgaPrePattuito.Len.TGA_PRE_PATTUITO;
        MarshalByte.writeString(buffer, position, tgaTpRival, Len.TGA_TP_RIVAL);
        position += Len.TGA_TP_RIVAL;
        tgaRisMat.getTgaRisMatAsBuffer(buffer, position);
        position += TgaRisMat.Len.TGA_RIS_MAT;
        tgaCptMinScad.getTgaCptMinScadAsBuffer(buffer, position);
        position += TgaCptMinScad.Len.TGA_CPT_MIN_SCAD;
        tgaCommisGest.getTgaCommisGestAsBuffer(buffer, position);
        position += TgaCommisGest.Len.TGA_COMMIS_GEST;
        MarshalByte.writeString(buffer, position, tgaTpManfeeAppl, Len.TGA_TP_MANFEE_APPL);
        position += Len.TGA_TP_MANFEE_APPL;
        MarshalByte.writeLongAsPacked(buffer, position, tgaDsRiga, Len.Int.TGA_DS_RIGA, 0);
        position += Len.TGA_DS_RIGA;
        MarshalByte.writeChar(buffer, position, tgaDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, tgaDsVer, Len.Int.TGA_DS_VER, 0);
        position += Len.TGA_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, tgaDsTsIniCptz, Len.Int.TGA_DS_TS_INI_CPTZ, 0);
        position += Len.TGA_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, tgaDsTsEndCptz, Len.Int.TGA_DS_TS_END_CPTZ, 0);
        position += Len.TGA_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, tgaDsUtente, Len.TGA_DS_UTENTE);
        position += Len.TGA_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, tgaDsStatoElab);
        position += Types.CHAR_SIZE;
        tgaPcCommisGest.getTgaPcCommisGestAsBuffer(buffer, position);
        position += TgaPcCommisGest.Len.TGA_PC_COMMIS_GEST;
        tgaNumGgRival.getTgaNumGgRivalAsBuffer(buffer, position);
        position += TgaNumGgRival.Len.TGA_NUM_GG_RIVAL;
        tgaImpTrasfe.getTgaImpTrasfeAsBuffer(buffer, position);
        position += TgaImpTrasfe.Len.TGA_IMP_TRASFE;
        tgaImpTfrStrc.getTgaImpTfrStrcAsBuffer(buffer, position);
        position += TgaImpTfrStrc.Len.TGA_IMP_TFR_STRC;
        tgaAcqExp.getTgaAcqExpAsBuffer(buffer, position);
        position += TgaAcqExp.Len.TGA_ACQ_EXP;
        tgaRemunAss.getTgaRemunAssAsBuffer(buffer, position);
        position += TgaRemunAss.Len.TGA_REMUN_ASS;
        tgaCommisInter.getTgaCommisInterAsBuffer(buffer, position);
        position += TgaCommisInter.Len.TGA_COMMIS_INTER;
        tgaAlqRemunAss.getTgaAlqRemunAssAsBuffer(buffer, position);
        position += TgaAlqRemunAss.Len.TGA_ALQ_REMUN_ASS;
        tgaAlqCommisInter.getTgaAlqCommisInterAsBuffer(buffer, position);
        position += TgaAlqCommisInter.Len.TGA_ALQ_COMMIS_INTER;
        tgaImpbRemunAss.getTgaImpbRemunAssAsBuffer(buffer, position);
        position += TgaImpbRemunAss.Len.TGA_IMPB_REMUN_ASS;
        tgaImpbCommisInter.getTgaImpbCommisInterAsBuffer(buffer, position);
        position += TgaImpbCommisInter.Len.TGA_IMPB_COMMIS_INTER;
        tgaCosRunAssva.getTgaCosRunAssvaAsBuffer(buffer, position);
        position += TgaCosRunAssva.Len.TGA_COS_RUN_ASSVA;
        tgaCosRunAssvaIdc.getTgaCosRunAssvaIdcAsBuffer(buffer, position);
        return buffer;
    }

    public void setTgaIdTrchDiGar(int tgaIdTrchDiGar) {
        this.tgaIdTrchDiGar = tgaIdTrchDiGar;
    }

    public int getTgaIdTrchDiGar() {
        return this.tgaIdTrchDiGar;
    }

    public void setTgaIdGar(int tgaIdGar) {
        this.tgaIdGar = tgaIdGar;
    }

    public int getTgaIdGar() {
        return this.tgaIdGar;
    }

    public void setTgaIdAdes(int tgaIdAdes) {
        this.tgaIdAdes = tgaIdAdes;
    }

    public int getTgaIdAdes() {
        return this.tgaIdAdes;
    }

    public void setTgaIdPoli(int tgaIdPoli) {
        this.tgaIdPoli = tgaIdPoli;
    }

    public int getTgaIdPoli() {
        return this.tgaIdPoli;
    }

    public void setTgaIdMoviCrz(int tgaIdMoviCrz) {
        this.tgaIdMoviCrz = tgaIdMoviCrz;
    }

    public int getTgaIdMoviCrz() {
        return this.tgaIdMoviCrz;
    }

    public void setTgaDtIniEff(int tgaDtIniEff) {
        this.tgaDtIniEff = tgaDtIniEff;
    }

    public int getTgaDtIniEff() {
        return this.tgaDtIniEff;
    }

    public void setTgaDtEndEff(int tgaDtEndEff) {
        this.tgaDtEndEff = tgaDtEndEff;
    }

    public int getTgaDtEndEff() {
        return this.tgaDtEndEff;
    }

    public void setTgaCodCompAnia(int tgaCodCompAnia) {
        this.tgaCodCompAnia = tgaCodCompAnia;
    }

    public int getTgaCodCompAnia() {
        return this.tgaCodCompAnia;
    }

    public void setTgaDtDecor(int tgaDtDecor) {
        this.tgaDtDecor = tgaDtDecor;
    }

    public int getTgaDtDecor() {
        return this.tgaDtDecor;
    }

    public void setTgaIbOgg(String tgaIbOgg) {
        this.tgaIbOgg = Functions.subString(tgaIbOgg, Len.TGA_IB_OGG);
    }

    public String getTgaIbOgg() {
        return this.tgaIbOgg;
    }

    public void setTgaTpRgmFisc(String tgaTpRgmFisc) {
        this.tgaTpRgmFisc = Functions.subString(tgaTpRgmFisc, Len.TGA_TP_RGM_FISC);
    }

    public String getTgaTpRgmFisc() {
        return this.tgaTpRgmFisc;
    }

    public void setTgaTpTrch(String tgaTpTrch) {
        this.tgaTpTrch = Functions.subString(tgaTpTrch, Len.TGA_TP_TRCH);
    }

    public String getTgaTpTrch() {
        return this.tgaTpTrch;
    }

    public void setTgaFlCarCont(char tgaFlCarCont) {
        this.tgaFlCarCont = tgaFlCarCont;
    }

    public char getTgaFlCarCont() {
        return this.tgaFlCarCont;
    }

    public void setTgaCodDvs(String tgaCodDvs) {
        this.tgaCodDvs = Functions.subString(tgaCodDvs, Len.TGA_COD_DVS);
    }

    public String getTgaCodDvs() {
        return this.tgaCodDvs;
    }

    public void setTgaTpAdegAbb(char tgaTpAdegAbb) {
        this.tgaTpAdegAbb = tgaTpAdegAbb;
    }

    public char getTgaTpAdegAbb() {
        return this.tgaTpAdegAbb;
    }

    public void setTgaModCalc(String tgaModCalc) {
        this.tgaModCalc = Functions.subString(tgaModCalc, Len.TGA_MOD_CALC);
    }

    public String getTgaModCalc() {
        return this.tgaModCalc;
    }

    public String getTgaModCalcFormatted() {
        return Functions.padBlanks(getTgaModCalc(), Len.TGA_MOD_CALC);
    }

    public void setTgaFlImportiForz(char tgaFlImportiForz) {
        this.tgaFlImportiForz = tgaFlImportiForz;
    }

    public char getTgaFlImportiForz() {
        return this.tgaFlImportiForz;
    }

    public void setTgaFlProvForz(char tgaFlProvForz) {
        this.tgaFlProvForz = tgaFlProvForz;
    }

    public char getTgaFlProvForz() {
        return this.tgaFlProvForz;
    }

    public void setTgaTpRival(String tgaTpRival) {
        this.tgaTpRival = Functions.subString(tgaTpRival, Len.TGA_TP_RIVAL);
    }

    public String getTgaTpRival() {
        return this.tgaTpRival;
    }

    public String getTgaTpRivalFormatted() {
        return Functions.padBlanks(getTgaTpRival(), Len.TGA_TP_RIVAL);
    }

    public void setTgaTpManfeeAppl(String tgaTpManfeeAppl) {
        this.tgaTpManfeeAppl = Functions.subString(tgaTpManfeeAppl, Len.TGA_TP_MANFEE_APPL);
    }

    public String getTgaTpManfeeAppl() {
        return this.tgaTpManfeeAppl;
    }

    public String getTgaTpManfeeApplFormatted() {
        return Functions.padBlanks(getTgaTpManfeeAppl(), Len.TGA_TP_MANFEE_APPL);
    }

    public void setTgaDsRiga(long tgaDsRiga) {
        this.tgaDsRiga = tgaDsRiga;
    }

    public long getTgaDsRiga() {
        return this.tgaDsRiga;
    }

    public void setTgaDsOperSql(char tgaDsOperSql) {
        this.tgaDsOperSql = tgaDsOperSql;
    }

    public char getTgaDsOperSql() {
        return this.tgaDsOperSql;
    }

    public void setTgaDsVer(int tgaDsVer) {
        this.tgaDsVer = tgaDsVer;
    }

    public int getTgaDsVer() {
        return this.tgaDsVer;
    }

    public void setTgaDsTsIniCptz(long tgaDsTsIniCptz) {
        this.tgaDsTsIniCptz = tgaDsTsIniCptz;
    }

    public long getTgaDsTsIniCptz() {
        return this.tgaDsTsIniCptz;
    }

    public void setTgaDsTsEndCptz(long tgaDsTsEndCptz) {
        this.tgaDsTsEndCptz = tgaDsTsEndCptz;
    }

    public long getTgaDsTsEndCptz() {
        return this.tgaDsTsEndCptz;
    }

    public void setTgaDsUtente(String tgaDsUtente) {
        this.tgaDsUtente = Functions.subString(tgaDsUtente, Len.TGA_DS_UTENTE);
    }

    public String getTgaDsUtente() {
        return this.tgaDsUtente;
    }

    public void setTgaDsStatoElab(char tgaDsStatoElab) {
        this.tgaDsStatoElab = tgaDsStatoElab;
    }

    public char getTgaDsStatoElab() {
        return this.tgaDsStatoElab;
    }

    public TgaAbbAnnuUlt getTgaAbbAnnuUlt() {
        return tgaAbbAnnuUlt;
    }

    public TgaAbbTotIni getTgaAbbTotIni() {
        return tgaAbbTotIni;
    }

    public TgaAbbTotUlt getTgaAbbTotUlt() {
        return tgaAbbTotUlt;
    }

    public TgaAcqExp getTgaAcqExp() {
        return tgaAcqExp;
    }

    public TgaAlqCommisInter getTgaAlqCommisInter() {
        return tgaAlqCommisInter;
    }

    public TgaAlqProvAcq getTgaAlqProvAcq() {
        return tgaAlqProvAcq;
    }

    public TgaAlqProvInc getTgaAlqProvInc() {
        return tgaAlqProvInc;
    }

    public TgaAlqProvRicor getTgaAlqProvRicor() {
        return tgaAlqProvRicor;
    }

    public TgaAlqRemunAss getTgaAlqRemunAss() {
        return tgaAlqRemunAss;
    }

    public TgaAlqScon getTgaAlqScon() {
        return tgaAlqScon;
    }

    public TgaBnsGiaLiqto getTgaBnsGiaLiqto() {
        return tgaBnsGiaLiqto;
    }

    public TgaCommisGest getTgaCommisGest() {
        return tgaCommisGest;
    }

    public TgaCommisInter getTgaCommisInter() {
        return tgaCommisInter;
    }

    public TgaCosRunAssva getTgaCosRunAssva() {
        return tgaCosRunAssva;
    }

    public TgaCosRunAssvaIdc getTgaCosRunAssvaIdc() {
        return tgaCosRunAssvaIdc;
    }

    public TgaCptInOpzRivto getTgaCptInOpzRivto() {
        return tgaCptInOpzRivto;
    }

    public TgaCptMinScad getTgaCptMinScad() {
        return tgaCptMinScad;
    }

    public TgaCptRshMor getTgaCptRshMor() {
        return tgaCptRshMor;
    }

    public TgaDtEffStab getTgaDtEffStab() {
        return tgaDtEffStab;
    }

    public TgaDtEmis getTgaDtEmis() {
        return tgaDtEmis;
    }

    public TgaDtIniValTar getTgaDtIniValTar() {
        return tgaDtIniValTar;
    }

    public TgaDtScad getTgaDtScad() {
        return tgaDtScad;
    }

    public TgaDtUltAdegPrePr getTgaDtUltAdegPrePr() {
        return tgaDtUltAdegPrePr;
    }

    public TgaDtVldtProd getTgaDtVldtProd() {
        return tgaDtVldtProd;
    }

    public TgaDurAa getTgaDurAa() {
        return tgaDurAa;
    }

    public TgaDurAbb getTgaDurAbb() {
        return tgaDurAbb;
    }

    public TgaDurGg getTgaDurGg() {
        return tgaDurGg;
    }

    public TgaDurMm getTgaDurMm() {
        return tgaDurMm;
    }

    public TgaEtaAa1oAssto getTgaEtaAa1oAssto() {
        return tgaEtaAa1oAssto;
    }

    public TgaEtaAa2oAssto getTgaEtaAa2oAssto() {
        return tgaEtaAa2oAssto;
    }

    public TgaEtaAa3oAssto getTgaEtaAa3oAssto() {
        return tgaEtaAa3oAssto;
    }

    public TgaEtaMm1oAssto getTgaEtaMm1oAssto() {
        return tgaEtaMm1oAssto;
    }

    public TgaEtaMm2oAssto getTgaEtaMm2oAssto() {
        return tgaEtaMm2oAssto;
    }

    public TgaEtaMm3oAssto getTgaEtaMm3oAssto() {
        return tgaEtaMm3oAssto;
    }

    public TgaIdMoviChiu getTgaIdMoviChiu() {
        return tgaIdMoviChiu;
    }

    public TgaImpAder getTgaImpAder() {
        return tgaImpAder;
    }

    public TgaImpAltSopr getTgaImpAltSopr() {
        return tgaImpAltSopr;
    }

    public TgaImpAz getTgaImpAz() {
        return tgaImpAz;
    }

    public TgaImpBns getTgaImpBns() {
        return tgaImpBns;
    }

    public TgaImpBnsAntic getTgaImpBnsAntic() {
        return tgaImpBnsAntic;
    }

    public TgaImpCarAcq getTgaImpCarAcq() {
        return tgaImpCarAcq;
    }

    public TgaImpCarGest getTgaImpCarGest() {
        return tgaImpCarGest;
    }

    public TgaImpCarInc getTgaImpCarInc() {
        return tgaImpCarInc;
    }

    public TgaImpScon getTgaImpScon() {
        return tgaImpScon;
    }

    public TgaImpSoprProf getTgaImpSoprProf() {
        return tgaImpSoprProf;
    }

    public TgaImpSoprSan getTgaImpSoprSan() {
        return tgaImpSoprSan;
    }

    public TgaImpSoprSpo getTgaImpSoprSpo() {
        return tgaImpSoprSpo;
    }

    public TgaImpSoprTec getTgaImpSoprTec() {
        return tgaImpSoprTec;
    }

    public TgaImpTfr getTgaImpTfr() {
        return tgaImpTfr;
    }

    public TgaImpTfrStrc getTgaImpTfrStrc() {
        return tgaImpTfrStrc;
    }

    public TgaImpTrasfe getTgaImpTrasfe() {
        return tgaImpTrasfe;
    }

    public TgaImpVolo getTgaImpVolo() {
        return tgaImpVolo;
    }

    public TgaImpbCommisInter getTgaImpbCommisInter() {
        return tgaImpbCommisInter;
    }

    public TgaImpbProvAcq getTgaImpbProvAcq() {
        return tgaImpbProvAcq;
    }

    public TgaImpbProvInc getTgaImpbProvInc() {
        return tgaImpbProvInc;
    }

    public TgaImpbProvRicor getTgaImpbProvRicor() {
        return tgaImpbProvRicor;
    }

    public TgaImpbRemunAss getTgaImpbRemunAss() {
        return tgaImpbRemunAss;
    }

    public TgaImpbVisEnd2000 getTgaImpbVisEnd2000() {
        return tgaImpbVisEnd2000;
    }

    public TgaIncrPre getTgaIncrPre() {
        return tgaIncrPre;
    }

    public TgaIncrPrstz getTgaIncrPrstz() {
        return tgaIncrPrstz;
    }

    public TgaIntrMora getTgaIntrMora() {
        return tgaIntrMora;
    }

    public TgaManfeeAntic getTgaManfeeAntic() {
        return tgaManfeeAntic;
    }

    public TgaManfeeRicor getTgaManfeeRicor() {
        return tgaManfeeRicor;
    }

    public TgaMatuEnd2000 getTgaMatuEnd2000() {
        return tgaMatuEnd2000;
    }

    public TgaMinGarto getTgaMinGarto() {
        return tgaMinGarto;
    }

    public TgaMinTrnut getTgaMinTrnut() {
        return tgaMinTrnut;
    }

    public TgaNumGgRival getTgaNumGgRival() {
        return tgaNumGgRival;
    }

    public TgaOldTsTec getTgaOldTsTec() {
        return tgaOldTsTec;
    }

    public TgaPcCommisGest getTgaPcCommisGest() {
        return tgaPcCommisGest;
    }

    public TgaPcIntrRiat getTgaPcIntrRiat() {
        return tgaPcIntrRiat;
    }

    public TgaPcRetr getTgaPcRetr() {
        return tgaPcRetr;
    }

    public TgaPcRipPre getTgaPcRipPre() {
        return tgaPcRipPre;
    }

    public TgaPreAttDiTrch getTgaPreAttDiTrch() {
        return tgaPreAttDiTrch;
    }

    public TgaPreCasoMor getTgaPreCasoMor() {
        return tgaPreCasoMor;
    }

    public TgaPreIniNet getTgaPreIniNet() {
        return tgaPreIniNet;
    }

    public TgaPreInvrioIni getTgaPreInvrioIni() {
        return tgaPreInvrioIni;
    }

    public TgaPreInvrioUlt getTgaPreInvrioUlt() {
        return tgaPreInvrioUlt;
    }

    public TgaPreLrd getTgaPreLrd() {
        return tgaPreLrd;
    }

    public TgaPrePattuito getTgaPrePattuito() {
        return tgaPrePattuito;
    }

    public TgaPrePpIni getTgaPrePpIni() {
        return tgaPrePpIni;
    }

    public TgaPrePpUlt getTgaPrePpUlt() {
        return tgaPrePpUlt;
    }

    public TgaPreRivto getTgaPreRivto() {
        return tgaPreRivto;
    }

    public TgaPreStab getTgaPreStab() {
        return tgaPreStab;
    }

    public TgaPreTariIni getTgaPreTariIni() {
        return tgaPreTariIni;
    }

    public TgaPreTariUlt getTgaPreTariUlt() {
        return tgaPreTariUlt;
    }

    public TgaPreUniRivto getTgaPreUniRivto() {
        return tgaPreUniRivto;
    }

    public TgaProv1aaAcq getTgaProv1aaAcq() {
        return tgaProv1aaAcq;
    }

    public TgaProv2aaAcq getTgaProv2aaAcq() {
        return tgaProv2aaAcq;
    }

    public TgaProvInc getTgaProvInc() {
        return tgaProvInc;
    }

    public TgaProvRicor getTgaProvRicor() {
        return tgaProvRicor;
    }

    public TgaPrstzAggIni getTgaPrstzAggIni() {
        return tgaPrstzAggIni;
    }

    public TgaPrstzAggUlt getTgaPrstzAggUlt() {
        return tgaPrstzAggUlt;
    }

    public TgaPrstzIni getTgaPrstzIni() {
        return tgaPrstzIni;
    }

    public TgaPrstzIniNewfis getTgaPrstzIniNewfis() {
        return tgaPrstzIniNewfis;
    }

    public TgaPrstzIniNforz getTgaPrstzIniNforz() {
        return tgaPrstzIniNforz;
    }

    public TgaPrstzIniStab getTgaPrstzIniStab() {
        return tgaPrstzIniStab;
    }

    public TgaPrstzRidIni getTgaPrstzRidIni() {
        return tgaPrstzRidIni;
    }

    public TgaPrstzUlt getTgaPrstzUlt() {
        return tgaPrstzUlt;
    }

    public TgaRatLrd getTgaRatLrd() {
        return tgaRatLrd;
    }

    public TgaRemunAss getTgaRemunAss() {
        return tgaRemunAss;
    }

    public TgaRenIniTsTec0 getTgaRenIniTsTec0() {
        return tgaRenIniTsTec0;
    }

    public TgaRendtoLrd getTgaRendtoLrd() {
        return tgaRendtoLrd;
    }

    public TgaRendtoRetr getTgaRendtoRetr() {
        return tgaRendtoRetr;
    }

    public TgaRisMat getTgaRisMat() {
        return tgaRisMat;
    }

    public TgaTsRivalFis getTgaTsRivalFis() {
        return tgaTsRivalFis;
    }

    public TgaTsRivalIndiciz getTgaTsRivalIndiciz() {
        return tgaTsRivalIndiciz;
    }

    public TgaTsRivalNet getTgaTsRivalNet() {
        return tgaTsRivalNet;
    }

    public TgaVisEnd2000 getTgaVisEnd2000() {
        return tgaVisEnd2000;
    }

    public TgaVisEnd2000Nforz getTgaVisEnd2000Nforz() {
        return tgaVisEnd2000Nforz;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IB_OGG = 40;
        public static final int TGA_TP_RGM_FISC = 2;
        public static final int TGA_TP_TRCH = 2;
        public static final int TGA_COD_DVS = 20;
        public static final int TGA_MOD_CALC = 2;
        public static final int TGA_TP_RIVAL = 2;
        public static final int TGA_TP_MANFEE_APPL = 2;
        public static final int TGA_DS_UTENTE = 20;
        public static final int TGA_ID_TRCH_DI_GAR = 5;
        public static final int TGA_ID_GAR = 5;
        public static final int TGA_ID_ADES = 5;
        public static final int TGA_ID_POLI = 5;
        public static final int TGA_ID_MOVI_CRZ = 5;
        public static final int TGA_DT_INI_EFF = 5;
        public static final int TGA_DT_END_EFF = 5;
        public static final int TGA_COD_COMP_ANIA = 3;
        public static final int TGA_DT_DECOR = 5;
        public static final int TGA_FL_CAR_CONT = 1;
        public static final int TGA_TP_ADEG_ABB = 1;
        public static final int TGA_FL_IMPORTI_FORZ = 1;
        public static final int TGA_FL_PROV_FORZ = 1;
        public static final int TGA_DS_RIGA = 6;
        public static final int TGA_DS_OPER_SQL = 1;
        public static final int TGA_DS_VER = 5;
        public static final int TGA_DS_TS_INI_CPTZ = 10;
        public static final int TGA_DS_TS_END_CPTZ = 10;
        public static final int TGA_DS_STATO_ELAB = 1;
        public static final int TRCH_DI_GAR = TGA_ID_TRCH_DI_GAR + TGA_ID_GAR + TGA_ID_ADES + TGA_ID_POLI + TGA_ID_MOVI_CRZ + TgaIdMoviChiu.Len.TGA_ID_MOVI_CHIU + TGA_DT_INI_EFF + TGA_DT_END_EFF + TGA_COD_COMP_ANIA + TGA_DT_DECOR + TgaDtScad.Len.TGA_DT_SCAD + TGA_IB_OGG + TGA_TP_RGM_FISC + TgaDtEmis.Len.TGA_DT_EMIS + TGA_TP_TRCH + TgaDurAa.Len.TGA_DUR_AA + TgaDurMm.Len.TGA_DUR_MM + TgaDurGg.Len.TGA_DUR_GG + TgaPreCasoMor.Len.TGA_PRE_CASO_MOR + TgaPcIntrRiat.Len.TGA_PC_INTR_RIAT + TgaImpBnsAntic.Len.TGA_IMP_BNS_ANTIC + TgaPreIniNet.Len.TGA_PRE_INI_NET + TgaPrePpIni.Len.TGA_PRE_PP_INI + TgaPrePpUlt.Len.TGA_PRE_PP_ULT + TgaPreTariIni.Len.TGA_PRE_TARI_INI + TgaPreTariUlt.Len.TGA_PRE_TARI_ULT + TgaPreInvrioIni.Len.TGA_PRE_INVRIO_INI + TgaPreInvrioUlt.Len.TGA_PRE_INVRIO_ULT + TgaPreRivto.Len.TGA_PRE_RIVTO + TgaImpSoprProf.Len.TGA_IMP_SOPR_PROF + TgaImpSoprSan.Len.TGA_IMP_SOPR_SAN + TgaImpSoprSpo.Len.TGA_IMP_SOPR_SPO + TgaImpSoprTec.Len.TGA_IMP_SOPR_TEC + TgaImpAltSopr.Len.TGA_IMP_ALT_SOPR + TgaPreStab.Len.TGA_PRE_STAB + TgaDtEffStab.Len.TGA_DT_EFF_STAB + TgaTsRivalFis.Len.TGA_TS_RIVAL_FIS + TgaTsRivalIndiciz.Len.TGA_TS_RIVAL_INDICIZ + TgaOldTsTec.Len.TGA_OLD_TS_TEC + TgaRatLrd.Len.TGA_RAT_LRD + TgaPreLrd.Len.TGA_PRE_LRD + TgaPrstzIni.Len.TGA_PRSTZ_INI + TgaPrstzUlt.Len.TGA_PRSTZ_ULT + TgaCptInOpzRivto.Len.TGA_CPT_IN_OPZ_RIVTO + TgaPrstzIniStab.Len.TGA_PRSTZ_INI_STAB + TgaCptRshMor.Len.TGA_CPT_RSH_MOR + TgaPrstzRidIni.Len.TGA_PRSTZ_RID_INI + TGA_FL_CAR_CONT + TgaBnsGiaLiqto.Len.TGA_BNS_GIA_LIQTO + TgaImpBns.Len.TGA_IMP_BNS + TGA_COD_DVS + TgaPrstzIniNewfis.Len.TGA_PRSTZ_INI_NEWFIS + TgaImpScon.Len.TGA_IMP_SCON + TgaAlqScon.Len.TGA_ALQ_SCON + TgaImpCarAcq.Len.TGA_IMP_CAR_ACQ + TgaImpCarInc.Len.TGA_IMP_CAR_INC + TgaImpCarGest.Len.TGA_IMP_CAR_GEST + TgaEtaAa1oAssto.Len.TGA_ETA_AA1O_ASSTO + TgaEtaMm1oAssto.Len.TGA_ETA_MM1O_ASSTO + TgaEtaAa2oAssto.Len.TGA_ETA_AA2O_ASSTO + TgaEtaMm2oAssto.Len.TGA_ETA_MM2O_ASSTO + TgaEtaAa3oAssto.Len.TGA_ETA_AA3O_ASSTO + TgaEtaMm3oAssto.Len.TGA_ETA_MM3O_ASSTO + TgaRendtoLrd.Len.TGA_RENDTO_LRD + TgaPcRetr.Len.TGA_PC_RETR + TgaRendtoRetr.Len.TGA_RENDTO_RETR + TgaMinGarto.Len.TGA_MIN_GARTO + TgaMinTrnut.Len.TGA_MIN_TRNUT + TgaPreAttDiTrch.Len.TGA_PRE_ATT_DI_TRCH + TgaMatuEnd2000.Len.TGA_MATU_END2000 + TgaAbbTotIni.Len.TGA_ABB_TOT_INI + TgaAbbTotUlt.Len.TGA_ABB_TOT_ULT + TgaAbbAnnuUlt.Len.TGA_ABB_ANNU_ULT + TgaDurAbb.Len.TGA_DUR_ABB + TGA_TP_ADEG_ABB + TGA_MOD_CALC + TgaImpAz.Len.TGA_IMP_AZ + TgaImpAder.Len.TGA_IMP_ADER + TgaImpTfr.Len.TGA_IMP_TFR + TgaImpVolo.Len.TGA_IMP_VOLO + TgaVisEnd2000.Len.TGA_VIS_END2000 + TgaDtVldtProd.Len.TGA_DT_VLDT_PROD + TgaDtIniValTar.Len.TGA_DT_INI_VAL_TAR + TgaImpbVisEnd2000.Len.TGA_IMPB_VIS_END2000 + TgaRenIniTsTec0.Len.TGA_REN_INI_TS_TEC0 + TgaPcRipPre.Len.TGA_PC_RIP_PRE + TGA_FL_IMPORTI_FORZ + TgaPrstzIniNforz.Len.TGA_PRSTZ_INI_NFORZ + TgaVisEnd2000Nforz.Len.TGA_VIS_END2000_NFORZ + TgaIntrMora.Len.TGA_INTR_MORA + TgaManfeeAntic.Len.TGA_MANFEE_ANTIC + TgaManfeeRicor.Len.TGA_MANFEE_RICOR + TgaPreUniRivto.Len.TGA_PRE_UNI_RIVTO + TgaProv1aaAcq.Len.TGA_PROV1AA_ACQ + TgaProv2aaAcq.Len.TGA_PROV2AA_ACQ + TgaProvRicor.Len.TGA_PROV_RICOR + TgaProvInc.Len.TGA_PROV_INC + TgaAlqProvAcq.Len.TGA_ALQ_PROV_ACQ + TgaAlqProvInc.Len.TGA_ALQ_PROV_INC + TgaAlqProvRicor.Len.TGA_ALQ_PROV_RICOR + TgaImpbProvAcq.Len.TGA_IMPB_PROV_ACQ + TgaImpbProvInc.Len.TGA_IMPB_PROV_INC + TgaImpbProvRicor.Len.TGA_IMPB_PROV_RICOR + TGA_FL_PROV_FORZ + TgaPrstzAggIni.Len.TGA_PRSTZ_AGG_INI + TgaIncrPre.Len.TGA_INCR_PRE + TgaIncrPrstz.Len.TGA_INCR_PRSTZ + TgaDtUltAdegPrePr.Len.TGA_DT_ULT_ADEG_PRE_PR + TgaPrstzAggUlt.Len.TGA_PRSTZ_AGG_ULT + TgaTsRivalNet.Len.TGA_TS_RIVAL_NET + TgaPrePattuito.Len.TGA_PRE_PATTUITO + TGA_TP_RIVAL + TgaRisMat.Len.TGA_RIS_MAT + TgaCptMinScad.Len.TGA_CPT_MIN_SCAD + TgaCommisGest.Len.TGA_COMMIS_GEST + TGA_TP_MANFEE_APPL + TGA_DS_RIGA + TGA_DS_OPER_SQL + TGA_DS_VER + TGA_DS_TS_INI_CPTZ + TGA_DS_TS_END_CPTZ + TGA_DS_UTENTE + TGA_DS_STATO_ELAB + TgaPcCommisGest.Len.TGA_PC_COMMIS_GEST + TgaNumGgRival.Len.TGA_NUM_GG_RIVAL + TgaImpTrasfe.Len.TGA_IMP_TRASFE + TgaImpTfrStrc.Len.TGA_IMP_TFR_STRC + TgaAcqExp.Len.TGA_ACQ_EXP + TgaRemunAss.Len.TGA_REMUN_ASS + TgaCommisInter.Len.TGA_COMMIS_INTER + TgaAlqRemunAss.Len.TGA_ALQ_REMUN_ASS + TgaAlqCommisInter.Len.TGA_ALQ_COMMIS_INTER + TgaImpbRemunAss.Len.TGA_IMPB_REMUN_ASS + TgaImpbCommisInter.Len.TGA_IMPB_COMMIS_INTER + TgaCosRunAssva.Len.TGA_COS_RUN_ASSVA + TgaCosRunAssvaIdc.Len.TGA_COS_RUN_ASSVA_IDC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ID_TRCH_DI_GAR = 9;
            public static final int TGA_ID_GAR = 9;
            public static final int TGA_ID_ADES = 9;
            public static final int TGA_ID_POLI = 9;
            public static final int TGA_ID_MOVI_CRZ = 9;
            public static final int TGA_DT_INI_EFF = 8;
            public static final int TGA_DT_END_EFF = 8;
            public static final int TGA_COD_COMP_ANIA = 5;
            public static final int TGA_DT_DECOR = 8;
            public static final int TGA_DS_RIGA = 10;
            public static final int TGA_DS_VER = 9;
            public static final int TGA_DS_TS_INI_CPTZ = 18;
            public static final int TGA_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
