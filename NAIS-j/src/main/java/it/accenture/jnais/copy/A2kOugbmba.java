package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: A2K-OUGBMBA<br>
 * Variable: A2K-OUGBMBA from copybook LCCC0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class A2kOugbmba {

    //==== PROPERTIES ====
    //Original name: A2K-OUGG02
    private String ougg02 = DefaultValues.stringVal(Len.OUGG02);
    //Original name: A2K-OUS102
    private char ous102 = DefaultValues.CHAR_VAL;
    //Original name: A2K-OUMM02
    private String oumm02 = DefaultValues.stringVal(Len.OUMM02);
    //Original name: A2K-OUS202
    private char ous202 = DefaultValues.CHAR_VAL;
    //Original name: A2K-OUSS02
    private String ouss02 = DefaultValues.stringVal(Len.OUSS02);
    //Original name: A2K-OUAA02
    private String ouaa02 = DefaultValues.stringVal(Len.OUAA02);

    //==== METHODS ====
    public void setA2kOugbmbaBytes(byte[] buffer, int offset) {
        int position = offset;
        ougg02 = MarshalByte.readFixedString(buffer, position, Len.OUGG02);
        position += Len.OUGG02;
        ous102 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        oumm02 = MarshalByte.readFixedString(buffer, position, Len.OUMM02);
        position += Len.OUMM02;
        ous202 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ouss02 = MarshalByte.readFixedString(buffer, position, Len.OUSS02);
        position += Len.OUSS02;
        ouaa02 = MarshalByte.readFixedString(buffer, position, Len.OUAA02);
    }

    public byte[] getA2kOugbmbaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ougg02, Len.OUGG02);
        position += Len.OUGG02;
        MarshalByte.writeChar(buffer, position, ous102);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, oumm02, Len.OUMM02);
        position += Len.OUMM02;
        MarshalByte.writeChar(buffer, position, ous202);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ouss02, Len.OUSS02);
        position += Len.OUSS02;
        MarshalByte.writeString(buffer, position, ouaa02, Len.OUAA02);
        return buffer;
    }

    public void initA2kOugbmbaSpaces() {
        ougg02 = "";
        ous102 = Types.SPACE_CHAR;
        oumm02 = "";
        ous202 = Types.SPACE_CHAR;
        ouss02 = "";
        ouaa02 = "";
    }

    public void setOugg02Formatted(String ougg02) {
        this.ougg02 = Trunc.toUnsignedNumeric(ougg02, Len.OUGG02);
    }

    public short getOugg02() {
        return NumericDisplay.asShort(this.ougg02);
    }

    public void setOus102(char ous102) {
        this.ous102 = ous102;
    }

    public void setOus102Formatted(String ous102) {
        setOus102(Functions.charAt(ous102, Types.CHAR_SIZE));
    }

    public char getOus102() {
        return this.ous102;
    }

    public void setOumm02Formatted(String oumm02) {
        this.oumm02 = Trunc.toUnsignedNumeric(oumm02, Len.OUMM02);
    }

    public short getOumm02() {
        return NumericDisplay.asShort(this.oumm02);
    }

    public void setOus202(char ous202) {
        this.ous202 = ous202;
    }

    public void setOus202Formatted(String ous202) {
        setOus202(Functions.charAt(ous202, Types.CHAR_SIZE));
    }

    public char getOus202() {
        return this.ous202;
    }

    public void setOuss02Formatted(String ouss02) {
        this.ouss02 = Trunc.toUnsignedNumeric(ouss02, Len.OUSS02);
    }

    public short getOuss02() {
        return NumericDisplay.asShort(this.ouss02);
    }

    public void setOuaa02Formatted(String ouaa02) {
        this.ouaa02 = Trunc.toUnsignedNumeric(ouaa02, Len.OUAA02);
    }

    public short getOuaa02() {
        return NumericDisplay.asShort(this.ouaa02);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OUGG02 = 2;
        public static final int OUS102 = 1;
        public static final int OUMM02 = 2;
        public static final int OUS202 = 1;
        public static final int OUSS02 = 2;
        public static final int OUAA02 = 2;
        public static final int A2K_OUGBMBA = OUGG02 + OUS102 + OUMM02 + OUS202 + OUSS02 + OUAA02;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
