package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: TAB-GUIDE-LETTE-REPORT<br>
 * Variable: TAB-GUIDE-LETTE-REPORT from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TabGuideLetteReport {

    //==== PROPERTIES ====
    //Original name: FILLER-TAB-GUIDE-LETTE-REPORT
    private String flr1 = "OCCORRENZE GUIDA LETTE";
    //Original name: FILLER-TAB-GUIDE-LETTE-REPORT-1
    private String flr2 = " :";
    //Original name: REP-TAB-GUIDE-LETTE
    private String repTabGuideLette = "000000000";

    //==== METHODS ====
    public String getTabGuideLetteReportFormatted() {
        return MarshalByteExt.bufferToStr(getTabGuideLetteReportBytes());
    }

    public byte[] getTabGuideLetteReportBytes() {
        byte[] buffer = new byte[Len.TAB_GUIDE_LETTE_REPORT];
        return getTabGuideLetteReportBytes(buffer, 1);
    }

    public byte[] getTabGuideLetteReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, repTabGuideLette, Len.REP_TAB_GUIDE_LETTE);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public void setRepTabGuideLette(long repTabGuideLette) {
        this.repTabGuideLette = PicFormatter.display("Z(8)9").format(repTabGuideLette).toString();
    }

    public long getRepTabGuideLette() {
        return PicParser.display("Z(8)9").parseLong(this.repTabGuideLette);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int REP_TAB_GUIDE_LETTE = 9;
        public static final int FLR1 = 50;
        public static final int FLR2 = 4;
        public static final int TAB_GUIDE_LETTE_REPORT = REP_TAB_GUIDE_LETTE + FLR1 + FLR2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
