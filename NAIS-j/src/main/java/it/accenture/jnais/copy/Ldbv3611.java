package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV3611<br>
 * Variable: LDBV3611 from copybook LDBV3611<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv3611 {

    //==== PROPERTIES ====
    //Original name: LDBV3611-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV3611-DT-A
    private int dtA = DefaultValues.INT_VAL;
    //Original name: LDBV3611-DT-A-DB
    private String dtADb = "";
    //Original name: LDBV3611-DT-DA
    private int dtDa = DefaultValues.INT_VAL;
    //Original name: LDBV3611-DT-DA-DB
    private String dtDaDb = "";
    //Original name: LDBV3611-TP-MOVI-1
    private int tpMovi1 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-2
    private int tpMovi2 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-3
    private int tpMovi3 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-4
    private int tpMovi4 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-5
    private int tpMovi5 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-6
    private int tpMovi6 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-7
    private int tpMovi7 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-8
    private int tpMovi8 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-9
    private int tpMovi9 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-10
    private int tpMovi10 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-11
    private int tpMovi11 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-12
    private int tpMovi12 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-13
    private int tpMovi13 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-14
    private int tpMovi14 = DefaultValues.INT_VAL;
    //Original name: LDBV3611-TP-MOVI-15
    private int tpMovi15 = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbv3611Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV3611];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV3611);
        setLdbv3611Bytes(buffer, 1);
    }

    public String getLdbv3611Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv3611Bytes());
    }

    public byte[] getLdbv3611Bytes() {
        byte[] buffer = new byte[Len.LDBV3611];
        return getLdbv3611Bytes(buffer, 1);
    }

    public void setLdbv3611Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        dtA = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_A, 0);
        position += Len.DT_A;
        dtADb = MarshalByte.readString(buffer, position, Len.DT_A_DB);
        position += Len.DT_A_DB;
        dtDa = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DA, 0);
        position += Len.DT_DA;
        dtDaDb = MarshalByte.readString(buffer, position, Len.DT_DA_DB);
        position += Len.DT_DA_DB;
        tpMovi1 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI1, 0);
        position += Len.TP_MOVI1;
        tpMovi2 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI2, 0);
        position += Len.TP_MOVI2;
        tpMovi3 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI3, 0);
        position += Len.TP_MOVI3;
        tpMovi4 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI4, 0);
        position += Len.TP_MOVI4;
        tpMovi5 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI5, 0);
        position += Len.TP_MOVI5;
        tpMovi6 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI6, 0);
        position += Len.TP_MOVI6;
        tpMovi7 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI7, 0);
        position += Len.TP_MOVI7;
        tpMovi8 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI8, 0);
        position += Len.TP_MOVI8;
        tpMovi9 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI9, 0);
        position += Len.TP_MOVI9;
        tpMovi10 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI10, 0);
        position += Len.TP_MOVI10;
        tpMovi11 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI11, 0);
        position += Len.TP_MOVI11;
        tpMovi12 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI12, 0);
        position += Len.TP_MOVI12;
        tpMovi13 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI13, 0);
        position += Len.TP_MOVI13;
        tpMovi14 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI14, 0);
        position += Len.TP_MOVI14;
        tpMovi15 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI15, 0);
    }

    public byte[] getLdbv3611Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, dtA, Len.Int.DT_A, 0);
        position += Len.DT_A;
        MarshalByte.writeString(buffer, position, dtADb, Len.DT_A_DB);
        position += Len.DT_A_DB;
        MarshalByte.writeIntAsPacked(buffer, position, dtDa, Len.Int.DT_DA, 0);
        position += Len.DT_DA;
        MarshalByte.writeString(buffer, position, dtDaDb, Len.DT_DA_DB);
        position += Len.DT_DA_DB;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi1, Len.Int.TP_MOVI1, 0);
        position += Len.TP_MOVI1;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi2, Len.Int.TP_MOVI2, 0);
        position += Len.TP_MOVI2;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi3, Len.Int.TP_MOVI3, 0);
        position += Len.TP_MOVI3;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi4, Len.Int.TP_MOVI4, 0);
        position += Len.TP_MOVI4;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi5, Len.Int.TP_MOVI5, 0);
        position += Len.TP_MOVI5;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi6, Len.Int.TP_MOVI6, 0);
        position += Len.TP_MOVI6;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi7, Len.Int.TP_MOVI7, 0);
        position += Len.TP_MOVI7;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi8, Len.Int.TP_MOVI8, 0);
        position += Len.TP_MOVI8;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi9, Len.Int.TP_MOVI9, 0);
        position += Len.TP_MOVI9;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi10, Len.Int.TP_MOVI10, 0);
        position += Len.TP_MOVI10;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi11, Len.Int.TP_MOVI11, 0);
        position += Len.TP_MOVI11;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi12, Len.Int.TP_MOVI12, 0);
        position += Len.TP_MOVI12;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi13, Len.Int.TP_MOVI13, 0);
        position += Len.TP_MOVI13;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi14, Len.Int.TP_MOVI14, 0);
        position += Len.TP_MOVI14;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi15, Len.Int.TP_MOVI15, 0);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setDtA(int dtA) {
        this.dtA = dtA;
    }

    public int getDtA() {
        return this.dtA;
    }

    public void setDtADb(String dtADb) {
        this.dtADb = Functions.subString(dtADb, Len.DT_A_DB);
    }

    public String getDtADb() {
        return this.dtADb;
    }

    public void setDtDa(int dtDa) {
        this.dtDa = dtDa;
    }

    public int getDtDa() {
        return this.dtDa;
    }

    public void setDtDaDb(String dtDaDb) {
        this.dtDaDb = Functions.subString(dtDaDb, Len.DT_DA_DB);
    }

    public String getDtDaDb() {
        return this.dtDaDb;
    }

    public void setTpMovi1(int tpMovi1) {
        this.tpMovi1 = tpMovi1;
    }

    public int getTpMovi1() {
        return this.tpMovi1;
    }

    public void setTpMovi2(int tpMovi2) {
        this.tpMovi2 = tpMovi2;
    }

    public int getTpMovi2() {
        return this.tpMovi2;
    }

    public void setTpMovi3(int tpMovi3) {
        this.tpMovi3 = tpMovi3;
    }

    public int getTpMovi3() {
        return this.tpMovi3;
    }

    public void setTpMovi4(int tpMovi4) {
        this.tpMovi4 = tpMovi4;
    }

    public int getTpMovi4() {
        return this.tpMovi4;
    }

    public void setTpMovi5(int tpMovi5) {
        this.tpMovi5 = tpMovi5;
    }

    public int getTpMovi5() {
        return this.tpMovi5;
    }

    public void setTpMovi6(int tpMovi6) {
        this.tpMovi6 = tpMovi6;
    }

    public int getTpMovi6() {
        return this.tpMovi6;
    }

    public void setTpMovi7(int tpMovi7) {
        this.tpMovi7 = tpMovi7;
    }

    public int getTpMovi7() {
        return this.tpMovi7;
    }

    public void setTpMovi8(int tpMovi8) {
        this.tpMovi8 = tpMovi8;
    }

    public int getTpMovi8() {
        return this.tpMovi8;
    }

    public void setTpMovi9(int tpMovi9) {
        this.tpMovi9 = tpMovi9;
    }

    public int getTpMovi9() {
        return this.tpMovi9;
    }

    public void setTpMovi10(int tpMovi10) {
        this.tpMovi10 = tpMovi10;
    }

    public int getTpMovi10() {
        return this.tpMovi10;
    }

    public void setTpMovi11(int tpMovi11) {
        this.tpMovi11 = tpMovi11;
    }

    public int getTpMovi11() {
        return this.tpMovi11;
    }

    public void setTpMovi12(int tpMovi12) {
        this.tpMovi12 = tpMovi12;
    }

    public int getTpMovi12() {
        return this.tpMovi12;
    }

    public void setTpMovi13(int tpMovi13) {
        this.tpMovi13 = tpMovi13;
    }

    public int getTpMovi13() {
        return this.tpMovi13;
    }

    public void setTpMovi14(int tpMovi14) {
        this.tpMovi14 = tpMovi14;
    }

    public int getTpMovi14() {
        return this.tpMovi14;
    }

    public void setTpMovi15(int tpMovi15) {
        this.tpMovi15 = tpMovi15;
    }

    public int getTpMovi15() {
        return this.tpMovi15;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int DT_A_DB = 10;
        public static final int DT_DA_DB = 10;
        public static final int ID_OGG = 5;
        public static final int DT_A = 5;
        public static final int DT_DA = 5;
        public static final int TP_MOVI1 = 3;
        public static final int TP_MOVI2 = 3;
        public static final int TP_MOVI3 = 3;
        public static final int TP_MOVI4 = 3;
        public static final int TP_MOVI5 = 3;
        public static final int TP_MOVI6 = 3;
        public static final int TP_MOVI7 = 3;
        public static final int TP_MOVI8 = 3;
        public static final int TP_MOVI9 = 3;
        public static final int TP_MOVI10 = 3;
        public static final int TP_MOVI11 = 3;
        public static final int TP_MOVI12 = 3;
        public static final int TP_MOVI13 = 3;
        public static final int TP_MOVI14 = 3;
        public static final int TP_MOVI15 = 3;
        public static final int LDBV3611 = ID_OGG + TP_OGG + DT_A + DT_A_DB + DT_DA + DT_DA_DB + TP_MOVI1 + TP_MOVI2 + TP_MOVI3 + TP_MOVI4 + TP_MOVI5 + TP_MOVI6 + TP_MOVI7 + TP_MOVI8 + TP_MOVI9 + TP_MOVI10 + TP_MOVI11 + TP_MOVI12 + TP_MOVI13 + TP_MOVI14 + TP_MOVI15;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int DT_A = 8;
            public static final int DT_DA = 8;
            public static final int TP_MOVI1 = 5;
            public static final int TP_MOVI2 = 5;
            public static final int TP_MOVI3 = 5;
            public static final int TP_MOVI4 = 5;
            public static final int TP_MOVI5 = 5;
            public static final int TP_MOVI6 = 5;
            public static final int TP_MOVI7 = 5;
            public static final int TP_MOVI8 = 5;
            public static final int TP_MOVI9 = 5;
            public static final int TP_MOVI10 = 5;
            public static final int TP_MOVI11 = 5;
            public static final int TP_MOVI12 = 5;
            public static final int TP_MOVI13 = 5;
            public static final int TP_MOVI14 = 5;
            public static final int TP_MOVI15 = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
