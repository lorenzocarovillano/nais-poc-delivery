package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVRDF1<br>
 * Variable: LCCVRDF1 from copybook LCCVRDF1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvrdf1 implements ICopyable<Lccvrdf1> {

    //==== PROPERTIES ====
    /**Original name: WRDF-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA RICH_DIS_FND
	 *    ALIAS RDF
	 *    ULTIMO AGG. 25 NOV 2019
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WRDF-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WRDF-DATI
    private WrdfDati dati = new WrdfDati();

    //==== CONSTRUCTORS ====
    public Lccvrdf1() {
    }

    public Lccvrdf1(Lccvrdf1 lccvrdf1) {
        this();
        this.status.setStatus(lccvrdf1.status.getStatus());
        this.idPtf = lccvrdf1.idPtf;
        this.dati = lccvrdf1.dati.copy();
    }

    //==== METHODS ====
    public void initLccvrdf1Spaces() {
        status.setStatus(Types.SPACE_CHAR);
        idPtf = Types.INVALID_INT_VAL;
        dati.initDatiSpaces();
    }

    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WrdfDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    public Lccvrdf1 copy() {
        return new Lccvrdf1(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
