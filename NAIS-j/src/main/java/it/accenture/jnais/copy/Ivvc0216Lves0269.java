package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.occurs.Ivvv0212CtrlAutOper;
import it.accenture.jnais.ws.redefines.WskdTabValP;
import it.accenture.jnais.ws.redefines.WskdTabValT;

/**Original name: IVVC0216<br>
 * Variable: IVVC0216 from copybook IVVC0216<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0216Lves0269 {

    //==== PROPERTIES ====
    public static final int WSKD_CTRL_AUT_OPER_MAXOCCURS = 20;
    /**Original name: WSKD-DEE<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA OUTPUT VALORIZZATORE VARIABILI
	 *    - area skeda per servizi di prodotto
	 *    - area variabili autonomia operativa
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *    AREA OUTPUT PRODOTTO SCHEDE 'P' 'Q' 'O'
	 * ----------------------------------------------------------------*</pre>*/
    private String wskdDee = DefaultValues.stringVal(Len.WSKD_DEE);
    //Original name: WSKD-ELE-LIVELLO-MAX-P
    private short wskdEleLivelloMaxP = DefaultValues.SHORT_VAL;
    //Original name: WSKD-TAB-VAL-P
    private WskdTabValP wskdTabValP = new WskdTabValP();
    /**Original name: WSKD-ELE-LIVELLO-MAX-T<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA OUTPUT TRANCHE SCHEDE 'G' 'H'
	 * ----------------------------------------------------------------*</pre>*/
    private short wskdEleLivelloMaxT = DefaultValues.SHORT_VAL;
    //Original name: WSKD-TAB-VAL-T
    private WskdTabValT wskdTabValT = new WskdTabValT();
    //Original name: WSKD-ELE-CTRL-AUT-OPER-MAX
    private short wskdEleCtrlAutOperMax = DefaultValues.SHORT_VAL;
    //Original name: WSKD-CTRL-AUT-OPER
    private Ivvv0212CtrlAutOper[] wskdCtrlAutOper = new Ivvv0212CtrlAutOper[WSKD_CTRL_AUT_OPER_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ivvc0216Lves0269() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wskdCtrlAutOperIdx = 1; wskdCtrlAutOperIdx <= WSKD_CTRL_AUT_OPER_MAXOCCURS; wskdCtrlAutOperIdx++) {
            wskdCtrlAutOper[wskdCtrlAutOperIdx - 1] = new Ivvv0212CtrlAutOper();
        }
    }

    public void setWskdDee(String wskdDee) {
        this.wskdDee = Functions.subString(wskdDee, Len.WSKD_DEE);
    }

    public String getWskdDee() {
        return this.wskdDee;
    }

    public void setWskdEleLivelloMaxP(short wskdEleLivelloMaxP) {
        this.wskdEleLivelloMaxP = wskdEleLivelloMaxP;
    }

    public short getWskdEleLivelloMaxP() {
        return this.wskdEleLivelloMaxP;
    }

    public void setWskdEleLivelloMaxT(short wskdEleLivelloMaxT) {
        this.wskdEleLivelloMaxT = wskdEleLivelloMaxT;
    }

    public short getWskdEleLivelloMaxT() {
        return this.wskdEleLivelloMaxT;
    }

    public void setWskdVarAutOperBytes(byte[] buffer, int offset) {
        int position = offset;
        wskdEleCtrlAutOperMax = MarshalByte.readPackedAsShort(buffer, position, Len.Int.WSKD_ELE_CTRL_AUT_OPER_MAX, 0);
        position += Len.WSKD_ELE_CTRL_AUT_OPER_MAX;
        for (int idx = 1; idx <= WSKD_CTRL_AUT_OPER_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wskdCtrlAutOper[idx - 1].setCtrlAutOperBytes(buffer, position);
                position += Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
            }
            else {
                wskdCtrlAutOper[idx - 1].initCtrlAutOperSpaces();
                position += Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
            }
        }
    }

    public void setWskdEleCtrlAutOperMax(short wskdEleCtrlAutOperMax) {
        this.wskdEleCtrlAutOperMax = wskdEleCtrlAutOperMax;
    }

    public short getWskdEleCtrlAutOperMax() {
        return this.wskdEleCtrlAutOperMax;
    }

    public WskdTabValP getWskdTabValP() {
        return wskdTabValP;
    }

    public WskdTabValT getWskdTabValT() {
        return wskdTabValT;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WSKD_DEE = 8;
        public static final int WSKD_ELE_LIVELLO_MAX_P = 3;
        public static final int WSKD_ELE_LIVELLO_MAX_T = 3;
        public static final int WSKD_ELE_CTRL_AUT_OPER_MAX = 3;
        public static final int WSKD_VAR_AUT_OPER = WSKD_ELE_CTRL_AUT_OPER_MAX + Ivvc0216Lves0269.WSKD_CTRL_AUT_OPER_MAXOCCURS * Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WSKD_ELE_LIVELLO_MAX_P = 4;
            public static final int WSKD_ELE_LIVELLO_MAX_T = 4;
            public static final int WSKD_ELE_CTRL_AUT_OPER_MAX = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
