package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LDBV6191<br>
 * Variable: LDBV6191 from copybook LDBV6191<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv6191 {

    //==== PROPERTIES ====
    //Original name: LDBV6191-DT-INF
    private String inf = DefaultValues.stringVal(Len.INF);
    //Original name: LDBV6191-DT-INF-DB
    private String infDb = DefaultValues.stringVal(Len.INF_DB);
    //Original name: LDBV6191-DT-SUP
    private String sup = DefaultValues.stringVal(Len.SUP);
    //Original name: LDBV6191-DT-SUP-DB
    private String supDb = DefaultValues.stringVal(Len.SUP_DB);

    //==== METHODS ====
    public void setLdbv6191Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV6191];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV6191);
        setLdbv6191Bytes(buffer, 1);
    }

    public String getLdbv6191Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv6191Bytes());
    }

    public byte[] getLdbv6191Bytes() {
        byte[] buffer = new byte[Len.LDBV6191];
        return getLdbv6191Bytes(buffer, 1);
    }

    public void setLdbv6191Bytes(byte[] buffer, int offset) {
        int position = offset;
        inf = MarshalByte.readFixedString(buffer, position, Len.INF);
        position += Len.INF;
        infDb = MarshalByte.readString(buffer, position, Len.INF_DB);
        position += Len.INF_DB;
        sup = MarshalByte.readFixedString(buffer, position, Len.SUP);
        position += Len.SUP;
        supDb = MarshalByte.readString(buffer, position, Len.SUP_DB);
    }

    public byte[] getLdbv6191Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, inf, Len.INF);
        position += Len.INF;
        MarshalByte.writeString(buffer, position, infDb, Len.INF_DB);
        position += Len.INF_DB;
        MarshalByte.writeString(buffer, position, sup, Len.SUP);
        position += Len.SUP;
        MarshalByte.writeString(buffer, position, supDb, Len.SUP_DB);
        return buffer;
    }

    public void setInfFormatted(String inf) {
        this.inf = Trunc.toUnsignedNumeric(inf, Len.INF);
    }

    public int getInf() {
        return NumericDisplay.asInt(this.inf);
    }

    public String getInfFormatted() {
        return this.inf;
    }

    public void setInfDb(String infDb) {
        this.infDb = Functions.subString(infDb, Len.INF_DB);
    }

    public String getInfDb() {
        return this.infDb;
    }

    public void setSupFormatted(String sup) {
        this.sup = Trunc.toUnsignedNumeric(sup, Len.SUP);
    }

    public int getSup() {
        return NumericDisplay.asInt(this.sup);
    }

    public String getSupFormatted() {
        return this.sup;
    }

    public void setSupDb(String supDb) {
        this.supDb = Functions.subString(supDb, Len.SUP_DB);
    }

    public String getSupDb() {
        return this.supDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INF_DB = 10;
        public static final int SUP_DB = 10;
        public static final int INF = 8;
        public static final int SUP = 8;
        public static final int LDBV6191 = INF + INF_DB + SUP + SUP_DB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
