package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WisoAlqIs;
import it.accenture.jnais.ws.redefines.WisoCumPreVers;
import it.accenture.jnais.ws.redefines.WisoDtEndPer;
import it.accenture.jnais.ws.redefines.WisoDtIniPer;
import it.accenture.jnais.ws.redefines.WisoIdMoviChiu;
import it.accenture.jnais.ws.redefines.WisoIdOgg;
import it.accenture.jnais.ws.redefines.WisoImpbIs;
import it.accenture.jnais.ws.redefines.WisoImpstSost;
import it.accenture.jnais.ws.redefines.WisoPrstzLrdAnteIs;
import it.accenture.jnais.ws.redefines.WisoPrstzNet;
import it.accenture.jnais.ws.redefines.WisoPrstzPrec;
import it.accenture.jnais.ws.redefines.WisoRisMatAnteTax;
import it.accenture.jnais.ws.redefines.WisoRisMatNetPrec;
import it.accenture.jnais.ws.redefines.WisoRisMatPostTax;

/**Original name: WISO-DATI<br>
 * Variable: WISO-DATI from copybook LCCVISO1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WisoDati {

    //==== PROPERTIES ====
    //Original name: WISO-ID-IMPST-SOST
    private int wisoIdImpstSost = DefaultValues.INT_VAL;
    //Original name: WISO-ID-OGG
    private WisoIdOgg wisoIdOgg = new WisoIdOgg();
    //Original name: WISO-TP-OGG
    private String wisoTpOgg = DefaultValues.stringVal(Len.WISO_TP_OGG);
    //Original name: WISO-ID-MOVI-CRZ
    private int wisoIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WISO-ID-MOVI-CHIU
    private WisoIdMoviChiu wisoIdMoviChiu = new WisoIdMoviChiu();
    //Original name: WISO-DT-INI-EFF
    private int wisoDtIniEff = DefaultValues.INT_VAL;
    //Original name: WISO-DT-END-EFF
    private int wisoDtEndEff = DefaultValues.INT_VAL;
    //Original name: WISO-DT-INI-PER
    private WisoDtIniPer wisoDtIniPer = new WisoDtIniPer();
    //Original name: WISO-DT-END-PER
    private WisoDtEndPer wisoDtEndPer = new WisoDtEndPer();
    //Original name: WISO-COD-COMP-ANIA
    private int wisoCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WISO-IMPST-SOST
    private WisoImpstSost wisoImpstSost = new WisoImpstSost();
    //Original name: WISO-IMPB-IS
    private WisoImpbIs wisoImpbIs = new WisoImpbIs();
    //Original name: WISO-ALQ-IS
    private WisoAlqIs wisoAlqIs = new WisoAlqIs();
    //Original name: WISO-COD-TRB
    private String wisoCodTrb = DefaultValues.stringVal(Len.WISO_COD_TRB);
    //Original name: WISO-PRSTZ-LRD-ANTE-IS
    private WisoPrstzLrdAnteIs wisoPrstzLrdAnteIs = new WisoPrstzLrdAnteIs();
    //Original name: WISO-RIS-MAT-NET-PREC
    private WisoRisMatNetPrec wisoRisMatNetPrec = new WisoRisMatNetPrec();
    //Original name: WISO-RIS-MAT-ANTE-TAX
    private WisoRisMatAnteTax wisoRisMatAnteTax = new WisoRisMatAnteTax();
    //Original name: WISO-RIS-MAT-POST-TAX
    private WisoRisMatPostTax wisoRisMatPostTax = new WisoRisMatPostTax();
    //Original name: WISO-PRSTZ-NET
    private WisoPrstzNet wisoPrstzNet = new WisoPrstzNet();
    //Original name: WISO-PRSTZ-PREC
    private WisoPrstzPrec wisoPrstzPrec = new WisoPrstzPrec();
    //Original name: WISO-CUM-PRE-VERS
    private WisoCumPreVers wisoCumPreVers = new WisoCumPreVers();
    //Original name: WISO-TP-CALC-IMPST
    private String wisoTpCalcImpst = DefaultValues.stringVal(Len.WISO_TP_CALC_IMPST);
    //Original name: WISO-IMP-GIA-TASSATO
    private AfDecimal wisoImpGiaTassato = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WISO-DS-RIGA
    private long wisoDsRiga = DefaultValues.LONG_VAL;
    //Original name: WISO-DS-OPER-SQL
    private char wisoDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WISO-DS-VER
    private int wisoDsVer = DefaultValues.INT_VAL;
    //Original name: WISO-DS-TS-INI-CPTZ
    private long wisoDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WISO-DS-TS-END-CPTZ
    private long wisoDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WISO-DS-UTENTE
    private String wisoDsUtente = DefaultValues.stringVal(Len.WISO_DS_UTENTE);
    //Original name: WISO-DS-STATO-ELAB
    private char wisoDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wisoIdImpstSost = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WISO_ID_IMPST_SOST, 0);
        position += Len.WISO_ID_IMPST_SOST;
        wisoIdOgg.setWisoIdOggFromBuffer(buffer, position);
        position += WisoIdOgg.Len.WISO_ID_OGG;
        wisoTpOgg = MarshalByte.readString(buffer, position, Len.WISO_TP_OGG);
        position += Len.WISO_TP_OGG;
        wisoIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WISO_ID_MOVI_CRZ, 0);
        position += Len.WISO_ID_MOVI_CRZ;
        wisoIdMoviChiu.setWisoIdMoviChiuFromBuffer(buffer, position);
        position += WisoIdMoviChiu.Len.WISO_ID_MOVI_CHIU;
        wisoDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WISO_DT_INI_EFF, 0);
        position += Len.WISO_DT_INI_EFF;
        wisoDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WISO_DT_END_EFF, 0);
        position += Len.WISO_DT_END_EFF;
        wisoDtIniPer.setWisoDtIniPerFromBuffer(buffer, position);
        position += WisoDtIniPer.Len.WISO_DT_INI_PER;
        wisoDtEndPer.setWisoDtEndPerFromBuffer(buffer, position);
        position += WisoDtEndPer.Len.WISO_DT_END_PER;
        wisoCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WISO_COD_COMP_ANIA, 0);
        position += Len.WISO_COD_COMP_ANIA;
        wisoImpstSost.setWisoImpstSostFromBuffer(buffer, position);
        position += WisoImpstSost.Len.WISO_IMPST_SOST;
        wisoImpbIs.setWisoImpbIsFromBuffer(buffer, position);
        position += WisoImpbIs.Len.WISO_IMPB_IS;
        wisoAlqIs.setWisoAlqIsFromBuffer(buffer, position);
        position += WisoAlqIs.Len.WISO_ALQ_IS;
        wisoCodTrb = MarshalByte.readString(buffer, position, Len.WISO_COD_TRB);
        position += Len.WISO_COD_TRB;
        wisoPrstzLrdAnteIs.setWisoPrstzLrdAnteIsFromBuffer(buffer, position);
        position += WisoPrstzLrdAnteIs.Len.WISO_PRSTZ_LRD_ANTE_IS;
        wisoRisMatNetPrec.setWisoRisMatNetPrecFromBuffer(buffer, position);
        position += WisoRisMatNetPrec.Len.WISO_RIS_MAT_NET_PREC;
        wisoRisMatAnteTax.setWisoRisMatAnteTaxFromBuffer(buffer, position);
        position += WisoRisMatAnteTax.Len.WISO_RIS_MAT_ANTE_TAX;
        wisoRisMatPostTax.setWisoRisMatPostTaxFromBuffer(buffer, position);
        position += WisoRisMatPostTax.Len.WISO_RIS_MAT_POST_TAX;
        wisoPrstzNet.setWisoPrstzNetFromBuffer(buffer, position);
        position += WisoPrstzNet.Len.WISO_PRSTZ_NET;
        wisoPrstzPrec.setWisoPrstzPrecFromBuffer(buffer, position);
        position += WisoPrstzPrec.Len.WISO_PRSTZ_PREC;
        wisoCumPreVers.setWisoCumPreVersFromBuffer(buffer, position);
        position += WisoCumPreVers.Len.WISO_CUM_PRE_VERS;
        wisoTpCalcImpst = MarshalByte.readString(buffer, position, Len.WISO_TP_CALC_IMPST);
        position += Len.WISO_TP_CALC_IMPST;
        wisoImpGiaTassato.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WISO_IMP_GIA_TASSATO, Len.Fract.WISO_IMP_GIA_TASSATO));
        position += Len.WISO_IMP_GIA_TASSATO;
        wisoDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WISO_DS_RIGA, 0);
        position += Len.WISO_DS_RIGA;
        wisoDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wisoDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WISO_DS_VER, 0);
        position += Len.WISO_DS_VER;
        wisoDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WISO_DS_TS_INI_CPTZ, 0);
        position += Len.WISO_DS_TS_INI_CPTZ;
        wisoDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WISO_DS_TS_END_CPTZ, 0);
        position += Len.WISO_DS_TS_END_CPTZ;
        wisoDsUtente = MarshalByte.readString(buffer, position, Len.WISO_DS_UTENTE);
        position += Len.WISO_DS_UTENTE;
        wisoDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wisoIdImpstSost, Len.Int.WISO_ID_IMPST_SOST, 0);
        position += Len.WISO_ID_IMPST_SOST;
        wisoIdOgg.getWisoIdOggAsBuffer(buffer, position);
        position += WisoIdOgg.Len.WISO_ID_OGG;
        MarshalByte.writeString(buffer, position, wisoTpOgg, Len.WISO_TP_OGG);
        position += Len.WISO_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, wisoIdMoviCrz, Len.Int.WISO_ID_MOVI_CRZ, 0);
        position += Len.WISO_ID_MOVI_CRZ;
        wisoIdMoviChiu.getWisoIdMoviChiuAsBuffer(buffer, position);
        position += WisoIdMoviChiu.Len.WISO_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wisoDtIniEff, Len.Int.WISO_DT_INI_EFF, 0);
        position += Len.WISO_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wisoDtEndEff, Len.Int.WISO_DT_END_EFF, 0);
        position += Len.WISO_DT_END_EFF;
        wisoDtIniPer.getWisoDtIniPerAsBuffer(buffer, position);
        position += WisoDtIniPer.Len.WISO_DT_INI_PER;
        wisoDtEndPer.getWisoDtEndPerAsBuffer(buffer, position);
        position += WisoDtEndPer.Len.WISO_DT_END_PER;
        MarshalByte.writeIntAsPacked(buffer, position, wisoCodCompAnia, Len.Int.WISO_COD_COMP_ANIA, 0);
        position += Len.WISO_COD_COMP_ANIA;
        wisoImpstSost.getWisoImpstSostAsBuffer(buffer, position);
        position += WisoImpstSost.Len.WISO_IMPST_SOST;
        wisoImpbIs.getWisoImpbIsAsBuffer(buffer, position);
        position += WisoImpbIs.Len.WISO_IMPB_IS;
        wisoAlqIs.getWisoAlqIsAsBuffer(buffer, position);
        position += WisoAlqIs.Len.WISO_ALQ_IS;
        MarshalByte.writeString(buffer, position, wisoCodTrb, Len.WISO_COD_TRB);
        position += Len.WISO_COD_TRB;
        wisoPrstzLrdAnteIs.getWisoPrstzLrdAnteIsAsBuffer(buffer, position);
        position += WisoPrstzLrdAnteIs.Len.WISO_PRSTZ_LRD_ANTE_IS;
        wisoRisMatNetPrec.getWisoRisMatNetPrecAsBuffer(buffer, position);
        position += WisoRisMatNetPrec.Len.WISO_RIS_MAT_NET_PREC;
        wisoRisMatAnteTax.getWisoRisMatAnteTaxAsBuffer(buffer, position);
        position += WisoRisMatAnteTax.Len.WISO_RIS_MAT_ANTE_TAX;
        wisoRisMatPostTax.getWisoRisMatPostTaxAsBuffer(buffer, position);
        position += WisoRisMatPostTax.Len.WISO_RIS_MAT_POST_TAX;
        wisoPrstzNet.getWisoPrstzNetAsBuffer(buffer, position);
        position += WisoPrstzNet.Len.WISO_PRSTZ_NET;
        wisoPrstzPrec.getWisoPrstzPrecAsBuffer(buffer, position);
        position += WisoPrstzPrec.Len.WISO_PRSTZ_PREC;
        wisoCumPreVers.getWisoCumPreVersAsBuffer(buffer, position);
        position += WisoCumPreVers.Len.WISO_CUM_PRE_VERS;
        MarshalByte.writeString(buffer, position, wisoTpCalcImpst, Len.WISO_TP_CALC_IMPST);
        position += Len.WISO_TP_CALC_IMPST;
        MarshalByte.writeDecimalAsPacked(buffer, position, wisoImpGiaTassato.copy());
        position += Len.WISO_IMP_GIA_TASSATO;
        MarshalByte.writeLongAsPacked(buffer, position, wisoDsRiga, Len.Int.WISO_DS_RIGA, 0);
        position += Len.WISO_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wisoDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wisoDsVer, Len.Int.WISO_DS_VER, 0);
        position += Len.WISO_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wisoDsTsIniCptz, Len.Int.WISO_DS_TS_INI_CPTZ, 0);
        position += Len.WISO_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wisoDsTsEndCptz, Len.Int.WISO_DS_TS_END_CPTZ, 0);
        position += Len.WISO_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wisoDsUtente, Len.WISO_DS_UTENTE);
        position += Len.WISO_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wisoDsStatoElab);
        return buffer;
    }

    public void initDatiSpaces() {
        wisoIdImpstSost = Types.INVALID_INT_VAL;
        wisoIdOgg.initWisoIdOggSpaces();
        wisoTpOgg = "";
        wisoIdMoviCrz = Types.INVALID_INT_VAL;
        wisoIdMoviChiu.initWisoIdMoviChiuSpaces();
        wisoDtIniEff = Types.INVALID_INT_VAL;
        wisoDtEndEff = Types.INVALID_INT_VAL;
        wisoDtIniPer.initWisoDtIniPerSpaces();
        wisoDtEndPer.initWisoDtEndPerSpaces();
        wisoCodCompAnia = Types.INVALID_INT_VAL;
        wisoImpstSost.initWisoImpstSostSpaces();
        wisoImpbIs.initWisoImpbIsSpaces();
        wisoAlqIs.initWisoAlqIsSpaces();
        wisoCodTrb = "";
        wisoPrstzLrdAnteIs.initWisoPrstzLrdAnteIsSpaces();
        wisoRisMatNetPrec.initWisoRisMatNetPrecSpaces();
        wisoRisMatAnteTax.initWisoRisMatAnteTaxSpaces();
        wisoRisMatPostTax.initWisoRisMatPostTaxSpaces();
        wisoPrstzNet.initWisoPrstzNetSpaces();
        wisoPrstzPrec.initWisoPrstzPrecSpaces();
        wisoCumPreVers.initWisoCumPreVersSpaces();
        wisoTpCalcImpst = "";
        wisoImpGiaTassato.setNaN();
        wisoDsRiga = Types.INVALID_LONG_VAL;
        wisoDsOperSql = Types.SPACE_CHAR;
        wisoDsVer = Types.INVALID_INT_VAL;
        wisoDsTsIniCptz = Types.INVALID_LONG_VAL;
        wisoDsTsEndCptz = Types.INVALID_LONG_VAL;
        wisoDsUtente = "";
        wisoDsStatoElab = Types.SPACE_CHAR;
    }

    public void setWisoIdImpstSost(int wisoIdImpstSost) {
        this.wisoIdImpstSost = wisoIdImpstSost;
    }

    public int getWisoIdImpstSost() {
        return this.wisoIdImpstSost;
    }

    public void setWisoTpOgg(String wisoTpOgg) {
        this.wisoTpOgg = Functions.subString(wisoTpOgg, Len.WISO_TP_OGG);
    }

    public String getWisoTpOgg() {
        return this.wisoTpOgg;
    }

    public void setWisoIdMoviCrz(int wisoIdMoviCrz) {
        this.wisoIdMoviCrz = wisoIdMoviCrz;
    }

    public int getWisoIdMoviCrz() {
        return this.wisoIdMoviCrz;
    }

    public void setWisoDtIniEff(int wisoDtIniEff) {
        this.wisoDtIniEff = wisoDtIniEff;
    }

    public int getWisoDtIniEff() {
        return this.wisoDtIniEff;
    }

    public void setWisoDtEndEff(int wisoDtEndEff) {
        this.wisoDtEndEff = wisoDtEndEff;
    }

    public int getWisoDtEndEff() {
        return this.wisoDtEndEff;
    }

    public void setWisoCodCompAnia(int wisoCodCompAnia) {
        this.wisoCodCompAnia = wisoCodCompAnia;
    }

    public int getWisoCodCompAnia() {
        return this.wisoCodCompAnia;
    }

    public void setWisoCodTrb(String wisoCodTrb) {
        this.wisoCodTrb = Functions.subString(wisoCodTrb, Len.WISO_COD_TRB);
    }

    public String getWisoCodTrb() {
        return this.wisoCodTrb;
    }

    public void setWisoTpCalcImpst(String wisoTpCalcImpst) {
        this.wisoTpCalcImpst = Functions.subString(wisoTpCalcImpst, Len.WISO_TP_CALC_IMPST);
    }

    public String getWisoTpCalcImpst() {
        return this.wisoTpCalcImpst;
    }

    public void setWisoImpGiaTassato(AfDecimal wisoImpGiaTassato) {
        this.wisoImpGiaTassato.assign(wisoImpGiaTassato);
    }

    public AfDecimal getWisoImpGiaTassato() {
        return this.wisoImpGiaTassato.copy();
    }

    public void setWisoDsRiga(long wisoDsRiga) {
        this.wisoDsRiga = wisoDsRiga;
    }

    public long getWisoDsRiga() {
        return this.wisoDsRiga;
    }

    public void setWisoDsOperSql(char wisoDsOperSql) {
        this.wisoDsOperSql = wisoDsOperSql;
    }

    public char getWisoDsOperSql() {
        return this.wisoDsOperSql;
    }

    public void setWisoDsVer(int wisoDsVer) {
        this.wisoDsVer = wisoDsVer;
    }

    public int getWisoDsVer() {
        return this.wisoDsVer;
    }

    public void setWisoDsTsIniCptz(long wisoDsTsIniCptz) {
        this.wisoDsTsIniCptz = wisoDsTsIniCptz;
    }

    public long getWisoDsTsIniCptz() {
        return this.wisoDsTsIniCptz;
    }

    public void setWisoDsTsEndCptz(long wisoDsTsEndCptz) {
        this.wisoDsTsEndCptz = wisoDsTsEndCptz;
    }

    public long getWisoDsTsEndCptz() {
        return this.wisoDsTsEndCptz;
    }

    public void setWisoDsUtente(String wisoDsUtente) {
        this.wisoDsUtente = Functions.subString(wisoDsUtente, Len.WISO_DS_UTENTE);
    }

    public String getWisoDsUtente() {
        return this.wisoDsUtente;
    }

    public void setWisoDsStatoElab(char wisoDsStatoElab) {
        this.wisoDsStatoElab = wisoDsStatoElab;
    }

    public char getWisoDsStatoElab() {
        return this.wisoDsStatoElab;
    }

    public WisoAlqIs getWisoAlqIs() {
        return wisoAlqIs;
    }

    public WisoCumPreVers getWisoCumPreVers() {
        return wisoCumPreVers;
    }

    public WisoDtEndPer getWisoDtEndPer() {
        return wisoDtEndPer;
    }

    public WisoDtIniPer getWisoDtIniPer() {
        return wisoDtIniPer;
    }

    public WisoIdMoviChiu getWisoIdMoviChiu() {
        return wisoIdMoviChiu;
    }

    public WisoIdOgg getWisoIdOgg() {
        return wisoIdOgg;
    }

    public WisoImpbIs getWisoImpbIs() {
        return wisoImpbIs;
    }

    public WisoImpstSost getWisoImpstSost() {
        return wisoImpstSost;
    }

    public WisoPrstzLrdAnteIs getWisoPrstzLrdAnteIs() {
        return wisoPrstzLrdAnteIs;
    }

    public WisoPrstzNet getWisoPrstzNet() {
        return wisoPrstzNet;
    }

    public WisoPrstzPrec getWisoPrstzPrec() {
        return wisoPrstzPrec;
    }

    public WisoRisMatAnteTax getWisoRisMatAnteTax() {
        return wisoRisMatAnteTax;
    }

    public WisoRisMatNetPrec getWisoRisMatNetPrec() {
        return wisoRisMatNetPrec;
    }

    public WisoRisMatPostTax getWisoRisMatPostTax() {
        return wisoRisMatPostTax;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_ID_IMPST_SOST = 5;
        public static final int WISO_TP_OGG = 2;
        public static final int WISO_ID_MOVI_CRZ = 5;
        public static final int WISO_DT_INI_EFF = 5;
        public static final int WISO_DT_END_EFF = 5;
        public static final int WISO_COD_COMP_ANIA = 3;
        public static final int WISO_COD_TRB = 20;
        public static final int WISO_TP_CALC_IMPST = 2;
        public static final int WISO_IMP_GIA_TASSATO = 8;
        public static final int WISO_DS_RIGA = 6;
        public static final int WISO_DS_OPER_SQL = 1;
        public static final int WISO_DS_VER = 5;
        public static final int WISO_DS_TS_INI_CPTZ = 10;
        public static final int WISO_DS_TS_END_CPTZ = 10;
        public static final int WISO_DS_UTENTE = 20;
        public static final int WISO_DS_STATO_ELAB = 1;
        public static final int DATI = WISO_ID_IMPST_SOST + WisoIdOgg.Len.WISO_ID_OGG + WISO_TP_OGG + WISO_ID_MOVI_CRZ + WisoIdMoviChiu.Len.WISO_ID_MOVI_CHIU + WISO_DT_INI_EFF + WISO_DT_END_EFF + WisoDtIniPer.Len.WISO_DT_INI_PER + WisoDtEndPer.Len.WISO_DT_END_PER + WISO_COD_COMP_ANIA + WisoImpstSost.Len.WISO_IMPST_SOST + WisoImpbIs.Len.WISO_IMPB_IS + WisoAlqIs.Len.WISO_ALQ_IS + WISO_COD_TRB + WisoPrstzLrdAnteIs.Len.WISO_PRSTZ_LRD_ANTE_IS + WisoRisMatNetPrec.Len.WISO_RIS_MAT_NET_PREC + WisoRisMatAnteTax.Len.WISO_RIS_MAT_ANTE_TAX + WisoRisMatPostTax.Len.WISO_RIS_MAT_POST_TAX + WisoPrstzNet.Len.WISO_PRSTZ_NET + WisoPrstzPrec.Len.WISO_PRSTZ_PREC + WisoCumPreVers.Len.WISO_CUM_PRE_VERS + WISO_TP_CALC_IMPST + WISO_IMP_GIA_TASSATO + WISO_DS_RIGA + WISO_DS_OPER_SQL + WISO_DS_VER + WISO_DS_TS_INI_CPTZ + WISO_DS_TS_END_CPTZ + WISO_DS_UTENTE + WISO_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_ID_IMPST_SOST = 9;
            public static final int WISO_ID_MOVI_CRZ = 9;
            public static final int WISO_DT_INI_EFF = 8;
            public static final int WISO_DT_END_EFF = 8;
            public static final int WISO_COD_COMP_ANIA = 5;
            public static final int WISO_IMP_GIA_TASSATO = 12;
            public static final int WISO_DS_RIGA = 10;
            public static final int WISO_DS_VER = 9;
            public static final int WISO_DS_TS_INI_CPTZ = 18;
            public static final int WISO_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WISO_IMP_GIA_TASSATO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
