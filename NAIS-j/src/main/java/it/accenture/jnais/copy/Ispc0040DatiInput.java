package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: ISPC0040-DATI-INPUT<br>
 * Variable: ISPC0040-DATI-INPUT from copybook ISPC0040<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ispc0040DatiInput implements ICopyable<Ispc0040DatiInput> {

    //==== PROPERTIES ====
    //Original name: ISPC0040-COD-COMPAGNIA
    private String codCompagnia = DefaultValues.stringVal(Len.COD_COMPAGNIA);
    //Original name: ISPC0040-COD-PRODOTTO
    private String codProdotto = DefaultValues.stringVal(Len.COD_PRODOTTO);
    //Original name: ISPC0040-COD-CONVENZIONE
    private String codConvenzione = DefaultValues.stringVal(Len.COD_CONVENZIONE);
    //Original name: ISPC0040-DATA-INIZ-VALID-CONV
    private String dataInizValidConv = DefaultValues.stringVal(Len.DATA_INIZ_VALID_CONV);
    //Original name: ISPC0040-DATA-RIFERIMENTO
    private String dataRiferimento = DefaultValues.stringVal(Len.DATA_RIFERIMENTO);
    //Original name: ISPC0040-DATA-EMISSIONE
    private String dataEmissione = DefaultValues.stringVal(Len.DATA_EMISSIONE);
    //Original name: ISPC0040-DATA-PROPOSTA
    private String dataProposta = DefaultValues.stringVal(Len.DATA_PROPOSTA);
    //Original name: ISPC0040-LIVELLO-UTENTE
    private String livelloUtente = DefaultValues.stringVal(Len.LIVELLO_UTENTE);
    //Original name: ISPC0040-FUNZIONALITA
    private String funzionalita = DefaultValues.stringVal(Len.FUNZIONALITA);
    //Original name: ISPC0040-SESSION-ID
    private String sessionId = DefaultValues.stringVal(Len.SESSION_ID);

    //==== CONSTRUCTORS ====
    public Ispc0040DatiInput() {
    }

    public Ispc0040DatiInput(Ispc0040DatiInput datiInput) {
        this();
        this.codCompagnia = datiInput.codCompagnia;
        this.codProdotto = datiInput.codProdotto;
        this.codConvenzione = datiInput.codConvenzione;
        this.dataInizValidConv = datiInput.dataInizValidConv;
        this.dataRiferimento = datiInput.dataRiferimento;
        this.dataEmissione = datiInput.dataEmissione;
        this.dataProposta = datiInput.dataProposta;
        this.livelloUtente = datiInput.livelloUtente;
        this.funzionalita = datiInput.funzionalita;
        this.sessionId = datiInput.sessionId;
    }

    //==== METHODS ====
    public void setIspc0040DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        codCompagnia = MarshalByte.readFixedString(buffer, position, Len.COD_COMPAGNIA);
        position += Len.COD_COMPAGNIA;
        codProdotto = MarshalByte.readString(buffer, position, Len.COD_PRODOTTO);
        position += Len.COD_PRODOTTO;
        codConvenzione = MarshalByte.readString(buffer, position, Len.COD_CONVENZIONE);
        position += Len.COD_CONVENZIONE;
        dataInizValidConv = MarshalByte.readString(buffer, position, Len.DATA_INIZ_VALID_CONV);
        position += Len.DATA_INIZ_VALID_CONV;
        dataRiferimento = MarshalByte.readString(buffer, position, Len.DATA_RIFERIMENTO);
        position += Len.DATA_RIFERIMENTO;
        dataEmissione = MarshalByte.readString(buffer, position, Len.DATA_EMISSIONE);
        position += Len.DATA_EMISSIONE;
        dataProposta = MarshalByte.readString(buffer, position, Len.DATA_PROPOSTA);
        position += Len.DATA_PROPOSTA;
        livelloUtente = MarshalByte.readFixedString(buffer, position, Len.LIVELLO_UTENTE);
        position += Len.LIVELLO_UTENTE;
        funzionalita = MarshalByte.readFixedString(buffer, position, Len.FUNZIONALITA);
        position += Len.FUNZIONALITA;
        sessionId = MarshalByte.readString(buffer, position, Len.SESSION_ID);
    }

    public byte[] getIspc0040DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codCompagnia, Len.COD_COMPAGNIA);
        position += Len.COD_COMPAGNIA;
        MarshalByte.writeString(buffer, position, codProdotto, Len.COD_PRODOTTO);
        position += Len.COD_PRODOTTO;
        MarshalByte.writeString(buffer, position, codConvenzione, Len.COD_CONVENZIONE);
        position += Len.COD_CONVENZIONE;
        MarshalByte.writeString(buffer, position, dataInizValidConv, Len.DATA_INIZ_VALID_CONV);
        position += Len.DATA_INIZ_VALID_CONV;
        MarshalByte.writeString(buffer, position, dataRiferimento, Len.DATA_RIFERIMENTO);
        position += Len.DATA_RIFERIMENTO;
        MarshalByte.writeString(buffer, position, dataEmissione, Len.DATA_EMISSIONE);
        position += Len.DATA_EMISSIONE;
        MarshalByte.writeString(buffer, position, dataProposta, Len.DATA_PROPOSTA);
        position += Len.DATA_PROPOSTA;
        MarshalByte.writeString(buffer, position, livelloUtente, Len.LIVELLO_UTENTE);
        position += Len.LIVELLO_UTENTE;
        MarshalByte.writeString(buffer, position, funzionalita, Len.FUNZIONALITA);
        position += Len.FUNZIONALITA;
        MarshalByte.writeString(buffer, position, sessionId, Len.SESSION_ID);
        return buffer;
    }

    public void setCodCompagniaFormatted(String codCompagnia) {
        this.codCompagnia = Trunc.toUnsignedNumeric(codCompagnia, Len.COD_COMPAGNIA);
    }

    public int getCodCompagnia() {
        return NumericDisplay.asInt(this.codCompagnia);
    }

    public String getCodCompagniaFormatted() {
        return this.codCompagnia;
    }

    public void setCodProdotto(String codProdotto) {
        this.codProdotto = Functions.subString(codProdotto, Len.COD_PRODOTTO);
    }

    public String getCodProdotto() {
        return this.codProdotto;
    }

    public void setCodConvenzione(String codConvenzione) {
        this.codConvenzione = Functions.subString(codConvenzione, Len.COD_CONVENZIONE);
    }

    public String getCodConvenzione() {
        return this.codConvenzione;
    }

    public void setDataInizValidConv(String dataInizValidConv) {
        this.dataInizValidConv = Functions.subString(dataInizValidConv, Len.DATA_INIZ_VALID_CONV);
    }

    public String getDataInizValidConv() {
        return this.dataInizValidConv;
    }

    public void setDataRiferimento(String dataRiferimento) {
        this.dataRiferimento = Functions.subString(dataRiferimento, Len.DATA_RIFERIMENTO);
    }

    public String getDataRiferimento() {
        return this.dataRiferimento;
    }

    public void setDataEmissione(String dataEmissione) {
        this.dataEmissione = Functions.subString(dataEmissione, Len.DATA_EMISSIONE);
    }

    public String getDataEmissione() {
        return this.dataEmissione;
    }

    public void setDataProposta(String dataProposta) {
        this.dataProposta = Functions.subString(dataProposta, Len.DATA_PROPOSTA);
    }

    public String getDataProposta() {
        return this.dataProposta;
    }

    public void setIspc0040LivelloUtente(short ispc0040LivelloUtente) {
        this.livelloUtente = NumericDisplay.asString(ispc0040LivelloUtente, Len.LIVELLO_UTENTE);
    }

    public void setLivelloUtenteFormatted(String livelloUtente) {
        this.livelloUtente = Trunc.toUnsignedNumeric(livelloUtente, Len.LIVELLO_UTENTE);
    }

    public short getLivelloUtente() {
        return NumericDisplay.asShort(this.livelloUtente);
    }

    public String getLivelloUtenteFormatted() {
        return this.livelloUtente;
    }

    public void setFunzionalitaFormatted(String funzionalita) {
        this.funzionalita = Trunc.toUnsignedNumeric(funzionalita, Len.FUNZIONALITA);
    }

    public int getFunzionalita() {
        return NumericDisplay.asInt(this.funzionalita);
    }

    public String getFunzionalitaFormatted() {
        return this.funzionalita;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = Functions.subString(sessionId, Len.SESSION_ID);
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public Ispc0040DatiInput copy() {
        return new Ispc0040DatiInput(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_COMPAGNIA = 5;
        public static final int COD_PRODOTTO = 12;
        public static final int COD_CONVENZIONE = 12;
        public static final int DATA_INIZ_VALID_CONV = 8;
        public static final int DATA_RIFERIMENTO = 8;
        public static final int DATA_EMISSIONE = 8;
        public static final int DATA_PROPOSTA = 8;
        public static final int LIVELLO_UTENTE = 2;
        public static final int FUNZIONALITA = 5;
        public static final int SESSION_ID = 20;
        public static final int ISPC0040_DATI_INPUT = COD_COMPAGNIA + COD_PRODOTTO + COD_CONVENZIONE + DATA_INIZ_VALID_CONV + DATA_RIFERIMENTO + DATA_EMISSIONE + DATA_PROPOSTA + LIVELLO_UTENTE + FUNZIONALITA + SESSION_ID;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
