package it.accenture.jnais.copy;

/**Original name: IDSV0503-TIPO-FORMATO<br>
 * Variable: IDSV0503-TIPO-FORMATO from copybook IDSV0503<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Idsv0503TipoFormato {

    //==== PROPERTIES ====
    //Original name: IDSV0503-IMPORTO
    private char importo = 'I';
    //Original name: IDSV0503-NUMERICO
    private char numerico = 'N';
    //Original name: IDSV0503-MILLESIMI
    private char millesimi = 'M';
    //Original name: IDSV0503-PERCENTUALE
    private char percentuale = 'P';
    //Original name: IDSV0503-DT
    private char dt = 'D';
    //Original name: IDSV0503-STRINGA
    private char stringa = 'S';
    //Original name: IDSV0503-TASSO
    private char tasso = 'A';
    //Original name: IDSV0503-LISTA-IMPORTO
    private char listaImporto = 'R';
    //Original name: IDSV0503-LISTA-NUMERICO
    private char listaNumerico = 'H';
    //Original name: IDSV0503-LISTA-MILLESIMI
    private char listaMillesimi = 'Z';
    //Original name: IDSV0503-LISTA-PERCENTUALE
    private char listaPercentuale = 'O';
    //Original name: IDSV0503-LISTA-DT
    private char listaDt = 'C';
    //Original name: IDSV0503-LISTA-STRINGA
    private char listaStringa = 'L';
    //Original name: IDSV0503-LISTA-TASSO
    private char listaTasso = 'T';
    //Original name: IDSV0503-LISTA-MISTA
    private char listaMista = 'X';

    //==== METHODS ====
    public char getImporto() {
        return this.importo;
    }

    public char getNumerico() {
        return this.numerico;
    }

    public char getMillesimi() {
        return this.millesimi;
    }

    public char getPercentuale() {
        return this.percentuale;
    }

    public char getDt() {
        return this.dt;
    }

    public char getStringa() {
        return this.stringa;
    }

    public char getTasso() {
        return this.tasso;
    }

    public char getListaImporto() {
        return this.listaImporto;
    }

    public char getListaNumerico() {
        return this.listaNumerico;
    }

    public char getListaMillesimi() {
        return this.listaMillesimi;
    }

    public char getListaPercentuale() {
        return this.listaPercentuale;
    }

    public char getListaDt() {
        return this.listaDt;
    }

    public char getListaStringa() {
        return this.listaStringa;
    }

    public char getListaTasso() {
        return this.listaTasso;
    }

    public char getListaMista() {
        return this.listaMista;
    }
}
