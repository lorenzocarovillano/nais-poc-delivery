package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.OcoCarAcq;
import it.accenture.jnais.ws.redefines.OcoDtDecor;
import it.accenture.jnais.ws.redefines.OcoDtScad;
import it.accenture.jnais.ws.redefines.OcoDtUltPrePag;
import it.accenture.jnais.ws.redefines.OcoIdMoviChiu;
import it.accenture.jnais.ws.redefines.OcoImpCollg;
import it.accenture.jnais.ws.redefines.OcoImpReinvst;
import it.accenture.jnais.ws.redefines.OcoImpTrasf;
import it.accenture.jnais.ws.redefines.OcoImpTrasferito;
import it.accenture.jnais.ws.redefines.OcoPcPreTrasferito;
import it.accenture.jnais.ws.redefines.OcoPcReinvstRilievi;
import it.accenture.jnais.ws.redefines.OcoPre1aAnnualita;
import it.accenture.jnais.ws.redefines.OcoPrePerTrasf;
import it.accenture.jnais.ws.redefines.OcoRecProv;
import it.accenture.jnais.ws.redefines.OcoRisMat;
import it.accenture.jnais.ws.redefines.OcoRisZil;
import it.accenture.jnais.ws.redefines.OcoTotPre;

/**Original name: OGG-COLLG<br>
 * Variable: OGG-COLLG from copybook IDBVOCO1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class OggCollg {

    //==== PROPERTIES ====
    //Original name: OCO-ID-OGG-COLLG
    private int ocoIdOggCollg = DefaultValues.INT_VAL;
    //Original name: OCO-ID-OGG-COINV
    private int ocoIdOggCoinv = DefaultValues.INT_VAL;
    //Original name: OCO-TP-OGG-COINV
    private String ocoTpOggCoinv = DefaultValues.stringVal(Len.OCO_TP_OGG_COINV);
    //Original name: OCO-ID-OGG-DER
    private int ocoIdOggDer = DefaultValues.INT_VAL;
    //Original name: OCO-TP-OGG-DER
    private String ocoTpOggDer = DefaultValues.stringVal(Len.OCO_TP_OGG_DER);
    //Original name: OCO-IB-RIFTO-ESTNO
    private String ocoIbRiftoEstno = DefaultValues.stringVal(Len.OCO_IB_RIFTO_ESTNO);
    //Original name: OCO-ID-MOVI-CRZ
    private int ocoIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: OCO-ID-MOVI-CHIU
    private OcoIdMoviChiu ocoIdMoviChiu = new OcoIdMoviChiu();
    //Original name: OCO-DT-INI-EFF
    private int ocoDtIniEff = DefaultValues.INT_VAL;
    //Original name: OCO-DT-END-EFF
    private int ocoDtEndEff = DefaultValues.INT_VAL;
    //Original name: OCO-COD-COMP-ANIA
    private int ocoCodCompAnia = DefaultValues.INT_VAL;
    //Original name: OCO-COD-PROD
    private String ocoCodProd = DefaultValues.stringVal(Len.OCO_COD_PROD);
    //Original name: OCO-DT-SCAD
    private OcoDtScad ocoDtScad = new OcoDtScad();
    //Original name: OCO-TP-COLLGM
    private String ocoTpCollgm = DefaultValues.stringVal(Len.OCO_TP_COLLGM);
    //Original name: OCO-TP-TRASF
    private String ocoTpTrasf = DefaultValues.stringVal(Len.OCO_TP_TRASF);
    //Original name: OCO-REC-PROV
    private OcoRecProv ocoRecProv = new OcoRecProv();
    //Original name: OCO-DT-DECOR
    private OcoDtDecor ocoDtDecor = new OcoDtDecor();
    //Original name: OCO-DT-ULT-PRE-PAG
    private OcoDtUltPrePag ocoDtUltPrePag = new OcoDtUltPrePag();
    //Original name: OCO-TOT-PRE
    private OcoTotPre ocoTotPre = new OcoTotPre();
    //Original name: OCO-RIS-MAT
    private OcoRisMat ocoRisMat = new OcoRisMat();
    //Original name: OCO-RIS-ZIL
    private OcoRisZil ocoRisZil = new OcoRisZil();
    //Original name: OCO-IMP-TRASF
    private OcoImpTrasf ocoImpTrasf = new OcoImpTrasf();
    //Original name: OCO-IMP-REINVST
    private OcoImpReinvst ocoImpReinvst = new OcoImpReinvst();
    //Original name: OCO-PC-REINVST-RILIEVI
    private OcoPcReinvstRilievi ocoPcReinvstRilievi = new OcoPcReinvstRilievi();
    //Original name: OCO-IB-2O-RIFTO-ESTNO
    private String ocoIb2oRiftoEstno = DefaultValues.stringVal(Len.OCO_IB2O_RIFTO_ESTNO);
    //Original name: OCO-IND-LIQ-AGG-MAN
    private char ocoIndLiqAggMan = DefaultValues.CHAR_VAL;
    //Original name: OCO-DS-RIGA
    private long ocoDsRiga = DefaultValues.LONG_VAL;
    //Original name: OCO-DS-OPER-SQL
    private char ocoDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: OCO-DS-VER
    private int ocoDsVer = DefaultValues.INT_VAL;
    //Original name: OCO-DS-TS-INI-CPTZ
    private long ocoDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: OCO-DS-TS-END-CPTZ
    private long ocoDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: OCO-DS-UTENTE
    private String ocoDsUtente = DefaultValues.stringVal(Len.OCO_DS_UTENTE);
    //Original name: OCO-DS-STATO-ELAB
    private char ocoDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: OCO-IMP-COLLG
    private OcoImpCollg ocoImpCollg = new OcoImpCollg();
    //Original name: OCO-PRE-PER-TRASF
    private OcoPrePerTrasf ocoPrePerTrasf = new OcoPrePerTrasf();
    //Original name: OCO-CAR-ACQ
    private OcoCarAcq ocoCarAcq = new OcoCarAcq();
    //Original name: OCO-PRE-1A-ANNUALITA
    private OcoPre1aAnnualita ocoPre1aAnnualita = new OcoPre1aAnnualita();
    //Original name: OCO-IMP-TRASFERITO
    private OcoImpTrasferito ocoImpTrasferito = new OcoImpTrasferito();
    //Original name: OCO-TP-MOD-ABBINAMENTO
    private String ocoTpModAbbinamento = DefaultValues.stringVal(Len.OCO_TP_MOD_ABBINAMENTO);
    //Original name: OCO-PC-PRE-TRASFERITO
    private OcoPcPreTrasferito ocoPcPreTrasferito = new OcoPcPreTrasferito();

    //==== METHODS ====
    public void setOggCollgFormatted(String data) {
        byte[] buffer = new byte[Len.OGG_COLLG];
        MarshalByte.writeString(buffer, 1, data, Len.OGG_COLLG);
        setOggCollgBytes(buffer, 1);
    }

    public void setOggCollgBytes(byte[] buffer, int offset) {
        int position = offset;
        ocoIdOggCollg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCO_ID_OGG_COLLG, 0);
        position += Len.OCO_ID_OGG_COLLG;
        ocoIdOggCoinv = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCO_ID_OGG_COINV, 0);
        position += Len.OCO_ID_OGG_COINV;
        ocoTpOggCoinv = MarshalByte.readString(buffer, position, Len.OCO_TP_OGG_COINV);
        position += Len.OCO_TP_OGG_COINV;
        ocoIdOggDer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCO_ID_OGG_DER, 0);
        position += Len.OCO_ID_OGG_DER;
        ocoTpOggDer = MarshalByte.readString(buffer, position, Len.OCO_TP_OGG_DER);
        position += Len.OCO_TP_OGG_DER;
        ocoIbRiftoEstno = MarshalByte.readString(buffer, position, Len.OCO_IB_RIFTO_ESTNO);
        position += Len.OCO_IB_RIFTO_ESTNO;
        ocoIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCO_ID_MOVI_CRZ, 0);
        position += Len.OCO_ID_MOVI_CRZ;
        ocoIdMoviChiu.setOcoIdMoviChiuFromBuffer(buffer, position);
        position += OcoIdMoviChiu.Len.OCO_ID_MOVI_CHIU;
        ocoDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCO_DT_INI_EFF, 0);
        position += Len.OCO_DT_INI_EFF;
        ocoDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCO_DT_END_EFF, 0);
        position += Len.OCO_DT_END_EFF;
        ocoCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCO_COD_COMP_ANIA, 0);
        position += Len.OCO_COD_COMP_ANIA;
        ocoCodProd = MarshalByte.readString(buffer, position, Len.OCO_COD_PROD);
        position += Len.OCO_COD_PROD;
        ocoDtScad.setOcoDtScadFromBuffer(buffer, position);
        position += OcoDtScad.Len.OCO_DT_SCAD;
        ocoTpCollgm = MarshalByte.readString(buffer, position, Len.OCO_TP_COLLGM);
        position += Len.OCO_TP_COLLGM;
        ocoTpTrasf = MarshalByte.readString(buffer, position, Len.OCO_TP_TRASF);
        position += Len.OCO_TP_TRASF;
        ocoRecProv.setOcoRecProvFromBuffer(buffer, position);
        position += OcoRecProv.Len.OCO_REC_PROV;
        ocoDtDecor.setOcoDtDecorFromBuffer(buffer, position);
        position += OcoDtDecor.Len.OCO_DT_DECOR;
        ocoDtUltPrePag.setOcoDtUltPrePagFromBuffer(buffer, position);
        position += OcoDtUltPrePag.Len.OCO_DT_ULT_PRE_PAG;
        ocoTotPre.setOcoTotPreFromBuffer(buffer, position);
        position += OcoTotPre.Len.OCO_TOT_PRE;
        ocoRisMat.setOcoRisMatFromBuffer(buffer, position);
        position += OcoRisMat.Len.OCO_RIS_MAT;
        ocoRisZil.setOcoRisZilFromBuffer(buffer, position);
        position += OcoRisZil.Len.OCO_RIS_ZIL;
        ocoImpTrasf.setOcoImpTrasfFromBuffer(buffer, position);
        position += OcoImpTrasf.Len.OCO_IMP_TRASF;
        ocoImpReinvst.setOcoImpReinvstFromBuffer(buffer, position);
        position += OcoImpReinvst.Len.OCO_IMP_REINVST;
        ocoPcReinvstRilievi.setOcoPcReinvstRilieviFromBuffer(buffer, position);
        position += OcoPcReinvstRilievi.Len.OCO_PC_REINVST_RILIEVI;
        ocoIb2oRiftoEstno = MarshalByte.readString(buffer, position, Len.OCO_IB2O_RIFTO_ESTNO);
        position += Len.OCO_IB2O_RIFTO_ESTNO;
        ocoIndLiqAggMan = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ocoDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.OCO_DS_RIGA, 0);
        position += Len.OCO_DS_RIGA;
        ocoDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ocoDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCO_DS_VER, 0);
        position += Len.OCO_DS_VER;
        ocoDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.OCO_DS_TS_INI_CPTZ, 0);
        position += Len.OCO_DS_TS_INI_CPTZ;
        ocoDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.OCO_DS_TS_END_CPTZ, 0);
        position += Len.OCO_DS_TS_END_CPTZ;
        ocoDsUtente = MarshalByte.readString(buffer, position, Len.OCO_DS_UTENTE);
        position += Len.OCO_DS_UTENTE;
        ocoDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ocoImpCollg.setOcoImpCollgFromBuffer(buffer, position);
        position += OcoImpCollg.Len.OCO_IMP_COLLG;
        ocoPrePerTrasf.setOcoPrePerTrasfFromBuffer(buffer, position);
        position += OcoPrePerTrasf.Len.OCO_PRE_PER_TRASF;
        ocoCarAcq.setOcoCarAcqFromBuffer(buffer, position);
        position += OcoCarAcq.Len.OCO_CAR_ACQ;
        ocoPre1aAnnualita.setOcoPre1aAnnualitaFromBuffer(buffer, position);
        position += OcoPre1aAnnualita.Len.OCO_PRE1A_ANNUALITA;
        ocoImpTrasferito.setOcoImpTrasferitoFromBuffer(buffer, position);
        position += OcoImpTrasferito.Len.OCO_IMP_TRASFERITO;
        ocoTpModAbbinamento = MarshalByte.readString(buffer, position, Len.OCO_TP_MOD_ABBINAMENTO);
        position += Len.OCO_TP_MOD_ABBINAMENTO;
        ocoPcPreTrasferito.setOcoPcPreTrasferitoFromBuffer(buffer, position);
    }

    public void setOcoIdOggCollg(int ocoIdOggCollg) {
        this.ocoIdOggCollg = ocoIdOggCollg;
    }

    public int getOcoIdOggCollg() {
        return this.ocoIdOggCollg;
    }

    public void setOcoIdOggCoinv(int ocoIdOggCoinv) {
        this.ocoIdOggCoinv = ocoIdOggCoinv;
    }

    public int getOcoIdOggCoinv() {
        return this.ocoIdOggCoinv;
    }

    public void setOcoTpOggCoinv(String ocoTpOggCoinv) {
        this.ocoTpOggCoinv = Functions.subString(ocoTpOggCoinv, Len.OCO_TP_OGG_COINV);
    }

    public String getOcoTpOggCoinv() {
        return this.ocoTpOggCoinv;
    }

    public void setOcoIdOggDer(int ocoIdOggDer) {
        this.ocoIdOggDer = ocoIdOggDer;
    }

    public int getOcoIdOggDer() {
        return this.ocoIdOggDer;
    }

    public void setOcoTpOggDer(String ocoTpOggDer) {
        this.ocoTpOggDer = Functions.subString(ocoTpOggDer, Len.OCO_TP_OGG_DER);
    }

    public String getOcoTpOggDer() {
        return this.ocoTpOggDer;
    }

    public void setOcoIbRiftoEstno(String ocoIbRiftoEstno) {
        this.ocoIbRiftoEstno = Functions.subString(ocoIbRiftoEstno, Len.OCO_IB_RIFTO_ESTNO);
    }

    public String getOcoIbRiftoEstno() {
        return this.ocoIbRiftoEstno;
    }

    public void setOcoIdMoviCrz(int ocoIdMoviCrz) {
        this.ocoIdMoviCrz = ocoIdMoviCrz;
    }

    public int getOcoIdMoviCrz() {
        return this.ocoIdMoviCrz;
    }

    public void setOcoDtIniEff(int ocoDtIniEff) {
        this.ocoDtIniEff = ocoDtIniEff;
    }

    public int getOcoDtIniEff() {
        return this.ocoDtIniEff;
    }

    public void setOcoDtEndEff(int ocoDtEndEff) {
        this.ocoDtEndEff = ocoDtEndEff;
    }

    public int getOcoDtEndEff() {
        return this.ocoDtEndEff;
    }

    public void setOcoCodCompAnia(int ocoCodCompAnia) {
        this.ocoCodCompAnia = ocoCodCompAnia;
    }

    public int getOcoCodCompAnia() {
        return this.ocoCodCompAnia;
    }

    public void setOcoCodProd(String ocoCodProd) {
        this.ocoCodProd = Functions.subString(ocoCodProd, Len.OCO_COD_PROD);
    }

    public String getOcoCodProd() {
        return this.ocoCodProd;
    }

    public void setOcoTpCollgm(String ocoTpCollgm) {
        this.ocoTpCollgm = Functions.subString(ocoTpCollgm, Len.OCO_TP_COLLGM);
    }

    public String getOcoTpCollgm() {
        return this.ocoTpCollgm;
    }

    public void setOcoTpTrasf(String ocoTpTrasf) {
        this.ocoTpTrasf = Functions.subString(ocoTpTrasf, Len.OCO_TP_TRASF);
    }

    public String getOcoTpTrasf() {
        return this.ocoTpTrasf;
    }

    public String getOcoTpTrasfFormatted() {
        return Functions.padBlanks(getOcoTpTrasf(), Len.OCO_TP_TRASF);
    }

    public void setOcoIb2oRiftoEstno(String ocoIb2oRiftoEstno) {
        this.ocoIb2oRiftoEstno = Functions.subString(ocoIb2oRiftoEstno, Len.OCO_IB2O_RIFTO_ESTNO);
    }

    public String getOcoIb2oRiftoEstno() {
        return this.ocoIb2oRiftoEstno;
    }

    public void setOcoIndLiqAggMan(char ocoIndLiqAggMan) {
        this.ocoIndLiqAggMan = ocoIndLiqAggMan;
    }

    public char getOcoIndLiqAggMan() {
        return this.ocoIndLiqAggMan;
    }

    public void setOcoDsRiga(long ocoDsRiga) {
        this.ocoDsRiga = ocoDsRiga;
    }

    public long getOcoDsRiga() {
        return this.ocoDsRiga;
    }

    public void setOcoDsOperSql(char ocoDsOperSql) {
        this.ocoDsOperSql = ocoDsOperSql;
    }

    public char getOcoDsOperSql() {
        return this.ocoDsOperSql;
    }

    public void setOcoDsVer(int ocoDsVer) {
        this.ocoDsVer = ocoDsVer;
    }

    public int getOcoDsVer() {
        return this.ocoDsVer;
    }

    public void setOcoDsTsIniCptz(long ocoDsTsIniCptz) {
        this.ocoDsTsIniCptz = ocoDsTsIniCptz;
    }

    public long getOcoDsTsIniCptz() {
        return this.ocoDsTsIniCptz;
    }

    public void setOcoDsTsEndCptz(long ocoDsTsEndCptz) {
        this.ocoDsTsEndCptz = ocoDsTsEndCptz;
    }

    public long getOcoDsTsEndCptz() {
        return this.ocoDsTsEndCptz;
    }

    public void setOcoDsUtente(String ocoDsUtente) {
        this.ocoDsUtente = Functions.subString(ocoDsUtente, Len.OCO_DS_UTENTE);
    }

    public String getOcoDsUtente() {
        return this.ocoDsUtente;
    }

    public void setOcoDsStatoElab(char ocoDsStatoElab) {
        this.ocoDsStatoElab = ocoDsStatoElab;
    }

    public char getOcoDsStatoElab() {
        return this.ocoDsStatoElab;
    }

    public void setOcoTpModAbbinamento(String ocoTpModAbbinamento) {
        this.ocoTpModAbbinamento = Functions.subString(ocoTpModAbbinamento, Len.OCO_TP_MOD_ABBINAMENTO);
    }

    public String getOcoTpModAbbinamento() {
        return this.ocoTpModAbbinamento;
    }

    public String getOcoTpModAbbinamentoFormatted() {
        return Functions.padBlanks(getOcoTpModAbbinamento(), Len.OCO_TP_MOD_ABBINAMENTO);
    }

    public OcoCarAcq getOcoCarAcq() {
        return ocoCarAcq;
    }

    public OcoDtDecor getOcoDtDecor() {
        return ocoDtDecor;
    }

    public OcoDtScad getOcoDtScad() {
        return ocoDtScad;
    }

    public OcoDtUltPrePag getOcoDtUltPrePag() {
        return ocoDtUltPrePag;
    }

    public OcoIdMoviChiu getOcoIdMoviChiu() {
        return ocoIdMoviChiu;
    }

    public OcoImpCollg getOcoImpCollg() {
        return ocoImpCollg;
    }

    public OcoImpReinvst getOcoImpReinvst() {
        return ocoImpReinvst;
    }

    public OcoImpTrasf getOcoImpTrasf() {
        return ocoImpTrasf;
    }

    public OcoImpTrasferito getOcoImpTrasferito() {
        return ocoImpTrasferito;
    }

    public OcoPcPreTrasferito getOcoPcPreTrasferito() {
        return ocoPcPreTrasferito;
    }

    public OcoPcReinvstRilievi getOcoPcReinvstRilievi() {
        return ocoPcReinvstRilievi;
    }

    public OcoPre1aAnnualita getOcoPre1aAnnualita() {
        return ocoPre1aAnnualita;
    }

    public OcoPrePerTrasf getOcoPrePerTrasf() {
        return ocoPrePerTrasf;
    }

    public OcoRecProv getOcoRecProv() {
        return ocoRecProv;
    }

    public OcoRisMat getOcoRisMat() {
        return ocoRisMat;
    }

    public OcoRisZil getOcoRisZil() {
        return ocoRisZil;
    }

    public OcoTotPre getOcoTotPre() {
        return ocoTotPre;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_TP_OGG_COINV = 2;
        public static final int OCO_TP_OGG_DER = 2;
        public static final int OCO_IB_RIFTO_ESTNO = 40;
        public static final int OCO_COD_PROD = 12;
        public static final int OCO_TP_COLLGM = 2;
        public static final int OCO_TP_TRASF = 2;
        public static final int OCO_IB2O_RIFTO_ESTNO = 40;
        public static final int OCO_DS_UTENTE = 20;
        public static final int OCO_TP_MOD_ABBINAMENTO = 2;
        public static final int OCO_ID_OGG_COLLG = 5;
        public static final int OCO_ID_OGG_COINV = 5;
        public static final int OCO_ID_OGG_DER = 5;
        public static final int OCO_ID_MOVI_CRZ = 5;
        public static final int OCO_DT_INI_EFF = 5;
        public static final int OCO_DT_END_EFF = 5;
        public static final int OCO_COD_COMP_ANIA = 3;
        public static final int OCO_IND_LIQ_AGG_MAN = 1;
        public static final int OCO_DS_RIGA = 6;
        public static final int OCO_DS_OPER_SQL = 1;
        public static final int OCO_DS_VER = 5;
        public static final int OCO_DS_TS_INI_CPTZ = 10;
        public static final int OCO_DS_TS_END_CPTZ = 10;
        public static final int OCO_DS_STATO_ELAB = 1;
        public static final int OGG_COLLG = OCO_ID_OGG_COLLG + OCO_ID_OGG_COINV + OCO_TP_OGG_COINV + OCO_ID_OGG_DER + OCO_TP_OGG_DER + OCO_IB_RIFTO_ESTNO + OCO_ID_MOVI_CRZ + OcoIdMoviChiu.Len.OCO_ID_MOVI_CHIU + OCO_DT_INI_EFF + OCO_DT_END_EFF + OCO_COD_COMP_ANIA + OCO_COD_PROD + OcoDtScad.Len.OCO_DT_SCAD + OCO_TP_COLLGM + OCO_TP_TRASF + OcoRecProv.Len.OCO_REC_PROV + OcoDtDecor.Len.OCO_DT_DECOR + OcoDtUltPrePag.Len.OCO_DT_ULT_PRE_PAG + OcoTotPre.Len.OCO_TOT_PRE + OcoRisMat.Len.OCO_RIS_MAT + OcoRisZil.Len.OCO_RIS_ZIL + OcoImpTrasf.Len.OCO_IMP_TRASF + OcoImpReinvst.Len.OCO_IMP_REINVST + OcoPcReinvstRilievi.Len.OCO_PC_REINVST_RILIEVI + OCO_IB2O_RIFTO_ESTNO + OCO_IND_LIQ_AGG_MAN + OCO_DS_RIGA + OCO_DS_OPER_SQL + OCO_DS_VER + OCO_DS_TS_INI_CPTZ + OCO_DS_TS_END_CPTZ + OCO_DS_UTENTE + OCO_DS_STATO_ELAB + OcoImpCollg.Len.OCO_IMP_COLLG + OcoPrePerTrasf.Len.OCO_PRE_PER_TRASF + OcoCarAcq.Len.OCO_CAR_ACQ + OcoPre1aAnnualita.Len.OCO_PRE1A_ANNUALITA + OcoImpTrasferito.Len.OCO_IMP_TRASFERITO + OCO_TP_MOD_ABBINAMENTO + OcoPcPreTrasferito.Len.OCO_PC_PRE_TRASFERITO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_ID_OGG_COLLG = 9;
            public static final int OCO_ID_OGG_COINV = 9;
            public static final int OCO_ID_OGG_DER = 9;
            public static final int OCO_ID_MOVI_CRZ = 9;
            public static final int OCO_DT_INI_EFF = 8;
            public static final int OCO_DT_END_EFF = 8;
            public static final int OCO_COD_COMP_ANIA = 5;
            public static final int OCO_DS_RIGA = 10;
            public static final int OCO_DS_VER = 9;
            public static final int OCO_DS_TS_INI_CPTZ = 18;
            public static final int OCO_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
