package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-PARAM-MOVI<br>
 * Variable: IND-PARAM-MOVI from copybook IDBVPMO2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndParamMovi {

    //==== PROPERTIES ====
    //Original name: IND-PMO-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-TP-MOVI
    private short tpMovi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-FRQ-MOVI
    private short frqMovi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-DUR-AA
    private short durAa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-DUR-MM
    private short durMm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-DUR-GG
    private short durGg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-DT-RICOR-PREC
    private short dtRicorPrec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-DT-RICOR-SUCC
    private short dtRicorSucc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-PC-INTR-FRAZ
    private short pcIntrFraz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-IMP-BNS-DA-SCO-TOT
    private short impBnsDaScoTot = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-IMP-BNS-DA-SCO
    private short impBnsDaSco = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-PC-ANTIC-BNS
    private short pcAnticBns = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-TP-RINN-COLL
    private short tpRinnColl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-TP-RIVAL-PRE
    private short tpRivalPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-TP-RIVAL-PRSTZ
    private short tpRivalPrstz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-FL-EVID-RIVAL
    private short flEvidRival = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-ULT-PC-PERD
    private short ultPcPerd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-TOT-AA-GIA-PROR
    private short totAaGiaPror = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-TP-OPZ
    private short tpOpz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-AA-REN-CER
    private short aaRenCer = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-PC-REVRSB
    private short pcRevrsb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-IMP-RISC-PARZ-PRGT
    private short impRiscParzPrgt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-IMP-LRD-DI-RAT
    private short impLrdDiRat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-IB-OGG
    private short ibOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-COS-ONER
    private short cosOner = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-SPE-PC
    private short spePc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-FL-ATTIV-GAR
    private short flAttivGar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-CAMBIO-VER-PROD
    private short cambioVerProd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-MM-DIFF
    private short mmDiff = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-IMP-RAT-MANFEE
    private short impRatManfee = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-DT-ULT-EROG-MANFEE
    private short dtUltErogManfee = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-TP-OGG-RIVAL
    private short tpOggRival = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-SOM-ASSTA-GARAC
    private short somAsstaGarac = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-PC-APPLZ-OPZ
    private short pcApplzOpz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-ID-ADES
    private short idAdes = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-TP-ESTR-CNT
    private short tpEstrCnt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-COD-RAMO
    private short codRamo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-GEN-DA-SIN
    private short genDaSin = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-COD-TARI
    private short codTari = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-NUM-RAT-PAG-PRE
    private short numRatPagPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-PC-SERV-VAL
    private short pcServVal = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PMO-ETA-AA-SOGL-BNFICR
    private short etaAaSoglBnficr = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setTpMovi(short tpMovi) {
        this.tpMovi = tpMovi;
    }

    public short getTpMovi() {
        return this.tpMovi;
    }

    public void setFrqMovi(short frqMovi) {
        this.frqMovi = frqMovi;
    }

    public short getFrqMovi() {
        return this.frqMovi;
    }

    public void setDurAa(short durAa) {
        this.durAa = durAa;
    }

    public short getDurAa() {
        return this.durAa;
    }

    public void setDurMm(short durMm) {
        this.durMm = durMm;
    }

    public short getDurMm() {
        return this.durMm;
    }

    public void setDurGg(short durGg) {
        this.durGg = durGg;
    }

    public short getDurGg() {
        return this.durGg;
    }

    public void setDtRicorPrec(short dtRicorPrec) {
        this.dtRicorPrec = dtRicorPrec;
    }

    public short getDtRicorPrec() {
        return this.dtRicorPrec;
    }

    public void setDtRicorSucc(short dtRicorSucc) {
        this.dtRicorSucc = dtRicorSucc;
    }

    public short getDtRicorSucc() {
        return this.dtRicorSucc;
    }

    public void setPcIntrFraz(short pcIntrFraz) {
        this.pcIntrFraz = pcIntrFraz;
    }

    public short getPcIntrFraz() {
        return this.pcIntrFraz;
    }

    public void setImpBnsDaScoTot(short impBnsDaScoTot) {
        this.impBnsDaScoTot = impBnsDaScoTot;
    }

    public short getImpBnsDaScoTot() {
        return this.impBnsDaScoTot;
    }

    public void setImpBnsDaSco(short impBnsDaSco) {
        this.impBnsDaSco = impBnsDaSco;
    }

    public short getImpBnsDaSco() {
        return this.impBnsDaSco;
    }

    public void setPcAnticBns(short pcAnticBns) {
        this.pcAnticBns = pcAnticBns;
    }

    public short getPcAnticBns() {
        return this.pcAnticBns;
    }

    public void setTpRinnColl(short tpRinnColl) {
        this.tpRinnColl = tpRinnColl;
    }

    public short getTpRinnColl() {
        return this.tpRinnColl;
    }

    public void setTpRivalPre(short tpRivalPre) {
        this.tpRivalPre = tpRivalPre;
    }

    public short getTpRivalPre() {
        return this.tpRivalPre;
    }

    public void setTpRivalPrstz(short tpRivalPrstz) {
        this.tpRivalPrstz = tpRivalPrstz;
    }

    public short getTpRivalPrstz() {
        return this.tpRivalPrstz;
    }

    public void setFlEvidRival(short flEvidRival) {
        this.flEvidRival = flEvidRival;
    }

    public short getFlEvidRival() {
        return this.flEvidRival;
    }

    public void setUltPcPerd(short ultPcPerd) {
        this.ultPcPerd = ultPcPerd;
    }

    public short getUltPcPerd() {
        return this.ultPcPerd;
    }

    public void setTotAaGiaPror(short totAaGiaPror) {
        this.totAaGiaPror = totAaGiaPror;
    }

    public short getTotAaGiaPror() {
        return this.totAaGiaPror;
    }

    public void setTpOpz(short tpOpz) {
        this.tpOpz = tpOpz;
    }

    public short getTpOpz() {
        return this.tpOpz;
    }

    public void setAaRenCer(short aaRenCer) {
        this.aaRenCer = aaRenCer;
    }

    public short getAaRenCer() {
        return this.aaRenCer;
    }

    public void setPcRevrsb(short pcRevrsb) {
        this.pcRevrsb = pcRevrsb;
    }

    public short getPcRevrsb() {
        return this.pcRevrsb;
    }

    public void setImpRiscParzPrgt(short impRiscParzPrgt) {
        this.impRiscParzPrgt = impRiscParzPrgt;
    }

    public short getImpRiscParzPrgt() {
        return this.impRiscParzPrgt;
    }

    public void setImpLrdDiRat(short impLrdDiRat) {
        this.impLrdDiRat = impLrdDiRat;
    }

    public short getImpLrdDiRat() {
        return this.impLrdDiRat;
    }

    public void setIbOgg(short ibOgg) {
        this.ibOgg = ibOgg;
    }

    public short getIbOgg() {
        return this.ibOgg;
    }

    public void setCosOner(short cosOner) {
        this.cosOner = cosOner;
    }

    public short getCosOner() {
        return this.cosOner;
    }

    public void setSpePc(short spePc) {
        this.spePc = spePc;
    }

    public short getSpePc() {
        return this.spePc;
    }

    public void setFlAttivGar(short flAttivGar) {
        this.flAttivGar = flAttivGar;
    }

    public short getFlAttivGar() {
        return this.flAttivGar;
    }

    public void setCambioVerProd(short cambioVerProd) {
        this.cambioVerProd = cambioVerProd;
    }

    public short getCambioVerProd() {
        return this.cambioVerProd;
    }

    public void setMmDiff(short mmDiff) {
        this.mmDiff = mmDiff;
    }

    public short getMmDiff() {
        return this.mmDiff;
    }

    public void setImpRatManfee(short impRatManfee) {
        this.impRatManfee = impRatManfee;
    }

    public short getImpRatManfee() {
        return this.impRatManfee;
    }

    public void setDtUltErogManfee(short dtUltErogManfee) {
        this.dtUltErogManfee = dtUltErogManfee;
    }

    public short getDtUltErogManfee() {
        return this.dtUltErogManfee;
    }

    public void setTpOggRival(short tpOggRival) {
        this.tpOggRival = tpOggRival;
    }

    public short getTpOggRival() {
        return this.tpOggRival;
    }

    public void setSomAsstaGarac(short somAsstaGarac) {
        this.somAsstaGarac = somAsstaGarac;
    }

    public short getSomAsstaGarac() {
        return this.somAsstaGarac;
    }

    public void setPcApplzOpz(short pcApplzOpz) {
        this.pcApplzOpz = pcApplzOpz;
    }

    public short getPcApplzOpz() {
        return this.pcApplzOpz;
    }

    public void setIdAdes(short idAdes) {
        this.idAdes = idAdes;
    }

    public short getIdAdes() {
        return this.idAdes;
    }

    public void setTpEstrCnt(short tpEstrCnt) {
        this.tpEstrCnt = tpEstrCnt;
    }

    public short getTpEstrCnt() {
        return this.tpEstrCnt;
    }

    public void setCodRamo(short codRamo) {
        this.codRamo = codRamo;
    }

    public short getCodRamo() {
        return this.codRamo;
    }

    public void setGenDaSin(short genDaSin) {
        this.genDaSin = genDaSin;
    }

    public short getGenDaSin() {
        return this.genDaSin;
    }

    public void setCodTari(short codTari) {
        this.codTari = codTari;
    }

    public short getCodTari() {
        return this.codTari;
    }

    public void setNumRatPagPre(short numRatPagPre) {
        this.numRatPagPre = numRatPagPre;
    }

    public short getNumRatPagPre() {
        return this.numRatPagPre;
    }

    public void setPcServVal(short pcServVal) {
        this.pcServVal = pcServVal;
    }

    public short getPcServVal() {
        return this.pcServVal;
    }

    public void setEtaAaSoglBnficr(short etaAaSoglBnficr) {
        this.etaAaSoglBnficr = etaAaSoglBnficr;
    }

    public short getEtaAaSoglBnficr() {
        return this.etaAaSoglBnficr;
    }
}
