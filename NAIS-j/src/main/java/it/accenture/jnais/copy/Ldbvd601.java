package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVD601<br>
 * Variable: LDBVD601 from copybook LDBVD601<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbvd601 {

    //==== PROPERTIES ====
    //Original name: LDBVD601-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBVD601-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBVD601-TP-STA-TIT
    private String tpStaTit = DefaultValues.stringVal(Len.TP_STA_TIT);
    //Original name: LDBVD601-TP-TIT
    private String tpTit = DefaultValues.stringVal(Len.TP_TIT);

    //==== METHODS ====
    public void setLdbvd601Formatted(String data) {
        byte[] buffer = new byte[Len.LDBVD601];
        MarshalByte.writeString(buffer, 1, data, Len.LDBVD601);
        setLdbvd601Bytes(buffer, 1);
    }

    public String getLdbvd601Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvd601Bytes());
    }

    public byte[] getLdbvd601Bytes() {
        byte[] buffer = new byte[Len.LDBVD601];
        return getLdbvd601Bytes(buffer, 1);
    }

    public void setLdbvd601Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpStaTit = MarshalByte.readString(buffer, position, Len.TP_STA_TIT);
        position += Len.TP_STA_TIT;
        tpTit = MarshalByte.readString(buffer, position, Len.TP_TIT);
    }

    public byte[] getLdbvd601Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tpStaTit, Len.TP_STA_TIT);
        position += Len.TP_STA_TIT;
        MarshalByte.writeString(buffer, position, tpTit, Len.TP_TIT);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpStaTit(String tpStaTit) {
        this.tpStaTit = Functions.subString(tpStaTit, Len.TP_STA_TIT);
    }

    public String getTpStaTit() {
        return this.tpStaTit;
    }

    public void setTpTit(String tpTit) {
        this.tpTit = Functions.subString(tpTit, Len.TP_TIT);
    }

    public String getTpTit() {
        return this.tpTit;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int TP_STA_TIT = 2;
        public static final int TP_TIT = 2;
        public static final int ID_OGG = 5;
        public static final int LDBVD601 = ID_OGG + TP_OGG + TP_STA_TIT + TP_TIT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
