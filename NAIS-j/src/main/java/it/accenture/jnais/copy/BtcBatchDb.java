package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: BTC-BATCH-DB<br>
 * Variable: BTC-BATCH-DB from copybook IDBVBTC3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class BtcBatchDb {

    //==== PROPERTIES ====
    //Original name: BTC-DT-INS-DB
    private String insDb = DefaultValues.stringVal(Len.INS_DB);
    //Original name: BTC-DT-START-DB
    private String startDb = DefaultValues.stringVal(Len.START_DB);
    //Original name: BTC-DT-END-DB
    private String endDb = DefaultValues.stringVal(Len.END_DB);
    //Original name: BTC-DT-EFF-DB
    private String effDb = DefaultValues.stringVal(Len.EFF_DB);

    //==== METHODS ====
    public void setInsDb(String insDb) {
        this.insDb = Functions.subString(insDb, Len.INS_DB);
    }

    public String getInsDb() {
        return this.insDb;
    }

    public void setStartDb(String startDb) {
        this.startDb = Functions.subString(startDb, Len.START_DB);
    }

    public String getStartDb() {
        return this.startDb;
    }

    public void setEndDb(String endDb) {
        this.endDb = Functions.subString(endDb, Len.END_DB);
    }

    public String getEndDb() {
        return this.endDb;
    }

    public void setEffDb(String effDb) {
        this.effDb = Functions.subString(effDb, Len.EFF_DB);
    }

    public String getEffDb() {
        return this.effDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INS_DB = 26;
        public static final int START_DB = 26;
        public static final int END_DB = 26;
        public static final int EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
