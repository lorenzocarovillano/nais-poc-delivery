package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WL71-DATI<br>
 * Variable: WL71-DATI from copybook LCCVL711<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wl71Dati {

    //==== PROPERTIES ====
    //Original name: WL71-ID-MATR-ELAB-BATCH
    private int idMatrElabBatch = DefaultValues.INT_VAL;
    //Original name: WL71-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: WL71-TP-FRM-ASSVA
    private String tpFrmAssva = DefaultValues.stringVal(Len.TP_FRM_ASSVA);
    //Original name: WL71-TP-MOVI
    private int tpMovi = DefaultValues.INT_VAL;
    //Original name: WL71-COD-RAMO
    private String codRamo = DefaultValues.stringVal(Len.COD_RAMO);
    //Original name: WL71-DT-ULT-ELAB
    private int dtUltElab = DefaultValues.INT_VAL;
    //Original name: WL71-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WL71-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: WL71-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WL71-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: WL71-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setIdMatrElabBatch(int idMatrElabBatch) {
        this.idMatrElabBatch = idMatrElabBatch;
    }

    public int getIdMatrElabBatch() {
        return this.idMatrElabBatch;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setTpFrmAssva(String tpFrmAssva) {
        this.tpFrmAssva = Functions.subString(tpFrmAssva, Len.TP_FRM_ASSVA);
    }

    public String getTpFrmAssva() {
        return this.tpFrmAssva;
    }

    public void setTpMovi(int tpMovi) {
        this.tpMovi = tpMovi;
    }

    public int getTpMovi() {
        return this.tpMovi;
    }

    public void setCodRamo(String codRamo) {
        this.codRamo = Functions.subString(codRamo, Len.COD_RAMO);
    }

    public String getCodRamo() {
        return this.codRamo;
    }

    public String getCodRamoFormatted() {
        return Functions.padBlanks(getCodRamo(), Len.COD_RAMO);
    }

    public void setDtUltElab(int dtUltElab) {
        this.dtUltElab = dtUltElab;
    }

    public int getDtUltElab() {
        return this.dtUltElab;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_FRM_ASSVA = 2;
        public static final int COD_RAMO = 12;
        public static final int DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
