package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-D-FORZ-LIQ<br>
 * Variable: IND-D-FORZ-LIQ from copybook IDBVDFL2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndDForzLiq {

    //==== PROPERTIES ====
    //Original name: IND-DFL-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMP-LRD-CALC
    private short impLrdCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMP-LRD-DFZ
    private short impLrdDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMP-LRD-EFFLQ
    private short impLrdEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMP-NET-CALC
    private short impNetCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMP-NET-DFZ
    private short impNetDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMP-NET-EFFLQ
    private short impNetEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-PRVR-CALC
    private short impstPrvrCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-PRVR-DFZ
    private short impstPrvrDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-PRVR-EFFLQ
    private short impstPrvrEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-VIS-CALC
    private short impstVisCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-VIS-DFZ
    private short impstVisDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-VIS-EFFLQ
    private short impstVisEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RIT-ACC-CALC
    private short ritAccCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RIT-ACC-DFZ
    private short ritAccDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RIT-ACC-EFFLQ
    private short ritAccEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RIT-IRPEF-CALC
    private short ritIrpefCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RIT-IRPEF-DFZ
    private short ritIrpefDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RIT-IRPEF-EFFLQ
    private short ritIrpefEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-SOST-CALC
    private short impstSostCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-SOST-DFZ
    private short impstSostDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-SOST-EFFLQ
    private short impstSostEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-TAX-SEP-CALC
    private short taxSepCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-TAX-SEP-DFZ
    private short taxSepDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-TAX-SEP-EFFLQ
    private short taxSepEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-INTR-PREST-CALC
    private short intrPrestCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-INTR-PREST-DFZ
    private short intrPrestDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-INTR-PREST-EFFLQ
    private short intrPrestEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ACCPRE-SOST-CALC
    private short accpreSostCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ACCPRE-SOST-DFZ
    private short accpreSostDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ACCPRE-SOST-EFFLQ
    private short accpreSostEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ACCPRE-VIS-CALC
    private short accpreVisCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ACCPRE-VIS-DFZ
    private short accpreVisDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ACCPRE-VIS-EFFLQ
    private short accpreVisEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ACCPRE-ACC-CALC
    private short accpreAccCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ACCPRE-ACC-DFZ
    private short accpreAccDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ACCPRE-ACC-EFFLQ
    private short accpreAccEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RES-PRSTZ-CALC
    private short resPrstzCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RES-PRSTZ-DFZ
    private short resPrstzDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RES-PRSTZ-EFFLQ
    private short resPrstzEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RES-PRE-ATT-CALC
    private short resPreAttCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RES-PRE-ATT-DFZ
    private short resPreAttDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RES-PRE-ATT-EFFLQ
    private short resPreAttEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMP-EXCONTR-EFF
    private short impExcontrEff = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-VIS-CALC
    private short impbVisCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-VIS-EFFLQ
    private short impbVisEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-VIS-DFZ
    private short impbVisDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-RIT-ACC-CALC
    private short impbRitAccCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-RIT-ACC-EFFLQ
    private short impbRitAccEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-RIT-ACC-DFZ
    private short impbRitAccDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-TFR-CALC
    private short impbTfrCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-TFR-EFFLQ
    private short impbTfrEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-TFR-DFZ
    private short impbTfrDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-IS-CALC
    private short impbIsCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-IS-EFFLQ
    private short impbIsEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-IS-DFZ
    private short impbIsDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-TAX-SEP-CALC
    private short impbTaxSepCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-TAX-SEP-EFFLQ
    private short impbTaxSepEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-TAX-SEP-DFZ
    private short impbTaxSepDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IINT-PREST-CALC
    private short iintPrestCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IINT-PREST-EFFLQ
    private short iintPrestEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IINT-PREST-DFZ
    private short iintPrestDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MONT-END2000-CALC
    private short montEnd2000Calc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MONT-END2000-EFFLQ
    private short montEnd2000Efflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MONT-END2000-DFZ
    private short montEnd2000Dfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MONT-END2006-CALC
    private short montEnd2006Calc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MONT-END2006-EFFLQ
    private short montEnd2006Efflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MONT-END2006-DFZ
    private short montEnd2006Dfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MONT-DAL2007-CALC
    private short montDal2007Calc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MONT-DAL2007-EFFLQ
    private short montDal2007Efflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MONT-DAL2007-DFZ
    private short montDal2007Dfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IIMPST-PRVR-CALC
    private short iimpstPrvrCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IIMPST-PRVR-EFFLQ
    private short iimpstPrvrEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IIMPST-PRVR-DFZ
    private short iimpstPrvrDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IIMPST-252-CALC
    private short iimpst252Calc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IIMPST-252-EFFLQ
    private short iimpst252Efflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IIMPST-252-DFZ
    private short iimpst252Dfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-252-CALC
    private short impst252Calc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-252-EFFLQ
    private short impst252Efflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RIT-TFR-CALC
    private short ritTfrCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RIT-TFR-EFFLQ
    private short ritTfrEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-RIT-TFR-DFZ
    private short ritTfrDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNBT-INPSTFM-CALC
    private short cnbtInpstfmCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNBT-INPSTFM-EFFLQ
    private short cnbtInpstfmEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNBT-INPSTFM-DFZ
    private short cnbtInpstfmDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ICNB-INPSTFM-CALC
    private short icnbInpstfmCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ICNB-INPSTFM-EFFLQ
    private short icnbInpstfmEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ICNB-INPSTFM-DFZ
    private short icnbInpstfmDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNDE-END2000-CALC
    private short cndeEnd2000Calc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNDE-END2000-EFFLQ
    private short cndeEnd2000Efflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNDE-END2000-DFZ
    private short cndeEnd2000Dfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNDE-END2006-CALC
    private short cndeEnd2006Calc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNDE-END2006-EFFLQ
    private short cndeEnd2006Efflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNDE-END2006-DFZ
    private short cndeEnd2006Dfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNDE-DAL2007-CALC
    private short cndeDal2007Calc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNDE-DAL2007-EFFLQ
    private short cndeDal2007Efflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-CNDE-DAL2007-DFZ
    private short cndeDal2007Dfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-AA-CNBZ-END2000-EF
    private short aaCnbzEnd2000Ef = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-AA-CNBZ-END2006-EF
    private short aaCnbzEnd2006Ef = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-AA-CNBZ-DAL2007-EF
    private short aaCnbzDal2007Ef = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MM-CNBZ-END2000-EF
    private short mmCnbzEnd2000Ef = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MM-CNBZ-END2006-EF
    private short mmCnbzEnd2006Ef = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-MM-CNBZ-DAL2007-EF
    private short mmCnbzDal2007Ef = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-DA-RIMB-EFF
    private short impstDaRimbEff = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ALQ-TAX-SEP-CALC
    private short alqTaxSepCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ALQ-TAX-SEP-EFFLQ
    private short alqTaxSepEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ALQ-TAX-SEP-DFZ
    private short alqTaxSepDfz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ALQ-CNBT-INPSTFM-C
    private short alqCnbtInpstfmC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ALQ-CNBT-INPSTFM-E
    private short alqCnbtInpstfmE = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-ALQ-CNBT-INPSTFM-D
    private short alqCnbtInpstfmD = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-VIS-1382011C
    private short impbVis1382011c = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-VIS-1382011D
    private short impbVis1382011d = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-VIS-1382011L
    private short impbVis1382011l = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-VIS-1382011C
    private short impstVis1382011c = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-VIS-1382011D
    private short impstVis1382011d = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-VIS-1382011L
    private short impstVis1382011l = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-IS-1382011C
    private short impbIs1382011c = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-IS-1382011D
    private short impbIs1382011d = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-IS-1382011L
    private short impbIs1382011l = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IS-1382011C
    private short is1382011c = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IS-1382011D
    private short is1382011d = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IS-1382011L
    private short is1382011l = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMP-INTR-RIT-PAG-C
    private short impIntrRitPagC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMP-INTR-RIT-PAG-D
    private short impIntrRitPagD = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMP-INTR-RIT-PAG-L
    private short impIntrRitPagL = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-BOLLO-DETT-C
    private short impbBolloDettC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-BOLLO-DETT-D
    private short impbBolloDettD = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-BOLLO-DETT-L
    private short impbBolloDettL = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-BOLLO-DETT-C
    private short impstBolloDettC = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-BOLLO-DETT-D
    private short impstBolloDettD = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-BOLLO-DETT-L
    private short impstBolloDettL = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-BOLLO-TOT-VC
    private short impstBolloTotVc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-BOLLO-TOT-VD
    private short impstBolloTotVd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-BOLLO-TOT-VL
    private short impstBolloTotVl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-VIS-662014C
    private short impbVis662014c = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-VIS-662014D
    private short impbVis662014d = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-VIS-662014L
    private short impbVis662014l = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-VIS-662014C
    private short impstVis662014c = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-VIS-662014D
    private short impstVis662014d = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPST-VIS-662014L
    private short impstVis662014l = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-IS-662014C
    private short impbIs662014c = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-IS-662014D
    private short impbIs662014d = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IMPB-IS-662014L
    private short impbIs662014l = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IS-662014C
    private short is662014c = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IS-662014D
    private short is662014d = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DFL-IS-662014L
    private short is662014l = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setImpLrdCalc(short impLrdCalc) {
        this.impLrdCalc = impLrdCalc;
    }

    public short getImpLrdCalc() {
        return this.impLrdCalc;
    }

    public void setImpLrdDfz(short impLrdDfz) {
        this.impLrdDfz = impLrdDfz;
    }

    public short getImpLrdDfz() {
        return this.impLrdDfz;
    }

    public void setImpLrdEfflq(short impLrdEfflq) {
        this.impLrdEfflq = impLrdEfflq;
    }

    public short getImpLrdEfflq() {
        return this.impLrdEfflq;
    }

    public void setImpNetCalc(short impNetCalc) {
        this.impNetCalc = impNetCalc;
    }

    public short getImpNetCalc() {
        return this.impNetCalc;
    }

    public void setImpNetDfz(short impNetDfz) {
        this.impNetDfz = impNetDfz;
    }

    public short getImpNetDfz() {
        return this.impNetDfz;
    }

    public void setImpNetEfflq(short impNetEfflq) {
        this.impNetEfflq = impNetEfflq;
    }

    public short getImpNetEfflq() {
        return this.impNetEfflq;
    }

    public void setImpstPrvrCalc(short impstPrvrCalc) {
        this.impstPrvrCalc = impstPrvrCalc;
    }

    public short getImpstPrvrCalc() {
        return this.impstPrvrCalc;
    }

    public void setImpstPrvrDfz(short impstPrvrDfz) {
        this.impstPrvrDfz = impstPrvrDfz;
    }

    public short getImpstPrvrDfz() {
        return this.impstPrvrDfz;
    }

    public void setImpstPrvrEfflq(short impstPrvrEfflq) {
        this.impstPrvrEfflq = impstPrvrEfflq;
    }

    public short getImpstPrvrEfflq() {
        return this.impstPrvrEfflq;
    }

    public void setImpstVisCalc(short impstVisCalc) {
        this.impstVisCalc = impstVisCalc;
    }

    public short getImpstVisCalc() {
        return this.impstVisCalc;
    }

    public void setImpstVisDfz(short impstVisDfz) {
        this.impstVisDfz = impstVisDfz;
    }

    public short getImpstVisDfz() {
        return this.impstVisDfz;
    }

    public void setImpstVisEfflq(short impstVisEfflq) {
        this.impstVisEfflq = impstVisEfflq;
    }

    public short getImpstVisEfflq() {
        return this.impstVisEfflq;
    }

    public void setRitAccCalc(short ritAccCalc) {
        this.ritAccCalc = ritAccCalc;
    }

    public short getRitAccCalc() {
        return this.ritAccCalc;
    }

    public void setRitAccDfz(short ritAccDfz) {
        this.ritAccDfz = ritAccDfz;
    }

    public short getRitAccDfz() {
        return this.ritAccDfz;
    }

    public void setRitAccEfflq(short ritAccEfflq) {
        this.ritAccEfflq = ritAccEfflq;
    }

    public short getRitAccEfflq() {
        return this.ritAccEfflq;
    }

    public void setRitIrpefCalc(short ritIrpefCalc) {
        this.ritIrpefCalc = ritIrpefCalc;
    }

    public short getRitIrpefCalc() {
        return this.ritIrpefCalc;
    }

    public void setRitIrpefDfz(short ritIrpefDfz) {
        this.ritIrpefDfz = ritIrpefDfz;
    }

    public short getRitIrpefDfz() {
        return this.ritIrpefDfz;
    }

    public void setRitIrpefEfflq(short ritIrpefEfflq) {
        this.ritIrpefEfflq = ritIrpefEfflq;
    }

    public short getRitIrpefEfflq() {
        return this.ritIrpefEfflq;
    }

    public void setImpstSostCalc(short impstSostCalc) {
        this.impstSostCalc = impstSostCalc;
    }

    public short getImpstSostCalc() {
        return this.impstSostCalc;
    }

    public void setImpstSostDfz(short impstSostDfz) {
        this.impstSostDfz = impstSostDfz;
    }

    public short getImpstSostDfz() {
        return this.impstSostDfz;
    }

    public void setImpstSostEfflq(short impstSostEfflq) {
        this.impstSostEfflq = impstSostEfflq;
    }

    public short getImpstSostEfflq() {
        return this.impstSostEfflq;
    }

    public void setTaxSepCalc(short taxSepCalc) {
        this.taxSepCalc = taxSepCalc;
    }

    public short getTaxSepCalc() {
        return this.taxSepCalc;
    }

    public void setTaxSepDfz(short taxSepDfz) {
        this.taxSepDfz = taxSepDfz;
    }

    public short getTaxSepDfz() {
        return this.taxSepDfz;
    }

    public void setTaxSepEfflq(short taxSepEfflq) {
        this.taxSepEfflq = taxSepEfflq;
    }

    public short getTaxSepEfflq() {
        return this.taxSepEfflq;
    }

    public void setIntrPrestCalc(short intrPrestCalc) {
        this.intrPrestCalc = intrPrestCalc;
    }

    public short getIntrPrestCalc() {
        return this.intrPrestCalc;
    }

    public void setIntrPrestDfz(short intrPrestDfz) {
        this.intrPrestDfz = intrPrestDfz;
    }

    public short getIntrPrestDfz() {
        return this.intrPrestDfz;
    }

    public void setIntrPrestEfflq(short intrPrestEfflq) {
        this.intrPrestEfflq = intrPrestEfflq;
    }

    public short getIntrPrestEfflq() {
        return this.intrPrestEfflq;
    }

    public void setAccpreSostCalc(short accpreSostCalc) {
        this.accpreSostCalc = accpreSostCalc;
    }

    public short getAccpreSostCalc() {
        return this.accpreSostCalc;
    }

    public void setAccpreSostDfz(short accpreSostDfz) {
        this.accpreSostDfz = accpreSostDfz;
    }

    public short getAccpreSostDfz() {
        return this.accpreSostDfz;
    }

    public void setAccpreSostEfflq(short accpreSostEfflq) {
        this.accpreSostEfflq = accpreSostEfflq;
    }

    public short getAccpreSostEfflq() {
        return this.accpreSostEfflq;
    }

    public void setAccpreVisCalc(short accpreVisCalc) {
        this.accpreVisCalc = accpreVisCalc;
    }

    public short getAccpreVisCalc() {
        return this.accpreVisCalc;
    }

    public void setAccpreVisDfz(short accpreVisDfz) {
        this.accpreVisDfz = accpreVisDfz;
    }

    public short getAccpreVisDfz() {
        return this.accpreVisDfz;
    }

    public void setAccpreVisEfflq(short accpreVisEfflq) {
        this.accpreVisEfflq = accpreVisEfflq;
    }

    public short getAccpreVisEfflq() {
        return this.accpreVisEfflq;
    }

    public void setAccpreAccCalc(short accpreAccCalc) {
        this.accpreAccCalc = accpreAccCalc;
    }

    public short getAccpreAccCalc() {
        return this.accpreAccCalc;
    }

    public void setAccpreAccDfz(short accpreAccDfz) {
        this.accpreAccDfz = accpreAccDfz;
    }

    public short getAccpreAccDfz() {
        return this.accpreAccDfz;
    }

    public void setAccpreAccEfflq(short accpreAccEfflq) {
        this.accpreAccEfflq = accpreAccEfflq;
    }

    public short getAccpreAccEfflq() {
        return this.accpreAccEfflq;
    }

    public void setResPrstzCalc(short resPrstzCalc) {
        this.resPrstzCalc = resPrstzCalc;
    }

    public short getResPrstzCalc() {
        return this.resPrstzCalc;
    }

    public void setResPrstzDfz(short resPrstzDfz) {
        this.resPrstzDfz = resPrstzDfz;
    }

    public short getResPrstzDfz() {
        return this.resPrstzDfz;
    }

    public void setResPrstzEfflq(short resPrstzEfflq) {
        this.resPrstzEfflq = resPrstzEfflq;
    }

    public short getResPrstzEfflq() {
        return this.resPrstzEfflq;
    }

    public void setResPreAttCalc(short resPreAttCalc) {
        this.resPreAttCalc = resPreAttCalc;
    }

    public short getResPreAttCalc() {
        return this.resPreAttCalc;
    }

    public void setResPreAttDfz(short resPreAttDfz) {
        this.resPreAttDfz = resPreAttDfz;
    }

    public short getResPreAttDfz() {
        return this.resPreAttDfz;
    }

    public void setResPreAttEfflq(short resPreAttEfflq) {
        this.resPreAttEfflq = resPreAttEfflq;
    }

    public short getResPreAttEfflq() {
        return this.resPreAttEfflq;
    }

    public void setImpExcontrEff(short impExcontrEff) {
        this.impExcontrEff = impExcontrEff;
    }

    public short getImpExcontrEff() {
        return this.impExcontrEff;
    }

    public void setImpbVisCalc(short impbVisCalc) {
        this.impbVisCalc = impbVisCalc;
    }

    public short getImpbVisCalc() {
        return this.impbVisCalc;
    }

    public void setImpbVisEfflq(short impbVisEfflq) {
        this.impbVisEfflq = impbVisEfflq;
    }

    public short getImpbVisEfflq() {
        return this.impbVisEfflq;
    }

    public void setImpbVisDfz(short impbVisDfz) {
        this.impbVisDfz = impbVisDfz;
    }

    public short getImpbVisDfz() {
        return this.impbVisDfz;
    }

    public void setImpbRitAccCalc(short impbRitAccCalc) {
        this.impbRitAccCalc = impbRitAccCalc;
    }

    public short getImpbRitAccCalc() {
        return this.impbRitAccCalc;
    }

    public void setImpbRitAccEfflq(short impbRitAccEfflq) {
        this.impbRitAccEfflq = impbRitAccEfflq;
    }

    public short getImpbRitAccEfflq() {
        return this.impbRitAccEfflq;
    }

    public void setImpbRitAccDfz(short impbRitAccDfz) {
        this.impbRitAccDfz = impbRitAccDfz;
    }

    public short getImpbRitAccDfz() {
        return this.impbRitAccDfz;
    }

    public void setImpbTfrCalc(short impbTfrCalc) {
        this.impbTfrCalc = impbTfrCalc;
    }

    public short getImpbTfrCalc() {
        return this.impbTfrCalc;
    }

    public void setImpbTfrEfflq(short impbTfrEfflq) {
        this.impbTfrEfflq = impbTfrEfflq;
    }

    public short getImpbTfrEfflq() {
        return this.impbTfrEfflq;
    }

    public void setImpbTfrDfz(short impbTfrDfz) {
        this.impbTfrDfz = impbTfrDfz;
    }

    public short getImpbTfrDfz() {
        return this.impbTfrDfz;
    }

    public void setImpbIsCalc(short impbIsCalc) {
        this.impbIsCalc = impbIsCalc;
    }

    public short getImpbIsCalc() {
        return this.impbIsCalc;
    }

    public void setImpbIsEfflq(short impbIsEfflq) {
        this.impbIsEfflq = impbIsEfflq;
    }

    public short getImpbIsEfflq() {
        return this.impbIsEfflq;
    }

    public void setImpbIsDfz(short impbIsDfz) {
        this.impbIsDfz = impbIsDfz;
    }

    public short getImpbIsDfz() {
        return this.impbIsDfz;
    }

    public void setImpbTaxSepCalc(short impbTaxSepCalc) {
        this.impbTaxSepCalc = impbTaxSepCalc;
    }

    public short getImpbTaxSepCalc() {
        return this.impbTaxSepCalc;
    }

    public void setImpbTaxSepEfflq(short impbTaxSepEfflq) {
        this.impbTaxSepEfflq = impbTaxSepEfflq;
    }

    public short getImpbTaxSepEfflq() {
        return this.impbTaxSepEfflq;
    }

    public void setImpbTaxSepDfz(short impbTaxSepDfz) {
        this.impbTaxSepDfz = impbTaxSepDfz;
    }

    public short getImpbTaxSepDfz() {
        return this.impbTaxSepDfz;
    }

    public void setIintPrestCalc(short iintPrestCalc) {
        this.iintPrestCalc = iintPrestCalc;
    }

    public short getIintPrestCalc() {
        return this.iintPrestCalc;
    }

    public void setIintPrestEfflq(short iintPrestEfflq) {
        this.iintPrestEfflq = iintPrestEfflq;
    }

    public short getIintPrestEfflq() {
        return this.iintPrestEfflq;
    }

    public void setIintPrestDfz(short iintPrestDfz) {
        this.iintPrestDfz = iintPrestDfz;
    }

    public short getIintPrestDfz() {
        return this.iintPrestDfz;
    }

    public void setMontEnd2000Calc(short montEnd2000Calc) {
        this.montEnd2000Calc = montEnd2000Calc;
    }

    public short getMontEnd2000Calc() {
        return this.montEnd2000Calc;
    }

    public void setMontEnd2000Efflq(short montEnd2000Efflq) {
        this.montEnd2000Efflq = montEnd2000Efflq;
    }

    public short getMontEnd2000Efflq() {
        return this.montEnd2000Efflq;
    }

    public void setMontEnd2000Dfz(short montEnd2000Dfz) {
        this.montEnd2000Dfz = montEnd2000Dfz;
    }

    public short getMontEnd2000Dfz() {
        return this.montEnd2000Dfz;
    }

    public void setMontEnd2006Calc(short montEnd2006Calc) {
        this.montEnd2006Calc = montEnd2006Calc;
    }

    public short getMontEnd2006Calc() {
        return this.montEnd2006Calc;
    }

    public void setMontEnd2006Efflq(short montEnd2006Efflq) {
        this.montEnd2006Efflq = montEnd2006Efflq;
    }

    public short getMontEnd2006Efflq() {
        return this.montEnd2006Efflq;
    }

    public void setMontEnd2006Dfz(short montEnd2006Dfz) {
        this.montEnd2006Dfz = montEnd2006Dfz;
    }

    public short getMontEnd2006Dfz() {
        return this.montEnd2006Dfz;
    }

    public void setMontDal2007Calc(short montDal2007Calc) {
        this.montDal2007Calc = montDal2007Calc;
    }

    public short getMontDal2007Calc() {
        return this.montDal2007Calc;
    }

    public void setMontDal2007Efflq(short montDal2007Efflq) {
        this.montDal2007Efflq = montDal2007Efflq;
    }

    public short getMontDal2007Efflq() {
        return this.montDal2007Efflq;
    }

    public void setMontDal2007Dfz(short montDal2007Dfz) {
        this.montDal2007Dfz = montDal2007Dfz;
    }

    public short getMontDal2007Dfz() {
        return this.montDal2007Dfz;
    }

    public void setIimpstPrvrCalc(short iimpstPrvrCalc) {
        this.iimpstPrvrCalc = iimpstPrvrCalc;
    }

    public short getIimpstPrvrCalc() {
        return this.iimpstPrvrCalc;
    }

    public void setIimpstPrvrEfflq(short iimpstPrvrEfflq) {
        this.iimpstPrvrEfflq = iimpstPrvrEfflq;
    }

    public short getIimpstPrvrEfflq() {
        return this.iimpstPrvrEfflq;
    }

    public void setIimpstPrvrDfz(short iimpstPrvrDfz) {
        this.iimpstPrvrDfz = iimpstPrvrDfz;
    }

    public short getIimpstPrvrDfz() {
        return this.iimpstPrvrDfz;
    }

    public void setIimpst252Calc(short iimpst252Calc) {
        this.iimpst252Calc = iimpst252Calc;
    }

    public short getIimpst252Calc() {
        return this.iimpst252Calc;
    }

    public void setIimpst252Efflq(short iimpst252Efflq) {
        this.iimpst252Efflq = iimpst252Efflq;
    }

    public short getIimpst252Efflq() {
        return this.iimpst252Efflq;
    }

    public void setIimpst252Dfz(short iimpst252Dfz) {
        this.iimpst252Dfz = iimpst252Dfz;
    }

    public short getIimpst252Dfz() {
        return this.iimpst252Dfz;
    }

    public void setImpst252Calc(short impst252Calc) {
        this.impst252Calc = impst252Calc;
    }

    public short getImpst252Calc() {
        return this.impst252Calc;
    }

    public void setImpst252Efflq(short impst252Efflq) {
        this.impst252Efflq = impst252Efflq;
    }

    public short getImpst252Efflq() {
        return this.impst252Efflq;
    }

    public void setRitTfrCalc(short ritTfrCalc) {
        this.ritTfrCalc = ritTfrCalc;
    }

    public short getRitTfrCalc() {
        return this.ritTfrCalc;
    }

    public void setRitTfrEfflq(short ritTfrEfflq) {
        this.ritTfrEfflq = ritTfrEfflq;
    }

    public short getRitTfrEfflq() {
        return this.ritTfrEfflq;
    }

    public void setRitTfrDfz(short ritTfrDfz) {
        this.ritTfrDfz = ritTfrDfz;
    }

    public short getRitTfrDfz() {
        return this.ritTfrDfz;
    }

    public void setCnbtInpstfmCalc(short cnbtInpstfmCalc) {
        this.cnbtInpstfmCalc = cnbtInpstfmCalc;
    }

    public short getCnbtInpstfmCalc() {
        return this.cnbtInpstfmCalc;
    }

    public void setCnbtInpstfmEfflq(short cnbtInpstfmEfflq) {
        this.cnbtInpstfmEfflq = cnbtInpstfmEfflq;
    }

    public short getCnbtInpstfmEfflq() {
        return this.cnbtInpstfmEfflq;
    }

    public void setCnbtInpstfmDfz(short cnbtInpstfmDfz) {
        this.cnbtInpstfmDfz = cnbtInpstfmDfz;
    }

    public short getCnbtInpstfmDfz() {
        return this.cnbtInpstfmDfz;
    }

    public void setIcnbInpstfmCalc(short icnbInpstfmCalc) {
        this.icnbInpstfmCalc = icnbInpstfmCalc;
    }

    public short getIcnbInpstfmCalc() {
        return this.icnbInpstfmCalc;
    }

    public void setIcnbInpstfmEfflq(short icnbInpstfmEfflq) {
        this.icnbInpstfmEfflq = icnbInpstfmEfflq;
    }

    public short getIcnbInpstfmEfflq() {
        return this.icnbInpstfmEfflq;
    }

    public void setIcnbInpstfmDfz(short icnbInpstfmDfz) {
        this.icnbInpstfmDfz = icnbInpstfmDfz;
    }

    public short getIcnbInpstfmDfz() {
        return this.icnbInpstfmDfz;
    }

    public void setCndeEnd2000Calc(short cndeEnd2000Calc) {
        this.cndeEnd2000Calc = cndeEnd2000Calc;
    }

    public short getCndeEnd2000Calc() {
        return this.cndeEnd2000Calc;
    }

    public void setCndeEnd2000Efflq(short cndeEnd2000Efflq) {
        this.cndeEnd2000Efflq = cndeEnd2000Efflq;
    }

    public short getCndeEnd2000Efflq() {
        return this.cndeEnd2000Efflq;
    }

    public void setCndeEnd2000Dfz(short cndeEnd2000Dfz) {
        this.cndeEnd2000Dfz = cndeEnd2000Dfz;
    }

    public short getCndeEnd2000Dfz() {
        return this.cndeEnd2000Dfz;
    }

    public void setCndeEnd2006Calc(short cndeEnd2006Calc) {
        this.cndeEnd2006Calc = cndeEnd2006Calc;
    }

    public short getCndeEnd2006Calc() {
        return this.cndeEnd2006Calc;
    }

    public void setCndeEnd2006Efflq(short cndeEnd2006Efflq) {
        this.cndeEnd2006Efflq = cndeEnd2006Efflq;
    }

    public short getCndeEnd2006Efflq() {
        return this.cndeEnd2006Efflq;
    }

    public void setCndeEnd2006Dfz(short cndeEnd2006Dfz) {
        this.cndeEnd2006Dfz = cndeEnd2006Dfz;
    }

    public short getCndeEnd2006Dfz() {
        return this.cndeEnd2006Dfz;
    }

    public void setCndeDal2007Calc(short cndeDal2007Calc) {
        this.cndeDal2007Calc = cndeDal2007Calc;
    }

    public short getCndeDal2007Calc() {
        return this.cndeDal2007Calc;
    }

    public void setCndeDal2007Efflq(short cndeDal2007Efflq) {
        this.cndeDal2007Efflq = cndeDal2007Efflq;
    }

    public short getCndeDal2007Efflq() {
        return this.cndeDal2007Efflq;
    }

    public void setCndeDal2007Dfz(short cndeDal2007Dfz) {
        this.cndeDal2007Dfz = cndeDal2007Dfz;
    }

    public short getCndeDal2007Dfz() {
        return this.cndeDal2007Dfz;
    }

    public void setAaCnbzEnd2000Ef(short aaCnbzEnd2000Ef) {
        this.aaCnbzEnd2000Ef = aaCnbzEnd2000Ef;
    }

    public short getAaCnbzEnd2000Ef() {
        return this.aaCnbzEnd2000Ef;
    }

    public void setAaCnbzEnd2006Ef(short aaCnbzEnd2006Ef) {
        this.aaCnbzEnd2006Ef = aaCnbzEnd2006Ef;
    }

    public short getAaCnbzEnd2006Ef() {
        return this.aaCnbzEnd2006Ef;
    }

    public void setAaCnbzDal2007Ef(short aaCnbzDal2007Ef) {
        this.aaCnbzDal2007Ef = aaCnbzDal2007Ef;
    }

    public short getAaCnbzDal2007Ef() {
        return this.aaCnbzDal2007Ef;
    }

    public void setMmCnbzEnd2000Ef(short mmCnbzEnd2000Ef) {
        this.mmCnbzEnd2000Ef = mmCnbzEnd2000Ef;
    }

    public short getMmCnbzEnd2000Ef() {
        return this.mmCnbzEnd2000Ef;
    }

    public void setMmCnbzEnd2006Ef(short mmCnbzEnd2006Ef) {
        this.mmCnbzEnd2006Ef = mmCnbzEnd2006Ef;
    }

    public short getMmCnbzEnd2006Ef() {
        return this.mmCnbzEnd2006Ef;
    }

    public void setMmCnbzDal2007Ef(short mmCnbzDal2007Ef) {
        this.mmCnbzDal2007Ef = mmCnbzDal2007Ef;
    }

    public short getMmCnbzDal2007Ef() {
        return this.mmCnbzDal2007Ef;
    }

    public void setImpstDaRimbEff(short impstDaRimbEff) {
        this.impstDaRimbEff = impstDaRimbEff;
    }

    public short getImpstDaRimbEff() {
        return this.impstDaRimbEff;
    }

    public void setAlqTaxSepCalc(short alqTaxSepCalc) {
        this.alqTaxSepCalc = alqTaxSepCalc;
    }

    public short getAlqTaxSepCalc() {
        return this.alqTaxSepCalc;
    }

    public void setAlqTaxSepEfflq(short alqTaxSepEfflq) {
        this.alqTaxSepEfflq = alqTaxSepEfflq;
    }

    public short getAlqTaxSepEfflq() {
        return this.alqTaxSepEfflq;
    }

    public void setAlqTaxSepDfz(short alqTaxSepDfz) {
        this.alqTaxSepDfz = alqTaxSepDfz;
    }

    public short getAlqTaxSepDfz() {
        return this.alqTaxSepDfz;
    }

    public void setAlqCnbtInpstfmC(short alqCnbtInpstfmC) {
        this.alqCnbtInpstfmC = alqCnbtInpstfmC;
    }

    public short getAlqCnbtInpstfmC() {
        return this.alqCnbtInpstfmC;
    }

    public void setAlqCnbtInpstfmE(short alqCnbtInpstfmE) {
        this.alqCnbtInpstfmE = alqCnbtInpstfmE;
    }

    public short getAlqCnbtInpstfmE() {
        return this.alqCnbtInpstfmE;
    }

    public void setAlqCnbtInpstfmD(short alqCnbtInpstfmD) {
        this.alqCnbtInpstfmD = alqCnbtInpstfmD;
    }

    public short getAlqCnbtInpstfmD() {
        return this.alqCnbtInpstfmD;
    }

    public void setImpbVis1382011c(short impbVis1382011c) {
        this.impbVis1382011c = impbVis1382011c;
    }

    public short getImpbVis1382011c() {
        return this.impbVis1382011c;
    }

    public void setImpbVis1382011d(short impbVis1382011d) {
        this.impbVis1382011d = impbVis1382011d;
    }

    public short getImpbVis1382011d() {
        return this.impbVis1382011d;
    }

    public void setImpbVis1382011l(short impbVis1382011l) {
        this.impbVis1382011l = impbVis1382011l;
    }

    public short getImpbVis1382011l() {
        return this.impbVis1382011l;
    }

    public void setImpstVis1382011c(short impstVis1382011c) {
        this.impstVis1382011c = impstVis1382011c;
    }

    public short getImpstVis1382011c() {
        return this.impstVis1382011c;
    }

    public void setImpstVis1382011d(short impstVis1382011d) {
        this.impstVis1382011d = impstVis1382011d;
    }

    public short getImpstVis1382011d() {
        return this.impstVis1382011d;
    }

    public void setImpstVis1382011l(short impstVis1382011l) {
        this.impstVis1382011l = impstVis1382011l;
    }

    public short getImpstVis1382011l() {
        return this.impstVis1382011l;
    }

    public void setImpbIs1382011c(short impbIs1382011c) {
        this.impbIs1382011c = impbIs1382011c;
    }

    public short getImpbIs1382011c() {
        return this.impbIs1382011c;
    }

    public void setImpbIs1382011d(short impbIs1382011d) {
        this.impbIs1382011d = impbIs1382011d;
    }

    public short getImpbIs1382011d() {
        return this.impbIs1382011d;
    }

    public void setImpbIs1382011l(short impbIs1382011l) {
        this.impbIs1382011l = impbIs1382011l;
    }

    public short getImpbIs1382011l() {
        return this.impbIs1382011l;
    }

    public void setIs1382011c(short is1382011c) {
        this.is1382011c = is1382011c;
    }

    public short getIs1382011c() {
        return this.is1382011c;
    }

    public void setIs1382011d(short is1382011d) {
        this.is1382011d = is1382011d;
    }

    public short getIs1382011d() {
        return this.is1382011d;
    }

    public void setIs1382011l(short is1382011l) {
        this.is1382011l = is1382011l;
    }

    public short getIs1382011l() {
        return this.is1382011l;
    }

    public void setImpIntrRitPagC(short impIntrRitPagC) {
        this.impIntrRitPagC = impIntrRitPagC;
    }

    public short getImpIntrRitPagC() {
        return this.impIntrRitPagC;
    }

    public void setImpIntrRitPagD(short impIntrRitPagD) {
        this.impIntrRitPagD = impIntrRitPagD;
    }

    public short getImpIntrRitPagD() {
        return this.impIntrRitPagD;
    }

    public void setImpIntrRitPagL(short impIntrRitPagL) {
        this.impIntrRitPagL = impIntrRitPagL;
    }

    public short getImpIntrRitPagL() {
        return this.impIntrRitPagL;
    }

    public void setImpbBolloDettC(short impbBolloDettC) {
        this.impbBolloDettC = impbBolloDettC;
    }

    public short getImpbBolloDettC() {
        return this.impbBolloDettC;
    }

    public void setImpbBolloDettD(short impbBolloDettD) {
        this.impbBolloDettD = impbBolloDettD;
    }

    public short getImpbBolloDettD() {
        return this.impbBolloDettD;
    }

    public void setImpbBolloDettL(short impbBolloDettL) {
        this.impbBolloDettL = impbBolloDettL;
    }

    public short getImpbBolloDettL() {
        return this.impbBolloDettL;
    }

    public void setImpstBolloDettC(short impstBolloDettC) {
        this.impstBolloDettC = impstBolloDettC;
    }

    public short getImpstBolloDettC() {
        return this.impstBolloDettC;
    }

    public void setImpstBolloDettD(short impstBolloDettD) {
        this.impstBolloDettD = impstBolloDettD;
    }

    public short getImpstBolloDettD() {
        return this.impstBolloDettD;
    }

    public void setImpstBolloDettL(short impstBolloDettL) {
        this.impstBolloDettL = impstBolloDettL;
    }

    public short getImpstBolloDettL() {
        return this.impstBolloDettL;
    }

    public void setImpstBolloTotVc(short impstBolloTotVc) {
        this.impstBolloTotVc = impstBolloTotVc;
    }

    public short getImpstBolloTotVc() {
        return this.impstBolloTotVc;
    }

    public void setImpstBolloTotVd(short impstBolloTotVd) {
        this.impstBolloTotVd = impstBolloTotVd;
    }

    public short getImpstBolloTotVd() {
        return this.impstBolloTotVd;
    }

    public void setImpstBolloTotVl(short impstBolloTotVl) {
        this.impstBolloTotVl = impstBolloTotVl;
    }

    public short getImpstBolloTotVl() {
        return this.impstBolloTotVl;
    }

    public void setImpbVis662014c(short impbVis662014c) {
        this.impbVis662014c = impbVis662014c;
    }

    public short getImpbVis662014c() {
        return this.impbVis662014c;
    }

    public void setImpbVis662014d(short impbVis662014d) {
        this.impbVis662014d = impbVis662014d;
    }

    public short getImpbVis662014d() {
        return this.impbVis662014d;
    }

    public void setImpbVis662014l(short impbVis662014l) {
        this.impbVis662014l = impbVis662014l;
    }

    public short getImpbVis662014l() {
        return this.impbVis662014l;
    }

    public void setImpstVis662014c(short impstVis662014c) {
        this.impstVis662014c = impstVis662014c;
    }

    public short getImpstVis662014c() {
        return this.impstVis662014c;
    }

    public void setImpstVis662014d(short impstVis662014d) {
        this.impstVis662014d = impstVis662014d;
    }

    public short getImpstVis662014d() {
        return this.impstVis662014d;
    }

    public void setImpstVis662014l(short impstVis662014l) {
        this.impstVis662014l = impstVis662014l;
    }

    public short getImpstVis662014l() {
        return this.impstVis662014l;
    }

    public void setImpbIs662014c(short impbIs662014c) {
        this.impbIs662014c = impbIs662014c;
    }

    public short getImpbIs662014c() {
        return this.impbIs662014c;
    }

    public void setImpbIs662014d(short impbIs662014d) {
        this.impbIs662014d = impbIs662014d;
    }

    public short getImpbIs662014d() {
        return this.impbIs662014d;
    }

    public void setImpbIs662014l(short impbIs662014l) {
        this.impbIs662014l = impbIs662014l;
    }

    public short getImpbIs662014l() {
        return this.impbIs662014l;
    }

    public void setIs662014c(short is662014c) {
        this.is662014c = is662014c;
    }

    public short getIs662014c() {
        return this.is662014c;
    }

    public void setIs662014d(short is662014d) {
        this.is662014d = is662014d;
    }

    public short getIs662014d() {
        return this.is662014d;
    }

    public void setIs662014l(short is662014l) {
        this.is662014l = is662014l;
    }

    public short getIs662014l() {
        return this.is662014l;
    }
}
