package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVOCS3<br>
 * Copybook: IDBVOCS3 from copybook IDBVOCS3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvocs3 {

    //==== PROPERTIES ====
    //Original name: OCS-DT-INI-EFF-DB
    private String ocsDtIniEffDb = DefaultValues.stringVal(Len.OCS_DT_INI_EFF_DB);
    //Original name: OCS-DT-END-EFF-DB
    private String ocsDtEndEffDb = DefaultValues.stringVal(Len.OCS_DT_END_EFF_DB);

    //==== METHODS ====
    public void setOcsDtIniEffDb(String ocsDtIniEffDb) {
        this.ocsDtIniEffDb = Functions.subString(ocsDtIniEffDb, Len.OCS_DT_INI_EFF_DB);
    }

    public String getOcsDtIniEffDb() {
        return this.ocsDtIniEffDb;
    }

    public void setOcsDtEndEffDb(String ocsDtEndEffDb) {
        this.ocsDtEndEffDb = Functions.subString(ocsDtEndEffDb, Len.OCS_DT_END_EFF_DB);
    }

    public String getOcsDtEndEffDb() {
        return this.ocsDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OCS_DT_INI_EFF_DB = 10;
        public static final int OCS_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
