package it.accenture.jnais.copy;

import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.StatoSeqguide;

/**Original name: IABVSQG1<br>
 * Variable: IABVSQG1 from copybook IABVSQG1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Iabvsqg1 {

    //==== PROPERTIES ====
    /**Original name: FS-SEQGUIDE<br>
	 * <pre>*****************************************************************
	 *  --          GESTIONE GUIDA DA SEQUENZIALE
	 * *****************************************************************
	 *    fine COPY IDSV0006
	 * *************************************************************
	 *  COPY X GESTIONE GUIDA DA SEQUENZIALE
	 *  N.B. - DA UTILIZZARE CON LA COPY IABPSQG1
	 * *************************************************************</pre>*/
    private String fsSeqguide = "00";
    //Original name: NOME-SEQGUIDE
    private String nomeSeqguide = "SEQGUIDE";
    /**Original name: STATO-SEQGUIDE<br>
	 * <pre>*****************************************************************
	 *  FLAG DI CONTROLLO x FILES SEQUENZIALI
	 * *****************************************************************</pre>*/
    private StatoSeqguide statoSeqguide = new StatoSeqguide();

    //==== METHODS ====
    public void setFsSeqguide(String fsSeqguide) {
        this.fsSeqguide = Functions.subString(fsSeqguide, Len.FS_SEQGUIDE);
    }

    public String getFsSeqguide() {
        return this.fsSeqguide;
    }

    public String getNomeSeqguide() {
        return this.nomeSeqguide;
    }

    public StatoSeqguide getStatoSeqguide() {
        return statoSeqguide;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FS_SEQGUIDE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
