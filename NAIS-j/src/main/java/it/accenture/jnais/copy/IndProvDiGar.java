package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-PROV-DI-GAR<br>
 * Variable: IND-PROV-DI-GAR from copybook IDBVPVT2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndProvDiGar {

    //==== PROPERTIES ====
    //Original name: IND-PVT-ID-GAR
    private short idGar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-TP-PROV
    private short tpProv = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-PROV-1AA-ACQ
    private short prov1aaAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-PROV-2AA-ACQ
    private short prov2aaAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-PROV-RICOR
    private short provRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-PROV-INC
    private short provInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-ALQ-PROV-ACQ
    private short alqProvAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-ALQ-PROV-INC
    private short alqProvInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-ALQ-PROV-RICOR
    private short alqProvRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-FL-STOR-PROV-ACQ
    private short flStorProvAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-FL-REC-PROV-STORN
    private short flRecProvStorn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-FL-PROV-FORZ
    private short flProvForz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-TP-CALC-PROV
    private short tpCalcProv = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-REMUN-ASS
    private short remunAss = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-COMMIS-INTER
    private short commisInter = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-ALQ-REMUN-ASS
    private short alqRemunAss = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-PVT-ALQ-COMMIS-INTER
    private short alqCommisInter = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdGar(short idGar) {
        this.idGar = idGar;
    }

    public short getIdGar() {
        return this.idGar;
    }

    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setTpProv(short tpProv) {
        this.tpProv = tpProv;
    }

    public short getTpProv() {
        return this.tpProv;
    }

    public void setProv1aaAcq(short prov1aaAcq) {
        this.prov1aaAcq = prov1aaAcq;
    }

    public short getProv1aaAcq() {
        return this.prov1aaAcq;
    }

    public void setProv2aaAcq(short prov2aaAcq) {
        this.prov2aaAcq = prov2aaAcq;
    }

    public short getProv2aaAcq() {
        return this.prov2aaAcq;
    }

    public void setProvRicor(short provRicor) {
        this.provRicor = provRicor;
    }

    public short getProvRicor() {
        return this.provRicor;
    }

    public void setProvInc(short provInc) {
        this.provInc = provInc;
    }

    public short getProvInc() {
        return this.provInc;
    }

    public void setAlqProvAcq(short alqProvAcq) {
        this.alqProvAcq = alqProvAcq;
    }

    public short getAlqProvAcq() {
        return this.alqProvAcq;
    }

    public void setAlqProvInc(short alqProvInc) {
        this.alqProvInc = alqProvInc;
    }

    public short getAlqProvInc() {
        return this.alqProvInc;
    }

    public void setAlqProvRicor(short alqProvRicor) {
        this.alqProvRicor = alqProvRicor;
    }

    public short getAlqProvRicor() {
        return this.alqProvRicor;
    }

    public void setFlStorProvAcq(short flStorProvAcq) {
        this.flStorProvAcq = flStorProvAcq;
    }

    public short getFlStorProvAcq() {
        return this.flStorProvAcq;
    }

    public void setFlRecProvStorn(short flRecProvStorn) {
        this.flRecProvStorn = flRecProvStorn;
    }

    public short getFlRecProvStorn() {
        return this.flRecProvStorn;
    }

    public void setFlProvForz(short flProvForz) {
        this.flProvForz = flProvForz;
    }

    public short getFlProvForz() {
        return this.flProvForz;
    }

    public void setTpCalcProv(short tpCalcProv) {
        this.tpCalcProv = tpCalcProv;
    }

    public short getTpCalcProv() {
        return this.tpCalcProv;
    }

    public void setRemunAss(short remunAss) {
        this.remunAss = remunAss;
    }

    public short getRemunAss() {
        return this.remunAss;
    }

    public void setCommisInter(short commisInter) {
        this.commisInter = commisInter;
    }

    public short getCommisInter() {
        return this.commisInter;
    }

    public void setAlqRemunAss(short alqRemunAss) {
        this.alqRemunAss = alqRemunAss;
    }

    public short getAlqRemunAss() {
        return this.alqRemunAss;
    }

    public void setAlqCommisInter(short alqCommisInter) {
        this.alqCommisInter = alqCommisInter;
    }

    public short getAlqCommisInter() {
        return this.alqCommisInter;
    }
}
