package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV3101<br>
 * Variable: LDBV3101 from copybook LDBV3101<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv3101 {

    //==== PROPERTIES ====
    //Original name: LDBV3101-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV3101-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV3101-TP-STA-TIT
    private String tpStaTit = DefaultValues.stringVal(Len.TP_STA_TIT);
    //Original name: LDBV3101-TP-TIT
    private String tpTit = DefaultValues.stringVal(Len.TP_TIT);
    //Original name: LDBV3101-DT-DA
    private int dtDa = DefaultValues.INT_VAL;
    //Original name: LDBV3101-DT-DA-DB
    private String dtDaDb = "";
    //Original name: LDBV3101-DT-A
    private int dtA = DefaultValues.INT_VAL;
    //Original name: LDBV3101-DT-A-DB
    private String dtADb = "";

    //==== METHODS ====
    public void setLdbv3101Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV3101];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV3101);
        setLdbv3101Bytes(buffer, 1);
    }

    public String getLdbv3101Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv3101Bytes());
    }

    public byte[] getLdbv3101Bytes() {
        byte[] buffer = new byte[Len.LDBV3101];
        return getLdbv3101Bytes(buffer, 1);
    }

    public void setLdbv3101Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpStaTit = MarshalByte.readString(buffer, position, Len.TP_STA_TIT);
        position += Len.TP_STA_TIT;
        tpTit = MarshalByte.readString(buffer, position, Len.TP_TIT);
        position += Len.TP_TIT;
        dtDa = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DA, 0);
        position += Len.DT_DA;
        dtDaDb = MarshalByte.readString(buffer, position, Len.DT_DA_DB);
        position += Len.DT_DA_DB;
        dtA = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_A, 0);
        position += Len.DT_A;
        dtADb = MarshalByte.readString(buffer, position, Len.DT_A_DB);
    }

    public byte[] getLdbv3101Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tpStaTit, Len.TP_STA_TIT);
        position += Len.TP_STA_TIT;
        MarshalByte.writeString(buffer, position, tpTit, Len.TP_TIT);
        position += Len.TP_TIT;
        MarshalByte.writeIntAsPacked(buffer, position, dtDa, Len.Int.DT_DA, 0);
        position += Len.DT_DA;
        MarshalByte.writeString(buffer, position, dtDaDb, Len.DT_DA_DB);
        position += Len.DT_DA_DB;
        MarshalByte.writeIntAsPacked(buffer, position, dtA, Len.Int.DT_A, 0);
        position += Len.DT_A;
        MarshalByte.writeString(buffer, position, dtADb, Len.DT_A_DB);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpStaTit(String tpStaTit) {
        this.tpStaTit = Functions.subString(tpStaTit, Len.TP_STA_TIT);
    }

    public String getTpStaTit() {
        return this.tpStaTit;
    }

    public void setTpTit(String tpTit) {
        this.tpTit = Functions.subString(tpTit, Len.TP_TIT);
    }

    public String getTpTit() {
        return this.tpTit;
    }

    public void setDtDa(int dtDa) {
        this.dtDa = dtDa;
    }

    public int getDtDa() {
        return this.dtDa;
    }

    public void setDtDaDb(String dtDaDb) {
        this.dtDaDb = Functions.subString(dtDaDb, Len.DT_DA_DB);
    }

    public String getDtDaDb() {
        return this.dtDaDb;
    }

    public void setDtA(int dtA) {
        this.dtA = dtA;
    }

    public int getDtA() {
        return this.dtA;
    }

    public void setDtADb(String dtADb) {
        this.dtADb = Functions.subString(dtADb, Len.DT_A_DB);
    }

    public String getDtADb() {
        return this.dtADb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int TP_STA_TIT = 2;
        public static final int TP_TIT = 2;
        public static final int ID_OGG = 5;
        public static final int DT_DA = 5;
        public static final int DT_DA_DB = 10;
        public static final int DT_A = 5;
        public static final int DT_A_DB = 10;
        public static final int LDBV3101 = ID_OGG + TP_OGG + TP_STA_TIT + TP_TIT + DT_DA + DT_DA_DB + DT_A + DT_A_DB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int DT_DA = 8;
            public static final int DT_A = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
