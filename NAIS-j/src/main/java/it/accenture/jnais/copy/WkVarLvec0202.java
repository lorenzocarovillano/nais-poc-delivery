package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.FgFineCiclo;

/**Original name: WK-VAR-LVEC0202<br>
 * Variable: WK-VAR-LVEC0202 from copybook LVEC0202<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVarLvec0202 {

    //==== PROPERTIES ====
    //Original name: WK-IB-PROP-APPO
    private String wkIbPropAppo = "";
    //Original name: WK-IB-PROP-APPO2
    private String wkIbPropAppo2 = "";
    //Original name: WK-LUNG-CAMPO
    private String wkLungCampo = "00";
    //Original name: WK-LUNG-TOT-CAMPO
    private String wkLungTotCampo = "00";
    //Original name: WK-NUM-CAR-RIMAN
    private short wkNumCarRiman = ((short)0);
    //Original name: WK-NUM-CAR-ZERO
    private short wkNumCarZero = ((short)0);
    //Original name: IX-TAB
    private short ixTab = DefaultValues.BIN_SHORT_VAL;
    //Original name: FG-FINE-CICLO
    private FgFineCiclo fgFineCiclo = new FgFineCiclo();

    //==== METHODS ====
    public void setWkIbPropAppo(String wkIbPropAppo) {
        this.wkIbPropAppo = Functions.subString(wkIbPropAppo, Len.WK_IB_PROP_APPO);
    }

    public String getWkIbPropAppo() {
        return this.wkIbPropAppo;
    }

    public String getWkIbPropAppoFormatted() {
        return Functions.padBlanks(getWkIbPropAppo(), Len.WK_IB_PROP_APPO);
    }

    public void setWkIbPropAppo2(String wkIbPropAppo2) {
        this.wkIbPropAppo2 = Functions.subString(wkIbPropAppo2, Len.WK_IB_PROP_APPO2);
    }

    public String getWkIbPropAppo2() {
        return this.wkIbPropAppo2;
    }

    public void setWkLungCampo(short wkLungCampo) {
        this.wkLungCampo = NumericDisplay.asString(wkLungCampo, Len.WK_LUNG_CAMPO);
    }

    public void setWkLungCampoFormatted(String wkLungCampo) {
        this.wkLungCampo = Trunc.toUnsignedNumeric(wkLungCampo, Len.WK_LUNG_CAMPO);
    }

    public short getWkLungCampo() {
        return NumericDisplay.asShort(this.wkLungCampo);
    }

    public void setWkLungTotCampo(short wkLungTotCampo) {
        this.wkLungTotCampo = NumericDisplay.asString(wkLungTotCampo, Len.WK_LUNG_TOT_CAMPO);
    }

    public short getWkLungTotCampo() {
        return NumericDisplay.asShort(this.wkLungTotCampo);
    }

    public String getWkLungTotCampoFormatted() {
        return this.wkLungTotCampo;
    }

    public void setWkNumCarRiman(short wkNumCarRiman) {
        this.wkNumCarRiman = wkNumCarRiman;
    }

    public short getWkNumCarRiman() {
        return this.wkNumCarRiman;
    }

    public void setWkNumCarZero(short wkNumCarZero) {
        this.wkNumCarZero = wkNumCarZero;
    }

    public short getWkNumCarZero() {
        return this.wkNumCarZero;
    }

    public void setIxTab(short ixTab) {
        this.ixTab = ixTab;
    }

    public short getIxTab() {
        return this.ixTab;
    }

    public FgFineCiclo getFgFineCiclo() {
        return fgFineCiclo;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_LUNG_CAMPO = 2;
        public static final int WK_LUNG_TOT_CAMPO = 2;
        public static final int WK_NUM_CAR_RIMAN = 2;
        public static final int WK_NUM_CAR_ZERO = 2;
        public static final int WK_IB_PROP_APPO = 40;
        public static final int WK_IB_PROP_APPO2 = 40;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
