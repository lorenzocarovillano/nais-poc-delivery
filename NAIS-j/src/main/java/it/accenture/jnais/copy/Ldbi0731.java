package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBI0731<br>
 * Variable: LDBI0731 from copybook LDBI0731<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbi0731 {

    //==== PROPERTIES ====
    //Original name: LDBI0731-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LDBI0731-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LDBI0731-ID-TRCH
    private int idTrch = DefaultValues.INT_VAL;
    //Original name: LDBI0731-TP-CALL
    private String tpCall = DefaultValues.stringVal(Len.TP_CALL);
    //Original name: LDBI0731-TP-STAT-BUS
    private String tpStatBus = DefaultValues.stringVal(Len.TP_STAT_BUS);
    //Original name: LDBI0731-TRASFORMATA
    private char trasformata = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setLdbi0731Formatted(String data) {
        byte[] buffer = new byte[Len.LDBI0731];
        MarshalByte.writeString(buffer, 1, data, Len.LDBI0731);
        setLdbi0731Bytes(buffer, 1);
    }

    public String getLdbi0731Formatted() {
        return MarshalByteExt.bufferToStr(getLdbi0731Bytes());
    }

    public byte[] getLdbi0731Bytes() {
        byte[] buffer = new byte[Len.LDBI0731];
        return getLdbi0731Bytes(buffer, 1);
    }

    public void setLdbi0731Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        idTrch = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_TRCH, 0);
        position += Len.ID_TRCH;
        tpCall = MarshalByte.readString(buffer, position, Len.TP_CALL);
        position += Len.TP_CALL;
        tpStatBus = MarshalByte.readString(buffer, position, Len.TP_STAT_BUS);
        position += Len.TP_STAT_BUS;
        trasformata = MarshalByte.readChar(buffer, position);
    }

    public byte[] getLdbi0731Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, idTrch, Len.Int.ID_TRCH, 0);
        position += Len.ID_TRCH;
        MarshalByte.writeString(buffer, position, tpCall, Len.TP_CALL);
        position += Len.TP_CALL;
        MarshalByte.writeString(buffer, position, tpStatBus, Len.TP_STAT_BUS);
        position += Len.TP_STAT_BUS;
        MarshalByte.writeChar(buffer, position, trasformata);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setIdTrch(int idTrch) {
        this.idTrch = idTrch;
    }

    public int getIdTrch() {
        return this.idTrch;
    }

    public void setTpCall(String tpCall) {
        this.tpCall = Functions.subString(tpCall, Len.TP_CALL);
    }

    public String getTpCall() {
        return this.tpCall;
    }

    public void setTpStatBus(String tpStatBus) {
        this.tpStatBus = Functions.subString(tpStatBus, Len.TP_STAT_BUS);
    }

    public String getTpStatBus() {
        return this.tpStatBus;
    }

    public void setTrasformata(char trasformata) {
        this.trasformata = trasformata;
    }

    public char getTrasformata() {
        return this.trasformata;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_CALL = 2;
        public static final int TP_STAT_BUS = 2;
        public static final int ID_POLI = 5;
        public static final int ID_ADES = 5;
        public static final int ID_TRCH = 5;
        public static final int TRASFORMATA = 1;
        public static final int LDBI0731 = ID_POLI + ID_ADES + ID_TRCH + TP_CALL + TP_STAT_BUS + TRASFORMATA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int ID_ADES = 9;
            public static final int ID_TRCH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
