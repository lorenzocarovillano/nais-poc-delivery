package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVRRE1<br>
 * Variable: LCCVRRE1 from copybook LCCVRRE1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvrre1 {

    //==== PROPERTIES ====
    /**Original name: WRRE-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA RAPP_RETE
	 *    ALIAS RRE
	 *    ULTIMO AGG. 01 OTT 2014
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WRRE-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WRRE-DATI
    private WrreDati dati = new WrreDati();

    //==== METHODS ====
    public void initLccvrre1Spaces() {
        status.setStatus(Types.SPACE_CHAR);
        idPtf = Types.INVALID_INT_VAL;
        dati.initDatiSpaces();
    }

    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WrreDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
