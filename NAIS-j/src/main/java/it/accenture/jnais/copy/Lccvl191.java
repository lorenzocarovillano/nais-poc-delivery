package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVL191<br>
 * Copybook: LCCVL191 from copybook LCCVL191<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvl191 {

    //==== PROPERTIES ====
    /**Original name: WL19-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA QUOTZ_FND_UNIT
	 *    ALIAS L19
	 *    ULTIMO AGG. 24 APR 2015
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WL19-DATI
    private Wl19Dati dati = new Wl19Dati();

    //==== METHODS ====
    public void initLccvl191Spaces() {
        status.setStatus(Types.SPACE_CHAR);
        dati.initDatiSpaces();
    }

    public Wl19Dati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }
}
