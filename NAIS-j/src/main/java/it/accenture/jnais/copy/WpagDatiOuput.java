package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.WpagParamQuestsan;
import it.accenture.jnais.ws.enums.WpagParamQuestsanSpeciale;
import it.accenture.jnais.ws.occurs.WpagTabAsset;
import it.accenture.jnais.ws.occurs.WpagTabCalcoli;
import it.accenture.jnais.ws.redefines.WpagDtEndCopTit;

/**Original name: WPAG-DATI-OUPUT<br>
 * Variable: WPAG-DATI-OUPUT from copybook LVEC0268<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WpagDatiOuput {

    //==== PROPERTIES ====
    public static final int WPAG_TAB_ASSET_MAXOCCURS = 250;
    public static final int WPAG_TAB_CALCOLI_MAXOCCURS = 20;
    /**Original name: WPAG-ELE-ASSET-MAX<br>
	 * <pre>--  Tabella Asset per fondi</pre>*/
    private short wpagEleAssetMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPAG-TAB-ASSET
    private WpagTabAsset[] wpagTabAsset = new WpagTabAsset[WPAG_TAB_ASSET_MAXOCCURS];
    /**Original name: WPAG-PARAM-QUESTSAN<br>
	 * <pre>--  Parametro questionario</pre>*/
    private WpagParamQuestsan wpagParamQuestsan = new WpagParamQuestsan();
    /**Original name: WPAG-PARAM-QUESTSAN-SPECIALE<br>
	 * <pre>--  Parametro questionario SPECIALE</pre>*/
    private WpagParamQuestsanSpeciale wpagParamQuestsanSpeciale = new WpagParamQuestsanSpeciale();
    /**Original name: WPAG-NUM-RATE-ACC<br>
	 * <pre>--  Numero di rate accorpate</pre>*/
    private short wpagNumRateAcc = DefaultValues.SHORT_VAL;
    /**Original name: WPAG-RATE-RETRODATATE<br>
	 * <pre>--  Numero di rate retrodatate</pre>*/
    private short wpagRateRetrodatate = DefaultValues.SHORT_VAL;
    //Original name: WPAG-TP-IAS-ADE
    private String wpagTpIasAde = DefaultValues.stringVal(Len.WPAG_TP_IAS_ADE);
    //Original name: WPAG-TP-MOD-PROV
    private char wpagTpModProv = DefaultValues.CHAR_VAL;
    /**Original name: WPAG-ELE-CALCOLI-MAX<br>
	 * <pre>--  Tabella mappatura componente --> Garanzia</pre>*/
    private short wpagEleCalcoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPAG-TAB-CALCOLI
    private WpagTabCalcoli[] wpagTabCalcoli = new WpagTabCalcoli[WPAG_TAB_CALCOLI_MAXOCCURS];
    /**Original name: WPAG-DT-END-COP-TIT<br>
	 * <pre>--- Data fine copertura titolo contabile</pre>*/
    private WpagDtEndCopTit wpagDtEndCopTit = new WpagDtEndCopTit();
    /**Original name: WPAG-DT-EFF-CALCOLO-I<br>
	 * <pre>--- Data effetto calcolo</pre>*/
    private String wpagDtEffCalcoloI = DefaultValues.stringVal(Len.WPAG_DT_EFF_CALCOLO_I);

    //==== CONSTRUCTORS ====
    public WpagDatiOuput() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wpagTabAssetIdx = 1; wpagTabAssetIdx <= WPAG_TAB_ASSET_MAXOCCURS; wpagTabAssetIdx++) {
            wpagTabAsset[wpagTabAssetIdx - 1] = new WpagTabAsset();
        }
        for (int wpagTabCalcoliIdx = 1; wpagTabCalcoliIdx <= WPAG_TAB_CALCOLI_MAXOCCURS; wpagTabCalcoliIdx++) {
            wpagTabCalcoli[wpagTabCalcoliIdx - 1] = new WpagTabCalcoli();
        }
    }

    public void setDatiOuputBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagEleAssetMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_TAB_ASSET_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpagTabAsset[idx - 1].setWpagTabAssetBytes(buffer, position);
                position += WpagTabAsset.Len.WPAG_TAB_ASSET;
            }
            else {
                wpagTabAsset[idx - 1].initWpagTabAssetSpaces();
                position += WpagTabAsset.Len.WPAG_TAB_ASSET;
            }
        }
        wpagParamQuestsan.setWpagParamQuestsan(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        wpagParamQuestsanSpeciale.setWpagParamQuestsanSpeciale(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        wpagNumRateAcc = MarshalByte.readPackedAsShort(buffer, position, Len.Int.WPAG_NUM_RATE_ACC, 0);
        position += Len.WPAG_NUM_RATE_ACC;
        wpagRateRetrodatate = MarshalByte.readPackedAsShort(buffer, position, Len.Int.WPAG_RATE_RETRODATATE, 0);
        position += Len.WPAG_RATE_RETRODATATE;
        setWpagDatiProdottoBytes(buffer, position);
        position += Len.WPAG_DATI_PRODOTTO;
        wpagEleCalcoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_TAB_CALCOLI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpagTabCalcoli[idx - 1].setWpagTabCalcoliBytes(buffer, position);
                position += WpagTabCalcoli.Len.WPAG_TAB_CALCOLI;
            }
            else {
                wpagTabCalcoli[idx - 1].initWpagTabCalcoliSpaces();
                position += WpagTabCalcoli.Len.WPAG_TAB_CALCOLI;
            }
        }
        wpagDtEndCopTit.setWpagDtEndCopTitFromBuffer(buffer, position);
        position += WpagDtEndCopTit.Len.WPAG_DT_END_COP_TIT;
        wpagDtEffCalcoloI = MarshalByte.readFixedString(buffer, position, Len.WPAG_DT_EFF_CALCOLO_I);
    }

    public byte[] getDatiOuputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpagEleAssetMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_TAB_ASSET_MAXOCCURS; idx++) {
            wpagTabAsset[idx - 1].getWpagTabAssetBytes(buffer, position);
            position += WpagTabAsset.Len.WPAG_TAB_ASSET;
        }
        MarshalByte.writeChar(buffer, position, wpagParamQuestsan.getWpagParamQuestsan());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpagParamQuestsanSpeciale.getWpagParamQuestsanSpeciale());
        position += Types.CHAR_SIZE;
        MarshalByte.writeShortAsPacked(buffer, position, wpagNumRateAcc, Len.Int.WPAG_NUM_RATE_ACC, 0);
        position += Len.WPAG_NUM_RATE_ACC;
        MarshalByte.writeShortAsPacked(buffer, position, wpagRateRetrodatate, Len.Int.WPAG_RATE_RETRODATATE, 0);
        position += Len.WPAG_RATE_RETRODATATE;
        getWpagDatiProdottoBytes(buffer, position);
        position += Len.WPAG_DATI_PRODOTTO;
        MarshalByte.writeBinaryShort(buffer, position, wpagEleCalcoliMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_TAB_CALCOLI_MAXOCCURS; idx++) {
            wpagTabCalcoli[idx - 1].getWpagTabCalcoliBytes(buffer, position);
            position += WpagTabCalcoli.Len.WPAG_TAB_CALCOLI;
        }
        wpagDtEndCopTit.getWpagDtEndCopTitAsBuffer(buffer, position);
        position += WpagDtEndCopTit.Len.WPAG_DT_END_COP_TIT;
        MarshalByte.writeString(buffer, position, wpagDtEffCalcoloI, Len.WPAG_DT_EFF_CALCOLO_I);
        return buffer;
    }

    public void setWpagEleAssetMax(short wpagEleAssetMax) {
        this.wpagEleAssetMax = wpagEleAssetMax;
    }

    public short getWpagEleAssetMax() {
        return this.wpagEleAssetMax;
    }

    public void setWpagNumRateAcc(short wpagNumRateAcc) {
        this.wpagNumRateAcc = wpagNumRateAcc;
    }

    public short getWpagNumRateAcc() {
        return this.wpagNumRateAcc;
    }

    public void setWpagRateRetrodatate(short wpagRateRetrodatate) {
        this.wpagRateRetrodatate = wpagRateRetrodatate;
    }

    public short getWpagRateRetrodatate() {
        return this.wpagRateRetrodatate;
    }

    public void setWpagDatiProdottoBytes(byte[] buffer, int offset) {
        int position = offset;
        setWpagDatiAdesioneBytes(buffer, position);
        position += Len.WPAG_DATI_ADESIONE;
        setWpagTpProvBytes(buffer, position);
    }

    public byte[] getWpagDatiProdottoBytes(byte[] buffer, int offset) {
        int position = offset;
        getWpagDatiAdesioneBytes(buffer, position);
        position += Len.WPAG_DATI_ADESIONE;
        getWpagTpProvBytes(buffer, position);
        return buffer;
    }

    public void setWpagDatiAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagTpIasAde = MarshalByte.readString(buffer, position, Len.WPAG_TP_IAS_ADE);
    }

    public byte[] getWpagDatiAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wpagTpIasAde, Len.WPAG_TP_IAS_ADE);
        return buffer;
    }

    public void setWpagTpIasAde(String wpagTpIasAde) {
        this.wpagTpIasAde = Functions.subString(wpagTpIasAde, Len.WPAG_TP_IAS_ADE);
    }

    public String getWpagTpIasAde() {
        return this.wpagTpIasAde;
    }

    public void setWpagTpProvBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagTpModProv = MarshalByte.readChar(buffer, position);
    }

    public byte[] getWpagTpProvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, wpagTpModProv);
        return buffer;
    }

    public void setWpagTpModProv(char wpagTpModProv) {
        this.wpagTpModProv = wpagTpModProv;
    }

    public void setWpagTpModProvFormatted(String wpagTpModProv) {
        setWpagTpModProv(Functions.charAt(wpagTpModProv, Types.CHAR_SIZE));
    }

    public char getWpagTpModProv() {
        return this.wpagTpModProv;
    }

    public void setWpagEleCalcoliMax(short wpagEleCalcoliMax) {
        this.wpagEleCalcoliMax = wpagEleCalcoliMax;
    }

    public short getWpagEleCalcoliMax() {
        return this.wpagEleCalcoliMax;
    }

    public WpagDtEndCopTit getWpagDtEndCopTit() {
        return wpagDtEndCopTit;
    }

    public WpagParamQuestsan getWpagParamQuestsan() {
        return wpagParamQuestsan;
    }

    public WpagParamQuestsanSpeciale getWpagParamQuestsanSpeciale() {
        return wpagParamQuestsanSpeciale;
    }

    public WpagTabAsset getWpagTabAsset(int idx) {
        return wpagTabAsset[idx - 1];
    }

    public WpagTabCalcoli getWpagTabCalcoli(int idx) {
        return wpagTabCalcoli[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_ELE_ASSET_MAX = 2;
        public static final int WPAG_NUM_RATE_ACC = 3;
        public static final int WPAG_RATE_RETRODATATE = 3;
        public static final int WPAG_TP_IAS_ADE = 2;
        public static final int WPAG_DATI_ADESIONE = WPAG_TP_IAS_ADE;
        public static final int WPAG_TP_MOD_PROV = 1;
        public static final int WPAG_TP_PROV = WPAG_TP_MOD_PROV;
        public static final int WPAG_DATI_PRODOTTO = WPAG_DATI_ADESIONE + WPAG_TP_PROV;
        public static final int WPAG_ELE_CALCOLI_MAX = 2;
        public static final int WPAG_DT_EFF_CALCOLO_I = 8;
        public static final int DATI_OUPUT = WPAG_ELE_ASSET_MAX + WpagDatiOuput.WPAG_TAB_ASSET_MAXOCCURS * WpagTabAsset.Len.WPAG_TAB_ASSET + WpagParamQuestsan.Len.WPAG_PARAM_QUESTSAN + WpagParamQuestsanSpeciale.Len.WPAG_PARAM_QUESTSAN_SPECIALE + WPAG_NUM_RATE_ACC + WPAG_RATE_RETRODATATE + WPAG_DATI_PRODOTTO + WPAG_ELE_CALCOLI_MAX + WpagDatiOuput.WPAG_TAB_CALCOLI_MAXOCCURS * WpagTabCalcoli.Len.WPAG_TAB_CALCOLI + WpagDtEndCopTit.Len.WPAG_DT_END_COP_TIT + WPAG_DT_EFF_CALCOLO_I;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_NUM_RATE_ACC = 4;
            public static final int WPAG_RATE_RETRODATATE = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
