package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVL193<br>
 * Copybook: IDBVL193 from copybook IDBVL193<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvl193 {

    //==== PROPERTIES ====
    //Original name: L19-DT-QTZ-DB
    private String l19DtQtzDb = DefaultValues.stringVal(Len.L19_DT_QTZ_DB);
    //Original name: L19-DT-RILEVAZIONE-NAV-DB
    private String l19DtRilevazioneNavDb = DefaultValues.stringVal(Len.L19_DT_RILEVAZIONE_NAV_DB);

    //==== METHODS ====
    public void setL19DtQtzDb(String l19DtQtzDb) {
        this.l19DtQtzDb = Functions.subString(l19DtQtzDb, Len.L19_DT_QTZ_DB);
    }

    public String getL19DtQtzDb() {
        return this.l19DtQtzDb;
    }

    public void setL19DtRilevazioneNavDb(String l19DtRilevazioneNavDb) {
        this.l19DtRilevazioneNavDb = Functions.subString(l19DtRilevazioneNavDb, Len.L19_DT_RILEVAZIONE_NAV_DB);
    }

    public String getL19DtRilevazioneNavDb() {
        return this.l19DtRilevazioneNavDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L19_DT_QTZ_DB = 10;
        public static final int L19_DT_RILEVAZIONE_NAV_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
