package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WB04IdRichEstrazAggLlbs0230;
import it.accenture.jnais.ws.redefines.WB04ValImpLlbs0230;
import it.accenture.jnais.ws.redefines.WB04ValPcLlbs0230;

/**Original name: W-B04-REC-FISSO<br>
 * Variable: W-B04-REC-FISSO from copybook LCCVB048<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WB04RecFisso {

    //==== PROPERTIES ====
    //Original name: W-B04-ID-BILA-VAR-CALC-P
    private int wB04IdBilaVarCalcP = DefaultValues.INT_VAL;
    //Original name: W-B04-COD-COMP-ANIA
    private int wB04CodCompAnia = DefaultValues.INT_VAL;
    //Original name: W-B04-ID-RICH-ESTRAZ-MAS
    private int wB04IdRichEstrazMas = DefaultValues.INT_VAL;
    //Original name: W-B04-ID-RICH-ESTRAZ-AGG-IND
    private char wB04IdRichEstrazAggInd = DefaultValues.CHAR_VAL;
    //Original name: W-B04-ID-RICH-ESTRAZ-AGG
    private WB04IdRichEstrazAggLlbs0230 wB04IdRichEstrazAgg = new WB04IdRichEstrazAggLlbs0230();
    //Original name: W-B04-DT-RIS
    private String wB04DtRis = DefaultValues.stringVal(Len.W_B04_DT_RIS);
    //Original name: W-B04-ID-POLI
    private int wB04IdPoli = DefaultValues.INT_VAL;
    //Original name: W-B04-ID-ADES
    private int wB04IdAdes = DefaultValues.INT_VAL;
    //Original name: W-B04-PROG-SCHEDA-VALOR
    private int wB04ProgSchedaValor = DefaultValues.INT_VAL;
    //Original name: W-B04-TP-RGM-FISC
    private String wB04TpRgmFisc = DefaultValues.stringVal(Len.W_B04_TP_RGM_FISC);
    //Original name: W-B04-DT-INI-VLDT-PROD
    private String wB04DtIniVldtProd = DefaultValues.stringVal(Len.W_B04_DT_INI_VLDT_PROD);
    //Original name: W-B04-COD-VAR
    private String wB04CodVar = DefaultValues.stringVal(Len.W_B04_COD_VAR);
    //Original name: W-B04-TP-D
    private char wB04TpD = DefaultValues.CHAR_VAL;
    //Original name: W-B04-VAL-IMP-IND
    private char wB04ValImpInd = DefaultValues.CHAR_VAL;
    //Original name: W-B04-VAL-IMP
    private WB04ValImpLlbs0230 wB04ValImp = new WB04ValImpLlbs0230();
    //Original name: W-B04-VAL-PC-IND
    private char wB04ValPcInd = DefaultValues.CHAR_VAL;
    //Original name: W-B04-VAL-PC
    private WB04ValPcLlbs0230 wB04ValPc = new WB04ValPcLlbs0230();
    //Original name: W-B04-VAL-STRINGA-IND
    private char wB04ValStringaInd = DefaultValues.CHAR_VAL;
    //Original name: W-B04-VAL-STRINGA
    private String wB04ValStringa = DefaultValues.stringVal(Len.W_B04_VAL_STRINGA);
    //Original name: W-B04-DS-OPER-SQL
    private char wB04DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: W-B04-DS-VER
    private int wB04DsVer = DefaultValues.INT_VAL;
    //Original name: W-B04-DS-TS-CPTZ
    private long wB04DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: W-B04-DS-UTENTE
    private String wB04DsUtente = DefaultValues.stringVal(Len.W_B04_DS_UTENTE);
    //Original name: W-B04-DS-STATO-ELAB
    private char wB04DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public byte[] getRecFissoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wB04IdBilaVarCalcP, Len.Int.W_B04_ID_BILA_VAR_CALC_P, 0);
        position += Len.W_B04_ID_BILA_VAR_CALC_P;
        MarshalByte.writeIntAsPacked(buffer, position, wB04CodCompAnia, Len.Int.W_B04_COD_COMP_ANIA, 0);
        position += Len.W_B04_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wB04IdRichEstrazMas, Len.Int.W_B04_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.W_B04_ID_RICH_ESTRAZ_MAS;
        MarshalByte.writeChar(buffer, position, wB04IdRichEstrazAggInd);
        position += Types.CHAR_SIZE;
        wB04IdRichEstrazAgg.getwB04IdRichEstrazAggAsBuffer(buffer, position);
        position += WB04IdRichEstrazAggLlbs0230.Len.W_B04_ID_RICH_ESTRAZ_AGG;
        MarshalByte.writeString(buffer, position, wB04DtRis, Len.W_B04_DT_RIS);
        position += Len.W_B04_DT_RIS;
        MarshalByte.writeIntAsPacked(buffer, position, wB04IdPoli, Len.Int.W_B04_ID_POLI, 0);
        position += Len.W_B04_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wB04IdAdes, Len.Int.W_B04_ID_ADES, 0);
        position += Len.W_B04_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wB04ProgSchedaValor, Len.Int.W_B04_PROG_SCHEDA_VALOR, 0);
        position += Len.W_B04_PROG_SCHEDA_VALOR;
        MarshalByte.writeString(buffer, position, wB04TpRgmFisc, Len.W_B04_TP_RGM_FISC);
        position += Len.W_B04_TP_RGM_FISC;
        MarshalByte.writeString(buffer, position, wB04DtIniVldtProd, Len.W_B04_DT_INI_VLDT_PROD);
        position += Len.W_B04_DT_INI_VLDT_PROD;
        MarshalByte.writeString(buffer, position, wB04CodVar, Len.W_B04_COD_VAR);
        position += Len.W_B04_COD_VAR;
        MarshalByte.writeChar(buffer, position, wB04TpD);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB04ValImpInd);
        position += Types.CHAR_SIZE;
        wB04ValImp.getwB04ValImpAsBuffer(buffer, position);
        position += WB04ValImpLlbs0230.Len.W_B04_VAL_IMP;
        MarshalByte.writeChar(buffer, position, wB04ValPcInd);
        position += Types.CHAR_SIZE;
        wB04ValPc.getwB04ValPcAsBuffer(buffer, position);
        position += WB04ValPcLlbs0230.Len.W_B04_VAL_PC;
        MarshalByte.writeChar(buffer, position, wB04ValStringaInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB04ValStringa, Len.W_B04_VAL_STRINGA);
        position += Len.W_B04_VAL_STRINGA;
        MarshalByte.writeChar(buffer, position, wB04DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wB04DsVer, Len.Int.W_B04_DS_VER, 0);
        position += Len.W_B04_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wB04DsTsCptz, Len.Int.W_B04_DS_TS_CPTZ, 0);
        position += Len.W_B04_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wB04DsUtente, Len.W_B04_DS_UTENTE);
        position += Len.W_B04_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wB04DsStatoElab);
        return buffer;
    }

    public void setwB04IdBilaVarCalcP(int wB04IdBilaVarCalcP) {
        this.wB04IdBilaVarCalcP = wB04IdBilaVarCalcP;
    }

    public int getwB04IdBilaVarCalcP() {
        return this.wB04IdBilaVarCalcP;
    }

    public void setwB04CodCompAnia(int wB04CodCompAnia) {
        this.wB04CodCompAnia = wB04CodCompAnia;
    }

    public int getwB04CodCompAnia() {
        return this.wB04CodCompAnia;
    }

    public void setwB04IdRichEstrazMas(int wB04IdRichEstrazMas) {
        this.wB04IdRichEstrazMas = wB04IdRichEstrazMas;
    }

    public int getwB04IdRichEstrazMas() {
        return this.wB04IdRichEstrazMas;
    }

    public void setwB04IdRichEstrazAggInd(char wB04IdRichEstrazAggInd) {
        this.wB04IdRichEstrazAggInd = wB04IdRichEstrazAggInd;
    }

    public void setwB04IdRichEstrazAggIndFormatted(String wB04IdRichEstrazAggInd) {
        setwB04IdRichEstrazAggInd(Functions.charAt(wB04IdRichEstrazAggInd, Types.CHAR_SIZE));
    }

    public char getwB04IdRichEstrazAggInd() {
        return this.wB04IdRichEstrazAggInd;
    }

    public void setwB04DtRis(String wB04DtRis) {
        this.wB04DtRis = Functions.subString(wB04DtRis, Len.W_B04_DT_RIS);
    }

    public String getwB04DtRis() {
        return this.wB04DtRis;
    }

    public void setwB04IdPoli(int wB04IdPoli) {
        this.wB04IdPoli = wB04IdPoli;
    }

    public int getwB04IdPoli() {
        return this.wB04IdPoli;
    }

    public void setwB04IdAdes(int wB04IdAdes) {
        this.wB04IdAdes = wB04IdAdes;
    }

    public int getwB04IdAdes() {
        return this.wB04IdAdes;
    }

    public void setwB04ProgSchedaValor(int wB04ProgSchedaValor) {
        this.wB04ProgSchedaValor = wB04ProgSchedaValor;
    }

    public int getwB04ProgSchedaValor() {
        return this.wB04ProgSchedaValor;
    }

    public void setwB04TpRgmFisc(String wB04TpRgmFisc) {
        this.wB04TpRgmFisc = Functions.subString(wB04TpRgmFisc, Len.W_B04_TP_RGM_FISC);
    }

    public String getwB04TpRgmFisc() {
        return this.wB04TpRgmFisc;
    }

    public void setwB04DtIniVldtProd(String wB04DtIniVldtProd) {
        this.wB04DtIniVldtProd = Functions.subString(wB04DtIniVldtProd, Len.W_B04_DT_INI_VLDT_PROD);
    }

    public String getwB04DtIniVldtProd() {
        return this.wB04DtIniVldtProd;
    }

    public void setwB04CodVar(String wB04CodVar) {
        this.wB04CodVar = Functions.subString(wB04CodVar, Len.W_B04_COD_VAR);
    }

    public String getwB04CodVar() {
        return this.wB04CodVar;
    }

    public void setwB04TpD(char wB04TpD) {
        this.wB04TpD = wB04TpD;
    }

    public char getwB04TpD() {
        return this.wB04TpD;
    }

    public void setwB04ValImpInd(char wB04ValImpInd) {
        this.wB04ValImpInd = wB04ValImpInd;
    }

    public void setwB04ValImpIndFormatted(String wB04ValImpInd) {
        setwB04ValImpInd(Functions.charAt(wB04ValImpInd, Types.CHAR_SIZE));
    }

    public char getwB04ValImpInd() {
        return this.wB04ValImpInd;
    }

    public void setwB04ValPcInd(char wB04ValPcInd) {
        this.wB04ValPcInd = wB04ValPcInd;
    }

    public void setwB04ValPcIndFormatted(String wB04ValPcInd) {
        setwB04ValPcInd(Functions.charAt(wB04ValPcInd, Types.CHAR_SIZE));
    }

    public char getwB04ValPcInd() {
        return this.wB04ValPcInd;
    }

    public void setwB04ValStringaInd(char wB04ValStringaInd) {
        this.wB04ValStringaInd = wB04ValStringaInd;
    }

    public void setwB04ValStringaIndFormatted(String wB04ValStringaInd) {
        setwB04ValStringaInd(Functions.charAt(wB04ValStringaInd, Types.CHAR_SIZE));
    }

    public char getwB04ValStringaInd() {
        return this.wB04ValStringaInd;
    }

    public void setwB04ValStringa(String wB04ValStringa) {
        this.wB04ValStringa = Functions.subString(wB04ValStringa, Len.W_B04_VAL_STRINGA);
    }

    public String getwB04ValStringa() {
        return this.wB04ValStringa;
    }

    public void setwB04DsOperSql(char wB04DsOperSql) {
        this.wB04DsOperSql = wB04DsOperSql;
    }

    public void setwB04DsOperSqlFormatted(String wB04DsOperSql) {
        setwB04DsOperSql(Functions.charAt(wB04DsOperSql, Types.CHAR_SIZE));
    }

    public char getwB04DsOperSql() {
        return this.wB04DsOperSql;
    }

    public void setwB04DsVer(int wB04DsVer) {
        this.wB04DsVer = wB04DsVer;
    }

    public int getwB04DsVer() {
        return this.wB04DsVer;
    }

    public void setwB04DsTsCptz(long wB04DsTsCptz) {
        this.wB04DsTsCptz = wB04DsTsCptz;
    }

    public long getwB04DsTsCptz() {
        return this.wB04DsTsCptz;
    }

    public void setwB04DsUtente(String wB04DsUtente) {
        this.wB04DsUtente = Functions.subString(wB04DsUtente, Len.W_B04_DS_UTENTE);
    }

    public String getwB04DsUtente() {
        return this.wB04DsUtente;
    }

    public void setwB04DsStatoElab(char wB04DsStatoElab) {
        this.wB04DsStatoElab = wB04DsStatoElab;
    }

    public void setwB04DsStatoElabFormatted(String wB04DsStatoElab) {
        setwB04DsStatoElab(Functions.charAt(wB04DsStatoElab, Types.CHAR_SIZE));
    }

    public char getwB04DsStatoElab() {
        return this.wB04DsStatoElab;
    }

    public WB04IdRichEstrazAggLlbs0230 getwB04IdRichEstrazAgg() {
        return wB04IdRichEstrazAgg;
    }

    public WB04ValImpLlbs0230 getwB04ValImp() {
        return wB04ValImp;
    }

    public WB04ValPcLlbs0230 getwB04ValPc() {
        return wB04ValPc;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B04_DT_RIS = 10;
        public static final int W_B04_TP_RGM_FISC = 2;
        public static final int W_B04_DT_INI_VLDT_PROD = 10;
        public static final int W_B04_COD_VAR = 30;
        public static final int W_B04_VAL_STRINGA = 60;
        public static final int W_B04_DS_UTENTE = 20;
        public static final int W_B04_ID_BILA_VAR_CALC_P = 5;
        public static final int W_B04_COD_COMP_ANIA = 3;
        public static final int W_B04_ID_RICH_ESTRAZ_MAS = 5;
        public static final int W_B04_ID_RICH_ESTRAZ_AGG_IND = 1;
        public static final int W_B04_ID_POLI = 5;
        public static final int W_B04_ID_ADES = 5;
        public static final int W_B04_PROG_SCHEDA_VALOR = 5;
        public static final int W_B04_TP_D = 1;
        public static final int W_B04_VAL_IMP_IND = 1;
        public static final int W_B04_VAL_PC_IND = 1;
        public static final int W_B04_VAL_STRINGA_IND = 1;
        public static final int W_B04_DS_OPER_SQL = 1;
        public static final int W_B04_DS_VER = 5;
        public static final int W_B04_DS_TS_CPTZ = 10;
        public static final int W_B04_DS_STATO_ELAB = 1;
        public static final int REC_FISSO = W_B04_ID_BILA_VAR_CALC_P + W_B04_COD_COMP_ANIA + W_B04_ID_RICH_ESTRAZ_MAS + W_B04_ID_RICH_ESTRAZ_AGG_IND + WB04IdRichEstrazAggLlbs0230.Len.W_B04_ID_RICH_ESTRAZ_AGG + W_B04_DT_RIS + W_B04_ID_POLI + W_B04_ID_ADES + W_B04_PROG_SCHEDA_VALOR + W_B04_TP_RGM_FISC + W_B04_DT_INI_VLDT_PROD + W_B04_COD_VAR + W_B04_TP_D + W_B04_VAL_IMP_IND + WB04ValImpLlbs0230.Len.W_B04_VAL_IMP + W_B04_VAL_PC_IND + WB04ValPcLlbs0230.Len.W_B04_VAL_PC + W_B04_VAL_STRINGA_IND + W_B04_VAL_STRINGA + W_B04_DS_OPER_SQL + W_B04_DS_VER + W_B04_DS_TS_CPTZ + W_B04_DS_UTENTE + W_B04_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B04_ID_BILA_VAR_CALC_P = 9;
            public static final int W_B04_COD_COMP_ANIA = 5;
            public static final int W_B04_ID_RICH_ESTRAZ_MAS = 9;
            public static final int W_B04_ID_POLI = 9;
            public static final int W_B04_ID_ADES = 9;
            public static final int W_B04_PROG_SCHEDA_VALOR = 9;
            public static final int W_B04_DS_VER = 9;
            public static final int W_B04_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
