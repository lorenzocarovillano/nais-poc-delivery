package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.ws.redefines.WrifDtCambioVlt;
import it.accenture.jnais.ws.redefines.WrifDtInvst;
import it.accenture.jnais.ws.redefines.WrifDtInvstCalc;
import it.accenture.jnais.ws.redefines.WrifIdMoviChiu;
import it.accenture.jnais.ws.redefines.WrifImpGapEvent;
import it.accenture.jnais.ws.redefines.WrifImpMovto;
import it.accenture.jnais.ws.redefines.WrifNumQuo;
import it.accenture.jnais.ws.redefines.WrifPc;

/**Original name: WRIF-DATI<br>
 * Variable: WRIF-DATI from copybook LCCVRIF1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WrifDati implements ICopyable<WrifDati> {

    //==== PROPERTIES ====
    //Original name: WRIF-ID-RICH-INVST-FND
    private int wrifIdRichInvstFnd = DefaultValues.INT_VAL;
    //Original name: WRIF-ID-MOVI-FINRIO
    private int wrifIdMoviFinrio = DefaultValues.INT_VAL;
    //Original name: WRIF-ID-MOVI-CRZ
    private int wrifIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WRIF-ID-MOVI-CHIU
    private WrifIdMoviChiu wrifIdMoviChiu = new WrifIdMoviChiu();
    //Original name: WRIF-DT-INI-EFF
    private int wrifDtIniEff = DefaultValues.INT_VAL;
    //Original name: WRIF-DT-END-EFF
    private int wrifDtEndEff = DefaultValues.INT_VAL;
    //Original name: WRIF-COD-COMP-ANIA
    private int wrifCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WRIF-COD-FND
    private String wrifCodFnd = DefaultValues.stringVal(Len.WRIF_COD_FND);
    //Original name: WRIF-NUM-QUO
    private WrifNumQuo wrifNumQuo = new WrifNumQuo();
    //Original name: WRIF-PC
    private WrifPc wrifPc = new WrifPc();
    //Original name: WRIF-IMP-MOVTO
    private WrifImpMovto wrifImpMovto = new WrifImpMovto();
    //Original name: WRIF-DT-INVST
    private WrifDtInvst wrifDtInvst = new WrifDtInvst();
    //Original name: WRIF-COD-TARI
    private String wrifCodTari = DefaultValues.stringVal(Len.WRIF_COD_TARI);
    //Original name: WRIF-TP-STAT
    private String wrifTpStat = DefaultValues.stringVal(Len.WRIF_TP_STAT);
    //Original name: WRIF-TP-MOD-INVST
    private String wrifTpModInvst = DefaultValues.stringVal(Len.WRIF_TP_MOD_INVST);
    //Original name: WRIF-COD-DIV
    private String wrifCodDiv = DefaultValues.stringVal(Len.WRIF_COD_DIV);
    //Original name: WRIF-DT-CAMBIO-VLT
    private WrifDtCambioVlt wrifDtCambioVlt = new WrifDtCambioVlt();
    //Original name: WRIF-TP-FND
    private char wrifTpFnd = DefaultValues.CHAR_VAL;
    //Original name: WRIF-DS-RIGA
    private long wrifDsRiga = DefaultValues.LONG_VAL;
    //Original name: WRIF-DS-OPER-SQL
    private char wrifDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WRIF-DS-VER
    private int wrifDsVer = DefaultValues.INT_VAL;
    //Original name: WRIF-DS-TS-INI-CPTZ
    private long wrifDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WRIF-DS-TS-END-CPTZ
    private long wrifDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WRIF-DS-UTENTE
    private String wrifDsUtente = DefaultValues.stringVal(Len.WRIF_DS_UTENTE);
    //Original name: WRIF-DS-STATO-ELAB
    private char wrifDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WRIF-DT-INVST-CALC
    private WrifDtInvstCalc wrifDtInvstCalc = new WrifDtInvstCalc();
    //Original name: WRIF-FL-CALC-INVTO
    private char wrifFlCalcInvto = DefaultValues.CHAR_VAL;
    //Original name: WRIF-IMP-GAP-EVENT
    private WrifImpGapEvent wrifImpGapEvent = new WrifImpGapEvent();
    //Original name: WRIF-FL-SWM-BP2S
    private char wrifFlSwmBp2s = DefaultValues.CHAR_VAL;

    //==== CONSTRUCTORS ====
    public WrifDati() {
    }

    public WrifDati(WrifDati dati) {
        this();
        this.wrifIdRichInvstFnd = dati.wrifIdRichInvstFnd;
        this.wrifIdMoviFinrio = dati.wrifIdMoviFinrio;
        this.wrifIdMoviCrz = dati.wrifIdMoviCrz;
        this.wrifIdMoviChiu = ((WrifIdMoviChiu)dati.wrifIdMoviChiu.copy());
        this.wrifDtIniEff = dati.wrifDtIniEff;
        this.wrifDtEndEff = dati.wrifDtEndEff;
        this.wrifCodCompAnia = dati.wrifCodCompAnia;
        this.wrifCodFnd = dati.wrifCodFnd;
        this.wrifNumQuo = ((WrifNumQuo)dati.wrifNumQuo.copy());
        this.wrifPc = ((WrifPc)dati.wrifPc.copy());
        this.wrifImpMovto = ((WrifImpMovto)dati.wrifImpMovto.copy());
        this.wrifDtInvst = ((WrifDtInvst)dati.wrifDtInvst.copy());
        this.wrifCodTari = dati.wrifCodTari;
        this.wrifTpStat = dati.wrifTpStat;
        this.wrifTpModInvst = dati.wrifTpModInvst;
        this.wrifCodDiv = dati.wrifCodDiv;
        this.wrifDtCambioVlt = ((WrifDtCambioVlt)dati.wrifDtCambioVlt.copy());
        this.wrifTpFnd = dati.wrifTpFnd;
        this.wrifDsRiga = dati.wrifDsRiga;
        this.wrifDsOperSql = dati.wrifDsOperSql;
        this.wrifDsVer = dati.wrifDsVer;
        this.wrifDsTsIniCptz = dati.wrifDsTsIniCptz;
        this.wrifDsTsEndCptz = dati.wrifDsTsEndCptz;
        this.wrifDsUtente = dati.wrifDsUtente;
        this.wrifDsStatoElab = dati.wrifDsStatoElab;
        this.wrifDtInvstCalc = ((WrifDtInvstCalc)dati.wrifDtInvstCalc.copy());
        this.wrifFlCalcInvto = dati.wrifFlCalcInvto;
        this.wrifImpGapEvent = ((WrifImpGapEvent)dati.wrifImpGapEvent.copy());
        this.wrifFlSwmBp2s = dati.wrifFlSwmBp2s;
    }

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wrifIdRichInvstFnd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIF_ID_RICH_INVST_FND, 0);
        position += Len.WRIF_ID_RICH_INVST_FND;
        wrifIdMoviFinrio = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIF_ID_MOVI_FINRIO, 0);
        position += Len.WRIF_ID_MOVI_FINRIO;
        wrifIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIF_ID_MOVI_CRZ, 0);
        position += Len.WRIF_ID_MOVI_CRZ;
        wrifIdMoviChiu.setWrifIdMoviChiuFromBuffer(buffer, position);
        position += WrifIdMoviChiu.Len.WRIF_ID_MOVI_CHIU;
        wrifDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIF_DT_INI_EFF, 0);
        position += Len.WRIF_DT_INI_EFF;
        wrifDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIF_DT_END_EFF, 0);
        position += Len.WRIF_DT_END_EFF;
        wrifCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIF_COD_COMP_ANIA, 0);
        position += Len.WRIF_COD_COMP_ANIA;
        wrifCodFnd = MarshalByte.readString(buffer, position, Len.WRIF_COD_FND);
        position += Len.WRIF_COD_FND;
        wrifNumQuo.setWrifNumQuoFromBuffer(buffer, position);
        position += WrifNumQuo.Len.WRIF_NUM_QUO;
        wrifPc.setWrifPcFromBuffer(buffer, position);
        position += WrifPc.Len.WRIF_PC;
        wrifImpMovto.setWrifImpMovtoFromBuffer(buffer, position);
        position += WrifImpMovto.Len.WRIF_IMP_MOVTO;
        wrifDtInvst.setWrifDtInvstFromBuffer(buffer, position);
        position += WrifDtInvst.Len.WRIF_DT_INVST;
        wrifCodTari = MarshalByte.readString(buffer, position, Len.WRIF_COD_TARI);
        position += Len.WRIF_COD_TARI;
        wrifTpStat = MarshalByte.readString(buffer, position, Len.WRIF_TP_STAT);
        position += Len.WRIF_TP_STAT;
        wrifTpModInvst = MarshalByte.readString(buffer, position, Len.WRIF_TP_MOD_INVST);
        position += Len.WRIF_TP_MOD_INVST;
        wrifCodDiv = MarshalByte.readString(buffer, position, Len.WRIF_COD_DIV);
        position += Len.WRIF_COD_DIV;
        wrifDtCambioVlt.setWrifDtCambioVltFromBuffer(buffer, position);
        position += WrifDtCambioVlt.Len.WRIF_DT_CAMBIO_VLT;
        wrifTpFnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wrifDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRIF_DS_RIGA, 0);
        position += Len.WRIF_DS_RIGA;
        wrifDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wrifDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRIF_DS_VER, 0);
        position += Len.WRIF_DS_VER;
        wrifDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRIF_DS_TS_INI_CPTZ, 0);
        position += Len.WRIF_DS_TS_INI_CPTZ;
        wrifDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRIF_DS_TS_END_CPTZ, 0);
        position += Len.WRIF_DS_TS_END_CPTZ;
        wrifDsUtente = MarshalByte.readString(buffer, position, Len.WRIF_DS_UTENTE);
        position += Len.WRIF_DS_UTENTE;
        wrifDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wrifDtInvstCalc.setWrifDtInvstCalcFromBuffer(buffer, position);
        position += WrifDtInvstCalc.Len.WRIF_DT_INVST_CALC;
        wrifFlCalcInvto = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wrifImpGapEvent.setWrifImpGapEventFromBuffer(buffer, position);
        position += WrifImpGapEvent.Len.WRIF_IMP_GAP_EVENT;
        wrifFlSwmBp2s = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wrifIdRichInvstFnd, Len.Int.WRIF_ID_RICH_INVST_FND, 0);
        position += Len.WRIF_ID_RICH_INVST_FND;
        MarshalByte.writeIntAsPacked(buffer, position, wrifIdMoviFinrio, Len.Int.WRIF_ID_MOVI_FINRIO, 0);
        position += Len.WRIF_ID_MOVI_FINRIO;
        MarshalByte.writeIntAsPacked(buffer, position, wrifIdMoviCrz, Len.Int.WRIF_ID_MOVI_CRZ, 0);
        position += Len.WRIF_ID_MOVI_CRZ;
        wrifIdMoviChiu.getWrifIdMoviChiuAsBuffer(buffer, position);
        position += WrifIdMoviChiu.Len.WRIF_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wrifDtIniEff, Len.Int.WRIF_DT_INI_EFF, 0);
        position += Len.WRIF_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wrifDtEndEff, Len.Int.WRIF_DT_END_EFF, 0);
        position += Len.WRIF_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wrifCodCompAnia, Len.Int.WRIF_COD_COMP_ANIA, 0);
        position += Len.WRIF_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wrifCodFnd, Len.WRIF_COD_FND);
        position += Len.WRIF_COD_FND;
        wrifNumQuo.getWrifNumQuoAsBuffer(buffer, position);
        position += WrifNumQuo.Len.WRIF_NUM_QUO;
        wrifPc.getWrifPcAsBuffer(buffer, position);
        position += WrifPc.Len.WRIF_PC;
        wrifImpMovto.getWrifImpMovtoAsBuffer(buffer, position);
        position += WrifImpMovto.Len.WRIF_IMP_MOVTO;
        wrifDtInvst.getWrifDtInvstAsBuffer(buffer, position);
        position += WrifDtInvst.Len.WRIF_DT_INVST;
        MarshalByte.writeString(buffer, position, wrifCodTari, Len.WRIF_COD_TARI);
        position += Len.WRIF_COD_TARI;
        MarshalByte.writeString(buffer, position, wrifTpStat, Len.WRIF_TP_STAT);
        position += Len.WRIF_TP_STAT;
        MarshalByte.writeString(buffer, position, wrifTpModInvst, Len.WRIF_TP_MOD_INVST);
        position += Len.WRIF_TP_MOD_INVST;
        MarshalByte.writeString(buffer, position, wrifCodDiv, Len.WRIF_COD_DIV);
        position += Len.WRIF_COD_DIV;
        wrifDtCambioVlt.getWrifDtCambioVltAsBuffer(buffer, position);
        position += WrifDtCambioVlt.Len.WRIF_DT_CAMBIO_VLT;
        MarshalByte.writeChar(buffer, position, wrifTpFnd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, wrifDsRiga, Len.Int.WRIF_DS_RIGA, 0);
        position += Len.WRIF_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wrifDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wrifDsVer, Len.Int.WRIF_DS_VER, 0);
        position += Len.WRIF_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wrifDsTsIniCptz, Len.Int.WRIF_DS_TS_INI_CPTZ, 0);
        position += Len.WRIF_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wrifDsTsEndCptz, Len.Int.WRIF_DS_TS_END_CPTZ, 0);
        position += Len.WRIF_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wrifDsUtente, Len.WRIF_DS_UTENTE);
        position += Len.WRIF_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wrifDsStatoElab);
        position += Types.CHAR_SIZE;
        wrifDtInvstCalc.getWrifDtInvstCalcAsBuffer(buffer, position);
        position += WrifDtInvstCalc.Len.WRIF_DT_INVST_CALC;
        MarshalByte.writeChar(buffer, position, wrifFlCalcInvto);
        position += Types.CHAR_SIZE;
        wrifImpGapEvent.getWrifImpGapEventAsBuffer(buffer, position);
        position += WrifImpGapEvent.Len.WRIF_IMP_GAP_EVENT;
        MarshalByte.writeChar(buffer, position, wrifFlSwmBp2s);
        return buffer;
    }

    public void initDatiSpaces() {
        wrifIdRichInvstFnd = Types.INVALID_INT_VAL;
        wrifIdMoviFinrio = Types.INVALID_INT_VAL;
        wrifIdMoviCrz = Types.INVALID_INT_VAL;
        wrifIdMoviChiu.initWrifIdMoviChiuSpaces();
        wrifDtIniEff = Types.INVALID_INT_VAL;
        wrifDtEndEff = Types.INVALID_INT_VAL;
        wrifCodCompAnia = Types.INVALID_INT_VAL;
        wrifCodFnd = "";
        wrifNumQuo.initWrifNumQuoSpaces();
        wrifPc.initWrifPcSpaces();
        wrifImpMovto.initWrifImpMovtoSpaces();
        wrifDtInvst.initWrifDtInvstSpaces();
        wrifCodTari = "";
        wrifTpStat = "";
        wrifTpModInvst = "";
        wrifCodDiv = "";
        wrifDtCambioVlt.initWrifDtCambioVltSpaces();
        wrifTpFnd = Types.SPACE_CHAR;
        wrifDsRiga = Types.INVALID_LONG_VAL;
        wrifDsOperSql = Types.SPACE_CHAR;
        wrifDsVer = Types.INVALID_INT_VAL;
        wrifDsTsIniCptz = Types.INVALID_LONG_VAL;
        wrifDsTsEndCptz = Types.INVALID_LONG_VAL;
        wrifDsUtente = "";
        wrifDsStatoElab = Types.SPACE_CHAR;
        wrifDtInvstCalc.initWrifDtInvstCalcSpaces();
        wrifFlCalcInvto = Types.SPACE_CHAR;
        wrifImpGapEvent.initWrifImpGapEventSpaces();
        wrifFlSwmBp2s = Types.SPACE_CHAR;
    }

    public void setWrifIdRichInvstFnd(int wrifIdRichInvstFnd) {
        this.wrifIdRichInvstFnd = wrifIdRichInvstFnd;
    }

    public int getWrifIdRichInvstFnd() {
        return this.wrifIdRichInvstFnd;
    }

    public void setWrifIdMoviFinrio(int wrifIdMoviFinrio) {
        this.wrifIdMoviFinrio = wrifIdMoviFinrio;
    }

    public int getWrifIdMoviFinrio() {
        return this.wrifIdMoviFinrio;
    }

    public void setWrifIdMoviCrz(int wrifIdMoviCrz) {
        this.wrifIdMoviCrz = wrifIdMoviCrz;
    }

    public int getWrifIdMoviCrz() {
        return this.wrifIdMoviCrz;
    }

    public void setWrifDtIniEff(int wrifDtIniEff) {
        this.wrifDtIniEff = wrifDtIniEff;
    }

    public int getWrifDtIniEff() {
        return this.wrifDtIniEff;
    }

    public void setWrifDtEndEff(int wrifDtEndEff) {
        this.wrifDtEndEff = wrifDtEndEff;
    }

    public int getWrifDtEndEff() {
        return this.wrifDtEndEff;
    }

    public void setWrifCodCompAnia(int wrifCodCompAnia) {
        this.wrifCodCompAnia = wrifCodCompAnia;
    }

    public int getWrifCodCompAnia() {
        return this.wrifCodCompAnia;
    }

    public void setWrifCodFnd(String wrifCodFnd) {
        this.wrifCodFnd = Functions.subString(wrifCodFnd, Len.WRIF_COD_FND);
    }

    public String getWrifCodFnd() {
        return this.wrifCodFnd;
    }

    public void setWrifCodTari(String wrifCodTari) {
        this.wrifCodTari = Functions.subString(wrifCodTari, Len.WRIF_COD_TARI);
    }

    public String getWrifCodTari() {
        return this.wrifCodTari;
    }

    public void setWrifTpStat(String wrifTpStat) {
        this.wrifTpStat = Functions.subString(wrifTpStat, Len.WRIF_TP_STAT);
    }

    public String getWrifTpStat() {
        return this.wrifTpStat;
    }

    public void setWrifTpModInvst(String wrifTpModInvst) {
        this.wrifTpModInvst = Functions.subString(wrifTpModInvst, Len.WRIF_TP_MOD_INVST);
    }

    public String getWrifTpModInvst() {
        return this.wrifTpModInvst;
    }

    public void setWrifCodDiv(String wrifCodDiv) {
        this.wrifCodDiv = Functions.subString(wrifCodDiv, Len.WRIF_COD_DIV);
    }

    public String getWrifCodDiv() {
        return this.wrifCodDiv;
    }

    public void setWrifTpFnd(char wrifTpFnd) {
        this.wrifTpFnd = wrifTpFnd;
    }

    public char getWrifTpFnd() {
        return this.wrifTpFnd;
    }

    public void setWrifDsRiga(long wrifDsRiga) {
        this.wrifDsRiga = wrifDsRiga;
    }

    public long getWrifDsRiga() {
        return this.wrifDsRiga;
    }

    public void setWrifDsOperSql(char wrifDsOperSql) {
        this.wrifDsOperSql = wrifDsOperSql;
    }

    public char getWrifDsOperSql() {
        return this.wrifDsOperSql;
    }

    public void setWrifDsVer(int wrifDsVer) {
        this.wrifDsVer = wrifDsVer;
    }

    public int getWrifDsVer() {
        return this.wrifDsVer;
    }

    public void setWrifDsTsIniCptz(long wrifDsTsIniCptz) {
        this.wrifDsTsIniCptz = wrifDsTsIniCptz;
    }

    public long getWrifDsTsIniCptz() {
        return this.wrifDsTsIniCptz;
    }

    public void setWrifDsTsEndCptz(long wrifDsTsEndCptz) {
        this.wrifDsTsEndCptz = wrifDsTsEndCptz;
    }

    public long getWrifDsTsEndCptz() {
        return this.wrifDsTsEndCptz;
    }

    public void setWrifDsUtente(String wrifDsUtente) {
        this.wrifDsUtente = Functions.subString(wrifDsUtente, Len.WRIF_DS_UTENTE);
    }

    public String getWrifDsUtente() {
        return this.wrifDsUtente;
    }

    public void setWrifDsStatoElab(char wrifDsStatoElab) {
        this.wrifDsStatoElab = wrifDsStatoElab;
    }

    public char getWrifDsStatoElab() {
        return this.wrifDsStatoElab;
    }

    public void setWrifFlCalcInvto(char wrifFlCalcInvto) {
        this.wrifFlCalcInvto = wrifFlCalcInvto;
    }

    public char getWrifFlCalcInvto() {
        return this.wrifFlCalcInvto;
    }

    public void setWrifFlSwmBp2s(char wrifFlSwmBp2s) {
        this.wrifFlSwmBp2s = wrifFlSwmBp2s;
    }

    public char getWrifFlSwmBp2s() {
        return this.wrifFlSwmBp2s;
    }

    public WrifDtCambioVlt getWrifDtCambioVlt() {
        return wrifDtCambioVlt;
    }

    public WrifDtInvst getWrifDtInvst() {
        return wrifDtInvst;
    }

    public WrifDtInvstCalc getWrifDtInvstCalc() {
        return wrifDtInvstCalc;
    }

    public WrifIdMoviChiu getWrifIdMoviChiu() {
        return wrifIdMoviChiu;
    }

    public WrifImpGapEvent getWrifImpGapEvent() {
        return wrifImpGapEvent;
    }

    public WrifImpMovto getWrifImpMovto() {
        return wrifImpMovto;
    }

    public WrifNumQuo getWrifNumQuo() {
        return wrifNumQuo;
    }

    public WrifPc getWrifPc() {
        return wrifPc;
    }

    public WrifDati copy() {
        return new WrifDati(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIF_ID_RICH_INVST_FND = 5;
        public static final int WRIF_ID_MOVI_FINRIO = 5;
        public static final int WRIF_ID_MOVI_CRZ = 5;
        public static final int WRIF_DT_INI_EFF = 5;
        public static final int WRIF_DT_END_EFF = 5;
        public static final int WRIF_COD_COMP_ANIA = 3;
        public static final int WRIF_COD_FND = 20;
        public static final int WRIF_COD_TARI = 12;
        public static final int WRIF_TP_STAT = 2;
        public static final int WRIF_TP_MOD_INVST = 2;
        public static final int WRIF_COD_DIV = 20;
        public static final int WRIF_TP_FND = 1;
        public static final int WRIF_DS_RIGA = 6;
        public static final int WRIF_DS_OPER_SQL = 1;
        public static final int WRIF_DS_VER = 5;
        public static final int WRIF_DS_TS_INI_CPTZ = 10;
        public static final int WRIF_DS_TS_END_CPTZ = 10;
        public static final int WRIF_DS_UTENTE = 20;
        public static final int WRIF_DS_STATO_ELAB = 1;
        public static final int WRIF_FL_CALC_INVTO = 1;
        public static final int WRIF_FL_SWM_BP2S = 1;
        public static final int DATI = WRIF_ID_RICH_INVST_FND + WRIF_ID_MOVI_FINRIO + WRIF_ID_MOVI_CRZ + WrifIdMoviChiu.Len.WRIF_ID_MOVI_CHIU + WRIF_DT_INI_EFF + WRIF_DT_END_EFF + WRIF_COD_COMP_ANIA + WRIF_COD_FND + WrifNumQuo.Len.WRIF_NUM_QUO + WrifPc.Len.WRIF_PC + WrifImpMovto.Len.WRIF_IMP_MOVTO + WrifDtInvst.Len.WRIF_DT_INVST + WRIF_COD_TARI + WRIF_TP_STAT + WRIF_TP_MOD_INVST + WRIF_COD_DIV + WrifDtCambioVlt.Len.WRIF_DT_CAMBIO_VLT + WRIF_TP_FND + WRIF_DS_RIGA + WRIF_DS_OPER_SQL + WRIF_DS_VER + WRIF_DS_TS_INI_CPTZ + WRIF_DS_TS_END_CPTZ + WRIF_DS_UTENTE + WRIF_DS_STATO_ELAB + WrifDtInvstCalc.Len.WRIF_DT_INVST_CALC + WRIF_FL_CALC_INVTO + WrifImpGapEvent.Len.WRIF_IMP_GAP_EVENT + WRIF_FL_SWM_BP2S;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIF_ID_RICH_INVST_FND = 9;
            public static final int WRIF_ID_MOVI_FINRIO = 9;
            public static final int WRIF_ID_MOVI_CRZ = 9;
            public static final int WRIF_DT_INI_EFF = 8;
            public static final int WRIF_DT_END_EFF = 8;
            public static final int WRIF_COD_COMP_ANIA = 5;
            public static final int WRIF_DS_RIGA = 10;
            public static final int WRIF_DS_VER = 9;
            public static final int WRIF_DS_TS_INI_CPTZ = 18;
            public static final int WRIF_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
