package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: ORA-ELABORAZIONE-REPORT<br>
 * Variable: ORA-ELABORAZIONE-REPORT from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class OraElaborazioneReport {

    //==== PROPERTIES ====
    //Original name: FILLER-ORA-ELABORAZIONE-REPORT
    private String flr1 = "* ORA  ELABORAZIONE    :";
    //Original name: REP-ORA
    private String ora = "";
    //Original name: FILLER-ORA-ELABORAZIONE-REPORT-1
    private String flr2 = " :";
    //Original name: REP-MINUTI
    private String minuti = "";
    //Original name: FILLER-ORA-ELABORAZIONE-REPORT-2
    private String flr3 = " :";
    //Original name: REP-SECONDI
    private String secondi = "";

    //==== METHODS ====
    public String getOraElaborazioneReportFormatted() {
        return MarshalByteExt.bufferToStr(getOraElaborazioneReportBytes());
    }

    public byte[] getOraElaborazioneReportBytes() {
        byte[] buffer = new byte[Len.ORA_ELABORAZIONE_REPORT];
        return getOraElaborazioneReportBytes(buffer, 1);
    }

    public byte[] getOraElaborazioneReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, ora, Len.ORA);
        position += Len.ORA;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, minuti, Len.MINUTI);
        position += Len.MINUTI;
        MarshalByte.writeString(buffer, position, flr3, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, secondi, Len.SECONDI);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public void setOra(String ora) {
        this.ora = Functions.subString(ora, Len.ORA);
    }

    public String getOra() {
        return this.ora;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public void setMinuti(String minuti) {
        this.minuti = Functions.subString(minuti, Len.MINUTI);
    }

    public String getMinuti() {
        return this.minuti;
    }

    public String getFlr3() {
        return this.flr3;
    }

    public void setSecondi(String secondi) {
        this.secondi = Functions.subString(secondi, Len.SECONDI);
    }

    public String getSecondi() {
        return this.secondi;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 26;
        public static final int ORA = 2;
        public static final int FLR2 = 3;
        public static final int MINUTI = 2;
        public static final int SECONDI = 4;
        public static final int ORA_ELABORAZIONE_REPORT = ORA + MINUTI + SECONDI + FLR1 + 2 * FLR2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
