package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WodeCodLivAutApprt;
import it.accenture.jnais.ws.redefines.WodeIdMoviChiu;

/**Original name: WODE-DATI<br>
 * Variable: WODE-DATI from copybook LCCVODE1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WodeDati {

    //==== PROPERTIES ====
    //Original name: WODE-ID-OGG-DEROGA
    private int wodeIdOggDeroga = DefaultValues.INT_VAL;
    //Original name: WODE-ID-MOVI-CRZ
    private int wodeIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WODE-ID-MOVI-CHIU
    private WodeIdMoviChiu wodeIdMoviChiu = new WodeIdMoviChiu();
    //Original name: WODE-DT-INI-EFF
    private int wodeDtIniEff = DefaultValues.INT_VAL;
    //Original name: WODE-DT-END-EFF
    private int wodeDtEndEff = DefaultValues.INT_VAL;
    //Original name: WODE-COD-COMP-ANIA
    private int wodeCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WODE-ID-OGG
    private int wodeIdOgg = DefaultValues.INT_VAL;
    //Original name: WODE-TP-OGG
    private String wodeTpOgg = DefaultValues.stringVal(Len.WODE_TP_OGG);
    //Original name: WODE-IB-OGG
    private String wodeIbOgg = DefaultValues.stringVal(Len.WODE_IB_OGG);
    //Original name: WODE-TP-DEROGA
    private String wodeTpDeroga = DefaultValues.stringVal(Len.WODE_TP_DEROGA);
    //Original name: WODE-COD-GR-AUT-APPRT
    private long wodeCodGrAutApprt = DefaultValues.LONG_VAL;
    //Original name: WODE-COD-LIV-AUT-APPRT
    private WodeCodLivAutApprt wodeCodLivAutApprt = new WodeCodLivAutApprt();
    //Original name: WODE-COD-GR-AUT-SUP
    private long wodeCodGrAutSup = DefaultValues.LONG_VAL;
    //Original name: WODE-COD-LIV-AUT-SUP
    private int wodeCodLivAutSup = DefaultValues.INT_VAL;
    //Original name: WODE-DS-RIGA
    private long wodeDsRiga = DefaultValues.LONG_VAL;
    //Original name: WODE-DS-OPER-SQL
    private char wodeDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WODE-DS-VER
    private int wodeDsVer = DefaultValues.INT_VAL;
    //Original name: WODE-DS-TS-INI-CPTZ
    private long wodeDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WODE-DS-TS-END-CPTZ
    private long wodeDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WODE-DS-UTENTE
    private String wodeDsUtente = DefaultValues.stringVal(Len.WODE_DS_UTENTE);
    //Original name: WODE-DS-STATO-ELAB
    private char wodeDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setWodeIdOggDeroga(int wodeIdOggDeroga) {
        this.wodeIdOggDeroga = wodeIdOggDeroga;
    }

    public int getWodeIdOggDeroga() {
        return this.wodeIdOggDeroga;
    }

    public void setWodeIdMoviCrz(int wodeIdMoviCrz) {
        this.wodeIdMoviCrz = wodeIdMoviCrz;
    }

    public int getWodeIdMoviCrz() {
        return this.wodeIdMoviCrz;
    }

    public void setWodeDtIniEff(int wodeDtIniEff) {
        this.wodeDtIniEff = wodeDtIniEff;
    }

    public int getWodeDtIniEff() {
        return this.wodeDtIniEff;
    }

    public void setWodeDtEndEff(int wodeDtEndEff) {
        this.wodeDtEndEff = wodeDtEndEff;
    }

    public int getWodeDtEndEff() {
        return this.wodeDtEndEff;
    }

    public void setWodeCodCompAnia(int wodeCodCompAnia) {
        this.wodeCodCompAnia = wodeCodCompAnia;
    }

    public int getWodeCodCompAnia() {
        return this.wodeCodCompAnia;
    }

    public void setWodeIdOgg(int wodeIdOgg) {
        this.wodeIdOgg = wodeIdOgg;
    }

    public int getWodeIdOgg() {
        return this.wodeIdOgg;
    }

    public void setWodeTpOgg(String wodeTpOgg) {
        this.wodeTpOgg = Functions.subString(wodeTpOgg, Len.WODE_TP_OGG);
    }

    public String getWodeTpOgg() {
        return this.wodeTpOgg;
    }

    public void setWodeIbOgg(String wodeIbOgg) {
        this.wodeIbOgg = Functions.subString(wodeIbOgg, Len.WODE_IB_OGG);
    }

    public String getWodeIbOgg() {
        return this.wodeIbOgg;
    }

    public void setWodeTpDeroga(String wodeTpDeroga) {
        this.wodeTpDeroga = Functions.subString(wodeTpDeroga, Len.WODE_TP_DEROGA);
    }

    public String getWodeTpDeroga() {
        return this.wodeTpDeroga;
    }

    public void setWodeCodGrAutApprt(long wodeCodGrAutApprt) {
        this.wodeCodGrAutApprt = wodeCodGrAutApprt;
    }

    public long getWodeCodGrAutApprt() {
        return this.wodeCodGrAutApprt;
    }

    public void setWodeCodGrAutSup(long wodeCodGrAutSup) {
        this.wodeCodGrAutSup = wodeCodGrAutSup;
    }

    public long getWodeCodGrAutSup() {
        return this.wodeCodGrAutSup;
    }

    public void setWodeCodLivAutSup(int wodeCodLivAutSup) {
        this.wodeCodLivAutSup = wodeCodLivAutSup;
    }

    public int getWodeCodLivAutSup() {
        return this.wodeCodLivAutSup;
    }

    public void setWodeDsRiga(long wodeDsRiga) {
        this.wodeDsRiga = wodeDsRiga;
    }

    public long getWodeDsRiga() {
        return this.wodeDsRiga;
    }

    public void setWodeDsOperSql(char wodeDsOperSql) {
        this.wodeDsOperSql = wodeDsOperSql;
    }

    public char getWodeDsOperSql() {
        return this.wodeDsOperSql;
    }

    public void setWodeDsVer(int wodeDsVer) {
        this.wodeDsVer = wodeDsVer;
    }

    public int getWodeDsVer() {
        return this.wodeDsVer;
    }

    public void setWodeDsTsIniCptz(long wodeDsTsIniCptz) {
        this.wodeDsTsIniCptz = wodeDsTsIniCptz;
    }

    public long getWodeDsTsIniCptz() {
        return this.wodeDsTsIniCptz;
    }

    public void setWodeDsTsEndCptz(long wodeDsTsEndCptz) {
        this.wodeDsTsEndCptz = wodeDsTsEndCptz;
    }

    public long getWodeDsTsEndCptz() {
        return this.wodeDsTsEndCptz;
    }

    public void setWodeDsUtente(String wodeDsUtente) {
        this.wodeDsUtente = Functions.subString(wodeDsUtente, Len.WODE_DS_UTENTE);
    }

    public String getWodeDsUtente() {
        return this.wodeDsUtente;
    }

    public void setWodeDsStatoElab(char wodeDsStatoElab) {
        this.wodeDsStatoElab = wodeDsStatoElab;
    }

    public char getWodeDsStatoElab() {
        return this.wodeDsStatoElab;
    }

    public WodeCodLivAutApprt getWodeCodLivAutApprt() {
        return wodeCodLivAutApprt;
    }

    public WodeIdMoviChiu getWodeIdMoviChiu() {
        return wodeIdMoviChiu;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WODE_TP_OGG = 2;
        public static final int WODE_IB_OGG = 40;
        public static final int WODE_TP_DEROGA = 2;
        public static final int WODE_DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
