package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DETT-TIT-CONT-DB<br>
 * Variable: DETT-TIT-CONT-DB from copybook IDBVDTC3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DettTitContDb {

    //==== PROPERTIES ====
    //Original name: DTC-DT-INI-EFF-DB
    private String iniEffDb = DefaultValues.stringVal(Len.INI_EFF_DB);
    //Original name: DTC-DT-END-EFF-DB
    private String endEffDb = DefaultValues.stringVal(Len.END_EFF_DB);
    //Original name: DTC-DT-INI-COP-DB
    private String iniCopDb = DefaultValues.stringVal(Len.INI_COP_DB);
    //Original name: DTC-DT-END-COP-DB
    private String endCopDb = DefaultValues.stringVal(Len.END_COP_DB);
    //Original name: DTC-DT-ESI-TIT-DB
    private String esiTitDb = DefaultValues.stringVal(Len.ESI_TIT_DB);

    //==== METHODS ====
    public void setIniEffDb(String iniEffDb) {
        this.iniEffDb = Functions.subString(iniEffDb, Len.INI_EFF_DB);
    }

    public String getIniEffDb() {
        return this.iniEffDb;
    }

    public void setEndEffDb(String endEffDb) {
        this.endEffDb = Functions.subString(endEffDb, Len.END_EFF_DB);
    }

    public String getEndEffDb() {
        return this.endEffDb;
    }

    public void setIniCopDb(String iniCopDb) {
        this.iniCopDb = Functions.subString(iniCopDb, Len.INI_COP_DB);
    }

    public String getIniCopDb() {
        return this.iniCopDb;
    }

    public void setEndCopDb(String endCopDb) {
        this.endCopDb = Functions.subString(endCopDb, Len.END_COP_DB);
    }

    public String getEndCopDb() {
        return this.endCopDb;
    }

    public void setEsiTitDb(String esiTitDb) {
        this.esiTitDb = Functions.subString(esiTitDb, Len.ESI_TIT_DB);
    }

    public String getEsiTitDb() {
        return this.esiTitDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INI_EFF_DB = 10;
        public static final int END_EFF_DB = 10;
        public static final int INI_COP_DB = 10;
        public static final int END_COP_DB = 10;
        public static final int ESI_TIT_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
