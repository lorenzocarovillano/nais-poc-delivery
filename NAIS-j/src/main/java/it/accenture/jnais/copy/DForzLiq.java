package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.DflAaCnbzDal2007Ef;
import it.accenture.jnais.ws.redefines.DflAaCnbzEnd2000Ef;
import it.accenture.jnais.ws.redefines.DflAaCnbzEnd2006Ef;
import it.accenture.jnais.ws.redefines.DflAccpreAccCalc;
import it.accenture.jnais.ws.redefines.DflAccpreAccDfz;
import it.accenture.jnais.ws.redefines.DflAccpreAccEfflq;
import it.accenture.jnais.ws.redefines.DflAccpreSostCalc;
import it.accenture.jnais.ws.redefines.DflAccpreSostDfz;
import it.accenture.jnais.ws.redefines.DflAccpreSostEfflq;
import it.accenture.jnais.ws.redefines.DflAccpreVisCalc;
import it.accenture.jnais.ws.redefines.DflAccpreVisDfz;
import it.accenture.jnais.ws.redefines.DflAccpreVisEfflq;
import it.accenture.jnais.ws.redefines.DflAlqCnbtInpstfmC;
import it.accenture.jnais.ws.redefines.DflAlqCnbtInpstfmD;
import it.accenture.jnais.ws.redefines.DflAlqCnbtInpstfmE;
import it.accenture.jnais.ws.redefines.DflAlqTaxSepCalc;
import it.accenture.jnais.ws.redefines.DflAlqTaxSepDfz;
import it.accenture.jnais.ws.redefines.DflAlqTaxSepEfflq;
import it.accenture.jnais.ws.redefines.DflCnbtInpstfmCalc;
import it.accenture.jnais.ws.redefines.DflCnbtInpstfmDfz;
import it.accenture.jnais.ws.redefines.DflCnbtInpstfmEfflq;
import it.accenture.jnais.ws.redefines.DflCndeDal2007Calc;
import it.accenture.jnais.ws.redefines.DflCndeDal2007Dfz;
import it.accenture.jnais.ws.redefines.DflCndeDal2007Efflq;
import it.accenture.jnais.ws.redefines.DflCndeEnd2000Calc;
import it.accenture.jnais.ws.redefines.DflCndeEnd2000Dfz;
import it.accenture.jnais.ws.redefines.DflCndeEnd2000Efflq;
import it.accenture.jnais.ws.redefines.DflCndeEnd2006Calc;
import it.accenture.jnais.ws.redefines.DflCndeEnd2006Dfz;
import it.accenture.jnais.ws.redefines.DflCndeEnd2006Efflq;
import it.accenture.jnais.ws.redefines.DflIcnbInpstfmCalc;
import it.accenture.jnais.ws.redefines.DflIcnbInpstfmDfz;
import it.accenture.jnais.ws.redefines.DflIcnbInpstfmEfflq;
import it.accenture.jnais.ws.redefines.DflIdMoviChiu;
import it.accenture.jnais.ws.redefines.DflIimpst252Calc;
import it.accenture.jnais.ws.redefines.DflIimpst252Dfz;
import it.accenture.jnais.ws.redefines.DflIimpst252Efflq;
import it.accenture.jnais.ws.redefines.DflIimpstPrvrCalc;
import it.accenture.jnais.ws.redefines.DflIimpstPrvrDfz;
import it.accenture.jnais.ws.redefines.DflIimpstPrvrEfflq;
import it.accenture.jnais.ws.redefines.DflIintPrestCalc;
import it.accenture.jnais.ws.redefines.DflIintPrestDfz;
import it.accenture.jnais.ws.redefines.DflIintPrestEfflq;
import it.accenture.jnais.ws.redefines.DflImpbBolloDettC;
import it.accenture.jnais.ws.redefines.DflImpbBolloDettD;
import it.accenture.jnais.ws.redefines.DflImpbBolloDettL;
import it.accenture.jnais.ws.redefines.DflImpbIs1382011c;
import it.accenture.jnais.ws.redefines.DflImpbIs1382011d;
import it.accenture.jnais.ws.redefines.DflImpbIs1382011l;
import it.accenture.jnais.ws.redefines.DflImpbIs662014c;
import it.accenture.jnais.ws.redefines.DflImpbIs662014d;
import it.accenture.jnais.ws.redefines.DflImpbIs662014l;
import it.accenture.jnais.ws.redefines.DflImpbIsCalc;
import it.accenture.jnais.ws.redefines.DflImpbIsDfz;
import it.accenture.jnais.ws.redefines.DflImpbIsEfflq;
import it.accenture.jnais.ws.redefines.DflImpbRitAccCalc;
import it.accenture.jnais.ws.redefines.DflImpbRitAccDfz;
import it.accenture.jnais.ws.redefines.DflImpbRitAccEfflq;
import it.accenture.jnais.ws.redefines.DflImpbTaxSepCalc;
import it.accenture.jnais.ws.redefines.DflImpbTaxSepDfz;
import it.accenture.jnais.ws.redefines.DflImpbTaxSepEfflq;
import it.accenture.jnais.ws.redefines.DflImpbTfrCalc;
import it.accenture.jnais.ws.redefines.DflImpbTfrDfz;
import it.accenture.jnais.ws.redefines.DflImpbTfrEfflq;
import it.accenture.jnais.ws.redefines.DflImpbVis1382011c;
import it.accenture.jnais.ws.redefines.DflImpbVis1382011d;
import it.accenture.jnais.ws.redefines.DflImpbVis1382011l;
import it.accenture.jnais.ws.redefines.DflImpbVis662014c;
import it.accenture.jnais.ws.redefines.DflImpbVis662014d;
import it.accenture.jnais.ws.redefines.DflImpbVis662014l;
import it.accenture.jnais.ws.redefines.DflImpbVisCalc;
import it.accenture.jnais.ws.redefines.DflImpbVisDfz;
import it.accenture.jnais.ws.redefines.DflImpbVisEfflq;
import it.accenture.jnais.ws.redefines.DflImpExcontrEff;
import it.accenture.jnais.ws.redefines.DflImpIntrRitPagC;
import it.accenture.jnais.ws.redefines.DflImpIntrRitPagD;
import it.accenture.jnais.ws.redefines.DflImpIntrRitPagL;
import it.accenture.jnais.ws.redefines.DflImpLrdCalc;
import it.accenture.jnais.ws.redefines.DflImpLrdDfz;
import it.accenture.jnais.ws.redefines.DflImpLrdEfflq;
import it.accenture.jnais.ws.redefines.DflImpNetCalc;
import it.accenture.jnais.ws.redefines.DflImpNetDfz;
import it.accenture.jnais.ws.redefines.DflImpNetEfflq;
import it.accenture.jnais.ws.redefines.DflImpst252Calc;
import it.accenture.jnais.ws.redefines.DflImpst252Efflq;
import it.accenture.jnais.ws.redefines.DflImpstBolloDettC;
import it.accenture.jnais.ws.redefines.DflImpstBolloDettD;
import it.accenture.jnais.ws.redefines.DflImpstBolloDettL;
import it.accenture.jnais.ws.redefines.DflImpstBolloTotVc;
import it.accenture.jnais.ws.redefines.DflImpstBolloTotVd;
import it.accenture.jnais.ws.redefines.DflImpstBolloTotVl;
import it.accenture.jnais.ws.redefines.DflImpstDaRimbEff;
import it.accenture.jnais.ws.redefines.DflImpstPrvrCalc;
import it.accenture.jnais.ws.redefines.DflImpstPrvrDfz;
import it.accenture.jnais.ws.redefines.DflImpstPrvrEfflq;
import it.accenture.jnais.ws.redefines.DflImpstSostCalc;
import it.accenture.jnais.ws.redefines.DflImpstSostDfz;
import it.accenture.jnais.ws.redefines.DflImpstSostEfflq;
import it.accenture.jnais.ws.redefines.DflImpstVis1382011c;
import it.accenture.jnais.ws.redefines.DflImpstVis1382011d;
import it.accenture.jnais.ws.redefines.DflImpstVis1382011l;
import it.accenture.jnais.ws.redefines.DflImpstVis662014c;
import it.accenture.jnais.ws.redefines.DflImpstVis662014d;
import it.accenture.jnais.ws.redefines.DflImpstVis662014l;
import it.accenture.jnais.ws.redefines.DflImpstVisCalc;
import it.accenture.jnais.ws.redefines.DflImpstVisDfz;
import it.accenture.jnais.ws.redefines.DflImpstVisEfflq;
import it.accenture.jnais.ws.redefines.DflIntrPrestCalc;
import it.accenture.jnais.ws.redefines.DflIntrPrestDfz;
import it.accenture.jnais.ws.redefines.DflIntrPrestEfflq;
import it.accenture.jnais.ws.redefines.DflIs1382011c;
import it.accenture.jnais.ws.redefines.DflIs1382011d;
import it.accenture.jnais.ws.redefines.DflIs1382011l;
import it.accenture.jnais.ws.redefines.DflIs662014c;
import it.accenture.jnais.ws.redefines.DflIs662014d;
import it.accenture.jnais.ws.redefines.DflIs662014l;
import it.accenture.jnais.ws.redefines.DflMmCnbzDal2007Ef;
import it.accenture.jnais.ws.redefines.DflMmCnbzEnd2000Ef;
import it.accenture.jnais.ws.redefines.DflMmCnbzEnd2006Ef;
import it.accenture.jnais.ws.redefines.DflMontDal2007Calc;
import it.accenture.jnais.ws.redefines.DflMontDal2007Dfz;
import it.accenture.jnais.ws.redefines.DflMontDal2007Efflq;
import it.accenture.jnais.ws.redefines.DflMontEnd2000Calc;
import it.accenture.jnais.ws.redefines.DflMontEnd2000Dfz;
import it.accenture.jnais.ws.redefines.DflMontEnd2000Efflq;
import it.accenture.jnais.ws.redefines.DflMontEnd2006Calc;
import it.accenture.jnais.ws.redefines.DflMontEnd2006Dfz;
import it.accenture.jnais.ws.redefines.DflMontEnd2006Efflq;
import it.accenture.jnais.ws.redefines.DflResPreAttCalc;
import it.accenture.jnais.ws.redefines.DflResPreAttDfz;
import it.accenture.jnais.ws.redefines.DflResPreAttEfflq;
import it.accenture.jnais.ws.redefines.DflResPrstzCalc;
import it.accenture.jnais.ws.redefines.DflResPrstzDfz;
import it.accenture.jnais.ws.redefines.DflResPrstzEfflq;
import it.accenture.jnais.ws.redefines.DflRitAccCalc;
import it.accenture.jnais.ws.redefines.DflRitAccDfz;
import it.accenture.jnais.ws.redefines.DflRitAccEfflq;
import it.accenture.jnais.ws.redefines.DflRitIrpefCalc;
import it.accenture.jnais.ws.redefines.DflRitIrpefDfz;
import it.accenture.jnais.ws.redefines.DflRitIrpefEfflq;
import it.accenture.jnais.ws.redefines.DflRitTfrCalc;
import it.accenture.jnais.ws.redefines.DflRitTfrDfz;
import it.accenture.jnais.ws.redefines.DflRitTfrEfflq;
import it.accenture.jnais.ws.redefines.DflTaxSepCalc;
import it.accenture.jnais.ws.redefines.DflTaxSepDfz;
import it.accenture.jnais.ws.redefines.DflTaxSepEfflq;

/**Original name: D-FORZ-LIQ<br>
 * Variable: D-FORZ-LIQ from copybook IDBVDFL1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DForzLiq {

    //==== PROPERTIES ====
    //Original name: DFL-ID-D-FORZ-LIQ
    private int dflIdDForzLiq = DefaultValues.INT_VAL;
    //Original name: DFL-ID-LIQ
    private int dflIdLiq = DefaultValues.INT_VAL;
    //Original name: DFL-ID-MOVI-CRZ
    private int dflIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: DFL-ID-MOVI-CHIU
    private DflIdMoviChiu dflIdMoviChiu = new DflIdMoviChiu();
    //Original name: DFL-COD-COMP-ANIA
    private int dflCodCompAnia = DefaultValues.INT_VAL;
    //Original name: DFL-DT-INI-EFF
    private int dflDtIniEff = DefaultValues.INT_VAL;
    //Original name: DFL-DT-END-EFF
    private int dflDtEndEff = DefaultValues.INT_VAL;
    //Original name: DFL-IMP-LRD-CALC
    private DflImpLrdCalc dflImpLrdCalc = new DflImpLrdCalc();
    //Original name: DFL-IMP-LRD-DFZ
    private DflImpLrdDfz dflImpLrdDfz = new DflImpLrdDfz();
    //Original name: DFL-IMP-LRD-EFFLQ
    private DflImpLrdEfflq dflImpLrdEfflq = new DflImpLrdEfflq();
    //Original name: DFL-IMP-NET-CALC
    private DflImpNetCalc dflImpNetCalc = new DflImpNetCalc();
    //Original name: DFL-IMP-NET-DFZ
    private DflImpNetDfz dflImpNetDfz = new DflImpNetDfz();
    //Original name: DFL-IMP-NET-EFFLQ
    private DflImpNetEfflq dflImpNetEfflq = new DflImpNetEfflq();
    //Original name: DFL-IMPST-PRVR-CALC
    private DflImpstPrvrCalc dflImpstPrvrCalc = new DflImpstPrvrCalc();
    //Original name: DFL-IMPST-PRVR-DFZ
    private DflImpstPrvrDfz dflImpstPrvrDfz = new DflImpstPrvrDfz();
    //Original name: DFL-IMPST-PRVR-EFFLQ
    private DflImpstPrvrEfflq dflImpstPrvrEfflq = new DflImpstPrvrEfflq();
    //Original name: DFL-IMPST-VIS-CALC
    private DflImpstVisCalc dflImpstVisCalc = new DflImpstVisCalc();
    //Original name: DFL-IMPST-VIS-DFZ
    private DflImpstVisDfz dflImpstVisDfz = new DflImpstVisDfz();
    //Original name: DFL-IMPST-VIS-EFFLQ
    private DflImpstVisEfflq dflImpstVisEfflq = new DflImpstVisEfflq();
    //Original name: DFL-RIT-ACC-CALC
    private DflRitAccCalc dflRitAccCalc = new DflRitAccCalc();
    //Original name: DFL-RIT-ACC-DFZ
    private DflRitAccDfz dflRitAccDfz = new DflRitAccDfz();
    //Original name: DFL-RIT-ACC-EFFLQ
    private DflRitAccEfflq dflRitAccEfflq = new DflRitAccEfflq();
    //Original name: DFL-RIT-IRPEF-CALC
    private DflRitIrpefCalc dflRitIrpefCalc = new DflRitIrpefCalc();
    //Original name: DFL-RIT-IRPEF-DFZ
    private DflRitIrpefDfz dflRitIrpefDfz = new DflRitIrpefDfz();
    //Original name: DFL-RIT-IRPEF-EFFLQ
    private DflRitIrpefEfflq dflRitIrpefEfflq = new DflRitIrpefEfflq();
    //Original name: DFL-IMPST-SOST-CALC
    private DflImpstSostCalc dflImpstSostCalc = new DflImpstSostCalc();
    //Original name: DFL-IMPST-SOST-DFZ
    private DflImpstSostDfz dflImpstSostDfz = new DflImpstSostDfz();
    //Original name: DFL-IMPST-SOST-EFFLQ
    private DflImpstSostEfflq dflImpstSostEfflq = new DflImpstSostEfflq();
    //Original name: DFL-TAX-SEP-CALC
    private DflTaxSepCalc dflTaxSepCalc = new DflTaxSepCalc();
    //Original name: DFL-TAX-SEP-DFZ
    private DflTaxSepDfz dflTaxSepDfz = new DflTaxSepDfz();
    //Original name: DFL-TAX-SEP-EFFLQ
    private DflTaxSepEfflq dflTaxSepEfflq = new DflTaxSepEfflq();
    //Original name: DFL-INTR-PREST-CALC
    private DflIntrPrestCalc dflIntrPrestCalc = new DflIntrPrestCalc();
    //Original name: DFL-INTR-PREST-DFZ
    private DflIntrPrestDfz dflIntrPrestDfz = new DflIntrPrestDfz();
    //Original name: DFL-INTR-PREST-EFFLQ
    private DflIntrPrestEfflq dflIntrPrestEfflq = new DflIntrPrestEfflq();
    //Original name: DFL-ACCPRE-SOST-CALC
    private DflAccpreSostCalc dflAccpreSostCalc = new DflAccpreSostCalc();
    //Original name: DFL-ACCPRE-SOST-DFZ
    private DflAccpreSostDfz dflAccpreSostDfz = new DflAccpreSostDfz();
    //Original name: DFL-ACCPRE-SOST-EFFLQ
    private DflAccpreSostEfflq dflAccpreSostEfflq = new DflAccpreSostEfflq();
    //Original name: DFL-ACCPRE-VIS-CALC
    private DflAccpreVisCalc dflAccpreVisCalc = new DflAccpreVisCalc();
    //Original name: DFL-ACCPRE-VIS-DFZ
    private DflAccpreVisDfz dflAccpreVisDfz = new DflAccpreVisDfz();
    //Original name: DFL-ACCPRE-VIS-EFFLQ
    private DflAccpreVisEfflq dflAccpreVisEfflq = new DflAccpreVisEfflq();
    //Original name: DFL-ACCPRE-ACC-CALC
    private DflAccpreAccCalc dflAccpreAccCalc = new DflAccpreAccCalc();
    //Original name: DFL-ACCPRE-ACC-DFZ
    private DflAccpreAccDfz dflAccpreAccDfz = new DflAccpreAccDfz();
    //Original name: DFL-ACCPRE-ACC-EFFLQ
    private DflAccpreAccEfflq dflAccpreAccEfflq = new DflAccpreAccEfflq();
    //Original name: DFL-RES-PRSTZ-CALC
    private DflResPrstzCalc dflResPrstzCalc = new DflResPrstzCalc();
    //Original name: DFL-RES-PRSTZ-DFZ
    private DflResPrstzDfz dflResPrstzDfz = new DflResPrstzDfz();
    //Original name: DFL-RES-PRSTZ-EFFLQ
    private DflResPrstzEfflq dflResPrstzEfflq = new DflResPrstzEfflq();
    //Original name: DFL-RES-PRE-ATT-CALC
    private DflResPreAttCalc dflResPreAttCalc = new DflResPreAttCalc();
    //Original name: DFL-RES-PRE-ATT-DFZ
    private DflResPreAttDfz dflResPreAttDfz = new DflResPreAttDfz();
    //Original name: DFL-RES-PRE-ATT-EFFLQ
    private DflResPreAttEfflq dflResPreAttEfflq = new DflResPreAttEfflq();
    //Original name: DFL-IMP-EXCONTR-EFF
    private DflImpExcontrEff dflImpExcontrEff = new DflImpExcontrEff();
    //Original name: DFL-IMPB-VIS-CALC
    private DflImpbVisCalc dflImpbVisCalc = new DflImpbVisCalc();
    //Original name: DFL-IMPB-VIS-EFFLQ
    private DflImpbVisEfflq dflImpbVisEfflq = new DflImpbVisEfflq();
    //Original name: DFL-IMPB-VIS-DFZ
    private DflImpbVisDfz dflImpbVisDfz = new DflImpbVisDfz();
    //Original name: DFL-IMPB-RIT-ACC-CALC
    private DflImpbRitAccCalc dflImpbRitAccCalc = new DflImpbRitAccCalc();
    //Original name: DFL-IMPB-RIT-ACC-EFFLQ
    private DflImpbRitAccEfflq dflImpbRitAccEfflq = new DflImpbRitAccEfflq();
    //Original name: DFL-IMPB-RIT-ACC-DFZ
    private DflImpbRitAccDfz dflImpbRitAccDfz = new DflImpbRitAccDfz();
    //Original name: DFL-IMPB-TFR-CALC
    private DflImpbTfrCalc dflImpbTfrCalc = new DflImpbTfrCalc();
    //Original name: DFL-IMPB-TFR-EFFLQ
    private DflImpbTfrEfflq dflImpbTfrEfflq = new DflImpbTfrEfflq();
    //Original name: DFL-IMPB-TFR-DFZ
    private DflImpbTfrDfz dflImpbTfrDfz = new DflImpbTfrDfz();
    //Original name: DFL-IMPB-IS-CALC
    private DflImpbIsCalc dflImpbIsCalc = new DflImpbIsCalc();
    //Original name: DFL-IMPB-IS-EFFLQ
    private DflImpbIsEfflq dflImpbIsEfflq = new DflImpbIsEfflq();
    //Original name: DFL-IMPB-IS-DFZ
    private DflImpbIsDfz dflImpbIsDfz = new DflImpbIsDfz();
    //Original name: DFL-IMPB-TAX-SEP-CALC
    private DflImpbTaxSepCalc dflImpbTaxSepCalc = new DflImpbTaxSepCalc();
    //Original name: DFL-IMPB-TAX-SEP-EFFLQ
    private DflImpbTaxSepEfflq dflImpbTaxSepEfflq = new DflImpbTaxSepEfflq();
    //Original name: DFL-IMPB-TAX-SEP-DFZ
    private DflImpbTaxSepDfz dflImpbTaxSepDfz = new DflImpbTaxSepDfz();
    //Original name: DFL-IINT-PREST-CALC
    private DflIintPrestCalc dflIintPrestCalc = new DflIintPrestCalc();
    //Original name: DFL-IINT-PREST-EFFLQ
    private DflIintPrestEfflq dflIintPrestEfflq = new DflIintPrestEfflq();
    //Original name: DFL-IINT-PREST-DFZ
    private DflIintPrestDfz dflIintPrestDfz = new DflIintPrestDfz();
    //Original name: DFL-MONT-END2000-CALC
    private DflMontEnd2000Calc dflMontEnd2000Calc = new DflMontEnd2000Calc();
    //Original name: DFL-MONT-END2000-EFFLQ
    private DflMontEnd2000Efflq dflMontEnd2000Efflq = new DflMontEnd2000Efflq();
    //Original name: DFL-MONT-END2000-DFZ
    private DflMontEnd2000Dfz dflMontEnd2000Dfz = new DflMontEnd2000Dfz();
    //Original name: DFL-MONT-END2006-CALC
    private DflMontEnd2006Calc dflMontEnd2006Calc = new DflMontEnd2006Calc();
    //Original name: DFL-MONT-END2006-EFFLQ
    private DflMontEnd2006Efflq dflMontEnd2006Efflq = new DflMontEnd2006Efflq();
    //Original name: DFL-MONT-END2006-DFZ
    private DflMontEnd2006Dfz dflMontEnd2006Dfz = new DflMontEnd2006Dfz();
    //Original name: DFL-MONT-DAL2007-CALC
    private DflMontDal2007Calc dflMontDal2007Calc = new DflMontDal2007Calc();
    //Original name: DFL-MONT-DAL2007-EFFLQ
    private DflMontDal2007Efflq dflMontDal2007Efflq = new DflMontDal2007Efflq();
    //Original name: DFL-MONT-DAL2007-DFZ
    private DflMontDal2007Dfz dflMontDal2007Dfz = new DflMontDal2007Dfz();
    //Original name: DFL-IIMPST-PRVR-CALC
    private DflIimpstPrvrCalc dflIimpstPrvrCalc = new DflIimpstPrvrCalc();
    //Original name: DFL-IIMPST-PRVR-EFFLQ
    private DflIimpstPrvrEfflq dflIimpstPrvrEfflq = new DflIimpstPrvrEfflq();
    //Original name: DFL-IIMPST-PRVR-DFZ
    private DflIimpstPrvrDfz dflIimpstPrvrDfz = new DflIimpstPrvrDfz();
    //Original name: DFL-IIMPST-252-CALC
    private DflIimpst252Calc dflIimpst252Calc = new DflIimpst252Calc();
    //Original name: DFL-IIMPST-252-EFFLQ
    private DflIimpst252Efflq dflIimpst252Efflq = new DflIimpst252Efflq();
    //Original name: DFL-IIMPST-252-DFZ
    private DflIimpst252Dfz dflIimpst252Dfz = new DflIimpst252Dfz();
    //Original name: DFL-IMPST-252-CALC
    private DflImpst252Calc dflImpst252Calc = new DflImpst252Calc();
    //Original name: DFL-IMPST-252-EFFLQ
    private DflImpst252Efflq dflImpst252Efflq = new DflImpst252Efflq();
    //Original name: DFL-RIT-TFR-CALC
    private DflRitTfrCalc dflRitTfrCalc = new DflRitTfrCalc();
    //Original name: DFL-RIT-TFR-EFFLQ
    private DflRitTfrEfflq dflRitTfrEfflq = new DflRitTfrEfflq();
    //Original name: DFL-RIT-TFR-DFZ
    private DflRitTfrDfz dflRitTfrDfz = new DflRitTfrDfz();
    //Original name: DFL-CNBT-INPSTFM-CALC
    private DflCnbtInpstfmCalc dflCnbtInpstfmCalc = new DflCnbtInpstfmCalc();
    //Original name: DFL-CNBT-INPSTFM-EFFLQ
    private DflCnbtInpstfmEfflq dflCnbtInpstfmEfflq = new DflCnbtInpstfmEfflq();
    //Original name: DFL-CNBT-INPSTFM-DFZ
    private DflCnbtInpstfmDfz dflCnbtInpstfmDfz = new DflCnbtInpstfmDfz();
    //Original name: DFL-ICNB-INPSTFM-CALC
    private DflIcnbInpstfmCalc dflIcnbInpstfmCalc = new DflIcnbInpstfmCalc();
    //Original name: DFL-ICNB-INPSTFM-EFFLQ
    private DflIcnbInpstfmEfflq dflIcnbInpstfmEfflq = new DflIcnbInpstfmEfflq();
    //Original name: DFL-ICNB-INPSTFM-DFZ
    private DflIcnbInpstfmDfz dflIcnbInpstfmDfz = new DflIcnbInpstfmDfz();
    //Original name: DFL-CNDE-END2000-CALC
    private DflCndeEnd2000Calc dflCndeEnd2000Calc = new DflCndeEnd2000Calc();
    //Original name: DFL-CNDE-END2000-EFFLQ
    private DflCndeEnd2000Efflq dflCndeEnd2000Efflq = new DflCndeEnd2000Efflq();
    //Original name: DFL-CNDE-END2000-DFZ
    private DflCndeEnd2000Dfz dflCndeEnd2000Dfz = new DflCndeEnd2000Dfz();
    //Original name: DFL-CNDE-END2006-CALC
    private DflCndeEnd2006Calc dflCndeEnd2006Calc = new DflCndeEnd2006Calc();
    //Original name: DFL-CNDE-END2006-EFFLQ
    private DflCndeEnd2006Efflq dflCndeEnd2006Efflq = new DflCndeEnd2006Efflq();
    //Original name: DFL-CNDE-END2006-DFZ
    private DflCndeEnd2006Dfz dflCndeEnd2006Dfz = new DflCndeEnd2006Dfz();
    //Original name: DFL-CNDE-DAL2007-CALC
    private DflCndeDal2007Calc dflCndeDal2007Calc = new DflCndeDal2007Calc();
    //Original name: DFL-CNDE-DAL2007-EFFLQ
    private DflCndeDal2007Efflq dflCndeDal2007Efflq = new DflCndeDal2007Efflq();
    //Original name: DFL-CNDE-DAL2007-DFZ
    private DflCndeDal2007Dfz dflCndeDal2007Dfz = new DflCndeDal2007Dfz();
    //Original name: DFL-AA-CNBZ-END2000-EF
    private DflAaCnbzEnd2000Ef dflAaCnbzEnd2000Ef = new DflAaCnbzEnd2000Ef();
    //Original name: DFL-AA-CNBZ-END2006-EF
    private DflAaCnbzEnd2006Ef dflAaCnbzEnd2006Ef = new DflAaCnbzEnd2006Ef();
    //Original name: DFL-AA-CNBZ-DAL2007-EF
    private DflAaCnbzDal2007Ef dflAaCnbzDal2007Ef = new DflAaCnbzDal2007Ef();
    //Original name: DFL-MM-CNBZ-END2000-EF
    private DflMmCnbzEnd2000Ef dflMmCnbzEnd2000Ef = new DflMmCnbzEnd2000Ef();
    //Original name: DFL-MM-CNBZ-END2006-EF
    private DflMmCnbzEnd2006Ef dflMmCnbzEnd2006Ef = new DflMmCnbzEnd2006Ef();
    //Original name: DFL-MM-CNBZ-DAL2007-EF
    private DflMmCnbzDal2007Ef dflMmCnbzDal2007Ef = new DflMmCnbzDal2007Ef();
    //Original name: DFL-IMPST-DA-RIMB-EFF
    private DflImpstDaRimbEff dflImpstDaRimbEff = new DflImpstDaRimbEff();
    //Original name: DFL-ALQ-TAX-SEP-CALC
    private DflAlqTaxSepCalc dflAlqTaxSepCalc = new DflAlqTaxSepCalc();
    //Original name: DFL-ALQ-TAX-SEP-EFFLQ
    private DflAlqTaxSepEfflq dflAlqTaxSepEfflq = new DflAlqTaxSepEfflq();
    //Original name: DFL-ALQ-TAX-SEP-DFZ
    private DflAlqTaxSepDfz dflAlqTaxSepDfz = new DflAlqTaxSepDfz();
    //Original name: DFL-ALQ-CNBT-INPSTFM-C
    private DflAlqCnbtInpstfmC dflAlqCnbtInpstfmC = new DflAlqCnbtInpstfmC();
    //Original name: DFL-ALQ-CNBT-INPSTFM-E
    private DflAlqCnbtInpstfmE dflAlqCnbtInpstfmE = new DflAlqCnbtInpstfmE();
    //Original name: DFL-ALQ-CNBT-INPSTFM-D
    private DflAlqCnbtInpstfmD dflAlqCnbtInpstfmD = new DflAlqCnbtInpstfmD();
    //Original name: DFL-DS-RIGA
    private long dflDsRiga = DefaultValues.LONG_VAL;
    //Original name: DFL-DS-OPER-SQL
    private char dflDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: DFL-DS-VER
    private int dflDsVer = DefaultValues.INT_VAL;
    //Original name: DFL-DS-TS-INI-CPTZ
    private long dflDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: DFL-DS-TS-END-CPTZ
    private long dflDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: DFL-DS-UTENTE
    private String dflDsUtente = DefaultValues.stringVal(Len.DFL_DS_UTENTE);
    //Original name: DFL-DS-STATO-ELAB
    private char dflDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: DFL-IMPB-VIS-1382011C
    private DflImpbVis1382011c dflImpbVis1382011c = new DflImpbVis1382011c();
    //Original name: DFL-IMPB-VIS-1382011D
    private DflImpbVis1382011d dflImpbVis1382011d = new DflImpbVis1382011d();
    //Original name: DFL-IMPB-VIS-1382011L
    private DflImpbVis1382011l dflImpbVis1382011l = new DflImpbVis1382011l();
    //Original name: DFL-IMPST-VIS-1382011C
    private DflImpstVis1382011c dflImpstVis1382011c = new DflImpstVis1382011c();
    //Original name: DFL-IMPST-VIS-1382011D
    private DflImpstVis1382011d dflImpstVis1382011d = new DflImpstVis1382011d();
    //Original name: DFL-IMPST-VIS-1382011L
    private DflImpstVis1382011l dflImpstVis1382011l = new DflImpstVis1382011l();
    //Original name: DFL-IMPB-IS-1382011C
    private DflImpbIs1382011c dflImpbIs1382011c = new DflImpbIs1382011c();
    //Original name: DFL-IMPB-IS-1382011D
    private DflImpbIs1382011d dflImpbIs1382011d = new DflImpbIs1382011d();
    //Original name: DFL-IMPB-IS-1382011L
    private DflImpbIs1382011l dflImpbIs1382011l = new DflImpbIs1382011l();
    //Original name: DFL-IS-1382011C
    private DflIs1382011c dflIs1382011c = new DflIs1382011c();
    //Original name: DFL-IS-1382011D
    private DflIs1382011d dflIs1382011d = new DflIs1382011d();
    //Original name: DFL-IS-1382011L
    private DflIs1382011l dflIs1382011l = new DflIs1382011l();
    //Original name: DFL-IMP-INTR-RIT-PAG-C
    private DflImpIntrRitPagC dflImpIntrRitPagC = new DflImpIntrRitPagC();
    //Original name: DFL-IMP-INTR-RIT-PAG-D
    private DflImpIntrRitPagD dflImpIntrRitPagD = new DflImpIntrRitPagD();
    //Original name: DFL-IMP-INTR-RIT-PAG-L
    private DflImpIntrRitPagL dflImpIntrRitPagL = new DflImpIntrRitPagL();
    //Original name: DFL-IMPB-BOLLO-DETT-C
    private DflImpbBolloDettC dflImpbBolloDettC = new DflImpbBolloDettC();
    //Original name: DFL-IMPB-BOLLO-DETT-D
    private DflImpbBolloDettD dflImpbBolloDettD = new DflImpbBolloDettD();
    //Original name: DFL-IMPB-BOLLO-DETT-L
    private DflImpbBolloDettL dflImpbBolloDettL = new DflImpbBolloDettL();
    //Original name: DFL-IMPST-BOLLO-DETT-C
    private DflImpstBolloDettC dflImpstBolloDettC = new DflImpstBolloDettC();
    //Original name: DFL-IMPST-BOLLO-DETT-D
    private DflImpstBolloDettD dflImpstBolloDettD = new DflImpstBolloDettD();
    //Original name: DFL-IMPST-BOLLO-DETT-L
    private DflImpstBolloDettL dflImpstBolloDettL = new DflImpstBolloDettL();
    //Original name: DFL-IMPST-BOLLO-TOT-VC
    private DflImpstBolloTotVc dflImpstBolloTotVc = new DflImpstBolloTotVc();
    //Original name: DFL-IMPST-BOLLO-TOT-VD
    private DflImpstBolloTotVd dflImpstBolloTotVd = new DflImpstBolloTotVd();
    //Original name: DFL-IMPST-BOLLO-TOT-VL
    private DflImpstBolloTotVl dflImpstBolloTotVl = new DflImpstBolloTotVl();
    //Original name: DFL-IMPB-VIS-662014C
    private DflImpbVis662014c dflImpbVis662014c = new DflImpbVis662014c();
    //Original name: DFL-IMPB-VIS-662014D
    private DflImpbVis662014d dflImpbVis662014d = new DflImpbVis662014d();
    //Original name: DFL-IMPB-VIS-662014L
    private DflImpbVis662014l dflImpbVis662014l = new DflImpbVis662014l();
    //Original name: DFL-IMPST-VIS-662014C
    private DflImpstVis662014c dflImpstVis662014c = new DflImpstVis662014c();
    //Original name: DFL-IMPST-VIS-662014D
    private DflImpstVis662014d dflImpstVis662014d = new DflImpstVis662014d();
    //Original name: DFL-IMPST-VIS-662014L
    private DflImpstVis662014l dflImpstVis662014l = new DflImpstVis662014l();
    //Original name: DFL-IMPB-IS-662014C
    private DflImpbIs662014c dflImpbIs662014c = new DflImpbIs662014c();
    //Original name: DFL-IMPB-IS-662014D
    private DflImpbIs662014d dflImpbIs662014d = new DflImpbIs662014d();
    //Original name: DFL-IMPB-IS-662014L
    private DflImpbIs662014l dflImpbIs662014l = new DflImpbIs662014l();
    //Original name: DFL-IS-662014C
    private DflIs662014c dflIs662014c = new DflIs662014c();
    //Original name: DFL-IS-662014D
    private DflIs662014d dflIs662014d = new DflIs662014d();
    //Original name: DFL-IS-662014L
    private DflIs662014l dflIs662014l = new DflIs662014l();

    //==== METHODS ====
    public void setdForzLiqFormatted(String data) {
        byte[] buffer = new byte[Len.D_FORZ_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.D_FORZ_LIQ);
        setdForzLiqBytes(buffer, 1);
    }

    public void setdForzLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        dflIdDForzLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFL_ID_D_FORZ_LIQ, 0);
        position += Len.DFL_ID_D_FORZ_LIQ;
        dflIdLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFL_ID_LIQ, 0);
        position += Len.DFL_ID_LIQ;
        dflIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFL_ID_MOVI_CRZ, 0);
        position += Len.DFL_ID_MOVI_CRZ;
        dflIdMoviChiu.setDflIdMoviChiuFromBuffer(buffer, position);
        position += DflIdMoviChiu.Len.DFL_ID_MOVI_CHIU;
        dflCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFL_COD_COMP_ANIA, 0);
        position += Len.DFL_COD_COMP_ANIA;
        dflDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFL_DT_INI_EFF, 0);
        position += Len.DFL_DT_INI_EFF;
        dflDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFL_DT_END_EFF, 0);
        position += Len.DFL_DT_END_EFF;
        dflImpLrdCalc.setDflImpLrdCalcFromBuffer(buffer, position);
        position += DflImpLrdCalc.Len.DFL_IMP_LRD_CALC;
        dflImpLrdDfz.setDflImpLrdDfzFromBuffer(buffer, position);
        position += DflImpLrdDfz.Len.DFL_IMP_LRD_DFZ;
        dflImpLrdEfflq.setDflImpLrdEfflqFromBuffer(buffer, position);
        position += DflImpLrdEfflq.Len.DFL_IMP_LRD_EFFLQ;
        dflImpNetCalc.setDflImpNetCalcFromBuffer(buffer, position);
        position += DflImpNetCalc.Len.DFL_IMP_NET_CALC;
        dflImpNetDfz.setDflImpNetDfzFromBuffer(buffer, position);
        position += DflImpNetDfz.Len.DFL_IMP_NET_DFZ;
        dflImpNetEfflq.setDflImpNetEfflqFromBuffer(buffer, position);
        position += DflImpNetEfflq.Len.DFL_IMP_NET_EFFLQ;
        dflImpstPrvrCalc.setDflImpstPrvrCalcFromBuffer(buffer, position);
        position += DflImpstPrvrCalc.Len.DFL_IMPST_PRVR_CALC;
        dflImpstPrvrDfz.setDflImpstPrvrDfzFromBuffer(buffer, position);
        position += DflImpstPrvrDfz.Len.DFL_IMPST_PRVR_DFZ;
        dflImpstPrvrEfflq.setDflImpstPrvrEfflqFromBuffer(buffer, position);
        position += DflImpstPrvrEfflq.Len.DFL_IMPST_PRVR_EFFLQ;
        dflImpstVisCalc.setDflImpstVisCalcFromBuffer(buffer, position);
        position += DflImpstVisCalc.Len.DFL_IMPST_VIS_CALC;
        dflImpstVisDfz.setDflImpstVisDfzFromBuffer(buffer, position);
        position += DflImpstVisDfz.Len.DFL_IMPST_VIS_DFZ;
        dflImpstVisEfflq.setDflImpstVisEfflqFromBuffer(buffer, position);
        position += DflImpstVisEfflq.Len.DFL_IMPST_VIS_EFFLQ;
        dflRitAccCalc.setDflRitAccCalcFromBuffer(buffer, position);
        position += DflRitAccCalc.Len.DFL_RIT_ACC_CALC;
        dflRitAccDfz.setDflRitAccDfzFromBuffer(buffer, position);
        position += DflRitAccDfz.Len.DFL_RIT_ACC_DFZ;
        dflRitAccEfflq.setDflRitAccEfflqFromBuffer(buffer, position);
        position += DflRitAccEfflq.Len.DFL_RIT_ACC_EFFLQ;
        dflRitIrpefCalc.setDflRitIrpefCalcFromBuffer(buffer, position);
        position += DflRitIrpefCalc.Len.DFL_RIT_IRPEF_CALC;
        dflRitIrpefDfz.setDflRitIrpefDfzFromBuffer(buffer, position);
        position += DflRitIrpefDfz.Len.DFL_RIT_IRPEF_DFZ;
        dflRitIrpefEfflq.setDflRitIrpefEfflqFromBuffer(buffer, position);
        position += DflRitIrpefEfflq.Len.DFL_RIT_IRPEF_EFFLQ;
        dflImpstSostCalc.setDflImpstSostCalcFromBuffer(buffer, position);
        position += DflImpstSostCalc.Len.DFL_IMPST_SOST_CALC;
        dflImpstSostDfz.setDflImpstSostDfzFromBuffer(buffer, position);
        position += DflImpstSostDfz.Len.DFL_IMPST_SOST_DFZ;
        dflImpstSostEfflq.setDflImpstSostEfflqFromBuffer(buffer, position);
        position += DflImpstSostEfflq.Len.DFL_IMPST_SOST_EFFLQ;
        dflTaxSepCalc.setDflTaxSepCalcFromBuffer(buffer, position);
        position += DflTaxSepCalc.Len.DFL_TAX_SEP_CALC;
        dflTaxSepDfz.setDflTaxSepDfzFromBuffer(buffer, position);
        position += DflTaxSepDfz.Len.DFL_TAX_SEP_DFZ;
        dflTaxSepEfflq.setDflTaxSepEfflqFromBuffer(buffer, position);
        position += DflTaxSepEfflq.Len.DFL_TAX_SEP_EFFLQ;
        dflIntrPrestCalc.setDflIntrPrestCalcFromBuffer(buffer, position);
        position += DflIntrPrestCalc.Len.DFL_INTR_PREST_CALC;
        dflIntrPrestDfz.setDflIntrPrestDfzFromBuffer(buffer, position);
        position += DflIntrPrestDfz.Len.DFL_INTR_PREST_DFZ;
        dflIntrPrestEfflq.setDflIntrPrestEfflqFromBuffer(buffer, position);
        position += DflIntrPrestEfflq.Len.DFL_INTR_PREST_EFFLQ;
        dflAccpreSostCalc.setDflAccpreSostCalcFromBuffer(buffer, position);
        position += DflAccpreSostCalc.Len.DFL_ACCPRE_SOST_CALC;
        dflAccpreSostDfz.setDflAccpreSostDfzFromBuffer(buffer, position);
        position += DflAccpreSostDfz.Len.DFL_ACCPRE_SOST_DFZ;
        dflAccpreSostEfflq.setDflAccpreSostEfflqFromBuffer(buffer, position);
        position += DflAccpreSostEfflq.Len.DFL_ACCPRE_SOST_EFFLQ;
        dflAccpreVisCalc.setDflAccpreVisCalcFromBuffer(buffer, position);
        position += DflAccpreVisCalc.Len.DFL_ACCPRE_VIS_CALC;
        dflAccpreVisDfz.setDflAccpreVisDfzFromBuffer(buffer, position);
        position += DflAccpreVisDfz.Len.DFL_ACCPRE_VIS_DFZ;
        dflAccpreVisEfflq.setDflAccpreVisEfflqFromBuffer(buffer, position);
        position += DflAccpreVisEfflq.Len.DFL_ACCPRE_VIS_EFFLQ;
        dflAccpreAccCalc.setDflAccpreAccCalcFromBuffer(buffer, position);
        position += DflAccpreAccCalc.Len.DFL_ACCPRE_ACC_CALC;
        dflAccpreAccDfz.setDflAccpreAccDfzFromBuffer(buffer, position);
        position += DflAccpreAccDfz.Len.DFL_ACCPRE_ACC_DFZ;
        dflAccpreAccEfflq.setDflAccpreAccEfflqFromBuffer(buffer, position);
        position += DflAccpreAccEfflq.Len.DFL_ACCPRE_ACC_EFFLQ;
        dflResPrstzCalc.setDflResPrstzCalcFromBuffer(buffer, position);
        position += DflResPrstzCalc.Len.DFL_RES_PRSTZ_CALC;
        dflResPrstzDfz.setDflResPrstzDfzFromBuffer(buffer, position);
        position += DflResPrstzDfz.Len.DFL_RES_PRSTZ_DFZ;
        dflResPrstzEfflq.setDflResPrstzEfflqFromBuffer(buffer, position);
        position += DflResPrstzEfflq.Len.DFL_RES_PRSTZ_EFFLQ;
        dflResPreAttCalc.setDflResPreAttCalcFromBuffer(buffer, position);
        position += DflResPreAttCalc.Len.DFL_RES_PRE_ATT_CALC;
        dflResPreAttDfz.setDflResPreAttDfzFromBuffer(buffer, position);
        position += DflResPreAttDfz.Len.DFL_RES_PRE_ATT_DFZ;
        dflResPreAttEfflq.setDflResPreAttEfflqFromBuffer(buffer, position);
        position += DflResPreAttEfflq.Len.DFL_RES_PRE_ATT_EFFLQ;
        dflImpExcontrEff.setDflImpExcontrEffFromBuffer(buffer, position);
        position += DflImpExcontrEff.Len.DFL_IMP_EXCONTR_EFF;
        dflImpbVisCalc.setDflImpbVisCalcFromBuffer(buffer, position);
        position += DflImpbVisCalc.Len.DFL_IMPB_VIS_CALC;
        dflImpbVisEfflq.setDflImpbVisEfflqFromBuffer(buffer, position);
        position += DflImpbVisEfflq.Len.DFL_IMPB_VIS_EFFLQ;
        dflImpbVisDfz.setDflImpbVisDfzFromBuffer(buffer, position);
        position += DflImpbVisDfz.Len.DFL_IMPB_VIS_DFZ;
        dflImpbRitAccCalc.setDflImpbRitAccCalcFromBuffer(buffer, position);
        position += DflImpbRitAccCalc.Len.DFL_IMPB_RIT_ACC_CALC;
        dflImpbRitAccEfflq.setDflImpbRitAccEfflqFromBuffer(buffer, position);
        position += DflImpbRitAccEfflq.Len.DFL_IMPB_RIT_ACC_EFFLQ;
        dflImpbRitAccDfz.setDflImpbRitAccDfzFromBuffer(buffer, position);
        position += DflImpbRitAccDfz.Len.DFL_IMPB_RIT_ACC_DFZ;
        dflImpbTfrCalc.setDflImpbTfrCalcFromBuffer(buffer, position);
        position += DflImpbTfrCalc.Len.DFL_IMPB_TFR_CALC;
        dflImpbTfrEfflq.setDflImpbTfrEfflqFromBuffer(buffer, position);
        position += DflImpbTfrEfflq.Len.DFL_IMPB_TFR_EFFLQ;
        dflImpbTfrDfz.setDflImpbTfrDfzFromBuffer(buffer, position);
        position += DflImpbTfrDfz.Len.DFL_IMPB_TFR_DFZ;
        dflImpbIsCalc.setDflImpbIsCalcFromBuffer(buffer, position);
        position += DflImpbIsCalc.Len.DFL_IMPB_IS_CALC;
        dflImpbIsEfflq.setDflImpbIsEfflqFromBuffer(buffer, position);
        position += DflImpbIsEfflq.Len.DFL_IMPB_IS_EFFLQ;
        dflImpbIsDfz.setDflImpbIsDfzFromBuffer(buffer, position);
        position += DflImpbIsDfz.Len.DFL_IMPB_IS_DFZ;
        dflImpbTaxSepCalc.setDflImpbTaxSepCalcFromBuffer(buffer, position);
        position += DflImpbTaxSepCalc.Len.DFL_IMPB_TAX_SEP_CALC;
        dflImpbTaxSepEfflq.setDflImpbTaxSepEfflqFromBuffer(buffer, position);
        position += DflImpbTaxSepEfflq.Len.DFL_IMPB_TAX_SEP_EFFLQ;
        dflImpbTaxSepDfz.setDflImpbTaxSepDfzFromBuffer(buffer, position);
        position += DflImpbTaxSepDfz.Len.DFL_IMPB_TAX_SEP_DFZ;
        dflIintPrestCalc.setDflIintPrestCalcFromBuffer(buffer, position);
        position += DflIintPrestCalc.Len.DFL_IINT_PREST_CALC;
        dflIintPrestEfflq.setDflIintPrestEfflqFromBuffer(buffer, position);
        position += DflIintPrestEfflq.Len.DFL_IINT_PREST_EFFLQ;
        dflIintPrestDfz.setDflIintPrestDfzFromBuffer(buffer, position);
        position += DflIintPrestDfz.Len.DFL_IINT_PREST_DFZ;
        dflMontEnd2000Calc.setDflMontEnd2000CalcFromBuffer(buffer, position);
        position += DflMontEnd2000Calc.Len.DFL_MONT_END2000_CALC;
        dflMontEnd2000Efflq.setDflMontEnd2000EfflqFromBuffer(buffer, position);
        position += DflMontEnd2000Efflq.Len.DFL_MONT_END2000_EFFLQ;
        dflMontEnd2000Dfz.setDflMontEnd2000DfzFromBuffer(buffer, position);
        position += DflMontEnd2000Dfz.Len.DFL_MONT_END2000_DFZ;
        dflMontEnd2006Calc.setDflMontEnd2006CalcFromBuffer(buffer, position);
        position += DflMontEnd2006Calc.Len.DFL_MONT_END2006_CALC;
        dflMontEnd2006Efflq.setDflMontEnd2006EfflqFromBuffer(buffer, position);
        position += DflMontEnd2006Efflq.Len.DFL_MONT_END2006_EFFLQ;
        dflMontEnd2006Dfz.setDflMontEnd2006DfzFromBuffer(buffer, position);
        position += DflMontEnd2006Dfz.Len.DFL_MONT_END2006_DFZ;
        dflMontDal2007Calc.setDflMontDal2007CalcFromBuffer(buffer, position);
        position += DflMontDal2007Calc.Len.DFL_MONT_DAL2007_CALC;
        dflMontDal2007Efflq.setDflMontDal2007EfflqFromBuffer(buffer, position);
        position += DflMontDal2007Efflq.Len.DFL_MONT_DAL2007_EFFLQ;
        dflMontDal2007Dfz.setDflMontDal2007DfzFromBuffer(buffer, position);
        position += DflMontDal2007Dfz.Len.DFL_MONT_DAL2007_DFZ;
        dflIimpstPrvrCalc.setDflIimpstPrvrCalcFromBuffer(buffer, position);
        position += DflIimpstPrvrCalc.Len.DFL_IIMPST_PRVR_CALC;
        dflIimpstPrvrEfflq.setDflIimpstPrvrEfflqFromBuffer(buffer, position);
        position += DflIimpstPrvrEfflq.Len.DFL_IIMPST_PRVR_EFFLQ;
        dflIimpstPrvrDfz.setDflIimpstPrvrDfzFromBuffer(buffer, position);
        position += DflIimpstPrvrDfz.Len.DFL_IIMPST_PRVR_DFZ;
        dflIimpst252Calc.setDflIimpst252CalcFromBuffer(buffer, position);
        position += DflIimpst252Calc.Len.DFL_IIMPST252_CALC;
        dflIimpst252Efflq.setDflIimpst252EfflqFromBuffer(buffer, position);
        position += DflIimpst252Efflq.Len.DFL_IIMPST252_EFFLQ;
        dflIimpst252Dfz.setDflIimpst252DfzFromBuffer(buffer, position);
        position += DflIimpst252Dfz.Len.DFL_IIMPST252_DFZ;
        dflImpst252Calc.setDflImpst252CalcFromBuffer(buffer, position);
        position += DflImpst252Calc.Len.DFL_IMPST252_CALC;
        dflImpst252Efflq.setDflImpst252EfflqFromBuffer(buffer, position);
        position += DflImpst252Efflq.Len.DFL_IMPST252_EFFLQ;
        dflRitTfrCalc.setDflRitTfrCalcFromBuffer(buffer, position);
        position += DflRitTfrCalc.Len.DFL_RIT_TFR_CALC;
        dflRitTfrEfflq.setDflRitTfrEfflqFromBuffer(buffer, position);
        position += DflRitTfrEfflq.Len.DFL_RIT_TFR_EFFLQ;
        dflRitTfrDfz.setDflRitTfrDfzFromBuffer(buffer, position);
        position += DflRitTfrDfz.Len.DFL_RIT_TFR_DFZ;
        dflCnbtInpstfmCalc.setDflCnbtInpstfmCalcFromBuffer(buffer, position);
        position += DflCnbtInpstfmCalc.Len.DFL_CNBT_INPSTFM_CALC;
        dflCnbtInpstfmEfflq.setDflCnbtInpstfmEfflqFromBuffer(buffer, position);
        position += DflCnbtInpstfmEfflq.Len.DFL_CNBT_INPSTFM_EFFLQ;
        dflCnbtInpstfmDfz.setDflCnbtInpstfmDfzFromBuffer(buffer, position);
        position += DflCnbtInpstfmDfz.Len.DFL_CNBT_INPSTFM_DFZ;
        dflIcnbInpstfmCalc.setDflIcnbInpstfmCalcFromBuffer(buffer, position);
        position += DflIcnbInpstfmCalc.Len.DFL_ICNB_INPSTFM_CALC;
        dflIcnbInpstfmEfflq.setDflIcnbInpstfmEfflqFromBuffer(buffer, position);
        position += DflIcnbInpstfmEfflq.Len.DFL_ICNB_INPSTFM_EFFLQ;
        dflIcnbInpstfmDfz.setDflIcnbInpstfmDfzFromBuffer(buffer, position);
        position += DflIcnbInpstfmDfz.Len.DFL_ICNB_INPSTFM_DFZ;
        dflCndeEnd2000Calc.setDflCndeEnd2000CalcFromBuffer(buffer, position);
        position += DflCndeEnd2000Calc.Len.DFL_CNDE_END2000_CALC;
        dflCndeEnd2000Efflq.setDflCndeEnd2000EfflqFromBuffer(buffer, position);
        position += DflCndeEnd2000Efflq.Len.DFL_CNDE_END2000_EFFLQ;
        dflCndeEnd2000Dfz.setDflCndeEnd2000DfzFromBuffer(buffer, position);
        position += DflCndeEnd2000Dfz.Len.DFL_CNDE_END2000_DFZ;
        dflCndeEnd2006Calc.setDflCndeEnd2006CalcFromBuffer(buffer, position);
        position += DflCndeEnd2006Calc.Len.DFL_CNDE_END2006_CALC;
        dflCndeEnd2006Efflq.setDflCndeEnd2006EfflqFromBuffer(buffer, position);
        position += DflCndeEnd2006Efflq.Len.DFL_CNDE_END2006_EFFLQ;
        dflCndeEnd2006Dfz.setDflCndeEnd2006DfzFromBuffer(buffer, position);
        position += DflCndeEnd2006Dfz.Len.DFL_CNDE_END2006_DFZ;
        dflCndeDal2007Calc.setDflCndeDal2007CalcFromBuffer(buffer, position);
        position += DflCndeDal2007Calc.Len.DFL_CNDE_DAL2007_CALC;
        dflCndeDal2007Efflq.setDflCndeDal2007EfflqFromBuffer(buffer, position);
        position += DflCndeDal2007Efflq.Len.DFL_CNDE_DAL2007_EFFLQ;
        dflCndeDal2007Dfz.setDflCndeDal2007DfzFromBuffer(buffer, position);
        position += DflCndeDal2007Dfz.Len.DFL_CNDE_DAL2007_DFZ;
        dflAaCnbzEnd2000Ef.setDflAaCnbzEnd2000EfFromBuffer(buffer, position);
        position += DflAaCnbzEnd2000Ef.Len.DFL_AA_CNBZ_END2000_EF;
        dflAaCnbzEnd2006Ef.setDflAaCnbzEnd2006EfFromBuffer(buffer, position);
        position += DflAaCnbzEnd2006Ef.Len.DFL_AA_CNBZ_END2006_EF;
        dflAaCnbzDal2007Ef.setDflAaCnbzDal2007EfFromBuffer(buffer, position);
        position += DflAaCnbzDal2007Ef.Len.DFL_AA_CNBZ_DAL2007_EF;
        dflMmCnbzEnd2000Ef.setDflMmCnbzEnd2000EfFromBuffer(buffer, position);
        position += DflMmCnbzEnd2000Ef.Len.DFL_MM_CNBZ_END2000_EF;
        dflMmCnbzEnd2006Ef.setDflMmCnbzEnd2006EfFromBuffer(buffer, position);
        position += DflMmCnbzEnd2006Ef.Len.DFL_MM_CNBZ_END2006_EF;
        dflMmCnbzDal2007Ef.setDflMmCnbzDal2007EfFromBuffer(buffer, position);
        position += DflMmCnbzDal2007Ef.Len.DFL_MM_CNBZ_DAL2007_EF;
        dflImpstDaRimbEff.setDflImpstDaRimbEffFromBuffer(buffer, position);
        position += DflImpstDaRimbEff.Len.DFL_IMPST_DA_RIMB_EFF;
        dflAlqTaxSepCalc.setDflAlqTaxSepCalcFromBuffer(buffer, position);
        position += DflAlqTaxSepCalc.Len.DFL_ALQ_TAX_SEP_CALC;
        dflAlqTaxSepEfflq.setDflAlqTaxSepEfflqFromBuffer(buffer, position);
        position += DflAlqTaxSepEfflq.Len.DFL_ALQ_TAX_SEP_EFFLQ;
        dflAlqTaxSepDfz.setDflAlqTaxSepDfzFromBuffer(buffer, position);
        position += DflAlqTaxSepDfz.Len.DFL_ALQ_TAX_SEP_DFZ;
        dflAlqCnbtInpstfmC.setDflAlqCnbtInpstfmCFromBuffer(buffer, position);
        position += DflAlqCnbtInpstfmC.Len.DFL_ALQ_CNBT_INPSTFM_C;
        dflAlqCnbtInpstfmE.setDflAlqCnbtInpstfmEFromBuffer(buffer, position);
        position += DflAlqCnbtInpstfmE.Len.DFL_ALQ_CNBT_INPSTFM_E;
        dflAlqCnbtInpstfmD.setDflAlqCnbtInpstfmDFromBuffer(buffer, position);
        position += DflAlqCnbtInpstfmD.Len.DFL_ALQ_CNBT_INPSTFM_D;
        dflDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DFL_DS_RIGA, 0);
        position += Len.DFL_DS_RIGA;
        dflDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dflDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DFL_DS_VER, 0);
        position += Len.DFL_DS_VER;
        dflDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DFL_DS_TS_INI_CPTZ, 0);
        position += Len.DFL_DS_TS_INI_CPTZ;
        dflDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DFL_DS_TS_END_CPTZ, 0);
        position += Len.DFL_DS_TS_END_CPTZ;
        dflDsUtente = MarshalByte.readString(buffer, position, Len.DFL_DS_UTENTE);
        position += Len.DFL_DS_UTENTE;
        dflDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dflImpbVis1382011c.setDflImpbVis1382011cFromBuffer(buffer, position);
        position += DflImpbVis1382011c.Len.DFL_IMPB_VIS1382011C;
        dflImpbVis1382011d.setDflImpbVis1382011dFromBuffer(buffer, position);
        position += DflImpbVis1382011d.Len.DFL_IMPB_VIS1382011D;
        dflImpbVis1382011l.setDflImpbVis1382011lFromBuffer(buffer, position);
        position += DflImpbVis1382011l.Len.DFL_IMPB_VIS1382011L;
        dflImpstVis1382011c.setDflImpstVis1382011cFromBuffer(buffer, position);
        position += DflImpstVis1382011c.Len.DFL_IMPST_VIS1382011C;
        dflImpstVis1382011d.setDflImpstVis1382011dFromBuffer(buffer, position);
        position += DflImpstVis1382011d.Len.DFL_IMPST_VIS1382011D;
        dflImpstVis1382011l.setDflImpstVis1382011lFromBuffer(buffer, position);
        position += DflImpstVis1382011l.Len.DFL_IMPST_VIS1382011L;
        dflImpbIs1382011c.setDflImpbIs1382011cFromBuffer(buffer, position);
        position += DflImpbIs1382011c.Len.DFL_IMPB_IS1382011C;
        dflImpbIs1382011d.setDflImpbIs1382011dFromBuffer(buffer, position);
        position += DflImpbIs1382011d.Len.DFL_IMPB_IS1382011D;
        dflImpbIs1382011l.setDflImpbIs1382011lFromBuffer(buffer, position);
        position += DflImpbIs1382011l.Len.DFL_IMPB_IS1382011L;
        dflIs1382011c.setDflIs1382011cFromBuffer(buffer, position);
        position += DflIs1382011c.Len.DFL_IS1382011C;
        dflIs1382011d.setDflIs1382011dFromBuffer(buffer, position);
        position += DflIs1382011d.Len.DFL_IS1382011D;
        dflIs1382011l.setDflIs1382011lFromBuffer(buffer, position);
        position += DflIs1382011l.Len.DFL_IS1382011L;
        dflImpIntrRitPagC.setDflImpIntrRitPagCFromBuffer(buffer, position);
        position += DflImpIntrRitPagC.Len.DFL_IMP_INTR_RIT_PAG_C;
        dflImpIntrRitPagD.setDflImpIntrRitPagDFromBuffer(buffer, position);
        position += DflImpIntrRitPagD.Len.DFL_IMP_INTR_RIT_PAG_D;
        dflImpIntrRitPagL.setDflImpIntrRitPagLFromBuffer(buffer, position);
        position += DflImpIntrRitPagL.Len.DFL_IMP_INTR_RIT_PAG_L;
        dflImpbBolloDettC.setDflImpbBolloDettCFromBuffer(buffer, position);
        position += DflImpbBolloDettC.Len.DFL_IMPB_BOLLO_DETT_C;
        dflImpbBolloDettD.setDflImpbBolloDettDFromBuffer(buffer, position);
        position += DflImpbBolloDettD.Len.DFL_IMPB_BOLLO_DETT_D;
        dflImpbBolloDettL.setDflImpbBolloDettLFromBuffer(buffer, position);
        position += DflImpbBolloDettL.Len.DFL_IMPB_BOLLO_DETT_L;
        dflImpstBolloDettC.setDflImpstBolloDettCFromBuffer(buffer, position);
        position += DflImpstBolloDettC.Len.DFL_IMPST_BOLLO_DETT_C;
        dflImpstBolloDettD.setDflImpstBolloDettDFromBuffer(buffer, position);
        position += DflImpstBolloDettD.Len.DFL_IMPST_BOLLO_DETT_D;
        dflImpstBolloDettL.setDflImpstBolloDettLFromBuffer(buffer, position);
        position += DflImpstBolloDettL.Len.DFL_IMPST_BOLLO_DETT_L;
        dflImpstBolloTotVc.setDflImpstBolloTotVcFromBuffer(buffer, position);
        position += DflImpstBolloTotVc.Len.DFL_IMPST_BOLLO_TOT_VC;
        dflImpstBolloTotVd.setDflImpstBolloTotVdFromBuffer(buffer, position);
        position += DflImpstBolloTotVd.Len.DFL_IMPST_BOLLO_TOT_VD;
        dflImpstBolloTotVl.setDflImpstBolloTotVlFromBuffer(buffer, position);
        position += DflImpstBolloTotVl.Len.DFL_IMPST_BOLLO_TOT_VL;
        dflImpbVis662014c.setDflImpbVis662014cFromBuffer(buffer, position);
        position += DflImpbVis662014c.Len.DFL_IMPB_VIS662014C;
        dflImpbVis662014d.setDflImpbVis662014dFromBuffer(buffer, position);
        position += DflImpbVis662014d.Len.DFL_IMPB_VIS662014D;
        dflImpbVis662014l.setDflImpbVis662014lFromBuffer(buffer, position);
        position += DflImpbVis662014l.Len.DFL_IMPB_VIS662014L;
        dflImpstVis662014c.setDflImpstVis662014cFromBuffer(buffer, position);
        position += DflImpstVis662014c.Len.DFL_IMPST_VIS662014C;
        dflImpstVis662014d.setDflImpstVis662014dFromBuffer(buffer, position);
        position += DflImpstVis662014d.Len.DFL_IMPST_VIS662014D;
        dflImpstVis662014l.setDflImpstVis662014lFromBuffer(buffer, position);
        position += DflImpstVis662014l.Len.DFL_IMPST_VIS662014L;
        dflImpbIs662014c.setDflImpbIs662014cFromBuffer(buffer, position);
        position += DflImpbIs662014c.Len.DFL_IMPB_IS662014C;
        dflImpbIs662014d.setDflImpbIs662014dFromBuffer(buffer, position);
        position += DflImpbIs662014d.Len.DFL_IMPB_IS662014D;
        dflImpbIs662014l.setDflImpbIs662014lFromBuffer(buffer, position);
        position += DflImpbIs662014l.Len.DFL_IMPB_IS662014L;
        dflIs662014c.setDflIs662014cFromBuffer(buffer, position);
        position += DflIs662014c.Len.DFL_IS662014C;
        dflIs662014d.setDflIs662014dFromBuffer(buffer, position);
        position += DflIs662014d.Len.DFL_IS662014D;
        dflIs662014l.setDflIs662014lFromBuffer(buffer, position);
    }

    public void setDflIdDForzLiq(int dflIdDForzLiq) {
        this.dflIdDForzLiq = dflIdDForzLiq;
    }

    public int getDflIdDForzLiq() {
        return this.dflIdDForzLiq;
    }

    public void setDflIdLiq(int dflIdLiq) {
        this.dflIdLiq = dflIdLiq;
    }

    public int getDflIdLiq() {
        return this.dflIdLiq;
    }

    public void setDflIdMoviCrz(int dflIdMoviCrz) {
        this.dflIdMoviCrz = dflIdMoviCrz;
    }

    public int getDflIdMoviCrz() {
        return this.dflIdMoviCrz;
    }

    public void setDflCodCompAnia(int dflCodCompAnia) {
        this.dflCodCompAnia = dflCodCompAnia;
    }

    public int getDflCodCompAnia() {
        return this.dflCodCompAnia;
    }

    public void setDflDtIniEff(int dflDtIniEff) {
        this.dflDtIniEff = dflDtIniEff;
    }

    public int getDflDtIniEff() {
        return this.dflDtIniEff;
    }

    public void setDflDtEndEff(int dflDtEndEff) {
        this.dflDtEndEff = dflDtEndEff;
    }

    public int getDflDtEndEff() {
        return this.dflDtEndEff;
    }

    public void setDflDsRiga(long dflDsRiga) {
        this.dflDsRiga = dflDsRiga;
    }

    public long getDflDsRiga() {
        return this.dflDsRiga;
    }

    public void setDflDsOperSql(char dflDsOperSql) {
        this.dflDsOperSql = dflDsOperSql;
    }

    public char getDflDsOperSql() {
        return this.dflDsOperSql;
    }

    public void setDflDsVer(int dflDsVer) {
        this.dflDsVer = dflDsVer;
    }

    public int getDflDsVer() {
        return this.dflDsVer;
    }

    public void setDflDsTsIniCptz(long dflDsTsIniCptz) {
        this.dflDsTsIniCptz = dflDsTsIniCptz;
    }

    public long getDflDsTsIniCptz() {
        return this.dflDsTsIniCptz;
    }

    public void setDflDsTsEndCptz(long dflDsTsEndCptz) {
        this.dflDsTsEndCptz = dflDsTsEndCptz;
    }

    public long getDflDsTsEndCptz() {
        return this.dflDsTsEndCptz;
    }

    public void setDflDsUtente(String dflDsUtente) {
        this.dflDsUtente = Functions.subString(dflDsUtente, Len.DFL_DS_UTENTE);
    }

    public String getDflDsUtente() {
        return this.dflDsUtente;
    }

    public void setDflDsStatoElab(char dflDsStatoElab) {
        this.dflDsStatoElab = dflDsStatoElab;
    }

    public char getDflDsStatoElab() {
        return this.dflDsStatoElab;
    }

    public DflAaCnbzDal2007Ef getDflAaCnbzDal2007Ef() {
        return dflAaCnbzDal2007Ef;
    }

    public DflAaCnbzEnd2000Ef getDflAaCnbzEnd2000Ef() {
        return dflAaCnbzEnd2000Ef;
    }

    public DflAaCnbzEnd2006Ef getDflAaCnbzEnd2006Ef() {
        return dflAaCnbzEnd2006Ef;
    }

    public DflAccpreAccCalc getDflAccpreAccCalc() {
        return dflAccpreAccCalc;
    }

    public DflAccpreAccDfz getDflAccpreAccDfz() {
        return dflAccpreAccDfz;
    }

    public DflAccpreAccEfflq getDflAccpreAccEfflq() {
        return dflAccpreAccEfflq;
    }

    public DflAccpreSostCalc getDflAccpreSostCalc() {
        return dflAccpreSostCalc;
    }

    public DflAccpreSostDfz getDflAccpreSostDfz() {
        return dflAccpreSostDfz;
    }

    public DflAccpreSostEfflq getDflAccpreSostEfflq() {
        return dflAccpreSostEfflq;
    }

    public DflAccpreVisCalc getDflAccpreVisCalc() {
        return dflAccpreVisCalc;
    }

    public DflAccpreVisDfz getDflAccpreVisDfz() {
        return dflAccpreVisDfz;
    }

    public DflAccpreVisEfflq getDflAccpreVisEfflq() {
        return dflAccpreVisEfflq;
    }

    public DflAlqCnbtInpstfmC getDflAlqCnbtInpstfmC() {
        return dflAlqCnbtInpstfmC;
    }

    public DflAlqCnbtInpstfmD getDflAlqCnbtInpstfmD() {
        return dflAlqCnbtInpstfmD;
    }

    public DflAlqCnbtInpstfmE getDflAlqCnbtInpstfmE() {
        return dflAlqCnbtInpstfmE;
    }

    public DflAlqTaxSepCalc getDflAlqTaxSepCalc() {
        return dflAlqTaxSepCalc;
    }

    public DflAlqTaxSepDfz getDflAlqTaxSepDfz() {
        return dflAlqTaxSepDfz;
    }

    public DflAlqTaxSepEfflq getDflAlqTaxSepEfflq() {
        return dflAlqTaxSepEfflq;
    }

    public DflCnbtInpstfmCalc getDflCnbtInpstfmCalc() {
        return dflCnbtInpstfmCalc;
    }

    public DflCnbtInpstfmDfz getDflCnbtInpstfmDfz() {
        return dflCnbtInpstfmDfz;
    }

    public DflCnbtInpstfmEfflq getDflCnbtInpstfmEfflq() {
        return dflCnbtInpstfmEfflq;
    }

    public DflCndeDal2007Calc getDflCndeDal2007Calc() {
        return dflCndeDal2007Calc;
    }

    public DflCndeDal2007Dfz getDflCndeDal2007Dfz() {
        return dflCndeDal2007Dfz;
    }

    public DflCndeDal2007Efflq getDflCndeDal2007Efflq() {
        return dflCndeDal2007Efflq;
    }

    public DflCndeEnd2000Calc getDflCndeEnd2000Calc() {
        return dflCndeEnd2000Calc;
    }

    public DflCndeEnd2000Dfz getDflCndeEnd2000Dfz() {
        return dflCndeEnd2000Dfz;
    }

    public DflCndeEnd2000Efflq getDflCndeEnd2000Efflq() {
        return dflCndeEnd2000Efflq;
    }

    public DflCndeEnd2006Calc getDflCndeEnd2006Calc() {
        return dflCndeEnd2006Calc;
    }

    public DflCndeEnd2006Dfz getDflCndeEnd2006Dfz() {
        return dflCndeEnd2006Dfz;
    }

    public DflCndeEnd2006Efflq getDflCndeEnd2006Efflq() {
        return dflCndeEnd2006Efflq;
    }

    public DflIcnbInpstfmCalc getDflIcnbInpstfmCalc() {
        return dflIcnbInpstfmCalc;
    }

    public DflIcnbInpstfmDfz getDflIcnbInpstfmDfz() {
        return dflIcnbInpstfmDfz;
    }

    public DflIcnbInpstfmEfflq getDflIcnbInpstfmEfflq() {
        return dflIcnbInpstfmEfflq;
    }

    public DflIdMoviChiu getDflIdMoviChiu() {
        return dflIdMoviChiu;
    }

    public DflIimpst252Calc getDflIimpst252Calc() {
        return dflIimpst252Calc;
    }

    public DflIimpst252Dfz getDflIimpst252Dfz() {
        return dflIimpst252Dfz;
    }

    public DflIimpst252Efflq getDflIimpst252Efflq() {
        return dflIimpst252Efflq;
    }

    public DflIimpstPrvrCalc getDflIimpstPrvrCalc() {
        return dflIimpstPrvrCalc;
    }

    public DflIimpstPrvrDfz getDflIimpstPrvrDfz() {
        return dflIimpstPrvrDfz;
    }

    public DflIimpstPrvrEfflq getDflIimpstPrvrEfflq() {
        return dflIimpstPrvrEfflq;
    }

    public DflIintPrestCalc getDflIintPrestCalc() {
        return dflIintPrestCalc;
    }

    public DflIintPrestDfz getDflIintPrestDfz() {
        return dflIintPrestDfz;
    }

    public DflIintPrestEfflq getDflIintPrestEfflq() {
        return dflIintPrestEfflq;
    }

    public DflImpExcontrEff getDflImpExcontrEff() {
        return dflImpExcontrEff;
    }

    public DflImpIntrRitPagC getDflImpIntrRitPagC() {
        return dflImpIntrRitPagC;
    }

    public DflImpIntrRitPagD getDflImpIntrRitPagD() {
        return dflImpIntrRitPagD;
    }

    public DflImpIntrRitPagL getDflImpIntrRitPagL() {
        return dflImpIntrRitPagL;
    }

    public DflImpLrdCalc getDflImpLrdCalc() {
        return dflImpLrdCalc;
    }

    public DflImpLrdDfz getDflImpLrdDfz() {
        return dflImpLrdDfz;
    }

    public DflImpLrdEfflq getDflImpLrdEfflq() {
        return dflImpLrdEfflq;
    }

    public DflImpNetCalc getDflImpNetCalc() {
        return dflImpNetCalc;
    }

    public DflImpNetDfz getDflImpNetDfz() {
        return dflImpNetDfz;
    }

    public DflImpNetEfflq getDflImpNetEfflq() {
        return dflImpNetEfflq;
    }

    public DflImpbBolloDettC getDflImpbBolloDettC() {
        return dflImpbBolloDettC;
    }

    public DflImpbBolloDettD getDflImpbBolloDettD() {
        return dflImpbBolloDettD;
    }

    public DflImpbBolloDettL getDflImpbBolloDettL() {
        return dflImpbBolloDettL;
    }

    public DflImpbIs1382011c getDflImpbIs1382011c() {
        return dflImpbIs1382011c;
    }

    public DflImpbIs1382011d getDflImpbIs1382011d() {
        return dflImpbIs1382011d;
    }

    public DflImpbIs1382011l getDflImpbIs1382011l() {
        return dflImpbIs1382011l;
    }

    public DflImpbIs662014c getDflImpbIs662014c() {
        return dflImpbIs662014c;
    }

    public DflImpbIs662014d getDflImpbIs662014d() {
        return dflImpbIs662014d;
    }

    public DflImpbIs662014l getDflImpbIs662014l() {
        return dflImpbIs662014l;
    }

    public DflImpbIsCalc getDflImpbIsCalc() {
        return dflImpbIsCalc;
    }

    public DflImpbIsDfz getDflImpbIsDfz() {
        return dflImpbIsDfz;
    }

    public DflImpbIsEfflq getDflImpbIsEfflq() {
        return dflImpbIsEfflq;
    }

    public DflImpbRitAccCalc getDflImpbRitAccCalc() {
        return dflImpbRitAccCalc;
    }

    public DflImpbRitAccDfz getDflImpbRitAccDfz() {
        return dflImpbRitAccDfz;
    }

    public DflImpbRitAccEfflq getDflImpbRitAccEfflq() {
        return dflImpbRitAccEfflq;
    }

    public DflImpbTaxSepCalc getDflImpbTaxSepCalc() {
        return dflImpbTaxSepCalc;
    }

    public DflImpbTaxSepDfz getDflImpbTaxSepDfz() {
        return dflImpbTaxSepDfz;
    }

    public DflImpbTaxSepEfflq getDflImpbTaxSepEfflq() {
        return dflImpbTaxSepEfflq;
    }

    public DflImpbTfrCalc getDflImpbTfrCalc() {
        return dflImpbTfrCalc;
    }

    public DflImpbTfrDfz getDflImpbTfrDfz() {
        return dflImpbTfrDfz;
    }

    public DflImpbTfrEfflq getDflImpbTfrEfflq() {
        return dflImpbTfrEfflq;
    }

    public DflImpbVis1382011c getDflImpbVis1382011c() {
        return dflImpbVis1382011c;
    }

    public DflImpbVis1382011d getDflImpbVis1382011d() {
        return dflImpbVis1382011d;
    }

    public DflImpbVis1382011l getDflImpbVis1382011l() {
        return dflImpbVis1382011l;
    }

    public DflImpbVis662014c getDflImpbVis662014c() {
        return dflImpbVis662014c;
    }

    public DflImpbVis662014d getDflImpbVis662014d() {
        return dflImpbVis662014d;
    }

    public DflImpbVis662014l getDflImpbVis662014l() {
        return dflImpbVis662014l;
    }

    public DflImpbVisCalc getDflImpbVisCalc() {
        return dflImpbVisCalc;
    }

    public DflImpbVisDfz getDflImpbVisDfz() {
        return dflImpbVisDfz;
    }

    public DflImpbVisEfflq getDflImpbVisEfflq() {
        return dflImpbVisEfflq;
    }

    public DflImpst252Calc getDflImpst252Calc() {
        return dflImpst252Calc;
    }

    public DflImpst252Efflq getDflImpst252Efflq() {
        return dflImpst252Efflq;
    }

    public DflImpstBolloDettC getDflImpstBolloDettC() {
        return dflImpstBolloDettC;
    }

    public DflImpstBolloDettD getDflImpstBolloDettD() {
        return dflImpstBolloDettD;
    }

    public DflImpstBolloDettL getDflImpstBolloDettL() {
        return dflImpstBolloDettL;
    }

    public DflImpstBolloTotVc getDflImpstBolloTotVc() {
        return dflImpstBolloTotVc;
    }

    public DflImpstBolloTotVd getDflImpstBolloTotVd() {
        return dflImpstBolloTotVd;
    }

    public DflImpstBolloTotVl getDflImpstBolloTotVl() {
        return dflImpstBolloTotVl;
    }

    public DflImpstDaRimbEff getDflImpstDaRimbEff() {
        return dflImpstDaRimbEff;
    }

    public DflImpstPrvrCalc getDflImpstPrvrCalc() {
        return dflImpstPrvrCalc;
    }

    public DflImpstPrvrDfz getDflImpstPrvrDfz() {
        return dflImpstPrvrDfz;
    }

    public DflImpstPrvrEfflq getDflImpstPrvrEfflq() {
        return dflImpstPrvrEfflq;
    }

    public DflImpstSostCalc getDflImpstSostCalc() {
        return dflImpstSostCalc;
    }

    public DflImpstSostDfz getDflImpstSostDfz() {
        return dflImpstSostDfz;
    }

    public DflImpstSostEfflq getDflImpstSostEfflq() {
        return dflImpstSostEfflq;
    }

    public DflImpstVis1382011c getDflImpstVis1382011c() {
        return dflImpstVis1382011c;
    }

    public DflImpstVis1382011d getDflImpstVis1382011d() {
        return dflImpstVis1382011d;
    }

    public DflImpstVis1382011l getDflImpstVis1382011l() {
        return dflImpstVis1382011l;
    }

    public DflImpstVis662014c getDflImpstVis662014c() {
        return dflImpstVis662014c;
    }

    public DflImpstVis662014d getDflImpstVis662014d() {
        return dflImpstVis662014d;
    }

    public DflImpstVis662014l getDflImpstVis662014l() {
        return dflImpstVis662014l;
    }

    public DflImpstVisCalc getDflImpstVisCalc() {
        return dflImpstVisCalc;
    }

    public DflImpstVisDfz getDflImpstVisDfz() {
        return dflImpstVisDfz;
    }

    public DflImpstVisEfflq getDflImpstVisEfflq() {
        return dflImpstVisEfflq;
    }

    public DflIntrPrestCalc getDflIntrPrestCalc() {
        return dflIntrPrestCalc;
    }

    public DflIntrPrestDfz getDflIntrPrestDfz() {
        return dflIntrPrestDfz;
    }

    public DflIntrPrestEfflq getDflIntrPrestEfflq() {
        return dflIntrPrestEfflq;
    }

    public DflIs1382011c getDflIs1382011c() {
        return dflIs1382011c;
    }

    public DflIs1382011d getDflIs1382011d() {
        return dflIs1382011d;
    }

    public DflIs1382011l getDflIs1382011l() {
        return dflIs1382011l;
    }

    public DflIs662014c getDflIs662014c() {
        return dflIs662014c;
    }

    public DflIs662014d getDflIs662014d() {
        return dflIs662014d;
    }

    public DflIs662014l getDflIs662014l() {
        return dflIs662014l;
    }

    public DflMmCnbzDal2007Ef getDflMmCnbzDal2007Ef() {
        return dflMmCnbzDal2007Ef;
    }

    public DflMmCnbzEnd2000Ef getDflMmCnbzEnd2000Ef() {
        return dflMmCnbzEnd2000Ef;
    }

    public DflMmCnbzEnd2006Ef getDflMmCnbzEnd2006Ef() {
        return dflMmCnbzEnd2006Ef;
    }

    public DflMontDal2007Calc getDflMontDal2007Calc() {
        return dflMontDal2007Calc;
    }

    public DflMontDal2007Dfz getDflMontDal2007Dfz() {
        return dflMontDal2007Dfz;
    }

    public DflMontDal2007Efflq getDflMontDal2007Efflq() {
        return dflMontDal2007Efflq;
    }

    public DflMontEnd2000Calc getDflMontEnd2000Calc() {
        return dflMontEnd2000Calc;
    }

    public DflMontEnd2000Dfz getDflMontEnd2000Dfz() {
        return dflMontEnd2000Dfz;
    }

    public DflMontEnd2000Efflq getDflMontEnd2000Efflq() {
        return dflMontEnd2000Efflq;
    }

    public DflMontEnd2006Calc getDflMontEnd2006Calc() {
        return dflMontEnd2006Calc;
    }

    public DflMontEnd2006Dfz getDflMontEnd2006Dfz() {
        return dflMontEnd2006Dfz;
    }

    public DflMontEnd2006Efflq getDflMontEnd2006Efflq() {
        return dflMontEnd2006Efflq;
    }

    public DflResPreAttCalc getDflResPreAttCalc() {
        return dflResPreAttCalc;
    }

    public DflResPreAttDfz getDflResPreAttDfz() {
        return dflResPreAttDfz;
    }

    public DflResPreAttEfflq getDflResPreAttEfflq() {
        return dflResPreAttEfflq;
    }

    public DflResPrstzCalc getDflResPrstzCalc() {
        return dflResPrstzCalc;
    }

    public DflResPrstzDfz getDflResPrstzDfz() {
        return dflResPrstzDfz;
    }

    public DflResPrstzEfflq getDflResPrstzEfflq() {
        return dflResPrstzEfflq;
    }

    public DflRitAccCalc getDflRitAccCalc() {
        return dflRitAccCalc;
    }

    public DflRitAccDfz getDflRitAccDfz() {
        return dflRitAccDfz;
    }

    public DflRitAccEfflq getDflRitAccEfflq() {
        return dflRitAccEfflq;
    }

    public DflRitIrpefCalc getDflRitIrpefCalc() {
        return dflRitIrpefCalc;
    }

    public DflRitIrpefDfz getDflRitIrpefDfz() {
        return dflRitIrpefDfz;
    }

    public DflRitIrpefEfflq getDflRitIrpefEfflq() {
        return dflRitIrpefEfflq;
    }

    public DflRitTfrCalc getDflRitTfrCalc() {
        return dflRitTfrCalc;
    }

    public DflRitTfrDfz getDflRitTfrDfz() {
        return dflRitTfrDfz;
    }

    public DflRitTfrEfflq getDflRitTfrEfflq() {
        return dflRitTfrEfflq;
    }

    public DflTaxSepCalc getDflTaxSepCalc() {
        return dflTaxSepCalc;
    }

    public DflTaxSepDfz getDflTaxSepDfz() {
        return dflTaxSepDfz;
    }

    public DflTaxSepEfflq getDflTaxSepEfflq() {
        return dflTaxSepEfflq;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_DS_UTENTE = 20;
        public static final int DFL_ID_D_FORZ_LIQ = 5;
        public static final int DFL_ID_LIQ = 5;
        public static final int DFL_ID_MOVI_CRZ = 5;
        public static final int DFL_COD_COMP_ANIA = 3;
        public static final int DFL_DT_INI_EFF = 5;
        public static final int DFL_DT_END_EFF = 5;
        public static final int DFL_DS_RIGA = 6;
        public static final int DFL_DS_OPER_SQL = 1;
        public static final int DFL_DS_VER = 5;
        public static final int DFL_DS_TS_INI_CPTZ = 10;
        public static final int DFL_DS_TS_END_CPTZ = 10;
        public static final int DFL_DS_STATO_ELAB = 1;
        public static final int D_FORZ_LIQ = DFL_ID_D_FORZ_LIQ + DFL_ID_LIQ + DFL_ID_MOVI_CRZ + DflIdMoviChiu.Len.DFL_ID_MOVI_CHIU + DFL_COD_COMP_ANIA + DFL_DT_INI_EFF + DFL_DT_END_EFF + DflImpLrdCalc.Len.DFL_IMP_LRD_CALC + DflImpLrdDfz.Len.DFL_IMP_LRD_DFZ + DflImpLrdEfflq.Len.DFL_IMP_LRD_EFFLQ + DflImpNetCalc.Len.DFL_IMP_NET_CALC + DflImpNetDfz.Len.DFL_IMP_NET_DFZ + DflImpNetEfflq.Len.DFL_IMP_NET_EFFLQ + DflImpstPrvrCalc.Len.DFL_IMPST_PRVR_CALC + DflImpstPrvrDfz.Len.DFL_IMPST_PRVR_DFZ + DflImpstPrvrEfflq.Len.DFL_IMPST_PRVR_EFFLQ + DflImpstVisCalc.Len.DFL_IMPST_VIS_CALC + DflImpstVisDfz.Len.DFL_IMPST_VIS_DFZ + DflImpstVisEfflq.Len.DFL_IMPST_VIS_EFFLQ + DflRitAccCalc.Len.DFL_RIT_ACC_CALC + DflRitAccDfz.Len.DFL_RIT_ACC_DFZ + DflRitAccEfflq.Len.DFL_RIT_ACC_EFFLQ + DflRitIrpefCalc.Len.DFL_RIT_IRPEF_CALC + DflRitIrpefDfz.Len.DFL_RIT_IRPEF_DFZ + DflRitIrpefEfflq.Len.DFL_RIT_IRPEF_EFFLQ + DflImpstSostCalc.Len.DFL_IMPST_SOST_CALC + DflImpstSostDfz.Len.DFL_IMPST_SOST_DFZ + DflImpstSostEfflq.Len.DFL_IMPST_SOST_EFFLQ + DflTaxSepCalc.Len.DFL_TAX_SEP_CALC + DflTaxSepDfz.Len.DFL_TAX_SEP_DFZ + DflTaxSepEfflq.Len.DFL_TAX_SEP_EFFLQ + DflIntrPrestCalc.Len.DFL_INTR_PREST_CALC + DflIntrPrestDfz.Len.DFL_INTR_PREST_DFZ + DflIntrPrestEfflq.Len.DFL_INTR_PREST_EFFLQ + DflAccpreSostCalc.Len.DFL_ACCPRE_SOST_CALC + DflAccpreSostDfz.Len.DFL_ACCPRE_SOST_DFZ + DflAccpreSostEfflq.Len.DFL_ACCPRE_SOST_EFFLQ + DflAccpreVisCalc.Len.DFL_ACCPRE_VIS_CALC + DflAccpreVisDfz.Len.DFL_ACCPRE_VIS_DFZ + DflAccpreVisEfflq.Len.DFL_ACCPRE_VIS_EFFLQ + DflAccpreAccCalc.Len.DFL_ACCPRE_ACC_CALC + DflAccpreAccDfz.Len.DFL_ACCPRE_ACC_DFZ + DflAccpreAccEfflq.Len.DFL_ACCPRE_ACC_EFFLQ + DflResPrstzCalc.Len.DFL_RES_PRSTZ_CALC + DflResPrstzDfz.Len.DFL_RES_PRSTZ_DFZ + DflResPrstzEfflq.Len.DFL_RES_PRSTZ_EFFLQ + DflResPreAttCalc.Len.DFL_RES_PRE_ATT_CALC + DflResPreAttDfz.Len.DFL_RES_PRE_ATT_DFZ + DflResPreAttEfflq.Len.DFL_RES_PRE_ATT_EFFLQ + DflImpExcontrEff.Len.DFL_IMP_EXCONTR_EFF + DflImpbVisCalc.Len.DFL_IMPB_VIS_CALC + DflImpbVisEfflq.Len.DFL_IMPB_VIS_EFFLQ + DflImpbVisDfz.Len.DFL_IMPB_VIS_DFZ + DflImpbRitAccCalc.Len.DFL_IMPB_RIT_ACC_CALC + DflImpbRitAccEfflq.Len.DFL_IMPB_RIT_ACC_EFFLQ + DflImpbRitAccDfz.Len.DFL_IMPB_RIT_ACC_DFZ + DflImpbTfrCalc.Len.DFL_IMPB_TFR_CALC + DflImpbTfrEfflq.Len.DFL_IMPB_TFR_EFFLQ + DflImpbTfrDfz.Len.DFL_IMPB_TFR_DFZ + DflImpbIsCalc.Len.DFL_IMPB_IS_CALC + DflImpbIsEfflq.Len.DFL_IMPB_IS_EFFLQ + DflImpbIsDfz.Len.DFL_IMPB_IS_DFZ + DflImpbTaxSepCalc.Len.DFL_IMPB_TAX_SEP_CALC + DflImpbTaxSepEfflq.Len.DFL_IMPB_TAX_SEP_EFFLQ + DflImpbTaxSepDfz.Len.DFL_IMPB_TAX_SEP_DFZ + DflIintPrestCalc.Len.DFL_IINT_PREST_CALC + DflIintPrestEfflq.Len.DFL_IINT_PREST_EFFLQ + DflIintPrestDfz.Len.DFL_IINT_PREST_DFZ + DflMontEnd2000Calc.Len.DFL_MONT_END2000_CALC + DflMontEnd2000Efflq.Len.DFL_MONT_END2000_EFFLQ + DflMontEnd2000Dfz.Len.DFL_MONT_END2000_DFZ + DflMontEnd2006Calc.Len.DFL_MONT_END2006_CALC + DflMontEnd2006Efflq.Len.DFL_MONT_END2006_EFFLQ + DflMontEnd2006Dfz.Len.DFL_MONT_END2006_DFZ + DflMontDal2007Calc.Len.DFL_MONT_DAL2007_CALC + DflMontDal2007Efflq.Len.DFL_MONT_DAL2007_EFFLQ + DflMontDal2007Dfz.Len.DFL_MONT_DAL2007_DFZ + DflIimpstPrvrCalc.Len.DFL_IIMPST_PRVR_CALC + DflIimpstPrvrEfflq.Len.DFL_IIMPST_PRVR_EFFLQ + DflIimpstPrvrDfz.Len.DFL_IIMPST_PRVR_DFZ + DflIimpst252Calc.Len.DFL_IIMPST252_CALC + DflIimpst252Efflq.Len.DFL_IIMPST252_EFFLQ + DflIimpst252Dfz.Len.DFL_IIMPST252_DFZ + DflImpst252Calc.Len.DFL_IMPST252_CALC + DflImpst252Efflq.Len.DFL_IMPST252_EFFLQ + DflRitTfrCalc.Len.DFL_RIT_TFR_CALC + DflRitTfrEfflq.Len.DFL_RIT_TFR_EFFLQ + DflRitTfrDfz.Len.DFL_RIT_TFR_DFZ + DflCnbtInpstfmCalc.Len.DFL_CNBT_INPSTFM_CALC + DflCnbtInpstfmEfflq.Len.DFL_CNBT_INPSTFM_EFFLQ + DflCnbtInpstfmDfz.Len.DFL_CNBT_INPSTFM_DFZ + DflIcnbInpstfmCalc.Len.DFL_ICNB_INPSTFM_CALC + DflIcnbInpstfmEfflq.Len.DFL_ICNB_INPSTFM_EFFLQ + DflIcnbInpstfmDfz.Len.DFL_ICNB_INPSTFM_DFZ + DflCndeEnd2000Calc.Len.DFL_CNDE_END2000_CALC + DflCndeEnd2000Efflq.Len.DFL_CNDE_END2000_EFFLQ + DflCndeEnd2000Dfz.Len.DFL_CNDE_END2000_DFZ + DflCndeEnd2006Calc.Len.DFL_CNDE_END2006_CALC + DflCndeEnd2006Efflq.Len.DFL_CNDE_END2006_EFFLQ + DflCndeEnd2006Dfz.Len.DFL_CNDE_END2006_DFZ + DflCndeDal2007Calc.Len.DFL_CNDE_DAL2007_CALC + DflCndeDal2007Efflq.Len.DFL_CNDE_DAL2007_EFFLQ + DflCndeDal2007Dfz.Len.DFL_CNDE_DAL2007_DFZ + DflAaCnbzEnd2000Ef.Len.DFL_AA_CNBZ_END2000_EF + DflAaCnbzEnd2006Ef.Len.DFL_AA_CNBZ_END2006_EF + DflAaCnbzDal2007Ef.Len.DFL_AA_CNBZ_DAL2007_EF + DflMmCnbzEnd2000Ef.Len.DFL_MM_CNBZ_END2000_EF + DflMmCnbzEnd2006Ef.Len.DFL_MM_CNBZ_END2006_EF + DflMmCnbzDal2007Ef.Len.DFL_MM_CNBZ_DAL2007_EF + DflImpstDaRimbEff.Len.DFL_IMPST_DA_RIMB_EFF + DflAlqTaxSepCalc.Len.DFL_ALQ_TAX_SEP_CALC + DflAlqTaxSepEfflq.Len.DFL_ALQ_TAX_SEP_EFFLQ + DflAlqTaxSepDfz.Len.DFL_ALQ_TAX_SEP_DFZ + DflAlqCnbtInpstfmC.Len.DFL_ALQ_CNBT_INPSTFM_C + DflAlqCnbtInpstfmE.Len.DFL_ALQ_CNBT_INPSTFM_E + DflAlqCnbtInpstfmD.Len.DFL_ALQ_CNBT_INPSTFM_D + DFL_DS_RIGA + DFL_DS_OPER_SQL + DFL_DS_VER + DFL_DS_TS_INI_CPTZ + DFL_DS_TS_END_CPTZ + DFL_DS_UTENTE + DFL_DS_STATO_ELAB + DflImpbVis1382011c.Len.DFL_IMPB_VIS1382011C + DflImpbVis1382011d.Len.DFL_IMPB_VIS1382011D + DflImpbVis1382011l.Len.DFL_IMPB_VIS1382011L + DflImpstVis1382011c.Len.DFL_IMPST_VIS1382011C + DflImpstVis1382011d.Len.DFL_IMPST_VIS1382011D + DflImpstVis1382011l.Len.DFL_IMPST_VIS1382011L + DflImpbIs1382011c.Len.DFL_IMPB_IS1382011C + DflImpbIs1382011d.Len.DFL_IMPB_IS1382011D + DflImpbIs1382011l.Len.DFL_IMPB_IS1382011L + DflIs1382011c.Len.DFL_IS1382011C + DflIs1382011d.Len.DFL_IS1382011D + DflIs1382011l.Len.DFL_IS1382011L + DflImpIntrRitPagC.Len.DFL_IMP_INTR_RIT_PAG_C + DflImpIntrRitPagD.Len.DFL_IMP_INTR_RIT_PAG_D + DflImpIntrRitPagL.Len.DFL_IMP_INTR_RIT_PAG_L + DflImpbBolloDettC.Len.DFL_IMPB_BOLLO_DETT_C + DflImpbBolloDettD.Len.DFL_IMPB_BOLLO_DETT_D + DflImpbBolloDettL.Len.DFL_IMPB_BOLLO_DETT_L + DflImpstBolloDettC.Len.DFL_IMPST_BOLLO_DETT_C + DflImpstBolloDettD.Len.DFL_IMPST_BOLLO_DETT_D + DflImpstBolloDettL.Len.DFL_IMPST_BOLLO_DETT_L + DflImpstBolloTotVc.Len.DFL_IMPST_BOLLO_TOT_VC + DflImpstBolloTotVd.Len.DFL_IMPST_BOLLO_TOT_VD + DflImpstBolloTotVl.Len.DFL_IMPST_BOLLO_TOT_VL + DflImpbVis662014c.Len.DFL_IMPB_VIS662014C + DflImpbVis662014d.Len.DFL_IMPB_VIS662014D + DflImpbVis662014l.Len.DFL_IMPB_VIS662014L + DflImpstVis662014c.Len.DFL_IMPST_VIS662014C + DflImpstVis662014d.Len.DFL_IMPST_VIS662014D + DflImpstVis662014l.Len.DFL_IMPST_VIS662014L + DflImpbIs662014c.Len.DFL_IMPB_IS662014C + DflImpbIs662014d.Len.DFL_IMPB_IS662014D + DflImpbIs662014l.Len.DFL_IMPB_IS662014L + DflIs662014c.Len.DFL_IS662014C + DflIs662014d.Len.DFL_IS662014D + DflIs662014l.Len.DFL_IS662014L;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ID_D_FORZ_LIQ = 9;
            public static final int DFL_ID_LIQ = 9;
            public static final int DFL_ID_MOVI_CRZ = 9;
            public static final int DFL_COD_COMP_ANIA = 5;
            public static final int DFL_DT_INI_EFF = 8;
            public static final int DFL_DT_END_EFF = 8;
            public static final int DFL_DS_RIGA = 10;
            public static final int DFL_DS_VER = 9;
            public static final int DFL_DS_TS_INI_CPTZ = 18;
            public static final int DFL_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
