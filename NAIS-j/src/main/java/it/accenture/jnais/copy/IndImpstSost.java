package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-IMPST-SOST<br>
 * Variable: IND-IMPST-SOST from copybook IDBVISO2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndImpstSost {

    //==== PROPERTIES ====
    //Original name: IND-ISO-ID-OGG
    private short idOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-DT-INI-PER
    private short dtIniPer = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-DT-END-PER
    private short dtEndPer = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-IMPST-SOST
    private short impstSost = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-IMPB-IS
    private short impbIs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-ALQ-IS
    private short alqIs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-COD-TRB
    private short codTrb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-PRSTZ-LRD-ANTE-IS
    private short prstzLrdAnteIs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-RIS-MAT-NET-PREC
    private short risMatNetPrec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-RIS-MAT-ANTE-TAX
    private short risMatAnteTax = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-RIS-MAT-POST-TAX
    private short risMatPostTax = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-PRSTZ-NET
    private short prstzNet = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-PRSTZ-PREC
    private short prstzPrec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ISO-CUM-PRE-VERS
    private short cumPreVers = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdOgg(short idOgg) {
        this.idOgg = idOgg;
    }

    public short getIdOgg() {
        return this.idOgg;
    }

    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtIniPer(short dtIniPer) {
        this.dtIniPer = dtIniPer;
    }

    public short getDtIniPer() {
        return this.dtIniPer;
    }

    public void setDtEndPer(short dtEndPer) {
        this.dtEndPer = dtEndPer;
    }

    public short getDtEndPer() {
        return this.dtEndPer;
    }

    public void setImpstSost(short impstSost) {
        this.impstSost = impstSost;
    }

    public short getImpstSost() {
        return this.impstSost;
    }

    public void setImpbIs(short impbIs) {
        this.impbIs = impbIs;
    }

    public short getImpbIs() {
        return this.impbIs;
    }

    public void setAlqIs(short alqIs) {
        this.alqIs = alqIs;
    }

    public short getAlqIs() {
        return this.alqIs;
    }

    public void setCodTrb(short codTrb) {
        this.codTrb = codTrb;
    }

    public short getCodTrb() {
        return this.codTrb;
    }

    public void setPrstzLrdAnteIs(short prstzLrdAnteIs) {
        this.prstzLrdAnteIs = prstzLrdAnteIs;
    }

    public short getPrstzLrdAnteIs() {
        return this.prstzLrdAnteIs;
    }

    public void setRisMatNetPrec(short risMatNetPrec) {
        this.risMatNetPrec = risMatNetPrec;
    }

    public short getRisMatNetPrec() {
        return this.risMatNetPrec;
    }

    public void setRisMatAnteTax(short risMatAnteTax) {
        this.risMatAnteTax = risMatAnteTax;
    }

    public short getRisMatAnteTax() {
        return this.risMatAnteTax;
    }

    public void setRisMatPostTax(short risMatPostTax) {
        this.risMatPostTax = risMatPostTax;
    }

    public short getRisMatPostTax() {
        return this.risMatPostTax;
    }

    public void setPrstzNet(short prstzNet) {
        this.prstzNet = prstzNet;
    }

    public short getPrstzNet() {
        return this.prstzNet;
    }

    public void setPrstzPrec(short prstzPrec) {
        this.prstzPrec = prstzPrec;
    }

    public short getPrstzPrec() {
        return this.prstzPrec;
    }

    public void setCumPreVers(short cumPreVers) {
        this.cumPreVers = cumPreVers;
    }

    public short getCumPreVers() {
        return this.cumPreVers;
    }
}
