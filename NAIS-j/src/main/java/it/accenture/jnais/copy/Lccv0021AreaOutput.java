package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.Lccv0021ReturnCode;

/**Original name: LCCV0021-AREA-OUTPUT<br>
 * Variable: LCCV0021-AREA-OUTPUT from copybook LCCV0021<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccv0021AreaOutput {

    //==== PROPERTIES ====
    //Original name: LCCV0021-TP-MOV-ACT
    private String tpMovAct = DefaultValues.stringVal(Len.TP_MOV_ACT);
    //Original name: LCCV0021-COD-PROCESSO-WF
    private String codProcessoWf = DefaultValues.stringVal(Len.COD_PROCESSO_WF);
    /**Original name: LCCV0021-RETURN-CODE<br>
	 * <pre>-- return code</pre>*/
    private Lccv0021ReturnCode returnCode = new Lccv0021ReturnCode();
    /**Original name: LCCV0021-SQLCODE<br>
	 * <pre>-- sqlcode</pre>*/
    private Idso0011SqlcodeSigned sqlcode = new Idso0011SqlcodeSigned();
    //Original name: LCCV0021-DESCRIZ-ERR
    private String descrizErr = DefaultValues.stringVal(Len.DESCRIZ_ERR);
    //Original name: LCCV0021-COD-SERVIZIO-BE
    private String codServizioBe = DefaultValues.stringVal(Len.COD_SERVIZIO_BE);
    //Original name: LCCV0021-NOME-TABELLA
    private String nomeTabella = DefaultValues.stringVal(Len.NOME_TABELLA);
    //Original name: LCCV0021-KEY-TABELLA
    private String keyTabella = DefaultValues.stringVal(Len.KEY_TABELLA);

    //==== METHODS ====
    public void setAreaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        tpMovAct = MarshalByte.readFixedString(buffer, position, Len.TP_MOV_ACT);
        position += Len.TP_MOV_ACT;
        codProcessoWf = MarshalByte.readString(buffer, position, Len.COD_PROCESSO_WF);
        position += Len.COD_PROCESSO_WF;
        returnCode.setReturnCode(MarshalByte.readString(buffer, position, Lccv0021ReturnCode.Len.RETURN_CODE));
        position += Lccv0021ReturnCode.Len.RETURN_CODE;
        sqlcode.setSqlcodeSigned(MarshalByte.readInt(buffer, position, Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED));
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        setCampiEsitoBytes(buffer, position);
    }

    public byte[] getAreaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tpMovAct, Len.TP_MOV_ACT);
        position += Len.TP_MOV_ACT;
        MarshalByte.writeString(buffer, position, codProcessoWf, Len.COD_PROCESSO_WF);
        position += Len.COD_PROCESSO_WF;
        MarshalByte.writeString(buffer, position, returnCode.getReturnCode(), Lccv0021ReturnCode.Len.RETURN_CODE);
        position += Lccv0021ReturnCode.Len.RETURN_CODE;
        MarshalByte.writeInt(buffer, position, sqlcode.getSqlcodeSigned(), Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED);
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        getCampiEsitoBytes(buffer, position);
        return buffer;
    }

    public void setTpMovActFormatted(String tpMovAct) {
        this.tpMovAct = Trunc.toUnsignedNumeric(tpMovAct, Len.TP_MOV_ACT);
    }

    public int getTpMovAct() {
        return NumericDisplay.asInt(this.tpMovAct);
    }

    public String getLccv0021TpMovActFormatted() {
        return this.tpMovAct;
    }

    public void setCodProcessoWf(String codProcessoWf) {
        this.codProcessoWf = Functions.subString(codProcessoWf, Len.COD_PROCESSO_WF);
    }

    public String getCodProcessoWf() {
        return this.codProcessoWf;
    }

    public void setCampiEsitoBytes(byte[] buffer) {
        setCampiEsitoBytes(buffer, 1);
    }

    /**Original name: LCCV0021-CAMPI-ESITO<br>
	 * <pre>--campi esito</pre>*/
    public byte[] getLccv0021CampiEsitoBytes() {
        byte[] buffer = new byte[Len.CAMPI_ESITO];
        return getCampiEsitoBytes(buffer, 1);
    }

    public void setCampiEsitoBytes(byte[] buffer, int offset) {
        int position = offset;
        descrizErr = MarshalByte.readString(buffer, position, Len.DESCRIZ_ERR);
        position += Len.DESCRIZ_ERR;
        codServizioBe = MarshalByte.readString(buffer, position, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        nomeTabella = MarshalByte.readString(buffer, position, Len.NOME_TABELLA);
        position += Len.NOME_TABELLA;
        keyTabella = MarshalByte.readString(buffer, position, Len.KEY_TABELLA);
    }

    public byte[] getCampiEsitoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, descrizErr, Len.DESCRIZ_ERR);
        position += Len.DESCRIZ_ERR;
        MarshalByte.writeString(buffer, position, codServizioBe, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        MarshalByte.writeString(buffer, position, nomeTabella, Len.NOME_TABELLA);
        position += Len.NOME_TABELLA;
        MarshalByte.writeString(buffer, position, keyTabella, Len.KEY_TABELLA);
        return buffer;
    }

    public void setDescrizErr(String descrizErr) {
        this.descrizErr = Functions.subString(descrizErr, Len.DESCRIZ_ERR);
    }

    public String getDescrizErr() {
        return this.descrizErr;
    }

    public void setCodServizioBe(String codServizioBe) {
        this.codServizioBe = Functions.subString(codServizioBe, Len.COD_SERVIZIO_BE);
    }

    public String getCodServizioBe() {
        return this.codServizioBe;
    }

    public void setNomeTabella(String nomeTabella) {
        this.nomeTabella = Functions.subString(nomeTabella, Len.NOME_TABELLA);
    }

    public String getNomeTabella() {
        return this.nomeTabella;
    }

    public void setKeyTabella(String keyTabella) {
        this.keyTabella = Functions.subString(keyTabella, Len.KEY_TABELLA);
    }

    public String getKeyTabella() {
        return this.keyTabella;
    }

    public Lccv0021ReturnCode getReturnCode() {
        return returnCode;
    }

    public Idso0011SqlcodeSigned getSqlcode() {
        return sqlcode;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_MOV_ACT = 5;
        public static final int COD_PROCESSO_WF = 3;
        public static final int DESCRIZ_ERR = 300;
        public static final int COD_SERVIZIO_BE = 8;
        public static final int NOME_TABELLA = 18;
        public static final int KEY_TABELLA = 20;
        public static final int CAMPI_ESITO = DESCRIZ_ERR + COD_SERVIZIO_BE + NOME_TABELLA + KEY_TABELLA;
        public static final int AREA_OUTPUT = TP_MOV_ACT + COD_PROCESSO_WF + Lccv0021ReturnCode.Len.RETURN_CODE + Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED + CAMPI_ESITO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
