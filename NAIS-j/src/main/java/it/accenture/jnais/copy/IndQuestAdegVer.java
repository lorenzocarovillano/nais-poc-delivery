package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-QUEST-ADEG-VER<br>
 * Variable: IND-QUEST-ADEG-VER from copybook IDBVP562<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndQuestAdegVer {

    //==== PROPERTIES ====
    //Original name: IND-P56-ID-RAPP-ANA
    private short idRappAna = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-NATURA-OPRZ
    private short naturaOprz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-ORGN-FND
    private short orgnFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-QLFC-PROF
    private short codQlfcProf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-NAZ-QLFC-PROF
    private short codNazQlfcProf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-PRV-QLFC-PROF
    private short codPrvQlfcProf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FNT-REDD
    private short fntRedd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-REDD-FATT-ANNU
    private short reddFattAnnu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-ATECO
    private short codAteco = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-VALUT-COLL
    private short valutColl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-LUOGO-COSTITUZIONE
    private short luogoCostituzione = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-MOVI
    private short tpMovi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-RAG-RAPP
    private short flRagRapp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PRSZ-TIT-EFF
    private short flPrszTitEff = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-MOT-RISC
    private short tpMotRisc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-PNT-VND
    private short tpPntVnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-ADEG-VER
    private short tpAdegVer = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-RELA-ESEC
    private short tpRelaEsec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-SCO-FIN-RAPP
    private short tpScoFinRapp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PRSZ-3O-PAGAT
    private short flPrsz3oPagat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-AREA-GEO-PROV-FND
    private short areaGeoProvFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-DEST-FND
    private short tpDestFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PAESE-RESID-AUT
    private short flPaeseResidAut = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PAESE-CIT-AUT
    private short flPaeseCitAut = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PAESE-NAZ-AUT
    private short flPaeseNazAut = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-PROF-PREC
    private short codProfPrec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-AUT-PEP
    private short flAutPep = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-IMP-CAR-PUB
    private short flImpCarPub = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-LIS-TERR-SORV
    private short flLisTerrSorv = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-SIT-FIN-PAT
    private short tpSitFinPat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-SIT-FIN-PAT-CON
    private short tpSitFinPatCon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-IMP-TOT-AFF-UTIL
    private short impTotAffUtil = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-IMP-TOT-FIN-UTIL
    private short impTotFinUtil = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-IMP-TOT-AFF-ACC
    private short impTotAffAcc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-IMP-TOT-FIN-ACC
    private short impTotFinAcc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-FRM-GIUR-SAV
    private short tpFrmGiurSav = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-REG-COLL-POLI
    private short regCollPoli = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-NUM-TEL
    private short numTel = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-NUM-DIP
    private short numDip = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-SIT-FAM-CONV
    private short tpSitFamConv = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-PROF-CON
    private short codProfCon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-ES-PROC-PEN
    private short flEsProcPen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-COND-CLIENTE
    private short tpCondCliente = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-SAE
    private short codSae = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-OPER-ESTERO
    private short tpOperEstero = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-STAT-OPER-ESTERO
    private short statOperEstero = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-PRV-SVOL-ATT
    private short codPrvSvolAtt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-STAT-SVOL-ATT
    private short codStatSvolAtt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-SOC
    private short tpSoc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-IND-SOC-QUOT
    private short flIndSocQuot = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-SIT-GIUR
    private short tpSitGiur = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-PC-QUO-DET-TIT-EFF
    private short pcQuoDetTitEff = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-PRFL-RSH-PEP
    private short tpPrflRshPep = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-PEP
    private short tpPep = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-NOT-PREG
    private short flNotPreg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-DT-INI-FNT-REDD
    private short dtIniFntRedd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FNT-REDD-2
    private short fntRedd2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-DT-INI-FNT-REDD-2
    private short dtIniFntRedd2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FNT-REDD-3
    private short fntRedd3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-DT-INI-FNT-REDD-3
    private short dtIniFntRedd3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-MOT-ASS-TIT-EFF
    private short motAssTitEff = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FIN-COSTITUZIONE
    private short finCostituzione = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-DESC-IMP-CAR-PUB
    private short descImpCarPub = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-DESC-SCO-FIN-RAPP
    private short descScoFinRapp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-DESC-PROC-PNL
    private short descProcPnl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-DESC-NOT-PREG
    private short descNotPreg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-ID-ASSICURATI
    private short idAssicurati = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-REDD-CON
    private short reddCon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-DESC-LIB-MOT-RISC
    private short descLibMotRisc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-MOT-ASS-TIT-EFF
    private short tpMotAssTitEff = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-RAG-RAPP
    private short tpRagRapp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-CAN
    private short codCan = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-FIN-COST
    private short tpFinCost = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-NAZ-DEST-FND
    private short nazDestFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-AU-FATCA-AEOI
    private short flAuFatcaAeoi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-CAR-FIN-GIUR
    private short tpCarFinGiur = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-CAR-FIN-GIUR-AT
    private short tpCarFinGiurAt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-CAR-FIN-GIUR-PA
    private short tpCarFinGiurPa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-ISTITUZ-FIN
    private short flIstituzFin = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-ORI-FND-TIT-EFF
    private short tpOriFndTitEff = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-PC-ESP-AG-PA-MSC
    private short pcEspAgPaMsc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PR-TR-USA
    private short flPrTrUsa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PR-TR-NO-USA
    private short flPrTrNoUsa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-PC-RIP-PAT-AS-VITA
    private short pcRipPatAsVita = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-PC-RIP-PAT-IM
    private short pcRipPatIm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-PC-RIP-PAT-SET-IM
    private short pcRipPatSetIm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-STATUS-AEOI
    private short tpStatusAeoi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-STATUS-FATCA
    private short tpStatusFatca = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-RAPP-PA-MSC
    private short flRappPaMsc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-COMUN-SVOL-ATT
    private short codComunSvolAtt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-DT-1O-CON-CLI
    private short tpDt1oConCli = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-MOD-EN-RELA-INT
    private short tpModEnRelaInt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-REDD-ANNU-LRD
    private short tpReddAnnuLrd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-REDD-CON
    private short tpReddCon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-OPER-SOC-FID
    private short tpOperSocFid = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-PA-ESP-MSC-1
    private short codPaEspMsc1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-IMP-PA-ESP-MSC-1
    private short impPaEspMsc1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-PA-ESP-MSC-2
    private short codPaEspMsc2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-IMP-PA-ESP-MSC-2
    private short impPaEspMsc2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-PA-ESP-MSC-3
    private short codPaEspMsc3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-IMP-PA-ESP-MSC-3
    private short impPaEspMsc3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-PA-ESP-MSC-4
    private short codPaEspMsc4 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-IMP-PA-ESP-MSC-4
    private short impPaEspMsc4 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-PA-ESP-MSC-5
    private short codPaEspMsc5 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-IMP-PA-ESP-MSC-5
    private short impPaEspMsc5 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-DESC-ORGN-FND
    private short descOrgnFnd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-AUT-DUE-DIL
    private short codAutDueDil = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PR-QUEST-FATCA
    private short flPrQuestFatca = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PR-QUEST-AEOI
    private short flPrQuestAeoi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PR-QUEST-OFAC
    private short flPrQuestOfac = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PR-QUEST-KYC
    private short flPrQuestKyc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-PR-QUEST-MSCQ
    private short flPrQuestMscq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-NOT-PREG
    private short tpNotPreg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-PROC-PNL
    private short tpProcPnl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-IMP-CAR-PUB
    private short codImpCarPub = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-OPRZ-SOSPETTE
    private short oprzSospette = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-ULT-FATT-ANNU
    private short ultFattAnnu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-DESC-PEP
    private short descPep = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-NUM-TEL-2
    private short numTel2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-IMP-AFI
    private short impAfi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-FL-NEW-PRO
    private short flNewPro = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-TP-MOT-CAMBIO-CNTR
    private short tpMotCambioCntr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-DESC-MOT-CAMBIO-CN
    private short descMotCambioCn = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-P56-COD-SOGG
    private short codSogg = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdRappAna(short idRappAna) {
        this.idRappAna = idRappAna;
    }

    public short getIdRappAna() {
        return this.idRappAna;
    }

    public void setNaturaOprz(short naturaOprz) {
        this.naturaOprz = naturaOprz;
    }

    public short getNaturaOprz() {
        return this.naturaOprz;
    }

    public void setOrgnFnd(short orgnFnd) {
        this.orgnFnd = orgnFnd;
    }

    public short getOrgnFnd() {
        return this.orgnFnd;
    }

    public void setCodQlfcProf(short codQlfcProf) {
        this.codQlfcProf = codQlfcProf;
    }

    public short getCodQlfcProf() {
        return this.codQlfcProf;
    }

    public void setCodNazQlfcProf(short codNazQlfcProf) {
        this.codNazQlfcProf = codNazQlfcProf;
    }

    public short getCodNazQlfcProf() {
        return this.codNazQlfcProf;
    }

    public void setCodPrvQlfcProf(short codPrvQlfcProf) {
        this.codPrvQlfcProf = codPrvQlfcProf;
    }

    public short getCodPrvQlfcProf() {
        return this.codPrvQlfcProf;
    }

    public void setFntRedd(short fntRedd) {
        this.fntRedd = fntRedd;
    }

    public short getFntRedd() {
        return this.fntRedd;
    }

    public void setReddFattAnnu(short reddFattAnnu) {
        this.reddFattAnnu = reddFattAnnu;
    }

    public short getReddFattAnnu() {
        return this.reddFattAnnu;
    }

    public void setCodAteco(short codAteco) {
        this.codAteco = codAteco;
    }

    public short getCodAteco() {
        return this.codAteco;
    }

    public void setValutColl(short valutColl) {
        this.valutColl = valutColl;
    }

    public short getValutColl() {
        return this.valutColl;
    }

    public void setLuogoCostituzione(short luogoCostituzione) {
        this.luogoCostituzione = luogoCostituzione;
    }

    public short getLuogoCostituzione() {
        return this.luogoCostituzione;
    }

    public void setTpMovi(short tpMovi) {
        this.tpMovi = tpMovi;
    }

    public short getTpMovi() {
        return this.tpMovi;
    }

    public void setFlRagRapp(short flRagRapp) {
        this.flRagRapp = flRagRapp;
    }

    public short getFlRagRapp() {
        return this.flRagRapp;
    }

    public void setFlPrszTitEff(short flPrszTitEff) {
        this.flPrszTitEff = flPrszTitEff;
    }

    public short getFlPrszTitEff() {
        return this.flPrszTitEff;
    }

    public void setTpMotRisc(short tpMotRisc) {
        this.tpMotRisc = tpMotRisc;
    }

    public short getTpMotRisc() {
        return this.tpMotRisc;
    }

    public void setTpPntVnd(short tpPntVnd) {
        this.tpPntVnd = tpPntVnd;
    }

    public short getTpPntVnd() {
        return this.tpPntVnd;
    }

    public void setTpAdegVer(short tpAdegVer) {
        this.tpAdegVer = tpAdegVer;
    }

    public short getTpAdegVer() {
        return this.tpAdegVer;
    }

    public void setTpRelaEsec(short tpRelaEsec) {
        this.tpRelaEsec = tpRelaEsec;
    }

    public short getTpRelaEsec() {
        return this.tpRelaEsec;
    }

    public void setTpScoFinRapp(short tpScoFinRapp) {
        this.tpScoFinRapp = tpScoFinRapp;
    }

    public short getTpScoFinRapp() {
        return this.tpScoFinRapp;
    }

    public void setFlPrsz3oPagat(short flPrsz3oPagat) {
        this.flPrsz3oPagat = flPrsz3oPagat;
    }

    public short getFlPrsz3oPagat() {
        return this.flPrsz3oPagat;
    }

    public void setAreaGeoProvFnd(short areaGeoProvFnd) {
        this.areaGeoProvFnd = areaGeoProvFnd;
    }

    public short getAreaGeoProvFnd() {
        return this.areaGeoProvFnd;
    }

    public void setTpDestFnd(short tpDestFnd) {
        this.tpDestFnd = tpDestFnd;
    }

    public short getTpDestFnd() {
        return this.tpDestFnd;
    }

    public void setFlPaeseResidAut(short flPaeseResidAut) {
        this.flPaeseResidAut = flPaeseResidAut;
    }

    public short getFlPaeseResidAut() {
        return this.flPaeseResidAut;
    }

    public void setFlPaeseCitAut(short flPaeseCitAut) {
        this.flPaeseCitAut = flPaeseCitAut;
    }

    public short getFlPaeseCitAut() {
        return this.flPaeseCitAut;
    }

    public void setFlPaeseNazAut(short flPaeseNazAut) {
        this.flPaeseNazAut = flPaeseNazAut;
    }

    public short getFlPaeseNazAut() {
        return this.flPaeseNazAut;
    }

    public void setCodProfPrec(short codProfPrec) {
        this.codProfPrec = codProfPrec;
    }

    public short getCodProfPrec() {
        return this.codProfPrec;
    }

    public void setFlAutPep(short flAutPep) {
        this.flAutPep = flAutPep;
    }

    public short getFlAutPep() {
        return this.flAutPep;
    }

    public void setFlImpCarPub(short flImpCarPub) {
        this.flImpCarPub = flImpCarPub;
    }

    public short getFlImpCarPub() {
        return this.flImpCarPub;
    }

    public void setFlLisTerrSorv(short flLisTerrSorv) {
        this.flLisTerrSorv = flLisTerrSorv;
    }

    public short getFlLisTerrSorv() {
        return this.flLisTerrSorv;
    }

    public void setTpSitFinPat(short tpSitFinPat) {
        this.tpSitFinPat = tpSitFinPat;
    }

    public short getTpSitFinPat() {
        return this.tpSitFinPat;
    }

    public void setTpSitFinPatCon(short tpSitFinPatCon) {
        this.tpSitFinPatCon = tpSitFinPatCon;
    }

    public short getTpSitFinPatCon() {
        return this.tpSitFinPatCon;
    }

    public void setImpTotAffUtil(short impTotAffUtil) {
        this.impTotAffUtil = impTotAffUtil;
    }

    public short getImpTotAffUtil() {
        return this.impTotAffUtil;
    }

    public void setImpTotFinUtil(short impTotFinUtil) {
        this.impTotFinUtil = impTotFinUtil;
    }

    public short getImpTotFinUtil() {
        return this.impTotFinUtil;
    }

    public void setImpTotAffAcc(short impTotAffAcc) {
        this.impTotAffAcc = impTotAffAcc;
    }

    public short getImpTotAffAcc() {
        return this.impTotAffAcc;
    }

    public void setImpTotFinAcc(short impTotFinAcc) {
        this.impTotFinAcc = impTotFinAcc;
    }

    public short getImpTotFinAcc() {
        return this.impTotFinAcc;
    }

    public void setTpFrmGiurSav(short tpFrmGiurSav) {
        this.tpFrmGiurSav = tpFrmGiurSav;
    }

    public short getTpFrmGiurSav() {
        return this.tpFrmGiurSav;
    }

    public void setRegCollPoli(short regCollPoli) {
        this.regCollPoli = regCollPoli;
    }

    public short getRegCollPoli() {
        return this.regCollPoli;
    }

    public void setNumTel(short numTel) {
        this.numTel = numTel;
    }

    public short getNumTel() {
        return this.numTel;
    }

    public void setNumDip(short numDip) {
        this.numDip = numDip;
    }

    public short getNumDip() {
        return this.numDip;
    }

    public void setTpSitFamConv(short tpSitFamConv) {
        this.tpSitFamConv = tpSitFamConv;
    }

    public short getTpSitFamConv() {
        return this.tpSitFamConv;
    }

    public void setCodProfCon(short codProfCon) {
        this.codProfCon = codProfCon;
    }

    public short getCodProfCon() {
        return this.codProfCon;
    }

    public void setFlEsProcPen(short flEsProcPen) {
        this.flEsProcPen = flEsProcPen;
    }

    public short getFlEsProcPen() {
        return this.flEsProcPen;
    }

    public void setTpCondCliente(short tpCondCliente) {
        this.tpCondCliente = tpCondCliente;
    }

    public short getTpCondCliente() {
        return this.tpCondCliente;
    }

    public void setCodSae(short codSae) {
        this.codSae = codSae;
    }

    public short getCodSae() {
        return this.codSae;
    }

    public void setTpOperEstero(short tpOperEstero) {
        this.tpOperEstero = tpOperEstero;
    }

    public short getTpOperEstero() {
        return this.tpOperEstero;
    }

    public void setStatOperEstero(short statOperEstero) {
        this.statOperEstero = statOperEstero;
    }

    public short getStatOperEstero() {
        return this.statOperEstero;
    }

    public void setCodPrvSvolAtt(short codPrvSvolAtt) {
        this.codPrvSvolAtt = codPrvSvolAtt;
    }

    public short getCodPrvSvolAtt() {
        return this.codPrvSvolAtt;
    }

    public void setCodStatSvolAtt(short codStatSvolAtt) {
        this.codStatSvolAtt = codStatSvolAtt;
    }

    public short getCodStatSvolAtt() {
        return this.codStatSvolAtt;
    }

    public void setTpSoc(short tpSoc) {
        this.tpSoc = tpSoc;
    }

    public short getTpSoc() {
        return this.tpSoc;
    }

    public void setFlIndSocQuot(short flIndSocQuot) {
        this.flIndSocQuot = flIndSocQuot;
    }

    public short getFlIndSocQuot() {
        return this.flIndSocQuot;
    }

    public void setTpSitGiur(short tpSitGiur) {
        this.tpSitGiur = tpSitGiur;
    }

    public short getTpSitGiur() {
        return this.tpSitGiur;
    }

    public void setPcQuoDetTitEff(short pcQuoDetTitEff) {
        this.pcQuoDetTitEff = pcQuoDetTitEff;
    }

    public short getPcQuoDetTitEff() {
        return this.pcQuoDetTitEff;
    }

    public void setTpPrflRshPep(short tpPrflRshPep) {
        this.tpPrflRshPep = tpPrflRshPep;
    }

    public short getTpPrflRshPep() {
        return this.tpPrflRshPep;
    }

    public void setTpPep(short tpPep) {
        this.tpPep = tpPep;
    }

    public short getTpPep() {
        return this.tpPep;
    }

    public void setFlNotPreg(short flNotPreg) {
        this.flNotPreg = flNotPreg;
    }

    public short getFlNotPreg() {
        return this.flNotPreg;
    }

    public void setDtIniFntRedd(short dtIniFntRedd) {
        this.dtIniFntRedd = dtIniFntRedd;
    }

    public short getDtIniFntRedd() {
        return this.dtIniFntRedd;
    }

    public void setFntRedd2(short fntRedd2) {
        this.fntRedd2 = fntRedd2;
    }

    public short getFntRedd2() {
        return this.fntRedd2;
    }

    public void setDtIniFntRedd2(short dtIniFntRedd2) {
        this.dtIniFntRedd2 = dtIniFntRedd2;
    }

    public short getDtIniFntRedd2() {
        return this.dtIniFntRedd2;
    }

    public void setFntRedd3(short fntRedd3) {
        this.fntRedd3 = fntRedd3;
    }

    public short getFntRedd3() {
        return this.fntRedd3;
    }

    public void setDtIniFntRedd3(short dtIniFntRedd3) {
        this.dtIniFntRedd3 = dtIniFntRedd3;
    }

    public short getDtIniFntRedd3() {
        return this.dtIniFntRedd3;
    }

    public void setMotAssTitEff(short motAssTitEff) {
        this.motAssTitEff = motAssTitEff;
    }

    public short getMotAssTitEff() {
        return this.motAssTitEff;
    }

    public void setFinCostituzione(short finCostituzione) {
        this.finCostituzione = finCostituzione;
    }

    public short getFinCostituzione() {
        return this.finCostituzione;
    }

    public void setDescImpCarPub(short descImpCarPub) {
        this.descImpCarPub = descImpCarPub;
    }

    public short getDescImpCarPub() {
        return this.descImpCarPub;
    }

    public void setDescScoFinRapp(short descScoFinRapp) {
        this.descScoFinRapp = descScoFinRapp;
    }

    public short getDescScoFinRapp() {
        return this.descScoFinRapp;
    }

    public void setDescProcPnl(short descProcPnl) {
        this.descProcPnl = descProcPnl;
    }

    public short getDescProcPnl() {
        return this.descProcPnl;
    }

    public void setDescNotPreg(short descNotPreg) {
        this.descNotPreg = descNotPreg;
    }

    public short getDescNotPreg() {
        return this.descNotPreg;
    }

    public void setIdAssicurati(short idAssicurati) {
        this.idAssicurati = idAssicurati;
    }

    public short getIdAssicurati() {
        return this.idAssicurati;
    }

    public void setReddCon(short reddCon) {
        this.reddCon = reddCon;
    }

    public short getReddCon() {
        return this.reddCon;
    }

    public void setDescLibMotRisc(short descLibMotRisc) {
        this.descLibMotRisc = descLibMotRisc;
    }

    public short getDescLibMotRisc() {
        return this.descLibMotRisc;
    }

    public void setTpMotAssTitEff(short tpMotAssTitEff) {
        this.tpMotAssTitEff = tpMotAssTitEff;
    }

    public short getTpMotAssTitEff() {
        return this.tpMotAssTitEff;
    }

    public void setTpRagRapp(short tpRagRapp) {
        this.tpRagRapp = tpRagRapp;
    }

    public short getTpRagRapp() {
        return this.tpRagRapp;
    }

    public void setCodCan(short codCan) {
        this.codCan = codCan;
    }

    public short getCodCan() {
        return this.codCan;
    }

    public void setTpFinCost(short tpFinCost) {
        this.tpFinCost = tpFinCost;
    }

    public short getTpFinCost() {
        return this.tpFinCost;
    }

    public void setNazDestFnd(short nazDestFnd) {
        this.nazDestFnd = nazDestFnd;
    }

    public short getNazDestFnd() {
        return this.nazDestFnd;
    }

    public void setFlAuFatcaAeoi(short flAuFatcaAeoi) {
        this.flAuFatcaAeoi = flAuFatcaAeoi;
    }

    public short getFlAuFatcaAeoi() {
        return this.flAuFatcaAeoi;
    }

    public void setTpCarFinGiur(short tpCarFinGiur) {
        this.tpCarFinGiur = tpCarFinGiur;
    }

    public short getTpCarFinGiur() {
        return this.tpCarFinGiur;
    }

    public void setTpCarFinGiurAt(short tpCarFinGiurAt) {
        this.tpCarFinGiurAt = tpCarFinGiurAt;
    }

    public short getTpCarFinGiurAt() {
        return this.tpCarFinGiurAt;
    }

    public void setTpCarFinGiurPa(short tpCarFinGiurPa) {
        this.tpCarFinGiurPa = tpCarFinGiurPa;
    }

    public short getTpCarFinGiurPa() {
        return this.tpCarFinGiurPa;
    }

    public void setFlIstituzFin(short flIstituzFin) {
        this.flIstituzFin = flIstituzFin;
    }

    public short getFlIstituzFin() {
        return this.flIstituzFin;
    }

    public void setTpOriFndTitEff(short tpOriFndTitEff) {
        this.tpOriFndTitEff = tpOriFndTitEff;
    }

    public short getTpOriFndTitEff() {
        return this.tpOriFndTitEff;
    }

    public void setPcEspAgPaMsc(short pcEspAgPaMsc) {
        this.pcEspAgPaMsc = pcEspAgPaMsc;
    }

    public short getPcEspAgPaMsc() {
        return this.pcEspAgPaMsc;
    }

    public void setFlPrTrUsa(short flPrTrUsa) {
        this.flPrTrUsa = flPrTrUsa;
    }

    public short getFlPrTrUsa() {
        return this.flPrTrUsa;
    }

    public void setFlPrTrNoUsa(short flPrTrNoUsa) {
        this.flPrTrNoUsa = flPrTrNoUsa;
    }

    public short getFlPrTrNoUsa() {
        return this.flPrTrNoUsa;
    }

    public void setPcRipPatAsVita(short pcRipPatAsVita) {
        this.pcRipPatAsVita = pcRipPatAsVita;
    }

    public short getPcRipPatAsVita() {
        return this.pcRipPatAsVita;
    }

    public void setPcRipPatIm(short pcRipPatIm) {
        this.pcRipPatIm = pcRipPatIm;
    }

    public short getPcRipPatIm() {
        return this.pcRipPatIm;
    }

    public void setPcRipPatSetIm(short pcRipPatSetIm) {
        this.pcRipPatSetIm = pcRipPatSetIm;
    }

    public short getPcRipPatSetIm() {
        return this.pcRipPatSetIm;
    }

    public void setTpStatusAeoi(short tpStatusAeoi) {
        this.tpStatusAeoi = tpStatusAeoi;
    }

    public short getTpStatusAeoi() {
        return this.tpStatusAeoi;
    }

    public void setTpStatusFatca(short tpStatusFatca) {
        this.tpStatusFatca = tpStatusFatca;
    }

    public short getTpStatusFatca() {
        return this.tpStatusFatca;
    }

    public void setFlRappPaMsc(short flRappPaMsc) {
        this.flRappPaMsc = flRappPaMsc;
    }

    public short getFlRappPaMsc() {
        return this.flRappPaMsc;
    }

    public void setCodComunSvolAtt(short codComunSvolAtt) {
        this.codComunSvolAtt = codComunSvolAtt;
    }

    public short getCodComunSvolAtt() {
        return this.codComunSvolAtt;
    }

    public void setTpDt1oConCli(short tpDt1oConCli) {
        this.tpDt1oConCli = tpDt1oConCli;
    }

    public short getTpDt1oConCli() {
        return this.tpDt1oConCli;
    }

    public void setTpModEnRelaInt(short tpModEnRelaInt) {
        this.tpModEnRelaInt = tpModEnRelaInt;
    }

    public short getTpModEnRelaInt() {
        return this.tpModEnRelaInt;
    }

    public void setTpReddAnnuLrd(short tpReddAnnuLrd) {
        this.tpReddAnnuLrd = tpReddAnnuLrd;
    }

    public short getTpReddAnnuLrd() {
        return this.tpReddAnnuLrd;
    }

    public void setTpReddCon(short tpReddCon) {
        this.tpReddCon = tpReddCon;
    }

    public short getTpReddCon() {
        return this.tpReddCon;
    }

    public void setTpOperSocFid(short tpOperSocFid) {
        this.tpOperSocFid = tpOperSocFid;
    }

    public short getTpOperSocFid() {
        return this.tpOperSocFid;
    }

    public void setCodPaEspMsc1(short codPaEspMsc1) {
        this.codPaEspMsc1 = codPaEspMsc1;
    }

    public short getCodPaEspMsc1() {
        return this.codPaEspMsc1;
    }

    public void setImpPaEspMsc1(short impPaEspMsc1) {
        this.impPaEspMsc1 = impPaEspMsc1;
    }

    public short getImpPaEspMsc1() {
        return this.impPaEspMsc1;
    }

    public void setCodPaEspMsc2(short codPaEspMsc2) {
        this.codPaEspMsc2 = codPaEspMsc2;
    }

    public short getCodPaEspMsc2() {
        return this.codPaEspMsc2;
    }

    public void setImpPaEspMsc2(short impPaEspMsc2) {
        this.impPaEspMsc2 = impPaEspMsc2;
    }

    public short getImpPaEspMsc2() {
        return this.impPaEspMsc2;
    }

    public void setCodPaEspMsc3(short codPaEspMsc3) {
        this.codPaEspMsc3 = codPaEspMsc3;
    }

    public short getCodPaEspMsc3() {
        return this.codPaEspMsc3;
    }

    public void setImpPaEspMsc3(short impPaEspMsc3) {
        this.impPaEspMsc3 = impPaEspMsc3;
    }

    public short getImpPaEspMsc3() {
        return this.impPaEspMsc3;
    }

    public void setCodPaEspMsc4(short codPaEspMsc4) {
        this.codPaEspMsc4 = codPaEspMsc4;
    }

    public short getCodPaEspMsc4() {
        return this.codPaEspMsc4;
    }

    public void setImpPaEspMsc4(short impPaEspMsc4) {
        this.impPaEspMsc4 = impPaEspMsc4;
    }

    public short getImpPaEspMsc4() {
        return this.impPaEspMsc4;
    }

    public void setCodPaEspMsc5(short codPaEspMsc5) {
        this.codPaEspMsc5 = codPaEspMsc5;
    }

    public short getCodPaEspMsc5() {
        return this.codPaEspMsc5;
    }

    public void setImpPaEspMsc5(short impPaEspMsc5) {
        this.impPaEspMsc5 = impPaEspMsc5;
    }

    public short getImpPaEspMsc5() {
        return this.impPaEspMsc5;
    }

    public void setDescOrgnFnd(short descOrgnFnd) {
        this.descOrgnFnd = descOrgnFnd;
    }

    public short getDescOrgnFnd() {
        return this.descOrgnFnd;
    }

    public void setCodAutDueDil(short codAutDueDil) {
        this.codAutDueDil = codAutDueDil;
    }

    public short getCodAutDueDil() {
        return this.codAutDueDil;
    }

    public void setFlPrQuestFatca(short flPrQuestFatca) {
        this.flPrQuestFatca = flPrQuestFatca;
    }

    public short getFlPrQuestFatca() {
        return this.flPrQuestFatca;
    }

    public void setFlPrQuestAeoi(short flPrQuestAeoi) {
        this.flPrQuestAeoi = flPrQuestAeoi;
    }

    public short getFlPrQuestAeoi() {
        return this.flPrQuestAeoi;
    }

    public void setFlPrQuestOfac(short flPrQuestOfac) {
        this.flPrQuestOfac = flPrQuestOfac;
    }

    public short getFlPrQuestOfac() {
        return this.flPrQuestOfac;
    }

    public void setFlPrQuestKyc(short flPrQuestKyc) {
        this.flPrQuestKyc = flPrQuestKyc;
    }

    public short getFlPrQuestKyc() {
        return this.flPrQuestKyc;
    }

    public void setFlPrQuestMscq(short flPrQuestMscq) {
        this.flPrQuestMscq = flPrQuestMscq;
    }

    public short getFlPrQuestMscq() {
        return this.flPrQuestMscq;
    }

    public void setTpNotPreg(short tpNotPreg) {
        this.tpNotPreg = tpNotPreg;
    }

    public short getTpNotPreg() {
        return this.tpNotPreg;
    }

    public void setTpProcPnl(short tpProcPnl) {
        this.tpProcPnl = tpProcPnl;
    }

    public short getTpProcPnl() {
        return this.tpProcPnl;
    }

    public void setCodImpCarPub(short codImpCarPub) {
        this.codImpCarPub = codImpCarPub;
    }

    public short getCodImpCarPub() {
        return this.codImpCarPub;
    }

    public void setOprzSospette(short oprzSospette) {
        this.oprzSospette = oprzSospette;
    }

    public short getOprzSospette() {
        return this.oprzSospette;
    }

    public void setUltFattAnnu(short ultFattAnnu) {
        this.ultFattAnnu = ultFattAnnu;
    }

    public short getUltFattAnnu() {
        return this.ultFattAnnu;
    }

    public void setDescPep(short descPep) {
        this.descPep = descPep;
    }

    public short getDescPep() {
        return this.descPep;
    }

    public void setNumTel2(short numTel2) {
        this.numTel2 = numTel2;
    }

    public short getNumTel2() {
        return this.numTel2;
    }

    public void setImpAfi(short impAfi) {
        this.impAfi = impAfi;
    }

    public short getImpAfi() {
        return this.impAfi;
    }

    public void setFlNewPro(short flNewPro) {
        this.flNewPro = flNewPro;
    }

    public short getFlNewPro() {
        return this.flNewPro;
    }

    public void setTpMotCambioCntr(short tpMotCambioCntr) {
        this.tpMotCambioCntr = tpMotCambioCntr;
    }

    public short getTpMotCambioCntr() {
        return this.tpMotCambioCntr;
    }

    public void setDescMotCambioCn(short descMotCambioCn) {
        this.descMotCambioCn = descMotCambioCn;
    }

    public short getDescMotCambioCn() {
        return this.descMotCambioCn;
    }

    public void setCodSogg(short codSogg) {
        this.codSogg = codSogg;
    }

    public short getCodSogg() {
        return this.codSogg;
    }
}
