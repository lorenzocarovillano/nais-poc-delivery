package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: DETT-RICH<br>
 * Variable: DETT-RICH from copybook IDBVDER1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DettRich {

    //==== PROPERTIES ====
    //Original name: DER-ID-RICH
    private int idRich = DefaultValues.INT_VAL;
    //Original name: DER-PROG-DETT-RICH
    private int progDettRich = DefaultValues.INT_VAL;
    //Original name: DER-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: DER-TP-AREA-D-RICH
    private String tpAreaDRich = DefaultValues.stringVal(Len.TP_AREA_D_RICH);
    //Original name: DER-AREA-D-RICH-LEN
    private short areaDRichLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: DER-AREA-D-RICH
    private String areaDRich = DefaultValues.stringVal(Len.AREA_D_RICH);
    //Original name: DER-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: DER-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: DER-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: DER-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: DER-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setDettRichFormatted(String data) {
        byte[] buffer = new byte[Len.DETT_RICH];
        MarshalByte.writeString(buffer, 1, data, Len.DETT_RICH);
        setDettRichBytes(buffer, 1);
    }

    public String getDettRichFormatted() {
        return MarshalByteExt.bufferToStr(getDettRichBytes());
    }

    public byte[] getDettRichBytes() {
        byte[] buffer = new byte[Len.DETT_RICH];
        return getDettRichBytes(buffer, 1);
    }

    public void setDettRichBytes(byte[] buffer, int offset) {
        int position = offset;
        idRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_RICH, 0);
        position += Len.ID_RICH;
        progDettRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PROG_DETT_RICH, 0);
        position += Len.PROG_DETT_RICH;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        tpAreaDRich = MarshalByte.readString(buffer, position, Len.TP_AREA_D_RICH);
        position += Len.TP_AREA_D_RICH;
        setAreaDRichVcharBytes(buffer, position);
        position += Len.AREA_D_RICH_VCHAR;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDettRichBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idRich, Len.Int.ID_RICH, 0);
        position += Len.ID_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, progDettRich, Len.Int.PROG_DETT_RICH, 0);
        position += Len.PROG_DETT_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, tpAreaDRich, Len.TP_AREA_D_RICH);
        position += Len.TP_AREA_D_RICH;
        getAreaDRichVcharBytes(buffer, position);
        position += Len.AREA_D_RICH_VCHAR;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsCptz, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        return buffer;
    }

    public void setIdRich(int idRich) {
        this.idRich = idRich;
    }

    public int getIdRich() {
        return this.idRich;
    }

    public void setProgDettRich(int progDettRich) {
        this.progDettRich = progDettRich;
    }

    public int getProgDettRich() {
        return this.progDettRich;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setTpAreaDRich(String tpAreaDRich) {
        this.tpAreaDRich = Functions.subString(tpAreaDRich, Len.TP_AREA_D_RICH);
    }

    public String getTpAreaDRich() {
        return this.tpAreaDRich;
    }

    public void setAreaDRichVcharFormatted(String data) {
        byte[] buffer = new byte[Len.AREA_D_RICH_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.AREA_D_RICH_VCHAR);
        setAreaDRichVcharBytes(buffer, 1);
    }

    public String getAreaDRichVcharFormatted() {
        return MarshalByteExt.bufferToStr(getAreaDRichVcharBytes());
    }

    /**Original name: DER-AREA-D-RICH-VCHAR<br>*/
    public byte[] getAreaDRichVcharBytes() {
        byte[] buffer = new byte[Len.AREA_D_RICH_VCHAR];
        return getAreaDRichVcharBytes(buffer, 1);
    }

    public void setAreaDRichVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        areaDRichLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        areaDRich = MarshalByte.readString(buffer, position, Len.AREA_D_RICH);
    }

    public byte[] getAreaDRichVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, areaDRichLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, areaDRich, Len.AREA_D_RICH);
        return buffer;
    }

    public void setAreaDRichLen(short areaDRichLen) {
        this.areaDRichLen = areaDRichLen;
    }

    public short getAreaDRichLen() {
        return this.areaDRichLen;
    }

    public void setAreaDRich(String areaDRich) {
        this.areaDRich = Functions.subString(areaDRich, Len.AREA_D_RICH);
    }

    public String getAreaDRich() {
        return this.areaDRich;
    }

    public String getAreaDRichFormatted() {
        return Functions.padBlanks(getAreaDRich(), Len.AREA_D_RICH);
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_AREA_D_RICH = 3;
        public static final int AREA_D_RICH = 10000;
        public static final int DS_UTENTE = 20;
        public static final int AREA_D_RICH_LEN = 2;
        public static final int AREA_D_RICH_VCHAR = AREA_D_RICH_LEN + AREA_D_RICH;
        public static final int ID_RICH = 5;
        public static final int PROG_DETT_RICH = 3;
        public static final int COD_COMP_ANIA = 3;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_CPTZ = 10;
        public static final int DS_STATO_ELAB = 1;
        public static final int DETT_RICH = ID_RICH + PROG_DETT_RICH + COD_COMP_ANIA + TP_AREA_D_RICH + AREA_D_RICH_VCHAR + DS_OPER_SQL + DS_VER + DS_TS_CPTZ + DS_UTENTE + DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_RICH = 9;
            public static final int PROG_DETT_RICH = 5;
            public static final int COD_COMP_ANIA = 5;
            public static final int DS_VER = 9;
            public static final int DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
