package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVP883<br>
 * Copybook: IDBVP883 from copybook IDBVP883<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvp883 {

    //==== PROPERTIES ====
    //Original name: P88-DT-INI-EFF-DB
    private String p88DtIniEffDb = DefaultValues.stringVal(Len.P88_DT_INI_EFF_DB);
    //Original name: P88-DT-END-EFF-DB
    private String p88DtEndEffDb = DefaultValues.stringVal(Len.P88_DT_END_EFF_DB);

    //==== METHODS ====
    public void setP88DtIniEffDb(String p88DtIniEffDb) {
        this.p88DtIniEffDb = Functions.subString(p88DtIniEffDb, Len.P88_DT_INI_EFF_DB);
    }

    public String getP88DtIniEffDb() {
        return this.p88DtIniEffDb;
    }

    public void setP88DtEndEffDb(String p88DtEndEffDb) {
        this.p88DtEndEffDb = Functions.subString(p88DtEndEffDb, Len.P88_DT_END_EFF_DB);
    }

    public String getP88DtEndEffDb() {
        return this.p88DtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P88_DT_INI_EFF_DB = 10;
        public static final int P88_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
