package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-CONT-DB<br>
 * Variable: TIT-CONT-DB from copybook IDBVTIT3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TitContDb {

    //==== PROPERTIES ====
    //Original name: TIT-DT-INI-EFF-DB
    private String iniEffDb = DefaultValues.stringVal(Len.INI_EFF_DB);
    //Original name: TIT-DT-END-EFF-DB
    private String endEffDb = DefaultValues.stringVal(Len.END_EFF_DB);
    //Original name: TIT-DT-INI-COP-DB
    private String iniCopDb = DefaultValues.stringVal(Len.INI_COP_DB);
    //Original name: TIT-DT-END-COP-DB
    private String endCopDb = DefaultValues.stringVal(Len.END_COP_DB);
    //Original name: TIT-DT-APPLZ-MORA-DB
    private String applzMoraDb = DefaultValues.stringVal(Len.APPLZ_MORA_DB);
    //Original name: TIT-DT-EMIS-TIT-DB
    private String emisTitDb = DefaultValues.stringVal(Len.EMIS_TIT_DB);
    //Original name: TIT-DT-ESI-TIT-DB
    private String esiTitDb = DefaultValues.stringVal(Len.ESI_TIT_DB);
    //Original name: TIT-DT-VLT-DB
    private String vltDb = DefaultValues.stringVal(Len.VLT_DB);
    //Original name: TIT-DT-CAMBIO-VLT-DB
    private String cambioVltDb = DefaultValues.stringVal(Len.CAMBIO_VLT_DB);
    //Original name: TIT-DT-RICH-ADD-RID-DB
    private String richAddRidDb = DefaultValues.stringVal(Len.RICH_ADD_RID_DB);
    //Original name: TIT-DT-CERT-FISC-DB
    private String certFiscDb = DefaultValues.stringVal(Len.CERT_FISC_DB);

    //==== METHODS ====
    public void setIniEffDb(String iniEffDb) {
        this.iniEffDb = Functions.subString(iniEffDb, Len.INI_EFF_DB);
    }

    public String getIniEffDb() {
        return this.iniEffDb;
    }

    public void setEndEffDb(String endEffDb) {
        this.endEffDb = Functions.subString(endEffDb, Len.END_EFF_DB);
    }

    public String getEndEffDb() {
        return this.endEffDb;
    }

    public void setIniCopDb(String iniCopDb) {
        this.iniCopDb = Functions.subString(iniCopDb, Len.INI_COP_DB);
    }

    public String getIniCopDb() {
        return this.iniCopDb;
    }

    public void setEndCopDb(String endCopDb) {
        this.endCopDb = Functions.subString(endCopDb, Len.END_COP_DB);
    }

    public String getEndCopDb() {
        return this.endCopDb;
    }

    public void setApplzMoraDb(String applzMoraDb) {
        this.applzMoraDb = Functions.subString(applzMoraDb, Len.APPLZ_MORA_DB);
    }

    public String getApplzMoraDb() {
        return this.applzMoraDb;
    }

    public void setEmisTitDb(String emisTitDb) {
        this.emisTitDb = Functions.subString(emisTitDb, Len.EMIS_TIT_DB);
    }

    public String getEmisTitDb() {
        return this.emisTitDb;
    }

    public void setEsiTitDb(String esiTitDb) {
        this.esiTitDb = Functions.subString(esiTitDb, Len.ESI_TIT_DB);
    }

    public String getEsiTitDb() {
        return this.esiTitDb;
    }

    public void setVltDb(String vltDb) {
        this.vltDb = Functions.subString(vltDb, Len.VLT_DB);
    }

    public String getVltDb() {
        return this.vltDb;
    }

    public void setCambioVltDb(String cambioVltDb) {
        this.cambioVltDb = Functions.subString(cambioVltDb, Len.CAMBIO_VLT_DB);
    }

    public String getCambioVltDb() {
        return this.cambioVltDb;
    }

    public void setRichAddRidDb(String richAddRidDb) {
        this.richAddRidDb = Functions.subString(richAddRidDb, Len.RICH_ADD_RID_DB);
    }

    public String getRichAddRidDb() {
        return this.richAddRidDb;
    }

    public void setCertFiscDb(String certFiscDb) {
        this.certFiscDb = Functions.subString(certFiscDb, Len.CERT_FISC_DB);
    }

    public String getCertFiscDb() {
        return this.certFiscDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INI_EFF_DB = 10;
        public static final int END_EFF_DB = 10;
        public static final int INI_COP_DB = 10;
        public static final int END_COP_DB = 10;
        public static final int APPLZ_MORA_DB = 10;
        public static final int EMIS_TIT_DB = 10;
        public static final int ESI_TIT_DB = 10;
        public static final int VLT_DB = 10;
        public static final int CAMBIO_VLT_DB = 10;
        public static final int RICH_ADD_RID_DB = 10;
        public static final int CERT_FISC_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
