package it.accenture.jnais.copy;

/**Original name: IVVC0218<br>
 * Variable: IVVC0218 from copybook IVVC0218<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0218Ivvs0211 {

    //==== PROPERTIES ====
    /**Original name: IVVC0218-ALIAS-ADES<br>
	 * <pre>----------------------------------------------------------------*
	 *    ALIAS INPUT VARIABILI
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA
	 *    LUNG.
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 * ASSET
	 * *****************************************************************
	 * --  ALIAS ADESIONE</pre>*/
    private String aliasAdes = "ADE";
    /**Original name: IVVC0218-ALIAS-BENEF<br>
	 * <pre>--  ALIAS BENEFICIARI</pre>*/
    private String aliasBenef = "BEP";
    /**Original name: IVVC0218-ALIAS-BENEF-LIQ<br>
	 * <pre>--  ALIAS BENEFICIARIO DI LIQUIDAZIONE</pre>*/
    private String aliasBenefLiq = "BEL";
    /**Original name: IVVC0218-ALIAS-DT-COLL<br>
	 * <pre>--  ALIAS DATI COLLETTIVA</pre>*/
    private String aliasDtColl = "DCO";
    /**Original name: IVVC0218-ALIAS-DT-FISC-ADES<br>
	 * <pre>--  ALIAS DATI FISCALE ADESIONE</pre>*/
    private String aliasDtFiscAdes = "DFA";
    /**Original name: IVVC0218-ALIAS-DETT-QUEST<br>
	 * <pre>--  ALIAS DETTAGLIO QUESTIONARIO</pre>*/
    private String aliasDettQuest = "DEQ";
    /**Original name: IVVC0218-ALIAS-DETT-TIT-CONT<br>
	 * <pre>--  ALIAS DETTAGLIO TITOLO CONTABILE</pre>*/
    private String aliasDettTitCont = "DTC";
    /**Original name: IVVC0218-ALIAS-GARANZIA<br>
	 * <pre>--  ALIAS GARANZIA</pre>*/
    private String aliasGaranzia = "GRZ";
    /**Original name: IVVC0218-ALIAS-GARANZIA-OPZ<br>
	 * <pre>--  ALIAS GARANZIA OPZIONE</pre>*/
    private String aliasGaranziaOpz = "GOP";
    /**Original name: IVVC0218-ALIAS-GAR-LIQ<br>
	 * <pre>--  ALIAS GARANZIA DI LIQUIDAZIONE</pre>*/
    private String aliasGarLiq = "GRL";
    /**Original name: IVVC0218-ALIAS-IMPOSTA-BOLLO<br>
	 * <pre>--  ALIAS IMPOSTA DI BOLLO</pre>*/
    private String aliasImpostaBollo = "P58";
    /**Original name: IVVC0218-ALIAS-DATI-CRIST<br>
	 * <pre>--  ALIAS DATI CRISTALLIZZATI</pre>*/
    private String aliasDatiCrist = "P61";
    /**Original name: IVVC0218-ALIAS-IMPOSTA-SOST<br>
	 * <pre>--  ALIAS IMPOSTA SOSTITUTIVA</pre>*/
    private String aliasImpostaSost = "ISO";
    /**Original name: IVVC0218-ALIAS-LIQUIDAZ<br>
	 * <pre>--  ALIAS LIQUIDAZIONE</pre>*/
    private String aliasLiquidaz = "LQU";
    /**Original name: IVVC0218-ALIAS-MOVIMENTO<br>
	 * <pre>--  ALIAS MOVIMENTO</pre>*/
    private String aliasMovimento = "MOV";
    /**Original name: IVVC0218-ALIAS-MOVI-FINRIO<br>
	 * <pre>--  ALIAS MOVIMENTO FINANZIARIO</pre>*/
    private String aliasMoviFinrio = "MFZ";
    /**Original name: IVVC0218-ALIAS-PARAM-OGG<br>
	 * <pre>--  ALIAS PARAMETRO OGGETTO</pre>*/
    private String aliasParamOgg = "POG";
    /**Original name: IVVC0218-ALIAS-PARAM-MOV<br>
	 * <pre>--  ALIAS PARAMETRO MOVIMENTO</pre>*/
    private String aliasParamMov = "PMO";
    /**Original name: IVVC0218-ALIAS-PERC-LIQ<br>
	 * <pre>--  ALIAS PERCIPIENTE LIQUIDAZIONE</pre>*/
    private String aliasPercLiq = "PLI";
    /**Original name: IVVC0218-ALIAS-POLI<br>
	 * <pre>--  ALIAS POLIZZA</pre>*/
    private String aliasPoli = "POL";
    /**Original name: IVVC0218-ALIAS-PRESTITI<br>
	 * <pre>--  ALIAS PRESTITI</pre>*/
    private String aliasPrestiti = "PRE";
    /**Original name: IVVC0218-ALIAS-PROVV-TRAN<br>
	 * <pre>--  ALIAS PROVVIGIONE DI TRANCHE</pre>*/
    private String aliasProvvTran = "PVT";
    /**Original name: IVVC0218-ALIAS-QUEST<br>
	 * <pre>--  ALIAS QUESTIONARIO</pre>*/
    private String aliasQuest = "QUE";
    /**Original name: IVVC0218-ALIAS-RICH<br>
	 * <pre>--  ALIAS RICHIESTA</pre>*/
    private String aliasRich = "RIC";
    /**Original name: IVVC0218-ALIAS-RICH-DISINV-FND<br>
	 * <pre>--  ALIAS RICHIESTA DISINVESTIMENTO FONDO</pre>*/
    private String aliasRichDisinvFnd = "RDF";
    /**Original name: IVVC0218-ALIAS-RICH-INV-FND<br>
	 * <pre>--  ALIAS RICHIESTA INVESTIMENTO FONDO</pre>*/
    private String aliasRichInvFnd = "RIF";
    /**Original name: IVVC0218-ALIAS-RAPP-ANAG<br>
	 * <pre>--  ALIAS RAPPORTO ANAGRAFICO</pre>*/
    private String aliasRappAnag = "RAN";
    /**Original name: IVVC0218-ALIAS-EST-RAPP-ANAG<br>
	 * <pre>--  ALIAS EST RAPPORTO ANAGRAFICO</pre>*/
    private String aliasEstRappAnag = "E15";
    /**Original name: IVVC0218-ALIAS-RAPP-RETE<br>
	 * <pre>--  ALIAS RAPPORTO RETE</pre>*/
    private String aliasRappRete = "RRE";
    /**Original name: IVVC0218-ALIAS-SOPRAP-GAR<br>
	 * <pre>--  ALIAS SOPRAPREMIO DI GARANZIA</pre>*/
    private String aliasSoprapGar = "SPG";
    /**Original name: IVVC0218-ALIAS-STRA-INV<br>
	 * <pre>--  ALIAS STRATEGIA DI INVESTIMENTO</pre>*/
    private String aliasStraInv = "SDI";
    /**Original name: IVVC0218-ALIAS-TIT-CONT<br>
	 * <pre>--  ALIAS TITOLO CONTABILE</pre>*/
    private String aliasTitCont = "TIT";
    /**Original name: IVVC0218-ALIAS-TIT-LIQ<br>
	 * <pre>--  ALIAS TITOLO DI LIQUIDAZIONE</pre>*/
    private String aliasTitLiq = "TCL";
    /**Original name: IVVC0218-ALIAS-TRCH-GAR<br>
	 * <pre>--  ALIAS TRANCHE DI GARANZIA</pre>*/
    private String aliasTrchGar = "TGA";
    /**Original name: IVVC0218-ALIAS-TRCH-LIQ<br>
	 * <pre>-- ALIAS TRANCHE GARANZIA DI LIQUIDAZIONE</pre>*/
    private String aliasTrchLiq = "TLI";
    /**Original name: IVVC0218-ALIAS-TRCH-GAR-OPZ<br>
	 * <pre>--  ALIAS TRANCHE DI GARANZIA OPZIONE</pre>*/
    private String aliasTrchGarOpz = "TOP";
    /**Original name: IVVC0218-ALIAS-DFLT-ADES<br>
	 * <pre>-- ALIAS DEFAULT ADESIONE</pre>*/
    private String aliasDfltAdes = "DAD";
    /**Original name: IVVC0218-ALIAS-OGG-COLL<br>
	 * <pre>-- ALIAS OGGETTO COLLEGATO</pre>*/
    private String aliasOggColl = "OCO";
    /**Original name: IVVC0218-ALIAS-DT-FORZ-LIQ<br>
	 * <pre>-- ALIAS DATI FORZATI DI LIQUIDAZIONE</pre>*/
    private String aliasDtForzLiq = "DFL";
    /**Original name: IVVC0218-ALIAS-CLAU-TEST<br>
	 * <pre>--  ALIAS DATI CLAUSOLA TESTUAEL</pre>*/
    private String aliasClauTest = "CLT";
    /**Original name: IVVC0218-ALIAS-PARAM-COMP<br>
	 * <pre>--  ALIAS parametro compagnia</pre>*/
    private String aliasParamComp = "PCO";
    /**Original name: IVVC0218-ALIAS-OPZIONI<br>
	 * <pre>-- ALIAS OPZIONI</pre>*/
    private String aliasOpzioni = "OPZ";
    /**Original name: IVVC0218-ALIAS-DATI-CONTEST<br>
	 * <pre>--  ALIAS DATI CONTESTUALI</pre>*/
    private String aliasDatiContest = "CNT";
    /**Original name: IVVC0218-ALIAS-VINC-PEGN<br>
	 * <pre>--  ALIAS VINCOLI E PEGNI</pre>*/
    private String aliasVincPegn = "L23";
    /**Original name: IVVC0218-ALIAS-REINVST-POLI<br>
	 * <pre>--  ALIAS REINVESTIMENTO POLIZZA</pre>*/
    private String aliasReinvstPoli = "L30";
    /**Original name: IVVC0218-ALIAS-QOTAZ-FON<br>
	 * <pre>--  ALIAS REINVESTIMENTO POLIZZA</pre>*/
    private String aliasQotazFon = "L19";
    /**Original name: IVVC0218-ALIAS-RIS-DI-TRANCHE<br>
	 * <pre>--  ALIAR RIS DI TRANCHE</pre>*/
    private String aliasRisDiTranche = "RST";
    /**Original name: IVVC0218-ALIAS-AREA-FND-X-TRCH<br>
	 * <pre>--  ALIAS AREA FONDI X TRANCHE</pre>*/
    private String aliasAreaFndXTrch = "FXT";
    /**Original name: IVVC0218-ALIAS-RICH-EST<br>
	 * <pre>--  ALIAS RICH_EST</pre>*/
    private String aliasRichEst = "P01";
    /**Original name: IVVC0218-ALIAS-QUEST-ADEG-VER<br>
	 * <pre>--  ALIAS QUEST ADEG VER</pre>*/
    private String aliasQuestAdegVer = "P56";
    /**Original name: IVVC0218-ALIAS-EST-POLI-CPI-PR<br>
	 * <pre>--  ALIAS EST POLI CPI PR</pre>*/
    private String aliasEstPoliCpiPr = "P67";
    /**Original name: IVVC0218-ALIAS-ATT-SERV-VAL<br>
	 * <pre>-- ALIAS ATT-SERV-VAL</pre>*/
    private String aliasAttServVal = "P88";
    /**Original name: IVVC0218-ALIAS-D-ATT-SERV-VAL<br>
	 * <pre>-- ALIAS D-ATT-SERV-VAL</pre>*/
    private String aliasDAttServVal = "P89";
    /**Original name: IVVC0218-ALIAS-AREA-VAR-X-GAR<br>
	 * <pre>--  ALIAS BILA PROV AMM T</pre>*/
    private String aliasAreaVarXGar = "VXG";
    /**Original name: IVVC0218-ALIAS-AREA-FND-X-CDG<br>
	 * <pre>-- ALIAS FONDI PER COMMIS GEST</pre>*/
    private String aliasAreaFndXCdg = "FXC";

    //==== METHODS ====
    public String getAliasAdes() {
        return this.aliasAdes;
    }

    public String getAliasBenef() {
        return this.aliasBenef;
    }

    public String getAliasBenefLiq() {
        return this.aliasBenefLiq;
    }

    public String getAliasDtColl() {
        return this.aliasDtColl;
    }

    public String getAliasDtFiscAdes() {
        return this.aliasDtFiscAdes;
    }

    public String getAliasDettQuest() {
        return this.aliasDettQuest;
    }

    public String getAliasDettTitCont() {
        return this.aliasDettTitCont;
    }

    public String getAliasGaranzia() {
        return this.aliasGaranzia;
    }

    public String getAliasGaranziaOpz() {
        return this.aliasGaranziaOpz;
    }

    public String getAliasGarLiq() {
        return this.aliasGarLiq;
    }

    public String getAliasImpostaBollo() {
        return this.aliasImpostaBollo;
    }

    public String getAliasDatiCrist() {
        return this.aliasDatiCrist;
    }

    public String getAliasImpostaSost() {
        return this.aliasImpostaSost;
    }

    public String getAliasLiquidaz() {
        return this.aliasLiquidaz;
    }

    public String getAliasMovimento() {
        return this.aliasMovimento;
    }

    public String getAliasMoviFinrio() {
        return this.aliasMoviFinrio;
    }

    public String getAliasParamOgg() {
        return this.aliasParamOgg;
    }

    public String getAliasParamMov() {
        return this.aliasParamMov;
    }

    public String getAliasPercLiq() {
        return this.aliasPercLiq;
    }

    public String getAliasPoli() {
        return this.aliasPoli;
    }

    public String getAliasPrestiti() {
        return this.aliasPrestiti;
    }

    public String getAliasProvvTran() {
        return this.aliasProvvTran;
    }

    public String getAliasQuest() {
        return this.aliasQuest;
    }

    public String getAliasRich() {
        return this.aliasRich;
    }

    public String getAliasRichDisinvFnd() {
        return this.aliasRichDisinvFnd;
    }

    public String getAliasRichInvFnd() {
        return this.aliasRichInvFnd;
    }

    public String getAliasRappAnag() {
        return this.aliasRappAnag;
    }

    public String getAliasEstRappAnag() {
        return this.aliasEstRappAnag;
    }

    public String getAliasRappRete() {
        return this.aliasRappRete;
    }

    public String getAliasSoprapGar() {
        return this.aliasSoprapGar;
    }

    public String getAliasStraInv() {
        return this.aliasStraInv;
    }

    public String getAliasTitCont() {
        return this.aliasTitCont;
    }

    public String getAliasTitLiq() {
        return this.aliasTitLiq;
    }

    public String getAliasTrchGar() {
        return this.aliasTrchGar;
    }

    public String getAliasTrchLiq() {
        return this.aliasTrchLiq;
    }

    public String getAliasTrchGarOpz() {
        return this.aliasTrchGarOpz;
    }

    public String getAliasDfltAdes() {
        return this.aliasDfltAdes;
    }

    public String getAliasOggColl() {
        return this.aliasOggColl;
    }

    public String getAliasDtForzLiq() {
        return this.aliasDtForzLiq;
    }

    public String getAliasClauTest() {
        return this.aliasClauTest;
    }

    public String getAliasParamComp() {
        return this.aliasParamComp;
    }

    public String getAliasOpzioni() {
        return this.aliasOpzioni;
    }

    public String getAliasDatiContest() {
        return this.aliasDatiContest;
    }

    public String getAliasVincPegn() {
        return this.aliasVincPegn;
    }

    public String getAliasReinvstPoli() {
        return this.aliasReinvstPoli;
    }

    public String getAliasQotazFon() {
        return this.aliasQotazFon;
    }

    public String getAliasRisDiTranche() {
        return this.aliasRisDiTranche;
    }

    public String getAliasAreaFndXTrch() {
        return this.aliasAreaFndXTrch;
    }

    public String getAliasRichEst() {
        return this.aliasRichEst;
    }

    public String getAliasQuestAdegVer() {
        return this.aliasQuestAdegVer;
    }

    public String getAliasEstPoliCpiPr() {
        return this.aliasEstPoliCpiPr;
    }

    public String getAliasAttServVal() {
        return this.aliasAttServVal;
    }

    public String getAliasDAttServVal() {
        return this.aliasDAttServVal;
    }

    public String getAliasAreaVarXGar() {
        return this.aliasAreaVarXGar;
    }

    public String getAliasAreaFndXCdg() {
        return this.aliasAreaFndXCdg;
    }
}
