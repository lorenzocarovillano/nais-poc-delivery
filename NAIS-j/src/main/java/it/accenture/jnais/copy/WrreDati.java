package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WrreCodAcqsCntrt;
import it.accenture.jnais.ws.redefines.WrreCodCan;
import it.accenture.jnais.ws.redefines.WrreCodPntReteEnd;
import it.accenture.jnais.ws.redefines.WrreCodPntReteIni;
import it.accenture.jnais.ws.redefines.WrreCodPntReteIniC;
import it.accenture.jnais.ws.redefines.WrreIdMoviChiu;
import it.accenture.jnais.ws.redefines.WrreIdOgg;
import it.accenture.jnais.ws.redefines.WrreTpAcqsCntrt;

/**Original name: WRRE-DATI<br>
 * Variable: WRRE-DATI from copybook LCCVRRE1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WrreDati {

    //==== PROPERTIES ====
    //Original name: WRRE-ID-RAPP-RETE
    private int wrreIdRappRete = DefaultValues.INT_VAL;
    //Original name: WRRE-ID-OGG
    private WrreIdOgg wrreIdOgg = new WrreIdOgg();
    //Original name: WRRE-TP-OGG
    private String wrreTpOgg = DefaultValues.stringVal(Len.WRRE_TP_OGG);
    //Original name: WRRE-ID-MOVI-CRZ
    private int wrreIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WRRE-ID-MOVI-CHIU
    private WrreIdMoviChiu wrreIdMoviChiu = new WrreIdMoviChiu();
    //Original name: WRRE-DT-INI-EFF
    private int wrreDtIniEff = DefaultValues.INT_VAL;
    //Original name: WRRE-DT-END-EFF
    private int wrreDtEndEff = DefaultValues.INT_VAL;
    //Original name: WRRE-COD-COMP-ANIA
    private int wrreCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WRRE-TP-RETE
    private String wrreTpRete = DefaultValues.stringVal(Len.WRRE_TP_RETE);
    //Original name: WRRE-TP-ACQS-CNTRT
    private WrreTpAcqsCntrt wrreTpAcqsCntrt = new WrreTpAcqsCntrt();
    //Original name: WRRE-COD-ACQS-CNTRT
    private WrreCodAcqsCntrt wrreCodAcqsCntrt = new WrreCodAcqsCntrt();
    //Original name: WRRE-COD-PNT-RETE-INI
    private WrreCodPntReteIni wrreCodPntReteIni = new WrreCodPntReteIni();
    //Original name: WRRE-COD-PNT-RETE-END
    private WrreCodPntReteEnd wrreCodPntReteEnd = new WrreCodPntReteEnd();
    //Original name: WRRE-FL-PNT-RETE-1RIO
    private char wrreFlPntRete1rio = DefaultValues.CHAR_VAL;
    //Original name: WRRE-COD-CAN
    private WrreCodCan wrreCodCan = new WrreCodCan();
    //Original name: WRRE-DS-RIGA
    private long wrreDsRiga = DefaultValues.LONG_VAL;
    //Original name: WRRE-DS-OPER-SQL
    private char wrreDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WRRE-DS-VER
    private int wrreDsVer = DefaultValues.INT_VAL;
    //Original name: WRRE-DS-TS-INI-CPTZ
    private long wrreDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WRRE-DS-TS-END-CPTZ
    private long wrreDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WRRE-DS-UTENTE
    private String wrreDsUtente = DefaultValues.stringVal(Len.WRRE_DS_UTENTE);
    //Original name: WRRE-DS-STATO-ELAB
    private char wrreDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WRRE-COD-PNT-RETE-INI-C
    private WrreCodPntReteIniC wrreCodPntReteIniC = new WrreCodPntReteIniC();
    //Original name: WRRE-MATR-OPRT
    private String wrreMatrOprt = DefaultValues.stringVal(Len.WRRE_MATR_OPRT);

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wrreIdRappRete = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRRE_ID_RAPP_RETE, 0);
        position += Len.WRRE_ID_RAPP_RETE;
        wrreIdOgg.setWrreIdOggFromBuffer(buffer, position);
        position += WrreIdOgg.Len.WRRE_ID_OGG;
        wrreTpOgg = MarshalByte.readString(buffer, position, Len.WRRE_TP_OGG);
        position += Len.WRRE_TP_OGG;
        wrreIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRRE_ID_MOVI_CRZ, 0);
        position += Len.WRRE_ID_MOVI_CRZ;
        wrreIdMoviChiu.setWrreIdMoviChiuFromBuffer(buffer, position);
        position += WrreIdMoviChiu.Len.WRRE_ID_MOVI_CHIU;
        wrreDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRRE_DT_INI_EFF, 0);
        position += Len.WRRE_DT_INI_EFF;
        wrreDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRRE_DT_END_EFF, 0);
        position += Len.WRRE_DT_END_EFF;
        wrreCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRRE_COD_COMP_ANIA, 0);
        position += Len.WRRE_COD_COMP_ANIA;
        wrreTpRete = MarshalByte.readString(buffer, position, Len.WRRE_TP_RETE);
        position += Len.WRRE_TP_RETE;
        wrreTpAcqsCntrt.setWrreTpAcqsCntrtFromBuffer(buffer, position);
        position += WrreTpAcqsCntrt.Len.WRRE_TP_ACQS_CNTRT;
        wrreCodAcqsCntrt.setWrreCodAcqsCntrtFromBuffer(buffer, position);
        position += WrreCodAcqsCntrt.Len.WRRE_COD_ACQS_CNTRT;
        wrreCodPntReteIni.setWrreCodPntReteIniFromBuffer(buffer, position);
        position += WrreCodPntReteIni.Len.WRRE_COD_PNT_RETE_INI;
        wrreCodPntReteEnd.setWrreCodPntReteEndFromBuffer(buffer, position);
        position += WrreCodPntReteEnd.Len.WRRE_COD_PNT_RETE_END;
        wrreFlPntRete1rio = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wrreCodCan.setWrreCodCanFromBuffer(buffer, position);
        position += WrreCodCan.Len.WRRE_COD_CAN;
        wrreDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRRE_DS_RIGA, 0);
        position += Len.WRRE_DS_RIGA;
        wrreDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wrreDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRRE_DS_VER, 0);
        position += Len.WRRE_DS_VER;
        wrreDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRRE_DS_TS_INI_CPTZ, 0);
        position += Len.WRRE_DS_TS_INI_CPTZ;
        wrreDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRRE_DS_TS_END_CPTZ, 0);
        position += Len.WRRE_DS_TS_END_CPTZ;
        wrreDsUtente = MarshalByte.readString(buffer, position, Len.WRRE_DS_UTENTE);
        position += Len.WRRE_DS_UTENTE;
        wrreDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wrreCodPntReteIniC.setWrreCodPntReteIniCFromBuffer(buffer, position);
        position += WrreCodPntReteIniC.Len.WRRE_COD_PNT_RETE_INI_C;
        wrreMatrOprt = MarshalByte.readString(buffer, position, Len.WRRE_MATR_OPRT);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wrreIdRappRete, Len.Int.WRRE_ID_RAPP_RETE, 0);
        position += Len.WRRE_ID_RAPP_RETE;
        wrreIdOgg.getWrreIdOggAsBuffer(buffer, position);
        position += WrreIdOgg.Len.WRRE_ID_OGG;
        MarshalByte.writeString(buffer, position, wrreTpOgg, Len.WRRE_TP_OGG);
        position += Len.WRRE_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, wrreIdMoviCrz, Len.Int.WRRE_ID_MOVI_CRZ, 0);
        position += Len.WRRE_ID_MOVI_CRZ;
        wrreIdMoviChiu.getWrreIdMoviChiuAsBuffer(buffer, position);
        position += WrreIdMoviChiu.Len.WRRE_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wrreDtIniEff, Len.Int.WRRE_DT_INI_EFF, 0);
        position += Len.WRRE_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wrreDtEndEff, Len.Int.WRRE_DT_END_EFF, 0);
        position += Len.WRRE_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wrreCodCompAnia, Len.Int.WRRE_COD_COMP_ANIA, 0);
        position += Len.WRRE_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wrreTpRete, Len.WRRE_TP_RETE);
        position += Len.WRRE_TP_RETE;
        wrreTpAcqsCntrt.getWrreTpAcqsCntrtAsBuffer(buffer, position);
        position += WrreTpAcqsCntrt.Len.WRRE_TP_ACQS_CNTRT;
        wrreCodAcqsCntrt.getWrreCodAcqsCntrtAsBuffer(buffer, position);
        position += WrreCodAcqsCntrt.Len.WRRE_COD_ACQS_CNTRT;
        wrreCodPntReteIni.getWrreCodPntReteIniAsBuffer(buffer, position);
        position += WrreCodPntReteIni.Len.WRRE_COD_PNT_RETE_INI;
        wrreCodPntReteEnd.getWrreCodPntReteEndAsBuffer(buffer, position);
        position += WrreCodPntReteEnd.Len.WRRE_COD_PNT_RETE_END;
        MarshalByte.writeChar(buffer, position, wrreFlPntRete1rio);
        position += Types.CHAR_SIZE;
        wrreCodCan.getWrreCodCanAsBuffer(buffer, position);
        position += WrreCodCan.Len.WRRE_COD_CAN;
        MarshalByte.writeLongAsPacked(buffer, position, wrreDsRiga, Len.Int.WRRE_DS_RIGA, 0);
        position += Len.WRRE_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wrreDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wrreDsVer, Len.Int.WRRE_DS_VER, 0);
        position += Len.WRRE_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wrreDsTsIniCptz, Len.Int.WRRE_DS_TS_INI_CPTZ, 0);
        position += Len.WRRE_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wrreDsTsEndCptz, Len.Int.WRRE_DS_TS_END_CPTZ, 0);
        position += Len.WRRE_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wrreDsUtente, Len.WRRE_DS_UTENTE);
        position += Len.WRRE_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wrreDsStatoElab);
        position += Types.CHAR_SIZE;
        wrreCodPntReteIniC.getWrreCodPntReteIniCAsBuffer(buffer, position);
        position += WrreCodPntReteIniC.Len.WRRE_COD_PNT_RETE_INI_C;
        MarshalByte.writeString(buffer, position, wrreMatrOprt, Len.WRRE_MATR_OPRT);
        return buffer;
    }

    public void initDatiSpaces() {
        wrreIdRappRete = Types.INVALID_INT_VAL;
        wrreIdOgg.initWrreIdOggSpaces();
        wrreTpOgg = "";
        wrreIdMoviCrz = Types.INVALID_INT_VAL;
        wrreIdMoviChiu.initWrreIdMoviChiuSpaces();
        wrreDtIniEff = Types.INVALID_INT_VAL;
        wrreDtEndEff = Types.INVALID_INT_VAL;
        wrreCodCompAnia = Types.INVALID_INT_VAL;
        wrreTpRete = "";
        wrreTpAcqsCntrt.initWrreTpAcqsCntrtSpaces();
        wrreCodAcqsCntrt.initWrreCodAcqsCntrtSpaces();
        wrreCodPntReteIni.initWrreCodPntReteIniSpaces();
        wrreCodPntReteEnd.initWrreCodPntReteEndSpaces();
        wrreFlPntRete1rio = Types.SPACE_CHAR;
        wrreCodCan.initWrreCodCanSpaces();
        wrreDsRiga = Types.INVALID_LONG_VAL;
        wrreDsOperSql = Types.SPACE_CHAR;
        wrreDsVer = Types.INVALID_INT_VAL;
        wrreDsTsIniCptz = Types.INVALID_LONG_VAL;
        wrreDsTsEndCptz = Types.INVALID_LONG_VAL;
        wrreDsUtente = "";
        wrreDsStatoElab = Types.SPACE_CHAR;
        wrreCodPntReteIniC.initWrreCodPntReteIniCSpaces();
        wrreMatrOprt = "";
    }

    public void setWrreIdRappRete(int wrreIdRappRete) {
        this.wrreIdRappRete = wrreIdRappRete;
    }

    public int getWrreIdRappRete() {
        return this.wrreIdRappRete;
    }

    public void setWrreTpOgg(String wrreTpOgg) {
        this.wrreTpOgg = Functions.subString(wrreTpOgg, Len.WRRE_TP_OGG);
    }

    public String getWrreTpOgg() {
        return this.wrreTpOgg;
    }

    public void setWrreIdMoviCrz(int wrreIdMoviCrz) {
        this.wrreIdMoviCrz = wrreIdMoviCrz;
    }

    public int getWrreIdMoviCrz() {
        return this.wrreIdMoviCrz;
    }

    public void setWrreDtIniEff(int wrreDtIniEff) {
        this.wrreDtIniEff = wrreDtIniEff;
    }

    public int getWrreDtIniEff() {
        return this.wrreDtIniEff;
    }

    public void setWrreDtEndEff(int wrreDtEndEff) {
        this.wrreDtEndEff = wrreDtEndEff;
    }

    public int getWrreDtEndEff() {
        return this.wrreDtEndEff;
    }

    public void setWrreCodCompAnia(int wrreCodCompAnia) {
        this.wrreCodCompAnia = wrreCodCompAnia;
    }

    public int getWrreCodCompAnia() {
        return this.wrreCodCompAnia;
    }

    public void setWrreTpRete(String wrreTpRete) {
        this.wrreTpRete = Functions.subString(wrreTpRete, Len.WRRE_TP_RETE);
    }

    public String getWrreTpRete() {
        return this.wrreTpRete;
    }

    public void setWrreFlPntRete1rio(char wrreFlPntRete1rio) {
        this.wrreFlPntRete1rio = wrreFlPntRete1rio;
    }

    public char getWrreFlPntRete1rio() {
        return this.wrreFlPntRete1rio;
    }

    public void setWrreDsRiga(long wrreDsRiga) {
        this.wrreDsRiga = wrreDsRiga;
    }

    public long getWrreDsRiga() {
        return this.wrreDsRiga;
    }

    public void setWrreDsOperSql(char wrreDsOperSql) {
        this.wrreDsOperSql = wrreDsOperSql;
    }

    public char getWrreDsOperSql() {
        return this.wrreDsOperSql;
    }

    public void setWrreDsVer(int wrreDsVer) {
        this.wrreDsVer = wrreDsVer;
    }

    public int getWrreDsVer() {
        return this.wrreDsVer;
    }

    public void setWrreDsTsIniCptz(long wrreDsTsIniCptz) {
        this.wrreDsTsIniCptz = wrreDsTsIniCptz;
    }

    public long getWrreDsTsIniCptz() {
        return this.wrreDsTsIniCptz;
    }

    public void setWrreDsTsEndCptz(long wrreDsTsEndCptz) {
        this.wrreDsTsEndCptz = wrreDsTsEndCptz;
    }

    public long getWrreDsTsEndCptz() {
        return this.wrreDsTsEndCptz;
    }

    public void setWrreDsUtente(String wrreDsUtente) {
        this.wrreDsUtente = Functions.subString(wrreDsUtente, Len.WRRE_DS_UTENTE);
    }

    public String getWrreDsUtente() {
        return this.wrreDsUtente;
    }

    public void setWrreDsStatoElab(char wrreDsStatoElab) {
        this.wrreDsStatoElab = wrreDsStatoElab;
    }

    public char getWrreDsStatoElab() {
        return this.wrreDsStatoElab;
    }

    public void setWrreMatrOprt(String wrreMatrOprt) {
        this.wrreMatrOprt = Functions.subString(wrreMatrOprt, Len.WRRE_MATR_OPRT);
    }

    public String getWrreMatrOprt() {
        return this.wrreMatrOprt;
    }

    public WrreCodAcqsCntrt getWrreCodAcqsCntrt() {
        return wrreCodAcqsCntrt;
    }

    public WrreCodCan getWrreCodCan() {
        return wrreCodCan;
    }

    public WrreCodPntReteEnd getWrreCodPntReteEnd() {
        return wrreCodPntReteEnd;
    }

    public WrreCodPntReteIni getWrreCodPntReteIni() {
        return wrreCodPntReteIni;
    }

    public WrreCodPntReteIniC getWrreCodPntReteIniC() {
        return wrreCodPntReteIniC;
    }

    public WrreIdMoviChiu getWrreIdMoviChiu() {
        return wrreIdMoviChiu;
    }

    public WrreIdOgg getWrreIdOgg() {
        return wrreIdOgg;
    }

    public WrreTpAcqsCntrt getWrreTpAcqsCntrt() {
        return wrreTpAcqsCntrt;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRRE_TP_OGG = 2;
        public static final int WRRE_TP_RETE = 2;
        public static final int WRRE_DS_UTENTE = 20;
        public static final int WRRE_MATR_OPRT = 20;
        public static final int WRRE_ID_RAPP_RETE = 5;
        public static final int WRRE_ID_MOVI_CRZ = 5;
        public static final int WRRE_DT_INI_EFF = 5;
        public static final int WRRE_DT_END_EFF = 5;
        public static final int WRRE_COD_COMP_ANIA = 3;
        public static final int WRRE_FL_PNT_RETE1RIO = 1;
        public static final int WRRE_DS_RIGA = 6;
        public static final int WRRE_DS_OPER_SQL = 1;
        public static final int WRRE_DS_VER = 5;
        public static final int WRRE_DS_TS_INI_CPTZ = 10;
        public static final int WRRE_DS_TS_END_CPTZ = 10;
        public static final int WRRE_DS_STATO_ELAB = 1;
        public static final int DATI = WRRE_ID_RAPP_RETE + WrreIdOgg.Len.WRRE_ID_OGG + WRRE_TP_OGG + WRRE_ID_MOVI_CRZ + WrreIdMoviChiu.Len.WRRE_ID_MOVI_CHIU + WRRE_DT_INI_EFF + WRRE_DT_END_EFF + WRRE_COD_COMP_ANIA + WRRE_TP_RETE + WrreTpAcqsCntrt.Len.WRRE_TP_ACQS_CNTRT + WrreCodAcqsCntrt.Len.WRRE_COD_ACQS_CNTRT + WrreCodPntReteIni.Len.WRRE_COD_PNT_RETE_INI + WrreCodPntReteEnd.Len.WRRE_COD_PNT_RETE_END + WRRE_FL_PNT_RETE1RIO + WrreCodCan.Len.WRRE_COD_CAN + WRRE_DS_RIGA + WRRE_DS_OPER_SQL + WRRE_DS_VER + WRRE_DS_TS_INI_CPTZ + WRRE_DS_TS_END_CPTZ + WRRE_DS_UTENTE + WRRE_DS_STATO_ELAB + WrreCodPntReteIniC.Len.WRRE_COD_PNT_RETE_INI_C + WRRE_MATR_OPRT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRRE_ID_RAPP_RETE = 9;
            public static final int WRRE_ID_MOVI_CRZ = 9;
            public static final int WRRE_DT_INI_EFF = 8;
            public static final int WRRE_DT_END_EFF = 8;
            public static final int WRRE_COD_COMP_ANIA = 5;
            public static final int WRRE_DS_RIGA = 10;
            public static final int WRRE_DS_VER = 9;
            public static final int WRRE_DS_TS_INI_CPTZ = 18;
            public static final int WRRE_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
