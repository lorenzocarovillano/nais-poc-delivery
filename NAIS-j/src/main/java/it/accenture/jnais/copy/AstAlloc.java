package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.AllDtEndVldt;
import it.accenture.jnais.ws.redefines.AllIdMoviChiu;
import it.accenture.jnais.ws.redefines.AllPcRipAst;
import it.accenture.jnais.ws.redefines.AllPeriodo;

/**Original name: AST-ALLOC<br>
 * Variable: AST-ALLOC from copybook IDBVALL1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AstAlloc {

    //==== PROPERTIES ====
    //Original name: ALL-ID-AST-ALLOC
    private int allIdAstAlloc = DefaultValues.INT_VAL;
    //Original name: ALL-ID-STRA-DI-INVST
    private int allIdStraDiInvst = DefaultValues.INT_VAL;
    //Original name: ALL-ID-MOVI-CRZ
    private int allIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: ALL-ID-MOVI-CHIU
    private AllIdMoviChiu allIdMoviChiu = new AllIdMoviChiu();
    //Original name: ALL-DT-INI-VLDT
    private int allDtIniVldt = DefaultValues.INT_VAL;
    //Original name: ALL-DT-END-VLDT
    private AllDtEndVldt allDtEndVldt = new AllDtEndVldt();
    //Original name: ALL-DT-INI-EFF
    private int allDtIniEff = DefaultValues.INT_VAL;
    //Original name: ALL-DT-END-EFF
    private int allDtEndEff = DefaultValues.INT_VAL;
    //Original name: ALL-COD-COMP-ANIA
    private int allCodCompAnia = DefaultValues.INT_VAL;
    //Original name: ALL-COD-FND
    private String allCodFnd = DefaultValues.stringVal(Len.ALL_COD_FND);
    //Original name: ALL-COD-TARI
    private String allCodTari = DefaultValues.stringVal(Len.ALL_COD_TARI);
    //Original name: ALL-TP-APPLZ-AST
    private String allTpApplzAst = DefaultValues.stringVal(Len.ALL_TP_APPLZ_AST);
    //Original name: ALL-PC-RIP-AST
    private AllPcRipAst allPcRipAst = new AllPcRipAst();
    //Original name: ALL-TP-FND
    private char allTpFnd = DefaultValues.CHAR_VAL;
    //Original name: ALL-DS-RIGA
    private long allDsRiga = DefaultValues.LONG_VAL;
    //Original name: ALL-DS-OPER-SQL
    private char allDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: ALL-DS-VER
    private int allDsVer = DefaultValues.INT_VAL;
    //Original name: ALL-DS-TS-INI-CPTZ
    private long allDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: ALL-DS-TS-END-CPTZ
    private long allDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: ALL-DS-UTENTE
    private String allDsUtente = DefaultValues.stringVal(Len.ALL_DS_UTENTE);
    //Original name: ALL-DS-STATO-ELAB
    private char allDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: ALL-PERIODO
    private AllPeriodo allPeriodo = new AllPeriodo();
    //Original name: ALL-TP-RIBIL-FND
    private String allTpRibilFnd = DefaultValues.stringVal(Len.ALL_TP_RIBIL_FND);

    //==== METHODS ====
    public void setAstAllocFormatted(String data) {
        byte[] buffer = new byte[Len.AST_ALLOC];
        MarshalByte.writeString(buffer, 1, data, Len.AST_ALLOC);
        setAstAllocBytes(buffer, 1);
    }

    public String getAstAllocFormatted() {
        return MarshalByteExt.bufferToStr(getAstAllocBytes());
    }

    public byte[] getAstAllocBytes() {
        byte[] buffer = new byte[Len.AST_ALLOC];
        return getAstAllocBytes(buffer, 1);
    }

    public void setAstAllocBytes(byte[] buffer, int offset) {
        int position = offset;
        allIdAstAlloc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_ID_AST_ALLOC, 0);
        position += Len.ALL_ID_AST_ALLOC;
        allIdStraDiInvst = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_ID_STRA_DI_INVST, 0);
        position += Len.ALL_ID_STRA_DI_INVST;
        allIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_ID_MOVI_CRZ, 0);
        position += Len.ALL_ID_MOVI_CRZ;
        allIdMoviChiu.setAllIdMoviChiuFromBuffer(buffer, position);
        position += AllIdMoviChiu.Len.ALL_ID_MOVI_CHIU;
        allDtIniVldt = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_DT_INI_VLDT, 0);
        position += Len.ALL_DT_INI_VLDT;
        allDtEndVldt.setAllDtEndVldtFromBuffer(buffer, position);
        position += AllDtEndVldt.Len.ALL_DT_END_VLDT;
        allDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_DT_INI_EFF, 0);
        position += Len.ALL_DT_INI_EFF;
        allDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_DT_END_EFF, 0);
        position += Len.ALL_DT_END_EFF;
        allCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_COD_COMP_ANIA, 0);
        position += Len.ALL_COD_COMP_ANIA;
        allCodFnd = MarshalByte.readString(buffer, position, Len.ALL_COD_FND);
        position += Len.ALL_COD_FND;
        allCodTari = MarshalByte.readString(buffer, position, Len.ALL_COD_TARI);
        position += Len.ALL_COD_TARI;
        allTpApplzAst = MarshalByte.readString(buffer, position, Len.ALL_TP_APPLZ_AST);
        position += Len.ALL_TP_APPLZ_AST;
        allPcRipAst.setAllPcRipAstFromBuffer(buffer, position);
        position += AllPcRipAst.Len.ALL_PC_RIP_AST;
        allTpFnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        allDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ALL_DS_RIGA, 0);
        position += Len.ALL_DS_RIGA;
        allDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        allDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ALL_DS_VER, 0);
        position += Len.ALL_DS_VER;
        allDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ALL_DS_TS_INI_CPTZ, 0);
        position += Len.ALL_DS_TS_INI_CPTZ;
        allDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ALL_DS_TS_END_CPTZ, 0);
        position += Len.ALL_DS_TS_END_CPTZ;
        allDsUtente = MarshalByte.readString(buffer, position, Len.ALL_DS_UTENTE);
        position += Len.ALL_DS_UTENTE;
        allDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        allPeriodo.setAllPeriodoFromBuffer(buffer, position);
        position += AllPeriodo.Len.ALL_PERIODO;
        allTpRibilFnd = MarshalByte.readString(buffer, position, Len.ALL_TP_RIBIL_FND);
    }

    public byte[] getAstAllocBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, allIdAstAlloc, Len.Int.ALL_ID_AST_ALLOC, 0);
        position += Len.ALL_ID_AST_ALLOC;
        MarshalByte.writeIntAsPacked(buffer, position, allIdStraDiInvst, Len.Int.ALL_ID_STRA_DI_INVST, 0);
        position += Len.ALL_ID_STRA_DI_INVST;
        MarshalByte.writeIntAsPacked(buffer, position, allIdMoviCrz, Len.Int.ALL_ID_MOVI_CRZ, 0);
        position += Len.ALL_ID_MOVI_CRZ;
        allIdMoviChiu.getAllIdMoviChiuAsBuffer(buffer, position);
        position += AllIdMoviChiu.Len.ALL_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, allDtIniVldt, Len.Int.ALL_DT_INI_VLDT, 0);
        position += Len.ALL_DT_INI_VLDT;
        allDtEndVldt.getAllDtEndVldtAsBuffer(buffer, position);
        position += AllDtEndVldt.Len.ALL_DT_END_VLDT;
        MarshalByte.writeIntAsPacked(buffer, position, allDtIniEff, Len.Int.ALL_DT_INI_EFF, 0);
        position += Len.ALL_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, allDtEndEff, Len.Int.ALL_DT_END_EFF, 0);
        position += Len.ALL_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, allCodCompAnia, Len.Int.ALL_COD_COMP_ANIA, 0);
        position += Len.ALL_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, allCodFnd, Len.ALL_COD_FND);
        position += Len.ALL_COD_FND;
        MarshalByte.writeString(buffer, position, allCodTari, Len.ALL_COD_TARI);
        position += Len.ALL_COD_TARI;
        MarshalByte.writeString(buffer, position, allTpApplzAst, Len.ALL_TP_APPLZ_AST);
        position += Len.ALL_TP_APPLZ_AST;
        allPcRipAst.getAllPcRipAstAsBuffer(buffer, position);
        position += AllPcRipAst.Len.ALL_PC_RIP_AST;
        MarshalByte.writeChar(buffer, position, allTpFnd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, allDsRiga, Len.Int.ALL_DS_RIGA, 0);
        position += Len.ALL_DS_RIGA;
        MarshalByte.writeChar(buffer, position, allDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, allDsVer, Len.Int.ALL_DS_VER, 0);
        position += Len.ALL_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, allDsTsIniCptz, Len.Int.ALL_DS_TS_INI_CPTZ, 0);
        position += Len.ALL_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, allDsTsEndCptz, Len.Int.ALL_DS_TS_END_CPTZ, 0);
        position += Len.ALL_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, allDsUtente, Len.ALL_DS_UTENTE);
        position += Len.ALL_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, allDsStatoElab);
        position += Types.CHAR_SIZE;
        allPeriodo.getAllPeriodoAsBuffer(buffer, position);
        position += AllPeriodo.Len.ALL_PERIODO;
        MarshalByte.writeString(buffer, position, allTpRibilFnd, Len.ALL_TP_RIBIL_FND);
        return buffer;
    }

    public void setAllIdAstAlloc(int allIdAstAlloc) {
        this.allIdAstAlloc = allIdAstAlloc;
    }

    public int getAllIdAstAlloc() {
        return this.allIdAstAlloc;
    }

    public void setAllIdStraDiInvst(int allIdStraDiInvst) {
        this.allIdStraDiInvst = allIdStraDiInvst;
    }

    public int getAllIdStraDiInvst() {
        return this.allIdStraDiInvst;
    }

    public void setAllIdMoviCrz(int allIdMoviCrz) {
        this.allIdMoviCrz = allIdMoviCrz;
    }

    public int getAllIdMoviCrz() {
        return this.allIdMoviCrz;
    }

    public void setAllDtIniVldt(int allDtIniVldt) {
        this.allDtIniVldt = allDtIniVldt;
    }

    public int getAllDtIniVldt() {
        return this.allDtIniVldt;
    }

    public void setAllDtIniEff(int allDtIniEff) {
        this.allDtIniEff = allDtIniEff;
    }

    public int getAllDtIniEff() {
        return this.allDtIniEff;
    }

    public void setAllDtEndEff(int allDtEndEff) {
        this.allDtEndEff = allDtEndEff;
    }

    public int getAllDtEndEff() {
        return this.allDtEndEff;
    }

    public void setAllCodCompAnia(int allCodCompAnia) {
        this.allCodCompAnia = allCodCompAnia;
    }

    public int getAllCodCompAnia() {
        return this.allCodCompAnia;
    }

    public void setAllCodFnd(String allCodFnd) {
        this.allCodFnd = Functions.subString(allCodFnd, Len.ALL_COD_FND);
    }

    public String getAllCodFnd() {
        return this.allCodFnd;
    }

    public String getAllCodFndFormatted() {
        return Functions.padBlanks(getAllCodFnd(), Len.ALL_COD_FND);
    }

    public void setAllCodTari(String allCodTari) {
        this.allCodTari = Functions.subString(allCodTari, Len.ALL_COD_TARI);
    }

    public String getAllCodTari() {
        return this.allCodTari;
    }

    public String getAllCodTariFormatted() {
        return Functions.padBlanks(getAllCodTari(), Len.ALL_COD_TARI);
    }

    public void setAllTpApplzAst(String allTpApplzAst) {
        this.allTpApplzAst = Functions.subString(allTpApplzAst, Len.ALL_TP_APPLZ_AST);
    }

    public String getAllTpApplzAst() {
        return this.allTpApplzAst;
    }

    public String getAllTpApplzAstFormatted() {
        return Functions.padBlanks(getAllTpApplzAst(), Len.ALL_TP_APPLZ_AST);
    }

    public void setAllTpFnd(char allTpFnd) {
        this.allTpFnd = allTpFnd;
    }

    public char getAllTpFnd() {
        return this.allTpFnd;
    }

    public void setAllDsRiga(long allDsRiga) {
        this.allDsRiga = allDsRiga;
    }

    public long getAllDsRiga() {
        return this.allDsRiga;
    }

    public void setAllDsOperSql(char allDsOperSql) {
        this.allDsOperSql = allDsOperSql;
    }

    public char getAllDsOperSql() {
        return this.allDsOperSql;
    }

    public void setAllDsVer(int allDsVer) {
        this.allDsVer = allDsVer;
    }

    public int getAllDsVer() {
        return this.allDsVer;
    }

    public void setAllDsTsIniCptz(long allDsTsIniCptz) {
        this.allDsTsIniCptz = allDsTsIniCptz;
    }

    public long getAllDsTsIniCptz() {
        return this.allDsTsIniCptz;
    }

    public void setAllDsTsEndCptz(long allDsTsEndCptz) {
        this.allDsTsEndCptz = allDsTsEndCptz;
    }

    public long getAllDsTsEndCptz() {
        return this.allDsTsEndCptz;
    }

    public void setAllDsUtente(String allDsUtente) {
        this.allDsUtente = Functions.subString(allDsUtente, Len.ALL_DS_UTENTE);
    }

    public String getAllDsUtente() {
        return this.allDsUtente;
    }

    public void setAllDsStatoElab(char allDsStatoElab) {
        this.allDsStatoElab = allDsStatoElab;
    }

    public char getAllDsStatoElab() {
        return this.allDsStatoElab;
    }

    public void setAllTpRibilFnd(String allTpRibilFnd) {
        this.allTpRibilFnd = Functions.subString(allTpRibilFnd, Len.ALL_TP_RIBIL_FND);
    }

    public String getAllTpRibilFnd() {
        return this.allTpRibilFnd;
    }

    public String getAllTpRibilFndFormatted() {
        return Functions.padBlanks(getAllTpRibilFnd(), Len.ALL_TP_RIBIL_FND);
    }

    public AllDtEndVldt getAllDtEndVldt() {
        return allDtEndVldt;
    }

    public AllIdMoviChiu getAllIdMoviChiu() {
        return allIdMoviChiu;
    }

    public AllPcRipAst getAllPcRipAst() {
        return allPcRipAst;
    }

    public AllPeriodo getAllPeriodo() {
        return allPeriodo;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ALL_COD_FND = 20;
        public static final int ALL_COD_TARI = 12;
        public static final int ALL_TP_APPLZ_AST = 2;
        public static final int ALL_DS_UTENTE = 20;
        public static final int ALL_TP_RIBIL_FND = 2;
        public static final int ALL_ID_AST_ALLOC = 5;
        public static final int ALL_ID_STRA_DI_INVST = 5;
        public static final int ALL_ID_MOVI_CRZ = 5;
        public static final int ALL_DT_INI_VLDT = 5;
        public static final int ALL_DT_INI_EFF = 5;
        public static final int ALL_DT_END_EFF = 5;
        public static final int ALL_COD_COMP_ANIA = 3;
        public static final int ALL_TP_FND = 1;
        public static final int ALL_DS_RIGA = 6;
        public static final int ALL_DS_OPER_SQL = 1;
        public static final int ALL_DS_VER = 5;
        public static final int ALL_DS_TS_INI_CPTZ = 10;
        public static final int ALL_DS_TS_END_CPTZ = 10;
        public static final int ALL_DS_STATO_ELAB = 1;
        public static final int AST_ALLOC = ALL_ID_AST_ALLOC + ALL_ID_STRA_DI_INVST + ALL_ID_MOVI_CRZ + AllIdMoviChiu.Len.ALL_ID_MOVI_CHIU + ALL_DT_INI_VLDT + AllDtEndVldt.Len.ALL_DT_END_VLDT + ALL_DT_INI_EFF + ALL_DT_END_EFF + ALL_COD_COMP_ANIA + ALL_COD_FND + ALL_COD_TARI + ALL_TP_APPLZ_AST + AllPcRipAst.Len.ALL_PC_RIP_AST + ALL_TP_FND + ALL_DS_RIGA + ALL_DS_OPER_SQL + ALL_DS_VER + ALL_DS_TS_INI_CPTZ + ALL_DS_TS_END_CPTZ + ALL_DS_UTENTE + ALL_DS_STATO_ELAB + AllPeriodo.Len.ALL_PERIODO + ALL_TP_RIBIL_FND;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ALL_ID_AST_ALLOC = 9;
            public static final int ALL_ID_STRA_DI_INVST = 9;
            public static final int ALL_ID_MOVI_CRZ = 9;
            public static final int ALL_DT_INI_VLDT = 8;
            public static final int ALL_DT_INI_EFF = 8;
            public static final int ALL_DT_END_EFF = 8;
            public static final int ALL_COD_COMP_ANIA = 5;
            public static final int ALL_DS_RIGA = 10;
            public static final int ALL_DS_VER = 9;
            public static final int ALL_DS_TS_INI_CPTZ = 18;
            public static final int ALL_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
