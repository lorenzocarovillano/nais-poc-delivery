package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: PARAM-DI-CALC-DB<br>
 * Variable: PARAM-DI-CALC-DB from copybook IDBVPCA3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ParamDiCalcDb {

    //==== PROPERTIES ====
    //Original name: PCA-DT-INI-EFF-DB
    private String dtIniEffDb = DefaultValues.stringVal(Len.DT_INI_EFF_DB);
    //Original name: PCA-DT-END-EFF-DB
    private String dtEndEffDb = DefaultValues.stringVal(Len.DT_END_EFF_DB);
    //Original name: PCA-VAL-DT-DB
    private String valDtDb = DefaultValues.stringVal(Len.VAL_DT_DB);

    //==== METHODS ====
    public void setDtIniEffDb(String dtIniEffDb) {
        this.dtIniEffDb = Functions.subString(dtIniEffDb, Len.DT_INI_EFF_DB);
    }

    public String getDtIniEffDb() {
        return this.dtIniEffDb;
    }

    public void setDtEndEffDb(String dtEndEffDb) {
        this.dtEndEffDb = Functions.subString(dtEndEffDb, Len.DT_END_EFF_DB);
    }

    public String getDtEndEffDb() {
        return this.dtEndEffDb;
    }

    public void setValDtDb(String valDtDb) {
        this.valDtDb = Functions.subString(valDtDb, Len.VAL_DT_DB);
    }

    public String getValDtDb() {
        return this.valDtDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_INI_EFF_DB = 10;
        public static final int DT_END_EFF_DB = 10;
        public static final int VAL_DT_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
