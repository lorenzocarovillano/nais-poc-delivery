package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVDTR1<br>
 * Variable: LCCVDTR1 from copybook LCCVDTR1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvdtr1 {

    //==== PROPERTIES ====
    /**Original name: WDTR-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA DETT_TIT_DI_RAT
	 *    ALIAS DTR
	 *    ULTIMO AGG. 28 NOV 2014
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WDTR-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WDTR-DATI
    private WdtrDati dati = new WdtrDati();

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WdtrDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }
}
