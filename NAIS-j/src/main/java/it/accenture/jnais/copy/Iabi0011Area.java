package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Iabi0011AccessoBatch;
import it.accenture.jnais.ws.enums.Iabi0011AppendDataGates;
import it.accenture.jnais.ws.enums.Iabi0011FlagSimulazione;
import it.accenture.jnais.ws.enums.Iabi0011FlagStorEsecuz;
import it.accenture.jnais.ws.enums.Iabi0011ForzRc04;
import it.accenture.jnais.ws.enums.Iabi0011GestioneRipartenza;
import it.accenture.jnais.ws.enums.Iabi0011LengthDataGates;
import it.accenture.jnais.ws.enums.Iabi0011TemporaryTable;
import it.accenture.jnais.ws.enums.Iabi0011TipoOrderBy;
import it.accenture.jnais.ws.enums.Iabi0011TrattamentoCommit;
import it.accenture.jnais.ws.enums.Idsv0001LivelloDebug;
import it.accenture.jnais.ws.enums.Lccc006TrattDati;

/**Original name: IABI0011-AREA<br>
 * Variable: IABI0011-AREA from copybook IABI0011<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Iabi0011Area {

    //==== PROPERTIES ====
    public static final int ADDRESS_TYPE_MAXOCCURS = 5;
    //Original name: IABI0011-PROTOCOL
    private String protocol = DefaultValues.stringVal(Len.PROTOCOL);
    //Original name: IABI0011-PROG-PROTOCOL
    private String progProtocol = DefaultValues.stringVal(Len.PROG_PROTOCOL);
    //Original name: IABI0011-TIPO-BATCH
    private String tipoBatch = DefaultValues.stringVal(Len.TIPO_BATCH);
    //Original name: IABI0011-CODICE-COMPAGNIA-ANIA
    private String codiceCompagniaAnia = DefaultValues.stringVal(Len.CODICE_COMPAGNIA_ANIA);
    //Original name: IABI0011-LINGUA
    private String lingua = DefaultValues.stringVal(Len.LINGUA);
    //Original name: IABI0011-PAESE
    private String paese = DefaultValues.stringVal(Len.PAESE);
    //Original name: IABI0011-USER-NAME
    private String userName = DefaultValues.stringVal(Len.USER_NAME);
    //Original name: IABI0011-GRUPPO-AUTORIZZANTE
    private String gruppoAutorizzante = DefaultValues.stringVal(Len.GRUPPO_AUTORIZZANTE);
    //Original name: IABI0011-CANALE-VENDITA
    private String canaleVendita = DefaultValues.stringVal(Len.CANALE_VENDITA);
    //Original name: IABI0011-PUNTO-VENDITA
    private String puntoVendita = DefaultValues.stringVal(Len.PUNTO_VENDITA);
    //Original name: IABI0011-KEY-BUSINESS1
    private String keyBusiness1 = DefaultValues.stringVal(Len.KEY_BUSINESS1);
    //Original name: IABI0011-KEY-BUSINESS2
    private String keyBusiness2 = DefaultValues.stringVal(Len.KEY_BUSINESS2);
    //Original name: IABI0011-KEY-BUSINESS3
    private String keyBusiness3 = DefaultValues.stringVal(Len.KEY_BUSINESS3);
    //Original name: IABI0011-KEY-BUSINESS4
    private String keyBusiness4 = DefaultValues.stringVal(Len.KEY_BUSINESS4);
    //Original name: IABI0011-KEY-BUSINESS5
    private String keyBusiness5 = DefaultValues.stringVal(Len.KEY_BUSINESS5);
    //Original name: IABI0011-MACROFUNZIONALITA
    private String macrofunzionalita = DefaultValues.stringVal(Len.MACROFUNZIONALITA);
    //Original name: IABI0011-TIPO-MOVIMENTO
    private String tipoMovimento = DefaultValues.stringVal(Len.TIPO_MOVIMENTO);
    //Original name: IABI0011-DATA-EFFETTO
    private String dataEffetto = DefaultValues.stringVal(Len.DATA_EFFETTO);
    //Original name: IABI0011-TS-COMPETENZA
    private String tsCompetenza = DefaultValues.stringVal(Len.TS_COMPETENZA);
    //Original name: IABI0011-FLAG-STOR-ESECUZ
    private Iabi0011FlagStorEsecuz flagStorEsecuz = new Iabi0011FlagStorEsecuz();
    //Original name: IABI0011-FLAG-SIMULAZIONE
    private Iabi0011FlagSimulazione flagSimulazione = new Iabi0011FlagSimulazione();
    //Original name: IABI0011-TRATTAMENTO-COMMIT
    private Iabi0011TrattamentoCommit trattamentoCommit = new Iabi0011TrattamentoCommit();
    //Original name: IABI0011-GESTIONE-RIPARTENZA
    private Iabi0011GestioneRipartenza gestioneRipartenza = new Iabi0011GestioneRipartenza();
    //Original name: IABI0011-TIPO-ORDER-BY
    private Iabi0011TipoOrderBy tipoOrderBy = new Iabi0011TipoOrderBy();
    //Original name: IABI0011-LIVELLO-GRAVITA-LOG
    private Lccc006TrattDati livelloGravitaLog = new Lccc006TrattDati();
    //Original name: IABI0011-LIVELLO-DEBUG
    private Idsv0001LivelloDebug livelloDebug = new Idsv0001LivelloDebug();
    //Original name: IABI0011-TEMPORARY-TABLE
    private Iabi0011TemporaryTable temporaryTable = new Iabi0011TemporaryTable();
    //Original name: IABI0011-LENGTH-DATA-GATES
    private Iabi0011LengthDataGates lengthDataGates = new Iabi0011LengthDataGates();
    //Original name: IABI0011-APPEND-DATA-GATES
    private Iabi0011AppendDataGates appendDataGates = new Iabi0011AppendDataGates();
    //Original name: IABI0011-ADDRESS-TYPE
    private char[] addressType = new char[ADDRESS_TYPE_MAXOCCURS];
    //Original name: IABI0011-FORZ-RC-04
    private Iabi0011ForzRc04 forzRc04 = new Iabi0011ForzRc04();
    //Original name: IABI0011-CONTATORE
    private String contatore = DefaultValues.stringVal(Len.CONTATORE);
    //Original name: IABI0011-ACCESSO-BATCH
    private Iabi0011AccessoBatch accessoBatch = new Iabi0011AccessoBatch();
    //Original name: IABI0011-BUFFER-WHERE-COND
    private String bufferWhereCond = DefaultValues.stringVal(Len.BUFFER_WHERE_COND);

    //==== CONSTRUCTORS ====
    public Iabi0011Area() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int addressTypeIdx = 1; addressTypeIdx <= ADDRESS_TYPE_MAXOCCURS; addressTypeIdx++) {
            setAddressType(addressTypeIdx, DefaultValues.CHAR_VAL);
        }
    }

    public void setIabi0011AreaFormatted(String data) {
        byte[] buffer = new byte[Len.IABI0011_AREA];
        MarshalByte.writeString(buffer, 1, data, Len.IABI0011_AREA);
        setIabi0011AreaBytes(buffer, 1);
    }

    public String getIabi0011AreaFormatted() {
        return MarshalByteExt.bufferToStr(getIabi0011AreaBytes());
    }

    public byte[] getIabi0011AreaBytes() {
        byte[] buffer = new byte[Len.IABI0011_AREA];
        return getIabi0011AreaBytes(buffer, 1);
    }

    public void setIabi0011AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        protocol = MarshalByte.readString(buffer, position, Len.PROTOCOL);
        position += Len.PROTOCOL;
        progProtocol = MarshalByte.readFixedString(buffer, position, Len.PROG_PROTOCOL);
        position += Len.PROG_PROTOCOL;
        tipoBatch = MarshalByte.readFixedString(buffer, position, Len.TIPO_BATCH);
        position += Len.TIPO_BATCH;
        codiceCompagniaAnia = MarshalByte.readFixedString(buffer, position, Len.CODICE_COMPAGNIA_ANIA);
        position += Len.CODICE_COMPAGNIA_ANIA;
        lingua = MarshalByte.readString(buffer, position, Len.LINGUA);
        position += Len.LINGUA;
        paese = MarshalByte.readString(buffer, position, Len.PAESE);
        position += Len.PAESE;
        userName = MarshalByte.readString(buffer, position, Len.USER_NAME);
        position += Len.USER_NAME;
        gruppoAutorizzante = MarshalByte.readFixedString(buffer, position, Len.GRUPPO_AUTORIZZANTE);
        position += Len.GRUPPO_AUTORIZZANTE;
        canaleVendita = MarshalByte.readFixedString(buffer, position, Len.CANALE_VENDITA);
        position += Len.CANALE_VENDITA;
        puntoVendita = MarshalByte.readString(buffer, position, Len.PUNTO_VENDITA);
        position += Len.PUNTO_VENDITA;
        keyBusiness1 = MarshalByte.readString(buffer, position, Len.KEY_BUSINESS1);
        position += Len.KEY_BUSINESS1;
        keyBusiness2 = MarshalByte.readString(buffer, position, Len.KEY_BUSINESS2);
        position += Len.KEY_BUSINESS2;
        keyBusiness3 = MarshalByte.readString(buffer, position, Len.KEY_BUSINESS3);
        position += Len.KEY_BUSINESS3;
        keyBusiness4 = MarshalByte.readString(buffer, position, Len.KEY_BUSINESS4);
        position += Len.KEY_BUSINESS4;
        keyBusiness5 = MarshalByte.readString(buffer, position, Len.KEY_BUSINESS5);
        position += Len.KEY_BUSINESS5;
        macrofunzionalita = MarshalByte.readString(buffer, position, Len.MACROFUNZIONALITA);
        position += Len.MACROFUNZIONALITA;
        tipoMovimento = MarshalByte.readFixedString(buffer, position, Len.TIPO_MOVIMENTO);
        position += Len.TIPO_MOVIMENTO;
        dataEffetto = MarshalByte.readFixedString(buffer, position, Len.DATA_EFFETTO);
        position += Len.DATA_EFFETTO;
        tsCompetenza = MarshalByte.readFixedString(buffer, position, Len.TS_COMPETENZA);
        position += Len.TS_COMPETENZA;
        flagStorEsecuz.setFlagStorEsecuz(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        flagSimulazione.setFlagSimulazione(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        trattamentoCommit.setTrattamentoCommit(MarshalByte.readString(buffer, position, Iabi0011TrattamentoCommit.Len.TRATTAMENTO_COMMIT));
        position += Iabi0011TrattamentoCommit.Len.TRATTAMENTO_COMMIT;
        gestioneRipartenza.setGestioneRipartenza(MarshalByte.readString(buffer, position, Iabi0011GestioneRipartenza.Len.GESTIONE_RIPARTENZA));
        position += Iabi0011GestioneRipartenza.Len.GESTIONE_RIPARTENZA;
        tipoOrderBy.setTipoOrderBy(MarshalByte.readString(buffer, position, Iabi0011TipoOrderBy.Len.TIPO_ORDER_BY));
        position += Iabi0011TipoOrderBy.Len.TIPO_ORDER_BY;
        livelloGravitaLog.setFlagModCalcPreP(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        livelloDebug.value = MarshalByte.readFixedString(buffer, position, Idsv0001LivelloDebug.Len.LIVELLO_DEBUG);
        position += Idsv0001LivelloDebug.Len.LIVELLO_DEBUG;
        temporaryTable.setTemporaryTable(MarshalByte.readString(buffer, position, Iabi0011TemporaryTable.Len.TEMPORARY_TABLE));
        position += Iabi0011TemporaryTable.Len.TEMPORARY_TABLE;
        lengthDataGates.setLengthDataGates(MarshalByte.readString(buffer, position, Iabi0011LengthDataGates.Len.LENGTH_DATA_GATES));
        position += Iabi0011LengthDataGates.Len.LENGTH_DATA_GATES;
        appendDataGates.setAppendDataGates(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        setTabAddressBytes(buffer, position);
        position += Len.TAB_ADDRESS;
        forzRc04.setForzRc04(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        contatore = MarshalByte.readFixedString(buffer, position, Len.CONTATORE);
        position += Len.CONTATORE;
        accessoBatch.setAccessoBatch(MarshalByte.readString(buffer, position, Iabi0011AccessoBatch.Len.ACCESSO_BATCH));
        position += Iabi0011AccessoBatch.Len.ACCESSO_BATCH;
        bufferWhereCond = MarshalByte.readString(buffer, position, Len.BUFFER_WHERE_COND);
    }

    public byte[] getIabi0011AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, protocol, Len.PROTOCOL);
        position += Len.PROTOCOL;
        MarshalByte.writeString(buffer, position, progProtocol, Len.PROG_PROTOCOL);
        position += Len.PROG_PROTOCOL;
        MarshalByte.writeString(buffer, position, tipoBatch, Len.TIPO_BATCH);
        position += Len.TIPO_BATCH;
        MarshalByte.writeString(buffer, position, codiceCompagniaAnia, Len.CODICE_COMPAGNIA_ANIA);
        position += Len.CODICE_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, lingua, Len.LINGUA);
        position += Len.LINGUA;
        MarshalByte.writeString(buffer, position, paese, Len.PAESE);
        position += Len.PAESE;
        MarshalByte.writeString(buffer, position, userName, Len.USER_NAME);
        position += Len.USER_NAME;
        MarshalByte.writeString(buffer, position, gruppoAutorizzante, Len.GRUPPO_AUTORIZZANTE);
        position += Len.GRUPPO_AUTORIZZANTE;
        MarshalByte.writeString(buffer, position, canaleVendita, Len.CANALE_VENDITA);
        position += Len.CANALE_VENDITA;
        MarshalByte.writeString(buffer, position, puntoVendita, Len.PUNTO_VENDITA);
        position += Len.PUNTO_VENDITA;
        MarshalByte.writeString(buffer, position, keyBusiness1, Len.KEY_BUSINESS1);
        position += Len.KEY_BUSINESS1;
        MarshalByte.writeString(buffer, position, keyBusiness2, Len.KEY_BUSINESS2);
        position += Len.KEY_BUSINESS2;
        MarshalByte.writeString(buffer, position, keyBusiness3, Len.KEY_BUSINESS3);
        position += Len.KEY_BUSINESS3;
        MarshalByte.writeString(buffer, position, keyBusiness4, Len.KEY_BUSINESS4);
        position += Len.KEY_BUSINESS4;
        MarshalByte.writeString(buffer, position, keyBusiness5, Len.KEY_BUSINESS5);
        position += Len.KEY_BUSINESS5;
        MarshalByte.writeString(buffer, position, macrofunzionalita, Len.MACROFUNZIONALITA);
        position += Len.MACROFUNZIONALITA;
        MarshalByte.writeString(buffer, position, tipoMovimento, Len.TIPO_MOVIMENTO);
        position += Len.TIPO_MOVIMENTO;
        MarshalByte.writeString(buffer, position, dataEffetto, Len.DATA_EFFETTO);
        position += Len.DATA_EFFETTO;
        MarshalByte.writeString(buffer, position, tsCompetenza, Len.TS_COMPETENZA);
        position += Len.TS_COMPETENZA;
        MarshalByte.writeChar(buffer, position, flagStorEsecuz.getFlagStorEsecuz());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flagSimulazione.getFlagSimulazione());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, trattamentoCommit.getTrattamentoCommit(), Iabi0011TrattamentoCommit.Len.TRATTAMENTO_COMMIT);
        position += Iabi0011TrattamentoCommit.Len.TRATTAMENTO_COMMIT;
        MarshalByte.writeString(buffer, position, gestioneRipartenza.getGestioneRipartenza(), Iabi0011GestioneRipartenza.Len.GESTIONE_RIPARTENZA);
        position += Iabi0011GestioneRipartenza.Len.GESTIONE_RIPARTENZA;
        MarshalByte.writeString(buffer, position, tipoOrderBy.getTipoOrderBy(), Iabi0011TipoOrderBy.Len.TIPO_ORDER_BY);
        position += Iabi0011TipoOrderBy.Len.TIPO_ORDER_BY;
        MarshalByte.writeChar(buffer, position, livelloGravitaLog.getFlagModCalcPreP());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, livelloDebug.value, Idsv0001LivelloDebug.Len.LIVELLO_DEBUG);
        position += Idsv0001LivelloDebug.Len.LIVELLO_DEBUG;
        MarshalByte.writeString(buffer, position, temporaryTable.getTemporaryTable(), Iabi0011TemporaryTable.Len.TEMPORARY_TABLE);
        position += Iabi0011TemporaryTable.Len.TEMPORARY_TABLE;
        MarshalByte.writeString(buffer, position, lengthDataGates.getLengthDataGates(), Iabi0011LengthDataGates.Len.LENGTH_DATA_GATES);
        position += Iabi0011LengthDataGates.Len.LENGTH_DATA_GATES;
        MarshalByte.writeChar(buffer, position, appendDataGates.getAppendDataGates());
        position += Types.CHAR_SIZE;
        getTabAddressBytes(buffer, position);
        position += Len.TAB_ADDRESS;
        MarshalByte.writeChar(buffer, position, forzRc04.getForzRc04());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, contatore, Len.CONTATORE);
        position += Len.CONTATORE;
        MarshalByte.writeString(buffer, position, accessoBatch.getAccessoBatch(), Iabi0011AccessoBatch.Len.ACCESSO_BATCH);
        position += Iabi0011AccessoBatch.Len.ACCESSO_BATCH;
        MarshalByte.writeString(buffer, position, bufferWhereCond, Len.BUFFER_WHERE_COND);
        return buffer;
    }

    public void setProtocol(String protocol) {
        this.protocol = Functions.subString(protocol, Len.PROTOCOL);
    }

    public String getProtocol() {
        return this.protocol;
    }

    public void setProgProtocolFormatted(String progProtocol) {
        this.progProtocol = Trunc.toUnsignedNumeric(progProtocol, Len.PROG_PROTOCOL);
    }

    public int getProgProtocol() {
        return NumericDisplay.asInt(this.progProtocol);
    }

    public String getProgProtocolFormatted() {
        return this.progProtocol;
    }

    public void setTipoBatchFormatted(String tipoBatch) {
        this.tipoBatch = Trunc.toUnsignedNumeric(tipoBatch, Len.TIPO_BATCH);
    }

    public int getTipoBatch() {
        return NumericDisplay.asInt(this.tipoBatch);
    }

    public String getTipoBatchFormatted() {
        return this.tipoBatch;
    }

    public void setCodiceCompagniaAniaFormatted(String codiceCompagniaAnia) {
        this.codiceCompagniaAnia = Trunc.toUnsignedNumeric(codiceCompagniaAnia, Len.CODICE_COMPAGNIA_ANIA);
    }

    public int getCodiceCompagniaAnia() {
        return NumericDisplay.asInt(this.codiceCompagniaAnia);
    }

    public String getCodiceCompagniaAniaFormatted() {
        return this.codiceCompagniaAnia;
    }

    public void setLingua(String lingua) {
        this.lingua = Functions.subString(lingua, Len.LINGUA);
    }

    public String getLingua() {
        return this.lingua;
    }

    public String getLinguaFormatted() {
        return Functions.padBlanks(getLingua(), Len.LINGUA);
    }

    public void setPaese(String paese) {
        this.paese = Functions.subString(paese, Len.PAESE);
    }

    public String getPaese() {
        return this.paese;
    }

    public String getPaeseFormatted() {
        return Functions.padBlanks(getPaese(), Len.PAESE);
    }

    public void setUserName(String userName) {
        this.userName = Functions.subString(userName, Len.USER_NAME);
    }

    public String getUserName() {
        return this.userName;
    }

    public String getUserNameFormatted() {
        return Functions.padBlanks(getUserName(), Len.USER_NAME);
    }

    public void setGruppoAutorizzanteFormatted(String gruppoAutorizzante) {
        this.gruppoAutorizzante = Trunc.toUnsignedNumeric(gruppoAutorizzante, Len.GRUPPO_AUTORIZZANTE);
    }

    public long getGruppoAutorizzante() {
        return NumericDisplay.asLong(this.gruppoAutorizzante);
    }

    public String getGruppoAutorizzanteFormatted() {
        return this.gruppoAutorizzante;
    }

    public void setCanaleVenditaFormatted(String canaleVendita) {
        this.canaleVendita = Trunc.toUnsignedNumeric(canaleVendita, Len.CANALE_VENDITA);
    }

    public int getCanaleVendita() {
        return NumericDisplay.asInt(this.canaleVendita);
    }

    public String getCanaleVenditaFormatted() {
        return this.canaleVendita;
    }

    public void setPuntoVendita(String puntoVendita) {
        this.puntoVendita = Functions.subString(puntoVendita, Len.PUNTO_VENDITA);
    }

    public String getPuntoVendita() {
        return this.puntoVendita;
    }

    public String getPuntoVenditaFormatted() {
        return Functions.padBlanks(getPuntoVendita(), Len.PUNTO_VENDITA);
    }

    public void setKeyBusiness1(String keyBusiness1) {
        this.keyBusiness1 = Functions.subString(keyBusiness1, Len.KEY_BUSINESS1);
    }

    public String getKeyBusiness1() {
        return this.keyBusiness1;
    }

    public void setKeyBusiness2(String keyBusiness2) {
        this.keyBusiness2 = Functions.subString(keyBusiness2, Len.KEY_BUSINESS2);
    }

    public String getKeyBusiness2() {
        return this.keyBusiness2;
    }

    public void setKeyBusiness3(String keyBusiness3) {
        this.keyBusiness3 = Functions.subString(keyBusiness3, Len.KEY_BUSINESS3);
    }

    public String getKeyBusiness3() {
        return this.keyBusiness3;
    }

    public void setKeyBusiness4(String keyBusiness4) {
        this.keyBusiness4 = Functions.subString(keyBusiness4, Len.KEY_BUSINESS4);
    }

    public String getKeyBusiness4() {
        return this.keyBusiness4;
    }

    public void setKeyBusiness5(String keyBusiness5) {
        this.keyBusiness5 = Functions.subString(keyBusiness5, Len.KEY_BUSINESS5);
    }

    public String getKeyBusiness5() {
        return this.keyBusiness5;
    }

    public void setMacrofunzionalita(String macrofunzionalita) {
        this.macrofunzionalita = Functions.subString(macrofunzionalita, Len.MACROFUNZIONALITA);
    }

    public String getMacrofunzionalita() {
        return this.macrofunzionalita;
    }

    public String getMacrofunzionalitaFormatted() {
        return Functions.padBlanks(getMacrofunzionalita(), Len.MACROFUNZIONALITA);
    }

    public void setTipoMovimentoFormatted(String tipoMovimento) {
        this.tipoMovimento = Trunc.toUnsignedNumeric(tipoMovimento, Len.TIPO_MOVIMENTO);
    }

    public int getTipoMovimento() {
        return NumericDisplay.asInt(this.tipoMovimento);
    }

    public String getTipoMovimentoFormatted() {
        return this.tipoMovimento;
    }

    public void setDataEffettoFormatted(String dataEffetto) {
        this.dataEffetto = Trunc.toUnsignedNumeric(dataEffetto, Len.DATA_EFFETTO);
    }

    public int getDataEffetto() {
        return NumericDisplay.asInt(this.dataEffetto);
    }

    public String getDataEffettoFormatted() {
        return this.dataEffetto;
    }

    public void setTsCompetenza(long tsCompetenza) {
        this.tsCompetenza = NumericDisplay.asString(tsCompetenza, Len.TS_COMPETENZA);
    }

    public long getTsCompetenza() {
        return NumericDisplay.asLong(this.tsCompetenza);
    }

    public String getTsCompetenzaFormatted() {
        return this.tsCompetenza;
    }

    public void setTabAddressBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ADDRESS_TYPE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                setAddressType(idx, MarshalByte.readChar(buffer, position));
                position += Types.CHAR_SIZE;
            }
            else {
                setAddressType(idx, Types.SPACE_CHAR);
                position += Types.CHAR_SIZE;
            }
        }
    }

    public byte[] getTabAddressBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ADDRESS_TYPE_MAXOCCURS; idx++) {
            MarshalByte.writeChar(buffer, position, getAddressType(idx));
            position += Types.CHAR_SIZE;
        }
        return buffer;
    }

    public void setAddressType(int addressTypeIdx, char addressType) {
        this.addressType[addressTypeIdx - 1] = addressType;
    }

    public char getAddressType(int addressTypeIdx) {
        return this.addressType[addressTypeIdx - 1];
    }

    public void setContatoreFormatted(String contatore) {
        this.contatore = Trunc.toUnsignedNumeric(contatore, Len.CONTATORE);
    }

    public int getContatore() {
        return NumericDisplay.asInt(this.contatore);
    }

    public String getContatoreFormatted() {
        return this.contatore;
    }

    public void setBufferWhereCond(String bufferWhereCond) {
        this.bufferWhereCond = Functions.subString(bufferWhereCond, Len.BUFFER_WHERE_COND);
    }

    public void setBufferWhereCondSubstring(String replacement, int start, int length) {
        bufferWhereCond = Functions.setSubstring(bufferWhereCond, replacement, start, length);
    }

    public String getBufferWhereCond() {
        return this.bufferWhereCond;
    }

    public String getBufferWhereCondFormatted() {
        return Functions.padBlanks(getBufferWhereCond(), Len.BUFFER_WHERE_COND);
    }

    public Iabi0011AccessoBatch getAccessoBatch() {
        return accessoBatch;
    }

    public Iabi0011AppendDataGates getAppendDataGates() {
        return appendDataGates;
    }

    public Iabi0011FlagSimulazione getFlagSimulazione() {
        return flagSimulazione;
    }

    public Iabi0011FlagStorEsecuz getFlagStorEsecuz() {
        return flagStorEsecuz;
    }

    public Iabi0011ForzRc04 getForzRc04() {
        return forzRc04;
    }

    public Iabi0011GestioneRipartenza getGestioneRipartenza() {
        return gestioneRipartenza;
    }

    public Iabi0011LengthDataGates getLengthDataGates() {
        return lengthDataGates;
    }

    public Idsv0001LivelloDebug getLivelloDebug() {
        return livelloDebug;
    }

    public Lccc006TrattDati getLivelloGravitaLog() {
        return livelloGravitaLog;
    }

    public Iabi0011TemporaryTable getTemporaryTable() {
        return temporaryTable;
    }

    public Iabi0011TipoOrderBy getTipoOrderBy() {
        return tipoOrderBy;
    }

    public Iabi0011TrattamentoCommit getTrattamentoCommit() {
        return trattamentoCommit;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PROTOCOL = 50;
        public static final int PROG_PROTOCOL = 9;
        public static final int TIPO_BATCH = 9;
        public static final int CODICE_COMPAGNIA_ANIA = 5;
        public static final int LINGUA = 2;
        public static final int PAESE = 3;
        public static final int USER_NAME = 20;
        public static final int GRUPPO_AUTORIZZANTE = 10;
        public static final int CANALE_VENDITA = 5;
        public static final int PUNTO_VENDITA = 6;
        public static final int KEY_BUSINESS1 = 20;
        public static final int KEY_BUSINESS2 = 20;
        public static final int KEY_BUSINESS3 = 20;
        public static final int KEY_BUSINESS4 = 20;
        public static final int KEY_BUSINESS5 = 20;
        public static final int MACROFUNZIONALITA = 2;
        public static final int TIPO_MOVIMENTO = 5;
        public static final int DATA_EFFETTO = 8;
        public static final int TS_COMPETENZA = 18;
        public static final int CONTATORE = 5;
        public static final int BUFFER_WHERE_COND = 300;
        public static final int ADDRESS_TYPE = 1;
        public static final int TAB_ADDRESS = Iabi0011Area.ADDRESS_TYPE_MAXOCCURS * ADDRESS_TYPE;
        public static final int IABI0011_AREA = PROTOCOL + PROG_PROTOCOL + TIPO_BATCH + CODICE_COMPAGNIA_ANIA + LINGUA + PAESE + USER_NAME + GRUPPO_AUTORIZZANTE + CANALE_VENDITA + PUNTO_VENDITA + KEY_BUSINESS1 + KEY_BUSINESS2 + KEY_BUSINESS3 + KEY_BUSINESS4 + KEY_BUSINESS5 + MACROFUNZIONALITA + TIPO_MOVIMENTO + DATA_EFFETTO + TS_COMPETENZA + Iabi0011FlagStorEsecuz.Len.FLAG_STOR_ESECUZ + Iabi0011FlagSimulazione.Len.FLAG_SIMULAZIONE + Iabi0011TrattamentoCommit.Len.TRATTAMENTO_COMMIT + Iabi0011GestioneRipartenza.Len.GESTIONE_RIPARTENZA + Iabi0011TipoOrderBy.Len.TIPO_ORDER_BY + Lccc006TrattDati.Len.FLAG_MOD_CALC_PRE_P + Idsv0001LivelloDebug.Len.LIVELLO_DEBUG + Iabi0011TemporaryTable.Len.TEMPORARY_TABLE + Iabi0011LengthDataGates.Len.LENGTH_DATA_GATES + Iabi0011AppendDataGates.Len.APPEND_DATA_GATES + TAB_ADDRESS + Iabi0011ForzRc04.Len.FORZ_RC04 + CONTATORE + Iabi0011AccessoBatch.Len.ACCESSO_BATCH + BUFFER_WHERE_COND;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
