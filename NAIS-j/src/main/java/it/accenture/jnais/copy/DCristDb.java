package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: D-CRIST-DB<br>
 * Variable: D-CRIST-DB from copybook IDBVP613<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DCristDb {

    //==== PROPERTIES ====
    //Original name: P61-DT-INI-EFF-DB
    private String iniEffDb = DefaultValues.stringVal(Len.INI_EFF_DB);
    //Original name: P61-DT-END-EFF-DB
    private String endEffDb = DefaultValues.stringVal(Len.END_EFF_DB);
    //Original name: P61-DT-DECOR-DB
    private String decorDb = DefaultValues.stringVal(Len.DECOR_DB);

    //==== METHODS ====
    public void setIniEffDb(String iniEffDb) {
        this.iniEffDb = Functions.subString(iniEffDb, Len.INI_EFF_DB);
    }

    public String getIniEffDb() {
        return this.iniEffDb;
    }

    public void setEndEffDb(String endEffDb) {
        this.endEffDb = Functions.subString(endEffDb, Len.END_EFF_DB);
    }

    public String getEndEffDb() {
        return this.endEffDb;
    }

    public void setDecorDb(String decorDb) {
        this.decorDb = Functions.subString(decorDb, Len.DECOR_DB);
    }

    public String getDecorDb() {
        return this.decorDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INI_EFF_DB = 10;
        public static final int END_EFF_DB = 10;
        public static final int DECOR_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
