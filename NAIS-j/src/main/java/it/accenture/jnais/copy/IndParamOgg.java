package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-PARAM-OGG<br>
 * Variable: IND-PARAM-OGG from copybook IDBVPOG2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndParamOgg {

    //==== PROPERTIES ====
    //Original name: IND-POG-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-POG-COD-PARAM
    private short codParam = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-POG-TP-PARAM
    private short tpParam = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-POG-TP-D
    private short tpD = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-POG-VAL-IMP
    private short valImp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-POG-VAL-DT
    private short valDt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-POG-VAL-TS
    private short valTs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-POG-VAL-TXT
    private short valTxt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-POG-VAL-FL
    private short valFl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-POG-VAL-NUM
    private short valNum = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-POG-VAL-PC
    private short valPc = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setCodParam(short codParam) {
        this.codParam = codParam;
    }

    public short getCodParam() {
        return this.codParam;
    }

    public void setTpParam(short tpParam) {
        this.tpParam = tpParam;
    }

    public short getTpParam() {
        return this.tpParam;
    }

    public void setTpD(short tpD) {
        this.tpD = tpD;
    }

    public short getTpD() {
        return this.tpD;
    }

    public void setValImp(short valImp) {
        this.valImp = valImp;
    }

    public short getValImp() {
        return this.valImp;
    }

    public void setValDt(short valDt) {
        this.valDt = valDt;
    }

    public short getValDt() {
        return this.valDt;
    }

    public void setValTs(short valTs) {
        this.valTs = valTs;
    }

    public short getValTs() {
        return this.valTs;
    }

    public void setValTxt(short valTxt) {
        this.valTxt = valTxt;
    }

    public short getValTxt() {
        return this.valTxt;
    }

    public void setValFl(short valFl) {
        this.valFl = valFl;
    }

    public short getValFl() {
        return this.valFl;
    }

    public void setValNum(short valNum) {
        this.valNum = valNum;
    }

    public short getValNum() {
        return this.valNum;
    }

    public void setValPc(short valPc) {
        this.valPc = valPc;
    }

    public short getValPc() {
        return this.valPc;
    }
}
