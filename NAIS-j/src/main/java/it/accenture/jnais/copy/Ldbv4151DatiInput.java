package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV4151-DATI-INPUT<br>
 * Variable: LDBV4151-DATI-INPUT from copybook LDBV4151<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbv4151DatiInput {

    //==== PROPERTIES ====
    //Original name: LDBV4151-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV4151-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV4151-TP-STAT-TIT
    private String tpStatTit = DefaultValues.stringVal(Len.TP_STAT_TIT);
    //Original name: LDBV4151-DATA-INIZIO-PERIODO
    private int dataInizioPeriodo = DefaultValues.INT_VAL;
    //Original name: LDBV4151-DATA-FINE-PERIODO
    private int dataFinePeriodo = DefaultValues.INT_VAL;
    //Original name: LDBV4151-DATA-INIZ-PERIODO-DB
    private String dataInizPeriodoDb = DefaultValues.stringVal(Len.DATA_INIZ_PERIODO_DB);
    //Original name: LDBV4151-DATA-FINE-PERIODO-DB
    private String dataFinePeriodoDb = DefaultValues.stringVal(Len.DATA_FINE_PERIODO_DB);

    //==== METHODS ====
    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpStatTit = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT);
        position += Len.TP_STAT_TIT;
        dataInizioPeriodo = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DATA_INIZIO_PERIODO, 0);
        position += Len.DATA_INIZIO_PERIODO;
        dataFinePeriodo = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DATA_FINE_PERIODO, 0);
        position += Len.DATA_FINE_PERIODO;
        dataInizPeriodoDb = MarshalByte.readString(buffer, position, Len.DATA_INIZ_PERIODO_DB);
        position += Len.DATA_INIZ_PERIODO_DB;
        dataFinePeriodoDb = MarshalByte.readString(buffer, position, Len.DATA_FINE_PERIODO_DB);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpStatTit, Len.TP_STAT_TIT);
        position += Len.TP_STAT_TIT;
        MarshalByte.writeIntAsPacked(buffer, position, dataInizioPeriodo, Len.Int.DATA_INIZIO_PERIODO, 0);
        position += Len.DATA_INIZIO_PERIODO;
        MarshalByte.writeIntAsPacked(buffer, position, dataFinePeriodo, Len.Int.DATA_FINE_PERIODO, 0);
        position += Len.DATA_FINE_PERIODO;
        MarshalByte.writeString(buffer, position, dataInizPeriodoDb, Len.DATA_INIZ_PERIODO_DB);
        position += Len.DATA_INIZ_PERIODO_DB;
        MarshalByte.writeString(buffer, position, dataFinePeriodoDb, Len.DATA_FINE_PERIODO_DB);
        return buffer;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpStatTit(String tpStatTit) {
        this.tpStatTit = Functions.subString(tpStatTit, Len.TP_STAT_TIT);
    }

    public String getTpStatTit() {
        return this.tpStatTit;
    }

    public void setDataInizioPeriodo(int dataInizioPeriodo) {
        this.dataInizioPeriodo = dataInizioPeriodo;
    }

    public int getDataInizioPeriodo() {
        return this.dataInizioPeriodo;
    }

    public void setDataFinePeriodo(int dataFinePeriodo) {
        this.dataFinePeriodo = dataFinePeriodo;
    }

    public int getDataFinePeriodo() {
        return this.dataFinePeriodo;
    }

    public void setDataInizPeriodoDb(String dataInizPeriodoDb) {
        this.dataInizPeriodoDb = Functions.subString(dataInizPeriodoDb, Len.DATA_INIZ_PERIODO_DB);
    }

    public String getDataInizPeriodoDb() {
        return this.dataInizPeriodoDb;
    }

    public void setDataFinePeriodoDb(String dataFinePeriodoDb) {
        this.dataFinePeriodoDb = Functions.subString(dataFinePeriodoDb, Len.DATA_FINE_PERIODO_DB);
    }

    public String getDataFinePeriodoDb() {
        return this.dataFinePeriodoDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int ID_OGG = 5;
        public static final int TP_STAT_TIT = 2;
        public static final int DATA_INIZIO_PERIODO = 5;
        public static final int DATA_FINE_PERIODO = 5;
        public static final int DATA_INIZ_PERIODO_DB = 10;
        public static final int DATA_FINE_PERIODO_DB = 10;
        public static final int DATI_INPUT = TP_OGG + ID_OGG + TP_STAT_TIT + DATA_INIZIO_PERIODO + DATA_FINE_PERIODO + DATA_INIZ_PERIODO_DB + DATA_FINE_PERIODO_DB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int DATA_INIZIO_PERIODO = 8;
            public static final int DATA_FINE_PERIODO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
