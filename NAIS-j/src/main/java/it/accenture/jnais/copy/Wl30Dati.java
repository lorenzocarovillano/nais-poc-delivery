package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Wl30CodSubAge;
import it.accenture.jnais.ws.redefines.Wl30DtDecorPoliLq;
import it.accenture.jnais.ws.redefines.Wl30IdMoviChiu;
import it.accenture.jnais.ws.redefines.Wl30TpInvstLiq;

/**Original name: WL30-DATI<br>
 * Variable: WL30-DATI from copybook LCCVL301<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wl30Dati {

    //==== PROPERTIES ====
    //Original name: WL30-ID-REINVST-POLI-LQ
    private int wl30IdReinvstPoliLq = DefaultValues.INT_VAL;
    //Original name: WL30-ID-MOVI-CRZ
    private int wl30IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WL30-ID-MOVI-CHIU
    private Wl30IdMoviChiu wl30IdMoviChiu = new Wl30IdMoviChiu();
    //Original name: WL30-DT-INI-EFF
    private int wl30DtIniEff = DefaultValues.INT_VAL;
    //Original name: WL30-DT-END-EFF
    private int wl30DtEndEff = DefaultValues.INT_VAL;
    //Original name: WL30-COD-COMP-ANIA
    private int wl30CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WL30-COD-RAMO
    private String wl30CodRamo = DefaultValues.stringVal(Len.WL30_COD_RAMO);
    //Original name: WL30-IB-POLI
    private String wl30IbPoli = DefaultValues.stringVal(Len.WL30_IB_POLI);
    //Original name: WL30-PR-KEY-SIST-ESTNO
    private String wl30PrKeySistEstno = DefaultValues.stringVal(Len.WL30_PR_KEY_SIST_ESTNO);
    //Original name: WL30-SEC-KEY-SIST-ESTNO
    private String wl30SecKeySistEstno = DefaultValues.stringVal(Len.WL30_SEC_KEY_SIST_ESTNO);
    //Original name: WL30-COD-CAN
    private int wl30CodCan = DefaultValues.INT_VAL;
    //Original name: WL30-COD-AGE
    private int wl30CodAge = DefaultValues.INT_VAL;
    //Original name: WL30-COD-SUB-AGE
    private Wl30CodSubAge wl30CodSubAge = new Wl30CodSubAge();
    //Original name: WL30-IMP-RES
    private AfDecimal wl30ImpRes = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WL30-TP-LIQ
    private String wl30TpLiq = DefaultValues.stringVal(Len.WL30_TP_LIQ);
    //Original name: WL30-TP-SIST-ESTNO
    private String wl30TpSistEstno = DefaultValues.stringVal(Len.WL30_TP_SIST_ESTNO);
    //Original name: WL30-COD-FISC-PART-IVA
    private String wl30CodFiscPartIva = DefaultValues.stringVal(Len.WL30_COD_FISC_PART_IVA);
    //Original name: WL30-COD-MOVI-LIQ
    private int wl30CodMoviLiq = DefaultValues.INT_VAL;
    //Original name: WL30-TP-INVST-LIQ
    private Wl30TpInvstLiq wl30TpInvstLiq = new Wl30TpInvstLiq();
    //Original name: WL30-IMP-TOT-LIQ
    private AfDecimal wl30ImpTotLiq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WL30-DS-RIGA
    private long wl30DsRiga = DefaultValues.LONG_VAL;
    //Original name: WL30-DS-OPER-SQL
    private char wl30DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WL30-DS-VER
    private int wl30DsVer = DefaultValues.INT_VAL;
    //Original name: WL30-DS-TS-INI-CPTZ
    private long wl30DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WL30-DS-TS-END-CPTZ
    private long wl30DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WL30-DS-UTENTE
    private String wl30DsUtente = DefaultValues.stringVal(Len.WL30_DS_UTENTE);
    //Original name: WL30-DS-STATO-ELAB
    private char wl30DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WL30-DT-DECOR-POLI-LQ
    private Wl30DtDecorPoliLq wl30DtDecorPoliLq = new Wl30DtDecorPoliLq();

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wl30IdReinvstPoliLq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL30_ID_REINVST_POLI_LQ, 0);
        position += Len.WL30_ID_REINVST_POLI_LQ;
        wl30IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL30_ID_MOVI_CRZ, 0);
        position += Len.WL30_ID_MOVI_CRZ;
        wl30IdMoviChiu.setWl30IdMoviChiuFromBuffer(buffer, position);
        position += Wl30IdMoviChiu.Len.WL30_ID_MOVI_CHIU;
        wl30DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL30_DT_INI_EFF, 0);
        position += Len.WL30_DT_INI_EFF;
        wl30DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL30_DT_END_EFF, 0);
        position += Len.WL30_DT_END_EFF;
        wl30CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL30_COD_COMP_ANIA, 0);
        position += Len.WL30_COD_COMP_ANIA;
        wl30CodRamo = MarshalByte.readString(buffer, position, Len.WL30_COD_RAMO);
        position += Len.WL30_COD_RAMO;
        wl30IbPoli = MarshalByte.readString(buffer, position, Len.WL30_IB_POLI);
        position += Len.WL30_IB_POLI;
        wl30PrKeySistEstno = MarshalByte.readString(buffer, position, Len.WL30_PR_KEY_SIST_ESTNO);
        position += Len.WL30_PR_KEY_SIST_ESTNO;
        wl30SecKeySistEstno = MarshalByte.readString(buffer, position, Len.WL30_SEC_KEY_SIST_ESTNO);
        position += Len.WL30_SEC_KEY_SIST_ESTNO;
        wl30CodCan = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL30_COD_CAN, 0);
        position += Len.WL30_COD_CAN;
        wl30CodAge = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL30_COD_AGE, 0);
        position += Len.WL30_COD_AGE;
        wl30CodSubAge.setWl30CodSubAgeFromBuffer(buffer, position);
        position += Wl30CodSubAge.Len.WL30_COD_SUB_AGE;
        wl30ImpRes.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WL30_IMP_RES, Len.Fract.WL30_IMP_RES));
        position += Len.WL30_IMP_RES;
        wl30TpLiq = MarshalByte.readString(buffer, position, Len.WL30_TP_LIQ);
        position += Len.WL30_TP_LIQ;
        wl30TpSistEstno = MarshalByte.readString(buffer, position, Len.WL30_TP_SIST_ESTNO);
        position += Len.WL30_TP_SIST_ESTNO;
        wl30CodFiscPartIva = MarshalByte.readString(buffer, position, Len.WL30_COD_FISC_PART_IVA);
        position += Len.WL30_COD_FISC_PART_IVA;
        wl30CodMoviLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL30_COD_MOVI_LIQ, 0);
        position += Len.WL30_COD_MOVI_LIQ;
        wl30TpInvstLiq.setWl30TpInvstLiqFromBuffer(buffer, position);
        position += Wl30TpInvstLiq.Len.WL30_TP_INVST_LIQ;
        wl30ImpTotLiq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WL30_IMP_TOT_LIQ, Len.Fract.WL30_IMP_TOT_LIQ));
        position += Len.WL30_IMP_TOT_LIQ;
        wl30DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WL30_DS_RIGA, 0);
        position += Len.WL30_DS_RIGA;
        wl30DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wl30DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL30_DS_VER, 0);
        position += Len.WL30_DS_VER;
        wl30DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WL30_DS_TS_INI_CPTZ, 0);
        position += Len.WL30_DS_TS_INI_CPTZ;
        wl30DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WL30_DS_TS_END_CPTZ, 0);
        position += Len.WL30_DS_TS_END_CPTZ;
        wl30DsUtente = MarshalByte.readString(buffer, position, Len.WL30_DS_UTENTE);
        position += Len.WL30_DS_UTENTE;
        wl30DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wl30DtDecorPoliLq.setWl30DtDecorPoliLqFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wl30IdReinvstPoliLq, Len.Int.WL30_ID_REINVST_POLI_LQ, 0);
        position += Len.WL30_ID_REINVST_POLI_LQ;
        MarshalByte.writeIntAsPacked(buffer, position, wl30IdMoviCrz, Len.Int.WL30_ID_MOVI_CRZ, 0);
        position += Len.WL30_ID_MOVI_CRZ;
        wl30IdMoviChiu.getWl30IdMoviChiuAsBuffer(buffer, position);
        position += Wl30IdMoviChiu.Len.WL30_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wl30DtIniEff, Len.Int.WL30_DT_INI_EFF, 0);
        position += Len.WL30_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wl30DtEndEff, Len.Int.WL30_DT_END_EFF, 0);
        position += Len.WL30_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wl30CodCompAnia, Len.Int.WL30_COD_COMP_ANIA, 0);
        position += Len.WL30_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wl30CodRamo, Len.WL30_COD_RAMO);
        position += Len.WL30_COD_RAMO;
        MarshalByte.writeString(buffer, position, wl30IbPoli, Len.WL30_IB_POLI);
        position += Len.WL30_IB_POLI;
        MarshalByte.writeString(buffer, position, wl30PrKeySistEstno, Len.WL30_PR_KEY_SIST_ESTNO);
        position += Len.WL30_PR_KEY_SIST_ESTNO;
        MarshalByte.writeString(buffer, position, wl30SecKeySistEstno, Len.WL30_SEC_KEY_SIST_ESTNO);
        position += Len.WL30_SEC_KEY_SIST_ESTNO;
        MarshalByte.writeIntAsPacked(buffer, position, wl30CodCan, Len.Int.WL30_COD_CAN, 0);
        position += Len.WL30_COD_CAN;
        MarshalByte.writeIntAsPacked(buffer, position, wl30CodAge, Len.Int.WL30_COD_AGE, 0);
        position += Len.WL30_COD_AGE;
        wl30CodSubAge.getWl30CodSubAgeAsBuffer(buffer, position);
        position += Wl30CodSubAge.Len.WL30_COD_SUB_AGE;
        MarshalByte.writeDecimalAsPacked(buffer, position, wl30ImpRes.copy());
        position += Len.WL30_IMP_RES;
        MarshalByte.writeString(buffer, position, wl30TpLiq, Len.WL30_TP_LIQ);
        position += Len.WL30_TP_LIQ;
        MarshalByte.writeString(buffer, position, wl30TpSistEstno, Len.WL30_TP_SIST_ESTNO);
        position += Len.WL30_TP_SIST_ESTNO;
        MarshalByte.writeString(buffer, position, wl30CodFiscPartIva, Len.WL30_COD_FISC_PART_IVA);
        position += Len.WL30_COD_FISC_PART_IVA;
        MarshalByte.writeIntAsPacked(buffer, position, wl30CodMoviLiq, Len.Int.WL30_COD_MOVI_LIQ, 0);
        position += Len.WL30_COD_MOVI_LIQ;
        wl30TpInvstLiq.getWl30TpInvstLiqAsBuffer(buffer, position);
        position += Wl30TpInvstLiq.Len.WL30_TP_INVST_LIQ;
        MarshalByte.writeDecimalAsPacked(buffer, position, wl30ImpTotLiq.copy());
        position += Len.WL30_IMP_TOT_LIQ;
        MarshalByte.writeLongAsPacked(buffer, position, wl30DsRiga, Len.Int.WL30_DS_RIGA, 0);
        position += Len.WL30_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wl30DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wl30DsVer, Len.Int.WL30_DS_VER, 0);
        position += Len.WL30_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wl30DsTsIniCptz, Len.Int.WL30_DS_TS_INI_CPTZ, 0);
        position += Len.WL30_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wl30DsTsEndCptz, Len.Int.WL30_DS_TS_END_CPTZ, 0);
        position += Len.WL30_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wl30DsUtente, Len.WL30_DS_UTENTE);
        position += Len.WL30_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wl30DsStatoElab);
        position += Types.CHAR_SIZE;
        wl30DtDecorPoliLq.getWl30DtDecorPoliLqAsBuffer(buffer, position);
        return buffer;
    }

    public void initDatiSpaces() {
        wl30IdReinvstPoliLq = Types.INVALID_INT_VAL;
        wl30IdMoviCrz = Types.INVALID_INT_VAL;
        wl30IdMoviChiu.initWl30IdMoviChiuSpaces();
        wl30DtIniEff = Types.INVALID_INT_VAL;
        wl30DtEndEff = Types.INVALID_INT_VAL;
        wl30CodCompAnia = Types.INVALID_INT_VAL;
        wl30CodRamo = "";
        wl30IbPoli = "";
        wl30PrKeySistEstno = "";
        wl30SecKeySistEstno = "";
        wl30CodCan = Types.INVALID_INT_VAL;
        wl30CodAge = Types.INVALID_INT_VAL;
        wl30CodSubAge.initWl30CodSubAgeSpaces();
        wl30ImpRes.setNaN();
        wl30TpLiq = "";
        wl30TpSistEstno = "";
        wl30CodFiscPartIva = "";
        wl30CodMoviLiq = Types.INVALID_INT_VAL;
        wl30TpInvstLiq.initWl30TpInvstLiqSpaces();
        wl30ImpTotLiq.setNaN();
        wl30DsRiga = Types.INVALID_LONG_VAL;
        wl30DsOperSql = Types.SPACE_CHAR;
        wl30DsVer = Types.INVALID_INT_VAL;
        wl30DsTsIniCptz = Types.INVALID_LONG_VAL;
        wl30DsTsEndCptz = Types.INVALID_LONG_VAL;
        wl30DsUtente = "";
        wl30DsStatoElab = Types.SPACE_CHAR;
        wl30DtDecorPoliLq.initWl30DtDecorPoliLqSpaces();
    }

    public void setWl30IdReinvstPoliLq(int wl30IdReinvstPoliLq) {
        this.wl30IdReinvstPoliLq = wl30IdReinvstPoliLq;
    }

    public int getWl30IdReinvstPoliLq() {
        return this.wl30IdReinvstPoliLq;
    }

    public void setWl30IdMoviCrz(int wl30IdMoviCrz) {
        this.wl30IdMoviCrz = wl30IdMoviCrz;
    }

    public int getWl30IdMoviCrz() {
        return this.wl30IdMoviCrz;
    }

    public void setWl30DtIniEff(int wl30DtIniEff) {
        this.wl30DtIniEff = wl30DtIniEff;
    }

    public int getWl30DtIniEff() {
        return this.wl30DtIniEff;
    }

    public void setWl30DtEndEff(int wl30DtEndEff) {
        this.wl30DtEndEff = wl30DtEndEff;
    }

    public int getWl30DtEndEff() {
        return this.wl30DtEndEff;
    }

    public void setWl30CodCompAnia(int wl30CodCompAnia) {
        this.wl30CodCompAnia = wl30CodCompAnia;
    }

    public int getWl30CodCompAnia() {
        return this.wl30CodCompAnia;
    }

    public void setWl30CodRamo(String wl30CodRamo) {
        this.wl30CodRamo = Functions.subString(wl30CodRamo, Len.WL30_COD_RAMO);
    }

    public String getWl30CodRamo() {
        return this.wl30CodRamo;
    }

    public void setWl30IbPoli(String wl30IbPoli) {
        this.wl30IbPoli = Functions.subString(wl30IbPoli, Len.WL30_IB_POLI);
    }

    public String getWl30IbPoli() {
        return this.wl30IbPoli;
    }

    public void setWl30PrKeySistEstno(String wl30PrKeySistEstno) {
        this.wl30PrKeySistEstno = Functions.subString(wl30PrKeySistEstno, Len.WL30_PR_KEY_SIST_ESTNO);
    }

    public String getWl30PrKeySistEstno() {
        return this.wl30PrKeySistEstno;
    }

    public void setWl30SecKeySistEstno(String wl30SecKeySistEstno) {
        this.wl30SecKeySistEstno = Functions.subString(wl30SecKeySistEstno, Len.WL30_SEC_KEY_SIST_ESTNO);
    }

    public String getWl30SecKeySistEstno() {
        return this.wl30SecKeySistEstno;
    }

    public void setWl30CodCan(int wl30CodCan) {
        this.wl30CodCan = wl30CodCan;
    }

    public int getWl30CodCan() {
        return this.wl30CodCan;
    }

    public void setWl30CodAge(int wl30CodAge) {
        this.wl30CodAge = wl30CodAge;
    }

    public int getWl30CodAge() {
        return this.wl30CodAge;
    }

    public void setWl30ImpRes(AfDecimal wl30ImpRes) {
        this.wl30ImpRes.assign(wl30ImpRes);
    }

    public AfDecimal getWl30ImpRes() {
        return this.wl30ImpRes.copy();
    }

    public void setWl30TpLiq(String wl30TpLiq) {
        this.wl30TpLiq = Functions.subString(wl30TpLiq, Len.WL30_TP_LIQ);
    }

    public String getWl30TpLiq() {
        return this.wl30TpLiq;
    }

    public void setWl30TpSistEstno(String wl30TpSistEstno) {
        this.wl30TpSistEstno = Functions.subString(wl30TpSistEstno, Len.WL30_TP_SIST_ESTNO);
    }

    public String getWl30TpSistEstno() {
        return this.wl30TpSistEstno;
    }

    public void setWl30CodFiscPartIva(String wl30CodFiscPartIva) {
        this.wl30CodFiscPartIva = Functions.subString(wl30CodFiscPartIva, Len.WL30_COD_FISC_PART_IVA);
    }

    public String getWl30CodFiscPartIva() {
        return this.wl30CodFiscPartIva;
    }

    public void setWl30CodMoviLiq(int wl30CodMoviLiq) {
        this.wl30CodMoviLiq = wl30CodMoviLiq;
    }

    public int getWl30CodMoviLiq() {
        return this.wl30CodMoviLiq;
    }

    public void setWl30ImpTotLiq(AfDecimal wl30ImpTotLiq) {
        this.wl30ImpTotLiq.assign(wl30ImpTotLiq);
    }

    public AfDecimal getWl30ImpTotLiq() {
        return this.wl30ImpTotLiq.copy();
    }

    public void setWl30DsRiga(long wl30DsRiga) {
        this.wl30DsRiga = wl30DsRiga;
    }

    public long getWl30DsRiga() {
        return this.wl30DsRiga;
    }

    public void setWl30DsOperSql(char wl30DsOperSql) {
        this.wl30DsOperSql = wl30DsOperSql;
    }

    public char getWl30DsOperSql() {
        return this.wl30DsOperSql;
    }

    public void setWl30DsVer(int wl30DsVer) {
        this.wl30DsVer = wl30DsVer;
    }

    public int getWl30DsVer() {
        return this.wl30DsVer;
    }

    public void setWl30DsTsIniCptz(long wl30DsTsIniCptz) {
        this.wl30DsTsIniCptz = wl30DsTsIniCptz;
    }

    public long getWl30DsTsIniCptz() {
        return this.wl30DsTsIniCptz;
    }

    public void setWl30DsTsEndCptz(long wl30DsTsEndCptz) {
        this.wl30DsTsEndCptz = wl30DsTsEndCptz;
    }

    public long getWl30DsTsEndCptz() {
        return this.wl30DsTsEndCptz;
    }

    public void setWl30DsUtente(String wl30DsUtente) {
        this.wl30DsUtente = Functions.subString(wl30DsUtente, Len.WL30_DS_UTENTE);
    }

    public String getWl30DsUtente() {
        return this.wl30DsUtente;
    }

    public void setWl30DsStatoElab(char wl30DsStatoElab) {
        this.wl30DsStatoElab = wl30DsStatoElab;
    }

    public char getWl30DsStatoElab() {
        return this.wl30DsStatoElab;
    }

    public Wl30CodSubAge getWl30CodSubAge() {
        return wl30CodSubAge;
    }

    public Wl30DtDecorPoliLq getWl30DtDecorPoliLq() {
        return wl30DtDecorPoliLq;
    }

    public Wl30IdMoviChiu getWl30IdMoviChiu() {
        return wl30IdMoviChiu;
    }

    public Wl30TpInvstLiq getWl30TpInvstLiq() {
        return wl30TpInvstLiq;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WL30_ID_REINVST_POLI_LQ = 5;
        public static final int WL30_ID_MOVI_CRZ = 5;
        public static final int WL30_DT_INI_EFF = 5;
        public static final int WL30_DT_END_EFF = 5;
        public static final int WL30_COD_COMP_ANIA = 3;
        public static final int WL30_COD_RAMO = 12;
        public static final int WL30_IB_POLI = 40;
        public static final int WL30_PR_KEY_SIST_ESTNO = 40;
        public static final int WL30_SEC_KEY_SIST_ESTNO = 40;
        public static final int WL30_COD_CAN = 3;
        public static final int WL30_COD_AGE = 3;
        public static final int WL30_IMP_RES = 8;
        public static final int WL30_TP_LIQ = 2;
        public static final int WL30_TP_SIST_ESTNO = 2;
        public static final int WL30_COD_FISC_PART_IVA = 16;
        public static final int WL30_COD_MOVI_LIQ = 3;
        public static final int WL30_IMP_TOT_LIQ = 8;
        public static final int WL30_DS_RIGA = 6;
        public static final int WL30_DS_OPER_SQL = 1;
        public static final int WL30_DS_VER = 5;
        public static final int WL30_DS_TS_INI_CPTZ = 10;
        public static final int WL30_DS_TS_END_CPTZ = 10;
        public static final int WL30_DS_UTENTE = 20;
        public static final int WL30_DS_STATO_ELAB = 1;
        public static final int DATI = WL30_ID_REINVST_POLI_LQ + WL30_ID_MOVI_CRZ + Wl30IdMoviChiu.Len.WL30_ID_MOVI_CHIU + WL30_DT_INI_EFF + WL30_DT_END_EFF + WL30_COD_COMP_ANIA + WL30_COD_RAMO + WL30_IB_POLI + WL30_PR_KEY_SIST_ESTNO + WL30_SEC_KEY_SIST_ESTNO + WL30_COD_CAN + WL30_COD_AGE + Wl30CodSubAge.Len.WL30_COD_SUB_AGE + WL30_IMP_RES + WL30_TP_LIQ + WL30_TP_SIST_ESTNO + WL30_COD_FISC_PART_IVA + WL30_COD_MOVI_LIQ + Wl30TpInvstLiq.Len.WL30_TP_INVST_LIQ + WL30_IMP_TOT_LIQ + WL30_DS_RIGA + WL30_DS_OPER_SQL + WL30_DS_VER + WL30_DS_TS_INI_CPTZ + WL30_DS_TS_END_CPTZ + WL30_DS_UTENTE + WL30_DS_STATO_ELAB + Wl30DtDecorPoliLq.Len.WL30_DT_DECOR_POLI_LQ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL30_ID_REINVST_POLI_LQ = 9;
            public static final int WL30_ID_MOVI_CRZ = 9;
            public static final int WL30_DT_INI_EFF = 8;
            public static final int WL30_DT_END_EFF = 8;
            public static final int WL30_COD_COMP_ANIA = 5;
            public static final int WL30_COD_CAN = 5;
            public static final int WL30_COD_AGE = 5;
            public static final int WL30_IMP_RES = 12;
            public static final int WL30_COD_MOVI_LIQ = 5;
            public static final int WL30_IMP_TOT_LIQ = 12;
            public static final int WL30_DS_RIGA = 10;
            public static final int WL30_DS_VER = 9;
            public static final int WL30_DS_TS_INI_CPTZ = 18;
            public static final int WL30_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WL30_IMP_RES = 3;
            public static final int WL30_IMP_TOT_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
