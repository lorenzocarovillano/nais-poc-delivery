package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCVB048<br>
 * Variable: LCCVB048 from copybook LCCVB048<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvb048 {

    //==== PROPERTIES ====
    //Original name: W-B04-LEN-TOT
    private short lenTot = DefaultValues.BIN_SHORT_VAL;
    //Original name: W-B04-REC-FISSO
    private WB04RecFisso recFisso = new WB04RecFisso();
    //Original name: W-B04-AREA-D-VALOR-VAR-IND
    private char areaDValorVarInd = DefaultValues.CHAR_VAL;
    //Original name: W-B04-AREA-D-VALOR-VAR-LEN
    private short areaDValorVarLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: W-B04-AREA-D-VALOR-VAR
    private String areaDValorVar = DefaultValues.stringVal(Len.AREA_D_VALOR_VAR);

    //==== METHODS ====
    public void setLenTot(short lenTot) {
        this.lenTot = lenTot;
    }

    public short getLenTot() {
        return this.lenTot;
    }

    public void setAreaDValorVarInd(char areaDValorVarInd) {
        this.areaDValorVarInd = areaDValorVarInd;
    }

    public void setAreaDValorVarIndFormatted(String areaDValorVarInd) {
        setAreaDValorVarInd(Functions.charAt(areaDValorVarInd, Types.CHAR_SIZE));
    }

    public char getAreaDValorVarInd() {
        return this.areaDValorVarInd;
    }

    public byte[] getAreaDValorVarVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, areaDValorVarLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, areaDValorVar, Len.AREA_D_VALOR_VAR);
        return buffer;
    }

    public void setAreaDValorVarLen(short areaDValorVarLen) {
        this.areaDValorVarLen = areaDValorVarLen;
    }

    public short getAreaDValorVarLen() {
        return this.areaDValorVarLen;
    }

    public void setAreaDValorVar(String areaDValorVar) {
        this.areaDValorVar = Functions.subString(areaDValorVar, Len.AREA_D_VALOR_VAR);
    }

    public String getAreaDValorVar() {
        return this.areaDValorVar;
    }

    public WB04RecFisso getRecFisso() {
        return recFisso;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AREA_D_VALOR_VAR = 4000;
        public static final int LEN_TOT = 2;
        public static final int AREA_D_VALOR_VAR_IND = 1;
        public static final int AREA_D_VALOR_VAR_LEN = 2;
        public static final int AREA_D_VALOR_VAR_VCHAR = AREA_D_VALOR_VAR_LEN + AREA_D_VALOR_VAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
