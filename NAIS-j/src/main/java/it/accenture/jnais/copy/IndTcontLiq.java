package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-TCONT-LIQ<br>
 * Variable: IND-TCONT-LIQ from copybook IDBVTCL2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndTcontLiq {

    //==== PROPERTIES ====
    //Original name: IND-TCL-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-DT-VLT
    private short dtVlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-LRD-LIQTO
    private short impLrdLiqto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-PREST
    private short impPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-INTR-PREST
    private short impIntrPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-RAT
    private short impRat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-UTI
    private short impUti = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-RIT-TFR
    private short impRitTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-RIT-ACC
    private short impRitAcc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-RIT-VIS
    private short impRitVis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPB-TFR
    private short impbTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPB-ACC
    private short impbAcc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPB-VIS
    private short impbVis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-RIMB
    private short impRimb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-CORTVO
    private short impCortvo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPB-IMPST-PRVR
    private short impbImpstPrvr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPST-PRVR
    private short impstPrvr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPB-IMPST-252
    private short impbImpst252 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPST-252
    private short impst252 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-IS
    private short impIs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-DIR-LIQ
    private short impDirLiq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-NET-LIQTO
    private short impNetLiqto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-EFFLQ
    private short impEfflq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-COD-DVS
    private short codDvs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-TP-MEZ-PAG-ACCR
    private short tpMezPagAccr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-ESTR-CNT-CORR-ACCR
    private short estrCntCorrAccr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-TP-STAT-TIT
    private short tpStatTit = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPB-VIS-1382011
    private short impbVis1382011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPST-VIS-1382011
    private short impstVis1382011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPST-SOST-1382011
    private short impstSost1382011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMP-INTR-RIT-PAG
    private short impIntrRitPag = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPB-IS
    private short impbIs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPB-IS-1382011
    private short impbIs1382011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPB-VIS-662014
    private short impbVis662014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPST-VIS-662014
    private short impstVis662014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPB-IS-662014
    private short impbIs662014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TCL-IMPST-SOST-662014
    private short impstSost662014 = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtVlt(short dtVlt) {
        this.dtVlt = dtVlt;
    }

    public short getDtVlt() {
        return this.dtVlt;
    }

    public void setImpLrdLiqto(short impLrdLiqto) {
        this.impLrdLiqto = impLrdLiqto;
    }

    public short getImpLrdLiqto() {
        return this.impLrdLiqto;
    }

    public void setImpPrest(short impPrest) {
        this.impPrest = impPrest;
    }

    public short getImpPrest() {
        return this.impPrest;
    }

    public void setImpIntrPrest(short impIntrPrest) {
        this.impIntrPrest = impIntrPrest;
    }

    public short getImpIntrPrest() {
        return this.impIntrPrest;
    }

    public void setImpRat(short impRat) {
        this.impRat = impRat;
    }

    public short getImpRat() {
        return this.impRat;
    }

    public void setImpUti(short impUti) {
        this.impUti = impUti;
    }

    public short getImpUti() {
        return this.impUti;
    }

    public void setImpRitTfr(short impRitTfr) {
        this.impRitTfr = impRitTfr;
    }

    public short getImpRitTfr() {
        return this.impRitTfr;
    }

    public void setImpRitAcc(short impRitAcc) {
        this.impRitAcc = impRitAcc;
    }

    public short getImpRitAcc() {
        return this.impRitAcc;
    }

    public void setImpRitVis(short impRitVis) {
        this.impRitVis = impRitVis;
    }

    public short getImpRitVis() {
        return this.impRitVis;
    }

    public void setImpbTfr(short impbTfr) {
        this.impbTfr = impbTfr;
    }

    public short getImpbTfr() {
        return this.impbTfr;
    }

    public void setImpbAcc(short impbAcc) {
        this.impbAcc = impbAcc;
    }

    public short getImpbAcc() {
        return this.impbAcc;
    }

    public void setImpbVis(short impbVis) {
        this.impbVis = impbVis;
    }

    public short getImpbVis() {
        return this.impbVis;
    }

    public void setImpRimb(short impRimb) {
        this.impRimb = impRimb;
    }

    public short getImpRimb() {
        return this.impRimb;
    }

    public void setImpCortvo(short impCortvo) {
        this.impCortvo = impCortvo;
    }

    public short getImpCortvo() {
        return this.impCortvo;
    }

    public void setImpbImpstPrvr(short impbImpstPrvr) {
        this.impbImpstPrvr = impbImpstPrvr;
    }

    public short getImpbImpstPrvr() {
        return this.impbImpstPrvr;
    }

    public void setImpstPrvr(short impstPrvr) {
        this.impstPrvr = impstPrvr;
    }

    public short getImpstPrvr() {
        return this.impstPrvr;
    }

    public void setImpbImpst252(short impbImpst252) {
        this.impbImpst252 = impbImpst252;
    }

    public short getImpbImpst252() {
        return this.impbImpst252;
    }

    public void setImpst252(short impst252) {
        this.impst252 = impst252;
    }

    public short getImpst252() {
        return this.impst252;
    }

    public void setImpIs(short impIs) {
        this.impIs = impIs;
    }

    public short getImpIs() {
        return this.impIs;
    }

    public void setImpDirLiq(short impDirLiq) {
        this.impDirLiq = impDirLiq;
    }

    public short getImpDirLiq() {
        return this.impDirLiq;
    }

    public void setImpNetLiqto(short impNetLiqto) {
        this.impNetLiqto = impNetLiqto;
    }

    public short getImpNetLiqto() {
        return this.impNetLiqto;
    }

    public void setImpEfflq(short impEfflq) {
        this.impEfflq = impEfflq;
    }

    public short getImpEfflq() {
        return this.impEfflq;
    }

    public void setCodDvs(short codDvs) {
        this.codDvs = codDvs;
    }

    public short getCodDvs() {
        return this.codDvs;
    }

    public void setTpMezPagAccr(short tpMezPagAccr) {
        this.tpMezPagAccr = tpMezPagAccr;
    }

    public short getTpMezPagAccr() {
        return this.tpMezPagAccr;
    }

    public void setEstrCntCorrAccr(short estrCntCorrAccr) {
        this.estrCntCorrAccr = estrCntCorrAccr;
    }

    public short getEstrCntCorrAccr() {
        return this.estrCntCorrAccr;
    }

    public void setTpStatTit(short tpStatTit) {
        this.tpStatTit = tpStatTit;
    }

    public short getTpStatTit() {
        return this.tpStatTit;
    }

    public void setImpbVis1382011(short impbVis1382011) {
        this.impbVis1382011 = impbVis1382011;
    }

    public short getImpbVis1382011() {
        return this.impbVis1382011;
    }

    public void setImpstVis1382011(short impstVis1382011) {
        this.impstVis1382011 = impstVis1382011;
    }

    public short getImpstVis1382011() {
        return this.impstVis1382011;
    }

    public void setImpstSost1382011(short impstSost1382011) {
        this.impstSost1382011 = impstSost1382011;
    }

    public short getImpstSost1382011() {
        return this.impstSost1382011;
    }

    public void setImpIntrRitPag(short impIntrRitPag) {
        this.impIntrRitPag = impIntrRitPag;
    }

    public short getImpIntrRitPag() {
        return this.impIntrRitPag;
    }

    public void setImpbIs(short impbIs) {
        this.impbIs = impbIs;
    }

    public short getImpbIs() {
        return this.impbIs;
    }

    public void setImpbIs1382011(short impbIs1382011) {
        this.impbIs1382011 = impbIs1382011;
    }

    public short getImpbIs1382011() {
        return this.impbIs1382011;
    }

    public void setImpbVis662014(short impbVis662014) {
        this.impbVis662014 = impbVis662014;
    }

    public short getImpbVis662014() {
        return this.impbVis662014;
    }

    public void setImpstVis662014(short impstVis662014) {
        this.impstVis662014 = impstVis662014;
    }

    public short getImpstVis662014() {
        return this.impstVis662014;
    }

    public void setImpbIs662014(short impbIs662014) {
        this.impbIs662014 = impbIs662014;
    }

    public short getImpbIs662014() {
        return this.impbIs662014;
    }

    public void setImpstSost662014(short impstSost662014) {
        this.impstSost662014 = impstSost662014;
    }

    public short getImpstSost662014() {
        return this.impstSost662014;
    }
}
