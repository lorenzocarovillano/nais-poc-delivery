package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-TIT-RAT<br>
 * Variable: IND-TIT-RAT from copybook IDBVTDR2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndTitRat {

    //==== PROPERTIES ====
    //Original name: IND-TDR-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-PROG-TIT
    private short progTit = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-DT-INI-COP
    private short dtIniCop = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-DT-END-COP
    private short dtEndCop = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-IMP-PAG
    private short impPag = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-FL-SOLL
    private short flSoll = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-FRAZ
    private short fraz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-DT-APPLZ-MORA
    private short dtApplzMora = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-FL-MORA
    private short flMora = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-ID-RAPP-RETE
    private short idRappRete = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-ID-RAPP-ANA
    private short idRappAna = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-COD-DVS
    private short codDvs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-DT-ESI-TIT
    private short dtEsiTit = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-PRE-NET
    private short totPreNet = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-INTR-FRAZ
    private short totIntrFraz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-INTR-MORA
    private short totIntrMora = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-INTR-PREST
    private short totIntrPrest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-INTR-RETDT
    private short totIntrRetdt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-INTR-RIAT
    private short totIntrRiat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-DIR
    private short totDir = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-SPE-MED
    private short totSpeMed = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-SPE-AGE
    private short totSpeAge = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-TAX
    private short totTax = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-SOPR-SAN
    private short totSoprSan = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-SOPR-TEC
    private short totSoprTec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-SOPR-SPO
    private short totSoprSpo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-SOPR-PROF
    private short totSoprProf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-SOPR-ALT
    private short totSoprAlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-PRE-TOT
    private short totPreTot = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-PRE-PP-IAS
    private short totPrePpIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-CAR-IAS
    private short totCarIas = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-PRE-SOLO-RSH
    private short totPreSoloRsh = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-PROV-ACQ-1AA
    private short totProvAcq1aa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-PROV-ACQ-2AA
    private short totProvAcq2aa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-PROV-RICOR
    private short totProvRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-PROV-INC
    private short totProvInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-PROV-DA-REC
    private short totProvDaRec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-IMP-AZ
    private short impAz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-IMP-ADER
    private short impAder = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-IMP-TFR
    private short impTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-IMP-VOLO
    private short impVolo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-FL-VLDT-TIT
    private short flVldtTit = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-CAR-ACQ
    private short totCarAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-CAR-GEST
    private short totCarGest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-CAR-INC
    private short totCarInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-MANFEE-ANTIC
    private short totManfeeAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-MANFEE-RICOR
    private short totManfeeRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-MANFEE-REC
    private short totManfeeRec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-IMP-TRASFE
    private short impTrasfe = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-IMP-TFR-STRC
    private short impTfrStrc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-ACQ-EXP
    private short totAcqExp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-REMUN-ASS
    private short totRemunAss = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-COMMIS-INTER
    private short totCommisInter = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-TOT-CNBT-ANTIRAC
    private short totCnbtAntirac = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TDR-FL-INC-AUTOGEN
    private short flIncAutogen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setProgTit(short progTit) {
        this.progTit = progTit;
    }

    public short getProgTit() {
        return this.progTit;
    }

    public void setDtIniCop(short dtIniCop) {
        this.dtIniCop = dtIniCop;
    }

    public short getDtIniCop() {
        return this.dtIniCop;
    }

    public void setDtEndCop(short dtEndCop) {
        this.dtEndCop = dtEndCop;
    }

    public short getDtEndCop() {
        return this.dtEndCop;
    }

    public void setImpPag(short impPag) {
        this.impPag = impPag;
    }

    public short getImpPag() {
        return this.impPag;
    }

    public void setFlSoll(short flSoll) {
        this.flSoll = flSoll;
    }

    public short getFlSoll() {
        return this.flSoll;
    }

    public void setFraz(short fraz) {
        this.fraz = fraz;
    }

    public short getFraz() {
        return this.fraz;
    }

    public void setDtApplzMora(short dtApplzMora) {
        this.dtApplzMora = dtApplzMora;
    }

    public short getDtApplzMora() {
        return this.dtApplzMora;
    }

    public void setFlMora(short flMora) {
        this.flMora = flMora;
    }

    public short getFlMora() {
        return this.flMora;
    }

    public void setIdRappRete(short idRappRete) {
        this.idRappRete = idRappRete;
    }

    public short getIdRappRete() {
        return this.idRappRete;
    }

    public void setIdRappAna(short idRappAna) {
        this.idRappAna = idRappAna;
    }

    public short getIdRappAna() {
        return this.idRappAna;
    }

    public void setCodDvs(short codDvs) {
        this.codDvs = codDvs;
    }

    public short getCodDvs() {
        return this.codDvs;
    }

    public void setDtEsiTit(short dtEsiTit) {
        this.dtEsiTit = dtEsiTit;
    }

    public short getDtEsiTit() {
        return this.dtEsiTit;
    }

    public void setTotPreNet(short totPreNet) {
        this.totPreNet = totPreNet;
    }

    public short getTotPreNet() {
        return this.totPreNet;
    }

    public void setTotIntrFraz(short totIntrFraz) {
        this.totIntrFraz = totIntrFraz;
    }

    public short getTotIntrFraz() {
        return this.totIntrFraz;
    }

    public void setTotIntrMora(short totIntrMora) {
        this.totIntrMora = totIntrMora;
    }

    public short getTotIntrMora() {
        return this.totIntrMora;
    }

    public void setTotIntrPrest(short totIntrPrest) {
        this.totIntrPrest = totIntrPrest;
    }

    public short getTotIntrPrest() {
        return this.totIntrPrest;
    }

    public void setTotIntrRetdt(short totIntrRetdt) {
        this.totIntrRetdt = totIntrRetdt;
    }

    public short getTotIntrRetdt() {
        return this.totIntrRetdt;
    }

    public void setTotIntrRiat(short totIntrRiat) {
        this.totIntrRiat = totIntrRiat;
    }

    public short getTotIntrRiat() {
        return this.totIntrRiat;
    }

    public void setTotDir(short totDir) {
        this.totDir = totDir;
    }

    public short getTotDir() {
        return this.totDir;
    }

    public void setTotSpeMed(short totSpeMed) {
        this.totSpeMed = totSpeMed;
    }

    public short getTotSpeMed() {
        return this.totSpeMed;
    }

    public void setTotSpeAge(short totSpeAge) {
        this.totSpeAge = totSpeAge;
    }

    public short getTotSpeAge() {
        return this.totSpeAge;
    }

    public void setTotTax(short totTax) {
        this.totTax = totTax;
    }

    public short getTotTax() {
        return this.totTax;
    }

    public void setTotSoprSan(short totSoprSan) {
        this.totSoprSan = totSoprSan;
    }

    public short getTotSoprSan() {
        return this.totSoprSan;
    }

    public void setTotSoprTec(short totSoprTec) {
        this.totSoprTec = totSoprTec;
    }

    public short getTotSoprTec() {
        return this.totSoprTec;
    }

    public void setTotSoprSpo(short totSoprSpo) {
        this.totSoprSpo = totSoprSpo;
    }

    public short getTotSoprSpo() {
        return this.totSoprSpo;
    }

    public void setTotSoprProf(short totSoprProf) {
        this.totSoprProf = totSoprProf;
    }

    public short getTotSoprProf() {
        return this.totSoprProf;
    }

    public void setTotSoprAlt(short totSoprAlt) {
        this.totSoprAlt = totSoprAlt;
    }

    public short getTotSoprAlt() {
        return this.totSoprAlt;
    }

    public void setTotPreTot(short totPreTot) {
        this.totPreTot = totPreTot;
    }

    public short getTotPreTot() {
        return this.totPreTot;
    }

    public void setTotPrePpIas(short totPrePpIas) {
        this.totPrePpIas = totPrePpIas;
    }

    public short getTotPrePpIas() {
        return this.totPrePpIas;
    }

    public void setTotCarIas(short totCarIas) {
        this.totCarIas = totCarIas;
    }

    public short getTotCarIas() {
        return this.totCarIas;
    }

    public void setTotPreSoloRsh(short totPreSoloRsh) {
        this.totPreSoloRsh = totPreSoloRsh;
    }

    public short getTotPreSoloRsh() {
        return this.totPreSoloRsh;
    }

    public void setTotProvAcq1aa(short totProvAcq1aa) {
        this.totProvAcq1aa = totProvAcq1aa;
    }

    public short getTotProvAcq1aa() {
        return this.totProvAcq1aa;
    }

    public void setTotProvAcq2aa(short totProvAcq2aa) {
        this.totProvAcq2aa = totProvAcq2aa;
    }

    public short getTotProvAcq2aa() {
        return this.totProvAcq2aa;
    }

    public void setTotProvRicor(short totProvRicor) {
        this.totProvRicor = totProvRicor;
    }

    public short getTotProvRicor() {
        return this.totProvRicor;
    }

    public void setTotProvInc(short totProvInc) {
        this.totProvInc = totProvInc;
    }

    public short getTotProvInc() {
        return this.totProvInc;
    }

    public void setTotProvDaRec(short totProvDaRec) {
        this.totProvDaRec = totProvDaRec;
    }

    public short getTotProvDaRec() {
        return this.totProvDaRec;
    }

    public void setImpAz(short impAz) {
        this.impAz = impAz;
    }

    public short getImpAz() {
        return this.impAz;
    }

    public void setImpAder(short impAder) {
        this.impAder = impAder;
    }

    public short getImpAder() {
        return this.impAder;
    }

    public void setImpTfr(short impTfr) {
        this.impTfr = impTfr;
    }

    public short getImpTfr() {
        return this.impTfr;
    }

    public void setImpVolo(short impVolo) {
        this.impVolo = impVolo;
    }

    public short getImpVolo() {
        return this.impVolo;
    }

    public void setFlVldtTit(short flVldtTit) {
        this.flVldtTit = flVldtTit;
    }

    public short getFlVldtTit() {
        return this.flVldtTit;
    }

    public void setTotCarAcq(short totCarAcq) {
        this.totCarAcq = totCarAcq;
    }

    public short getTotCarAcq() {
        return this.totCarAcq;
    }

    public void setTotCarGest(short totCarGest) {
        this.totCarGest = totCarGest;
    }

    public short getTotCarGest() {
        return this.totCarGest;
    }

    public void setTotCarInc(short totCarInc) {
        this.totCarInc = totCarInc;
    }

    public short getTotCarInc() {
        return this.totCarInc;
    }

    public void setTotManfeeAntic(short totManfeeAntic) {
        this.totManfeeAntic = totManfeeAntic;
    }

    public short getTotManfeeAntic() {
        return this.totManfeeAntic;
    }

    public void setTotManfeeRicor(short totManfeeRicor) {
        this.totManfeeRicor = totManfeeRicor;
    }

    public short getTotManfeeRicor() {
        return this.totManfeeRicor;
    }

    public void setTotManfeeRec(short totManfeeRec) {
        this.totManfeeRec = totManfeeRec;
    }

    public short getTotManfeeRec() {
        return this.totManfeeRec;
    }

    public void setImpTrasfe(short impTrasfe) {
        this.impTrasfe = impTrasfe;
    }

    public short getImpTrasfe() {
        return this.impTrasfe;
    }

    public void setImpTfrStrc(short impTfrStrc) {
        this.impTfrStrc = impTfrStrc;
    }

    public short getImpTfrStrc() {
        return this.impTfrStrc;
    }

    public void setTotAcqExp(short totAcqExp) {
        this.totAcqExp = totAcqExp;
    }

    public short getTotAcqExp() {
        return this.totAcqExp;
    }

    public void setTotRemunAss(short totRemunAss) {
        this.totRemunAss = totRemunAss;
    }

    public short getTotRemunAss() {
        return this.totRemunAss;
    }

    public void setTotCommisInter(short totCommisInter) {
        this.totCommisInter = totCommisInter;
    }

    public short getTotCommisInter() {
        return this.totCommisInter;
    }

    public void setTotCnbtAntirac(short totCnbtAntirac) {
        this.totCnbtAntirac = totCnbtAntirac;
    }

    public short getTotCnbtAntirac() {
        return this.totCnbtAntirac;
    }

    public void setFlIncAutogen(short flIncAutogen) {
        this.flIncAutogen = flIncAutogen;
    }

    public short getFlIncAutogen() {
        return this.flIncAutogen;
    }
}
