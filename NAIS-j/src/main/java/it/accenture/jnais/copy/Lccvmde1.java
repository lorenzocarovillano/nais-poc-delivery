package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVMDE1<br>
 * Variable: LCCVMDE1 from copybook LCCVMDE1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvmde1 {

    //==== PROPERTIES ====
    /**Original name: WMDE-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA MOT_DEROGA
	 *    ALIAS MDE
	 *    ULTIMO AGG. 02 SET 2008
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WMDE-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WMDE-DATI
    private WmdeDati dati = new WmdeDati();

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WmdeDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }
}
