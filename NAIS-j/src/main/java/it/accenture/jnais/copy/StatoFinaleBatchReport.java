package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: STATO-FINALE-BATCH-REPORT<br>
 * Variable: STATO-FINALE-BATCH-REPORT from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class StatoFinaleBatchReport {

    //==== PROPERTIES ====
    //Original name: FILLER-STATO-FINALE-BATCH-REPORT
    private String flr1 = "STATO FINALE BATCH";
    //Original name: FILLER-STATO-FINALE-BATCH-REPORT-1
    private String flr2 = " :";
    //Original name: REP-STATO-FINALE
    private String repStatoFinale = "";

    //==== METHODS ====
    public String getStatoFinaleBatchReportFormatted() {
        return MarshalByteExt.bufferToStr(getStatoFinaleBatchReportBytes());
    }

    public byte[] getStatoFinaleBatchReportBytes() {
        byte[] buffer = new byte[Len.STATO_FINALE_BATCH_REPORT];
        return getStatoFinaleBatchReportBytes(buffer, 1);
    }

    public byte[] getStatoFinaleBatchReportBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, repStatoFinale, Len.REP_STATO_FINALE);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public void setRepStatoFinale(String repStatoFinale) {
        this.repStatoFinale = Functions.subString(repStatoFinale, Len.REP_STATO_FINALE);
    }

    public String getRepStatoFinale() {
        return this.repStatoFinale;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 50;
        public static final int FLR2 = 4;
        public static final int REP_STATO_FINALE = 50;
        public static final int STATO_FINALE_BATCH_REPORT = REP_STATO_FINALE + FLR1 + FLR2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
