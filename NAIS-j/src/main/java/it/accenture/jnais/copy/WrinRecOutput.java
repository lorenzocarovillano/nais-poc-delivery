package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.WrinTpRecordDati;

/**Original name: WRIN-REC-OUTPUT<br>
 * Variable: WRIN-REC-OUTPUT from copybook LRGC0031<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WrinRecOutput {

    //==== PROPERTIES ====
    /**Original name: WRIN-MACROFUNZIONALITA<br>
	 * <pre>----------------------------------------------------------------*
	 *  --    DATI ELABORAZIONE ESTRATTO CONTO (FISSA PER TUTTI I TP)
	 * ----------------------------------------------------------------*</pre>*/
    private String wrinMacrofunzionalita = DefaultValues.stringVal(Len.WRIN_MACROFUNZIONALITA);
    //Original name: WRIN-FUNZIONALITA
    private String wrinFunzionalita = DefaultValues.stringVal(Len.WRIN_FUNZIONALITA);
    //Original name: WRIN-COD-COMP-ANIA
    private String wrinCodCompAnia = DefaultValues.stringVal(Len.WRIN_COD_COMP_ANIA);
    //Original name: WRIN-IB-POLI-IND
    private String wrinIbPoliInd = DefaultValues.stringVal(Len.WRIN_IB_POLI_IND);
    //Original name: WRIN-IB-POLI-COL
    private String wrinIbPoliCol = DefaultValues.stringVal(Len.WRIN_IB_POLI_COL);
    //Original name: WRIN-IB-ADES
    private String wrinIbAdes = DefaultValues.stringVal(Len.WRIN_IB_ADES);
    //Original name: WRIN-IB-GAR
    private String wrinIbGar = DefaultValues.stringVal(Len.WRIN_IB_GAR);
    //Original name: WRIN-IB-TRCH-DI-GAR
    private String wrinIbTrchDiGar = DefaultValues.stringVal(Len.WRIN_IB_TRCH_DI_GAR);
    //Original name: WRIN-TP-INVST
    private String wrinTpInvst = DefaultValues.stringVal(Len.WRIN_TP_INVST);
    //Original name: WRIN-TP-RECORD
    private String wrinTpRecord = DefaultValues.stringVal(Len.WRIN_TP_RECORD);
    //Original name: WRIN-TP-RECORD-DATI
    private WrinTpRecordDati wrinTpRecordDati = new WrinTpRecordDati();

    //==== METHODS ====
    public String getWrinRecOutputFormatted() {
        return MarshalByteExt.bufferToStr(getWrinRecOutputBytes());
    }

    public void setWoutRecOutputBytes(byte[] buffer) {
        setWrinRecOutputBytes(buffer, 1);
    }

    public byte[] getWrinRecOutputBytes() {
        byte[] buffer = new byte[Len.WRIN_REC_OUTPUT];
        return getWrinRecOutputBytes(buffer, 1);
    }

    public void setWrinRecOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        wrinMacrofunzionalita = MarshalByte.readString(buffer, position, Len.WRIN_MACROFUNZIONALITA);
        position += Len.WRIN_MACROFUNZIONALITA;
        wrinFunzionalita = MarshalByte.readString(buffer, position, Len.WRIN_FUNZIONALITA);
        position += Len.WRIN_FUNZIONALITA;
        wrinCodCompAnia = MarshalByte.readFixedString(buffer, position, Len.WRIN_COD_COMP_ANIA);
        position += Len.WRIN_COD_COMP_ANIA;
        wrinIbPoliInd = MarshalByte.readString(buffer, position, Len.WRIN_IB_POLI_IND);
        position += Len.WRIN_IB_POLI_IND;
        wrinIbPoliCol = MarshalByte.readString(buffer, position, Len.WRIN_IB_POLI_COL);
        position += Len.WRIN_IB_POLI_COL;
        wrinIbAdes = MarshalByte.readString(buffer, position, Len.WRIN_IB_ADES);
        position += Len.WRIN_IB_ADES;
        wrinIbGar = MarshalByte.readString(buffer, position, Len.WRIN_IB_GAR);
        position += Len.WRIN_IB_GAR;
        wrinIbTrchDiGar = MarshalByte.readString(buffer, position, Len.WRIN_IB_TRCH_DI_GAR);
        position += Len.WRIN_IB_TRCH_DI_GAR;
        wrinTpInvst = MarshalByte.readString(buffer, position, Len.WRIN_TP_INVST);
        position += Len.WRIN_TP_INVST;
        wrinTpRecord = MarshalByte.readString(buffer, position, Len.WRIN_TP_RECORD);
        position += Len.WRIN_TP_RECORD;
        wrinTpRecordDati.setWrinTpRecordDatiFromBuffer(buffer, position);
    }

    public byte[] getWrinRecOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wrinMacrofunzionalita, Len.WRIN_MACROFUNZIONALITA);
        position += Len.WRIN_MACROFUNZIONALITA;
        MarshalByte.writeString(buffer, position, wrinFunzionalita, Len.WRIN_FUNZIONALITA);
        position += Len.WRIN_FUNZIONALITA;
        MarshalByte.writeString(buffer, position, wrinCodCompAnia, Len.WRIN_COD_COMP_ANIA);
        position += Len.WRIN_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wrinIbPoliInd, Len.WRIN_IB_POLI_IND);
        position += Len.WRIN_IB_POLI_IND;
        MarshalByte.writeString(buffer, position, wrinIbPoliCol, Len.WRIN_IB_POLI_COL);
        position += Len.WRIN_IB_POLI_COL;
        MarshalByte.writeString(buffer, position, wrinIbAdes, Len.WRIN_IB_ADES);
        position += Len.WRIN_IB_ADES;
        MarshalByte.writeString(buffer, position, wrinIbGar, Len.WRIN_IB_GAR);
        position += Len.WRIN_IB_GAR;
        MarshalByte.writeString(buffer, position, wrinIbTrchDiGar, Len.WRIN_IB_TRCH_DI_GAR);
        position += Len.WRIN_IB_TRCH_DI_GAR;
        MarshalByte.writeString(buffer, position, wrinTpInvst, Len.WRIN_TP_INVST);
        position += Len.WRIN_TP_INVST;
        MarshalByte.writeString(buffer, position, wrinTpRecord, Len.WRIN_TP_RECORD);
        position += Len.WRIN_TP_RECORD;
        wrinTpRecordDati.getWrinTpRecordDatiAsBuffer(buffer, position);
        return buffer;
    }

    public void setWrinMacrofunzionalita(String wrinMacrofunzionalita) {
        this.wrinMacrofunzionalita = Functions.subString(wrinMacrofunzionalita, Len.WRIN_MACROFUNZIONALITA);
    }

    public String getWrinMacrofunzionalita() {
        return this.wrinMacrofunzionalita;
    }

    public void setWrinFunzionalita(String wrinFunzionalita) {
        this.wrinFunzionalita = Functions.subString(wrinFunzionalita, Len.WRIN_FUNZIONALITA);
    }

    public String getWrinFunzionalita() {
        return this.wrinFunzionalita;
    }

    public void setWrinCodCompAniaFormatted(String wrinCodCompAnia) {
        this.wrinCodCompAnia = Trunc.toUnsignedNumeric(wrinCodCompAnia, Len.WRIN_COD_COMP_ANIA);
    }

    public int getWrinCodCompAnia() {
        return NumericDisplay.asInt(this.wrinCodCompAnia);
    }

    public void setWrinIbPoliInd(String wrinIbPoliInd) {
        this.wrinIbPoliInd = Functions.subString(wrinIbPoliInd, Len.WRIN_IB_POLI_IND);
    }

    public String getWrinIbPoliInd() {
        return this.wrinIbPoliInd;
    }

    public String getWrinIbPoliIndFormatted() {
        return Functions.padBlanks(getWrinIbPoliInd(), Len.WRIN_IB_POLI_IND);
    }

    public void setWrinIbPoliCol(String wrinIbPoliCol) {
        this.wrinIbPoliCol = Functions.subString(wrinIbPoliCol, Len.WRIN_IB_POLI_COL);
    }

    public String getWrinIbPoliCol() {
        return this.wrinIbPoliCol;
    }

    public void setWrinIbAdes(String wrinIbAdes) {
        this.wrinIbAdes = Functions.subString(wrinIbAdes, Len.WRIN_IB_ADES);
    }

    public String getWrinIbAdes() {
        return this.wrinIbAdes;
    }

    public void setWrinIbGar(String wrinIbGar) {
        this.wrinIbGar = Functions.subString(wrinIbGar, Len.WRIN_IB_GAR);
    }

    public String getWrinIbGar() {
        return this.wrinIbGar;
    }

    public void setWrinIbTrchDiGar(String wrinIbTrchDiGar) {
        this.wrinIbTrchDiGar = Functions.subString(wrinIbTrchDiGar, Len.WRIN_IB_TRCH_DI_GAR);
    }

    public String getWrinIbTrchDiGar() {
        return this.wrinIbTrchDiGar;
    }

    public void setWrinTpInvst(String wrinTpInvst) {
        this.wrinTpInvst = Functions.subString(wrinTpInvst, Len.WRIN_TP_INVST);
    }

    public String getWrinTpInvst() {
        return this.wrinTpInvst;
    }

    public void setWrinTpRecord(String wrinTpRecord) {
        this.wrinTpRecord = Functions.subString(wrinTpRecord, Len.WRIN_TP_RECORD);
    }

    public String getWrinTpRecord() {
        return this.wrinTpRecord;
    }

    public WrinTpRecordDati getWrinTpRecordDati() {
        return wrinTpRecordDati;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIN_MACROFUNZIONALITA = 2;
        public static final int WRIN_FUNZIONALITA = 4;
        public static final int WRIN_COD_COMP_ANIA = 5;
        public static final int WRIN_IB_POLI_IND = 40;
        public static final int WRIN_IB_POLI_COL = 40;
        public static final int WRIN_IB_ADES = 40;
        public static final int WRIN_IB_GAR = 40;
        public static final int WRIN_IB_TRCH_DI_GAR = 40;
        public static final int WRIN_TP_INVST = 3;
        public static final int WRIN_TP_RECORD = 4;
        public static final int WRIN_REC_OUTPUT = WRIN_MACROFUNZIONALITA + WRIN_FUNZIONALITA + WRIN_COD_COMP_ANIA + WRIN_IB_POLI_IND + WRIN_IB_POLI_COL + WRIN_IB_ADES + WRIN_IB_GAR + WRIN_IB_TRCH_DI_GAR + WRIN_TP_INVST + WRIN_TP_RECORD + WrinTpRecordDati.Len.WRIN_TP_RECORD_DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
