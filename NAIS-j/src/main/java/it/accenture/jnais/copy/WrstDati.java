package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WrstDtCalc;
import it.accenture.jnais.ws.redefines.WrstDtElab;
import it.accenture.jnais.ws.redefines.WrstFrazPrePp;
import it.accenture.jnais.ws.redefines.WrstIdMoviChiu;
import it.accenture.jnais.ws.redefines.WrstIncrXRival;
import it.accenture.jnais.ws.redefines.WrstRisAbb;
import it.accenture.jnais.ws.redefines.WrstRisAcq;
import it.accenture.jnais.ws.redefines.WrstRisBila;
import it.accenture.jnais.ws.redefines.WrstRisBnsfdt;
import it.accenture.jnais.ws.redefines.WrstRisBnsric;
import it.accenture.jnais.ws.redefines.WrstRisComponAssva;
import it.accenture.jnais.ws.redefines.WrstRisCosAmmtz;
import it.accenture.jnais.ws.redefines.WrstRisFaivl;
import it.accenture.jnais.ws.redefines.WrstRisGarCasoMor;
import it.accenture.jnais.ws.redefines.WrstRisIntegBasTec;
import it.accenture.jnais.ws.redefines.WrstRisIntegDecrTs;
import it.accenture.jnais.ws.redefines.WrstRisMat;
import it.accenture.jnais.ws.redefines.WrstRisMatEff;
import it.accenture.jnais.ws.redefines.WrstRisMinGarto;
import it.accenture.jnais.ws.redefines.WrstRisMoviNonInves;
import it.accenture.jnais.ws.redefines.WrstRisPrestFaivl;
import it.accenture.jnais.ws.redefines.WrstRisRistorniCap;
import it.accenture.jnais.ws.redefines.WrstRisRshDflt;
import it.accenture.jnais.ws.redefines.WrstRisSopr;
import it.accenture.jnais.ws.redefines.WrstRisSpe;
import it.accenture.jnais.ws.redefines.WrstRisSpeFaivl;
import it.accenture.jnais.ws.redefines.WrstRisTot;
import it.accenture.jnais.ws.redefines.WrstRisTrmBns;
import it.accenture.jnais.ws.redefines.WrstRisUti;
import it.accenture.jnais.ws.redefines.WrstRisZil;
import it.accenture.jnais.ws.redefines.WrstRptoPre;
import it.accenture.jnais.ws.redefines.WrstUltCoeffAggRis;
import it.accenture.jnais.ws.redefines.WrstUltCoeffRis;
import it.accenture.jnais.ws.redefines.WrstUltRm;

/**Original name: WRST-DATI<br>
 * Variable: WRST-DATI from copybook LCCVRST1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WrstDati {

    //==== PROPERTIES ====
    //Original name: WRST-ID-RIS-DI-TRCH
    private int wrstIdRisDiTrch = DefaultValues.INT_VAL;
    //Original name: WRST-ID-MOVI-CRZ
    private int wrstIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WRST-ID-MOVI-CHIU
    private WrstIdMoviChiu wrstIdMoviChiu = new WrstIdMoviChiu();
    //Original name: WRST-DT-INI-EFF
    private int wrstDtIniEff = DefaultValues.INT_VAL;
    //Original name: WRST-DT-END-EFF
    private int wrstDtEndEff = DefaultValues.INT_VAL;
    //Original name: WRST-COD-COMP-ANIA
    private int wrstCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WRST-TP-CALC-RIS
    private String wrstTpCalcRis = DefaultValues.stringVal(Len.WRST_TP_CALC_RIS);
    //Original name: WRST-ULT-RM
    private WrstUltRm wrstUltRm = new WrstUltRm();
    //Original name: WRST-DT-CALC
    private WrstDtCalc wrstDtCalc = new WrstDtCalc();
    //Original name: WRST-DT-ELAB
    private WrstDtElab wrstDtElab = new WrstDtElab();
    //Original name: WRST-RIS-BILA
    private WrstRisBila wrstRisBila = new WrstRisBila();
    //Original name: WRST-RIS-MAT
    private WrstRisMat wrstRisMat = new WrstRisMat();
    //Original name: WRST-INCR-X-RIVAL
    private WrstIncrXRival wrstIncrXRival = new WrstIncrXRival();
    //Original name: WRST-RPTO-PRE
    private WrstRptoPre wrstRptoPre = new WrstRptoPre();
    //Original name: WRST-FRAZ-PRE-PP
    private WrstFrazPrePp wrstFrazPrePp = new WrstFrazPrePp();
    //Original name: WRST-RIS-TOT
    private WrstRisTot wrstRisTot = new WrstRisTot();
    //Original name: WRST-RIS-SPE
    private WrstRisSpe wrstRisSpe = new WrstRisSpe();
    //Original name: WRST-RIS-ABB
    private WrstRisAbb wrstRisAbb = new WrstRisAbb();
    //Original name: WRST-RIS-BNSFDT
    private WrstRisBnsfdt wrstRisBnsfdt = new WrstRisBnsfdt();
    //Original name: WRST-RIS-SOPR
    private WrstRisSopr wrstRisSopr = new WrstRisSopr();
    //Original name: WRST-RIS-INTEG-BAS-TEC
    private WrstRisIntegBasTec wrstRisIntegBasTec = new WrstRisIntegBasTec();
    //Original name: WRST-RIS-INTEG-DECR-TS
    private WrstRisIntegDecrTs wrstRisIntegDecrTs = new WrstRisIntegDecrTs();
    //Original name: WRST-RIS-GAR-CASO-MOR
    private WrstRisGarCasoMor wrstRisGarCasoMor = new WrstRisGarCasoMor();
    //Original name: WRST-RIS-ZIL
    private WrstRisZil wrstRisZil = new WrstRisZil();
    //Original name: WRST-RIS-FAIVL
    private WrstRisFaivl wrstRisFaivl = new WrstRisFaivl();
    //Original name: WRST-RIS-COS-AMMTZ
    private WrstRisCosAmmtz wrstRisCosAmmtz = new WrstRisCosAmmtz();
    //Original name: WRST-RIS-SPE-FAIVL
    private WrstRisSpeFaivl wrstRisSpeFaivl = new WrstRisSpeFaivl();
    //Original name: WRST-RIS-PREST-FAIVL
    private WrstRisPrestFaivl wrstRisPrestFaivl = new WrstRisPrestFaivl();
    //Original name: WRST-RIS-COMPON-ASSVA
    private WrstRisComponAssva wrstRisComponAssva = new WrstRisComponAssva();
    //Original name: WRST-ULT-COEFF-RIS
    private WrstUltCoeffRis wrstUltCoeffRis = new WrstUltCoeffRis();
    //Original name: WRST-ULT-COEFF-AGG-RIS
    private WrstUltCoeffAggRis wrstUltCoeffAggRis = new WrstUltCoeffAggRis();
    //Original name: WRST-RIS-ACQ
    private WrstRisAcq wrstRisAcq = new WrstRisAcq();
    //Original name: WRST-RIS-UTI
    private WrstRisUti wrstRisUti = new WrstRisUti();
    //Original name: WRST-RIS-MAT-EFF
    private WrstRisMatEff wrstRisMatEff = new WrstRisMatEff();
    //Original name: WRST-RIS-RISTORNI-CAP
    private WrstRisRistorniCap wrstRisRistorniCap = new WrstRisRistorniCap();
    //Original name: WRST-RIS-TRM-BNS
    private WrstRisTrmBns wrstRisTrmBns = new WrstRisTrmBns();
    //Original name: WRST-RIS-BNSRIC
    private WrstRisBnsric wrstRisBnsric = new WrstRisBnsric();
    //Original name: WRST-DS-RIGA
    private long wrstDsRiga = DefaultValues.LONG_VAL;
    //Original name: WRST-DS-OPER-SQL
    private char wrstDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WRST-DS-VER
    private int wrstDsVer = DefaultValues.INT_VAL;
    //Original name: WRST-DS-TS-INI-CPTZ
    private long wrstDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WRST-DS-TS-END-CPTZ
    private long wrstDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WRST-DS-UTENTE
    private String wrstDsUtente = DefaultValues.stringVal(Len.WRST_DS_UTENTE);
    //Original name: WRST-DS-STATO-ELAB
    private char wrstDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WRST-ID-TRCH-DI-GAR
    private int wrstIdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: WRST-COD-FND
    private String wrstCodFnd = DefaultValues.stringVal(Len.WRST_COD_FND);
    //Original name: WRST-ID-POLI
    private int wrstIdPoli = DefaultValues.INT_VAL;
    //Original name: WRST-ID-ADES
    private int wrstIdAdes = DefaultValues.INT_VAL;
    //Original name: WRST-ID-GAR
    private int wrstIdGar = DefaultValues.INT_VAL;
    //Original name: WRST-RIS-MIN-GARTO
    private WrstRisMinGarto wrstRisMinGarto = new WrstRisMinGarto();
    //Original name: WRST-RIS-RSH-DFLT
    private WrstRisRshDflt wrstRisRshDflt = new WrstRisRshDflt();
    //Original name: WRST-RIS-MOVI-NON-INVES
    private WrstRisMoviNonInves wrstRisMoviNonInves = new WrstRisMoviNonInves();

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wrstIdRisDiTrch = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRST_ID_RIS_DI_TRCH, 0);
        position += Len.WRST_ID_RIS_DI_TRCH;
        wrstIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRST_ID_MOVI_CRZ, 0);
        position += Len.WRST_ID_MOVI_CRZ;
        wrstIdMoviChiu.setWrstIdMoviChiuFromBuffer(buffer, position);
        position += WrstIdMoviChiu.Len.WRST_ID_MOVI_CHIU;
        wrstDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRST_DT_INI_EFF, 0);
        position += Len.WRST_DT_INI_EFF;
        wrstDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRST_DT_END_EFF, 0);
        position += Len.WRST_DT_END_EFF;
        wrstCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRST_COD_COMP_ANIA, 0);
        position += Len.WRST_COD_COMP_ANIA;
        wrstTpCalcRis = MarshalByte.readString(buffer, position, Len.WRST_TP_CALC_RIS);
        position += Len.WRST_TP_CALC_RIS;
        wrstUltRm.setWrstUltRmFromBuffer(buffer, position);
        position += WrstUltRm.Len.WRST_ULT_RM;
        wrstDtCalc.setWrstDtCalcFromBuffer(buffer, position);
        position += WrstDtCalc.Len.WRST_DT_CALC;
        wrstDtElab.setWrstDtElabFromBuffer(buffer, position);
        position += WrstDtElab.Len.WRST_DT_ELAB;
        wrstRisBila.setWrstRisBilaFromBuffer(buffer, position);
        position += WrstRisBila.Len.WRST_RIS_BILA;
        wrstRisMat.setWrstRisMatFromBuffer(buffer, position);
        position += WrstRisMat.Len.WRST_RIS_MAT;
        wrstIncrXRival.setWrstIncrXRivalFromBuffer(buffer, position);
        position += WrstIncrXRival.Len.WRST_INCR_X_RIVAL;
        wrstRptoPre.setWrstRptoPreFromBuffer(buffer, position);
        position += WrstRptoPre.Len.WRST_RPTO_PRE;
        wrstFrazPrePp.setWrstFrazPrePpFromBuffer(buffer, position);
        position += WrstFrazPrePp.Len.WRST_FRAZ_PRE_PP;
        wrstRisTot.setWrstRisTotFromBuffer(buffer, position);
        position += WrstRisTot.Len.WRST_RIS_TOT;
        wrstRisSpe.setWrstRisSpeFromBuffer(buffer, position);
        position += WrstRisSpe.Len.WRST_RIS_SPE;
        wrstRisAbb.setWrstRisAbbFromBuffer(buffer, position);
        position += WrstRisAbb.Len.WRST_RIS_ABB;
        wrstRisBnsfdt.setWrstRisBnsfdtFromBuffer(buffer, position);
        position += WrstRisBnsfdt.Len.WRST_RIS_BNSFDT;
        wrstRisSopr.setWrstRisSoprFromBuffer(buffer, position);
        position += WrstRisSopr.Len.WRST_RIS_SOPR;
        wrstRisIntegBasTec.setWrstRisIntegBasTecFromBuffer(buffer, position);
        position += WrstRisIntegBasTec.Len.WRST_RIS_INTEG_BAS_TEC;
        wrstRisIntegDecrTs.setWrstRisIntegDecrTsFromBuffer(buffer, position);
        position += WrstRisIntegDecrTs.Len.WRST_RIS_INTEG_DECR_TS;
        wrstRisGarCasoMor.setWrstRisGarCasoMorFromBuffer(buffer, position);
        position += WrstRisGarCasoMor.Len.WRST_RIS_GAR_CASO_MOR;
        wrstRisZil.setWrstRisZilFromBuffer(buffer, position);
        position += WrstRisZil.Len.WRST_RIS_ZIL;
        wrstRisFaivl.setWrstRisFaivlFromBuffer(buffer, position);
        position += WrstRisFaivl.Len.WRST_RIS_FAIVL;
        wrstRisCosAmmtz.setWrstRisCosAmmtzFromBuffer(buffer, position);
        position += WrstRisCosAmmtz.Len.WRST_RIS_COS_AMMTZ;
        wrstRisSpeFaivl.setWrstRisSpeFaivlFromBuffer(buffer, position);
        position += WrstRisSpeFaivl.Len.WRST_RIS_SPE_FAIVL;
        wrstRisPrestFaivl.setWrstRisPrestFaivlFromBuffer(buffer, position);
        position += WrstRisPrestFaivl.Len.WRST_RIS_PREST_FAIVL;
        wrstRisComponAssva.setWrstRisComponAssvaFromBuffer(buffer, position);
        position += WrstRisComponAssva.Len.WRST_RIS_COMPON_ASSVA;
        wrstUltCoeffRis.setWrstUltCoeffRisFromBuffer(buffer, position);
        position += WrstUltCoeffRis.Len.WRST_ULT_COEFF_RIS;
        wrstUltCoeffAggRis.setWrstUltCoeffAggRisFromBuffer(buffer, position);
        position += WrstUltCoeffAggRis.Len.WRST_ULT_COEFF_AGG_RIS;
        wrstRisAcq.setWrstRisAcqFromBuffer(buffer, position);
        position += WrstRisAcq.Len.WRST_RIS_ACQ;
        wrstRisUti.setWrstRisUtiFromBuffer(buffer, position);
        position += WrstRisUti.Len.WRST_RIS_UTI;
        wrstRisMatEff.setWrstRisMatEffFromBuffer(buffer, position);
        position += WrstRisMatEff.Len.WRST_RIS_MAT_EFF;
        wrstRisRistorniCap.setWrstRisRistorniCapFromBuffer(buffer, position);
        position += WrstRisRistorniCap.Len.WRST_RIS_RISTORNI_CAP;
        wrstRisTrmBns.setWrstRisTrmBnsFromBuffer(buffer, position);
        position += WrstRisTrmBns.Len.WRST_RIS_TRM_BNS;
        wrstRisBnsric.setWrstRisBnsricFromBuffer(buffer, position);
        position += WrstRisBnsric.Len.WRST_RIS_BNSRIC;
        wrstDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRST_DS_RIGA, 0);
        position += Len.WRST_DS_RIGA;
        wrstDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wrstDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRST_DS_VER, 0);
        position += Len.WRST_DS_VER;
        wrstDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRST_DS_TS_INI_CPTZ, 0);
        position += Len.WRST_DS_TS_INI_CPTZ;
        wrstDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WRST_DS_TS_END_CPTZ, 0);
        position += Len.WRST_DS_TS_END_CPTZ;
        wrstDsUtente = MarshalByte.readString(buffer, position, Len.WRST_DS_UTENTE);
        position += Len.WRST_DS_UTENTE;
        wrstDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wrstIdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRST_ID_TRCH_DI_GAR, 0);
        position += Len.WRST_ID_TRCH_DI_GAR;
        wrstCodFnd = MarshalByte.readString(buffer, position, Len.WRST_COD_FND);
        position += Len.WRST_COD_FND;
        wrstIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRST_ID_POLI, 0);
        position += Len.WRST_ID_POLI;
        wrstIdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRST_ID_ADES, 0);
        position += Len.WRST_ID_ADES;
        wrstIdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WRST_ID_GAR, 0);
        position += Len.WRST_ID_GAR;
        wrstRisMinGarto.setWrstRisMinGartoFromBuffer(buffer, position);
        position += WrstRisMinGarto.Len.WRST_RIS_MIN_GARTO;
        wrstRisRshDflt.setWrstRisRshDfltFromBuffer(buffer, position);
        position += WrstRisRshDflt.Len.WRST_RIS_RSH_DFLT;
        wrstRisMoviNonInves.setWrstRisMoviNonInvesFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wrstIdRisDiTrch, Len.Int.WRST_ID_RIS_DI_TRCH, 0);
        position += Len.WRST_ID_RIS_DI_TRCH;
        MarshalByte.writeIntAsPacked(buffer, position, wrstIdMoviCrz, Len.Int.WRST_ID_MOVI_CRZ, 0);
        position += Len.WRST_ID_MOVI_CRZ;
        wrstIdMoviChiu.getWrstIdMoviChiuAsBuffer(buffer, position);
        position += WrstIdMoviChiu.Len.WRST_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wrstDtIniEff, Len.Int.WRST_DT_INI_EFF, 0);
        position += Len.WRST_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wrstDtEndEff, Len.Int.WRST_DT_END_EFF, 0);
        position += Len.WRST_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wrstCodCompAnia, Len.Int.WRST_COD_COMP_ANIA, 0);
        position += Len.WRST_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wrstTpCalcRis, Len.WRST_TP_CALC_RIS);
        position += Len.WRST_TP_CALC_RIS;
        wrstUltRm.getWrstUltRmAsBuffer(buffer, position);
        position += WrstUltRm.Len.WRST_ULT_RM;
        wrstDtCalc.getWrstDtCalcAsBuffer(buffer, position);
        position += WrstDtCalc.Len.WRST_DT_CALC;
        wrstDtElab.getWrstDtElabAsBuffer(buffer, position);
        position += WrstDtElab.Len.WRST_DT_ELAB;
        wrstRisBila.getWrstRisBilaAsBuffer(buffer, position);
        position += WrstRisBila.Len.WRST_RIS_BILA;
        wrstRisMat.getWrstRisMatAsBuffer(buffer, position);
        position += WrstRisMat.Len.WRST_RIS_MAT;
        wrstIncrXRival.getWrstIncrXRivalAsBuffer(buffer, position);
        position += WrstIncrXRival.Len.WRST_INCR_X_RIVAL;
        wrstRptoPre.getWrstRptoPreAsBuffer(buffer, position);
        position += WrstRptoPre.Len.WRST_RPTO_PRE;
        wrstFrazPrePp.getWrstFrazPrePpAsBuffer(buffer, position);
        position += WrstFrazPrePp.Len.WRST_FRAZ_PRE_PP;
        wrstRisTot.getWrstRisTotAsBuffer(buffer, position);
        position += WrstRisTot.Len.WRST_RIS_TOT;
        wrstRisSpe.getWrstRisSpeAsBuffer(buffer, position);
        position += WrstRisSpe.Len.WRST_RIS_SPE;
        wrstRisAbb.getWrstRisAbbAsBuffer(buffer, position);
        position += WrstRisAbb.Len.WRST_RIS_ABB;
        wrstRisBnsfdt.getWrstRisBnsfdtAsBuffer(buffer, position);
        position += WrstRisBnsfdt.Len.WRST_RIS_BNSFDT;
        wrstRisSopr.getWrstRisSoprAsBuffer(buffer, position);
        position += WrstRisSopr.Len.WRST_RIS_SOPR;
        wrstRisIntegBasTec.getWrstRisIntegBasTecAsBuffer(buffer, position);
        position += WrstRisIntegBasTec.Len.WRST_RIS_INTEG_BAS_TEC;
        wrstRisIntegDecrTs.getWrstRisIntegDecrTsAsBuffer(buffer, position);
        position += WrstRisIntegDecrTs.Len.WRST_RIS_INTEG_DECR_TS;
        wrstRisGarCasoMor.getWrstRisGarCasoMorAsBuffer(buffer, position);
        position += WrstRisGarCasoMor.Len.WRST_RIS_GAR_CASO_MOR;
        wrstRisZil.getWrstRisZilAsBuffer(buffer, position);
        position += WrstRisZil.Len.WRST_RIS_ZIL;
        wrstRisFaivl.getWrstRisFaivlAsBuffer(buffer, position);
        position += WrstRisFaivl.Len.WRST_RIS_FAIVL;
        wrstRisCosAmmtz.getWrstRisCosAmmtzAsBuffer(buffer, position);
        position += WrstRisCosAmmtz.Len.WRST_RIS_COS_AMMTZ;
        wrstRisSpeFaivl.getWrstRisSpeFaivlAsBuffer(buffer, position);
        position += WrstRisSpeFaivl.Len.WRST_RIS_SPE_FAIVL;
        wrstRisPrestFaivl.getWrstRisPrestFaivlAsBuffer(buffer, position);
        position += WrstRisPrestFaivl.Len.WRST_RIS_PREST_FAIVL;
        wrstRisComponAssva.getWrstRisComponAssvaAsBuffer(buffer, position);
        position += WrstRisComponAssva.Len.WRST_RIS_COMPON_ASSVA;
        wrstUltCoeffRis.getWrstUltCoeffRisAsBuffer(buffer, position);
        position += WrstUltCoeffRis.Len.WRST_ULT_COEFF_RIS;
        wrstUltCoeffAggRis.getWrstUltCoeffAggRisAsBuffer(buffer, position);
        position += WrstUltCoeffAggRis.Len.WRST_ULT_COEFF_AGG_RIS;
        wrstRisAcq.getWrstRisAcqAsBuffer(buffer, position);
        position += WrstRisAcq.Len.WRST_RIS_ACQ;
        wrstRisUti.getWrstRisUtiAsBuffer(buffer, position);
        position += WrstRisUti.Len.WRST_RIS_UTI;
        wrstRisMatEff.getWrstRisMatEffAsBuffer(buffer, position);
        position += WrstRisMatEff.Len.WRST_RIS_MAT_EFF;
        wrstRisRistorniCap.getWrstRisRistorniCapAsBuffer(buffer, position);
        position += WrstRisRistorniCap.Len.WRST_RIS_RISTORNI_CAP;
        wrstRisTrmBns.getWrstRisTrmBnsAsBuffer(buffer, position);
        position += WrstRisTrmBns.Len.WRST_RIS_TRM_BNS;
        wrstRisBnsric.getWrstRisBnsricAsBuffer(buffer, position);
        position += WrstRisBnsric.Len.WRST_RIS_BNSRIC;
        MarshalByte.writeLongAsPacked(buffer, position, wrstDsRiga, Len.Int.WRST_DS_RIGA, 0);
        position += Len.WRST_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wrstDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wrstDsVer, Len.Int.WRST_DS_VER, 0);
        position += Len.WRST_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wrstDsTsIniCptz, Len.Int.WRST_DS_TS_INI_CPTZ, 0);
        position += Len.WRST_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wrstDsTsEndCptz, Len.Int.WRST_DS_TS_END_CPTZ, 0);
        position += Len.WRST_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wrstDsUtente, Len.WRST_DS_UTENTE);
        position += Len.WRST_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wrstDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wrstIdTrchDiGar, Len.Int.WRST_ID_TRCH_DI_GAR, 0);
        position += Len.WRST_ID_TRCH_DI_GAR;
        MarshalByte.writeString(buffer, position, wrstCodFnd, Len.WRST_COD_FND);
        position += Len.WRST_COD_FND;
        MarshalByte.writeIntAsPacked(buffer, position, wrstIdPoli, Len.Int.WRST_ID_POLI, 0);
        position += Len.WRST_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wrstIdAdes, Len.Int.WRST_ID_ADES, 0);
        position += Len.WRST_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wrstIdGar, Len.Int.WRST_ID_GAR, 0);
        position += Len.WRST_ID_GAR;
        wrstRisMinGarto.getWrstRisMinGartoAsBuffer(buffer, position);
        position += WrstRisMinGarto.Len.WRST_RIS_MIN_GARTO;
        wrstRisRshDflt.getWrstRisRshDfltAsBuffer(buffer, position);
        position += WrstRisRshDflt.Len.WRST_RIS_RSH_DFLT;
        wrstRisMoviNonInves.getWrstRisMoviNonInvesAsBuffer(buffer, position);
        return buffer;
    }

    public void initDatiSpaces() {
        wrstIdRisDiTrch = Types.INVALID_INT_VAL;
        wrstIdMoviCrz = Types.INVALID_INT_VAL;
        wrstIdMoviChiu.initWrstIdMoviChiuSpaces();
        wrstDtIniEff = Types.INVALID_INT_VAL;
        wrstDtEndEff = Types.INVALID_INT_VAL;
        wrstCodCompAnia = Types.INVALID_INT_VAL;
        wrstTpCalcRis = "";
        wrstUltRm.initWrstUltRmSpaces();
        wrstDtCalc.initWrstDtCalcSpaces();
        wrstDtElab.initWrstDtElabSpaces();
        wrstRisBila.initWrstRisBilaSpaces();
        wrstRisMat.initWrstRisMatSpaces();
        wrstIncrXRival.initWrstIncrXRivalSpaces();
        wrstRptoPre.initWrstRptoPreSpaces();
        wrstFrazPrePp.initWrstFrazPrePpSpaces();
        wrstRisTot.initWrstRisTotSpaces();
        wrstRisSpe.initWrstRisSpeSpaces();
        wrstRisAbb.initWrstRisAbbSpaces();
        wrstRisBnsfdt.initWrstRisBnsfdtSpaces();
        wrstRisSopr.initWrstRisSoprSpaces();
        wrstRisIntegBasTec.initWrstRisIntegBasTecSpaces();
        wrstRisIntegDecrTs.initWrstRisIntegDecrTsSpaces();
        wrstRisGarCasoMor.initWrstRisGarCasoMorSpaces();
        wrstRisZil.initWrstRisZilSpaces();
        wrstRisFaivl.initWrstRisFaivlSpaces();
        wrstRisCosAmmtz.initWrstRisCosAmmtzSpaces();
        wrstRisSpeFaivl.initWrstRisSpeFaivlSpaces();
        wrstRisPrestFaivl.initWrstRisPrestFaivlSpaces();
        wrstRisComponAssva.initWrstRisComponAssvaSpaces();
        wrstUltCoeffRis.initWrstUltCoeffRisSpaces();
        wrstUltCoeffAggRis.initWrstUltCoeffAggRisSpaces();
        wrstRisAcq.initWrstRisAcqSpaces();
        wrstRisUti.initWrstRisUtiSpaces();
        wrstRisMatEff.initWrstRisMatEffSpaces();
        wrstRisRistorniCap.initWrstRisRistorniCapSpaces();
        wrstRisTrmBns.initWrstRisTrmBnsSpaces();
        wrstRisBnsric.initWrstRisBnsricSpaces();
        wrstDsRiga = Types.INVALID_LONG_VAL;
        wrstDsOperSql = Types.SPACE_CHAR;
        wrstDsVer = Types.INVALID_INT_VAL;
        wrstDsTsIniCptz = Types.INVALID_LONG_VAL;
        wrstDsTsEndCptz = Types.INVALID_LONG_VAL;
        wrstDsUtente = "";
        wrstDsStatoElab = Types.SPACE_CHAR;
        wrstIdTrchDiGar = Types.INVALID_INT_VAL;
        wrstCodFnd = "";
        wrstIdPoli = Types.INVALID_INT_VAL;
        wrstIdAdes = Types.INVALID_INT_VAL;
        wrstIdGar = Types.INVALID_INT_VAL;
        wrstRisMinGarto.initWrstRisMinGartoSpaces();
        wrstRisRshDflt.initWrstRisRshDfltSpaces();
        wrstRisMoviNonInves.initWrstRisMoviNonInvesSpaces();
    }

    public void setWrstIdRisDiTrch(int wrstIdRisDiTrch) {
        this.wrstIdRisDiTrch = wrstIdRisDiTrch;
    }

    public int getWrstIdRisDiTrch() {
        return this.wrstIdRisDiTrch;
    }

    public void setWrstIdMoviCrz(int wrstIdMoviCrz) {
        this.wrstIdMoviCrz = wrstIdMoviCrz;
    }

    public int getWrstIdMoviCrz() {
        return this.wrstIdMoviCrz;
    }

    public void setWrstDtIniEff(int wrstDtIniEff) {
        this.wrstDtIniEff = wrstDtIniEff;
    }

    public int getWrstDtIniEff() {
        return this.wrstDtIniEff;
    }

    public void setWrstDtEndEff(int wrstDtEndEff) {
        this.wrstDtEndEff = wrstDtEndEff;
    }

    public int getWrstDtEndEff() {
        return this.wrstDtEndEff;
    }

    public void setWrstCodCompAnia(int wrstCodCompAnia) {
        this.wrstCodCompAnia = wrstCodCompAnia;
    }

    public int getWrstCodCompAnia() {
        return this.wrstCodCompAnia;
    }

    public void setWrstTpCalcRis(String wrstTpCalcRis) {
        this.wrstTpCalcRis = Functions.subString(wrstTpCalcRis, Len.WRST_TP_CALC_RIS);
    }

    public String getWrstTpCalcRis() {
        return this.wrstTpCalcRis;
    }

    public void setWrstDsRiga(long wrstDsRiga) {
        this.wrstDsRiga = wrstDsRiga;
    }

    public long getWrstDsRiga() {
        return this.wrstDsRiga;
    }

    public void setWrstDsOperSql(char wrstDsOperSql) {
        this.wrstDsOperSql = wrstDsOperSql;
    }

    public char getWrstDsOperSql() {
        return this.wrstDsOperSql;
    }

    public void setWrstDsVer(int wrstDsVer) {
        this.wrstDsVer = wrstDsVer;
    }

    public int getWrstDsVer() {
        return this.wrstDsVer;
    }

    public void setWrstDsTsIniCptz(long wrstDsTsIniCptz) {
        this.wrstDsTsIniCptz = wrstDsTsIniCptz;
    }

    public long getWrstDsTsIniCptz() {
        return this.wrstDsTsIniCptz;
    }

    public void setWrstDsTsEndCptz(long wrstDsTsEndCptz) {
        this.wrstDsTsEndCptz = wrstDsTsEndCptz;
    }

    public long getWrstDsTsEndCptz() {
        return this.wrstDsTsEndCptz;
    }

    public void setWrstDsUtente(String wrstDsUtente) {
        this.wrstDsUtente = Functions.subString(wrstDsUtente, Len.WRST_DS_UTENTE);
    }

    public String getWrstDsUtente() {
        return this.wrstDsUtente;
    }

    public void setWrstDsStatoElab(char wrstDsStatoElab) {
        this.wrstDsStatoElab = wrstDsStatoElab;
    }

    public char getWrstDsStatoElab() {
        return this.wrstDsStatoElab;
    }

    public void setWrstIdTrchDiGar(int wrstIdTrchDiGar) {
        this.wrstIdTrchDiGar = wrstIdTrchDiGar;
    }

    public int getWrstIdTrchDiGar() {
        return this.wrstIdTrchDiGar;
    }

    public void setWrstCodFnd(String wrstCodFnd) {
        this.wrstCodFnd = Functions.subString(wrstCodFnd, Len.WRST_COD_FND);
    }

    public String getWrstCodFnd() {
        return this.wrstCodFnd;
    }

    public void setWrstIdPoli(int wrstIdPoli) {
        this.wrstIdPoli = wrstIdPoli;
    }

    public int getWrstIdPoli() {
        return this.wrstIdPoli;
    }

    public void setWrstIdAdes(int wrstIdAdes) {
        this.wrstIdAdes = wrstIdAdes;
    }

    public int getWrstIdAdes() {
        return this.wrstIdAdes;
    }

    public void setWrstIdGar(int wrstIdGar) {
        this.wrstIdGar = wrstIdGar;
    }

    public int getWrstIdGar() {
        return this.wrstIdGar;
    }

    public WrstDtCalc getWrstDtCalc() {
        return wrstDtCalc;
    }

    public WrstDtElab getWrstDtElab() {
        return wrstDtElab;
    }

    public WrstFrazPrePp getWrstFrazPrePp() {
        return wrstFrazPrePp;
    }

    public WrstIdMoviChiu getWrstIdMoviChiu() {
        return wrstIdMoviChiu;
    }

    public WrstIncrXRival getWrstIncrXRival() {
        return wrstIncrXRival;
    }

    public WrstRisAbb getWrstRisAbb() {
        return wrstRisAbb;
    }

    public WrstRisAcq getWrstRisAcq() {
        return wrstRisAcq;
    }

    public WrstRisBila getWrstRisBila() {
        return wrstRisBila;
    }

    public WrstRisBnsfdt getWrstRisBnsfdt() {
        return wrstRisBnsfdt;
    }

    public WrstRisBnsric getWrstRisBnsric() {
        return wrstRisBnsric;
    }

    public WrstRisComponAssva getWrstRisComponAssva() {
        return wrstRisComponAssva;
    }

    public WrstRisCosAmmtz getWrstRisCosAmmtz() {
        return wrstRisCosAmmtz;
    }

    public WrstRisFaivl getWrstRisFaivl() {
        return wrstRisFaivl;
    }

    public WrstRisGarCasoMor getWrstRisGarCasoMor() {
        return wrstRisGarCasoMor;
    }

    public WrstRisIntegBasTec getWrstRisIntegBasTec() {
        return wrstRisIntegBasTec;
    }

    public WrstRisIntegDecrTs getWrstRisIntegDecrTs() {
        return wrstRisIntegDecrTs;
    }

    public WrstRisMat getWrstRisMat() {
        return wrstRisMat;
    }

    public WrstRisMatEff getWrstRisMatEff() {
        return wrstRisMatEff;
    }

    public WrstRisMinGarto getWrstRisMinGarto() {
        return wrstRisMinGarto;
    }

    public WrstRisMoviNonInves getWrstRisMoviNonInves() {
        return wrstRisMoviNonInves;
    }

    public WrstRisPrestFaivl getWrstRisPrestFaivl() {
        return wrstRisPrestFaivl;
    }

    public WrstRisRistorniCap getWrstRisRistorniCap() {
        return wrstRisRistorniCap;
    }

    public WrstRisRshDflt getWrstRisRshDflt() {
        return wrstRisRshDflt;
    }

    public WrstRisSopr getWrstRisSopr() {
        return wrstRisSopr;
    }

    public WrstRisSpe getWrstRisSpe() {
        return wrstRisSpe;
    }

    public WrstRisSpeFaivl getWrstRisSpeFaivl() {
        return wrstRisSpeFaivl;
    }

    public WrstRisTot getWrstRisTot() {
        return wrstRisTot;
    }

    public WrstRisTrmBns getWrstRisTrmBns() {
        return wrstRisTrmBns;
    }

    public WrstRisUti getWrstRisUti() {
        return wrstRisUti;
    }

    public WrstRisZil getWrstRisZil() {
        return wrstRisZil;
    }

    public WrstRptoPre getWrstRptoPre() {
        return wrstRptoPre;
    }

    public WrstUltCoeffAggRis getWrstUltCoeffAggRis() {
        return wrstUltCoeffAggRis;
    }

    public WrstUltCoeffRis getWrstUltCoeffRis() {
        return wrstUltCoeffRis;
    }

    public WrstUltRm getWrstUltRm() {
        return wrstUltRm;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_ID_RIS_DI_TRCH = 5;
        public static final int WRST_ID_MOVI_CRZ = 5;
        public static final int WRST_DT_INI_EFF = 5;
        public static final int WRST_DT_END_EFF = 5;
        public static final int WRST_COD_COMP_ANIA = 3;
        public static final int WRST_TP_CALC_RIS = 2;
        public static final int WRST_DS_RIGA = 6;
        public static final int WRST_DS_OPER_SQL = 1;
        public static final int WRST_DS_VER = 5;
        public static final int WRST_DS_TS_INI_CPTZ = 10;
        public static final int WRST_DS_TS_END_CPTZ = 10;
        public static final int WRST_DS_UTENTE = 20;
        public static final int WRST_DS_STATO_ELAB = 1;
        public static final int WRST_ID_TRCH_DI_GAR = 5;
        public static final int WRST_COD_FND = 12;
        public static final int WRST_ID_POLI = 5;
        public static final int WRST_ID_ADES = 5;
        public static final int WRST_ID_GAR = 5;
        public static final int DATI = WRST_ID_RIS_DI_TRCH + WRST_ID_MOVI_CRZ + WrstIdMoviChiu.Len.WRST_ID_MOVI_CHIU + WRST_DT_INI_EFF + WRST_DT_END_EFF + WRST_COD_COMP_ANIA + WRST_TP_CALC_RIS + WrstUltRm.Len.WRST_ULT_RM + WrstDtCalc.Len.WRST_DT_CALC + WrstDtElab.Len.WRST_DT_ELAB + WrstRisBila.Len.WRST_RIS_BILA + WrstRisMat.Len.WRST_RIS_MAT + WrstIncrXRival.Len.WRST_INCR_X_RIVAL + WrstRptoPre.Len.WRST_RPTO_PRE + WrstFrazPrePp.Len.WRST_FRAZ_PRE_PP + WrstRisTot.Len.WRST_RIS_TOT + WrstRisSpe.Len.WRST_RIS_SPE + WrstRisAbb.Len.WRST_RIS_ABB + WrstRisBnsfdt.Len.WRST_RIS_BNSFDT + WrstRisSopr.Len.WRST_RIS_SOPR + WrstRisIntegBasTec.Len.WRST_RIS_INTEG_BAS_TEC + WrstRisIntegDecrTs.Len.WRST_RIS_INTEG_DECR_TS + WrstRisGarCasoMor.Len.WRST_RIS_GAR_CASO_MOR + WrstRisZil.Len.WRST_RIS_ZIL + WrstRisFaivl.Len.WRST_RIS_FAIVL + WrstRisCosAmmtz.Len.WRST_RIS_COS_AMMTZ + WrstRisSpeFaivl.Len.WRST_RIS_SPE_FAIVL + WrstRisPrestFaivl.Len.WRST_RIS_PREST_FAIVL + WrstRisComponAssva.Len.WRST_RIS_COMPON_ASSVA + WrstUltCoeffRis.Len.WRST_ULT_COEFF_RIS + WrstUltCoeffAggRis.Len.WRST_ULT_COEFF_AGG_RIS + WrstRisAcq.Len.WRST_RIS_ACQ + WrstRisUti.Len.WRST_RIS_UTI + WrstRisMatEff.Len.WRST_RIS_MAT_EFF + WrstRisRistorniCap.Len.WRST_RIS_RISTORNI_CAP + WrstRisTrmBns.Len.WRST_RIS_TRM_BNS + WrstRisBnsric.Len.WRST_RIS_BNSRIC + WRST_DS_RIGA + WRST_DS_OPER_SQL + WRST_DS_VER + WRST_DS_TS_INI_CPTZ + WRST_DS_TS_END_CPTZ + WRST_DS_UTENTE + WRST_DS_STATO_ELAB + WRST_ID_TRCH_DI_GAR + WRST_COD_FND + WRST_ID_POLI + WRST_ID_ADES + WRST_ID_GAR + WrstRisMinGarto.Len.WRST_RIS_MIN_GARTO + WrstRisRshDflt.Len.WRST_RIS_RSH_DFLT + WrstRisMoviNonInves.Len.WRST_RIS_MOVI_NON_INVES;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_ID_RIS_DI_TRCH = 9;
            public static final int WRST_ID_MOVI_CRZ = 9;
            public static final int WRST_DT_INI_EFF = 8;
            public static final int WRST_DT_END_EFF = 8;
            public static final int WRST_COD_COMP_ANIA = 5;
            public static final int WRST_DS_RIGA = 10;
            public static final int WRST_DS_VER = 9;
            public static final int WRST_DS_TS_INI_CPTZ = 18;
            public static final int WRST_DS_TS_END_CPTZ = 18;
            public static final int WRST_ID_TRCH_DI_GAR = 9;
            public static final int WRST_ID_POLI = 9;
            public static final int WRST_ID_ADES = 9;
            public static final int WRST_ID_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
