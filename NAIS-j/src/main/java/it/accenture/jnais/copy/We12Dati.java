package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.We12AccprePag;
import it.accenture.jnais.ws.redefines.We12CumPreAtt;
import it.accenture.jnais.ws.redefines.We12CumPrstz;
import it.accenture.jnais.ws.redefines.We12DtEmis;
import it.accenture.jnais.ws.redefines.We12IdMoviChiu;

/**Original name: WE12-DATI<br>
 * Variable: WE12-DATI from copybook LCCVE121<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class We12Dati {

    //==== PROPERTIES ====
    //Original name: WE12-ID-EST-TRCH-DI-GAR
    private int we12IdEstTrchDiGar = DefaultValues.INT_VAL;
    //Original name: WE12-ID-TRCH-DI-GAR
    private int we12IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: WE12-ID-GAR
    private int we12IdGar = DefaultValues.INT_VAL;
    //Original name: WE12-ID-ADES
    private int we12IdAdes = DefaultValues.INT_VAL;
    //Original name: WE12-ID-POLI
    private int we12IdPoli = DefaultValues.INT_VAL;
    //Original name: WE12-ID-MOVI-CRZ
    private int we12IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WE12-ID-MOVI-CHIU
    private We12IdMoviChiu we12IdMoviChiu = new We12IdMoviChiu();
    //Original name: WE12-DT-INI-EFF
    private int we12DtIniEff = DefaultValues.INT_VAL;
    //Original name: WE12-DT-END-EFF
    private int we12DtEndEff = DefaultValues.INT_VAL;
    //Original name: WE12-COD-COMP-ANIA
    private int we12CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WE12-DT-EMIS
    private We12DtEmis we12DtEmis = new We12DtEmis();
    //Original name: WE12-CUM-PRE-ATT
    private We12CumPreAtt we12CumPreAtt = new We12CumPreAtt();
    //Original name: WE12-ACCPRE-PAG
    private We12AccprePag we12AccprePag = new We12AccprePag();
    //Original name: WE12-CUM-PRSTZ
    private We12CumPrstz we12CumPrstz = new We12CumPrstz();
    //Original name: WE12-DS-RIGA
    private long we12DsRiga = DefaultValues.LONG_VAL;
    //Original name: WE12-DS-OPER-SQL
    private char we12DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WE12-DS-VER
    private int we12DsVer = DefaultValues.INT_VAL;
    //Original name: WE12-DS-TS-INI-CPTZ
    private long we12DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WE12-DS-TS-END-CPTZ
    private long we12DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WE12-DS-UTENTE
    private String we12DsUtente = DefaultValues.stringVal(Len.WE12_DS_UTENTE);
    //Original name: WE12-DS-STATO-ELAB
    private char we12DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WE12-TP-TRCH
    private String we12TpTrch = DefaultValues.stringVal(Len.WE12_TP_TRCH);
    //Original name: WE12-CAUS-SCON
    private String we12CausScon = DefaultValues.stringVal(Len.WE12_CAUS_SCON);

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, we12IdEstTrchDiGar, Len.Int.WE12_ID_EST_TRCH_DI_GAR, 0);
        position += Len.WE12_ID_EST_TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, we12IdTrchDiGar, Len.Int.WE12_ID_TRCH_DI_GAR, 0);
        position += Len.WE12_ID_TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, we12IdGar, Len.Int.WE12_ID_GAR, 0);
        position += Len.WE12_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, we12IdAdes, Len.Int.WE12_ID_ADES, 0);
        position += Len.WE12_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, we12IdPoli, Len.Int.WE12_ID_POLI, 0);
        position += Len.WE12_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, we12IdMoviCrz, Len.Int.WE12_ID_MOVI_CRZ, 0);
        position += Len.WE12_ID_MOVI_CRZ;
        we12IdMoviChiu.getWe12IdMoviChiuAsBuffer(buffer, position);
        position += We12IdMoviChiu.Len.WE12_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, we12DtIniEff, Len.Int.WE12_DT_INI_EFF, 0);
        position += Len.WE12_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, we12DtEndEff, Len.Int.WE12_DT_END_EFF, 0);
        position += Len.WE12_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, we12CodCompAnia, Len.Int.WE12_COD_COMP_ANIA, 0);
        position += Len.WE12_COD_COMP_ANIA;
        we12DtEmis.getWe12DtEmisAsBuffer(buffer, position);
        position += We12DtEmis.Len.WE12_DT_EMIS;
        we12CumPreAtt.getWe12CumPreAttAsBuffer(buffer, position);
        position += We12CumPreAtt.Len.WE12_CUM_PRE_ATT;
        we12AccprePag.getWe12AccprePagAsBuffer(buffer, position);
        position += We12AccprePag.Len.WE12_ACCPRE_PAG;
        we12CumPrstz.getWe12CumPrstzAsBuffer(buffer, position);
        position += We12CumPrstz.Len.WE12_CUM_PRSTZ;
        MarshalByte.writeLongAsPacked(buffer, position, we12DsRiga, Len.Int.WE12_DS_RIGA, 0);
        position += Len.WE12_DS_RIGA;
        MarshalByte.writeChar(buffer, position, we12DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, we12DsVer, Len.Int.WE12_DS_VER, 0);
        position += Len.WE12_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, we12DsTsIniCptz, Len.Int.WE12_DS_TS_INI_CPTZ, 0);
        position += Len.WE12_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, we12DsTsEndCptz, Len.Int.WE12_DS_TS_END_CPTZ, 0);
        position += Len.WE12_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, we12DsUtente, Len.WE12_DS_UTENTE);
        position += Len.WE12_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, we12DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, we12TpTrch, Len.WE12_TP_TRCH);
        position += Len.WE12_TP_TRCH;
        MarshalByte.writeString(buffer, position, we12CausScon, Len.WE12_CAUS_SCON);
        return buffer;
    }

    public void setWe12IdEstTrchDiGar(int we12IdEstTrchDiGar) {
        this.we12IdEstTrchDiGar = we12IdEstTrchDiGar;
    }

    public int getWe12IdEstTrchDiGar() {
        return this.we12IdEstTrchDiGar;
    }

    public void setWe12IdTrchDiGar(int we12IdTrchDiGar) {
        this.we12IdTrchDiGar = we12IdTrchDiGar;
    }

    public int getWe12IdTrchDiGar() {
        return this.we12IdTrchDiGar;
    }

    public void setWe12IdGar(int we12IdGar) {
        this.we12IdGar = we12IdGar;
    }

    public int getWe12IdGar() {
        return this.we12IdGar;
    }

    public void setWe12IdAdes(int we12IdAdes) {
        this.we12IdAdes = we12IdAdes;
    }

    public int getWe12IdAdes() {
        return this.we12IdAdes;
    }

    public void setWe12IdPoli(int we12IdPoli) {
        this.we12IdPoli = we12IdPoli;
    }

    public int getWe12IdPoli() {
        return this.we12IdPoli;
    }

    public void setWe12IdMoviCrz(int we12IdMoviCrz) {
        this.we12IdMoviCrz = we12IdMoviCrz;
    }

    public int getWe12IdMoviCrz() {
        return this.we12IdMoviCrz;
    }

    public void setWe12DtIniEff(int we12DtIniEff) {
        this.we12DtIniEff = we12DtIniEff;
    }

    public int getWe12DtIniEff() {
        return this.we12DtIniEff;
    }

    public void setWe12DtEndEff(int we12DtEndEff) {
        this.we12DtEndEff = we12DtEndEff;
    }

    public int getWe12DtEndEff() {
        return this.we12DtEndEff;
    }

    public void setWe12CodCompAnia(int we12CodCompAnia) {
        this.we12CodCompAnia = we12CodCompAnia;
    }

    public int getWe12CodCompAnia() {
        return this.we12CodCompAnia;
    }

    public void setWe12DsRiga(long we12DsRiga) {
        this.we12DsRiga = we12DsRiga;
    }

    public long getWe12DsRiga() {
        return this.we12DsRiga;
    }

    public void setWe12DsOperSql(char we12DsOperSql) {
        this.we12DsOperSql = we12DsOperSql;
    }

    public char getWe12DsOperSql() {
        return this.we12DsOperSql;
    }

    public void setWe12DsVer(int we12DsVer) {
        this.we12DsVer = we12DsVer;
    }

    public int getWe12DsVer() {
        return this.we12DsVer;
    }

    public void setWe12DsTsIniCptz(long we12DsTsIniCptz) {
        this.we12DsTsIniCptz = we12DsTsIniCptz;
    }

    public long getWe12DsTsIniCptz() {
        return this.we12DsTsIniCptz;
    }

    public void setWe12DsTsEndCptz(long we12DsTsEndCptz) {
        this.we12DsTsEndCptz = we12DsTsEndCptz;
    }

    public long getWe12DsTsEndCptz() {
        return this.we12DsTsEndCptz;
    }

    public void setWe12DsUtente(String we12DsUtente) {
        this.we12DsUtente = Functions.subString(we12DsUtente, Len.WE12_DS_UTENTE);
    }

    public String getWe12DsUtente() {
        return this.we12DsUtente;
    }

    public void setWe12DsStatoElab(char we12DsStatoElab) {
        this.we12DsStatoElab = we12DsStatoElab;
    }

    public char getWe12DsStatoElab() {
        return this.we12DsStatoElab;
    }

    public void setWe12TpTrch(String we12TpTrch) {
        this.we12TpTrch = Functions.subString(we12TpTrch, Len.WE12_TP_TRCH);
    }

    public String getWe12TpTrch() {
        return this.we12TpTrch;
    }

    public void setWe12CausScon(String we12CausScon) {
        this.we12CausScon = Functions.subString(we12CausScon, Len.WE12_CAUS_SCON);
    }

    public String getWe12CausScon() {
        return this.we12CausScon;
    }

    public We12AccprePag getWe12AccprePag() {
        return we12AccprePag;
    }

    public We12CumPreAtt getWe12CumPreAtt() {
        return we12CumPreAtt;
    }

    public We12CumPrstz getWe12CumPrstz() {
        return we12CumPrstz;
    }

    public We12DtEmis getWe12DtEmis() {
        return we12DtEmis;
    }

    public We12IdMoviChiu getWe12IdMoviChiu() {
        return we12IdMoviChiu;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WE12_DS_UTENTE = 20;
        public static final int WE12_TP_TRCH = 2;
        public static final int WE12_CAUS_SCON = 12;
        public static final int WE12_ID_EST_TRCH_DI_GAR = 5;
        public static final int WE12_ID_TRCH_DI_GAR = 5;
        public static final int WE12_ID_GAR = 5;
        public static final int WE12_ID_ADES = 5;
        public static final int WE12_ID_POLI = 5;
        public static final int WE12_ID_MOVI_CRZ = 5;
        public static final int WE12_DT_INI_EFF = 5;
        public static final int WE12_DT_END_EFF = 5;
        public static final int WE12_COD_COMP_ANIA = 3;
        public static final int WE12_DS_RIGA = 6;
        public static final int WE12_DS_OPER_SQL = 1;
        public static final int WE12_DS_VER = 5;
        public static final int WE12_DS_TS_INI_CPTZ = 10;
        public static final int WE12_DS_TS_END_CPTZ = 10;
        public static final int WE12_DS_STATO_ELAB = 1;
        public static final int DATI = WE12_ID_EST_TRCH_DI_GAR + WE12_ID_TRCH_DI_GAR + WE12_ID_GAR + WE12_ID_ADES + WE12_ID_POLI + WE12_ID_MOVI_CRZ + We12IdMoviChiu.Len.WE12_ID_MOVI_CHIU + WE12_DT_INI_EFF + WE12_DT_END_EFF + WE12_COD_COMP_ANIA + We12DtEmis.Len.WE12_DT_EMIS + We12CumPreAtt.Len.WE12_CUM_PRE_ATT + We12AccprePag.Len.WE12_ACCPRE_PAG + We12CumPrstz.Len.WE12_CUM_PRSTZ + WE12_DS_RIGA + WE12_DS_OPER_SQL + WE12_DS_VER + WE12_DS_TS_INI_CPTZ + WE12_DS_TS_END_CPTZ + WE12_DS_UTENTE + WE12_DS_STATO_ELAB + WE12_TP_TRCH + WE12_CAUS_SCON;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WE12_ID_EST_TRCH_DI_GAR = 9;
            public static final int WE12_ID_TRCH_DI_GAR = 9;
            public static final int WE12_ID_GAR = 9;
            public static final int WE12_ID_ADES = 9;
            public static final int WE12_ID_POLI = 9;
            public static final int WE12_ID_MOVI_CRZ = 9;
            public static final int WE12_DT_INI_EFF = 8;
            public static final int WE12_DT_END_EFF = 8;
            public static final int WE12_COD_COMP_ANIA = 5;
            public static final int WE12_DS_RIGA = 10;
            public static final int WE12_DS_VER = 9;
            public static final int WE12_DS_TS_INI_CPTZ = 18;
            public static final int WE12_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
