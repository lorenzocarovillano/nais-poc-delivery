package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Wl23CptVinctoPign;
import it.accenture.jnais.ws.redefines.Wl23DtAttivVinpg;
import it.accenture.jnais.ws.redefines.Wl23DtChiuVinpg;
import it.accenture.jnais.ws.redefines.Wl23DtNotificaBlocco;
import it.accenture.jnais.ws.redefines.Wl23DtProvvSeq;
import it.accenture.jnais.ws.redefines.Wl23IdMoviChiu;
import it.accenture.jnais.ws.redefines.Wl23NumProvvSeq;
import it.accenture.jnais.ws.redefines.Wl23SomPreVinpg;
import it.accenture.jnais.ws.redefines.Wl23ValRiscEndVinpg;
import it.accenture.jnais.ws.redefines.Wl23ValRiscIniVinpg;

/**Original name: WL23-DATI<br>
 * Variable: WL23-DATI from copybook LCCVL231<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wl23Dati {

    //==== PROPERTIES ====
    //Original name: WL23-ID-VINC-PEG
    private int wl23IdVincPeg = DefaultValues.INT_VAL;
    //Original name: WL23-ID-MOVI-CRZ
    private int wl23IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WL23-ID-MOVI-CHIU
    private Wl23IdMoviChiu wl23IdMoviChiu = new Wl23IdMoviChiu();
    //Original name: WL23-DT-INI-EFF
    private int wl23DtIniEff = DefaultValues.INT_VAL;
    //Original name: WL23-DT-END-EFF
    private int wl23DtEndEff = DefaultValues.INT_VAL;
    //Original name: WL23-COD-COMP-ANIA
    private int wl23CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WL23-ID-RAPP-ANA
    private int wl23IdRappAna = DefaultValues.INT_VAL;
    //Original name: WL23-TP-VINC
    private String wl23TpVinc = DefaultValues.stringVal(Len.WL23_TP_VINC);
    //Original name: WL23-FL-DELEGA-AL-RISC
    private char wl23FlDelegaAlRisc = DefaultValues.CHAR_VAL;
    //Original name: WL23-DESC
    private String wl23Desc = DefaultValues.stringVal(Len.WL23_DESC);
    //Original name: WL23-DT-ATTIV-VINPG
    private Wl23DtAttivVinpg wl23DtAttivVinpg = new Wl23DtAttivVinpg();
    //Original name: WL23-CPT-VINCTO-PIGN
    private Wl23CptVinctoPign wl23CptVinctoPign = new Wl23CptVinctoPign();
    //Original name: WL23-DT-CHIU-VINPG
    private Wl23DtChiuVinpg wl23DtChiuVinpg = new Wl23DtChiuVinpg();
    //Original name: WL23-VAL-RISC-INI-VINPG
    private Wl23ValRiscIniVinpg wl23ValRiscIniVinpg = new Wl23ValRiscIniVinpg();
    //Original name: WL23-VAL-RISC-END-VINPG
    private Wl23ValRiscEndVinpg wl23ValRiscEndVinpg = new Wl23ValRiscEndVinpg();
    //Original name: WL23-SOM-PRE-VINPG
    private Wl23SomPreVinpg wl23SomPreVinpg = new Wl23SomPreVinpg();
    //Original name: WL23-FL-VINPG-INT-PRSTZ
    private char wl23FlVinpgIntPrstz = DefaultValues.CHAR_VAL;
    //Original name: WL23-DESC-AGG-VINC
    private String wl23DescAggVinc = DefaultValues.stringVal(Len.WL23_DESC_AGG_VINC);
    //Original name: WL23-DS-RIGA
    private long wl23DsRiga = DefaultValues.LONG_VAL;
    //Original name: WL23-DS-OPER-SQL
    private char wl23DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WL23-DS-VER
    private int wl23DsVer = DefaultValues.INT_VAL;
    //Original name: WL23-DS-TS-INI-CPTZ
    private long wl23DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WL23-DS-TS-END-CPTZ
    private long wl23DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WL23-DS-UTENTE
    private String wl23DsUtente = DefaultValues.stringVal(Len.WL23_DS_UTENTE);
    //Original name: WL23-DS-STATO-ELAB
    private char wl23DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WL23-TP-AUT-SEQ
    private String wl23TpAutSeq = DefaultValues.stringVal(Len.WL23_TP_AUT_SEQ);
    //Original name: WL23-COD-UFF-SEQ
    private String wl23CodUffSeq = DefaultValues.stringVal(Len.WL23_COD_UFF_SEQ);
    //Original name: WL23-NUM-PROVV-SEQ
    private Wl23NumProvvSeq wl23NumProvvSeq = new Wl23NumProvvSeq();
    //Original name: WL23-TP-PROVV-SEQ
    private String wl23TpProvvSeq = DefaultValues.stringVal(Len.WL23_TP_PROVV_SEQ);
    //Original name: WL23-DT-PROVV-SEQ
    private Wl23DtProvvSeq wl23DtProvvSeq = new Wl23DtProvvSeq();
    //Original name: WL23-DT-NOTIFICA-BLOCCO
    private Wl23DtNotificaBlocco wl23DtNotificaBlocco = new Wl23DtNotificaBlocco();
    //Original name: WL23-NOTA-PROVV
    private String wl23NotaProvv = DefaultValues.stringVal(Len.WL23_NOTA_PROVV);

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wl23IdVincPeg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL23_ID_VINC_PEG, 0);
        position += Len.WL23_ID_VINC_PEG;
        wl23IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL23_ID_MOVI_CRZ, 0);
        position += Len.WL23_ID_MOVI_CRZ;
        wl23IdMoviChiu.setWl23IdMoviChiuFromBuffer(buffer, position);
        position += Wl23IdMoviChiu.Len.WL23_ID_MOVI_CHIU;
        wl23DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL23_DT_INI_EFF, 0);
        position += Len.WL23_DT_INI_EFF;
        wl23DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL23_DT_END_EFF, 0);
        position += Len.WL23_DT_END_EFF;
        wl23CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL23_COD_COMP_ANIA, 0);
        position += Len.WL23_COD_COMP_ANIA;
        wl23IdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL23_ID_RAPP_ANA, 0);
        position += Len.WL23_ID_RAPP_ANA;
        wl23TpVinc = MarshalByte.readString(buffer, position, Len.WL23_TP_VINC);
        position += Len.WL23_TP_VINC;
        wl23FlDelegaAlRisc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wl23Desc = MarshalByte.readString(buffer, position, Len.WL23_DESC);
        position += Len.WL23_DESC;
        wl23DtAttivVinpg.setWl23DtAttivVinpgFromBuffer(buffer, position);
        position += Wl23DtAttivVinpg.Len.WL23_DT_ATTIV_VINPG;
        wl23CptVinctoPign.setWl23CptVinctoPignFromBuffer(buffer, position);
        position += Wl23CptVinctoPign.Len.WL23_CPT_VINCTO_PIGN;
        wl23DtChiuVinpg.setWl23DtChiuVinpgFromBuffer(buffer, position);
        position += Wl23DtChiuVinpg.Len.WL23_DT_CHIU_VINPG;
        wl23ValRiscIniVinpg.setWl23ValRiscIniVinpgFromBuffer(buffer, position);
        position += Wl23ValRiscIniVinpg.Len.WL23_VAL_RISC_INI_VINPG;
        wl23ValRiscEndVinpg.setWl23ValRiscEndVinpgFromBuffer(buffer, position);
        position += Wl23ValRiscEndVinpg.Len.WL23_VAL_RISC_END_VINPG;
        wl23SomPreVinpg.setWl23SomPreVinpgFromBuffer(buffer, position);
        position += Wl23SomPreVinpg.Len.WL23_SOM_PRE_VINPG;
        wl23FlVinpgIntPrstz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wl23DescAggVinc = MarshalByte.readString(buffer, position, Len.WL23_DESC_AGG_VINC);
        position += Len.WL23_DESC_AGG_VINC;
        wl23DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WL23_DS_RIGA, 0);
        position += Len.WL23_DS_RIGA;
        wl23DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wl23DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WL23_DS_VER, 0);
        position += Len.WL23_DS_VER;
        wl23DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WL23_DS_TS_INI_CPTZ, 0);
        position += Len.WL23_DS_TS_INI_CPTZ;
        wl23DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WL23_DS_TS_END_CPTZ, 0);
        position += Len.WL23_DS_TS_END_CPTZ;
        wl23DsUtente = MarshalByte.readString(buffer, position, Len.WL23_DS_UTENTE);
        position += Len.WL23_DS_UTENTE;
        wl23DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wl23TpAutSeq = MarshalByte.readString(buffer, position, Len.WL23_TP_AUT_SEQ);
        position += Len.WL23_TP_AUT_SEQ;
        wl23CodUffSeq = MarshalByte.readString(buffer, position, Len.WL23_COD_UFF_SEQ);
        position += Len.WL23_COD_UFF_SEQ;
        wl23NumProvvSeq.setWl23NumProvvSeqFromBuffer(buffer, position);
        position += Wl23NumProvvSeq.Len.WL23_NUM_PROVV_SEQ;
        wl23TpProvvSeq = MarshalByte.readString(buffer, position, Len.WL23_TP_PROVV_SEQ);
        position += Len.WL23_TP_PROVV_SEQ;
        wl23DtProvvSeq.setWl23DtProvvSeqFromBuffer(buffer, position);
        position += Wl23DtProvvSeq.Len.WL23_DT_PROVV_SEQ;
        wl23DtNotificaBlocco.setWl23DtNotificaBloccoFromBuffer(buffer, position);
        position += Wl23DtNotificaBlocco.Len.WL23_DT_NOTIFICA_BLOCCO;
        wl23NotaProvv = MarshalByte.readString(buffer, position, Len.WL23_NOTA_PROVV);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wl23IdVincPeg, Len.Int.WL23_ID_VINC_PEG, 0);
        position += Len.WL23_ID_VINC_PEG;
        MarshalByte.writeIntAsPacked(buffer, position, wl23IdMoviCrz, Len.Int.WL23_ID_MOVI_CRZ, 0);
        position += Len.WL23_ID_MOVI_CRZ;
        wl23IdMoviChiu.getWl23IdMoviChiuAsBuffer(buffer, position);
        position += Wl23IdMoviChiu.Len.WL23_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wl23DtIniEff, Len.Int.WL23_DT_INI_EFF, 0);
        position += Len.WL23_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wl23DtEndEff, Len.Int.WL23_DT_END_EFF, 0);
        position += Len.WL23_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wl23CodCompAnia, Len.Int.WL23_COD_COMP_ANIA, 0);
        position += Len.WL23_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wl23IdRappAna, Len.Int.WL23_ID_RAPP_ANA, 0);
        position += Len.WL23_ID_RAPP_ANA;
        MarshalByte.writeString(buffer, position, wl23TpVinc, Len.WL23_TP_VINC);
        position += Len.WL23_TP_VINC;
        MarshalByte.writeChar(buffer, position, wl23FlDelegaAlRisc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wl23Desc, Len.WL23_DESC);
        position += Len.WL23_DESC;
        wl23DtAttivVinpg.getWl23DtAttivVinpgAsBuffer(buffer, position);
        position += Wl23DtAttivVinpg.Len.WL23_DT_ATTIV_VINPG;
        wl23CptVinctoPign.getWl23CptVinctoPignAsBuffer(buffer, position);
        position += Wl23CptVinctoPign.Len.WL23_CPT_VINCTO_PIGN;
        wl23DtChiuVinpg.getWl23DtChiuVinpgAsBuffer(buffer, position);
        position += Wl23DtChiuVinpg.Len.WL23_DT_CHIU_VINPG;
        wl23ValRiscIniVinpg.getWl23ValRiscIniVinpgAsBuffer(buffer, position);
        position += Wl23ValRiscIniVinpg.Len.WL23_VAL_RISC_INI_VINPG;
        wl23ValRiscEndVinpg.getWl23ValRiscEndVinpgAsBuffer(buffer, position);
        position += Wl23ValRiscEndVinpg.Len.WL23_VAL_RISC_END_VINPG;
        wl23SomPreVinpg.getWl23SomPreVinpgAsBuffer(buffer, position);
        position += Wl23SomPreVinpg.Len.WL23_SOM_PRE_VINPG;
        MarshalByte.writeChar(buffer, position, wl23FlVinpgIntPrstz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wl23DescAggVinc, Len.WL23_DESC_AGG_VINC);
        position += Len.WL23_DESC_AGG_VINC;
        MarshalByte.writeLongAsPacked(buffer, position, wl23DsRiga, Len.Int.WL23_DS_RIGA, 0);
        position += Len.WL23_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wl23DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wl23DsVer, Len.Int.WL23_DS_VER, 0);
        position += Len.WL23_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wl23DsTsIniCptz, Len.Int.WL23_DS_TS_INI_CPTZ, 0);
        position += Len.WL23_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wl23DsTsEndCptz, Len.Int.WL23_DS_TS_END_CPTZ, 0);
        position += Len.WL23_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wl23DsUtente, Len.WL23_DS_UTENTE);
        position += Len.WL23_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wl23DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wl23TpAutSeq, Len.WL23_TP_AUT_SEQ);
        position += Len.WL23_TP_AUT_SEQ;
        MarshalByte.writeString(buffer, position, wl23CodUffSeq, Len.WL23_COD_UFF_SEQ);
        position += Len.WL23_COD_UFF_SEQ;
        wl23NumProvvSeq.getWl23NumProvvSeqAsBuffer(buffer, position);
        position += Wl23NumProvvSeq.Len.WL23_NUM_PROVV_SEQ;
        MarshalByte.writeString(buffer, position, wl23TpProvvSeq, Len.WL23_TP_PROVV_SEQ);
        position += Len.WL23_TP_PROVV_SEQ;
        wl23DtProvvSeq.getWl23DtProvvSeqAsBuffer(buffer, position);
        position += Wl23DtProvvSeq.Len.WL23_DT_PROVV_SEQ;
        wl23DtNotificaBlocco.getWl23DtNotificaBloccoAsBuffer(buffer, position);
        position += Wl23DtNotificaBlocco.Len.WL23_DT_NOTIFICA_BLOCCO;
        MarshalByte.writeString(buffer, position, wl23NotaProvv, Len.WL23_NOTA_PROVV);
        return buffer;
    }

    public void initDatiSpaces() {
        wl23IdVincPeg = Types.INVALID_INT_VAL;
        wl23IdMoviCrz = Types.INVALID_INT_VAL;
        wl23IdMoviChiu.initWl23IdMoviChiuSpaces();
        wl23DtIniEff = Types.INVALID_INT_VAL;
        wl23DtEndEff = Types.INVALID_INT_VAL;
        wl23CodCompAnia = Types.INVALID_INT_VAL;
        wl23IdRappAna = Types.INVALID_INT_VAL;
        wl23TpVinc = "";
        wl23FlDelegaAlRisc = Types.SPACE_CHAR;
        wl23Desc = "";
        wl23DtAttivVinpg.initWl23DtAttivVinpgSpaces();
        wl23CptVinctoPign.initWl23CptVinctoPignSpaces();
        wl23DtChiuVinpg.initWl23DtChiuVinpgSpaces();
        wl23ValRiscIniVinpg.initWl23ValRiscIniVinpgSpaces();
        wl23ValRiscEndVinpg.initWl23ValRiscEndVinpgSpaces();
        wl23SomPreVinpg.initWl23SomPreVinpgSpaces();
        wl23FlVinpgIntPrstz = Types.SPACE_CHAR;
        wl23DescAggVinc = "";
        wl23DsRiga = Types.INVALID_LONG_VAL;
        wl23DsOperSql = Types.SPACE_CHAR;
        wl23DsVer = Types.INVALID_INT_VAL;
        wl23DsTsIniCptz = Types.INVALID_LONG_VAL;
        wl23DsTsEndCptz = Types.INVALID_LONG_VAL;
        wl23DsUtente = "";
        wl23DsStatoElab = Types.SPACE_CHAR;
        wl23TpAutSeq = "";
        wl23CodUffSeq = "";
        wl23NumProvvSeq.initWl23NumProvvSeqSpaces();
        wl23TpProvvSeq = "";
        wl23DtProvvSeq.initWl23DtProvvSeqSpaces();
        wl23DtNotificaBlocco.initWl23DtNotificaBloccoSpaces();
        wl23NotaProvv = "";
    }

    public void setWl23IdVincPeg(int wl23IdVincPeg) {
        this.wl23IdVincPeg = wl23IdVincPeg;
    }

    public int getWl23IdVincPeg() {
        return this.wl23IdVincPeg;
    }

    public void setWl23IdMoviCrz(int wl23IdMoviCrz) {
        this.wl23IdMoviCrz = wl23IdMoviCrz;
    }

    public int getWl23IdMoviCrz() {
        return this.wl23IdMoviCrz;
    }

    public void setWl23DtIniEff(int wl23DtIniEff) {
        this.wl23DtIniEff = wl23DtIniEff;
    }

    public int getWl23DtIniEff() {
        return this.wl23DtIniEff;
    }

    public void setWl23DtEndEff(int wl23DtEndEff) {
        this.wl23DtEndEff = wl23DtEndEff;
    }

    public int getWl23DtEndEff() {
        return this.wl23DtEndEff;
    }

    public void setWl23CodCompAnia(int wl23CodCompAnia) {
        this.wl23CodCompAnia = wl23CodCompAnia;
    }

    public int getWl23CodCompAnia() {
        return this.wl23CodCompAnia;
    }

    public void setWl23IdRappAna(int wl23IdRappAna) {
        this.wl23IdRappAna = wl23IdRappAna;
    }

    public int getWl23IdRappAna() {
        return this.wl23IdRappAna;
    }

    public void setWl23TpVinc(String wl23TpVinc) {
        this.wl23TpVinc = Functions.subString(wl23TpVinc, Len.WL23_TP_VINC);
    }

    public String getWl23TpVinc() {
        return this.wl23TpVinc;
    }

    public void setWl23FlDelegaAlRisc(char wl23FlDelegaAlRisc) {
        this.wl23FlDelegaAlRisc = wl23FlDelegaAlRisc;
    }

    public char getWl23FlDelegaAlRisc() {
        return this.wl23FlDelegaAlRisc;
    }

    public void setWl23Desc(String wl23Desc) {
        this.wl23Desc = Functions.subString(wl23Desc, Len.WL23_DESC);
    }

    public String getWl23Desc() {
        return this.wl23Desc;
    }

    public void setWl23FlVinpgIntPrstz(char wl23FlVinpgIntPrstz) {
        this.wl23FlVinpgIntPrstz = wl23FlVinpgIntPrstz;
    }

    public char getWl23FlVinpgIntPrstz() {
        return this.wl23FlVinpgIntPrstz;
    }

    public void setWl23DescAggVinc(String wl23DescAggVinc) {
        this.wl23DescAggVinc = Functions.subString(wl23DescAggVinc, Len.WL23_DESC_AGG_VINC);
    }

    public String getWl23DescAggVinc() {
        return this.wl23DescAggVinc;
    }

    public void setWl23DsRiga(long wl23DsRiga) {
        this.wl23DsRiga = wl23DsRiga;
    }

    public long getWl23DsRiga() {
        return this.wl23DsRiga;
    }

    public void setWl23DsOperSql(char wl23DsOperSql) {
        this.wl23DsOperSql = wl23DsOperSql;
    }

    public char getWl23DsOperSql() {
        return this.wl23DsOperSql;
    }

    public void setWl23DsVer(int wl23DsVer) {
        this.wl23DsVer = wl23DsVer;
    }

    public int getWl23DsVer() {
        return this.wl23DsVer;
    }

    public void setWl23DsTsIniCptz(long wl23DsTsIniCptz) {
        this.wl23DsTsIniCptz = wl23DsTsIniCptz;
    }

    public long getWl23DsTsIniCptz() {
        return this.wl23DsTsIniCptz;
    }

    public void setWl23DsTsEndCptz(long wl23DsTsEndCptz) {
        this.wl23DsTsEndCptz = wl23DsTsEndCptz;
    }

    public long getWl23DsTsEndCptz() {
        return this.wl23DsTsEndCptz;
    }

    public void setWl23DsUtente(String wl23DsUtente) {
        this.wl23DsUtente = Functions.subString(wl23DsUtente, Len.WL23_DS_UTENTE);
    }

    public String getWl23DsUtente() {
        return this.wl23DsUtente;
    }

    public void setWl23DsStatoElab(char wl23DsStatoElab) {
        this.wl23DsStatoElab = wl23DsStatoElab;
    }

    public char getWl23DsStatoElab() {
        return this.wl23DsStatoElab;
    }

    public void setWl23TpAutSeq(String wl23TpAutSeq) {
        this.wl23TpAutSeq = Functions.subString(wl23TpAutSeq, Len.WL23_TP_AUT_SEQ);
    }

    public String getWl23TpAutSeq() {
        return this.wl23TpAutSeq;
    }

    public void setWl23CodUffSeq(String wl23CodUffSeq) {
        this.wl23CodUffSeq = Functions.subString(wl23CodUffSeq, Len.WL23_COD_UFF_SEQ);
    }

    public String getWl23CodUffSeq() {
        return this.wl23CodUffSeq;
    }

    public void setWl23TpProvvSeq(String wl23TpProvvSeq) {
        this.wl23TpProvvSeq = Functions.subString(wl23TpProvvSeq, Len.WL23_TP_PROVV_SEQ);
    }

    public String getWl23TpProvvSeq() {
        return this.wl23TpProvvSeq;
    }

    public void setWl23NotaProvv(String wl23NotaProvv) {
        this.wl23NotaProvv = Functions.subString(wl23NotaProvv, Len.WL23_NOTA_PROVV);
    }

    public String getWl23NotaProvv() {
        return this.wl23NotaProvv;
    }

    public Wl23CptVinctoPign getWl23CptVinctoPign() {
        return wl23CptVinctoPign;
    }

    public Wl23DtAttivVinpg getWl23DtAttivVinpg() {
        return wl23DtAttivVinpg;
    }

    public Wl23DtChiuVinpg getWl23DtChiuVinpg() {
        return wl23DtChiuVinpg;
    }

    public Wl23DtNotificaBlocco getWl23DtNotificaBlocco() {
        return wl23DtNotificaBlocco;
    }

    public Wl23DtProvvSeq getWl23DtProvvSeq() {
        return wl23DtProvvSeq;
    }

    public Wl23IdMoviChiu getWl23IdMoviChiu() {
        return wl23IdMoviChiu;
    }

    public Wl23NumProvvSeq getWl23NumProvvSeq() {
        return wl23NumProvvSeq;
    }

    public Wl23SomPreVinpg getWl23SomPreVinpg() {
        return wl23SomPreVinpg;
    }

    public Wl23ValRiscEndVinpg getWl23ValRiscEndVinpg() {
        return wl23ValRiscEndVinpg;
    }

    public Wl23ValRiscIniVinpg getWl23ValRiscIniVinpg() {
        return wl23ValRiscIniVinpg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WL23_ID_VINC_PEG = 5;
        public static final int WL23_ID_MOVI_CRZ = 5;
        public static final int WL23_DT_INI_EFF = 5;
        public static final int WL23_DT_END_EFF = 5;
        public static final int WL23_COD_COMP_ANIA = 3;
        public static final int WL23_ID_RAPP_ANA = 5;
        public static final int WL23_TP_VINC = 2;
        public static final int WL23_FL_DELEGA_AL_RISC = 1;
        public static final int WL23_DESC = 1000;
        public static final int WL23_FL_VINPG_INT_PRSTZ = 1;
        public static final int WL23_DESC_AGG_VINC = 100;
        public static final int WL23_DS_RIGA = 6;
        public static final int WL23_DS_OPER_SQL = 1;
        public static final int WL23_DS_VER = 5;
        public static final int WL23_DS_TS_INI_CPTZ = 10;
        public static final int WL23_DS_TS_END_CPTZ = 10;
        public static final int WL23_DS_UTENTE = 20;
        public static final int WL23_DS_STATO_ELAB = 1;
        public static final int WL23_TP_AUT_SEQ = 2;
        public static final int WL23_COD_UFF_SEQ = 11;
        public static final int WL23_TP_PROVV_SEQ = 2;
        public static final int WL23_NOTA_PROVV = 300;
        public static final int DATI = WL23_ID_VINC_PEG + WL23_ID_MOVI_CRZ + Wl23IdMoviChiu.Len.WL23_ID_MOVI_CHIU + WL23_DT_INI_EFF + WL23_DT_END_EFF + WL23_COD_COMP_ANIA + WL23_ID_RAPP_ANA + WL23_TP_VINC + WL23_FL_DELEGA_AL_RISC + WL23_DESC + Wl23DtAttivVinpg.Len.WL23_DT_ATTIV_VINPG + Wl23CptVinctoPign.Len.WL23_CPT_VINCTO_PIGN + Wl23DtChiuVinpg.Len.WL23_DT_CHIU_VINPG + Wl23ValRiscIniVinpg.Len.WL23_VAL_RISC_INI_VINPG + Wl23ValRiscEndVinpg.Len.WL23_VAL_RISC_END_VINPG + Wl23SomPreVinpg.Len.WL23_SOM_PRE_VINPG + WL23_FL_VINPG_INT_PRSTZ + WL23_DESC_AGG_VINC + WL23_DS_RIGA + WL23_DS_OPER_SQL + WL23_DS_VER + WL23_DS_TS_INI_CPTZ + WL23_DS_TS_END_CPTZ + WL23_DS_UTENTE + WL23_DS_STATO_ELAB + WL23_TP_AUT_SEQ + WL23_COD_UFF_SEQ + Wl23NumProvvSeq.Len.WL23_NUM_PROVV_SEQ + WL23_TP_PROVV_SEQ + Wl23DtProvvSeq.Len.WL23_DT_PROVV_SEQ + Wl23DtNotificaBlocco.Len.WL23_DT_NOTIFICA_BLOCCO + WL23_NOTA_PROVV;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL23_ID_VINC_PEG = 9;
            public static final int WL23_ID_MOVI_CRZ = 9;
            public static final int WL23_DT_INI_EFF = 8;
            public static final int WL23_DT_END_EFF = 8;
            public static final int WL23_COD_COMP_ANIA = 5;
            public static final int WL23_ID_RAPP_ANA = 9;
            public static final int WL23_DS_RIGA = 10;
            public static final int WL23_DS_VER = 9;
            public static final int WL23_DS_TS_INI_CPTZ = 18;
            public static final int WL23_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
