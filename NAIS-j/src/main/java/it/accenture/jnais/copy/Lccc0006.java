package it.accenture.jnais.copy;

import it.accenture.jnais.ws.enums.ActTpGaranzia;

/**Original name: LCCC0006<br>
 * Variable: LCCC0006 from copybook LCCC0006<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccc0006 {

    //==== PROPERTIES ====
    /**Original name: ACT-TP-GARANZIA<br>
	 * <pre>----------------------------------------------------------------*
	 *     TP_GAR (ACTUATOR)
	 * ----------------------------------------------------------------*</pre>*/
    private ActTpGaranzia actTpGaranzia = new ActTpGaranzia();

    //==== METHODS ====
    public ActTpGaranzia getActTpGaranzia() {
        return actTpGaranzia;
    }
}
