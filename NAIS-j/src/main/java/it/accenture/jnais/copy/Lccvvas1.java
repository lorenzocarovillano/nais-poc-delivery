package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVVAS1<br>
 * Variable: LCCVVAS1 from copybook LCCVVAS1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvvas1 implements ICopyable<Lccvvas1> {

    //==== PROPERTIES ====
    /**Original name: WVAS-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA VAL_AST
	 *    ALIAS VAS
	 *    ULTIMO AGG. 09 LUG 2010
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WVAS-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WVAS-DATI
    private WvasDati dati = new WvasDati();

    //==== CONSTRUCTORS ====
    public Lccvvas1() {
    }

    public Lccvvas1(Lccvvas1 lccvvas1) {
        this();
        this.status.setStatus(lccvvas1.status.getStatus());
        this.idPtf = lccvvas1.idPtf;
        this.dati = lccvvas1.dati.copy();
    }

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WvasDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    public Lccvvas1 copy() {
        return new Lccvvas1(this);
    }
}
