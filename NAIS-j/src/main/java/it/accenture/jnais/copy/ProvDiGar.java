package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.PvtAlqCommisInter;
import it.accenture.jnais.ws.redefines.PvtAlqProvAcq;
import it.accenture.jnais.ws.redefines.PvtAlqProvInc;
import it.accenture.jnais.ws.redefines.PvtAlqProvRicor;
import it.accenture.jnais.ws.redefines.PvtAlqRemunAss;
import it.accenture.jnais.ws.redefines.PvtCommisInter;
import it.accenture.jnais.ws.redefines.PvtIdGar;
import it.accenture.jnais.ws.redefines.PvtIdMoviChiu;
import it.accenture.jnais.ws.redefines.PvtProv1aaAcq;
import it.accenture.jnais.ws.redefines.PvtProv2aaAcq;
import it.accenture.jnais.ws.redefines.PvtProvInc;
import it.accenture.jnais.ws.redefines.PvtProvRicor;
import it.accenture.jnais.ws.redefines.PvtRemunAss;

/**Original name: PROV-DI-GAR<br>
 * Variable: PROV-DI-GAR from copybook IDBVPVT1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ProvDiGar {

    //==== PROPERTIES ====
    //Original name: PVT-ID-PROV-DI-GAR
    private int pvtIdProvDiGar = DefaultValues.INT_VAL;
    //Original name: PVT-ID-GAR
    private PvtIdGar pvtIdGar = new PvtIdGar();
    //Original name: PVT-ID-MOVI-CRZ
    private int pvtIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: PVT-ID-MOVI-CHIU
    private PvtIdMoviChiu pvtIdMoviChiu = new PvtIdMoviChiu();
    //Original name: PVT-DT-INI-EFF
    private int pvtDtIniEff = DefaultValues.INT_VAL;
    //Original name: PVT-DT-END-EFF
    private int pvtDtEndEff = DefaultValues.INT_VAL;
    //Original name: PVT-COD-COMP-ANIA
    private int pvtCodCompAnia = DefaultValues.INT_VAL;
    //Original name: PVT-TP-PROV
    private String pvtTpProv = DefaultValues.stringVal(Len.PVT_TP_PROV);
    //Original name: PVT-PROV-1AA-ACQ
    private PvtProv1aaAcq pvtProv1aaAcq = new PvtProv1aaAcq();
    //Original name: PVT-PROV-2AA-ACQ
    private PvtProv2aaAcq pvtProv2aaAcq = new PvtProv2aaAcq();
    //Original name: PVT-PROV-RICOR
    private PvtProvRicor pvtProvRicor = new PvtProvRicor();
    //Original name: PVT-PROV-INC
    private PvtProvInc pvtProvInc = new PvtProvInc();
    //Original name: PVT-ALQ-PROV-ACQ
    private PvtAlqProvAcq pvtAlqProvAcq = new PvtAlqProvAcq();
    //Original name: PVT-ALQ-PROV-INC
    private PvtAlqProvInc pvtAlqProvInc = new PvtAlqProvInc();
    //Original name: PVT-ALQ-PROV-RICOR
    private PvtAlqProvRicor pvtAlqProvRicor = new PvtAlqProvRicor();
    //Original name: PVT-FL-STOR-PROV-ACQ
    private char pvtFlStorProvAcq = DefaultValues.CHAR_VAL;
    //Original name: PVT-FL-REC-PROV-STORN
    private char pvtFlRecProvStorn = DefaultValues.CHAR_VAL;
    //Original name: PVT-FL-PROV-FORZ
    private char pvtFlProvForz = DefaultValues.CHAR_VAL;
    //Original name: PVT-TP-CALC-PROV
    private char pvtTpCalcProv = DefaultValues.CHAR_VAL;
    //Original name: PVT-DS-RIGA
    private long pvtDsRiga = DefaultValues.LONG_VAL;
    //Original name: PVT-DS-OPER-SQL
    private char pvtDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: PVT-DS-VER
    private int pvtDsVer = DefaultValues.INT_VAL;
    //Original name: PVT-DS-TS-INI-CPTZ
    private long pvtDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: PVT-DS-TS-END-CPTZ
    private long pvtDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: PVT-DS-UTENTE
    private String pvtDsUtente = DefaultValues.stringVal(Len.PVT_DS_UTENTE);
    //Original name: PVT-DS-STATO-ELAB
    private char pvtDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: PVT-REMUN-ASS
    private PvtRemunAss pvtRemunAss = new PvtRemunAss();
    //Original name: PVT-COMMIS-INTER
    private PvtCommisInter pvtCommisInter = new PvtCommisInter();
    //Original name: PVT-ALQ-REMUN-ASS
    private PvtAlqRemunAss pvtAlqRemunAss = new PvtAlqRemunAss();
    //Original name: PVT-ALQ-COMMIS-INTER
    private PvtAlqCommisInter pvtAlqCommisInter = new PvtAlqCommisInter();

    //==== METHODS ====
    public void setProvDiGarFormatted(String data) {
        byte[] buffer = new byte[Len.PROV_DI_GAR];
        MarshalByte.writeString(buffer, 1, data, Len.PROV_DI_GAR);
        setProvDiGarBytes(buffer, 1);
    }

    public String getProvDiGarFormatted() {
        return MarshalByteExt.bufferToStr(getProvDiGarBytes());
    }

    public byte[] getProvDiGarBytes() {
        byte[] buffer = new byte[Len.PROV_DI_GAR];
        return getProvDiGarBytes(buffer, 1);
    }

    public void setProvDiGarBytes(byte[] buffer, int offset) {
        int position = offset;
        pvtIdProvDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PVT_ID_PROV_DI_GAR, 0);
        position += Len.PVT_ID_PROV_DI_GAR;
        pvtIdGar.setPvtIdGarFromBuffer(buffer, position);
        position += PvtIdGar.Len.PVT_ID_GAR;
        pvtIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PVT_ID_MOVI_CRZ, 0);
        position += Len.PVT_ID_MOVI_CRZ;
        pvtIdMoviChiu.setPvtIdMoviChiuFromBuffer(buffer, position);
        position += PvtIdMoviChiu.Len.PVT_ID_MOVI_CHIU;
        pvtDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PVT_DT_INI_EFF, 0);
        position += Len.PVT_DT_INI_EFF;
        pvtDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PVT_DT_END_EFF, 0);
        position += Len.PVT_DT_END_EFF;
        pvtCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PVT_COD_COMP_ANIA, 0);
        position += Len.PVT_COD_COMP_ANIA;
        pvtTpProv = MarshalByte.readString(buffer, position, Len.PVT_TP_PROV);
        position += Len.PVT_TP_PROV;
        pvtProv1aaAcq.setPvtProv1aaAcqFromBuffer(buffer, position);
        position += PvtProv1aaAcq.Len.PVT_PROV1AA_ACQ;
        pvtProv2aaAcq.setPvtProv2aaAcqFromBuffer(buffer, position);
        position += PvtProv2aaAcq.Len.PVT_PROV2AA_ACQ;
        pvtProvRicor.setPvtProvRicorFromBuffer(buffer, position);
        position += PvtProvRicor.Len.PVT_PROV_RICOR;
        pvtProvInc.setPvtProvIncFromBuffer(buffer, position);
        position += PvtProvInc.Len.PVT_PROV_INC;
        pvtAlqProvAcq.setPvtAlqProvAcqFromBuffer(buffer, position);
        position += PvtAlqProvAcq.Len.PVT_ALQ_PROV_ACQ;
        pvtAlqProvInc.setPvtAlqProvIncFromBuffer(buffer, position);
        position += PvtAlqProvInc.Len.PVT_ALQ_PROV_INC;
        pvtAlqProvRicor.setPvtAlqProvRicorFromBuffer(buffer, position);
        position += PvtAlqProvRicor.Len.PVT_ALQ_PROV_RICOR;
        pvtFlStorProvAcq = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pvtFlRecProvStorn = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pvtFlProvForz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pvtTpCalcProv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pvtDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PVT_DS_RIGA, 0);
        position += Len.PVT_DS_RIGA;
        pvtDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pvtDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PVT_DS_VER, 0);
        position += Len.PVT_DS_VER;
        pvtDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PVT_DS_TS_INI_CPTZ, 0);
        position += Len.PVT_DS_TS_INI_CPTZ;
        pvtDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PVT_DS_TS_END_CPTZ, 0);
        position += Len.PVT_DS_TS_END_CPTZ;
        pvtDsUtente = MarshalByte.readString(buffer, position, Len.PVT_DS_UTENTE);
        position += Len.PVT_DS_UTENTE;
        pvtDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pvtRemunAss.setPvtRemunAssFromBuffer(buffer, position);
        position += PvtRemunAss.Len.PVT_REMUN_ASS;
        pvtCommisInter.setPvtCommisInterFromBuffer(buffer, position);
        position += PvtCommisInter.Len.PVT_COMMIS_INTER;
        pvtAlqRemunAss.setPvtAlqRemunAssFromBuffer(buffer, position);
        position += PvtAlqRemunAss.Len.PVT_ALQ_REMUN_ASS;
        pvtAlqCommisInter.setPvtAlqCommisInterFromBuffer(buffer, position);
    }

    public byte[] getProvDiGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, pvtIdProvDiGar, Len.Int.PVT_ID_PROV_DI_GAR, 0);
        position += Len.PVT_ID_PROV_DI_GAR;
        pvtIdGar.getPvtIdGarAsBuffer(buffer, position);
        position += PvtIdGar.Len.PVT_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, pvtIdMoviCrz, Len.Int.PVT_ID_MOVI_CRZ, 0);
        position += Len.PVT_ID_MOVI_CRZ;
        pvtIdMoviChiu.getPvtIdMoviChiuAsBuffer(buffer, position);
        position += PvtIdMoviChiu.Len.PVT_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, pvtDtIniEff, Len.Int.PVT_DT_INI_EFF, 0);
        position += Len.PVT_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, pvtDtEndEff, Len.Int.PVT_DT_END_EFF, 0);
        position += Len.PVT_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, pvtCodCompAnia, Len.Int.PVT_COD_COMP_ANIA, 0);
        position += Len.PVT_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, pvtTpProv, Len.PVT_TP_PROV);
        position += Len.PVT_TP_PROV;
        pvtProv1aaAcq.getPvtProv1aaAcqAsBuffer(buffer, position);
        position += PvtProv1aaAcq.Len.PVT_PROV1AA_ACQ;
        pvtProv2aaAcq.getPvtProv2aaAcqAsBuffer(buffer, position);
        position += PvtProv2aaAcq.Len.PVT_PROV2AA_ACQ;
        pvtProvRicor.getPvtProvRicorAsBuffer(buffer, position);
        position += PvtProvRicor.Len.PVT_PROV_RICOR;
        pvtProvInc.getPvtProvIncAsBuffer(buffer, position);
        position += PvtProvInc.Len.PVT_PROV_INC;
        pvtAlqProvAcq.getPvtAlqProvAcqAsBuffer(buffer, position);
        position += PvtAlqProvAcq.Len.PVT_ALQ_PROV_ACQ;
        pvtAlqProvInc.getPvtAlqProvIncAsBuffer(buffer, position);
        position += PvtAlqProvInc.Len.PVT_ALQ_PROV_INC;
        pvtAlqProvRicor.getPvtAlqProvRicorAsBuffer(buffer, position);
        position += PvtAlqProvRicor.Len.PVT_ALQ_PROV_RICOR;
        MarshalByte.writeChar(buffer, position, pvtFlStorProvAcq);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, pvtFlRecProvStorn);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, pvtFlProvForz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, pvtTpCalcProv);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, pvtDsRiga, Len.Int.PVT_DS_RIGA, 0);
        position += Len.PVT_DS_RIGA;
        MarshalByte.writeChar(buffer, position, pvtDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, pvtDsVer, Len.Int.PVT_DS_VER, 0);
        position += Len.PVT_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, pvtDsTsIniCptz, Len.Int.PVT_DS_TS_INI_CPTZ, 0);
        position += Len.PVT_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, pvtDsTsEndCptz, Len.Int.PVT_DS_TS_END_CPTZ, 0);
        position += Len.PVT_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, pvtDsUtente, Len.PVT_DS_UTENTE);
        position += Len.PVT_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, pvtDsStatoElab);
        position += Types.CHAR_SIZE;
        pvtRemunAss.getPvtRemunAssAsBuffer(buffer, position);
        position += PvtRemunAss.Len.PVT_REMUN_ASS;
        pvtCommisInter.getPvtCommisInterAsBuffer(buffer, position);
        position += PvtCommisInter.Len.PVT_COMMIS_INTER;
        pvtAlqRemunAss.getPvtAlqRemunAssAsBuffer(buffer, position);
        position += PvtAlqRemunAss.Len.PVT_ALQ_REMUN_ASS;
        pvtAlqCommisInter.getPvtAlqCommisInterAsBuffer(buffer, position);
        return buffer;
    }

    public void setPvtIdProvDiGar(int pvtIdProvDiGar) {
        this.pvtIdProvDiGar = pvtIdProvDiGar;
    }

    public int getPvtIdProvDiGar() {
        return this.pvtIdProvDiGar;
    }

    public void setPvtIdMoviCrz(int pvtIdMoviCrz) {
        this.pvtIdMoviCrz = pvtIdMoviCrz;
    }

    public int getPvtIdMoviCrz() {
        return this.pvtIdMoviCrz;
    }

    public void setPvtDtIniEff(int pvtDtIniEff) {
        this.pvtDtIniEff = pvtDtIniEff;
    }

    public int getPvtDtIniEff() {
        return this.pvtDtIniEff;
    }

    public void setPvtDtEndEff(int pvtDtEndEff) {
        this.pvtDtEndEff = pvtDtEndEff;
    }

    public int getPvtDtEndEff() {
        return this.pvtDtEndEff;
    }

    public void setPvtCodCompAnia(int pvtCodCompAnia) {
        this.pvtCodCompAnia = pvtCodCompAnia;
    }

    public int getPvtCodCompAnia() {
        return this.pvtCodCompAnia;
    }

    public void setPvtTpProv(String pvtTpProv) {
        this.pvtTpProv = Functions.subString(pvtTpProv, Len.PVT_TP_PROV);
    }

    public String getPvtTpProv() {
        return this.pvtTpProv;
    }

    public String getPvtTpProvFormatted() {
        return Functions.padBlanks(getPvtTpProv(), Len.PVT_TP_PROV);
    }

    public void setPvtFlStorProvAcq(char pvtFlStorProvAcq) {
        this.pvtFlStorProvAcq = pvtFlStorProvAcq;
    }

    public char getPvtFlStorProvAcq() {
        return this.pvtFlStorProvAcq;
    }

    public void setPvtFlRecProvStorn(char pvtFlRecProvStorn) {
        this.pvtFlRecProvStorn = pvtFlRecProvStorn;
    }

    public char getPvtFlRecProvStorn() {
        return this.pvtFlRecProvStorn;
    }

    public void setPvtFlProvForz(char pvtFlProvForz) {
        this.pvtFlProvForz = pvtFlProvForz;
    }

    public char getPvtFlProvForz() {
        return this.pvtFlProvForz;
    }

    public void setPvtTpCalcProv(char pvtTpCalcProv) {
        this.pvtTpCalcProv = pvtTpCalcProv;
    }

    public char getPvtTpCalcProv() {
        return this.pvtTpCalcProv;
    }

    public void setPvtDsRiga(long pvtDsRiga) {
        this.pvtDsRiga = pvtDsRiga;
    }

    public long getPvtDsRiga() {
        return this.pvtDsRiga;
    }

    public void setPvtDsOperSql(char pvtDsOperSql) {
        this.pvtDsOperSql = pvtDsOperSql;
    }

    public char getPvtDsOperSql() {
        return this.pvtDsOperSql;
    }

    public void setPvtDsVer(int pvtDsVer) {
        this.pvtDsVer = pvtDsVer;
    }

    public int getPvtDsVer() {
        return this.pvtDsVer;
    }

    public void setPvtDsTsIniCptz(long pvtDsTsIniCptz) {
        this.pvtDsTsIniCptz = pvtDsTsIniCptz;
    }

    public long getPvtDsTsIniCptz() {
        return this.pvtDsTsIniCptz;
    }

    public void setPvtDsTsEndCptz(long pvtDsTsEndCptz) {
        this.pvtDsTsEndCptz = pvtDsTsEndCptz;
    }

    public long getPvtDsTsEndCptz() {
        return this.pvtDsTsEndCptz;
    }

    public void setPvtDsUtente(String pvtDsUtente) {
        this.pvtDsUtente = Functions.subString(pvtDsUtente, Len.PVT_DS_UTENTE);
    }

    public String getPvtDsUtente() {
        return this.pvtDsUtente;
    }

    public void setPvtDsStatoElab(char pvtDsStatoElab) {
        this.pvtDsStatoElab = pvtDsStatoElab;
    }

    public char getPvtDsStatoElab() {
        return this.pvtDsStatoElab;
    }

    public PvtAlqCommisInter getPvtAlqCommisInter() {
        return pvtAlqCommisInter;
    }

    public PvtAlqProvAcq getPvtAlqProvAcq() {
        return pvtAlqProvAcq;
    }

    public PvtAlqProvInc getPvtAlqProvInc() {
        return pvtAlqProvInc;
    }

    public PvtAlqProvRicor getPvtAlqProvRicor() {
        return pvtAlqProvRicor;
    }

    public PvtAlqRemunAss getPvtAlqRemunAss() {
        return pvtAlqRemunAss;
    }

    public PvtCommisInter getPvtCommisInter() {
        return pvtCommisInter;
    }

    public PvtIdGar getPvtIdGar() {
        return pvtIdGar;
    }

    public PvtIdMoviChiu getPvtIdMoviChiu() {
        return pvtIdMoviChiu;
    }

    public PvtProv1aaAcq getPvtProv1aaAcq() {
        return pvtProv1aaAcq;
    }

    public PvtProv2aaAcq getPvtProv2aaAcq() {
        return pvtProv2aaAcq;
    }

    public PvtProvInc getPvtProvInc() {
        return pvtProvInc;
    }

    public PvtProvRicor getPvtProvRicor() {
        return pvtProvRicor;
    }

    public PvtRemunAss getPvtRemunAss() {
        return pvtRemunAss;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_TP_PROV = 2;
        public static final int PVT_DS_UTENTE = 20;
        public static final int PVT_ID_PROV_DI_GAR = 5;
        public static final int PVT_ID_MOVI_CRZ = 5;
        public static final int PVT_DT_INI_EFF = 5;
        public static final int PVT_DT_END_EFF = 5;
        public static final int PVT_COD_COMP_ANIA = 3;
        public static final int PVT_FL_STOR_PROV_ACQ = 1;
        public static final int PVT_FL_REC_PROV_STORN = 1;
        public static final int PVT_FL_PROV_FORZ = 1;
        public static final int PVT_TP_CALC_PROV = 1;
        public static final int PVT_DS_RIGA = 6;
        public static final int PVT_DS_OPER_SQL = 1;
        public static final int PVT_DS_VER = 5;
        public static final int PVT_DS_TS_INI_CPTZ = 10;
        public static final int PVT_DS_TS_END_CPTZ = 10;
        public static final int PVT_DS_STATO_ELAB = 1;
        public static final int PROV_DI_GAR = PVT_ID_PROV_DI_GAR + PvtIdGar.Len.PVT_ID_GAR + PVT_ID_MOVI_CRZ + PvtIdMoviChiu.Len.PVT_ID_MOVI_CHIU + PVT_DT_INI_EFF + PVT_DT_END_EFF + PVT_COD_COMP_ANIA + PVT_TP_PROV + PvtProv1aaAcq.Len.PVT_PROV1AA_ACQ + PvtProv2aaAcq.Len.PVT_PROV2AA_ACQ + PvtProvRicor.Len.PVT_PROV_RICOR + PvtProvInc.Len.PVT_PROV_INC + PvtAlqProvAcq.Len.PVT_ALQ_PROV_ACQ + PvtAlqProvInc.Len.PVT_ALQ_PROV_INC + PvtAlqProvRicor.Len.PVT_ALQ_PROV_RICOR + PVT_FL_STOR_PROV_ACQ + PVT_FL_REC_PROV_STORN + PVT_FL_PROV_FORZ + PVT_TP_CALC_PROV + PVT_DS_RIGA + PVT_DS_OPER_SQL + PVT_DS_VER + PVT_DS_TS_INI_CPTZ + PVT_DS_TS_END_CPTZ + PVT_DS_UTENTE + PVT_DS_STATO_ELAB + PvtRemunAss.Len.PVT_REMUN_ASS + PvtCommisInter.Len.PVT_COMMIS_INTER + PvtAlqRemunAss.Len.PVT_ALQ_REMUN_ASS + PvtAlqCommisInter.Len.PVT_ALQ_COMMIS_INTER;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_ID_PROV_DI_GAR = 9;
            public static final int PVT_ID_MOVI_CRZ = 9;
            public static final int PVT_DT_INI_EFF = 8;
            public static final int PVT_DT_END_EFF = 8;
            public static final int PVT_COD_COMP_ANIA = 5;
            public static final int PVT_DS_RIGA = 10;
            public static final int PVT_DS_VER = 9;
            public static final int PVT_DS_TS_INI_CPTZ = 18;
            public static final int PVT_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
