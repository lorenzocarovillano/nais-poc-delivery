package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.BepCodBnfic;
import it.accenture.jnais.ws.redefines.BepIdBnficr;
import it.accenture.jnais.ws.redefines.BepIdMoviChiu;
import it.accenture.jnais.ws.redefines.BepPcDelBnficr;

/**Original name: BNFIC<br>
 * Variable: BNFIC from copybook IDBVBEP1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Bnfic {

    //==== PROPERTIES ====
    //Original name: BEP-ID-BNFIC
    private int bepIdBnfic = DefaultValues.INT_VAL;
    //Original name: BEP-ID-RAPP-ANA
    private int bepIdRappAna = DefaultValues.INT_VAL;
    //Original name: BEP-ID-BNFICR
    private BepIdBnficr bepIdBnficr = new BepIdBnficr();
    //Original name: BEP-ID-MOVI-CRZ
    private int bepIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: BEP-ID-MOVI-CHIU
    private BepIdMoviChiu bepIdMoviChiu = new BepIdMoviChiu();
    //Original name: BEP-DT-INI-EFF
    private int bepDtIniEff = DefaultValues.INT_VAL;
    //Original name: BEP-DT-END-EFF
    private int bepDtEndEff = DefaultValues.INT_VAL;
    //Original name: BEP-COD-COMP-ANIA
    private int bepCodCompAnia = DefaultValues.INT_VAL;
    //Original name: BEP-COD-BNFIC
    private BepCodBnfic bepCodBnfic = new BepCodBnfic();
    //Original name: BEP-TP-IND-BNFICR
    private String bepTpIndBnficr = DefaultValues.stringVal(Len.BEP_TP_IND_BNFICR);
    //Original name: BEP-COD-BNFICR
    private String bepCodBnficr = DefaultValues.stringVal(Len.BEP_COD_BNFICR);
    //Original name: BEP-DESC-BNFICR-LEN
    private short bepDescBnficrLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: BEP-DESC-BNFICR
    private String bepDescBnficr = DefaultValues.stringVal(Len.BEP_DESC_BNFICR);
    //Original name: BEP-PC-DEL-BNFICR
    private BepPcDelBnficr bepPcDelBnficr = new BepPcDelBnficr();
    //Original name: BEP-FL-ESE
    private char bepFlEse = DefaultValues.CHAR_VAL;
    //Original name: BEP-FL-IRREV
    private char bepFlIrrev = DefaultValues.CHAR_VAL;
    //Original name: BEP-FL-DFLT
    private char bepFlDflt = DefaultValues.CHAR_VAL;
    //Original name: BEP-ESRCN-ATTVT-IMPRS
    private char bepEsrcnAttvtImprs = DefaultValues.CHAR_VAL;
    //Original name: BEP-FL-BNFICR-COLL
    private char bepFlBnficrColl = DefaultValues.CHAR_VAL;
    //Original name: BEP-DS-RIGA
    private long bepDsRiga = DefaultValues.LONG_VAL;
    //Original name: BEP-DS-OPER-SQL
    private char bepDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: BEP-DS-VER
    private int bepDsVer = DefaultValues.INT_VAL;
    //Original name: BEP-DS-TS-INI-CPTZ
    private long bepDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: BEP-DS-TS-END-CPTZ
    private long bepDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: BEP-DS-UTENTE
    private String bepDsUtente = DefaultValues.stringVal(Len.BEP_DS_UTENTE);
    //Original name: BEP-DS-STATO-ELAB
    private char bepDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: BEP-TP-NORMAL-BNFIC
    private String bepTpNormalBnfic = DefaultValues.stringVal(Len.BEP_TP_NORMAL_BNFIC);

    //==== METHODS ====
    public void setBnficFormatted(String data) {
        byte[] buffer = new byte[Len.BNFIC];
        MarshalByte.writeString(buffer, 1, data, Len.BNFIC);
        setBnficBytes(buffer, 1);
    }

    public void setBnficBytes(byte[] buffer, int offset) {
        int position = offset;
        bepIdBnfic = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEP_ID_BNFIC, 0);
        position += Len.BEP_ID_BNFIC;
        bepIdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEP_ID_RAPP_ANA, 0);
        position += Len.BEP_ID_RAPP_ANA;
        bepIdBnficr.setBepIdBnficrFromBuffer(buffer, position);
        position += BepIdBnficr.Len.BEP_ID_BNFICR;
        bepIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEP_ID_MOVI_CRZ, 0);
        position += Len.BEP_ID_MOVI_CRZ;
        bepIdMoviChiu.setBepIdMoviChiuFromBuffer(buffer, position);
        position += BepIdMoviChiu.Len.BEP_ID_MOVI_CHIU;
        bepDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEP_DT_INI_EFF, 0);
        position += Len.BEP_DT_INI_EFF;
        bepDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEP_DT_END_EFF, 0);
        position += Len.BEP_DT_END_EFF;
        bepCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEP_COD_COMP_ANIA, 0);
        position += Len.BEP_COD_COMP_ANIA;
        bepCodBnfic.setBepCodBnficFromBuffer(buffer, position);
        position += BepCodBnfic.Len.BEP_COD_BNFIC;
        bepTpIndBnficr = MarshalByte.readString(buffer, position, Len.BEP_TP_IND_BNFICR);
        position += Len.BEP_TP_IND_BNFICR;
        bepCodBnficr = MarshalByte.readString(buffer, position, Len.BEP_COD_BNFICR);
        position += Len.BEP_COD_BNFICR;
        setBepDescBnficrVcharBytes(buffer, position);
        position += Len.BEP_DESC_BNFICR_VCHAR;
        bepPcDelBnficr.setBepPcDelBnficrFromBuffer(buffer, position);
        position += BepPcDelBnficr.Len.BEP_PC_DEL_BNFICR;
        bepFlEse = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bepFlIrrev = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bepFlDflt = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bepEsrcnAttvtImprs = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bepFlBnficrColl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bepDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.BEP_DS_RIGA, 0);
        position += Len.BEP_DS_RIGA;
        bepDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bepDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.BEP_DS_VER, 0);
        position += Len.BEP_DS_VER;
        bepDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.BEP_DS_TS_INI_CPTZ, 0);
        position += Len.BEP_DS_TS_INI_CPTZ;
        bepDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.BEP_DS_TS_END_CPTZ, 0);
        position += Len.BEP_DS_TS_END_CPTZ;
        bepDsUtente = MarshalByte.readString(buffer, position, Len.BEP_DS_UTENTE);
        position += Len.BEP_DS_UTENTE;
        bepDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bepTpNormalBnfic = MarshalByte.readString(buffer, position, Len.BEP_TP_NORMAL_BNFIC);
    }

    public void setBepIdBnfic(int bepIdBnfic) {
        this.bepIdBnfic = bepIdBnfic;
    }

    public int getBepIdBnfic() {
        return this.bepIdBnfic;
    }

    public void setBepIdRappAna(int bepIdRappAna) {
        this.bepIdRappAna = bepIdRappAna;
    }

    public int getBepIdRappAna() {
        return this.bepIdRappAna;
    }

    public void setBepIdMoviCrz(int bepIdMoviCrz) {
        this.bepIdMoviCrz = bepIdMoviCrz;
    }

    public int getBepIdMoviCrz() {
        return this.bepIdMoviCrz;
    }

    public void setBepDtIniEff(int bepDtIniEff) {
        this.bepDtIniEff = bepDtIniEff;
    }

    public int getBepDtIniEff() {
        return this.bepDtIniEff;
    }

    public void setBepDtEndEff(int bepDtEndEff) {
        this.bepDtEndEff = bepDtEndEff;
    }

    public int getBepDtEndEff() {
        return this.bepDtEndEff;
    }

    public void setBepCodCompAnia(int bepCodCompAnia) {
        this.bepCodCompAnia = bepCodCompAnia;
    }

    public int getBepCodCompAnia() {
        return this.bepCodCompAnia;
    }

    public void setBepTpIndBnficr(String bepTpIndBnficr) {
        this.bepTpIndBnficr = Functions.subString(bepTpIndBnficr, Len.BEP_TP_IND_BNFICR);
    }

    public String getBepTpIndBnficr() {
        return this.bepTpIndBnficr;
    }

    public void setBepCodBnficr(String bepCodBnficr) {
        this.bepCodBnficr = Functions.subString(bepCodBnficr, Len.BEP_COD_BNFICR);
    }

    public String getBepCodBnficr() {
        return this.bepCodBnficr;
    }

    public String getBepCodBnficrFormatted() {
        return Functions.padBlanks(getBepCodBnficr(), Len.BEP_COD_BNFICR);
    }

    public void setBepDescBnficrVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        bepDescBnficrLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        bepDescBnficr = MarshalByte.readString(buffer, position, Len.BEP_DESC_BNFICR);
    }

    public void setBepDescBnficrLen(short bepDescBnficrLen) {
        this.bepDescBnficrLen = bepDescBnficrLen;
    }

    public short getBepDescBnficrLen() {
        return this.bepDescBnficrLen;
    }

    public void setBepDescBnficr(String bepDescBnficr) {
        this.bepDescBnficr = Functions.subString(bepDescBnficr, Len.BEP_DESC_BNFICR);
    }

    public String getBepDescBnficr() {
        return this.bepDescBnficr;
    }

    public void setBepFlEse(char bepFlEse) {
        this.bepFlEse = bepFlEse;
    }

    public char getBepFlEse() {
        return this.bepFlEse;
    }

    public void setBepFlIrrev(char bepFlIrrev) {
        this.bepFlIrrev = bepFlIrrev;
    }

    public char getBepFlIrrev() {
        return this.bepFlIrrev;
    }

    public void setBepFlDflt(char bepFlDflt) {
        this.bepFlDflt = bepFlDflt;
    }

    public char getBepFlDflt() {
        return this.bepFlDflt;
    }

    public void setBepEsrcnAttvtImprs(char bepEsrcnAttvtImprs) {
        this.bepEsrcnAttvtImprs = bepEsrcnAttvtImprs;
    }

    public char getBepEsrcnAttvtImprs() {
        return this.bepEsrcnAttvtImprs;
    }

    public void setBepFlBnficrColl(char bepFlBnficrColl) {
        this.bepFlBnficrColl = bepFlBnficrColl;
    }

    public char getBepFlBnficrColl() {
        return this.bepFlBnficrColl;
    }

    public void setBepDsRiga(long bepDsRiga) {
        this.bepDsRiga = bepDsRiga;
    }

    public long getBepDsRiga() {
        return this.bepDsRiga;
    }

    public void setBepDsOperSql(char bepDsOperSql) {
        this.bepDsOperSql = bepDsOperSql;
    }

    public char getBepDsOperSql() {
        return this.bepDsOperSql;
    }

    public void setBepDsVer(int bepDsVer) {
        this.bepDsVer = bepDsVer;
    }

    public int getBepDsVer() {
        return this.bepDsVer;
    }

    public void setBepDsTsIniCptz(long bepDsTsIniCptz) {
        this.bepDsTsIniCptz = bepDsTsIniCptz;
    }

    public long getBepDsTsIniCptz() {
        return this.bepDsTsIniCptz;
    }

    public void setBepDsTsEndCptz(long bepDsTsEndCptz) {
        this.bepDsTsEndCptz = bepDsTsEndCptz;
    }

    public long getBepDsTsEndCptz() {
        return this.bepDsTsEndCptz;
    }

    public void setBepDsUtente(String bepDsUtente) {
        this.bepDsUtente = Functions.subString(bepDsUtente, Len.BEP_DS_UTENTE);
    }

    public String getBepDsUtente() {
        return this.bepDsUtente;
    }

    public void setBepDsStatoElab(char bepDsStatoElab) {
        this.bepDsStatoElab = bepDsStatoElab;
    }

    public char getBepDsStatoElab() {
        return this.bepDsStatoElab;
    }

    public void setBepTpNormalBnfic(String bepTpNormalBnfic) {
        this.bepTpNormalBnfic = Functions.subString(bepTpNormalBnfic, Len.BEP_TP_NORMAL_BNFIC);
    }

    public String getBepTpNormalBnfic() {
        return this.bepTpNormalBnfic;
    }

    public String getBepTpNormalBnficFormatted() {
        return Functions.padBlanks(getBepTpNormalBnfic(), Len.BEP_TP_NORMAL_BNFIC);
    }

    public BepCodBnfic getBepCodBnfic() {
        return bepCodBnfic;
    }

    public BepIdBnficr getBepIdBnficr() {
        return bepIdBnficr;
    }

    public BepIdMoviChiu getBepIdMoviChiu() {
        return bepIdMoviChiu;
    }

    public BepPcDelBnficr getBepPcDelBnficr() {
        return bepPcDelBnficr;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BEP_TP_IND_BNFICR = 2;
        public static final int BEP_COD_BNFICR = 20;
        public static final int BEP_DESC_BNFICR = 250;
        public static final int BEP_DS_UTENTE = 20;
        public static final int BEP_TP_NORMAL_BNFIC = 2;
        public static final int BEP_ID_BNFIC = 5;
        public static final int BEP_ID_RAPP_ANA = 5;
        public static final int BEP_ID_MOVI_CRZ = 5;
        public static final int BEP_DT_INI_EFF = 5;
        public static final int BEP_DT_END_EFF = 5;
        public static final int BEP_COD_COMP_ANIA = 3;
        public static final int BEP_DESC_BNFICR_LEN = 2;
        public static final int BEP_DESC_BNFICR_VCHAR = BEP_DESC_BNFICR_LEN + BEP_DESC_BNFICR;
        public static final int BEP_FL_ESE = 1;
        public static final int BEP_FL_IRREV = 1;
        public static final int BEP_FL_DFLT = 1;
        public static final int BEP_ESRCN_ATTVT_IMPRS = 1;
        public static final int BEP_FL_BNFICR_COLL = 1;
        public static final int BEP_DS_RIGA = 6;
        public static final int BEP_DS_OPER_SQL = 1;
        public static final int BEP_DS_VER = 5;
        public static final int BEP_DS_TS_INI_CPTZ = 10;
        public static final int BEP_DS_TS_END_CPTZ = 10;
        public static final int BEP_DS_STATO_ELAB = 1;
        public static final int BNFIC = BEP_ID_BNFIC + BEP_ID_RAPP_ANA + BepIdBnficr.Len.BEP_ID_BNFICR + BEP_ID_MOVI_CRZ + BepIdMoviChiu.Len.BEP_ID_MOVI_CHIU + BEP_DT_INI_EFF + BEP_DT_END_EFF + BEP_COD_COMP_ANIA + BepCodBnfic.Len.BEP_COD_BNFIC + BEP_TP_IND_BNFICR + BEP_COD_BNFICR + BEP_DESC_BNFICR_VCHAR + BepPcDelBnficr.Len.BEP_PC_DEL_BNFICR + BEP_FL_ESE + BEP_FL_IRREV + BEP_FL_DFLT + BEP_ESRCN_ATTVT_IMPRS + BEP_FL_BNFICR_COLL + BEP_DS_RIGA + BEP_DS_OPER_SQL + BEP_DS_VER + BEP_DS_TS_INI_CPTZ + BEP_DS_TS_END_CPTZ + BEP_DS_UTENTE + BEP_DS_STATO_ELAB + BEP_TP_NORMAL_BNFIC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEP_ID_BNFIC = 9;
            public static final int BEP_ID_RAPP_ANA = 9;
            public static final int BEP_ID_MOVI_CRZ = 9;
            public static final int BEP_DT_INI_EFF = 8;
            public static final int BEP_DT_END_EFF = 8;
            public static final int BEP_COD_COMP_ANIA = 5;
            public static final int BEP_DS_RIGA = 10;
            public static final int BEP_DS_VER = 9;
            public static final int BEP_DS_TS_INI_CPTZ = 18;
            public static final int BEP_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
