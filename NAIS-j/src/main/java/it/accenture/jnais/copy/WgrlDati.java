package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WgrlDtSin1oAssto;
import it.accenture.jnais.ws.redefines.WgrlDtSin2oAssto;
import it.accenture.jnais.ws.redefines.WgrlDtSin3oAssto;
import it.accenture.jnais.ws.redefines.WgrlIdMoviChiu;

/**Original name: WGRL-DATI<br>
 * Variable: WGRL-DATI from copybook LCCVGRL1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WgrlDati {

    //==== PROPERTIES ====
    //Original name: WGRL-ID-GAR-LIQ
    private int wgrlIdGarLiq = DefaultValues.INT_VAL;
    //Original name: WGRL-ID-LIQ
    private int wgrlIdLiq = DefaultValues.INT_VAL;
    //Original name: WGRL-ID-GAR
    private int wgrlIdGar = DefaultValues.INT_VAL;
    //Original name: WGRL-ID-MOVI-CRZ
    private int wgrlIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WGRL-ID-MOVI-CHIU
    private WgrlIdMoviChiu wgrlIdMoviChiu = new WgrlIdMoviChiu();
    //Original name: WGRL-DT-INI-EFF
    private int wgrlDtIniEff = DefaultValues.INT_VAL;
    //Original name: WGRL-DT-END-EFF
    private int wgrlDtEndEff = DefaultValues.INT_VAL;
    //Original name: WGRL-COD-COMP-ANIA
    private int wgrlCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WGRL-DT-SIN-1O-ASSTO
    private WgrlDtSin1oAssto wgrlDtSin1oAssto = new WgrlDtSin1oAssto();
    //Original name: WGRL-CAU-SIN-1O-ASSTO
    private String wgrlCauSin1oAssto = DefaultValues.stringVal(Len.WGRL_CAU_SIN1O_ASSTO);
    //Original name: WGRL-TP-SIN-1O-ASSTO
    private String wgrlTpSin1oAssto = DefaultValues.stringVal(Len.WGRL_TP_SIN1O_ASSTO);
    //Original name: WGRL-DT-SIN-2O-ASSTO
    private WgrlDtSin2oAssto wgrlDtSin2oAssto = new WgrlDtSin2oAssto();
    //Original name: WGRL-CAU-SIN-2O-ASSTO
    private String wgrlCauSin2oAssto = DefaultValues.stringVal(Len.WGRL_CAU_SIN2O_ASSTO);
    //Original name: WGRL-TP-SIN-2O-ASSTO
    private String wgrlTpSin2oAssto = DefaultValues.stringVal(Len.WGRL_TP_SIN2O_ASSTO);
    //Original name: WGRL-DT-SIN-3O-ASSTO
    private WgrlDtSin3oAssto wgrlDtSin3oAssto = new WgrlDtSin3oAssto();
    //Original name: WGRL-CAU-SIN-3O-ASSTO
    private String wgrlCauSin3oAssto = DefaultValues.stringVal(Len.WGRL_CAU_SIN3O_ASSTO);
    //Original name: WGRL-TP-SIN-3O-ASSTO
    private String wgrlTpSin3oAssto = DefaultValues.stringVal(Len.WGRL_TP_SIN3O_ASSTO);
    //Original name: WGRL-DS-RIGA
    private long wgrlDsRiga = DefaultValues.LONG_VAL;
    //Original name: WGRL-DS-OPER-SQL
    private char wgrlDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WGRL-DS-VER
    private int wgrlDsVer = DefaultValues.INT_VAL;
    //Original name: WGRL-DS-TS-INI-CPTZ
    private long wgrlDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WGRL-DS-TS-END-CPTZ
    private long wgrlDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WGRL-DS-UTENTE
    private String wgrlDsUtente = DefaultValues.stringVal(Len.WGRL_DS_UTENTE);
    //Original name: WGRL-DS-STATO-ELAB
    private char wgrlDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wgrlIdGarLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRL_ID_GAR_LIQ, 0);
        position += Len.WGRL_ID_GAR_LIQ;
        wgrlIdLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRL_ID_LIQ, 0);
        position += Len.WGRL_ID_LIQ;
        wgrlIdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRL_ID_GAR, 0);
        position += Len.WGRL_ID_GAR;
        wgrlIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRL_ID_MOVI_CRZ, 0);
        position += Len.WGRL_ID_MOVI_CRZ;
        wgrlIdMoviChiu.setWgrlIdMoviChiuFromBuffer(buffer, position);
        position += WgrlIdMoviChiu.Len.WGRL_ID_MOVI_CHIU;
        wgrlDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRL_DT_INI_EFF, 0);
        position += Len.WGRL_DT_INI_EFF;
        wgrlDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRL_DT_END_EFF, 0);
        position += Len.WGRL_DT_END_EFF;
        wgrlCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRL_COD_COMP_ANIA, 0);
        position += Len.WGRL_COD_COMP_ANIA;
        wgrlDtSin1oAssto.setWgrlDtSin1oAsstoFromBuffer(buffer, position);
        position += WgrlDtSin1oAssto.Len.WGRL_DT_SIN1O_ASSTO;
        wgrlCauSin1oAssto = MarshalByte.readString(buffer, position, Len.WGRL_CAU_SIN1O_ASSTO);
        position += Len.WGRL_CAU_SIN1O_ASSTO;
        wgrlTpSin1oAssto = MarshalByte.readString(buffer, position, Len.WGRL_TP_SIN1O_ASSTO);
        position += Len.WGRL_TP_SIN1O_ASSTO;
        wgrlDtSin2oAssto.setWgrlDtSin2oAsstoFromBuffer(buffer, position);
        position += WgrlDtSin2oAssto.Len.WGRL_DT_SIN2O_ASSTO;
        wgrlCauSin2oAssto = MarshalByte.readString(buffer, position, Len.WGRL_CAU_SIN2O_ASSTO);
        position += Len.WGRL_CAU_SIN2O_ASSTO;
        wgrlTpSin2oAssto = MarshalByte.readString(buffer, position, Len.WGRL_TP_SIN2O_ASSTO);
        position += Len.WGRL_TP_SIN2O_ASSTO;
        wgrlDtSin3oAssto.setWgrlDtSin3oAsstoFromBuffer(buffer, position);
        position += WgrlDtSin3oAssto.Len.WGRL_DT_SIN3O_ASSTO;
        wgrlCauSin3oAssto = MarshalByte.readString(buffer, position, Len.WGRL_CAU_SIN3O_ASSTO);
        position += Len.WGRL_CAU_SIN3O_ASSTO;
        wgrlTpSin3oAssto = MarshalByte.readString(buffer, position, Len.WGRL_TP_SIN3O_ASSTO);
        position += Len.WGRL_TP_SIN3O_ASSTO;
        wgrlDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WGRL_DS_RIGA, 0);
        position += Len.WGRL_DS_RIGA;
        wgrlDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wgrlDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WGRL_DS_VER, 0);
        position += Len.WGRL_DS_VER;
        wgrlDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WGRL_DS_TS_INI_CPTZ, 0);
        position += Len.WGRL_DS_TS_INI_CPTZ;
        wgrlDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WGRL_DS_TS_END_CPTZ, 0);
        position += Len.WGRL_DS_TS_END_CPTZ;
        wgrlDsUtente = MarshalByte.readString(buffer, position, Len.WGRL_DS_UTENTE);
        position += Len.WGRL_DS_UTENTE;
        wgrlDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wgrlIdGarLiq, Len.Int.WGRL_ID_GAR_LIQ, 0);
        position += Len.WGRL_ID_GAR_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, wgrlIdLiq, Len.Int.WGRL_ID_LIQ, 0);
        position += Len.WGRL_ID_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, wgrlIdGar, Len.Int.WGRL_ID_GAR, 0);
        position += Len.WGRL_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, wgrlIdMoviCrz, Len.Int.WGRL_ID_MOVI_CRZ, 0);
        position += Len.WGRL_ID_MOVI_CRZ;
        wgrlIdMoviChiu.getWgrlIdMoviChiuAsBuffer(buffer, position);
        position += WgrlIdMoviChiu.Len.WGRL_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wgrlDtIniEff, Len.Int.WGRL_DT_INI_EFF, 0);
        position += Len.WGRL_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wgrlDtEndEff, Len.Int.WGRL_DT_END_EFF, 0);
        position += Len.WGRL_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wgrlCodCompAnia, Len.Int.WGRL_COD_COMP_ANIA, 0);
        position += Len.WGRL_COD_COMP_ANIA;
        wgrlDtSin1oAssto.getWgrlDtSin1oAsstoAsBuffer(buffer, position);
        position += WgrlDtSin1oAssto.Len.WGRL_DT_SIN1O_ASSTO;
        MarshalByte.writeString(buffer, position, wgrlCauSin1oAssto, Len.WGRL_CAU_SIN1O_ASSTO);
        position += Len.WGRL_CAU_SIN1O_ASSTO;
        MarshalByte.writeString(buffer, position, wgrlTpSin1oAssto, Len.WGRL_TP_SIN1O_ASSTO);
        position += Len.WGRL_TP_SIN1O_ASSTO;
        wgrlDtSin2oAssto.getWgrlDtSin2oAsstoAsBuffer(buffer, position);
        position += WgrlDtSin2oAssto.Len.WGRL_DT_SIN2O_ASSTO;
        MarshalByte.writeString(buffer, position, wgrlCauSin2oAssto, Len.WGRL_CAU_SIN2O_ASSTO);
        position += Len.WGRL_CAU_SIN2O_ASSTO;
        MarshalByte.writeString(buffer, position, wgrlTpSin2oAssto, Len.WGRL_TP_SIN2O_ASSTO);
        position += Len.WGRL_TP_SIN2O_ASSTO;
        wgrlDtSin3oAssto.getWgrlDtSin3oAsstoAsBuffer(buffer, position);
        position += WgrlDtSin3oAssto.Len.WGRL_DT_SIN3O_ASSTO;
        MarshalByte.writeString(buffer, position, wgrlCauSin3oAssto, Len.WGRL_CAU_SIN3O_ASSTO);
        position += Len.WGRL_CAU_SIN3O_ASSTO;
        MarshalByte.writeString(buffer, position, wgrlTpSin3oAssto, Len.WGRL_TP_SIN3O_ASSTO);
        position += Len.WGRL_TP_SIN3O_ASSTO;
        MarshalByte.writeLongAsPacked(buffer, position, wgrlDsRiga, Len.Int.WGRL_DS_RIGA, 0);
        position += Len.WGRL_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wgrlDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wgrlDsVer, Len.Int.WGRL_DS_VER, 0);
        position += Len.WGRL_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wgrlDsTsIniCptz, Len.Int.WGRL_DS_TS_INI_CPTZ, 0);
        position += Len.WGRL_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wgrlDsTsEndCptz, Len.Int.WGRL_DS_TS_END_CPTZ, 0);
        position += Len.WGRL_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wgrlDsUtente, Len.WGRL_DS_UTENTE);
        position += Len.WGRL_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wgrlDsStatoElab);
        return buffer;
    }

    public void initDatiSpaces() {
        wgrlIdGarLiq = Types.INVALID_INT_VAL;
        wgrlIdLiq = Types.INVALID_INT_VAL;
        wgrlIdGar = Types.INVALID_INT_VAL;
        wgrlIdMoviCrz = Types.INVALID_INT_VAL;
        wgrlIdMoviChiu.initWgrlIdMoviChiuSpaces();
        wgrlDtIniEff = Types.INVALID_INT_VAL;
        wgrlDtEndEff = Types.INVALID_INT_VAL;
        wgrlCodCompAnia = Types.INVALID_INT_VAL;
        wgrlDtSin1oAssto.initWgrlDtSin1oAsstoSpaces();
        wgrlCauSin1oAssto = "";
        wgrlTpSin1oAssto = "";
        wgrlDtSin2oAssto.initWgrlDtSin2oAsstoSpaces();
        wgrlCauSin2oAssto = "";
        wgrlTpSin2oAssto = "";
        wgrlDtSin3oAssto.initWgrlDtSin3oAsstoSpaces();
        wgrlCauSin3oAssto = "";
        wgrlTpSin3oAssto = "";
        wgrlDsRiga = Types.INVALID_LONG_VAL;
        wgrlDsOperSql = Types.SPACE_CHAR;
        wgrlDsVer = Types.INVALID_INT_VAL;
        wgrlDsTsIniCptz = Types.INVALID_LONG_VAL;
        wgrlDsTsEndCptz = Types.INVALID_LONG_VAL;
        wgrlDsUtente = "";
        wgrlDsStatoElab = Types.SPACE_CHAR;
    }

    public void setWgrlIdGarLiq(int wgrlIdGarLiq) {
        this.wgrlIdGarLiq = wgrlIdGarLiq;
    }

    public int getWgrlIdGarLiq() {
        return this.wgrlIdGarLiq;
    }

    public void setWgrlIdLiq(int wgrlIdLiq) {
        this.wgrlIdLiq = wgrlIdLiq;
    }

    public int getWgrlIdLiq() {
        return this.wgrlIdLiq;
    }

    public void setWgrlIdGar(int wgrlIdGar) {
        this.wgrlIdGar = wgrlIdGar;
    }

    public int getWgrlIdGar() {
        return this.wgrlIdGar;
    }

    public void setWgrlIdMoviCrz(int wgrlIdMoviCrz) {
        this.wgrlIdMoviCrz = wgrlIdMoviCrz;
    }

    public int getWgrlIdMoviCrz() {
        return this.wgrlIdMoviCrz;
    }

    public void setWgrlDtIniEff(int wgrlDtIniEff) {
        this.wgrlDtIniEff = wgrlDtIniEff;
    }

    public int getWgrlDtIniEff() {
        return this.wgrlDtIniEff;
    }

    public void setWgrlDtEndEff(int wgrlDtEndEff) {
        this.wgrlDtEndEff = wgrlDtEndEff;
    }

    public int getWgrlDtEndEff() {
        return this.wgrlDtEndEff;
    }

    public void setWgrlCodCompAnia(int wgrlCodCompAnia) {
        this.wgrlCodCompAnia = wgrlCodCompAnia;
    }

    public int getWgrlCodCompAnia() {
        return this.wgrlCodCompAnia;
    }

    public void setWgrlCauSin1oAssto(String wgrlCauSin1oAssto) {
        this.wgrlCauSin1oAssto = Functions.subString(wgrlCauSin1oAssto, Len.WGRL_CAU_SIN1O_ASSTO);
    }

    public String getWgrlCauSin1oAssto() {
        return this.wgrlCauSin1oAssto;
    }

    public void setWgrlTpSin1oAssto(String wgrlTpSin1oAssto) {
        this.wgrlTpSin1oAssto = Functions.subString(wgrlTpSin1oAssto, Len.WGRL_TP_SIN1O_ASSTO);
    }

    public String getWgrlTpSin1oAssto() {
        return this.wgrlTpSin1oAssto;
    }

    public void setWgrlCauSin2oAssto(String wgrlCauSin2oAssto) {
        this.wgrlCauSin2oAssto = Functions.subString(wgrlCauSin2oAssto, Len.WGRL_CAU_SIN2O_ASSTO);
    }

    public String getWgrlCauSin2oAssto() {
        return this.wgrlCauSin2oAssto;
    }

    public void setWgrlTpSin2oAssto(String wgrlTpSin2oAssto) {
        this.wgrlTpSin2oAssto = Functions.subString(wgrlTpSin2oAssto, Len.WGRL_TP_SIN2O_ASSTO);
    }

    public String getWgrlTpSin2oAssto() {
        return this.wgrlTpSin2oAssto;
    }

    public void setWgrlCauSin3oAssto(String wgrlCauSin3oAssto) {
        this.wgrlCauSin3oAssto = Functions.subString(wgrlCauSin3oAssto, Len.WGRL_CAU_SIN3O_ASSTO);
    }

    public String getWgrlCauSin3oAssto() {
        return this.wgrlCauSin3oAssto;
    }

    public void setWgrlTpSin3oAssto(String wgrlTpSin3oAssto) {
        this.wgrlTpSin3oAssto = Functions.subString(wgrlTpSin3oAssto, Len.WGRL_TP_SIN3O_ASSTO);
    }

    public String getWgrlTpSin3oAssto() {
        return this.wgrlTpSin3oAssto;
    }

    public void setWgrlDsRiga(long wgrlDsRiga) {
        this.wgrlDsRiga = wgrlDsRiga;
    }

    public long getWgrlDsRiga() {
        return this.wgrlDsRiga;
    }

    public void setWgrlDsOperSql(char wgrlDsOperSql) {
        this.wgrlDsOperSql = wgrlDsOperSql;
    }

    public char getWgrlDsOperSql() {
        return this.wgrlDsOperSql;
    }

    public void setWgrlDsVer(int wgrlDsVer) {
        this.wgrlDsVer = wgrlDsVer;
    }

    public int getWgrlDsVer() {
        return this.wgrlDsVer;
    }

    public void setWgrlDsTsIniCptz(long wgrlDsTsIniCptz) {
        this.wgrlDsTsIniCptz = wgrlDsTsIniCptz;
    }

    public long getWgrlDsTsIniCptz() {
        return this.wgrlDsTsIniCptz;
    }

    public void setWgrlDsTsEndCptz(long wgrlDsTsEndCptz) {
        this.wgrlDsTsEndCptz = wgrlDsTsEndCptz;
    }

    public long getWgrlDsTsEndCptz() {
        return this.wgrlDsTsEndCptz;
    }

    public void setWgrlDsUtente(String wgrlDsUtente) {
        this.wgrlDsUtente = Functions.subString(wgrlDsUtente, Len.WGRL_DS_UTENTE);
    }

    public String getWgrlDsUtente() {
        return this.wgrlDsUtente;
    }

    public void setWgrlDsStatoElab(char wgrlDsStatoElab) {
        this.wgrlDsStatoElab = wgrlDsStatoElab;
    }

    public char getWgrlDsStatoElab() {
        return this.wgrlDsStatoElab;
    }

    public WgrlDtSin1oAssto getWgrlDtSin1oAssto() {
        return wgrlDtSin1oAssto;
    }

    public WgrlDtSin2oAssto getWgrlDtSin2oAssto() {
        return wgrlDtSin2oAssto;
    }

    public WgrlDtSin3oAssto getWgrlDtSin3oAssto() {
        return wgrlDtSin3oAssto;
    }

    public WgrlIdMoviChiu getWgrlIdMoviChiu() {
        return wgrlIdMoviChiu;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRL_ID_GAR_LIQ = 5;
        public static final int WGRL_ID_LIQ = 5;
        public static final int WGRL_ID_GAR = 5;
        public static final int WGRL_ID_MOVI_CRZ = 5;
        public static final int WGRL_DT_INI_EFF = 5;
        public static final int WGRL_DT_END_EFF = 5;
        public static final int WGRL_COD_COMP_ANIA = 3;
        public static final int WGRL_CAU_SIN1O_ASSTO = 2;
        public static final int WGRL_TP_SIN1O_ASSTO = 2;
        public static final int WGRL_CAU_SIN2O_ASSTO = 2;
        public static final int WGRL_TP_SIN2O_ASSTO = 2;
        public static final int WGRL_CAU_SIN3O_ASSTO = 2;
        public static final int WGRL_TP_SIN3O_ASSTO = 2;
        public static final int WGRL_DS_RIGA = 6;
        public static final int WGRL_DS_OPER_SQL = 1;
        public static final int WGRL_DS_VER = 5;
        public static final int WGRL_DS_TS_INI_CPTZ = 10;
        public static final int WGRL_DS_TS_END_CPTZ = 10;
        public static final int WGRL_DS_UTENTE = 20;
        public static final int WGRL_DS_STATO_ELAB = 1;
        public static final int DATI = WGRL_ID_GAR_LIQ + WGRL_ID_LIQ + WGRL_ID_GAR + WGRL_ID_MOVI_CRZ + WgrlIdMoviChiu.Len.WGRL_ID_MOVI_CHIU + WGRL_DT_INI_EFF + WGRL_DT_END_EFF + WGRL_COD_COMP_ANIA + WgrlDtSin1oAssto.Len.WGRL_DT_SIN1O_ASSTO + WGRL_CAU_SIN1O_ASSTO + WGRL_TP_SIN1O_ASSTO + WgrlDtSin2oAssto.Len.WGRL_DT_SIN2O_ASSTO + WGRL_CAU_SIN2O_ASSTO + WGRL_TP_SIN2O_ASSTO + WgrlDtSin3oAssto.Len.WGRL_DT_SIN3O_ASSTO + WGRL_CAU_SIN3O_ASSTO + WGRL_TP_SIN3O_ASSTO + WGRL_DS_RIGA + WGRL_DS_OPER_SQL + WGRL_DS_VER + WGRL_DS_TS_INI_CPTZ + WGRL_DS_TS_END_CPTZ + WGRL_DS_UTENTE + WGRL_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRL_ID_GAR_LIQ = 9;
            public static final int WGRL_ID_LIQ = 9;
            public static final int WGRL_ID_GAR = 9;
            public static final int WGRL_ID_MOVI_CRZ = 9;
            public static final int WGRL_DT_INI_EFF = 8;
            public static final int WGRL_DT_END_EFF = 8;
            public static final int WGRL_COD_COMP_ANIA = 5;
            public static final int WGRL_DS_RIGA = 10;
            public static final int WGRL_DS_VER = 9;
            public static final int WGRL_DS_TS_INI_CPTZ = 18;
            public static final int WGRL_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
