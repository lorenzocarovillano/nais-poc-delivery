package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV2441<br>
 * Variable: LDBV2441 from copybook LDBV2441<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv2441 {

    //==== PROPERTIES ====
    //Original name: LDBV2441-GRAV-FUNZ-FUNZ-1
    private String funz1 = DefaultValues.stringVal(Len.FUNZ1);
    //Original name: LDBV2441-GRAV-FUNZ-FUNZ-2
    private String funz2 = DefaultValues.stringVal(Len.FUNZ2);
    //Original name: LDBV2441-GRAV-FUNZ-FUNZ-3
    private String funz3 = DefaultValues.stringVal(Len.FUNZ3);
    //Original name: LDBV2441-GRAV-FUNZ-FUNZ-4
    private String funz4 = DefaultValues.stringVal(Len.FUNZ4);
    //Original name: LDBV2441-GRAV-FUNZ-FUNZ-5
    private String funz5 = DefaultValues.stringVal(Len.FUNZ5);

    //==== METHODS ====
    public void setLdbv2441Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV2441];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV2441);
        setLdbv2441Bytes(buffer, 1);
    }

    public String getLdbv2441Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv2441Bytes());
    }

    public byte[] getLdbv2441Bytes() {
        byte[] buffer = new byte[Len.LDBV2441];
        return getLdbv2441Bytes(buffer, 1);
    }

    public void setLdbv2441Bytes(byte[] buffer, int offset) {
        int position = offset;
        funz1 = MarshalByte.readString(buffer, position, Len.FUNZ1);
        position += Len.FUNZ1;
        funz2 = MarshalByte.readString(buffer, position, Len.FUNZ2);
        position += Len.FUNZ2;
        funz3 = MarshalByte.readString(buffer, position, Len.FUNZ3);
        position += Len.FUNZ3;
        funz4 = MarshalByte.readString(buffer, position, Len.FUNZ4);
        position += Len.FUNZ4;
        funz5 = MarshalByte.readString(buffer, position, Len.FUNZ5);
    }

    public byte[] getLdbv2441Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, funz1, Len.FUNZ1);
        position += Len.FUNZ1;
        MarshalByte.writeString(buffer, position, funz2, Len.FUNZ2);
        position += Len.FUNZ2;
        MarshalByte.writeString(buffer, position, funz3, Len.FUNZ3);
        position += Len.FUNZ3;
        MarshalByte.writeString(buffer, position, funz4, Len.FUNZ4);
        position += Len.FUNZ4;
        MarshalByte.writeString(buffer, position, funz5, Len.FUNZ5);
        return buffer;
    }

    public void setFunz1(String funz1) {
        this.funz1 = Functions.subString(funz1, Len.FUNZ1);
    }

    public String getFunz1() {
        return this.funz1;
    }

    public void setFunz2(String funz2) {
        this.funz2 = Functions.subString(funz2, Len.FUNZ2);
    }

    public String getFunz2() {
        return this.funz2;
    }

    public void setFunz3(String funz3) {
        this.funz3 = Functions.subString(funz3, Len.FUNZ3);
    }

    public String getFunz3() {
        return this.funz3;
    }

    public void setFunz4(String funz4) {
        this.funz4 = Functions.subString(funz4, Len.FUNZ4);
    }

    public String getFunz4() {
        return this.funz4;
    }

    public void setFunz5(String funz5) {
        this.funz5 = Functions.subString(funz5, Len.FUNZ5);
    }

    public String getFunz5() {
        return this.funz5;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FUNZ1 = 2;
        public static final int FUNZ2 = 2;
        public static final int FUNZ3 = 2;
        public static final int FUNZ4 = 2;
        public static final int FUNZ5 = 2;
        public static final int LDBV2441 = FUNZ1 + FUNZ2 + FUNZ3 + FUNZ4 + FUNZ5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
