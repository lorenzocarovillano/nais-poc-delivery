package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: CAMPI-LOG-ERRORE-REPORT-RIGA-2<br>
 * Variable: CAMPI-LOG-ERRORE-REPORT-RIGA-2 from copybook IABV0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CampiLogErroreReportRiga2 {

    //==== PROPERTIES ====
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2
    private String flr1 = "GRAVITA'";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-1
    private String flr2 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-2
    private String flr3 = "BUSINESS";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-3
    private String flr4 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-4
    private String flr5 = "OGGETTO";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-5
    private String flr6 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-6
    private String flr7 = "POLIZZA";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-7
    private String flr8 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-8
    private String flr9 = "ADESIONE";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-9
    private String flr10 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-10
    private String flr11 = "POLIZZA";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-11
    private String flr12 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-12
    private String flr13 = "ADESIONE";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-13
    private String flr14 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-14
    private String flr15 = "EFFETTO";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-15
    private String flr16 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-16
    private String flr17 = "ERRORE";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-17
    private String flr18 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-18
    private String flr19 = "MAIN";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-19
    private String flr20 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-20
    private String flr21 = "BACK-END";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-21
    private String flr22 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-22
    private String flr23 = "ERRORE";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-23
    private String flr24 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-24
    private String flr25 = "TABELLA";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-25
    private String flr26 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-26
    private String flr27 = "TABELLA";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-27
    private String flr28 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-28
    private String flr29 = "TABELLA";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-29
    private String flr30 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-30
    private String flr31 = "ERRORE";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-31
    private String flr32 = "";
    //Original name: FILLER-CAMPI-LOG-ERRORE-REPORT-RIGA-2-32
    private String flr33 = "TABELLA";

    //==== METHODS ====
    public String getCampiLogErroreReportRiga2Formatted() {
        return MarshalByteExt.bufferToStr(getCampiLogErroreReportRiga2Bytes());
    }

    public byte[] getCampiLogErroreReportRiga2Bytes() {
        byte[] buffer = new byte[Len.CAMPI_LOG_ERRORE_REPORT_RIGA2];
        return getCampiLogErroreReportRiga2Bytes(buffer, 1);
    }

    public byte[] getCampiLogErroreReportRiga2Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
        position += Len.FLR4;
        MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
        position += Len.FLR6;
        MarshalByte.writeString(buffer, position, flr7, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
        position += Len.FLR8;
        MarshalByte.writeString(buffer, position, flr9, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr10, Len.FLR10);
        position += Len.FLR10;
        MarshalByte.writeString(buffer, position, flr11, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeString(buffer, position, flr12, Len.FLR12);
        position += Len.FLR12;
        MarshalByte.writeString(buffer, position, flr13, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr14, Len.FLR14);
        position += Len.FLR14;
        MarshalByte.writeString(buffer, position, flr15, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeString(buffer, position, flr16, Len.FLR16);
        position += Len.FLR16;
        MarshalByte.writeString(buffer, position, flr17, Len.FLR17);
        position += Len.FLR17;
        MarshalByte.writeString(buffer, position, flr18, Len.FLR18);
        position += Len.FLR18;
        MarshalByte.writeString(buffer, position, flr19, Len.FLR19);
        position += Len.FLR19;
        MarshalByte.writeString(buffer, position, flr20, Len.FLR14);
        position += Len.FLR14;
        MarshalByte.writeString(buffer, position, flr21, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr22, Len.FLR22);
        position += Len.FLR22;
        MarshalByte.writeString(buffer, position, flr23, Len.FLR17);
        position += Len.FLR17;
        MarshalByte.writeString(buffer, position, flr24, Len.FLR22);
        position += Len.FLR22;
        MarshalByte.writeString(buffer, position, flr25, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeString(buffer, position, flr26, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr27, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeString(buffer, position, flr28, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, flr29, Len.FLR5);
        position += Len.FLR5;
        MarshalByte.writeString(buffer, position, flr30, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, flr31, Len.FLR17);
        position += Len.FLR17;
        MarshalByte.writeString(buffer, position, flr32, Len.FLR32);
        position += Len.FLR32;
        MarshalByte.writeString(buffer, position, flr33, Len.FLR5);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public String getFlr3() {
        return this.flr3;
    }

    public String getFlr4() {
        return this.flr4;
    }

    public String getFlr5() {
        return this.flr5;
    }

    public String getFlr6() {
        return this.flr6;
    }

    public String getFlr7() {
        return this.flr7;
    }

    public String getFlr8() {
        return this.flr8;
    }

    public String getFlr9() {
        return this.flr9;
    }

    public String getFlr10() {
        return this.flr10;
    }

    public String getFlr11() {
        return this.flr11;
    }

    public String getFlr12() {
        return this.flr12;
    }

    public String getFlr13() {
        return this.flr13;
    }

    public String getFlr14() {
        return this.flr14;
    }

    public String getFlr15() {
        return this.flr15;
    }

    public String getFlr16() {
        return this.flr16;
    }

    public String getFlr17() {
        return this.flr17;
    }

    public String getFlr18() {
        return this.flr18;
    }

    public String getFlr19() {
        return this.flr19;
    }

    public String getFlr20() {
        return this.flr20;
    }

    public String getFlr21() {
        return this.flr21;
    }

    public String getFlr22() {
        return this.flr22;
    }

    public String getFlr23() {
        return this.flr23;
    }

    public String getFlr24() {
        return this.flr24;
    }

    public String getFlr25() {
        return this.flr25;
    }

    public String getFlr26() {
        return this.flr26;
    }

    public String getFlr27() {
        return this.flr27;
    }

    public String getFlr28() {
        return this.flr28;
    }

    public String getFlr29() {
        return this.flr29;
    }

    public String getFlr30() {
        return this.flr30;
    }

    public String getFlr31() {
        return this.flr31;
    }

    public String getFlr32() {
        return this.flr32;
    }

    public String getFlr33() {
        return this.flr33;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 8;
        public static final int FLR2 = 12;
        public static final int FLR4 = 23;
        public static final int FLR5 = 7;
        public static final int FLR6 = 17;
        public static final int FLR8 = 34;
        public static final int FLR10 = 18;
        public static final int FLR12 = 2;
        public static final int FLR14 = 3;
        public static final int FLR16 = 49;
        public static final int FLR17 = 6;
        public static final int FLR18 = 51;
        public static final int FLR19 = 4;
        public static final int FLR22 = 13;
        public static final int FLR32 = 61;
        public static final int CAMPI_LOG_ERRORE_REPORT_RIGA2 = 7 * FLR1 + 2 * FLR2 + FLR4 + 8 * FLR5 + FLR19 + FLR6 + 3 * FLR17 + FLR8 + FLR18 + FLR16 + 2 * FLR14 + 2 * FLR22 + FLR12 + FLR32 + FLR10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
