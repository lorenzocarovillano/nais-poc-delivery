package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpmoAaRenCer;
import it.accenture.jnais.ws.redefines.WpmoCosOner;
import it.accenture.jnais.ws.redefines.WpmoDtRicorPrec;
import it.accenture.jnais.ws.redefines.WpmoDtRicorSucc;
import it.accenture.jnais.ws.redefines.WpmoDtUltErogManfee;
import it.accenture.jnais.ws.redefines.WpmoDurAa;
import it.accenture.jnais.ws.redefines.WpmoDurGg;
import it.accenture.jnais.ws.redefines.WpmoDurMm;
import it.accenture.jnais.ws.redefines.WpmoEtaAaSoglBnficr;
import it.accenture.jnais.ws.redefines.WpmoFrqMovi;
import it.accenture.jnais.ws.redefines.WpmoIdAdes;
import it.accenture.jnais.ws.redefines.WpmoIdMoviChiu;
import it.accenture.jnais.ws.redefines.WpmoImpBnsDaSco;
import it.accenture.jnais.ws.redefines.WpmoImpBnsDaScoTot;
import it.accenture.jnais.ws.redefines.WpmoImpLrdDiRat;
import it.accenture.jnais.ws.redefines.WpmoImpRatManfee;
import it.accenture.jnais.ws.redefines.WpmoImpRiscParzPrgt;
import it.accenture.jnais.ws.redefines.WpmoMmDiff;
import it.accenture.jnais.ws.redefines.WpmoNumRatPagPre;
import it.accenture.jnais.ws.redefines.WpmoPcAnticBns;
import it.accenture.jnais.ws.redefines.WpmoPcApplzOpz;
import it.accenture.jnais.ws.redefines.WpmoPcIntrFraz;
import it.accenture.jnais.ws.redefines.WpmoPcRevrsb;
import it.accenture.jnais.ws.redefines.WpmoPcServVal;
import it.accenture.jnais.ws.redefines.WpmoSomAsstaGarac;
import it.accenture.jnais.ws.redefines.WpmoSpePc;
import it.accenture.jnais.ws.redefines.WpmoTotAaGiaPror;
import it.accenture.jnais.ws.redefines.WpmoTpMovi;
import it.accenture.jnais.ws.redefines.WpmoUltPcPerd;

/**Original name: WPMO-DATI<br>
 * Variable: WPMO-DATI from copybook LCCVPMO1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WpmoDati {

    //==== PROPERTIES ====
    //Original name: WPMO-ID-PARAM-MOVI
    private int wpmoIdParamMovi = DefaultValues.INT_VAL;
    //Original name: WPMO-ID-OGG
    private int wpmoIdOgg = DefaultValues.INT_VAL;
    //Original name: WPMO-TP-OGG
    private String wpmoTpOgg = DefaultValues.stringVal(Len.WPMO_TP_OGG);
    //Original name: WPMO-ID-MOVI-CRZ
    private int wpmoIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WPMO-ID-MOVI-CHIU
    private WpmoIdMoviChiu wpmoIdMoviChiu = new WpmoIdMoviChiu();
    //Original name: WPMO-DT-INI-EFF
    private int wpmoDtIniEff = DefaultValues.INT_VAL;
    //Original name: WPMO-DT-END-EFF
    private int wpmoDtEndEff = DefaultValues.INT_VAL;
    //Original name: WPMO-COD-COMP-ANIA
    private int wpmoCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WPMO-TP-MOVI
    private WpmoTpMovi wpmoTpMovi = new WpmoTpMovi();
    //Original name: WPMO-FRQ-MOVI
    private WpmoFrqMovi wpmoFrqMovi = new WpmoFrqMovi();
    //Original name: WPMO-DUR-AA
    private WpmoDurAa wpmoDurAa = new WpmoDurAa();
    //Original name: WPMO-DUR-MM
    private WpmoDurMm wpmoDurMm = new WpmoDurMm();
    //Original name: WPMO-DUR-GG
    private WpmoDurGg wpmoDurGg = new WpmoDurGg();
    //Original name: WPMO-DT-RICOR-PREC
    private WpmoDtRicorPrec wpmoDtRicorPrec = new WpmoDtRicorPrec();
    //Original name: WPMO-DT-RICOR-SUCC
    private WpmoDtRicorSucc wpmoDtRicorSucc = new WpmoDtRicorSucc();
    //Original name: WPMO-PC-INTR-FRAZ
    private WpmoPcIntrFraz wpmoPcIntrFraz = new WpmoPcIntrFraz();
    //Original name: WPMO-IMP-BNS-DA-SCO-TOT
    private WpmoImpBnsDaScoTot wpmoImpBnsDaScoTot = new WpmoImpBnsDaScoTot();
    //Original name: WPMO-IMP-BNS-DA-SCO
    private WpmoImpBnsDaSco wpmoImpBnsDaSco = new WpmoImpBnsDaSco();
    //Original name: WPMO-PC-ANTIC-BNS
    private WpmoPcAnticBns wpmoPcAnticBns = new WpmoPcAnticBns();
    //Original name: WPMO-TP-RINN-COLL
    private String wpmoTpRinnColl = DefaultValues.stringVal(Len.WPMO_TP_RINN_COLL);
    //Original name: WPMO-TP-RIVAL-PRE
    private String wpmoTpRivalPre = DefaultValues.stringVal(Len.WPMO_TP_RIVAL_PRE);
    //Original name: WPMO-TP-RIVAL-PRSTZ
    private String wpmoTpRivalPrstz = DefaultValues.stringVal(Len.WPMO_TP_RIVAL_PRSTZ);
    //Original name: WPMO-FL-EVID-RIVAL
    private char wpmoFlEvidRival = DefaultValues.CHAR_VAL;
    //Original name: WPMO-ULT-PC-PERD
    private WpmoUltPcPerd wpmoUltPcPerd = new WpmoUltPcPerd();
    //Original name: WPMO-TOT-AA-GIA-PROR
    private WpmoTotAaGiaPror wpmoTotAaGiaPror = new WpmoTotAaGiaPror();
    //Original name: WPMO-TP-OPZ
    private String wpmoTpOpz = DefaultValues.stringVal(Len.WPMO_TP_OPZ);
    //Original name: WPMO-AA-REN-CER
    private WpmoAaRenCer wpmoAaRenCer = new WpmoAaRenCer();
    //Original name: WPMO-PC-REVRSB
    private WpmoPcRevrsb wpmoPcRevrsb = new WpmoPcRevrsb();
    //Original name: WPMO-IMP-RISC-PARZ-PRGT
    private WpmoImpRiscParzPrgt wpmoImpRiscParzPrgt = new WpmoImpRiscParzPrgt();
    //Original name: WPMO-IMP-LRD-DI-RAT
    private WpmoImpLrdDiRat wpmoImpLrdDiRat = new WpmoImpLrdDiRat();
    //Original name: WPMO-IB-OGG
    private String wpmoIbOgg = DefaultValues.stringVal(Len.WPMO_IB_OGG);
    //Original name: WPMO-COS-ONER
    private WpmoCosOner wpmoCosOner = new WpmoCosOner();
    //Original name: WPMO-SPE-PC
    private WpmoSpePc wpmoSpePc = new WpmoSpePc();
    //Original name: WPMO-FL-ATTIV-GAR
    private char wpmoFlAttivGar = DefaultValues.CHAR_VAL;
    //Original name: WPMO-CAMBIO-VER-PROD
    private char wpmoCambioVerProd = DefaultValues.CHAR_VAL;
    //Original name: WPMO-MM-DIFF
    private WpmoMmDiff wpmoMmDiff = new WpmoMmDiff();
    //Original name: WPMO-IMP-RAT-MANFEE
    private WpmoImpRatManfee wpmoImpRatManfee = new WpmoImpRatManfee();
    //Original name: WPMO-DT-ULT-EROG-MANFEE
    private WpmoDtUltErogManfee wpmoDtUltErogManfee = new WpmoDtUltErogManfee();
    //Original name: WPMO-TP-OGG-RIVAL
    private String wpmoTpOggRival = DefaultValues.stringVal(Len.WPMO_TP_OGG_RIVAL);
    //Original name: WPMO-SOM-ASSTA-GARAC
    private WpmoSomAsstaGarac wpmoSomAsstaGarac = new WpmoSomAsstaGarac();
    //Original name: WPMO-PC-APPLZ-OPZ
    private WpmoPcApplzOpz wpmoPcApplzOpz = new WpmoPcApplzOpz();
    //Original name: WPMO-ID-ADES
    private WpmoIdAdes wpmoIdAdes = new WpmoIdAdes();
    //Original name: WPMO-ID-POLI
    private int wpmoIdPoli = DefaultValues.INT_VAL;
    //Original name: WPMO-TP-FRM-ASSVA
    private String wpmoTpFrmAssva = DefaultValues.stringVal(Len.WPMO_TP_FRM_ASSVA);
    //Original name: WPMO-DS-RIGA
    private long wpmoDsRiga = DefaultValues.LONG_VAL;
    //Original name: WPMO-DS-OPER-SQL
    private char wpmoDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WPMO-DS-VER
    private int wpmoDsVer = DefaultValues.INT_VAL;
    //Original name: WPMO-DS-TS-INI-CPTZ
    private long wpmoDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WPMO-DS-TS-END-CPTZ
    private long wpmoDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WPMO-DS-UTENTE
    private String wpmoDsUtente = DefaultValues.stringVal(Len.WPMO_DS_UTENTE);
    //Original name: WPMO-DS-STATO-ELAB
    private char wpmoDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WPMO-TP-ESTR-CNT
    private String wpmoTpEstrCnt = DefaultValues.stringVal(Len.WPMO_TP_ESTR_CNT);
    //Original name: WPMO-COD-RAMO
    private String wpmoCodRamo = DefaultValues.stringVal(Len.WPMO_COD_RAMO);
    //Original name: WPMO-GEN-DA-SIN
    private char wpmoGenDaSin = DefaultValues.CHAR_VAL;
    //Original name: WPMO-COD-TARI
    private String wpmoCodTari = DefaultValues.stringVal(Len.WPMO_COD_TARI);
    //Original name: WPMO-NUM-RAT-PAG-PRE
    private WpmoNumRatPagPre wpmoNumRatPagPre = new WpmoNumRatPagPre();
    //Original name: WPMO-PC-SERV-VAL
    private WpmoPcServVal wpmoPcServVal = new WpmoPcServVal();
    //Original name: WPMO-ETA-AA-SOGL-BNFICR
    private WpmoEtaAaSoglBnficr wpmoEtaAaSoglBnficr = new WpmoEtaAaSoglBnficr();

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wpmoIdParamMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPMO_ID_PARAM_MOVI, 0);
        position += Len.WPMO_ID_PARAM_MOVI;
        wpmoIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPMO_ID_OGG, 0);
        position += Len.WPMO_ID_OGG;
        wpmoTpOgg = MarshalByte.readString(buffer, position, Len.WPMO_TP_OGG);
        position += Len.WPMO_TP_OGG;
        wpmoIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPMO_ID_MOVI_CRZ, 0);
        position += Len.WPMO_ID_MOVI_CRZ;
        wpmoIdMoviChiu.setWpmoIdMoviChiuFromBuffer(buffer, position);
        position += WpmoIdMoviChiu.Len.WPMO_ID_MOVI_CHIU;
        wpmoDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPMO_DT_INI_EFF, 0);
        position += Len.WPMO_DT_INI_EFF;
        wpmoDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPMO_DT_END_EFF, 0);
        position += Len.WPMO_DT_END_EFF;
        wpmoCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPMO_COD_COMP_ANIA, 0);
        position += Len.WPMO_COD_COMP_ANIA;
        wpmoTpMovi.setWpmoTpMoviFromBuffer(buffer, position);
        position += WpmoTpMovi.Len.WPMO_TP_MOVI;
        wpmoFrqMovi.setWpmoFrqMoviFromBuffer(buffer, position);
        position += WpmoFrqMovi.Len.WPMO_FRQ_MOVI;
        wpmoDurAa.setWpmoDurAaFromBuffer(buffer, position);
        position += WpmoDurAa.Len.WPMO_DUR_AA;
        wpmoDurMm.setWpmoDurMmFromBuffer(buffer, position);
        position += WpmoDurMm.Len.WPMO_DUR_MM;
        wpmoDurGg.setWpmoDurGgFromBuffer(buffer, position);
        position += WpmoDurGg.Len.WPMO_DUR_GG;
        wpmoDtRicorPrec.setWpmoDtRicorPrecFromBuffer(buffer, position);
        position += WpmoDtRicorPrec.Len.WPMO_DT_RICOR_PREC;
        wpmoDtRicorSucc.setWpmoDtRicorSuccFromBuffer(buffer, position);
        position += WpmoDtRicorSucc.Len.WPMO_DT_RICOR_SUCC;
        wpmoPcIntrFraz.setWpmoPcIntrFrazFromBuffer(buffer, position);
        position += WpmoPcIntrFraz.Len.WPMO_PC_INTR_FRAZ;
        wpmoImpBnsDaScoTot.setWpmoImpBnsDaScoTotFromBuffer(buffer, position);
        position += WpmoImpBnsDaScoTot.Len.WPMO_IMP_BNS_DA_SCO_TOT;
        wpmoImpBnsDaSco.setWpmoImpBnsDaScoFromBuffer(buffer, position);
        position += WpmoImpBnsDaSco.Len.WPMO_IMP_BNS_DA_SCO;
        wpmoPcAnticBns.setWpmoPcAnticBnsFromBuffer(buffer, position);
        position += WpmoPcAnticBns.Len.WPMO_PC_ANTIC_BNS;
        wpmoTpRinnColl = MarshalByte.readString(buffer, position, Len.WPMO_TP_RINN_COLL);
        position += Len.WPMO_TP_RINN_COLL;
        wpmoTpRivalPre = MarshalByte.readString(buffer, position, Len.WPMO_TP_RIVAL_PRE);
        position += Len.WPMO_TP_RIVAL_PRE;
        wpmoTpRivalPrstz = MarshalByte.readString(buffer, position, Len.WPMO_TP_RIVAL_PRSTZ);
        position += Len.WPMO_TP_RIVAL_PRSTZ;
        wpmoFlEvidRival = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpmoUltPcPerd.setWpmoUltPcPerdFromBuffer(buffer, position);
        position += WpmoUltPcPerd.Len.WPMO_ULT_PC_PERD;
        wpmoTotAaGiaPror.setWpmoTotAaGiaProrFromBuffer(buffer, position);
        position += WpmoTotAaGiaPror.Len.WPMO_TOT_AA_GIA_PROR;
        wpmoTpOpz = MarshalByte.readString(buffer, position, Len.WPMO_TP_OPZ);
        position += Len.WPMO_TP_OPZ;
        wpmoAaRenCer.setWpmoAaRenCerFromBuffer(buffer, position);
        position += WpmoAaRenCer.Len.WPMO_AA_REN_CER;
        wpmoPcRevrsb.setWpmoPcRevrsbFromBuffer(buffer, position);
        position += WpmoPcRevrsb.Len.WPMO_PC_REVRSB;
        wpmoImpRiscParzPrgt.setWpmoImpRiscParzPrgtFromBuffer(buffer, position);
        position += WpmoImpRiscParzPrgt.Len.WPMO_IMP_RISC_PARZ_PRGT;
        wpmoImpLrdDiRat.setWpmoImpLrdDiRatFromBuffer(buffer, position);
        position += WpmoImpLrdDiRat.Len.WPMO_IMP_LRD_DI_RAT;
        wpmoIbOgg = MarshalByte.readString(buffer, position, Len.WPMO_IB_OGG);
        position += Len.WPMO_IB_OGG;
        wpmoCosOner.setWpmoCosOnerFromBuffer(buffer, position);
        position += WpmoCosOner.Len.WPMO_COS_ONER;
        wpmoSpePc.setWpmoSpePcFromBuffer(buffer, position);
        position += WpmoSpePc.Len.WPMO_SPE_PC;
        wpmoFlAttivGar = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpmoCambioVerProd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpmoMmDiff.setWpmoMmDiffFromBuffer(buffer, position);
        position += WpmoMmDiff.Len.WPMO_MM_DIFF;
        wpmoImpRatManfee.setWpmoImpRatManfeeFromBuffer(buffer, position);
        position += WpmoImpRatManfee.Len.WPMO_IMP_RAT_MANFEE;
        wpmoDtUltErogManfee.setWpmoDtUltErogManfeeFromBuffer(buffer, position);
        position += WpmoDtUltErogManfee.Len.WPMO_DT_ULT_EROG_MANFEE;
        wpmoTpOggRival = MarshalByte.readString(buffer, position, Len.WPMO_TP_OGG_RIVAL);
        position += Len.WPMO_TP_OGG_RIVAL;
        wpmoSomAsstaGarac.setWpmoSomAsstaGaracFromBuffer(buffer, position);
        position += WpmoSomAsstaGarac.Len.WPMO_SOM_ASSTA_GARAC;
        wpmoPcApplzOpz.setWpmoPcApplzOpzFromBuffer(buffer, position);
        position += WpmoPcApplzOpz.Len.WPMO_PC_APPLZ_OPZ;
        wpmoIdAdes.setWpmoIdAdesFromBuffer(buffer, position);
        position += WpmoIdAdes.Len.WPMO_ID_ADES;
        wpmoIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPMO_ID_POLI, 0);
        position += Len.WPMO_ID_POLI;
        wpmoTpFrmAssva = MarshalByte.readString(buffer, position, Len.WPMO_TP_FRM_ASSVA);
        position += Len.WPMO_TP_FRM_ASSVA;
        wpmoDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPMO_DS_RIGA, 0);
        position += Len.WPMO_DS_RIGA;
        wpmoDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpmoDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPMO_DS_VER, 0);
        position += Len.WPMO_DS_VER;
        wpmoDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPMO_DS_TS_INI_CPTZ, 0);
        position += Len.WPMO_DS_TS_INI_CPTZ;
        wpmoDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPMO_DS_TS_END_CPTZ, 0);
        position += Len.WPMO_DS_TS_END_CPTZ;
        wpmoDsUtente = MarshalByte.readString(buffer, position, Len.WPMO_DS_UTENTE);
        position += Len.WPMO_DS_UTENTE;
        wpmoDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpmoTpEstrCnt = MarshalByte.readString(buffer, position, Len.WPMO_TP_ESTR_CNT);
        position += Len.WPMO_TP_ESTR_CNT;
        wpmoCodRamo = MarshalByte.readString(buffer, position, Len.WPMO_COD_RAMO);
        position += Len.WPMO_COD_RAMO;
        wpmoGenDaSin = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpmoCodTari = MarshalByte.readString(buffer, position, Len.WPMO_COD_TARI);
        position += Len.WPMO_COD_TARI;
        wpmoNumRatPagPre.setWpmoNumRatPagPreFromBuffer(buffer, position);
        position += WpmoNumRatPagPre.Len.WPMO_NUM_RAT_PAG_PRE;
        wpmoPcServVal.setWpmoPcServValFromBuffer(buffer, position);
        position += WpmoPcServVal.Len.WPMO_PC_SERV_VAL;
        wpmoEtaAaSoglBnficr.setWpmoEtaAaSoglBnficrFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wpmoIdParamMovi, Len.Int.WPMO_ID_PARAM_MOVI, 0);
        position += Len.WPMO_ID_PARAM_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, wpmoIdOgg, Len.Int.WPMO_ID_OGG, 0);
        position += Len.WPMO_ID_OGG;
        MarshalByte.writeString(buffer, position, wpmoTpOgg, Len.WPMO_TP_OGG);
        position += Len.WPMO_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, wpmoIdMoviCrz, Len.Int.WPMO_ID_MOVI_CRZ, 0);
        position += Len.WPMO_ID_MOVI_CRZ;
        wpmoIdMoviChiu.getWpmoIdMoviChiuAsBuffer(buffer, position);
        position += WpmoIdMoviChiu.Len.WPMO_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wpmoDtIniEff, Len.Int.WPMO_DT_INI_EFF, 0);
        position += Len.WPMO_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpmoDtEndEff, Len.Int.WPMO_DT_END_EFF, 0);
        position += Len.WPMO_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpmoCodCompAnia, Len.Int.WPMO_COD_COMP_ANIA, 0);
        position += Len.WPMO_COD_COMP_ANIA;
        wpmoTpMovi.getWpmoTpMoviAsBuffer(buffer, position);
        position += WpmoTpMovi.Len.WPMO_TP_MOVI;
        wpmoFrqMovi.getWpmoFrqMoviAsBuffer(buffer, position);
        position += WpmoFrqMovi.Len.WPMO_FRQ_MOVI;
        wpmoDurAa.getWpmoDurAaAsBuffer(buffer, position);
        position += WpmoDurAa.Len.WPMO_DUR_AA;
        wpmoDurMm.getWpmoDurMmAsBuffer(buffer, position);
        position += WpmoDurMm.Len.WPMO_DUR_MM;
        wpmoDurGg.getWpmoDurGgAsBuffer(buffer, position);
        position += WpmoDurGg.Len.WPMO_DUR_GG;
        wpmoDtRicorPrec.getWpmoDtRicorPrecAsBuffer(buffer, position);
        position += WpmoDtRicorPrec.Len.WPMO_DT_RICOR_PREC;
        wpmoDtRicorSucc.getWpmoDtRicorSuccAsBuffer(buffer, position);
        position += WpmoDtRicorSucc.Len.WPMO_DT_RICOR_SUCC;
        wpmoPcIntrFraz.getWpmoPcIntrFrazAsBuffer(buffer, position);
        position += WpmoPcIntrFraz.Len.WPMO_PC_INTR_FRAZ;
        wpmoImpBnsDaScoTot.getWpmoImpBnsDaScoTotAsBuffer(buffer, position);
        position += WpmoImpBnsDaScoTot.Len.WPMO_IMP_BNS_DA_SCO_TOT;
        wpmoImpBnsDaSco.getWpmoImpBnsDaScoAsBuffer(buffer, position);
        position += WpmoImpBnsDaSco.Len.WPMO_IMP_BNS_DA_SCO;
        wpmoPcAnticBns.getWpmoPcAnticBnsAsBuffer(buffer, position);
        position += WpmoPcAnticBns.Len.WPMO_PC_ANTIC_BNS;
        MarshalByte.writeString(buffer, position, wpmoTpRinnColl, Len.WPMO_TP_RINN_COLL);
        position += Len.WPMO_TP_RINN_COLL;
        MarshalByte.writeString(buffer, position, wpmoTpRivalPre, Len.WPMO_TP_RIVAL_PRE);
        position += Len.WPMO_TP_RIVAL_PRE;
        MarshalByte.writeString(buffer, position, wpmoTpRivalPrstz, Len.WPMO_TP_RIVAL_PRSTZ);
        position += Len.WPMO_TP_RIVAL_PRSTZ;
        MarshalByte.writeChar(buffer, position, wpmoFlEvidRival);
        position += Types.CHAR_SIZE;
        wpmoUltPcPerd.getWpmoUltPcPerdAsBuffer(buffer, position);
        position += WpmoUltPcPerd.Len.WPMO_ULT_PC_PERD;
        wpmoTotAaGiaPror.getWpmoTotAaGiaProrAsBuffer(buffer, position);
        position += WpmoTotAaGiaPror.Len.WPMO_TOT_AA_GIA_PROR;
        MarshalByte.writeString(buffer, position, wpmoTpOpz, Len.WPMO_TP_OPZ);
        position += Len.WPMO_TP_OPZ;
        wpmoAaRenCer.getWpmoAaRenCerAsBuffer(buffer, position);
        position += WpmoAaRenCer.Len.WPMO_AA_REN_CER;
        wpmoPcRevrsb.getWpmoPcRevrsbAsBuffer(buffer, position);
        position += WpmoPcRevrsb.Len.WPMO_PC_REVRSB;
        wpmoImpRiscParzPrgt.getWpmoImpRiscParzPrgtAsBuffer(buffer, position);
        position += WpmoImpRiscParzPrgt.Len.WPMO_IMP_RISC_PARZ_PRGT;
        wpmoImpLrdDiRat.getWpmoImpLrdDiRatAsBuffer(buffer, position);
        position += WpmoImpLrdDiRat.Len.WPMO_IMP_LRD_DI_RAT;
        MarshalByte.writeString(buffer, position, wpmoIbOgg, Len.WPMO_IB_OGG);
        position += Len.WPMO_IB_OGG;
        wpmoCosOner.getWpmoCosOnerAsBuffer(buffer, position);
        position += WpmoCosOner.Len.WPMO_COS_ONER;
        wpmoSpePc.getWpmoSpePcAsBuffer(buffer, position);
        position += WpmoSpePc.Len.WPMO_SPE_PC;
        MarshalByte.writeChar(buffer, position, wpmoFlAttivGar);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpmoCambioVerProd);
        position += Types.CHAR_SIZE;
        wpmoMmDiff.getWpmoMmDiffAsBuffer(buffer, position);
        position += WpmoMmDiff.Len.WPMO_MM_DIFF;
        wpmoImpRatManfee.getWpmoImpRatManfeeAsBuffer(buffer, position);
        position += WpmoImpRatManfee.Len.WPMO_IMP_RAT_MANFEE;
        wpmoDtUltErogManfee.getWpmoDtUltErogManfeeAsBuffer(buffer, position);
        position += WpmoDtUltErogManfee.Len.WPMO_DT_ULT_EROG_MANFEE;
        MarshalByte.writeString(buffer, position, wpmoTpOggRival, Len.WPMO_TP_OGG_RIVAL);
        position += Len.WPMO_TP_OGG_RIVAL;
        wpmoSomAsstaGarac.getWpmoSomAsstaGaracAsBuffer(buffer, position);
        position += WpmoSomAsstaGarac.Len.WPMO_SOM_ASSTA_GARAC;
        wpmoPcApplzOpz.getWpmoPcApplzOpzAsBuffer(buffer, position);
        position += WpmoPcApplzOpz.Len.WPMO_PC_APPLZ_OPZ;
        wpmoIdAdes.getWpmoIdAdesAsBuffer(buffer, position);
        position += WpmoIdAdes.Len.WPMO_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wpmoIdPoli, Len.Int.WPMO_ID_POLI, 0);
        position += Len.WPMO_ID_POLI;
        MarshalByte.writeString(buffer, position, wpmoTpFrmAssva, Len.WPMO_TP_FRM_ASSVA);
        position += Len.WPMO_TP_FRM_ASSVA;
        MarshalByte.writeLongAsPacked(buffer, position, wpmoDsRiga, Len.Int.WPMO_DS_RIGA, 0);
        position += Len.WPMO_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wpmoDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wpmoDsVer, Len.Int.WPMO_DS_VER, 0);
        position += Len.WPMO_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wpmoDsTsIniCptz, Len.Int.WPMO_DS_TS_INI_CPTZ, 0);
        position += Len.WPMO_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wpmoDsTsEndCptz, Len.Int.WPMO_DS_TS_END_CPTZ, 0);
        position += Len.WPMO_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wpmoDsUtente, Len.WPMO_DS_UTENTE);
        position += Len.WPMO_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wpmoDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wpmoTpEstrCnt, Len.WPMO_TP_ESTR_CNT);
        position += Len.WPMO_TP_ESTR_CNT;
        MarshalByte.writeString(buffer, position, wpmoCodRamo, Len.WPMO_COD_RAMO);
        position += Len.WPMO_COD_RAMO;
        MarshalByte.writeChar(buffer, position, wpmoGenDaSin);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wpmoCodTari, Len.WPMO_COD_TARI);
        position += Len.WPMO_COD_TARI;
        wpmoNumRatPagPre.getWpmoNumRatPagPreAsBuffer(buffer, position);
        position += WpmoNumRatPagPre.Len.WPMO_NUM_RAT_PAG_PRE;
        wpmoPcServVal.getWpmoPcServValAsBuffer(buffer, position);
        position += WpmoPcServVal.Len.WPMO_PC_SERV_VAL;
        wpmoEtaAaSoglBnficr.getWpmoEtaAaSoglBnficrAsBuffer(buffer, position);
        return buffer;
    }

    public void initDatiSpaces() {
        wpmoIdParamMovi = Types.INVALID_INT_VAL;
        wpmoIdOgg = Types.INVALID_INT_VAL;
        wpmoTpOgg = "";
        wpmoIdMoviCrz = Types.INVALID_INT_VAL;
        wpmoIdMoviChiu.initWpmoIdMoviChiuSpaces();
        wpmoDtIniEff = Types.INVALID_INT_VAL;
        wpmoDtEndEff = Types.INVALID_INT_VAL;
        wpmoCodCompAnia = Types.INVALID_INT_VAL;
        wpmoTpMovi.initWpmoTpMoviSpaces();
        wpmoFrqMovi.initWpmoFrqMoviSpaces();
        wpmoDurAa.initWpmoDurAaSpaces();
        wpmoDurMm.initWpmoDurMmSpaces();
        wpmoDurGg.initWpmoDurGgSpaces();
        wpmoDtRicorPrec.initWpmoDtRicorPrecSpaces();
        wpmoDtRicorSucc.initWpmoDtRicorSuccSpaces();
        wpmoPcIntrFraz.initWpmoPcIntrFrazSpaces();
        wpmoImpBnsDaScoTot.initWpmoImpBnsDaScoTotSpaces();
        wpmoImpBnsDaSco.initWpmoImpBnsDaScoSpaces();
        wpmoPcAnticBns.initWpmoPcAnticBnsSpaces();
        wpmoTpRinnColl = "";
        wpmoTpRivalPre = "";
        wpmoTpRivalPrstz = "";
        wpmoFlEvidRival = Types.SPACE_CHAR;
        wpmoUltPcPerd.initWpmoUltPcPerdSpaces();
        wpmoTotAaGiaPror.initWpmoTotAaGiaProrSpaces();
        wpmoTpOpz = "";
        wpmoAaRenCer.initWpmoAaRenCerSpaces();
        wpmoPcRevrsb.initWpmoPcRevrsbSpaces();
        wpmoImpRiscParzPrgt.initWpmoImpRiscParzPrgtSpaces();
        wpmoImpLrdDiRat.initWpmoImpLrdDiRatSpaces();
        wpmoIbOgg = "";
        wpmoCosOner.initWpmoCosOnerSpaces();
        wpmoSpePc.initWpmoSpePcSpaces();
        wpmoFlAttivGar = Types.SPACE_CHAR;
        wpmoCambioVerProd = Types.SPACE_CHAR;
        wpmoMmDiff.initWpmoMmDiffSpaces();
        wpmoImpRatManfee.initWpmoImpRatManfeeSpaces();
        wpmoDtUltErogManfee.initWpmoDtUltErogManfeeSpaces();
        wpmoTpOggRival = "";
        wpmoSomAsstaGarac.initWpmoSomAsstaGaracSpaces();
        wpmoPcApplzOpz.initWpmoPcApplzOpzSpaces();
        wpmoIdAdes.initWpmoIdAdesSpaces();
        wpmoIdPoli = Types.INVALID_INT_VAL;
        wpmoTpFrmAssva = "";
        wpmoDsRiga = Types.INVALID_LONG_VAL;
        wpmoDsOperSql = Types.SPACE_CHAR;
        wpmoDsVer = Types.INVALID_INT_VAL;
        wpmoDsTsIniCptz = Types.INVALID_LONG_VAL;
        wpmoDsTsEndCptz = Types.INVALID_LONG_VAL;
        wpmoDsUtente = "";
        wpmoDsStatoElab = Types.SPACE_CHAR;
        wpmoTpEstrCnt = "";
        wpmoCodRamo = "";
        wpmoGenDaSin = Types.SPACE_CHAR;
        wpmoCodTari = "";
        wpmoNumRatPagPre.initWpmoNumRatPagPreSpaces();
        wpmoPcServVal.initWpmoPcServValSpaces();
        wpmoEtaAaSoglBnficr.initWpmoEtaAaSoglBnficrSpaces();
    }

    public void setWpmoIdParamMovi(int wpmoIdParamMovi) {
        this.wpmoIdParamMovi = wpmoIdParamMovi;
    }

    public int getWpmoIdParamMovi() {
        return this.wpmoIdParamMovi;
    }

    public void setWpmoIdOgg(int wpmoIdOgg) {
        this.wpmoIdOgg = wpmoIdOgg;
    }

    public int getWpmoIdOgg() {
        return this.wpmoIdOgg;
    }

    public String getWpmoIdOggFormatted() {
        return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.PACKED)).format(getWpmoIdOgg()).toString();
    }

    public void setWpmoTpOgg(String wpmoTpOgg) {
        this.wpmoTpOgg = Functions.subString(wpmoTpOgg, Len.WPMO_TP_OGG);
    }

    public String getWpmoTpOgg() {
        return this.wpmoTpOgg;
    }

    public void setWpmoIdMoviCrz(int wpmoIdMoviCrz) {
        this.wpmoIdMoviCrz = wpmoIdMoviCrz;
    }

    public int getWpmoIdMoviCrz() {
        return this.wpmoIdMoviCrz;
    }

    public void setWpmoDtIniEff(int wpmoDtIniEff) {
        this.wpmoDtIniEff = wpmoDtIniEff;
    }

    public int getWpmoDtIniEff() {
        return this.wpmoDtIniEff;
    }

    public void setWpmoDtEndEff(int wpmoDtEndEff) {
        this.wpmoDtEndEff = wpmoDtEndEff;
    }

    public int getWpmoDtEndEff() {
        return this.wpmoDtEndEff;
    }

    public void setWpmoCodCompAnia(int wpmoCodCompAnia) {
        this.wpmoCodCompAnia = wpmoCodCompAnia;
    }

    public int getWpmoCodCompAnia() {
        return this.wpmoCodCompAnia;
    }

    public void setWpmoTpRinnColl(String wpmoTpRinnColl) {
        this.wpmoTpRinnColl = Functions.subString(wpmoTpRinnColl, Len.WPMO_TP_RINN_COLL);
    }

    public String getWpmoTpRinnColl() {
        return this.wpmoTpRinnColl;
    }

    public String getWpmoTpRinnCollFormatted() {
        return Functions.padBlanks(getWpmoTpRinnColl(), Len.WPMO_TP_RINN_COLL);
    }

    public void setWpmoTpRivalPre(String wpmoTpRivalPre) {
        this.wpmoTpRivalPre = Functions.subString(wpmoTpRivalPre, Len.WPMO_TP_RIVAL_PRE);
    }

    public String getWpmoTpRivalPre() {
        return this.wpmoTpRivalPre;
    }

    public String getWpmoTpRivalPreFormatted() {
        return Functions.padBlanks(getWpmoTpRivalPre(), Len.WPMO_TP_RIVAL_PRE);
    }

    public void setWpmoTpRivalPrstz(String wpmoTpRivalPrstz) {
        this.wpmoTpRivalPrstz = Functions.subString(wpmoTpRivalPrstz, Len.WPMO_TP_RIVAL_PRSTZ);
    }

    public String getWpmoTpRivalPrstz() {
        return this.wpmoTpRivalPrstz;
    }

    public String getWpmoTpRivalPrstzFormatted() {
        return Functions.padBlanks(getWpmoTpRivalPrstz(), Len.WPMO_TP_RIVAL_PRSTZ);
    }

    public void setWpmoFlEvidRival(char wpmoFlEvidRival) {
        this.wpmoFlEvidRival = wpmoFlEvidRival;
    }

    public char getWpmoFlEvidRival() {
        return this.wpmoFlEvidRival;
    }

    public void setWpmoTpOpz(String wpmoTpOpz) {
        this.wpmoTpOpz = Functions.subString(wpmoTpOpz, Len.WPMO_TP_OPZ);
    }

    public String getWpmoTpOpz() {
        return this.wpmoTpOpz;
    }

    public String getWpmoTpOpzFormatted() {
        return Functions.padBlanks(getWpmoTpOpz(), Len.WPMO_TP_OPZ);
    }

    public void setWpmoIbOgg(String wpmoIbOgg) {
        this.wpmoIbOgg = Functions.subString(wpmoIbOgg, Len.WPMO_IB_OGG);
    }

    public String getWpmoIbOgg() {
        return this.wpmoIbOgg;
    }

    public void setWpmoFlAttivGar(char wpmoFlAttivGar) {
        this.wpmoFlAttivGar = wpmoFlAttivGar;
    }

    public char getWpmoFlAttivGar() {
        return this.wpmoFlAttivGar;
    }

    public void setWpmoCambioVerProd(char wpmoCambioVerProd) {
        this.wpmoCambioVerProd = wpmoCambioVerProd;
    }

    public char getWpmoCambioVerProd() {
        return this.wpmoCambioVerProd;
    }

    public void setWpmoTpOggRival(String wpmoTpOggRival) {
        this.wpmoTpOggRival = Functions.subString(wpmoTpOggRival, Len.WPMO_TP_OGG_RIVAL);
    }

    public String getWpmoTpOggRival() {
        return this.wpmoTpOggRival;
    }

    public String getWpmoTpOggRivalFormatted() {
        return Functions.padBlanks(getWpmoTpOggRival(), Len.WPMO_TP_OGG_RIVAL);
    }

    public void setWpmoIdPoli(int wpmoIdPoli) {
        this.wpmoIdPoli = wpmoIdPoli;
    }

    public int getWpmoIdPoli() {
        return this.wpmoIdPoli;
    }

    public String getWpmoIdPoliFormatted() {
        return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.PACKED)).format(getWpmoIdPoli()).toString();
    }

    public void setWpmoTpFrmAssva(String wpmoTpFrmAssva) {
        this.wpmoTpFrmAssva = Functions.subString(wpmoTpFrmAssva, Len.WPMO_TP_FRM_ASSVA);
    }

    public String getWpmoTpFrmAssva() {
        return this.wpmoTpFrmAssva;
    }

    public void setWpmoDsRiga(long wpmoDsRiga) {
        this.wpmoDsRiga = wpmoDsRiga;
    }

    public long getWpmoDsRiga() {
        return this.wpmoDsRiga;
    }

    public void setWpmoDsOperSql(char wpmoDsOperSql) {
        this.wpmoDsOperSql = wpmoDsOperSql;
    }

    public char getWpmoDsOperSql() {
        return this.wpmoDsOperSql;
    }

    public void setWpmoDsVer(int wpmoDsVer) {
        this.wpmoDsVer = wpmoDsVer;
    }

    public int getWpmoDsVer() {
        return this.wpmoDsVer;
    }

    public void setWpmoDsTsIniCptz(long wpmoDsTsIniCptz) {
        this.wpmoDsTsIniCptz = wpmoDsTsIniCptz;
    }

    public long getWpmoDsTsIniCptz() {
        return this.wpmoDsTsIniCptz;
    }

    public void setWpmoDsTsEndCptz(long wpmoDsTsEndCptz) {
        this.wpmoDsTsEndCptz = wpmoDsTsEndCptz;
    }

    public long getWpmoDsTsEndCptz() {
        return this.wpmoDsTsEndCptz;
    }

    public void setWpmoDsUtente(String wpmoDsUtente) {
        this.wpmoDsUtente = Functions.subString(wpmoDsUtente, Len.WPMO_DS_UTENTE);
    }

    public String getWpmoDsUtente() {
        return this.wpmoDsUtente;
    }

    public void setWpmoDsStatoElab(char wpmoDsStatoElab) {
        this.wpmoDsStatoElab = wpmoDsStatoElab;
    }

    public char getWpmoDsStatoElab() {
        return this.wpmoDsStatoElab;
    }

    public void setWpmoTpEstrCnt(String wpmoTpEstrCnt) {
        this.wpmoTpEstrCnt = Functions.subString(wpmoTpEstrCnt, Len.WPMO_TP_ESTR_CNT);
    }

    public String getWpmoTpEstrCnt() {
        return this.wpmoTpEstrCnt;
    }

    public String getWpmoTpEstrCntFormatted() {
        return Functions.padBlanks(getWpmoTpEstrCnt(), Len.WPMO_TP_ESTR_CNT);
    }

    public void setWpmoCodRamo(String wpmoCodRamo) {
        this.wpmoCodRamo = Functions.subString(wpmoCodRamo, Len.WPMO_COD_RAMO);
    }

    public String getWpmoCodRamo() {
        return this.wpmoCodRamo;
    }

    public String getWpmoCodRamoFormatted() {
        return Functions.padBlanks(getWpmoCodRamo(), Len.WPMO_COD_RAMO);
    }

    public void setWpmoGenDaSin(char wpmoGenDaSin) {
        this.wpmoGenDaSin = wpmoGenDaSin;
    }

    public char getWpmoGenDaSin() {
        return this.wpmoGenDaSin;
    }

    public void setWpmoCodTari(String wpmoCodTari) {
        this.wpmoCodTari = Functions.subString(wpmoCodTari, Len.WPMO_COD_TARI);
    }

    public String getWpmoCodTari() {
        return this.wpmoCodTari;
    }

    public String getWpmoCodTariFormatted() {
        return Functions.padBlanks(getWpmoCodTari(), Len.WPMO_COD_TARI);
    }

    public WpmoAaRenCer getWpmoAaRenCer() {
        return wpmoAaRenCer;
    }

    public WpmoCosOner getWpmoCosOner() {
        return wpmoCosOner;
    }

    public WpmoDtRicorPrec getWpmoDtRicorPrec() {
        return wpmoDtRicorPrec;
    }

    public WpmoDtRicorSucc getWpmoDtRicorSucc() {
        return wpmoDtRicorSucc;
    }

    public WpmoDtUltErogManfee getWpmoDtUltErogManfee() {
        return wpmoDtUltErogManfee;
    }

    public WpmoDurAa getWpmoDurAa() {
        return wpmoDurAa;
    }

    public WpmoDurGg getWpmoDurGg() {
        return wpmoDurGg;
    }

    public WpmoDurMm getWpmoDurMm() {
        return wpmoDurMm;
    }

    public WpmoEtaAaSoglBnficr getWpmoEtaAaSoglBnficr() {
        return wpmoEtaAaSoglBnficr;
    }

    public WpmoFrqMovi getWpmoFrqMovi() {
        return wpmoFrqMovi;
    }

    public WpmoIdAdes getWpmoIdAdes() {
        return wpmoIdAdes;
    }

    public WpmoIdMoviChiu getWpmoIdMoviChiu() {
        return wpmoIdMoviChiu;
    }

    public WpmoImpBnsDaSco getWpmoImpBnsDaSco() {
        return wpmoImpBnsDaSco;
    }

    public WpmoImpBnsDaScoTot getWpmoImpBnsDaScoTot() {
        return wpmoImpBnsDaScoTot;
    }

    public WpmoImpLrdDiRat getWpmoImpLrdDiRat() {
        return wpmoImpLrdDiRat;
    }

    public WpmoImpRatManfee getWpmoImpRatManfee() {
        return wpmoImpRatManfee;
    }

    public WpmoImpRiscParzPrgt getWpmoImpRiscParzPrgt() {
        return wpmoImpRiscParzPrgt;
    }

    public WpmoMmDiff getWpmoMmDiff() {
        return wpmoMmDiff;
    }

    public WpmoNumRatPagPre getWpmoNumRatPagPre() {
        return wpmoNumRatPagPre;
    }

    public WpmoPcAnticBns getWpmoPcAnticBns() {
        return wpmoPcAnticBns;
    }

    public WpmoPcApplzOpz getWpmoPcApplzOpz() {
        return wpmoPcApplzOpz;
    }

    public WpmoPcIntrFraz getWpmoPcIntrFraz() {
        return wpmoPcIntrFraz;
    }

    public WpmoPcRevrsb getWpmoPcRevrsb() {
        return wpmoPcRevrsb;
    }

    public WpmoPcServVal getWpmoPcServVal() {
        return wpmoPcServVal;
    }

    public WpmoSomAsstaGarac getWpmoSomAsstaGarac() {
        return wpmoSomAsstaGarac;
    }

    public WpmoSpePc getWpmoSpePc() {
        return wpmoSpePc;
    }

    public WpmoTotAaGiaPror getWpmoTotAaGiaPror() {
        return wpmoTotAaGiaPror;
    }

    public WpmoTpMovi getWpmoTpMovi() {
        return wpmoTpMovi;
    }

    public WpmoUltPcPerd getWpmoUltPcPerd() {
        return wpmoUltPcPerd;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_ID_PARAM_MOVI = 5;
        public static final int WPMO_ID_OGG = 5;
        public static final int WPMO_TP_OGG = 2;
        public static final int WPMO_ID_MOVI_CRZ = 5;
        public static final int WPMO_DT_INI_EFF = 5;
        public static final int WPMO_DT_END_EFF = 5;
        public static final int WPMO_COD_COMP_ANIA = 3;
        public static final int WPMO_TP_RINN_COLL = 2;
        public static final int WPMO_TP_RIVAL_PRE = 2;
        public static final int WPMO_TP_RIVAL_PRSTZ = 2;
        public static final int WPMO_FL_EVID_RIVAL = 1;
        public static final int WPMO_TP_OPZ = 12;
        public static final int WPMO_IB_OGG = 40;
        public static final int WPMO_FL_ATTIV_GAR = 1;
        public static final int WPMO_CAMBIO_VER_PROD = 1;
        public static final int WPMO_TP_OGG_RIVAL = 2;
        public static final int WPMO_ID_POLI = 5;
        public static final int WPMO_TP_FRM_ASSVA = 2;
        public static final int WPMO_DS_RIGA = 6;
        public static final int WPMO_DS_OPER_SQL = 1;
        public static final int WPMO_DS_VER = 5;
        public static final int WPMO_DS_TS_INI_CPTZ = 10;
        public static final int WPMO_DS_TS_END_CPTZ = 10;
        public static final int WPMO_DS_UTENTE = 20;
        public static final int WPMO_DS_STATO_ELAB = 1;
        public static final int WPMO_TP_ESTR_CNT = 2;
        public static final int WPMO_COD_RAMO = 12;
        public static final int WPMO_GEN_DA_SIN = 1;
        public static final int WPMO_COD_TARI = 12;
        public static final int DATI = WPMO_ID_PARAM_MOVI + WPMO_ID_OGG + WPMO_TP_OGG + WPMO_ID_MOVI_CRZ + WpmoIdMoviChiu.Len.WPMO_ID_MOVI_CHIU + WPMO_DT_INI_EFF + WPMO_DT_END_EFF + WPMO_COD_COMP_ANIA + WpmoTpMovi.Len.WPMO_TP_MOVI + WpmoFrqMovi.Len.WPMO_FRQ_MOVI + WpmoDurAa.Len.WPMO_DUR_AA + WpmoDurMm.Len.WPMO_DUR_MM + WpmoDurGg.Len.WPMO_DUR_GG + WpmoDtRicorPrec.Len.WPMO_DT_RICOR_PREC + WpmoDtRicorSucc.Len.WPMO_DT_RICOR_SUCC + WpmoPcIntrFraz.Len.WPMO_PC_INTR_FRAZ + WpmoImpBnsDaScoTot.Len.WPMO_IMP_BNS_DA_SCO_TOT + WpmoImpBnsDaSco.Len.WPMO_IMP_BNS_DA_SCO + WpmoPcAnticBns.Len.WPMO_PC_ANTIC_BNS + WPMO_TP_RINN_COLL + WPMO_TP_RIVAL_PRE + WPMO_TP_RIVAL_PRSTZ + WPMO_FL_EVID_RIVAL + WpmoUltPcPerd.Len.WPMO_ULT_PC_PERD + WpmoTotAaGiaPror.Len.WPMO_TOT_AA_GIA_PROR + WPMO_TP_OPZ + WpmoAaRenCer.Len.WPMO_AA_REN_CER + WpmoPcRevrsb.Len.WPMO_PC_REVRSB + WpmoImpRiscParzPrgt.Len.WPMO_IMP_RISC_PARZ_PRGT + WpmoImpLrdDiRat.Len.WPMO_IMP_LRD_DI_RAT + WPMO_IB_OGG + WpmoCosOner.Len.WPMO_COS_ONER + WpmoSpePc.Len.WPMO_SPE_PC + WPMO_FL_ATTIV_GAR + WPMO_CAMBIO_VER_PROD + WpmoMmDiff.Len.WPMO_MM_DIFF + WpmoImpRatManfee.Len.WPMO_IMP_RAT_MANFEE + WpmoDtUltErogManfee.Len.WPMO_DT_ULT_EROG_MANFEE + WPMO_TP_OGG_RIVAL + WpmoSomAsstaGarac.Len.WPMO_SOM_ASSTA_GARAC + WpmoPcApplzOpz.Len.WPMO_PC_APPLZ_OPZ + WpmoIdAdes.Len.WPMO_ID_ADES + WPMO_ID_POLI + WPMO_TP_FRM_ASSVA + WPMO_DS_RIGA + WPMO_DS_OPER_SQL + WPMO_DS_VER + WPMO_DS_TS_INI_CPTZ + WPMO_DS_TS_END_CPTZ + WPMO_DS_UTENTE + WPMO_DS_STATO_ELAB + WPMO_TP_ESTR_CNT + WPMO_COD_RAMO + WPMO_GEN_DA_SIN + WPMO_COD_TARI + WpmoNumRatPagPre.Len.WPMO_NUM_RAT_PAG_PRE + WpmoPcServVal.Len.WPMO_PC_SERV_VAL + WpmoEtaAaSoglBnficr.Len.WPMO_ETA_AA_SOGL_BNFICR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_ID_PARAM_MOVI = 9;
            public static final int WPMO_ID_OGG = 9;
            public static final int WPMO_ID_MOVI_CRZ = 9;
            public static final int WPMO_DT_INI_EFF = 8;
            public static final int WPMO_DT_END_EFF = 8;
            public static final int WPMO_COD_COMP_ANIA = 5;
            public static final int WPMO_ID_POLI = 9;
            public static final int WPMO_DS_RIGA = 10;
            public static final int WPMO_DS_VER = 9;
            public static final int WPMO_DS_TS_INI_CPTZ = 18;
            public static final int WPMO_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
