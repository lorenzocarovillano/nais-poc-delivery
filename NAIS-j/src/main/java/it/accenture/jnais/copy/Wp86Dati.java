package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP86-DATI<br>
 * Variable: WP86-DATI from copybook LCCVP861<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wp86Dati {

    //==== PROPERTIES ====
    //Original name: WP86-ID-MOT-LIQ
    private int idMotLiq = DefaultValues.INT_VAL;
    //Original name: WP86-ID-LIQ
    private int idLiq = DefaultValues.INT_VAL;
    //Original name: WP86-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: WP86-ID-MOVI-CHIU
    private int idMoviChiu = DefaultValues.INT_VAL;
    //Original name: WP86-DT-INI-EFF
    private int dtIniEff = DefaultValues.INT_VAL;
    //Original name: WP86-DT-END-EFF
    private int dtEndEff = DefaultValues.INT_VAL;
    //Original name: WP86-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: WP86-TP-LIQ
    private String tpLiq = DefaultValues.stringVal(Len.TP_LIQ);
    //Original name: WP86-TP-MOT-LIQ
    private String tpMotLiq = DefaultValues.stringVal(Len.TP_MOT_LIQ);
    //Original name: WP86-DS-RIGA
    private long dsRiga = DefaultValues.LONG_VAL;
    //Original name: WP86-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WP86-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: WP86-DS-TS-INI-CPTZ
    private long dsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WP86-DS-TS-END-CPTZ
    private long dsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WP86-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: WP86-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WP86-DESC-LIB-MOT-LIQ
    private String descLibMotLiq = DefaultValues.stringVal(Len.DESC_LIB_MOT_LIQ);

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        idMotLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOT_LIQ, 0);
        position += Len.ID_MOT_LIQ;
        idLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_LIQ, 0);
        position += Len.ID_LIQ;
        idMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        idMoviChiu = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        dtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        dtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        tpLiq = MarshalByte.readString(buffer, position, Len.TP_LIQ);
        position += Len.TP_LIQ;
        tpMotLiq = MarshalByte.readString(buffer, position, Len.TP_MOT_LIQ);
        position += Len.TP_MOT_LIQ;
        dsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        dsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        descLibMotLiq = MarshalByte.readString(buffer, position, Len.DESC_LIB_MOT_LIQ);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idMotLiq, Len.Int.ID_MOT_LIQ, 0);
        position += Len.ID_MOT_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, idLiq, Len.Int.ID_LIQ, 0);
        position += Len.ID_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviCrz, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviChiu, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniEff, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtEndEff, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, tpLiq, Len.TP_LIQ);
        position += Len.TP_LIQ;
        MarshalByte.writeString(buffer, position, tpMotLiq, Len.TP_MOT_LIQ);
        position += Len.TP_MOT_LIQ;
        MarshalByte.writeLongAsPacked(buffer, position, dsRiga, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, descLibMotLiq, Len.DESC_LIB_MOT_LIQ);
        return buffer;
    }

    public void initDatiSpaces() {
        idMotLiq = Types.INVALID_INT_VAL;
        idLiq = Types.INVALID_INT_VAL;
        idMoviCrz = Types.INVALID_INT_VAL;
        idMoviChiu = Types.INVALID_INT_VAL;
        dtIniEff = Types.INVALID_INT_VAL;
        dtEndEff = Types.INVALID_INT_VAL;
        codCompAnia = Types.INVALID_INT_VAL;
        tpLiq = "";
        tpMotLiq = "";
        dsRiga = Types.INVALID_LONG_VAL;
        dsOperSql = Types.SPACE_CHAR;
        dsVer = Types.INVALID_INT_VAL;
        dsTsIniCptz = Types.INVALID_LONG_VAL;
        dsTsEndCptz = Types.INVALID_LONG_VAL;
        dsUtente = "";
        dsStatoElab = Types.SPACE_CHAR;
        descLibMotLiq = "";
    }

    public void setIdMotLiq(int idMotLiq) {
        this.idMotLiq = idMotLiq;
    }

    public int getIdMotLiq() {
        return this.idMotLiq;
    }

    public void setIdLiq(int idLiq) {
        this.idLiq = idLiq;
    }

    public int getIdLiq() {
        return this.idLiq;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setIdMoviChiu(int idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public int getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtIniEff(int dtIniEff) {
        this.dtIniEff = dtIniEff;
    }

    public int getDtIniEff() {
        return this.dtIniEff;
    }

    public void setDtEndEff(int dtEndEff) {
        this.dtEndEff = dtEndEff;
    }

    public int getDtEndEff() {
        return this.dtEndEff;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setTpLiq(String tpLiq) {
        this.tpLiq = Functions.subString(tpLiq, Len.TP_LIQ);
    }

    public String getTpLiq() {
        return this.tpLiq;
    }

    public void setTpMotLiq(String tpMotLiq) {
        this.tpMotLiq = Functions.subString(tpMotLiq, Len.TP_MOT_LIQ);
    }

    public String getTpMotLiq() {
        return this.tpMotLiq;
    }

    public void setDsRiga(long dsRiga) {
        this.dsRiga = dsRiga;
    }

    public long getDsRiga() {
        return this.dsRiga;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dsTsIniCptz = dsTsIniCptz;
    }

    public long getDsTsIniCptz() {
        return this.dsTsIniCptz;
    }

    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dsTsEndCptz = dsTsEndCptz;
    }

    public long getDsTsEndCptz() {
        return this.dsTsEndCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setDescLibMotLiq(String descLibMotLiq) {
        this.descLibMotLiq = Functions.subString(descLibMotLiq, Len.DESC_LIB_MOT_LIQ);
    }

    public String getDescLibMotLiq() {
        return this.descLibMotLiq;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_MOT_LIQ = 5;
        public static final int ID_LIQ = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int TP_LIQ = 2;
        public static final int TP_MOT_LIQ = 2;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int DESC_LIB_MOT_LIQ = 250;
        public static final int DATI = ID_MOT_LIQ + ID_LIQ + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + TP_LIQ + TP_MOT_LIQ + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + DESC_LIB_MOT_LIQ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_MOT_LIQ = 9;
            public static final int ID_LIQ = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
