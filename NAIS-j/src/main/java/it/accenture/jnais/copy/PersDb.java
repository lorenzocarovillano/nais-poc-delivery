package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: PERS-DB<br>
 * Variable: PERS-DB from copybook IDBVA253<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class PersDb {

    //==== PROPERTIES ====
    //Original name: A25-DT-NASC-CLI-DB
    private String nascCliDb = DefaultValues.stringVal(Len.NASC_CLI_DB);
    //Original name: A25-DT-ACQS-PERS-DB
    private String acqsPersDb = DefaultValues.stringVal(Len.ACQS_PERS_DB);
    //Original name: A25-DT-END-VLDT-PERS-DB
    private String endVldtPersDb = DefaultValues.stringVal(Len.END_VLDT_PERS_DB);
    //Original name: A25-DT-DEAD-PERS-DB
    private String deadPersDb = DefaultValues.stringVal(Len.DEAD_PERS_DB);
    //Original name: A25-DT-BLOC-CLI-DB
    private String blocCliDb = DefaultValues.stringVal(Len.BLOC_CLI_DB);
    //Original name: A25-DT-1A-ATVT-DB
    private String dt1aAtvtDb = DefaultValues.stringVal(Len.DT1A_ATVT_DB);
    //Original name: A25-DT-SEGNAL-PARTNER-DB
    private String segnalPartnerDb = DefaultValues.stringVal(Len.SEGNAL_PARTNER_DB);

    //==== METHODS ====
    public void setNascCliDb(String nascCliDb) {
        this.nascCliDb = Functions.subString(nascCliDb, Len.NASC_CLI_DB);
    }

    public String getNascCliDb() {
        return this.nascCliDb;
    }

    public void setAcqsPersDb(String acqsPersDb) {
        this.acqsPersDb = Functions.subString(acqsPersDb, Len.ACQS_PERS_DB);
    }

    public String getAcqsPersDb() {
        return this.acqsPersDb;
    }

    public void setEndVldtPersDb(String endVldtPersDb) {
        this.endVldtPersDb = Functions.subString(endVldtPersDb, Len.END_VLDT_PERS_DB);
    }

    public String getEndVldtPersDb() {
        return this.endVldtPersDb;
    }

    public void setDeadPersDb(String deadPersDb) {
        this.deadPersDb = Functions.subString(deadPersDb, Len.DEAD_PERS_DB);
    }

    public String getDeadPersDb() {
        return this.deadPersDb;
    }

    public void setBlocCliDb(String blocCliDb) {
        this.blocCliDb = Functions.subString(blocCliDb, Len.BLOC_CLI_DB);
    }

    public String getBlocCliDb() {
        return this.blocCliDb;
    }

    public void setDt1aAtvtDb(String dt1aAtvtDb) {
        this.dt1aAtvtDb = Functions.subString(dt1aAtvtDb, Len.DT1A_ATVT_DB);
    }

    public String getDt1aAtvtDb() {
        return this.dt1aAtvtDb;
    }

    public void setSegnalPartnerDb(String segnalPartnerDb) {
        this.segnalPartnerDb = Functions.subString(segnalPartnerDb, Len.SEGNAL_PARTNER_DB);
    }

    public String getSegnalPartnerDb() {
        return this.segnalPartnerDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NASC_CLI_DB = 10;
        public static final int ACQS_PERS_DB = 10;
        public static final int END_VLDT_PERS_DB = 10;
        public static final int DEAD_PERS_DB = 10;
        public static final int BLOC_CLI_DB = 10;
        public static final int DT1A_ATVT_DB = 10;
        public static final int SEGNAL_PARTNER_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
