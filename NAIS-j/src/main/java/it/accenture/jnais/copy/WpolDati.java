package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpolAaDiffProrDflt;
import it.accenture.jnais.ws.redefines.WpolDir1oVers;
import it.accenture.jnais.ws.redefines.WpolDirEmis;
import it.accenture.jnais.ws.redefines.WpolDirQuiet;
import it.accenture.jnais.ws.redefines.WpolDirVersAgg;
import it.accenture.jnais.ws.redefines.WpolDtApplzConv;
import it.accenture.jnais.ws.redefines.WpolDtIniVldtConv;
import it.accenture.jnais.ws.redefines.WpolDtPresc;
import it.accenture.jnais.ws.redefines.WpolDtProp;
import it.accenture.jnais.ws.redefines.WpolDtScad;
import it.accenture.jnais.ws.redefines.WpolDurAa;
import it.accenture.jnais.ws.redefines.WpolDurGg;
import it.accenture.jnais.ws.redefines.WpolDurMm;
import it.accenture.jnais.ws.redefines.WpolIdAccComm;
import it.accenture.jnais.ws.redefines.WpolIdMoviChiu;
import it.accenture.jnais.ws.redefines.WpolSpeMed;

/**Original name: WPOL-DATI<br>
 * Variable: WPOL-DATI from copybook LCCVPOL1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WpolDati {

    //==== PROPERTIES ====
    //Original name: WPOL-ID-POLI
    private int wpolIdPoli = DefaultValues.INT_VAL;
    //Original name: WPOL-ID-MOVI-CRZ
    private int wpolIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WPOL-ID-MOVI-CHIU
    private WpolIdMoviChiu wpolIdMoviChiu = new WpolIdMoviChiu();
    //Original name: WPOL-IB-OGG
    private String wpolIbOgg = DefaultValues.stringVal(Len.WPOL_IB_OGG);
    //Original name: WPOL-IB-PROP
    private String wpolIbProp = DefaultValues.stringVal(Len.WPOL_IB_PROP);
    //Original name: WPOL-DT-PROP
    private WpolDtProp wpolDtProp = new WpolDtProp();
    //Original name: WPOL-DT-INI-EFF
    private int wpolDtIniEff = DefaultValues.INT_VAL;
    //Original name: WPOL-DT-END-EFF
    private int wpolDtEndEff = DefaultValues.INT_VAL;
    //Original name: WPOL-COD-COMP-ANIA
    private int wpolCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WPOL-DT-DECOR
    private int wpolDtDecor = DefaultValues.INT_VAL;
    //Original name: WPOL-DT-EMIS
    private int wpolDtEmis = DefaultValues.INT_VAL;
    //Original name: WPOL-TP-POLI
    private String wpolTpPoli = DefaultValues.stringVal(Len.WPOL_TP_POLI);
    //Original name: WPOL-DUR-AA
    private WpolDurAa wpolDurAa = new WpolDurAa();
    //Original name: WPOL-DUR-MM
    private WpolDurMm wpolDurMm = new WpolDurMm();
    //Original name: WPOL-DT-SCAD
    private WpolDtScad wpolDtScad = new WpolDtScad();
    //Original name: WPOL-COD-PROD
    private String wpolCodProd = DefaultValues.stringVal(Len.WPOL_COD_PROD);
    //Original name: WPOL-DT-INI-VLDT-PROD
    private int wpolDtIniVldtProd = DefaultValues.INT_VAL;
    //Original name: WPOL-COD-CONV
    private String wpolCodConv = DefaultValues.stringVal(Len.WPOL_COD_CONV);
    //Original name: WPOL-COD-RAMO
    private String wpolCodRamo = DefaultValues.stringVal(Len.WPOL_COD_RAMO);
    //Original name: WPOL-DT-INI-VLDT-CONV
    private WpolDtIniVldtConv wpolDtIniVldtConv = new WpolDtIniVldtConv();
    //Original name: WPOL-DT-APPLZ-CONV
    private WpolDtApplzConv wpolDtApplzConv = new WpolDtApplzConv();
    //Original name: WPOL-TP-FRM-ASSVA
    private String wpolTpFrmAssva = DefaultValues.stringVal(Len.WPOL_TP_FRM_ASSVA);
    //Original name: WPOL-TP-RGM-FISC
    private String wpolTpRgmFisc = DefaultValues.stringVal(Len.WPOL_TP_RGM_FISC);
    //Original name: WPOL-FL-ESTAS
    private char wpolFlEstas = DefaultValues.CHAR_VAL;
    //Original name: WPOL-FL-RSH-COMUN
    private char wpolFlRshComun = DefaultValues.CHAR_VAL;
    //Original name: WPOL-FL-RSH-COMUN-COND
    private char wpolFlRshComunCond = DefaultValues.CHAR_VAL;
    //Original name: WPOL-TP-LIV-GENZ-TIT
    private String wpolTpLivGenzTit = DefaultValues.stringVal(Len.WPOL_TP_LIV_GENZ_TIT);
    //Original name: WPOL-FL-COP-FINANZ
    private char wpolFlCopFinanz = DefaultValues.CHAR_VAL;
    //Original name: WPOL-TP-APPLZ-DIR
    private String wpolTpApplzDir = DefaultValues.stringVal(Len.WPOL_TP_APPLZ_DIR);
    //Original name: WPOL-SPE-MED
    private WpolSpeMed wpolSpeMed = new WpolSpeMed();
    //Original name: WPOL-DIR-EMIS
    private WpolDirEmis wpolDirEmis = new WpolDirEmis();
    //Original name: WPOL-DIR-1O-VERS
    private WpolDir1oVers wpolDir1oVers = new WpolDir1oVers();
    //Original name: WPOL-DIR-VERS-AGG
    private WpolDirVersAgg wpolDirVersAgg = new WpolDirVersAgg();
    //Original name: WPOL-COD-DVS
    private String wpolCodDvs = DefaultValues.stringVal(Len.WPOL_COD_DVS);
    //Original name: WPOL-FL-FNT-AZ
    private char wpolFlFntAz = DefaultValues.CHAR_VAL;
    //Original name: WPOL-FL-FNT-ADER
    private char wpolFlFntAder = DefaultValues.CHAR_VAL;
    //Original name: WPOL-FL-FNT-TFR
    private char wpolFlFntTfr = DefaultValues.CHAR_VAL;
    //Original name: WPOL-FL-FNT-VOLO
    private char wpolFlFntVolo = DefaultValues.CHAR_VAL;
    //Original name: WPOL-TP-OPZ-A-SCAD
    private String wpolTpOpzAScad = DefaultValues.stringVal(Len.WPOL_TP_OPZ_A_SCAD);
    //Original name: WPOL-AA-DIFF-PROR-DFLT
    private WpolAaDiffProrDflt wpolAaDiffProrDflt = new WpolAaDiffProrDflt();
    //Original name: WPOL-FL-VER-PROD
    private String wpolFlVerProd = DefaultValues.stringVal(Len.WPOL_FL_VER_PROD);
    //Original name: WPOL-DUR-GG
    private WpolDurGg wpolDurGg = new WpolDurGg();
    //Original name: WPOL-DIR-QUIET
    private WpolDirQuiet wpolDirQuiet = new WpolDirQuiet();
    //Original name: WPOL-TP-PTF-ESTNO
    private String wpolTpPtfEstno = DefaultValues.stringVal(Len.WPOL_TP_PTF_ESTNO);
    //Original name: WPOL-FL-CUM-PRE-CNTR
    private char wpolFlCumPreCntr = DefaultValues.CHAR_VAL;
    //Original name: WPOL-FL-AMMB-MOVI
    private char wpolFlAmmbMovi = DefaultValues.CHAR_VAL;
    //Original name: WPOL-CONV-GECO
    private String wpolConvGeco = DefaultValues.stringVal(Len.WPOL_CONV_GECO);
    //Original name: WPOL-DS-RIGA
    private long wpolDsRiga = DefaultValues.LONG_VAL;
    //Original name: WPOL-DS-OPER-SQL
    private char wpolDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WPOL-DS-VER
    private int wpolDsVer = DefaultValues.INT_VAL;
    //Original name: WPOL-DS-TS-INI-CPTZ
    private long wpolDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WPOL-DS-TS-END-CPTZ
    private long wpolDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WPOL-DS-UTENTE
    private String wpolDsUtente = DefaultValues.stringVal(Len.WPOL_DS_UTENTE);
    //Original name: WPOL-DS-STATO-ELAB
    private char wpolDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WPOL-FL-SCUDO-FISC
    private char wpolFlScudoFisc = DefaultValues.CHAR_VAL;
    //Original name: WPOL-FL-TRASFE
    private char wpolFlTrasfe = DefaultValues.CHAR_VAL;
    //Original name: WPOL-FL-TFR-STRC
    private char wpolFlTfrStrc = DefaultValues.CHAR_VAL;
    //Original name: WPOL-DT-PRESC
    private WpolDtPresc wpolDtPresc = new WpolDtPresc();
    //Original name: WPOL-COD-CONV-AGG
    private String wpolCodConvAgg = DefaultValues.stringVal(Len.WPOL_COD_CONV_AGG);
    //Original name: WPOL-SUBCAT-PROD
    private String wpolSubcatProd = DefaultValues.stringVal(Len.WPOL_SUBCAT_PROD);
    //Original name: WPOL-FL-QUEST-ADEGZ-ASS
    private char wpolFlQuestAdegzAss = DefaultValues.CHAR_VAL;
    //Original name: WPOL-COD-TPA
    private String wpolCodTpa = DefaultValues.stringVal(Len.WPOL_COD_TPA);
    //Original name: WPOL-ID-ACC-COMM
    private WpolIdAccComm wpolIdAccComm = new WpolIdAccComm();
    //Original name: WPOL-FL-POLI-CPI-PR
    private char wpolFlPoliCpiPr = DefaultValues.CHAR_VAL;
    //Original name: WPOL-FL-POLI-BUNDLING
    private char wpolFlPoliBundling = DefaultValues.CHAR_VAL;
    //Original name: WPOL-IND-POLI-PRIN-COLL
    private char wpolIndPoliPrinColl = DefaultValues.CHAR_VAL;
    //Original name: WPOL-FL-VND-BUNDLE
    private char wpolFlVndBundle = DefaultValues.CHAR_VAL;
    //Original name: WPOL-IB-BS
    private String wpolIbBs = DefaultValues.stringVal(Len.WPOL_IB_BS);
    //Original name: WPOL-FL-POLI-IFP
    private char wpolFlPoliIfp = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wpolIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOL_ID_POLI, 0);
        position += Len.WPOL_ID_POLI;
        wpolIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOL_ID_MOVI_CRZ, 0);
        position += Len.WPOL_ID_MOVI_CRZ;
        wpolIdMoviChiu.setWpolIdMoviChiuFromBuffer(buffer, position);
        position += WpolIdMoviChiu.Len.WPOL_ID_MOVI_CHIU;
        wpolIbOgg = MarshalByte.readString(buffer, position, Len.WPOL_IB_OGG);
        position += Len.WPOL_IB_OGG;
        wpolIbProp = MarshalByte.readString(buffer, position, Len.WPOL_IB_PROP);
        position += Len.WPOL_IB_PROP;
        wpolDtProp.setWpolDtPropFromBuffer(buffer, position);
        position += WpolDtProp.Len.WPOL_DT_PROP;
        wpolDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOL_DT_INI_EFF, 0);
        position += Len.WPOL_DT_INI_EFF;
        wpolDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOL_DT_END_EFF, 0);
        position += Len.WPOL_DT_END_EFF;
        wpolCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOL_COD_COMP_ANIA, 0);
        position += Len.WPOL_COD_COMP_ANIA;
        wpolDtDecor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOL_DT_DECOR, 0);
        position += Len.WPOL_DT_DECOR;
        wpolDtEmis = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOL_DT_EMIS, 0);
        position += Len.WPOL_DT_EMIS;
        wpolTpPoli = MarshalByte.readString(buffer, position, Len.WPOL_TP_POLI);
        position += Len.WPOL_TP_POLI;
        wpolDurAa.setWpolDurAaFromBuffer(buffer, position);
        position += WpolDurAa.Len.WPOL_DUR_AA;
        wpolDurMm.setWpolDurMmFromBuffer(buffer, position);
        position += WpolDurMm.Len.WPOL_DUR_MM;
        wpolDtScad.setWpolDtScadFromBuffer(buffer, position);
        position += WpolDtScad.Len.WPOL_DT_SCAD;
        wpolCodProd = MarshalByte.readString(buffer, position, Len.WPOL_COD_PROD);
        position += Len.WPOL_COD_PROD;
        wpolDtIniVldtProd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOL_DT_INI_VLDT_PROD, 0);
        position += Len.WPOL_DT_INI_VLDT_PROD;
        wpolCodConv = MarshalByte.readString(buffer, position, Len.WPOL_COD_CONV);
        position += Len.WPOL_COD_CONV;
        wpolCodRamo = MarshalByte.readString(buffer, position, Len.WPOL_COD_RAMO);
        position += Len.WPOL_COD_RAMO;
        wpolDtIniVldtConv.setWpolDtIniVldtConvFromBuffer(buffer, position);
        position += WpolDtIniVldtConv.Len.WPOL_DT_INI_VLDT_CONV;
        wpolDtApplzConv.setWpolDtApplzConvFromBuffer(buffer, position);
        position += WpolDtApplzConv.Len.WPOL_DT_APPLZ_CONV;
        wpolTpFrmAssva = MarshalByte.readString(buffer, position, Len.WPOL_TP_FRM_ASSVA);
        position += Len.WPOL_TP_FRM_ASSVA;
        wpolTpRgmFisc = MarshalByte.readString(buffer, position, Len.WPOL_TP_RGM_FISC);
        position += Len.WPOL_TP_RGM_FISC;
        wpolFlEstas = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolFlRshComun = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolFlRshComunCond = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolTpLivGenzTit = MarshalByte.readString(buffer, position, Len.WPOL_TP_LIV_GENZ_TIT);
        position += Len.WPOL_TP_LIV_GENZ_TIT;
        wpolFlCopFinanz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolTpApplzDir = MarshalByte.readString(buffer, position, Len.WPOL_TP_APPLZ_DIR);
        position += Len.WPOL_TP_APPLZ_DIR;
        wpolSpeMed.setWpolSpeMedFromBuffer(buffer, position);
        position += WpolSpeMed.Len.WPOL_SPE_MED;
        wpolDirEmis.setWpolDirEmisFromBuffer(buffer, position);
        position += WpolDirEmis.Len.WPOL_DIR_EMIS;
        wpolDir1oVers.setWpolDir1oVersFromBuffer(buffer, position);
        position += WpolDir1oVers.Len.WPOL_DIR1O_VERS;
        wpolDirVersAgg.setWpolDirVersAggFromBuffer(buffer, position);
        position += WpolDirVersAgg.Len.WPOL_DIR_VERS_AGG;
        wpolCodDvs = MarshalByte.readString(buffer, position, Len.WPOL_COD_DVS);
        position += Len.WPOL_COD_DVS;
        wpolFlFntAz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolFlFntAder = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolFlFntTfr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolFlFntVolo = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolTpOpzAScad = MarshalByte.readString(buffer, position, Len.WPOL_TP_OPZ_A_SCAD);
        position += Len.WPOL_TP_OPZ_A_SCAD;
        wpolAaDiffProrDflt.setWpolAaDiffProrDfltFromBuffer(buffer, position);
        position += WpolAaDiffProrDflt.Len.WPOL_AA_DIFF_PROR_DFLT;
        wpolFlVerProd = MarshalByte.readString(buffer, position, Len.WPOL_FL_VER_PROD);
        position += Len.WPOL_FL_VER_PROD;
        wpolDurGg.setWpolDurGgFromBuffer(buffer, position);
        position += WpolDurGg.Len.WPOL_DUR_GG;
        wpolDirQuiet.setWpolDirQuietFromBuffer(buffer, position);
        position += WpolDirQuiet.Len.WPOL_DIR_QUIET;
        wpolTpPtfEstno = MarshalByte.readString(buffer, position, Len.WPOL_TP_PTF_ESTNO);
        position += Len.WPOL_TP_PTF_ESTNO;
        wpolFlCumPreCntr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolFlAmmbMovi = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolConvGeco = MarshalByte.readString(buffer, position, Len.WPOL_CONV_GECO);
        position += Len.WPOL_CONV_GECO;
        wpolDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPOL_DS_RIGA, 0);
        position += Len.WPOL_DS_RIGA;
        wpolDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPOL_DS_VER, 0);
        position += Len.WPOL_DS_VER;
        wpolDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPOL_DS_TS_INI_CPTZ, 0);
        position += Len.WPOL_DS_TS_INI_CPTZ;
        wpolDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPOL_DS_TS_END_CPTZ, 0);
        position += Len.WPOL_DS_TS_END_CPTZ;
        wpolDsUtente = MarshalByte.readString(buffer, position, Len.WPOL_DS_UTENTE);
        position += Len.WPOL_DS_UTENTE;
        wpolDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolFlScudoFisc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolFlTrasfe = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolFlTfrStrc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolDtPresc.setWpolDtPrescFromBuffer(buffer, position);
        position += WpolDtPresc.Len.WPOL_DT_PRESC;
        wpolCodConvAgg = MarshalByte.readString(buffer, position, Len.WPOL_COD_CONV_AGG);
        position += Len.WPOL_COD_CONV_AGG;
        wpolSubcatProd = MarshalByte.readString(buffer, position, Len.WPOL_SUBCAT_PROD);
        position += Len.WPOL_SUBCAT_PROD;
        wpolFlQuestAdegzAss = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolCodTpa = MarshalByte.readString(buffer, position, Len.WPOL_COD_TPA);
        position += Len.WPOL_COD_TPA;
        wpolIdAccComm.setWpolIdAccCommFromBuffer(buffer, position);
        position += WpolIdAccComm.Len.WPOL_ID_ACC_COMM;
        wpolFlPoliCpiPr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolFlPoliBundling = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolIndPoliPrinColl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolFlVndBundle = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpolIbBs = MarshalByte.readString(buffer, position, Len.WPOL_IB_BS);
        position += Len.WPOL_IB_BS;
        wpolFlPoliIfp = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wpolIdPoli, Len.Int.WPOL_ID_POLI, 0);
        position += Len.WPOL_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wpolIdMoviCrz, Len.Int.WPOL_ID_MOVI_CRZ, 0);
        position += Len.WPOL_ID_MOVI_CRZ;
        wpolIdMoviChiu.getWpolIdMoviChiuAsBuffer(buffer, position);
        position += WpolIdMoviChiu.Len.WPOL_ID_MOVI_CHIU;
        MarshalByte.writeString(buffer, position, wpolIbOgg, Len.WPOL_IB_OGG);
        position += Len.WPOL_IB_OGG;
        MarshalByte.writeString(buffer, position, wpolIbProp, Len.WPOL_IB_PROP);
        position += Len.WPOL_IB_PROP;
        wpolDtProp.getWpolDtPropAsBuffer(buffer, position);
        position += WpolDtProp.Len.WPOL_DT_PROP;
        MarshalByte.writeIntAsPacked(buffer, position, wpolDtIniEff, Len.Int.WPOL_DT_INI_EFF, 0);
        position += Len.WPOL_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpolDtEndEff, Len.Int.WPOL_DT_END_EFF, 0);
        position += Len.WPOL_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpolCodCompAnia, Len.Int.WPOL_COD_COMP_ANIA, 0);
        position += Len.WPOL_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wpolDtDecor, Len.Int.WPOL_DT_DECOR, 0);
        position += Len.WPOL_DT_DECOR;
        MarshalByte.writeIntAsPacked(buffer, position, wpolDtEmis, Len.Int.WPOL_DT_EMIS, 0);
        position += Len.WPOL_DT_EMIS;
        MarshalByte.writeString(buffer, position, wpolTpPoli, Len.WPOL_TP_POLI);
        position += Len.WPOL_TP_POLI;
        wpolDurAa.getWpolDurAaAsBuffer(buffer, position);
        position += WpolDurAa.Len.WPOL_DUR_AA;
        wpolDurMm.getWpolDurMmAsBuffer(buffer, position);
        position += WpolDurMm.Len.WPOL_DUR_MM;
        wpolDtScad.getWpolDtScadAsBuffer(buffer, position);
        position += WpolDtScad.Len.WPOL_DT_SCAD;
        MarshalByte.writeString(buffer, position, wpolCodProd, Len.WPOL_COD_PROD);
        position += Len.WPOL_COD_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, wpolDtIniVldtProd, Len.Int.WPOL_DT_INI_VLDT_PROD, 0);
        position += Len.WPOL_DT_INI_VLDT_PROD;
        MarshalByte.writeString(buffer, position, wpolCodConv, Len.WPOL_COD_CONV);
        position += Len.WPOL_COD_CONV;
        MarshalByte.writeString(buffer, position, wpolCodRamo, Len.WPOL_COD_RAMO);
        position += Len.WPOL_COD_RAMO;
        wpolDtIniVldtConv.getWpolDtIniVldtConvAsBuffer(buffer, position);
        position += WpolDtIniVldtConv.Len.WPOL_DT_INI_VLDT_CONV;
        wpolDtApplzConv.getWpolDtApplzConvAsBuffer(buffer, position);
        position += WpolDtApplzConv.Len.WPOL_DT_APPLZ_CONV;
        MarshalByte.writeString(buffer, position, wpolTpFrmAssva, Len.WPOL_TP_FRM_ASSVA);
        position += Len.WPOL_TP_FRM_ASSVA;
        MarshalByte.writeString(buffer, position, wpolTpRgmFisc, Len.WPOL_TP_RGM_FISC);
        position += Len.WPOL_TP_RGM_FISC;
        MarshalByte.writeChar(buffer, position, wpolFlEstas);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolFlRshComun);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolFlRshComunCond);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wpolTpLivGenzTit, Len.WPOL_TP_LIV_GENZ_TIT);
        position += Len.WPOL_TP_LIV_GENZ_TIT;
        MarshalByte.writeChar(buffer, position, wpolFlCopFinanz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wpolTpApplzDir, Len.WPOL_TP_APPLZ_DIR);
        position += Len.WPOL_TP_APPLZ_DIR;
        wpolSpeMed.getWpolSpeMedAsBuffer(buffer, position);
        position += WpolSpeMed.Len.WPOL_SPE_MED;
        wpolDirEmis.getWpolDirEmisAsBuffer(buffer, position);
        position += WpolDirEmis.Len.WPOL_DIR_EMIS;
        wpolDir1oVers.getWpolDir1oVersAsBuffer(buffer, position);
        position += WpolDir1oVers.Len.WPOL_DIR1O_VERS;
        wpolDirVersAgg.getWpolDirVersAggAsBuffer(buffer, position);
        position += WpolDirVersAgg.Len.WPOL_DIR_VERS_AGG;
        MarshalByte.writeString(buffer, position, wpolCodDvs, Len.WPOL_COD_DVS);
        position += Len.WPOL_COD_DVS;
        MarshalByte.writeChar(buffer, position, wpolFlFntAz);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolFlFntAder);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolFlFntTfr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolFlFntVolo);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wpolTpOpzAScad, Len.WPOL_TP_OPZ_A_SCAD);
        position += Len.WPOL_TP_OPZ_A_SCAD;
        wpolAaDiffProrDflt.getWpolAaDiffProrDfltAsBuffer(buffer, position);
        position += WpolAaDiffProrDflt.Len.WPOL_AA_DIFF_PROR_DFLT;
        MarshalByte.writeString(buffer, position, wpolFlVerProd, Len.WPOL_FL_VER_PROD);
        position += Len.WPOL_FL_VER_PROD;
        wpolDurGg.getWpolDurGgAsBuffer(buffer, position);
        position += WpolDurGg.Len.WPOL_DUR_GG;
        wpolDirQuiet.getWpolDirQuietAsBuffer(buffer, position);
        position += WpolDirQuiet.Len.WPOL_DIR_QUIET;
        MarshalByte.writeString(buffer, position, wpolTpPtfEstno, Len.WPOL_TP_PTF_ESTNO);
        position += Len.WPOL_TP_PTF_ESTNO;
        MarshalByte.writeChar(buffer, position, wpolFlCumPreCntr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolFlAmmbMovi);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wpolConvGeco, Len.WPOL_CONV_GECO);
        position += Len.WPOL_CONV_GECO;
        MarshalByte.writeLongAsPacked(buffer, position, wpolDsRiga, Len.Int.WPOL_DS_RIGA, 0);
        position += Len.WPOL_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wpolDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wpolDsVer, Len.Int.WPOL_DS_VER, 0);
        position += Len.WPOL_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wpolDsTsIniCptz, Len.Int.WPOL_DS_TS_INI_CPTZ, 0);
        position += Len.WPOL_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wpolDsTsEndCptz, Len.Int.WPOL_DS_TS_END_CPTZ, 0);
        position += Len.WPOL_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wpolDsUtente, Len.WPOL_DS_UTENTE);
        position += Len.WPOL_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wpolDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolFlScudoFisc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolFlTrasfe);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolFlTfrStrc);
        position += Types.CHAR_SIZE;
        wpolDtPresc.getWpolDtPrescAsBuffer(buffer, position);
        position += WpolDtPresc.Len.WPOL_DT_PRESC;
        MarshalByte.writeString(buffer, position, wpolCodConvAgg, Len.WPOL_COD_CONV_AGG);
        position += Len.WPOL_COD_CONV_AGG;
        MarshalByte.writeString(buffer, position, wpolSubcatProd, Len.WPOL_SUBCAT_PROD);
        position += Len.WPOL_SUBCAT_PROD;
        MarshalByte.writeChar(buffer, position, wpolFlQuestAdegzAss);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wpolCodTpa, Len.WPOL_COD_TPA);
        position += Len.WPOL_COD_TPA;
        wpolIdAccComm.getWpolIdAccCommAsBuffer(buffer, position);
        position += WpolIdAccComm.Len.WPOL_ID_ACC_COMM;
        MarshalByte.writeChar(buffer, position, wpolFlPoliCpiPr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolFlPoliBundling);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolIndPoliPrinColl);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wpolFlVndBundle);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wpolIbBs, Len.WPOL_IB_BS);
        position += Len.WPOL_IB_BS;
        MarshalByte.writeChar(buffer, position, wpolFlPoliIfp);
        return buffer;
    }

    public void setWpolIdPoli(int wpolIdPoli) {
        this.wpolIdPoli = wpolIdPoli;
    }

    public int getWpolIdPoli() {
        return this.wpolIdPoli;
    }

    public void setWpolIdMoviCrz(int wpolIdMoviCrz) {
        this.wpolIdMoviCrz = wpolIdMoviCrz;
    }

    public int getWpolIdMoviCrz() {
        return this.wpolIdMoviCrz;
    }

    public void setWpolIbOgg(String wpolIbOgg) {
        this.wpolIbOgg = Functions.subString(wpolIbOgg, Len.WPOL_IB_OGG);
    }

    public String getWpolIbOgg() {
        return this.wpolIbOgg;
    }

    public String getWpolIbOggFormatted() {
        return Functions.padBlanks(getWpolIbOgg(), Len.WPOL_IB_OGG);
    }

    public void setWpolIbProp(String wpolIbProp) {
        this.wpolIbProp = Functions.subString(wpolIbProp, Len.WPOL_IB_PROP);
    }

    public String getWpolIbProp() {
        return this.wpolIbProp;
    }

    public void setWpolDtIniEff(int wpolDtIniEff) {
        this.wpolDtIniEff = wpolDtIniEff;
    }

    public int getWpolDtIniEff() {
        return this.wpolDtIniEff;
    }

    public void setWpolDtEndEff(int wpolDtEndEff) {
        this.wpolDtEndEff = wpolDtEndEff;
    }

    public int getWpolDtEndEff() {
        return this.wpolDtEndEff;
    }

    public void setWpolCodCompAnia(int wpolCodCompAnia) {
        this.wpolCodCompAnia = wpolCodCompAnia;
    }

    public int getWpolCodCompAnia() {
        return this.wpolCodCompAnia;
    }

    public void setWpolDtDecor(int wpolDtDecor) {
        this.wpolDtDecor = wpolDtDecor;
    }

    public int getWpolDtDecor() {
        return this.wpolDtDecor;
    }

    public String getWpolDtDecorFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getWpolDtDecor()).toString();
    }

    public void setWpolDtEmis(int wpolDtEmis) {
        this.wpolDtEmis = wpolDtEmis;
    }

    public int getWpolDtEmis() {
        return this.wpolDtEmis;
    }

    public void setWpolTpPoli(String wpolTpPoli) {
        this.wpolTpPoli = Functions.subString(wpolTpPoli, Len.WPOL_TP_POLI);
    }

    public String getWpolTpPoli() {
        return this.wpolTpPoli;
    }

    public void setWpolCodProd(String wpolCodProd) {
        this.wpolCodProd = Functions.subString(wpolCodProd, Len.WPOL_COD_PROD);
    }

    public String getWpolCodProd() {
        return this.wpolCodProd;
    }

    public void setWpolDtIniVldtProd(int wpolDtIniVldtProd) {
        this.wpolDtIniVldtProd = wpolDtIniVldtProd;
    }

    public int getWpolDtIniVldtProd() {
        return this.wpolDtIniVldtProd;
    }

    public String getWpolDtIniVldtProdFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getWpolDtIniVldtProd()).toString();
    }

    public void setWpolCodConv(String wpolCodConv) {
        this.wpolCodConv = Functions.subString(wpolCodConv, Len.WPOL_COD_CONV);
    }

    public String getWpolCodConv() {
        return this.wpolCodConv;
    }

    public String getWpolCodConvFormatted() {
        return Functions.padBlanks(getWpolCodConv(), Len.WPOL_COD_CONV);
    }

    public void setWpolCodRamo(String wpolCodRamo) {
        this.wpolCodRamo = Functions.subString(wpolCodRamo, Len.WPOL_COD_RAMO);
    }

    public String getWpolCodRamo() {
        return this.wpolCodRamo;
    }

    public String getWpolCodRamoFormatted() {
        return Functions.padBlanks(getWpolCodRamo(), Len.WPOL_COD_RAMO);
    }

    public void setWpolTpFrmAssva(String wpolTpFrmAssva) {
        this.wpolTpFrmAssva = Functions.subString(wpolTpFrmAssva, Len.WPOL_TP_FRM_ASSVA);
    }

    public String getWpolTpFrmAssva() {
        return this.wpolTpFrmAssva;
    }

    public String getWpolTpFrmAssvaFormatted() {
        return Functions.padBlanks(getWpolTpFrmAssva(), Len.WPOL_TP_FRM_ASSVA);
    }

    public void setWpolTpRgmFisc(String wpolTpRgmFisc) {
        this.wpolTpRgmFisc = Functions.subString(wpolTpRgmFisc, Len.WPOL_TP_RGM_FISC);
    }

    public String getWpolTpRgmFisc() {
        return this.wpolTpRgmFisc;
    }

    public void setWpolFlEstas(char wpolFlEstas) {
        this.wpolFlEstas = wpolFlEstas;
    }

    public char getWpolFlEstas() {
        return this.wpolFlEstas;
    }

    public void setWpolFlRshComun(char wpolFlRshComun) {
        this.wpolFlRshComun = wpolFlRshComun;
    }

    public char getWpolFlRshComun() {
        return this.wpolFlRshComun;
    }

    public void setWpolFlRshComunCond(char wpolFlRshComunCond) {
        this.wpolFlRshComunCond = wpolFlRshComunCond;
    }

    public char getWpolFlRshComunCond() {
        return this.wpolFlRshComunCond;
    }

    public void setWpolTpLivGenzTit(String wpolTpLivGenzTit) {
        this.wpolTpLivGenzTit = Functions.subString(wpolTpLivGenzTit, Len.WPOL_TP_LIV_GENZ_TIT);
    }

    public String getWpolTpLivGenzTit() {
        return this.wpolTpLivGenzTit;
    }

    public void setWpolFlCopFinanz(char wpolFlCopFinanz) {
        this.wpolFlCopFinanz = wpolFlCopFinanz;
    }

    public char getWpolFlCopFinanz() {
        return this.wpolFlCopFinanz;
    }

    public void setWpolTpApplzDir(String wpolTpApplzDir) {
        this.wpolTpApplzDir = Functions.subString(wpolTpApplzDir, Len.WPOL_TP_APPLZ_DIR);
    }

    public String getWpolTpApplzDir() {
        return this.wpolTpApplzDir;
    }

    public void setWpolCodDvs(String wpolCodDvs) {
        this.wpolCodDvs = Functions.subString(wpolCodDvs, Len.WPOL_COD_DVS);
    }

    public String getWpolCodDvs() {
        return this.wpolCodDvs;
    }

    public void setWpolFlFntAz(char wpolFlFntAz) {
        this.wpolFlFntAz = wpolFlFntAz;
    }

    public char getWpolFlFntAz() {
        return this.wpolFlFntAz;
    }

    public void setWpolFlFntAder(char wpolFlFntAder) {
        this.wpolFlFntAder = wpolFlFntAder;
    }

    public char getWpolFlFntAder() {
        return this.wpolFlFntAder;
    }

    public void setWpolFlFntTfr(char wpolFlFntTfr) {
        this.wpolFlFntTfr = wpolFlFntTfr;
    }

    public char getWpolFlFntTfr() {
        return this.wpolFlFntTfr;
    }

    public void setWpolFlFntVolo(char wpolFlFntVolo) {
        this.wpolFlFntVolo = wpolFlFntVolo;
    }

    public char getWpolFlFntVolo() {
        return this.wpolFlFntVolo;
    }

    public void setWpolTpOpzAScad(String wpolTpOpzAScad) {
        this.wpolTpOpzAScad = Functions.subString(wpolTpOpzAScad, Len.WPOL_TP_OPZ_A_SCAD);
    }

    public String getWpolTpOpzAScad() {
        return this.wpolTpOpzAScad;
    }

    public void setWpolFlVerProd(String wpolFlVerProd) {
        this.wpolFlVerProd = Functions.subString(wpolFlVerProd, Len.WPOL_FL_VER_PROD);
    }

    public String getWpolFlVerProd() {
        return this.wpolFlVerProd;
    }

    public String getWpolFlVerProdFormatted() {
        return Functions.padBlanks(getWpolFlVerProd(), Len.WPOL_FL_VER_PROD);
    }

    public void setWpolTpPtfEstno(String wpolTpPtfEstno) {
        this.wpolTpPtfEstno = Functions.subString(wpolTpPtfEstno, Len.WPOL_TP_PTF_ESTNO);
    }

    public String getWpolTpPtfEstno() {
        return this.wpolTpPtfEstno;
    }

    public void setWpolFlCumPreCntr(char wpolFlCumPreCntr) {
        this.wpolFlCumPreCntr = wpolFlCumPreCntr;
    }

    public char getWpolFlCumPreCntr() {
        return this.wpolFlCumPreCntr;
    }

    public void setWpolFlAmmbMovi(char wpolFlAmmbMovi) {
        this.wpolFlAmmbMovi = wpolFlAmmbMovi;
    }

    public char getWpolFlAmmbMovi() {
        return this.wpolFlAmmbMovi;
    }

    public void setWpolConvGeco(String wpolConvGeco) {
        this.wpolConvGeco = Functions.subString(wpolConvGeco, Len.WPOL_CONV_GECO);
    }

    public String getWpolConvGeco() {
        return this.wpolConvGeco;
    }

    public void setWpolDsRiga(long wpolDsRiga) {
        this.wpolDsRiga = wpolDsRiga;
    }

    public long getWpolDsRiga() {
        return this.wpolDsRiga;
    }

    public void setWpolDsOperSql(char wpolDsOperSql) {
        this.wpolDsOperSql = wpolDsOperSql;
    }

    public char getWpolDsOperSql() {
        return this.wpolDsOperSql;
    }

    public void setWpolDsVer(int wpolDsVer) {
        this.wpolDsVer = wpolDsVer;
    }

    public int getWpolDsVer() {
        return this.wpolDsVer;
    }

    public void setWpolDsTsIniCptz(long wpolDsTsIniCptz) {
        this.wpolDsTsIniCptz = wpolDsTsIniCptz;
    }

    public long getWpolDsTsIniCptz() {
        return this.wpolDsTsIniCptz;
    }

    public void setWpolDsTsEndCptz(long wpolDsTsEndCptz) {
        this.wpolDsTsEndCptz = wpolDsTsEndCptz;
    }

    public long getWpolDsTsEndCptz() {
        return this.wpolDsTsEndCptz;
    }

    public void setWpolDsUtente(String wpolDsUtente) {
        this.wpolDsUtente = Functions.subString(wpolDsUtente, Len.WPOL_DS_UTENTE);
    }

    public String getWpolDsUtente() {
        return this.wpolDsUtente;
    }

    public void setWpolDsStatoElab(char wpolDsStatoElab) {
        this.wpolDsStatoElab = wpolDsStatoElab;
    }

    public char getWpolDsStatoElab() {
        return this.wpolDsStatoElab;
    }

    public void setWpolFlScudoFisc(char wpolFlScudoFisc) {
        this.wpolFlScudoFisc = wpolFlScudoFisc;
    }

    public char getWpolFlScudoFisc() {
        return this.wpolFlScudoFisc;
    }

    public void setWpolFlTrasfe(char wpolFlTrasfe) {
        this.wpolFlTrasfe = wpolFlTrasfe;
    }

    public char getWpolFlTrasfe() {
        return this.wpolFlTrasfe;
    }

    public void setWpolFlTfrStrc(char wpolFlTfrStrc) {
        this.wpolFlTfrStrc = wpolFlTfrStrc;
    }

    public char getWpolFlTfrStrc() {
        return this.wpolFlTfrStrc;
    }

    public void setWpolCodConvAgg(String wpolCodConvAgg) {
        this.wpolCodConvAgg = Functions.subString(wpolCodConvAgg, Len.WPOL_COD_CONV_AGG);
    }

    public String getWpolCodConvAgg() {
        return this.wpolCodConvAgg;
    }

    public void setWpolSubcatProd(String wpolSubcatProd) {
        this.wpolSubcatProd = Functions.subString(wpolSubcatProd, Len.WPOL_SUBCAT_PROD);
    }

    public String getWpolSubcatProd() {
        return this.wpolSubcatProd;
    }

    public void setWpolFlQuestAdegzAss(char wpolFlQuestAdegzAss) {
        this.wpolFlQuestAdegzAss = wpolFlQuestAdegzAss;
    }

    public char getWpolFlQuestAdegzAss() {
        return this.wpolFlQuestAdegzAss;
    }

    public void setWpolCodTpa(String wpolCodTpa) {
        this.wpolCodTpa = Functions.subString(wpolCodTpa, Len.WPOL_COD_TPA);
    }

    public String getWpolCodTpa() {
        return this.wpolCodTpa;
    }

    public void setWpolFlPoliCpiPr(char wpolFlPoliCpiPr) {
        this.wpolFlPoliCpiPr = wpolFlPoliCpiPr;
    }

    public char getWpolFlPoliCpiPr() {
        return this.wpolFlPoliCpiPr;
    }

    public void setWpolFlPoliBundling(char wpolFlPoliBundling) {
        this.wpolFlPoliBundling = wpolFlPoliBundling;
    }

    public char getWpolFlPoliBundling() {
        return this.wpolFlPoliBundling;
    }

    public void setWpolIndPoliPrinColl(char wpolIndPoliPrinColl) {
        this.wpolIndPoliPrinColl = wpolIndPoliPrinColl;
    }

    public char getWpolIndPoliPrinColl() {
        return this.wpolIndPoliPrinColl;
    }

    public void setWpolFlVndBundle(char wpolFlVndBundle) {
        this.wpolFlVndBundle = wpolFlVndBundle;
    }

    public char getWpolFlVndBundle() {
        return this.wpolFlVndBundle;
    }

    public void setWpolIbBs(String wpolIbBs) {
        this.wpolIbBs = Functions.subString(wpolIbBs, Len.WPOL_IB_BS);
    }

    public String getWpolIbBs() {
        return this.wpolIbBs;
    }

    public void setWpolFlPoliIfp(char wpolFlPoliIfp) {
        this.wpolFlPoliIfp = wpolFlPoliIfp;
    }

    public char getWpolFlPoliIfp() {
        return this.wpolFlPoliIfp;
    }

    public WpolAaDiffProrDflt getWpolAaDiffProrDflt() {
        return wpolAaDiffProrDflt;
    }

    public WpolDir1oVers getWpolDir1oVers() {
        return wpolDir1oVers;
    }

    public WpolDirEmis getWpolDirEmis() {
        return wpolDirEmis;
    }

    public WpolDirQuiet getWpolDirQuiet() {
        return wpolDirQuiet;
    }

    public WpolDirVersAgg getWpolDirVersAgg() {
        return wpolDirVersAgg;
    }

    public WpolDtApplzConv getWpolDtApplzConv() {
        return wpolDtApplzConv;
    }

    public WpolDtIniVldtConv getWpolDtIniVldtConv() {
        return wpolDtIniVldtConv;
    }

    public WpolDtPresc getWpolDtPresc() {
        return wpolDtPresc;
    }

    public WpolDtProp getWpolDtProp() {
        return wpolDtProp;
    }

    public WpolDtScad getWpolDtScad() {
        return wpolDtScad;
    }

    public WpolDurAa getWpolDurAa() {
        return wpolDurAa;
    }

    public WpolDurGg getWpolDurGg() {
        return wpolDurGg;
    }

    public WpolDurMm getWpolDurMm() {
        return wpolDurMm;
    }

    public WpolIdAccComm getWpolIdAccComm() {
        return wpolIdAccComm;
    }

    public WpolIdMoviChiu getWpolIdMoviChiu() {
        return wpolIdMoviChiu;
    }

    public WpolSpeMed getWpolSpeMed() {
        return wpolSpeMed;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_IB_OGG = 40;
        public static final int WPOL_IB_PROP = 40;
        public static final int WPOL_TP_POLI = 2;
        public static final int WPOL_COD_PROD = 12;
        public static final int WPOL_COD_CONV = 12;
        public static final int WPOL_COD_RAMO = 12;
        public static final int WPOL_TP_FRM_ASSVA = 2;
        public static final int WPOL_TP_RGM_FISC = 2;
        public static final int WPOL_TP_LIV_GENZ_TIT = 2;
        public static final int WPOL_TP_APPLZ_DIR = 2;
        public static final int WPOL_COD_DVS = 20;
        public static final int WPOL_TP_OPZ_A_SCAD = 2;
        public static final int WPOL_FL_VER_PROD = 2;
        public static final int WPOL_TP_PTF_ESTNO = 2;
        public static final int WPOL_CONV_GECO = 5;
        public static final int WPOL_DS_UTENTE = 20;
        public static final int WPOL_COD_CONV_AGG = 12;
        public static final int WPOL_SUBCAT_PROD = 12;
        public static final int WPOL_COD_TPA = 4;
        public static final int WPOL_IB_BS = 40;
        public static final int WPOL_ID_POLI = 5;
        public static final int WPOL_ID_MOVI_CRZ = 5;
        public static final int WPOL_DT_INI_EFF = 5;
        public static final int WPOL_DT_END_EFF = 5;
        public static final int WPOL_COD_COMP_ANIA = 3;
        public static final int WPOL_DT_DECOR = 5;
        public static final int WPOL_DT_EMIS = 5;
        public static final int WPOL_DT_INI_VLDT_PROD = 5;
        public static final int WPOL_FL_ESTAS = 1;
        public static final int WPOL_FL_RSH_COMUN = 1;
        public static final int WPOL_FL_RSH_COMUN_COND = 1;
        public static final int WPOL_FL_COP_FINANZ = 1;
        public static final int WPOL_FL_FNT_AZ = 1;
        public static final int WPOL_FL_FNT_ADER = 1;
        public static final int WPOL_FL_FNT_TFR = 1;
        public static final int WPOL_FL_FNT_VOLO = 1;
        public static final int WPOL_FL_CUM_PRE_CNTR = 1;
        public static final int WPOL_FL_AMMB_MOVI = 1;
        public static final int WPOL_DS_RIGA = 6;
        public static final int WPOL_DS_OPER_SQL = 1;
        public static final int WPOL_DS_VER = 5;
        public static final int WPOL_DS_TS_INI_CPTZ = 10;
        public static final int WPOL_DS_TS_END_CPTZ = 10;
        public static final int WPOL_DS_STATO_ELAB = 1;
        public static final int WPOL_FL_SCUDO_FISC = 1;
        public static final int WPOL_FL_TRASFE = 1;
        public static final int WPOL_FL_TFR_STRC = 1;
        public static final int WPOL_FL_QUEST_ADEGZ_ASS = 1;
        public static final int WPOL_FL_POLI_CPI_PR = 1;
        public static final int WPOL_FL_POLI_BUNDLING = 1;
        public static final int WPOL_IND_POLI_PRIN_COLL = 1;
        public static final int WPOL_FL_VND_BUNDLE = 1;
        public static final int WPOL_FL_POLI_IFP = 1;
        public static final int DATI = WPOL_ID_POLI + WPOL_ID_MOVI_CRZ + WpolIdMoviChiu.Len.WPOL_ID_MOVI_CHIU + WPOL_IB_OGG + WPOL_IB_PROP + WpolDtProp.Len.WPOL_DT_PROP + WPOL_DT_INI_EFF + WPOL_DT_END_EFF + WPOL_COD_COMP_ANIA + WPOL_DT_DECOR + WPOL_DT_EMIS + WPOL_TP_POLI + WpolDurAa.Len.WPOL_DUR_AA + WpolDurMm.Len.WPOL_DUR_MM + WpolDtScad.Len.WPOL_DT_SCAD + WPOL_COD_PROD + WPOL_DT_INI_VLDT_PROD + WPOL_COD_CONV + WPOL_COD_RAMO + WpolDtIniVldtConv.Len.WPOL_DT_INI_VLDT_CONV + WpolDtApplzConv.Len.WPOL_DT_APPLZ_CONV + WPOL_TP_FRM_ASSVA + WPOL_TP_RGM_FISC + WPOL_FL_ESTAS + WPOL_FL_RSH_COMUN + WPOL_FL_RSH_COMUN_COND + WPOL_TP_LIV_GENZ_TIT + WPOL_FL_COP_FINANZ + WPOL_TP_APPLZ_DIR + WpolSpeMed.Len.WPOL_SPE_MED + WpolDirEmis.Len.WPOL_DIR_EMIS + WpolDir1oVers.Len.WPOL_DIR1O_VERS + WpolDirVersAgg.Len.WPOL_DIR_VERS_AGG + WPOL_COD_DVS + WPOL_FL_FNT_AZ + WPOL_FL_FNT_ADER + WPOL_FL_FNT_TFR + WPOL_FL_FNT_VOLO + WPOL_TP_OPZ_A_SCAD + WpolAaDiffProrDflt.Len.WPOL_AA_DIFF_PROR_DFLT + WPOL_FL_VER_PROD + WpolDurGg.Len.WPOL_DUR_GG + WpolDirQuiet.Len.WPOL_DIR_QUIET + WPOL_TP_PTF_ESTNO + WPOL_FL_CUM_PRE_CNTR + WPOL_FL_AMMB_MOVI + WPOL_CONV_GECO + WPOL_DS_RIGA + WPOL_DS_OPER_SQL + WPOL_DS_VER + WPOL_DS_TS_INI_CPTZ + WPOL_DS_TS_END_CPTZ + WPOL_DS_UTENTE + WPOL_DS_STATO_ELAB + WPOL_FL_SCUDO_FISC + WPOL_FL_TRASFE + WPOL_FL_TFR_STRC + WpolDtPresc.Len.WPOL_DT_PRESC + WPOL_COD_CONV_AGG + WPOL_SUBCAT_PROD + WPOL_FL_QUEST_ADEGZ_ASS + WPOL_COD_TPA + WpolIdAccComm.Len.WPOL_ID_ACC_COMM + WPOL_FL_POLI_CPI_PR + WPOL_FL_POLI_BUNDLING + WPOL_IND_POLI_PRIN_COLL + WPOL_FL_VND_BUNDLE + WPOL_IB_BS + WPOL_FL_POLI_IFP;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_ID_POLI = 9;
            public static final int WPOL_ID_MOVI_CRZ = 9;
            public static final int WPOL_DT_INI_EFF = 8;
            public static final int WPOL_DT_END_EFF = 8;
            public static final int WPOL_COD_COMP_ANIA = 5;
            public static final int WPOL_DT_DECOR = 8;
            public static final int WPOL_DT_EMIS = 8;
            public static final int WPOL_DT_INI_VLDT_PROD = 8;
            public static final int WPOL_DS_RIGA = 10;
            public static final int WPOL_DS_VER = 9;
            public static final int WPOL_DS_TS_INI_CPTZ = 18;
            public static final int WPOL_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
