package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.TitDtApplzMora;
import it.accenture.jnais.ws.redefines.TitDtCambioVlt;
import it.accenture.jnais.ws.redefines.TitDtCertFisc;
import it.accenture.jnais.ws.redefines.TitDtEmisTit;
import it.accenture.jnais.ws.redefines.TitDtEndCop;
import it.accenture.jnais.ws.redefines.TitDtEsiTit;
import it.accenture.jnais.ws.redefines.TitDtIniCop;
import it.accenture.jnais.ws.redefines.TitDtRichAddRid;
import it.accenture.jnais.ws.redefines.TitDtVlt;
import it.accenture.jnais.ws.redefines.TitFraz;
import it.accenture.jnais.ws.redefines.TitIdMoviChiu;
import it.accenture.jnais.ws.redefines.TitIdRappAna;
import it.accenture.jnais.ws.redefines.TitIdRappRete;
import it.accenture.jnais.ws.redefines.TitImpAder;
import it.accenture.jnais.ws.redefines.TitImpAz;
import it.accenture.jnais.ws.redefines.TitImpPag;
import it.accenture.jnais.ws.redefines.TitImpTfr;
import it.accenture.jnais.ws.redefines.TitImpTfrStrc;
import it.accenture.jnais.ws.redefines.TitImpTrasfe;
import it.accenture.jnais.ws.redefines.TitImpVolo;
import it.accenture.jnais.ws.redefines.TitNumRatAccorpate;
import it.accenture.jnais.ws.redefines.TitProgTit;
import it.accenture.jnais.ws.redefines.TitTotAcqExp;
import it.accenture.jnais.ws.redefines.TitTotCarAcq;
import it.accenture.jnais.ws.redefines.TitTotCarGest;
import it.accenture.jnais.ws.redefines.TitTotCarIas;
import it.accenture.jnais.ws.redefines.TitTotCarInc;
import it.accenture.jnais.ws.redefines.TitTotCnbtAntirac;
import it.accenture.jnais.ws.redefines.TitTotCommisInter;
import it.accenture.jnais.ws.redefines.TitTotDir;
import it.accenture.jnais.ws.redefines.TitTotIntrFraz;
import it.accenture.jnais.ws.redefines.TitTotIntrMora;
import it.accenture.jnais.ws.redefines.TitTotIntrPrest;
import it.accenture.jnais.ws.redefines.TitTotIntrRetdt;
import it.accenture.jnais.ws.redefines.TitTotIntrRiat;
import it.accenture.jnais.ws.redefines.TitTotManfeeAntic;
import it.accenture.jnais.ws.redefines.TitTotManfeeRec;
import it.accenture.jnais.ws.redefines.TitTotManfeeRicor;
import it.accenture.jnais.ws.redefines.TitTotPreNet;
import it.accenture.jnais.ws.redefines.TitTotPrePpIas;
import it.accenture.jnais.ws.redefines.TitTotPreSoloRsh;
import it.accenture.jnais.ws.redefines.TitTotPreTot;
import it.accenture.jnais.ws.redefines.TitTotProvAcq1aa;
import it.accenture.jnais.ws.redefines.TitTotProvAcq2aa;
import it.accenture.jnais.ws.redefines.TitTotProvDaRec;
import it.accenture.jnais.ws.redefines.TitTotProvInc;
import it.accenture.jnais.ws.redefines.TitTotProvRicor;
import it.accenture.jnais.ws.redefines.TitTotRemunAss;
import it.accenture.jnais.ws.redefines.TitTotSoprAlt;
import it.accenture.jnais.ws.redefines.TitTotSoprProf;
import it.accenture.jnais.ws.redefines.TitTotSoprSan;
import it.accenture.jnais.ws.redefines.TitTotSoprSpo;
import it.accenture.jnais.ws.redefines.TitTotSoprTec;
import it.accenture.jnais.ws.redefines.TitTotSpeAge;
import it.accenture.jnais.ws.redefines.TitTotSpeMed;
import it.accenture.jnais.ws.redefines.TitTotTax;
import it.accenture.jnais.ws.redefines.TitTpCausStor;

/**Original name: TIT-CONT<br>
 * Variable: TIT-CONT from copybook IDBVTIT1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TitCont {

    //==== PROPERTIES ====
    //Original name: TIT-ID-TIT-CONT
    private int titIdTitCont = DefaultValues.INT_VAL;
    //Original name: TIT-ID-OGG
    private int titIdOgg = DefaultValues.INT_VAL;
    //Original name: TIT-TP-OGG
    private String titTpOgg = DefaultValues.stringVal(Len.TIT_TP_OGG);
    //Original name: TIT-IB-RICH
    private String titIbRich = DefaultValues.stringVal(Len.TIT_IB_RICH);
    //Original name: TIT-ID-MOVI-CRZ
    private int titIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: TIT-ID-MOVI-CHIU
    private TitIdMoviChiu titIdMoviChiu = new TitIdMoviChiu();
    //Original name: TIT-DT-INI-EFF
    private int titDtIniEff = DefaultValues.INT_VAL;
    //Original name: TIT-DT-END-EFF
    private int titDtEndEff = DefaultValues.INT_VAL;
    //Original name: TIT-COD-COMP-ANIA
    private int titCodCompAnia = DefaultValues.INT_VAL;
    //Original name: TIT-TP-TIT
    private String titTpTit = DefaultValues.stringVal(Len.TIT_TP_TIT);
    //Original name: TIT-PROG-TIT
    private TitProgTit titProgTit = new TitProgTit();
    //Original name: TIT-TP-PRE-TIT
    private String titTpPreTit = DefaultValues.stringVal(Len.TIT_TP_PRE_TIT);
    //Original name: TIT-TP-STAT-TIT
    private String titTpStatTit = DefaultValues.stringVal(Len.TIT_TP_STAT_TIT);
    //Original name: TIT-DT-INI-COP
    private TitDtIniCop titDtIniCop = new TitDtIniCop();
    //Original name: TIT-DT-END-COP
    private TitDtEndCop titDtEndCop = new TitDtEndCop();
    //Original name: TIT-IMP-PAG
    private TitImpPag titImpPag = new TitImpPag();
    //Original name: TIT-FL-SOLL
    private char titFlSoll = DefaultValues.CHAR_VAL;
    //Original name: TIT-FRAZ
    private TitFraz titFraz = new TitFraz();
    //Original name: TIT-DT-APPLZ-MORA
    private TitDtApplzMora titDtApplzMora = new TitDtApplzMora();
    //Original name: TIT-FL-MORA
    private char titFlMora = DefaultValues.CHAR_VAL;
    //Original name: TIT-ID-RAPP-RETE
    private TitIdRappRete titIdRappRete = new TitIdRappRete();
    //Original name: TIT-ID-RAPP-ANA
    private TitIdRappAna titIdRappAna = new TitIdRappAna();
    //Original name: TIT-COD-DVS
    private String titCodDvs = DefaultValues.stringVal(Len.TIT_COD_DVS);
    //Original name: TIT-DT-EMIS-TIT
    private TitDtEmisTit titDtEmisTit = new TitDtEmisTit();
    //Original name: TIT-DT-ESI-TIT
    private TitDtEsiTit titDtEsiTit = new TitDtEsiTit();
    //Original name: TIT-TOT-PRE-NET
    private TitTotPreNet titTotPreNet = new TitTotPreNet();
    //Original name: TIT-TOT-INTR-FRAZ
    private TitTotIntrFraz titTotIntrFraz = new TitTotIntrFraz();
    //Original name: TIT-TOT-INTR-MORA
    private TitTotIntrMora titTotIntrMora = new TitTotIntrMora();
    //Original name: TIT-TOT-INTR-PREST
    private TitTotIntrPrest titTotIntrPrest = new TitTotIntrPrest();
    //Original name: TIT-TOT-INTR-RETDT
    private TitTotIntrRetdt titTotIntrRetdt = new TitTotIntrRetdt();
    //Original name: TIT-TOT-INTR-RIAT
    private TitTotIntrRiat titTotIntrRiat = new TitTotIntrRiat();
    //Original name: TIT-TOT-DIR
    private TitTotDir titTotDir = new TitTotDir();
    //Original name: TIT-TOT-SPE-MED
    private TitTotSpeMed titTotSpeMed = new TitTotSpeMed();
    //Original name: TIT-TOT-TAX
    private TitTotTax titTotTax = new TitTotTax();
    //Original name: TIT-TOT-SOPR-SAN
    private TitTotSoprSan titTotSoprSan = new TitTotSoprSan();
    //Original name: TIT-TOT-SOPR-TEC
    private TitTotSoprTec titTotSoprTec = new TitTotSoprTec();
    //Original name: TIT-TOT-SOPR-SPO
    private TitTotSoprSpo titTotSoprSpo = new TitTotSoprSpo();
    //Original name: TIT-TOT-SOPR-PROF
    private TitTotSoprProf titTotSoprProf = new TitTotSoprProf();
    //Original name: TIT-TOT-SOPR-ALT
    private TitTotSoprAlt titTotSoprAlt = new TitTotSoprAlt();
    //Original name: TIT-TOT-PRE-TOT
    private TitTotPreTot titTotPreTot = new TitTotPreTot();
    //Original name: TIT-TOT-PRE-PP-IAS
    private TitTotPrePpIas titTotPrePpIas = new TitTotPrePpIas();
    //Original name: TIT-TOT-CAR-ACQ
    private TitTotCarAcq titTotCarAcq = new TitTotCarAcq();
    //Original name: TIT-TOT-CAR-GEST
    private TitTotCarGest titTotCarGest = new TitTotCarGest();
    //Original name: TIT-TOT-CAR-INC
    private TitTotCarInc titTotCarInc = new TitTotCarInc();
    //Original name: TIT-TOT-PRE-SOLO-RSH
    private TitTotPreSoloRsh titTotPreSoloRsh = new TitTotPreSoloRsh();
    //Original name: TIT-TOT-PROV-ACQ-1AA
    private TitTotProvAcq1aa titTotProvAcq1aa = new TitTotProvAcq1aa();
    //Original name: TIT-TOT-PROV-ACQ-2AA
    private TitTotProvAcq2aa titTotProvAcq2aa = new TitTotProvAcq2aa();
    //Original name: TIT-TOT-PROV-RICOR
    private TitTotProvRicor titTotProvRicor = new TitTotProvRicor();
    //Original name: TIT-TOT-PROV-INC
    private TitTotProvInc titTotProvInc = new TitTotProvInc();
    //Original name: TIT-TOT-PROV-DA-REC
    private TitTotProvDaRec titTotProvDaRec = new TitTotProvDaRec();
    //Original name: TIT-IMP-AZ
    private TitImpAz titImpAz = new TitImpAz();
    //Original name: TIT-IMP-ADER
    private TitImpAder titImpAder = new TitImpAder();
    //Original name: TIT-IMP-TFR
    private TitImpTfr titImpTfr = new TitImpTfr();
    //Original name: TIT-IMP-VOLO
    private TitImpVolo titImpVolo = new TitImpVolo();
    //Original name: TIT-TOT-MANFEE-ANTIC
    private TitTotManfeeAntic titTotManfeeAntic = new TitTotManfeeAntic();
    //Original name: TIT-TOT-MANFEE-RICOR
    private TitTotManfeeRicor titTotManfeeRicor = new TitTotManfeeRicor();
    //Original name: TIT-TOT-MANFEE-REC
    private TitTotManfeeRec titTotManfeeRec = new TitTotManfeeRec();
    //Original name: TIT-TP-MEZ-PAG-ADD
    private String titTpMezPagAdd = DefaultValues.stringVal(Len.TIT_TP_MEZ_PAG_ADD);
    //Original name: TIT-ESTR-CNT-CORR-ADD
    private String titEstrCntCorrAdd = DefaultValues.stringVal(Len.TIT_ESTR_CNT_CORR_ADD);
    //Original name: TIT-DT-VLT
    private TitDtVlt titDtVlt = new TitDtVlt();
    //Original name: TIT-FL-FORZ-DT-VLT
    private char titFlForzDtVlt = DefaultValues.CHAR_VAL;
    //Original name: TIT-DT-CAMBIO-VLT
    private TitDtCambioVlt titDtCambioVlt = new TitDtCambioVlt();
    //Original name: TIT-TOT-SPE-AGE
    private TitTotSpeAge titTotSpeAge = new TitTotSpeAge();
    //Original name: TIT-TOT-CAR-IAS
    private TitTotCarIas titTotCarIas = new TitTotCarIas();
    //Original name: TIT-NUM-RAT-ACCORPATE
    private TitNumRatAccorpate titNumRatAccorpate = new TitNumRatAccorpate();
    //Original name: TIT-DS-RIGA
    private long titDsRiga = DefaultValues.LONG_VAL;
    //Original name: TIT-DS-OPER-SQL
    private char titDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: TIT-DS-VER
    private int titDsVer = DefaultValues.INT_VAL;
    //Original name: TIT-DS-TS-INI-CPTZ
    private long titDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: TIT-DS-TS-END-CPTZ
    private long titDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: TIT-DS-UTENTE
    private String titDsUtente = DefaultValues.stringVal(Len.TIT_DS_UTENTE);
    //Original name: TIT-DS-STATO-ELAB
    private char titDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: TIT-FL-TIT-DA-REINVST
    private char titFlTitDaReinvst = DefaultValues.CHAR_VAL;
    //Original name: TIT-DT-RICH-ADD-RID
    private TitDtRichAddRid titDtRichAddRid = new TitDtRichAddRid();
    //Original name: TIT-TP-ESI-RID
    private String titTpEsiRid = DefaultValues.stringVal(Len.TIT_TP_ESI_RID);
    //Original name: TIT-COD-IBAN
    private String titCodIban = DefaultValues.stringVal(Len.TIT_COD_IBAN);
    //Original name: TIT-IMP-TRASFE
    private TitImpTrasfe titImpTrasfe = new TitImpTrasfe();
    //Original name: TIT-IMP-TFR-STRC
    private TitImpTfrStrc titImpTfrStrc = new TitImpTfrStrc();
    //Original name: TIT-DT-CERT-FISC
    private TitDtCertFisc titDtCertFisc = new TitDtCertFisc();
    //Original name: TIT-TP-CAUS-STOR
    private TitTpCausStor titTpCausStor = new TitTpCausStor();
    //Original name: TIT-TP-CAUS-DISP-STOR
    private String titTpCausDispStor = DefaultValues.stringVal(Len.TIT_TP_CAUS_DISP_STOR);
    //Original name: TIT-TP-TIT-MIGRAZ
    private String titTpTitMigraz = DefaultValues.stringVal(Len.TIT_TP_TIT_MIGRAZ);
    //Original name: TIT-TOT-ACQ-EXP
    private TitTotAcqExp titTotAcqExp = new TitTotAcqExp();
    //Original name: TIT-TOT-REMUN-ASS
    private TitTotRemunAss titTotRemunAss = new TitTotRemunAss();
    //Original name: TIT-TOT-COMMIS-INTER
    private TitTotCommisInter titTotCommisInter = new TitTotCommisInter();
    //Original name: TIT-TP-CAUS-RIMB
    private String titTpCausRimb = DefaultValues.stringVal(Len.TIT_TP_CAUS_RIMB);
    //Original name: TIT-TOT-CNBT-ANTIRAC
    private TitTotCnbtAntirac titTotCnbtAntirac = new TitTotCnbtAntirac();
    //Original name: TIT-FL-INC-AUTOGEN
    private char titFlIncAutogen = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setTitContFormatted(String data) {
        byte[] buffer = new byte[Len.TIT_CONT];
        MarshalByte.writeString(buffer, 1, data, Len.TIT_CONT);
        setTitContBytes(buffer, 1);
    }

    public String getTitContFormatted() {
        return MarshalByteExt.bufferToStr(getTitContBytes());
    }

    public byte[] getTitContBytes() {
        byte[] buffer = new byte[Len.TIT_CONT];
        return getTitContBytes(buffer, 1);
    }

    public void setTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        titIdTitCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TIT_ID_TIT_CONT, 0);
        position += Len.TIT_ID_TIT_CONT;
        titIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TIT_ID_OGG, 0);
        position += Len.TIT_ID_OGG;
        titTpOgg = MarshalByte.readString(buffer, position, Len.TIT_TP_OGG);
        position += Len.TIT_TP_OGG;
        titIbRich = MarshalByte.readString(buffer, position, Len.TIT_IB_RICH);
        position += Len.TIT_IB_RICH;
        titIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TIT_ID_MOVI_CRZ, 0);
        position += Len.TIT_ID_MOVI_CRZ;
        titIdMoviChiu.setTitIdMoviChiuFromBuffer(buffer, position);
        position += TitIdMoviChiu.Len.TIT_ID_MOVI_CHIU;
        titDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TIT_DT_INI_EFF, 0);
        position += Len.TIT_DT_INI_EFF;
        titDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TIT_DT_END_EFF, 0);
        position += Len.TIT_DT_END_EFF;
        titCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TIT_COD_COMP_ANIA, 0);
        position += Len.TIT_COD_COMP_ANIA;
        titTpTit = MarshalByte.readString(buffer, position, Len.TIT_TP_TIT);
        position += Len.TIT_TP_TIT;
        titProgTit.setTitProgTitFromBuffer(buffer, position);
        position += TitProgTit.Len.TIT_PROG_TIT;
        titTpPreTit = MarshalByte.readString(buffer, position, Len.TIT_TP_PRE_TIT);
        position += Len.TIT_TP_PRE_TIT;
        titTpStatTit = MarshalByte.readString(buffer, position, Len.TIT_TP_STAT_TIT);
        position += Len.TIT_TP_STAT_TIT;
        titDtIniCop.setTitDtIniCopFromBuffer(buffer, position);
        position += TitDtIniCop.Len.TIT_DT_INI_COP;
        titDtEndCop.setTitDtEndCopFromBuffer(buffer, position);
        position += TitDtEndCop.Len.TIT_DT_END_COP;
        titImpPag.setTitImpPagFromBuffer(buffer, position);
        position += TitImpPag.Len.TIT_IMP_PAG;
        titFlSoll = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        titFraz.setTitFrazFromBuffer(buffer, position);
        position += TitFraz.Len.TIT_FRAZ;
        titDtApplzMora.setTitDtApplzMoraFromBuffer(buffer, position);
        position += TitDtApplzMora.Len.TIT_DT_APPLZ_MORA;
        titFlMora = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        titIdRappRete.setTitIdRappReteFromBuffer(buffer, position);
        position += TitIdRappRete.Len.TIT_ID_RAPP_RETE;
        titIdRappAna.setTitIdRappAnaFromBuffer(buffer, position);
        position += TitIdRappAna.Len.TIT_ID_RAPP_ANA;
        titCodDvs = MarshalByte.readString(buffer, position, Len.TIT_COD_DVS);
        position += Len.TIT_COD_DVS;
        titDtEmisTit.setTitDtEmisTitFromBuffer(buffer, position);
        position += TitDtEmisTit.Len.TIT_DT_EMIS_TIT;
        titDtEsiTit.setTitDtEsiTitFromBuffer(buffer, position);
        position += TitDtEsiTit.Len.TIT_DT_ESI_TIT;
        titTotPreNet.setTitTotPreNetFromBuffer(buffer, position);
        position += TitTotPreNet.Len.TIT_TOT_PRE_NET;
        titTotIntrFraz.setTitTotIntrFrazFromBuffer(buffer, position);
        position += TitTotIntrFraz.Len.TIT_TOT_INTR_FRAZ;
        titTotIntrMora.setTitTotIntrMoraFromBuffer(buffer, position);
        position += TitTotIntrMora.Len.TIT_TOT_INTR_MORA;
        titTotIntrPrest.setTitTotIntrPrestFromBuffer(buffer, position);
        position += TitTotIntrPrest.Len.TIT_TOT_INTR_PREST;
        titTotIntrRetdt.setTitTotIntrRetdtFromBuffer(buffer, position);
        position += TitTotIntrRetdt.Len.TIT_TOT_INTR_RETDT;
        titTotIntrRiat.setTitTotIntrRiatFromBuffer(buffer, position);
        position += TitTotIntrRiat.Len.TIT_TOT_INTR_RIAT;
        titTotDir.setTitTotDirFromBuffer(buffer, position);
        position += TitTotDir.Len.TIT_TOT_DIR;
        titTotSpeMed.setTitTotSpeMedFromBuffer(buffer, position);
        position += TitTotSpeMed.Len.TIT_TOT_SPE_MED;
        titTotTax.setTitTotTaxFromBuffer(buffer, position);
        position += TitTotTax.Len.TIT_TOT_TAX;
        titTotSoprSan.setTitTotSoprSanFromBuffer(buffer, position);
        position += TitTotSoprSan.Len.TIT_TOT_SOPR_SAN;
        titTotSoprTec.setTitTotSoprTecFromBuffer(buffer, position);
        position += TitTotSoprTec.Len.TIT_TOT_SOPR_TEC;
        titTotSoprSpo.setTitTotSoprSpoFromBuffer(buffer, position);
        position += TitTotSoprSpo.Len.TIT_TOT_SOPR_SPO;
        titTotSoprProf.setTitTotSoprProfFromBuffer(buffer, position);
        position += TitTotSoprProf.Len.TIT_TOT_SOPR_PROF;
        titTotSoprAlt.setTitTotSoprAltFromBuffer(buffer, position);
        position += TitTotSoprAlt.Len.TIT_TOT_SOPR_ALT;
        titTotPreTot.setTitTotPreTotFromBuffer(buffer, position);
        position += TitTotPreTot.Len.TIT_TOT_PRE_TOT;
        titTotPrePpIas.setTitTotPrePpIasFromBuffer(buffer, position);
        position += TitTotPrePpIas.Len.TIT_TOT_PRE_PP_IAS;
        titTotCarAcq.setTitTotCarAcqFromBuffer(buffer, position);
        position += TitTotCarAcq.Len.TIT_TOT_CAR_ACQ;
        titTotCarGest.setTitTotCarGestFromBuffer(buffer, position);
        position += TitTotCarGest.Len.TIT_TOT_CAR_GEST;
        titTotCarInc.setTitTotCarIncFromBuffer(buffer, position);
        position += TitTotCarInc.Len.TIT_TOT_CAR_INC;
        titTotPreSoloRsh.setTitTotPreSoloRshFromBuffer(buffer, position);
        position += TitTotPreSoloRsh.Len.TIT_TOT_PRE_SOLO_RSH;
        titTotProvAcq1aa.setTitTotProvAcq1aaFromBuffer(buffer, position);
        position += TitTotProvAcq1aa.Len.TIT_TOT_PROV_ACQ1AA;
        titTotProvAcq2aa.setTitTotProvAcq2aaFromBuffer(buffer, position);
        position += TitTotProvAcq2aa.Len.TIT_TOT_PROV_ACQ2AA;
        titTotProvRicor.setTitTotProvRicorFromBuffer(buffer, position);
        position += TitTotProvRicor.Len.TIT_TOT_PROV_RICOR;
        titTotProvInc.setTitTotProvIncFromBuffer(buffer, position);
        position += TitTotProvInc.Len.TIT_TOT_PROV_INC;
        titTotProvDaRec.setTitTotProvDaRecFromBuffer(buffer, position);
        position += TitTotProvDaRec.Len.TIT_TOT_PROV_DA_REC;
        titImpAz.setTitImpAzFromBuffer(buffer, position);
        position += TitImpAz.Len.TIT_IMP_AZ;
        titImpAder.setTitImpAderFromBuffer(buffer, position);
        position += TitImpAder.Len.TIT_IMP_ADER;
        titImpTfr.setTitImpTfrFromBuffer(buffer, position);
        position += TitImpTfr.Len.TIT_IMP_TFR;
        titImpVolo.setTitImpVoloFromBuffer(buffer, position);
        position += TitImpVolo.Len.TIT_IMP_VOLO;
        titTotManfeeAntic.setTitTotManfeeAnticFromBuffer(buffer, position);
        position += TitTotManfeeAntic.Len.TIT_TOT_MANFEE_ANTIC;
        titTotManfeeRicor.setTitTotManfeeRicorFromBuffer(buffer, position);
        position += TitTotManfeeRicor.Len.TIT_TOT_MANFEE_RICOR;
        titTotManfeeRec.setTitTotManfeeRecFromBuffer(buffer, position);
        position += TitTotManfeeRec.Len.TIT_TOT_MANFEE_REC;
        titTpMezPagAdd = MarshalByte.readString(buffer, position, Len.TIT_TP_MEZ_PAG_ADD);
        position += Len.TIT_TP_MEZ_PAG_ADD;
        titEstrCntCorrAdd = MarshalByte.readString(buffer, position, Len.TIT_ESTR_CNT_CORR_ADD);
        position += Len.TIT_ESTR_CNT_CORR_ADD;
        titDtVlt.setTitDtVltFromBuffer(buffer, position);
        position += TitDtVlt.Len.TIT_DT_VLT;
        titFlForzDtVlt = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        titDtCambioVlt.setTitDtCambioVltFromBuffer(buffer, position);
        position += TitDtCambioVlt.Len.TIT_DT_CAMBIO_VLT;
        titTotSpeAge.setTitTotSpeAgeFromBuffer(buffer, position);
        position += TitTotSpeAge.Len.TIT_TOT_SPE_AGE;
        titTotCarIas.setTitTotCarIasFromBuffer(buffer, position);
        position += TitTotCarIas.Len.TIT_TOT_CAR_IAS;
        titNumRatAccorpate.setTitNumRatAccorpateFromBuffer(buffer, position);
        position += TitNumRatAccorpate.Len.TIT_NUM_RAT_ACCORPATE;
        titDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TIT_DS_RIGA, 0);
        position += Len.TIT_DS_RIGA;
        titDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        titDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TIT_DS_VER, 0);
        position += Len.TIT_DS_VER;
        titDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TIT_DS_TS_INI_CPTZ, 0);
        position += Len.TIT_DS_TS_INI_CPTZ;
        titDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TIT_DS_TS_END_CPTZ, 0);
        position += Len.TIT_DS_TS_END_CPTZ;
        titDsUtente = MarshalByte.readString(buffer, position, Len.TIT_DS_UTENTE);
        position += Len.TIT_DS_UTENTE;
        titDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        titFlTitDaReinvst = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        titDtRichAddRid.setTitDtRichAddRidFromBuffer(buffer, position);
        position += TitDtRichAddRid.Len.TIT_DT_RICH_ADD_RID;
        titTpEsiRid = MarshalByte.readString(buffer, position, Len.TIT_TP_ESI_RID);
        position += Len.TIT_TP_ESI_RID;
        titCodIban = MarshalByte.readString(buffer, position, Len.TIT_COD_IBAN);
        position += Len.TIT_COD_IBAN;
        titImpTrasfe.setTitImpTrasfeFromBuffer(buffer, position);
        position += TitImpTrasfe.Len.TIT_IMP_TRASFE;
        titImpTfrStrc.setTitImpTfrStrcFromBuffer(buffer, position);
        position += TitImpTfrStrc.Len.TIT_IMP_TFR_STRC;
        titDtCertFisc.setTitDtCertFiscFromBuffer(buffer, position);
        position += TitDtCertFisc.Len.TIT_DT_CERT_FISC;
        titTpCausStor.setTitTpCausStorFromBuffer(buffer, position);
        position += TitTpCausStor.Len.TIT_TP_CAUS_STOR;
        titTpCausDispStor = MarshalByte.readString(buffer, position, Len.TIT_TP_CAUS_DISP_STOR);
        position += Len.TIT_TP_CAUS_DISP_STOR;
        titTpTitMigraz = MarshalByte.readString(buffer, position, Len.TIT_TP_TIT_MIGRAZ);
        position += Len.TIT_TP_TIT_MIGRAZ;
        titTotAcqExp.setTitTotAcqExpFromBuffer(buffer, position);
        position += TitTotAcqExp.Len.TIT_TOT_ACQ_EXP;
        titTotRemunAss.setTitTotRemunAssFromBuffer(buffer, position);
        position += TitTotRemunAss.Len.TIT_TOT_REMUN_ASS;
        titTotCommisInter.setTitTotCommisInterFromBuffer(buffer, position);
        position += TitTotCommisInter.Len.TIT_TOT_COMMIS_INTER;
        titTpCausRimb = MarshalByte.readString(buffer, position, Len.TIT_TP_CAUS_RIMB);
        position += Len.TIT_TP_CAUS_RIMB;
        titTotCnbtAntirac.setTitTotCnbtAntiracFromBuffer(buffer, position);
        position += TitTotCnbtAntirac.Len.TIT_TOT_CNBT_ANTIRAC;
        titFlIncAutogen = MarshalByte.readChar(buffer, position);
    }

    public byte[] getTitContBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, titIdTitCont, Len.Int.TIT_ID_TIT_CONT, 0);
        position += Len.TIT_ID_TIT_CONT;
        MarshalByte.writeIntAsPacked(buffer, position, titIdOgg, Len.Int.TIT_ID_OGG, 0);
        position += Len.TIT_ID_OGG;
        MarshalByte.writeString(buffer, position, titTpOgg, Len.TIT_TP_OGG);
        position += Len.TIT_TP_OGG;
        MarshalByte.writeString(buffer, position, titIbRich, Len.TIT_IB_RICH);
        position += Len.TIT_IB_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, titIdMoviCrz, Len.Int.TIT_ID_MOVI_CRZ, 0);
        position += Len.TIT_ID_MOVI_CRZ;
        titIdMoviChiu.getTitIdMoviChiuAsBuffer(buffer, position);
        position += TitIdMoviChiu.Len.TIT_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, titDtIniEff, Len.Int.TIT_DT_INI_EFF, 0);
        position += Len.TIT_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, titDtEndEff, Len.Int.TIT_DT_END_EFF, 0);
        position += Len.TIT_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, titCodCompAnia, Len.Int.TIT_COD_COMP_ANIA, 0);
        position += Len.TIT_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, titTpTit, Len.TIT_TP_TIT);
        position += Len.TIT_TP_TIT;
        titProgTit.getTitProgTitAsBuffer(buffer, position);
        position += TitProgTit.Len.TIT_PROG_TIT;
        MarshalByte.writeString(buffer, position, titTpPreTit, Len.TIT_TP_PRE_TIT);
        position += Len.TIT_TP_PRE_TIT;
        MarshalByte.writeString(buffer, position, titTpStatTit, Len.TIT_TP_STAT_TIT);
        position += Len.TIT_TP_STAT_TIT;
        titDtIniCop.getTitDtIniCopAsBuffer(buffer, position);
        position += TitDtIniCop.Len.TIT_DT_INI_COP;
        titDtEndCop.getTitDtEndCopAsBuffer(buffer, position);
        position += TitDtEndCop.Len.TIT_DT_END_COP;
        titImpPag.getTitImpPagAsBuffer(buffer, position);
        position += TitImpPag.Len.TIT_IMP_PAG;
        MarshalByte.writeChar(buffer, position, titFlSoll);
        position += Types.CHAR_SIZE;
        titFraz.getTitFrazAsBuffer(buffer, position);
        position += TitFraz.Len.TIT_FRAZ;
        titDtApplzMora.getTitDtApplzMoraAsBuffer(buffer, position);
        position += TitDtApplzMora.Len.TIT_DT_APPLZ_MORA;
        MarshalByte.writeChar(buffer, position, titFlMora);
        position += Types.CHAR_SIZE;
        titIdRappRete.getTitIdRappReteAsBuffer(buffer, position);
        position += TitIdRappRete.Len.TIT_ID_RAPP_RETE;
        titIdRappAna.getTitIdRappAnaAsBuffer(buffer, position);
        position += TitIdRappAna.Len.TIT_ID_RAPP_ANA;
        MarshalByte.writeString(buffer, position, titCodDvs, Len.TIT_COD_DVS);
        position += Len.TIT_COD_DVS;
        titDtEmisTit.getTitDtEmisTitAsBuffer(buffer, position);
        position += TitDtEmisTit.Len.TIT_DT_EMIS_TIT;
        titDtEsiTit.getTitDtEsiTitAsBuffer(buffer, position);
        position += TitDtEsiTit.Len.TIT_DT_ESI_TIT;
        titTotPreNet.getTitTotPreNetAsBuffer(buffer, position);
        position += TitTotPreNet.Len.TIT_TOT_PRE_NET;
        titTotIntrFraz.getTitTotIntrFrazAsBuffer(buffer, position);
        position += TitTotIntrFraz.Len.TIT_TOT_INTR_FRAZ;
        titTotIntrMora.getTitTotIntrMoraAsBuffer(buffer, position);
        position += TitTotIntrMora.Len.TIT_TOT_INTR_MORA;
        titTotIntrPrest.getTitTotIntrPrestAsBuffer(buffer, position);
        position += TitTotIntrPrest.Len.TIT_TOT_INTR_PREST;
        titTotIntrRetdt.getTitTotIntrRetdtAsBuffer(buffer, position);
        position += TitTotIntrRetdt.Len.TIT_TOT_INTR_RETDT;
        titTotIntrRiat.getTitTotIntrRiatAsBuffer(buffer, position);
        position += TitTotIntrRiat.Len.TIT_TOT_INTR_RIAT;
        titTotDir.getTitTotDirAsBuffer(buffer, position);
        position += TitTotDir.Len.TIT_TOT_DIR;
        titTotSpeMed.getTitTotSpeMedAsBuffer(buffer, position);
        position += TitTotSpeMed.Len.TIT_TOT_SPE_MED;
        titTotTax.getTitTotTaxAsBuffer(buffer, position);
        position += TitTotTax.Len.TIT_TOT_TAX;
        titTotSoprSan.getTitTotSoprSanAsBuffer(buffer, position);
        position += TitTotSoprSan.Len.TIT_TOT_SOPR_SAN;
        titTotSoprTec.getTitTotSoprTecAsBuffer(buffer, position);
        position += TitTotSoprTec.Len.TIT_TOT_SOPR_TEC;
        titTotSoprSpo.getTitTotSoprSpoAsBuffer(buffer, position);
        position += TitTotSoprSpo.Len.TIT_TOT_SOPR_SPO;
        titTotSoprProf.getTitTotSoprProfAsBuffer(buffer, position);
        position += TitTotSoprProf.Len.TIT_TOT_SOPR_PROF;
        titTotSoprAlt.getTitTotSoprAltAsBuffer(buffer, position);
        position += TitTotSoprAlt.Len.TIT_TOT_SOPR_ALT;
        titTotPreTot.getTitTotPreTotAsBuffer(buffer, position);
        position += TitTotPreTot.Len.TIT_TOT_PRE_TOT;
        titTotPrePpIas.getTitTotPrePpIasAsBuffer(buffer, position);
        position += TitTotPrePpIas.Len.TIT_TOT_PRE_PP_IAS;
        titTotCarAcq.getTitTotCarAcqAsBuffer(buffer, position);
        position += TitTotCarAcq.Len.TIT_TOT_CAR_ACQ;
        titTotCarGest.getTitTotCarGestAsBuffer(buffer, position);
        position += TitTotCarGest.Len.TIT_TOT_CAR_GEST;
        titTotCarInc.getTitTotCarIncAsBuffer(buffer, position);
        position += TitTotCarInc.Len.TIT_TOT_CAR_INC;
        titTotPreSoloRsh.getTitTotPreSoloRshAsBuffer(buffer, position);
        position += TitTotPreSoloRsh.Len.TIT_TOT_PRE_SOLO_RSH;
        titTotProvAcq1aa.getTitTotProvAcq1aaAsBuffer(buffer, position);
        position += TitTotProvAcq1aa.Len.TIT_TOT_PROV_ACQ1AA;
        titTotProvAcq2aa.getTitTotProvAcq2aaAsBuffer(buffer, position);
        position += TitTotProvAcq2aa.Len.TIT_TOT_PROV_ACQ2AA;
        titTotProvRicor.getTitTotProvRicorAsBuffer(buffer, position);
        position += TitTotProvRicor.Len.TIT_TOT_PROV_RICOR;
        titTotProvInc.getTitTotProvIncAsBuffer(buffer, position);
        position += TitTotProvInc.Len.TIT_TOT_PROV_INC;
        titTotProvDaRec.getTitTotProvDaRecAsBuffer(buffer, position);
        position += TitTotProvDaRec.Len.TIT_TOT_PROV_DA_REC;
        titImpAz.getTitImpAzAsBuffer(buffer, position);
        position += TitImpAz.Len.TIT_IMP_AZ;
        titImpAder.getTitImpAderAsBuffer(buffer, position);
        position += TitImpAder.Len.TIT_IMP_ADER;
        titImpTfr.getTitImpTfrAsBuffer(buffer, position);
        position += TitImpTfr.Len.TIT_IMP_TFR;
        titImpVolo.getTitImpVoloAsBuffer(buffer, position);
        position += TitImpVolo.Len.TIT_IMP_VOLO;
        titTotManfeeAntic.getTitTotManfeeAnticAsBuffer(buffer, position);
        position += TitTotManfeeAntic.Len.TIT_TOT_MANFEE_ANTIC;
        titTotManfeeRicor.getTitTotManfeeRicorAsBuffer(buffer, position);
        position += TitTotManfeeRicor.Len.TIT_TOT_MANFEE_RICOR;
        titTotManfeeRec.getTitTotManfeeRecAsBuffer(buffer, position);
        position += TitTotManfeeRec.Len.TIT_TOT_MANFEE_REC;
        MarshalByte.writeString(buffer, position, titTpMezPagAdd, Len.TIT_TP_MEZ_PAG_ADD);
        position += Len.TIT_TP_MEZ_PAG_ADD;
        MarshalByte.writeString(buffer, position, titEstrCntCorrAdd, Len.TIT_ESTR_CNT_CORR_ADD);
        position += Len.TIT_ESTR_CNT_CORR_ADD;
        titDtVlt.getTitDtVltAsBuffer(buffer, position);
        position += TitDtVlt.Len.TIT_DT_VLT;
        MarshalByte.writeChar(buffer, position, titFlForzDtVlt);
        position += Types.CHAR_SIZE;
        titDtCambioVlt.getTitDtCambioVltAsBuffer(buffer, position);
        position += TitDtCambioVlt.Len.TIT_DT_CAMBIO_VLT;
        titTotSpeAge.getTitTotSpeAgeAsBuffer(buffer, position);
        position += TitTotSpeAge.Len.TIT_TOT_SPE_AGE;
        titTotCarIas.getTitTotCarIasAsBuffer(buffer, position);
        position += TitTotCarIas.Len.TIT_TOT_CAR_IAS;
        titNumRatAccorpate.getTitNumRatAccorpateAsBuffer(buffer, position);
        position += TitNumRatAccorpate.Len.TIT_NUM_RAT_ACCORPATE;
        MarshalByte.writeLongAsPacked(buffer, position, titDsRiga, Len.Int.TIT_DS_RIGA, 0);
        position += Len.TIT_DS_RIGA;
        MarshalByte.writeChar(buffer, position, titDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, titDsVer, Len.Int.TIT_DS_VER, 0);
        position += Len.TIT_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, titDsTsIniCptz, Len.Int.TIT_DS_TS_INI_CPTZ, 0);
        position += Len.TIT_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, titDsTsEndCptz, Len.Int.TIT_DS_TS_END_CPTZ, 0);
        position += Len.TIT_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, titDsUtente, Len.TIT_DS_UTENTE);
        position += Len.TIT_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, titDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, titFlTitDaReinvst);
        position += Types.CHAR_SIZE;
        titDtRichAddRid.getTitDtRichAddRidAsBuffer(buffer, position);
        position += TitDtRichAddRid.Len.TIT_DT_RICH_ADD_RID;
        MarshalByte.writeString(buffer, position, titTpEsiRid, Len.TIT_TP_ESI_RID);
        position += Len.TIT_TP_ESI_RID;
        MarshalByte.writeString(buffer, position, titCodIban, Len.TIT_COD_IBAN);
        position += Len.TIT_COD_IBAN;
        titImpTrasfe.getTitImpTrasfeAsBuffer(buffer, position);
        position += TitImpTrasfe.Len.TIT_IMP_TRASFE;
        titImpTfrStrc.getTitImpTfrStrcAsBuffer(buffer, position);
        position += TitImpTfrStrc.Len.TIT_IMP_TFR_STRC;
        titDtCertFisc.getTitDtCertFiscAsBuffer(buffer, position);
        position += TitDtCertFisc.Len.TIT_DT_CERT_FISC;
        titTpCausStor.getTitTpCausStorAsBuffer(buffer, position);
        position += TitTpCausStor.Len.TIT_TP_CAUS_STOR;
        MarshalByte.writeString(buffer, position, titTpCausDispStor, Len.TIT_TP_CAUS_DISP_STOR);
        position += Len.TIT_TP_CAUS_DISP_STOR;
        MarshalByte.writeString(buffer, position, titTpTitMigraz, Len.TIT_TP_TIT_MIGRAZ);
        position += Len.TIT_TP_TIT_MIGRAZ;
        titTotAcqExp.getTitTotAcqExpAsBuffer(buffer, position);
        position += TitTotAcqExp.Len.TIT_TOT_ACQ_EXP;
        titTotRemunAss.getTitTotRemunAssAsBuffer(buffer, position);
        position += TitTotRemunAss.Len.TIT_TOT_REMUN_ASS;
        titTotCommisInter.getTitTotCommisInterAsBuffer(buffer, position);
        position += TitTotCommisInter.Len.TIT_TOT_COMMIS_INTER;
        MarshalByte.writeString(buffer, position, titTpCausRimb, Len.TIT_TP_CAUS_RIMB);
        position += Len.TIT_TP_CAUS_RIMB;
        titTotCnbtAntirac.getTitTotCnbtAntiracAsBuffer(buffer, position);
        position += TitTotCnbtAntirac.Len.TIT_TOT_CNBT_ANTIRAC;
        MarshalByte.writeChar(buffer, position, titFlIncAutogen);
        return buffer;
    }

    public void setTitIdTitCont(int titIdTitCont) {
        this.titIdTitCont = titIdTitCont;
    }

    public int getTitIdTitCont() {
        return this.titIdTitCont;
    }

    public void setTitIdOgg(int titIdOgg) {
        this.titIdOgg = titIdOgg;
    }

    public int getTitIdOgg() {
        return this.titIdOgg;
    }

    public void setTitTpOgg(String titTpOgg) {
        this.titTpOgg = Functions.subString(titTpOgg, Len.TIT_TP_OGG);
    }

    public String getTitTpOgg() {
        return this.titTpOgg;
    }

    public void setTitIbRich(String titIbRich) {
        this.titIbRich = Functions.subString(titIbRich, Len.TIT_IB_RICH);
    }

    public String getTitIbRich() {
        return this.titIbRich;
    }

    public void setTitIdMoviCrz(int titIdMoviCrz) {
        this.titIdMoviCrz = titIdMoviCrz;
    }

    public int getTitIdMoviCrz() {
        return this.titIdMoviCrz;
    }

    public void setTitDtIniEff(int titDtIniEff) {
        this.titDtIniEff = titDtIniEff;
    }

    public int getTitDtIniEff() {
        return this.titDtIniEff;
    }

    public void setTitDtEndEff(int titDtEndEff) {
        this.titDtEndEff = titDtEndEff;
    }

    public int getTitDtEndEff() {
        return this.titDtEndEff;
    }

    public void setTitCodCompAnia(int titCodCompAnia) {
        this.titCodCompAnia = titCodCompAnia;
    }

    public int getTitCodCompAnia() {
        return this.titCodCompAnia;
    }

    public void setTitTpTit(String titTpTit) {
        this.titTpTit = Functions.subString(titTpTit, Len.TIT_TP_TIT);
    }

    public String getTitTpTit() {
        return this.titTpTit;
    }

    public void setTitTpPreTit(String titTpPreTit) {
        this.titTpPreTit = Functions.subString(titTpPreTit, Len.TIT_TP_PRE_TIT);
    }

    public String getTitTpPreTit() {
        return this.titTpPreTit;
    }

    public void setTitTpStatTit(String titTpStatTit) {
        this.titTpStatTit = Functions.subString(titTpStatTit, Len.TIT_TP_STAT_TIT);
    }

    public String getTitTpStatTit() {
        return this.titTpStatTit;
    }

    public void setTitFlSoll(char titFlSoll) {
        this.titFlSoll = titFlSoll;
    }

    public char getTitFlSoll() {
        return this.titFlSoll;
    }

    public void setTitFlMora(char titFlMora) {
        this.titFlMora = titFlMora;
    }

    public char getTitFlMora() {
        return this.titFlMora;
    }

    public void setTitCodDvs(String titCodDvs) {
        this.titCodDvs = Functions.subString(titCodDvs, Len.TIT_COD_DVS);
    }

    public String getTitCodDvs() {
        return this.titCodDvs;
    }

    public String getTitCodDvsFormatted() {
        return Functions.padBlanks(getTitCodDvs(), Len.TIT_COD_DVS);
    }

    public void setTitTpMezPagAdd(String titTpMezPagAdd) {
        this.titTpMezPagAdd = Functions.subString(titTpMezPagAdd, Len.TIT_TP_MEZ_PAG_ADD);
    }

    public String getTitTpMezPagAdd() {
        return this.titTpMezPagAdd;
    }

    public String getTitTpMezPagAddFormatted() {
        return Functions.padBlanks(getTitTpMezPagAdd(), Len.TIT_TP_MEZ_PAG_ADD);
    }

    public void setTitEstrCntCorrAdd(String titEstrCntCorrAdd) {
        this.titEstrCntCorrAdd = Functions.subString(titEstrCntCorrAdd, Len.TIT_ESTR_CNT_CORR_ADD);
    }

    public String getTitEstrCntCorrAdd() {
        return this.titEstrCntCorrAdd;
    }

    public String getTitEstrCntCorrAddFormatted() {
        return Functions.padBlanks(getTitEstrCntCorrAdd(), Len.TIT_ESTR_CNT_CORR_ADD);
    }

    public void setTitFlForzDtVlt(char titFlForzDtVlt) {
        this.titFlForzDtVlt = titFlForzDtVlt;
    }

    public char getTitFlForzDtVlt() {
        return this.titFlForzDtVlt;
    }

    public void setTitDsRiga(long titDsRiga) {
        this.titDsRiga = titDsRiga;
    }

    public long getTitDsRiga() {
        return this.titDsRiga;
    }

    public void setTitDsOperSql(char titDsOperSql) {
        this.titDsOperSql = titDsOperSql;
    }

    public char getTitDsOperSql() {
        return this.titDsOperSql;
    }

    public void setTitDsVer(int titDsVer) {
        this.titDsVer = titDsVer;
    }

    public int getTitDsVer() {
        return this.titDsVer;
    }

    public void setTitDsTsIniCptz(long titDsTsIniCptz) {
        this.titDsTsIniCptz = titDsTsIniCptz;
    }

    public long getTitDsTsIniCptz() {
        return this.titDsTsIniCptz;
    }

    public void setTitDsTsEndCptz(long titDsTsEndCptz) {
        this.titDsTsEndCptz = titDsTsEndCptz;
    }

    public long getTitDsTsEndCptz() {
        return this.titDsTsEndCptz;
    }

    public void setTitDsUtente(String titDsUtente) {
        this.titDsUtente = Functions.subString(titDsUtente, Len.TIT_DS_UTENTE);
    }

    public String getTitDsUtente() {
        return this.titDsUtente;
    }

    public void setTitDsStatoElab(char titDsStatoElab) {
        this.titDsStatoElab = titDsStatoElab;
    }

    public char getTitDsStatoElab() {
        return this.titDsStatoElab;
    }

    public void setTitFlTitDaReinvst(char titFlTitDaReinvst) {
        this.titFlTitDaReinvst = titFlTitDaReinvst;
    }

    public char getTitFlTitDaReinvst() {
        return this.titFlTitDaReinvst;
    }

    public void setTitTpEsiRid(String titTpEsiRid) {
        this.titTpEsiRid = Functions.subString(titTpEsiRid, Len.TIT_TP_ESI_RID);
    }

    public String getTitTpEsiRid() {
        return this.titTpEsiRid;
    }

    public String getTitTpEsiRidFormatted() {
        return Functions.padBlanks(getTitTpEsiRid(), Len.TIT_TP_ESI_RID);
    }

    public void setTitCodIban(String titCodIban) {
        this.titCodIban = Functions.subString(titCodIban, Len.TIT_COD_IBAN);
    }

    public String getTitCodIban() {
        return this.titCodIban;
    }

    public void setTitTpCausDispStor(String titTpCausDispStor) {
        this.titTpCausDispStor = Functions.subString(titTpCausDispStor, Len.TIT_TP_CAUS_DISP_STOR);
    }

    public String getTitTpCausDispStor() {
        return this.titTpCausDispStor;
    }

    public String getTitTpCausDispStorFormatted() {
        return Functions.padBlanks(getTitTpCausDispStor(), Len.TIT_TP_CAUS_DISP_STOR);
    }

    public void setTitTpTitMigraz(String titTpTitMigraz) {
        this.titTpTitMigraz = Functions.subString(titTpTitMigraz, Len.TIT_TP_TIT_MIGRAZ);
    }

    public String getTitTpTitMigraz() {
        return this.titTpTitMigraz;
    }

    public String getTitTpTitMigrazFormatted() {
        return Functions.padBlanks(getTitTpTitMigraz(), Len.TIT_TP_TIT_MIGRAZ);
    }

    public void setTitTpCausRimb(String titTpCausRimb) {
        this.titTpCausRimb = Functions.subString(titTpCausRimb, Len.TIT_TP_CAUS_RIMB);
    }

    public String getTitTpCausRimb() {
        return this.titTpCausRimb;
    }

    public String getTitTpCausRimbFormatted() {
        return Functions.padBlanks(getTitTpCausRimb(), Len.TIT_TP_CAUS_RIMB);
    }

    public void setTitFlIncAutogen(char titFlIncAutogen) {
        this.titFlIncAutogen = titFlIncAutogen;
    }

    public char getTitFlIncAutogen() {
        return this.titFlIncAutogen;
    }

    public TitDtApplzMora getTitDtApplzMora() {
        return titDtApplzMora;
    }

    public TitDtCambioVlt getTitDtCambioVlt() {
        return titDtCambioVlt;
    }

    public TitDtCertFisc getTitDtCertFisc() {
        return titDtCertFisc;
    }

    public TitDtEmisTit getTitDtEmisTit() {
        return titDtEmisTit;
    }

    public TitDtEndCop getTitDtEndCop() {
        return titDtEndCop;
    }

    public TitDtEsiTit getTitDtEsiTit() {
        return titDtEsiTit;
    }

    public TitDtIniCop getTitDtIniCop() {
        return titDtIniCop;
    }

    public TitDtRichAddRid getTitDtRichAddRid() {
        return titDtRichAddRid;
    }

    public TitDtVlt getTitDtVlt() {
        return titDtVlt;
    }

    public TitFraz getTitFraz() {
        return titFraz;
    }

    public TitIdMoviChiu getTitIdMoviChiu() {
        return titIdMoviChiu;
    }

    public TitIdRappAna getTitIdRappAna() {
        return titIdRappAna;
    }

    public TitIdRappRete getTitIdRappRete() {
        return titIdRappRete;
    }

    public TitImpAder getTitImpAder() {
        return titImpAder;
    }

    public TitImpAz getTitImpAz() {
        return titImpAz;
    }

    public TitImpPag getTitImpPag() {
        return titImpPag;
    }

    public TitImpTfr getTitImpTfr() {
        return titImpTfr;
    }

    public TitImpTfrStrc getTitImpTfrStrc() {
        return titImpTfrStrc;
    }

    public TitImpTrasfe getTitImpTrasfe() {
        return titImpTrasfe;
    }

    public TitImpVolo getTitImpVolo() {
        return titImpVolo;
    }

    public TitNumRatAccorpate getTitNumRatAccorpate() {
        return titNumRatAccorpate;
    }

    public TitProgTit getTitProgTit() {
        return titProgTit;
    }

    public TitTotAcqExp getTitTotAcqExp() {
        return titTotAcqExp;
    }

    public TitTotCarAcq getTitTotCarAcq() {
        return titTotCarAcq;
    }

    public TitTotCarGest getTitTotCarGest() {
        return titTotCarGest;
    }

    public TitTotCarIas getTitTotCarIas() {
        return titTotCarIas;
    }

    public TitTotCarInc getTitTotCarInc() {
        return titTotCarInc;
    }

    public TitTotCnbtAntirac getTitTotCnbtAntirac() {
        return titTotCnbtAntirac;
    }

    public TitTotCommisInter getTitTotCommisInter() {
        return titTotCommisInter;
    }

    public TitTotDir getTitTotDir() {
        return titTotDir;
    }

    public TitTotIntrFraz getTitTotIntrFraz() {
        return titTotIntrFraz;
    }

    public TitTotIntrMora getTitTotIntrMora() {
        return titTotIntrMora;
    }

    public TitTotIntrPrest getTitTotIntrPrest() {
        return titTotIntrPrest;
    }

    public TitTotIntrRetdt getTitTotIntrRetdt() {
        return titTotIntrRetdt;
    }

    public TitTotIntrRiat getTitTotIntrRiat() {
        return titTotIntrRiat;
    }

    public TitTotManfeeAntic getTitTotManfeeAntic() {
        return titTotManfeeAntic;
    }

    public TitTotManfeeRec getTitTotManfeeRec() {
        return titTotManfeeRec;
    }

    public TitTotManfeeRicor getTitTotManfeeRicor() {
        return titTotManfeeRicor;
    }

    public TitTotPreNet getTitTotPreNet() {
        return titTotPreNet;
    }

    public TitTotPrePpIas getTitTotPrePpIas() {
        return titTotPrePpIas;
    }

    public TitTotPreSoloRsh getTitTotPreSoloRsh() {
        return titTotPreSoloRsh;
    }

    public TitTotPreTot getTitTotPreTot() {
        return titTotPreTot;
    }

    public TitTotProvAcq1aa getTitTotProvAcq1aa() {
        return titTotProvAcq1aa;
    }

    public TitTotProvAcq2aa getTitTotProvAcq2aa() {
        return titTotProvAcq2aa;
    }

    public TitTotProvDaRec getTitTotProvDaRec() {
        return titTotProvDaRec;
    }

    public TitTotProvInc getTitTotProvInc() {
        return titTotProvInc;
    }

    public TitTotProvRicor getTitTotProvRicor() {
        return titTotProvRicor;
    }

    public TitTotRemunAss getTitTotRemunAss() {
        return titTotRemunAss;
    }

    public TitTotSoprAlt getTitTotSoprAlt() {
        return titTotSoprAlt;
    }

    public TitTotSoprProf getTitTotSoprProf() {
        return titTotSoprProf;
    }

    public TitTotSoprSan getTitTotSoprSan() {
        return titTotSoprSan;
    }

    public TitTotSoprSpo getTitTotSoprSpo() {
        return titTotSoprSpo;
    }

    public TitTotSoprTec getTitTotSoprTec() {
        return titTotSoprTec;
    }

    public TitTotSpeAge getTitTotSpeAge() {
        return titTotSpeAge;
    }

    public TitTotSpeMed getTitTotSpeMed() {
        return titTotSpeMed;
    }

    public TitTotTax getTitTotTax() {
        return titTotTax;
    }

    public TitTpCausStor getTitTpCausStor() {
        return titTpCausStor;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TP_OGG = 2;
        public static final int TIT_IB_RICH = 40;
        public static final int TIT_TP_TIT = 2;
        public static final int TIT_TP_PRE_TIT = 2;
        public static final int TIT_TP_STAT_TIT = 2;
        public static final int TIT_COD_DVS = 20;
        public static final int TIT_TP_MEZ_PAG_ADD = 2;
        public static final int TIT_ESTR_CNT_CORR_ADD = 20;
        public static final int TIT_DS_UTENTE = 20;
        public static final int TIT_TP_ESI_RID = 2;
        public static final int TIT_COD_IBAN = 34;
        public static final int TIT_TP_CAUS_DISP_STOR = 10;
        public static final int TIT_TP_TIT_MIGRAZ = 2;
        public static final int TIT_TP_CAUS_RIMB = 2;
        public static final int TIT_ID_TIT_CONT = 5;
        public static final int TIT_ID_OGG = 5;
        public static final int TIT_ID_MOVI_CRZ = 5;
        public static final int TIT_DT_INI_EFF = 5;
        public static final int TIT_DT_END_EFF = 5;
        public static final int TIT_COD_COMP_ANIA = 3;
        public static final int TIT_FL_SOLL = 1;
        public static final int TIT_FL_MORA = 1;
        public static final int TIT_FL_FORZ_DT_VLT = 1;
        public static final int TIT_DS_RIGA = 6;
        public static final int TIT_DS_OPER_SQL = 1;
        public static final int TIT_DS_VER = 5;
        public static final int TIT_DS_TS_INI_CPTZ = 10;
        public static final int TIT_DS_TS_END_CPTZ = 10;
        public static final int TIT_DS_STATO_ELAB = 1;
        public static final int TIT_FL_TIT_DA_REINVST = 1;
        public static final int TIT_FL_INC_AUTOGEN = 1;
        public static final int TIT_CONT = TIT_ID_TIT_CONT + TIT_ID_OGG + TIT_TP_OGG + TIT_IB_RICH + TIT_ID_MOVI_CRZ + TitIdMoviChiu.Len.TIT_ID_MOVI_CHIU + TIT_DT_INI_EFF + TIT_DT_END_EFF + TIT_COD_COMP_ANIA + TIT_TP_TIT + TitProgTit.Len.TIT_PROG_TIT + TIT_TP_PRE_TIT + TIT_TP_STAT_TIT + TitDtIniCop.Len.TIT_DT_INI_COP + TitDtEndCop.Len.TIT_DT_END_COP + TitImpPag.Len.TIT_IMP_PAG + TIT_FL_SOLL + TitFraz.Len.TIT_FRAZ + TitDtApplzMora.Len.TIT_DT_APPLZ_MORA + TIT_FL_MORA + TitIdRappRete.Len.TIT_ID_RAPP_RETE + TitIdRappAna.Len.TIT_ID_RAPP_ANA + TIT_COD_DVS + TitDtEmisTit.Len.TIT_DT_EMIS_TIT + TitDtEsiTit.Len.TIT_DT_ESI_TIT + TitTotPreNet.Len.TIT_TOT_PRE_NET + TitTotIntrFraz.Len.TIT_TOT_INTR_FRAZ + TitTotIntrMora.Len.TIT_TOT_INTR_MORA + TitTotIntrPrest.Len.TIT_TOT_INTR_PREST + TitTotIntrRetdt.Len.TIT_TOT_INTR_RETDT + TitTotIntrRiat.Len.TIT_TOT_INTR_RIAT + TitTotDir.Len.TIT_TOT_DIR + TitTotSpeMed.Len.TIT_TOT_SPE_MED + TitTotTax.Len.TIT_TOT_TAX + TitTotSoprSan.Len.TIT_TOT_SOPR_SAN + TitTotSoprTec.Len.TIT_TOT_SOPR_TEC + TitTotSoprSpo.Len.TIT_TOT_SOPR_SPO + TitTotSoprProf.Len.TIT_TOT_SOPR_PROF + TitTotSoprAlt.Len.TIT_TOT_SOPR_ALT + TitTotPreTot.Len.TIT_TOT_PRE_TOT + TitTotPrePpIas.Len.TIT_TOT_PRE_PP_IAS + TitTotCarAcq.Len.TIT_TOT_CAR_ACQ + TitTotCarGest.Len.TIT_TOT_CAR_GEST + TitTotCarInc.Len.TIT_TOT_CAR_INC + TitTotPreSoloRsh.Len.TIT_TOT_PRE_SOLO_RSH + TitTotProvAcq1aa.Len.TIT_TOT_PROV_ACQ1AA + TitTotProvAcq2aa.Len.TIT_TOT_PROV_ACQ2AA + TitTotProvRicor.Len.TIT_TOT_PROV_RICOR + TitTotProvInc.Len.TIT_TOT_PROV_INC + TitTotProvDaRec.Len.TIT_TOT_PROV_DA_REC + TitImpAz.Len.TIT_IMP_AZ + TitImpAder.Len.TIT_IMP_ADER + TitImpTfr.Len.TIT_IMP_TFR + TitImpVolo.Len.TIT_IMP_VOLO + TitTotManfeeAntic.Len.TIT_TOT_MANFEE_ANTIC + TitTotManfeeRicor.Len.TIT_TOT_MANFEE_RICOR + TitTotManfeeRec.Len.TIT_TOT_MANFEE_REC + TIT_TP_MEZ_PAG_ADD + TIT_ESTR_CNT_CORR_ADD + TitDtVlt.Len.TIT_DT_VLT + TIT_FL_FORZ_DT_VLT + TitDtCambioVlt.Len.TIT_DT_CAMBIO_VLT + TitTotSpeAge.Len.TIT_TOT_SPE_AGE + TitTotCarIas.Len.TIT_TOT_CAR_IAS + TitNumRatAccorpate.Len.TIT_NUM_RAT_ACCORPATE + TIT_DS_RIGA + TIT_DS_OPER_SQL + TIT_DS_VER + TIT_DS_TS_INI_CPTZ + TIT_DS_TS_END_CPTZ + TIT_DS_UTENTE + TIT_DS_STATO_ELAB + TIT_FL_TIT_DA_REINVST + TitDtRichAddRid.Len.TIT_DT_RICH_ADD_RID + TIT_TP_ESI_RID + TIT_COD_IBAN + TitImpTrasfe.Len.TIT_IMP_TRASFE + TitImpTfrStrc.Len.TIT_IMP_TFR_STRC + TitDtCertFisc.Len.TIT_DT_CERT_FISC + TitTpCausStor.Len.TIT_TP_CAUS_STOR + TIT_TP_CAUS_DISP_STOR + TIT_TP_TIT_MIGRAZ + TitTotAcqExp.Len.TIT_TOT_ACQ_EXP + TitTotRemunAss.Len.TIT_TOT_REMUN_ASS + TitTotCommisInter.Len.TIT_TOT_COMMIS_INTER + TIT_TP_CAUS_RIMB + TitTotCnbtAntirac.Len.TIT_TOT_CNBT_ANTIRAC + TIT_FL_INC_AUTOGEN;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_ID_TIT_CONT = 9;
            public static final int TIT_ID_OGG = 9;
            public static final int TIT_ID_MOVI_CRZ = 9;
            public static final int TIT_DT_INI_EFF = 8;
            public static final int TIT_DT_END_EFF = 8;
            public static final int TIT_COD_COMP_ANIA = 5;
            public static final int TIT_DS_RIGA = 10;
            public static final int TIT_DS_VER = 9;
            public static final int TIT_DS_TS_INI_CPTZ = 18;
            public static final int TIT_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
