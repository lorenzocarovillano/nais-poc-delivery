package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.RicIdBatch;
import it.accenture.jnais.ws.redefines.RicIdJob;
import it.accenture.jnais.ws.redefines.RicIdOgg;
import it.accenture.jnais.ws.redefines.RicIdRichCollg;
import it.accenture.jnais.ws.redefines.RicTsEffEsecRich;

/**Original name: RICH<br>
 * Variable: RICH from copybook IDBVRIC1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Rich {

    //==== PROPERTIES ====
    //Original name: RIC-ID-RICH
    private int ricIdRich = DefaultValues.INT_VAL;
    //Original name: RIC-COD-COMP-ANIA
    private int ricCodCompAnia = DefaultValues.INT_VAL;
    //Original name: RIC-TP-RICH
    private String ricTpRich = DefaultValues.stringVal(Len.RIC_TP_RICH);
    //Original name: RIC-COD-MACROFUNCT
    private String ricCodMacrofunct = DefaultValues.stringVal(Len.RIC_COD_MACROFUNCT);
    //Original name: RIC-TP-MOVI
    private int ricTpMovi = DefaultValues.INT_VAL;
    //Original name: RIC-IB-RICH
    private String ricIbRich = DefaultValues.stringVal(Len.RIC_IB_RICH);
    //Original name: RIC-DT-EFF
    private int ricDtEff = DefaultValues.INT_VAL;
    //Original name: RIC-DT-RGSTRZ-RICH
    private int ricDtRgstrzRich = DefaultValues.INT_VAL;
    //Original name: RIC-DT-PERV-RICH
    private int ricDtPervRich = DefaultValues.INT_VAL;
    //Original name: RIC-DT-ESEC-RICH
    private int ricDtEsecRich = DefaultValues.INT_VAL;
    //Original name: RIC-TS-EFF-ESEC-RICH
    private RicTsEffEsecRich ricTsEffEsecRich = new RicTsEffEsecRich();
    //Original name: RIC-ID-OGG
    private RicIdOgg ricIdOgg = new RicIdOgg();
    //Original name: RIC-TP-OGG
    private String ricTpOgg = DefaultValues.stringVal(Len.RIC_TP_OGG);
    //Original name: RIC-IB-POLI
    private String ricIbPoli = DefaultValues.stringVal(Len.RIC_IB_POLI);
    //Original name: RIC-IB-ADES
    private String ricIbAdes = DefaultValues.stringVal(Len.RIC_IB_ADES);
    //Original name: RIC-IB-GAR
    private String ricIbGar = DefaultValues.stringVal(Len.RIC_IB_GAR);
    //Original name: RIC-IB-TRCH-DI-GAR
    private String ricIbTrchDiGar = DefaultValues.stringVal(Len.RIC_IB_TRCH_DI_GAR);
    //Original name: RIC-ID-BATCH
    private RicIdBatch ricIdBatch = new RicIdBatch();
    //Original name: RIC-ID-JOB
    private RicIdJob ricIdJob = new RicIdJob();
    //Original name: RIC-FL-SIMULAZIONE
    private char ricFlSimulazione = DefaultValues.CHAR_VAL;
    //Original name: RIC-KEY-ORDINAMENTO-LEN
    private short ricKeyOrdinamentoLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: RIC-KEY-ORDINAMENTO
    private String ricKeyOrdinamento = DefaultValues.stringVal(Len.RIC_KEY_ORDINAMENTO);
    //Original name: RIC-ID-RICH-COLLG
    private RicIdRichCollg ricIdRichCollg = new RicIdRichCollg();
    //Original name: RIC-TP-RAMO-BILA
    private String ricTpRamoBila = DefaultValues.stringVal(Len.RIC_TP_RAMO_BILA);
    //Original name: RIC-TP-FRM-ASSVA
    private String ricTpFrmAssva = DefaultValues.stringVal(Len.RIC_TP_FRM_ASSVA);
    //Original name: RIC-TP-CALC-RIS
    private String ricTpCalcRis = DefaultValues.stringVal(Len.RIC_TP_CALC_RIS);
    //Original name: RIC-DS-OPER-SQL
    private char ricDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: RIC-DS-VER
    private int ricDsVer = DefaultValues.INT_VAL;
    //Original name: RIC-DS-TS-CPTZ
    private long ricDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: RIC-DS-UTENTE
    private String ricDsUtente = DefaultValues.stringVal(Len.RIC_DS_UTENTE);
    //Original name: RIC-DS-STATO-ELAB
    private char ricDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: RIC-RAMO-BILA
    private String ricRamoBila = DefaultValues.stringVal(Len.RIC_RAMO_BILA);

    //==== METHODS ====
    public void setRichFormatted(String data) {
        byte[] buffer = new byte[Len.RICH];
        MarshalByte.writeString(buffer, 1, data, Len.RICH);
        setRichBytes(buffer, 1);
    }

    public String getRichFormatted() {
        return MarshalByteExt.bufferToStr(getRichBytes());
    }

    public byte[] getRichBytes() {
        byte[] buffer = new byte[Len.RICH];
        return getRichBytes(buffer, 1);
    }

    public void setRichBytes(byte[] buffer, int offset) {
        int position = offset;
        ricIdRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_ID_RICH, 0);
        position += Len.RIC_ID_RICH;
        ricCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_COD_COMP_ANIA, 0);
        position += Len.RIC_COD_COMP_ANIA;
        ricTpRich = MarshalByte.readString(buffer, position, Len.RIC_TP_RICH);
        position += Len.RIC_TP_RICH;
        ricCodMacrofunct = MarshalByte.readString(buffer, position, Len.RIC_COD_MACROFUNCT);
        position += Len.RIC_COD_MACROFUNCT;
        ricTpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_TP_MOVI, 0);
        position += Len.RIC_TP_MOVI;
        ricIbRich = MarshalByte.readString(buffer, position, Len.RIC_IB_RICH);
        position += Len.RIC_IB_RICH;
        ricDtEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_DT_EFF, 0);
        position += Len.RIC_DT_EFF;
        ricDtRgstrzRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_DT_RGSTRZ_RICH, 0);
        position += Len.RIC_DT_RGSTRZ_RICH;
        ricDtPervRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_DT_PERV_RICH, 0);
        position += Len.RIC_DT_PERV_RICH;
        ricDtEsecRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_DT_ESEC_RICH, 0);
        position += Len.RIC_DT_ESEC_RICH;
        ricTsEffEsecRich.setRicTsEffEsecRichFromBuffer(buffer, position);
        position += RicTsEffEsecRich.Len.RIC_TS_EFF_ESEC_RICH;
        ricIdOgg.setRicIdOggFromBuffer(buffer, position);
        position += RicIdOgg.Len.RIC_ID_OGG;
        ricTpOgg = MarshalByte.readString(buffer, position, Len.RIC_TP_OGG);
        position += Len.RIC_TP_OGG;
        ricIbPoli = MarshalByte.readString(buffer, position, Len.RIC_IB_POLI);
        position += Len.RIC_IB_POLI;
        ricIbAdes = MarshalByte.readString(buffer, position, Len.RIC_IB_ADES);
        position += Len.RIC_IB_ADES;
        ricIbGar = MarshalByte.readString(buffer, position, Len.RIC_IB_GAR);
        position += Len.RIC_IB_GAR;
        ricIbTrchDiGar = MarshalByte.readString(buffer, position, Len.RIC_IB_TRCH_DI_GAR);
        position += Len.RIC_IB_TRCH_DI_GAR;
        ricIdBatch.setRicIdBatchFromBuffer(buffer, position);
        position += RicIdBatch.Len.RIC_ID_BATCH;
        ricIdJob.setRicIdJobFromBuffer(buffer, position);
        position += RicIdJob.Len.RIC_ID_JOB;
        ricFlSimulazione = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setRicKeyOrdinamentoVcharBytes(buffer, position);
        position += Len.RIC_KEY_ORDINAMENTO_VCHAR;
        ricIdRichCollg.setRicIdRichCollgFromBuffer(buffer, position);
        position += RicIdRichCollg.Len.RIC_ID_RICH_COLLG;
        ricTpRamoBila = MarshalByte.readString(buffer, position, Len.RIC_TP_RAMO_BILA);
        position += Len.RIC_TP_RAMO_BILA;
        ricTpFrmAssva = MarshalByte.readString(buffer, position, Len.RIC_TP_FRM_ASSVA);
        position += Len.RIC_TP_FRM_ASSVA;
        ricTpCalcRis = MarshalByte.readString(buffer, position, Len.RIC_TP_CALC_RIS);
        position += Len.RIC_TP_CALC_RIS;
        ricDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ricDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_DS_VER, 0);
        position += Len.RIC_DS_VER;
        ricDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RIC_DS_TS_CPTZ, 0);
        position += Len.RIC_DS_TS_CPTZ;
        ricDsUtente = MarshalByte.readString(buffer, position, Len.RIC_DS_UTENTE);
        position += Len.RIC_DS_UTENTE;
        ricDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ricRamoBila = MarshalByte.readString(buffer, position, Len.RIC_RAMO_BILA);
    }

    public byte[] getRichBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ricIdRich, Len.Int.RIC_ID_RICH, 0);
        position += Len.RIC_ID_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, ricCodCompAnia, Len.Int.RIC_COD_COMP_ANIA, 0);
        position += Len.RIC_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, ricTpRich, Len.RIC_TP_RICH);
        position += Len.RIC_TP_RICH;
        MarshalByte.writeString(buffer, position, ricCodMacrofunct, Len.RIC_COD_MACROFUNCT);
        position += Len.RIC_COD_MACROFUNCT;
        MarshalByte.writeIntAsPacked(buffer, position, ricTpMovi, Len.Int.RIC_TP_MOVI, 0);
        position += Len.RIC_TP_MOVI;
        MarshalByte.writeString(buffer, position, ricIbRich, Len.RIC_IB_RICH);
        position += Len.RIC_IB_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, ricDtEff, Len.Int.RIC_DT_EFF, 0);
        position += Len.RIC_DT_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, ricDtRgstrzRich, Len.Int.RIC_DT_RGSTRZ_RICH, 0);
        position += Len.RIC_DT_RGSTRZ_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, ricDtPervRich, Len.Int.RIC_DT_PERV_RICH, 0);
        position += Len.RIC_DT_PERV_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, ricDtEsecRich, Len.Int.RIC_DT_ESEC_RICH, 0);
        position += Len.RIC_DT_ESEC_RICH;
        ricTsEffEsecRich.getRicTsEffEsecRichAsBuffer(buffer, position);
        position += RicTsEffEsecRich.Len.RIC_TS_EFF_ESEC_RICH;
        ricIdOgg.getRicIdOggAsBuffer(buffer, position);
        position += RicIdOgg.Len.RIC_ID_OGG;
        MarshalByte.writeString(buffer, position, ricTpOgg, Len.RIC_TP_OGG);
        position += Len.RIC_TP_OGG;
        MarshalByte.writeString(buffer, position, ricIbPoli, Len.RIC_IB_POLI);
        position += Len.RIC_IB_POLI;
        MarshalByte.writeString(buffer, position, ricIbAdes, Len.RIC_IB_ADES);
        position += Len.RIC_IB_ADES;
        MarshalByte.writeString(buffer, position, ricIbGar, Len.RIC_IB_GAR);
        position += Len.RIC_IB_GAR;
        MarshalByte.writeString(buffer, position, ricIbTrchDiGar, Len.RIC_IB_TRCH_DI_GAR);
        position += Len.RIC_IB_TRCH_DI_GAR;
        ricIdBatch.getRicIdBatchAsBuffer(buffer, position);
        position += RicIdBatch.Len.RIC_ID_BATCH;
        ricIdJob.getRicIdJobAsBuffer(buffer, position);
        position += RicIdJob.Len.RIC_ID_JOB;
        MarshalByte.writeChar(buffer, position, ricFlSimulazione);
        position += Types.CHAR_SIZE;
        getRicKeyOrdinamentoVcharBytes(buffer, position);
        position += Len.RIC_KEY_ORDINAMENTO_VCHAR;
        ricIdRichCollg.getRicIdRichCollgAsBuffer(buffer, position);
        position += RicIdRichCollg.Len.RIC_ID_RICH_COLLG;
        MarshalByte.writeString(buffer, position, ricTpRamoBila, Len.RIC_TP_RAMO_BILA);
        position += Len.RIC_TP_RAMO_BILA;
        MarshalByte.writeString(buffer, position, ricTpFrmAssva, Len.RIC_TP_FRM_ASSVA);
        position += Len.RIC_TP_FRM_ASSVA;
        MarshalByte.writeString(buffer, position, ricTpCalcRis, Len.RIC_TP_CALC_RIS);
        position += Len.RIC_TP_CALC_RIS;
        MarshalByte.writeChar(buffer, position, ricDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, ricDsVer, Len.Int.RIC_DS_VER, 0);
        position += Len.RIC_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, ricDsTsCptz, Len.Int.RIC_DS_TS_CPTZ, 0);
        position += Len.RIC_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, ricDsUtente, Len.RIC_DS_UTENTE);
        position += Len.RIC_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, ricDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ricRamoBila, Len.RIC_RAMO_BILA);
        return buffer;
    }

    public void setRicIdRich(int ricIdRich) {
        this.ricIdRich = ricIdRich;
    }

    public int getRicIdRich() {
        return this.ricIdRich;
    }

    public void setRicCodCompAnia(int ricCodCompAnia) {
        this.ricCodCompAnia = ricCodCompAnia;
    }

    public int getRicCodCompAnia() {
        return this.ricCodCompAnia;
    }

    public void setRicTpRich(String ricTpRich) {
        this.ricTpRich = Functions.subString(ricTpRich, Len.RIC_TP_RICH);
    }

    public String getRicTpRich() {
        return this.ricTpRich;
    }

    public void setRicCodMacrofunct(String ricCodMacrofunct) {
        this.ricCodMacrofunct = Functions.subString(ricCodMacrofunct, Len.RIC_COD_MACROFUNCT);
    }

    public String getRicCodMacrofunct() {
        return this.ricCodMacrofunct;
    }

    public void setRicTpMovi(int ricTpMovi) {
        this.ricTpMovi = ricTpMovi;
    }

    public int getRicTpMovi() {
        return this.ricTpMovi;
    }

    public void setRicIbRich(String ricIbRich) {
        this.ricIbRich = Functions.subString(ricIbRich, Len.RIC_IB_RICH);
    }

    public String getRicIbRich() {
        return this.ricIbRich;
    }

    public void setRicDtEff(int ricDtEff) {
        this.ricDtEff = ricDtEff;
    }

    public int getRicDtEff() {
        return this.ricDtEff;
    }

    public void setRicDtRgstrzRich(int ricDtRgstrzRich) {
        this.ricDtRgstrzRich = ricDtRgstrzRich;
    }

    public int getRicDtRgstrzRich() {
        return this.ricDtRgstrzRich;
    }

    public void setRicDtPervRich(int ricDtPervRich) {
        this.ricDtPervRich = ricDtPervRich;
    }

    public int getRicDtPervRich() {
        return this.ricDtPervRich;
    }

    public void setRicDtEsecRich(int ricDtEsecRich) {
        this.ricDtEsecRich = ricDtEsecRich;
    }

    public int getRicDtEsecRich() {
        return this.ricDtEsecRich;
    }

    public void setRicTpOgg(String ricTpOgg) {
        this.ricTpOgg = Functions.subString(ricTpOgg, Len.RIC_TP_OGG);
    }

    public String getRicTpOgg() {
        return this.ricTpOgg;
    }

    public String getRicTpOggFormatted() {
        return Functions.padBlanks(getRicTpOgg(), Len.RIC_TP_OGG);
    }

    public void setRicIbPoli(String ricIbPoli) {
        this.ricIbPoli = Functions.subString(ricIbPoli, Len.RIC_IB_POLI);
    }

    public String getRicIbPoli() {
        return this.ricIbPoli;
    }

    public void setRicIbAdes(String ricIbAdes) {
        this.ricIbAdes = Functions.subString(ricIbAdes, Len.RIC_IB_ADES);
    }

    public String getRicIbAdes() {
        return this.ricIbAdes;
    }

    public void setRicIbGar(String ricIbGar) {
        this.ricIbGar = Functions.subString(ricIbGar, Len.RIC_IB_GAR);
    }

    public String getRicIbGar() {
        return this.ricIbGar;
    }

    public void setRicIbTrchDiGar(String ricIbTrchDiGar) {
        this.ricIbTrchDiGar = Functions.subString(ricIbTrchDiGar, Len.RIC_IB_TRCH_DI_GAR);
    }

    public String getRicIbTrchDiGar() {
        return this.ricIbTrchDiGar;
    }

    public void setRicFlSimulazione(char ricFlSimulazione) {
        this.ricFlSimulazione = ricFlSimulazione;
    }

    public char getRicFlSimulazione() {
        return this.ricFlSimulazione;
    }

    public void setRicKeyOrdinamentoVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ricKeyOrdinamentoLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ricKeyOrdinamento = MarshalByte.readString(buffer, position, Len.RIC_KEY_ORDINAMENTO);
    }

    public byte[] getRicKeyOrdinamentoVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ricKeyOrdinamentoLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ricKeyOrdinamento, Len.RIC_KEY_ORDINAMENTO);
        return buffer;
    }

    public void setRicKeyOrdinamentoLen(short ricKeyOrdinamentoLen) {
        this.ricKeyOrdinamentoLen = ricKeyOrdinamentoLen;
    }

    public short getRicKeyOrdinamentoLen() {
        return this.ricKeyOrdinamentoLen;
    }

    public void setRicKeyOrdinamento(String ricKeyOrdinamento) {
        this.ricKeyOrdinamento = Functions.subString(ricKeyOrdinamento, Len.RIC_KEY_ORDINAMENTO);
    }

    public String getRicKeyOrdinamento() {
        return this.ricKeyOrdinamento;
    }

    public void setRicTpRamoBila(String ricTpRamoBila) {
        this.ricTpRamoBila = Functions.subString(ricTpRamoBila, Len.RIC_TP_RAMO_BILA);
    }

    public String getRicTpRamoBila() {
        return this.ricTpRamoBila;
    }

    public String getRicTpRamoBilaFormatted() {
        return Functions.padBlanks(getRicTpRamoBila(), Len.RIC_TP_RAMO_BILA);
    }

    public void setRicTpFrmAssva(String ricTpFrmAssva) {
        this.ricTpFrmAssva = Functions.subString(ricTpFrmAssva, Len.RIC_TP_FRM_ASSVA);
    }

    public String getRicTpFrmAssva() {
        return this.ricTpFrmAssva;
    }

    public String getRicTpFrmAssvaFormatted() {
        return Functions.padBlanks(getRicTpFrmAssva(), Len.RIC_TP_FRM_ASSVA);
    }

    public void setRicTpCalcRis(String ricTpCalcRis) {
        this.ricTpCalcRis = Functions.subString(ricTpCalcRis, Len.RIC_TP_CALC_RIS);
    }

    public String getRicTpCalcRis() {
        return this.ricTpCalcRis;
    }

    public String getRicTpCalcRisFormatted() {
        return Functions.padBlanks(getRicTpCalcRis(), Len.RIC_TP_CALC_RIS);
    }

    public void setRicDsOperSql(char ricDsOperSql) {
        this.ricDsOperSql = ricDsOperSql;
    }

    public char getRicDsOperSql() {
        return this.ricDsOperSql;
    }

    public void setRicDsVer(int ricDsVer) {
        this.ricDsVer = ricDsVer;
    }

    public int getRicDsVer() {
        return this.ricDsVer;
    }

    public void setRicDsTsCptz(long ricDsTsCptz) {
        this.ricDsTsCptz = ricDsTsCptz;
    }

    public long getRicDsTsCptz() {
        return this.ricDsTsCptz;
    }

    public void setRicDsUtente(String ricDsUtente) {
        this.ricDsUtente = Functions.subString(ricDsUtente, Len.RIC_DS_UTENTE);
    }

    public String getRicDsUtente() {
        return this.ricDsUtente;
    }

    public void setRicDsStatoElab(char ricDsStatoElab) {
        this.ricDsStatoElab = ricDsStatoElab;
    }

    public char getRicDsStatoElab() {
        return this.ricDsStatoElab;
    }

    public void setRicRamoBila(String ricRamoBila) {
        this.ricRamoBila = Functions.subString(ricRamoBila, Len.RIC_RAMO_BILA);
    }

    public String getRicRamoBila() {
        return this.ricRamoBila;
    }

    public String getRicRamoBilaFormatted() {
        return Functions.padBlanks(getRicRamoBila(), Len.RIC_RAMO_BILA);
    }

    public RicIdBatch getRicIdBatch() {
        return ricIdBatch;
    }

    public RicIdJob getRicIdJob() {
        return ricIdJob;
    }

    public RicIdOgg getRicIdOgg() {
        return ricIdOgg;
    }

    public RicIdRichCollg getRicIdRichCollg() {
        return ricIdRichCollg;
    }

    public RicTsEffEsecRich getRicTsEffEsecRich() {
        return ricTsEffEsecRich;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RIC_TP_RICH = 2;
        public static final int RIC_COD_MACROFUNCT = 2;
        public static final int RIC_IB_RICH = 40;
        public static final int RIC_TP_OGG = 2;
        public static final int RIC_IB_POLI = 40;
        public static final int RIC_IB_ADES = 40;
        public static final int RIC_IB_GAR = 40;
        public static final int RIC_IB_TRCH_DI_GAR = 40;
        public static final int RIC_KEY_ORDINAMENTO = 300;
        public static final int RIC_TP_RAMO_BILA = 2;
        public static final int RIC_TP_FRM_ASSVA = 2;
        public static final int RIC_TP_CALC_RIS = 2;
        public static final int RIC_DS_UTENTE = 20;
        public static final int RIC_RAMO_BILA = 12;
        public static final int RIC_ID_RICH = 5;
        public static final int RIC_COD_COMP_ANIA = 3;
        public static final int RIC_TP_MOVI = 3;
        public static final int RIC_DT_EFF = 5;
        public static final int RIC_DT_RGSTRZ_RICH = 5;
        public static final int RIC_DT_PERV_RICH = 5;
        public static final int RIC_DT_ESEC_RICH = 5;
        public static final int RIC_FL_SIMULAZIONE = 1;
        public static final int RIC_KEY_ORDINAMENTO_LEN = 2;
        public static final int RIC_KEY_ORDINAMENTO_VCHAR = RIC_KEY_ORDINAMENTO_LEN + RIC_KEY_ORDINAMENTO;
        public static final int RIC_DS_OPER_SQL = 1;
        public static final int RIC_DS_VER = 5;
        public static final int RIC_DS_TS_CPTZ = 10;
        public static final int RIC_DS_STATO_ELAB = 1;
        public static final int RICH = RIC_ID_RICH + RIC_COD_COMP_ANIA + RIC_TP_RICH + RIC_COD_MACROFUNCT + RIC_TP_MOVI + RIC_IB_RICH + RIC_DT_EFF + RIC_DT_RGSTRZ_RICH + RIC_DT_PERV_RICH + RIC_DT_ESEC_RICH + RicTsEffEsecRich.Len.RIC_TS_EFF_ESEC_RICH + RicIdOgg.Len.RIC_ID_OGG + RIC_TP_OGG + RIC_IB_POLI + RIC_IB_ADES + RIC_IB_GAR + RIC_IB_TRCH_DI_GAR + RicIdBatch.Len.RIC_ID_BATCH + RicIdJob.Len.RIC_ID_JOB + RIC_FL_SIMULAZIONE + RIC_KEY_ORDINAMENTO_VCHAR + RicIdRichCollg.Len.RIC_ID_RICH_COLLG + RIC_TP_RAMO_BILA + RIC_TP_FRM_ASSVA + RIC_TP_CALC_RIS + RIC_DS_OPER_SQL + RIC_DS_VER + RIC_DS_TS_CPTZ + RIC_DS_UTENTE + RIC_DS_STATO_ELAB + RIC_RAMO_BILA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIC_ID_RICH = 9;
            public static final int RIC_COD_COMP_ANIA = 5;
            public static final int RIC_TP_MOVI = 5;
            public static final int RIC_DT_EFF = 8;
            public static final int RIC_DT_RGSTRZ_RICH = 8;
            public static final int RIC_DT_PERV_RICH = 8;
            public static final int RIC_DT_ESEC_RICH = 8;
            public static final int RIC_DS_VER = 9;
            public static final int RIC_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
