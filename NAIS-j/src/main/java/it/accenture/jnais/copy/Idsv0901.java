package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0901<br>
 * Variable: IDSV0901 from copybook IDSV0901<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Idsv0901 {

    //==== PROPERTIES ====
    //Original name: IDSV0901-DATI-GENERICI
    private String generici = DefaultValues.stringVal(Len.GENERICI);
    //Original name: IDSV0901-DATI-FUNZ
    private String funz = DefaultValues.stringVal(Len.FUNZ);

    //==== METHODS ====
    public void setIdsv0901Formatted(String data) {
        byte[] buffer = new byte[Len.IDSV0901];
        MarshalByte.writeString(buffer, 1, data, Len.IDSV0901);
        setIdsv0901Bytes(buffer, 1);
    }

    public void setIdsv0901Bytes(byte[] buffer, int offset) {
        int position = offset;
        generici = MarshalByte.readString(buffer, position, Len.GENERICI);
        position += Len.GENERICI;
        funz = MarshalByte.readString(buffer, position, Len.FUNZ);
    }

    public void setGenerici(String generici) {
        this.generici = Functions.subString(generici, Len.GENERICI);
    }

    public String getGenerici() {
        return this.generici;
    }

    public void setFunz(String funz) {
        this.funz = Functions.subString(funz, Len.FUNZ);
    }

    public String getFunz() {
        return this.funz;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GENERICI = 100;
        public static final int FUNZ = 200;
        public static final int IDSV0901 = GENERICI + FUNZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
