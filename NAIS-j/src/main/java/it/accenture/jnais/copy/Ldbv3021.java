package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV3021<br>
 * Variable: LDBV3021 from copybook LDBV3021<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv3021 {

    //==== PROPERTIES ====
    //Original name: LDBV3021-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV3021-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV3021-TP-STAT-BUS
    private String tpStatBus = DefaultValues.stringVal(Len.TP_STAT_BUS);
    //Original name: LDBV3021-TP-CAUS-BUS1
    private String tpCausBus1 = DefaultValues.stringVal(Len.TP_CAUS_BUS1);
    //Original name: LDBV3021-TP-CAUS-BUS2
    private String tpCausBus2 = DefaultValues.stringVal(Len.TP_CAUS_BUS2);
    //Original name: LDBV3021-TP-CAUS-BUS3
    private String tpCausBus3 = DefaultValues.stringVal(Len.TP_CAUS_BUS3);
    //Original name: LDBV3021-TP-CAUS-BUS4
    private String tpCausBus4 = DefaultValues.stringVal(Len.TP_CAUS_BUS4);
    //Original name: LDBV3021-TP-CAUS-BUS5
    private String tpCausBus5 = DefaultValues.stringVal(Len.TP_CAUS_BUS5);
    //Original name: LDBV3021-TP-CAUS-BUS6
    private String tpCausBus6 = DefaultValues.stringVal(Len.TP_CAUS_BUS6);
    //Original name: LDBV3021-TP-CAUS-BUS7
    private String tpCausBus7 = DefaultValues.stringVal(Len.TP_CAUS_BUS7);
    //Original name: LDBV3021-TP-CAUS-BUS8
    private String tpCausBus8 = DefaultValues.stringVal(Len.TP_CAUS_BUS8);
    //Original name: LDBV3021-TP-CAUS-BUS9
    private String tpCausBus9 = DefaultValues.stringVal(Len.TP_CAUS_BUS9);
    //Original name: LDBV3021-TP-CAUS-BUS10
    private String tpCausBus10 = DefaultValues.stringVal(Len.TP_CAUS_BUS10);
    //Original name: LDBV3021-TP-CAUS-BUS11
    private String tpCausBus11 = DefaultValues.stringVal(Len.TP_CAUS_BUS11);
    //Original name: LDBV3021-TP-CAUS-BUS12
    private String tpCausBus12 = DefaultValues.stringVal(Len.TP_CAUS_BUS12);
    //Original name: LDBV3021-TP-CAUS-BUS13
    private String tpCausBus13 = DefaultValues.stringVal(Len.TP_CAUS_BUS13);
    //Original name: LDBV3021-TP-CAUS-BUS14
    private String tpCausBus14 = DefaultValues.stringVal(Len.TP_CAUS_BUS14);
    //Original name: LDBV3021-TP-CAUS-BUS15
    private String tpCausBus15 = DefaultValues.stringVal(Len.TP_CAUS_BUS15);
    //Original name: LDBV3021-TP-CAUS-BUS16
    private String tpCausBus16 = DefaultValues.stringVal(Len.TP_CAUS_BUS16);
    //Original name: LDBV3021-TP-CAUS-BUS17
    private String tpCausBus17 = DefaultValues.stringVal(Len.TP_CAUS_BUS17);
    //Original name: LDBV3021-TP-CAUS-BUS18
    private String tpCausBus18 = DefaultValues.stringVal(Len.TP_CAUS_BUS18);
    //Original name: LDBV3021-TP-CAUS-BUS19
    private String tpCausBus19 = DefaultValues.stringVal(Len.TP_CAUS_BUS19);
    //Original name: LDBV3021-TP-CAUS-BUS20
    private String tpCausBus20 = DefaultValues.stringVal(Len.TP_CAUS_BUS20);

    //==== METHODS ====
    public void setLdbv3021Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV3021];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV3021);
        setLdbv3021Bytes(buffer, 1);
    }

    public String getLdbv3021Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv3021Bytes());
    }

    public byte[] getLdbv3021Bytes() {
        byte[] buffer = new byte[Len.LDBV3021];
        return getLdbv3021Bytes(buffer, 1);
    }

    public void setLdbv3021Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpStatBus = MarshalByte.readString(buffer, position, Len.TP_STAT_BUS);
        position += Len.TP_STAT_BUS;
        tpCausBus1 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS1);
        position += Len.TP_CAUS_BUS1;
        tpCausBus2 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS2);
        position += Len.TP_CAUS_BUS2;
        tpCausBus3 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS3);
        position += Len.TP_CAUS_BUS3;
        tpCausBus4 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS4);
        position += Len.TP_CAUS_BUS4;
        tpCausBus5 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS5);
        position += Len.TP_CAUS_BUS5;
        tpCausBus6 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS6);
        position += Len.TP_CAUS_BUS6;
        tpCausBus7 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS7);
        position += Len.TP_CAUS_BUS7;
        tpCausBus8 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS8);
        position += Len.TP_CAUS_BUS8;
        tpCausBus9 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS9);
        position += Len.TP_CAUS_BUS9;
        tpCausBus10 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS10);
        position += Len.TP_CAUS_BUS10;
        tpCausBus11 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS11);
        position += Len.TP_CAUS_BUS11;
        tpCausBus12 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS12);
        position += Len.TP_CAUS_BUS12;
        tpCausBus13 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS13);
        position += Len.TP_CAUS_BUS13;
        tpCausBus14 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS14);
        position += Len.TP_CAUS_BUS14;
        tpCausBus15 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS15);
        position += Len.TP_CAUS_BUS15;
        tpCausBus16 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS16);
        position += Len.TP_CAUS_BUS16;
        tpCausBus17 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS17);
        position += Len.TP_CAUS_BUS17;
        tpCausBus18 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS18);
        position += Len.TP_CAUS_BUS18;
        tpCausBus19 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS19);
        position += Len.TP_CAUS_BUS19;
        tpCausBus20 = MarshalByte.readString(buffer, position, Len.TP_CAUS_BUS20);
    }

    public byte[] getLdbv3021Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tpStatBus, Len.TP_STAT_BUS);
        position += Len.TP_STAT_BUS;
        MarshalByte.writeString(buffer, position, tpCausBus1, Len.TP_CAUS_BUS1);
        position += Len.TP_CAUS_BUS1;
        MarshalByte.writeString(buffer, position, tpCausBus2, Len.TP_CAUS_BUS2);
        position += Len.TP_CAUS_BUS2;
        MarshalByte.writeString(buffer, position, tpCausBus3, Len.TP_CAUS_BUS3);
        position += Len.TP_CAUS_BUS3;
        MarshalByte.writeString(buffer, position, tpCausBus4, Len.TP_CAUS_BUS4);
        position += Len.TP_CAUS_BUS4;
        MarshalByte.writeString(buffer, position, tpCausBus5, Len.TP_CAUS_BUS5);
        position += Len.TP_CAUS_BUS5;
        MarshalByte.writeString(buffer, position, tpCausBus6, Len.TP_CAUS_BUS6);
        position += Len.TP_CAUS_BUS6;
        MarshalByte.writeString(buffer, position, tpCausBus7, Len.TP_CAUS_BUS7);
        position += Len.TP_CAUS_BUS7;
        MarshalByte.writeString(buffer, position, tpCausBus8, Len.TP_CAUS_BUS8);
        position += Len.TP_CAUS_BUS8;
        MarshalByte.writeString(buffer, position, tpCausBus9, Len.TP_CAUS_BUS9);
        position += Len.TP_CAUS_BUS9;
        MarshalByte.writeString(buffer, position, tpCausBus10, Len.TP_CAUS_BUS10);
        position += Len.TP_CAUS_BUS10;
        MarshalByte.writeString(buffer, position, tpCausBus11, Len.TP_CAUS_BUS11);
        position += Len.TP_CAUS_BUS11;
        MarshalByte.writeString(buffer, position, tpCausBus12, Len.TP_CAUS_BUS12);
        position += Len.TP_CAUS_BUS12;
        MarshalByte.writeString(buffer, position, tpCausBus13, Len.TP_CAUS_BUS13);
        position += Len.TP_CAUS_BUS13;
        MarshalByte.writeString(buffer, position, tpCausBus14, Len.TP_CAUS_BUS14);
        position += Len.TP_CAUS_BUS14;
        MarshalByte.writeString(buffer, position, tpCausBus15, Len.TP_CAUS_BUS15);
        position += Len.TP_CAUS_BUS15;
        MarshalByte.writeString(buffer, position, tpCausBus16, Len.TP_CAUS_BUS16);
        position += Len.TP_CAUS_BUS16;
        MarshalByte.writeString(buffer, position, tpCausBus17, Len.TP_CAUS_BUS17);
        position += Len.TP_CAUS_BUS17;
        MarshalByte.writeString(buffer, position, tpCausBus18, Len.TP_CAUS_BUS18);
        position += Len.TP_CAUS_BUS18;
        MarshalByte.writeString(buffer, position, tpCausBus19, Len.TP_CAUS_BUS19);
        position += Len.TP_CAUS_BUS19;
        MarshalByte.writeString(buffer, position, tpCausBus20, Len.TP_CAUS_BUS20);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpStatBus(String tpStatBus) {
        this.tpStatBus = Functions.subString(tpStatBus, Len.TP_STAT_BUS);
    }

    public String getTpStatBus() {
        return this.tpStatBus;
    }

    public void setTpCausBus1(String tpCausBus1) {
        this.tpCausBus1 = Functions.subString(tpCausBus1, Len.TP_CAUS_BUS1);
    }

    public String getTpCausBus1() {
        return this.tpCausBus1;
    }

    public void setTpCausBus2(String tpCausBus2) {
        this.tpCausBus2 = Functions.subString(tpCausBus2, Len.TP_CAUS_BUS2);
    }

    public String getTpCausBus2() {
        return this.tpCausBus2;
    }

    public void setTpCausBus3(String tpCausBus3) {
        this.tpCausBus3 = Functions.subString(tpCausBus3, Len.TP_CAUS_BUS3);
    }

    public String getTpCausBus3() {
        return this.tpCausBus3;
    }

    public void setTpCausBus4(String tpCausBus4) {
        this.tpCausBus4 = Functions.subString(tpCausBus4, Len.TP_CAUS_BUS4);
    }

    public String getTpCausBus4() {
        return this.tpCausBus4;
    }

    public void setTpCausBus5(String tpCausBus5) {
        this.tpCausBus5 = Functions.subString(tpCausBus5, Len.TP_CAUS_BUS5);
    }

    public String getTpCausBus5() {
        return this.tpCausBus5;
    }

    public void setTpCausBus6(String tpCausBus6) {
        this.tpCausBus6 = Functions.subString(tpCausBus6, Len.TP_CAUS_BUS6);
    }

    public String getTpCausBus6() {
        return this.tpCausBus6;
    }

    public void setTpCausBus7(String tpCausBus7) {
        this.tpCausBus7 = Functions.subString(tpCausBus7, Len.TP_CAUS_BUS7);
    }

    public String getTpCausBus7() {
        return this.tpCausBus7;
    }

    public void setTpCausBus8(String tpCausBus8) {
        this.tpCausBus8 = Functions.subString(tpCausBus8, Len.TP_CAUS_BUS8);
    }

    public String getTpCausBus8() {
        return this.tpCausBus8;
    }

    public void setTpCausBus9(String tpCausBus9) {
        this.tpCausBus9 = Functions.subString(tpCausBus9, Len.TP_CAUS_BUS9);
    }

    public String getTpCausBus9() {
        return this.tpCausBus9;
    }

    public void setTpCausBus10(String tpCausBus10) {
        this.tpCausBus10 = Functions.subString(tpCausBus10, Len.TP_CAUS_BUS10);
    }

    public String getTpCausBus10() {
        return this.tpCausBus10;
    }

    public void setTpCausBus11(String tpCausBus11) {
        this.tpCausBus11 = Functions.subString(tpCausBus11, Len.TP_CAUS_BUS11);
    }

    public String getTpCausBus11() {
        return this.tpCausBus11;
    }

    public void setTpCausBus12(String tpCausBus12) {
        this.tpCausBus12 = Functions.subString(tpCausBus12, Len.TP_CAUS_BUS12);
    }

    public String getTpCausBus12() {
        return this.tpCausBus12;
    }

    public void setTpCausBus13(String tpCausBus13) {
        this.tpCausBus13 = Functions.subString(tpCausBus13, Len.TP_CAUS_BUS13);
    }

    public String getTpCausBus13() {
        return this.tpCausBus13;
    }

    public void setTpCausBus14(String tpCausBus14) {
        this.tpCausBus14 = Functions.subString(tpCausBus14, Len.TP_CAUS_BUS14);
    }

    public String getTpCausBus14() {
        return this.tpCausBus14;
    }

    public void setTpCausBus15(String tpCausBus15) {
        this.tpCausBus15 = Functions.subString(tpCausBus15, Len.TP_CAUS_BUS15);
    }

    public String getTpCausBus15() {
        return this.tpCausBus15;
    }

    public void setTpCausBus16(String tpCausBus16) {
        this.tpCausBus16 = Functions.subString(tpCausBus16, Len.TP_CAUS_BUS16);
    }

    public String getTpCausBus16() {
        return this.tpCausBus16;
    }

    public void setTpCausBus17(String tpCausBus17) {
        this.tpCausBus17 = Functions.subString(tpCausBus17, Len.TP_CAUS_BUS17);
    }

    public String getTpCausBus17() {
        return this.tpCausBus17;
    }

    public void setTpCausBus18(String tpCausBus18) {
        this.tpCausBus18 = Functions.subString(tpCausBus18, Len.TP_CAUS_BUS18);
    }

    public String getTpCausBus18() {
        return this.tpCausBus18;
    }

    public void setTpCausBus19(String tpCausBus19) {
        this.tpCausBus19 = Functions.subString(tpCausBus19, Len.TP_CAUS_BUS19);
    }

    public String getTpCausBus19() {
        return this.tpCausBus19;
    }

    public void setTpCausBus20(String tpCausBus20) {
        this.tpCausBus20 = Functions.subString(tpCausBus20, Len.TP_CAUS_BUS20);
    }

    public String getTpCausBus20() {
        return this.tpCausBus20;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int TP_STAT_BUS = 2;
        public static final int TP_CAUS_BUS1 = 2;
        public static final int TP_CAUS_BUS2 = 2;
        public static final int TP_CAUS_BUS3 = 2;
        public static final int TP_CAUS_BUS4 = 2;
        public static final int TP_CAUS_BUS5 = 2;
        public static final int TP_CAUS_BUS6 = 2;
        public static final int TP_CAUS_BUS7 = 2;
        public static final int TP_CAUS_BUS8 = 2;
        public static final int TP_CAUS_BUS9 = 2;
        public static final int TP_CAUS_BUS10 = 2;
        public static final int TP_CAUS_BUS11 = 2;
        public static final int TP_CAUS_BUS12 = 2;
        public static final int TP_CAUS_BUS13 = 2;
        public static final int TP_CAUS_BUS14 = 2;
        public static final int TP_CAUS_BUS15 = 2;
        public static final int TP_CAUS_BUS16 = 2;
        public static final int TP_CAUS_BUS17 = 2;
        public static final int TP_CAUS_BUS18 = 2;
        public static final int TP_CAUS_BUS19 = 2;
        public static final int TP_CAUS_BUS20 = 2;
        public static final int ID_OGG = 5;
        public static final int LDBV3021 = ID_OGG + TP_OGG + TP_STAT_BUS + TP_CAUS_BUS1 + TP_CAUS_BUS2 + TP_CAUS_BUS3 + TP_CAUS_BUS4 + TP_CAUS_BUS5 + TP_CAUS_BUS6 + TP_CAUS_BUS7 + TP_CAUS_BUS8 + TP_CAUS_BUS9 + TP_CAUS_BUS10 + TP_CAUS_BUS11 + TP_CAUS_BUS12 + TP_CAUS_BUS13 + TP_CAUS_BUS14 + TP_CAUS_BUS15 + TP_CAUS_BUS16 + TP_CAUS_BUS17 + TP_CAUS_BUS18 + TP_CAUS_BUS19 + TP_CAUS_BUS20;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
