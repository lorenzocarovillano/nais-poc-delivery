package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV5571<br>
 * Variable: LDBV5571 from copybook LDBV5571<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbv5571 {

    //==== PROPERTIES ====
    //Original name: LDBV5571-ID-OGG-DER
    private int ldbv5571IdOggDer = DefaultValues.INT_VAL;
    //Original name: LDBV5571-TP-OGG-DER
    private String ldbv5571TpOggDer = DefaultValues.stringVal(Len.LDBV5571_TP_OGG_DER);
    //Original name: LDBV5571-TP-COLL-1
    private String ldbv5571TpColl1 = DefaultValues.stringVal(Len.LDBV5571_TP_COLL1);
    //Original name: LDBV5571-TP-COLL-2
    private String ldbv5571TpColl2 = DefaultValues.stringVal(Len.LDBV5571_TP_COLL2);
    //Original name: LDBV5571-TP-COLL-3
    private String ldbv5571TpColl3 = DefaultValues.stringVal(Len.LDBV5571_TP_COLL3);
    //Original name: LDBV5571-TOT-IMP-COLL
    private AfDecimal ldbv5571TotImpColl = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    public void setLdbv5571Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV5571];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV5571);
        setLdbv5571Bytes(buffer, 1);
    }

    public String getLdbv5571Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv5571Bytes());
    }

    /**Original name: LDBV5571<br>
	 * <pre>--  Interfaccia LDBS5570 - Sommatoria Imp_collegato dalla tab.
	 * --  Oggetto collegato</pre>*/
    public byte[] getLdbv5571Bytes() {
        byte[] buffer = new byte[Len.LDBV5571];
        return getLdbv5571Bytes(buffer, 1);
    }

    public void setLdbv5571Bytes(byte[] buffer, int offset) {
        int position = offset;
        setLdbv5571InputBytes(buffer, position);
        position += Len.LDBV5571_INPUT;
        setLdbv5571OutputBytes(buffer, position);
    }

    public byte[] getLdbv5571Bytes(byte[] buffer, int offset) {
        int position = offset;
        getLdbv5571InputBytes(buffer, position);
        position += Len.LDBV5571_INPUT;
        getLdbv5571OutputBytes(buffer, position);
        return buffer;
    }

    public void setLdbv5571InputBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv5571IdOggDer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV5571_ID_OGG_DER, 0);
        position += Len.LDBV5571_ID_OGG_DER;
        ldbv5571TpOggDer = MarshalByte.readString(buffer, position, Len.LDBV5571_TP_OGG_DER);
        position += Len.LDBV5571_TP_OGG_DER;
        ldbv5571TpColl1 = MarshalByte.readString(buffer, position, Len.LDBV5571_TP_COLL1);
        position += Len.LDBV5571_TP_COLL1;
        ldbv5571TpColl2 = MarshalByte.readString(buffer, position, Len.LDBV5571_TP_COLL2);
        position += Len.LDBV5571_TP_COLL2;
        ldbv5571TpColl3 = MarshalByte.readString(buffer, position, Len.LDBV5571_TP_COLL3);
    }

    public byte[] getLdbv5571InputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv5571IdOggDer, Len.Int.LDBV5571_ID_OGG_DER, 0);
        position += Len.LDBV5571_ID_OGG_DER;
        MarshalByte.writeString(buffer, position, ldbv5571TpOggDer, Len.LDBV5571_TP_OGG_DER);
        position += Len.LDBV5571_TP_OGG_DER;
        MarshalByte.writeString(buffer, position, ldbv5571TpColl1, Len.LDBV5571_TP_COLL1);
        position += Len.LDBV5571_TP_COLL1;
        MarshalByte.writeString(buffer, position, ldbv5571TpColl2, Len.LDBV5571_TP_COLL2);
        position += Len.LDBV5571_TP_COLL2;
        MarshalByte.writeString(buffer, position, ldbv5571TpColl3, Len.LDBV5571_TP_COLL3);
        return buffer;
    }

    public void setLdbv5571IdOggDer(int ldbv5571IdOggDer) {
        this.ldbv5571IdOggDer = ldbv5571IdOggDer;
    }

    public int getLdbv5571IdOggDer() {
        return this.ldbv5571IdOggDer;
    }

    public void setLdbv5571TpOggDer(String ldbv5571TpOggDer) {
        this.ldbv5571TpOggDer = Functions.subString(ldbv5571TpOggDer, Len.LDBV5571_TP_OGG_DER);
    }

    public String getLdbv5571TpOggDer() {
        return this.ldbv5571TpOggDer;
    }

    public void setLdbv5571TpColl1(String ldbv5571TpColl1) {
        this.ldbv5571TpColl1 = Functions.subString(ldbv5571TpColl1, Len.LDBV5571_TP_COLL1);
    }

    public String getLdbv5571TpColl1() {
        return this.ldbv5571TpColl1;
    }

    public void setLdbv5571TpColl2(String ldbv5571TpColl2) {
        this.ldbv5571TpColl2 = Functions.subString(ldbv5571TpColl2, Len.LDBV5571_TP_COLL2);
    }

    public String getLdbv5571TpColl2() {
        return this.ldbv5571TpColl2;
    }

    public void setLdbv5571TpColl3(String ldbv5571TpColl3) {
        this.ldbv5571TpColl3 = Functions.subString(ldbv5571TpColl3, Len.LDBV5571_TP_COLL3);
    }

    public String getLdbv5571TpColl3() {
        return this.ldbv5571TpColl3;
    }

    public void setLdbv5571OutputBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv5571TotImpColl.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LDBV5571_TOT_IMP_COLL, Len.Fract.LDBV5571_TOT_IMP_COLL));
    }

    public byte[] getLdbv5571OutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimalAsPacked(buffer, position, ldbv5571TotImpColl.copy());
        return buffer;
    }

    public void setLdbv5571TotImpColl(AfDecimal ldbv5571TotImpColl) {
        this.ldbv5571TotImpColl.assign(ldbv5571TotImpColl);
    }

    public AfDecimal getLdbv5571TotImpColl() {
        return this.ldbv5571TotImpColl.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV5571_TP_OGG_DER = 2;
        public static final int LDBV5571_TP_COLL1 = 2;
        public static final int LDBV5571_TP_COLL2 = 2;
        public static final int LDBV5571_TP_COLL3 = 2;
        public static final int LDBV5571_ID_OGG_DER = 5;
        public static final int LDBV5571_INPUT = LDBV5571_ID_OGG_DER + LDBV5571_TP_OGG_DER + LDBV5571_TP_COLL1 + LDBV5571_TP_COLL2 + LDBV5571_TP_COLL3;
        public static final int LDBV5571_TOT_IMP_COLL = 8;
        public static final int LDBV5571_OUTPUT = LDBV5571_TOT_IMP_COLL;
        public static final int LDBV5571 = LDBV5571_INPUT + LDBV5571_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV5571_ID_OGG_DER = 9;
            public static final int LDBV5571_TOT_IMP_COLL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LDBV5571_TOT_IMP_COLL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
