package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IDSV0016-DATA<br>
 * Variable: IDSV0016-DATA from copybook IDSV0016<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Idsv0016Data {

    //==== PROPERTIES ====
    //Original name: IDSV0016-AAAA
    private String aaaa = DefaultValues.stringVal(Len.AAAA);
    //Original name: IDSV0016-MM
    private short mm = DefaultValues.SHORT_VAL;
    //Original name: IDSV0016-GG
    private String gg = DefaultValues.stringVal(Len.GG);

    //==== METHODS ====
    public void setAaaaFormatted(String aaaa) {
        this.aaaa = Trunc.toUnsignedNumeric(aaaa, Len.AAAA);
    }

    public short getAaaa() {
        return NumericDisplay.asShort(this.aaaa);
    }

    public String getAaaaFormatted() {
        return this.aaaa;
    }

    public void setMm(short mm) {
        this.mm = mm;
    }

    public void setMmFormatted(String mm) {
        setMm(PicParser.display("9(2)").parseShort(mm));
    }

    public short getMm() {
        return this.mm;
    }

    public void setGgFormatted(String gg) {
        this.gg = Trunc.toUnsignedNumeric(gg, Len.GG);
    }

    public short getGg() {
        return NumericDisplay.asShort(this.gg);
    }

    public String getGgFormatted() {
        return this.gg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA = 4;
        public static final int GG = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
