package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADES-DB<br>
 * Variable: ADES-DB from copybook IDBVADE3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AdesDb {

    //==== PROPERTIES ====
    //Original name: ADE-DT-INI-EFF-DB
    private String iniEffDb = DefaultValues.stringVal(Len.INI_EFF_DB);
    //Original name: ADE-DT-END-EFF-DB
    private String endEffDb = DefaultValues.stringVal(Len.END_EFF_DB);
    //Original name: ADE-DT-DECOR-DB
    private String decorDb = DefaultValues.stringVal(Len.DECOR_DB);
    //Original name: ADE-DT-SCAD-DB
    private String scadDb = DefaultValues.stringVal(Len.SCAD_DB);
    //Original name: ADE-DT-VARZ-TP-IAS-DB
    private String varzTpIasDb = DefaultValues.stringVal(Len.VARZ_TP_IAS_DB);
    //Original name: ADE-DT-NOVA-RGM-FISC-DB
    private String novaRgmFiscDb = DefaultValues.stringVal(Len.NOVA_RGM_FISC_DB);
    //Original name: ADE-DT-DECOR-PREST-BAN-DB
    private String decorPrestBanDb = DefaultValues.stringVal(Len.DECOR_PREST_BAN_DB);
    //Original name: ADE-DT-EFF-VARZ-STAT-T-DB
    private String effVarzStatTDb = DefaultValues.stringVal(Len.EFF_VARZ_STAT_T_DB);
    //Original name: ADE-DT-ULT-CONS-CNBT-DB
    private String ultConsCnbtDb = DefaultValues.stringVal(Len.ULT_CONS_CNBT_DB);
    //Original name: ADE-DT-PRESC-DB
    private String prescDb = DefaultValues.stringVal(Len.PRESC_DB);

    //==== METHODS ====
    public void setIniEffDb(String iniEffDb) {
        this.iniEffDb = Functions.subString(iniEffDb, Len.INI_EFF_DB);
    }

    public String getIniEffDb() {
        return this.iniEffDb;
    }

    public void setEndEffDb(String endEffDb) {
        this.endEffDb = Functions.subString(endEffDb, Len.END_EFF_DB);
    }

    public String getEndEffDb() {
        return this.endEffDb;
    }

    public void setDecorDb(String decorDb) {
        this.decorDb = Functions.subString(decorDb, Len.DECOR_DB);
    }

    public String getDecorDb() {
        return this.decorDb;
    }

    public void setScadDb(String scadDb) {
        this.scadDb = Functions.subString(scadDb, Len.SCAD_DB);
    }

    public String getScadDb() {
        return this.scadDb;
    }

    public void setVarzTpIasDb(String varzTpIasDb) {
        this.varzTpIasDb = Functions.subString(varzTpIasDb, Len.VARZ_TP_IAS_DB);
    }

    public String getVarzTpIasDb() {
        return this.varzTpIasDb;
    }

    public void setNovaRgmFiscDb(String novaRgmFiscDb) {
        this.novaRgmFiscDb = Functions.subString(novaRgmFiscDb, Len.NOVA_RGM_FISC_DB);
    }

    public String getNovaRgmFiscDb() {
        return this.novaRgmFiscDb;
    }

    public void setDecorPrestBanDb(String decorPrestBanDb) {
        this.decorPrestBanDb = Functions.subString(decorPrestBanDb, Len.DECOR_PREST_BAN_DB);
    }

    public String getDecorPrestBanDb() {
        return this.decorPrestBanDb;
    }

    public void setEffVarzStatTDb(String effVarzStatTDb) {
        this.effVarzStatTDb = Functions.subString(effVarzStatTDb, Len.EFF_VARZ_STAT_T_DB);
    }

    public String getEffVarzStatTDb() {
        return this.effVarzStatTDb;
    }

    public void setUltConsCnbtDb(String ultConsCnbtDb) {
        this.ultConsCnbtDb = Functions.subString(ultConsCnbtDb, Len.ULT_CONS_CNBT_DB);
    }

    public String getUltConsCnbtDb() {
        return this.ultConsCnbtDb;
    }

    public void setPrescDb(String prescDb) {
        this.prescDb = Functions.subString(prescDb, Len.PRESC_DB);
    }

    public String getPrescDb() {
        return this.prescDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INI_EFF_DB = 10;
        public static final int END_EFF_DB = 10;
        public static final int DECOR_DB = 10;
        public static final int SCAD_DB = 10;
        public static final int VARZ_TP_IAS_DB = 10;
        public static final int NOVA_RGM_FISC_DB = 10;
        public static final int DECOR_PREST_BAN_DB = 10;
        public static final int EFF_VARZ_STAT_T_DB = 10;
        public static final int ULT_CONS_CNBT_DB = 10;
        public static final int PRESC_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
