package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.redefines.WkTabVar;

/**Original name: LCCC0211<br>
 * Copybook: LCCC0211 from copybook LCCC0211<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccc0211 {

    //==== PROPERTIES ====
    /**Original name: WK-ELE-LIVELLO-MAX<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA INPUT VARIABILI
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA
	 *    LUNG.
	 * ----------------------------------------------------------------*
	 *  03 AREA-TAB-VARIABILI.</pre>*/
    private short wkEleLivelloMax = DefaultValues.SHORT_VAL;
    //Original name: WK-TAB-VAR
    private WkTabVar wkTabVar = new WkTabVar();

    //==== METHODS ====
    public void setWkEleLivelloMax(short wkEleLivelloMax) {
        this.wkEleLivelloMax = wkEleLivelloMax;
    }

    public short getWkEleLivelloMax() {
        return this.wkEleLivelloMax;
    }

    public WkTabVar getWkTabVar() {
        return wkTabVar;
    }
}
