package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.ws.redefines.WtliComponTaxRimb;
import it.accenture.jnais.ws.redefines.WtliIasMggSin;
import it.accenture.jnais.ws.redefines.WtliIasOnerPrvntFin;
import it.accenture.jnais.ws.redefines.WtliIasPnl;
import it.accenture.jnais.ws.redefines.WtliIasRstDpst;
import it.accenture.jnais.ws.redefines.WtliIdGarLiq;
import it.accenture.jnais.ws.redefines.WtliIdMoviChiu;
import it.accenture.jnais.ws.redefines.WtliImpLrdDfz;
import it.accenture.jnais.ws.redefines.WtliImpNetDfz;
import it.accenture.jnais.ws.redefines.WtliImpRimb;
import it.accenture.jnais.ws.redefines.WtliImpUti;
import it.accenture.jnais.ws.redefines.WtliRisMat;
import it.accenture.jnais.ws.redefines.WtliRisSpe;

/**Original name: WTLI-DATI<br>
 * Variable: WTLI-DATI from copybook LCCVTLI1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WtliDati implements ICopyable<WtliDati> {

    //==== PROPERTIES ====
    //Original name: WTLI-ID-TRCH-LIQ
    private int wtliIdTrchLiq = DefaultValues.INT_VAL;
    //Original name: WTLI-ID-GAR-LIQ
    private WtliIdGarLiq wtliIdGarLiq = new WtliIdGarLiq();
    //Original name: WTLI-ID-LIQ
    private int wtliIdLiq = DefaultValues.INT_VAL;
    //Original name: WTLI-ID-TRCH-DI-GAR
    private int wtliIdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: WTLI-ID-MOVI-CRZ
    private int wtliIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WTLI-ID-MOVI-CHIU
    private WtliIdMoviChiu wtliIdMoviChiu = new WtliIdMoviChiu();
    //Original name: WTLI-DT-INI-EFF
    private int wtliDtIniEff = DefaultValues.INT_VAL;
    //Original name: WTLI-DT-END-EFF
    private int wtliDtEndEff = DefaultValues.INT_VAL;
    //Original name: WTLI-COD-COMP-ANIA
    private int wtliCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WTLI-IMP-LRD-CALC
    private AfDecimal wtliImpLrdCalc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTLI-IMP-LRD-DFZ
    private WtliImpLrdDfz wtliImpLrdDfz = new WtliImpLrdDfz();
    //Original name: WTLI-IMP-LRD-EFFLQ
    private AfDecimal wtliImpLrdEfflq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTLI-IMP-NET-CALC
    private AfDecimal wtliImpNetCalc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTLI-IMP-NET-DFZ
    private WtliImpNetDfz wtliImpNetDfz = new WtliImpNetDfz();
    //Original name: WTLI-IMP-NET-EFFLQ
    private AfDecimal wtliImpNetEfflq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTLI-IMP-UTI
    private WtliImpUti wtliImpUti = new WtliImpUti();
    //Original name: WTLI-COD-TARI
    private String wtliCodTari = DefaultValues.stringVal(Len.WTLI_COD_TARI);
    //Original name: WTLI-COD-DVS
    private String wtliCodDvs = DefaultValues.stringVal(Len.WTLI_COD_DVS);
    //Original name: WTLI-IAS-ONER-PRVNT-FIN
    private WtliIasOnerPrvntFin wtliIasOnerPrvntFin = new WtliIasOnerPrvntFin();
    //Original name: WTLI-IAS-MGG-SIN
    private WtliIasMggSin wtliIasMggSin = new WtliIasMggSin();
    //Original name: WTLI-IAS-RST-DPST
    private WtliIasRstDpst wtliIasRstDpst = new WtliIasRstDpst();
    //Original name: WTLI-IMP-RIMB
    private WtliImpRimb wtliImpRimb = new WtliImpRimb();
    //Original name: WTLI-COMPON-TAX-RIMB
    private WtliComponTaxRimb wtliComponTaxRimb = new WtliComponTaxRimb();
    //Original name: WTLI-RIS-MAT
    private WtliRisMat wtliRisMat = new WtliRisMat();
    //Original name: WTLI-RIS-SPE
    private WtliRisSpe wtliRisSpe = new WtliRisSpe();
    //Original name: WTLI-DS-RIGA
    private long wtliDsRiga = DefaultValues.LONG_VAL;
    //Original name: WTLI-DS-OPER-SQL
    private char wtliDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WTLI-DS-VER
    private int wtliDsVer = DefaultValues.INT_VAL;
    //Original name: WTLI-DS-TS-INI-CPTZ
    private long wtliDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WTLI-DS-TS-END-CPTZ
    private long wtliDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WTLI-DS-UTENTE
    private String wtliDsUtente = DefaultValues.stringVal(Len.WTLI_DS_UTENTE);
    //Original name: WTLI-DS-STATO-ELAB
    private char wtliDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WTLI-IAS-PNL
    private WtliIasPnl wtliIasPnl = new WtliIasPnl();

    //==== CONSTRUCTORS ====
    public WtliDati() {
    }

    public WtliDati(WtliDati dati) {
        this();
        this.wtliIdTrchLiq = dati.wtliIdTrchLiq;
        this.wtliIdGarLiq = ((WtliIdGarLiq)dati.wtliIdGarLiq.copy());
        this.wtliIdLiq = dati.wtliIdLiq;
        this.wtliIdTrchDiGar = dati.wtliIdTrchDiGar;
        this.wtliIdMoviCrz = dati.wtliIdMoviCrz;
        this.wtliIdMoviChiu = ((WtliIdMoviChiu)dati.wtliIdMoviChiu.copy());
        this.wtliDtIniEff = dati.wtliDtIniEff;
        this.wtliDtEndEff = dati.wtliDtEndEff;
        this.wtliCodCompAnia = dati.wtliCodCompAnia;
        this.wtliImpLrdCalc.assign(dati.wtliImpLrdCalc);
        this.wtliImpLrdDfz = ((WtliImpLrdDfz)dati.wtliImpLrdDfz.copy());
        this.wtliImpLrdEfflq.assign(dati.wtliImpLrdEfflq);
        this.wtliImpNetCalc.assign(dati.wtliImpNetCalc);
        this.wtliImpNetDfz = ((WtliImpNetDfz)dati.wtliImpNetDfz.copy());
        this.wtliImpNetEfflq.assign(dati.wtliImpNetEfflq);
        this.wtliImpUti = ((WtliImpUti)dati.wtliImpUti.copy());
        this.wtliCodTari = dati.wtliCodTari;
        this.wtliCodDvs = dati.wtliCodDvs;
        this.wtliIasOnerPrvntFin = ((WtliIasOnerPrvntFin)dati.wtliIasOnerPrvntFin.copy());
        this.wtliIasMggSin = ((WtliIasMggSin)dati.wtliIasMggSin.copy());
        this.wtliIasRstDpst = ((WtliIasRstDpst)dati.wtliIasRstDpst.copy());
        this.wtliImpRimb = ((WtliImpRimb)dati.wtliImpRimb.copy());
        this.wtliComponTaxRimb = ((WtliComponTaxRimb)dati.wtliComponTaxRimb.copy());
        this.wtliRisMat = ((WtliRisMat)dati.wtliRisMat.copy());
        this.wtliRisSpe = ((WtliRisSpe)dati.wtliRisSpe.copy());
        this.wtliDsRiga = dati.wtliDsRiga;
        this.wtliDsOperSql = dati.wtliDsOperSql;
        this.wtliDsVer = dati.wtliDsVer;
        this.wtliDsTsIniCptz = dati.wtliDsTsIniCptz;
        this.wtliDsTsEndCptz = dati.wtliDsTsEndCptz;
        this.wtliDsUtente = dati.wtliDsUtente;
        this.wtliDsStatoElab = dati.wtliDsStatoElab;
        this.wtliIasPnl = ((WtliIasPnl)dati.wtliIasPnl.copy());
    }

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wtliIdTrchLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTLI_ID_TRCH_LIQ, 0);
        position += Len.WTLI_ID_TRCH_LIQ;
        wtliIdGarLiq.setWtliIdGarLiqFromBuffer(buffer, position);
        position += WtliIdGarLiq.Len.WTLI_ID_GAR_LIQ;
        wtliIdLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTLI_ID_LIQ, 0);
        position += Len.WTLI_ID_LIQ;
        wtliIdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTLI_ID_TRCH_DI_GAR, 0);
        position += Len.WTLI_ID_TRCH_DI_GAR;
        wtliIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTLI_ID_MOVI_CRZ, 0);
        position += Len.WTLI_ID_MOVI_CRZ;
        wtliIdMoviChiu.setWtliIdMoviChiuFromBuffer(buffer, position);
        position += WtliIdMoviChiu.Len.WTLI_ID_MOVI_CHIU;
        wtliDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTLI_DT_INI_EFF, 0);
        position += Len.WTLI_DT_INI_EFF;
        wtliDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTLI_DT_END_EFF, 0);
        position += Len.WTLI_DT_END_EFF;
        wtliCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTLI_COD_COMP_ANIA, 0);
        position += Len.WTLI_COD_COMP_ANIA;
        wtliImpLrdCalc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WTLI_IMP_LRD_CALC, Len.Fract.WTLI_IMP_LRD_CALC));
        position += Len.WTLI_IMP_LRD_CALC;
        wtliImpLrdDfz.setWtliImpLrdDfzFromBuffer(buffer, position);
        position += WtliImpLrdDfz.Len.WTLI_IMP_LRD_DFZ;
        wtliImpLrdEfflq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WTLI_IMP_LRD_EFFLQ, Len.Fract.WTLI_IMP_LRD_EFFLQ));
        position += Len.WTLI_IMP_LRD_EFFLQ;
        wtliImpNetCalc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WTLI_IMP_NET_CALC, Len.Fract.WTLI_IMP_NET_CALC));
        position += Len.WTLI_IMP_NET_CALC;
        wtliImpNetDfz.setWtliImpNetDfzFromBuffer(buffer, position);
        position += WtliImpNetDfz.Len.WTLI_IMP_NET_DFZ;
        wtliImpNetEfflq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WTLI_IMP_NET_EFFLQ, Len.Fract.WTLI_IMP_NET_EFFLQ));
        position += Len.WTLI_IMP_NET_EFFLQ;
        wtliImpUti.setWtliImpUtiFromBuffer(buffer, position);
        position += WtliImpUti.Len.WTLI_IMP_UTI;
        wtliCodTari = MarshalByte.readString(buffer, position, Len.WTLI_COD_TARI);
        position += Len.WTLI_COD_TARI;
        wtliCodDvs = MarshalByte.readString(buffer, position, Len.WTLI_COD_DVS);
        position += Len.WTLI_COD_DVS;
        wtliIasOnerPrvntFin.setWtliIasOnerPrvntFinFromBuffer(buffer, position);
        position += WtliIasOnerPrvntFin.Len.WTLI_IAS_ONER_PRVNT_FIN;
        wtliIasMggSin.setWtliIasMggSinFromBuffer(buffer, position);
        position += WtliIasMggSin.Len.WTLI_IAS_MGG_SIN;
        wtliIasRstDpst.setWtliIasRstDpstFromBuffer(buffer, position);
        position += WtliIasRstDpst.Len.WTLI_IAS_RST_DPST;
        wtliImpRimb.setWtliImpRimbFromBuffer(buffer, position);
        position += WtliImpRimb.Len.WTLI_IMP_RIMB;
        wtliComponTaxRimb.setWtliComponTaxRimbFromBuffer(buffer, position);
        position += WtliComponTaxRimb.Len.WTLI_COMPON_TAX_RIMB;
        wtliRisMat.setWtliRisMatFromBuffer(buffer, position);
        position += WtliRisMat.Len.WTLI_RIS_MAT;
        wtliRisSpe.setWtliRisSpeFromBuffer(buffer, position);
        position += WtliRisSpe.Len.WTLI_RIS_SPE;
        wtliDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WTLI_DS_RIGA, 0);
        position += Len.WTLI_DS_RIGA;
        wtliDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtliDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTLI_DS_VER, 0);
        position += Len.WTLI_DS_VER;
        wtliDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WTLI_DS_TS_INI_CPTZ, 0);
        position += Len.WTLI_DS_TS_INI_CPTZ;
        wtliDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WTLI_DS_TS_END_CPTZ, 0);
        position += Len.WTLI_DS_TS_END_CPTZ;
        wtliDsUtente = MarshalByte.readString(buffer, position, Len.WTLI_DS_UTENTE);
        position += Len.WTLI_DS_UTENTE;
        wtliDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtliIasPnl.setWtliIasPnlFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wtliIdTrchLiq, Len.Int.WTLI_ID_TRCH_LIQ, 0);
        position += Len.WTLI_ID_TRCH_LIQ;
        wtliIdGarLiq.getWtliIdGarLiqAsBuffer(buffer, position);
        position += WtliIdGarLiq.Len.WTLI_ID_GAR_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, wtliIdLiq, Len.Int.WTLI_ID_LIQ, 0);
        position += Len.WTLI_ID_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, wtliIdTrchDiGar, Len.Int.WTLI_ID_TRCH_DI_GAR, 0);
        position += Len.WTLI_ID_TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, wtliIdMoviCrz, Len.Int.WTLI_ID_MOVI_CRZ, 0);
        position += Len.WTLI_ID_MOVI_CRZ;
        wtliIdMoviChiu.getWtliIdMoviChiuAsBuffer(buffer, position);
        position += WtliIdMoviChiu.Len.WTLI_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wtliDtIniEff, Len.Int.WTLI_DT_INI_EFF, 0);
        position += Len.WTLI_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wtliDtEndEff, Len.Int.WTLI_DT_END_EFF, 0);
        position += Len.WTLI_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wtliCodCompAnia, Len.Int.WTLI_COD_COMP_ANIA, 0);
        position += Len.WTLI_COD_COMP_ANIA;
        MarshalByte.writeDecimalAsPacked(buffer, position, wtliImpLrdCalc.copy());
        position += Len.WTLI_IMP_LRD_CALC;
        wtliImpLrdDfz.getWtliImpLrdDfzAsBuffer(buffer, position);
        position += WtliImpLrdDfz.Len.WTLI_IMP_LRD_DFZ;
        MarshalByte.writeDecimalAsPacked(buffer, position, wtliImpLrdEfflq.copy());
        position += Len.WTLI_IMP_LRD_EFFLQ;
        MarshalByte.writeDecimalAsPacked(buffer, position, wtliImpNetCalc.copy());
        position += Len.WTLI_IMP_NET_CALC;
        wtliImpNetDfz.getWtliImpNetDfzAsBuffer(buffer, position);
        position += WtliImpNetDfz.Len.WTLI_IMP_NET_DFZ;
        MarshalByte.writeDecimalAsPacked(buffer, position, wtliImpNetEfflq.copy());
        position += Len.WTLI_IMP_NET_EFFLQ;
        wtliImpUti.getWtliImpUtiAsBuffer(buffer, position);
        position += WtliImpUti.Len.WTLI_IMP_UTI;
        MarshalByte.writeString(buffer, position, wtliCodTari, Len.WTLI_COD_TARI);
        position += Len.WTLI_COD_TARI;
        MarshalByte.writeString(buffer, position, wtliCodDvs, Len.WTLI_COD_DVS);
        position += Len.WTLI_COD_DVS;
        wtliIasOnerPrvntFin.getWtliIasOnerPrvntFinAsBuffer(buffer, position);
        position += WtliIasOnerPrvntFin.Len.WTLI_IAS_ONER_PRVNT_FIN;
        wtliIasMggSin.getWtliIasMggSinAsBuffer(buffer, position);
        position += WtliIasMggSin.Len.WTLI_IAS_MGG_SIN;
        wtliIasRstDpst.getWtliIasRstDpstAsBuffer(buffer, position);
        position += WtliIasRstDpst.Len.WTLI_IAS_RST_DPST;
        wtliImpRimb.getWtliImpRimbAsBuffer(buffer, position);
        position += WtliImpRimb.Len.WTLI_IMP_RIMB;
        wtliComponTaxRimb.getWtliComponTaxRimbAsBuffer(buffer, position);
        position += WtliComponTaxRimb.Len.WTLI_COMPON_TAX_RIMB;
        wtliRisMat.getWtliRisMatAsBuffer(buffer, position);
        position += WtliRisMat.Len.WTLI_RIS_MAT;
        wtliRisSpe.getWtliRisSpeAsBuffer(buffer, position);
        position += WtliRisSpe.Len.WTLI_RIS_SPE;
        MarshalByte.writeLongAsPacked(buffer, position, wtliDsRiga, Len.Int.WTLI_DS_RIGA, 0);
        position += Len.WTLI_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wtliDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wtliDsVer, Len.Int.WTLI_DS_VER, 0);
        position += Len.WTLI_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wtliDsTsIniCptz, Len.Int.WTLI_DS_TS_INI_CPTZ, 0);
        position += Len.WTLI_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wtliDsTsEndCptz, Len.Int.WTLI_DS_TS_END_CPTZ, 0);
        position += Len.WTLI_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wtliDsUtente, Len.WTLI_DS_UTENTE);
        position += Len.WTLI_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wtliDsStatoElab);
        position += Types.CHAR_SIZE;
        wtliIasPnl.getWtliIasPnlAsBuffer(buffer, position);
        return buffer;
    }

    public void initDatiSpaces() {
        wtliIdTrchLiq = Types.INVALID_INT_VAL;
        wtliIdGarLiq.initWtliIdGarLiqSpaces();
        wtliIdLiq = Types.INVALID_INT_VAL;
        wtliIdTrchDiGar = Types.INVALID_INT_VAL;
        wtliIdMoviCrz = Types.INVALID_INT_VAL;
        wtliIdMoviChiu.initWtliIdMoviChiuSpaces();
        wtliDtIniEff = Types.INVALID_INT_VAL;
        wtliDtEndEff = Types.INVALID_INT_VAL;
        wtliCodCompAnia = Types.INVALID_INT_VAL;
        wtliImpLrdCalc.setNaN();
        wtliImpLrdDfz.initWtliImpLrdDfzSpaces();
        wtliImpLrdEfflq.setNaN();
        wtliImpNetCalc.setNaN();
        wtliImpNetDfz.initWtliImpNetDfzSpaces();
        wtliImpNetEfflq.setNaN();
        wtliImpUti.initWtliImpUtiSpaces();
        wtliCodTari = "";
        wtliCodDvs = "";
        wtliIasOnerPrvntFin.initWtliIasOnerPrvntFinSpaces();
        wtliIasMggSin.initWtliIasMggSinSpaces();
        wtliIasRstDpst.initWtliIasRstDpstSpaces();
        wtliImpRimb.initWtliImpRimbSpaces();
        wtliComponTaxRimb.initWtliComponTaxRimbSpaces();
        wtliRisMat.initWtliRisMatSpaces();
        wtliRisSpe.initWtliRisSpeSpaces();
        wtliDsRiga = Types.INVALID_LONG_VAL;
        wtliDsOperSql = Types.SPACE_CHAR;
        wtliDsVer = Types.INVALID_INT_VAL;
        wtliDsTsIniCptz = Types.INVALID_LONG_VAL;
        wtliDsTsEndCptz = Types.INVALID_LONG_VAL;
        wtliDsUtente = "";
        wtliDsStatoElab = Types.SPACE_CHAR;
        wtliIasPnl.initWtliIasPnlSpaces();
    }

    public void setWtliIdTrchLiq(int wtliIdTrchLiq) {
        this.wtliIdTrchLiq = wtliIdTrchLiq;
    }

    public int getWtliIdTrchLiq() {
        return this.wtliIdTrchLiq;
    }

    public void setWtliIdLiq(int wtliIdLiq) {
        this.wtliIdLiq = wtliIdLiq;
    }

    public int getWtliIdLiq() {
        return this.wtliIdLiq;
    }

    public void setWtliIdTrchDiGar(int wtliIdTrchDiGar) {
        this.wtliIdTrchDiGar = wtliIdTrchDiGar;
    }

    public int getWtliIdTrchDiGar() {
        return this.wtliIdTrchDiGar;
    }

    public void setWtliIdMoviCrz(int wtliIdMoviCrz) {
        this.wtliIdMoviCrz = wtliIdMoviCrz;
    }

    public int getWtliIdMoviCrz() {
        return this.wtliIdMoviCrz;
    }

    public void setWtliDtIniEff(int wtliDtIniEff) {
        this.wtliDtIniEff = wtliDtIniEff;
    }

    public int getWtliDtIniEff() {
        return this.wtliDtIniEff;
    }

    public void setWtliDtEndEff(int wtliDtEndEff) {
        this.wtliDtEndEff = wtliDtEndEff;
    }

    public int getWtliDtEndEff() {
        return this.wtliDtEndEff;
    }

    public void setWtliCodCompAnia(int wtliCodCompAnia) {
        this.wtliCodCompAnia = wtliCodCompAnia;
    }

    public int getWtliCodCompAnia() {
        return this.wtliCodCompAnia;
    }

    public void setWtliImpLrdCalc(AfDecimal wtliImpLrdCalc) {
        this.wtliImpLrdCalc.assign(wtliImpLrdCalc);
    }

    public AfDecimal getWtliImpLrdCalc() {
        return this.wtliImpLrdCalc.copy();
    }

    public void setWtliImpLrdEfflq(AfDecimal wtliImpLrdEfflq) {
        this.wtliImpLrdEfflq.assign(wtliImpLrdEfflq);
    }

    public AfDecimal getWtliImpLrdEfflq() {
        return this.wtliImpLrdEfflq.copy();
    }

    public void setWtliImpNetCalc(AfDecimal wtliImpNetCalc) {
        this.wtliImpNetCalc.assign(wtliImpNetCalc);
    }

    public AfDecimal getWtliImpNetCalc() {
        return this.wtliImpNetCalc.copy();
    }

    public void setWtliImpNetEfflq(AfDecimal wtliImpNetEfflq) {
        this.wtliImpNetEfflq.assign(wtliImpNetEfflq);
    }

    public AfDecimal getWtliImpNetEfflq() {
        return this.wtliImpNetEfflq.copy();
    }

    public void setWtliCodTari(String wtliCodTari) {
        this.wtliCodTari = Functions.subString(wtliCodTari, Len.WTLI_COD_TARI);
    }

    public String getWtliCodTari() {
        return this.wtliCodTari;
    }

    public void setWtliCodDvs(String wtliCodDvs) {
        this.wtliCodDvs = Functions.subString(wtliCodDvs, Len.WTLI_COD_DVS);
    }

    public String getWtliCodDvs() {
        return this.wtliCodDvs;
    }

    public void setWtliDsRiga(long wtliDsRiga) {
        this.wtliDsRiga = wtliDsRiga;
    }

    public long getWtliDsRiga() {
        return this.wtliDsRiga;
    }

    public void setWtliDsOperSql(char wtliDsOperSql) {
        this.wtliDsOperSql = wtliDsOperSql;
    }

    public char getWtliDsOperSql() {
        return this.wtliDsOperSql;
    }

    public void setWtliDsVer(int wtliDsVer) {
        this.wtliDsVer = wtliDsVer;
    }

    public int getWtliDsVer() {
        return this.wtliDsVer;
    }

    public void setWtliDsTsIniCptz(long wtliDsTsIniCptz) {
        this.wtliDsTsIniCptz = wtliDsTsIniCptz;
    }

    public long getWtliDsTsIniCptz() {
        return this.wtliDsTsIniCptz;
    }

    public void setWtliDsTsEndCptz(long wtliDsTsEndCptz) {
        this.wtliDsTsEndCptz = wtliDsTsEndCptz;
    }

    public long getWtliDsTsEndCptz() {
        return this.wtliDsTsEndCptz;
    }

    public void setWtliDsUtente(String wtliDsUtente) {
        this.wtliDsUtente = Functions.subString(wtliDsUtente, Len.WTLI_DS_UTENTE);
    }

    public String getWtliDsUtente() {
        return this.wtliDsUtente;
    }

    public void setWtliDsStatoElab(char wtliDsStatoElab) {
        this.wtliDsStatoElab = wtliDsStatoElab;
    }

    public char getWtliDsStatoElab() {
        return this.wtliDsStatoElab;
    }

    public WtliComponTaxRimb getWtliComponTaxRimb() {
        return wtliComponTaxRimb;
    }

    public WtliIasMggSin getWtliIasMggSin() {
        return wtliIasMggSin;
    }

    public WtliIasOnerPrvntFin getWtliIasOnerPrvntFin() {
        return wtliIasOnerPrvntFin;
    }

    public WtliIasPnl getWtliIasPnl() {
        return wtliIasPnl;
    }

    public WtliIasRstDpst getWtliIasRstDpst() {
        return wtliIasRstDpst;
    }

    public WtliIdGarLiq getWtliIdGarLiq() {
        return wtliIdGarLiq;
    }

    public WtliIdMoviChiu getWtliIdMoviChiu() {
        return wtliIdMoviChiu;
    }

    public WtliImpLrdDfz getWtliImpLrdDfz() {
        return wtliImpLrdDfz;
    }

    public WtliImpNetDfz getWtliImpNetDfz() {
        return wtliImpNetDfz;
    }

    public WtliImpRimb getWtliImpRimb() {
        return wtliImpRimb;
    }

    public WtliImpUti getWtliImpUti() {
        return wtliImpUti;
    }

    public WtliRisMat getWtliRisMat() {
        return wtliRisMat;
    }

    public WtliRisSpe getWtliRisSpe() {
        return wtliRisSpe;
    }

    public WtliDati copy() {
        return new WtliDati(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_ID_TRCH_LIQ = 5;
        public static final int WTLI_ID_LIQ = 5;
        public static final int WTLI_ID_TRCH_DI_GAR = 5;
        public static final int WTLI_ID_MOVI_CRZ = 5;
        public static final int WTLI_DT_INI_EFF = 5;
        public static final int WTLI_DT_END_EFF = 5;
        public static final int WTLI_COD_COMP_ANIA = 3;
        public static final int WTLI_IMP_LRD_CALC = 8;
        public static final int WTLI_IMP_LRD_EFFLQ = 8;
        public static final int WTLI_IMP_NET_CALC = 8;
        public static final int WTLI_IMP_NET_EFFLQ = 8;
        public static final int WTLI_COD_TARI = 12;
        public static final int WTLI_COD_DVS = 20;
        public static final int WTLI_DS_RIGA = 6;
        public static final int WTLI_DS_OPER_SQL = 1;
        public static final int WTLI_DS_VER = 5;
        public static final int WTLI_DS_TS_INI_CPTZ = 10;
        public static final int WTLI_DS_TS_END_CPTZ = 10;
        public static final int WTLI_DS_UTENTE = 20;
        public static final int WTLI_DS_STATO_ELAB = 1;
        public static final int DATI = WTLI_ID_TRCH_LIQ + WtliIdGarLiq.Len.WTLI_ID_GAR_LIQ + WTLI_ID_LIQ + WTLI_ID_TRCH_DI_GAR + WTLI_ID_MOVI_CRZ + WtliIdMoviChiu.Len.WTLI_ID_MOVI_CHIU + WTLI_DT_INI_EFF + WTLI_DT_END_EFF + WTLI_COD_COMP_ANIA + WTLI_IMP_LRD_CALC + WtliImpLrdDfz.Len.WTLI_IMP_LRD_DFZ + WTLI_IMP_LRD_EFFLQ + WTLI_IMP_NET_CALC + WtliImpNetDfz.Len.WTLI_IMP_NET_DFZ + WTLI_IMP_NET_EFFLQ + WtliImpUti.Len.WTLI_IMP_UTI + WTLI_COD_TARI + WTLI_COD_DVS + WtliIasOnerPrvntFin.Len.WTLI_IAS_ONER_PRVNT_FIN + WtliIasMggSin.Len.WTLI_IAS_MGG_SIN + WtliIasRstDpst.Len.WTLI_IAS_RST_DPST + WtliImpRimb.Len.WTLI_IMP_RIMB + WtliComponTaxRimb.Len.WTLI_COMPON_TAX_RIMB + WtliRisMat.Len.WTLI_RIS_MAT + WtliRisSpe.Len.WTLI_RIS_SPE + WTLI_DS_RIGA + WTLI_DS_OPER_SQL + WTLI_DS_VER + WTLI_DS_TS_INI_CPTZ + WTLI_DS_TS_END_CPTZ + WTLI_DS_UTENTE + WTLI_DS_STATO_ELAB + WtliIasPnl.Len.WTLI_IAS_PNL;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_ID_TRCH_LIQ = 9;
            public static final int WTLI_ID_LIQ = 9;
            public static final int WTLI_ID_TRCH_DI_GAR = 9;
            public static final int WTLI_ID_MOVI_CRZ = 9;
            public static final int WTLI_DT_INI_EFF = 8;
            public static final int WTLI_DT_END_EFF = 8;
            public static final int WTLI_COD_COMP_ANIA = 5;
            public static final int WTLI_IMP_LRD_CALC = 12;
            public static final int WTLI_IMP_LRD_EFFLQ = 12;
            public static final int WTLI_IMP_NET_CALC = 12;
            public static final int WTLI_IMP_NET_EFFLQ = 12;
            public static final int WTLI_DS_RIGA = 10;
            public static final int WTLI_DS_VER = 9;
            public static final int WTLI_DS_TS_INI_CPTZ = 18;
            public static final int WTLI_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_IMP_LRD_CALC = 3;
            public static final int WTLI_IMP_LRD_EFFLQ = 3;
            public static final int WTLI_IMP_NET_CALC = 3;
            public static final int WTLI_IMP_NET_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
