package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WocoCarAcq;
import it.accenture.jnais.ws.redefines.WocoDtDecor;
import it.accenture.jnais.ws.redefines.WocoDtScad;
import it.accenture.jnais.ws.redefines.WocoDtUltPrePag;
import it.accenture.jnais.ws.redefines.WocoIdMoviChiu;
import it.accenture.jnais.ws.redefines.WocoImpCollg;
import it.accenture.jnais.ws.redefines.WocoImpReinvst;
import it.accenture.jnais.ws.redefines.WocoImpTrasf;
import it.accenture.jnais.ws.redefines.WocoImpTrasferito;
import it.accenture.jnais.ws.redefines.WocoPcPreTrasferito;
import it.accenture.jnais.ws.redefines.WocoPcReinvstRilievi;
import it.accenture.jnais.ws.redefines.WocoPre1aAnnualita;
import it.accenture.jnais.ws.redefines.WocoPrePerTrasf;
import it.accenture.jnais.ws.redefines.WocoRecProv;
import it.accenture.jnais.ws.redefines.WocoRisMat;
import it.accenture.jnais.ws.redefines.WocoRisZil;
import it.accenture.jnais.ws.redefines.WocoTotPre;

/**Original name: WOCO-DATI<br>
 * Variable: WOCO-DATI from copybook LCCVOCO1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WocoDati {

    //==== PROPERTIES ====
    //Original name: WOCO-ID-OGG-COLLG
    private int wocoIdOggCollg = DefaultValues.INT_VAL;
    //Original name: WOCO-ID-OGG-COINV
    private int wocoIdOggCoinv = DefaultValues.INT_VAL;
    //Original name: WOCO-TP-OGG-COINV
    private String wocoTpOggCoinv = DefaultValues.stringVal(Len.WOCO_TP_OGG_COINV);
    //Original name: WOCO-ID-OGG-DER
    private int wocoIdOggDer = DefaultValues.INT_VAL;
    //Original name: WOCO-TP-OGG-DER
    private String wocoTpOggDer = DefaultValues.stringVal(Len.WOCO_TP_OGG_DER);
    //Original name: WOCO-IB-RIFTO-ESTNO
    private String wocoIbRiftoEstno = DefaultValues.stringVal(Len.WOCO_IB_RIFTO_ESTNO);
    //Original name: WOCO-ID-MOVI-CRZ
    private int wocoIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WOCO-ID-MOVI-CHIU
    private WocoIdMoviChiu wocoIdMoviChiu = new WocoIdMoviChiu();
    //Original name: WOCO-DT-INI-EFF
    private int wocoDtIniEff = DefaultValues.INT_VAL;
    //Original name: WOCO-DT-END-EFF
    private int wocoDtEndEff = DefaultValues.INT_VAL;
    //Original name: WOCO-COD-COMP-ANIA
    private int wocoCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WOCO-COD-PROD
    private String wocoCodProd = DefaultValues.stringVal(Len.WOCO_COD_PROD);
    //Original name: WOCO-DT-SCAD
    private WocoDtScad wocoDtScad = new WocoDtScad();
    //Original name: WOCO-TP-COLLGM
    private String wocoTpCollgm = DefaultValues.stringVal(Len.WOCO_TP_COLLGM);
    //Original name: WOCO-TP-TRASF
    private String wocoTpTrasf = DefaultValues.stringVal(Len.WOCO_TP_TRASF);
    //Original name: WOCO-REC-PROV
    private WocoRecProv wocoRecProv = new WocoRecProv();
    //Original name: WOCO-DT-DECOR
    private WocoDtDecor wocoDtDecor = new WocoDtDecor();
    //Original name: WOCO-DT-ULT-PRE-PAG
    private WocoDtUltPrePag wocoDtUltPrePag = new WocoDtUltPrePag();
    //Original name: WOCO-TOT-PRE
    private WocoTotPre wocoTotPre = new WocoTotPre();
    //Original name: WOCO-RIS-MAT
    private WocoRisMat wocoRisMat = new WocoRisMat();
    //Original name: WOCO-RIS-ZIL
    private WocoRisZil wocoRisZil = new WocoRisZil();
    //Original name: WOCO-IMP-TRASF
    private WocoImpTrasf wocoImpTrasf = new WocoImpTrasf();
    //Original name: WOCO-IMP-REINVST
    private WocoImpReinvst wocoImpReinvst = new WocoImpReinvst();
    //Original name: WOCO-PC-REINVST-RILIEVI
    private WocoPcReinvstRilievi wocoPcReinvstRilievi = new WocoPcReinvstRilievi();
    //Original name: WOCO-IB-2O-RIFTO-ESTNO
    private String wocoIb2oRiftoEstno = DefaultValues.stringVal(Len.WOCO_IB2O_RIFTO_ESTNO);
    //Original name: WOCO-IND-LIQ-AGG-MAN
    private char wocoIndLiqAggMan = DefaultValues.CHAR_VAL;
    //Original name: WOCO-DS-RIGA
    private long wocoDsRiga = DefaultValues.LONG_VAL;
    //Original name: WOCO-DS-OPER-SQL
    private char wocoDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WOCO-DS-VER
    private int wocoDsVer = DefaultValues.INT_VAL;
    //Original name: WOCO-DS-TS-INI-CPTZ
    private long wocoDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WOCO-DS-TS-END-CPTZ
    private long wocoDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WOCO-DS-UTENTE
    private String wocoDsUtente = DefaultValues.stringVal(Len.WOCO_DS_UTENTE);
    //Original name: WOCO-DS-STATO-ELAB
    private char wocoDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WOCO-IMP-COLLG
    private WocoImpCollg wocoImpCollg = new WocoImpCollg();
    //Original name: WOCO-PRE-PER-TRASF
    private WocoPrePerTrasf wocoPrePerTrasf = new WocoPrePerTrasf();
    //Original name: WOCO-CAR-ACQ
    private WocoCarAcq wocoCarAcq = new WocoCarAcq();
    //Original name: WOCO-PRE-1A-ANNUALITA
    private WocoPre1aAnnualita wocoPre1aAnnualita = new WocoPre1aAnnualita();
    //Original name: WOCO-IMP-TRASFERITO
    private WocoImpTrasferito wocoImpTrasferito = new WocoImpTrasferito();
    //Original name: WOCO-TP-MOD-ABBINAMENTO
    private String wocoTpModAbbinamento = DefaultValues.stringVal(Len.WOCO_TP_MOD_ABBINAMENTO);
    //Original name: WOCO-PC-PRE-TRASFERITO
    private WocoPcPreTrasferito wocoPcPreTrasferito = new WocoPcPreTrasferito();

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wocoIdOggCollg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WOCO_ID_OGG_COLLG, 0);
        position += Len.WOCO_ID_OGG_COLLG;
        wocoIdOggCoinv = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WOCO_ID_OGG_COINV, 0);
        position += Len.WOCO_ID_OGG_COINV;
        wocoTpOggCoinv = MarshalByte.readString(buffer, position, Len.WOCO_TP_OGG_COINV);
        position += Len.WOCO_TP_OGG_COINV;
        wocoIdOggDer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WOCO_ID_OGG_DER, 0);
        position += Len.WOCO_ID_OGG_DER;
        wocoTpOggDer = MarshalByte.readString(buffer, position, Len.WOCO_TP_OGG_DER);
        position += Len.WOCO_TP_OGG_DER;
        wocoIbRiftoEstno = MarshalByte.readString(buffer, position, Len.WOCO_IB_RIFTO_ESTNO);
        position += Len.WOCO_IB_RIFTO_ESTNO;
        wocoIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WOCO_ID_MOVI_CRZ, 0);
        position += Len.WOCO_ID_MOVI_CRZ;
        wocoIdMoviChiu.setWocoIdMoviChiuFromBuffer(buffer, position);
        position += WocoIdMoviChiu.Len.WOCO_ID_MOVI_CHIU;
        wocoDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WOCO_DT_INI_EFF, 0);
        position += Len.WOCO_DT_INI_EFF;
        wocoDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WOCO_DT_END_EFF, 0);
        position += Len.WOCO_DT_END_EFF;
        wocoCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WOCO_COD_COMP_ANIA, 0);
        position += Len.WOCO_COD_COMP_ANIA;
        wocoCodProd = MarshalByte.readString(buffer, position, Len.WOCO_COD_PROD);
        position += Len.WOCO_COD_PROD;
        wocoDtScad.setWocoDtScadFromBuffer(buffer, position);
        position += WocoDtScad.Len.WOCO_DT_SCAD;
        wocoTpCollgm = MarshalByte.readString(buffer, position, Len.WOCO_TP_COLLGM);
        position += Len.WOCO_TP_COLLGM;
        wocoTpTrasf = MarshalByte.readString(buffer, position, Len.WOCO_TP_TRASF);
        position += Len.WOCO_TP_TRASF;
        wocoRecProv.setWocoRecProvFromBuffer(buffer, position);
        position += WocoRecProv.Len.WOCO_REC_PROV;
        wocoDtDecor.setWocoDtDecorFromBuffer(buffer, position);
        position += WocoDtDecor.Len.WOCO_DT_DECOR;
        wocoDtUltPrePag.setWocoDtUltPrePagFromBuffer(buffer, position);
        position += WocoDtUltPrePag.Len.WOCO_DT_ULT_PRE_PAG;
        wocoTotPre.setWocoTotPreFromBuffer(buffer, position);
        position += WocoTotPre.Len.WOCO_TOT_PRE;
        wocoRisMat.setWocoRisMatFromBuffer(buffer, position);
        position += WocoRisMat.Len.WOCO_RIS_MAT;
        wocoRisZil.setWocoRisZilFromBuffer(buffer, position);
        position += WocoRisZil.Len.WOCO_RIS_ZIL;
        wocoImpTrasf.setWocoImpTrasfFromBuffer(buffer, position);
        position += WocoImpTrasf.Len.WOCO_IMP_TRASF;
        wocoImpReinvst.setWocoImpReinvstFromBuffer(buffer, position);
        position += WocoImpReinvst.Len.WOCO_IMP_REINVST;
        wocoPcReinvstRilievi.setWocoPcReinvstRilieviFromBuffer(buffer, position);
        position += WocoPcReinvstRilievi.Len.WOCO_PC_REINVST_RILIEVI;
        wocoIb2oRiftoEstno = MarshalByte.readString(buffer, position, Len.WOCO_IB2O_RIFTO_ESTNO);
        position += Len.WOCO_IB2O_RIFTO_ESTNO;
        wocoIndLiqAggMan = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wocoDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WOCO_DS_RIGA, 0);
        position += Len.WOCO_DS_RIGA;
        wocoDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wocoDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WOCO_DS_VER, 0);
        position += Len.WOCO_DS_VER;
        wocoDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WOCO_DS_TS_INI_CPTZ, 0);
        position += Len.WOCO_DS_TS_INI_CPTZ;
        wocoDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WOCO_DS_TS_END_CPTZ, 0);
        position += Len.WOCO_DS_TS_END_CPTZ;
        wocoDsUtente = MarshalByte.readString(buffer, position, Len.WOCO_DS_UTENTE);
        position += Len.WOCO_DS_UTENTE;
        wocoDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wocoImpCollg.setWocoImpCollgFromBuffer(buffer, position);
        position += WocoImpCollg.Len.WOCO_IMP_COLLG;
        wocoPrePerTrasf.setWocoPrePerTrasfFromBuffer(buffer, position);
        position += WocoPrePerTrasf.Len.WOCO_PRE_PER_TRASF;
        wocoCarAcq.setWocoCarAcqFromBuffer(buffer, position);
        position += WocoCarAcq.Len.WOCO_CAR_ACQ;
        wocoPre1aAnnualita.setWocoPre1aAnnualitaFromBuffer(buffer, position);
        position += WocoPre1aAnnualita.Len.WOCO_PRE1A_ANNUALITA;
        wocoImpTrasferito.setWocoImpTrasferitoFromBuffer(buffer, position);
        position += WocoImpTrasferito.Len.WOCO_IMP_TRASFERITO;
        wocoTpModAbbinamento = MarshalByte.readString(buffer, position, Len.WOCO_TP_MOD_ABBINAMENTO);
        position += Len.WOCO_TP_MOD_ABBINAMENTO;
        wocoPcPreTrasferito.setWocoPcPreTrasferitoFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wocoIdOggCollg, Len.Int.WOCO_ID_OGG_COLLG, 0);
        position += Len.WOCO_ID_OGG_COLLG;
        MarshalByte.writeIntAsPacked(buffer, position, wocoIdOggCoinv, Len.Int.WOCO_ID_OGG_COINV, 0);
        position += Len.WOCO_ID_OGG_COINV;
        MarshalByte.writeString(buffer, position, wocoTpOggCoinv, Len.WOCO_TP_OGG_COINV);
        position += Len.WOCO_TP_OGG_COINV;
        MarshalByte.writeIntAsPacked(buffer, position, wocoIdOggDer, Len.Int.WOCO_ID_OGG_DER, 0);
        position += Len.WOCO_ID_OGG_DER;
        MarshalByte.writeString(buffer, position, wocoTpOggDer, Len.WOCO_TP_OGG_DER);
        position += Len.WOCO_TP_OGG_DER;
        MarshalByte.writeString(buffer, position, wocoIbRiftoEstno, Len.WOCO_IB_RIFTO_ESTNO);
        position += Len.WOCO_IB_RIFTO_ESTNO;
        MarshalByte.writeIntAsPacked(buffer, position, wocoIdMoviCrz, Len.Int.WOCO_ID_MOVI_CRZ, 0);
        position += Len.WOCO_ID_MOVI_CRZ;
        wocoIdMoviChiu.getWocoIdMoviChiuAsBuffer(buffer, position);
        position += WocoIdMoviChiu.Len.WOCO_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wocoDtIniEff, Len.Int.WOCO_DT_INI_EFF, 0);
        position += Len.WOCO_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wocoDtEndEff, Len.Int.WOCO_DT_END_EFF, 0);
        position += Len.WOCO_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wocoCodCompAnia, Len.Int.WOCO_COD_COMP_ANIA, 0);
        position += Len.WOCO_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wocoCodProd, Len.WOCO_COD_PROD);
        position += Len.WOCO_COD_PROD;
        wocoDtScad.getWocoDtScadAsBuffer(buffer, position);
        position += WocoDtScad.Len.WOCO_DT_SCAD;
        MarshalByte.writeString(buffer, position, wocoTpCollgm, Len.WOCO_TP_COLLGM);
        position += Len.WOCO_TP_COLLGM;
        MarshalByte.writeString(buffer, position, wocoTpTrasf, Len.WOCO_TP_TRASF);
        position += Len.WOCO_TP_TRASF;
        wocoRecProv.getWocoRecProvAsBuffer(buffer, position);
        position += WocoRecProv.Len.WOCO_REC_PROV;
        wocoDtDecor.getWocoDtDecorAsBuffer(buffer, position);
        position += WocoDtDecor.Len.WOCO_DT_DECOR;
        wocoDtUltPrePag.getWocoDtUltPrePagAsBuffer(buffer, position);
        position += WocoDtUltPrePag.Len.WOCO_DT_ULT_PRE_PAG;
        wocoTotPre.getWocoTotPreAsBuffer(buffer, position);
        position += WocoTotPre.Len.WOCO_TOT_PRE;
        wocoRisMat.getWocoRisMatAsBuffer(buffer, position);
        position += WocoRisMat.Len.WOCO_RIS_MAT;
        wocoRisZil.getWocoRisZilAsBuffer(buffer, position);
        position += WocoRisZil.Len.WOCO_RIS_ZIL;
        wocoImpTrasf.getWocoImpTrasfAsBuffer(buffer, position);
        position += WocoImpTrasf.Len.WOCO_IMP_TRASF;
        wocoImpReinvst.getWocoImpReinvstAsBuffer(buffer, position);
        position += WocoImpReinvst.Len.WOCO_IMP_REINVST;
        wocoPcReinvstRilievi.getWocoPcReinvstRilieviAsBuffer(buffer, position);
        position += WocoPcReinvstRilievi.Len.WOCO_PC_REINVST_RILIEVI;
        MarshalByte.writeString(buffer, position, wocoIb2oRiftoEstno, Len.WOCO_IB2O_RIFTO_ESTNO);
        position += Len.WOCO_IB2O_RIFTO_ESTNO;
        MarshalByte.writeChar(buffer, position, wocoIndLiqAggMan);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, wocoDsRiga, Len.Int.WOCO_DS_RIGA, 0);
        position += Len.WOCO_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wocoDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wocoDsVer, Len.Int.WOCO_DS_VER, 0);
        position += Len.WOCO_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wocoDsTsIniCptz, Len.Int.WOCO_DS_TS_INI_CPTZ, 0);
        position += Len.WOCO_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wocoDsTsEndCptz, Len.Int.WOCO_DS_TS_END_CPTZ, 0);
        position += Len.WOCO_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wocoDsUtente, Len.WOCO_DS_UTENTE);
        position += Len.WOCO_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wocoDsStatoElab);
        position += Types.CHAR_SIZE;
        wocoImpCollg.getWocoImpCollgAsBuffer(buffer, position);
        position += WocoImpCollg.Len.WOCO_IMP_COLLG;
        wocoPrePerTrasf.getWocoPrePerTrasfAsBuffer(buffer, position);
        position += WocoPrePerTrasf.Len.WOCO_PRE_PER_TRASF;
        wocoCarAcq.getWocoCarAcqAsBuffer(buffer, position);
        position += WocoCarAcq.Len.WOCO_CAR_ACQ;
        wocoPre1aAnnualita.getWocoPre1aAnnualitaAsBuffer(buffer, position);
        position += WocoPre1aAnnualita.Len.WOCO_PRE1A_ANNUALITA;
        wocoImpTrasferito.getWocoImpTrasferitoAsBuffer(buffer, position);
        position += WocoImpTrasferito.Len.WOCO_IMP_TRASFERITO;
        MarshalByte.writeString(buffer, position, wocoTpModAbbinamento, Len.WOCO_TP_MOD_ABBINAMENTO);
        position += Len.WOCO_TP_MOD_ABBINAMENTO;
        wocoPcPreTrasferito.getWocoPcPreTrasferitoAsBuffer(buffer, position);
        return buffer;
    }

    public void initDatiSpaces() {
        wocoIdOggCollg = Types.INVALID_INT_VAL;
        wocoIdOggCoinv = Types.INVALID_INT_VAL;
        wocoTpOggCoinv = "";
        wocoIdOggDer = Types.INVALID_INT_VAL;
        wocoTpOggDer = "";
        wocoIbRiftoEstno = "";
        wocoIdMoviCrz = Types.INVALID_INT_VAL;
        wocoIdMoviChiu.initWocoIdMoviChiuSpaces();
        wocoDtIniEff = Types.INVALID_INT_VAL;
        wocoDtEndEff = Types.INVALID_INT_VAL;
        wocoCodCompAnia = Types.INVALID_INT_VAL;
        wocoCodProd = "";
        wocoDtScad.initWocoDtScadSpaces();
        wocoTpCollgm = "";
        wocoTpTrasf = "";
        wocoRecProv.initWocoRecProvSpaces();
        wocoDtDecor.initWocoDtDecorSpaces();
        wocoDtUltPrePag.initWocoDtUltPrePagSpaces();
        wocoTotPre.initWocoTotPreSpaces();
        wocoRisMat.initWocoRisMatSpaces();
        wocoRisZil.initWocoRisZilSpaces();
        wocoImpTrasf.initWocoImpTrasfSpaces();
        wocoImpReinvst.initWocoImpReinvstSpaces();
        wocoPcReinvstRilievi.initWocoPcReinvstRilieviSpaces();
        wocoIb2oRiftoEstno = "";
        wocoIndLiqAggMan = Types.SPACE_CHAR;
        wocoDsRiga = Types.INVALID_LONG_VAL;
        wocoDsOperSql = Types.SPACE_CHAR;
        wocoDsVer = Types.INVALID_INT_VAL;
        wocoDsTsIniCptz = Types.INVALID_LONG_VAL;
        wocoDsTsEndCptz = Types.INVALID_LONG_VAL;
        wocoDsUtente = "";
        wocoDsStatoElab = Types.SPACE_CHAR;
        wocoImpCollg.initWocoImpCollgSpaces();
        wocoPrePerTrasf.initWocoPrePerTrasfSpaces();
        wocoCarAcq.initWocoCarAcqSpaces();
        wocoPre1aAnnualita.initWocoPre1aAnnualitaSpaces();
        wocoImpTrasferito.initWocoImpTrasferitoSpaces();
        wocoTpModAbbinamento = "";
        wocoPcPreTrasferito.initWocoPcPreTrasferitoSpaces();
    }

    public void setWocoIdOggCollg(int wocoIdOggCollg) {
        this.wocoIdOggCollg = wocoIdOggCollg;
    }

    public int getWocoIdOggCollg() {
        return this.wocoIdOggCollg;
    }

    public void setWocoIdOggCoinv(int wocoIdOggCoinv) {
        this.wocoIdOggCoinv = wocoIdOggCoinv;
    }

    public int getWocoIdOggCoinv() {
        return this.wocoIdOggCoinv;
    }

    public void setWocoTpOggCoinv(String wocoTpOggCoinv) {
        this.wocoTpOggCoinv = Functions.subString(wocoTpOggCoinv, Len.WOCO_TP_OGG_COINV);
    }

    public String getWocoTpOggCoinv() {
        return this.wocoTpOggCoinv;
    }

    public void setWocoIdOggDer(int wocoIdOggDer) {
        this.wocoIdOggDer = wocoIdOggDer;
    }

    public int getWocoIdOggDer() {
        return this.wocoIdOggDer;
    }

    public void setWocoTpOggDer(String wocoTpOggDer) {
        this.wocoTpOggDer = Functions.subString(wocoTpOggDer, Len.WOCO_TP_OGG_DER);
    }

    public String getWocoTpOggDer() {
        return this.wocoTpOggDer;
    }

    public void setWocoIbRiftoEstno(String wocoIbRiftoEstno) {
        this.wocoIbRiftoEstno = Functions.subString(wocoIbRiftoEstno, Len.WOCO_IB_RIFTO_ESTNO);
    }

    public String getWocoIbRiftoEstno() {
        return this.wocoIbRiftoEstno;
    }

    public void setWocoIdMoviCrz(int wocoIdMoviCrz) {
        this.wocoIdMoviCrz = wocoIdMoviCrz;
    }

    public int getWocoIdMoviCrz() {
        return this.wocoIdMoviCrz;
    }

    public void setWocoDtIniEff(int wocoDtIniEff) {
        this.wocoDtIniEff = wocoDtIniEff;
    }

    public int getWocoDtIniEff() {
        return this.wocoDtIniEff;
    }

    public void setWocoDtEndEff(int wocoDtEndEff) {
        this.wocoDtEndEff = wocoDtEndEff;
    }

    public int getWocoDtEndEff() {
        return this.wocoDtEndEff;
    }

    public void setWocoCodCompAnia(int wocoCodCompAnia) {
        this.wocoCodCompAnia = wocoCodCompAnia;
    }

    public int getWocoCodCompAnia() {
        return this.wocoCodCompAnia;
    }

    public void setWocoCodProd(String wocoCodProd) {
        this.wocoCodProd = Functions.subString(wocoCodProd, Len.WOCO_COD_PROD);
    }

    public String getWocoCodProd() {
        return this.wocoCodProd;
    }

    public void setWocoTpCollgm(String wocoTpCollgm) {
        this.wocoTpCollgm = Functions.subString(wocoTpCollgm, Len.WOCO_TP_COLLGM);
    }

    public String getWocoTpCollgm() {
        return this.wocoTpCollgm;
    }

    public void setWocoTpTrasf(String wocoTpTrasf) {
        this.wocoTpTrasf = Functions.subString(wocoTpTrasf, Len.WOCO_TP_TRASF);
    }

    public String getWocoTpTrasf() {
        return this.wocoTpTrasf;
    }

    public void setWocoIb2oRiftoEstno(String wocoIb2oRiftoEstno) {
        this.wocoIb2oRiftoEstno = Functions.subString(wocoIb2oRiftoEstno, Len.WOCO_IB2O_RIFTO_ESTNO);
    }

    public String getWocoIb2oRiftoEstno() {
        return this.wocoIb2oRiftoEstno;
    }

    public void setWocoIndLiqAggMan(char wocoIndLiqAggMan) {
        this.wocoIndLiqAggMan = wocoIndLiqAggMan;
    }

    public char getWocoIndLiqAggMan() {
        return this.wocoIndLiqAggMan;
    }

    public void setWocoDsRiga(long wocoDsRiga) {
        this.wocoDsRiga = wocoDsRiga;
    }

    public long getWocoDsRiga() {
        return this.wocoDsRiga;
    }

    public void setWocoDsOperSql(char wocoDsOperSql) {
        this.wocoDsOperSql = wocoDsOperSql;
    }

    public char getWocoDsOperSql() {
        return this.wocoDsOperSql;
    }

    public void setWocoDsVer(int wocoDsVer) {
        this.wocoDsVer = wocoDsVer;
    }

    public int getWocoDsVer() {
        return this.wocoDsVer;
    }

    public void setWocoDsTsIniCptz(long wocoDsTsIniCptz) {
        this.wocoDsTsIniCptz = wocoDsTsIniCptz;
    }

    public long getWocoDsTsIniCptz() {
        return this.wocoDsTsIniCptz;
    }

    public void setWocoDsTsEndCptz(long wocoDsTsEndCptz) {
        this.wocoDsTsEndCptz = wocoDsTsEndCptz;
    }

    public long getWocoDsTsEndCptz() {
        return this.wocoDsTsEndCptz;
    }

    public void setWocoDsUtente(String wocoDsUtente) {
        this.wocoDsUtente = Functions.subString(wocoDsUtente, Len.WOCO_DS_UTENTE);
    }

    public String getWocoDsUtente() {
        return this.wocoDsUtente;
    }

    public void setWocoDsStatoElab(char wocoDsStatoElab) {
        this.wocoDsStatoElab = wocoDsStatoElab;
    }

    public char getWocoDsStatoElab() {
        return this.wocoDsStatoElab;
    }

    public void setWocoTpModAbbinamento(String wocoTpModAbbinamento) {
        this.wocoTpModAbbinamento = Functions.subString(wocoTpModAbbinamento, Len.WOCO_TP_MOD_ABBINAMENTO);
    }

    public String getWocoTpModAbbinamento() {
        return this.wocoTpModAbbinamento;
    }

    public WocoCarAcq getWocoCarAcq() {
        return wocoCarAcq;
    }

    public WocoDtDecor getWocoDtDecor() {
        return wocoDtDecor;
    }

    public WocoDtScad getWocoDtScad() {
        return wocoDtScad;
    }

    public WocoDtUltPrePag getWocoDtUltPrePag() {
        return wocoDtUltPrePag;
    }

    public WocoIdMoviChiu getWocoIdMoviChiu() {
        return wocoIdMoviChiu;
    }

    public WocoImpCollg getWocoImpCollg() {
        return wocoImpCollg;
    }

    public WocoImpReinvst getWocoImpReinvst() {
        return wocoImpReinvst;
    }

    public WocoImpTrasf getWocoImpTrasf() {
        return wocoImpTrasf;
    }

    public WocoImpTrasferito getWocoImpTrasferito() {
        return wocoImpTrasferito;
    }

    public WocoPcPreTrasferito getWocoPcPreTrasferito() {
        return wocoPcPreTrasferito;
    }

    public WocoPcReinvstRilievi getWocoPcReinvstRilievi() {
        return wocoPcReinvstRilievi;
    }

    public WocoPre1aAnnualita getWocoPre1aAnnualita() {
        return wocoPre1aAnnualita;
    }

    public WocoPrePerTrasf getWocoPrePerTrasf() {
        return wocoPrePerTrasf;
    }

    public WocoRecProv getWocoRecProv() {
        return wocoRecProv;
    }

    public WocoRisMat getWocoRisMat() {
        return wocoRisMat;
    }

    public WocoRisZil getWocoRisZil() {
        return wocoRisZil;
    }

    public WocoTotPre getWocoTotPre() {
        return wocoTotPre;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_ID_OGG_COLLG = 5;
        public static final int WOCO_ID_OGG_COINV = 5;
        public static final int WOCO_TP_OGG_COINV = 2;
        public static final int WOCO_ID_OGG_DER = 5;
        public static final int WOCO_TP_OGG_DER = 2;
        public static final int WOCO_IB_RIFTO_ESTNO = 40;
        public static final int WOCO_ID_MOVI_CRZ = 5;
        public static final int WOCO_DT_INI_EFF = 5;
        public static final int WOCO_DT_END_EFF = 5;
        public static final int WOCO_COD_COMP_ANIA = 3;
        public static final int WOCO_COD_PROD = 12;
        public static final int WOCO_TP_COLLGM = 2;
        public static final int WOCO_TP_TRASF = 2;
        public static final int WOCO_IB2O_RIFTO_ESTNO = 40;
        public static final int WOCO_IND_LIQ_AGG_MAN = 1;
        public static final int WOCO_DS_RIGA = 6;
        public static final int WOCO_DS_OPER_SQL = 1;
        public static final int WOCO_DS_VER = 5;
        public static final int WOCO_DS_TS_INI_CPTZ = 10;
        public static final int WOCO_DS_TS_END_CPTZ = 10;
        public static final int WOCO_DS_UTENTE = 20;
        public static final int WOCO_DS_STATO_ELAB = 1;
        public static final int WOCO_TP_MOD_ABBINAMENTO = 2;
        public static final int DATI = WOCO_ID_OGG_COLLG + WOCO_ID_OGG_COINV + WOCO_TP_OGG_COINV + WOCO_ID_OGG_DER + WOCO_TP_OGG_DER + WOCO_IB_RIFTO_ESTNO + WOCO_ID_MOVI_CRZ + WocoIdMoviChiu.Len.WOCO_ID_MOVI_CHIU + WOCO_DT_INI_EFF + WOCO_DT_END_EFF + WOCO_COD_COMP_ANIA + WOCO_COD_PROD + WocoDtScad.Len.WOCO_DT_SCAD + WOCO_TP_COLLGM + WOCO_TP_TRASF + WocoRecProv.Len.WOCO_REC_PROV + WocoDtDecor.Len.WOCO_DT_DECOR + WocoDtUltPrePag.Len.WOCO_DT_ULT_PRE_PAG + WocoTotPre.Len.WOCO_TOT_PRE + WocoRisMat.Len.WOCO_RIS_MAT + WocoRisZil.Len.WOCO_RIS_ZIL + WocoImpTrasf.Len.WOCO_IMP_TRASF + WocoImpReinvst.Len.WOCO_IMP_REINVST + WocoPcReinvstRilievi.Len.WOCO_PC_REINVST_RILIEVI + WOCO_IB2O_RIFTO_ESTNO + WOCO_IND_LIQ_AGG_MAN + WOCO_DS_RIGA + WOCO_DS_OPER_SQL + WOCO_DS_VER + WOCO_DS_TS_INI_CPTZ + WOCO_DS_TS_END_CPTZ + WOCO_DS_UTENTE + WOCO_DS_STATO_ELAB + WocoImpCollg.Len.WOCO_IMP_COLLG + WocoPrePerTrasf.Len.WOCO_PRE_PER_TRASF + WocoCarAcq.Len.WOCO_CAR_ACQ + WocoPre1aAnnualita.Len.WOCO_PRE1A_ANNUALITA + WocoImpTrasferito.Len.WOCO_IMP_TRASFERITO + WOCO_TP_MOD_ABBINAMENTO + WocoPcPreTrasferito.Len.WOCO_PC_PRE_TRASFERITO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_ID_OGG_COLLG = 9;
            public static final int WOCO_ID_OGG_COINV = 9;
            public static final int WOCO_ID_OGG_DER = 9;
            public static final int WOCO_ID_MOVI_CRZ = 9;
            public static final int WOCO_DT_INI_EFF = 8;
            public static final int WOCO_DT_END_EFF = 8;
            public static final int WOCO_COD_COMP_ANIA = 5;
            public static final int WOCO_DS_RIGA = 10;
            public static final int WOCO_DS_VER = 9;
            public static final int WOCO_DS_TS_INI_CPTZ = 18;
            public static final int WOCO_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
