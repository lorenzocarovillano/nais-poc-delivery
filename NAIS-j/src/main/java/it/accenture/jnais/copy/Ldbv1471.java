package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: LDBV1471<br>
 * Variable: LDBV1471 from copybook LDBV1471<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv1471 {

    //==== PROPERTIES ====
    //Original name: LDBV1471-TP-MOVI-01
    private int movi01 = DefaultValues.INT_VAL;
    //Original name: LDBV1471-TP-MOVI-02
    private int movi02 = DefaultValues.INT_VAL;
    //Original name: LDBV1471-TP-MOVI-03
    private int movi03 = DefaultValues.INT_VAL;
    //Original name: LDBV1471-TP-MOVI-04
    private int movi04 = DefaultValues.INT_VAL;
    //Original name: LDBV1471-TP-MOVI-05
    private int movi05 = DefaultValues.INT_VAL;
    //Original name: LDBV1471-TP-MOVI-06
    private int movi06 = DefaultValues.INT_VAL;
    //Original name: LDBV1471-TP-MOVI-07
    private int movi07 = DefaultValues.INT_VAL;
    //Original name: LDBV1471-TP-MOVI-08
    private int movi08 = DefaultValues.INT_VAL;
    //Original name: LDBV1471-TP-MOVI-09
    private int movi09 = DefaultValues.INT_VAL;
    //Original name: LDBV1471-TP-MOVI-10
    private int movi10 = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbv1471Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV1471];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV1471);
        setLdbv1471Bytes(buffer, 1);
    }

    public String getLdbv1471Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv1471Bytes());
    }

    public byte[] getLdbv1471Bytes() {
        byte[] buffer = new byte[Len.LDBV1471];
        return getLdbv1471Bytes(buffer, 1);
    }

    public void setLdbv1471Bytes(byte[] buffer, int offset) {
        int position = offset;
        movi01 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOVI01, 0);
        position += Len.MOVI01;
        movi02 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOVI02, 0);
        position += Len.MOVI02;
        movi03 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOVI03, 0);
        position += Len.MOVI03;
        movi04 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOVI04, 0);
        position += Len.MOVI04;
        movi05 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOVI05, 0);
        position += Len.MOVI05;
        movi06 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOVI06, 0);
        position += Len.MOVI06;
        movi07 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOVI07, 0);
        position += Len.MOVI07;
        movi08 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOVI08, 0);
        position += Len.MOVI08;
        movi09 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOVI09, 0);
        position += Len.MOVI09;
        movi10 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MOVI10, 0);
    }

    public byte[] getLdbv1471Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, movi01, Len.Int.MOVI01, 0);
        position += Len.MOVI01;
        MarshalByte.writeIntAsPacked(buffer, position, movi02, Len.Int.MOVI02, 0);
        position += Len.MOVI02;
        MarshalByte.writeIntAsPacked(buffer, position, movi03, Len.Int.MOVI03, 0);
        position += Len.MOVI03;
        MarshalByte.writeIntAsPacked(buffer, position, movi04, Len.Int.MOVI04, 0);
        position += Len.MOVI04;
        MarshalByte.writeIntAsPacked(buffer, position, movi05, Len.Int.MOVI05, 0);
        position += Len.MOVI05;
        MarshalByte.writeIntAsPacked(buffer, position, movi06, Len.Int.MOVI06, 0);
        position += Len.MOVI06;
        MarshalByte.writeIntAsPacked(buffer, position, movi07, Len.Int.MOVI07, 0);
        position += Len.MOVI07;
        MarshalByte.writeIntAsPacked(buffer, position, movi08, Len.Int.MOVI08, 0);
        position += Len.MOVI08;
        MarshalByte.writeIntAsPacked(buffer, position, movi09, Len.Int.MOVI09, 0);
        position += Len.MOVI09;
        MarshalByte.writeIntAsPacked(buffer, position, movi10, Len.Int.MOVI10, 0);
        return buffer;
    }

    public void setMovi01(int movi01) {
        this.movi01 = movi01;
    }

    public void setMovi01Formatted(String movi01) {
        setMovi01(PicParser.display(new PicParams("S9(5)").setUsage(PicUsage.PACKED)).parseInt(movi01));
    }

    public int getMovi01() {
        return this.movi01;
    }

    public void setMovi02(int movi02) {
        this.movi02 = movi02;
    }

    public void setMovi02Formatted(String movi02) {
        setMovi02(PicParser.display(new PicParams("S9(5)").setUsage(PicUsage.PACKED)).parseInt(movi02));
    }

    public int getMovi02() {
        return this.movi02;
    }

    public void setMovi03(int movi03) {
        this.movi03 = movi03;
    }

    public void setMovi03Formatted(String movi03) {
        setMovi03(PicParser.display(new PicParams("S9(5)").setUsage(PicUsage.PACKED)).parseInt(movi03));
    }

    public int getMovi03() {
        return this.movi03;
    }

    public void setMovi04(int movi04) {
        this.movi04 = movi04;
    }

    public int getMovi04() {
        return this.movi04;
    }

    public void setMovi05(int movi05) {
        this.movi05 = movi05;
    }

    public int getMovi05() {
        return this.movi05;
    }

    public void setMovi06(int movi06) {
        this.movi06 = movi06;
    }

    public int getMovi06() {
        return this.movi06;
    }

    public void setMovi07(int movi07) {
        this.movi07 = movi07;
    }

    public int getMovi07() {
        return this.movi07;
    }

    public void setMovi08(int movi08) {
        this.movi08 = movi08;
    }

    public int getMovi08() {
        return this.movi08;
    }

    public void setMovi09(int movi09) {
        this.movi09 = movi09;
    }

    public int getMovi09() {
        return this.movi09;
    }

    public void setMovi10(int movi10) {
        this.movi10 = movi10;
    }

    public int getMovi10() {
        return this.movi10;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MOVI01 = 3;
        public static final int MOVI02 = 3;
        public static final int MOVI03 = 3;
        public static final int MOVI04 = 3;
        public static final int MOVI05 = 3;
        public static final int MOVI06 = 3;
        public static final int MOVI07 = 3;
        public static final int MOVI08 = 3;
        public static final int MOVI09 = 3;
        public static final int MOVI10 = 3;
        public static final int LDBV1471 = MOVI01 + MOVI02 + MOVI03 + MOVI04 + MOVI05 + MOVI06 + MOVI07 + MOVI08 + MOVI09 + MOVI10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MOVI01 = 5;
            public static final int MOVI02 = 5;
            public static final int MOVI03 = 5;
            public static final int MOVI04 = 5;
            public static final int MOVI05 = 5;
            public static final int MOVI06 = 5;
            public static final int MOVI07 = 5;
            public static final int MOVI08 = 5;
            public static final int MOVI09 = 5;
            public static final int MOVI10 = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
