package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WmovIdMoviAnn;
import it.accenture.jnais.ws.redefines.WmovIdMoviCollg;
import it.accenture.jnais.ws.redefines.WmovIdOgg;
import it.accenture.jnais.ws.redefines.WmovIdRich;
import it.accenture.jnais.ws.redefines.WmovTpMovi;

/**Original name: WMOV-DATI<br>
 * Variable: WMOV-DATI from copybook LCCVMOV1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WmovDati {

    //==== PROPERTIES ====
    //Original name: WMOV-ID-MOVI
    private int wmovIdMovi = DefaultValues.INT_VAL;
    //Original name: WMOV-COD-COMP-ANIA
    private int wmovCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WMOV-ID-OGG
    private WmovIdOgg wmovIdOgg = new WmovIdOgg();
    //Original name: WMOV-IB-OGG
    private String wmovIbOgg = DefaultValues.stringVal(Len.WMOV_IB_OGG);
    //Original name: WMOV-IB-MOVI
    private String wmovIbMovi = DefaultValues.stringVal(Len.WMOV_IB_MOVI);
    //Original name: WMOV-TP-OGG
    private String wmovTpOgg = DefaultValues.stringVal(Len.WMOV_TP_OGG);
    //Original name: WMOV-ID-RICH
    private WmovIdRich wmovIdRich = new WmovIdRich();
    //Original name: WMOV-TP-MOVI
    private WmovTpMovi wmovTpMovi = new WmovTpMovi();
    //Original name: WMOV-DT-EFF
    private int wmovDtEff = DefaultValues.INT_VAL;
    //Original name: WMOV-ID-MOVI-ANN
    private WmovIdMoviAnn wmovIdMoviAnn = new WmovIdMoviAnn();
    //Original name: WMOV-ID-MOVI-COLLG
    private WmovIdMoviCollg wmovIdMoviCollg = new WmovIdMoviCollg();
    //Original name: WMOV-DS-OPER-SQL
    private char wmovDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WMOV-DS-VER
    private int wmovDsVer = DefaultValues.INT_VAL;
    //Original name: WMOV-DS-TS-CPTZ
    private long wmovDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WMOV-DS-UTENTE
    private String wmovDsUtente = DefaultValues.stringVal(Len.WMOV_DS_UTENTE);
    //Original name: WMOV-DS-STATO-ELAB
    private char wmovDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public void setDatiBytes(byte[] buffer) {
        setDatiBytes(buffer, 1);
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wmovIdMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WMOV_ID_MOVI, 0);
        position += Len.WMOV_ID_MOVI;
        wmovCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WMOV_COD_COMP_ANIA, 0);
        position += Len.WMOV_COD_COMP_ANIA;
        wmovIdOgg.setWmovIdOggFromBuffer(buffer, position);
        position += WmovIdOgg.Len.WMOV_ID_OGG;
        wmovIbOgg = MarshalByte.readString(buffer, position, Len.WMOV_IB_OGG);
        position += Len.WMOV_IB_OGG;
        wmovIbMovi = MarshalByte.readString(buffer, position, Len.WMOV_IB_MOVI);
        position += Len.WMOV_IB_MOVI;
        wmovTpOgg = MarshalByte.readString(buffer, position, Len.WMOV_TP_OGG);
        position += Len.WMOV_TP_OGG;
        wmovIdRich.setWmovIdRichFromBuffer(buffer, position);
        position += WmovIdRich.Len.WMOV_ID_RICH;
        wmovTpMovi.setWmovTpMoviFromBuffer(buffer, position);
        position += WmovTpMovi.Len.WMOV_TP_MOVI;
        wmovDtEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WMOV_DT_EFF, 0);
        position += Len.WMOV_DT_EFF;
        wmovIdMoviAnn.setWmovIdMoviAnnFromBuffer(buffer, position);
        position += WmovIdMoviAnn.Len.WMOV_ID_MOVI_ANN;
        wmovIdMoviCollg.setWmovIdMoviCollgFromBuffer(buffer, position);
        position += WmovIdMoviCollg.Len.WMOV_ID_MOVI_COLLG;
        wmovDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wmovDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WMOV_DS_VER, 0);
        position += Len.WMOV_DS_VER;
        wmovDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WMOV_DS_TS_CPTZ, 0);
        position += Len.WMOV_DS_TS_CPTZ;
        wmovDsUtente = MarshalByte.readString(buffer, position, Len.WMOV_DS_UTENTE);
        position += Len.WMOV_DS_UTENTE;
        wmovDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wmovIdMovi, Len.Int.WMOV_ID_MOVI, 0);
        position += Len.WMOV_ID_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, wmovCodCompAnia, Len.Int.WMOV_COD_COMP_ANIA, 0);
        position += Len.WMOV_COD_COMP_ANIA;
        wmovIdOgg.getWmovIdOggAsBuffer(buffer, position);
        position += WmovIdOgg.Len.WMOV_ID_OGG;
        MarshalByte.writeString(buffer, position, wmovIbOgg, Len.WMOV_IB_OGG);
        position += Len.WMOV_IB_OGG;
        MarshalByte.writeString(buffer, position, wmovIbMovi, Len.WMOV_IB_MOVI);
        position += Len.WMOV_IB_MOVI;
        MarshalByte.writeString(buffer, position, wmovTpOgg, Len.WMOV_TP_OGG);
        position += Len.WMOV_TP_OGG;
        wmovIdRich.getWmovIdRichAsBuffer(buffer, position);
        position += WmovIdRich.Len.WMOV_ID_RICH;
        wmovTpMovi.getWmovTpMoviAsBuffer(buffer, position);
        position += WmovTpMovi.Len.WMOV_TP_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, wmovDtEff, Len.Int.WMOV_DT_EFF, 0);
        position += Len.WMOV_DT_EFF;
        wmovIdMoviAnn.getWmovIdMoviAnnAsBuffer(buffer, position);
        position += WmovIdMoviAnn.Len.WMOV_ID_MOVI_ANN;
        wmovIdMoviCollg.getWmovIdMoviCollgAsBuffer(buffer, position);
        position += WmovIdMoviCollg.Len.WMOV_ID_MOVI_COLLG;
        MarshalByte.writeChar(buffer, position, wmovDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wmovDsVer, Len.Int.WMOV_DS_VER, 0);
        position += Len.WMOV_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wmovDsTsCptz, Len.Int.WMOV_DS_TS_CPTZ, 0);
        position += Len.WMOV_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wmovDsUtente, Len.WMOV_DS_UTENTE);
        position += Len.WMOV_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wmovDsStatoElab);
        return buffer;
    }

    public void setWmovIdMovi(int wmovIdMovi) {
        this.wmovIdMovi = wmovIdMovi;
    }

    public int getWmovIdMovi() {
        return this.wmovIdMovi;
    }

    public void setWmovCodCompAnia(int wmovCodCompAnia) {
        this.wmovCodCompAnia = wmovCodCompAnia;
    }

    public int getWmovCodCompAnia() {
        return this.wmovCodCompAnia;
    }

    public void setWmovIbOgg(String wmovIbOgg) {
        this.wmovIbOgg = Functions.subString(wmovIbOgg, Len.WMOV_IB_OGG);
    }

    public String getWmovIbOgg() {
        return this.wmovIbOgg;
    }

    public void setWmovIbMovi(String wmovIbMovi) {
        this.wmovIbMovi = Functions.subString(wmovIbMovi, Len.WMOV_IB_MOVI);
    }

    public String getWmovIbMovi() {
        return this.wmovIbMovi;
    }

    public void setWmovTpOgg(String wmovTpOgg) {
        this.wmovTpOgg = Functions.subString(wmovTpOgg, Len.WMOV_TP_OGG);
    }

    public String getWmovTpOgg() {
        return this.wmovTpOgg;
    }

    public String getWmovTpOggFormatted() {
        return Functions.padBlanks(getWmovTpOgg(), Len.WMOV_TP_OGG);
    }

    public void setWmovDtEff(int wmovDtEff) {
        this.wmovDtEff = wmovDtEff;
    }

    public int getWmovDtEff() {
        return this.wmovDtEff;
    }

    public void setWmovDsOperSql(char wmovDsOperSql) {
        this.wmovDsOperSql = wmovDsOperSql;
    }

    public char getWmovDsOperSql() {
        return this.wmovDsOperSql;
    }

    public void setWmovDsVer(int wmovDsVer) {
        this.wmovDsVer = wmovDsVer;
    }

    public int getWmovDsVer() {
        return this.wmovDsVer;
    }

    public void setWmovDsTsCptz(long wmovDsTsCptz) {
        this.wmovDsTsCptz = wmovDsTsCptz;
    }

    public long getWmovDsTsCptz() {
        return this.wmovDsTsCptz;
    }

    public void setWmovDsUtente(String wmovDsUtente) {
        this.wmovDsUtente = Functions.subString(wmovDsUtente, Len.WMOV_DS_UTENTE);
    }

    public String getWmovDsUtente() {
        return this.wmovDsUtente;
    }

    public void setWmovDsStatoElab(char wmovDsStatoElab) {
        this.wmovDsStatoElab = wmovDsStatoElab;
    }

    public char getWmovDsStatoElab() {
        return this.wmovDsStatoElab;
    }

    public WmovIdMoviAnn getWmovIdMoviAnn() {
        return wmovIdMoviAnn;
    }

    public WmovIdMoviCollg getWmovIdMoviCollg() {
        return wmovIdMoviCollg;
    }

    public WmovIdOgg getWmovIdOgg() {
        return wmovIdOgg;
    }

    public WmovIdRich getWmovIdRich() {
        return wmovIdRich;
    }

    public WmovTpMovi getWmovTpMovi() {
        return wmovTpMovi;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WMOV_ID_MOVI = 5;
        public static final int WMOV_COD_COMP_ANIA = 3;
        public static final int WMOV_IB_OGG = 40;
        public static final int WMOV_IB_MOVI = 40;
        public static final int WMOV_TP_OGG = 2;
        public static final int WMOV_DT_EFF = 5;
        public static final int WMOV_DS_OPER_SQL = 1;
        public static final int WMOV_DS_VER = 5;
        public static final int WMOV_DS_TS_CPTZ = 10;
        public static final int WMOV_DS_UTENTE = 20;
        public static final int WMOV_DS_STATO_ELAB = 1;
        public static final int DATI = WMOV_ID_MOVI + WMOV_COD_COMP_ANIA + WmovIdOgg.Len.WMOV_ID_OGG + WMOV_IB_OGG + WMOV_IB_MOVI + WMOV_TP_OGG + WmovIdRich.Len.WMOV_ID_RICH + WmovTpMovi.Len.WMOV_TP_MOVI + WMOV_DT_EFF + WmovIdMoviAnn.Len.WMOV_ID_MOVI_ANN + WmovIdMoviCollg.Len.WMOV_ID_MOVI_COLLG + WMOV_DS_OPER_SQL + WMOV_DS_VER + WMOV_DS_TS_CPTZ + WMOV_DS_UTENTE + WMOV_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMOV_ID_MOVI = 9;
            public static final int WMOV_COD_COMP_ANIA = 5;
            public static final int WMOV_DT_EFF = 8;
            public static final int WMOV_DS_VER = 9;
            public static final int WMOV_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
