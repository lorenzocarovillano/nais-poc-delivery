package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WdtrAcqExp;
import it.accenture.jnais.ws.redefines.WdtrCarAcq;
import it.accenture.jnais.ws.redefines.WdtrCarGest;
import it.accenture.jnais.ws.redefines.WdtrCarIas;
import it.accenture.jnais.ws.redefines.WdtrCarInc;
import it.accenture.jnais.ws.redefines.WdtrCnbtAntirac;
import it.accenture.jnais.ws.redefines.WdtrCommisInter;
import it.accenture.jnais.ws.redefines.WdtrDir;
import it.accenture.jnais.ws.redefines.WdtrDtEndCop;
import it.accenture.jnais.ws.redefines.WdtrDtIniCop;
import it.accenture.jnais.ws.redefines.WdtrFrqMovi;
import it.accenture.jnais.ws.redefines.WdtrIdMoviChiu;
import it.accenture.jnais.ws.redefines.WdtrImpAder;
import it.accenture.jnais.ws.redefines.WdtrImpAz;
import it.accenture.jnais.ws.redefines.WdtrImpTfr;
import it.accenture.jnais.ws.redefines.WdtrImpTfrStrc;
import it.accenture.jnais.ws.redefines.WdtrImpTrasfe;
import it.accenture.jnais.ws.redefines.WdtrImpVolo;
import it.accenture.jnais.ws.redefines.WdtrIntrFraz;
import it.accenture.jnais.ws.redefines.WdtrIntrMora;
import it.accenture.jnais.ws.redefines.WdtrIntrRetdt;
import it.accenture.jnais.ws.redefines.WdtrIntrRiat;
import it.accenture.jnais.ws.redefines.WdtrManfeeAntic;
import it.accenture.jnais.ws.redefines.WdtrManfeeRicor;
import it.accenture.jnais.ws.redefines.WdtrPreNet;
import it.accenture.jnais.ws.redefines.WdtrPrePpIas;
import it.accenture.jnais.ws.redefines.WdtrPreSoloRsh;
import it.accenture.jnais.ws.redefines.WdtrPreTot;
import it.accenture.jnais.ws.redefines.WdtrProvAcq1aa;
import it.accenture.jnais.ws.redefines.WdtrProvAcq2aa;
import it.accenture.jnais.ws.redefines.WdtrProvDaRec;
import it.accenture.jnais.ws.redefines.WdtrProvInc;
import it.accenture.jnais.ws.redefines.WdtrProvRicor;
import it.accenture.jnais.ws.redefines.WdtrRemunAss;
import it.accenture.jnais.ws.redefines.WdtrSoprAlt;
import it.accenture.jnais.ws.redefines.WdtrSoprProf;
import it.accenture.jnais.ws.redefines.WdtrSoprSan;
import it.accenture.jnais.ws.redefines.WdtrSoprSpo;
import it.accenture.jnais.ws.redefines.WdtrSoprTec;
import it.accenture.jnais.ws.redefines.WdtrSpeAge;
import it.accenture.jnais.ws.redefines.WdtrSpeMed;
import it.accenture.jnais.ws.redefines.WdtrTax;
import it.accenture.jnais.ws.redefines.WdtrTotIntrPrest;

/**Original name: WDTR-DATI<br>
 * Variable: WDTR-DATI from copybook LCCVDTR1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WdtrDati {

    //==== PROPERTIES ====
    //Original name: WDTR-ID-DETT-TIT-DI-RAT
    private int wdtrIdDettTitDiRat = DefaultValues.INT_VAL;
    //Original name: WDTR-ID-TIT-RAT
    private int wdtrIdTitRat = DefaultValues.INT_VAL;
    //Original name: WDTR-ID-OGG
    private int wdtrIdOgg = DefaultValues.INT_VAL;
    //Original name: WDTR-TP-OGG
    private String wdtrTpOgg = DefaultValues.stringVal(Len.WDTR_TP_OGG);
    //Original name: WDTR-ID-MOVI-CRZ
    private int wdtrIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WDTR-ID-MOVI-CHIU
    private WdtrIdMoviChiu wdtrIdMoviChiu = new WdtrIdMoviChiu();
    //Original name: WDTR-DT-INI-EFF
    private int wdtrDtIniEff = DefaultValues.INT_VAL;
    //Original name: WDTR-DT-END-EFF
    private int wdtrDtEndEff = DefaultValues.INT_VAL;
    //Original name: WDTR-COD-COMP-ANIA
    private int wdtrCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WDTR-DT-INI-COP
    private WdtrDtIniCop wdtrDtIniCop = new WdtrDtIniCop();
    //Original name: WDTR-DT-END-COP
    private WdtrDtEndCop wdtrDtEndCop = new WdtrDtEndCop();
    //Original name: WDTR-PRE-NET
    private WdtrPreNet wdtrPreNet = new WdtrPreNet();
    //Original name: WDTR-INTR-FRAZ
    private WdtrIntrFraz wdtrIntrFraz = new WdtrIntrFraz();
    //Original name: WDTR-INTR-MORA
    private WdtrIntrMora wdtrIntrMora = new WdtrIntrMora();
    //Original name: WDTR-INTR-RETDT
    private WdtrIntrRetdt wdtrIntrRetdt = new WdtrIntrRetdt();
    //Original name: WDTR-INTR-RIAT
    private WdtrIntrRiat wdtrIntrRiat = new WdtrIntrRiat();
    //Original name: WDTR-DIR
    private WdtrDir wdtrDir = new WdtrDir();
    //Original name: WDTR-SPE-MED
    private WdtrSpeMed wdtrSpeMed = new WdtrSpeMed();
    //Original name: WDTR-SPE-AGE
    private WdtrSpeAge wdtrSpeAge = new WdtrSpeAge();
    //Original name: WDTR-TAX
    private WdtrTax wdtrTax = new WdtrTax();
    //Original name: WDTR-SOPR-SAN
    private WdtrSoprSan wdtrSoprSan = new WdtrSoprSan();
    //Original name: WDTR-SOPR-SPO
    private WdtrSoprSpo wdtrSoprSpo = new WdtrSoprSpo();
    //Original name: WDTR-SOPR-TEC
    private WdtrSoprTec wdtrSoprTec = new WdtrSoprTec();
    //Original name: WDTR-SOPR-PROF
    private WdtrSoprProf wdtrSoprProf = new WdtrSoprProf();
    //Original name: WDTR-SOPR-ALT
    private WdtrSoprAlt wdtrSoprAlt = new WdtrSoprAlt();
    //Original name: WDTR-PRE-TOT
    private WdtrPreTot wdtrPreTot = new WdtrPreTot();
    //Original name: WDTR-PRE-PP-IAS
    private WdtrPrePpIas wdtrPrePpIas = new WdtrPrePpIas();
    //Original name: WDTR-PRE-SOLO-RSH
    private WdtrPreSoloRsh wdtrPreSoloRsh = new WdtrPreSoloRsh();
    //Original name: WDTR-CAR-IAS
    private WdtrCarIas wdtrCarIas = new WdtrCarIas();
    //Original name: WDTR-PROV-ACQ-1AA
    private WdtrProvAcq1aa wdtrProvAcq1aa = new WdtrProvAcq1aa();
    //Original name: WDTR-PROV-ACQ-2AA
    private WdtrProvAcq2aa wdtrProvAcq2aa = new WdtrProvAcq2aa();
    //Original name: WDTR-PROV-RICOR
    private WdtrProvRicor wdtrProvRicor = new WdtrProvRicor();
    //Original name: WDTR-PROV-INC
    private WdtrProvInc wdtrProvInc = new WdtrProvInc();
    //Original name: WDTR-PROV-DA-REC
    private WdtrProvDaRec wdtrProvDaRec = new WdtrProvDaRec();
    //Original name: WDTR-COD-DVS
    private String wdtrCodDvs = DefaultValues.stringVal(Len.WDTR_COD_DVS);
    //Original name: WDTR-FRQ-MOVI
    private WdtrFrqMovi wdtrFrqMovi = new WdtrFrqMovi();
    //Original name: WDTR-TP-RGM-FISC
    private String wdtrTpRgmFisc = DefaultValues.stringVal(Len.WDTR_TP_RGM_FISC);
    //Original name: WDTR-COD-TARI
    private String wdtrCodTari = DefaultValues.stringVal(Len.WDTR_COD_TARI);
    //Original name: WDTR-TP-STAT-TIT
    private String wdtrTpStatTit = DefaultValues.stringVal(Len.WDTR_TP_STAT_TIT);
    //Original name: WDTR-IMP-AZ
    private WdtrImpAz wdtrImpAz = new WdtrImpAz();
    //Original name: WDTR-IMP-ADER
    private WdtrImpAder wdtrImpAder = new WdtrImpAder();
    //Original name: WDTR-IMP-TFR
    private WdtrImpTfr wdtrImpTfr = new WdtrImpTfr();
    //Original name: WDTR-IMP-VOLO
    private WdtrImpVolo wdtrImpVolo = new WdtrImpVolo();
    //Original name: WDTR-FL-VLDT-TIT
    private char wdtrFlVldtTit = DefaultValues.CHAR_VAL;
    //Original name: WDTR-CAR-ACQ
    private WdtrCarAcq wdtrCarAcq = new WdtrCarAcq();
    //Original name: WDTR-CAR-GEST
    private WdtrCarGest wdtrCarGest = new WdtrCarGest();
    //Original name: WDTR-CAR-INC
    private WdtrCarInc wdtrCarInc = new WdtrCarInc();
    //Original name: WDTR-MANFEE-ANTIC
    private WdtrManfeeAntic wdtrManfeeAntic = new WdtrManfeeAntic();
    //Original name: WDTR-MANFEE-RICOR
    private WdtrManfeeRicor wdtrManfeeRicor = new WdtrManfeeRicor();
    //Original name: WDTR-MANFEE-REC
    private String wdtrManfeeRec = DefaultValues.stringVal(Len.WDTR_MANFEE_REC);
    //Original name: WDTR-TOT-INTR-PREST
    private WdtrTotIntrPrest wdtrTotIntrPrest = new WdtrTotIntrPrest();
    //Original name: WDTR-DS-RIGA
    private long wdtrDsRiga = DefaultValues.LONG_VAL;
    //Original name: WDTR-DS-OPER-SQL
    private char wdtrDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WDTR-DS-VER
    private int wdtrDsVer = DefaultValues.INT_VAL;
    //Original name: WDTR-DS-TS-INI-CPTZ
    private long wdtrDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WDTR-DS-TS-END-CPTZ
    private long wdtrDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WDTR-DS-UTENTE
    private String wdtrDsUtente = DefaultValues.stringVal(Len.WDTR_DS_UTENTE);
    //Original name: WDTR-DS-STATO-ELAB
    private char wdtrDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WDTR-IMP-TRASFE
    private WdtrImpTrasfe wdtrImpTrasfe = new WdtrImpTrasfe();
    //Original name: WDTR-IMP-TFR-STRC
    private WdtrImpTfrStrc wdtrImpTfrStrc = new WdtrImpTfrStrc();
    //Original name: WDTR-ACQ-EXP
    private WdtrAcqExp wdtrAcqExp = new WdtrAcqExp();
    //Original name: WDTR-REMUN-ASS
    private WdtrRemunAss wdtrRemunAss = new WdtrRemunAss();
    //Original name: WDTR-COMMIS-INTER
    private WdtrCommisInter wdtrCommisInter = new WdtrCommisInter();
    //Original name: WDTR-CNBT-ANTIRAC
    private WdtrCnbtAntirac wdtrCnbtAntirac = new WdtrCnbtAntirac();

    //==== METHODS ====
    public void setWdtrIdDettTitDiRat(int wdtrIdDettTitDiRat) {
        this.wdtrIdDettTitDiRat = wdtrIdDettTitDiRat;
    }

    public int getWdtrIdDettTitDiRat() {
        return this.wdtrIdDettTitDiRat;
    }

    public void setWdtrIdTitRat(int wdtrIdTitRat) {
        this.wdtrIdTitRat = wdtrIdTitRat;
    }

    public int getWdtrIdTitRat() {
        return this.wdtrIdTitRat;
    }

    public void setWdtrIdOgg(int wdtrIdOgg) {
        this.wdtrIdOgg = wdtrIdOgg;
    }

    public int getWdtrIdOgg() {
        return this.wdtrIdOgg;
    }

    public void setWdtrTpOgg(String wdtrTpOgg) {
        this.wdtrTpOgg = Functions.subString(wdtrTpOgg, Len.WDTR_TP_OGG);
    }

    public String getWdtrTpOgg() {
        return this.wdtrTpOgg;
    }

    public void setWdtrIdMoviCrz(int wdtrIdMoviCrz) {
        this.wdtrIdMoviCrz = wdtrIdMoviCrz;
    }

    public int getWdtrIdMoviCrz() {
        return this.wdtrIdMoviCrz;
    }

    public void setWdtrDtIniEff(int wdtrDtIniEff) {
        this.wdtrDtIniEff = wdtrDtIniEff;
    }

    public int getWdtrDtIniEff() {
        return this.wdtrDtIniEff;
    }

    public void setWdtrDtEndEff(int wdtrDtEndEff) {
        this.wdtrDtEndEff = wdtrDtEndEff;
    }

    public int getWdtrDtEndEff() {
        return this.wdtrDtEndEff;
    }

    public void setWdtrCodCompAnia(int wdtrCodCompAnia) {
        this.wdtrCodCompAnia = wdtrCodCompAnia;
    }

    public int getWdtrCodCompAnia() {
        return this.wdtrCodCompAnia;
    }

    public void setWdtrCodDvs(String wdtrCodDvs) {
        this.wdtrCodDvs = Functions.subString(wdtrCodDvs, Len.WDTR_COD_DVS);
    }

    public String getWdtrCodDvs() {
        return this.wdtrCodDvs;
    }

    public String getWdtrCodDvsFormatted() {
        return Functions.padBlanks(getWdtrCodDvs(), Len.WDTR_COD_DVS);
    }

    public void setWdtrTpRgmFisc(String wdtrTpRgmFisc) {
        this.wdtrTpRgmFisc = Functions.subString(wdtrTpRgmFisc, Len.WDTR_TP_RGM_FISC);
    }

    public String getWdtrTpRgmFisc() {
        return this.wdtrTpRgmFisc;
    }

    public void setWdtrCodTari(String wdtrCodTari) {
        this.wdtrCodTari = Functions.subString(wdtrCodTari, Len.WDTR_COD_TARI);
    }

    public String getWdtrCodTari() {
        return this.wdtrCodTari;
    }

    public String getWdtrCodTariFormatted() {
        return Functions.padBlanks(getWdtrCodTari(), Len.WDTR_COD_TARI);
    }

    public void setWdtrTpStatTit(String wdtrTpStatTit) {
        this.wdtrTpStatTit = Functions.subString(wdtrTpStatTit, Len.WDTR_TP_STAT_TIT);
    }

    public String getWdtrTpStatTit() {
        return this.wdtrTpStatTit;
    }

    public void setWdtrFlVldtTit(char wdtrFlVldtTit) {
        this.wdtrFlVldtTit = wdtrFlVldtTit;
    }

    public void setWdtrFlVldtTitFormatted(String wdtrFlVldtTit) {
        setWdtrFlVldtTit(Functions.charAt(wdtrFlVldtTit, Types.CHAR_SIZE));
    }

    public char getWdtrFlVldtTit() {
        return this.wdtrFlVldtTit;
    }

    public void setWdtrManfeeRec(String wdtrManfeeRec) {
        this.wdtrManfeeRec = Functions.subString(wdtrManfeeRec, Len.WDTR_MANFEE_REC);
    }

    public String getWdtrManfeeRec() {
        return this.wdtrManfeeRec;
    }

    public String getWdtrManfeeRecFormatted() {
        return Functions.padBlanks(getWdtrManfeeRec(), Len.WDTR_MANFEE_REC);
    }

    public void setWdtrDsRiga(long wdtrDsRiga) {
        this.wdtrDsRiga = wdtrDsRiga;
    }

    public long getWdtrDsRiga() {
        return this.wdtrDsRiga;
    }

    public void setWdtrDsOperSql(char wdtrDsOperSql) {
        this.wdtrDsOperSql = wdtrDsOperSql;
    }

    public char getWdtrDsOperSql() {
        return this.wdtrDsOperSql;
    }

    public void setWdtrDsVer(int wdtrDsVer) {
        this.wdtrDsVer = wdtrDsVer;
    }

    public int getWdtrDsVer() {
        return this.wdtrDsVer;
    }

    public void setWdtrDsTsIniCptz(long wdtrDsTsIniCptz) {
        this.wdtrDsTsIniCptz = wdtrDsTsIniCptz;
    }

    public long getWdtrDsTsIniCptz() {
        return this.wdtrDsTsIniCptz;
    }

    public void setWdtrDsTsEndCptz(long wdtrDsTsEndCptz) {
        this.wdtrDsTsEndCptz = wdtrDsTsEndCptz;
    }

    public long getWdtrDsTsEndCptz() {
        return this.wdtrDsTsEndCptz;
    }

    public void setWdtrDsUtente(String wdtrDsUtente) {
        this.wdtrDsUtente = Functions.subString(wdtrDsUtente, Len.WDTR_DS_UTENTE);
    }

    public String getWdtrDsUtente() {
        return this.wdtrDsUtente;
    }

    public void setWdtrDsStatoElab(char wdtrDsStatoElab) {
        this.wdtrDsStatoElab = wdtrDsStatoElab;
    }

    public char getWdtrDsStatoElab() {
        return this.wdtrDsStatoElab;
    }

    public WdtrAcqExp getWdtrAcqExp() {
        return wdtrAcqExp;
    }

    public WdtrCarAcq getWdtrCarAcq() {
        return wdtrCarAcq;
    }

    public WdtrCarGest getWdtrCarGest() {
        return wdtrCarGest;
    }

    public WdtrCarIas getWdtrCarIas() {
        return wdtrCarIas;
    }

    public WdtrCarInc getWdtrCarInc() {
        return wdtrCarInc;
    }

    public WdtrCnbtAntirac getWdtrCnbtAntirac() {
        return wdtrCnbtAntirac;
    }

    public WdtrCommisInter getWdtrCommisInter() {
        return wdtrCommisInter;
    }

    public WdtrDir getWdtrDir() {
        return wdtrDir;
    }

    public WdtrDtEndCop getWdtrDtEndCop() {
        return wdtrDtEndCop;
    }

    public WdtrDtIniCop getWdtrDtIniCop() {
        return wdtrDtIniCop;
    }

    public WdtrFrqMovi getWdtrFrqMovi() {
        return wdtrFrqMovi;
    }

    public WdtrIdMoviChiu getWdtrIdMoviChiu() {
        return wdtrIdMoviChiu;
    }

    public WdtrImpAder getWdtrImpAder() {
        return wdtrImpAder;
    }

    public WdtrImpAz getWdtrImpAz() {
        return wdtrImpAz;
    }

    public WdtrImpTfr getWdtrImpTfr() {
        return wdtrImpTfr;
    }

    public WdtrImpTfrStrc getWdtrImpTfrStrc() {
        return wdtrImpTfrStrc;
    }

    public WdtrImpTrasfe getWdtrImpTrasfe() {
        return wdtrImpTrasfe;
    }

    public WdtrImpVolo getWdtrImpVolo() {
        return wdtrImpVolo;
    }

    public WdtrIntrFraz getWdtrIntrFraz() {
        return wdtrIntrFraz;
    }

    public WdtrIntrMora getWdtrIntrMora() {
        return wdtrIntrMora;
    }

    public WdtrIntrRetdt getWdtrIntrRetdt() {
        return wdtrIntrRetdt;
    }

    public WdtrIntrRiat getWdtrIntrRiat() {
        return wdtrIntrRiat;
    }

    public WdtrManfeeAntic getWdtrManfeeAntic() {
        return wdtrManfeeAntic;
    }

    public WdtrManfeeRicor getWdtrManfeeRicor() {
        return wdtrManfeeRicor;
    }

    public WdtrPreNet getWdtrPreNet() {
        return wdtrPreNet;
    }

    public WdtrPrePpIas getWdtrPrePpIas() {
        return wdtrPrePpIas;
    }

    public WdtrPreSoloRsh getWdtrPreSoloRsh() {
        return wdtrPreSoloRsh;
    }

    public WdtrPreTot getWdtrPreTot() {
        return wdtrPreTot;
    }

    public WdtrProvAcq1aa getWdtrProvAcq1aa() {
        return wdtrProvAcq1aa;
    }

    public WdtrProvAcq2aa getWdtrProvAcq2aa() {
        return wdtrProvAcq2aa;
    }

    public WdtrProvDaRec getWdtrProvDaRec() {
        return wdtrProvDaRec;
    }

    public WdtrProvInc getWdtrProvInc() {
        return wdtrProvInc;
    }

    public WdtrProvRicor getWdtrProvRicor() {
        return wdtrProvRicor;
    }

    public WdtrRemunAss getWdtrRemunAss() {
        return wdtrRemunAss;
    }

    public WdtrSoprAlt getWdtrSoprAlt() {
        return wdtrSoprAlt;
    }

    public WdtrSoprProf getWdtrSoprProf() {
        return wdtrSoprProf;
    }

    public WdtrSoprSan getWdtrSoprSan() {
        return wdtrSoprSan;
    }

    public WdtrSoprSpo getWdtrSoprSpo() {
        return wdtrSoprSpo;
    }

    public WdtrSoprTec getWdtrSoprTec() {
        return wdtrSoprTec;
    }

    public WdtrSpeAge getWdtrSpeAge() {
        return wdtrSpeAge;
    }

    public WdtrSpeMed getWdtrSpeMed() {
        return wdtrSpeMed;
    }

    public WdtrTax getWdtrTax() {
        return wdtrTax;
    }

    public WdtrTotIntrPrest getWdtrTotIntrPrest() {
        return wdtrTotIntrPrest;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_TP_OGG = 2;
        public static final int WDTR_COD_DVS = 20;
        public static final int WDTR_TP_RGM_FISC = 2;
        public static final int WDTR_COD_TARI = 12;
        public static final int WDTR_TP_STAT_TIT = 2;
        public static final int WDTR_MANFEE_REC = 18;
        public static final int WDTR_DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
