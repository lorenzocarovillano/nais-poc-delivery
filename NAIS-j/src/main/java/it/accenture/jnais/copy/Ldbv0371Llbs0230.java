package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Ldbv0371DtcDir;
import it.accenture.jnais.ws.redefines.Ldbv0371DtcPreTot;
import it.accenture.jnais.ws.redefines.Ldbv0371Fraz;
import it.accenture.jnais.ws.redefines.Ldbv0371TitPreTot;

/**Original name: LDBV0371<br>
 * Variable: LDBV0371 from copybook LDBV0371<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbv0371Llbs0230 {

    //==== PROPERTIES ====
    //Original name: LDBV0371-ID-OGG
    private int ldbv0371IdOgg = DefaultValues.INT_VAL;
    //Original name: LDBV0371-TP-OGG
    private String ldbv0371TpOgg = DefaultValues.stringVal(Len.LDBV0371_TP_OGG);
    //Original name: LDBV0371-TP-PRE-TIT
    private String ldbv0371TpPreTit = DefaultValues.stringVal(Len.LDBV0371_TP_PRE_TIT);
    //Original name: LDBV0371-TP-STAT-TIT1
    private String ldbv0371TpStatTit1 = DefaultValues.stringVal(Len.LDBV0371_TP_STAT_TIT1);
    //Original name: LDBV0371-TP-STAT-TIT2
    private String ldbv0371TpStatTit2 = DefaultValues.stringVal(Len.LDBV0371_TP_STAT_TIT2);
    //Original name: LDBV0371-ID-TIT-CONT
    private int ldbv0371IdTitCont = DefaultValues.INT_VAL;
    //Original name: LDBV0371-ID-DETT-TIT-CONT
    private int ldbv0371IdDettTitCont = DefaultValues.INT_VAL;
    //Original name: LDBV0371-FRAZ
    private Ldbv0371Fraz ldbv0371Fraz = new Ldbv0371Fraz();
    //Original name: LDBV0371-TIT-PRE-TOT
    private Ldbv0371TitPreTot ldbv0371TitPreTot = new Ldbv0371TitPreTot();
    //Original name: LDBV0371-DTC-PRE-TOT
    private Ldbv0371DtcPreTot ldbv0371DtcPreTot = new Ldbv0371DtcPreTot();
    //Original name: LDBV0371-DTC-DIR
    private Ldbv0371DtcDir ldbv0371DtcDir = new Ldbv0371DtcDir();

    //==== METHODS ====
    public void setLdbv0371Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV0371];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV0371);
        setLdbv0371Bytes(buffer, 1);
    }

    public String getLdbv0371Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv0371Bytes());
    }

    public byte[] getLdbv0371Bytes() {
        byte[] buffer = new byte[Len.LDBV0371];
        return getLdbv0371Bytes(buffer, 1);
    }

    public void setLdbv0371Bytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaLdbv0371InBytes(buffer, position);
        position += Len.AREA_LDBV0371_IN;
        setAreaLdbv0371OutBytes(buffer, position);
    }

    public byte[] getLdbv0371Bytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaLdbv0371InBytes(buffer, position);
        position += Len.AREA_LDBV0371_IN;
        getAreaLdbv0371OutBytes(buffer, position);
        return buffer;
    }

    public void setAreaLdbv0371InBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv0371IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV0371_ID_OGG, 0);
        position += Len.LDBV0371_ID_OGG;
        ldbv0371TpOgg = MarshalByte.readString(buffer, position, Len.LDBV0371_TP_OGG);
        position += Len.LDBV0371_TP_OGG;
        ldbv0371TpPreTit = MarshalByte.readString(buffer, position, Len.LDBV0371_TP_PRE_TIT);
        position += Len.LDBV0371_TP_PRE_TIT;
        ldbv0371TpStatTit1 = MarshalByte.readString(buffer, position, Len.LDBV0371_TP_STAT_TIT1);
        position += Len.LDBV0371_TP_STAT_TIT1;
        ldbv0371TpStatTit2 = MarshalByte.readString(buffer, position, Len.LDBV0371_TP_STAT_TIT2);
    }

    public byte[] getAreaLdbv0371InBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv0371IdOgg, Len.Int.LDBV0371_ID_OGG, 0);
        position += Len.LDBV0371_ID_OGG;
        MarshalByte.writeString(buffer, position, ldbv0371TpOgg, Len.LDBV0371_TP_OGG);
        position += Len.LDBV0371_TP_OGG;
        MarshalByte.writeString(buffer, position, ldbv0371TpPreTit, Len.LDBV0371_TP_PRE_TIT);
        position += Len.LDBV0371_TP_PRE_TIT;
        MarshalByte.writeString(buffer, position, ldbv0371TpStatTit1, Len.LDBV0371_TP_STAT_TIT1);
        position += Len.LDBV0371_TP_STAT_TIT1;
        MarshalByte.writeString(buffer, position, ldbv0371TpStatTit2, Len.LDBV0371_TP_STAT_TIT2);
        return buffer;
    }

    public void setLdbv0371IdOgg(int ldbv0371IdOgg) {
        this.ldbv0371IdOgg = ldbv0371IdOgg;
    }

    public int getLdbv0371IdOgg() {
        return this.ldbv0371IdOgg;
    }

    public void setLdbv0371TpOgg(String ldbv0371TpOgg) {
        this.ldbv0371TpOgg = Functions.subString(ldbv0371TpOgg, Len.LDBV0371_TP_OGG);
    }

    public String getLdbv0371TpOgg() {
        return this.ldbv0371TpOgg;
    }

    public void setLdbv0371TpPreTit(String ldbv0371TpPreTit) {
        this.ldbv0371TpPreTit = Functions.subString(ldbv0371TpPreTit, Len.LDBV0371_TP_PRE_TIT);
    }

    public String getLdbv0371TpPreTit() {
        return this.ldbv0371TpPreTit;
    }

    public void setLdbv0371TpStatTit1(String ldbv0371TpStatTit1) {
        this.ldbv0371TpStatTit1 = Functions.subString(ldbv0371TpStatTit1, Len.LDBV0371_TP_STAT_TIT1);
    }

    public String getLdbv0371TpStatTit1() {
        return this.ldbv0371TpStatTit1;
    }

    public void setLdbv0371TpStatTit2(String ldbv0371TpStatTit2) {
        this.ldbv0371TpStatTit2 = Functions.subString(ldbv0371TpStatTit2, Len.LDBV0371_TP_STAT_TIT2);
    }

    public String getLdbv0371TpStatTit2() {
        return this.ldbv0371TpStatTit2;
    }

    public void setAreaLdbv0371OutBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv0371IdTitCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV0371_ID_TIT_CONT, 0);
        position += Len.LDBV0371_ID_TIT_CONT;
        ldbv0371IdDettTitCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV0371_ID_DETT_TIT_CONT, 0);
        position += Len.LDBV0371_ID_DETT_TIT_CONT;
        ldbv0371Fraz.setLdbv0371FrazFromBuffer(buffer, position);
        position += Ldbv0371Fraz.Len.LDBV0371_FRAZ;
        ldbv0371TitPreTot.setLdbv0371TitPreTotFromBuffer(buffer, position);
        position += Ldbv0371TitPreTot.Len.LDBV0371_TIT_PRE_TOT;
        ldbv0371DtcPreTot.setLdbv0371DtcPreTotFromBuffer(buffer, position);
        position += Ldbv0371DtcPreTot.Len.LDBV0371_DTC_PRE_TOT;
        ldbv0371DtcDir.setLdbv0371DtcDirFromBuffer(buffer, position);
    }

    public byte[] getAreaLdbv0371OutBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv0371IdTitCont, Len.Int.LDBV0371_ID_TIT_CONT, 0);
        position += Len.LDBV0371_ID_TIT_CONT;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv0371IdDettTitCont, Len.Int.LDBV0371_ID_DETT_TIT_CONT, 0);
        position += Len.LDBV0371_ID_DETT_TIT_CONT;
        ldbv0371Fraz.getLdbv0371FrazAsBuffer(buffer, position);
        position += Ldbv0371Fraz.Len.LDBV0371_FRAZ;
        ldbv0371TitPreTot.getLdbv0371TitPreTotAsBuffer(buffer, position);
        position += Ldbv0371TitPreTot.Len.LDBV0371_TIT_PRE_TOT;
        ldbv0371DtcPreTot.getLdbv0371DtcPreTotAsBuffer(buffer, position);
        position += Ldbv0371DtcPreTot.Len.LDBV0371_DTC_PRE_TOT;
        ldbv0371DtcDir.getLdbv0371DtcDirAsBuffer(buffer, position);
        return buffer;
    }

    public void setLdbv0371IdTitCont(int ldbv0371IdTitCont) {
        this.ldbv0371IdTitCont = ldbv0371IdTitCont;
    }

    public int getLdbv0371IdTitCont() {
        return this.ldbv0371IdTitCont;
    }

    public void setLdbv0371IdDettTitCont(int ldbv0371IdDettTitCont) {
        this.ldbv0371IdDettTitCont = ldbv0371IdDettTitCont;
    }

    public int getLdbv0371IdDettTitCont() {
        return this.ldbv0371IdDettTitCont;
    }

    public Ldbv0371DtcDir getLdbv0371DtcDir() {
        return ldbv0371DtcDir;
    }

    public Ldbv0371DtcPreTot getLdbv0371DtcPreTot() {
        return ldbv0371DtcPreTot;
    }

    public Ldbv0371Fraz getLdbv0371Fraz() {
        return ldbv0371Fraz;
    }

    public Ldbv0371TitPreTot getLdbv0371TitPreTot() {
        return ldbv0371TitPreTot;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV0371_TP_OGG = 2;
        public static final int LDBV0371_TP_PRE_TIT = 2;
        public static final int LDBV0371_TP_STAT_TIT1 = 2;
        public static final int LDBV0371_TP_STAT_TIT2 = 2;
        public static final int LDBV0371_ID_OGG = 5;
        public static final int AREA_LDBV0371_IN = LDBV0371_ID_OGG + LDBV0371_TP_OGG + LDBV0371_TP_PRE_TIT + LDBV0371_TP_STAT_TIT1 + LDBV0371_TP_STAT_TIT2;
        public static final int LDBV0371_ID_TIT_CONT = 5;
        public static final int LDBV0371_ID_DETT_TIT_CONT = 5;
        public static final int AREA_LDBV0371_OUT = LDBV0371_ID_TIT_CONT + LDBV0371_ID_DETT_TIT_CONT + Ldbv0371Fraz.Len.LDBV0371_FRAZ + Ldbv0371TitPreTot.Len.LDBV0371_TIT_PRE_TOT + Ldbv0371DtcPreTot.Len.LDBV0371_DTC_PRE_TOT + Ldbv0371DtcDir.Len.LDBV0371_DTC_DIR;
        public static final int LDBV0371 = AREA_LDBV0371_IN + AREA_LDBV0371_OUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV0371_ID_OGG = 9;
            public static final int LDBV0371_ID_TIT_CONT = 9;
            public static final int LDBV0371_ID_DETT_TIT_CONT = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
