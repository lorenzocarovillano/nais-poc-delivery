package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Ispc0040AdesSenzaAssic;
import it.accenture.jnais.ws.enums.Ispc0040DistintaContr;
import it.accenture.jnais.ws.enums.Ispc0040FlagCoperturaFin;
import it.accenture.jnais.ws.occurs.Ispc0040OperFormaReinvest;
import it.accenture.jnais.ws.occurs.Ispc0040TipoPremio;

/**Original name: ISPC0040-DATI-OUTPUT2<br>
 * Variable: ISPC0040-DATI-OUTPUT2 from copybook ISPC0040<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ispc0040DatiOutput2 {

    //==== PROPERTIES ====
    public static final int TIPO_PREMIO_MAXOCCURS = 3;
    public static final int OPER_FORMA_REINVEST_MAXOCCURS = 40;
    //Original name: ISPC0040-TRATTAMENTO-FONDO
    private char trattamentoFondo = DefaultValues.CHAR_VAL;
    /**Original name: ISPC0040-FLAG-COPERTURA-FIN<br>
	 * <pre>--       Copertura</pre>*/
    private Ispc0040FlagCoperturaFin flagCoperturaFin = new Ispc0040FlagCoperturaFin();
    /**Original name: ISPC0040-ADES-SENZA-ASSIC<br>
	 * <pre>--       Adesione senza testa assicurata</pre>*/
    private Ispc0040AdesSenzaAssic adesSenzaAssic = new Ispc0040AdesSenzaAssic();
    /**Original name: ISPC0040-FLAG-CUMULO-CONTR<br>
	 * <pre>--       Flag Cumulo Contraente</pre>*/
    private String flagCumuloContr = DefaultValues.stringVal(Len.FLAG_CUMULO_CONTR);
    //Original name: ISPC0040-FLAG-TRATT-DIR-EMIS
    private char flagTrattDirEmis = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-IMP-DIR-EMIS
    private AfDecimal impDirEmis = new AfDecimal(DefaultValues.DEC_VAL, 18, 3);
    //Original name: ISPC0040-FLAG-TRATT-DIR-QUIE
    private char flagTrattDirQuie = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-IMP-DIR-QUIE
    private AfDecimal impDirQuie = new AfDecimal(DefaultValues.DEC_VAL, 18, 3);
    //Original name: ISPC0040-FLAG-TRATT-DIR-1-VER
    private char flagTrattDir1Ver = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-IMP-DIR-1-VER
    private AfDecimal impDir1Ver = new AfDecimal(DefaultValues.DEC_VAL, 18, 3);
    //Original name: ISPC0040-FLAG-TRATT-VERS-AGG
    private char flagTrattVersAgg = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-IMP-DIR-VERS-AGG
    private AfDecimal impDirVersAgg = new AfDecimal(DefaultValues.DEC_VAL, 18, 3);
    //Original name: ISPC0040-FLAG-SPESE-MEDICHE
    private char flagSpeseMediche = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-IMP-SPESE-MEDICHE
    private AfDecimal impSpeseMediche = new AfDecimal(DefaultValues.DEC_VAL, 18, 3);
    /**Original name: ISPC0040-TP-PREMIO-NUM-E<br>
	 * <pre>--       Tipo Premio</pre>*/
    private String tpPremioNumE = DefaultValues.stringVal(Len.TP_PREMIO_NUM_E);
    //Original name: ISPC0040-TIPO-PREMIO
    private Ispc0040TipoPremio[] tipoPremio = new Ispc0040TipoPremio[TIPO_PREMIO_MAXOCCURS];
    //Original name: ISPC0040-TRATT-TIPO-PREMIO
    private char trattTipoPremio = DefaultValues.CHAR_VAL;
    /**Original name: ISPC0040-DISTINTA-CONTR<br>
	 * <pre>--       Distinta contributiva</pre>*/
    private Ispc0040DistintaContr distintaContr = new Ispc0040DistintaContr();
    /**Original name: ISPC0040-DATA-DECOR<br>
	 * <pre>--       Data Decorrenza ricalcolata</pre>*/
    private String dataDecor = DefaultValues.stringVal(Len.DATA_DECOR);
    //Original name: ISPC0040-DATA-DECOR-TRATT
    private char dataDecorTratt = DefaultValues.CHAR_VAL;
    /**Original name: ISPC0040-NUM-GG-REINVEST<br>
	 * <pre>--       Numero giorni da reinvestimento</pre>*/
    private String numGgReinvest = DefaultValues.stringVal(Len.NUM_GG_REINVEST);
    /**Original name: ISPC0040-MAX-OPER-FORM-INVEST<br>
	 * <pre>--       Tabella Reinvestimento</pre>*/
    private String maxOperFormInvest = DefaultValues.stringVal(Len.MAX_OPER_FORM_INVEST);
    //Original name: ISPC0040-OPER-FORMA-REINVEST
    private Ispc0040OperFormaReinvest[] operFormaReinvest = new Ispc0040OperFormaReinvest[OPER_FORMA_REINVEST_MAXOCCURS];
    /**Original name: ISPC0040-SOTTOCATEGORIA<br>
	 * <pre>--       Sottocategoria</pre>*/
    private String sottocategoria = DefaultValues.stringVal(Len.SOTTOCATEGORIA);

    //==== CONSTRUCTORS ====
    public Ispc0040DatiOutput2() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tipoPremioIdx = 1; tipoPremioIdx <= TIPO_PREMIO_MAXOCCURS; tipoPremioIdx++) {
            tipoPremio[tipoPremioIdx - 1] = new Ispc0040TipoPremio();
        }
        for (int operFormaReinvestIdx = 1; operFormaReinvestIdx <= OPER_FORMA_REINVEST_MAXOCCURS; operFormaReinvestIdx++) {
            operFormaReinvest[operFormaReinvestIdx - 1] = new Ispc0040OperFormaReinvest();
        }
    }

    public void setIspc0040DatiOutput2Bytes(byte[] buffer, int offset) {
        int position = offset;
        trattamentoFondo = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flagCoperturaFin.setFlagCoperturaFin(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        adesSenzaAssic.setAdesSenzaAssic(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        flagCumuloContr = MarshalByte.readFixedString(buffer, position, Len.FLAG_CUMULO_CONTR);
        position += Len.FLAG_CUMULO_CONTR;
        setDirittiEmissioneBytes(buffer, position);
        position += Len.DIRITTI_EMISSIONE;
        setDirittiQuietanzaBytes(buffer, position);
        position += Len.DIRITTI_QUIETANZA;
        setDirittiPrimoVersBytes(buffer, position);
        position += Len.DIRITTI_PRIMO_VERS;
        setDirittiVersAggBytes(buffer, position);
        position += Len.DIRITTI_VERS_AGG;
        setSpeseMedicheBytes(buffer, position);
        position += Len.SPESE_MEDICHE;
        tpPremioNumE = MarshalByte.readFixedString(buffer, position, Len.TP_PREMIO_NUM_E);
        position += Len.TP_PREMIO_NUM_E;
        for (int idx = 1; idx <= TIPO_PREMIO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tipoPremio[idx - 1].setTipoPremioBytes(buffer, position);
                position += Ispc0040TipoPremio.Len.TIPO_PREMIO;
            }
            else {
                tipoPremio[idx - 1].initTipoPremioSpaces();
                position += Ispc0040TipoPremio.Len.TIPO_PREMIO;
            }
        }
        trattTipoPremio = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        distintaContr.setDistintaContr(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        dataDecor = MarshalByte.readString(buffer, position, Len.DATA_DECOR);
        position += Len.DATA_DECOR;
        dataDecorTratt = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        numGgReinvest = MarshalByte.readFixedString(buffer, position, Len.NUM_GG_REINVEST);
        position += Len.NUM_GG_REINVEST;
        maxOperFormInvest = MarshalByte.readFixedString(buffer, position, Len.MAX_OPER_FORM_INVEST);
        position += Len.MAX_OPER_FORM_INVEST;
        for (int idx = 1; idx <= OPER_FORMA_REINVEST_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                operFormaReinvest[idx - 1].setOperFormaReinvestBytes(buffer, position);
                position += Ispc0040OperFormaReinvest.Len.OPER_FORMA_REINVEST;
            }
            else {
                operFormaReinvest[idx - 1].initOperFormaReinvestSpaces();
                position += Ispc0040OperFormaReinvest.Len.OPER_FORMA_REINVEST;
            }
        }
        sottocategoria = MarshalByte.readString(buffer, position, Len.SOTTOCATEGORIA);
    }

    public byte[] getIspc0040DatiOutput2Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, trattamentoFondo);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flagCoperturaFin.getFlagCoperturaFin());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, adesSenzaAssic.getAdesSenzaAssic());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, flagCumuloContr, Len.FLAG_CUMULO_CONTR);
        position += Len.FLAG_CUMULO_CONTR;
        getDirittiEmissioneBytes(buffer, position);
        position += Len.DIRITTI_EMISSIONE;
        getDirittiQuietanzaBytes(buffer, position);
        position += Len.DIRITTI_QUIETANZA;
        getDirittiPrimoVersBytes(buffer, position);
        position += Len.DIRITTI_PRIMO_VERS;
        getDirittiVersAggBytes(buffer, position);
        position += Len.DIRITTI_VERS_AGG;
        getSpeseMedicheBytes(buffer, position);
        position += Len.SPESE_MEDICHE;
        MarshalByte.writeString(buffer, position, tpPremioNumE, Len.TP_PREMIO_NUM_E);
        position += Len.TP_PREMIO_NUM_E;
        for (int idx = 1; idx <= TIPO_PREMIO_MAXOCCURS; idx++) {
            tipoPremio[idx - 1].getTipoPremioBytes(buffer, position);
            position += Ispc0040TipoPremio.Len.TIPO_PREMIO;
        }
        MarshalByte.writeChar(buffer, position, trattTipoPremio);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, distintaContr.getDistintaContr());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dataDecor, Len.DATA_DECOR);
        position += Len.DATA_DECOR;
        MarshalByte.writeChar(buffer, position, dataDecorTratt);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, numGgReinvest, Len.NUM_GG_REINVEST);
        position += Len.NUM_GG_REINVEST;
        MarshalByte.writeString(buffer, position, maxOperFormInvest, Len.MAX_OPER_FORM_INVEST);
        position += Len.MAX_OPER_FORM_INVEST;
        for (int idx = 1; idx <= OPER_FORMA_REINVEST_MAXOCCURS; idx++) {
            operFormaReinvest[idx - 1].getOperFormaReinvestBytes(buffer, position);
            position += Ispc0040OperFormaReinvest.Len.OPER_FORMA_REINVEST;
        }
        MarshalByte.writeString(buffer, position, sottocategoria, Len.SOTTOCATEGORIA);
        return buffer;
    }

    public void setTrattamentoFondo(char trattamentoFondo) {
        this.trattamentoFondo = trattamentoFondo;
    }

    public char getTrattamentoFondo() {
        return this.trattamentoFondo;
    }

    public void setIspc0040FlagCumuloContrFormatted(String ispc0040FlagCumuloContr) {
        this.flagCumuloContr = Trunc.toUnsignedNumeric(ispc0040FlagCumuloContr, Len.FLAG_CUMULO_CONTR);
    }

    public short getIspc0040FlagCumuloContr() {
        return NumericDisplay.asShort(this.flagCumuloContr);
    }

    public void setDirittiEmissioneBytes(byte[] buffer, int offset) {
        int position = offset;
        flagTrattDirEmis = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        impDirEmis.assign(MarshalByte.readDecimal(buffer, position, Len.Int.IMP_DIR_EMIS, Len.Fract.IMP_DIR_EMIS));
    }

    public byte[] getDirittiEmissioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, flagTrattDirEmis);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimal(buffer, position, impDirEmis.copy());
        return buffer;
    }

    public void setFlagTrattDirEmis(char flagTrattDirEmis) {
        this.flagTrattDirEmis = flagTrattDirEmis;
    }

    public char getFlagTrattDirEmis() {
        return this.flagTrattDirEmis;
    }

    public void setImpDirEmis(AfDecimal impDirEmis) {
        this.impDirEmis.assign(impDirEmis);
    }

    public AfDecimal getImpDirEmis() {
        return this.impDirEmis.copy();
    }

    public void setDirittiQuietanzaBytes(byte[] buffer, int offset) {
        int position = offset;
        flagTrattDirQuie = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        impDirQuie.assign(MarshalByte.readDecimal(buffer, position, Len.Int.IMP_DIR_QUIE, Len.Fract.IMP_DIR_QUIE));
    }

    public byte[] getDirittiQuietanzaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, flagTrattDirQuie);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimal(buffer, position, impDirQuie.copy());
        return buffer;
    }

    public void setFlagTrattDirQuie(char flagTrattDirQuie) {
        this.flagTrattDirQuie = flagTrattDirQuie;
    }

    public char getFlagTrattDirQuie() {
        return this.flagTrattDirQuie;
    }

    public void setImpDirQuie(AfDecimal impDirQuie) {
        this.impDirQuie.assign(impDirQuie);
    }

    public AfDecimal getImpDirQuie() {
        return this.impDirQuie.copy();
    }

    public void setDirittiPrimoVersBytes(byte[] buffer, int offset) {
        int position = offset;
        flagTrattDir1Ver = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        impDir1Ver.assign(MarshalByte.readDecimal(buffer, position, Len.Int.IMP_DIR1_VER, Len.Fract.IMP_DIR1_VER));
    }

    public byte[] getDirittiPrimoVersBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, flagTrattDir1Ver);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimal(buffer, position, impDir1Ver.copy());
        return buffer;
    }

    public void setFlagTrattDir1Ver(char flagTrattDir1Ver) {
        this.flagTrattDir1Ver = flagTrattDir1Ver;
    }

    public char getFlagTrattDir1Ver() {
        return this.flagTrattDir1Ver;
    }

    public void setImpDir1Ver(AfDecimal impDir1Ver) {
        this.impDir1Ver.assign(impDir1Ver);
    }

    public AfDecimal getImpDir1Ver() {
        return this.impDir1Ver.copy();
    }

    public void setDirittiVersAggBytes(byte[] buffer, int offset) {
        int position = offset;
        flagTrattVersAgg = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        impDirVersAgg.assign(MarshalByte.readDecimal(buffer, position, Len.Int.IMP_DIR_VERS_AGG, Len.Fract.IMP_DIR_VERS_AGG));
    }

    public byte[] getDirittiVersAggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, flagTrattVersAgg);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimal(buffer, position, impDirVersAgg.copy());
        return buffer;
    }

    public void setFlagTrattVersAgg(char flagTrattVersAgg) {
        this.flagTrattVersAgg = flagTrattVersAgg;
    }

    public char getFlagTrattVersAgg() {
        return this.flagTrattVersAgg;
    }

    public void setImpDirVersAgg(AfDecimal impDirVersAgg) {
        this.impDirVersAgg.assign(impDirVersAgg);
    }

    public AfDecimal getImpDirVersAgg() {
        return this.impDirVersAgg.copy();
    }

    public void setSpeseMedicheBytes(byte[] buffer, int offset) {
        int position = offset;
        flagSpeseMediche = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        impSpeseMediche.assign(MarshalByte.readDecimal(buffer, position, Len.Int.IMP_SPESE_MEDICHE, Len.Fract.IMP_SPESE_MEDICHE));
    }

    public byte[] getSpeseMedicheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, flagSpeseMediche);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimal(buffer, position, impSpeseMediche.copy());
        return buffer;
    }

    public void setFlagSpeseMediche(char flagSpeseMediche) {
        this.flagSpeseMediche = flagSpeseMediche;
    }

    public char getFlagSpeseMediche() {
        return this.flagSpeseMediche;
    }

    public void setImpSpeseMediche(AfDecimal impSpeseMediche) {
        this.impSpeseMediche.assign(impSpeseMediche);
    }

    public AfDecimal getImpSpeseMediche() {
        return this.impSpeseMediche.copy();
    }

    public void setIspc0040TpPremioNumEFormatted(String ispc0040TpPremioNumE) {
        this.tpPremioNumE = Trunc.toUnsignedNumeric(ispc0040TpPremioNumE, Len.TP_PREMIO_NUM_E);
    }

    public short getIspc0040TpPremioNumE() {
        return NumericDisplay.asShort(this.tpPremioNumE);
    }

    public void setTrattTipoPremio(char trattTipoPremio) {
        this.trattTipoPremio = trattTipoPremio;
    }

    public char getTrattTipoPremio() {
        return this.trattTipoPremio;
    }

    public void setDataDecor(String dataDecor) {
        this.dataDecor = Functions.subString(dataDecor, Len.DATA_DECOR);
    }

    public String getDataDecor() {
        return this.dataDecor;
    }

    public void setDataDecorTratt(char dataDecorTratt) {
        this.dataDecorTratt = dataDecorTratt;
    }

    public char getDataDecorTratt() {
        return this.dataDecorTratt;
    }

    public void setIspc0040NumGgReinvestFormatted(String ispc0040NumGgReinvest) {
        this.numGgReinvest = Trunc.toUnsignedNumeric(ispc0040NumGgReinvest, Len.NUM_GG_REINVEST);
    }

    public int getIspc0040NumGgReinvest() {
        return NumericDisplay.asInt(this.numGgReinvest);
    }

    public void setIspc0040MaxOperFormInvestFormatted(String ispc0040MaxOperFormInvest) {
        this.maxOperFormInvest = Trunc.toUnsignedNumeric(ispc0040MaxOperFormInvest, Len.MAX_OPER_FORM_INVEST);
    }

    public short getIspc0040MaxOperFormInvest() {
        return NumericDisplay.asShort(this.maxOperFormInvest);
    }

    public void setSottocategoria(String sottocategoria) {
        this.sottocategoria = Functions.subString(sottocategoria, Len.SOTTOCATEGORIA);
    }

    public String getSottocategoria() {
        return this.sottocategoria;
    }

    public Ispc0040AdesSenzaAssic getAdesSenzaAssic() {
        return adesSenzaAssic;
    }

    public Ispc0040DistintaContr getDistintaContr() {
        return distintaContr;
    }

    public Ispc0040FlagCoperturaFin getFlagCoperturaFin() {
        return flagCoperturaFin;
    }

    public Ispc0040OperFormaReinvest getOperFormaReinvest(int idx) {
        return operFormaReinvest[idx - 1];
    }

    public Ispc0040TipoPremio getTipoPremio(int idx) {
        return tipoPremio[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TRATTAMENTO_FONDO = 1;
        public static final int FLAG_CUMULO_CONTR = 1;
        public static final int FLAG_TRATT_DIR_EMIS = 1;
        public static final int IMP_DIR_EMIS = 18;
        public static final int DIRITTI_EMISSIONE = FLAG_TRATT_DIR_EMIS + IMP_DIR_EMIS;
        public static final int FLAG_TRATT_DIR_QUIE = 1;
        public static final int IMP_DIR_QUIE = 18;
        public static final int DIRITTI_QUIETANZA = FLAG_TRATT_DIR_QUIE + IMP_DIR_QUIE;
        public static final int FLAG_TRATT_DIR1_VER = 1;
        public static final int IMP_DIR1_VER = 18;
        public static final int DIRITTI_PRIMO_VERS = FLAG_TRATT_DIR1_VER + IMP_DIR1_VER;
        public static final int FLAG_TRATT_VERS_AGG = 1;
        public static final int IMP_DIR_VERS_AGG = 18;
        public static final int DIRITTI_VERS_AGG = FLAG_TRATT_VERS_AGG + IMP_DIR_VERS_AGG;
        public static final int FLAG_SPESE_MEDICHE = 1;
        public static final int IMP_SPESE_MEDICHE = 18;
        public static final int SPESE_MEDICHE = FLAG_SPESE_MEDICHE + IMP_SPESE_MEDICHE;
        public static final int TP_PREMIO_NUM_E = 3;
        public static final int TRATT_TIPO_PREMIO = 1;
        public static final int DATA_DECOR = 8;
        public static final int DATA_DECOR_TRATT = 1;
        public static final int NUM_GG_REINVEST = 5;
        public static final int MAX_OPER_FORM_INVEST = 3;
        public static final int SOTTOCATEGORIA = 12;
        public static final int ISPC0040_DATI_OUTPUT2 = TRATTAMENTO_FONDO + Ispc0040FlagCoperturaFin.Len.FLAG_COPERTURA_FIN + Ispc0040AdesSenzaAssic.Len.ADES_SENZA_ASSIC + FLAG_CUMULO_CONTR + DIRITTI_EMISSIONE + DIRITTI_QUIETANZA + DIRITTI_PRIMO_VERS + DIRITTI_VERS_AGG + SPESE_MEDICHE + TP_PREMIO_NUM_E + Ispc0040DatiOutput2.TIPO_PREMIO_MAXOCCURS * Ispc0040TipoPremio.Len.TIPO_PREMIO + TRATT_TIPO_PREMIO + Ispc0040DistintaContr.Len.DISTINTA_CONTR + DATA_DECOR + DATA_DECOR_TRATT + NUM_GG_REINVEST + MAX_OPER_FORM_INVEST + Ispc0040DatiOutput2.OPER_FORMA_REINVEST_MAXOCCURS * Ispc0040OperFormaReinvest.Len.OPER_FORMA_REINVEST + SOTTOCATEGORIA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IMP_DIR_EMIS = 15;
            public static final int IMP_DIR_QUIE = 15;
            public static final int IMP_DIR1_VER = 15;
            public static final int IMP_DIR_VERS_AGG = 15;
            public static final int IMP_SPESE_MEDICHE = 15;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMP_DIR_EMIS = 3;
            public static final int IMP_DIR_QUIE = 3;
            public static final int IMP_DIR1_VER = 3;
            public static final int IMP_DIR_VERS_AGG = 3;
            public static final int IMP_SPESE_MEDICHE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
