package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: TEMPORARY-DATA<br>
 * Variable: TEMPORARY-DATA from copybook IDBVD081<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TemporaryData {

    //==== PROPERTIES ====
    //Original name: D08-ID-TEMPORARY-DATA
    private long idTemporaryData = DefaultValues.LONG_VAL;
    //Original name: D08-ALIAS-STR-DATO
    private String aliasStrDato = DefaultValues.stringVal(Len.ALIAS_STR_DATO);
    //Original name: D08-NUM-FRAME
    private int numFrame = DefaultValues.INT_VAL;
    //Original name: D08-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: D08-ID-SESSION
    private String idSession = DefaultValues.stringVal(Len.ID_SESSION);
    //Original name: D08-FL-CONTIGUOUS-DATA
    private char flContiguousData = DefaultValues.CHAR_VAL;
    //Original name: D08-TOTAL-RECURRENCE
    private int totalRecurrence = DefaultValues.INT_VAL;
    //Original name: D08-PARTIAL-RECURRENCE
    private int partialRecurrence = DefaultValues.INT_VAL;
    //Original name: D08-ACTUAL-RECURRENCE
    private int actualRecurrence = DefaultValues.INT_VAL;
    //Original name: D08-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: D08-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: D08-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: D08-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: D08-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: D08-TYPE-RECORD
    private String typeRecord = DefaultValues.stringVal(Len.TYPE_RECORD);
    //Original name: D08-BUFFER-DATA-LEN
    private short bufferDataLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: D08-BUFFER-DATA
    private String bufferData = DefaultValues.stringVal(Len.BUFFER_DATA);

    //==== METHODS ====
    public void setIdTemporaryData(long idTemporaryData) {
        this.idTemporaryData = idTemporaryData;
    }

    public long getIdTemporaryData() {
        return this.idTemporaryData;
    }

    public void setAliasStrDato(String aliasStrDato) {
        this.aliasStrDato = Functions.subString(aliasStrDato, Len.ALIAS_STR_DATO);
    }

    public String getAliasStrDato() {
        return this.aliasStrDato;
    }

    public void setNumFrame(int numFrame) {
        this.numFrame = numFrame;
    }

    public int getNumFrame() {
        return this.numFrame;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setIdSession(String idSession) {
        this.idSession = Functions.subString(idSession, Len.ID_SESSION);
    }

    public String getIdSession() {
        return this.idSession;
    }

    public void setFlContiguousData(char flContiguousData) {
        this.flContiguousData = flContiguousData;
    }

    public char getFlContiguousData() {
        return this.flContiguousData;
    }

    public void setTotalRecurrence(int totalRecurrence) {
        this.totalRecurrence = totalRecurrence;
    }

    public int getTotalRecurrence() {
        return this.totalRecurrence;
    }

    public void setPartialRecurrence(int partialRecurrence) {
        this.partialRecurrence = partialRecurrence;
    }

    public int getPartialRecurrence() {
        return this.partialRecurrence;
    }

    public void setActualRecurrence(int actualRecurrence) {
        this.actualRecurrence = actualRecurrence;
    }

    public int getActualRecurrence() {
        return this.actualRecurrence;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public void setDsOperSqlFormatted(String dsOperSql) {
        setDsOperSql(Functions.charAt(dsOperSql, Types.CHAR_SIZE));
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public void setDsStatoElabFormatted(String dsStatoElab) {
        setDsStatoElab(Functions.charAt(dsStatoElab, Types.CHAR_SIZE));
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setTypeRecord(String typeRecord) {
        this.typeRecord = Functions.subString(typeRecord, Len.TYPE_RECORD);
    }

    public String getTypeRecord() {
        return this.typeRecord;
    }

    public void setBufferDataVcharFormatted(String data) {
        byte[] buffer = new byte[Len.BUFFER_DATA_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.BUFFER_DATA_VCHAR);
        setBufferDataVcharBytes(buffer, 1);
    }

    public String getBufferDataVcharFormatted() {
        return MarshalByteExt.bufferToStr(getBufferDataVcharBytes());
    }

    /**Original name: D08-BUFFER-DATA-VCHAR<br>*/
    public byte[] getBufferDataVcharBytes() {
        byte[] buffer = new byte[Len.BUFFER_DATA_VCHAR];
        return getBufferDataVcharBytes(buffer, 1);
    }

    public void setBufferDataVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        bufferDataLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        bufferData = MarshalByte.readString(buffer, position, Len.BUFFER_DATA);
    }

    public byte[] getBufferDataVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, bufferDataLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, bufferData, Len.BUFFER_DATA);
        return buffer;
    }

    public void setBufferDataLen(short bufferDataLen) {
        this.bufferDataLen = bufferDataLen;
    }

    public short getBufferDataLen() {
        return this.bufferDataLen;
    }

    public void setBufferData(String bufferData) {
        this.bufferData = Functions.subString(bufferData, Len.BUFFER_DATA);
    }

    public String getBufferData() {
        return this.bufferData;
    }

    public String getBufferDataFormatted() {
        return Functions.padBlanks(getBufferData(), Len.BUFFER_DATA);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ALIAS_STR_DATO = 30;
        public static final int ID_SESSION = 20;
        public static final int DS_UTENTE = 20;
        public static final int TYPE_RECORD = 3;
        public static final int BUFFER_DATA_LEN = 2;
        public static final int BUFFER_DATA = 32000;
        public static final int BUFFER_DATA_VCHAR = BUFFER_DATA_LEN + BUFFER_DATA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
