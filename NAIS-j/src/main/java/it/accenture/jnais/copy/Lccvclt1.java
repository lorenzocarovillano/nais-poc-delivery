package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVCLT1<br>
 * Variable: LCCVCLT1 from copybook LCCVCLT1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvclt1 {

    //==== PROPERTIES ====
    /**Original name: WCLT-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA CLAU_TXT
	 *    ALIAS CLT
	 *    ULTIMO AGG. 28 GEN 2009
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WCLT-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WCLT-DATI
    private WcltDati dati = new WcltDati();

    //==== METHODS ====
    public void initLccvclt1Spaces() {
        status.setStatus(Types.SPACE_CHAR);
        idPtf = Types.INVALID_INT_VAL;
        dati.initDatiSpaces();
    }

    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WcltDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
