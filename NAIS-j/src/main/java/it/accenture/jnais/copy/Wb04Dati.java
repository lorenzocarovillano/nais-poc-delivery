package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Wb04IdRichEstrazAgg;
import it.accenture.jnais.ws.redefines.Wb04ValImp;
import it.accenture.jnais.ws.redefines.Wb04ValPc;

/**Original name: WB04-DATI<br>
 * Variable: WB04-DATI from copybook LCCVB041<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wb04Dati {

    //==== PROPERTIES ====
    //Original name: WB04-ID-BILA-VAR-CALC-P
    private int wb04IdBilaVarCalcP = DefaultValues.INT_VAL;
    //Original name: WB04-COD-COMP-ANIA
    private int wb04CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WB04-ID-RICH-ESTRAZ-MAS
    private int wb04IdRichEstrazMas = DefaultValues.INT_VAL;
    //Original name: WB04-ID-RICH-ESTRAZ-AGG
    private Wb04IdRichEstrazAgg wb04IdRichEstrazAgg = new Wb04IdRichEstrazAgg();
    //Original name: WB04-DT-RIS
    private int wb04DtRis = DefaultValues.INT_VAL;
    //Original name: WB04-ID-POLI
    private int wb04IdPoli = DefaultValues.INT_VAL;
    //Original name: WB04-ID-ADES
    private int wb04IdAdes = DefaultValues.INT_VAL;
    //Original name: WB04-PROG-SCHEDA-VALOR
    private int wb04ProgSchedaValor = DefaultValues.INT_VAL;
    //Original name: WB04-TP-RGM-FISC
    private String wb04TpRgmFisc = DefaultValues.stringVal(Len.WB04_TP_RGM_FISC);
    //Original name: WB04-DT-INI-VLDT-PROD
    private int wb04DtIniVldtProd = DefaultValues.INT_VAL;
    //Original name: WB04-COD-VAR
    private String wb04CodVar = DefaultValues.stringVal(Len.WB04_COD_VAR);
    //Original name: WB04-TP-D
    private char wb04TpD = DefaultValues.CHAR_VAL;
    //Original name: WB04-VAL-IMP
    private Wb04ValImp wb04ValImp = new Wb04ValImp();
    //Original name: WB04-VAL-PC
    private Wb04ValPc wb04ValPc = new Wb04ValPc();
    //Original name: WB04-VAL-STRINGA
    private String wb04ValStringa = DefaultValues.stringVal(Len.WB04_VAL_STRINGA);
    //Original name: WB04-DS-OPER-SQL
    private char wb04DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WB04-DS-VER
    private int wb04DsVer = DefaultValues.INT_VAL;
    //Original name: WB04-DS-TS-CPTZ
    private long wb04DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WB04-DS-UTENTE
    private String wb04DsUtente = DefaultValues.stringVal(Len.WB04_DS_UTENTE);
    //Original name: WB04-DS-STATO-ELAB
    private char wb04DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WB04-AREA-D-VALOR-VAR-LEN
    private short wb04AreaDValorVarLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: WB04-AREA-D-VALOR-VAR
    private String wb04AreaDValorVar = DefaultValues.stringVal(Len.WB04_AREA_D_VALOR_VAR);

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wb04IdBilaVarCalcP = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB04_ID_BILA_VAR_CALC_P, 0);
        position += Len.WB04_ID_BILA_VAR_CALC_P;
        wb04CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB04_COD_COMP_ANIA, 0);
        position += Len.WB04_COD_COMP_ANIA;
        wb04IdRichEstrazMas = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB04_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.WB04_ID_RICH_ESTRAZ_MAS;
        wb04IdRichEstrazAgg.setWb04IdRichEstrazAggFromBuffer(buffer, position);
        position += Wb04IdRichEstrazAgg.Len.WB04_ID_RICH_ESTRAZ_AGG;
        wb04DtRis = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB04_DT_RIS, 0);
        position += Len.WB04_DT_RIS;
        wb04IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB04_ID_POLI, 0);
        position += Len.WB04_ID_POLI;
        wb04IdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB04_ID_ADES, 0);
        position += Len.WB04_ID_ADES;
        wb04ProgSchedaValor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB04_PROG_SCHEDA_VALOR, 0);
        position += Len.WB04_PROG_SCHEDA_VALOR;
        wb04TpRgmFisc = MarshalByte.readString(buffer, position, Len.WB04_TP_RGM_FISC);
        position += Len.WB04_TP_RGM_FISC;
        wb04DtIniVldtProd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB04_DT_INI_VLDT_PROD, 0);
        position += Len.WB04_DT_INI_VLDT_PROD;
        wb04CodVar = MarshalByte.readString(buffer, position, Len.WB04_COD_VAR);
        position += Len.WB04_COD_VAR;
        wb04TpD = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb04ValImp.setWb04ValImpFromBuffer(buffer, position);
        position += Wb04ValImp.Len.WB04_VAL_IMP;
        wb04ValPc.setWb04ValPcFromBuffer(buffer, position);
        position += Wb04ValPc.Len.WB04_VAL_PC;
        wb04ValStringa = MarshalByte.readString(buffer, position, Len.WB04_VAL_STRINGA);
        position += Len.WB04_VAL_STRINGA;
        wb04DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wb04DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB04_DS_VER, 0);
        position += Len.WB04_DS_VER;
        wb04DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WB04_DS_TS_CPTZ, 0);
        position += Len.WB04_DS_TS_CPTZ;
        wb04DsUtente = MarshalByte.readString(buffer, position, Len.WB04_DS_UTENTE);
        position += Len.WB04_DS_UTENTE;
        wb04DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setWb04AreaDValorVarVcharBytes(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wb04IdBilaVarCalcP, Len.Int.WB04_ID_BILA_VAR_CALC_P, 0);
        position += Len.WB04_ID_BILA_VAR_CALC_P;
        MarshalByte.writeIntAsPacked(buffer, position, wb04CodCompAnia, Len.Int.WB04_COD_COMP_ANIA, 0);
        position += Len.WB04_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wb04IdRichEstrazMas, Len.Int.WB04_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.WB04_ID_RICH_ESTRAZ_MAS;
        wb04IdRichEstrazAgg.getWb04IdRichEstrazAggAsBuffer(buffer, position);
        position += Wb04IdRichEstrazAgg.Len.WB04_ID_RICH_ESTRAZ_AGG;
        MarshalByte.writeIntAsPacked(buffer, position, wb04DtRis, Len.Int.WB04_DT_RIS, 0);
        position += Len.WB04_DT_RIS;
        MarshalByte.writeIntAsPacked(buffer, position, wb04IdPoli, Len.Int.WB04_ID_POLI, 0);
        position += Len.WB04_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wb04IdAdes, Len.Int.WB04_ID_ADES, 0);
        position += Len.WB04_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wb04ProgSchedaValor, Len.Int.WB04_PROG_SCHEDA_VALOR, 0);
        position += Len.WB04_PROG_SCHEDA_VALOR;
        MarshalByte.writeString(buffer, position, wb04TpRgmFisc, Len.WB04_TP_RGM_FISC);
        position += Len.WB04_TP_RGM_FISC;
        MarshalByte.writeIntAsPacked(buffer, position, wb04DtIniVldtProd, Len.Int.WB04_DT_INI_VLDT_PROD, 0);
        position += Len.WB04_DT_INI_VLDT_PROD;
        MarshalByte.writeString(buffer, position, wb04CodVar, Len.WB04_COD_VAR);
        position += Len.WB04_COD_VAR;
        MarshalByte.writeChar(buffer, position, wb04TpD);
        position += Types.CHAR_SIZE;
        wb04ValImp.getWb04ValImpAsBuffer(buffer, position);
        position += Wb04ValImp.Len.WB04_VAL_IMP;
        wb04ValPc.getWb04ValPcAsBuffer(buffer, position);
        position += Wb04ValPc.Len.WB04_VAL_PC;
        MarshalByte.writeString(buffer, position, wb04ValStringa, Len.WB04_VAL_STRINGA);
        position += Len.WB04_VAL_STRINGA;
        MarshalByte.writeChar(buffer, position, wb04DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wb04DsVer, Len.Int.WB04_DS_VER, 0);
        position += Len.WB04_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wb04DsTsCptz, Len.Int.WB04_DS_TS_CPTZ, 0);
        position += Len.WB04_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wb04DsUtente, Len.WB04_DS_UTENTE);
        position += Len.WB04_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wb04DsStatoElab);
        position += Types.CHAR_SIZE;
        getWb04AreaDValorVarVcharBytes(buffer, position);
        return buffer;
    }

    public void setWb04IdBilaVarCalcP(int wb04IdBilaVarCalcP) {
        this.wb04IdBilaVarCalcP = wb04IdBilaVarCalcP;
    }

    public int getWb04IdBilaVarCalcP() {
        return this.wb04IdBilaVarCalcP;
    }

    public void setWb04CodCompAnia(int wb04CodCompAnia) {
        this.wb04CodCompAnia = wb04CodCompAnia;
    }

    public int getWb04CodCompAnia() {
        return this.wb04CodCompAnia;
    }

    public void setWb04IdRichEstrazMas(int wb04IdRichEstrazMas) {
        this.wb04IdRichEstrazMas = wb04IdRichEstrazMas;
    }

    public int getWb04IdRichEstrazMas() {
        return this.wb04IdRichEstrazMas;
    }

    public void setWb04DtRis(int wb04DtRis) {
        this.wb04DtRis = wb04DtRis;
    }

    public int getWb04DtRis() {
        return this.wb04DtRis;
    }

    public void setWb04IdPoli(int wb04IdPoli) {
        this.wb04IdPoli = wb04IdPoli;
    }

    public int getWb04IdPoli() {
        return this.wb04IdPoli;
    }

    public void setWb04IdAdes(int wb04IdAdes) {
        this.wb04IdAdes = wb04IdAdes;
    }

    public int getWb04IdAdes() {
        return this.wb04IdAdes;
    }

    public void setWb04ProgSchedaValor(int wb04ProgSchedaValor) {
        this.wb04ProgSchedaValor = wb04ProgSchedaValor;
    }

    public int getWb04ProgSchedaValor() {
        return this.wb04ProgSchedaValor;
    }

    public void setWb04TpRgmFisc(String wb04TpRgmFisc) {
        this.wb04TpRgmFisc = Functions.subString(wb04TpRgmFisc, Len.WB04_TP_RGM_FISC);
    }

    public String getWb04TpRgmFisc() {
        return this.wb04TpRgmFisc;
    }

    public void setWb04DtIniVldtProd(int wb04DtIniVldtProd) {
        this.wb04DtIniVldtProd = wb04DtIniVldtProd;
    }

    public void setWb04DtIniVldtProdFormatted(String wb04DtIniVldtProd) {
        setWb04DtIniVldtProd(PicParser.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).parseInt(wb04DtIniVldtProd));
    }

    public int getWb04DtIniVldtProd() {
        return this.wb04DtIniVldtProd;
    }

    public void setWb04CodVar(String wb04CodVar) {
        this.wb04CodVar = Functions.subString(wb04CodVar, Len.WB04_COD_VAR);
    }

    public String getWb04CodVar() {
        return this.wb04CodVar;
    }

    public void setWb04TpD(char wb04TpD) {
        this.wb04TpD = wb04TpD;
    }

    public char getWb04TpD() {
        return this.wb04TpD;
    }

    public void setWb04ValStringa(String wb04ValStringa) {
        this.wb04ValStringa = Functions.subString(wb04ValStringa, Len.WB04_VAL_STRINGA);
    }

    public String getWb04ValStringa() {
        return this.wb04ValStringa;
    }

    public void setWb04DsOperSql(char wb04DsOperSql) {
        this.wb04DsOperSql = wb04DsOperSql;
    }

    public char getWb04DsOperSql() {
        return this.wb04DsOperSql;
    }

    public void setWb04DsVer(int wb04DsVer) {
        this.wb04DsVer = wb04DsVer;
    }

    public int getWb04DsVer() {
        return this.wb04DsVer;
    }

    public void setWb04DsTsCptz(long wb04DsTsCptz) {
        this.wb04DsTsCptz = wb04DsTsCptz;
    }

    public long getWb04DsTsCptz() {
        return this.wb04DsTsCptz;
    }

    public void setWb04DsUtente(String wb04DsUtente) {
        this.wb04DsUtente = Functions.subString(wb04DsUtente, Len.WB04_DS_UTENTE);
    }

    public String getWb04DsUtente() {
        return this.wb04DsUtente;
    }

    public void setWb04DsStatoElab(char wb04DsStatoElab) {
        this.wb04DsStatoElab = wb04DsStatoElab;
    }

    public char getWb04DsStatoElab() {
        return this.wb04DsStatoElab;
    }

    /**Original name: WB04-AREA-D-VALOR-VAR-VCHAR<br>*/
    public byte[] getWb04AreaDValorVarVcharBytes() {
        byte[] buffer = new byte[Len.WB04_AREA_D_VALOR_VAR_VCHAR];
        return getWb04AreaDValorVarVcharBytes(buffer, 1);
    }

    public void setWb04AreaDValorVarVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        wb04AreaDValorVarLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wb04AreaDValorVar = MarshalByte.readString(buffer, position, Len.WB04_AREA_D_VALOR_VAR);
    }

    public byte[] getWb04AreaDValorVarVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wb04AreaDValorVarLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, wb04AreaDValorVar, Len.WB04_AREA_D_VALOR_VAR);
        return buffer;
    }

    public void setWb04AreaDValorVarLen(short wb04AreaDValorVarLen) {
        this.wb04AreaDValorVarLen = wb04AreaDValorVarLen;
    }

    public short getWb04AreaDValorVarLen() {
        return this.wb04AreaDValorVarLen;
    }

    public void setWb04AreaDValorVar(String wb04AreaDValorVar) {
        this.wb04AreaDValorVar = Functions.subString(wb04AreaDValorVar, Len.WB04_AREA_D_VALOR_VAR);
    }

    public String getWb04AreaDValorVar() {
        return this.wb04AreaDValorVar;
    }

    public Wb04IdRichEstrazAgg getWb04IdRichEstrazAgg() {
        return wb04IdRichEstrazAgg;
    }

    public Wb04ValImp getWb04ValImp() {
        return wb04ValImp;
    }

    public Wb04ValPc getWb04ValPc() {
        return wb04ValPc;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WB04_ID_BILA_VAR_CALC_P = 5;
        public static final int WB04_COD_COMP_ANIA = 3;
        public static final int WB04_ID_RICH_ESTRAZ_MAS = 5;
        public static final int WB04_DT_RIS = 5;
        public static final int WB04_ID_POLI = 5;
        public static final int WB04_ID_ADES = 5;
        public static final int WB04_PROG_SCHEDA_VALOR = 5;
        public static final int WB04_TP_RGM_FISC = 2;
        public static final int WB04_DT_INI_VLDT_PROD = 5;
        public static final int WB04_COD_VAR = 30;
        public static final int WB04_TP_D = 1;
        public static final int WB04_VAL_STRINGA = 60;
        public static final int WB04_DS_OPER_SQL = 1;
        public static final int WB04_DS_VER = 5;
        public static final int WB04_DS_TS_CPTZ = 10;
        public static final int WB04_DS_UTENTE = 20;
        public static final int WB04_DS_STATO_ELAB = 1;
        public static final int WB04_AREA_D_VALOR_VAR_LEN = 2;
        public static final int WB04_AREA_D_VALOR_VAR = 4000;
        public static final int WB04_AREA_D_VALOR_VAR_VCHAR = WB04_AREA_D_VALOR_VAR_LEN + WB04_AREA_D_VALOR_VAR;
        public static final int DATI = WB04_ID_BILA_VAR_CALC_P + WB04_COD_COMP_ANIA + WB04_ID_RICH_ESTRAZ_MAS + Wb04IdRichEstrazAgg.Len.WB04_ID_RICH_ESTRAZ_AGG + WB04_DT_RIS + WB04_ID_POLI + WB04_ID_ADES + WB04_PROG_SCHEDA_VALOR + WB04_TP_RGM_FISC + WB04_DT_INI_VLDT_PROD + WB04_COD_VAR + WB04_TP_D + Wb04ValImp.Len.WB04_VAL_IMP + Wb04ValPc.Len.WB04_VAL_PC + WB04_VAL_STRINGA + WB04_DS_OPER_SQL + WB04_DS_VER + WB04_DS_TS_CPTZ + WB04_DS_UTENTE + WB04_DS_STATO_ELAB + WB04_AREA_D_VALOR_VAR_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB04_ID_BILA_VAR_CALC_P = 9;
            public static final int WB04_COD_COMP_ANIA = 5;
            public static final int WB04_ID_RICH_ESTRAZ_MAS = 9;
            public static final int WB04_DT_RIS = 8;
            public static final int WB04_ID_POLI = 9;
            public static final int WB04_ID_ADES = 9;
            public static final int WB04_PROG_SCHEDA_VALOR = 9;
            public static final int WB04_DT_INI_VLDT_PROD = 8;
            public static final int WB04_DS_VER = 9;
            public static final int WB04_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
