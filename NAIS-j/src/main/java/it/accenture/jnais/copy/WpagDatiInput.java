package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.WpagCodOggetto;
import it.accenture.jnais.ws.enums.WpagTipoNumerazione;
import it.accenture.jnais.ws.occurs.WpagTabInfo;

/**Original name: WPAG-DATI-INPUT<br>
 * Variable: WPAG-DATI-INPUT from copybook IDSV0161<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WpagDatiInput {

    //==== PROPERTIES ====
    public static final int TAB_INFO_MAXOCCURS = 100;
    /**Original name: WPAG-COD-OGGETTO<br>
	 * <pre>-- CODICE OGGETTO</pre>*/
    private WpagCodOggetto codOggetto = new WpagCodOggetto();
    /**Original name: WPAG-TIPO-NUMERAZIONE<br>
	 * <pre>-- TIPO NUMERAZIONE</pre>*/
    private WpagTipoNumerazione tipoNumerazione = new WpagTipoNumerazione();
    /**Original name: WPAG-IB-OGGETTO-I<br>
	 * <pre>-- IB OGGETTO PASSATO IN INPUT X TIPO NUMERAZIONE MANUALE</pre>*/
    private String ibOggettoI = DefaultValues.stringVal(Len.IB_OGGETTO_I);
    //Original name: WPAG-IX-CONTA
    private int ixConta = DefaultValues.BIN_INT_VAL;
    //Original name: WPAG-APPO-LUNGHEZZA
    private int appoLunghezza = DefaultValues.INT_VAL;
    //Original name: WPAG-ELE-INFO-MAX
    private short eleInfoMax = DefaultValues.SHORT_VAL;
    //Original name: WPAG-TAB-INFO
    private WpagTabInfo[] tabInfo = new WpagTabInfo[TAB_INFO_MAXOCCURS];
    //Original name: WPAG-BUFFER-DATI
    private String bufferDati = DefaultValues.stringVal(Len.BUFFER_DATI);

    //==== CONSTRUCTORS ====
    public WpagDatiInput() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabInfoIdx = 1; tabInfoIdx <= TAB_INFO_MAXOCCURS; tabInfoIdx++) {
            tabInfo[tabInfoIdx - 1] = new WpagTabInfo();
        }
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        codOggetto.setCodOggetto(MarshalByte.readString(buffer, position, WpagCodOggetto.Len.COD_OGGETTO));
        position += WpagCodOggetto.Len.COD_OGGETTO;
        tipoNumerazione.setTipoNumerazione(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        ibOggettoI = MarshalByte.readString(buffer, position, Len.IB_OGGETTO_I);
        position += Len.IB_OGGETTO_I;
        setStrutturaMappingAreeBytes(buffer, position);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codOggetto.getCodOggetto(), WpagCodOggetto.Len.COD_OGGETTO);
        position += WpagCodOggetto.Len.COD_OGGETTO;
        MarshalByte.writeChar(buffer, position, tipoNumerazione.getTipoNumerazione());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ibOggettoI, Len.IB_OGGETTO_I);
        position += Len.IB_OGGETTO_I;
        getStrutturaMappingAreeBytes(buffer, position);
        return buffer;
    }

    public void setIbOggettoI(String ibOggettoI) {
        this.ibOggettoI = Functions.subString(ibOggettoI, Len.IB_OGGETTO_I);
    }

    public String getIbOggettoI() {
        return this.ibOggettoI;
    }

    public String getIbOggettoIFormatted() {
        return Functions.padBlanks(getIbOggettoI(), Len.IB_OGGETTO_I);
    }

    public void setStrutturaMappingAreeBytes(byte[] buffer, int offset) {
        int position = offset;
        ixConta = MarshalByte.readBinaryUnsignedShort(buffer, position);
        position += Types.SHORT_SIZE;
        appoLunghezza = MarshalByte.readPackedAsInt(buffer, position, Len.Int.APPO_LUNGHEZZA, 0);
        position += Len.APPO_LUNGHEZZA;
        eleInfoMax = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_INFO_MAX, 0);
        position += Len.ELE_INFO_MAX;
        setTabInfo1Bytes(buffer, position);
        position += Len.TAB_INFO1;
        bufferDati = MarshalByte.readString(buffer, position, Len.BUFFER_DATI);
    }

    public byte[] getStrutturaMappingAreeBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryUnsignedShort(buffer, position, ixConta);
        position += Types.SHORT_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, appoLunghezza, Len.Int.APPO_LUNGHEZZA, 0);
        position += Len.APPO_LUNGHEZZA;
        MarshalByte.writeShortAsPacked(buffer, position, eleInfoMax, Len.Int.ELE_INFO_MAX, 0);
        position += Len.ELE_INFO_MAX;
        getTabInfo1Bytes(buffer, position);
        position += Len.TAB_INFO1;
        MarshalByte.writeString(buffer, position, bufferDati, Len.BUFFER_DATI);
        return buffer;
    }

    public void setIxConta(int ixConta) {
        this.ixConta = ixConta;
    }

    public int getIxConta() {
        return this.ixConta;
    }

    public void setAppoLunghezza(int appoLunghezza) {
        this.appoLunghezza = appoLunghezza;
    }

    public int getAppoLunghezza() {
        return this.appoLunghezza;
    }

    public void setEleInfoMax(short eleInfoMax) {
        this.eleInfoMax = eleInfoMax;
    }

    public short getEleInfoMax() {
        return this.eleInfoMax;
    }

    public void setTabInfo1Bytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_INFO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabInfo[idx - 1].setTabInfoBytes(buffer, position);
                position += WpagTabInfo.Len.TAB_INFO;
            }
            else {
                tabInfo[idx - 1].initTabInfoSpaces();
                position += WpagTabInfo.Len.TAB_INFO;
            }
        }
    }

    public byte[] getTabInfo1Bytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_INFO_MAXOCCURS; idx++) {
            tabInfo[idx - 1].getTabInfoBytes(buffer, position);
            position += WpagTabInfo.Len.TAB_INFO;
        }
        return buffer;
    }

    public void setBufferDati(String bufferDati) {
        this.bufferDati = Functions.subString(bufferDati, Len.BUFFER_DATI);
    }

    public void setC161BufferDatiSubstring(String replacement, int start, int length) {
        bufferDati = Functions.setSubstring(bufferDati, replacement, start, length);
    }

    public String getBufferDati() {
        return this.bufferDati;
    }

    public String getBufferDatiFormatted() {
        return Functions.padBlanks(getBufferDati(), Len.BUFFER_DATI);
    }

    public WpagCodOggetto getCodOggetto() {
        return codOggetto;
    }

    public WpagTabInfo getTabInfo(int idx) {
        return tabInfo[idx - 1];
    }

    public WpagTipoNumerazione getTipoNumerazione() {
        return tipoNumerazione;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IB_OGGETTO_I = 40;
        public static final int IX_CONTA = 2;
        public static final int APPO_LUNGHEZZA = 5;
        public static final int ELE_INFO_MAX = 3;
        public static final int TAB_INFO1 = WpagDatiInput.TAB_INFO_MAXOCCURS * WpagTabInfo.Len.TAB_INFO;
        public static final int BUFFER_DATI = 10000;
        public static final int STRUTTURA_MAPPING_AREE = IX_CONTA + APPO_LUNGHEZZA + ELE_INFO_MAX + TAB_INFO1 + BUFFER_DATI;
        public static final int DATI_INPUT = WpagCodOggetto.Len.COD_OGGETTO + WpagTipoNumerazione.Len.TIPO_NUMERAZIONE + IB_OGGETTO_I + STRUTTURA_MAPPING_AREE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int APPO_LUNGHEZZA = 9;
            public static final int ELE_INFO_MAX = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
