package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.MsgOperazioni;

/**Original name: MSG-ERR-FILE<br>
 * Variable: MSG-ERR-FILE from copybook IABCSQ99<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class MsgErrFile {

    //==== PROPERTIES ====
    //Original name: FILLER-MSG-ERR-FILE
    private String flr1 = "ERRORE :";
    //Original name: MSG-OPERAZIONI
    private MsgOperazioni operazioni = new MsgOperazioni();
    //Original name: FILLER-MSG-ERR-FILE-1
    private String flr2 = " - FILE NAME :";
    //Original name: MSG-NOME-FILE
    private String nomeFile = "";
    //Original name: FILLER-MSG-ERR-FILE-2
    private String flr3 = " - FILE STATUS :";
    //Original name: MSG-RC
    private String rc = "";

    //==== METHODS ====
    public String getMsgErrFileFormatted() {
        return MarshalByteExt.bufferToStr(getMsgErrFileBytes());
    }

    public byte[] getMsgErrFileBytes() {
        byte[] buffer = new byte[Len.MSG_ERR_FILE];
        return getMsgErrFileBytes(buffer, 1);
    }

    public byte[] getMsgErrFileBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        position += Len.FLR1;
        MarshalByte.writeString(buffer, position, operazioni.getOperazioni(), MsgOperazioni.Len.OPERAZIONI);
        position += MsgOperazioni.Len.OPERAZIONI;
        MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
        position += Len.FLR2;
        MarshalByte.writeString(buffer, position, nomeFile, Len.NOME_FILE);
        position += Len.NOME_FILE;
        MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
        position += Len.FLR3;
        MarshalByte.writeString(buffer, position, rc, Len.RC);
        return buffer;
    }

    public String getFlr1() {
        return this.flr1;
    }

    public String getFlr2() {
        return this.flr2;
    }

    public void setNomeFile(String nomeFile) {
        this.nomeFile = Functions.subString(nomeFile, Len.NOME_FILE);
    }

    public String getNomeFile() {
        return this.nomeFile;
    }

    public String getFlr3() {
        return this.flr3;
    }

    public void setRc(String rc) {
        this.rc = Functions.subString(rc, Len.RC);
    }

    public String getRc() {
        return this.rc;
    }

    public MsgOperazioni getOperazioni() {
        return operazioni;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NOME_FILE = 8;
        public static final int RC = 2;
        public static final int FLR1 = 9;
        public static final int FLR2 = 15;
        public static final int FLR3 = 17;
        public static final int MSG_ERR_FILE = MsgOperazioni.Len.OPERAZIONI + NOME_FILE + RC + FLR1 + FLR2 + FLR3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
