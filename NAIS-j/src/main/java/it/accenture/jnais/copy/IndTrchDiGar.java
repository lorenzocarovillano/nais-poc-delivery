package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-TRCH-DI-GAR<br>
 * Variable: IND-TRCH-DI-GAR from copybook IDBVTGA2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndTrchDiGar {

    //==== PROPERTIES ====
    //Original name: IND-TGA-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-DT-SCAD
    private short dtScad = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IB-OGG
    private short ibOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-DT-EMIS
    private short dtEmis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-DUR-AA
    private short durAa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-DUR-MM
    private short durMm = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-DUR-GG
    private short durGg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-CASO-MOR
    private short preCasoMor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PC-INTR-RIAT
    private short pcIntrRiat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-BNS-ANTIC
    private short impBnsAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-INI-NET
    private short preIniNet = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-PP-INI
    private short prePpIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-PP-ULT
    private short prePpUlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-TARI-INI
    private short preTariIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-TARI-ULT
    private short preTariUlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-INVRIO-INI
    private short preInvrioIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-INVRIO-ULT
    private short preInvrioUlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-RIVTO
    private short preRivto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-SOPR-PROF
    private short impSoprProf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-SOPR-SAN
    private short impSoprSan = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-SOPR-SPO
    private short impSoprSpo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-SOPR-TEC
    private short impSoprTec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-ALT-SOPR
    private short impAltSopr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-STAB
    private short preStab = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-DT-EFF-STAB
    private short dtEffStab = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-TS-RIVAL-FIS
    private short tsRivalFis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-TS-RIVAL-INDICIZ
    private short tsRivalIndiciz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-OLD-TS-TEC
    private short oldTsTec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-RAT-LRD
    private short ratLrd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-LRD
    private short preLrd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRSTZ-INI
    private short prstzIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRSTZ-ULT
    private short prstzUlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-CPT-IN-OPZ-RIVTO
    private short cptInOpzRivto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRSTZ-INI-STAB
    private short prstzIniStab = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-CPT-RSH-MOR
    private short cptRshMor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRSTZ-RID-INI
    private short prstzRidIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-FL-CAR-CONT
    private short flCarCont = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-BNS-GIA-LIQTO
    private short bnsGiaLiqto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-BNS
    private short impBns = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRSTZ-INI-NEWFIS
    private short prstzIniNewfis = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-SCON
    private short impScon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ALQ-SCON
    private short alqScon = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-CAR-ACQ
    private short impCarAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-CAR-INC
    private short impCarInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-CAR-GEST
    private short impCarGest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ETA-AA-1O-ASSTO
    private short etaAa1oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ETA-MM-1O-ASSTO
    private short etaMm1oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ETA-AA-2O-ASSTO
    private short etaAa2oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ETA-MM-2O-ASSTO
    private short etaMm2oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ETA-AA-3O-ASSTO
    private short etaAa3oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ETA-MM-3O-ASSTO
    private short etaMm3oAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-RENDTO-LRD
    private short rendtoLrd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PC-RETR
    private short pcRetr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-RENDTO-RETR
    private short rendtoRetr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-MIN-GARTO
    private short minGarto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-MIN-TRNUT
    private short minTrnut = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-ATT-DI-TRCH
    private short preAttDiTrch = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-MATU-END2000
    private short matuEnd2000 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ABB-TOT-INI
    private short abbTotIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ABB-TOT-ULT
    private short abbTotUlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ABB-ANNU-ULT
    private short abbAnnuUlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-DUR-ABB
    private short durAbb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-TP-ADEG-ABB
    private short tpAdegAbb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-MOD-CALC
    private short modCalc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-AZ
    private short impAz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-ADER
    private short impAder = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-TFR
    private short impTfr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-VOLO
    private short impVolo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-VIS-END2000
    private short visEnd2000 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-DT-VLDT-PROD
    private short dtVldtProd = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-DT-INI-VAL-TAR
    private short dtIniValTar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMPB-VIS-END2000
    private short impbVisEnd2000 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-REN-INI-TS-TEC-0
    private short renIniTsTec0 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PC-RIP-PRE
    private short pcRipPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-FL-IMPORTI-FORZ
    private short flImportiForz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRSTZ-INI-NFORZ
    private short prstzIniNforz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-VIS-END2000-NFORZ
    private short visEnd2000Nforz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-INTR-MORA
    private short intrMora = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-MANFEE-ANTIC
    private short manfeeAntic = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-MANFEE-RICOR
    private short manfeeRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-UNI-RIVTO
    private short preUniRivto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PROV-1AA-ACQ
    private short prov1aaAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PROV-2AA-ACQ
    private short prov2aaAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PROV-RICOR
    private short provRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PROV-INC
    private short provInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ALQ-PROV-ACQ
    private short alqProvAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ALQ-PROV-INC
    private short alqProvInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ALQ-PROV-RICOR
    private short alqProvRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMPB-PROV-ACQ
    private short impbProvAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMPB-PROV-INC
    private short impbProvInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMPB-PROV-RICOR
    private short impbProvRicor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-FL-PROV-FORZ
    private short flProvForz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRSTZ-AGG-INI
    private short prstzAggIni = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-INCR-PRE
    private short incrPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-INCR-PRSTZ
    private short incrPrstz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-DT-ULT-ADEG-PRE-PR
    private short dtUltAdegPrePr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRSTZ-AGG-ULT
    private short prstzAggUlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-TS-RIVAL-NET
    private short tsRivalNet = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PRE-PATTUITO
    private short prePattuito = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-TP-RIVAL
    private short tpRival = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-RIS-MAT
    private short risMat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-CPT-MIN-SCAD
    private short cptMinScad = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-COMMIS-GEST
    private short commisGest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-TP-MANFEE-APPL
    private short tpManfeeAppl = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-PC-COMMIS-GEST
    private short pcCommisGest = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-NUM-GG-RIVAL
    private short numGgRival = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-TRASFE
    private short impTrasfe = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMP-TFR-STRC
    private short impTfrStrc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ACQ-EXP
    private short acqExp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-REMUN-ASS
    private short remunAss = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-COMMIS-INTER
    private short commisInter = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ALQ-REMUN-ASS
    private short alqRemunAss = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-ALQ-COMMIS-INTER
    private short alqCommisInter = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMPB-REMUN-ASS
    private short impbRemunAss = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-IMPB-COMMIS-INTER
    private short impbCommisInter = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-COS-RUN-ASSVA
    private short cosRunAssva = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA-COS-RUN-ASSVA-IDC
    private short cosRunAssvaIdc = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtScad(short dtScad) {
        this.dtScad = dtScad;
    }

    public short getDtScad() {
        return this.dtScad;
    }

    public void setIbOgg(short ibOgg) {
        this.ibOgg = ibOgg;
    }

    public short getIbOgg() {
        return this.ibOgg;
    }

    public void setDtEmis(short dtEmis) {
        this.dtEmis = dtEmis;
    }

    public short getDtEmis() {
        return this.dtEmis;
    }

    public void setDurAa(short durAa) {
        this.durAa = durAa;
    }

    public short getDurAa() {
        return this.durAa;
    }

    public void setDurMm(short durMm) {
        this.durMm = durMm;
    }

    public short getDurMm() {
        return this.durMm;
    }

    public void setDurGg(short durGg) {
        this.durGg = durGg;
    }

    public short getDurGg() {
        return this.durGg;
    }

    public void setPreCasoMor(short preCasoMor) {
        this.preCasoMor = preCasoMor;
    }

    public short getPreCasoMor() {
        return this.preCasoMor;
    }

    public void setPcIntrRiat(short pcIntrRiat) {
        this.pcIntrRiat = pcIntrRiat;
    }

    public short getPcIntrRiat() {
        return this.pcIntrRiat;
    }

    public void setImpBnsAntic(short impBnsAntic) {
        this.impBnsAntic = impBnsAntic;
    }

    public short getImpBnsAntic() {
        return this.impBnsAntic;
    }

    public void setPreIniNet(short preIniNet) {
        this.preIniNet = preIniNet;
    }

    public short getPreIniNet() {
        return this.preIniNet;
    }

    public void setPrePpIni(short prePpIni) {
        this.prePpIni = prePpIni;
    }

    public short getPrePpIni() {
        return this.prePpIni;
    }

    public void setPrePpUlt(short prePpUlt) {
        this.prePpUlt = prePpUlt;
    }

    public short getPrePpUlt() {
        return this.prePpUlt;
    }

    public void setPreTariIni(short preTariIni) {
        this.preTariIni = preTariIni;
    }

    public short getPreTariIni() {
        return this.preTariIni;
    }

    public void setPreTariUlt(short preTariUlt) {
        this.preTariUlt = preTariUlt;
    }

    public short getPreTariUlt() {
        return this.preTariUlt;
    }

    public void setPreInvrioIni(short preInvrioIni) {
        this.preInvrioIni = preInvrioIni;
    }

    public short getPreInvrioIni() {
        return this.preInvrioIni;
    }

    public void setPreInvrioUlt(short preInvrioUlt) {
        this.preInvrioUlt = preInvrioUlt;
    }

    public short getPreInvrioUlt() {
        return this.preInvrioUlt;
    }

    public void setPreRivto(short preRivto) {
        this.preRivto = preRivto;
    }

    public short getPreRivto() {
        return this.preRivto;
    }

    public void setImpSoprProf(short impSoprProf) {
        this.impSoprProf = impSoprProf;
    }

    public short getImpSoprProf() {
        return this.impSoprProf;
    }

    public void setImpSoprSan(short impSoprSan) {
        this.impSoprSan = impSoprSan;
    }

    public short getImpSoprSan() {
        return this.impSoprSan;
    }

    public void setImpSoprSpo(short impSoprSpo) {
        this.impSoprSpo = impSoprSpo;
    }

    public short getImpSoprSpo() {
        return this.impSoprSpo;
    }

    public void setImpSoprTec(short impSoprTec) {
        this.impSoprTec = impSoprTec;
    }

    public short getImpSoprTec() {
        return this.impSoprTec;
    }

    public void setImpAltSopr(short impAltSopr) {
        this.impAltSopr = impAltSopr;
    }

    public short getImpAltSopr() {
        return this.impAltSopr;
    }

    public void setPreStab(short preStab) {
        this.preStab = preStab;
    }

    public short getPreStab() {
        return this.preStab;
    }

    public void setDtEffStab(short dtEffStab) {
        this.dtEffStab = dtEffStab;
    }

    public short getDtEffStab() {
        return this.dtEffStab;
    }

    public void setTsRivalFis(short tsRivalFis) {
        this.tsRivalFis = tsRivalFis;
    }

    public short getTsRivalFis() {
        return this.tsRivalFis;
    }

    public void setTsRivalIndiciz(short tsRivalIndiciz) {
        this.tsRivalIndiciz = tsRivalIndiciz;
    }

    public short getTsRivalIndiciz() {
        return this.tsRivalIndiciz;
    }

    public void setOldTsTec(short oldTsTec) {
        this.oldTsTec = oldTsTec;
    }

    public short getOldTsTec() {
        return this.oldTsTec;
    }

    public void setRatLrd(short ratLrd) {
        this.ratLrd = ratLrd;
    }

    public short getRatLrd() {
        return this.ratLrd;
    }

    public void setPreLrd(short preLrd) {
        this.preLrd = preLrd;
    }

    public short getPreLrd() {
        return this.preLrd;
    }

    public void setPrstzIni(short prstzIni) {
        this.prstzIni = prstzIni;
    }

    public short getPrstzIni() {
        return this.prstzIni;
    }

    public void setPrstzUlt(short prstzUlt) {
        this.prstzUlt = prstzUlt;
    }

    public short getPrstzUlt() {
        return this.prstzUlt;
    }

    public void setCptInOpzRivto(short cptInOpzRivto) {
        this.cptInOpzRivto = cptInOpzRivto;
    }

    public short getCptInOpzRivto() {
        return this.cptInOpzRivto;
    }

    public void setPrstzIniStab(short prstzIniStab) {
        this.prstzIniStab = prstzIniStab;
    }

    public short getPrstzIniStab() {
        return this.prstzIniStab;
    }

    public void setCptRshMor(short cptRshMor) {
        this.cptRshMor = cptRshMor;
    }

    public short getCptRshMor() {
        return this.cptRshMor;
    }

    public void setPrstzRidIni(short prstzRidIni) {
        this.prstzRidIni = prstzRidIni;
    }

    public short getPrstzRidIni() {
        return this.prstzRidIni;
    }

    public void setFlCarCont(short flCarCont) {
        this.flCarCont = flCarCont;
    }

    public short getFlCarCont() {
        return this.flCarCont;
    }

    public void setBnsGiaLiqto(short bnsGiaLiqto) {
        this.bnsGiaLiqto = bnsGiaLiqto;
    }

    public short getBnsGiaLiqto() {
        return this.bnsGiaLiqto;
    }

    public void setImpBns(short impBns) {
        this.impBns = impBns;
    }

    public short getImpBns() {
        return this.impBns;
    }

    public void setPrstzIniNewfis(short prstzIniNewfis) {
        this.prstzIniNewfis = prstzIniNewfis;
    }

    public short getPrstzIniNewfis() {
        return this.prstzIniNewfis;
    }

    public void setImpScon(short impScon) {
        this.impScon = impScon;
    }

    public short getImpScon() {
        return this.impScon;
    }

    public void setAlqScon(short alqScon) {
        this.alqScon = alqScon;
    }

    public short getAlqScon() {
        return this.alqScon;
    }

    public void setImpCarAcq(short impCarAcq) {
        this.impCarAcq = impCarAcq;
    }

    public short getImpCarAcq() {
        return this.impCarAcq;
    }

    public void setImpCarInc(short impCarInc) {
        this.impCarInc = impCarInc;
    }

    public short getImpCarInc() {
        return this.impCarInc;
    }

    public void setImpCarGest(short impCarGest) {
        this.impCarGest = impCarGest;
    }

    public short getImpCarGest() {
        return this.impCarGest;
    }

    public void setEtaAa1oAssto(short etaAa1oAssto) {
        this.etaAa1oAssto = etaAa1oAssto;
    }

    public short getEtaAa1oAssto() {
        return this.etaAa1oAssto;
    }

    public void setEtaMm1oAssto(short etaMm1oAssto) {
        this.etaMm1oAssto = etaMm1oAssto;
    }

    public short getEtaMm1oAssto() {
        return this.etaMm1oAssto;
    }

    public void setEtaAa2oAssto(short etaAa2oAssto) {
        this.etaAa2oAssto = etaAa2oAssto;
    }

    public short getEtaAa2oAssto() {
        return this.etaAa2oAssto;
    }

    public void setEtaMm2oAssto(short etaMm2oAssto) {
        this.etaMm2oAssto = etaMm2oAssto;
    }

    public short getEtaMm2oAssto() {
        return this.etaMm2oAssto;
    }

    public void setEtaAa3oAssto(short etaAa3oAssto) {
        this.etaAa3oAssto = etaAa3oAssto;
    }

    public short getEtaAa3oAssto() {
        return this.etaAa3oAssto;
    }

    public void setEtaMm3oAssto(short etaMm3oAssto) {
        this.etaMm3oAssto = etaMm3oAssto;
    }

    public short getEtaMm3oAssto() {
        return this.etaMm3oAssto;
    }

    public void setRendtoLrd(short rendtoLrd) {
        this.rendtoLrd = rendtoLrd;
    }

    public short getRendtoLrd() {
        return this.rendtoLrd;
    }

    public void setPcRetr(short pcRetr) {
        this.pcRetr = pcRetr;
    }

    public short getPcRetr() {
        return this.pcRetr;
    }

    public void setRendtoRetr(short rendtoRetr) {
        this.rendtoRetr = rendtoRetr;
    }

    public short getRendtoRetr() {
        return this.rendtoRetr;
    }

    public void setMinGarto(short minGarto) {
        this.minGarto = minGarto;
    }

    public short getMinGarto() {
        return this.minGarto;
    }

    public void setMinTrnut(short minTrnut) {
        this.minTrnut = minTrnut;
    }

    public short getMinTrnut() {
        return this.minTrnut;
    }

    public void setPreAttDiTrch(short preAttDiTrch) {
        this.preAttDiTrch = preAttDiTrch;
    }

    public short getPreAttDiTrch() {
        return this.preAttDiTrch;
    }

    public void setMatuEnd2000(short matuEnd2000) {
        this.matuEnd2000 = matuEnd2000;
    }

    public short getMatuEnd2000() {
        return this.matuEnd2000;
    }

    public void setAbbTotIni(short abbTotIni) {
        this.abbTotIni = abbTotIni;
    }

    public short getAbbTotIni() {
        return this.abbTotIni;
    }

    public void setAbbTotUlt(short abbTotUlt) {
        this.abbTotUlt = abbTotUlt;
    }

    public short getAbbTotUlt() {
        return this.abbTotUlt;
    }

    public void setAbbAnnuUlt(short abbAnnuUlt) {
        this.abbAnnuUlt = abbAnnuUlt;
    }

    public short getAbbAnnuUlt() {
        return this.abbAnnuUlt;
    }

    public void setDurAbb(short durAbb) {
        this.durAbb = durAbb;
    }

    public short getDurAbb() {
        return this.durAbb;
    }

    public void setTpAdegAbb(short tpAdegAbb) {
        this.tpAdegAbb = tpAdegAbb;
    }

    public short getTpAdegAbb() {
        return this.tpAdegAbb;
    }

    public void setModCalc(short modCalc) {
        this.modCalc = modCalc;
    }

    public short getModCalc() {
        return this.modCalc;
    }

    public void setImpAz(short impAz) {
        this.impAz = impAz;
    }

    public short getImpAz() {
        return this.impAz;
    }

    public void setImpAder(short impAder) {
        this.impAder = impAder;
    }

    public short getImpAder() {
        return this.impAder;
    }

    public void setImpTfr(short impTfr) {
        this.impTfr = impTfr;
    }

    public short getImpTfr() {
        return this.impTfr;
    }

    public void setImpVolo(short impVolo) {
        this.impVolo = impVolo;
    }

    public short getImpVolo() {
        return this.impVolo;
    }

    public void setVisEnd2000(short visEnd2000) {
        this.visEnd2000 = visEnd2000;
    }

    public short getVisEnd2000() {
        return this.visEnd2000;
    }

    public void setDtVldtProd(short dtVldtProd) {
        this.dtVldtProd = dtVldtProd;
    }

    public short getDtVldtProd() {
        return this.dtVldtProd;
    }

    public void setDtIniValTar(short dtIniValTar) {
        this.dtIniValTar = dtIniValTar;
    }

    public short getDtIniValTar() {
        return this.dtIniValTar;
    }

    public void setImpbVisEnd2000(short impbVisEnd2000) {
        this.impbVisEnd2000 = impbVisEnd2000;
    }

    public short getImpbVisEnd2000() {
        return this.impbVisEnd2000;
    }

    public void setRenIniTsTec0(short renIniTsTec0) {
        this.renIniTsTec0 = renIniTsTec0;
    }

    public short getRenIniTsTec0() {
        return this.renIniTsTec0;
    }

    public void setPcRipPre(short pcRipPre) {
        this.pcRipPre = pcRipPre;
    }

    public short getPcRipPre() {
        return this.pcRipPre;
    }

    public void setFlImportiForz(short flImportiForz) {
        this.flImportiForz = flImportiForz;
    }

    public short getFlImportiForz() {
        return this.flImportiForz;
    }

    public void setPrstzIniNforz(short prstzIniNforz) {
        this.prstzIniNforz = prstzIniNforz;
    }

    public short getPrstzIniNforz() {
        return this.prstzIniNforz;
    }

    public void setVisEnd2000Nforz(short visEnd2000Nforz) {
        this.visEnd2000Nforz = visEnd2000Nforz;
    }

    public short getVisEnd2000Nforz() {
        return this.visEnd2000Nforz;
    }

    public void setIntrMora(short intrMora) {
        this.intrMora = intrMora;
    }

    public short getIntrMora() {
        return this.intrMora;
    }

    public void setManfeeAntic(short manfeeAntic) {
        this.manfeeAntic = manfeeAntic;
    }

    public short getManfeeAntic() {
        return this.manfeeAntic;
    }

    public void setManfeeRicor(short manfeeRicor) {
        this.manfeeRicor = manfeeRicor;
    }

    public short getManfeeRicor() {
        return this.manfeeRicor;
    }

    public void setPreUniRivto(short preUniRivto) {
        this.preUniRivto = preUniRivto;
    }

    public short getPreUniRivto() {
        return this.preUniRivto;
    }

    public void setProv1aaAcq(short prov1aaAcq) {
        this.prov1aaAcq = prov1aaAcq;
    }

    public short getProv1aaAcq() {
        return this.prov1aaAcq;
    }

    public void setProv2aaAcq(short prov2aaAcq) {
        this.prov2aaAcq = prov2aaAcq;
    }

    public short getProv2aaAcq() {
        return this.prov2aaAcq;
    }

    public void setProvRicor(short provRicor) {
        this.provRicor = provRicor;
    }

    public short getProvRicor() {
        return this.provRicor;
    }

    public void setProvInc(short provInc) {
        this.provInc = provInc;
    }

    public short getProvInc() {
        return this.provInc;
    }

    public void setAlqProvAcq(short alqProvAcq) {
        this.alqProvAcq = alqProvAcq;
    }

    public short getAlqProvAcq() {
        return this.alqProvAcq;
    }

    public void setAlqProvInc(short alqProvInc) {
        this.alqProvInc = alqProvInc;
    }

    public short getAlqProvInc() {
        return this.alqProvInc;
    }

    public void setAlqProvRicor(short alqProvRicor) {
        this.alqProvRicor = alqProvRicor;
    }

    public short getAlqProvRicor() {
        return this.alqProvRicor;
    }

    public void setImpbProvAcq(short impbProvAcq) {
        this.impbProvAcq = impbProvAcq;
    }

    public short getImpbProvAcq() {
        return this.impbProvAcq;
    }

    public void setImpbProvInc(short impbProvInc) {
        this.impbProvInc = impbProvInc;
    }

    public short getImpbProvInc() {
        return this.impbProvInc;
    }

    public void setImpbProvRicor(short impbProvRicor) {
        this.impbProvRicor = impbProvRicor;
    }

    public short getImpbProvRicor() {
        return this.impbProvRicor;
    }

    public void setFlProvForz(short flProvForz) {
        this.flProvForz = flProvForz;
    }

    public short getFlProvForz() {
        return this.flProvForz;
    }

    public void setPrstzAggIni(short prstzAggIni) {
        this.prstzAggIni = prstzAggIni;
    }

    public short getPrstzAggIni() {
        return this.prstzAggIni;
    }

    public void setIncrPre(short incrPre) {
        this.incrPre = incrPre;
    }

    public short getIncrPre() {
        return this.incrPre;
    }

    public void setIncrPrstz(short incrPrstz) {
        this.incrPrstz = incrPrstz;
    }

    public short getIncrPrstz() {
        return this.incrPrstz;
    }

    public void setDtUltAdegPrePr(short dtUltAdegPrePr) {
        this.dtUltAdegPrePr = dtUltAdegPrePr;
    }

    public short getDtUltAdegPrePr() {
        return this.dtUltAdegPrePr;
    }

    public void setPrstzAggUlt(short prstzAggUlt) {
        this.prstzAggUlt = prstzAggUlt;
    }

    public short getPrstzAggUlt() {
        return this.prstzAggUlt;
    }

    public void setTsRivalNet(short tsRivalNet) {
        this.tsRivalNet = tsRivalNet;
    }

    public short getTsRivalNet() {
        return this.tsRivalNet;
    }

    public void setPrePattuito(short prePattuito) {
        this.prePattuito = prePattuito;
    }

    public short getPrePattuito() {
        return this.prePattuito;
    }

    public void setTpRival(short tpRival) {
        this.tpRival = tpRival;
    }

    public short getTpRival() {
        return this.tpRival;
    }

    public void setRisMat(short risMat) {
        this.risMat = risMat;
    }

    public short getRisMat() {
        return this.risMat;
    }

    public void setCptMinScad(short cptMinScad) {
        this.cptMinScad = cptMinScad;
    }

    public short getCptMinScad() {
        return this.cptMinScad;
    }

    public void setCommisGest(short commisGest) {
        this.commisGest = commisGest;
    }

    public short getCommisGest() {
        return this.commisGest;
    }

    public void setTpManfeeAppl(short tpManfeeAppl) {
        this.tpManfeeAppl = tpManfeeAppl;
    }

    public short getTpManfeeAppl() {
        return this.tpManfeeAppl;
    }

    public void setPcCommisGest(short pcCommisGest) {
        this.pcCommisGest = pcCommisGest;
    }

    public short getPcCommisGest() {
        return this.pcCommisGest;
    }

    public void setNumGgRival(short numGgRival) {
        this.numGgRival = numGgRival;
    }

    public short getNumGgRival() {
        return this.numGgRival;
    }

    public void setImpTrasfe(short impTrasfe) {
        this.impTrasfe = impTrasfe;
    }

    public short getImpTrasfe() {
        return this.impTrasfe;
    }

    public void setImpTfrStrc(short impTfrStrc) {
        this.impTfrStrc = impTfrStrc;
    }

    public short getImpTfrStrc() {
        return this.impTfrStrc;
    }

    public void setAcqExp(short acqExp) {
        this.acqExp = acqExp;
    }

    public short getAcqExp() {
        return this.acqExp;
    }

    public void setRemunAss(short remunAss) {
        this.remunAss = remunAss;
    }

    public short getRemunAss() {
        return this.remunAss;
    }

    public void setCommisInter(short commisInter) {
        this.commisInter = commisInter;
    }

    public short getCommisInter() {
        return this.commisInter;
    }

    public void setAlqRemunAss(short alqRemunAss) {
        this.alqRemunAss = alqRemunAss;
    }

    public short getAlqRemunAss() {
        return this.alqRemunAss;
    }

    public void setAlqCommisInter(short alqCommisInter) {
        this.alqCommisInter = alqCommisInter;
    }

    public short getAlqCommisInter() {
        return this.alqCommisInter;
    }

    public void setImpbRemunAss(short impbRemunAss) {
        this.impbRemunAss = impbRemunAss;
    }

    public short getImpbRemunAss() {
        return this.impbRemunAss;
    }

    public void setImpbCommisInter(short impbCommisInter) {
        this.impbCommisInter = impbCommisInter;
    }

    public short getImpbCommisInter() {
        return this.impbCommisInter;
    }

    public void setCosRunAssva(short cosRunAssva) {
        this.cosRunAssva = cosRunAssva;
    }

    public short getCosRunAssva() {
        return this.cosRunAssva;
    }

    public void setCosRunAssvaIdc(short cosRunAssvaIdc) {
        this.cosRunAssvaIdc = cosRunAssvaIdc;
    }

    public short getCosRunAssvaIdc() {
        return this.cosRunAssvaIdc;
    }
}
