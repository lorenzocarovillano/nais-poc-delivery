package it.accenture.jnais.copy;

/**Original name: IVVC0218<br>
 * Variable: IVVC0218 from copybook IVVC0218<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0218Ivvs0216 {

    //==== PROPERTIES ====
    /**Original name: C214-ALIAS-ADES<br>
	 * <pre>----------------------------------------------------------------*
	 *    ALIAS INPUT VARIABILI
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA
	 *    LUNG.
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 * ASSET
	 * *****************************************************************
	 * --  ALIAS ADESIONE</pre>*/
    private String aliasAdes = "ADE";
    /**Original name: C214-ALIAS-BENEF<br>
	 * <pre>--  ALIAS BENEFICIARI</pre>*/
    private String aliasBenef = "BEP";
    /**Original name: C214-ALIAS-BENEF-LIQ<br>
	 * <pre>--  ALIAS BENEFICIARIO DI LIQUIDAZIONE</pre>*/
    private String aliasBenefLiq = "BEL";
    /**Original name: C214-ALIAS-DT-COLL<br>
	 * <pre>--  ALIAS DATI COLLETTIVA</pre>*/
    private String aliasDtColl = "DCO";
    /**Original name: C214-ALIAS-DT-FISC-ADES<br>
	 * <pre>--  ALIAS DATI FISCALE ADESIONE</pre>*/
    private String aliasDtFiscAdes = "DFA";
    /**Original name: C214-ALIAS-DETT-QUEST<br>
	 * <pre>--  ALIAS DETTAGLIO QUESTIONARIO</pre>*/
    private String aliasDettQuest = "DEQ";
    /**Original name: C214-ALIAS-DETT-TIT-CONT<br>
	 * <pre>--  ALIAS DETTAGLIO TITOLO CONTABILE</pre>*/
    private String aliasDettTitCont = "DTC";
    /**Original name: C214-ALIAS-GARANZIA<br>
	 * <pre>--  ALIAS GARANZIA</pre>*/
    private String aliasGaranzia = "GRZ";
    /**Original name: C214-ALIAS-GAR-LIQ<br>
	 * <pre>--  ALIAS GARANZIA DI LIQUIDAZIONE</pre>*/
    private String aliasGarLiq = "GRL";
    /**Original name: C214-ALIAS-IMPOSTA-BOLLO<br>
	 * <pre>--  ALIAS IMPOSTA DI BOLLO</pre>*/
    private String aliasImpostaBollo = "P58";
    /**Original name: C214-ALIAS-DATI-CRIST<br>
	 * <pre>--  ALIAS DATI CRISTALLIZZATI</pre>*/
    private String aliasDatiCrist = "P61";
    /**Original name: C214-ALIAS-IMPOSTA-SOST<br>
	 * <pre>--  ALIAS IMPOSTA SOSTITUTIVA</pre>*/
    private String aliasImpostaSost = "ISO";
    /**Original name: C214-ALIAS-LIQUIDAZ<br>
	 * <pre>--  ALIAS LIQUIDAZIONE</pre>*/
    private String aliasLiquidaz = "LQU";
    /**Original name: C214-ALIAS-MOVIMENTO<br>
	 * <pre>--  ALIAS MOVIMENTO</pre>*/
    private String aliasMovimento = "MOV";
    /**Original name: C214-ALIAS-PARAM-OGG<br>
	 * <pre>--  ALIAS PARAMETRO OGGETTO</pre>*/
    private String aliasParamOgg = "POG";
    /**Original name: C214-ALIAS-PARAM-MOV<br>
	 * <pre>--  ALIAS PARAMETRO MOVIMENTO</pre>*/
    private String aliasParamMov = "PMO";
    /**Original name: C214-ALIAS-PERC-LIQ<br>
	 * <pre>--  ALIAS PERCIPIENTE LIQUIDAZIONE</pre>*/
    private String aliasPercLiq = "PLI";
    /**Original name: C214-ALIAS-POLI<br>
	 * <pre>--  ALIAS POLIZZA</pre>*/
    private String aliasPoli = "POL";
    /**Original name: C214-ALIAS-PROVV-TRAN<br>
	 * <pre>--  ALIAS PROVVIGIONE DI TRANCHE</pre>*/
    private String aliasProvvTran = "PVT";
    /**Original name: C214-ALIAS-QUEST<br>
	 * <pre>--  ALIAS QUESTIONARIO</pre>*/
    private String aliasQuest = "QUE";
    /**Original name: C214-ALIAS-RICH-DISINV-FND<br>
	 * <pre>--  ALIAS RICHIESTA DISINVESTIMENTO FONDO</pre>*/
    private String aliasRichDisinvFnd = "RDF";
    /**Original name: C214-ALIAS-RICH-INV-FND<br>
	 * <pre>--  ALIAS RICHIESTA INVESTIMENTO FONDO</pre>*/
    private String aliasRichInvFnd = "RIF";
    /**Original name: C214-ALIAS-RAPP-ANAG<br>
	 * <pre>--  ALIAS RAPPORTO ANAGRAFICO</pre>*/
    private String aliasRappAnag = "RAN";
    /**Original name: C214-ALIAS-RAPP-RETE<br>
	 * <pre>--  ALIAS RAPPORTO RETE</pre>*/
    private String aliasRappRete = "RRE";
    /**Original name: C214-ALIAS-SOPRAP-GAR<br>
	 * <pre>--  ALIAS SOPRAPREMIO DI GARANZIA</pre>*/
    private String aliasSoprapGar = "SPG";
    /**Original name: C214-ALIAS-STRA-INV<br>
	 * <pre>--  ALIAS STRATEGIA DI INVESTIMENTO</pre>*/
    private String aliasStraInv = "SDI";
    /**Original name: C214-ALIAS-TIT-CONT<br>
	 * <pre>--  ALIAS TITOLO CONTABILE</pre>*/
    private String aliasTitCont = "TIT";
    /**Original name: C214-ALIAS-TRCH-GAR<br>
	 * <pre>--  ALIAS TRANCHE DI GARANZIA</pre>*/
    private String aliasTrchGar = "TGA";
    /**Original name: C214-ALIAS-TRCH-LIQ<br>
	 * <pre>-- ALIAS TRANCHE GARANZIA DI LIQUIDAZIONE</pre>*/
    private String aliasTrchLiq = "TLI";
    /**Original name: C214-ALIAS-DFLT-ADES<br>
	 * <pre>-- ALIAS DEFAULT ADESIONE</pre>*/
    private String aliasDfltAdes = "DAD";
    /**Original name: C214-ALIAS-OGG-COLL<br>
	 * <pre>-- ALIAS OGGETTO COLLEGATO</pre>*/
    private String aliasOggColl = "OCO";
    /**Original name: C214-ALIAS-PARAM-COMP<br>
	 * <pre>--  ALIAS parametro compagnia</pre>*/
    private String aliasParamComp = "PCO";
    /**Original name: C214-ALIAS-DOC-LIQ<br>
	 * <pre>--  ALIAS DOC IN LIQUIDAZIONE</pre>*/
    private String aliasDocLiq = "DLQ";
    /**Original name: C214-ALIAS-VINC-PEGN<br>
	 * <pre>--  ALIAS VINCOLI E PEGNI</pre>*/
    private String aliasVincPegn = "L23";
    /**Original name: C214-ALIAS-REINVST-POLI<br>
	 * <pre>--  ALIAS REINVESTIMENTO POLIZZA</pre>*/
    private String aliasReinvstPoli = "L30";
    /**Original name: C214-ALIAS-QOTAZ-FON<br>
	 * <pre>--  ALIAS REINVESTIMENTO POLIZZA</pre>*/
    private String aliasQotazFon = "L19";
    /**Original name: C214-ALIAS-RIS-DI-TRANCHE<br>
	 * <pre>--  ALIAR RIS DI TRANCHE</pre>*/
    private String aliasRisDiTranche = "RST";
    /**Original name: C214-ALIAS-AREA-FND-X-TRCH<br>
	 * <pre>--  ALIAS AREA FONDI X TRANCHE</pre>*/
    private String aliasAreaFndXTrch = "FXT";
    /**Original name: C214-ALIAS-RICH-EST<br>
	 * <pre>--  ALIAS RICH_EST</pre>*/
    private String aliasRichEst = "P01";
    /**Original name: C214-ALIAS-QUEST-ADEG-VER<br>
	 * <pre>--  ALIAS QUEST ADEG VER</pre>*/
    private String aliasQuestAdegVer = "P56";
    /**Original name: C214-ALIAS-EST-POLI-CPI-PR<br>
	 * <pre>--  ALIAS EST POLI CPI PR</pre>*/
    private String aliasEstPoliCpiPr = "P67";
    /**Original name: C214-ALIAS-MOT-LIQ<br>
	 * <pre>-- ALIAS MOT-LIQ</pre>*/
    private String aliasMotLiq = "P86";
    /**Original name: C214-ALIAS-ATT-SERV-VAL<br>
	 * <pre>-- ALIAS ATT-SERV-VAL</pre>*/
    private String aliasAttServVal = "P88";
    /**Original name: C214-ALIAS-D-ATT-SERV-VAL<br>
	 * <pre>-- ALIAS D-ATT-SERV-VAL</pre>*/
    private String aliasDAttServVal = "P89";
    /**Original name: C214-ALIAS-EST-TRCH-DI-GAR<br>
	 * <pre>--  ALIAS EST TRCH DI GAR</pre>*/
    private String aliasEstTrchDiGar = "E12";
    /**Original name: C214-ALIAS-AREA-VAR-X-GAR<br>
	 * <pre>--  ALIAS BILA PROV AMM T</pre>*/
    private String aliasAreaVarXGar = "VXG";
    /**Original name: C214-ALIAS-AREA-FND-X-CDG<br>
	 * <pre>-- ALIAS FONDI PER COMMIS GEST</pre>*/
    private String aliasAreaFndXCdg = "FXC";

    //==== METHODS ====
    public String getAliasAdes() {
        return this.aliasAdes;
    }

    public String getAliasBenef() {
        return this.aliasBenef;
    }

    public String getAliasBenefLiq() {
        return this.aliasBenefLiq;
    }

    public String getAliasDtColl() {
        return this.aliasDtColl;
    }

    public String getAliasDtFiscAdes() {
        return this.aliasDtFiscAdes;
    }

    public String getAliasDettQuest() {
        return this.aliasDettQuest;
    }

    public String getAliasDettTitCont() {
        return this.aliasDettTitCont;
    }

    public String getAliasGaranzia() {
        return this.aliasGaranzia;
    }

    public String getAliasGarLiq() {
        return this.aliasGarLiq;
    }

    public String getAliasImpostaBollo() {
        return this.aliasImpostaBollo;
    }

    public String getAliasDatiCrist() {
        return this.aliasDatiCrist;
    }

    public String getAliasImpostaSost() {
        return this.aliasImpostaSost;
    }

    public String getAliasLiquidaz() {
        return this.aliasLiquidaz;
    }

    public String getAliasMovimento() {
        return this.aliasMovimento;
    }

    public String getAliasParamOgg() {
        return this.aliasParamOgg;
    }

    public String getAliasParamMov() {
        return this.aliasParamMov;
    }

    public String getAliasPercLiq() {
        return this.aliasPercLiq;
    }

    public String getAliasPoli() {
        return this.aliasPoli;
    }

    public String getAliasProvvTran() {
        return this.aliasProvvTran;
    }

    public String getAliasQuest() {
        return this.aliasQuest;
    }

    public String getAliasRichDisinvFnd() {
        return this.aliasRichDisinvFnd;
    }

    public String getAliasRichInvFnd() {
        return this.aliasRichInvFnd;
    }

    public String getAliasRappAnag() {
        return this.aliasRappAnag;
    }

    public String getAliasRappRete() {
        return this.aliasRappRete;
    }

    public String getAliasSoprapGar() {
        return this.aliasSoprapGar;
    }

    public String getAliasStraInv() {
        return this.aliasStraInv;
    }

    public String getAliasTitCont() {
        return this.aliasTitCont;
    }

    public String getAliasTrchGar() {
        return this.aliasTrchGar;
    }

    public String getAliasTrchLiq() {
        return this.aliasTrchLiq;
    }

    public String getAliasDfltAdes() {
        return this.aliasDfltAdes;
    }

    public String getAliasOggColl() {
        return this.aliasOggColl;
    }

    public String getAliasParamComp() {
        return this.aliasParamComp;
    }

    public String getAliasDocLiq() {
        return this.aliasDocLiq;
    }

    public String getAliasVincPegn() {
        return this.aliasVincPegn;
    }

    public String getAliasReinvstPoli() {
        return this.aliasReinvstPoli;
    }

    public String getAliasQotazFon() {
        return this.aliasQotazFon;
    }

    public String getAliasRisDiTranche() {
        return this.aliasRisDiTranche;
    }

    public String getAliasAreaFndXTrch() {
        return this.aliasAreaFndXTrch;
    }

    public String getAliasRichEst() {
        return this.aliasRichEst;
    }

    public String getAliasQuestAdegVer() {
        return this.aliasQuestAdegVer;
    }

    public String getAliasEstPoliCpiPr() {
        return this.aliasEstPoliCpiPr;
    }

    public String getAliasMotLiq() {
        return this.aliasMotLiq;
    }

    public String getAliasAttServVal() {
        return this.aliasAttServVal;
    }

    public String getAliasDAttServVal() {
        return this.aliasDAttServVal;
    }

    public String getAliasEstTrchDiGar() {
        return this.aliasEstTrchDiGar;
    }

    public String getAliasAreaVarXGar() {
        return this.aliasAreaVarXGar;
    }

    public String getAliasAreaFndXCdg() {
        return this.aliasAreaFndXCdg;
    }
}
