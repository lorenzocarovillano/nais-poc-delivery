package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WB05IdRichEstrazAggLlbs0230;
import it.accenture.jnais.ws.redefines.WB05ProgSchedaValorLlbs0230;
import it.accenture.jnais.ws.redefines.WB05ValImpLlbs0230;
import it.accenture.jnais.ws.redefines.WB05ValPcLlbs0230;

/**Original name: W-B05-REC-FISSO<br>
 * Variable: W-B05-REC-FISSO from copybook LCCVB058<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WB05RecFisso {

    //==== PROPERTIES ====
    //Original name: W-B05-ID-BILA-VAR-CALC-T
    private int wB05IdBilaVarCalcT = DefaultValues.INT_VAL;
    //Original name: W-B05-COD-COMP-ANIA
    private int wB05CodCompAnia = DefaultValues.INT_VAL;
    //Original name: W-B05-ID-BILA-TRCH-ESTR
    private int wB05IdBilaTrchEstr = DefaultValues.INT_VAL;
    //Original name: W-B05-ID-RICH-ESTRAZ-MAS
    private int wB05IdRichEstrazMas = DefaultValues.INT_VAL;
    //Original name: W-B05-ID-RICH-ESTRAZ-AGG-IND
    private char wB05IdRichEstrazAggInd = DefaultValues.CHAR_VAL;
    //Original name: W-B05-ID-RICH-ESTRAZ-AGG
    private WB05IdRichEstrazAggLlbs0230 wB05IdRichEstrazAgg = new WB05IdRichEstrazAggLlbs0230();
    //Original name: W-B05-DT-RIS-IND
    private char wB05DtRisInd = DefaultValues.CHAR_VAL;
    //Original name: W-B05-DT-RIS
    private String wB05DtRis = DefaultValues.stringVal(Len.W_B05_DT_RIS);
    //Original name: W-B05-ID-POLI
    private int wB05IdPoli = DefaultValues.INT_VAL;
    //Original name: W-B05-ID-ADES
    private int wB05IdAdes = DefaultValues.INT_VAL;
    //Original name: W-B05-ID-TRCH-DI-GAR
    private int wB05IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: W-B05-PROG-SCHEDA-VALOR-IND
    private char wB05ProgSchedaValorInd = DefaultValues.CHAR_VAL;
    //Original name: W-B05-PROG-SCHEDA-VALOR
    private WB05ProgSchedaValorLlbs0230 wB05ProgSchedaValor = new WB05ProgSchedaValorLlbs0230();
    //Original name: W-B05-DT-INI-VLDT-TARI
    private String wB05DtIniVldtTari = DefaultValues.stringVal(Len.W_B05_DT_INI_VLDT_TARI);
    //Original name: W-B05-TP-RGM-FISC
    private String wB05TpRgmFisc = DefaultValues.stringVal(Len.W_B05_TP_RGM_FISC);
    //Original name: W-B05-DT-INI-VLDT-PROD
    private String wB05DtIniVldtProd = DefaultValues.stringVal(Len.W_B05_DT_INI_VLDT_PROD);
    //Original name: W-B05-DT-DECOR-TRCH
    private String wB05DtDecorTrch = DefaultValues.stringVal(Len.W_B05_DT_DECOR_TRCH);
    //Original name: W-B05-COD-VAR
    private String wB05CodVar = DefaultValues.stringVal(Len.W_B05_COD_VAR);
    //Original name: W-B05-TP-D
    private char wB05TpD = DefaultValues.CHAR_VAL;
    //Original name: W-B05-VAL-IMP-IND
    private char wB05ValImpInd = DefaultValues.CHAR_VAL;
    //Original name: W-B05-VAL-IMP
    private WB05ValImpLlbs0230 wB05ValImp = new WB05ValImpLlbs0230();
    //Original name: W-B05-VAL-PC-IND
    private char wB05ValPcInd = DefaultValues.CHAR_VAL;
    //Original name: W-B05-VAL-PC
    private WB05ValPcLlbs0230 wB05ValPc = new WB05ValPcLlbs0230();
    //Original name: W-B05-VAL-STRINGA-IND
    private char wB05ValStringaInd = DefaultValues.CHAR_VAL;
    //Original name: W-B05-VAL-STRINGA
    private String wB05ValStringa = DefaultValues.stringVal(Len.W_B05_VAL_STRINGA);
    //Original name: W-B05-DS-OPER-SQL
    private char wB05DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: W-B05-DS-VER
    private int wB05DsVer = DefaultValues.INT_VAL;
    //Original name: W-B05-DS-TS-CPTZ
    private long wB05DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: W-B05-DS-UTENTE
    private String wB05DsUtente = DefaultValues.stringVal(Len.W_B05_DS_UTENTE);
    //Original name: W-B05-DS-STATO-ELAB
    private char wB05DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public byte[] getRecFissoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wB05IdBilaVarCalcT, Len.Int.W_B05_ID_BILA_VAR_CALC_T, 0);
        position += Len.W_B05_ID_BILA_VAR_CALC_T;
        MarshalByte.writeIntAsPacked(buffer, position, wB05CodCompAnia, Len.Int.W_B05_COD_COMP_ANIA, 0);
        position += Len.W_B05_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wB05IdBilaTrchEstr, Len.Int.W_B05_ID_BILA_TRCH_ESTR, 0);
        position += Len.W_B05_ID_BILA_TRCH_ESTR;
        MarshalByte.writeIntAsPacked(buffer, position, wB05IdRichEstrazMas, Len.Int.W_B05_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.W_B05_ID_RICH_ESTRAZ_MAS;
        MarshalByte.writeChar(buffer, position, wB05IdRichEstrazAggInd);
        position += Types.CHAR_SIZE;
        wB05IdRichEstrazAgg.getwB05IdRichEstrazAggAsBuffer(buffer, position);
        position += WB05IdRichEstrazAggLlbs0230.Len.W_B05_ID_RICH_ESTRAZ_AGG;
        MarshalByte.writeChar(buffer, position, wB05DtRisInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB05DtRis, Len.W_B05_DT_RIS);
        position += Len.W_B05_DT_RIS;
        MarshalByte.writeIntAsPacked(buffer, position, wB05IdPoli, Len.Int.W_B05_ID_POLI, 0);
        position += Len.W_B05_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wB05IdAdes, Len.Int.W_B05_ID_ADES, 0);
        position += Len.W_B05_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wB05IdTrchDiGar, Len.Int.W_B05_ID_TRCH_DI_GAR, 0);
        position += Len.W_B05_ID_TRCH_DI_GAR;
        MarshalByte.writeChar(buffer, position, wB05ProgSchedaValorInd);
        position += Types.CHAR_SIZE;
        wB05ProgSchedaValor.getwB05ProgSchedaValorAsBuffer(buffer, position);
        position += WB05ProgSchedaValorLlbs0230.Len.W_B05_PROG_SCHEDA_VALOR;
        MarshalByte.writeString(buffer, position, wB05DtIniVldtTari, Len.W_B05_DT_INI_VLDT_TARI);
        position += Len.W_B05_DT_INI_VLDT_TARI;
        MarshalByte.writeString(buffer, position, wB05TpRgmFisc, Len.W_B05_TP_RGM_FISC);
        position += Len.W_B05_TP_RGM_FISC;
        MarshalByte.writeString(buffer, position, wB05DtIniVldtProd, Len.W_B05_DT_INI_VLDT_PROD);
        position += Len.W_B05_DT_INI_VLDT_PROD;
        MarshalByte.writeString(buffer, position, wB05DtDecorTrch, Len.W_B05_DT_DECOR_TRCH);
        position += Len.W_B05_DT_DECOR_TRCH;
        MarshalByte.writeString(buffer, position, wB05CodVar, Len.W_B05_COD_VAR);
        position += Len.W_B05_COD_VAR;
        MarshalByte.writeChar(buffer, position, wB05TpD);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wB05ValImpInd);
        position += Types.CHAR_SIZE;
        wB05ValImp.getwB05ValImpAsBuffer(buffer, position);
        position += WB05ValImpLlbs0230.Len.W_B05_VAL_IMP;
        MarshalByte.writeChar(buffer, position, wB05ValPcInd);
        position += Types.CHAR_SIZE;
        wB05ValPc.getwB05ValPcAsBuffer(buffer, position);
        position += WB05ValPcLlbs0230.Len.W_B05_VAL_PC;
        MarshalByte.writeChar(buffer, position, wB05ValStringaInd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wB05ValStringa, Len.W_B05_VAL_STRINGA);
        position += Len.W_B05_VAL_STRINGA;
        MarshalByte.writeChar(buffer, position, wB05DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wB05DsVer, Len.Int.W_B05_DS_VER, 0);
        position += Len.W_B05_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wB05DsTsCptz, Len.Int.W_B05_DS_TS_CPTZ, 0);
        position += Len.W_B05_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, wB05DsUtente, Len.W_B05_DS_UTENTE);
        position += Len.W_B05_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wB05DsStatoElab);
        return buffer;
    }

    public void setwB05IdBilaVarCalcT(int wB05IdBilaVarCalcT) {
        this.wB05IdBilaVarCalcT = wB05IdBilaVarCalcT;
    }

    public int getwB05IdBilaVarCalcT() {
        return this.wB05IdBilaVarCalcT;
    }

    public void setwB05CodCompAnia(int wB05CodCompAnia) {
        this.wB05CodCompAnia = wB05CodCompAnia;
    }

    public int getwB05CodCompAnia() {
        return this.wB05CodCompAnia;
    }

    public void setwB05IdBilaTrchEstr(int wB05IdBilaTrchEstr) {
        this.wB05IdBilaTrchEstr = wB05IdBilaTrchEstr;
    }

    public int getwB05IdBilaTrchEstr() {
        return this.wB05IdBilaTrchEstr;
    }

    public void setwB05IdRichEstrazMas(int wB05IdRichEstrazMas) {
        this.wB05IdRichEstrazMas = wB05IdRichEstrazMas;
    }

    public int getwB05IdRichEstrazMas() {
        return this.wB05IdRichEstrazMas;
    }

    public void setwB05IdRichEstrazAggInd(char wB05IdRichEstrazAggInd) {
        this.wB05IdRichEstrazAggInd = wB05IdRichEstrazAggInd;
    }

    public void setwB05IdRichEstrazAggIndFormatted(String wB05IdRichEstrazAggInd) {
        setwB05IdRichEstrazAggInd(Functions.charAt(wB05IdRichEstrazAggInd, Types.CHAR_SIZE));
    }

    public char getwB05IdRichEstrazAggInd() {
        return this.wB05IdRichEstrazAggInd;
    }

    public void setwB05DtRisInd(char wB05DtRisInd) {
        this.wB05DtRisInd = wB05DtRisInd;
    }

    public void setwB05DtRisIndFormatted(String wB05DtRisInd) {
        setwB05DtRisInd(Functions.charAt(wB05DtRisInd, Types.CHAR_SIZE));
    }

    public char getwB05DtRisInd() {
        return this.wB05DtRisInd;
    }

    public void setwB05DtRis(String wB05DtRis) {
        this.wB05DtRis = Functions.subString(wB05DtRis, Len.W_B05_DT_RIS);
    }

    public String getwB05DtRis() {
        return this.wB05DtRis;
    }

    public void setwB05IdPoli(int wB05IdPoli) {
        this.wB05IdPoli = wB05IdPoli;
    }

    public int getwB05IdPoli() {
        return this.wB05IdPoli;
    }

    public void setwB05IdAdes(int wB05IdAdes) {
        this.wB05IdAdes = wB05IdAdes;
    }

    public int getwB05IdAdes() {
        return this.wB05IdAdes;
    }

    public void setwB05IdTrchDiGar(int wB05IdTrchDiGar) {
        this.wB05IdTrchDiGar = wB05IdTrchDiGar;
    }

    public int getwB05IdTrchDiGar() {
        return this.wB05IdTrchDiGar;
    }

    public void setwB05ProgSchedaValorInd(char wB05ProgSchedaValorInd) {
        this.wB05ProgSchedaValorInd = wB05ProgSchedaValorInd;
    }

    public void setwB05ProgSchedaValorIndFormatted(String wB05ProgSchedaValorInd) {
        setwB05ProgSchedaValorInd(Functions.charAt(wB05ProgSchedaValorInd, Types.CHAR_SIZE));
    }

    public char getwB05ProgSchedaValorInd() {
        return this.wB05ProgSchedaValorInd;
    }

    public void setwB05DtIniVldtTari(String wB05DtIniVldtTari) {
        this.wB05DtIniVldtTari = Functions.subString(wB05DtIniVldtTari, Len.W_B05_DT_INI_VLDT_TARI);
    }

    public String getwB05DtIniVldtTari() {
        return this.wB05DtIniVldtTari;
    }

    public void setwB05TpRgmFisc(String wB05TpRgmFisc) {
        this.wB05TpRgmFisc = Functions.subString(wB05TpRgmFisc, Len.W_B05_TP_RGM_FISC);
    }

    public String getwB05TpRgmFisc() {
        return this.wB05TpRgmFisc;
    }

    public void setwB05DtIniVldtProd(String wB05DtIniVldtProd) {
        this.wB05DtIniVldtProd = Functions.subString(wB05DtIniVldtProd, Len.W_B05_DT_INI_VLDT_PROD);
    }

    public String getwB05DtIniVldtProd() {
        return this.wB05DtIniVldtProd;
    }

    public void setwB05DtDecorTrch(String wB05DtDecorTrch) {
        this.wB05DtDecorTrch = Functions.subString(wB05DtDecorTrch, Len.W_B05_DT_DECOR_TRCH);
    }

    public String getwB05DtDecorTrch() {
        return this.wB05DtDecorTrch;
    }

    public void setwB05CodVar(String wB05CodVar) {
        this.wB05CodVar = Functions.subString(wB05CodVar, Len.W_B05_COD_VAR);
    }

    public String getwB05CodVar() {
        return this.wB05CodVar;
    }

    public void setwB05TpD(char wB05TpD) {
        this.wB05TpD = wB05TpD;
    }

    public char getwB05TpD() {
        return this.wB05TpD;
    }

    public void setwB05ValImpInd(char wB05ValImpInd) {
        this.wB05ValImpInd = wB05ValImpInd;
    }

    public void setwB05ValImpIndFormatted(String wB05ValImpInd) {
        setwB05ValImpInd(Functions.charAt(wB05ValImpInd, Types.CHAR_SIZE));
    }

    public char getwB05ValImpInd() {
        return this.wB05ValImpInd;
    }

    public void setwB05ValPcInd(char wB05ValPcInd) {
        this.wB05ValPcInd = wB05ValPcInd;
    }

    public void setwB05ValPcIndFormatted(String wB05ValPcInd) {
        setwB05ValPcInd(Functions.charAt(wB05ValPcInd, Types.CHAR_SIZE));
    }

    public char getwB05ValPcInd() {
        return this.wB05ValPcInd;
    }

    public void setwB05ValStringaInd(char wB05ValStringaInd) {
        this.wB05ValStringaInd = wB05ValStringaInd;
    }

    public void setwB05ValStringaIndFormatted(String wB05ValStringaInd) {
        setwB05ValStringaInd(Functions.charAt(wB05ValStringaInd, Types.CHAR_SIZE));
    }

    public char getwB05ValStringaInd() {
        return this.wB05ValStringaInd;
    }

    public void setwB05ValStringa(String wB05ValStringa) {
        this.wB05ValStringa = Functions.subString(wB05ValStringa, Len.W_B05_VAL_STRINGA);
    }

    public String getwB05ValStringa() {
        return this.wB05ValStringa;
    }

    public void setwB05DsOperSql(char wB05DsOperSql) {
        this.wB05DsOperSql = wB05DsOperSql;
    }

    public void setwB05DsOperSqlFormatted(String wB05DsOperSql) {
        setwB05DsOperSql(Functions.charAt(wB05DsOperSql, Types.CHAR_SIZE));
    }

    public char getwB05DsOperSql() {
        return this.wB05DsOperSql;
    }

    public void setwB05DsVer(int wB05DsVer) {
        this.wB05DsVer = wB05DsVer;
    }

    public int getwB05DsVer() {
        return this.wB05DsVer;
    }

    public void setwB05DsTsCptz(long wB05DsTsCptz) {
        this.wB05DsTsCptz = wB05DsTsCptz;
    }

    public long getwB05DsTsCptz() {
        return this.wB05DsTsCptz;
    }

    public void setwB05DsUtente(String wB05DsUtente) {
        this.wB05DsUtente = Functions.subString(wB05DsUtente, Len.W_B05_DS_UTENTE);
    }

    public String getwB05DsUtente() {
        return this.wB05DsUtente;
    }

    public void setwB05DsStatoElab(char wB05DsStatoElab) {
        this.wB05DsStatoElab = wB05DsStatoElab;
    }

    public void setwB05DsStatoElabFormatted(String wB05DsStatoElab) {
        setwB05DsStatoElab(Functions.charAt(wB05DsStatoElab, Types.CHAR_SIZE));
    }

    public char getwB05DsStatoElab() {
        return this.wB05DsStatoElab;
    }

    public WB05IdRichEstrazAggLlbs0230 getwB05IdRichEstrazAgg() {
        return wB05IdRichEstrazAgg;
    }

    public WB05ProgSchedaValorLlbs0230 getwB05ProgSchedaValor() {
        return wB05ProgSchedaValor;
    }

    public WB05ValImpLlbs0230 getwB05ValImp() {
        return wB05ValImp;
    }

    public WB05ValPcLlbs0230 getwB05ValPc() {
        return wB05ValPc;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B05_DT_RIS = 10;
        public static final int W_B05_DT_INI_VLDT_TARI = 10;
        public static final int W_B05_TP_RGM_FISC = 2;
        public static final int W_B05_DT_INI_VLDT_PROD = 10;
        public static final int W_B05_DT_DECOR_TRCH = 10;
        public static final int W_B05_COD_VAR = 30;
        public static final int W_B05_VAL_STRINGA = 60;
        public static final int W_B05_DS_UTENTE = 20;
        public static final int W_B05_ID_BILA_VAR_CALC_T = 5;
        public static final int W_B05_COD_COMP_ANIA = 3;
        public static final int W_B05_ID_BILA_TRCH_ESTR = 5;
        public static final int W_B05_ID_RICH_ESTRAZ_MAS = 5;
        public static final int W_B05_ID_RICH_ESTRAZ_AGG_IND = 1;
        public static final int W_B05_DT_RIS_IND = 1;
        public static final int W_B05_ID_POLI = 5;
        public static final int W_B05_ID_ADES = 5;
        public static final int W_B05_ID_TRCH_DI_GAR = 5;
        public static final int W_B05_PROG_SCHEDA_VALOR_IND = 1;
        public static final int W_B05_TP_D = 1;
        public static final int W_B05_VAL_IMP_IND = 1;
        public static final int W_B05_VAL_PC_IND = 1;
        public static final int W_B05_VAL_STRINGA_IND = 1;
        public static final int W_B05_DS_OPER_SQL = 1;
        public static final int W_B05_DS_VER = 5;
        public static final int W_B05_DS_TS_CPTZ = 10;
        public static final int W_B05_DS_STATO_ELAB = 1;
        public static final int REC_FISSO = W_B05_ID_BILA_VAR_CALC_T + W_B05_COD_COMP_ANIA + W_B05_ID_BILA_TRCH_ESTR + W_B05_ID_RICH_ESTRAZ_MAS + W_B05_ID_RICH_ESTRAZ_AGG_IND + WB05IdRichEstrazAggLlbs0230.Len.W_B05_ID_RICH_ESTRAZ_AGG + W_B05_DT_RIS_IND + W_B05_DT_RIS + W_B05_ID_POLI + W_B05_ID_ADES + W_B05_ID_TRCH_DI_GAR + W_B05_PROG_SCHEDA_VALOR_IND + WB05ProgSchedaValorLlbs0230.Len.W_B05_PROG_SCHEDA_VALOR + W_B05_DT_INI_VLDT_TARI + W_B05_TP_RGM_FISC + W_B05_DT_INI_VLDT_PROD + W_B05_DT_DECOR_TRCH + W_B05_COD_VAR + W_B05_TP_D + W_B05_VAL_IMP_IND + WB05ValImpLlbs0230.Len.W_B05_VAL_IMP + W_B05_VAL_PC_IND + WB05ValPcLlbs0230.Len.W_B05_VAL_PC + W_B05_VAL_STRINGA_IND + W_B05_VAL_STRINGA + W_B05_DS_OPER_SQL + W_B05_DS_VER + W_B05_DS_TS_CPTZ + W_B05_DS_UTENTE + W_B05_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B05_ID_BILA_VAR_CALC_T = 9;
            public static final int W_B05_COD_COMP_ANIA = 5;
            public static final int W_B05_ID_BILA_TRCH_ESTR = 9;
            public static final int W_B05_ID_RICH_ESTRAZ_MAS = 9;
            public static final int W_B05_ID_POLI = 9;
            public static final int W_B05_ID_ADES = 9;
            public static final int W_B05_ID_TRCH_DI_GAR = 9;
            public static final int W_B05_DS_VER = 9;
            public static final int W_B05_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
