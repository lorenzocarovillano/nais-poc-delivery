package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV2321<br>
 * Variable: LDBV2321 from copybook LDBV2321<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv2321 {

    //==== PROPERTIES ====
    //Original name: LDBV2321-ID-RAPP-ANA
    private int idRappAna = DefaultValues.INT_VAL;
    //Original name: LDBV2321-ID-GAR
    private int idGar = DefaultValues.INT_VAL;
    //Original name: LDBV2321-TP-QUEST
    private String tpQuest = DefaultValues.stringVal(Len.TP_QUEST);

    //==== METHODS ====
    public String getLdbv2321Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv2321Bytes());
    }

    public byte[] getLdbv2321Bytes() {
        byte[] buffer = new byte[Len.LDBV2321];
        return getLdbv2321Bytes(buffer, 1);
    }

    public byte[] getLdbv2321Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idRappAna, Len.Int.ID_RAPP_ANA, 0);
        position += Len.ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, idGar, Len.Int.ID_GAR, 0);
        position += Len.ID_GAR;
        MarshalByte.writeString(buffer, position, tpQuest, Len.TP_QUEST);
        return buffer;
    }

    public void setIdRappAna(int idRappAna) {
        this.idRappAna = idRappAna;
    }

    public int getIdRappAna() {
        return this.idRappAna;
    }

    public void setIdGar(int idGar) {
        this.idGar = idGar;
    }

    public int getIdGar() {
        return this.idGar;
    }

    public void setTpQuest(String tpQuest) {
        this.tpQuest = Functions.subString(tpQuest, Len.TP_QUEST);
    }

    public String getTpQuest() {
        return this.tpQuest;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_QUEST = 2;
        public static final int ID_RAPP_ANA = 5;
        public static final int ID_GAR = 5;
        public static final int LDBV2321 = ID_RAPP_ANA + ID_GAR + TP_QUEST;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_RAPP_ANA = 9;
            public static final int ID_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
