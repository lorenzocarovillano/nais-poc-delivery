package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Lvvc0000FormatDate;

/**Original name: LVVC0000<br>
 * Variable: LVVC0000 from copybook LVVC0000<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lvvc0000 {

    //==== PROPERTIES ====
    /**Original name: LVVC0000-FORMAT-DATE<br>
	 * <pre>----------------------------------------------------------------*
	 *  AREA CHIAMATA MODULI CALCOLO DATA
	 * ----------------------------------------------------------------*</pre>*/
    private Lvvc0000FormatDate formatDate = new Lvvc0000FormatDate();
    //Original name: LVVC0000-DATA-INPUT
    private String dataInput = DefaultValues.stringVal(Len.DATA_INPUT);
    //Original name: LVVC0000-DATA-INPUT-1
    private String dataInput1 = DefaultValues.stringVal(Len.DATA_INPUT1);
    //Original name: LVVC0000-DATI-INPUT-2
    private Lvvc0000DatiInput2 datiInput2 = new Lvvc0000DatiInput2();
    //Original name: LVVC0000-ANNI-INPUT-3
    private String anniInput3 = DefaultValues.stringVal(Len.ANNI_INPUT3);
    //Original name: LVVC0000-MESI-INPUT-3
    private String mesiInput3 = DefaultValues.stringVal(Len.MESI_INPUT3);
    //Original name: LVVC0000-DATA-OUTPUT
    private AfDecimal dataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);

    //==== METHODS ====
    public void setDataInputFormatted(String dataInput) {
        this.dataInput = Trunc.toUnsignedNumeric(dataInput, Len.DATA_INPUT);
    }

    public int getDataInput() {
        return NumericDisplay.asInt(this.dataInput);
    }

    public void setDataInput1Formatted(String dataInput1) {
        this.dataInput1 = Trunc.toUnsignedNumeric(dataInput1, Len.DATA_INPUT1);
    }

    public int getDataInput1() {
        return NumericDisplay.asInt(this.dataInput1);
    }

    public void setAnniInput3Formatted(String anniInput3) {
        this.anniInput3 = Trunc.toUnsignedNumeric(anniInput3, Len.ANNI_INPUT3);
    }

    public int getAnniInput3() {
        return NumericDisplay.asInt(this.anniInput3);
    }

    public void setMesiInput3Formatted(String mesiInput3) {
        this.mesiInput3 = Trunc.toUnsignedNumeric(mesiInput3, Len.MESI_INPUT3);
    }

    public int getMesiInput3() {
        return NumericDisplay.asInt(this.mesiInput3);
    }

    public void setDataOutput(AfDecimal dataOutput) {
        this.dataOutput.assign(dataOutput);
    }

    public AfDecimal getDataOutput() {
        return this.dataOutput.copy();
    }

    public Lvvc0000DatiInput2 getDatiInput2() {
        return datiInput2;
    }

    public Lvvc0000FormatDate getFormatDate() {
        return formatDate;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DATA_INPUT = 8;
        public static final int DATA_INPUT1 = 8;
        public static final int ANNI_INPUT3 = 5;
        public static final int MESI_INPUT3 = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
