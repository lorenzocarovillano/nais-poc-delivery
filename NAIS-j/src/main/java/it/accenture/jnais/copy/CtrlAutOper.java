package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.CaoProgCond;
import it.accenture.jnais.ws.redefines.CaoTpMovi;

/**Original name: CTRL-AUT-OPER<br>
 * Variable: CTRL-AUT-OPER from copybook IDBVCAO1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CtrlAutOper {

    //==== PROPERTIES ====
    //Original name: CAO-ID-CTRL-AUT-OPER
    private int caoIdCtrlAutOper = DefaultValues.INT_VAL;
    //Original name: CAO-COD-COMPAGNIA-ANIA
    private int caoCodCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: CAO-TP-MOVI
    private CaoTpMovi caoTpMovi = new CaoTpMovi();
    //Original name: CAO-COD-ERRORE
    private int caoCodErrore = DefaultValues.BIN_INT_VAL;
    //Original name: CAO-KEY-AUT-OPER1
    private String caoKeyAutOper1 = DefaultValues.stringVal(Len.CAO_KEY_AUT_OPER1);
    //Original name: CAO-KEY-AUT-OPER2
    private String caoKeyAutOper2 = DefaultValues.stringVal(Len.CAO_KEY_AUT_OPER2);
    //Original name: CAO-KEY-AUT-OPER3
    private String caoKeyAutOper3 = DefaultValues.stringVal(Len.CAO_KEY_AUT_OPER3);
    //Original name: CAO-KEY-AUT-OPER4
    private String caoKeyAutOper4 = DefaultValues.stringVal(Len.CAO_KEY_AUT_OPER4);
    //Original name: CAO-KEY-AUT-OPER5
    private String caoKeyAutOper5 = DefaultValues.stringVal(Len.CAO_KEY_AUT_OPER5);
    //Original name: CAO-COD-LIV-AUT
    private int caoCodLivAut = DefaultValues.INT_VAL;
    //Original name: CAO-TP-MOT-DEROGA
    private String caoTpMotDeroga = DefaultValues.stringVal(Len.CAO_TP_MOT_DEROGA);
    //Original name: CAO-MODULO-VERIFICA
    private String caoModuloVerifica = DefaultValues.stringVal(Len.CAO_MODULO_VERIFICA);
    //Original name: CAO-COD-COND
    private String caoCodCond = DefaultValues.stringVal(Len.CAO_COD_COND);
    //Original name: CAO-PROG-COND
    private CaoProgCond caoProgCond = new CaoProgCond();
    //Original name: CAO-RISULT-COND
    private char caoRisultCond = DefaultValues.CHAR_VAL;
    //Original name: CAO-PARAM-AUT-OPER-LEN
    private short caoParamAutOperLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: CAO-PARAM-AUT-OPER
    private String caoParamAutOper = DefaultValues.stringVal(Len.CAO_PARAM_AUT_OPER);
    //Original name: CAO-STATO-ATTIVAZIONE
    private char caoStatoAttivazione = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setCaoIdCtrlAutOper(int caoIdCtrlAutOper) {
        this.caoIdCtrlAutOper = caoIdCtrlAutOper;
    }

    public int getCaoIdCtrlAutOper() {
        return this.caoIdCtrlAutOper;
    }

    public void setCaoCodCompagniaAnia(int caoCodCompagniaAnia) {
        this.caoCodCompagniaAnia = caoCodCompagniaAnia;
    }

    public int getCaoCodCompagniaAnia() {
        return this.caoCodCompagniaAnia;
    }

    public void setCaoCodErrore(int caoCodErrore) {
        this.caoCodErrore = caoCodErrore;
    }

    public int getCaoCodErrore() {
        return this.caoCodErrore;
    }

    public void setCaoKeyAutOper1(String caoKeyAutOper1) {
        this.caoKeyAutOper1 = Functions.subString(caoKeyAutOper1, Len.CAO_KEY_AUT_OPER1);
    }

    public String getCaoKeyAutOper1() {
        return this.caoKeyAutOper1;
    }

    public void setCaoKeyAutOper2(String caoKeyAutOper2) {
        this.caoKeyAutOper2 = Functions.subString(caoKeyAutOper2, Len.CAO_KEY_AUT_OPER2);
    }

    public String getCaoKeyAutOper2() {
        return this.caoKeyAutOper2;
    }

    public void setCaoKeyAutOper3(String caoKeyAutOper3) {
        this.caoKeyAutOper3 = Functions.subString(caoKeyAutOper3, Len.CAO_KEY_AUT_OPER3);
    }

    public String getCaoKeyAutOper3() {
        return this.caoKeyAutOper3;
    }

    public void setCaoKeyAutOper4(String caoKeyAutOper4) {
        this.caoKeyAutOper4 = Functions.subString(caoKeyAutOper4, Len.CAO_KEY_AUT_OPER4);
    }

    public String getCaoKeyAutOper4() {
        return this.caoKeyAutOper4;
    }

    public void setCaoKeyAutOper5(String caoKeyAutOper5) {
        this.caoKeyAutOper5 = Functions.subString(caoKeyAutOper5, Len.CAO_KEY_AUT_OPER5);
    }

    public String getCaoKeyAutOper5() {
        return this.caoKeyAutOper5;
    }

    public void setCaoCodLivAut(int caoCodLivAut) {
        this.caoCodLivAut = caoCodLivAut;
    }

    public int getCaoCodLivAut() {
        return this.caoCodLivAut;
    }

    public void setCaoTpMotDeroga(String caoTpMotDeroga) {
        this.caoTpMotDeroga = Functions.subString(caoTpMotDeroga, Len.CAO_TP_MOT_DEROGA);
    }

    public String getCaoTpMotDeroga() {
        return this.caoTpMotDeroga;
    }

    public void setCaoModuloVerifica(String caoModuloVerifica) {
        this.caoModuloVerifica = Functions.subString(caoModuloVerifica, Len.CAO_MODULO_VERIFICA);
    }

    public String getCaoModuloVerifica() {
        return this.caoModuloVerifica;
    }

    public void setCaoCodCond(String caoCodCond) {
        this.caoCodCond = Functions.subString(caoCodCond, Len.CAO_COD_COND);
    }

    public String getCaoCodCond() {
        return this.caoCodCond;
    }

    public void setCaoRisultCond(char caoRisultCond) {
        this.caoRisultCond = caoRisultCond;
    }

    public char getCaoRisultCond() {
        return this.caoRisultCond;
    }

    public void setCaoParamAutOperVcharFormatted(String data) {
        byte[] buffer = new byte[Len.CAO_PARAM_AUT_OPER_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.CAO_PARAM_AUT_OPER_VCHAR);
        setCaoParamAutOperVcharBytes(buffer, 1);
    }

    public String getCaoParamAutOperVcharFormatted() {
        return MarshalByteExt.bufferToStr(getCaoParamAutOperVcharBytes());
    }

    /**Original name: CAO-PARAM-AUT-OPER-VCHAR<br>*/
    public byte[] getCaoParamAutOperVcharBytes() {
        byte[] buffer = new byte[Len.CAO_PARAM_AUT_OPER_VCHAR];
        return getCaoParamAutOperVcharBytes(buffer, 1);
    }

    public void setCaoParamAutOperVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        caoParamAutOperLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        caoParamAutOper = MarshalByte.readString(buffer, position, Len.CAO_PARAM_AUT_OPER);
    }

    public byte[] getCaoParamAutOperVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, caoParamAutOperLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, caoParamAutOper, Len.CAO_PARAM_AUT_OPER);
        return buffer;
    }

    public void setCaoParamAutOperLen(short caoParamAutOperLen) {
        this.caoParamAutOperLen = caoParamAutOperLen;
    }

    public short getCaoParamAutOperLen() {
        return this.caoParamAutOperLen;
    }

    public void setCaoParamAutOper(String caoParamAutOper) {
        this.caoParamAutOper = Functions.subString(caoParamAutOper, Len.CAO_PARAM_AUT_OPER);
    }

    public String getCaoParamAutOper() {
        return this.caoParamAutOper;
    }

    public void setCaoStatoAttivazione(char caoStatoAttivazione) {
        this.caoStatoAttivazione = caoStatoAttivazione;
    }

    public char getCaoStatoAttivazione() {
        return this.caoStatoAttivazione;
    }

    public CaoProgCond getCaoProgCond() {
        return caoProgCond;
    }

    public CaoTpMovi getCaoTpMovi() {
        return caoTpMovi;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CAO_KEY_AUT_OPER1 = 20;
        public static final int CAO_KEY_AUT_OPER2 = 20;
        public static final int CAO_KEY_AUT_OPER3 = 20;
        public static final int CAO_KEY_AUT_OPER4 = 20;
        public static final int CAO_KEY_AUT_OPER5 = 20;
        public static final int CAO_TP_MOT_DEROGA = 2;
        public static final int CAO_MODULO_VERIFICA = 8;
        public static final int CAO_COD_COND = 50;
        public static final int CAO_PARAM_AUT_OPER_LEN = 2;
        public static final int CAO_PARAM_AUT_OPER = 2000;
        public static final int CAO_PARAM_AUT_OPER_VCHAR = CAO_PARAM_AUT_OPER_LEN + CAO_PARAM_AUT_OPER;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
