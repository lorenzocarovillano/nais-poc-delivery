package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDBVL272<br>
 * Copybook: IDBVL272 from copybook IDBVL272<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvl272 {

    //==== PROPERTIES ====
    //Original name: IND-L27-ID-OGG
    private short l27IdOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L27-TP-OGG
    private short l27TpOgg = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setL27IdOgg(short l27IdOgg) {
        this.l27IdOgg = l27IdOgg;
    }

    public short getL27IdOgg() {
        return this.l27IdOgg;
    }

    public void setL27TpOgg(short l27TpOgg) {
        this.l27TpOgg = l27TpOgg;
    }

    public short getL27TpOgg() {
        return this.l27TpOgg;
    }
}
