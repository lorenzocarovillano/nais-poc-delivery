package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-MATR-VAL-VAR<br>
 * Variable: IND-MATR-VAL-VAR from copybook IDBVMVV2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndMatrValVar {

    //==== PROPERTIES ====
    //Original name: IND-MVV-IDP-MATR-VAL-VAR
    private short idpMatrValVar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-TIPO-MOVIMENTO
    private short tipoMovimento = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-COD-DATO-EXT
    private short codDatoExt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-OBBLIGATORIETA
    private short obbligatorieta = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-VALORE-DEFAULT
    private short valoreDefault = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-COD-STR-DATO-PTF
    private short codStrDatoPtf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-COD-DATO-PTF
    private short codDatoPtf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-COD-PARAMETRO
    private short codParametro = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-OPERAZIONE
    private short operazione = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-LIVELLO-OPERAZIONE
    private short livelloOperazione = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-TIPO-OGGETTO
    private short tipoOggetto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-WHERE-CONDITION
    private short whereCondition = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-SERVIZIO-LETTURA
    private short servizioLettura = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-MODULO-CALCOLO
    private short moduloCalcolo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-STEP-VALORIZZATORE
    private short stepValorizzatore = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-STEP-CONVERSAZIONE
    private short stepConversazione = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-OPER-LOG-STATO-BUS
    private short operLogStatoBus = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-ARRAY-STATO-BUS
    private short arrayStatoBus = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-OPER-LOG-CAUSALE
    private short operLogCausale = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-ARRAY-CAUSALE
    private short arrayCausale = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-MVV-COD-DATO-INTERNO
    private short codDatoInterno = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdpMatrValVar(short idpMatrValVar) {
        this.idpMatrValVar = idpMatrValVar;
    }

    public short getIdpMatrValVar() {
        return this.idpMatrValVar;
    }

    public void setTipoMovimento(short tipoMovimento) {
        this.tipoMovimento = tipoMovimento;
    }

    public short getTipoMovimento() {
        return this.tipoMovimento;
    }

    public void setCodDatoExt(short codDatoExt) {
        this.codDatoExt = codDatoExt;
    }

    public short getCodDatoExt() {
        return this.codDatoExt;
    }

    public void setObbligatorieta(short obbligatorieta) {
        this.obbligatorieta = obbligatorieta;
    }

    public short getObbligatorieta() {
        return this.obbligatorieta;
    }

    public void setValoreDefault(short valoreDefault) {
        this.valoreDefault = valoreDefault;
    }

    public short getValoreDefault() {
        return this.valoreDefault;
    }

    public void setCodStrDatoPtf(short codStrDatoPtf) {
        this.codStrDatoPtf = codStrDatoPtf;
    }

    public short getCodStrDatoPtf() {
        return this.codStrDatoPtf;
    }

    public void setCodDatoPtf(short codDatoPtf) {
        this.codDatoPtf = codDatoPtf;
    }

    public short getCodDatoPtf() {
        return this.codDatoPtf;
    }

    public void setCodParametro(short codParametro) {
        this.codParametro = codParametro;
    }

    public short getCodParametro() {
        return this.codParametro;
    }

    public void setOperazione(short operazione) {
        this.operazione = operazione;
    }

    public short getOperazione() {
        return this.operazione;
    }

    public void setLivelloOperazione(short livelloOperazione) {
        this.livelloOperazione = livelloOperazione;
    }

    public short getLivelloOperazione() {
        return this.livelloOperazione;
    }

    public void setTipoOggetto(short tipoOggetto) {
        this.tipoOggetto = tipoOggetto;
    }

    public short getTipoOggetto() {
        return this.tipoOggetto;
    }

    public void setWhereCondition(short whereCondition) {
        this.whereCondition = whereCondition;
    }

    public short getWhereCondition() {
        return this.whereCondition;
    }

    public void setServizioLettura(short servizioLettura) {
        this.servizioLettura = servizioLettura;
    }

    public short getServizioLettura() {
        return this.servizioLettura;
    }

    public void setModuloCalcolo(short moduloCalcolo) {
        this.moduloCalcolo = moduloCalcolo;
    }

    public short getModuloCalcolo() {
        return this.moduloCalcolo;
    }

    public void setStepValorizzatore(short stepValorizzatore) {
        this.stepValorizzatore = stepValorizzatore;
    }

    public short getStepValorizzatore() {
        return this.stepValorizzatore;
    }

    public void setStepConversazione(short stepConversazione) {
        this.stepConversazione = stepConversazione;
    }

    public short getStepConversazione() {
        return this.stepConversazione;
    }

    public void setOperLogStatoBus(short operLogStatoBus) {
        this.operLogStatoBus = operLogStatoBus;
    }

    public short getOperLogStatoBus() {
        return this.operLogStatoBus;
    }

    public void setArrayStatoBus(short arrayStatoBus) {
        this.arrayStatoBus = arrayStatoBus;
    }

    public short getArrayStatoBus() {
        return this.arrayStatoBus;
    }

    public void setOperLogCausale(short operLogCausale) {
        this.operLogCausale = operLogCausale;
    }

    public short getOperLogCausale() {
        return this.operLogCausale;
    }

    public void setArrayCausale(short arrayCausale) {
        this.arrayCausale = arrayCausale;
    }

    public short getArrayCausale() {
        return this.arrayCausale;
    }

    public void setCodDatoInterno(short codDatoInterno) {
        this.codDatoInterno = codDatoInterno;
    }

    public short getCodDatoInterno() {
        return this.codDatoInterno;
    }
}
