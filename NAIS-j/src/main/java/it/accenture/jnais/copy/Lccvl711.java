package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVL711<br>
 * Variable: LCCVL711 from copybook LCCVL711<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvl711 {

    //==== PROPERTIES ====
    /**Original name: WL71-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA MATR_ELAB_BATCH
	 *    ALIAS L71
	 *    ULTIMO AGG. 22 APR 2010
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WL71-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WL71-DATI
    private Wl71Dati dati = new Wl71Dati();

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public Wl71Dati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }
}
