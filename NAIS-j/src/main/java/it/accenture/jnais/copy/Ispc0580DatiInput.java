package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ISPC0580-DATI-INPUT<br>
 * Variable: ISPC0580-DATI-INPUT from copybook ISPC0580<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ispc0580DatiInput {

    //==== PROPERTIES ====
    //Original name: ISPC0580-COD-COMPAGNIA-ANIA
    private String codCompagniaAnia = DefaultValues.stringVal(Len.COD_COMPAGNIA_ANIA);
    //Original name: ISPC0580-DATA-INIZ-ELAB
    private String dataInizElab = DefaultValues.stringVal(Len.DATA_INIZ_ELAB);
    //Original name: ISPC0580-DATA-FINE-ELAB
    private String dataFineElab = DefaultValues.stringVal(Len.DATA_FINE_ELAB);
    //Original name: ISPC0580-DATA-RIFERIMENTO
    private String dataRiferimento = DefaultValues.stringVal(Len.DATA_RIFERIMENTO);
    //Original name: ISPC0580-LIVELLO-UTENTE
    private String livelloUtente = DefaultValues.stringVal(Len.LIVELLO_UTENTE);
    //Original name: ISPC0580-FUNZIONALITA
    private String funzionalita = DefaultValues.stringVal(Len.FUNZIONALITA);
    //Original name: ISPC0580-SESSION-ID
    private String sessionId = DefaultValues.stringVal(Len.SESSION_ID);

    //==== METHODS ====
    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        codCompagniaAnia = MarshalByte.readFixedString(buffer, position, Len.COD_COMPAGNIA_ANIA);
        position += Len.COD_COMPAGNIA_ANIA;
        dataInizElab = MarshalByte.readString(buffer, position, Len.DATA_INIZ_ELAB);
        position += Len.DATA_INIZ_ELAB;
        dataFineElab = MarshalByte.readString(buffer, position, Len.DATA_FINE_ELAB);
        position += Len.DATA_FINE_ELAB;
        dataRiferimento = MarshalByte.readString(buffer, position, Len.DATA_RIFERIMENTO);
        position += Len.DATA_RIFERIMENTO;
        livelloUtente = MarshalByte.readFixedString(buffer, position, Len.LIVELLO_UTENTE);
        position += Len.LIVELLO_UTENTE;
        funzionalita = MarshalByte.readFixedString(buffer, position, Len.FUNZIONALITA);
        position += Len.FUNZIONALITA;
        sessionId = MarshalByte.readString(buffer, position, Len.SESSION_ID);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
        position += Len.COD_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, dataInizElab, Len.DATA_INIZ_ELAB);
        position += Len.DATA_INIZ_ELAB;
        MarshalByte.writeString(buffer, position, dataFineElab, Len.DATA_FINE_ELAB);
        position += Len.DATA_FINE_ELAB;
        MarshalByte.writeString(buffer, position, dataRiferimento, Len.DATA_RIFERIMENTO);
        position += Len.DATA_RIFERIMENTO;
        MarshalByte.writeString(buffer, position, livelloUtente, Len.LIVELLO_UTENTE);
        position += Len.LIVELLO_UTENTE;
        MarshalByte.writeString(buffer, position, funzionalita, Len.FUNZIONALITA);
        position += Len.FUNZIONALITA;
        MarshalByte.writeString(buffer, position, sessionId, Len.SESSION_ID);
        return buffer;
    }

    public void setCodCompagniaAniaFormatted(String codCompagniaAnia) {
        this.codCompagniaAnia = Trunc.toUnsignedNumeric(codCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
    }

    public int getCodCompagniaAnia() {
        return NumericDisplay.asInt(this.codCompagniaAnia);
    }

    public void setDataInizElab(String dataInizElab) {
        this.dataInizElab = Functions.subString(dataInizElab, Len.DATA_INIZ_ELAB);
    }

    public String getDataInizElab() {
        return this.dataInizElab;
    }

    public void setDataFineElab(String dataFineElab) {
        this.dataFineElab = Functions.subString(dataFineElab, Len.DATA_FINE_ELAB);
    }

    public String getDataFineElab() {
        return this.dataFineElab;
    }

    public void setDataRiferimento(String dataRiferimento) {
        this.dataRiferimento = Functions.subString(dataRiferimento, Len.DATA_RIFERIMENTO);
    }

    public String getDataRiferimento() {
        return this.dataRiferimento;
    }

    public void setIspc0580LivelloUtente(short ispc0580LivelloUtente) {
        this.livelloUtente = NumericDisplay.asString(ispc0580LivelloUtente, Len.LIVELLO_UTENTE);
    }

    public void setIspc0580LivelloUtenteFormatted(String ispc0580LivelloUtente) {
        this.livelloUtente = Trunc.toUnsignedNumeric(ispc0580LivelloUtente, Len.LIVELLO_UTENTE);
    }

    public short getIspc0580LivelloUtente() {
        return NumericDisplay.asShort(this.livelloUtente);
    }

    public void setIspc0580FunzionalitaFormatted(String ispc0580Funzionalita) {
        this.funzionalita = Trunc.toUnsignedNumeric(ispc0580Funzionalita, Len.FUNZIONALITA);
    }

    public int getIspc0580Funzionalita() {
        return NumericDisplay.asInt(this.funzionalita);
    }

    public void setSessionId(String sessionId) {
        this.sessionId = Functions.subString(sessionId, Len.SESSION_ID);
    }

    public String getSessionId() {
        return this.sessionId;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_COMPAGNIA_ANIA = 5;
        public static final int DATA_INIZ_ELAB = 8;
        public static final int DATA_FINE_ELAB = 8;
        public static final int DATA_RIFERIMENTO = 8;
        public static final int LIVELLO_UTENTE = 2;
        public static final int FUNZIONALITA = 5;
        public static final int SESSION_ID = 20;
        public static final int DATI_INPUT = COD_COMPAGNIA_ANIA + DATA_INIZ_ELAB + DATA_FINE_ELAB + DATA_RIFERIMENTO + LIVELLO_UTENTE + FUNZIONALITA + SESSION_ID;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
