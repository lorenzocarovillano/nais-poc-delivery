package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DETT-QUEST-DB<br>
 * Variable: DETT-QUEST-DB from copybook IDBVDEQ3<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DettQuestDb {

    //==== PROPERTIES ====
    //Original name: DEQ-DT-INI-EFF-DB
    private String dtIniEffDb = DefaultValues.stringVal(Len.DT_INI_EFF_DB);
    //Original name: DEQ-DT-END-EFF-DB
    private String dtEndEffDb = DefaultValues.stringVal(Len.DT_END_EFF_DB);
    //Original name: DEQ-RISP-DT-DB
    private String rispDtDb = DefaultValues.stringVal(Len.RISP_DT_DB);

    //==== METHODS ====
    public void setDtIniEffDb(String dtIniEffDb) {
        this.dtIniEffDb = Functions.subString(dtIniEffDb, Len.DT_INI_EFF_DB);
    }

    public String getDtIniEffDb() {
        return this.dtIniEffDb;
    }

    public void setDtEndEffDb(String dtEndEffDb) {
        this.dtEndEffDb = Functions.subString(dtEndEffDb, Len.DT_END_EFF_DB);
    }

    public String getDtEndEffDb() {
        return this.dtEndEffDb;
    }

    public void setRispDtDb(String rispDtDb) {
        this.rispDtDb = Functions.subString(rispDtDb, Len.RISP_DT_DB);
    }

    public String getRispDtDb() {
        return this.rispDtDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_INI_EFF_DB = 10;
        public static final int DT_END_EFF_DB = 10;
        public static final int RISP_DT_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
