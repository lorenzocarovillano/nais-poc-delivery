package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.Ispc0001TabErrori;

/**Original name: ISPC0001-AREA-ERRORI<br>
 * Variable: ISPC0001-AREA-ERRORI from copybook IEAV9903<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ispc0001AreaErrori {

    //==== PROPERTIES ====
    public static final int TAB_ERRORI_MAXOCCURS = 10;
    //Original name: ISPC0001-ESITO
    private String esito = DefaultValues.stringVal(Len.ESITO);
    //Original name: ISPC0001-ERR-NUM-ELE
    private String errNumEle = DefaultValues.stringVal(Len.ERR_NUM_ELE);
    //Original name: ISPC0001-TAB-ERRORI
    private Ispc0001TabErrori[] tabErrori = new Ispc0001TabErrori[TAB_ERRORI_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ispc0001AreaErrori() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabErroriIdx = 1; tabErroriIdx <= TAB_ERRORI_MAXOCCURS; tabErroriIdx++) {
            tabErrori[tabErroriIdx - 1] = new Ispc0001TabErrori();
        }
    }

    public void setIspc0001AreaErroriBytes(byte[] buffer) {
        setIspc0001AreaErroriBytes(buffer, 1);
    }

    public void setIspc0001AreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        esito = MarshalByte.readFixedString(buffer, position, Len.ESITO);
        position += Len.ESITO;
        errNumEle = MarshalByte.readFixedString(buffer, position, Len.ERR_NUM_ELE);
        position += Len.ERR_NUM_ELE;
        for (int idx = 1; idx <= TAB_ERRORI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabErrori[idx - 1].setIspc0001TabErroriBytes(buffer, position);
                position += Ispc0001TabErrori.Len.ISPC0001_TAB_ERRORI;
            }
            else {
                tabErrori[idx - 1].initIspc0001TabErroriSpaces();
                position += Ispc0001TabErrori.Len.ISPC0001_TAB_ERRORI;
            }
        }
    }

    public short getIspc0001Esito() {
        return NumericDisplay.asShort(this.esito);
    }

    public short getIspc0001ErrNumEle() {
        return NumericDisplay.asShort(this.errNumEle);
    }

    public Ispc0001TabErrori getTabErrori(int idx) {
        return tabErrori[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ESITO = 2;
        public static final int ERR_NUM_ELE = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
