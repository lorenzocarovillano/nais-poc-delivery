package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: MATR-ELAB-BATCH<br>
 * Variable: MATR-ELAB-BATCH from copybook IDBVL711<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class MatrElabBatch {

    //==== PROPERTIES ====
    //Original name: L71-ID-MATR-ELAB-BATCH
    private int idMatrElabBatch = DefaultValues.INT_VAL;
    //Original name: L71-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: L71-TP-FRM-ASSVA
    private String tpFrmAssva = DefaultValues.stringVal(Len.TP_FRM_ASSVA);
    //Original name: L71-TP-MOVI
    private int tpMovi = DefaultValues.INT_VAL;
    //Original name: L71-COD-RAMO
    private String codRamo = DefaultValues.stringVal(Len.COD_RAMO);
    //Original name: L71-DT-ULT-ELAB
    private int dtUltElab = DefaultValues.INT_VAL;
    //Original name: L71-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: L71-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: L71-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: L71-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: L71-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setMatrElabBatchFormatted(String data) {
        byte[] buffer = new byte[Len.MATR_ELAB_BATCH];
        MarshalByte.writeString(buffer, 1, data, Len.MATR_ELAB_BATCH);
        setMatrElabBatchBytes(buffer, 1);
    }

    public String getMatrElabBatchFormatted() {
        return MarshalByteExt.bufferToStr(getMatrElabBatchBytes());
    }

    public byte[] getMatrElabBatchBytes() {
        byte[] buffer = new byte[Len.MATR_ELAB_BATCH];
        return getMatrElabBatchBytes(buffer, 1);
    }

    public void setMatrElabBatchBytes(byte[] buffer, int offset) {
        int position = offset;
        idMatrElabBatch = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MATR_ELAB_BATCH, 0);
        position += Len.ID_MATR_ELAB_BATCH;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        tpFrmAssva = MarshalByte.readString(buffer, position, Len.TP_FRM_ASSVA);
        position += Len.TP_FRM_ASSVA;
        tpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI, 0);
        position += Len.TP_MOVI;
        codRamo = MarshalByte.readString(buffer, position, Len.COD_RAMO);
        position += Len.COD_RAMO;
        dtUltElab = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_ULT_ELAB, 0);
        position += Len.DT_ULT_ELAB;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getMatrElabBatchBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idMatrElabBatch, Len.Int.ID_MATR_ELAB_BATCH, 0);
        position += Len.ID_MATR_ELAB_BATCH;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, tpFrmAssva, Len.TP_FRM_ASSVA);
        position += Len.TP_FRM_ASSVA;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi, Len.Int.TP_MOVI, 0);
        position += Len.TP_MOVI;
        MarshalByte.writeString(buffer, position, codRamo, Len.COD_RAMO);
        position += Len.COD_RAMO;
        MarshalByte.writeIntAsPacked(buffer, position, dtUltElab, Len.Int.DT_ULT_ELAB, 0);
        position += Len.DT_ULT_ELAB;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsCptz, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        return buffer;
    }

    public void setIdMatrElabBatch(int idMatrElabBatch) {
        this.idMatrElabBatch = idMatrElabBatch;
    }

    public int getIdMatrElabBatch() {
        return this.idMatrElabBatch;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setTpFrmAssva(String tpFrmAssva) {
        this.tpFrmAssva = Functions.subString(tpFrmAssva, Len.TP_FRM_ASSVA);
    }

    public String getTpFrmAssva() {
        return this.tpFrmAssva;
    }

    public void setTpMovi(int tpMovi) {
        this.tpMovi = tpMovi;
    }

    public int getTpMovi() {
        return this.tpMovi;
    }

    public void setCodRamo(String codRamo) {
        this.codRamo = Functions.subString(codRamo, Len.COD_RAMO);
    }

    public String getCodRamo() {
        return this.codRamo;
    }

    public String getCodRamoFormatted() {
        return Functions.padBlanks(getCodRamo(), Len.COD_RAMO);
    }

    public void setDtUltElab(int dtUltElab) {
        this.dtUltElab = dtUltElab;
    }

    public int getDtUltElab() {
        return this.dtUltElab;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_FRM_ASSVA = 2;
        public static final int COD_RAMO = 12;
        public static final int DS_UTENTE = 20;
        public static final int ID_MATR_ELAB_BATCH = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int TP_MOVI = 3;
        public static final int DT_ULT_ELAB = 5;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_CPTZ = 10;
        public static final int DS_STATO_ELAB = 1;
        public static final int MATR_ELAB_BATCH = ID_MATR_ELAB_BATCH + COD_COMP_ANIA + TP_FRM_ASSVA + TP_MOVI + COD_RAMO + DT_ULT_ELAB + DS_OPER_SQL + DS_VER + DS_TS_CPTZ + DS_UTENTE + DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_MATR_ELAB_BATCH = 9;
            public static final int COD_COMP_ANIA = 5;
            public static final int TP_MOVI = 5;
            public static final int DT_ULT_ELAB = 8;
            public static final int DS_VER = 9;
            public static final int DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
