package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpreDtConcsPrest;
import it.accenture.jnais.ws.redefines.WpreDtDecorPrest;
import it.accenture.jnais.ws.redefines.WpreDtRimb;
import it.accenture.jnais.ws.redefines.WpreFrazPagIntr;
import it.accenture.jnais.ws.redefines.WpreIdMoviChiu;
import it.accenture.jnais.ws.redefines.WpreImpPrest;
import it.accenture.jnais.ws.redefines.WpreImpPrestLiqto;
import it.accenture.jnais.ws.redefines.WpreImpRimb;
import it.accenture.jnais.ws.redefines.WpreIntrPrest;
import it.accenture.jnais.ws.redefines.WprePrestResEff;
import it.accenture.jnais.ws.redefines.WpreRimbEff;
import it.accenture.jnais.ws.redefines.WpreSdoIntr;
import it.accenture.jnais.ws.redefines.WpreSpePrest;

/**Original name: WPRE-DATI<br>
 * Variable: WPRE-DATI from copybook LCCVPRE1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WpreDati {

    //==== PROPERTIES ====
    //Original name: WPRE-ID-PREST
    private int wpreIdPrest = DefaultValues.INT_VAL;
    //Original name: WPRE-ID-OGG
    private int wpreIdOgg = DefaultValues.INT_VAL;
    //Original name: WPRE-TP-OGG
    private String wpreTpOgg = DefaultValues.stringVal(Len.WPRE_TP_OGG);
    //Original name: WPRE-ID-MOVI-CRZ
    private int wpreIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WPRE-ID-MOVI-CHIU
    private WpreIdMoviChiu wpreIdMoviChiu = new WpreIdMoviChiu();
    //Original name: WPRE-DT-INI-EFF
    private int wpreDtIniEff = DefaultValues.INT_VAL;
    //Original name: WPRE-DT-END-EFF
    private int wpreDtEndEff = DefaultValues.INT_VAL;
    //Original name: WPRE-COD-COMP-ANIA
    private int wpreCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WPRE-DT-CONCS-PREST
    private WpreDtConcsPrest wpreDtConcsPrest = new WpreDtConcsPrest();
    //Original name: WPRE-DT-DECOR-PREST
    private WpreDtDecorPrest wpreDtDecorPrest = new WpreDtDecorPrest();
    //Original name: WPRE-IMP-PREST
    private WpreImpPrest wpreImpPrest = new WpreImpPrest();
    //Original name: WPRE-INTR-PREST
    private WpreIntrPrest wpreIntrPrest = new WpreIntrPrest();
    //Original name: WPRE-TP-PREST
    private String wpreTpPrest = DefaultValues.stringVal(Len.WPRE_TP_PREST);
    //Original name: WPRE-FRAZ-PAG-INTR
    private WpreFrazPagIntr wpreFrazPagIntr = new WpreFrazPagIntr();
    //Original name: WPRE-DT-RIMB
    private WpreDtRimb wpreDtRimb = new WpreDtRimb();
    //Original name: WPRE-IMP-RIMB
    private WpreImpRimb wpreImpRimb = new WpreImpRimb();
    //Original name: WPRE-COD-DVS
    private String wpreCodDvs = DefaultValues.stringVal(Len.WPRE_COD_DVS);
    //Original name: WPRE-DT-RICH-PREST
    private int wpreDtRichPrest = DefaultValues.INT_VAL;
    //Original name: WPRE-MOD-INTR-PREST
    private String wpreModIntrPrest = DefaultValues.stringVal(Len.WPRE_MOD_INTR_PREST);
    //Original name: WPRE-SPE-PREST
    private WpreSpePrest wpreSpePrest = new WpreSpePrest();
    //Original name: WPRE-IMP-PREST-LIQTO
    private WpreImpPrestLiqto wpreImpPrestLiqto = new WpreImpPrestLiqto();
    //Original name: WPRE-SDO-INTR
    private WpreSdoIntr wpreSdoIntr = new WpreSdoIntr();
    //Original name: WPRE-RIMB-EFF
    private WpreRimbEff wpreRimbEff = new WpreRimbEff();
    //Original name: WPRE-PREST-RES-EFF
    private WprePrestResEff wprePrestResEff = new WprePrestResEff();
    //Original name: WPRE-DS-RIGA
    private long wpreDsRiga = DefaultValues.LONG_VAL;
    //Original name: WPRE-DS-OPER-SQL
    private char wpreDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WPRE-DS-VER
    private int wpreDsVer = DefaultValues.INT_VAL;
    //Original name: WPRE-DS-TS-INI-CPTZ
    private long wpreDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WPRE-DS-TS-END-CPTZ
    private long wpreDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WPRE-DS-UTENTE
    private String wpreDsUtente = DefaultValues.stringVal(Len.WPRE_DS_UTENTE);
    //Original name: WPRE-DS-STATO-ELAB
    private char wpreDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wpreIdPrest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPRE_ID_PREST, 0);
        position += Len.WPRE_ID_PREST;
        wpreIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPRE_ID_OGG, 0);
        position += Len.WPRE_ID_OGG;
        wpreTpOgg = MarshalByte.readString(buffer, position, Len.WPRE_TP_OGG);
        position += Len.WPRE_TP_OGG;
        wpreIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPRE_ID_MOVI_CRZ, 0);
        position += Len.WPRE_ID_MOVI_CRZ;
        wpreIdMoviChiu.setWpreIdMoviChiuFromBuffer(buffer, position);
        position += WpreIdMoviChiu.Len.WPRE_ID_MOVI_CHIU;
        wpreDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPRE_DT_INI_EFF, 0);
        position += Len.WPRE_DT_INI_EFF;
        wpreDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPRE_DT_END_EFF, 0);
        position += Len.WPRE_DT_END_EFF;
        wpreCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPRE_COD_COMP_ANIA, 0);
        position += Len.WPRE_COD_COMP_ANIA;
        wpreDtConcsPrest.setWpreDtConcsPrestFromBuffer(buffer, position);
        position += WpreDtConcsPrest.Len.WPRE_DT_CONCS_PREST;
        wpreDtDecorPrest.setWpreDtDecorPrestFromBuffer(buffer, position);
        position += WpreDtDecorPrest.Len.WPRE_DT_DECOR_PREST;
        wpreImpPrest.setWpreImpPrestFromBuffer(buffer, position);
        position += WpreImpPrest.Len.WPRE_IMP_PREST;
        wpreIntrPrest.setWpreIntrPrestFromBuffer(buffer, position);
        position += WpreIntrPrest.Len.WPRE_INTR_PREST;
        wpreTpPrest = MarshalByte.readString(buffer, position, Len.WPRE_TP_PREST);
        position += Len.WPRE_TP_PREST;
        wpreFrazPagIntr.setWpreFrazPagIntrFromBuffer(buffer, position);
        position += WpreFrazPagIntr.Len.WPRE_FRAZ_PAG_INTR;
        wpreDtRimb.setWpreDtRimbFromBuffer(buffer, position);
        position += WpreDtRimb.Len.WPRE_DT_RIMB;
        wpreImpRimb.setWpreImpRimbFromBuffer(buffer, position);
        position += WpreImpRimb.Len.WPRE_IMP_RIMB;
        wpreCodDvs = MarshalByte.readString(buffer, position, Len.WPRE_COD_DVS);
        position += Len.WPRE_COD_DVS;
        wpreDtRichPrest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPRE_DT_RICH_PREST, 0);
        position += Len.WPRE_DT_RICH_PREST;
        wpreModIntrPrest = MarshalByte.readString(buffer, position, Len.WPRE_MOD_INTR_PREST);
        position += Len.WPRE_MOD_INTR_PREST;
        wpreSpePrest.setWpreSpePrestFromBuffer(buffer, position);
        position += WpreSpePrest.Len.WPRE_SPE_PREST;
        wpreImpPrestLiqto.setWpreImpPrestLiqtoFromBuffer(buffer, position);
        position += WpreImpPrestLiqto.Len.WPRE_IMP_PREST_LIQTO;
        wpreSdoIntr.setWpreSdoIntrFromBuffer(buffer, position);
        position += WpreSdoIntr.Len.WPRE_SDO_INTR;
        wpreRimbEff.setWpreRimbEffFromBuffer(buffer, position);
        position += WpreRimbEff.Len.WPRE_RIMB_EFF;
        wprePrestResEff.setWprePrestResEffFromBuffer(buffer, position);
        position += WprePrestResEff.Len.WPRE_PREST_RES_EFF;
        wpreDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPRE_DS_RIGA, 0);
        position += Len.WPRE_DS_RIGA;
        wpreDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wpreDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WPRE_DS_VER, 0);
        position += Len.WPRE_DS_VER;
        wpreDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPRE_DS_TS_INI_CPTZ, 0);
        position += Len.WPRE_DS_TS_INI_CPTZ;
        wpreDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WPRE_DS_TS_END_CPTZ, 0);
        position += Len.WPRE_DS_TS_END_CPTZ;
        wpreDsUtente = MarshalByte.readString(buffer, position, Len.WPRE_DS_UTENTE);
        position += Len.WPRE_DS_UTENTE;
        wpreDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wpreIdPrest, Len.Int.WPRE_ID_PREST, 0);
        position += Len.WPRE_ID_PREST;
        MarshalByte.writeIntAsPacked(buffer, position, wpreIdOgg, Len.Int.WPRE_ID_OGG, 0);
        position += Len.WPRE_ID_OGG;
        MarshalByte.writeString(buffer, position, wpreTpOgg, Len.WPRE_TP_OGG);
        position += Len.WPRE_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, wpreIdMoviCrz, Len.Int.WPRE_ID_MOVI_CRZ, 0);
        position += Len.WPRE_ID_MOVI_CRZ;
        wpreIdMoviChiu.getWpreIdMoviChiuAsBuffer(buffer, position);
        position += WpreIdMoviChiu.Len.WPRE_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wpreDtIniEff, Len.Int.WPRE_DT_INI_EFF, 0);
        position += Len.WPRE_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpreDtEndEff, Len.Int.WPRE_DT_END_EFF, 0);
        position += Len.WPRE_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wpreCodCompAnia, Len.Int.WPRE_COD_COMP_ANIA, 0);
        position += Len.WPRE_COD_COMP_ANIA;
        wpreDtConcsPrest.getWpreDtConcsPrestAsBuffer(buffer, position);
        position += WpreDtConcsPrest.Len.WPRE_DT_CONCS_PREST;
        wpreDtDecorPrest.getWpreDtDecorPrestAsBuffer(buffer, position);
        position += WpreDtDecorPrest.Len.WPRE_DT_DECOR_PREST;
        wpreImpPrest.getWpreImpPrestAsBuffer(buffer, position);
        position += WpreImpPrest.Len.WPRE_IMP_PREST;
        wpreIntrPrest.getWpreIntrPrestAsBuffer(buffer, position);
        position += WpreIntrPrest.Len.WPRE_INTR_PREST;
        MarshalByte.writeString(buffer, position, wpreTpPrest, Len.WPRE_TP_PREST);
        position += Len.WPRE_TP_PREST;
        wpreFrazPagIntr.getWpreFrazPagIntrAsBuffer(buffer, position);
        position += WpreFrazPagIntr.Len.WPRE_FRAZ_PAG_INTR;
        wpreDtRimb.getWpreDtRimbAsBuffer(buffer, position);
        position += WpreDtRimb.Len.WPRE_DT_RIMB;
        wpreImpRimb.getWpreImpRimbAsBuffer(buffer, position);
        position += WpreImpRimb.Len.WPRE_IMP_RIMB;
        MarshalByte.writeString(buffer, position, wpreCodDvs, Len.WPRE_COD_DVS);
        position += Len.WPRE_COD_DVS;
        MarshalByte.writeIntAsPacked(buffer, position, wpreDtRichPrest, Len.Int.WPRE_DT_RICH_PREST, 0);
        position += Len.WPRE_DT_RICH_PREST;
        MarshalByte.writeString(buffer, position, wpreModIntrPrest, Len.WPRE_MOD_INTR_PREST);
        position += Len.WPRE_MOD_INTR_PREST;
        wpreSpePrest.getWpreSpePrestAsBuffer(buffer, position);
        position += WpreSpePrest.Len.WPRE_SPE_PREST;
        wpreImpPrestLiqto.getWpreImpPrestLiqtoAsBuffer(buffer, position);
        position += WpreImpPrestLiqto.Len.WPRE_IMP_PREST_LIQTO;
        wpreSdoIntr.getWpreSdoIntrAsBuffer(buffer, position);
        position += WpreSdoIntr.Len.WPRE_SDO_INTR;
        wpreRimbEff.getWpreRimbEffAsBuffer(buffer, position);
        position += WpreRimbEff.Len.WPRE_RIMB_EFF;
        wprePrestResEff.getWprePrestResEffAsBuffer(buffer, position);
        position += WprePrestResEff.Len.WPRE_PREST_RES_EFF;
        MarshalByte.writeLongAsPacked(buffer, position, wpreDsRiga, Len.Int.WPRE_DS_RIGA, 0);
        position += Len.WPRE_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wpreDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wpreDsVer, Len.Int.WPRE_DS_VER, 0);
        position += Len.WPRE_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wpreDsTsIniCptz, Len.Int.WPRE_DS_TS_INI_CPTZ, 0);
        position += Len.WPRE_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wpreDsTsEndCptz, Len.Int.WPRE_DS_TS_END_CPTZ, 0);
        position += Len.WPRE_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wpreDsUtente, Len.WPRE_DS_UTENTE);
        position += Len.WPRE_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wpreDsStatoElab);
        return buffer;
    }

    public void initDatiSpaces() {
        wpreIdPrest = Types.INVALID_INT_VAL;
        wpreIdOgg = Types.INVALID_INT_VAL;
        wpreTpOgg = "";
        wpreIdMoviCrz = Types.INVALID_INT_VAL;
        wpreIdMoviChiu.initWpreIdMoviChiuSpaces();
        wpreDtIniEff = Types.INVALID_INT_VAL;
        wpreDtEndEff = Types.INVALID_INT_VAL;
        wpreCodCompAnia = Types.INVALID_INT_VAL;
        wpreDtConcsPrest.initWpreDtConcsPrestSpaces();
        wpreDtDecorPrest.initWpreDtDecorPrestSpaces();
        wpreImpPrest.initWpreImpPrestSpaces();
        wpreIntrPrest.initWpreIntrPrestSpaces();
        wpreTpPrest = "";
        wpreFrazPagIntr.initWpreFrazPagIntrSpaces();
        wpreDtRimb.initWpreDtRimbSpaces();
        wpreImpRimb.initWpreImpRimbSpaces();
        wpreCodDvs = "";
        wpreDtRichPrest = Types.INVALID_INT_VAL;
        wpreModIntrPrest = "";
        wpreSpePrest.initWpreSpePrestSpaces();
        wpreImpPrestLiqto.initWpreImpPrestLiqtoSpaces();
        wpreSdoIntr.initWpreSdoIntrSpaces();
        wpreRimbEff.initWpreRimbEffSpaces();
        wprePrestResEff.initWprePrestResEffSpaces();
        wpreDsRiga = Types.INVALID_LONG_VAL;
        wpreDsOperSql = Types.SPACE_CHAR;
        wpreDsVer = Types.INVALID_INT_VAL;
        wpreDsTsIniCptz = Types.INVALID_LONG_VAL;
        wpreDsTsEndCptz = Types.INVALID_LONG_VAL;
        wpreDsUtente = "";
        wpreDsStatoElab = Types.SPACE_CHAR;
    }

    public void setWpreIdPrest(int wpreIdPrest) {
        this.wpreIdPrest = wpreIdPrest;
    }

    public int getWpreIdPrest() {
        return this.wpreIdPrest;
    }

    public void setWpreIdOgg(int wpreIdOgg) {
        this.wpreIdOgg = wpreIdOgg;
    }

    public int getWpreIdOgg() {
        return this.wpreIdOgg;
    }

    public void setWpreTpOgg(String wpreTpOgg) {
        this.wpreTpOgg = Functions.subString(wpreTpOgg, Len.WPRE_TP_OGG);
    }

    public String getWpreTpOgg() {
        return this.wpreTpOgg;
    }

    public void setWpreIdMoviCrz(int wpreIdMoviCrz) {
        this.wpreIdMoviCrz = wpreIdMoviCrz;
    }

    public int getWpreIdMoviCrz() {
        return this.wpreIdMoviCrz;
    }

    public void setWpreDtIniEff(int wpreDtIniEff) {
        this.wpreDtIniEff = wpreDtIniEff;
    }

    public int getWpreDtIniEff() {
        return this.wpreDtIniEff;
    }

    public void setWpreDtEndEff(int wpreDtEndEff) {
        this.wpreDtEndEff = wpreDtEndEff;
    }

    public int getWpreDtEndEff() {
        return this.wpreDtEndEff;
    }

    public void setWpreCodCompAnia(int wpreCodCompAnia) {
        this.wpreCodCompAnia = wpreCodCompAnia;
    }

    public int getWpreCodCompAnia() {
        return this.wpreCodCompAnia;
    }

    public void setWpreTpPrest(String wpreTpPrest) {
        this.wpreTpPrest = Functions.subString(wpreTpPrest, Len.WPRE_TP_PREST);
    }

    public String getWpreTpPrest() {
        return this.wpreTpPrest;
    }

    public void setWpreCodDvs(String wpreCodDvs) {
        this.wpreCodDvs = Functions.subString(wpreCodDvs, Len.WPRE_COD_DVS);
    }

    public String getWpreCodDvs() {
        return this.wpreCodDvs;
    }

    public void setWpreDtRichPrest(int wpreDtRichPrest) {
        this.wpreDtRichPrest = wpreDtRichPrest;
    }

    public int getWpreDtRichPrest() {
        return this.wpreDtRichPrest;
    }

    public void setWpreModIntrPrest(String wpreModIntrPrest) {
        this.wpreModIntrPrest = Functions.subString(wpreModIntrPrest, Len.WPRE_MOD_INTR_PREST);
    }

    public String getWpreModIntrPrest() {
        return this.wpreModIntrPrest;
    }

    public void setWpreDsRiga(long wpreDsRiga) {
        this.wpreDsRiga = wpreDsRiga;
    }

    public long getWpreDsRiga() {
        return this.wpreDsRiga;
    }

    public void setWpreDsOperSql(char wpreDsOperSql) {
        this.wpreDsOperSql = wpreDsOperSql;
    }

    public char getWpreDsOperSql() {
        return this.wpreDsOperSql;
    }

    public void setWpreDsVer(int wpreDsVer) {
        this.wpreDsVer = wpreDsVer;
    }

    public int getWpreDsVer() {
        return this.wpreDsVer;
    }

    public void setWpreDsTsIniCptz(long wpreDsTsIniCptz) {
        this.wpreDsTsIniCptz = wpreDsTsIniCptz;
    }

    public long getWpreDsTsIniCptz() {
        return this.wpreDsTsIniCptz;
    }

    public void setWpreDsTsEndCptz(long wpreDsTsEndCptz) {
        this.wpreDsTsEndCptz = wpreDsTsEndCptz;
    }

    public long getWpreDsTsEndCptz() {
        return this.wpreDsTsEndCptz;
    }

    public void setWpreDsUtente(String wpreDsUtente) {
        this.wpreDsUtente = Functions.subString(wpreDsUtente, Len.WPRE_DS_UTENTE);
    }

    public String getWpreDsUtente() {
        return this.wpreDsUtente;
    }

    public void setWpreDsStatoElab(char wpreDsStatoElab) {
        this.wpreDsStatoElab = wpreDsStatoElab;
    }

    public char getWpreDsStatoElab() {
        return this.wpreDsStatoElab;
    }

    public WpreDtConcsPrest getWpreDtConcsPrest() {
        return wpreDtConcsPrest;
    }

    public WpreDtDecorPrest getWpreDtDecorPrest() {
        return wpreDtDecorPrest;
    }

    public WpreDtRimb getWpreDtRimb() {
        return wpreDtRimb;
    }

    public WpreFrazPagIntr getWpreFrazPagIntr() {
        return wpreFrazPagIntr;
    }

    public WpreIdMoviChiu getWpreIdMoviChiu() {
        return wpreIdMoviChiu;
    }

    public WpreImpPrest getWpreImpPrest() {
        return wpreImpPrest;
    }

    public WpreImpPrestLiqto getWpreImpPrestLiqto() {
        return wpreImpPrestLiqto;
    }

    public WpreImpRimb getWpreImpRimb() {
        return wpreImpRimb;
    }

    public WpreIntrPrest getWpreIntrPrest() {
        return wpreIntrPrest;
    }

    public WprePrestResEff getWprePrestResEff() {
        return wprePrestResEff;
    }

    public WpreRimbEff getWpreRimbEff() {
        return wpreRimbEff;
    }

    public WpreSdoIntr getWpreSdoIntr() {
        return wpreSdoIntr;
    }

    public WpreSpePrest getWpreSpePrest() {
        return wpreSpePrest;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_ID_PREST = 5;
        public static final int WPRE_ID_OGG = 5;
        public static final int WPRE_TP_OGG = 2;
        public static final int WPRE_ID_MOVI_CRZ = 5;
        public static final int WPRE_DT_INI_EFF = 5;
        public static final int WPRE_DT_END_EFF = 5;
        public static final int WPRE_COD_COMP_ANIA = 3;
        public static final int WPRE_TP_PREST = 2;
        public static final int WPRE_COD_DVS = 20;
        public static final int WPRE_DT_RICH_PREST = 5;
        public static final int WPRE_MOD_INTR_PREST = 2;
        public static final int WPRE_DS_RIGA = 6;
        public static final int WPRE_DS_OPER_SQL = 1;
        public static final int WPRE_DS_VER = 5;
        public static final int WPRE_DS_TS_INI_CPTZ = 10;
        public static final int WPRE_DS_TS_END_CPTZ = 10;
        public static final int WPRE_DS_UTENTE = 20;
        public static final int WPRE_DS_STATO_ELAB = 1;
        public static final int DATI = WPRE_ID_PREST + WPRE_ID_OGG + WPRE_TP_OGG + WPRE_ID_MOVI_CRZ + WpreIdMoviChiu.Len.WPRE_ID_MOVI_CHIU + WPRE_DT_INI_EFF + WPRE_DT_END_EFF + WPRE_COD_COMP_ANIA + WpreDtConcsPrest.Len.WPRE_DT_CONCS_PREST + WpreDtDecorPrest.Len.WPRE_DT_DECOR_PREST + WpreImpPrest.Len.WPRE_IMP_PREST + WpreIntrPrest.Len.WPRE_INTR_PREST + WPRE_TP_PREST + WpreFrazPagIntr.Len.WPRE_FRAZ_PAG_INTR + WpreDtRimb.Len.WPRE_DT_RIMB + WpreImpRimb.Len.WPRE_IMP_RIMB + WPRE_COD_DVS + WPRE_DT_RICH_PREST + WPRE_MOD_INTR_PREST + WpreSpePrest.Len.WPRE_SPE_PREST + WpreImpPrestLiqto.Len.WPRE_IMP_PREST_LIQTO + WpreSdoIntr.Len.WPRE_SDO_INTR + WpreRimbEff.Len.WPRE_RIMB_EFF + WprePrestResEff.Len.WPRE_PREST_RES_EFF + WPRE_DS_RIGA + WPRE_DS_OPER_SQL + WPRE_DS_VER + WPRE_DS_TS_INI_CPTZ + WPRE_DS_TS_END_CPTZ + WPRE_DS_UTENTE + WPRE_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_ID_PREST = 9;
            public static final int WPRE_ID_OGG = 9;
            public static final int WPRE_ID_MOVI_CRZ = 9;
            public static final int WPRE_DT_INI_EFF = 8;
            public static final int WPRE_DT_END_EFF = 8;
            public static final int WPRE_COD_COMP_ANIA = 5;
            public static final int WPRE_DT_RICH_PREST = 8;
            public static final int WPRE_DS_RIGA = 10;
            public static final int WPRE_DS_VER = 9;
            public static final int WPRE_DS_TS_INI_CPTZ = 18;
            public static final int WPRE_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
