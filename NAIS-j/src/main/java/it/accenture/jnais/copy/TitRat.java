package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.TdrDtApplzMora;
import it.accenture.jnais.ws.redefines.TdrDtEndCop;
import it.accenture.jnais.ws.redefines.TdrDtEsiTit;
import it.accenture.jnais.ws.redefines.TdrDtIniCop;
import it.accenture.jnais.ws.redefines.TdrFraz;
import it.accenture.jnais.ws.redefines.TdrIdMoviChiu;
import it.accenture.jnais.ws.redefines.TdrIdRappAna;
import it.accenture.jnais.ws.redefines.TdrIdRappRete;
import it.accenture.jnais.ws.redefines.TdrImpAder;
import it.accenture.jnais.ws.redefines.TdrImpAz;
import it.accenture.jnais.ws.redefines.TdrImpPag;
import it.accenture.jnais.ws.redefines.TdrImpTfr;
import it.accenture.jnais.ws.redefines.TdrImpTfrStrc;
import it.accenture.jnais.ws.redefines.TdrImpTrasfe;
import it.accenture.jnais.ws.redefines.TdrImpVolo;
import it.accenture.jnais.ws.redefines.TdrProgTit;
import it.accenture.jnais.ws.redefines.TdrTotAcqExp;
import it.accenture.jnais.ws.redefines.TdrTotCarAcq;
import it.accenture.jnais.ws.redefines.TdrTotCarGest;
import it.accenture.jnais.ws.redefines.TdrTotCarIas;
import it.accenture.jnais.ws.redefines.TdrTotCarInc;
import it.accenture.jnais.ws.redefines.TdrTotCnbtAntirac;
import it.accenture.jnais.ws.redefines.TdrTotCommisInter;
import it.accenture.jnais.ws.redefines.TdrTotDir;
import it.accenture.jnais.ws.redefines.TdrTotIntrFraz;
import it.accenture.jnais.ws.redefines.TdrTotIntrMora;
import it.accenture.jnais.ws.redefines.TdrTotIntrPrest;
import it.accenture.jnais.ws.redefines.TdrTotIntrRetdt;
import it.accenture.jnais.ws.redefines.TdrTotIntrRiat;
import it.accenture.jnais.ws.redefines.TdrTotManfeeAntic;
import it.accenture.jnais.ws.redefines.TdrTotManfeeRec;
import it.accenture.jnais.ws.redefines.TdrTotManfeeRicor;
import it.accenture.jnais.ws.redefines.TdrTotPreNet;
import it.accenture.jnais.ws.redefines.TdrTotPrePpIas;
import it.accenture.jnais.ws.redefines.TdrTotPreSoloRsh;
import it.accenture.jnais.ws.redefines.TdrTotPreTot;
import it.accenture.jnais.ws.redefines.TdrTotProvAcq1aa;
import it.accenture.jnais.ws.redefines.TdrTotProvAcq2aa;
import it.accenture.jnais.ws.redefines.TdrTotProvDaRec;
import it.accenture.jnais.ws.redefines.TdrTotProvInc;
import it.accenture.jnais.ws.redefines.TdrTotProvRicor;
import it.accenture.jnais.ws.redefines.TdrTotRemunAss;
import it.accenture.jnais.ws.redefines.TdrTotSoprAlt;
import it.accenture.jnais.ws.redefines.TdrTotSoprProf;
import it.accenture.jnais.ws.redefines.TdrTotSoprSan;
import it.accenture.jnais.ws.redefines.TdrTotSoprSpo;
import it.accenture.jnais.ws.redefines.TdrTotSoprTec;
import it.accenture.jnais.ws.redefines.TdrTotSpeAge;
import it.accenture.jnais.ws.redefines.TdrTotSpeMed;
import it.accenture.jnais.ws.redefines.TdrTotTax;

/**Original name: TIT-RAT<br>
 * Variable: TIT-RAT from copybook IDBVTDR1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TitRat {

    //==== PROPERTIES ====
    //Original name: TDR-ID-TIT-RAT
    private int tdrIdTitRat = DefaultValues.INT_VAL;
    //Original name: TDR-ID-OGG
    private int tdrIdOgg = DefaultValues.INT_VAL;
    //Original name: TDR-TP-OGG
    private String tdrTpOgg = DefaultValues.stringVal(Len.TDR_TP_OGG);
    //Original name: TDR-ID-MOVI-CRZ
    private int tdrIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: TDR-ID-MOVI-CHIU
    private TdrIdMoviChiu tdrIdMoviChiu = new TdrIdMoviChiu();
    //Original name: TDR-DT-INI-EFF
    private int tdrDtIniEff = DefaultValues.INT_VAL;
    //Original name: TDR-DT-END-EFF
    private int tdrDtEndEff = DefaultValues.INT_VAL;
    //Original name: TDR-COD-COMP-ANIA
    private int tdrCodCompAnia = DefaultValues.INT_VAL;
    //Original name: TDR-TP-TIT
    private String tdrTpTit = DefaultValues.stringVal(Len.TDR_TP_TIT);
    //Original name: TDR-PROG-TIT
    private TdrProgTit tdrProgTit = new TdrProgTit();
    //Original name: TDR-TP-PRE-TIT
    private String tdrTpPreTit = DefaultValues.stringVal(Len.TDR_TP_PRE_TIT);
    //Original name: TDR-TP-STAT-TIT
    private String tdrTpStatTit = DefaultValues.stringVal(Len.TDR_TP_STAT_TIT);
    //Original name: TDR-DT-INI-COP
    private TdrDtIniCop tdrDtIniCop = new TdrDtIniCop();
    //Original name: TDR-DT-END-COP
    private TdrDtEndCop tdrDtEndCop = new TdrDtEndCop();
    //Original name: TDR-IMP-PAG
    private TdrImpPag tdrImpPag = new TdrImpPag();
    //Original name: TDR-FL-SOLL
    private char tdrFlSoll = DefaultValues.CHAR_VAL;
    //Original name: TDR-FRAZ
    private TdrFraz tdrFraz = new TdrFraz();
    //Original name: TDR-DT-APPLZ-MORA
    private TdrDtApplzMora tdrDtApplzMora = new TdrDtApplzMora();
    //Original name: TDR-FL-MORA
    private char tdrFlMora = DefaultValues.CHAR_VAL;
    //Original name: TDR-ID-RAPP-RETE
    private TdrIdRappRete tdrIdRappRete = new TdrIdRappRete();
    //Original name: TDR-ID-RAPP-ANA
    private TdrIdRappAna tdrIdRappAna = new TdrIdRappAna();
    //Original name: TDR-COD-DVS
    private String tdrCodDvs = DefaultValues.stringVal(Len.TDR_COD_DVS);
    //Original name: TDR-DT-EMIS-TIT
    private int tdrDtEmisTit = DefaultValues.INT_VAL;
    //Original name: TDR-DT-ESI-TIT
    private TdrDtEsiTit tdrDtEsiTit = new TdrDtEsiTit();
    //Original name: TDR-TOT-PRE-NET
    private TdrTotPreNet tdrTotPreNet = new TdrTotPreNet();
    //Original name: TDR-TOT-INTR-FRAZ
    private TdrTotIntrFraz tdrTotIntrFraz = new TdrTotIntrFraz();
    //Original name: TDR-TOT-INTR-MORA
    private TdrTotIntrMora tdrTotIntrMora = new TdrTotIntrMora();
    //Original name: TDR-TOT-INTR-PREST
    private TdrTotIntrPrest tdrTotIntrPrest = new TdrTotIntrPrest();
    //Original name: TDR-TOT-INTR-RETDT
    private TdrTotIntrRetdt tdrTotIntrRetdt = new TdrTotIntrRetdt();
    //Original name: TDR-TOT-INTR-RIAT
    private TdrTotIntrRiat tdrTotIntrRiat = new TdrTotIntrRiat();
    //Original name: TDR-TOT-DIR
    private TdrTotDir tdrTotDir = new TdrTotDir();
    //Original name: TDR-TOT-SPE-MED
    private TdrTotSpeMed tdrTotSpeMed = new TdrTotSpeMed();
    //Original name: TDR-TOT-SPE-AGE
    private TdrTotSpeAge tdrTotSpeAge = new TdrTotSpeAge();
    //Original name: TDR-TOT-TAX
    private TdrTotTax tdrTotTax = new TdrTotTax();
    //Original name: TDR-TOT-SOPR-SAN
    private TdrTotSoprSan tdrTotSoprSan = new TdrTotSoprSan();
    //Original name: TDR-TOT-SOPR-TEC
    private TdrTotSoprTec tdrTotSoprTec = new TdrTotSoprTec();
    //Original name: TDR-TOT-SOPR-SPO
    private TdrTotSoprSpo tdrTotSoprSpo = new TdrTotSoprSpo();
    //Original name: TDR-TOT-SOPR-PROF
    private TdrTotSoprProf tdrTotSoprProf = new TdrTotSoprProf();
    //Original name: TDR-TOT-SOPR-ALT
    private TdrTotSoprAlt tdrTotSoprAlt = new TdrTotSoprAlt();
    //Original name: TDR-TOT-PRE-TOT
    private TdrTotPreTot tdrTotPreTot = new TdrTotPreTot();
    //Original name: TDR-TOT-PRE-PP-IAS
    private TdrTotPrePpIas tdrTotPrePpIas = new TdrTotPrePpIas();
    //Original name: TDR-TOT-CAR-IAS
    private TdrTotCarIas tdrTotCarIas = new TdrTotCarIas();
    //Original name: TDR-TOT-PRE-SOLO-RSH
    private TdrTotPreSoloRsh tdrTotPreSoloRsh = new TdrTotPreSoloRsh();
    //Original name: TDR-TOT-PROV-ACQ-1AA
    private TdrTotProvAcq1aa tdrTotProvAcq1aa = new TdrTotProvAcq1aa();
    //Original name: TDR-TOT-PROV-ACQ-2AA
    private TdrTotProvAcq2aa tdrTotProvAcq2aa = new TdrTotProvAcq2aa();
    //Original name: TDR-TOT-PROV-RICOR
    private TdrTotProvRicor tdrTotProvRicor = new TdrTotProvRicor();
    //Original name: TDR-TOT-PROV-INC
    private TdrTotProvInc tdrTotProvInc = new TdrTotProvInc();
    //Original name: TDR-TOT-PROV-DA-REC
    private TdrTotProvDaRec tdrTotProvDaRec = new TdrTotProvDaRec();
    //Original name: TDR-IMP-AZ
    private TdrImpAz tdrImpAz = new TdrImpAz();
    //Original name: TDR-IMP-ADER
    private TdrImpAder tdrImpAder = new TdrImpAder();
    //Original name: TDR-IMP-TFR
    private TdrImpTfr tdrImpTfr = new TdrImpTfr();
    //Original name: TDR-IMP-VOLO
    private TdrImpVolo tdrImpVolo = new TdrImpVolo();
    //Original name: TDR-FL-VLDT-TIT
    private char tdrFlVldtTit = DefaultValues.CHAR_VAL;
    //Original name: TDR-TOT-CAR-ACQ
    private TdrTotCarAcq tdrTotCarAcq = new TdrTotCarAcq();
    //Original name: TDR-TOT-CAR-GEST
    private TdrTotCarGest tdrTotCarGest = new TdrTotCarGest();
    //Original name: TDR-TOT-CAR-INC
    private TdrTotCarInc tdrTotCarInc = new TdrTotCarInc();
    //Original name: TDR-TOT-MANFEE-ANTIC
    private TdrTotManfeeAntic tdrTotManfeeAntic = new TdrTotManfeeAntic();
    //Original name: TDR-TOT-MANFEE-RICOR
    private TdrTotManfeeRicor tdrTotManfeeRicor = new TdrTotManfeeRicor();
    //Original name: TDR-TOT-MANFEE-REC
    private TdrTotManfeeRec tdrTotManfeeRec = new TdrTotManfeeRec();
    //Original name: TDR-DS-RIGA
    private long tdrDsRiga = DefaultValues.LONG_VAL;
    //Original name: TDR-DS-OPER-SQL
    private char tdrDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: TDR-DS-VER
    private int tdrDsVer = DefaultValues.INT_VAL;
    //Original name: TDR-DS-TS-INI-CPTZ
    private long tdrDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: TDR-DS-TS-END-CPTZ
    private long tdrDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: TDR-DS-UTENTE
    private String tdrDsUtente = DefaultValues.stringVal(Len.TDR_DS_UTENTE);
    //Original name: TDR-DS-STATO-ELAB
    private char tdrDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: TDR-IMP-TRASFE
    private TdrImpTrasfe tdrImpTrasfe = new TdrImpTrasfe();
    //Original name: TDR-IMP-TFR-STRC
    private TdrImpTfrStrc tdrImpTfrStrc = new TdrImpTfrStrc();
    //Original name: TDR-TOT-ACQ-EXP
    private TdrTotAcqExp tdrTotAcqExp = new TdrTotAcqExp();
    //Original name: TDR-TOT-REMUN-ASS
    private TdrTotRemunAss tdrTotRemunAss = new TdrTotRemunAss();
    //Original name: TDR-TOT-COMMIS-INTER
    private TdrTotCommisInter tdrTotCommisInter = new TdrTotCommisInter();
    //Original name: TDR-TOT-CNBT-ANTIRAC
    private TdrTotCnbtAntirac tdrTotCnbtAntirac = new TdrTotCnbtAntirac();
    //Original name: TDR-FL-INC-AUTOGEN
    private char tdrFlIncAutogen = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setTitRatFormatted(String data) {
        byte[] buffer = new byte[Len.TIT_RAT];
        MarshalByte.writeString(buffer, 1, data, Len.TIT_RAT);
        setTitRatBytes(buffer, 1);
    }

    public String getTitRatFormatted() {
        return MarshalByteExt.bufferToStr(getTitRatBytes());
    }

    public byte[] getTitRatBytes() {
        byte[] buffer = new byte[Len.TIT_RAT];
        return getTitRatBytes(buffer, 1);
    }

    public void setTitRatBytes(byte[] buffer, int offset) {
        int position = offset;
        tdrIdTitRat = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TDR_ID_TIT_RAT, 0);
        position += Len.TDR_ID_TIT_RAT;
        tdrIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TDR_ID_OGG, 0);
        position += Len.TDR_ID_OGG;
        tdrTpOgg = MarshalByte.readString(buffer, position, Len.TDR_TP_OGG);
        position += Len.TDR_TP_OGG;
        tdrIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TDR_ID_MOVI_CRZ, 0);
        position += Len.TDR_ID_MOVI_CRZ;
        tdrIdMoviChiu.setTdrIdMoviChiuFromBuffer(buffer, position);
        position += TdrIdMoviChiu.Len.TDR_ID_MOVI_CHIU;
        tdrDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TDR_DT_INI_EFF, 0);
        position += Len.TDR_DT_INI_EFF;
        tdrDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TDR_DT_END_EFF, 0);
        position += Len.TDR_DT_END_EFF;
        tdrCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TDR_COD_COMP_ANIA, 0);
        position += Len.TDR_COD_COMP_ANIA;
        tdrTpTit = MarshalByte.readString(buffer, position, Len.TDR_TP_TIT);
        position += Len.TDR_TP_TIT;
        tdrProgTit.setTdrProgTitFromBuffer(buffer, position);
        position += TdrProgTit.Len.TDR_PROG_TIT;
        tdrTpPreTit = MarshalByte.readString(buffer, position, Len.TDR_TP_PRE_TIT);
        position += Len.TDR_TP_PRE_TIT;
        tdrTpStatTit = MarshalByte.readString(buffer, position, Len.TDR_TP_STAT_TIT);
        position += Len.TDR_TP_STAT_TIT;
        tdrDtIniCop.setTdrDtIniCopFromBuffer(buffer, position);
        position += TdrDtIniCop.Len.TDR_DT_INI_COP;
        tdrDtEndCop.setTdrDtEndCopFromBuffer(buffer, position);
        position += TdrDtEndCop.Len.TDR_DT_END_COP;
        tdrImpPag.setTdrImpPagFromBuffer(buffer, position);
        position += TdrImpPag.Len.TDR_IMP_PAG;
        tdrFlSoll = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tdrFraz.setTdrFrazFromBuffer(buffer, position);
        position += TdrFraz.Len.TDR_FRAZ;
        tdrDtApplzMora.setTdrDtApplzMoraFromBuffer(buffer, position);
        position += TdrDtApplzMora.Len.TDR_DT_APPLZ_MORA;
        tdrFlMora = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tdrIdRappRete.setTdrIdRappReteFromBuffer(buffer, position);
        position += TdrIdRappRete.Len.TDR_ID_RAPP_RETE;
        tdrIdRappAna.setTdrIdRappAnaFromBuffer(buffer, position);
        position += TdrIdRappAna.Len.TDR_ID_RAPP_ANA;
        tdrCodDvs = MarshalByte.readString(buffer, position, Len.TDR_COD_DVS);
        position += Len.TDR_COD_DVS;
        tdrDtEmisTit = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TDR_DT_EMIS_TIT, 0);
        position += Len.TDR_DT_EMIS_TIT;
        tdrDtEsiTit.setTdrDtEsiTitFromBuffer(buffer, position);
        position += TdrDtEsiTit.Len.TDR_DT_ESI_TIT;
        tdrTotPreNet.setTdrTotPreNetFromBuffer(buffer, position);
        position += TdrTotPreNet.Len.TDR_TOT_PRE_NET;
        tdrTotIntrFraz.setTdrTotIntrFrazFromBuffer(buffer, position);
        position += TdrTotIntrFraz.Len.TDR_TOT_INTR_FRAZ;
        tdrTotIntrMora.setTdrTotIntrMoraFromBuffer(buffer, position);
        position += TdrTotIntrMora.Len.TDR_TOT_INTR_MORA;
        tdrTotIntrPrest.setTdrTotIntrPrestFromBuffer(buffer, position);
        position += TdrTotIntrPrest.Len.TDR_TOT_INTR_PREST;
        tdrTotIntrRetdt.setTdrTotIntrRetdtFromBuffer(buffer, position);
        position += TdrTotIntrRetdt.Len.TDR_TOT_INTR_RETDT;
        tdrTotIntrRiat.setTdrTotIntrRiatFromBuffer(buffer, position);
        position += TdrTotIntrRiat.Len.TDR_TOT_INTR_RIAT;
        tdrTotDir.setTdrTotDirFromBuffer(buffer, position);
        position += TdrTotDir.Len.TDR_TOT_DIR;
        tdrTotSpeMed.setTdrTotSpeMedFromBuffer(buffer, position);
        position += TdrTotSpeMed.Len.TDR_TOT_SPE_MED;
        tdrTotSpeAge.setTdrTotSpeAgeFromBuffer(buffer, position);
        position += TdrTotSpeAge.Len.TDR_TOT_SPE_AGE;
        tdrTotTax.setTdrTotTaxFromBuffer(buffer, position);
        position += TdrTotTax.Len.TDR_TOT_TAX;
        tdrTotSoprSan.setTdrTotSoprSanFromBuffer(buffer, position);
        position += TdrTotSoprSan.Len.TDR_TOT_SOPR_SAN;
        tdrTotSoprTec.setTdrTotSoprTecFromBuffer(buffer, position);
        position += TdrTotSoprTec.Len.TDR_TOT_SOPR_TEC;
        tdrTotSoprSpo.setTdrTotSoprSpoFromBuffer(buffer, position);
        position += TdrTotSoprSpo.Len.TDR_TOT_SOPR_SPO;
        tdrTotSoprProf.setTdrTotSoprProfFromBuffer(buffer, position);
        position += TdrTotSoprProf.Len.TDR_TOT_SOPR_PROF;
        tdrTotSoprAlt.setTdrTotSoprAltFromBuffer(buffer, position);
        position += TdrTotSoprAlt.Len.TDR_TOT_SOPR_ALT;
        tdrTotPreTot.setTdrTotPreTotFromBuffer(buffer, position);
        position += TdrTotPreTot.Len.TDR_TOT_PRE_TOT;
        tdrTotPrePpIas.setTdrTotPrePpIasFromBuffer(buffer, position);
        position += TdrTotPrePpIas.Len.TDR_TOT_PRE_PP_IAS;
        tdrTotCarIas.setTdrTotCarIasFromBuffer(buffer, position);
        position += TdrTotCarIas.Len.TDR_TOT_CAR_IAS;
        tdrTotPreSoloRsh.setTdrTotPreSoloRshFromBuffer(buffer, position);
        position += TdrTotPreSoloRsh.Len.TDR_TOT_PRE_SOLO_RSH;
        tdrTotProvAcq1aa.setTdrTotProvAcq1aaFromBuffer(buffer, position);
        position += TdrTotProvAcq1aa.Len.TDR_TOT_PROV_ACQ1AA;
        tdrTotProvAcq2aa.setTdrTotProvAcq2aaFromBuffer(buffer, position);
        position += TdrTotProvAcq2aa.Len.TDR_TOT_PROV_ACQ2AA;
        tdrTotProvRicor.setTdrTotProvRicorFromBuffer(buffer, position);
        position += TdrTotProvRicor.Len.TDR_TOT_PROV_RICOR;
        tdrTotProvInc.setTdrTotProvIncFromBuffer(buffer, position);
        position += TdrTotProvInc.Len.TDR_TOT_PROV_INC;
        tdrTotProvDaRec.setTdrTotProvDaRecFromBuffer(buffer, position);
        position += TdrTotProvDaRec.Len.TDR_TOT_PROV_DA_REC;
        tdrImpAz.setTdrImpAzFromBuffer(buffer, position);
        position += TdrImpAz.Len.TDR_IMP_AZ;
        tdrImpAder.setTdrImpAderFromBuffer(buffer, position);
        position += TdrImpAder.Len.TDR_IMP_ADER;
        tdrImpTfr.setTdrImpTfrFromBuffer(buffer, position);
        position += TdrImpTfr.Len.TDR_IMP_TFR;
        tdrImpVolo.setTdrImpVoloFromBuffer(buffer, position);
        position += TdrImpVolo.Len.TDR_IMP_VOLO;
        tdrFlVldtTit = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tdrTotCarAcq.setTdrTotCarAcqFromBuffer(buffer, position);
        position += TdrTotCarAcq.Len.TDR_TOT_CAR_ACQ;
        tdrTotCarGest.setTdrTotCarGestFromBuffer(buffer, position);
        position += TdrTotCarGest.Len.TDR_TOT_CAR_GEST;
        tdrTotCarInc.setTdrTotCarIncFromBuffer(buffer, position);
        position += TdrTotCarInc.Len.TDR_TOT_CAR_INC;
        tdrTotManfeeAntic.setTdrTotManfeeAnticFromBuffer(buffer, position);
        position += TdrTotManfeeAntic.Len.TDR_TOT_MANFEE_ANTIC;
        tdrTotManfeeRicor.setTdrTotManfeeRicorFromBuffer(buffer, position);
        position += TdrTotManfeeRicor.Len.TDR_TOT_MANFEE_RICOR;
        tdrTotManfeeRec.setTdrTotManfeeRecFromBuffer(buffer, position);
        position += TdrTotManfeeRec.Len.TDR_TOT_MANFEE_REC;
        tdrDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TDR_DS_RIGA, 0);
        position += Len.TDR_DS_RIGA;
        tdrDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tdrDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TDR_DS_VER, 0);
        position += Len.TDR_DS_VER;
        tdrDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TDR_DS_TS_INI_CPTZ, 0);
        position += Len.TDR_DS_TS_INI_CPTZ;
        tdrDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TDR_DS_TS_END_CPTZ, 0);
        position += Len.TDR_DS_TS_END_CPTZ;
        tdrDsUtente = MarshalByte.readString(buffer, position, Len.TDR_DS_UTENTE);
        position += Len.TDR_DS_UTENTE;
        tdrDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tdrImpTrasfe.setTdrImpTrasfeFromBuffer(buffer, position);
        position += TdrImpTrasfe.Len.TDR_IMP_TRASFE;
        tdrImpTfrStrc.setTdrImpTfrStrcFromBuffer(buffer, position);
        position += TdrImpTfrStrc.Len.TDR_IMP_TFR_STRC;
        tdrTotAcqExp.setTdrTotAcqExpFromBuffer(buffer, position);
        position += TdrTotAcqExp.Len.TDR_TOT_ACQ_EXP;
        tdrTotRemunAss.setTdrTotRemunAssFromBuffer(buffer, position);
        position += TdrTotRemunAss.Len.TDR_TOT_REMUN_ASS;
        tdrTotCommisInter.setTdrTotCommisInterFromBuffer(buffer, position);
        position += TdrTotCommisInter.Len.TDR_TOT_COMMIS_INTER;
        tdrTotCnbtAntirac.setTdrTotCnbtAntiracFromBuffer(buffer, position);
        position += TdrTotCnbtAntirac.Len.TDR_TOT_CNBT_ANTIRAC;
        tdrFlIncAutogen = MarshalByte.readChar(buffer, position);
    }

    public byte[] getTitRatBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, tdrIdTitRat, Len.Int.TDR_ID_TIT_RAT, 0);
        position += Len.TDR_ID_TIT_RAT;
        MarshalByte.writeIntAsPacked(buffer, position, tdrIdOgg, Len.Int.TDR_ID_OGG, 0);
        position += Len.TDR_ID_OGG;
        MarshalByte.writeString(buffer, position, tdrTpOgg, Len.TDR_TP_OGG);
        position += Len.TDR_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, tdrIdMoviCrz, Len.Int.TDR_ID_MOVI_CRZ, 0);
        position += Len.TDR_ID_MOVI_CRZ;
        tdrIdMoviChiu.getTdrIdMoviChiuAsBuffer(buffer, position);
        position += TdrIdMoviChiu.Len.TDR_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, tdrDtIniEff, Len.Int.TDR_DT_INI_EFF, 0);
        position += Len.TDR_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, tdrDtEndEff, Len.Int.TDR_DT_END_EFF, 0);
        position += Len.TDR_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, tdrCodCompAnia, Len.Int.TDR_COD_COMP_ANIA, 0);
        position += Len.TDR_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, tdrTpTit, Len.TDR_TP_TIT);
        position += Len.TDR_TP_TIT;
        tdrProgTit.getTdrProgTitAsBuffer(buffer, position);
        position += TdrProgTit.Len.TDR_PROG_TIT;
        MarshalByte.writeString(buffer, position, tdrTpPreTit, Len.TDR_TP_PRE_TIT);
        position += Len.TDR_TP_PRE_TIT;
        MarshalByte.writeString(buffer, position, tdrTpStatTit, Len.TDR_TP_STAT_TIT);
        position += Len.TDR_TP_STAT_TIT;
        tdrDtIniCop.getTdrDtIniCopAsBuffer(buffer, position);
        position += TdrDtIniCop.Len.TDR_DT_INI_COP;
        tdrDtEndCop.getTdrDtEndCopAsBuffer(buffer, position);
        position += TdrDtEndCop.Len.TDR_DT_END_COP;
        tdrImpPag.getTdrImpPagAsBuffer(buffer, position);
        position += TdrImpPag.Len.TDR_IMP_PAG;
        MarshalByte.writeChar(buffer, position, tdrFlSoll);
        position += Types.CHAR_SIZE;
        tdrFraz.getTdrFrazAsBuffer(buffer, position);
        position += TdrFraz.Len.TDR_FRAZ;
        tdrDtApplzMora.getTdrDtApplzMoraAsBuffer(buffer, position);
        position += TdrDtApplzMora.Len.TDR_DT_APPLZ_MORA;
        MarshalByte.writeChar(buffer, position, tdrFlMora);
        position += Types.CHAR_SIZE;
        tdrIdRappRete.getTdrIdRappReteAsBuffer(buffer, position);
        position += TdrIdRappRete.Len.TDR_ID_RAPP_RETE;
        tdrIdRappAna.getTdrIdRappAnaAsBuffer(buffer, position);
        position += TdrIdRappAna.Len.TDR_ID_RAPP_ANA;
        MarshalByte.writeString(buffer, position, tdrCodDvs, Len.TDR_COD_DVS);
        position += Len.TDR_COD_DVS;
        MarshalByte.writeIntAsPacked(buffer, position, tdrDtEmisTit, Len.Int.TDR_DT_EMIS_TIT, 0);
        position += Len.TDR_DT_EMIS_TIT;
        tdrDtEsiTit.getTdrDtEsiTitAsBuffer(buffer, position);
        position += TdrDtEsiTit.Len.TDR_DT_ESI_TIT;
        tdrTotPreNet.getTdrTotPreNetAsBuffer(buffer, position);
        position += TdrTotPreNet.Len.TDR_TOT_PRE_NET;
        tdrTotIntrFraz.getTdrTotIntrFrazAsBuffer(buffer, position);
        position += TdrTotIntrFraz.Len.TDR_TOT_INTR_FRAZ;
        tdrTotIntrMora.getTdrTotIntrMoraAsBuffer(buffer, position);
        position += TdrTotIntrMora.Len.TDR_TOT_INTR_MORA;
        tdrTotIntrPrest.getTdrTotIntrPrestAsBuffer(buffer, position);
        position += TdrTotIntrPrest.Len.TDR_TOT_INTR_PREST;
        tdrTotIntrRetdt.getTdrTotIntrRetdtAsBuffer(buffer, position);
        position += TdrTotIntrRetdt.Len.TDR_TOT_INTR_RETDT;
        tdrTotIntrRiat.getTdrTotIntrRiatAsBuffer(buffer, position);
        position += TdrTotIntrRiat.Len.TDR_TOT_INTR_RIAT;
        tdrTotDir.getTdrTotDirAsBuffer(buffer, position);
        position += TdrTotDir.Len.TDR_TOT_DIR;
        tdrTotSpeMed.getTdrTotSpeMedAsBuffer(buffer, position);
        position += TdrTotSpeMed.Len.TDR_TOT_SPE_MED;
        tdrTotSpeAge.getTdrTotSpeAgeAsBuffer(buffer, position);
        position += TdrTotSpeAge.Len.TDR_TOT_SPE_AGE;
        tdrTotTax.getTdrTotTaxAsBuffer(buffer, position);
        position += TdrTotTax.Len.TDR_TOT_TAX;
        tdrTotSoprSan.getTdrTotSoprSanAsBuffer(buffer, position);
        position += TdrTotSoprSan.Len.TDR_TOT_SOPR_SAN;
        tdrTotSoprTec.getTdrTotSoprTecAsBuffer(buffer, position);
        position += TdrTotSoprTec.Len.TDR_TOT_SOPR_TEC;
        tdrTotSoprSpo.getTdrTotSoprSpoAsBuffer(buffer, position);
        position += TdrTotSoprSpo.Len.TDR_TOT_SOPR_SPO;
        tdrTotSoprProf.getTdrTotSoprProfAsBuffer(buffer, position);
        position += TdrTotSoprProf.Len.TDR_TOT_SOPR_PROF;
        tdrTotSoprAlt.getTdrTotSoprAltAsBuffer(buffer, position);
        position += TdrTotSoprAlt.Len.TDR_TOT_SOPR_ALT;
        tdrTotPreTot.getTdrTotPreTotAsBuffer(buffer, position);
        position += TdrTotPreTot.Len.TDR_TOT_PRE_TOT;
        tdrTotPrePpIas.getTdrTotPrePpIasAsBuffer(buffer, position);
        position += TdrTotPrePpIas.Len.TDR_TOT_PRE_PP_IAS;
        tdrTotCarIas.getTdrTotCarIasAsBuffer(buffer, position);
        position += TdrTotCarIas.Len.TDR_TOT_CAR_IAS;
        tdrTotPreSoloRsh.getTdrTotPreSoloRshAsBuffer(buffer, position);
        position += TdrTotPreSoloRsh.Len.TDR_TOT_PRE_SOLO_RSH;
        tdrTotProvAcq1aa.getTdrTotProvAcq1aaAsBuffer(buffer, position);
        position += TdrTotProvAcq1aa.Len.TDR_TOT_PROV_ACQ1AA;
        tdrTotProvAcq2aa.getTdrTotProvAcq2aaAsBuffer(buffer, position);
        position += TdrTotProvAcq2aa.Len.TDR_TOT_PROV_ACQ2AA;
        tdrTotProvRicor.getTdrTotProvRicorAsBuffer(buffer, position);
        position += TdrTotProvRicor.Len.TDR_TOT_PROV_RICOR;
        tdrTotProvInc.getTdrTotProvIncAsBuffer(buffer, position);
        position += TdrTotProvInc.Len.TDR_TOT_PROV_INC;
        tdrTotProvDaRec.getTdrTotProvDaRecAsBuffer(buffer, position);
        position += TdrTotProvDaRec.Len.TDR_TOT_PROV_DA_REC;
        tdrImpAz.getTdrImpAzAsBuffer(buffer, position);
        position += TdrImpAz.Len.TDR_IMP_AZ;
        tdrImpAder.getTdrImpAderAsBuffer(buffer, position);
        position += TdrImpAder.Len.TDR_IMP_ADER;
        tdrImpTfr.getTdrImpTfrAsBuffer(buffer, position);
        position += TdrImpTfr.Len.TDR_IMP_TFR;
        tdrImpVolo.getTdrImpVoloAsBuffer(buffer, position);
        position += TdrImpVolo.Len.TDR_IMP_VOLO;
        MarshalByte.writeChar(buffer, position, tdrFlVldtTit);
        position += Types.CHAR_SIZE;
        tdrTotCarAcq.getTdrTotCarAcqAsBuffer(buffer, position);
        position += TdrTotCarAcq.Len.TDR_TOT_CAR_ACQ;
        tdrTotCarGest.getTdrTotCarGestAsBuffer(buffer, position);
        position += TdrTotCarGest.Len.TDR_TOT_CAR_GEST;
        tdrTotCarInc.getTdrTotCarIncAsBuffer(buffer, position);
        position += TdrTotCarInc.Len.TDR_TOT_CAR_INC;
        tdrTotManfeeAntic.getTdrTotManfeeAnticAsBuffer(buffer, position);
        position += TdrTotManfeeAntic.Len.TDR_TOT_MANFEE_ANTIC;
        tdrTotManfeeRicor.getTdrTotManfeeRicorAsBuffer(buffer, position);
        position += TdrTotManfeeRicor.Len.TDR_TOT_MANFEE_RICOR;
        tdrTotManfeeRec.getTdrTotManfeeRecAsBuffer(buffer, position);
        position += TdrTotManfeeRec.Len.TDR_TOT_MANFEE_REC;
        MarshalByte.writeLongAsPacked(buffer, position, tdrDsRiga, Len.Int.TDR_DS_RIGA, 0);
        position += Len.TDR_DS_RIGA;
        MarshalByte.writeChar(buffer, position, tdrDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, tdrDsVer, Len.Int.TDR_DS_VER, 0);
        position += Len.TDR_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, tdrDsTsIniCptz, Len.Int.TDR_DS_TS_INI_CPTZ, 0);
        position += Len.TDR_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, tdrDsTsEndCptz, Len.Int.TDR_DS_TS_END_CPTZ, 0);
        position += Len.TDR_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, tdrDsUtente, Len.TDR_DS_UTENTE);
        position += Len.TDR_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, tdrDsStatoElab);
        position += Types.CHAR_SIZE;
        tdrImpTrasfe.getTdrImpTrasfeAsBuffer(buffer, position);
        position += TdrImpTrasfe.Len.TDR_IMP_TRASFE;
        tdrImpTfrStrc.getTdrImpTfrStrcAsBuffer(buffer, position);
        position += TdrImpTfrStrc.Len.TDR_IMP_TFR_STRC;
        tdrTotAcqExp.getTdrTotAcqExpAsBuffer(buffer, position);
        position += TdrTotAcqExp.Len.TDR_TOT_ACQ_EXP;
        tdrTotRemunAss.getTdrTotRemunAssAsBuffer(buffer, position);
        position += TdrTotRemunAss.Len.TDR_TOT_REMUN_ASS;
        tdrTotCommisInter.getTdrTotCommisInterAsBuffer(buffer, position);
        position += TdrTotCommisInter.Len.TDR_TOT_COMMIS_INTER;
        tdrTotCnbtAntirac.getTdrTotCnbtAntiracAsBuffer(buffer, position);
        position += TdrTotCnbtAntirac.Len.TDR_TOT_CNBT_ANTIRAC;
        MarshalByte.writeChar(buffer, position, tdrFlIncAutogen);
        return buffer;
    }

    public void setTdrIdTitRat(int tdrIdTitRat) {
        this.tdrIdTitRat = tdrIdTitRat;
    }

    public int getTdrIdTitRat() {
        return this.tdrIdTitRat;
    }

    public void setTdrIdOgg(int tdrIdOgg) {
        this.tdrIdOgg = tdrIdOgg;
    }

    public int getTdrIdOgg() {
        return this.tdrIdOgg;
    }

    public void setTdrTpOgg(String tdrTpOgg) {
        this.tdrTpOgg = Functions.subString(tdrTpOgg, Len.TDR_TP_OGG);
    }

    public String getTdrTpOgg() {
        return this.tdrTpOgg;
    }

    public void setTdrIdMoviCrz(int tdrIdMoviCrz) {
        this.tdrIdMoviCrz = tdrIdMoviCrz;
    }

    public int getTdrIdMoviCrz() {
        return this.tdrIdMoviCrz;
    }

    public void setTdrDtIniEff(int tdrDtIniEff) {
        this.tdrDtIniEff = tdrDtIniEff;
    }

    public int getTdrDtIniEff() {
        return this.tdrDtIniEff;
    }

    public void setTdrDtEndEff(int tdrDtEndEff) {
        this.tdrDtEndEff = tdrDtEndEff;
    }

    public int getTdrDtEndEff() {
        return this.tdrDtEndEff;
    }

    public void setTdrCodCompAnia(int tdrCodCompAnia) {
        this.tdrCodCompAnia = tdrCodCompAnia;
    }

    public int getTdrCodCompAnia() {
        return this.tdrCodCompAnia;
    }

    public void setTdrTpTit(String tdrTpTit) {
        this.tdrTpTit = Functions.subString(tdrTpTit, Len.TDR_TP_TIT);
    }

    public String getTdrTpTit() {
        return this.tdrTpTit;
    }

    public void setTdrTpPreTit(String tdrTpPreTit) {
        this.tdrTpPreTit = Functions.subString(tdrTpPreTit, Len.TDR_TP_PRE_TIT);
    }

    public String getTdrTpPreTit() {
        return this.tdrTpPreTit;
    }

    public void setTdrTpStatTit(String tdrTpStatTit) {
        this.tdrTpStatTit = Functions.subString(tdrTpStatTit, Len.TDR_TP_STAT_TIT);
    }

    public String getTdrTpStatTit() {
        return this.tdrTpStatTit;
    }

    public void setTdrFlSoll(char tdrFlSoll) {
        this.tdrFlSoll = tdrFlSoll;
    }

    public char getTdrFlSoll() {
        return this.tdrFlSoll;
    }

    public void setTdrFlMora(char tdrFlMora) {
        this.tdrFlMora = tdrFlMora;
    }

    public char getTdrFlMora() {
        return this.tdrFlMora;
    }

    public void setTdrCodDvs(String tdrCodDvs) {
        this.tdrCodDvs = Functions.subString(tdrCodDvs, Len.TDR_COD_DVS);
    }

    public String getTdrCodDvs() {
        return this.tdrCodDvs;
    }

    public String getTdrCodDvsFormatted() {
        return Functions.padBlanks(getTdrCodDvs(), Len.TDR_COD_DVS);
    }

    public void setTdrDtEmisTit(int tdrDtEmisTit) {
        this.tdrDtEmisTit = tdrDtEmisTit;
    }

    public int getTdrDtEmisTit() {
        return this.tdrDtEmisTit;
    }

    public void setTdrFlVldtTit(char tdrFlVldtTit) {
        this.tdrFlVldtTit = tdrFlVldtTit;
    }

    public char getTdrFlVldtTit() {
        return this.tdrFlVldtTit;
    }

    public void setTdrDsRiga(long tdrDsRiga) {
        this.tdrDsRiga = tdrDsRiga;
    }

    public long getTdrDsRiga() {
        return this.tdrDsRiga;
    }

    public void setTdrDsOperSql(char tdrDsOperSql) {
        this.tdrDsOperSql = tdrDsOperSql;
    }

    public char getTdrDsOperSql() {
        return this.tdrDsOperSql;
    }

    public void setTdrDsVer(int tdrDsVer) {
        this.tdrDsVer = tdrDsVer;
    }

    public int getTdrDsVer() {
        return this.tdrDsVer;
    }

    public void setTdrDsTsIniCptz(long tdrDsTsIniCptz) {
        this.tdrDsTsIniCptz = tdrDsTsIniCptz;
    }

    public long getTdrDsTsIniCptz() {
        return this.tdrDsTsIniCptz;
    }

    public void setTdrDsTsEndCptz(long tdrDsTsEndCptz) {
        this.tdrDsTsEndCptz = tdrDsTsEndCptz;
    }

    public long getTdrDsTsEndCptz() {
        return this.tdrDsTsEndCptz;
    }

    public void setTdrDsUtente(String tdrDsUtente) {
        this.tdrDsUtente = Functions.subString(tdrDsUtente, Len.TDR_DS_UTENTE);
    }

    public String getTdrDsUtente() {
        return this.tdrDsUtente;
    }

    public void setTdrDsStatoElab(char tdrDsStatoElab) {
        this.tdrDsStatoElab = tdrDsStatoElab;
    }

    public char getTdrDsStatoElab() {
        return this.tdrDsStatoElab;
    }

    public void setTdrFlIncAutogen(char tdrFlIncAutogen) {
        this.tdrFlIncAutogen = tdrFlIncAutogen;
    }

    public char getTdrFlIncAutogen() {
        return this.tdrFlIncAutogen;
    }

    public TdrDtApplzMora getTdrDtApplzMora() {
        return tdrDtApplzMora;
    }

    public TdrDtEndCop getTdrDtEndCop() {
        return tdrDtEndCop;
    }

    public TdrDtEsiTit getTdrDtEsiTit() {
        return tdrDtEsiTit;
    }

    public TdrDtIniCop getTdrDtIniCop() {
        return tdrDtIniCop;
    }

    public TdrFraz getTdrFraz() {
        return tdrFraz;
    }

    public TdrIdMoviChiu getTdrIdMoviChiu() {
        return tdrIdMoviChiu;
    }

    public TdrIdRappAna getTdrIdRappAna() {
        return tdrIdRappAna;
    }

    public TdrIdRappRete getTdrIdRappRete() {
        return tdrIdRappRete;
    }

    public TdrImpAder getTdrImpAder() {
        return tdrImpAder;
    }

    public TdrImpAz getTdrImpAz() {
        return tdrImpAz;
    }

    public TdrImpPag getTdrImpPag() {
        return tdrImpPag;
    }

    public TdrImpTfr getTdrImpTfr() {
        return tdrImpTfr;
    }

    public TdrImpTfrStrc getTdrImpTfrStrc() {
        return tdrImpTfrStrc;
    }

    public TdrImpTrasfe getTdrImpTrasfe() {
        return tdrImpTrasfe;
    }

    public TdrImpVolo getTdrImpVolo() {
        return tdrImpVolo;
    }

    public TdrProgTit getTdrProgTit() {
        return tdrProgTit;
    }

    public TdrTotAcqExp getTdrTotAcqExp() {
        return tdrTotAcqExp;
    }

    public TdrTotCarAcq getTdrTotCarAcq() {
        return tdrTotCarAcq;
    }

    public TdrTotCarGest getTdrTotCarGest() {
        return tdrTotCarGest;
    }

    public TdrTotCarIas getTdrTotCarIas() {
        return tdrTotCarIas;
    }

    public TdrTotCarInc getTdrTotCarInc() {
        return tdrTotCarInc;
    }

    public TdrTotCnbtAntirac getTdrTotCnbtAntirac() {
        return tdrTotCnbtAntirac;
    }

    public TdrTotCommisInter getTdrTotCommisInter() {
        return tdrTotCommisInter;
    }

    public TdrTotDir getTdrTotDir() {
        return tdrTotDir;
    }

    public TdrTotIntrFraz getTdrTotIntrFraz() {
        return tdrTotIntrFraz;
    }

    public TdrTotIntrMora getTdrTotIntrMora() {
        return tdrTotIntrMora;
    }

    public TdrTotIntrPrest getTdrTotIntrPrest() {
        return tdrTotIntrPrest;
    }

    public TdrTotIntrRetdt getTdrTotIntrRetdt() {
        return tdrTotIntrRetdt;
    }

    public TdrTotIntrRiat getTdrTotIntrRiat() {
        return tdrTotIntrRiat;
    }

    public TdrTotManfeeAntic getTdrTotManfeeAntic() {
        return tdrTotManfeeAntic;
    }

    public TdrTotManfeeRec getTdrTotManfeeRec() {
        return tdrTotManfeeRec;
    }

    public TdrTotManfeeRicor getTdrTotManfeeRicor() {
        return tdrTotManfeeRicor;
    }

    public TdrTotPreNet getTdrTotPreNet() {
        return tdrTotPreNet;
    }

    public TdrTotPrePpIas getTdrTotPrePpIas() {
        return tdrTotPrePpIas;
    }

    public TdrTotPreSoloRsh getTdrTotPreSoloRsh() {
        return tdrTotPreSoloRsh;
    }

    public TdrTotPreTot getTdrTotPreTot() {
        return tdrTotPreTot;
    }

    public TdrTotProvAcq1aa getTdrTotProvAcq1aa() {
        return tdrTotProvAcq1aa;
    }

    public TdrTotProvAcq2aa getTdrTotProvAcq2aa() {
        return tdrTotProvAcq2aa;
    }

    public TdrTotProvDaRec getTdrTotProvDaRec() {
        return tdrTotProvDaRec;
    }

    public TdrTotProvInc getTdrTotProvInc() {
        return tdrTotProvInc;
    }

    public TdrTotProvRicor getTdrTotProvRicor() {
        return tdrTotProvRicor;
    }

    public TdrTotRemunAss getTdrTotRemunAss() {
        return tdrTotRemunAss;
    }

    public TdrTotSoprAlt getTdrTotSoprAlt() {
        return tdrTotSoprAlt;
    }

    public TdrTotSoprProf getTdrTotSoprProf() {
        return tdrTotSoprProf;
    }

    public TdrTotSoprSan getTdrTotSoprSan() {
        return tdrTotSoprSan;
    }

    public TdrTotSoprSpo getTdrTotSoprSpo() {
        return tdrTotSoprSpo;
    }

    public TdrTotSoprTec getTdrTotSoprTec() {
        return tdrTotSoprTec;
    }

    public TdrTotSpeAge getTdrTotSpeAge() {
        return tdrTotSpeAge;
    }

    public TdrTotSpeMed getTdrTotSpeMed() {
        return tdrTotSpeMed;
    }

    public TdrTotTax getTdrTotTax() {
        return tdrTotTax;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TP_OGG = 2;
        public static final int TDR_TP_TIT = 2;
        public static final int TDR_TP_PRE_TIT = 2;
        public static final int TDR_TP_STAT_TIT = 2;
        public static final int TDR_COD_DVS = 20;
        public static final int TDR_DS_UTENTE = 20;
        public static final int TDR_ID_TIT_RAT = 5;
        public static final int TDR_ID_OGG = 5;
        public static final int TDR_ID_MOVI_CRZ = 5;
        public static final int TDR_DT_INI_EFF = 5;
        public static final int TDR_DT_END_EFF = 5;
        public static final int TDR_COD_COMP_ANIA = 3;
        public static final int TDR_FL_SOLL = 1;
        public static final int TDR_FL_MORA = 1;
        public static final int TDR_DT_EMIS_TIT = 5;
        public static final int TDR_FL_VLDT_TIT = 1;
        public static final int TDR_DS_RIGA = 6;
        public static final int TDR_DS_OPER_SQL = 1;
        public static final int TDR_DS_VER = 5;
        public static final int TDR_DS_TS_INI_CPTZ = 10;
        public static final int TDR_DS_TS_END_CPTZ = 10;
        public static final int TDR_DS_STATO_ELAB = 1;
        public static final int TDR_FL_INC_AUTOGEN = 1;
        public static final int TIT_RAT = TDR_ID_TIT_RAT + TDR_ID_OGG + TDR_TP_OGG + TDR_ID_MOVI_CRZ + TdrIdMoviChiu.Len.TDR_ID_MOVI_CHIU + TDR_DT_INI_EFF + TDR_DT_END_EFF + TDR_COD_COMP_ANIA + TDR_TP_TIT + TdrProgTit.Len.TDR_PROG_TIT + TDR_TP_PRE_TIT + TDR_TP_STAT_TIT + TdrDtIniCop.Len.TDR_DT_INI_COP + TdrDtEndCop.Len.TDR_DT_END_COP + TdrImpPag.Len.TDR_IMP_PAG + TDR_FL_SOLL + TdrFraz.Len.TDR_FRAZ + TdrDtApplzMora.Len.TDR_DT_APPLZ_MORA + TDR_FL_MORA + TdrIdRappRete.Len.TDR_ID_RAPP_RETE + TdrIdRappAna.Len.TDR_ID_RAPP_ANA + TDR_COD_DVS + TDR_DT_EMIS_TIT + TdrDtEsiTit.Len.TDR_DT_ESI_TIT + TdrTotPreNet.Len.TDR_TOT_PRE_NET + TdrTotIntrFraz.Len.TDR_TOT_INTR_FRAZ + TdrTotIntrMora.Len.TDR_TOT_INTR_MORA + TdrTotIntrPrest.Len.TDR_TOT_INTR_PREST + TdrTotIntrRetdt.Len.TDR_TOT_INTR_RETDT + TdrTotIntrRiat.Len.TDR_TOT_INTR_RIAT + TdrTotDir.Len.TDR_TOT_DIR + TdrTotSpeMed.Len.TDR_TOT_SPE_MED + TdrTotSpeAge.Len.TDR_TOT_SPE_AGE + TdrTotTax.Len.TDR_TOT_TAX + TdrTotSoprSan.Len.TDR_TOT_SOPR_SAN + TdrTotSoprTec.Len.TDR_TOT_SOPR_TEC + TdrTotSoprSpo.Len.TDR_TOT_SOPR_SPO + TdrTotSoprProf.Len.TDR_TOT_SOPR_PROF + TdrTotSoprAlt.Len.TDR_TOT_SOPR_ALT + TdrTotPreTot.Len.TDR_TOT_PRE_TOT + TdrTotPrePpIas.Len.TDR_TOT_PRE_PP_IAS + TdrTotCarIas.Len.TDR_TOT_CAR_IAS + TdrTotPreSoloRsh.Len.TDR_TOT_PRE_SOLO_RSH + TdrTotProvAcq1aa.Len.TDR_TOT_PROV_ACQ1AA + TdrTotProvAcq2aa.Len.TDR_TOT_PROV_ACQ2AA + TdrTotProvRicor.Len.TDR_TOT_PROV_RICOR + TdrTotProvInc.Len.TDR_TOT_PROV_INC + TdrTotProvDaRec.Len.TDR_TOT_PROV_DA_REC + TdrImpAz.Len.TDR_IMP_AZ + TdrImpAder.Len.TDR_IMP_ADER + TdrImpTfr.Len.TDR_IMP_TFR + TdrImpVolo.Len.TDR_IMP_VOLO + TDR_FL_VLDT_TIT + TdrTotCarAcq.Len.TDR_TOT_CAR_ACQ + TdrTotCarGest.Len.TDR_TOT_CAR_GEST + TdrTotCarInc.Len.TDR_TOT_CAR_INC + TdrTotManfeeAntic.Len.TDR_TOT_MANFEE_ANTIC + TdrTotManfeeRicor.Len.TDR_TOT_MANFEE_RICOR + TdrTotManfeeRec.Len.TDR_TOT_MANFEE_REC + TDR_DS_RIGA + TDR_DS_OPER_SQL + TDR_DS_VER + TDR_DS_TS_INI_CPTZ + TDR_DS_TS_END_CPTZ + TDR_DS_UTENTE + TDR_DS_STATO_ELAB + TdrImpTrasfe.Len.TDR_IMP_TRASFE + TdrImpTfrStrc.Len.TDR_IMP_TFR_STRC + TdrTotAcqExp.Len.TDR_TOT_ACQ_EXP + TdrTotRemunAss.Len.TDR_TOT_REMUN_ASS + TdrTotCommisInter.Len.TDR_TOT_COMMIS_INTER + TdrTotCnbtAntirac.Len.TDR_TOT_CNBT_ANTIRAC + TDR_FL_INC_AUTOGEN;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_ID_TIT_RAT = 9;
            public static final int TDR_ID_OGG = 9;
            public static final int TDR_ID_MOVI_CRZ = 9;
            public static final int TDR_DT_INI_EFF = 8;
            public static final int TDR_DT_END_EFF = 8;
            public static final int TDR_COD_COMP_ANIA = 5;
            public static final int TDR_DT_EMIS_TIT = 8;
            public static final int TDR_DS_RIGA = 10;
            public static final int TDR_DS_VER = 9;
            public static final int TDR_DS_TS_INI_CPTZ = 18;
            public static final int TDR_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
