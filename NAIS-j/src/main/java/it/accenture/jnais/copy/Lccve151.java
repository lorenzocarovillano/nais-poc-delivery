package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVE151<br>
 * Variable: LCCVE151 from copybook LCCVE151<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccve151 {

    //==== PROPERTIES ====
    /**Original name: WE15-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA EST_RAPP_ANA
	 *    ALIAS E15
	 *    ULTIMO AGG. 04 MAR 2020
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WE15-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WE15-DATI
    private We15Dati dati = new We15Dati();

    //==== METHODS ====
    public void initLccve151Spaces() {
        status.setStatus(Types.SPACE_CHAR);
        idPtf = Types.INVALID_INT_VAL;
        dati.initDatiSpaces();
    }

    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public We15Dati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
