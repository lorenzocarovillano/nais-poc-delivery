package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.We15IdMoviChiu;
import it.accenture.jnais.ws.redefines.We15IdRappAnaCollg;
import it.accenture.jnais.ws.redefines.We15IdSegmentazCli;

/**Original name: WE15-DATI<br>
 * Variable: WE15-DATI from copybook LCCVE151<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class We15Dati {

    //==== PROPERTIES ====
    //Original name: WE15-ID-EST-RAPP-ANA
    private int we15IdEstRappAna = DefaultValues.INT_VAL;
    //Original name: WE15-ID-RAPP-ANA
    private int we15IdRappAna = DefaultValues.INT_VAL;
    //Original name: WE15-ID-RAPP-ANA-COLLG
    private We15IdRappAnaCollg we15IdRappAnaCollg = new We15IdRappAnaCollg();
    //Original name: WE15-ID-OGG
    private int we15IdOgg = DefaultValues.INT_VAL;
    //Original name: WE15-TP-OGG
    private String we15TpOgg = DefaultValues.stringVal(Len.WE15_TP_OGG);
    //Original name: WE15-ID-MOVI-CRZ
    private int we15IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WE15-ID-MOVI-CHIU
    private We15IdMoviChiu we15IdMoviChiu = new We15IdMoviChiu();
    //Original name: WE15-DT-INI-EFF
    private int we15DtIniEff = DefaultValues.INT_VAL;
    //Original name: WE15-DT-END-EFF
    private int we15DtEndEff = DefaultValues.INT_VAL;
    //Original name: WE15-COD-COMP-ANIA
    private int we15CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WE15-COD-SOGG
    private String we15CodSogg = DefaultValues.stringVal(Len.WE15_COD_SOGG);
    //Original name: WE15-TP-RAPP-ANA
    private String we15TpRappAna = DefaultValues.stringVal(Len.WE15_TP_RAPP_ANA);
    //Original name: WE15-DS-RIGA
    private long we15DsRiga = DefaultValues.LONG_VAL;
    //Original name: WE15-DS-OPER-SQL
    private char we15DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WE15-DS-VER
    private int we15DsVer = DefaultValues.INT_VAL;
    //Original name: WE15-DS-TS-INI-CPTZ
    private long we15DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WE15-DS-TS-END-CPTZ
    private long we15DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WE15-DS-UTENTE
    private String we15DsUtente = DefaultValues.stringVal(Len.WE15_DS_UTENTE);
    //Original name: WE15-DS-STATO-ELAB
    private char we15DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WE15-ID-SEGMENTAZ-CLI
    private We15IdSegmentazCli we15IdSegmentazCli = new We15IdSegmentazCli();
    //Original name: WE15-FL-COINC-TIT-EFF
    private char we15FlCoincTitEff = DefaultValues.CHAR_VAL;
    //Original name: WE15-FL-PERS-ESP-POL
    private char we15FlPersEspPol = DefaultValues.CHAR_VAL;
    //Original name: WE15-DESC-PERS-ESP-POL
    private String we15DescPersEspPol = DefaultValues.stringVal(Len.WE15_DESC_PERS_ESP_POL);
    //Original name: WE15-TP-LEG-CNTR
    private String we15TpLegCntr = DefaultValues.stringVal(Len.WE15_TP_LEG_CNTR);
    //Original name: WE15-DESC-LEG-CNTR
    private String we15DescLegCntr = DefaultValues.stringVal(Len.WE15_DESC_LEG_CNTR);
    //Original name: WE15-TP-LEG-PERC-BNFICR
    private String we15TpLegPercBnficr = DefaultValues.stringVal(Len.WE15_TP_LEG_PERC_BNFICR);
    //Original name: WE15-D-LEG-PERC-BNFICR
    private String we15DLegPercBnficr = DefaultValues.stringVal(Len.WE15_D_LEG_PERC_BNFICR);
    //Original name: WE15-TP-CNT-CORR
    private String we15TpCntCorr = DefaultValues.stringVal(Len.WE15_TP_CNT_CORR);
    //Original name: WE15-TP-COINC-PIC-PAC
    private String we15TpCoincPicPac = DefaultValues.stringVal(Len.WE15_TP_COINC_PIC_PAC);

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        we15IdEstRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WE15_ID_EST_RAPP_ANA, 0);
        position += Len.WE15_ID_EST_RAPP_ANA;
        we15IdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WE15_ID_RAPP_ANA, 0);
        position += Len.WE15_ID_RAPP_ANA;
        we15IdRappAnaCollg.setWe15IdRappAnaCollgFromBuffer(buffer, position);
        position += We15IdRappAnaCollg.Len.WE15_ID_RAPP_ANA_COLLG;
        we15IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WE15_ID_OGG, 0);
        position += Len.WE15_ID_OGG;
        we15TpOgg = MarshalByte.readString(buffer, position, Len.WE15_TP_OGG);
        position += Len.WE15_TP_OGG;
        we15IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WE15_ID_MOVI_CRZ, 0);
        position += Len.WE15_ID_MOVI_CRZ;
        we15IdMoviChiu.setWe15IdMoviChiuFromBuffer(buffer, position);
        position += We15IdMoviChiu.Len.WE15_ID_MOVI_CHIU;
        we15DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WE15_DT_INI_EFF, 0);
        position += Len.WE15_DT_INI_EFF;
        we15DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WE15_DT_END_EFF, 0);
        position += Len.WE15_DT_END_EFF;
        we15CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WE15_COD_COMP_ANIA, 0);
        position += Len.WE15_COD_COMP_ANIA;
        we15CodSogg = MarshalByte.readString(buffer, position, Len.WE15_COD_SOGG);
        position += Len.WE15_COD_SOGG;
        we15TpRappAna = MarshalByte.readString(buffer, position, Len.WE15_TP_RAPP_ANA);
        position += Len.WE15_TP_RAPP_ANA;
        we15DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WE15_DS_RIGA, 0);
        position += Len.WE15_DS_RIGA;
        we15DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        we15DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WE15_DS_VER, 0);
        position += Len.WE15_DS_VER;
        we15DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WE15_DS_TS_INI_CPTZ, 0);
        position += Len.WE15_DS_TS_INI_CPTZ;
        we15DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WE15_DS_TS_END_CPTZ, 0);
        position += Len.WE15_DS_TS_END_CPTZ;
        we15DsUtente = MarshalByte.readString(buffer, position, Len.WE15_DS_UTENTE);
        position += Len.WE15_DS_UTENTE;
        we15DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        we15IdSegmentazCli.setWe15IdSegmentazCliFromBuffer(buffer, position);
        position += We15IdSegmentazCli.Len.WE15_ID_SEGMENTAZ_CLI;
        we15FlCoincTitEff = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        we15FlPersEspPol = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        we15DescPersEspPol = MarshalByte.readString(buffer, position, Len.WE15_DESC_PERS_ESP_POL);
        position += Len.WE15_DESC_PERS_ESP_POL;
        we15TpLegCntr = MarshalByte.readString(buffer, position, Len.WE15_TP_LEG_CNTR);
        position += Len.WE15_TP_LEG_CNTR;
        we15DescLegCntr = MarshalByte.readString(buffer, position, Len.WE15_DESC_LEG_CNTR);
        position += Len.WE15_DESC_LEG_CNTR;
        we15TpLegPercBnficr = MarshalByte.readString(buffer, position, Len.WE15_TP_LEG_PERC_BNFICR);
        position += Len.WE15_TP_LEG_PERC_BNFICR;
        we15DLegPercBnficr = MarshalByte.readString(buffer, position, Len.WE15_D_LEG_PERC_BNFICR);
        position += Len.WE15_D_LEG_PERC_BNFICR;
        we15TpCntCorr = MarshalByte.readString(buffer, position, Len.WE15_TP_CNT_CORR);
        position += Len.WE15_TP_CNT_CORR;
        we15TpCoincPicPac = MarshalByte.readString(buffer, position, Len.WE15_TP_COINC_PIC_PAC);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, we15IdEstRappAna, Len.Int.WE15_ID_EST_RAPP_ANA, 0);
        position += Len.WE15_ID_EST_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, we15IdRappAna, Len.Int.WE15_ID_RAPP_ANA, 0);
        position += Len.WE15_ID_RAPP_ANA;
        we15IdRappAnaCollg.getWe15IdRappAnaCollgAsBuffer(buffer, position);
        position += We15IdRappAnaCollg.Len.WE15_ID_RAPP_ANA_COLLG;
        MarshalByte.writeIntAsPacked(buffer, position, we15IdOgg, Len.Int.WE15_ID_OGG, 0);
        position += Len.WE15_ID_OGG;
        MarshalByte.writeString(buffer, position, we15TpOgg, Len.WE15_TP_OGG);
        position += Len.WE15_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, we15IdMoviCrz, Len.Int.WE15_ID_MOVI_CRZ, 0);
        position += Len.WE15_ID_MOVI_CRZ;
        we15IdMoviChiu.getWe15IdMoviChiuAsBuffer(buffer, position);
        position += We15IdMoviChiu.Len.WE15_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, we15DtIniEff, Len.Int.WE15_DT_INI_EFF, 0);
        position += Len.WE15_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, we15DtEndEff, Len.Int.WE15_DT_END_EFF, 0);
        position += Len.WE15_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, we15CodCompAnia, Len.Int.WE15_COD_COMP_ANIA, 0);
        position += Len.WE15_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, we15CodSogg, Len.WE15_COD_SOGG);
        position += Len.WE15_COD_SOGG;
        MarshalByte.writeString(buffer, position, we15TpRappAna, Len.WE15_TP_RAPP_ANA);
        position += Len.WE15_TP_RAPP_ANA;
        MarshalByte.writeLongAsPacked(buffer, position, we15DsRiga, Len.Int.WE15_DS_RIGA, 0);
        position += Len.WE15_DS_RIGA;
        MarshalByte.writeChar(buffer, position, we15DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, we15DsVer, Len.Int.WE15_DS_VER, 0);
        position += Len.WE15_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, we15DsTsIniCptz, Len.Int.WE15_DS_TS_INI_CPTZ, 0);
        position += Len.WE15_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, we15DsTsEndCptz, Len.Int.WE15_DS_TS_END_CPTZ, 0);
        position += Len.WE15_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, we15DsUtente, Len.WE15_DS_UTENTE);
        position += Len.WE15_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, we15DsStatoElab);
        position += Types.CHAR_SIZE;
        we15IdSegmentazCli.getWe15IdSegmentazCliAsBuffer(buffer, position);
        position += We15IdSegmentazCli.Len.WE15_ID_SEGMENTAZ_CLI;
        MarshalByte.writeChar(buffer, position, we15FlCoincTitEff);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, we15FlPersEspPol);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, we15DescPersEspPol, Len.WE15_DESC_PERS_ESP_POL);
        position += Len.WE15_DESC_PERS_ESP_POL;
        MarshalByte.writeString(buffer, position, we15TpLegCntr, Len.WE15_TP_LEG_CNTR);
        position += Len.WE15_TP_LEG_CNTR;
        MarshalByte.writeString(buffer, position, we15DescLegCntr, Len.WE15_DESC_LEG_CNTR);
        position += Len.WE15_DESC_LEG_CNTR;
        MarshalByte.writeString(buffer, position, we15TpLegPercBnficr, Len.WE15_TP_LEG_PERC_BNFICR);
        position += Len.WE15_TP_LEG_PERC_BNFICR;
        MarshalByte.writeString(buffer, position, we15DLegPercBnficr, Len.WE15_D_LEG_PERC_BNFICR);
        position += Len.WE15_D_LEG_PERC_BNFICR;
        MarshalByte.writeString(buffer, position, we15TpCntCorr, Len.WE15_TP_CNT_CORR);
        position += Len.WE15_TP_CNT_CORR;
        MarshalByte.writeString(buffer, position, we15TpCoincPicPac, Len.WE15_TP_COINC_PIC_PAC);
        return buffer;
    }

    public void initDatiSpaces() {
        we15IdEstRappAna = Types.INVALID_INT_VAL;
        we15IdRappAna = Types.INVALID_INT_VAL;
        we15IdRappAnaCollg.initWe15IdRappAnaCollgSpaces();
        we15IdOgg = Types.INVALID_INT_VAL;
        we15TpOgg = "";
        we15IdMoviCrz = Types.INVALID_INT_VAL;
        we15IdMoviChiu.initWe15IdMoviChiuSpaces();
        we15DtIniEff = Types.INVALID_INT_VAL;
        we15DtEndEff = Types.INVALID_INT_VAL;
        we15CodCompAnia = Types.INVALID_INT_VAL;
        we15CodSogg = "";
        we15TpRappAna = "";
        we15DsRiga = Types.INVALID_LONG_VAL;
        we15DsOperSql = Types.SPACE_CHAR;
        we15DsVer = Types.INVALID_INT_VAL;
        we15DsTsIniCptz = Types.INVALID_LONG_VAL;
        we15DsTsEndCptz = Types.INVALID_LONG_VAL;
        we15DsUtente = "";
        we15DsStatoElab = Types.SPACE_CHAR;
        we15IdSegmentazCli.initWe15IdSegmentazCliSpaces();
        we15FlCoincTitEff = Types.SPACE_CHAR;
        we15FlPersEspPol = Types.SPACE_CHAR;
        we15DescPersEspPol = "";
        we15TpLegCntr = "";
        we15DescLegCntr = "";
        we15TpLegPercBnficr = "";
        we15DLegPercBnficr = "";
        we15TpCntCorr = "";
        we15TpCoincPicPac = "";
    }

    public void setWe15IdEstRappAna(int we15IdEstRappAna) {
        this.we15IdEstRappAna = we15IdEstRappAna;
    }

    public int getWe15IdEstRappAna() {
        return this.we15IdEstRappAna;
    }

    public void setWe15IdRappAna(int we15IdRappAna) {
        this.we15IdRappAna = we15IdRappAna;
    }

    public int getWe15IdRappAna() {
        return this.we15IdRappAna;
    }

    public void setWe15IdOgg(int we15IdOgg) {
        this.we15IdOgg = we15IdOgg;
    }

    public int getWe15IdOgg() {
        return this.we15IdOgg;
    }

    public void setWe15TpOgg(String we15TpOgg) {
        this.we15TpOgg = Functions.subString(we15TpOgg, Len.WE15_TP_OGG);
    }

    public String getWe15TpOgg() {
        return this.we15TpOgg;
    }

    public void setWe15IdMoviCrz(int we15IdMoviCrz) {
        this.we15IdMoviCrz = we15IdMoviCrz;
    }

    public int getWe15IdMoviCrz() {
        return this.we15IdMoviCrz;
    }

    public void setWe15DtIniEff(int we15DtIniEff) {
        this.we15DtIniEff = we15DtIniEff;
    }

    public int getWe15DtIniEff() {
        return this.we15DtIniEff;
    }

    public void setWe15DtEndEff(int we15DtEndEff) {
        this.we15DtEndEff = we15DtEndEff;
    }

    public int getWe15DtEndEff() {
        return this.we15DtEndEff;
    }

    public void setWe15CodCompAnia(int we15CodCompAnia) {
        this.we15CodCompAnia = we15CodCompAnia;
    }

    public int getWe15CodCompAnia() {
        return this.we15CodCompAnia;
    }

    public void setWe15CodSogg(String we15CodSogg) {
        this.we15CodSogg = Functions.subString(we15CodSogg, Len.WE15_COD_SOGG);
    }

    public String getWe15CodSogg() {
        return this.we15CodSogg;
    }

    public void setWe15TpRappAna(String we15TpRappAna) {
        this.we15TpRappAna = Functions.subString(we15TpRappAna, Len.WE15_TP_RAPP_ANA);
    }

    public String getWe15TpRappAna() {
        return this.we15TpRappAna;
    }

    public void setWe15DsRiga(long we15DsRiga) {
        this.we15DsRiga = we15DsRiga;
    }

    public long getWe15DsRiga() {
        return this.we15DsRiga;
    }

    public void setWe15DsOperSql(char we15DsOperSql) {
        this.we15DsOperSql = we15DsOperSql;
    }

    public char getWe15DsOperSql() {
        return this.we15DsOperSql;
    }

    public void setWe15DsVer(int we15DsVer) {
        this.we15DsVer = we15DsVer;
    }

    public int getWe15DsVer() {
        return this.we15DsVer;
    }

    public void setWe15DsTsIniCptz(long we15DsTsIniCptz) {
        this.we15DsTsIniCptz = we15DsTsIniCptz;
    }

    public long getWe15DsTsIniCptz() {
        return this.we15DsTsIniCptz;
    }

    public void setWe15DsTsEndCptz(long we15DsTsEndCptz) {
        this.we15DsTsEndCptz = we15DsTsEndCptz;
    }

    public long getWe15DsTsEndCptz() {
        return this.we15DsTsEndCptz;
    }

    public void setWe15DsUtente(String we15DsUtente) {
        this.we15DsUtente = Functions.subString(we15DsUtente, Len.WE15_DS_UTENTE);
    }

    public String getWe15DsUtente() {
        return this.we15DsUtente;
    }

    public void setWe15DsStatoElab(char we15DsStatoElab) {
        this.we15DsStatoElab = we15DsStatoElab;
    }

    public char getWe15DsStatoElab() {
        return this.we15DsStatoElab;
    }

    public void setWe15FlCoincTitEff(char we15FlCoincTitEff) {
        this.we15FlCoincTitEff = we15FlCoincTitEff;
    }

    public char getWe15FlCoincTitEff() {
        return this.we15FlCoincTitEff;
    }

    public void setWe15FlPersEspPol(char we15FlPersEspPol) {
        this.we15FlPersEspPol = we15FlPersEspPol;
    }

    public char getWe15FlPersEspPol() {
        return this.we15FlPersEspPol;
    }

    public void setWe15DescPersEspPol(String we15DescPersEspPol) {
        this.we15DescPersEspPol = Functions.subString(we15DescPersEspPol, Len.WE15_DESC_PERS_ESP_POL);
    }

    public String getWe15DescPersEspPol() {
        return this.we15DescPersEspPol;
    }

    public void setWe15TpLegCntr(String we15TpLegCntr) {
        this.we15TpLegCntr = Functions.subString(we15TpLegCntr, Len.WE15_TP_LEG_CNTR);
    }

    public String getWe15TpLegCntr() {
        return this.we15TpLegCntr;
    }

    public void setWe15DescLegCntr(String we15DescLegCntr) {
        this.we15DescLegCntr = Functions.subString(we15DescLegCntr, Len.WE15_DESC_LEG_CNTR);
    }

    public String getWe15DescLegCntr() {
        return this.we15DescLegCntr;
    }

    public void setWe15TpLegPercBnficr(String we15TpLegPercBnficr) {
        this.we15TpLegPercBnficr = Functions.subString(we15TpLegPercBnficr, Len.WE15_TP_LEG_PERC_BNFICR);
    }

    public String getWe15TpLegPercBnficr() {
        return this.we15TpLegPercBnficr;
    }

    public void setWe15DLegPercBnficr(String we15DLegPercBnficr) {
        this.we15DLegPercBnficr = Functions.subString(we15DLegPercBnficr, Len.WE15_D_LEG_PERC_BNFICR);
    }

    public String getWe15DLegPercBnficr() {
        return this.we15DLegPercBnficr;
    }

    public void setWe15TpCntCorr(String we15TpCntCorr) {
        this.we15TpCntCorr = Functions.subString(we15TpCntCorr, Len.WE15_TP_CNT_CORR);
    }

    public String getWe15TpCntCorr() {
        return this.we15TpCntCorr;
    }

    public void setWe15TpCoincPicPac(String we15TpCoincPicPac) {
        this.we15TpCoincPicPac = Functions.subString(we15TpCoincPicPac, Len.WE15_TP_COINC_PIC_PAC);
    }

    public String getWe15TpCoincPicPac() {
        return this.we15TpCoincPicPac;
    }

    public We15IdMoviChiu getWe15IdMoviChiu() {
        return we15IdMoviChiu;
    }

    public We15IdRappAnaCollg getWe15IdRappAnaCollg() {
        return we15IdRappAnaCollg;
    }

    public We15IdSegmentazCli getWe15IdSegmentazCli() {
        return we15IdSegmentazCli;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WE15_ID_EST_RAPP_ANA = 5;
        public static final int WE15_ID_RAPP_ANA = 5;
        public static final int WE15_ID_OGG = 5;
        public static final int WE15_TP_OGG = 2;
        public static final int WE15_ID_MOVI_CRZ = 5;
        public static final int WE15_DT_INI_EFF = 5;
        public static final int WE15_DT_END_EFF = 5;
        public static final int WE15_COD_COMP_ANIA = 3;
        public static final int WE15_COD_SOGG = 20;
        public static final int WE15_TP_RAPP_ANA = 2;
        public static final int WE15_DS_RIGA = 6;
        public static final int WE15_DS_OPER_SQL = 1;
        public static final int WE15_DS_VER = 5;
        public static final int WE15_DS_TS_INI_CPTZ = 10;
        public static final int WE15_DS_TS_END_CPTZ = 10;
        public static final int WE15_DS_UTENTE = 20;
        public static final int WE15_DS_STATO_ELAB = 1;
        public static final int WE15_FL_COINC_TIT_EFF = 1;
        public static final int WE15_FL_PERS_ESP_POL = 1;
        public static final int WE15_DESC_PERS_ESP_POL = 250;
        public static final int WE15_TP_LEG_CNTR = 2;
        public static final int WE15_DESC_LEG_CNTR = 250;
        public static final int WE15_TP_LEG_PERC_BNFICR = 2;
        public static final int WE15_D_LEG_PERC_BNFICR = 250;
        public static final int WE15_TP_CNT_CORR = 2;
        public static final int WE15_TP_COINC_PIC_PAC = 2;
        public static final int DATI = WE15_ID_EST_RAPP_ANA + WE15_ID_RAPP_ANA + We15IdRappAnaCollg.Len.WE15_ID_RAPP_ANA_COLLG + WE15_ID_OGG + WE15_TP_OGG + WE15_ID_MOVI_CRZ + We15IdMoviChiu.Len.WE15_ID_MOVI_CHIU + WE15_DT_INI_EFF + WE15_DT_END_EFF + WE15_COD_COMP_ANIA + WE15_COD_SOGG + WE15_TP_RAPP_ANA + WE15_DS_RIGA + WE15_DS_OPER_SQL + WE15_DS_VER + WE15_DS_TS_INI_CPTZ + WE15_DS_TS_END_CPTZ + WE15_DS_UTENTE + WE15_DS_STATO_ELAB + We15IdSegmentazCli.Len.WE15_ID_SEGMENTAZ_CLI + WE15_FL_COINC_TIT_EFF + WE15_FL_PERS_ESP_POL + WE15_DESC_PERS_ESP_POL + WE15_TP_LEG_CNTR + WE15_DESC_LEG_CNTR + WE15_TP_LEG_PERC_BNFICR + WE15_D_LEG_PERC_BNFICR + WE15_TP_CNT_CORR + WE15_TP_COINC_PIC_PAC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WE15_ID_EST_RAPP_ANA = 9;
            public static final int WE15_ID_RAPP_ANA = 9;
            public static final int WE15_ID_OGG = 9;
            public static final int WE15_ID_MOVI_CRZ = 9;
            public static final int WE15_DT_INI_EFF = 8;
            public static final int WE15_DT_END_EFF = 8;
            public static final int WE15_COD_COMP_ANIA = 5;
            public static final int WE15_DS_RIGA = 10;
            public static final int WE15_DS_VER = 9;
            public static final int WE15_DS_TS_INI_CPTZ = 18;
            public static final int WE15_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
