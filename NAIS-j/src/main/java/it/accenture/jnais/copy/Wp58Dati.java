package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Wp58IdMoviChiu;
import it.accenture.jnais.ws.redefines.Wp58ImpstBolloDettV;
import it.accenture.jnais.ws.redefines.Wp58ImpstBolloTotV;

/**Original name: WP58-DATI<br>
 * Variable: WP58-DATI from copybook LCCVP581<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wp58Dati {

    //==== PROPERTIES ====
    //Original name: WP58-ID-IMPST-BOLLO
    private int wp58IdImpstBollo = DefaultValues.INT_VAL;
    //Original name: WP58-COD-COMP-ANIA
    private int wp58CodCompAnia = DefaultValues.INT_VAL;
    //Original name: WP58-ID-POLI
    private int wp58IdPoli = DefaultValues.INT_VAL;
    //Original name: WP58-IB-POLI
    private String wp58IbPoli = DefaultValues.stringVal(Len.WP58_IB_POLI);
    //Original name: WP58-COD-FISC
    private String wp58CodFisc = DefaultValues.stringVal(Len.WP58_COD_FISC);
    //Original name: WP58-COD-PART-IVA
    private String wp58CodPartIva = DefaultValues.stringVal(Len.WP58_COD_PART_IVA);
    //Original name: WP58-ID-RAPP-ANA
    private int wp58IdRappAna = DefaultValues.INT_VAL;
    //Original name: WP58-ID-MOVI-CRZ
    private int wp58IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WP58-ID-MOVI-CHIU
    private Wp58IdMoviChiu wp58IdMoviChiu = new Wp58IdMoviChiu();
    //Original name: WP58-DT-INI-EFF
    private int wp58DtIniEff = DefaultValues.INT_VAL;
    //Original name: WP58-DT-END-EFF
    private int wp58DtEndEff = DefaultValues.INT_VAL;
    //Original name: WP58-DT-INI-CALC
    private int wp58DtIniCalc = DefaultValues.INT_VAL;
    //Original name: WP58-DT-END-CALC
    private int wp58DtEndCalc = DefaultValues.INT_VAL;
    //Original name: WP58-TP-CAUS-BOLLO
    private String wp58TpCausBollo = DefaultValues.stringVal(Len.WP58_TP_CAUS_BOLLO);
    //Original name: WP58-IMPST-BOLLO-DETT-C
    private AfDecimal wp58ImpstBolloDettC = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP58-IMPST-BOLLO-DETT-V
    private Wp58ImpstBolloDettV wp58ImpstBolloDettV = new Wp58ImpstBolloDettV();
    //Original name: WP58-IMPST-BOLLO-TOT-V
    private Wp58ImpstBolloTotV wp58ImpstBolloTotV = new Wp58ImpstBolloTotV();
    //Original name: WP58-IMPST-BOLLO-TOT-R
    private AfDecimal wp58ImpstBolloTotR = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP58-DS-RIGA
    private long wp58DsRiga = DefaultValues.LONG_VAL;
    //Original name: WP58-DS-OPER-SQL
    private char wp58DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WP58-DS-VER
    private int wp58DsVer = DefaultValues.INT_VAL;
    //Original name: WP58-DS-TS-INI-CPTZ
    private long wp58DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WP58-DS-TS-END-CPTZ
    private long wp58DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WP58-DS-UTENTE
    private String wp58DsUtente = DefaultValues.stringVal(Len.WP58_DS_UTENTE);
    //Original name: WP58-DS-STATO-ELAB
    private char wp58DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wp58IdImpstBollo = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP58_ID_IMPST_BOLLO, 0);
        position += Len.WP58_ID_IMPST_BOLLO;
        wp58CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP58_COD_COMP_ANIA, 0);
        position += Len.WP58_COD_COMP_ANIA;
        wp58IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP58_ID_POLI, 0);
        position += Len.WP58_ID_POLI;
        wp58IbPoli = MarshalByte.readString(buffer, position, Len.WP58_IB_POLI);
        position += Len.WP58_IB_POLI;
        wp58CodFisc = MarshalByte.readString(buffer, position, Len.WP58_COD_FISC);
        position += Len.WP58_COD_FISC;
        wp58CodPartIva = MarshalByte.readString(buffer, position, Len.WP58_COD_PART_IVA);
        position += Len.WP58_COD_PART_IVA;
        wp58IdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP58_ID_RAPP_ANA, 0);
        position += Len.WP58_ID_RAPP_ANA;
        wp58IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP58_ID_MOVI_CRZ, 0);
        position += Len.WP58_ID_MOVI_CRZ;
        wp58IdMoviChiu.setWp58IdMoviChiuFromBuffer(buffer, position);
        position += Wp58IdMoviChiu.Len.WP58_ID_MOVI_CHIU;
        wp58DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP58_DT_INI_EFF, 0);
        position += Len.WP58_DT_INI_EFF;
        wp58DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP58_DT_END_EFF, 0);
        position += Len.WP58_DT_END_EFF;
        wp58DtIniCalc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP58_DT_INI_CALC, 0);
        position += Len.WP58_DT_INI_CALC;
        wp58DtEndCalc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP58_DT_END_CALC, 0);
        position += Len.WP58_DT_END_CALC;
        wp58TpCausBollo = MarshalByte.readString(buffer, position, Len.WP58_TP_CAUS_BOLLO);
        position += Len.WP58_TP_CAUS_BOLLO;
        wp58ImpstBolloDettC.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WP58_IMPST_BOLLO_DETT_C, Len.Fract.WP58_IMPST_BOLLO_DETT_C));
        position += Len.WP58_IMPST_BOLLO_DETT_C;
        wp58ImpstBolloDettV.setWp58ImpstBolloDettVFromBuffer(buffer, position);
        position += Wp58ImpstBolloDettV.Len.WP58_IMPST_BOLLO_DETT_V;
        wp58ImpstBolloTotV.setWp58ImpstBolloTotVFromBuffer(buffer, position);
        position += Wp58ImpstBolloTotV.Len.WP58_IMPST_BOLLO_TOT_V;
        wp58ImpstBolloTotR.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WP58_IMPST_BOLLO_TOT_R, Len.Fract.WP58_IMPST_BOLLO_TOT_R));
        position += Len.WP58_IMPST_BOLLO_TOT_R;
        wp58DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WP58_DS_RIGA, 0);
        position += Len.WP58_DS_RIGA;
        wp58DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wp58DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WP58_DS_VER, 0);
        position += Len.WP58_DS_VER;
        wp58DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WP58_DS_TS_INI_CPTZ, 0);
        position += Len.WP58_DS_TS_INI_CPTZ;
        wp58DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WP58_DS_TS_END_CPTZ, 0);
        position += Len.WP58_DS_TS_END_CPTZ;
        wp58DsUtente = MarshalByte.readString(buffer, position, Len.WP58_DS_UTENTE);
        position += Len.WP58_DS_UTENTE;
        wp58DsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wp58IdImpstBollo, Len.Int.WP58_ID_IMPST_BOLLO, 0);
        position += Len.WP58_ID_IMPST_BOLLO;
        MarshalByte.writeIntAsPacked(buffer, position, wp58CodCompAnia, Len.Int.WP58_COD_COMP_ANIA, 0);
        position += Len.WP58_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wp58IdPoli, Len.Int.WP58_ID_POLI, 0);
        position += Len.WP58_ID_POLI;
        MarshalByte.writeString(buffer, position, wp58IbPoli, Len.WP58_IB_POLI);
        position += Len.WP58_IB_POLI;
        MarshalByte.writeString(buffer, position, wp58CodFisc, Len.WP58_COD_FISC);
        position += Len.WP58_COD_FISC;
        MarshalByte.writeString(buffer, position, wp58CodPartIva, Len.WP58_COD_PART_IVA);
        position += Len.WP58_COD_PART_IVA;
        MarshalByte.writeIntAsPacked(buffer, position, wp58IdRappAna, Len.Int.WP58_ID_RAPP_ANA, 0);
        position += Len.WP58_ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, wp58IdMoviCrz, Len.Int.WP58_ID_MOVI_CRZ, 0);
        position += Len.WP58_ID_MOVI_CRZ;
        wp58IdMoviChiu.getWp58IdMoviChiuAsBuffer(buffer, position);
        position += Wp58IdMoviChiu.Len.WP58_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wp58DtIniEff, Len.Int.WP58_DT_INI_EFF, 0);
        position += Len.WP58_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wp58DtEndEff, Len.Int.WP58_DT_END_EFF, 0);
        position += Len.WP58_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wp58DtIniCalc, Len.Int.WP58_DT_INI_CALC, 0);
        position += Len.WP58_DT_INI_CALC;
        MarshalByte.writeIntAsPacked(buffer, position, wp58DtEndCalc, Len.Int.WP58_DT_END_CALC, 0);
        position += Len.WP58_DT_END_CALC;
        MarshalByte.writeString(buffer, position, wp58TpCausBollo, Len.WP58_TP_CAUS_BOLLO);
        position += Len.WP58_TP_CAUS_BOLLO;
        MarshalByte.writeDecimalAsPacked(buffer, position, wp58ImpstBolloDettC.copy());
        position += Len.WP58_IMPST_BOLLO_DETT_C;
        wp58ImpstBolloDettV.getWp58ImpstBolloDettVAsBuffer(buffer, position);
        position += Wp58ImpstBolloDettV.Len.WP58_IMPST_BOLLO_DETT_V;
        wp58ImpstBolloTotV.getWp58ImpstBolloTotVAsBuffer(buffer, position);
        position += Wp58ImpstBolloTotV.Len.WP58_IMPST_BOLLO_TOT_V;
        MarshalByte.writeDecimalAsPacked(buffer, position, wp58ImpstBolloTotR.copy());
        position += Len.WP58_IMPST_BOLLO_TOT_R;
        MarshalByte.writeLongAsPacked(buffer, position, wp58DsRiga, Len.Int.WP58_DS_RIGA, 0);
        position += Len.WP58_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wp58DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wp58DsVer, Len.Int.WP58_DS_VER, 0);
        position += Len.WP58_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wp58DsTsIniCptz, Len.Int.WP58_DS_TS_INI_CPTZ, 0);
        position += Len.WP58_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wp58DsTsEndCptz, Len.Int.WP58_DS_TS_END_CPTZ, 0);
        position += Len.WP58_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wp58DsUtente, Len.WP58_DS_UTENTE);
        position += Len.WP58_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wp58DsStatoElab);
        return buffer;
    }

    public void initDatiSpaces() {
        wp58IdImpstBollo = Types.INVALID_INT_VAL;
        wp58CodCompAnia = Types.INVALID_INT_VAL;
        wp58IdPoli = Types.INVALID_INT_VAL;
        wp58IbPoli = "";
        wp58CodFisc = "";
        wp58CodPartIva = "";
        wp58IdRappAna = Types.INVALID_INT_VAL;
        wp58IdMoviCrz = Types.INVALID_INT_VAL;
        wp58IdMoviChiu.initWp58IdMoviChiuSpaces();
        wp58DtIniEff = Types.INVALID_INT_VAL;
        wp58DtEndEff = Types.INVALID_INT_VAL;
        wp58DtIniCalc = Types.INVALID_INT_VAL;
        wp58DtEndCalc = Types.INVALID_INT_VAL;
        wp58TpCausBollo = "";
        wp58ImpstBolloDettC.setNaN();
        wp58ImpstBolloDettV.initWp58ImpstBolloDettVSpaces();
        wp58ImpstBolloTotV.initWp58ImpstBolloTotVSpaces();
        wp58ImpstBolloTotR.setNaN();
        wp58DsRiga = Types.INVALID_LONG_VAL;
        wp58DsOperSql = Types.SPACE_CHAR;
        wp58DsVer = Types.INVALID_INT_VAL;
        wp58DsTsIniCptz = Types.INVALID_LONG_VAL;
        wp58DsTsEndCptz = Types.INVALID_LONG_VAL;
        wp58DsUtente = "";
        wp58DsStatoElab = Types.SPACE_CHAR;
    }

    public void setWp58IdImpstBollo(int wp58IdImpstBollo) {
        this.wp58IdImpstBollo = wp58IdImpstBollo;
    }

    public int getWp58IdImpstBollo() {
        return this.wp58IdImpstBollo;
    }

    public void setWp58CodCompAnia(int wp58CodCompAnia) {
        this.wp58CodCompAnia = wp58CodCompAnia;
    }

    public int getWp58CodCompAnia() {
        return this.wp58CodCompAnia;
    }

    public void setWp58IdPoli(int wp58IdPoli) {
        this.wp58IdPoli = wp58IdPoli;
    }

    public int getWp58IdPoli() {
        return this.wp58IdPoli;
    }

    public void setWp58IbPoli(String wp58IbPoli) {
        this.wp58IbPoli = Functions.subString(wp58IbPoli, Len.WP58_IB_POLI);
    }

    public String getWp58IbPoli() {
        return this.wp58IbPoli;
    }

    public void setWp58CodFisc(String wp58CodFisc) {
        this.wp58CodFisc = Functions.subString(wp58CodFisc, Len.WP58_COD_FISC);
    }

    public String getWp58CodFisc() {
        return this.wp58CodFisc;
    }

    public String getDp58CodFiscFormatted() {
        return Functions.padBlanks(getWp58CodFisc(), Len.WP58_COD_FISC);
    }

    public void setWp58CodPartIva(String wp58CodPartIva) {
        this.wp58CodPartIva = Functions.subString(wp58CodPartIva, Len.WP58_COD_PART_IVA);
    }

    public String getWp58CodPartIva() {
        return this.wp58CodPartIva;
    }

    public void setWp58IdRappAna(int wp58IdRappAna) {
        this.wp58IdRappAna = wp58IdRappAna;
    }

    public int getWp58IdRappAna() {
        return this.wp58IdRappAna;
    }

    public void setWp58IdMoviCrz(int wp58IdMoviCrz) {
        this.wp58IdMoviCrz = wp58IdMoviCrz;
    }

    public int getWp58IdMoviCrz() {
        return this.wp58IdMoviCrz;
    }

    public void setWp58DtIniEff(int wp58DtIniEff) {
        this.wp58DtIniEff = wp58DtIniEff;
    }

    public int getWp58DtIniEff() {
        return this.wp58DtIniEff;
    }

    public void setWp58DtEndEff(int wp58DtEndEff) {
        this.wp58DtEndEff = wp58DtEndEff;
    }

    public int getWp58DtEndEff() {
        return this.wp58DtEndEff;
    }

    public void setWp58DtIniCalc(int wp58DtIniCalc) {
        this.wp58DtIniCalc = wp58DtIniCalc;
    }

    public int getWp58DtIniCalc() {
        return this.wp58DtIniCalc;
    }

    public void setWp58DtEndCalc(int wp58DtEndCalc) {
        this.wp58DtEndCalc = wp58DtEndCalc;
    }

    public int getWp58DtEndCalc() {
        return this.wp58DtEndCalc;
    }

    public void setWp58TpCausBollo(String wp58TpCausBollo) {
        this.wp58TpCausBollo = Functions.subString(wp58TpCausBollo, Len.WP58_TP_CAUS_BOLLO);
    }

    public String getWp58TpCausBollo() {
        return this.wp58TpCausBollo;
    }

    public void setWp58ImpstBolloDettC(AfDecimal wp58ImpstBolloDettC) {
        this.wp58ImpstBolloDettC.assign(wp58ImpstBolloDettC);
    }

    public AfDecimal getWp58ImpstBolloDettC() {
        return this.wp58ImpstBolloDettC.copy();
    }

    public void setWp58ImpstBolloTotR(AfDecimal wp58ImpstBolloTotR) {
        this.wp58ImpstBolloTotR.assign(wp58ImpstBolloTotR);
    }

    public AfDecimal getWp58ImpstBolloTotR() {
        return this.wp58ImpstBolloTotR.copy();
    }

    public void setWp58DsRiga(long wp58DsRiga) {
        this.wp58DsRiga = wp58DsRiga;
    }

    public long getWp58DsRiga() {
        return this.wp58DsRiga;
    }

    public void setWp58DsOperSql(char wp58DsOperSql) {
        this.wp58DsOperSql = wp58DsOperSql;
    }

    public char getWp58DsOperSql() {
        return this.wp58DsOperSql;
    }

    public void setWp58DsVer(int wp58DsVer) {
        this.wp58DsVer = wp58DsVer;
    }

    public int getWp58DsVer() {
        return this.wp58DsVer;
    }

    public void setWp58DsTsIniCptz(long wp58DsTsIniCptz) {
        this.wp58DsTsIniCptz = wp58DsTsIniCptz;
    }

    public long getWp58DsTsIniCptz() {
        return this.wp58DsTsIniCptz;
    }

    public void setWp58DsTsEndCptz(long wp58DsTsEndCptz) {
        this.wp58DsTsEndCptz = wp58DsTsEndCptz;
    }

    public long getWp58DsTsEndCptz() {
        return this.wp58DsTsEndCptz;
    }

    public void setWp58DsUtente(String wp58DsUtente) {
        this.wp58DsUtente = Functions.subString(wp58DsUtente, Len.WP58_DS_UTENTE);
    }

    public String getWp58DsUtente() {
        return this.wp58DsUtente;
    }

    public void setWp58DsStatoElab(char wp58DsStatoElab) {
        this.wp58DsStatoElab = wp58DsStatoElab;
    }

    public char getWp58DsStatoElab() {
        return this.wp58DsStatoElab;
    }

    public Wp58IdMoviChiu getWp58IdMoviChiu() {
        return wp58IdMoviChiu;
    }

    public Wp58ImpstBolloDettV getWp58ImpstBolloDettV() {
        return wp58ImpstBolloDettV;
    }

    public Wp58ImpstBolloTotV getWp58ImpstBolloTotV() {
        return wp58ImpstBolloTotV;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WP58_ID_IMPST_BOLLO = 5;
        public static final int WP58_COD_COMP_ANIA = 3;
        public static final int WP58_ID_POLI = 5;
        public static final int WP58_IB_POLI = 40;
        public static final int WP58_COD_FISC = 16;
        public static final int WP58_COD_PART_IVA = 11;
        public static final int WP58_ID_RAPP_ANA = 5;
        public static final int WP58_ID_MOVI_CRZ = 5;
        public static final int WP58_DT_INI_EFF = 5;
        public static final int WP58_DT_END_EFF = 5;
        public static final int WP58_DT_INI_CALC = 5;
        public static final int WP58_DT_END_CALC = 5;
        public static final int WP58_TP_CAUS_BOLLO = 2;
        public static final int WP58_IMPST_BOLLO_DETT_C = 8;
        public static final int WP58_IMPST_BOLLO_TOT_R = 8;
        public static final int WP58_DS_RIGA = 6;
        public static final int WP58_DS_OPER_SQL = 1;
        public static final int WP58_DS_VER = 5;
        public static final int WP58_DS_TS_INI_CPTZ = 10;
        public static final int WP58_DS_TS_END_CPTZ = 10;
        public static final int WP58_DS_UTENTE = 20;
        public static final int WP58_DS_STATO_ELAB = 1;
        public static final int DATI = WP58_ID_IMPST_BOLLO + WP58_COD_COMP_ANIA + WP58_ID_POLI + WP58_IB_POLI + WP58_COD_FISC + WP58_COD_PART_IVA + WP58_ID_RAPP_ANA + WP58_ID_MOVI_CRZ + Wp58IdMoviChiu.Len.WP58_ID_MOVI_CHIU + WP58_DT_INI_EFF + WP58_DT_END_EFF + WP58_DT_INI_CALC + WP58_DT_END_CALC + WP58_TP_CAUS_BOLLO + WP58_IMPST_BOLLO_DETT_C + Wp58ImpstBolloDettV.Len.WP58_IMPST_BOLLO_DETT_V + Wp58ImpstBolloTotV.Len.WP58_IMPST_BOLLO_TOT_V + WP58_IMPST_BOLLO_TOT_R + WP58_DS_RIGA + WP58_DS_OPER_SQL + WP58_DS_VER + WP58_DS_TS_INI_CPTZ + WP58_DS_TS_END_CPTZ + WP58_DS_UTENTE + WP58_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP58_ID_IMPST_BOLLO = 9;
            public static final int WP58_COD_COMP_ANIA = 5;
            public static final int WP58_ID_POLI = 9;
            public static final int WP58_ID_RAPP_ANA = 9;
            public static final int WP58_ID_MOVI_CRZ = 9;
            public static final int WP58_DT_INI_EFF = 8;
            public static final int WP58_DT_END_EFF = 8;
            public static final int WP58_DT_INI_CALC = 8;
            public static final int WP58_DT_END_CALC = 8;
            public static final int WP58_IMPST_BOLLO_DETT_C = 12;
            public static final int WP58_IMPST_BOLLO_TOT_R = 12;
            public static final int WP58_DS_RIGA = 10;
            public static final int WP58_DS_VER = 9;
            public static final int WP58_DS_TS_INI_CPTZ = 18;
            public static final int WP58_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP58_IMPST_BOLLO_DETT_C = 3;
            public static final int WP58_IMPST_BOLLO_TOT_R = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
