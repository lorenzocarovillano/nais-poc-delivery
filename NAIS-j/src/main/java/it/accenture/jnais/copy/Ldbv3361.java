package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV3361<br>
 * Copybook: LDBV3361 from copybook LDBV3361<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Ldbv3361 {

    //==== PROPERTIES ====
    //Original name: LDBV3361-ID-OGG
    private int ldbv3361IdOgg = DefaultValues.INT_VAL;
    //Original name: LDBV3361-TP-OGG
    private String ldbv3361TpOgg = DefaultValues.stringVal(Len.LDBV3361_TP_OGG);

    //==== METHODS ====
    public void setLdbv3361Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV3361];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV3361);
        setLdbv3361Bytes(buffer, 1);
    }

    public String getLdbv3361Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv3361Bytes());
    }

    /**Original name: LDBV3361<br>
	 * <pre>* COPY WHERE CONDITION JOIN STW-ODE
	 *    fine COPY IDSV0006</pre>*/
    public byte[] getLdbv3361Bytes() {
        byte[] buffer = new byte[Len.LDBV3361];
        return getLdbv3361Bytes(buffer, 1);
    }

    public void setLdbv3361Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv3361IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV3361_ID_OGG, 0);
        position += Len.LDBV3361_ID_OGG;
        ldbv3361TpOgg = MarshalByte.readString(buffer, position, Len.LDBV3361_TP_OGG);
    }

    public byte[] getLdbv3361Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv3361IdOgg, Len.Int.LDBV3361_ID_OGG, 0);
        position += Len.LDBV3361_ID_OGG;
        MarshalByte.writeString(buffer, position, ldbv3361TpOgg, Len.LDBV3361_TP_OGG);
        return buffer;
    }

    public void setLdbv3361IdOgg(int ldbv3361IdOgg) {
        this.ldbv3361IdOgg = ldbv3361IdOgg;
    }

    public int getLdbv3361IdOgg() {
        return this.ldbv3361IdOgg;
    }

    public void setLdbv3361TpOgg(String ldbv3361TpOgg) {
        this.ldbv3361TpOgg = Functions.subString(ldbv3361TpOgg, Len.LDBV3361_TP_OGG);
    }

    public String getLdbv3361TpOgg() {
        return this.ldbv3361TpOgg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV3361_TP_OGG = 2;
        public static final int LDBV3361_ID_OGG = 5;
        public static final int LDBV3361 = LDBV3361_ID_OGG + LDBV3361_TP_OGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV3361_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
