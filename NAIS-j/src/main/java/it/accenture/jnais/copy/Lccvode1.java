package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVODE1<br>
 * Variable: LCCVODE1 from copybook LCCVODE1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvode1 {

    //==== PROPERTIES ====
    /**Original name: WODE-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA OGG_DEROGA
	 *    ALIAS ODE
	 *    ULTIMO AGG. 02 SET 2008
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WODE-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WODE-DATI
    private WodeDati dati = new WodeDati();

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WodeDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }
}
