package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: LDBV1301<br>
 * Copybook: LDBV1301 from copybook LDBV1301<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Ldbv1301 {

    //==== PROPERTIES ====
    //Original name: LDBV1301-ID-TAB
    private int ldbv1301IdTab = DefaultValues.INT_VAL;
    //Original name: LDBV1301-TIMESTAMP
    private long ldbv1301Timestamp = DefaultValues.LONG_VAL;

    //==== METHODS ====
    public void setLdbv1301Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV1301];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV1301);
        setLdbv1301Bytes(buffer, 1);
    }

    public String getLdbv1301Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv1301Bytes());
    }

    /**Original name: LDBV1301<br>
	 * <pre>--> AREA MODULO LDBS1300</pre>*/
    public byte[] getLdbv1301Bytes() {
        byte[] buffer = new byte[Len.LDBV1301];
        return getLdbv1301Bytes(buffer, 1);
    }

    public void setLdbv1301Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv1301IdTab = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV1301_ID_TAB, 0);
        position += Len.LDBV1301_ID_TAB;
        ldbv1301Timestamp = MarshalByte.readPackedAsLong(buffer, position, Len.Int.LDBV1301_TIMESTAMP, 0);
    }

    public byte[] getLdbv1301Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv1301IdTab, Len.Int.LDBV1301_ID_TAB, 0);
        position += Len.LDBV1301_ID_TAB;
        MarshalByte.writeLongAsPacked(buffer, position, ldbv1301Timestamp, Len.Int.LDBV1301_TIMESTAMP, 0);
        return buffer;
    }

    public void setLdbv1301IdTab(int ldbv1301IdTab) {
        this.ldbv1301IdTab = ldbv1301IdTab;
    }

    public int getLdbv1301IdTab() {
        return this.ldbv1301IdTab;
    }

    public void setLdbv1301Timestamp(long ldbv1301Timestamp) {
        this.ldbv1301Timestamp = ldbv1301Timestamp;
    }

    public long getLdbv1301Timestamp() {
        return this.ldbv1301Timestamp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV1301_ID_TAB = 5;
        public static final int LDBV1301_TIMESTAMP = 10;
        public static final int LDBV1301 = LDBV1301_ID_TAB + LDBV1301_TIMESTAMP;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV1301_ID_TAB = 9;
            public static final int LDBV1301_TIMESTAMP = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
