package it.accenture.jnais.copy;

/**Original name: LOAR0820<br>
 * Copybook: LOAR0820 from copybook LOAR0820<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Loar0820 {

    //==== PROPERTIES ====
    //Original name: W820-AREA-TESTATA
    private W820AreaTestata testata = new W820AreaTestata();
    //Original name: W820-AREA-DATI
    private W820AreaDati dati = new W820AreaDati();

    //==== METHODS ====
    public W820AreaDati getDati() {
        return dati;
    }

    public W820AreaTestata getTestata() {
        return testata;
    }
}
