package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: C214-DATI-LIVELLO<br>
 * Variable: C214-DATI-LIVELLO from copybook IVVC0213<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class C214DatiLivello {

    //==== PROPERTIES ====
    //Original name: C214-TP-LIVELLO
    private char tpLivello = DefaultValues.CHAR_VAL;
    //Original name: C214-COD-LIVELLO
    private String codLivello = DefaultValues.stringVal(Len.COD_LIVELLO);
    /**Original name: C214-ID-LIVELLO<br>
	 * <pre> INFORMAZIONI DI TRANCHE</pre>*/
    private String idLivello = DefaultValues.stringVal(Len.ID_LIVELLO);
    //Original name: C214-DT-INIZ-TARI
    private String dtInizTari = DefaultValues.stringVal(Len.DT_INIZ_TARI);
    //Original name: C214-DT-INIZ-PROD
    private String dtInizProd = DefaultValues.stringVal(Len.DT_INIZ_PROD);
    //Original name: C214-COD-RGM-FISC
    private String codRgmFisc = DefaultValues.stringVal(Len.COD_RGM_FISC);
    //Original name: C214-DT-DECOR
    private String dtDecor = DefaultValues.stringVal(Len.DT_DECOR);
    //Original name: C214-COD-VARIABILE
    private String codVariabile = DefaultValues.stringVal(Len.COD_VARIABILE);
    //Original name: C214-TP-DATO
    private char tpDato = DefaultValues.CHAR_VAL;
    //Original name: C214-VAL-IMP
    private AfDecimal valImp = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);
    //Original name: C214-VAL-PERC
    private AfDecimal valPerc = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);
    //Original name: C214-VAL-STR
    private String valStr = DefaultValues.stringVal(Len.VAL_STR);
    //Original name: C214-NOME-SERVIZIO
    private String nomeServizio = DefaultValues.stringVal(Len.NOME_SERVIZIO);

    //==== METHODS ====
    public void setDatiLivelloBytes(byte[] buffer, int offset) {
        int position = offset;
        tpLivello = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        codLivello = MarshalByte.readString(buffer, position, Len.COD_LIVELLO);
        position += Len.COD_LIVELLO;
        idLivello = MarshalByte.readFixedString(buffer, position, Len.ID_LIVELLO);
        position += Len.ID_LIVELLO;
        dtInizTari = MarshalByte.readString(buffer, position, Len.DT_INIZ_TARI);
        position += Len.DT_INIZ_TARI;
        dtInizProd = MarshalByte.readString(buffer, position, Len.DT_INIZ_PROD);
        position += Len.DT_INIZ_PROD;
        codRgmFisc = MarshalByte.readString(buffer, position, Len.COD_RGM_FISC);
        position += Len.COD_RGM_FISC;
        dtDecor = MarshalByte.readString(buffer, position, Len.DT_DECOR);
        position += Len.DT_DECOR;
        setAreaVariabiliBytes(buffer, position);
        position += Len.AREA_VARIABILI;
        nomeServizio = MarshalByte.readString(buffer, position, Len.NOME_SERVIZIO);
    }

    public byte[] getDatiLivelloBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, tpLivello);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codLivello, Len.COD_LIVELLO);
        position += Len.COD_LIVELLO;
        MarshalByte.writeString(buffer, position, idLivello, Len.ID_LIVELLO);
        position += Len.ID_LIVELLO;
        MarshalByte.writeString(buffer, position, dtInizTari, Len.DT_INIZ_TARI);
        position += Len.DT_INIZ_TARI;
        MarshalByte.writeString(buffer, position, dtInizProd, Len.DT_INIZ_PROD);
        position += Len.DT_INIZ_PROD;
        MarshalByte.writeString(buffer, position, codRgmFisc, Len.COD_RGM_FISC);
        position += Len.COD_RGM_FISC;
        MarshalByte.writeString(buffer, position, dtDecor, Len.DT_DECOR);
        position += Len.DT_DECOR;
        getAreaVariabiliBytes(buffer, position);
        position += Len.AREA_VARIABILI;
        MarshalByte.writeString(buffer, position, nomeServizio, Len.NOME_SERVIZIO);
        return buffer;
    }

    public void setTpLivello(char tpLivello) {
        this.tpLivello = tpLivello;
    }

    public char getTpLivello() {
        return this.tpLivello;
    }

    public void setCodLivello(String codLivello) {
        this.codLivello = Functions.subString(codLivello, Len.COD_LIVELLO);
    }

    public String getCodLivello() {
        return this.codLivello;
    }

    public void setIdLivelloFormatted(String idLivello) {
        this.idLivello = Trunc.toUnsignedNumeric(idLivello, Len.ID_LIVELLO);
    }

    public int getIdLivello() {
        return NumericDisplay.asInt(this.idLivello);
    }

    public String getIvvc0213IdLivelloFormatted() {
        return this.idLivello;
    }

    public void setDtInizTari(String dtInizTari) {
        this.dtInizTari = Functions.subString(dtInizTari, Len.DT_INIZ_TARI);
    }

    public String getDtInizTari() {
        return this.dtInizTari;
    }

    public void setDtInizProd(String dtInizProd) {
        this.dtInizProd = Functions.subString(dtInizProd, Len.DT_INIZ_PROD);
    }

    public String getDtInizProd() {
        return this.dtInizProd;
    }

    public void setCodRgmFisc(String codRgmFisc) {
        this.codRgmFisc = Functions.subString(codRgmFisc, Len.COD_RGM_FISC);
    }

    public String getCodRgmFisc() {
        return this.codRgmFisc;
    }

    public void setDtDecor(String dtDecor) {
        this.dtDecor = Functions.subString(dtDecor, Len.DT_DECOR);
    }

    public String getDtDecor() {
        return this.dtDecor;
    }

    public String getIvvc0213DtDecorFormatted() {
        return Functions.padBlanks(getDtDecor(), Len.DT_DECOR);
    }

    public void setAreaVariabiliBytes(byte[] buffer, int offset) {
        int position = offset;
        setTabVariabiliBytes(buffer, position);
    }

    public byte[] getAreaVariabiliBytes(byte[] buffer, int offset) {
        int position = offset;
        getTabVariabiliBytes(buffer, position);
        return buffer;
    }

    public void setTabVariabiliBytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaVariabileBytes(buffer, position);
    }

    public byte[] getTabVariabiliBytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaVariabileBytes(buffer, position);
        return buffer;
    }

    /**Original name: IVVC0213-AREA-VARIABILE<br>*/
    public byte[] getIvvc0213AreaVariabileBytes() {
        byte[] buffer = new byte[Len.AREA_VARIABILE];
        return getAreaVariabileBytes(buffer, 1);
    }

    public void setAreaVariabileBytes(byte[] buffer, int offset) {
        int position = offset;
        codVariabile = MarshalByte.readString(buffer, position, Len.COD_VARIABILE);
        position += Len.COD_VARIABILE;
        tpDato = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        valImp.assign(MarshalByte.readDecimal(buffer, position, Len.Int.VAL_IMP, Len.Fract.VAL_IMP));
        position += Len.VAL_IMP;
        valPerc.assign(MarshalByte.readDecimal(buffer, position, Len.Int.VAL_PERC, Len.Fract.VAL_PERC, SignType.NO_SIGN));
        position += Len.VAL_PERC;
        valStr = MarshalByte.readString(buffer, position, Len.VAL_STR);
    }

    public byte[] getAreaVariabileBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codVariabile, Len.COD_VARIABILE);
        position += Len.COD_VARIABILE;
        MarshalByte.writeChar(buffer, position, tpDato);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimal(buffer, position, valImp.copy());
        position += Len.VAL_IMP;
        MarshalByte.writeDecimal(buffer, position, valPerc.copy(), SignType.NO_SIGN);
        position += Len.VAL_PERC;
        MarshalByte.writeString(buffer, position, valStr, Len.VAL_STR);
        return buffer;
    }

    public void setCodVariabile(String codVariabile) {
        this.codVariabile = Functions.subString(codVariabile, Len.COD_VARIABILE);
    }

    public String getCodVariabile() {
        return this.codVariabile;
    }

    public void setTpDato(char tpDato) {
        this.tpDato = tpDato;
    }

    public char getTpDato() {
        return this.tpDato;
    }

    public void setValImp(AfDecimal valImp) {
        this.valImp.assign(valImp);
    }

    public AfDecimal getValImp() {
        return this.valImp.copy();
    }

    public void setValPerc(AfDecimal valPerc) {
        this.valPerc.assign(valPerc);
    }

    public AfDecimal getValPerc() {
        return this.valPerc.copy();
    }

    public void setValStr(String valStr) {
        this.valStr = Functions.subString(valStr, Len.VAL_STR);
    }

    public String getValStr() {
        return this.valStr;
    }

    public void setNomeServizio(String nomeServizio) {
        this.nomeServizio = Functions.subString(nomeServizio, Len.NOME_SERVIZIO);
    }

    public String getNomeServizio() {
        return this.nomeServizio;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_LIVELLO = 1;
        public static final int COD_LIVELLO = 12;
        public static final int ID_LIVELLO = 9;
        public static final int DT_INIZ_TARI = 8;
        public static final int DT_INIZ_PROD = 8;
        public static final int COD_RGM_FISC = 2;
        public static final int DT_DECOR = 8;
        public static final int COD_VARIABILE = 12;
        public static final int TP_DATO = 1;
        public static final int VAL_IMP = 18;
        public static final int VAL_PERC = 14;
        public static final int VAL_STR = 12;
        public static final int AREA_VARIABILE = COD_VARIABILE + TP_DATO + VAL_IMP + VAL_PERC + VAL_STR;
        public static final int TAB_VARIABILI = AREA_VARIABILE;
        public static final int AREA_VARIABILI = TAB_VARIABILI;
        public static final int NOME_SERVIZIO = 8;
        public static final int DATI_LIVELLO = TP_LIVELLO + COD_LIVELLO + ID_LIVELLO + DT_INIZ_TARI + DT_INIZ_PROD + COD_RGM_FISC + DT_DECOR + AREA_VARIABILI + NOME_SERVIZIO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAL_IMP = 11;
            public static final int VAL_PERC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAL_IMP = 7;
            public static final int VAL_PERC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
