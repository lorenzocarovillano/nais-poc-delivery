package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-OGG-COLLG<br>
 * Variable: IND-OGG-COLLG from copybook IDBVOCO2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndOggCollg {

    //==== PROPERTIES ====
    //Original name: IND-OCO-IB-RIFTO-ESTNO
    private short ibRiftoEstno = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-DT-SCAD
    private short dtScad = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-TP-TRASF
    private short tpTrasf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-REC-PROV
    private short recProv = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-DT-DECOR
    private short dtDecor = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-DT-ULT-PRE-PAG
    private short dtUltPrePag = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-TOT-PRE
    private short totPre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-RIS-MAT
    private short risMat = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-RIS-ZIL
    private short risZil = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-IMP-TRASF
    private short impTrasf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-IMP-REINVST
    private short impReinvst = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-PC-REINVST-RILIEVI
    private short pcReinvstRilievi = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-IB-2O-RIFTO-ESTNO
    private short ib2oRiftoEstno = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-IND-LIQ-AGG-MAN
    private short indLiqAggMan = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-IMP-COLLG
    private short impCollg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-PRE-PER-TRASF
    private short prePerTrasf = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-CAR-ACQ
    private short carAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-PRE-1A-ANNUALITA
    private short pre1aAnnualita = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-IMP-TRASFERITO
    private short impTrasferito = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-TP-MOD-ABBINAMENTO
    private short tpModAbbinamento = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OCO-PC-PRE-TRASFERITO
    private short pcPreTrasferito = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIbRiftoEstno(short ibRiftoEstno) {
        this.ibRiftoEstno = ibRiftoEstno;
    }

    public short getIbRiftoEstno() {
        return this.ibRiftoEstno;
    }

    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtScad(short dtScad) {
        this.dtScad = dtScad;
    }

    public short getDtScad() {
        return this.dtScad;
    }

    public void setTpTrasf(short tpTrasf) {
        this.tpTrasf = tpTrasf;
    }

    public short getTpTrasf() {
        return this.tpTrasf;
    }

    public void setRecProv(short recProv) {
        this.recProv = recProv;
    }

    public short getRecProv() {
        return this.recProv;
    }

    public void setDtDecor(short dtDecor) {
        this.dtDecor = dtDecor;
    }

    public short getDtDecor() {
        return this.dtDecor;
    }

    public void setDtUltPrePag(short dtUltPrePag) {
        this.dtUltPrePag = dtUltPrePag;
    }

    public short getDtUltPrePag() {
        return this.dtUltPrePag;
    }

    public void setTotPre(short totPre) {
        this.totPre = totPre;
    }

    public short getTotPre() {
        return this.totPre;
    }

    public void setRisMat(short risMat) {
        this.risMat = risMat;
    }

    public short getRisMat() {
        return this.risMat;
    }

    public void setRisZil(short risZil) {
        this.risZil = risZil;
    }

    public short getRisZil() {
        return this.risZil;
    }

    public void setImpTrasf(short impTrasf) {
        this.impTrasf = impTrasf;
    }

    public short getImpTrasf() {
        return this.impTrasf;
    }

    public void setImpReinvst(short impReinvst) {
        this.impReinvst = impReinvst;
    }

    public short getImpReinvst() {
        return this.impReinvst;
    }

    public void setPcReinvstRilievi(short pcReinvstRilievi) {
        this.pcReinvstRilievi = pcReinvstRilievi;
    }

    public short getPcReinvstRilievi() {
        return this.pcReinvstRilievi;
    }

    public void setIb2oRiftoEstno(short ib2oRiftoEstno) {
        this.ib2oRiftoEstno = ib2oRiftoEstno;
    }

    public short getIb2oRiftoEstno() {
        return this.ib2oRiftoEstno;
    }

    public void setIndLiqAggMan(short indLiqAggMan) {
        this.indLiqAggMan = indLiqAggMan;
    }

    public short getIndLiqAggMan() {
        return this.indLiqAggMan;
    }

    public void setImpCollg(short impCollg) {
        this.impCollg = impCollg;
    }

    public short getImpCollg() {
        return this.impCollg;
    }

    public void setPrePerTrasf(short prePerTrasf) {
        this.prePerTrasf = prePerTrasf;
    }

    public short getPrePerTrasf() {
        return this.prePerTrasf;
    }

    public void setCarAcq(short carAcq) {
        this.carAcq = carAcq;
    }

    public short getCarAcq() {
        return this.carAcq;
    }

    public void setPre1aAnnualita(short pre1aAnnualita) {
        this.pre1aAnnualita = pre1aAnnualita;
    }

    public short getPre1aAnnualita() {
        return this.pre1aAnnualita;
    }

    public void setImpTrasferito(short impTrasferito) {
        this.impTrasferito = impTrasferito;
    }

    public short getImpTrasferito() {
        return this.impTrasferito;
    }

    public void setTpModAbbinamento(short tpModAbbinamento) {
        this.tpModAbbinamento = tpModAbbinamento;
    }

    public short getTpModAbbinamento() {
        return this.tpModAbbinamento;
    }

    public void setPcPreTrasferito(short pcPreTrasferito) {
        this.pcPreTrasferito = pcPreTrasferito;
    }

    public short getPcPreTrasferito() {
        return this.pcPreTrasferito;
    }
}
