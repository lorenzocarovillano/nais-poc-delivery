package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IDSV0010<br>
 * Variable: IDSV0010 from copybook IDSV0010<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv0010 {

    //==== PROPERTIES ====
    //Original name: QUARANTA
    private String quaranta = "40";
    //Original name: WS-TS-INFINITO
    private long wsTsInfinito = 999912314023595999L;
    //Original name: WS-TS-INFINITO-1
    private long wsTsInfinito1 = 999912304023595999L;
    //Original name: WS-DT-INFINITO
    private String wsDtInfinito = "99991231";
    //Original name: WS-DT-INFINITO-1
    private String wsDtInfinito1 = "9999-12-30";
    //Original name: WS-RISULTATO
    private String wsRisultato = DefaultValues.stringVal(Len.WS_RISULTATO);
    //Original name: WS-DIFFERENZA
    private int wsDifferenza = DefaultValues.BIN_INT_VAL;
    //Original name: WS-DATE-SYSTEM-DB
    private String wsDateSystemDb = DefaultValues.stringVal(Len.WS_DATE_SYSTEM_DB);
    //Original name: WS-TS-SYSTEM-DB
    private String wsTsSystemDb = DefaultValues.stringVal(Len.WS_TS_SYSTEM_DB);
    //Original name: WS-TS-SYSTEM
    private String wsTsSystem = DefaultValues.stringVal(Len.WS_TS_SYSTEM);
    //Original name: WS-DATA-INIZIO-EFFETTO-DB
    private String wsDataInizioEffettoDb = DefaultValues.stringVal(Len.WS_DATA_INIZIO_EFFETTO_DB);
    //Original name: WS-DATA-FINE-EFFETTO-DB
    private String wsDataFineEffettoDb = DefaultValues.stringVal(Len.WS_DATA_FINE_EFFETTO_DB);
    //Original name: WS-TS-COMPETENZA
    private long wsTsCompetenza = DefaultValues.LONG_VAL;
    //Original name: WS-TS-COMPETENZA-AGG-STOR
    private long wsTsCompetenzaAggStor = DefaultValues.LONG_VAL;
    //Original name: WS-TS-DISPLAY
    private String wsTsDisplay = DefaultValues.stringVal(Len.WS_TS_DISPLAY);
    //Original name: WS-DATE-N
    private String wsDateN = DefaultValues.stringVal(Len.WS_DATE_N);
    //Original name: WS-DATE-X
    private String wsDateX = DefaultValues.stringVal(Len.WS_DATE_X);
    //Original name: WS-TIMESTAMP-N
    private String wsTimestampN = DefaultValues.stringVal(Len.WS_TIMESTAMP_N);
    //Original name: WS-TIMESTAMP-X
    private String wsTimestampX = DefaultValues.stringVal(Len.WS_TIMESTAMP_X);

    //==== METHODS ====
    public short getQuaranta() {
        return NumericDisplay.asShort(this.quaranta);
    }

    public String getQuarantaFormatted() {
        return this.quaranta;
    }

    public long getWsTsInfinito() {
        return this.wsTsInfinito;
    }

    public void setWsTsInfinito1(long wsTsInfinito1) {
        this.wsTsInfinito1 = wsTsInfinito1;
    }

    public long getWsTsInfinito1() {
        return this.wsTsInfinito1;
    }

    public int getWsDtInfinito() {
        return NumericDisplay.asInt(this.wsDtInfinito);
    }

    public String getWsDtInfinitoFormatted() {
        return this.wsDtInfinito;
    }

    public void setWsDtInfinito1(String wsDtInfinito1) {
        this.wsDtInfinito1 = Functions.subString(wsDtInfinito1, Len.WS_DT_INFINITO1);
    }

    public String getWsDtInfinito1() {
        return this.wsDtInfinito1;
    }

    public String getWsStrRisultatoFormatted() {
        return getWsRisultatoFormatted();
    }

    public void setWsRisultato(short wsRisultato) {
        this.wsRisultato = NumericDisplay.asString(wsRisultato, Len.WS_RISULTATO);
    }

    public void setWsRisultatoFormatted(String wsRisultato) {
        this.wsRisultato = Trunc.toUnsignedNumeric(wsRisultato, Len.WS_RISULTATO);
    }

    public short getWsRisultato() {
        return NumericDisplay.asShort(this.wsRisultato);
    }

    public String getWsRisultatoFormatted() {
        return this.wsRisultato;
    }

    public void setWsDifferenza(int wsDifferenza) {
        this.wsDifferenza = wsDifferenza;
    }

    public int getWsDifferenza() {
        return this.wsDifferenza;
    }

    public void setWsDateSystemDb(String wsDateSystemDb) {
        this.wsDateSystemDb = Functions.subString(wsDateSystemDb, Len.WS_DATE_SYSTEM_DB);
    }

    public String getWsDateSystemDb() {
        return this.wsDateSystemDb;
    }

    public void setWsTsSystemDb(String wsTsSystemDb) {
        this.wsTsSystemDb = Functions.subString(wsTsSystemDb, Len.WS_TS_SYSTEM_DB);
    }

    public String getWsTsSystemDb() {
        return this.wsTsSystemDb;
    }

    public void setWsTsSystem(long wsTsSystem) {
        this.wsTsSystem = NumericDisplay.asString(wsTsSystem, Len.WS_TS_SYSTEM);
    }

    public void setWsTsSystemFormatted(String wsTsSystem) {
        this.wsTsSystem = Trunc.toUnsignedNumeric(wsTsSystem, Len.WS_TS_SYSTEM);
    }

    public long getWsTsSystem() {
        return NumericDisplay.asLong(this.wsTsSystem);
    }

    public String getWsTsSystemFormatted() {
        return this.wsTsSystem;
    }

    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.wsDataInizioEffettoDb = Functions.subString(wsDataInizioEffettoDb, Len.WS_DATA_INIZIO_EFFETTO_DB);
    }

    public String getWsDataInizioEffettoDb() {
        return this.wsDataInizioEffettoDb;
    }

    public void setWsDataFineEffettoDb(String wsDataFineEffettoDb) {
        this.wsDataFineEffettoDb = Functions.subString(wsDataFineEffettoDb, Len.WS_DATA_FINE_EFFETTO_DB);
    }

    public String getWsDataFineEffettoDb() {
        return this.wsDataFineEffettoDb;
    }

    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.wsTsCompetenza = wsTsCompetenza;
    }

    public long getWsTsCompetenza() {
        return this.wsTsCompetenza;
    }

    public void setWsTsCompetenzaAggStor(long wsTsCompetenzaAggStor) {
        this.wsTsCompetenzaAggStor = wsTsCompetenzaAggStor;
    }

    public long getWsTsCompetenzaAggStor() {
        return this.wsTsCompetenzaAggStor;
    }

    public void setWsTsDisplaySubstring(String replacement, int start, int length) {
        wsTsDisplay = Functions.setSubstring(wsTsDisplay, replacement, start, length);
    }

    public String getWsTsDisplayFormatted() {
        return this.wsTsDisplay;
    }

    public void setWsStrDateNFormatted(String data) {
        byte[] buffer = new byte[Len.WS_STR_DATE_N];
        MarshalByte.writeString(buffer, 1, data, Len.WS_STR_DATE_N);
        setWsStrDateNBytes(buffer, 1);
    }

    public String getWsStrDateNFormatted() {
        return getWsDateNFormatted();
    }

    public void setWsStrDateNBytes(byte[] buffer, int offset) {
        int position = offset;
        wsDateN = MarshalByte.readFixedString(buffer, position, Len.WS_DATE_N);
    }

    public void setWsDateN(int wsDateN) {
        this.wsDateN = NumericDisplay.asString(wsDateN, Len.WS_DATE_N);
    }

    public void setWsDateNFormatted(String wsDateN) {
        this.wsDateN = Trunc.toUnsignedNumeric(wsDateN, Len.WS_DATE_N);
    }

    public void setWsDateNFromBuffer(byte[] buffer) {
        wsDateN = MarshalByte.readFixedString(buffer, 1, Len.WS_DATE_N);
    }

    public int getWsDateN() {
        return NumericDisplay.asInt(this.wsDateN);
    }

    public String getWsDateNFormatted() {
        return this.wsDateN;
    }

    public void setWsDateX(String wsDateX) {
        this.wsDateX = Functions.subString(wsDateX, Len.WS_DATE_X);
    }

    public String getWsDateX() {
        return this.wsDateX;
    }

    public String getWsDateXFormatted() {
        return Functions.padBlanks(getWsDateX(), Len.WS_DATE_X);
    }

    public void setWsStrTimestampNFormatted(String data) {
        byte[] buffer = new byte[Len.WS_STR_TIMESTAMP_N];
        MarshalByte.writeString(buffer, 1, data, Len.WS_STR_TIMESTAMP_N);
        setWsStrTimestampNBytes(buffer, 1);
    }

    public String getWsStrTimestampNFormatted() {
        return getWsTimestampNFormatted();
    }

    public void setWsStrTimestampNBytes(byte[] buffer, int offset) {
        int position = offset;
        wsTimestampN = MarshalByte.readFixedString(buffer, position, Len.WS_TIMESTAMP_N);
    }

    public void setWsTimestampN(long wsTimestampN) {
        this.wsTimestampN = NumericDisplay.asString(wsTimestampN, Len.WS_TIMESTAMP_N);
    }

    public long getWsTimestampN() {
        return NumericDisplay.asLong(this.wsTimestampN);
    }

    public String getWsTimestampNFormatted() {
        return this.wsTimestampN;
    }

    public void setWsTimestampX(String wsTimestampX) {
        this.wsTimestampX = Functions.subString(wsTimestampX, Len.WS_TIMESTAMP_X);
    }

    public String getWsTimestampX() {
        return this.wsTimestampX;
    }

    public String getWsTimestampXFormatted() {
        return Functions.padBlanks(getWsTimestampX(), Len.WS_TIMESTAMP_X);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_RISULTATO = 2;
        public static final int WS_DATE_SYSTEM_DB = 10;
        public static final int WS_TS_SYSTEM_DB = 26;
        public static final int WS_TS_SYSTEM = 18;
        public static final int WS_DATA_INIZIO_EFFETTO_DB = 10;
        public static final int WS_DATA_FINE_EFFETTO_DB = 10;
        public static final int WS_TS_DISPLAY = 18;
        public static final int WS_DATE_N = 8;
        public static final int WS_DATE_X = 10;
        public static final int WS_TIMESTAMP_N = 18;
        public static final int WS_TIMESTAMP_X = 26;
        public static final int WS_STR_DATE_N = WS_DATE_N;
        public static final int WS_STR_TIMESTAMP_N = WS_TIMESTAMP_N;
        public static final int WS_DT_INFINITO1 = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
