package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0303<br>
 * Copybook: IDSV0303 from copybook IDSV0303<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idsv0303 {

    //==== PROPERTIES ====
    //Original name: IDSV0303-ELE-MAX-APPO
    private int idsv0303EleMaxAppo = DefaultValues.INT_VAL;
    //Original name: IDSV0303-ELE-MAX-TOT
    private int idsv0303EleMaxTot = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setIdsv0303EleMaxAppo(int idsv0303EleMaxAppo) {
        this.idsv0303EleMaxAppo = idsv0303EleMaxAppo;
    }

    public int getIdsv0303EleMaxAppo() {
        return this.idsv0303EleMaxAppo;
    }

    public void setIdsv0303EleMaxTot(int idsv0303EleMaxTot) {
        this.idsv0303EleMaxTot = idsv0303EleMaxTot;
    }

    public int getIdsv0303EleMaxTot() {
        return this.idsv0303EleMaxTot;
    }
}
