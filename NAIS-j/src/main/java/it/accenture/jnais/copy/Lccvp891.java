package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVP891<br>
 * Variable: LCCVP891 from copybook LCCVP891<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvp891 {

    //==== PROPERTIES ====
    /**Original name: WP89-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA D_ATT_SERV_VAL
	 *    ALIAS P89
	 *    ULTIMO AGG. 17 FEB 2015
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WP89-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WP89-DATI
    private Wp89Dati dati = new Wp89Dati();

    //==== METHODS ====
    public void initLccvp891Spaces() {
        status.setStatus(Types.SPACE_CHAR);
        idPtf = Types.INVALID_INT_VAL;
        dati.initDatiSpaces();
    }

    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public Wp89Dati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
