package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: A2K-OUSTA<br>
 * Variable: A2K-OUSTA from copybook LCCC0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class A2kOusta {

    //==== PROPERTIES ====
    //Original name: A2K-OUGG04
    private String ougg04 = DefaultValues.stringVal(Len.OUGG04);
    //Original name: FILLER-A2K-OUSTA
    private char flr1 = DefaultValues.CHAR_VAL;
    //Original name: A2K-OUMM04
    private String oumm04 = DefaultValues.stringVal(Len.OUMM04);
    //Original name: FILLER-A2K-OUSTA-1
    private char flr2 = DefaultValues.CHAR_VAL;
    //Original name: A2K-OUSS04
    private String ouss04 = DefaultValues.stringVal(Len.OUSS04);
    //Original name: A2K-OUAA04
    private String ouaa04 = DefaultValues.stringVal(Len.OUAA04);

    //==== METHODS ====
    public void setA2kOustaBytes(byte[] buffer, int offset) {
        int position = offset;
        ougg04 = MarshalByte.readFixedString(buffer, position, Len.OUGG04);
        position += Len.OUGG04;
        flr1 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        oumm04 = MarshalByte.readString(buffer, position, Len.OUMM04);
        position += Len.OUMM04;
        flr2 = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ouss04 = MarshalByte.readFixedString(buffer, position, Len.OUSS04);
        position += Len.OUSS04;
        ouaa04 = MarshalByte.readFixedString(buffer, position, Len.OUAA04);
    }

    public byte[] getA2kOustaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ougg04, Len.OUGG04);
        position += Len.OUGG04;
        MarshalByte.writeChar(buffer, position, flr1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, oumm04, Len.OUMM04);
        position += Len.OUMM04;
        MarshalByte.writeChar(buffer, position, flr2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ouss04, Len.OUSS04);
        position += Len.OUSS04;
        MarshalByte.writeString(buffer, position, ouaa04, Len.OUAA04);
        return buffer;
    }

    public void initA2kOustaSpaces() {
        ougg04 = "";
        flr1 = Types.SPACE_CHAR;
        oumm04 = "";
        flr2 = Types.SPACE_CHAR;
        ouss04 = "";
        ouaa04 = "";
    }

    public void setOugg04Formatted(String ougg04) {
        this.ougg04 = Trunc.toUnsignedNumeric(ougg04, Len.OUGG04);
    }

    public short getOugg04() {
        return NumericDisplay.asShort(this.ougg04);
    }

    public void setFlr1(char flr1) {
        this.flr1 = flr1;
    }

    public char getFlr1() {
        return this.flr1;
    }

    public void setOumm04(String oumm04) {
        this.oumm04 = Functions.subString(oumm04, Len.OUMM04);
    }

    public String getOumm04() {
        return this.oumm04;
    }

    public void setFlr2(char flr2) {
        this.flr2 = flr2;
    }

    public char getFlr2() {
        return this.flr2;
    }

    public void setOuss04Formatted(String ouss04) {
        this.ouss04 = Trunc.toUnsignedNumeric(ouss04, Len.OUSS04);
    }

    public short getOuss04() {
        return NumericDisplay.asShort(this.ouss04);
    }

    public void setOuaa04Formatted(String ouaa04) {
        this.ouaa04 = Trunc.toUnsignedNumeric(ouaa04, Len.OUAA04);
    }

    public short getOuaa04() {
        return NumericDisplay.asShort(this.ouaa04);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OUGG04 = 2;
        public static final int FLR1 = 1;
        public static final int OUMM04 = 9;
        public static final int OUSS04 = 2;
        public static final int OUAA04 = 2;
        public static final int A2K_OUSTA = OUGG04 + OUMM04 + OUSS04 + OUAA04 + 2 * FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
