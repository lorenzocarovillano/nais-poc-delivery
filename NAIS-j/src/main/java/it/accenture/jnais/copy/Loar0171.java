package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WrecOut;

/**Original name: LOAR0171<br>
 * Copybook: LOAR0171 from copybook LOAR0171<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Loar0171 {

    //==== PROPERTIES ====
    /**Original name: WREC-TIPO-REC<br>
	 * <pre>  --------------------------------------------------------------*
	 *      STRUTTURA ARCHIVIO IN OUTPUT - LOAR0171 - LREC=500         *
	 *   --------------------------------------------------------------*</pre>*/
    private String wrecTipoRec = DefaultValues.stringVal(Len.WREC_TIPO_REC);
    //Original name: WREC-OUT
    private WrecOut wrecOut = new WrecOut();

    //==== METHODS ====
    public void setWrecTipoRec(String wrecTipoRec) {
        this.wrecTipoRec = Functions.subString(wrecTipoRec, Len.WREC_TIPO_REC);
    }

    public String getWrecTipoRec() {
        return this.wrecTipoRec;
    }

    public WrecOut getWrecOut() {
        return wrecOut;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WREC_TIPO_REC = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
