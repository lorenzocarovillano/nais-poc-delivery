package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.WcomBsCallType;
import it.accenture.jnais.ws.enums.WcomPrimaVolta;
import it.accenture.jnais.ws.enums.WcomStepSucc;
import it.accenture.jnais.ws.enums.WcomWrite;

/**Original name: LCCC0261<br>
 * Variable: LCCC0261 from copybook LCCC0261<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccc0261 {

    //==== PROPERTIES ====
    /**Original name: WCOM-BS-CALL-TYPE<br>
	 * <pre>------------------------------------------------------------*
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA                      *
	 *    AREA INFRASTRUTTURALE OPERAZIONI AUTOMATICHE             *
	 *    ULTIMO AGG.  02 LUG 2007                                 *
	 * ------------------------------------------------------------*
	 *  --  PROVENIENZA CHIAMATA</pre>*/
    private WcomBsCallType bsCallType = new WcomBsCallType();
    //Original name: WCOM-DATI-POLIZZA
    private WcomDatiPolizza datiPolizza = new WcomDatiPolizza();
    //Original name: WCOM-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: WCOM-ADE-IB-OGG
    private String adeIbOgg = DefaultValues.stringVal(Len.ADE_IB_OGG);
    /**Original name: WCOM-ID-MOVI-CREAZ<br>
	 * <pre> --  MOVIMENTO CREAZIONE</pre>*/
    private int idMoviCreaz = DefaultValues.INT_VAL;
    /**Original name: WCOM-PRIMA-VOLTA<br>
	 * <pre> --  PRIMA VOLTA</pre>*/
    private WcomPrimaVolta primaVolta = new WcomPrimaVolta();
    /**Original name: WCOM-STEP-SUCC<br>
	 * <pre> -- INDICATORE DI ESECUZIONE STEP SUCCESSIVI</pre>*/
    private WcomStepSucc stepSucc = new WcomStepSucc();
    /**Original name: WCOM-WRITE<br>
	 * <pre> -- GESTIONE ERRORE ELABORATIVO</pre>*/
    private WcomWrite write = new WcomWrite();
    //Original name: WCOM-DATI-MBS
    private WcomDatiMbs datiMbs = new WcomDatiMbs();
    //Original name: WCOM-DATI-BLOCCO
    private WcomDatiBlocco datiBlocco = new WcomDatiBlocco();

    //==== METHODS ====
    public void setDatiAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        adeIbOgg = MarshalByte.readString(buffer, position, Len.ADE_IB_OGG);
    }

    public byte[] getDatiAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeString(buffer, position, adeIbOgg, Len.ADE_IB_OGG);
        return buffer;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setAdeIbOgg(String adeIbOgg) {
        this.adeIbOgg = Functions.subString(adeIbOgg, Len.ADE_IB_OGG);
    }

    public String getAdeIbOgg() {
        return this.adeIbOgg;
    }

    public void setIdMoviCreaz(int idMoviCreaz) {
        this.idMoviCreaz = idMoviCreaz;
    }

    public int getIdMoviCreaz() {
        return this.idMoviCreaz;
    }

    public WcomBsCallType getBsCallType() {
        return bsCallType;
    }

    public WcomDatiBlocco getDatiBlocco() {
        return datiBlocco;
    }

    public WcomDatiMbs getDatiMbs() {
        return datiMbs;
    }

    public WcomDatiPolizza getDatiPolizza() {
        return datiPolizza;
    }

    public WcomPrimaVolta getPrimaVolta() {
        return primaVolta;
    }

    public WcomStepSucc getStepSucc() {
        return stepSucc;
    }

    public WcomWrite getWrite() {
        return write;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_ADES = 5;
        public static final int ADE_IB_OGG = 40;
        public static final int DATI_ADESIONE = ID_ADES + ADE_IB_OGG;
        public static final int ID_MOVI_CREAZ = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_ADES = 9;
            public static final int ID_MOVI_CREAZ = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
