package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-QUOTZ-FND-UNIT<br>
 * Variable: IND-QUOTZ-FND-UNIT from copybook IDBVL192<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndQuotzFndUnit {

    //==== PROPERTIES ====
    //Original name: IND-L19-VAL-QUO
    private short valQuo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L19-VAL-QUO-MANFEE
    private short valQuoManfee = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L19-DT-RILEVAZIONE-NAV
    private short dtRilevazioneNav = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L19-VAL-QUO-ACQ
    private short valQuoAcq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-L19-FL-NO-NAV
    private short flNoNav = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setValQuo(short valQuo) {
        this.valQuo = valQuo;
    }

    public short getValQuo() {
        return this.valQuo;
    }

    public void setValQuoManfee(short valQuoManfee) {
        this.valQuoManfee = valQuoManfee;
    }

    public short getValQuoManfee() {
        return this.valQuoManfee;
    }

    public void setDtRilevazioneNav(short dtRilevazioneNav) {
        this.dtRilevazioneNav = dtRilevazioneNav;
    }

    public short getDtRilevazioneNav() {
        return this.dtRilevazioneNav;
    }

    public void setValQuoAcq(short valQuoAcq) {
        this.valQuoAcq = valQuoAcq;
    }

    public short getValQuoAcq() {
        return this.valQuoAcq;
    }

    public void setFlNoNav(short flNoNav) {
        this.flNoNav = flNoNav;
    }

    public short getFlNoNav() {
        return this.flNoNav;
    }
}
