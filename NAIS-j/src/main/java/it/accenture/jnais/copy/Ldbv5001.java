package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: LDBV5001<br>
 * Copybook: LDBV5001 from copybook LDBV5001<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Ldbv5001 {

    //==== PROPERTIES ====
    //Original name: LDBV5001-ID-RAPP-ANA
    private int ldbv5001IdRappAna = DefaultValues.INT_VAL;
    //Original name: LDBV5001-ID-COMP-QUEST
    private int ldbv5001IdCompQuest = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setLdbv5001Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV5001];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV5001);
        setLdbv5001Bytes(buffer, 1);
    }

    public String getLdbv5001Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv5001Bytes());
    }

    /**Original name: LDBV5001<br>*/
    public byte[] getLdbv5001Bytes() {
        byte[] buffer = new byte[Len.LDBV5001];
        return getLdbv5001Bytes(buffer, 1);
    }

    public void setLdbv5001Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv5001IdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV5001_ID_RAPP_ANA, 0);
        position += Len.LDBV5001_ID_RAPP_ANA;
        ldbv5001IdCompQuest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV5001_ID_COMP_QUEST, 0);
    }

    public byte[] getLdbv5001Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv5001IdRappAna, Len.Int.LDBV5001_ID_RAPP_ANA, 0);
        position += Len.LDBV5001_ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv5001IdCompQuest, Len.Int.LDBV5001_ID_COMP_QUEST, 0);
        return buffer;
    }

    public void setLdbv5001IdRappAna(int ldbv5001IdRappAna) {
        this.ldbv5001IdRappAna = ldbv5001IdRappAna;
    }

    public int getLdbv5001IdRappAna() {
        return this.ldbv5001IdRappAna;
    }

    public void setLdbv5001IdCompQuest(int ldbv5001IdCompQuest) {
        this.ldbv5001IdCompQuest = ldbv5001IdCompQuest;
    }

    public int getLdbv5001IdCompQuest() {
        return this.ldbv5001IdCompQuest;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV5001_ID_RAPP_ANA = 5;
        public static final int LDBV5001_ID_COMP_QUEST = 5;
        public static final int LDBV5001 = LDBV5001_ID_RAPP_ANA + LDBV5001_ID_COMP_QUEST;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV5001_ID_RAPP_ANA = 9;
            public static final int LDBV5001_ID_COMP_QUEST = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
