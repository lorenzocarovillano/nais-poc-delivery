package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.LorTipoOggetto;

/**Original name: LOG-ERRORE<br>
 * Variable: LOG-ERRORE from copybook IDBVLOR1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class LogErrore {

    //==== PROPERTIES ====
    //Original name: LOR-ID-LOG-ERRORE
    private int lorIdLogErrore = DefaultValues.INT_VAL;
    //Original name: LOR-PROG-LOG-ERRORE
    private int lorProgLogErrore = DefaultValues.INT_VAL;
    //Original name: LOR-ID-GRAVITA-ERRORE
    private int lorIdGravitaErrore = DefaultValues.INT_VAL;
    //Original name: LOR-DESC-ERRORE-ESTESA-LEN
    private short lorDescErroreEstesaLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: LOR-DESC-ERRORE-ESTESA
    private String lorDescErroreEstesa = DefaultValues.stringVal(Len.LOR_DESC_ERRORE_ESTESA);
    //Original name: LOR-COD-MAIN-BATCH
    private String lorCodMainBatch = DefaultValues.stringVal(Len.LOR_COD_MAIN_BATCH);
    //Original name: LOR-COD-SERVIZIO-BE
    private String lorCodServizioBe = DefaultValues.stringVal(Len.LOR_COD_SERVIZIO_BE);
    //Original name: LOR-LABEL-ERR
    private String lorLabelErr = DefaultValues.stringVal(Len.LOR_LABEL_ERR);
    //Original name: LOR-OPER-TABELLA
    private String lorOperTabella = DefaultValues.stringVal(Len.LOR_OPER_TABELLA);
    //Original name: LOR-NOME-TABELLA
    private String lorNomeTabella = DefaultValues.stringVal(Len.LOR_NOME_TABELLA);
    //Original name: LOR-STATUS-TABELLA
    private String lorStatusTabella = DefaultValues.stringVal(Len.LOR_STATUS_TABELLA);
    //Original name: LOR-KEY-TABELLA-LEN
    private short lorKeyTabellaLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: LOR-KEY-TABELLA
    private String lorKeyTabella = DefaultValues.stringVal(Len.LOR_KEY_TABELLA);
    //Original name: LOR-TIMESTAMP-REG
    private long lorTimestampReg = DefaultValues.LONG_VAL;
    //Original name: LOR-TIPO-OGGETTO
    private LorTipoOggetto lorTipoOggetto = new LorTipoOggetto();
    //Original name: LOR-IB-OGGETTO
    private String lorIbOggetto = DefaultValues.stringVal(Len.LOR_IB_OGGETTO);

    //==== METHODS ====
    public void setLorIdLogErrore(int lorIdLogErrore) {
        this.lorIdLogErrore = lorIdLogErrore;
    }

    public int getLorIdLogErrore() {
        return this.lorIdLogErrore;
    }

    public void setLorProgLogErrore(int lorProgLogErrore) {
        this.lorProgLogErrore = lorProgLogErrore;
    }

    public int getLorProgLogErrore() {
        return this.lorProgLogErrore;
    }

    public void setLorIdGravitaErrore(int lorIdGravitaErrore) {
        this.lorIdGravitaErrore = lorIdGravitaErrore;
    }

    public int getLorIdGravitaErrore() {
        return this.lorIdGravitaErrore;
    }

    public void setLorDescErroreEstesaVcharFormatted(String data) {
        byte[] buffer = new byte[Len.LOR_DESC_ERRORE_ESTESA_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.LOR_DESC_ERRORE_ESTESA_VCHAR);
        setLorDescErroreEstesaVcharBytes(buffer, 1);
    }

    public String getLorDescErroreEstesaVcharFormatted() {
        return MarshalByteExt.bufferToStr(getLorDescErroreEstesaVcharBytes());
    }

    /**Original name: LOR-DESC-ERRORE-ESTESA-VCHAR<br>*/
    public byte[] getLorDescErroreEstesaVcharBytes() {
        byte[] buffer = new byte[Len.LOR_DESC_ERRORE_ESTESA_VCHAR];
        return getLorDescErroreEstesaVcharBytes(buffer, 1);
    }

    public void setLorDescErroreEstesaVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        lorDescErroreEstesaLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        lorDescErroreEstesa = MarshalByte.readString(buffer, position, Len.LOR_DESC_ERRORE_ESTESA);
    }

    public byte[] getLorDescErroreEstesaVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, lorDescErroreEstesaLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, lorDescErroreEstesa, Len.LOR_DESC_ERRORE_ESTESA);
        return buffer;
    }

    public void setLorDescErroreEstesaLen(short lorDescErroreEstesaLen) {
        this.lorDescErroreEstesaLen = lorDescErroreEstesaLen;
    }

    public short getLorDescErroreEstesaLen() {
        return this.lorDescErroreEstesaLen;
    }

    public void setLorDescErroreEstesa(String lorDescErroreEstesa) {
        this.lorDescErroreEstesa = Functions.subString(lorDescErroreEstesa, Len.LOR_DESC_ERRORE_ESTESA);
    }

    public String getLorDescErroreEstesa() {
        return this.lorDescErroreEstesa;
    }

    public void setLorCodMainBatch(String lorCodMainBatch) {
        this.lorCodMainBatch = Functions.subString(lorCodMainBatch, Len.LOR_COD_MAIN_BATCH);
    }

    public String getLorCodMainBatch() {
        return this.lorCodMainBatch;
    }

    public void setLorCodServizioBe(String lorCodServizioBe) {
        this.lorCodServizioBe = Functions.subString(lorCodServizioBe, Len.LOR_COD_SERVIZIO_BE);
    }

    public String getLorCodServizioBe() {
        return this.lorCodServizioBe;
    }

    public void setLorLabelErr(String lorLabelErr) {
        this.lorLabelErr = Functions.subString(lorLabelErr, Len.LOR_LABEL_ERR);
    }

    public String getLorLabelErr() {
        return this.lorLabelErr;
    }

    public void setLorOperTabella(String lorOperTabella) {
        this.lorOperTabella = Functions.subString(lorOperTabella, Len.LOR_OPER_TABELLA);
    }

    public String getLorOperTabella() {
        return this.lorOperTabella;
    }

    public void setLorNomeTabella(String lorNomeTabella) {
        this.lorNomeTabella = Functions.subString(lorNomeTabella, Len.LOR_NOME_TABELLA);
    }

    public String getLorNomeTabella() {
        return this.lorNomeTabella;
    }

    public void setLorStatusTabella(String lorStatusTabella) {
        this.lorStatusTabella = Functions.subString(lorStatusTabella, Len.LOR_STATUS_TABELLA);
    }

    public String getLorStatusTabella() {
        return this.lorStatusTabella;
    }

    public void setLorKeyTabellaVcharFormatted(String data) {
        byte[] buffer = new byte[Len.LOR_KEY_TABELLA_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.LOR_KEY_TABELLA_VCHAR);
        setLorKeyTabellaVcharBytes(buffer, 1);
    }

    public String getLorKeyTabellaVcharFormatted() {
        return MarshalByteExt.bufferToStr(getLorKeyTabellaVcharBytes());
    }

    /**Original name: LOR-KEY-TABELLA-VCHAR<br>*/
    public byte[] getLorKeyTabellaVcharBytes() {
        byte[] buffer = new byte[Len.LOR_KEY_TABELLA_VCHAR];
        return getLorKeyTabellaVcharBytes(buffer, 1);
    }

    public void setLorKeyTabellaVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        lorKeyTabellaLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        lorKeyTabella = MarshalByte.readString(buffer, position, Len.LOR_KEY_TABELLA);
    }

    public byte[] getLorKeyTabellaVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, lorKeyTabellaLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, lorKeyTabella, Len.LOR_KEY_TABELLA);
        return buffer;
    }

    public void setLorKeyTabellaLen(short lorKeyTabellaLen) {
        this.lorKeyTabellaLen = lorKeyTabellaLen;
    }

    public short getLorKeyTabellaLen() {
        return this.lorKeyTabellaLen;
    }

    public void setLorKeyTabella(String lorKeyTabella) {
        this.lorKeyTabella = Functions.subString(lorKeyTabella, Len.LOR_KEY_TABELLA);
    }

    public String getLorKeyTabella() {
        return this.lorKeyTabella;
    }

    public void setLorTimestampReg(long lorTimestampReg) {
        this.lorTimestampReg = lorTimestampReg;
    }

    public long getLorTimestampReg() {
        return this.lorTimestampReg;
    }

    public void setLorIbOggetto(String lorIbOggetto) {
        this.lorIbOggetto = Functions.subString(lorIbOggetto, Len.LOR_IB_OGGETTO);
    }

    public String getLorIbOggetto() {
        return this.lorIbOggetto;
    }

    public LorTipoOggetto getLorTipoOggetto() {
        return lorTipoOggetto;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LOR_DESC_ERRORE_ESTESA_LEN = 2;
        public static final int LOR_DESC_ERRORE_ESTESA = 200;
        public static final int LOR_DESC_ERRORE_ESTESA_VCHAR = LOR_DESC_ERRORE_ESTESA_LEN + LOR_DESC_ERRORE_ESTESA;
        public static final int LOR_COD_MAIN_BATCH = 8;
        public static final int LOR_COD_SERVIZIO_BE = 8;
        public static final int LOR_LABEL_ERR = 30;
        public static final int LOR_OPER_TABELLA = 2;
        public static final int LOR_NOME_TABELLA = 18;
        public static final int LOR_STATUS_TABELLA = 10;
        public static final int LOR_KEY_TABELLA_LEN = 2;
        public static final int LOR_KEY_TABELLA = 100;
        public static final int LOR_KEY_TABELLA_VCHAR = LOR_KEY_TABELLA_LEN + LOR_KEY_TABELLA;
        public static final int LOR_IB_OGGETTO = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
