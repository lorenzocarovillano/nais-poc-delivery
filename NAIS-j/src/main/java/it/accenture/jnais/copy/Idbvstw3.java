package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVSTW3<br>
 * Copybook: IDBVSTW3 from copybook IDBVSTW3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvstw3 {

    //==== PROPERTIES ====
    //Original name: STW-DT-INI-EFF-DB
    private String stwDtIniEffDb = DefaultValues.stringVal(Len.STW_DT_INI_EFF_DB);
    //Original name: STW-DT-END-EFF-DB
    private String stwDtEndEffDb = DefaultValues.stringVal(Len.STW_DT_END_EFF_DB);

    //==== METHODS ====
    public void setStwDtIniEffDb(String stwDtIniEffDb) {
        this.stwDtIniEffDb = Functions.subString(stwDtIniEffDb, Len.STW_DT_INI_EFF_DB);
    }

    public String getStwDtIniEffDb() {
        return this.stwDtIniEffDb;
    }

    public void setStwDtEndEffDb(String stwDtEndEffDb) {
        this.stwDtEndEffDb = Functions.subString(stwDtEndEffDb, Len.STW_DT_END_EFF_DB);
    }

    public String getStwDtEndEffDb() {
        return this.stwDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int STW_DT_INI_EFF_DB = 10;
        public static final int STW_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
