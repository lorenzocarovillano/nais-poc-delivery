package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.MvvIdpMatrValVar;
import it.accenture.jnais.ws.redefines.MvvTipoMovimento;

/**Original name: MATR-VAL-VAR<br>
 * Variable: MATR-VAL-VAR from copybook IDBVMVV1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class MatrValVar {

    //==== PROPERTIES ====
    //Original name: MVV-ID-MATR-VAL-VAR
    private int mvvIdMatrValVar = DefaultValues.BIN_INT_VAL;
    //Original name: MVV-IDP-MATR-VAL-VAR
    private MvvIdpMatrValVar mvvIdpMatrValVar = new MvvIdpMatrValVar();
    //Original name: MVV-COD-COMPAGNIA-ANIA
    private int mvvCodCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: MVV-TIPO-MOVIMENTO
    private MvvTipoMovimento mvvTipoMovimento = new MvvTipoMovimento();
    //Original name: MVV-COD-DATO-EXT
    private String mvvCodDatoExt = DefaultValues.stringVal(Len.MVV_COD_DATO_EXT);
    //Original name: MVV-OBBLIGATORIETA
    private char mvvObbligatorieta = DefaultValues.CHAR_VAL;
    //Original name: MVV-VALORE-DEFAULT
    private String mvvValoreDefault = DefaultValues.stringVal(Len.MVV_VALORE_DEFAULT);
    //Original name: MVV-COD-STR-DATO-PTF
    private String mvvCodStrDatoPtf = DefaultValues.stringVal(Len.MVV_COD_STR_DATO_PTF);
    //Original name: MVV-COD-DATO-PTF
    private String mvvCodDatoPtf = DefaultValues.stringVal(Len.MVV_COD_DATO_PTF);
    //Original name: MVV-COD-PARAMETRO
    private String mvvCodParametro = DefaultValues.stringVal(Len.MVV_COD_PARAMETRO);
    //Original name: MVV-OPERAZIONE
    private String mvvOperazione = DefaultValues.stringVal(Len.MVV_OPERAZIONE);
    //Original name: MVV-LIVELLO-OPERAZIONE
    private String mvvLivelloOperazione = DefaultValues.stringVal(Len.MVV_LIVELLO_OPERAZIONE);
    //Original name: MVV-TIPO-OGGETTO
    private String mvvTipoOggetto = DefaultValues.stringVal(Len.MVV_TIPO_OGGETTO);
    //Original name: MVV-WHERE-CONDITION-LEN
    private short mvvWhereConditionLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: MVV-WHERE-CONDITION
    private String mvvWhereCondition = DefaultValues.stringVal(Len.MVV_WHERE_CONDITION);
    //Original name: MVV-SERVIZIO-LETTURA
    private String mvvServizioLettura = DefaultValues.stringVal(Len.MVV_SERVIZIO_LETTURA);
    //Original name: MVV-MODULO-CALCOLO
    private String mvvModuloCalcolo = DefaultValues.stringVal(Len.MVV_MODULO_CALCOLO);
    //Original name: MVV-STEP-VALORIZZATORE
    private char mvvStepValorizzatore = DefaultValues.CHAR_VAL;
    //Original name: MVV-STEP-CONVERSAZIONE
    private char mvvStepConversazione = DefaultValues.CHAR_VAL;
    //Original name: MVV-OPER-LOG-STATO-BUS
    private String mvvOperLogStatoBus = DefaultValues.stringVal(Len.MVV_OPER_LOG_STATO_BUS);
    //Original name: MVV-ARRAY-STATO-BUS
    private String mvvArrayStatoBus = DefaultValues.stringVal(Len.MVV_ARRAY_STATO_BUS);
    //Original name: MVV-OPER-LOG-CAUSALE
    private String mvvOperLogCausale = DefaultValues.stringVal(Len.MVV_OPER_LOG_CAUSALE);
    //Original name: MVV-ARRAY-CAUSALE
    private String mvvArrayCausale = DefaultValues.stringVal(Len.MVV_ARRAY_CAUSALE);
    //Original name: MVV-COD-DATO-INTERNO
    private String mvvCodDatoInterno = DefaultValues.stringVal(Len.MVV_COD_DATO_INTERNO);

    //==== METHODS ====
    public void setMvvIdMatrValVar(int mvvIdMatrValVar) {
        this.mvvIdMatrValVar = mvvIdMatrValVar;
    }

    public int getMvvIdMatrValVar() {
        return this.mvvIdMatrValVar;
    }

    public void setMvvCodCompagniaAnia(int mvvCodCompagniaAnia) {
        this.mvvCodCompagniaAnia = mvvCodCompagniaAnia;
    }

    public int getMvvCodCompagniaAnia() {
        return this.mvvCodCompagniaAnia;
    }

    public void setMvvCodDatoExt(String mvvCodDatoExt) {
        this.mvvCodDatoExt = Functions.subString(mvvCodDatoExt, Len.MVV_COD_DATO_EXT);
    }

    public String getMvvCodDatoExt() {
        return this.mvvCodDatoExt;
    }

    public void setMvvObbligatorieta(char mvvObbligatorieta) {
        this.mvvObbligatorieta = mvvObbligatorieta;
    }

    public char getMvvObbligatorieta() {
        return this.mvvObbligatorieta;
    }

    public void setMvvValoreDefault(String mvvValoreDefault) {
        this.mvvValoreDefault = Functions.subString(mvvValoreDefault, Len.MVV_VALORE_DEFAULT);
    }

    public String getMvvValoreDefault() {
        return this.mvvValoreDefault;
    }

    public void setMvvCodStrDatoPtf(String mvvCodStrDatoPtf) {
        this.mvvCodStrDatoPtf = Functions.subString(mvvCodStrDatoPtf, Len.MVV_COD_STR_DATO_PTF);
    }

    public String getMvvCodStrDatoPtf() {
        return this.mvvCodStrDatoPtf;
    }

    public void setMvvCodDatoPtf(String mvvCodDatoPtf) {
        this.mvvCodDatoPtf = Functions.subString(mvvCodDatoPtf, Len.MVV_COD_DATO_PTF);
    }

    public String getMvvCodDatoPtf() {
        return this.mvvCodDatoPtf;
    }

    public void setMvvCodParametro(String mvvCodParametro) {
        this.mvvCodParametro = Functions.subString(mvvCodParametro, Len.MVV_COD_PARAMETRO);
    }

    public String getMvvCodParametro() {
        return this.mvvCodParametro;
    }

    public void setMvvOperazione(String mvvOperazione) {
        this.mvvOperazione = Functions.subString(mvvOperazione, Len.MVV_OPERAZIONE);
    }

    public String getMvvOperazione() {
        return this.mvvOperazione;
    }

    public void setMvvLivelloOperazione(String mvvLivelloOperazione) {
        this.mvvLivelloOperazione = Functions.subString(mvvLivelloOperazione, Len.MVV_LIVELLO_OPERAZIONE);
    }

    public String getMvvLivelloOperazione() {
        return this.mvvLivelloOperazione;
    }

    public String getMvvLivelloOperazioneFormatted() {
        return Functions.padBlanks(getMvvLivelloOperazione(), Len.MVV_LIVELLO_OPERAZIONE);
    }

    public void setMvvTipoOggetto(String mvvTipoOggetto) {
        this.mvvTipoOggetto = Functions.subString(mvvTipoOggetto, Len.MVV_TIPO_OGGETTO);
    }

    public String getMvvTipoOggetto() {
        return this.mvvTipoOggetto;
    }

    public String getMvvTipoOggettoFormatted() {
        return Functions.padBlanks(getMvvTipoOggetto(), Len.MVV_TIPO_OGGETTO);
    }

    public void setMvvWhereConditionVcharFormatted(String data) {
        byte[] buffer = new byte[Len.MVV_WHERE_CONDITION_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.MVV_WHERE_CONDITION_VCHAR);
        setMvvWhereConditionVcharBytes(buffer, 1);
    }

    public String getMvvWhereConditionVcharFormatted() {
        return MarshalByteExt.bufferToStr(getMvvWhereConditionVcharBytes());
    }

    /**Original name: MVV-WHERE-CONDITION-VCHAR<br>*/
    public byte[] getMvvWhereConditionVcharBytes() {
        byte[] buffer = new byte[Len.MVV_WHERE_CONDITION_VCHAR];
        return getMvvWhereConditionVcharBytes(buffer, 1);
    }

    public void setMvvWhereConditionVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        mvvWhereConditionLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        mvvWhereCondition = MarshalByte.readString(buffer, position, Len.MVV_WHERE_CONDITION);
    }

    public byte[] getMvvWhereConditionVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, mvvWhereConditionLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, mvvWhereCondition, Len.MVV_WHERE_CONDITION);
        return buffer;
    }

    public void setMvvWhereConditionLen(short mvvWhereConditionLen) {
        this.mvvWhereConditionLen = mvvWhereConditionLen;
    }

    public short getMvvWhereConditionLen() {
        return this.mvvWhereConditionLen;
    }

    public void setMvvWhereCondition(String mvvWhereCondition) {
        this.mvvWhereCondition = Functions.subString(mvvWhereCondition, Len.MVV_WHERE_CONDITION);
    }

    public String getMvvWhereCondition() {
        return this.mvvWhereCondition;
    }

    public void setMvvServizioLettura(String mvvServizioLettura) {
        this.mvvServizioLettura = Functions.subString(mvvServizioLettura, Len.MVV_SERVIZIO_LETTURA);
    }

    public String getMvvServizioLettura() {
        return this.mvvServizioLettura;
    }

    public String getMvvServizioLetturaFormatted() {
        return Functions.padBlanks(getMvvServizioLettura(), Len.MVV_SERVIZIO_LETTURA);
    }

    public void setMvvModuloCalcolo(String mvvModuloCalcolo) {
        this.mvvModuloCalcolo = Functions.subString(mvvModuloCalcolo, Len.MVV_MODULO_CALCOLO);
    }

    public String getMvvModuloCalcolo() {
        return this.mvvModuloCalcolo;
    }

    public String getMvvModuloCalcoloFormatted() {
        return Functions.padBlanks(getMvvModuloCalcolo(), Len.MVV_MODULO_CALCOLO);
    }

    public void setMvvStepValorizzatore(char mvvStepValorizzatore) {
        this.mvvStepValorizzatore = mvvStepValorizzatore;
    }

    public char getMvvStepValorizzatore() {
        return this.mvvStepValorizzatore;
    }

    public void setMvvStepConversazione(char mvvStepConversazione) {
        this.mvvStepConversazione = mvvStepConversazione;
    }

    public char getMvvStepConversazione() {
        return this.mvvStepConversazione;
    }

    public void setMvvOperLogStatoBus(String mvvOperLogStatoBus) {
        this.mvvOperLogStatoBus = Functions.subString(mvvOperLogStatoBus, Len.MVV_OPER_LOG_STATO_BUS);
    }

    public String getMvvOperLogStatoBus() {
        return this.mvvOperLogStatoBus;
    }

    public void setMvvArrayStatoBus(String mvvArrayStatoBus) {
        this.mvvArrayStatoBus = Functions.subString(mvvArrayStatoBus, Len.MVV_ARRAY_STATO_BUS);
    }

    public String getMvvArrayStatoBus() {
        return this.mvvArrayStatoBus;
    }

    public void setMvvOperLogCausale(String mvvOperLogCausale) {
        this.mvvOperLogCausale = Functions.subString(mvvOperLogCausale, Len.MVV_OPER_LOG_CAUSALE);
    }

    public String getMvvOperLogCausale() {
        return this.mvvOperLogCausale;
    }

    public void setMvvArrayCausale(String mvvArrayCausale) {
        this.mvvArrayCausale = Functions.subString(mvvArrayCausale, Len.MVV_ARRAY_CAUSALE);
    }

    public String getMvvArrayCausale() {
        return this.mvvArrayCausale;
    }

    public void setMvvCodDatoInterno(String mvvCodDatoInterno) {
        this.mvvCodDatoInterno = Functions.subString(mvvCodDatoInterno, Len.MVV_COD_DATO_INTERNO);
    }

    public String getMvvCodDatoInterno() {
        return this.mvvCodDatoInterno;
    }

    public MvvIdpMatrValVar getMvvIdpMatrValVar() {
        return mvvIdpMatrValVar;
    }

    public MvvTipoMovimento getMvvTipoMovimento() {
        return mvvTipoMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MVV_COD_DATO_EXT = 30;
        public static final int MVV_VALORE_DEFAULT = 50;
        public static final int MVV_COD_STR_DATO_PTF = 30;
        public static final int MVV_COD_DATO_PTF = 30;
        public static final int MVV_COD_PARAMETRO = 20;
        public static final int MVV_OPERAZIONE = 15;
        public static final int MVV_LIVELLO_OPERAZIONE = 3;
        public static final int MVV_TIPO_OGGETTO = 2;
        public static final int MVV_WHERE_CONDITION_LEN = 2;
        public static final int MVV_WHERE_CONDITION = 300;
        public static final int MVV_WHERE_CONDITION_VCHAR = MVV_WHERE_CONDITION_LEN + MVV_WHERE_CONDITION;
        public static final int MVV_SERVIZIO_LETTURA = 8;
        public static final int MVV_MODULO_CALCOLO = 8;
        public static final int MVV_COD_DATO_INTERNO = 30;
        public static final int MVV_OPER_LOG_STATO_BUS = 3;
        public static final int MVV_ARRAY_STATO_BUS = 20;
        public static final int MVV_OPER_LOG_CAUSALE = 3;
        public static final int MVV_ARRAY_CAUSALE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
