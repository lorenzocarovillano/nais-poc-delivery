package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.commons.data.to.ISinonimoStr;

/**Original name: SINONIMO-STR<br>
 * Variable: SINONIMO-STR from copybook IDBVSST1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SinonimoStr implements ISinonimoStr {

    //==== PROPERTIES ====
    //Original name: SST-COD-COMPAGNIA-ANIA
    private int codCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: SST-COD-STR-DATO
    private String codStrDato = DefaultValues.stringVal(Len.COD_STR_DATO);
    //Original name: SST-FLAG-MOD-ESECUTIVA
    private char flagModEsecutiva = DefaultValues.CHAR_VAL;
    //Original name: SST-OPERAZIONE
    private String operazione = DefaultValues.stringVal(Len.OPERAZIONE);
    //Original name: SST-COD-SIN-STR-DATO
    private String codSinStrDato = DefaultValues.stringVal(Len.COD_SIN_STR_DATO);

    //==== METHODS ====
    public void setCodCompagniaAnia(int codCompagniaAnia) {
        this.codCompagniaAnia = codCompagniaAnia;
    }

    public int getCodCompagniaAnia() {
        return this.codCompagniaAnia;
    }

    public void setCodStrDato(String codStrDato) {
        this.codStrDato = Functions.subString(codStrDato, Len.COD_STR_DATO);
    }

    public String getCodStrDato() {
        return this.codStrDato;
    }

    public void setFlagModEsecutiva(char flagModEsecutiva) {
        this.flagModEsecutiva = flagModEsecutiva;
    }

    public void setFlagModEsecutivaFormatted(String flagModEsecutiva) {
        setFlagModEsecutiva(Functions.charAt(flagModEsecutiva, Types.CHAR_SIZE));
    }

    public char getFlagModEsecutiva() {
        return this.flagModEsecutiva;
    }

    public void setOperazione(String operazione) {
        this.operazione = Functions.subString(operazione, Len.OPERAZIONE);
    }

    public String getOperazione() {
        return this.operazione;
    }

    public String getCodSinStrDato() {
        return this.codSinStrDato;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_STR_DATO = 30;
        public static final int OPERAZIONE = 15;
        public static final int COD_SIN_STR_DATO = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
