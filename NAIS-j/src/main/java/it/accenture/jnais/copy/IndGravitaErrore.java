package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-GRAVITA-ERRORE<br>
 * Variable: IND-GRAVITA-ERRORE from copybook IDBVGER2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndGravitaErrore {

    //==== PROPERTIES ====
    //Original name: IND-GER-COD-COMPAGNIA-ANIA
    private short codCompagniaAnia = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GER-COD-CONV-MAIN-BTC
    private short codConvMainBtc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GER-COD-PAGINA-SERV-BE
    private short codPaginaServBe = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GER-KEY-FILTRO2
    private short keyFiltro2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GER-KEY-FILTRO1
    private short keyFiltro1 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GER-KEY-FILTRO3
    private short keyFiltro3 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GER-KEY-FILTRO4
    private short keyFiltro4 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GER-KEY-FILTRO5
    private short keyFiltro5 = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setDtStart(short dtStart) {
        this.codCompagniaAnia = dtStart;
    }

    public short getDtStart() {
        return this.codCompagniaAnia;
    }

    public void setDtEnd(short dtEnd) {
        this.codConvMainBtc = dtEnd;
    }

    public short getDtEnd() {
        return this.codConvMainBtc;
    }

    public void setUserStart(short userStart) {
        this.codPaginaServBe = userStart;
    }

    public short getUserStart() {
        return this.codPaginaServBe;
    }

    public void setDescParallelism(short descParallelism) {
        this.keyFiltro2 = descParallelism;
    }

    public short getDescParallelism() {
        return this.keyFiltro2;
    }

    public void setIdOggDa(short idOggDa) {
        this.keyFiltro1 = idOggDa;
    }

    public short getIdOggDa() {
        return this.keyFiltro1;
    }

    public void setIdOggA(short idOggA) {
        this.keyFiltro3 = idOggA;
    }

    public short getIdOggA() {
        return this.keyFiltro3;
    }

    public void setTpOgg(short tpOgg) {
        this.keyFiltro4 = tpOgg;
    }

    public short getTpOgg() {
        return this.keyFiltro4;
    }

    public void setNumRowSchedule(short numRowSchedule) {
        this.keyFiltro5 = numRowSchedule;
    }

    public short getNumRowSchedule() {
        return this.keyFiltro5;
    }
}
