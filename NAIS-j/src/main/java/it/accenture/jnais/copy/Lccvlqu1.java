package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVLQU1<br>
 * Variable: LCCVLQU1 from copybook LCCVLQU1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvlqu1 {

    //==== PROPERTIES ====
    /**Original name: S089-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA LIQ
	 *    ALIAS LQU
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: S089-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: S089-DATI
    private S089Dati dati = new S089Dati();

    //==== METHODS ====
    public void initLccvlqu1Spaces() {
        status.setStatus(Types.SPACE_CHAR);
        idPtf = Types.INVALID_INT_VAL;
        dati.initWlquDatiSpaces();
    }

    public void setWlquIdPtf(int wlquIdPtf) {
        this.idPtf = wlquIdPtf;
    }

    public int getWlquIdPtf() {
        return this.idPtf;
    }

    public S089Dati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WLQU_ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
