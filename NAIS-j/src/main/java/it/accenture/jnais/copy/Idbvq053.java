package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVQ053<br>
 * Copybook: IDBVQ053 from copybook IDBVQ053<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvq053 {

    //==== PROPERTIES ====
    //Original name: Q05-DT-INI-EFF-DB
    private String q05DtIniEffDb = DefaultValues.stringVal(Len.Q05_DT_INI_EFF_DB);
    //Original name: Q05-DT-END-EFF-DB
    private String q05DtEndEffDb = DefaultValues.stringVal(Len.Q05_DT_END_EFF_DB);

    //==== METHODS ====
    public void setQ05DtIniEffDb(String q05DtIniEffDb) {
        this.q05DtIniEffDb = Functions.subString(q05DtIniEffDb, Len.Q05_DT_INI_EFF_DB);
    }

    public String getQ05DtIniEffDb() {
        return this.q05DtIniEffDb;
    }

    public void setQ05DtEndEffDb(String q05DtEndEffDb) {
        this.q05DtEndEffDb = Functions.subString(q05DtEndEffDb, Len.Q05_DT_END_EFF_DB);
    }

    public String getQ05DtEndEffDb() {
        return this.q05DtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int Q05_DT_INI_EFF_DB = 10;
        public static final int Q05_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
