package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WallDtEndVldt;
import it.accenture.jnais.ws.redefines.WallIdMoviChiu;
import it.accenture.jnais.ws.redefines.WallPcRipAst;
import it.accenture.jnais.ws.redefines.WallPeriodo;

/**Original name: WALL-DATI<br>
 * Variable: WALL-DATI from copybook LCCVALL1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WallDati {

    //==== PROPERTIES ====
    //Original name: WALL-ID-AST-ALLOC
    private int wallIdAstAlloc = DefaultValues.INT_VAL;
    //Original name: WALL-ID-STRA-DI-INVST
    private int wallIdStraDiInvst = DefaultValues.INT_VAL;
    //Original name: WALL-ID-MOVI-CRZ
    private int wallIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WALL-ID-MOVI-CHIU
    private WallIdMoviChiu wallIdMoviChiu = new WallIdMoviChiu();
    //Original name: WALL-DT-INI-VLDT
    private int wallDtIniVldt = DefaultValues.INT_VAL;
    //Original name: WALL-DT-END-VLDT
    private WallDtEndVldt wallDtEndVldt = new WallDtEndVldt();
    //Original name: WALL-DT-INI-EFF
    private int wallDtIniEff = DefaultValues.INT_VAL;
    //Original name: WALL-DT-END-EFF
    private int wallDtEndEff = DefaultValues.INT_VAL;
    //Original name: WALL-COD-COMP-ANIA
    private int wallCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WALL-COD-FND
    private String wallCodFnd = DefaultValues.stringVal(Len.WALL_COD_FND);
    //Original name: WALL-COD-TARI
    private String wallCodTari = DefaultValues.stringVal(Len.WALL_COD_TARI);
    //Original name: WALL-TP-APPLZ-AST
    private String wallTpApplzAst = DefaultValues.stringVal(Len.WALL_TP_APPLZ_AST);
    //Original name: WALL-PC-RIP-AST
    private WallPcRipAst wallPcRipAst = new WallPcRipAst();
    //Original name: WALL-TP-FND
    private char wallTpFnd = DefaultValues.CHAR_VAL;
    //Original name: WALL-DS-RIGA
    private long wallDsRiga = DefaultValues.LONG_VAL;
    //Original name: WALL-DS-OPER-SQL
    private char wallDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WALL-DS-VER
    private int wallDsVer = DefaultValues.INT_VAL;
    //Original name: WALL-DS-TS-INI-CPTZ
    private long wallDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WALL-DS-TS-END-CPTZ
    private long wallDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WALL-DS-UTENTE
    private String wallDsUtente = DefaultValues.stringVal(Len.WALL_DS_UTENTE);
    //Original name: WALL-DS-STATO-ELAB
    private char wallDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WALL-PERIODO
    private WallPeriodo wallPeriodo = new WallPeriodo();
    //Original name: WALL-TP-RIBIL-FND
    private String wallTpRibilFnd = DefaultValues.stringVal(Len.WALL_TP_RIBIL_FND);

    //==== METHODS ====
    public void setWallDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wallIdAstAlloc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WALL_ID_AST_ALLOC, 0);
        position += Len.WALL_ID_AST_ALLOC;
        wallIdStraDiInvst = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WALL_ID_STRA_DI_INVST, 0);
        position += Len.WALL_ID_STRA_DI_INVST;
        wallIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WALL_ID_MOVI_CRZ, 0);
        position += Len.WALL_ID_MOVI_CRZ;
        wallIdMoviChiu.setWallIdMoviChiuFromBuffer(buffer, position);
        position += WallIdMoviChiu.Len.WALL_ID_MOVI_CHIU;
        wallDtIniVldt = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WALL_DT_INI_VLDT, 0);
        position += Len.WALL_DT_INI_VLDT;
        wallDtEndVldt.setWallDtEndVldtFromBuffer(buffer, position);
        position += WallDtEndVldt.Len.WALL_DT_END_VLDT;
        wallDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WALL_DT_INI_EFF, 0);
        position += Len.WALL_DT_INI_EFF;
        wallDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WALL_DT_END_EFF, 0);
        position += Len.WALL_DT_END_EFF;
        wallCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WALL_COD_COMP_ANIA, 0);
        position += Len.WALL_COD_COMP_ANIA;
        wallCodFnd = MarshalByte.readString(buffer, position, Len.WALL_COD_FND);
        position += Len.WALL_COD_FND;
        wallCodTari = MarshalByte.readString(buffer, position, Len.WALL_COD_TARI);
        position += Len.WALL_COD_TARI;
        wallTpApplzAst = MarshalByte.readString(buffer, position, Len.WALL_TP_APPLZ_AST);
        position += Len.WALL_TP_APPLZ_AST;
        wallPcRipAst.setWallPcRipAstFromBuffer(buffer, position);
        position += WallPcRipAst.Len.WALL_PC_RIP_AST;
        wallTpFnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wallDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WALL_DS_RIGA, 0);
        position += Len.WALL_DS_RIGA;
        wallDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wallDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WALL_DS_VER, 0);
        position += Len.WALL_DS_VER;
        wallDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WALL_DS_TS_INI_CPTZ, 0);
        position += Len.WALL_DS_TS_INI_CPTZ;
        wallDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WALL_DS_TS_END_CPTZ, 0);
        position += Len.WALL_DS_TS_END_CPTZ;
        wallDsUtente = MarshalByte.readString(buffer, position, Len.WALL_DS_UTENTE);
        position += Len.WALL_DS_UTENTE;
        wallDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wallPeriodo.setWallPeriodoFromBuffer(buffer, position);
        position += WallPeriodo.Len.WALL_PERIODO;
        wallTpRibilFnd = MarshalByte.readString(buffer, position, Len.WALL_TP_RIBIL_FND);
    }

    public byte[] getWallDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wallIdAstAlloc, Len.Int.WALL_ID_AST_ALLOC, 0);
        position += Len.WALL_ID_AST_ALLOC;
        MarshalByte.writeIntAsPacked(buffer, position, wallIdStraDiInvst, Len.Int.WALL_ID_STRA_DI_INVST, 0);
        position += Len.WALL_ID_STRA_DI_INVST;
        MarshalByte.writeIntAsPacked(buffer, position, wallIdMoviCrz, Len.Int.WALL_ID_MOVI_CRZ, 0);
        position += Len.WALL_ID_MOVI_CRZ;
        wallIdMoviChiu.getWallIdMoviChiuAsBuffer(buffer, position);
        position += WallIdMoviChiu.Len.WALL_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wallDtIniVldt, Len.Int.WALL_DT_INI_VLDT, 0);
        position += Len.WALL_DT_INI_VLDT;
        wallDtEndVldt.getWallDtEndVldtAsBuffer(buffer, position);
        position += WallDtEndVldt.Len.WALL_DT_END_VLDT;
        MarshalByte.writeIntAsPacked(buffer, position, wallDtIniEff, Len.Int.WALL_DT_INI_EFF, 0);
        position += Len.WALL_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wallDtEndEff, Len.Int.WALL_DT_END_EFF, 0);
        position += Len.WALL_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wallCodCompAnia, Len.Int.WALL_COD_COMP_ANIA, 0);
        position += Len.WALL_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wallCodFnd, Len.WALL_COD_FND);
        position += Len.WALL_COD_FND;
        MarshalByte.writeString(buffer, position, wallCodTari, Len.WALL_COD_TARI);
        position += Len.WALL_COD_TARI;
        MarshalByte.writeString(buffer, position, wallTpApplzAst, Len.WALL_TP_APPLZ_AST);
        position += Len.WALL_TP_APPLZ_AST;
        wallPcRipAst.getWallPcRipAstAsBuffer(buffer, position);
        position += WallPcRipAst.Len.WALL_PC_RIP_AST;
        MarshalByte.writeChar(buffer, position, wallTpFnd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, wallDsRiga, Len.Int.WALL_DS_RIGA, 0);
        position += Len.WALL_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wallDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wallDsVer, Len.Int.WALL_DS_VER, 0);
        position += Len.WALL_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wallDsTsIniCptz, Len.Int.WALL_DS_TS_INI_CPTZ, 0);
        position += Len.WALL_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wallDsTsEndCptz, Len.Int.WALL_DS_TS_END_CPTZ, 0);
        position += Len.WALL_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wallDsUtente, Len.WALL_DS_UTENTE);
        position += Len.WALL_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wallDsStatoElab);
        position += Types.CHAR_SIZE;
        wallPeriodo.getWallPeriodoAsBuffer(buffer, position);
        position += WallPeriodo.Len.WALL_PERIODO;
        MarshalByte.writeString(buffer, position, wallTpRibilFnd, Len.WALL_TP_RIBIL_FND);
        return buffer;
    }

    public void initWallDatiSpaces() {
        wallIdAstAlloc = Types.INVALID_INT_VAL;
        wallIdStraDiInvst = Types.INVALID_INT_VAL;
        wallIdMoviCrz = Types.INVALID_INT_VAL;
        wallIdMoviChiu.initWallIdMoviChiuSpaces();
        wallDtIniVldt = Types.INVALID_INT_VAL;
        wallDtEndVldt.initWallDtEndVldtSpaces();
        wallDtIniEff = Types.INVALID_INT_VAL;
        wallDtEndEff = Types.INVALID_INT_VAL;
        wallCodCompAnia = Types.INVALID_INT_VAL;
        wallCodFnd = "";
        wallCodTari = "";
        wallTpApplzAst = "";
        wallPcRipAst.initWallPcRipAstSpaces();
        wallTpFnd = Types.SPACE_CHAR;
        wallDsRiga = Types.INVALID_LONG_VAL;
        wallDsOperSql = Types.SPACE_CHAR;
        wallDsVer = Types.INVALID_INT_VAL;
        wallDsTsIniCptz = Types.INVALID_LONG_VAL;
        wallDsTsEndCptz = Types.INVALID_LONG_VAL;
        wallDsUtente = "";
        wallDsStatoElab = Types.SPACE_CHAR;
        wallPeriodo.initWallPeriodoSpaces();
        wallTpRibilFnd = "";
    }

    public void setWallIdAstAlloc(int wallIdAstAlloc) {
        this.wallIdAstAlloc = wallIdAstAlloc;
    }

    public int getWallIdAstAlloc() {
        return this.wallIdAstAlloc;
    }

    public void setWallIdStraDiInvst(int wallIdStraDiInvst) {
        this.wallIdStraDiInvst = wallIdStraDiInvst;
    }

    public int getWallIdStraDiInvst() {
        return this.wallIdStraDiInvst;
    }

    public void setWallIdMoviCrz(int wallIdMoviCrz) {
        this.wallIdMoviCrz = wallIdMoviCrz;
    }

    public int getWallIdMoviCrz() {
        return this.wallIdMoviCrz;
    }

    public void setWallDtIniVldt(int wallDtIniVldt) {
        this.wallDtIniVldt = wallDtIniVldt;
    }

    public int getWallDtIniVldt() {
        return this.wallDtIniVldt;
    }

    public void setWallDtIniEff(int wallDtIniEff) {
        this.wallDtIniEff = wallDtIniEff;
    }

    public int getWallDtIniEff() {
        return this.wallDtIniEff;
    }

    public void setWallDtEndEff(int wallDtEndEff) {
        this.wallDtEndEff = wallDtEndEff;
    }

    public int getWallDtEndEff() {
        return this.wallDtEndEff;
    }

    public void setWallCodCompAnia(int wallCodCompAnia) {
        this.wallCodCompAnia = wallCodCompAnia;
    }

    public int getWallCodCompAnia() {
        return this.wallCodCompAnia;
    }

    public void setWallCodFnd(String wallCodFnd) {
        this.wallCodFnd = Functions.subString(wallCodFnd, Len.WALL_COD_FND);
    }

    public String getWallCodFnd() {
        return this.wallCodFnd;
    }

    public void setWallCodTari(String wallCodTari) {
        this.wallCodTari = Functions.subString(wallCodTari, Len.WALL_COD_TARI);
    }

    public String getWallCodTari() {
        return this.wallCodTari;
    }

    public void setWallTpApplzAst(String wallTpApplzAst) {
        this.wallTpApplzAst = Functions.subString(wallTpApplzAst, Len.WALL_TP_APPLZ_AST);
    }

    public String getWallTpApplzAst() {
        return this.wallTpApplzAst;
    }

    public void setWallTpFnd(char wallTpFnd) {
        this.wallTpFnd = wallTpFnd;
    }

    public char getWallTpFnd() {
        return this.wallTpFnd;
    }

    public void setWallDsRiga(long wallDsRiga) {
        this.wallDsRiga = wallDsRiga;
    }

    public long getWallDsRiga() {
        return this.wallDsRiga;
    }

    public void setWallDsOperSql(char wallDsOperSql) {
        this.wallDsOperSql = wallDsOperSql;
    }

    public char getWallDsOperSql() {
        return this.wallDsOperSql;
    }

    public void setWallDsVer(int wallDsVer) {
        this.wallDsVer = wallDsVer;
    }

    public int getWallDsVer() {
        return this.wallDsVer;
    }

    public void setWallDsTsIniCptz(long wallDsTsIniCptz) {
        this.wallDsTsIniCptz = wallDsTsIniCptz;
    }

    public long getWallDsTsIniCptz() {
        return this.wallDsTsIniCptz;
    }

    public void setWallDsTsEndCptz(long wallDsTsEndCptz) {
        this.wallDsTsEndCptz = wallDsTsEndCptz;
    }

    public long getWallDsTsEndCptz() {
        return this.wallDsTsEndCptz;
    }

    public void setWallDsUtente(String wallDsUtente) {
        this.wallDsUtente = Functions.subString(wallDsUtente, Len.WALL_DS_UTENTE);
    }

    public String getWallDsUtente() {
        return this.wallDsUtente;
    }

    public void setWallDsStatoElab(char wallDsStatoElab) {
        this.wallDsStatoElab = wallDsStatoElab;
    }

    public char getWallDsStatoElab() {
        return this.wallDsStatoElab;
    }

    public void setWallTpRibilFnd(String wallTpRibilFnd) {
        this.wallTpRibilFnd = Functions.subString(wallTpRibilFnd, Len.WALL_TP_RIBIL_FND);
    }

    public String getWallTpRibilFnd() {
        return this.wallTpRibilFnd;
    }

    public WallDtEndVldt getWallDtEndVldt() {
        return wallDtEndVldt;
    }

    public WallIdMoviChiu getWallIdMoviChiu() {
        return wallIdMoviChiu;
    }

    public WallPcRipAst getWallPcRipAst() {
        return wallPcRipAst;
    }

    public WallPeriodo getWallPeriodo() {
        return wallPeriodo;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WALL_COD_FND = 20;
        public static final int WALL_COD_TARI = 12;
        public static final int WALL_TP_APPLZ_AST = 2;
        public static final int WALL_DS_UTENTE = 20;
        public static final int WALL_TP_RIBIL_FND = 2;
        public static final int WALL_ID_AST_ALLOC = 5;
        public static final int WALL_ID_STRA_DI_INVST = 5;
        public static final int WALL_ID_MOVI_CRZ = 5;
        public static final int WALL_DT_INI_VLDT = 5;
        public static final int WALL_DT_INI_EFF = 5;
        public static final int WALL_DT_END_EFF = 5;
        public static final int WALL_COD_COMP_ANIA = 3;
        public static final int WALL_TP_FND = 1;
        public static final int WALL_DS_RIGA = 6;
        public static final int WALL_DS_OPER_SQL = 1;
        public static final int WALL_DS_VER = 5;
        public static final int WALL_DS_TS_INI_CPTZ = 10;
        public static final int WALL_DS_TS_END_CPTZ = 10;
        public static final int WALL_DS_STATO_ELAB = 1;
        public static final int WALL_DATI = WALL_ID_AST_ALLOC + WALL_ID_STRA_DI_INVST + WALL_ID_MOVI_CRZ + WallIdMoviChiu.Len.WALL_ID_MOVI_CHIU + WALL_DT_INI_VLDT + WallDtEndVldt.Len.WALL_DT_END_VLDT + WALL_DT_INI_EFF + WALL_DT_END_EFF + WALL_COD_COMP_ANIA + WALL_COD_FND + WALL_COD_TARI + WALL_TP_APPLZ_AST + WallPcRipAst.Len.WALL_PC_RIP_AST + WALL_TP_FND + WALL_DS_RIGA + WALL_DS_OPER_SQL + WALL_DS_VER + WALL_DS_TS_INI_CPTZ + WALL_DS_TS_END_CPTZ + WALL_DS_UTENTE + WALL_DS_STATO_ELAB + WallPeriodo.Len.WALL_PERIODO + WALL_TP_RIBIL_FND;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WALL_ID_AST_ALLOC = 9;
            public static final int WALL_ID_STRA_DI_INVST = 9;
            public static final int WALL_ID_MOVI_CRZ = 9;
            public static final int WALL_DT_INI_VLDT = 8;
            public static final int WALL_DT_INI_EFF = 8;
            public static final int WALL_DT_END_EFF = 8;
            public static final int WALL_COD_COMP_ANIA = 5;
            public static final int WALL_DS_RIGA = 10;
            public static final int WALL_DS_VER = 9;
            public static final int WALL_DS_TS_INI_CPTZ = 18;
            public static final int WALL_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
