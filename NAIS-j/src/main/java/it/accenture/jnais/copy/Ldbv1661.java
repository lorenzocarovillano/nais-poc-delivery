package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV1661<br>
 * Variable: LDBV1661 from copybook LDBV1661<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv1661 {

    //==== PROPERTIES ====
    //Original name: LDBV1661-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LDBV1661-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV1661-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV1661-TP-STAT-TIT
    private String tpStatTit = DefaultValues.stringVal(Len.TP_STAT_TIT);
    //Original name: LDBV1661-IMP-TOT
    private AfDecimal impTot = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpStatTit(String tpStatTit) {
        this.tpStatTit = Functions.subString(tpStatTit, Len.TP_STAT_TIT);
    }

    public String getTpStatTit() {
        return this.tpStatTit;
    }

    public void setImpTot(AfDecimal impTot) {
        this.impTot.assign(impTot);
    }

    public AfDecimal getImpTot() {
        return this.impTot.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int TP_STAT_TIT = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
