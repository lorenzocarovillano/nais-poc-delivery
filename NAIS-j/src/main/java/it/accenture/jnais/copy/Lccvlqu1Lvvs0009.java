package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVLQU1<br>
 * Variable: LCCVLQU1 from copybook LCCVLQU1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvlqu1Lvvs0009 {

    //==== PROPERTIES ====
    /**Original name: DLQU-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA LIQ
	 *    ALIAS LQU
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: DLQU-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: DLQU-DATI
    private S089Dati dati = new S089Dati();

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public S089Dati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
