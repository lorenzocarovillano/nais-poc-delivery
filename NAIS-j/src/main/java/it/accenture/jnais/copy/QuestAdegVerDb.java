package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: QUEST-ADEG-VER-DB<br>
 * Variable: QUEST-ADEG-VER-DB from copybook IDBVP563<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class QuestAdegVerDb {

    //==== PROPERTIES ====
    //Original name: P56-DT-INI-FNT-REDD-DB
    private String db = DefaultValues.stringVal(Len.DB);
    //Original name: P56-DT-INI-FNT-REDD-2-DB
    private String redd2Db = DefaultValues.stringVal(Len.REDD2_DB);
    //Original name: P56-DT-INI-FNT-REDD-3-DB
    private String redd3Db = DefaultValues.stringVal(Len.REDD3_DB);

    //==== METHODS ====
    public void setDb(String db) {
        this.db = Functions.subString(db, Len.DB);
    }

    public String getDb() {
        return this.db;
    }

    public void setRedd2Db(String redd2Db) {
        this.redd2Db = Functions.subString(redd2Db, Len.REDD2_DB);
    }

    public String getRedd2Db() {
        return this.redd2Db;
    }

    public void setRedd3Db(String redd3Db) {
        this.redd3Db = Functions.subString(redd3Db, Len.REDD3_DB);
    }

    public String getRedd3Db() {
        return this.redd3Db;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DB = 10;
        public static final int REDD2_DB = 10;
        public static final int REDD3_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
