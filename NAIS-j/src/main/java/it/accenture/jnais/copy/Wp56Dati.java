package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP56-DATI<br>
 * Variable: WP56-DATI from copybook LCCVP561<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wp56Dati {

    //==== PROPERTIES ====
    //Original name: WP56-ID-QUEST-ADEG-VER
    private int idQuestAdegVer = DefaultValues.INT_VAL;
    //Original name: WP56-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: WP56-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: WP56-ID-RAPP-ANA
    private int idRappAna = DefaultValues.INT_VAL;
    //Original name: WP56-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: WP56-NATURA-OPRZ
    private String naturaOprz = DefaultValues.stringVal(Len.NATURA_OPRZ);
    //Original name: WP56-ORGN-FND
    private String orgnFnd = DefaultValues.stringVal(Len.ORGN_FND);
    //Original name: WP56-COD-QLFC-PROF
    private String codQlfcProf = DefaultValues.stringVal(Len.COD_QLFC_PROF);
    //Original name: WP56-COD-NAZ-QLFC-PROF
    private String codNazQlfcProf = DefaultValues.stringVal(Len.COD_NAZ_QLFC_PROF);
    //Original name: WP56-COD-PRV-QLFC-PROF
    private String codPrvQlfcProf = DefaultValues.stringVal(Len.COD_PRV_QLFC_PROF);
    //Original name: WP56-FNT-REDD
    private String fntRedd = DefaultValues.stringVal(Len.FNT_REDD);
    //Original name: WP56-REDD-FATT-ANNU
    private AfDecimal reddFattAnnu = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-COD-ATECO
    private String codAteco = DefaultValues.stringVal(Len.COD_ATECO);
    //Original name: WP56-VALUT-COLL
    private String valutColl = DefaultValues.stringVal(Len.VALUT_COLL);
    //Original name: WP56-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WP56-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: WP56-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: WP56-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: WP56-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WP56-LUOGO-COSTITUZIONE
    private String luogoCostituzione = DefaultValues.stringVal(Len.LUOGO_COSTITUZIONE);
    //Original name: WP56-TP-MOVI
    private int tpMovi = DefaultValues.INT_VAL;
    //Original name: WP56-FL-RAG-RAPP
    private char flRagRapp = DefaultValues.CHAR_VAL;
    //Original name: WP56-FL-PRSZ-TIT-EFF
    private char flPrszTitEff = DefaultValues.CHAR_VAL;
    //Original name: WP56-TP-MOT-RISC
    private String tpMotRisc = DefaultValues.stringVal(Len.TP_MOT_RISC);
    //Original name: WP56-TP-PNT-VND
    private String tpPntVnd = DefaultValues.stringVal(Len.TP_PNT_VND);
    //Original name: WP56-TP-ADEG-VER
    private String tpAdegVer = DefaultValues.stringVal(Len.TP_ADEG_VER);
    //Original name: WP56-TP-RELA-ESEC
    private String tpRelaEsec = DefaultValues.stringVal(Len.TP_RELA_ESEC);
    //Original name: WP56-TP-SCO-FIN-RAPP
    private String tpScoFinRapp = DefaultValues.stringVal(Len.TP_SCO_FIN_RAPP);
    //Original name: WP56-FL-PRSZ-3O-PAGAT
    private char flPrsz3oPagat = DefaultValues.CHAR_VAL;
    //Original name: WP56-AREA-GEO-PROV-FND
    private String areaGeoProvFnd = DefaultValues.stringVal(Len.AREA_GEO_PROV_FND);
    //Original name: WP56-TP-DEST-FND
    private String tpDestFnd = DefaultValues.stringVal(Len.TP_DEST_FND);
    //Original name: WP56-FL-PAESE-RESID-AUT
    private char flPaeseResidAut = DefaultValues.CHAR_VAL;
    //Original name: WP56-FL-PAESE-CIT-AUT
    private char flPaeseCitAut = DefaultValues.CHAR_VAL;
    //Original name: WP56-FL-PAESE-NAZ-AUT
    private char flPaeseNazAut = DefaultValues.CHAR_VAL;
    //Original name: WP56-COD-PROF-PREC
    private String codProfPrec = DefaultValues.stringVal(Len.COD_PROF_PREC);
    //Original name: WP56-FL-AUT-PEP
    private char flAutPep = DefaultValues.CHAR_VAL;
    //Original name: WP56-FL-IMP-CAR-PUB
    private char flImpCarPub = DefaultValues.CHAR_VAL;
    //Original name: WP56-FL-LIS-TERR-SORV
    private char flLisTerrSorv = DefaultValues.CHAR_VAL;
    //Original name: WP56-TP-SIT-FIN-PAT
    private String tpSitFinPat = DefaultValues.stringVal(Len.TP_SIT_FIN_PAT);
    //Original name: WP56-TP-SIT-FIN-PAT-CON
    private String tpSitFinPatCon = DefaultValues.stringVal(Len.TP_SIT_FIN_PAT_CON);
    //Original name: WP56-IMP-TOT-AFF-UTIL
    private AfDecimal impTotAffUtil = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-IMP-TOT-FIN-UTIL
    private AfDecimal impTotFinUtil = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-IMP-TOT-AFF-ACC
    private AfDecimal impTotAffAcc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-IMP-TOT-FIN-ACC
    private AfDecimal impTotFinAcc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-TP-FRM-GIUR-SAV
    private String tpFrmGiurSav = DefaultValues.stringVal(Len.TP_FRM_GIUR_SAV);
    //Original name: WP56-REG-COLL-POLI
    private String regCollPoli = DefaultValues.stringVal(Len.REG_COLL_POLI);
    //Original name: WP56-NUM-TEL
    private String numTel = DefaultValues.stringVal(Len.NUM_TEL);
    //Original name: WP56-NUM-DIP
    private int numDip = DefaultValues.INT_VAL;
    //Original name: WP56-TP-SIT-FAM-CONV
    private String tpSitFamConv = DefaultValues.stringVal(Len.TP_SIT_FAM_CONV);
    //Original name: WP56-COD-PROF-CON
    private String codProfCon = DefaultValues.stringVal(Len.COD_PROF_CON);
    //Original name: WP56-FL-ES-PROC-PEN
    private char flEsProcPen = DefaultValues.CHAR_VAL;
    //Original name: WP56-TP-COND-CLIENTE
    private String tpCondCliente = DefaultValues.stringVal(Len.TP_COND_CLIENTE);
    //Original name: WP56-COD-SAE
    private String codSae = DefaultValues.stringVal(Len.COD_SAE);
    //Original name: WP56-TP-OPER-ESTERO
    private String tpOperEstero = DefaultValues.stringVal(Len.TP_OPER_ESTERO);
    //Original name: WP56-STAT-OPER-ESTERO
    private String statOperEstero = DefaultValues.stringVal(Len.STAT_OPER_ESTERO);
    //Original name: WP56-COD-PRV-SVOL-ATT
    private String codPrvSvolAtt = DefaultValues.stringVal(Len.COD_PRV_SVOL_ATT);
    //Original name: WP56-COD-STAT-SVOL-ATT
    private String codStatSvolAtt = DefaultValues.stringVal(Len.COD_STAT_SVOL_ATT);
    //Original name: WP56-TP-SOC
    private String tpSoc = DefaultValues.stringVal(Len.TP_SOC);
    //Original name: WP56-FL-IND-SOC-QUOT
    private char flIndSocQuot = DefaultValues.CHAR_VAL;
    //Original name: WP56-TP-SIT-GIUR
    private String tpSitGiur = DefaultValues.stringVal(Len.TP_SIT_GIUR);
    //Original name: WP56-PC-QUO-DET-TIT-EFF
    private AfDecimal pcQuoDetTitEff = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: WP56-TP-PRFL-RSH-PEP
    private String tpPrflRshPep = DefaultValues.stringVal(Len.TP_PRFL_RSH_PEP);
    //Original name: WP56-TP-PEP
    private String tpPep = DefaultValues.stringVal(Len.TP_PEP);
    //Original name: WP56-FL-NOT-PREG
    private char flNotPreg = DefaultValues.CHAR_VAL;
    //Original name: WP56-DT-INI-FNT-REDD
    private int dtIniFntRedd = DefaultValues.INT_VAL;
    //Original name: WP56-FNT-REDD-2
    private String fntRedd2 = DefaultValues.stringVal(Len.FNT_REDD2);
    //Original name: WP56-DT-INI-FNT-REDD-2
    private int dtIniFntRedd2 = DefaultValues.INT_VAL;
    //Original name: WP56-FNT-REDD-3
    private String fntRedd3 = DefaultValues.stringVal(Len.FNT_REDD3);
    //Original name: WP56-DT-INI-FNT-REDD-3
    private int dtIniFntRedd3 = DefaultValues.INT_VAL;
    //Original name: WP56-MOT-ASS-TIT-EFF
    private String motAssTitEff = DefaultValues.stringVal(Len.MOT_ASS_TIT_EFF);
    //Original name: WP56-FIN-COSTITUZIONE
    private String finCostituzione = DefaultValues.stringVal(Len.FIN_COSTITUZIONE);
    //Original name: WP56-DESC-IMP-CAR-PUB
    private String descImpCarPub = DefaultValues.stringVal(Len.DESC_IMP_CAR_PUB);
    //Original name: WP56-DESC-SCO-FIN-RAPP
    private String descScoFinRapp = DefaultValues.stringVal(Len.DESC_SCO_FIN_RAPP);
    //Original name: WP56-DESC-PROC-PNL
    private String descProcPnl = DefaultValues.stringVal(Len.DESC_PROC_PNL);
    //Original name: WP56-DESC-NOT-PREG
    private String descNotPreg = DefaultValues.stringVal(Len.DESC_NOT_PREG);
    //Original name: WP56-ID-ASSICURATI
    private int idAssicurati = DefaultValues.INT_VAL;
    //Original name: WP56-REDD-CON
    private AfDecimal reddCon = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-DESC-LIB-MOT-RISC
    private String descLibMotRisc = DefaultValues.stringVal(Len.DESC_LIB_MOT_RISC);
    //Original name: WP56-TP-MOT-ASS-TIT-EFF
    private String tpMotAssTitEff = DefaultValues.stringVal(Len.TP_MOT_ASS_TIT_EFF);
    //Original name: WP56-TP-RAG-RAPP
    private String tpRagRapp = DefaultValues.stringVal(Len.TP_RAG_RAPP);
    //Original name: WP56-COD-CAN
    private int codCan = DefaultValues.INT_VAL;
    //Original name: WP56-TP-FIN-COST
    private String tpFinCost = DefaultValues.stringVal(Len.TP_FIN_COST);
    //Original name: WP56-NAZ-DEST-FND
    private String nazDestFnd = DefaultValues.stringVal(Len.NAZ_DEST_FND);
    //Original name: WP56-FL-AU-FATCA-AEOI
    private char flAuFatcaAeoi = DefaultValues.CHAR_VAL;
    //Original name: WP56-TP-CAR-FIN-GIUR
    private String tpCarFinGiur = DefaultValues.stringVal(Len.TP_CAR_FIN_GIUR);
    //Original name: WP56-TP-CAR-FIN-GIUR-AT
    private String tpCarFinGiurAt = DefaultValues.stringVal(Len.TP_CAR_FIN_GIUR_AT);
    //Original name: WP56-TP-CAR-FIN-GIUR-PA
    private String tpCarFinGiurPa = DefaultValues.stringVal(Len.TP_CAR_FIN_GIUR_PA);
    //Original name: WP56-FL-ISTITUZ-FIN
    private char flIstituzFin = DefaultValues.CHAR_VAL;
    //Original name: WP56-TP-ORI-FND-TIT-EFF
    private String tpOriFndTitEff = DefaultValues.stringVal(Len.TP_ORI_FND_TIT_EFF);
    //Original name: WP56-PC-ESP-AG-PA-MSC
    private AfDecimal pcEspAgPaMsc = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: WP56-FL-PR-TR-USA
    private char flPrTrUsa = DefaultValues.CHAR_VAL;
    //Original name: WP56-FL-PR-TR-NO-USA
    private char flPrTrNoUsa = DefaultValues.CHAR_VAL;
    //Original name: WP56-PC-RIP-PAT-AS-VITA
    private AfDecimal pcRipPatAsVita = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: WP56-PC-RIP-PAT-IM
    private AfDecimal pcRipPatIm = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: WP56-PC-RIP-PAT-SET-IM
    private AfDecimal pcRipPatSetIm = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: WP56-TP-STATUS-AEOI
    private String tpStatusAeoi = DefaultValues.stringVal(Len.TP_STATUS_AEOI);
    //Original name: WP56-TP-STATUS-FATCA
    private String tpStatusFatca = DefaultValues.stringVal(Len.TP_STATUS_FATCA);
    //Original name: WP56-FL-RAPP-PA-MSC
    private char flRappPaMsc = DefaultValues.CHAR_VAL;
    //Original name: WP56-COD-COMUN-SVOL-ATT
    private String codComunSvolAtt = DefaultValues.stringVal(Len.COD_COMUN_SVOL_ATT);
    //Original name: WP56-TP-DT-1O-CON-CLI
    private String tpDt1oConCli = DefaultValues.stringVal(Len.TP_DT1O_CON_CLI);
    //Original name: WP56-TP-MOD-EN-RELA-INT
    private String tpModEnRelaInt = DefaultValues.stringVal(Len.TP_MOD_EN_RELA_INT);
    //Original name: WP56-TP-REDD-ANNU-LRD
    private String tpReddAnnuLrd = DefaultValues.stringVal(Len.TP_REDD_ANNU_LRD);
    //Original name: WP56-TP-REDD-CON
    private String tpReddCon = DefaultValues.stringVal(Len.TP_REDD_CON);
    //Original name: WP56-TP-OPER-SOC-FID
    private String tpOperSocFid = DefaultValues.stringVal(Len.TP_OPER_SOC_FID);
    //Original name: WP56-COD-PA-ESP-MSC-1
    private String codPaEspMsc1 = DefaultValues.stringVal(Len.COD_PA_ESP_MSC1);
    //Original name: WP56-IMP-PA-ESP-MSC-1
    private AfDecimal impPaEspMsc1 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-COD-PA-ESP-MSC-2
    private String codPaEspMsc2 = DefaultValues.stringVal(Len.COD_PA_ESP_MSC2);
    //Original name: WP56-IMP-PA-ESP-MSC-2
    private AfDecimal impPaEspMsc2 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-COD-PA-ESP-MSC-3
    private String codPaEspMsc3 = DefaultValues.stringVal(Len.COD_PA_ESP_MSC3);
    //Original name: WP56-IMP-PA-ESP-MSC-3
    private AfDecimal impPaEspMsc3 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-COD-PA-ESP-MSC-4
    private String codPaEspMsc4 = DefaultValues.stringVal(Len.COD_PA_ESP_MSC4);
    //Original name: WP56-IMP-PA-ESP-MSC-4
    private AfDecimal impPaEspMsc4 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-COD-PA-ESP-MSC-5
    private String codPaEspMsc5 = DefaultValues.stringVal(Len.COD_PA_ESP_MSC5);
    //Original name: WP56-IMP-PA-ESP-MSC-5
    private AfDecimal impPaEspMsc5 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-DESC-ORGN-FND
    private String descOrgnFnd = DefaultValues.stringVal(Len.DESC_ORGN_FND);
    //Original name: WP56-COD-AUT-DUE-DIL
    private String codAutDueDil = DefaultValues.stringVal(Len.COD_AUT_DUE_DIL);
    //Original name: WP56-FL-PR-QUEST-FATCA
    private char flPrQuestFatca = DefaultValues.CHAR_VAL;
    //Original name: WP56-FL-PR-QUEST-AEOI
    private char flPrQuestAeoi = DefaultValues.CHAR_VAL;
    //Original name: WP56-FL-PR-QUEST-OFAC
    private char flPrQuestOfac = DefaultValues.CHAR_VAL;
    //Original name: WP56-FL-PR-QUEST-KYC
    private char flPrQuestKyc = DefaultValues.CHAR_VAL;
    //Original name: WP56-FL-PR-QUEST-MSCQ
    private char flPrQuestMscq = DefaultValues.CHAR_VAL;
    //Original name: WP56-TP-NOT-PREG
    private String tpNotPreg = DefaultValues.stringVal(Len.TP_NOT_PREG);
    //Original name: WP56-TP-PROC-PNL
    private String tpProcPnl = DefaultValues.stringVal(Len.TP_PROC_PNL);
    //Original name: WP56-COD-IMP-CAR-PUB
    private String codImpCarPub = DefaultValues.stringVal(Len.COD_IMP_CAR_PUB);
    //Original name: WP56-OPRZ-SOSPETTE
    private char oprzSospette = DefaultValues.CHAR_VAL;
    //Original name: WP56-ULT-FATT-ANNU
    private AfDecimal ultFattAnnu = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-DESC-PEP
    private String descPep = DefaultValues.stringVal(Len.DESC_PEP);
    //Original name: WP56-NUM-TEL-2
    private String numTel2 = DefaultValues.stringVal(Len.NUM_TEL2);
    //Original name: WP56-IMP-AFI
    private AfDecimal impAfi = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WP56-FL-NEW-PRO
    private char flNewPro = DefaultValues.CHAR_VAL;
    //Original name: WP56-TP-MOT-CAMBIO-CNTR
    private String tpMotCambioCntr = DefaultValues.stringVal(Len.TP_MOT_CAMBIO_CNTR);
    //Original name: WP56-DESC-MOT-CAMBIO-CN
    private String descMotCambioCn = DefaultValues.stringVal(Len.DESC_MOT_CAMBIO_CN);
    //Original name: WP56-COD-SOGG
    private String codSogg = DefaultValues.stringVal(Len.COD_SOGG);

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        idQuestAdegVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_QUEST_ADEG_VER, 0);
        position += Len.ID_QUEST_ADEG_VER;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        idMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        idRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_RAPP_ANA, 0);
        position += Len.ID_RAPP_ANA;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        naturaOprz = MarshalByte.readString(buffer, position, Len.NATURA_OPRZ);
        position += Len.NATURA_OPRZ;
        orgnFnd = MarshalByte.readString(buffer, position, Len.ORGN_FND);
        position += Len.ORGN_FND;
        codQlfcProf = MarshalByte.readString(buffer, position, Len.COD_QLFC_PROF);
        position += Len.COD_QLFC_PROF;
        codNazQlfcProf = MarshalByte.readString(buffer, position, Len.COD_NAZ_QLFC_PROF);
        position += Len.COD_NAZ_QLFC_PROF;
        codPrvQlfcProf = MarshalByte.readString(buffer, position, Len.COD_PRV_QLFC_PROF);
        position += Len.COD_PRV_QLFC_PROF;
        fntRedd = MarshalByte.readString(buffer, position, Len.FNT_REDD);
        position += Len.FNT_REDD;
        reddFattAnnu.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.REDD_FATT_ANNU, Len.Fract.REDD_FATT_ANNU));
        position += Len.REDD_FATT_ANNU;
        codAteco = MarshalByte.readString(buffer, position, Len.COD_ATECO);
        position += Len.COD_ATECO;
        valutColl = MarshalByte.readString(buffer, position, Len.VALUT_COLL);
        position += Len.VALUT_COLL;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        luogoCostituzione = MarshalByte.readString(buffer, position, Len.LUOGO_COSTITUZIONE);
        position += Len.LUOGO_COSTITUZIONE;
        tpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI, 0);
        position += Len.TP_MOVI;
        flRagRapp = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flPrszTitEff = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpMotRisc = MarshalByte.readString(buffer, position, Len.TP_MOT_RISC);
        position += Len.TP_MOT_RISC;
        tpPntVnd = MarshalByte.readString(buffer, position, Len.TP_PNT_VND);
        position += Len.TP_PNT_VND;
        tpAdegVer = MarshalByte.readString(buffer, position, Len.TP_ADEG_VER);
        position += Len.TP_ADEG_VER;
        tpRelaEsec = MarshalByte.readString(buffer, position, Len.TP_RELA_ESEC);
        position += Len.TP_RELA_ESEC;
        tpScoFinRapp = MarshalByte.readString(buffer, position, Len.TP_SCO_FIN_RAPP);
        position += Len.TP_SCO_FIN_RAPP;
        flPrsz3oPagat = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        areaGeoProvFnd = MarshalByte.readString(buffer, position, Len.AREA_GEO_PROV_FND);
        position += Len.AREA_GEO_PROV_FND;
        tpDestFnd = MarshalByte.readString(buffer, position, Len.TP_DEST_FND);
        position += Len.TP_DEST_FND;
        flPaeseResidAut = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flPaeseCitAut = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flPaeseNazAut = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        codProfPrec = MarshalByte.readString(buffer, position, Len.COD_PROF_PREC);
        position += Len.COD_PROF_PREC;
        flAutPep = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flImpCarPub = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flLisTerrSorv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpSitFinPat = MarshalByte.readString(buffer, position, Len.TP_SIT_FIN_PAT);
        position += Len.TP_SIT_FIN_PAT;
        tpSitFinPatCon = MarshalByte.readString(buffer, position, Len.TP_SIT_FIN_PAT_CON);
        position += Len.TP_SIT_FIN_PAT_CON;
        impTotAffUtil.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_TOT_AFF_UTIL, Len.Fract.IMP_TOT_AFF_UTIL));
        position += Len.IMP_TOT_AFF_UTIL;
        impTotFinUtil.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_TOT_FIN_UTIL, Len.Fract.IMP_TOT_FIN_UTIL));
        position += Len.IMP_TOT_FIN_UTIL;
        impTotAffAcc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_TOT_AFF_ACC, Len.Fract.IMP_TOT_AFF_ACC));
        position += Len.IMP_TOT_AFF_ACC;
        impTotFinAcc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_TOT_FIN_ACC, Len.Fract.IMP_TOT_FIN_ACC));
        position += Len.IMP_TOT_FIN_ACC;
        tpFrmGiurSav = MarshalByte.readString(buffer, position, Len.TP_FRM_GIUR_SAV);
        position += Len.TP_FRM_GIUR_SAV;
        regCollPoli = MarshalByte.readString(buffer, position, Len.REG_COLL_POLI);
        position += Len.REG_COLL_POLI;
        numTel = MarshalByte.readString(buffer, position, Len.NUM_TEL);
        position += Len.NUM_TEL;
        numDip = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_DIP, 0);
        position += Len.NUM_DIP;
        tpSitFamConv = MarshalByte.readString(buffer, position, Len.TP_SIT_FAM_CONV);
        position += Len.TP_SIT_FAM_CONV;
        codProfCon = MarshalByte.readString(buffer, position, Len.COD_PROF_CON);
        position += Len.COD_PROF_CON;
        flEsProcPen = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpCondCliente = MarshalByte.readString(buffer, position, Len.TP_COND_CLIENTE);
        position += Len.TP_COND_CLIENTE;
        codSae = MarshalByte.readString(buffer, position, Len.COD_SAE);
        position += Len.COD_SAE;
        tpOperEstero = MarshalByte.readString(buffer, position, Len.TP_OPER_ESTERO);
        position += Len.TP_OPER_ESTERO;
        statOperEstero = MarshalByte.readString(buffer, position, Len.STAT_OPER_ESTERO);
        position += Len.STAT_OPER_ESTERO;
        codPrvSvolAtt = MarshalByte.readString(buffer, position, Len.COD_PRV_SVOL_ATT);
        position += Len.COD_PRV_SVOL_ATT;
        codStatSvolAtt = MarshalByte.readString(buffer, position, Len.COD_STAT_SVOL_ATT);
        position += Len.COD_STAT_SVOL_ATT;
        tpSoc = MarshalByte.readString(buffer, position, Len.TP_SOC);
        position += Len.TP_SOC;
        flIndSocQuot = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpSitGiur = MarshalByte.readString(buffer, position, Len.TP_SIT_GIUR);
        position += Len.TP_SIT_GIUR;
        pcQuoDetTitEff.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_QUO_DET_TIT_EFF, Len.Fract.PC_QUO_DET_TIT_EFF));
        position += Len.PC_QUO_DET_TIT_EFF;
        tpPrflRshPep = MarshalByte.readString(buffer, position, Len.TP_PRFL_RSH_PEP);
        position += Len.TP_PRFL_RSH_PEP;
        tpPep = MarshalByte.readString(buffer, position, Len.TP_PEP);
        position += Len.TP_PEP;
        flNotPreg = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dtIniFntRedd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_FNT_REDD, 0);
        position += Len.DT_INI_FNT_REDD;
        fntRedd2 = MarshalByte.readString(buffer, position, Len.FNT_REDD2);
        position += Len.FNT_REDD2;
        dtIniFntRedd2 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_FNT_REDD2, 0);
        position += Len.DT_INI_FNT_REDD2;
        fntRedd3 = MarshalByte.readString(buffer, position, Len.FNT_REDD3);
        position += Len.FNT_REDD3;
        dtIniFntRedd3 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_FNT_REDD3, 0);
        position += Len.DT_INI_FNT_REDD3;
        motAssTitEff = MarshalByte.readString(buffer, position, Len.MOT_ASS_TIT_EFF);
        position += Len.MOT_ASS_TIT_EFF;
        finCostituzione = MarshalByte.readString(buffer, position, Len.FIN_COSTITUZIONE);
        position += Len.FIN_COSTITUZIONE;
        descImpCarPub = MarshalByte.readString(buffer, position, Len.DESC_IMP_CAR_PUB);
        position += Len.DESC_IMP_CAR_PUB;
        descScoFinRapp = MarshalByte.readString(buffer, position, Len.DESC_SCO_FIN_RAPP);
        position += Len.DESC_SCO_FIN_RAPP;
        descProcPnl = MarshalByte.readString(buffer, position, Len.DESC_PROC_PNL);
        position += Len.DESC_PROC_PNL;
        descNotPreg = MarshalByte.readString(buffer, position, Len.DESC_NOT_PREG);
        position += Len.DESC_NOT_PREG;
        idAssicurati = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ASSICURATI, 0);
        position += Len.ID_ASSICURATI;
        reddCon.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.REDD_CON, Len.Fract.REDD_CON));
        position += Len.REDD_CON;
        descLibMotRisc = MarshalByte.readString(buffer, position, Len.DESC_LIB_MOT_RISC);
        position += Len.DESC_LIB_MOT_RISC;
        tpMotAssTitEff = MarshalByte.readString(buffer, position, Len.TP_MOT_ASS_TIT_EFF);
        position += Len.TP_MOT_ASS_TIT_EFF;
        tpRagRapp = MarshalByte.readString(buffer, position, Len.TP_RAG_RAPP);
        position += Len.TP_RAG_RAPP;
        codCan = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_CAN, 0);
        position += Len.COD_CAN;
        tpFinCost = MarshalByte.readString(buffer, position, Len.TP_FIN_COST);
        position += Len.TP_FIN_COST;
        nazDestFnd = MarshalByte.readString(buffer, position, Len.NAZ_DEST_FND);
        position += Len.NAZ_DEST_FND;
        flAuFatcaAeoi = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpCarFinGiur = MarshalByte.readString(buffer, position, Len.TP_CAR_FIN_GIUR);
        position += Len.TP_CAR_FIN_GIUR;
        tpCarFinGiurAt = MarshalByte.readString(buffer, position, Len.TP_CAR_FIN_GIUR_AT);
        position += Len.TP_CAR_FIN_GIUR_AT;
        tpCarFinGiurPa = MarshalByte.readString(buffer, position, Len.TP_CAR_FIN_GIUR_PA);
        position += Len.TP_CAR_FIN_GIUR_PA;
        flIstituzFin = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpOriFndTitEff = MarshalByte.readString(buffer, position, Len.TP_ORI_FND_TIT_EFF);
        position += Len.TP_ORI_FND_TIT_EFF;
        pcEspAgPaMsc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_ESP_AG_PA_MSC, Len.Fract.PC_ESP_AG_PA_MSC));
        position += Len.PC_ESP_AG_PA_MSC;
        flPrTrUsa = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flPrTrNoUsa = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcRipPatAsVita.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_RIP_PAT_AS_VITA, Len.Fract.PC_RIP_PAT_AS_VITA));
        position += Len.PC_RIP_PAT_AS_VITA;
        pcRipPatIm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_RIP_PAT_IM, Len.Fract.PC_RIP_PAT_IM));
        position += Len.PC_RIP_PAT_IM;
        pcRipPatSetIm.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_RIP_PAT_SET_IM, Len.Fract.PC_RIP_PAT_SET_IM));
        position += Len.PC_RIP_PAT_SET_IM;
        tpStatusAeoi = MarshalByte.readString(buffer, position, Len.TP_STATUS_AEOI);
        position += Len.TP_STATUS_AEOI;
        tpStatusFatca = MarshalByte.readString(buffer, position, Len.TP_STATUS_FATCA);
        position += Len.TP_STATUS_FATCA;
        flRappPaMsc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        codComunSvolAtt = MarshalByte.readString(buffer, position, Len.COD_COMUN_SVOL_ATT);
        position += Len.COD_COMUN_SVOL_ATT;
        tpDt1oConCli = MarshalByte.readString(buffer, position, Len.TP_DT1O_CON_CLI);
        position += Len.TP_DT1O_CON_CLI;
        tpModEnRelaInt = MarshalByte.readString(buffer, position, Len.TP_MOD_EN_RELA_INT);
        position += Len.TP_MOD_EN_RELA_INT;
        tpReddAnnuLrd = MarshalByte.readString(buffer, position, Len.TP_REDD_ANNU_LRD);
        position += Len.TP_REDD_ANNU_LRD;
        tpReddCon = MarshalByte.readString(buffer, position, Len.TP_REDD_CON);
        position += Len.TP_REDD_CON;
        tpOperSocFid = MarshalByte.readString(buffer, position, Len.TP_OPER_SOC_FID);
        position += Len.TP_OPER_SOC_FID;
        codPaEspMsc1 = MarshalByte.readString(buffer, position, Len.COD_PA_ESP_MSC1);
        position += Len.COD_PA_ESP_MSC1;
        impPaEspMsc1.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_PA_ESP_MSC1, Len.Fract.IMP_PA_ESP_MSC1));
        position += Len.IMP_PA_ESP_MSC1;
        codPaEspMsc2 = MarshalByte.readString(buffer, position, Len.COD_PA_ESP_MSC2);
        position += Len.COD_PA_ESP_MSC2;
        impPaEspMsc2.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_PA_ESP_MSC2, Len.Fract.IMP_PA_ESP_MSC2));
        position += Len.IMP_PA_ESP_MSC2;
        codPaEspMsc3 = MarshalByte.readString(buffer, position, Len.COD_PA_ESP_MSC3);
        position += Len.COD_PA_ESP_MSC3;
        impPaEspMsc3.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_PA_ESP_MSC3, Len.Fract.IMP_PA_ESP_MSC3));
        position += Len.IMP_PA_ESP_MSC3;
        codPaEspMsc4 = MarshalByte.readString(buffer, position, Len.COD_PA_ESP_MSC4);
        position += Len.COD_PA_ESP_MSC4;
        impPaEspMsc4.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_PA_ESP_MSC4, Len.Fract.IMP_PA_ESP_MSC4));
        position += Len.IMP_PA_ESP_MSC4;
        codPaEspMsc5 = MarshalByte.readString(buffer, position, Len.COD_PA_ESP_MSC5);
        position += Len.COD_PA_ESP_MSC5;
        impPaEspMsc5.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_PA_ESP_MSC5, Len.Fract.IMP_PA_ESP_MSC5));
        position += Len.IMP_PA_ESP_MSC5;
        descOrgnFnd = MarshalByte.readString(buffer, position, Len.DESC_ORGN_FND);
        position += Len.DESC_ORGN_FND;
        codAutDueDil = MarshalByte.readString(buffer, position, Len.COD_AUT_DUE_DIL);
        position += Len.COD_AUT_DUE_DIL;
        flPrQuestFatca = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flPrQuestAeoi = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flPrQuestOfac = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flPrQuestKyc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flPrQuestMscq = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpNotPreg = MarshalByte.readString(buffer, position, Len.TP_NOT_PREG);
        position += Len.TP_NOT_PREG;
        tpProcPnl = MarshalByte.readString(buffer, position, Len.TP_PROC_PNL);
        position += Len.TP_PROC_PNL;
        codImpCarPub = MarshalByte.readString(buffer, position, Len.COD_IMP_CAR_PUB);
        position += Len.COD_IMP_CAR_PUB;
        oprzSospette = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ultFattAnnu.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ULT_FATT_ANNU, Len.Fract.ULT_FATT_ANNU));
        position += Len.ULT_FATT_ANNU;
        descPep = MarshalByte.readString(buffer, position, Len.DESC_PEP);
        position += Len.DESC_PEP;
        numTel2 = MarshalByte.readString(buffer, position, Len.NUM_TEL2);
        position += Len.NUM_TEL2;
        impAfi.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_AFI, Len.Fract.IMP_AFI));
        position += Len.IMP_AFI;
        flNewPro = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpMotCambioCntr = MarshalByte.readString(buffer, position, Len.TP_MOT_CAMBIO_CNTR);
        position += Len.TP_MOT_CAMBIO_CNTR;
        descMotCambioCn = MarshalByte.readString(buffer, position, Len.DESC_MOT_CAMBIO_CN);
        position += Len.DESC_MOT_CAMBIO_CN;
        codSogg = MarshalByte.readString(buffer, position, Len.COD_SOGG);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idQuestAdegVer, Len.Int.ID_QUEST_ADEG_VER, 0);
        position += Len.ID_QUEST_ADEG_VER;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviCrz, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        MarshalByte.writeIntAsPacked(buffer, position, idRappAna, Len.Int.ID_RAPP_ANA, 0);
        position += Len.ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeString(buffer, position, naturaOprz, Len.NATURA_OPRZ);
        position += Len.NATURA_OPRZ;
        MarshalByte.writeString(buffer, position, orgnFnd, Len.ORGN_FND);
        position += Len.ORGN_FND;
        MarshalByte.writeString(buffer, position, codQlfcProf, Len.COD_QLFC_PROF);
        position += Len.COD_QLFC_PROF;
        MarshalByte.writeString(buffer, position, codNazQlfcProf, Len.COD_NAZ_QLFC_PROF);
        position += Len.COD_NAZ_QLFC_PROF;
        MarshalByte.writeString(buffer, position, codPrvQlfcProf, Len.COD_PRV_QLFC_PROF);
        position += Len.COD_PRV_QLFC_PROF;
        MarshalByte.writeString(buffer, position, fntRedd, Len.FNT_REDD);
        position += Len.FNT_REDD;
        MarshalByte.writeDecimalAsPacked(buffer, position, reddFattAnnu.copy());
        position += Len.REDD_FATT_ANNU;
        MarshalByte.writeString(buffer, position, codAteco, Len.COD_ATECO);
        position += Len.COD_ATECO;
        MarshalByte.writeString(buffer, position, valutColl, Len.VALUT_COLL);
        position += Len.VALUT_COLL;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsCptz, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, luogoCostituzione, Len.LUOGO_COSTITUZIONE);
        position += Len.LUOGO_COSTITUZIONE;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi, Len.Int.TP_MOVI, 0);
        position += Len.TP_MOVI;
        MarshalByte.writeChar(buffer, position, flRagRapp);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flPrszTitEff);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpMotRisc, Len.TP_MOT_RISC);
        position += Len.TP_MOT_RISC;
        MarshalByte.writeString(buffer, position, tpPntVnd, Len.TP_PNT_VND);
        position += Len.TP_PNT_VND;
        MarshalByte.writeString(buffer, position, tpAdegVer, Len.TP_ADEG_VER);
        position += Len.TP_ADEG_VER;
        MarshalByte.writeString(buffer, position, tpRelaEsec, Len.TP_RELA_ESEC);
        position += Len.TP_RELA_ESEC;
        MarshalByte.writeString(buffer, position, tpScoFinRapp, Len.TP_SCO_FIN_RAPP);
        position += Len.TP_SCO_FIN_RAPP;
        MarshalByte.writeChar(buffer, position, flPrsz3oPagat);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, areaGeoProvFnd, Len.AREA_GEO_PROV_FND);
        position += Len.AREA_GEO_PROV_FND;
        MarshalByte.writeString(buffer, position, tpDestFnd, Len.TP_DEST_FND);
        position += Len.TP_DEST_FND;
        MarshalByte.writeChar(buffer, position, flPaeseResidAut);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flPaeseCitAut);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flPaeseNazAut);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codProfPrec, Len.COD_PROF_PREC);
        position += Len.COD_PROF_PREC;
        MarshalByte.writeChar(buffer, position, flAutPep);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flImpCarPub);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flLisTerrSorv);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpSitFinPat, Len.TP_SIT_FIN_PAT);
        position += Len.TP_SIT_FIN_PAT;
        MarshalByte.writeString(buffer, position, tpSitFinPatCon, Len.TP_SIT_FIN_PAT_CON);
        position += Len.TP_SIT_FIN_PAT_CON;
        MarshalByte.writeDecimalAsPacked(buffer, position, impTotAffUtil.copy());
        position += Len.IMP_TOT_AFF_UTIL;
        MarshalByte.writeDecimalAsPacked(buffer, position, impTotFinUtil.copy());
        position += Len.IMP_TOT_FIN_UTIL;
        MarshalByte.writeDecimalAsPacked(buffer, position, impTotAffAcc.copy());
        position += Len.IMP_TOT_AFF_ACC;
        MarshalByte.writeDecimalAsPacked(buffer, position, impTotFinAcc.copy());
        position += Len.IMP_TOT_FIN_ACC;
        MarshalByte.writeString(buffer, position, tpFrmGiurSav, Len.TP_FRM_GIUR_SAV);
        position += Len.TP_FRM_GIUR_SAV;
        MarshalByte.writeString(buffer, position, regCollPoli, Len.REG_COLL_POLI);
        position += Len.REG_COLL_POLI;
        MarshalByte.writeString(buffer, position, numTel, Len.NUM_TEL);
        position += Len.NUM_TEL;
        MarshalByte.writeIntAsPacked(buffer, position, numDip, Len.Int.NUM_DIP, 0);
        position += Len.NUM_DIP;
        MarshalByte.writeString(buffer, position, tpSitFamConv, Len.TP_SIT_FAM_CONV);
        position += Len.TP_SIT_FAM_CONV;
        MarshalByte.writeString(buffer, position, codProfCon, Len.COD_PROF_CON);
        position += Len.COD_PROF_CON;
        MarshalByte.writeChar(buffer, position, flEsProcPen);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpCondCliente, Len.TP_COND_CLIENTE);
        position += Len.TP_COND_CLIENTE;
        MarshalByte.writeString(buffer, position, codSae, Len.COD_SAE);
        position += Len.COD_SAE;
        MarshalByte.writeString(buffer, position, tpOperEstero, Len.TP_OPER_ESTERO);
        position += Len.TP_OPER_ESTERO;
        MarshalByte.writeString(buffer, position, statOperEstero, Len.STAT_OPER_ESTERO);
        position += Len.STAT_OPER_ESTERO;
        MarshalByte.writeString(buffer, position, codPrvSvolAtt, Len.COD_PRV_SVOL_ATT);
        position += Len.COD_PRV_SVOL_ATT;
        MarshalByte.writeString(buffer, position, codStatSvolAtt, Len.COD_STAT_SVOL_ATT);
        position += Len.COD_STAT_SVOL_ATT;
        MarshalByte.writeString(buffer, position, tpSoc, Len.TP_SOC);
        position += Len.TP_SOC;
        MarshalByte.writeChar(buffer, position, flIndSocQuot);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpSitGiur, Len.TP_SIT_GIUR);
        position += Len.TP_SIT_GIUR;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcQuoDetTitEff.copy());
        position += Len.PC_QUO_DET_TIT_EFF;
        MarshalByte.writeString(buffer, position, tpPrflRshPep, Len.TP_PRFL_RSH_PEP);
        position += Len.TP_PRFL_RSH_PEP;
        MarshalByte.writeString(buffer, position, tpPep, Len.TP_PEP);
        position += Len.TP_PEP;
        MarshalByte.writeChar(buffer, position, flNotPreg);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniFntRedd, Len.Int.DT_INI_FNT_REDD, 0);
        position += Len.DT_INI_FNT_REDD;
        MarshalByte.writeString(buffer, position, fntRedd2, Len.FNT_REDD2);
        position += Len.FNT_REDD2;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniFntRedd2, Len.Int.DT_INI_FNT_REDD2, 0);
        position += Len.DT_INI_FNT_REDD2;
        MarshalByte.writeString(buffer, position, fntRedd3, Len.FNT_REDD3);
        position += Len.FNT_REDD3;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniFntRedd3, Len.Int.DT_INI_FNT_REDD3, 0);
        position += Len.DT_INI_FNT_REDD3;
        MarshalByte.writeString(buffer, position, motAssTitEff, Len.MOT_ASS_TIT_EFF);
        position += Len.MOT_ASS_TIT_EFF;
        MarshalByte.writeString(buffer, position, finCostituzione, Len.FIN_COSTITUZIONE);
        position += Len.FIN_COSTITUZIONE;
        MarshalByte.writeString(buffer, position, descImpCarPub, Len.DESC_IMP_CAR_PUB);
        position += Len.DESC_IMP_CAR_PUB;
        MarshalByte.writeString(buffer, position, descScoFinRapp, Len.DESC_SCO_FIN_RAPP);
        position += Len.DESC_SCO_FIN_RAPP;
        MarshalByte.writeString(buffer, position, descProcPnl, Len.DESC_PROC_PNL);
        position += Len.DESC_PROC_PNL;
        MarshalByte.writeString(buffer, position, descNotPreg, Len.DESC_NOT_PREG);
        position += Len.DESC_NOT_PREG;
        MarshalByte.writeIntAsPacked(buffer, position, idAssicurati, Len.Int.ID_ASSICURATI, 0);
        position += Len.ID_ASSICURATI;
        MarshalByte.writeDecimalAsPacked(buffer, position, reddCon.copy());
        position += Len.REDD_CON;
        MarshalByte.writeString(buffer, position, descLibMotRisc, Len.DESC_LIB_MOT_RISC);
        position += Len.DESC_LIB_MOT_RISC;
        MarshalByte.writeString(buffer, position, tpMotAssTitEff, Len.TP_MOT_ASS_TIT_EFF);
        position += Len.TP_MOT_ASS_TIT_EFF;
        MarshalByte.writeString(buffer, position, tpRagRapp, Len.TP_RAG_RAPP);
        position += Len.TP_RAG_RAPP;
        MarshalByte.writeIntAsPacked(buffer, position, codCan, Len.Int.COD_CAN, 0);
        position += Len.COD_CAN;
        MarshalByte.writeString(buffer, position, tpFinCost, Len.TP_FIN_COST);
        position += Len.TP_FIN_COST;
        MarshalByte.writeString(buffer, position, nazDestFnd, Len.NAZ_DEST_FND);
        position += Len.NAZ_DEST_FND;
        MarshalByte.writeChar(buffer, position, flAuFatcaAeoi);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpCarFinGiur, Len.TP_CAR_FIN_GIUR);
        position += Len.TP_CAR_FIN_GIUR;
        MarshalByte.writeString(buffer, position, tpCarFinGiurAt, Len.TP_CAR_FIN_GIUR_AT);
        position += Len.TP_CAR_FIN_GIUR_AT;
        MarshalByte.writeString(buffer, position, tpCarFinGiurPa, Len.TP_CAR_FIN_GIUR_PA);
        position += Len.TP_CAR_FIN_GIUR_PA;
        MarshalByte.writeChar(buffer, position, flIstituzFin);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpOriFndTitEff, Len.TP_ORI_FND_TIT_EFF);
        position += Len.TP_ORI_FND_TIT_EFF;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcEspAgPaMsc.copy());
        position += Len.PC_ESP_AG_PA_MSC;
        MarshalByte.writeChar(buffer, position, flPrTrUsa);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flPrTrNoUsa);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcRipPatAsVita.copy());
        position += Len.PC_RIP_PAT_AS_VITA;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcRipPatIm.copy());
        position += Len.PC_RIP_PAT_IM;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcRipPatSetIm.copy());
        position += Len.PC_RIP_PAT_SET_IM;
        MarshalByte.writeString(buffer, position, tpStatusAeoi, Len.TP_STATUS_AEOI);
        position += Len.TP_STATUS_AEOI;
        MarshalByte.writeString(buffer, position, tpStatusFatca, Len.TP_STATUS_FATCA);
        position += Len.TP_STATUS_FATCA;
        MarshalByte.writeChar(buffer, position, flRappPaMsc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codComunSvolAtt, Len.COD_COMUN_SVOL_ATT);
        position += Len.COD_COMUN_SVOL_ATT;
        MarshalByte.writeString(buffer, position, tpDt1oConCli, Len.TP_DT1O_CON_CLI);
        position += Len.TP_DT1O_CON_CLI;
        MarshalByte.writeString(buffer, position, tpModEnRelaInt, Len.TP_MOD_EN_RELA_INT);
        position += Len.TP_MOD_EN_RELA_INT;
        MarshalByte.writeString(buffer, position, tpReddAnnuLrd, Len.TP_REDD_ANNU_LRD);
        position += Len.TP_REDD_ANNU_LRD;
        MarshalByte.writeString(buffer, position, tpReddCon, Len.TP_REDD_CON);
        position += Len.TP_REDD_CON;
        MarshalByte.writeString(buffer, position, tpOperSocFid, Len.TP_OPER_SOC_FID);
        position += Len.TP_OPER_SOC_FID;
        MarshalByte.writeString(buffer, position, codPaEspMsc1, Len.COD_PA_ESP_MSC1);
        position += Len.COD_PA_ESP_MSC1;
        MarshalByte.writeDecimalAsPacked(buffer, position, impPaEspMsc1.copy());
        position += Len.IMP_PA_ESP_MSC1;
        MarshalByte.writeString(buffer, position, codPaEspMsc2, Len.COD_PA_ESP_MSC2);
        position += Len.COD_PA_ESP_MSC2;
        MarshalByte.writeDecimalAsPacked(buffer, position, impPaEspMsc2.copy());
        position += Len.IMP_PA_ESP_MSC2;
        MarshalByte.writeString(buffer, position, codPaEspMsc3, Len.COD_PA_ESP_MSC3);
        position += Len.COD_PA_ESP_MSC3;
        MarshalByte.writeDecimalAsPacked(buffer, position, impPaEspMsc3.copy());
        position += Len.IMP_PA_ESP_MSC3;
        MarshalByte.writeString(buffer, position, codPaEspMsc4, Len.COD_PA_ESP_MSC4);
        position += Len.COD_PA_ESP_MSC4;
        MarshalByte.writeDecimalAsPacked(buffer, position, impPaEspMsc4.copy());
        position += Len.IMP_PA_ESP_MSC4;
        MarshalByte.writeString(buffer, position, codPaEspMsc5, Len.COD_PA_ESP_MSC5);
        position += Len.COD_PA_ESP_MSC5;
        MarshalByte.writeDecimalAsPacked(buffer, position, impPaEspMsc5.copy());
        position += Len.IMP_PA_ESP_MSC5;
        MarshalByte.writeString(buffer, position, descOrgnFnd, Len.DESC_ORGN_FND);
        position += Len.DESC_ORGN_FND;
        MarshalByte.writeString(buffer, position, codAutDueDil, Len.COD_AUT_DUE_DIL);
        position += Len.COD_AUT_DUE_DIL;
        MarshalByte.writeChar(buffer, position, flPrQuestFatca);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flPrQuestAeoi);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flPrQuestOfac);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flPrQuestKyc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flPrQuestMscq);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpNotPreg, Len.TP_NOT_PREG);
        position += Len.TP_NOT_PREG;
        MarshalByte.writeString(buffer, position, tpProcPnl, Len.TP_PROC_PNL);
        position += Len.TP_PROC_PNL;
        MarshalByte.writeString(buffer, position, codImpCarPub, Len.COD_IMP_CAR_PUB);
        position += Len.COD_IMP_CAR_PUB;
        MarshalByte.writeChar(buffer, position, oprzSospette);
        position += Types.CHAR_SIZE;
        MarshalByte.writeDecimalAsPacked(buffer, position, ultFattAnnu.copy());
        position += Len.ULT_FATT_ANNU;
        MarshalByte.writeString(buffer, position, descPep, Len.DESC_PEP);
        position += Len.DESC_PEP;
        MarshalByte.writeString(buffer, position, numTel2, Len.NUM_TEL2);
        position += Len.NUM_TEL2;
        MarshalByte.writeDecimalAsPacked(buffer, position, impAfi.copy());
        position += Len.IMP_AFI;
        MarshalByte.writeChar(buffer, position, flNewPro);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpMotCambioCntr, Len.TP_MOT_CAMBIO_CNTR);
        position += Len.TP_MOT_CAMBIO_CNTR;
        MarshalByte.writeString(buffer, position, descMotCambioCn, Len.DESC_MOT_CAMBIO_CN);
        position += Len.DESC_MOT_CAMBIO_CN;
        MarshalByte.writeString(buffer, position, codSogg, Len.COD_SOGG);
        return buffer;
    }

    public void initDatiSpaces() {
        idQuestAdegVer = Types.INVALID_INT_VAL;
        codCompAnia = Types.INVALID_INT_VAL;
        idMoviCrz = Types.INVALID_INT_VAL;
        idRappAna = Types.INVALID_INT_VAL;
        idPoli = Types.INVALID_INT_VAL;
        naturaOprz = "";
        orgnFnd = "";
        codQlfcProf = "";
        codNazQlfcProf = "";
        codPrvQlfcProf = "";
        fntRedd = "";
        reddFattAnnu.setNaN();
        codAteco = "";
        valutColl = "";
        dsOperSql = Types.SPACE_CHAR;
        dsVer = Types.INVALID_INT_VAL;
        dsTsCptz = Types.INVALID_LONG_VAL;
        dsUtente = "";
        dsStatoElab = Types.SPACE_CHAR;
        luogoCostituzione = "";
        tpMovi = Types.INVALID_INT_VAL;
        flRagRapp = Types.SPACE_CHAR;
        flPrszTitEff = Types.SPACE_CHAR;
        tpMotRisc = "";
        tpPntVnd = "";
        tpAdegVer = "";
        tpRelaEsec = "";
        tpScoFinRapp = "";
        flPrsz3oPagat = Types.SPACE_CHAR;
        areaGeoProvFnd = "";
        tpDestFnd = "";
        flPaeseResidAut = Types.SPACE_CHAR;
        flPaeseCitAut = Types.SPACE_CHAR;
        flPaeseNazAut = Types.SPACE_CHAR;
        codProfPrec = "";
        flAutPep = Types.SPACE_CHAR;
        flImpCarPub = Types.SPACE_CHAR;
        flLisTerrSorv = Types.SPACE_CHAR;
        tpSitFinPat = "";
        tpSitFinPatCon = "";
        impTotAffUtil.setNaN();
        impTotFinUtil.setNaN();
        impTotAffAcc.setNaN();
        impTotFinAcc.setNaN();
        tpFrmGiurSav = "";
        regCollPoli = "";
        numTel = "";
        numDip = Types.INVALID_INT_VAL;
        tpSitFamConv = "";
        codProfCon = "";
        flEsProcPen = Types.SPACE_CHAR;
        tpCondCliente = "";
        codSae = "";
        tpOperEstero = "";
        statOperEstero = "";
        codPrvSvolAtt = "";
        codStatSvolAtt = "";
        tpSoc = "";
        flIndSocQuot = Types.SPACE_CHAR;
        tpSitGiur = "";
        pcQuoDetTitEff.setNaN();
        tpPrflRshPep = "";
        tpPep = "";
        flNotPreg = Types.SPACE_CHAR;
        dtIniFntRedd = Types.INVALID_INT_VAL;
        fntRedd2 = "";
        dtIniFntRedd2 = Types.INVALID_INT_VAL;
        fntRedd3 = "";
        dtIniFntRedd3 = Types.INVALID_INT_VAL;
        motAssTitEff = "";
        finCostituzione = "";
        descImpCarPub = "";
        descScoFinRapp = "";
        descProcPnl = "";
        descNotPreg = "";
        idAssicurati = Types.INVALID_INT_VAL;
        reddCon.setNaN();
        descLibMotRisc = "";
        tpMotAssTitEff = "";
        tpRagRapp = "";
        codCan = Types.INVALID_INT_VAL;
        tpFinCost = "";
        nazDestFnd = "";
        flAuFatcaAeoi = Types.SPACE_CHAR;
        tpCarFinGiur = "";
        tpCarFinGiurAt = "";
        tpCarFinGiurPa = "";
        flIstituzFin = Types.SPACE_CHAR;
        tpOriFndTitEff = "";
        pcEspAgPaMsc.setNaN();
        flPrTrUsa = Types.SPACE_CHAR;
        flPrTrNoUsa = Types.SPACE_CHAR;
        pcRipPatAsVita.setNaN();
        pcRipPatIm.setNaN();
        pcRipPatSetIm.setNaN();
        tpStatusAeoi = "";
        tpStatusFatca = "";
        flRappPaMsc = Types.SPACE_CHAR;
        codComunSvolAtt = "";
        tpDt1oConCli = "";
        tpModEnRelaInt = "";
        tpReddAnnuLrd = "";
        tpReddCon = "";
        tpOperSocFid = "";
        codPaEspMsc1 = "";
        impPaEspMsc1.setNaN();
        codPaEspMsc2 = "";
        impPaEspMsc2.setNaN();
        codPaEspMsc3 = "";
        impPaEspMsc3.setNaN();
        codPaEspMsc4 = "";
        impPaEspMsc4.setNaN();
        codPaEspMsc5 = "";
        impPaEspMsc5.setNaN();
        descOrgnFnd = "";
        codAutDueDil = "";
        flPrQuestFatca = Types.SPACE_CHAR;
        flPrQuestAeoi = Types.SPACE_CHAR;
        flPrQuestOfac = Types.SPACE_CHAR;
        flPrQuestKyc = Types.SPACE_CHAR;
        flPrQuestMscq = Types.SPACE_CHAR;
        tpNotPreg = "";
        tpProcPnl = "";
        codImpCarPub = "";
        oprzSospette = Types.SPACE_CHAR;
        ultFattAnnu.setNaN();
        descPep = "";
        numTel2 = "";
        impAfi.setNaN();
        flNewPro = Types.SPACE_CHAR;
        tpMotCambioCntr = "";
        descMotCambioCn = "";
        codSogg = "";
    }

    public void setIdQuestAdegVer(int idQuestAdegVer) {
        this.idQuestAdegVer = idQuestAdegVer;
    }

    public int getIdQuestAdegVer() {
        return this.idQuestAdegVer;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setIdRappAna(int idRappAna) {
        this.idRappAna = idRappAna;
    }

    public int getIdRappAna() {
        return this.idRappAna;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setNaturaOprz(String naturaOprz) {
        this.naturaOprz = Functions.subString(naturaOprz, Len.NATURA_OPRZ);
    }

    public String getNaturaOprz() {
        return this.naturaOprz;
    }

    public void setOrgnFnd(String orgnFnd) {
        this.orgnFnd = Functions.subString(orgnFnd, Len.ORGN_FND);
    }

    public String getOrgnFnd() {
        return this.orgnFnd;
    }

    public void setCodQlfcProf(String codQlfcProf) {
        this.codQlfcProf = Functions.subString(codQlfcProf, Len.COD_QLFC_PROF);
    }

    public String getCodQlfcProf() {
        return this.codQlfcProf;
    }

    public void setCodNazQlfcProf(String codNazQlfcProf) {
        this.codNazQlfcProf = Functions.subString(codNazQlfcProf, Len.COD_NAZ_QLFC_PROF);
    }

    public String getCodNazQlfcProf() {
        return this.codNazQlfcProf;
    }

    public void setCodPrvQlfcProf(String codPrvQlfcProf) {
        this.codPrvQlfcProf = Functions.subString(codPrvQlfcProf, Len.COD_PRV_QLFC_PROF);
    }

    public String getCodPrvQlfcProf() {
        return this.codPrvQlfcProf;
    }

    public void setFntRedd(String fntRedd) {
        this.fntRedd = Functions.subString(fntRedd, Len.FNT_REDD);
    }

    public String getFntRedd() {
        return this.fntRedd;
    }

    public void setReddFattAnnu(AfDecimal reddFattAnnu) {
        this.reddFattAnnu.assign(reddFattAnnu);
    }

    public AfDecimal getReddFattAnnu() {
        return this.reddFattAnnu.copy();
    }

    public void setCodAteco(String codAteco) {
        this.codAteco = Functions.subString(codAteco, Len.COD_ATECO);
    }

    public String getCodAteco() {
        return this.codAteco;
    }

    public void setValutColl(String valutColl) {
        this.valutColl = Functions.subString(valutColl, Len.VALUT_COLL);
    }

    public String getValutColl() {
        return this.valutColl;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setLuogoCostituzione(String luogoCostituzione) {
        this.luogoCostituzione = Functions.subString(luogoCostituzione, Len.LUOGO_COSTITUZIONE);
    }

    public String getLuogoCostituzione() {
        return this.luogoCostituzione;
    }

    public void setTpMovi(int tpMovi) {
        this.tpMovi = tpMovi;
    }

    public int getTpMovi() {
        return this.tpMovi;
    }

    public void setFlRagRapp(char flRagRapp) {
        this.flRagRapp = flRagRapp;
    }

    public char getFlRagRapp() {
        return this.flRagRapp;
    }

    public void setFlPrszTitEff(char flPrszTitEff) {
        this.flPrszTitEff = flPrszTitEff;
    }

    public char getFlPrszTitEff() {
        return this.flPrszTitEff;
    }

    public void setTpMotRisc(String tpMotRisc) {
        this.tpMotRisc = Functions.subString(tpMotRisc, Len.TP_MOT_RISC);
    }

    public String getTpMotRisc() {
        return this.tpMotRisc;
    }

    public void setTpPntVnd(String tpPntVnd) {
        this.tpPntVnd = Functions.subString(tpPntVnd, Len.TP_PNT_VND);
    }

    public String getTpPntVnd() {
        return this.tpPntVnd;
    }

    public void setTpAdegVer(String tpAdegVer) {
        this.tpAdegVer = Functions.subString(tpAdegVer, Len.TP_ADEG_VER);
    }

    public String getTpAdegVer() {
        return this.tpAdegVer;
    }

    public void setTpRelaEsec(String tpRelaEsec) {
        this.tpRelaEsec = Functions.subString(tpRelaEsec, Len.TP_RELA_ESEC);
    }

    public String getTpRelaEsec() {
        return this.tpRelaEsec;
    }

    public void setTpScoFinRapp(String tpScoFinRapp) {
        this.tpScoFinRapp = Functions.subString(tpScoFinRapp, Len.TP_SCO_FIN_RAPP);
    }

    public String getTpScoFinRapp() {
        return this.tpScoFinRapp;
    }

    public void setFlPrsz3oPagat(char flPrsz3oPagat) {
        this.flPrsz3oPagat = flPrsz3oPagat;
    }

    public char getFlPrsz3oPagat() {
        return this.flPrsz3oPagat;
    }

    public void setAreaGeoProvFnd(String areaGeoProvFnd) {
        this.areaGeoProvFnd = Functions.subString(areaGeoProvFnd, Len.AREA_GEO_PROV_FND);
    }

    public String getAreaGeoProvFnd() {
        return this.areaGeoProvFnd;
    }

    public void setTpDestFnd(String tpDestFnd) {
        this.tpDestFnd = Functions.subString(tpDestFnd, Len.TP_DEST_FND);
    }

    public String getTpDestFnd() {
        return this.tpDestFnd;
    }

    public void setFlPaeseResidAut(char flPaeseResidAut) {
        this.flPaeseResidAut = flPaeseResidAut;
    }

    public char getFlPaeseResidAut() {
        return this.flPaeseResidAut;
    }

    public void setFlPaeseCitAut(char flPaeseCitAut) {
        this.flPaeseCitAut = flPaeseCitAut;
    }

    public char getFlPaeseCitAut() {
        return this.flPaeseCitAut;
    }

    public void setFlPaeseNazAut(char flPaeseNazAut) {
        this.flPaeseNazAut = flPaeseNazAut;
    }

    public char getFlPaeseNazAut() {
        return this.flPaeseNazAut;
    }

    public void setCodProfPrec(String codProfPrec) {
        this.codProfPrec = Functions.subString(codProfPrec, Len.COD_PROF_PREC);
    }

    public String getCodProfPrec() {
        return this.codProfPrec;
    }

    public void setFlAutPep(char flAutPep) {
        this.flAutPep = flAutPep;
    }

    public char getFlAutPep() {
        return this.flAutPep;
    }

    public void setFlImpCarPub(char flImpCarPub) {
        this.flImpCarPub = flImpCarPub;
    }

    public char getFlImpCarPub() {
        return this.flImpCarPub;
    }

    public void setFlLisTerrSorv(char flLisTerrSorv) {
        this.flLisTerrSorv = flLisTerrSorv;
    }

    public char getFlLisTerrSorv() {
        return this.flLisTerrSorv;
    }

    public void setTpSitFinPat(String tpSitFinPat) {
        this.tpSitFinPat = Functions.subString(tpSitFinPat, Len.TP_SIT_FIN_PAT);
    }

    public String getTpSitFinPat() {
        return this.tpSitFinPat;
    }

    public void setTpSitFinPatCon(String tpSitFinPatCon) {
        this.tpSitFinPatCon = Functions.subString(tpSitFinPatCon, Len.TP_SIT_FIN_PAT_CON);
    }

    public String getTpSitFinPatCon() {
        return this.tpSitFinPatCon;
    }

    public void setImpTotAffUtil(AfDecimal impTotAffUtil) {
        this.impTotAffUtil.assign(impTotAffUtil);
    }

    public AfDecimal getImpTotAffUtil() {
        return this.impTotAffUtil.copy();
    }

    public void setImpTotFinUtil(AfDecimal impTotFinUtil) {
        this.impTotFinUtil.assign(impTotFinUtil);
    }

    public AfDecimal getImpTotFinUtil() {
        return this.impTotFinUtil.copy();
    }

    public void setImpTotAffAcc(AfDecimal impTotAffAcc) {
        this.impTotAffAcc.assign(impTotAffAcc);
    }

    public AfDecimal getImpTotAffAcc() {
        return this.impTotAffAcc.copy();
    }

    public void setImpTotFinAcc(AfDecimal impTotFinAcc) {
        this.impTotFinAcc.assign(impTotFinAcc);
    }

    public AfDecimal getImpTotFinAcc() {
        return this.impTotFinAcc.copy();
    }

    public void setTpFrmGiurSav(String tpFrmGiurSav) {
        this.tpFrmGiurSav = Functions.subString(tpFrmGiurSav, Len.TP_FRM_GIUR_SAV);
    }

    public String getTpFrmGiurSav() {
        return this.tpFrmGiurSav;
    }

    public void setRegCollPoli(String regCollPoli) {
        this.regCollPoli = Functions.subString(regCollPoli, Len.REG_COLL_POLI);
    }

    public String getRegCollPoli() {
        return this.regCollPoli;
    }

    public void setNumTel(String numTel) {
        this.numTel = Functions.subString(numTel, Len.NUM_TEL);
    }

    public String getNumTel() {
        return this.numTel;
    }

    public void setNumDip(int numDip) {
        this.numDip = numDip;
    }

    public int getNumDip() {
        return this.numDip;
    }

    public void setTpSitFamConv(String tpSitFamConv) {
        this.tpSitFamConv = Functions.subString(tpSitFamConv, Len.TP_SIT_FAM_CONV);
    }

    public String getTpSitFamConv() {
        return this.tpSitFamConv;
    }

    public void setCodProfCon(String codProfCon) {
        this.codProfCon = Functions.subString(codProfCon, Len.COD_PROF_CON);
    }

    public String getCodProfCon() {
        return this.codProfCon;
    }

    public void setFlEsProcPen(char flEsProcPen) {
        this.flEsProcPen = flEsProcPen;
    }

    public char getFlEsProcPen() {
        return this.flEsProcPen;
    }

    public void setTpCondCliente(String tpCondCliente) {
        this.tpCondCliente = Functions.subString(tpCondCliente, Len.TP_COND_CLIENTE);
    }

    public String getTpCondCliente() {
        return this.tpCondCliente;
    }

    public void setCodSae(String codSae) {
        this.codSae = Functions.subString(codSae, Len.COD_SAE);
    }

    public String getCodSae() {
        return this.codSae;
    }

    public void setTpOperEstero(String tpOperEstero) {
        this.tpOperEstero = Functions.subString(tpOperEstero, Len.TP_OPER_ESTERO);
    }

    public String getTpOperEstero() {
        return this.tpOperEstero;
    }

    public void setStatOperEstero(String statOperEstero) {
        this.statOperEstero = Functions.subString(statOperEstero, Len.STAT_OPER_ESTERO);
    }

    public String getStatOperEstero() {
        return this.statOperEstero;
    }

    public void setCodPrvSvolAtt(String codPrvSvolAtt) {
        this.codPrvSvolAtt = Functions.subString(codPrvSvolAtt, Len.COD_PRV_SVOL_ATT);
    }

    public String getCodPrvSvolAtt() {
        return this.codPrvSvolAtt;
    }

    public void setCodStatSvolAtt(String codStatSvolAtt) {
        this.codStatSvolAtt = Functions.subString(codStatSvolAtt, Len.COD_STAT_SVOL_ATT);
    }

    public String getCodStatSvolAtt() {
        return this.codStatSvolAtt;
    }

    public void setTpSoc(String tpSoc) {
        this.tpSoc = Functions.subString(tpSoc, Len.TP_SOC);
    }

    public String getTpSoc() {
        return this.tpSoc;
    }

    public void setFlIndSocQuot(char flIndSocQuot) {
        this.flIndSocQuot = flIndSocQuot;
    }

    public char getFlIndSocQuot() {
        return this.flIndSocQuot;
    }

    public void setTpSitGiur(String tpSitGiur) {
        this.tpSitGiur = Functions.subString(tpSitGiur, Len.TP_SIT_GIUR);
    }

    public String getTpSitGiur() {
        return this.tpSitGiur;
    }

    public void setPcQuoDetTitEff(AfDecimal pcQuoDetTitEff) {
        this.pcQuoDetTitEff.assign(pcQuoDetTitEff);
    }

    public AfDecimal getPcQuoDetTitEff() {
        return this.pcQuoDetTitEff.copy();
    }

    public void setTpPrflRshPep(String tpPrflRshPep) {
        this.tpPrflRshPep = Functions.subString(tpPrflRshPep, Len.TP_PRFL_RSH_PEP);
    }

    public String getTpPrflRshPep() {
        return this.tpPrflRshPep;
    }

    public void setTpPep(String tpPep) {
        this.tpPep = Functions.subString(tpPep, Len.TP_PEP);
    }

    public String getTpPep() {
        return this.tpPep;
    }

    public void setFlNotPreg(char flNotPreg) {
        this.flNotPreg = flNotPreg;
    }

    public char getFlNotPreg() {
        return this.flNotPreg;
    }

    public void setDtIniFntRedd(int dtIniFntRedd) {
        this.dtIniFntRedd = dtIniFntRedd;
    }

    public int getDtIniFntRedd() {
        return this.dtIniFntRedd;
    }

    public void setFntRedd2(String fntRedd2) {
        this.fntRedd2 = Functions.subString(fntRedd2, Len.FNT_REDD2);
    }

    public String getFntRedd2() {
        return this.fntRedd2;
    }

    public void setDtIniFntRedd2(int dtIniFntRedd2) {
        this.dtIniFntRedd2 = dtIniFntRedd2;
    }

    public int getDtIniFntRedd2() {
        return this.dtIniFntRedd2;
    }

    public void setFntRedd3(String fntRedd3) {
        this.fntRedd3 = Functions.subString(fntRedd3, Len.FNT_REDD3);
    }

    public String getFntRedd3() {
        return this.fntRedd3;
    }

    public void setDtIniFntRedd3(int dtIniFntRedd3) {
        this.dtIniFntRedd3 = dtIniFntRedd3;
    }

    public int getDtIniFntRedd3() {
        return this.dtIniFntRedd3;
    }

    public void setMotAssTitEff(String motAssTitEff) {
        this.motAssTitEff = Functions.subString(motAssTitEff, Len.MOT_ASS_TIT_EFF);
    }

    public String getMotAssTitEff() {
        return this.motAssTitEff;
    }

    public void setFinCostituzione(String finCostituzione) {
        this.finCostituzione = Functions.subString(finCostituzione, Len.FIN_COSTITUZIONE);
    }

    public String getFinCostituzione() {
        return this.finCostituzione;
    }

    public void setDescImpCarPub(String descImpCarPub) {
        this.descImpCarPub = Functions.subString(descImpCarPub, Len.DESC_IMP_CAR_PUB);
    }

    public String getDescImpCarPub() {
        return this.descImpCarPub;
    }

    public void setDescScoFinRapp(String descScoFinRapp) {
        this.descScoFinRapp = Functions.subString(descScoFinRapp, Len.DESC_SCO_FIN_RAPP);
    }

    public String getDescScoFinRapp() {
        return this.descScoFinRapp;
    }

    public void setDescProcPnl(String descProcPnl) {
        this.descProcPnl = Functions.subString(descProcPnl, Len.DESC_PROC_PNL);
    }

    public String getDescProcPnl() {
        return this.descProcPnl;
    }

    public void setDescNotPreg(String descNotPreg) {
        this.descNotPreg = Functions.subString(descNotPreg, Len.DESC_NOT_PREG);
    }

    public String getDescNotPreg() {
        return this.descNotPreg;
    }

    public void setIdAssicurati(int idAssicurati) {
        this.idAssicurati = idAssicurati;
    }

    public int getIdAssicurati() {
        return this.idAssicurati;
    }

    public void setReddCon(AfDecimal reddCon) {
        this.reddCon.assign(reddCon);
    }

    public AfDecimal getReddCon() {
        return this.reddCon.copy();
    }

    public void setDescLibMotRisc(String descLibMotRisc) {
        this.descLibMotRisc = Functions.subString(descLibMotRisc, Len.DESC_LIB_MOT_RISC);
    }

    public String getDescLibMotRisc() {
        return this.descLibMotRisc;
    }

    public void setTpMotAssTitEff(String tpMotAssTitEff) {
        this.tpMotAssTitEff = Functions.subString(tpMotAssTitEff, Len.TP_MOT_ASS_TIT_EFF);
    }

    public String getTpMotAssTitEff() {
        return this.tpMotAssTitEff;
    }

    public void setTpRagRapp(String tpRagRapp) {
        this.tpRagRapp = Functions.subString(tpRagRapp, Len.TP_RAG_RAPP);
    }

    public String getTpRagRapp() {
        return this.tpRagRapp;
    }

    public void setCodCan(int codCan) {
        this.codCan = codCan;
    }

    public int getCodCan() {
        return this.codCan;
    }

    public void setTpFinCost(String tpFinCost) {
        this.tpFinCost = Functions.subString(tpFinCost, Len.TP_FIN_COST);
    }

    public String getTpFinCost() {
        return this.tpFinCost;
    }

    public void setNazDestFnd(String nazDestFnd) {
        this.nazDestFnd = Functions.subString(nazDestFnd, Len.NAZ_DEST_FND);
    }

    public String getNazDestFnd() {
        return this.nazDestFnd;
    }

    public void setFlAuFatcaAeoi(char flAuFatcaAeoi) {
        this.flAuFatcaAeoi = flAuFatcaAeoi;
    }

    public char getFlAuFatcaAeoi() {
        return this.flAuFatcaAeoi;
    }

    public void setTpCarFinGiur(String tpCarFinGiur) {
        this.tpCarFinGiur = Functions.subString(tpCarFinGiur, Len.TP_CAR_FIN_GIUR);
    }

    public String getTpCarFinGiur() {
        return this.tpCarFinGiur;
    }

    public void setTpCarFinGiurAt(String tpCarFinGiurAt) {
        this.tpCarFinGiurAt = Functions.subString(tpCarFinGiurAt, Len.TP_CAR_FIN_GIUR_AT);
    }

    public String getTpCarFinGiurAt() {
        return this.tpCarFinGiurAt;
    }

    public void setTpCarFinGiurPa(String tpCarFinGiurPa) {
        this.tpCarFinGiurPa = Functions.subString(tpCarFinGiurPa, Len.TP_CAR_FIN_GIUR_PA);
    }

    public String getTpCarFinGiurPa() {
        return this.tpCarFinGiurPa;
    }

    public void setFlIstituzFin(char flIstituzFin) {
        this.flIstituzFin = flIstituzFin;
    }

    public char getFlIstituzFin() {
        return this.flIstituzFin;
    }

    public void setTpOriFndTitEff(String tpOriFndTitEff) {
        this.tpOriFndTitEff = Functions.subString(tpOriFndTitEff, Len.TP_ORI_FND_TIT_EFF);
    }

    public String getTpOriFndTitEff() {
        return this.tpOriFndTitEff;
    }

    public void setPcEspAgPaMsc(AfDecimal pcEspAgPaMsc) {
        this.pcEspAgPaMsc.assign(pcEspAgPaMsc);
    }

    public AfDecimal getPcEspAgPaMsc() {
        return this.pcEspAgPaMsc.copy();
    }

    public void setFlPrTrUsa(char flPrTrUsa) {
        this.flPrTrUsa = flPrTrUsa;
    }

    public char getFlPrTrUsa() {
        return this.flPrTrUsa;
    }

    public void setFlPrTrNoUsa(char flPrTrNoUsa) {
        this.flPrTrNoUsa = flPrTrNoUsa;
    }

    public char getFlPrTrNoUsa() {
        return this.flPrTrNoUsa;
    }

    public void setPcRipPatAsVita(AfDecimal pcRipPatAsVita) {
        this.pcRipPatAsVita.assign(pcRipPatAsVita);
    }

    public AfDecimal getPcRipPatAsVita() {
        return this.pcRipPatAsVita.copy();
    }

    public void setPcRipPatIm(AfDecimal pcRipPatIm) {
        this.pcRipPatIm.assign(pcRipPatIm);
    }

    public AfDecimal getPcRipPatIm() {
        return this.pcRipPatIm.copy();
    }

    public void setPcRipPatSetIm(AfDecimal pcRipPatSetIm) {
        this.pcRipPatSetIm.assign(pcRipPatSetIm);
    }

    public AfDecimal getPcRipPatSetIm() {
        return this.pcRipPatSetIm.copy();
    }

    public void setTpStatusAeoi(String tpStatusAeoi) {
        this.tpStatusAeoi = Functions.subString(tpStatusAeoi, Len.TP_STATUS_AEOI);
    }

    public String getTpStatusAeoi() {
        return this.tpStatusAeoi;
    }

    public void setTpStatusFatca(String tpStatusFatca) {
        this.tpStatusFatca = Functions.subString(tpStatusFatca, Len.TP_STATUS_FATCA);
    }

    public String getTpStatusFatca() {
        return this.tpStatusFatca;
    }

    public void setFlRappPaMsc(char flRappPaMsc) {
        this.flRappPaMsc = flRappPaMsc;
    }

    public char getFlRappPaMsc() {
        return this.flRappPaMsc;
    }

    public void setCodComunSvolAtt(String codComunSvolAtt) {
        this.codComunSvolAtt = Functions.subString(codComunSvolAtt, Len.COD_COMUN_SVOL_ATT);
    }

    public String getCodComunSvolAtt() {
        return this.codComunSvolAtt;
    }

    public void setTpDt1oConCli(String tpDt1oConCli) {
        this.tpDt1oConCli = Functions.subString(tpDt1oConCli, Len.TP_DT1O_CON_CLI);
    }

    public String getTpDt1oConCli() {
        return this.tpDt1oConCli;
    }

    public void setTpModEnRelaInt(String tpModEnRelaInt) {
        this.tpModEnRelaInt = Functions.subString(tpModEnRelaInt, Len.TP_MOD_EN_RELA_INT);
    }

    public String getTpModEnRelaInt() {
        return this.tpModEnRelaInt;
    }

    public void setTpReddAnnuLrd(String tpReddAnnuLrd) {
        this.tpReddAnnuLrd = Functions.subString(tpReddAnnuLrd, Len.TP_REDD_ANNU_LRD);
    }

    public String getTpReddAnnuLrd() {
        return this.tpReddAnnuLrd;
    }

    public void setTpReddCon(String tpReddCon) {
        this.tpReddCon = Functions.subString(tpReddCon, Len.TP_REDD_CON);
    }

    public String getTpReddCon() {
        return this.tpReddCon;
    }

    public void setTpOperSocFid(String tpOperSocFid) {
        this.tpOperSocFid = Functions.subString(tpOperSocFid, Len.TP_OPER_SOC_FID);
    }

    public String getTpOperSocFid() {
        return this.tpOperSocFid;
    }

    public void setCodPaEspMsc1(String codPaEspMsc1) {
        this.codPaEspMsc1 = Functions.subString(codPaEspMsc1, Len.COD_PA_ESP_MSC1);
    }

    public String getCodPaEspMsc1() {
        return this.codPaEspMsc1;
    }

    public void setImpPaEspMsc1(AfDecimal impPaEspMsc1) {
        this.impPaEspMsc1.assign(impPaEspMsc1);
    }

    public AfDecimal getImpPaEspMsc1() {
        return this.impPaEspMsc1.copy();
    }

    public void setCodPaEspMsc2(String codPaEspMsc2) {
        this.codPaEspMsc2 = Functions.subString(codPaEspMsc2, Len.COD_PA_ESP_MSC2);
    }

    public String getCodPaEspMsc2() {
        return this.codPaEspMsc2;
    }

    public void setImpPaEspMsc2(AfDecimal impPaEspMsc2) {
        this.impPaEspMsc2.assign(impPaEspMsc2);
    }

    public AfDecimal getImpPaEspMsc2() {
        return this.impPaEspMsc2.copy();
    }

    public void setCodPaEspMsc3(String codPaEspMsc3) {
        this.codPaEspMsc3 = Functions.subString(codPaEspMsc3, Len.COD_PA_ESP_MSC3);
    }

    public String getCodPaEspMsc3() {
        return this.codPaEspMsc3;
    }

    public void setImpPaEspMsc3(AfDecimal impPaEspMsc3) {
        this.impPaEspMsc3.assign(impPaEspMsc3);
    }

    public AfDecimal getImpPaEspMsc3() {
        return this.impPaEspMsc3.copy();
    }

    public void setCodPaEspMsc4(String codPaEspMsc4) {
        this.codPaEspMsc4 = Functions.subString(codPaEspMsc4, Len.COD_PA_ESP_MSC4);
    }

    public String getCodPaEspMsc4() {
        return this.codPaEspMsc4;
    }

    public void setImpPaEspMsc4(AfDecimal impPaEspMsc4) {
        this.impPaEspMsc4.assign(impPaEspMsc4);
    }

    public AfDecimal getImpPaEspMsc4() {
        return this.impPaEspMsc4.copy();
    }

    public void setCodPaEspMsc5(String codPaEspMsc5) {
        this.codPaEspMsc5 = Functions.subString(codPaEspMsc5, Len.COD_PA_ESP_MSC5);
    }

    public String getCodPaEspMsc5() {
        return this.codPaEspMsc5;
    }

    public void setImpPaEspMsc5(AfDecimal impPaEspMsc5) {
        this.impPaEspMsc5.assign(impPaEspMsc5);
    }

    public AfDecimal getImpPaEspMsc5() {
        return this.impPaEspMsc5.copy();
    }

    public void setDescOrgnFnd(String descOrgnFnd) {
        this.descOrgnFnd = Functions.subString(descOrgnFnd, Len.DESC_ORGN_FND);
    }

    public String getDescOrgnFnd() {
        return this.descOrgnFnd;
    }

    public void setCodAutDueDil(String codAutDueDil) {
        this.codAutDueDil = Functions.subString(codAutDueDil, Len.COD_AUT_DUE_DIL);
    }

    public String getCodAutDueDil() {
        return this.codAutDueDil;
    }

    public void setFlPrQuestFatca(char flPrQuestFatca) {
        this.flPrQuestFatca = flPrQuestFatca;
    }

    public char getFlPrQuestFatca() {
        return this.flPrQuestFatca;
    }

    public void setFlPrQuestAeoi(char flPrQuestAeoi) {
        this.flPrQuestAeoi = flPrQuestAeoi;
    }

    public char getFlPrQuestAeoi() {
        return this.flPrQuestAeoi;
    }

    public void setFlPrQuestOfac(char flPrQuestOfac) {
        this.flPrQuestOfac = flPrQuestOfac;
    }

    public char getFlPrQuestOfac() {
        return this.flPrQuestOfac;
    }

    public void setFlPrQuestKyc(char flPrQuestKyc) {
        this.flPrQuestKyc = flPrQuestKyc;
    }

    public char getFlPrQuestKyc() {
        return this.flPrQuestKyc;
    }

    public void setFlPrQuestMscq(char flPrQuestMscq) {
        this.flPrQuestMscq = flPrQuestMscq;
    }

    public char getFlPrQuestMscq() {
        return this.flPrQuestMscq;
    }

    public void setTpNotPreg(String tpNotPreg) {
        this.tpNotPreg = Functions.subString(tpNotPreg, Len.TP_NOT_PREG);
    }

    public String getTpNotPreg() {
        return this.tpNotPreg;
    }

    public void setTpProcPnl(String tpProcPnl) {
        this.tpProcPnl = Functions.subString(tpProcPnl, Len.TP_PROC_PNL);
    }

    public String getTpProcPnl() {
        return this.tpProcPnl;
    }

    public void setCodImpCarPub(String codImpCarPub) {
        this.codImpCarPub = Functions.subString(codImpCarPub, Len.COD_IMP_CAR_PUB);
    }

    public String getCodImpCarPub() {
        return this.codImpCarPub;
    }

    public void setOprzSospette(char oprzSospette) {
        this.oprzSospette = oprzSospette;
    }

    public char getOprzSospette() {
        return this.oprzSospette;
    }

    public void setUltFattAnnu(AfDecimal ultFattAnnu) {
        this.ultFattAnnu.assign(ultFattAnnu);
    }

    public AfDecimal getUltFattAnnu() {
        return this.ultFattAnnu.copy();
    }

    public void setDescPep(String descPep) {
        this.descPep = Functions.subString(descPep, Len.DESC_PEP);
    }

    public String getDescPep() {
        return this.descPep;
    }

    public void setNumTel2(String numTel2) {
        this.numTel2 = Functions.subString(numTel2, Len.NUM_TEL2);
    }

    public String getNumTel2() {
        return this.numTel2;
    }

    public void setImpAfi(AfDecimal impAfi) {
        this.impAfi.assign(impAfi);
    }

    public AfDecimal getImpAfi() {
        return this.impAfi.copy();
    }

    public void setFlNewPro(char flNewPro) {
        this.flNewPro = flNewPro;
    }

    public char getFlNewPro() {
        return this.flNewPro;
    }

    public void setTpMotCambioCntr(String tpMotCambioCntr) {
        this.tpMotCambioCntr = Functions.subString(tpMotCambioCntr, Len.TP_MOT_CAMBIO_CNTR);
    }

    public String getTpMotCambioCntr() {
        return this.tpMotCambioCntr;
    }

    public void setDescMotCambioCn(String descMotCambioCn) {
        this.descMotCambioCn = Functions.subString(descMotCambioCn, Len.DESC_MOT_CAMBIO_CN);
    }

    public String getDescMotCambioCn() {
        return this.descMotCambioCn;
    }

    public void setCodSogg(String codSogg) {
        this.codSogg = Functions.subString(codSogg, Len.COD_SOGG);
    }

    public String getCodSogg() {
        return this.codSogg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_QUEST_ADEG_VER = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_RAPP_ANA = 5;
        public static final int ID_POLI = 5;
        public static final int NATURA_OPRZ = 2;
        public static final int ORGN_FND = 2;
        public static final int COD_QLFC_PROF = 20;
        public static final int COD_NAZ_QLFC_PROF = 4;
        public static final int COD_PRV_QLFC_PROF = 4;
        public static final int FNT_REDD = 2;
        public static final int REDD_FATT_ANNU = 8;
        public static final int COD_ATECO = 20;
        public static final int VALUT_COLL = 2;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int LUOGO_COSTITUZIONE = 250;
        public static final int TP_MOVI = 3;
        public static final int FL_RAG_RAPP = 1;
        public static final int FL_PRSZ_TIT_EFF = 1;
        public static final int TP_MOT_RISC = 2;
        public static final int TP_PNT_VND = 2;
        public static final int TP_ADEG_VER = 2;
        public static final int TP_RELA_ESEC = 2;
        public static final int TP_SCO_FIN_RAPP = 2;
        public static final int FL_PRSZ3O_PAGAT = 1;
        public static final int AREA_GEO_PROV_FND = 4;
        public static final int TP_DEST_FND = 2;
        public static final int FL_PAESE_RESID_AUT = 1;
        public static final int FL_PAESE_CIT_AUT = 1;
        public static final int FL_PAESE_NAZ_AUT = 1;
        public static final int COD_PROF_PREC = 20;
        public static final int FL_AUT_PEP = 1;
        public static final int FL_IMP_CAR_PUB = 1;
        public static final int FL_LIS_TERR_SORV = 1;
        public static final int TP_SIT_FIN_PAT = 2;
        public static final int TP_SIT_FIN_PAT_CON = 2;
        public static final int IMP_TOT_AFF_UTIL = 8;
        public static final int IMP_TOT_FIN_UTIL = 8;
        public static final int IMP_TOT_AFF_ACC = 8;
        public static final int IMP_TOT_FIN_ACC = 8;
        public static final int TP_FRM_GIUR_SAV = 2;
        public static final int REG_COLL_POLI = 4;
        public static final int NUM_TEL = 20;
        public static final int NUM_DIP = 3;
        public static final int TP_SIT_FAM_CONV = 2;
        public static final int COD_PROF_CON = 20;
        public static final int FL_ES_PROC_PEN = 1;
        public static final int TP_COND_CLIENTE = 2;
        public static final int COD_SAE = 4;
        public static final int TP_OPER_ESTERO = 2;
        public static final int STAT_OPER_ESTERO = 4;
        public static final int COD_PRV_SVOL_ATT = 4;
        public static final int COD_STAT_SVOL_ATT = 4;
        public static final int TP_SOC = 2;
        public static final int FL_IND_SOC_QUOT = 1;
        public static final int TP_SIT_GIUR = 2;
        public static final int PC_QUO_DET_TIT_EFF = 4;
        public static final int TP_PRFL_RSH_PEP = 2;
        public static final int TP_PEP = 2;
        public static final int FL_NOT_PREG = 1;
        public static final int DT_INI_FNT_REDD = 5;
        public static final int FNT_REDD2 = 2;
        public static final int DT_INI_FNT_REDD2 = 5;
        public static final int FNT_REDD3 = 2;
        public static final int DT_INI_FNT_REDD3 = 5;
        public static final int MOT_ASS_TIT_EFF = 250;
        public static final int FIN_COSTITUZIONE = 250;
        public static final int DESC_IMP_CAR_PUB = 250;
        public static final int DESC_SCO_FIN_RAPP = 250;
        public static final int DESC_PROC_PNL = 250;
        public static final int DESC_NOT_PREG = 250;
        public static final int ID_ASSICURATI = 5;
        public static final int REDD_CON = 8;
        public static final int DESC_LIB_MOT_RISC = 250;
        public static final int TP_MOT_ASS_TIT_EFF = 2;
        public static final int TP_RAG_RAPP = 2;
        public static final int COD_CAN = 3;
        public static final int TP_FIN_COST = 2;
        public static final int NAZ_DEST_FND = 4;
        public static final int FL_AU_FATCA_AEOI = 1;
        public static final int TP_CAR_FIN_GIUR = 2;
        public static final int TP_CAR_FIN_GIUR_AT = 2;
        public static final int TP_CAR_FIN_GIUR_PA = 2;
        public static final int FL_ISTITUZ_FIN = 1;
        public static final int TP_ORI_FND_TIT_EFF = 2;
        public static final int PC_ESP_AG_PA_MSC = 4;
        public static final int FL_PR_TR_USA = 1;
        public static final int FL_PR_TR_NO_USA = 1;
        public static final int PC_RIP_PAT_AS_VITA = 4;
        public static final int PC_RIP_PAT_IM = 4;
        public static final int PC_RIP_PAT_SET_IM = 4;
        public static final int TP_STATUS_AEOI = 2;
        public static final int TP_STATUS_FATCA = 2;
        public static final int FL_RAPP_PA_MSC = 1;
        public static final int COD_COMUN_SVOL_ATT = 4;
        public static final int TP_DT1O_CON_CLI = 2;
        public static final int TP_MOD_EN_RELA_INT = 2;
        public static final int TP_REDD_ANNU_LRD = 2;
        public static final int TP_REDD_CON = 2;
        public static final int TP_OPER_SOC_FID = 2;
        public static final int COD_PA_ESP_MSC1 = 4;
        public static final int IMP_PA_ESP_MSC1 = 8;
        public static final int COD_PA_ESP_MSC2 = 4;
        public static final int IMP_PA_ESP_MSC2 = 8;
        public static final int COD_PA_ESP_MSC3 = 4;
        public static final int IMP_PA_ESP_MSC3 = 8;
        public static final int COD_PA_ESP_MSC4 = 4;
        public static final int IMP_PA_ESP_MSC4 = 8;
        public static final int COD_PA_ESP_MSC5 = 4;
        public static final int IMP_PA_ESP_MSC5 = 8;
        public static final int DESC_ORGN_FND = 250;
        public static final int COD_AUT_DUE_DIL = 10;
        public static final int FL_PR_QUEST_FATCA = 1;
        public static final int FL_PR_QUEST_AEOI = 1;
        public static final int FL_PR_QUEST_OFAC = 1;
        public static final int FL_PR_QUEST_KYC = 1;
        public static final int FL_PR_QUEST_MSCQ = 1;
        public static final int TP_NOT_PREG = 2;
        public static final int TP_PROC_PNL = 2;
        public static final int COD_IMP_CAR_PUB = 2;
        public static final int OPRZ_SOSPETTE = 1;
        public static final int ULT_FATT_ANNU = 8;
        public static final int DESC_PEP = 100;
        public static final int NUM_TEL2 = 20;
        public static final int IMP_AFI = 8;
        public static final int FL_NEW_PRO = 1;
        public static final int TP_MOT_CAMBIO_CNTR = 2;
        public static final int DESC_MOT_CAMBIO_CN = 250;
        public static final int COD_SOGG = 20;
        public static final int DATI = ID_QUEST_ADEG_VER + COD_COMP_ANIA + ID_MOVI_CRZ + ID_RAPP_ANA + ID_POLI + NATURA_OPRZ + ORGN_FND + COD_QLFC_PROF + COD_NAZ_QLFC_PROF + COD_PRV_QLFC_PROF + FNT_REDD + REDD_FATT_ANNU + COD_ATECO + VALUT_COLL + DS_OPER_SQL + DS_VER + DS_TS_CPTZ + DS_UTENTE + DS_STATO_ELAB + LUOGO_COSTITUZIONE + TP_MOVI + FL_RAG_RAPP + FL_PRSZ_TIT_EFF + TP_MOT_RISC + TP_PNT_VND + TP_ADEG_VER + TP_RELA_ESEC + TP_SCO_FIN_RAPP + FL_PRSZ3O_PAGAT + AREA_GEO_PROV_FND + TP_DEST_FND + FL_PAESE_RESID_AUT + FL_PAESE_CIT_AUT + FL_PAESE_NAZ_AUT + COD_PROF_PREC + FL_AUT_PEP + FL_IMP_CAR_PUB + FL_LIS_TERR_SORV + TP_SIT_FIN_PAT + TP_SIT_FIN_PAT_CON + IMP_TOT_AFF_UTIL + IMP_TOT_FIN_UTIL + IMP_TOT_AFF_ACC + IMP_TOT_FIN_ACC + TP_FRM_GIUR_SAV + REG_COLL_POLI + NUM_TEL + NUM_DIP + TP_SIT_FAM_CONV + COD_PROF_CON + FL_ES_PROC_PEN + TP_COND_CLIENTE + COD_SAE + TP_OPER_ESTERO + STAT_OPER_ESTERO + COD_PRV_SVOL_ATT + COD_STAT_SVOL_ATT + TP_SOC + FL_IND_SOC_QUOT + TP_SIT_GIUR + PC_QUO_DET_TIT_EFF + TP_PRFL_RSH_PEP + TP_PEP + FL_NOT_PREG + DT_INI_FNT_REDD + FNT_REDD2 + DT_INI_FNT_REDD2 + FNT_REDD3 + DT_INI_FNT_REDD3 + MOT_ASS_TIT_EFF + FIN_COSTITUZIONE + DESC_IMP_CAR_PUB + DESC_SCO_FIN_RAPP + DESC_PROC_PNL + DESC_NOT_PREG + ID_ASSICURATI + REDD_CON + DESC_LIB_MOT_RISC + TP_MOT_ASS_TIT_EFF + TP_RAG_RAPP + COD_CAN + TP_FIN_COST + NAZ_DEST_FND + FL_AU_FATCA_AEOI + TP_CAR_FIN_GIUR + TP_CAR_FIN_GIUR_AT + TP_CAR_FIN_GIUR_PA + FL_ISTITUZ_FIN + TP_ORI_FND_TIT_EFF + PC_ESP_AG_PA_MSC + FL_PR_TR_USA + FL_PR_TR_NO_USA + PC_RIP_PAT_AS_VITA + PC_RIP_PAT_IM + PC_RIP_PAT_SET_IM + TP_STATUS_AEOI + TP_STATUS_FATCA + FL_RAPP_PA_MSC + COD_COMUN_SVOL_ATT + TP_DT1O_CON_CLI + TP_MOD_EN_RELA_INT + TP_REDD_ANNU_LRD + TP_REDD_CON + TP_OPER_SOC_FID + COD_PA_ESP_MSC1 + IMP_PA_ESP_MSC1 + COD_PA_ESP_MSC2 + IMP_PA_ESP_MSC2 + COD_PA_ESP_MSC3 + IMP_PA_ESP_MSC3 + COD_PA_ESP_MSC4 + IMP_PA_ESP_MSC4 + COD_PA_ESP_MSC5 + IMP_PA_ESP_MSC5 + DESC_ORGN_FND + COD_AUT_DUE_DIL + FL_PR_QUEST_FATCA + FL_PR_QUEST_AEOI + FL_PR_QUEST_OFAC + FL_PR_QUEST_KYC + FL_PR_QUEST_MSCQ + TP_NOT_PREG + TP_PROC_PNL + COD_IMP_CAR_PUB + OPRZ_SOSPETTE + ULT_FATT_ANNU + DESC_PEP + NUM_TEL2 + IMP_AFI + FL_NEW_PRO + TP_MOT_CAMBIO_CNTR + DESC_MOT_CAMBIO_CN + COD_SOGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_QUEST_ADEG_VER = 9;
            public static final int COD_COMP_ANIA = 5;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_RAPP_ANA = 9;
            public static final int ID_POLI = 9;
            public static final int REDD_FATT_ANNU = 12;
            public static final int DS_VER = 9;
            public static final int DS_TS_CPTZ = 18;
            public static final int TP_MOVI = 5;
            public static final int IMP_TOT_AFF_UTIL = 12;
            public static final int IMP_TOT_FIN_UTIL = 12;
            public static final int IMP_TOT_AFF_ACC = 12;
            public static final int IMP_TOT_FIN_ACC = 12;
            public static final int NUM_DIP = 5;
            public static final int PC_QUO_DET_TIT_EFF = 3;
            public static final int DT_INI_FNT_REDD = 8;
            public static final int DT_INI_FNT_REDD2 = 8;
            public static final int DT_INI_FNT_REDD3 = 8;
            public static final int ID_ASSICURATI = 9;
            public static final int REDD_CON = 12;
            public static final int COD_CAN = 5;
            public static final int PC_ESP_AG_PA_MSC = 3;
            public static final int PC_RIP_PAT_AS_VITA = 3;
            public static final int PC_RIP_PAT_IM = 3;
            public static final int PC_RIP_PAT_SET_IM = 3;
            public static final int IMP_PA_ESP_MSC1 = 12;
            public static final int IMP_PA_ESP_MSC2 = 12;
            public static final int IMP_PA_ESP_MSC3 = 12;
            public static final int IMP_PA_ESP_MSC4 = 12;
            public static final int IMP_PA_ESP_MSC5 = 12;
            public static final int ULT_FATT_ANNU = 12;
            public static final int IMP_AFI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int REDD_FATT_ANNU = 3;
            public static final int IMP_TOT_AFF_UTIL = 3;
            public static final int IMP_TOT_FIN_UTIL = 3;
            public static final int IMP_TOT_AFF_ACC = 3;
            public static final int IMP_TOT_FIN_ACC = 3;
            public static final int PC_QUO_DET_TIT_EFF = 3;
            public static final int REDD_CON = 3;
            public static final int PC_ESP_AG_PA_MSC = 3;
            public static final int PC_RIP_PAT_AS_VITA = 3;
            public static final int PC_RIP_PAT_IM = 3;
            public static final int PC_RIP_PAT_SET_IM = 3;
            public static final int IMP_PA_ESP_MSC1 = 3;
            public static final int IMP_PA_ESP_MSC2 = 3;
            public static final int IMP_PA_ESP_MSC3 = 3;
            public static final int IMP_PA_ESP_MSC4 = 3;
            public static final int IMP_PA_ESP_MSC5 = 3;
            public static final int ULT_FATT_ANNU = 3;
            public static final int IMP_AFI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
