package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import it.accenture.jnais.ws.enums.Iwfo0051Esito;

/**Original name: IWFO0051<br>
 * Variable: IWFO0051 from copybook IWFO0051<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Iwfo0051 {

    //==== PROPERTIES ====
    //Original name: IWFO0051-CAMPO-OUTPUT-DEFI
    private AfDecimal campoOutputDefi = new AfDecimal(DefaultValues.DEC_VAL, 18, 5);
    //Original name: IWFO0051-ESITO
    private Iwfo0051Esito esito = new Iwfo0051Esito();
    //Original name: IWFO0051-LOG-ERRORE
    private Idsv0001LogErrore logErrore = new Idsv0001LogErrore();

    //==== METHODS ====
    public void initIwfo0051HighValues() {
        initIwfo0051ZonaDatiHighValues();
    }

    public void setZonaDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        campoOutputDefi.assign(MarshalByte.readDecimal(buffer, position, Len.Int.CAMPO_OUTPUT_DEFI, Len.Fract.CAMPO_OUTPUT_DEFI));
        position += Len.CAMPO_OUTPUT_DEFI;
        setAreaErroriBytes(buffer, position);
    }

    public byte[] getZonaDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimal(buffer, position, campoOutputDefi.copy());
        position += Len.CAMPO_OUTPUT_DEFI;
        getAreaErroriBytes(buffer, position);
        return buffer;
    }

    public void initIwfo0051ZonaDatiHighValues() {
        campoOutputDefi.setHigh();
        initIwfo0051AreaErroriHighValues();
    }

    public void setCampoOutputDefi(AfDecimal campoOutputDefi) {
        this.campoOutputDefi.assign(campoOutputDefi);
    }

    public AfDecimal getCampoOutputDefi() {
        return this.campoOutputDefi.copy();
    }

    public void setAreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        esito.setEsito(MarshalByte.readString(buffer, position, Iwfo0051Esito.Len.ESITO));
        position += Iwfo0051Esito.Len.ESITO;
        logErrore.setLogErroreBytes(buffer, position);
    }

    public byte[] getAreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, esito.getEsito(), Iwfo0051Esito.Len.ESITO);
        position += Iwfo0051Esito.Len.ESITO;
        logErrore.getLogErroreBytes(buffer, position);
        return buffer;
    }

    public void initIwfo0051AreaErroriHighValues() {
        esito.setEsito(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Iwfo0051Esito.Len.ESITO));
        logErrore.initIwfo0051LogErroreHighValues();
    }

    public Iwfo0051Esito getEsito() {
        return esito;
    }

    public Idsv0001LogErrore getLogErrore() {
        return logErrore;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CAMPO_OUTPUT_DEFI = 18;
        public static final int AREA_ERRORI = Iwfo0051Esito.Len.ESITO + Idsv0001LogErrore.Len.LOG_ERRORE;
        public static final int ZONA_DATI = CAMPO_OUTPUT_DEFI + AREA_ERRORI;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CAMPO_OUTPUT_DEFI = 13;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int CAMPO_OUTPUT_DEFI = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
