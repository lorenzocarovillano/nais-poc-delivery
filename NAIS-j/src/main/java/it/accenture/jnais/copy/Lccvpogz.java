package it.accenture.jnais.copy;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LCCVPOGZ<br>
 * Copybook: LCCVPOGZ from copybook LCCVPOGZ<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Lccvpogz {

    //==== PROPERTIES ====
    //Original name: WK-POG-MAX-A
    private short maxA = ((short)100);

    //==== METHODS ====
    public short getMaxA() {
        return this.maxA;
    }
}
