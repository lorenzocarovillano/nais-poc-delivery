package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVF301<br>
 * Variable: LDBVF301 from copybook LDBVF301<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbvf301 {

    //==== PROPERTIES ====
    //Original name: LDBVF301-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LDBVF301-DT-LIQ-CED-DA
    private int dtLiqCedDa = DefaultValues.INT_VAL;
    //Original name: LDBVF301-DT-LIQ-CED-DA-DB
    private String dtLiqCedDaDb = DefaultValues.stringVal(Len.DT_LIQ_CED_DA_DB);
    //Original name: LDBVF301-DT-LIQ-CED-A
    private int dtLiqCedA = DefaultValues.INT_VAL;
    //Original name: LDBVF301-DT-LIQ-CED-A-DB
    private String dtLiqCedADb = DefaultValues.stringVal(Len.DT_LIQ_CED_A_DB);

    //==== METHODS ====
    public void setLdbvf301Formatted(String data) {
        byte[] buffer = new byte[Len.LDBVF301];
        MarshalByte.writeString(buffer, 1, data, Len.LDBVF301);
        setLdbvf301Bytes(buffer, 1);
    }

    public String getLdbvf301Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvf301Bytes());
    }

    public byte[] getLdbvf301Bytes() {
        byte[] buffer = new byte[Len.LDBVF301];
        return getLdbvf301Bytes(buffer, 1);
    }

    public void setLdbvf301Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        dtLiqCedDa = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_LIQ_CED_DA, 0);
        position += Len.DT_LIQ_CED_DA;
        dtLiqCedDaDb = MarshalByte.readString(buffer, position, Len.DT_LIQ_CED_DA_DB);
        position += Len.DT_LIQ_CED_DA_DB;
        dtLiqCedA = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_LIQ_CED_A, 0);
        position += Len.DT_LIQ_CED_A;
        dtLiqCedADb = MarshalByte.readString(buffer, position, Len.DT_LIQ_CED_A_DB);
    }

    public byte[] getLdbvf301Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, dtLiqCedDa, Len.Int.DT_LIQ_CED_DA, 0);
        position += Len.DT_LIQ_CED_DA;
        MarshalByte.writeString(buffer, position, dtLiqCedDaDb, Len.DT_LIQ_CED_DA_DB);
        position += Len.DT_LIQ_CED_DA_DB;
        MarshalByte.writeIntAsPacked(buffer, position, dtLiqCedA, Len.Int.DT_LIQ_CED_A, 0);
        position += Len.DT_LIQ_CED_A;
        MarshalByte.writeString(buffer, position, dtLiqCedADb, Len.DT_LIQ_CED_A_DB);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setDtLiqCedDa(int dtLiqCedDa) {
        this.dtLiqCedDa = dtLiqCedDa;
    }

    public int getDtLiqCedDa() {
        return this.dtLiqCedDa;
    }

    public void setDtLiqCedDaDb(String dtLiqCedDaDb) {
        this.dtLiqCedDaDb = Functions.subString(dtLiqCedDaDb, Len.DT_LIQ_CED_DA_DB);
    }

    public String getDtLiqCedDaDb() {
        return this.dtLiqCedDaDb;
    }

    public void setDtLiqCedA(int dtLiqCedA) {
        this.dtLiqCedA = dtLiqCedA;
    }

    public int getDtLiqCedA() {
        return this.dtLiqCedA;
    }

    public void setDtLiqCedADb(String dtLiqCedADb) {
        this.dtLiqCedADb = Functions.subString(dtLiqCedADb, Len.DT_LIQ_CED_A_DB);
    }

    public String getDtLiqCedADb() {
        return this.dtLiqCedADb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_LIQ_CED_DA_DB = 10;
        public static final int DT_LIQ_CED_A_DB = 10;
        public static final int ID_POLI = 5;
        public static final int DT_LIQ_CED_DA = 5;
        public static final int DT_LIQ_CED_A = 5;
        public static final int LDBVF301 = ID_POLI + DT_LIQ_CED_DA + DT_LIQ_CED_DA_DB + DT_LIQ_CED_A + DT_LIQ_CED_A_DB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int DT_LIQ_CED_DA = 8;
            public static final int DT_LIQ_CED_A = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
