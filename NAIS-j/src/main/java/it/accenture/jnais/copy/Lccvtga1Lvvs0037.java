package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVTGA1<br>
 * Variable: LCCVTGA1 from copybook LCCVTGA1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvtga1Lvvs0037 {

    //==== PROPERTIES ====
    /**Original name: DTGA-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA TRCH_DI_GAR
	 *    ALIAS TGA
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: DTGA-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: DTGA-DATI
    private WtgaDati dati = new WtgaDati();

    //==== METHODS ====
    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public WtgaDati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
