package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0002<br>
 * Variable: IDSV0002 from copybook IDSV0002<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv0002 {

    //==== PROPERTIES ====
    //Original name: CALL-PGM
    private String callPgm = "";
    //Original name: CALL-DESC
    private String callDesc = "";

    //==== METHODS ====
    public void setCallPgm(String callPgm) {
        this.callPgm = Functions.subString(callPgm, Len.CALL_PGM);
    }

    public String getCallPgm() {
        return this.callPgm;
    }

    public void setCallDesc(String callDesc) {
        this.callDesc = Functions.subString(callDesc, Len.CALL_DESC);
    }

    public String getCallDesc() {
        return this.callDesc;
    }

    public String getCallDescFormatted() {
        return Functions.padBlanks(getCallDesc(), Len.CALL_DESC);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CALL_DESC = 100;
        public static final int CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
