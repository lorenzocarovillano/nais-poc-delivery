package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-OGGETTO-DEROGA<br>
 * Variable: WCOM-OGGETTO-DEROGA from copybook LCCC0001<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WcomOggettoDeroga {

    //==== PROPERTIES ====
    //Original name: WCOM-ID-OGG-DEROGA
    private int idOggDeroga = DefaultValues.INT_VAL;
    //Original name: WCOM-COD-LIV-AUT-APPARTZA
    private int codLivAutAppartza = DefaultValues.INT_VAL;
    //Original name: WCOM-COD-GRU-AUT-APPARTZA
    private long codGruAutAppartza = DefaultValues.LONG_VAL;
    //Original name: WCOM-COD-LIV-AUT-SUPER
    private int codLivAutSuper = DefaultValues.INT_VAL;
    //Original name: WCOM-COD-GRU-AUT-SUPER
    private long codGruAutSuper = DefaultValues.LONG_VAL;
    //Original name: WCOM-TIPO-DEROGA
    private String tipoDeroga = DefaultValues.stringVal(Len.TIPO_DEROGA);
    //Original name: WCOM-IB-OGG
    private String ibOgg = DefaultValues.stringVal(Len.IB_OGG);
    //Original name: WCOM-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: WCOM-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: WCOM-DS-VER
    private int dsVer = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setOggettoDerogaBytes(byte[] buffer, int offset) {
        int position = offset;
        idOggDeroga = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG_DEROGA, 0);
        position += Len.ID_OGG_DEROGA;
        codLivAutAppartza = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_LIV_AUT_APPARTZA, 0);
        position += Len.COD_LIV_AUT_APPARTZA;
        codGruAutAppartza = MarshalByte.readPackedAsLong(buffer, position, Len.Int.COD_GRU_AUT_APPARTZA, 0);
        position += Len.COD_GRU_AUT_APPARTZA;
        codLivAutSuper = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_LIV_AUT_SUPER, 0);
        position += Len.COD_LIV_AUT_SUPER;
        codGruAutSuper = MarshalByte.readPackedAsLong(buffer, position, Len.Int.COD_GRU_AUT_SUPER, 0);
        position += Len.COD_GRU_AUT_SUPER;
        tipoDeroga = MarshalByte.readString(buffer, position, Len.TIPO_DEROGA);
        position += Len.TIPO_DEROGA;
        ibOgg = MarshalByte.readString(buffer, position, Len.IB_OGG);
        position += Len.IB_OGG;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
    }

    public byte[] getOggettoDerogaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOggDeroga, Len.Int.ID_OGG_DEROGA, 0);
        position += Len.ID_OGG_DEROGA;
        MarshalByte.writeIntAsPacked(buffer, position, codLivAutAppartza, Len.Int.COD_LIV_AUT_APPARTZA, 0);
        position += Len.COD_LIV_AUT_APPARTZA;
        MarshalByte.writeLongAsPacked(buffer, position, codGruAutAppartza, Len.Int.COD_GRU_AUT_APPARTZA, 0);
        position += Len.COD_GRU_AUT_APPARTZA;
        MarshalByte.writeIntAsPacked(buffer, position, codLivAutSuper, Len.Int.COD_LIV_AUT_SUPER, 0);
        position += Len.COD_LIV_AUT_SUPER;
        MarshalByte.writeLongAsPacked(buffer, position, codGruAutSuper, Len.Int.COD_GRU_AUT_SUPER, 0);
        position += Len.COD_GRU_AUT_SUPER;
        MarshalByte.writeString(buffer, position, tipoDeroga, Len.TIPO_DEROGA);
        position += Len.TIPO_DEROGA;
        MarshalByte.writeString(buffer, position, ibOgg, Len.IB_OGG);
        position += Len.IB_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        return buffer;
    }

    public void setIdOggDeroga(int idOggDeroga) {
        this.idOggDeroga = idOggDeroga;
    }

    public int getIdOggDeroga() {
        return this.idOggDeroga;
    }

    public void setCodLivAutAppartza(int codLivAutAppartza) {
        this.codLivAutAppartza = codLivAutAppartza;
    }

    public int getCodLivAutAppartza() {
        return this.codLivAutAppartza;
    }

    public void setCodGruAutAppartza(long codGruAutAppartza) {
        this.codGruAutAppartza = codGruAutAppartza;
    }

    public long getCodGruAutAppartza() {
        return this.codGruAutAppartza;
    }

    public void setCodLivAutSuper(int codLivAutSuper) {
        this.codLivAutSuper = codLivAutSuper;
    }

    public int getCodLivAutSuper() {
        return this.codLivAutSuper;
    }

    public void setCodGruAutSuper(long codGruAutSuper) {
        this.codGruAutSuper = codGruAutSuper;
    }

    public long getCodGruAutSuper() {
        return this.codGruAutSuper;
    }

    public void setTipoDeroga(String tipoDeroga) {
        this.tipoDeroga = Functions.subString(tipoDeroga, Len.TIPO_DEROGA);
    }

    public String getTipoDeroga() {
        return this.tipoDeroga;
    }

    public void setIbOgg(String ibOgg) {
        this.ibOgg = Functions.subString(ibOgg, Len.IB_OGG);
    }

    public String getIbOgg() {
        return this.ibOgg;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG_DEROGA = 5;
        public static final int COD_LIV_AUT_APPARTZA = 3;
        public static final int COD_GRU_AUT_APPARTZA = 6;
        public static final int COD_LIV_AUT_SUPER = 3;
        public static final int COD_GRU_AUT_SUPER = 6;
        public static final int TIPO_DEROGA = 2;
        public static final int IB_OGG = 40;
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int DS_VER = 5;
        public static final int OGGETTO_DEROGA = ID_OGG_DEROGA + COD_LIV_AUT_APPARTZA + COD_GRU_AUT_APPARTZA + COD_LIV_AUT_SUPER + COD_GRU_AUT_SUPER + TIPO_DEROGA + IB_OGG + ID_OGG + TP_OGG + DS_VER;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG_DEROGA = 9;
            public static final int COD_LIV_AUT_APPARTZA = 5;
            public static final int COD_GRU_AUT_APPARTZA = 10;
            public static final int COD_LIV_AUT_SUPER = 5;
            public static final int COD_GRU_AUT_SUPER = 10;
            public static final int ID_OGG = 9;
            public static final int DS_VER = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
