package it.accenture.jnais.copy;

/**Original name: IVVC0218<br>
 * Variable: IVVC0218 from copybook IVVC0218<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ivvc0218Lvvs0032 {

    //==== PROPERTIES ====
    /**Original name: IVVC0213-ALIAS-ADES<br>
	 * <pre>----------------------------------------------------------------*
	 *    ALIAS INPUT VARIABILI
	 *    PORTAFOGLIO VITA - PROCESSO VENDITA
	 *    LUNG.
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 * ASSET
	 * *****************************************************************
	 * --  ALIAS ADESIONE</pre>*/
    private String aliasAdes = "ADE";

    //==== METHODS ====
    public String getAliasAdes() {
        return this.aliasAdes;
    }
}
