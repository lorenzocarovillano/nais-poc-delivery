package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IDSV0014<br>
 * Variable: IDSV0014 from copybook IDSV0014<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsv0014 {

    //==== PROPERTIES ====
    //Original name: WS-TIMESTAMP-N
    private String timestampN = DefaultValues.stringVal(Len.TIMESTAMP_N);
    //Original name: WS-TIMESTAMP-X
    private String timestampX = DefaultValues.stringVal(Len.TIMESTAMP_X);

    //==== METHODS ====
    public void setStrTimestampNFormatted(String data) {
        byte[] buffer = new byte[Len.STR_TIMESTAMP_N];
        MarshalByte.writeString(buffer, 1, data, Len.STR_TIMESTAMP_N);
        setStrTimestampNBytes(buffer, 1);
    }

    public String getStrTimestampNFormatted() {
        return getTimestampNFormatted();
    }

    public void setStrTimestampNBytes(byte[] buffer, int offset) {
        int position = offset;
        timestampN = MarshalByte.readFixedString(buffer, position, Len.TIMESTAMP_N);
    }

    public void setTimestampN(long timestampN) {
        this.timestampN = NumericDisplay.asString(timestampN, Len.TIMESTAMP_N);
    }

    public long getTimestampN() {
        return NumericDisplay.asLong(this.timestampN);
    }

    public String getTimestampNFormatted() {
        return this.timestampN;
    }

    public void setTimestampX(String timestampX) {
        this.timestampX = Functions.subString(timestampX, Len.TIMESTAMP_X);
    }

    public String getTimestampX() {
        return this.timestampX;
    }

    public String getTimestampXFormatted() {
        return Functions.padBlanks(getTimestampX(), Len.TIMESTAMP_X);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DATE_N = 8;
        public static final int DATE_X = 10;
        public static final int TIMESTAMP_N = 18;
        public static final int TIMESTAMP_X = 26;
        public static final int STR_TIMESTAMP_N = TIMESTAMP_N;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
