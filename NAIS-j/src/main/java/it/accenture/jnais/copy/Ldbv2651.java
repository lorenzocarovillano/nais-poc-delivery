package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV2651<br>
 * Variable: LDBV2651 from copybook LDBV2651<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbv2651 {

    //==== PROPERTIES ====
    //Original name: LDBV2651-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV2651-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV2651-TP-MOV1
    private int tpMov1 = DefaultValues.INT_VAL;
    //Original name: LDBV2651-TP-MOV2
    private int tpMov2 = DefaultValues.INT_VAL;
    //Original name: LDBV2651-TP-MOV3
    private int tpMov3 = DefaultValues.INT_VAL;
    //Original name: LDBV2651-DT-INIZ
    private int dtIniz = DefaultValues.INT_VAL;
    //Original name: LDBV2651-DT-FINE
    private int dtFine = DefaultValues.INT_VAL;
    //Original name: LDBV2651-DT-INIZ-DB
    private String dtInizDb = DefaultValues.stringVal(Len.DT_INIZ_DB);
    //Original name: LDBV2651-DT-FINE-DB
    private String dtFineDb = DefaultValues.stringVal(Len.DT_FINE_DB);

    //==== METHODS ====
    public void setLdbv2651Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV2651];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV2651);
        setLdbv2651Bytes(buffer, 1);
    }

    public String getLdbv2651Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv2651Bytes());
    }

    public byte[] getLdbv2651Bytes() {
        byte[] buffer = new byte[Len.LDBV2651];
        return getLdbv2651Bytes(buffer, 1);
    }

    public void setLdbv2651Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpMov1 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOV1, 0);
        position += Len.TP_MOV1;
        tpMov2 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOV2, 0);
        position += Len.TP_MOV2;
        tpMov3 = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOV3, 0);
        position += Len.TP_MOV3;
        dtIniz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INIZ, 0);
        position += Len.DT_INIZ;
        dtFine = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_FINE, 0);
        position += Len.DT_FINE;
        dtInizDb = MarshalByte.readString(buffer, position, Len.DT_INIZ_DB);
        position += Len.DT_INIZ_DB;
        dtFineDb = MarshalByte.readString(buffer, position, Len.DT_FINE_DB);
    }

    public byte[] getLdbv2651Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, tpMov1, Len.Int.TP_MOV1, 0);
        position += Len.TP_MOV1;
        MarshalByte.writeIntAsPacked(buffer, position, tpMov2, Len.Int.TP_MOV2, 0);
        position += Len.TP_MOV2;
        MarshalByte.writeIntAsPacked(buffer, position, tpMov3, Len.Int.TP_MOV3, 0);
        position += Len.TP_MOV3;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniz, Len.Int.DT_INIZ, 0);
        position += Len.DT_INIZ;
        MarshalByte.writeIntAsPacked(buffer, position, dtFine, Len.Int.DT_FINE, 0);
        position += Len.DT_FINE;
        MarshalByte.writeString(buffer, position, dtInizDb, Len.DT_INIZ_DB);
        position += Len.DT_INIZ_DB;
        MarshalByte.writeString(buffer, position, dtFineDb, Len.DT_FINE_DB);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpMov1(int tpMov1) {
        this.tpMov1 = tpMov1;
    }

    public int getTpMov1() {
        return this.tpMov1;
    }

    public void setTpMov2(int tpMov2) {
        this.tpMov2 = tpMov2;
    }

    public int getTpMov2() {
        return this.tpMov2;
    }

    public void setTpMov3(int tpMov3) {
        this.tpMov3 = tpMov3;
    }

    public int getTpMov3() {
        return this.tpMov3;
    }

    public void setDtIniz(int dtIniz) {
        this.dtIniz = dtIniz;
    }

    public int getDtIniz() {
        return this.dtIniz;
    }

    public void setDtFine(int dtFine) {
        this.dtFine = dtFine;
    }

    public int getDtFine() {
        return this.dtFine;
    }

    public void setDtInizDb(String dtInizDb) {
        this.dtInizDb = Functions.subString(dtInizDb, Len.DT_INIZ_DB);
    }

    public String getDtInizDb() {
        return this.dtInizDb;
    }

    public void setDtFineDb(String dtFineDb) {
        this.dtFineDb = Functions.subString(dtFineDb, Len.DT_FINE_DB);
    }

    public String getDtFineDb() {
        return this.dtFineDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int DT_INIZ_DB = 10;
        public static final int DT_FINE_DB = 10;
        public static final int ID_OGG = 5;
        public static final int TP_MOV1 = 3;
        public static final int TP_MOV2 = 3;
        public static final int TP_MOV3 = 3;
        public static final int DT_INIZ = 5;
        public static final int DT_FINE = 5;
        public static final int LDBV2651 = ID_OGG + TP_OGG + TP_MOV1 + TP_MOV2 + TP_MOV3 + DT_INIZ + DT_FINE + DT_INIZ_DB + DT_FINE_DB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;
            public static final int TP_MOV1 = 5;
            public static final int TP_MOV2 = 5;
            public static final int TP_MOV3 = 5;
            public static final int DT_INIZ = 8;
            public static final int DT_FINE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
