package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.WcomTpFrmAssva;
import it.accenture.jnais.ws.enums.WcomTpOggMbs;
import it.accenture.jnais.ws.redefines.WcomIdBatch;
import it.accenture.jnais.ws.redefines.WcomIdJob;

/**Original name: WCOM-DATI-MBS<br>
 * Variable: WCOM-DATI-MBS from copybook LCCC0261<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WcomDatiMbs {

    //==== PROPERTIES ====
    //Original name: WCOM-TP-OGG-MBS
    private WcomTpOggMbs wcomTpOggMbs = new WcomTpOggMbs();
    //Original name: WCOM-ID-BATCH
    private WcomIdBatch wcomIdBatch = new WcomIdBatch();
    //Original name: WCOM-ID-JOB
    private WcomIdJob wcomIdJob = new WcomIdJob();
    //Original name: WCOM-TP-FRM-ASSVA
    private WcomTpFrmAssva wcomTpFrmAssva = new WcomTpFrmAssva();
    //Original name: WCOM-STEP-ELAB
    private char wcomStepElab = DefaultValues.CHAR_VAL;
    //Original name: WCOM-D-INPUT-MOVI-SOSP
    private String wcomDInputMoviSosp = DefaultValues.stringVal(Len.WCOM_D_INPUT_MOVI_SOSP);

    //==== METHODS ====
    public void setDatiMbsBytes(byte[] buffer, int offset) {
        int position = offset;
        wcomTpOggMbs.setWcomTpOggMbs(MarshalByte.readString(buffer, position, WcomTpOggMbs.Len.WCOM_TP_OGG_MBS));
        position += WcomTpOggMbs.Len.WCOM_TP_OGG_MBS;
        wcomIdBatch.setWcomIdBatchFromBuffer(buffer, position);
        position += WcomIdBatch.Len.WCOM_ID_BATCH;
        wcomIdJob.setWcomIdJobFromBuffer(buffer, position);
        position += WcomIdJob.Len.WCOM_ID_JOB;
        wcomTpFrmAssva.setWcomTpFrmAssva(MarshalByte.readString(buffer, position, WcomTpFrmAssva.Len.WCOM_TP_FRM_ASSVA));
        position += WcomTpFrmAssva.Len.WCOM_TP_FRM_ASSVA;
        wcomStepElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wcomDInputMoviSosp = MarshalByte.readString(buffer, position, Len.WCOM_D_INPUT_MOVI_SOSP);
    }

    public byte[] getDatiMbsBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wcomTpOggMbs.getWcomTpOggMbs(), WcomTpOggMbs.Len.WCOM_TP_OGG_MBS);
        position += WcomTpOggMbs.Len.WCOM_TP_OGG_MBS;
        wcomIdBatch.getWcomIdBatchAsBuffer(buffer, position);
        position += WcomIdBatch.Len.WCOM_ID_BATCH;
        wcomIdJob.getWcomIdJobAsBuffer(buffer, position);
        position += WcomIdJob.Len.WCOM_ID_JOB;
        MarshalByte.writeString(buffer, position, wcomTpFrmAssva.getWcomTpFrmAssva(), WcomTpFrmAssva.Len.WCOM_TP_FRM_ASSVA);
        position += WcomTpFrmAssva.Len.WCOM_TP_FRM_ASSVA;
        MarshalByte.writeChar(buffer, position, wcomStepElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wcomDInputMoviSosp, Len.WCOM_D_INPUT_MOVI_SOSP);
        return buffer;
    }

    public void setWcomStepElab(char wcomStepElab) {
        this.wcomStepElab = wcomStepElab;
    }

    public void setWcomStepElabFormatted(String wcomStepElab) {
        setWcomStepElab(Functions.charAt(wcomStepElab, Types.CHAR_SIZE));
    }

    public char getWcomStepElab() {
        return this.wcomStepElab;
    }

    public void setWcomDInputMoviSosp(String wcomDInputMoviSosp) {
        this.wcomDInputMoviSosp = Functions.subString(wcomDInputMoviSosp, Len.WCOM_D_INPUT_MOVI_SOSP);
    }

    public String getWcomDInputMoviSosp() {
        return this.wcomDInputMoviSosp;
    }

    public WcomIdBatch getWcomIdBatch() {
        return wcomIdBatch;
    }

    public WcomIdJob getWcomIdJob() {
        return wcomIdJob;
    }

    public WcomTpFrmAssva getWcomTpFrmAssva() {
        return wcomTpFrmAssva;
    }

    public WcomTpOggMbs getWcomTpOggMbs() {
        return wcomTpOggMbs;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WCOM_STEP_ELAB = 1;
        public static final int WCOM_D_INPUT_MOVI_SOSP = 300;
        public static final int DATI_MBS = WcomTpOggMbs.Len.WCOM_TP_OGG_MBS + WcomIdBatch.Len.WCOM_ID_BATCH + WcomIdJob.Len.WCOM_ID_JOB + WcomTpFrmAssva.Len.WCOM_TP_FRM_ASSVA + WCOM_STEP_ELAB + WCOM_D_INPUT_MOVI_SOSP;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
