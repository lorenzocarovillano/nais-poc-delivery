package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WtitDtApplzMora;
import it.accenture.jnais.ws.redefines.WtitDtCambioVlt;
import it.accenture.jnais.ws.redefines.WtitDtCertFisc;
import it.accenture.jnais.ws.redefines.WtitDtEmisTit;
import it.accenture.jnais.ws.redefines.WtitDtEndCop;
import it.accenture.jnais.ws.redefines.WtitDtEsiTit;
import it.accenture.jnais.ws.redefines.WtitDtIniCop;
import it.accenture.jnais.ws.redefines.WtitDtRichAddRid;
import it.accenture.jnais.ws.redefines.WtitDtVlt;
import it.accenture.jnais.ws.redefines.WtitFraz;
import it.accenture.jnais.ws.redefines.WtitIdMoviChiu;
import it.accenture.jnais.ws.redefines.WtitIdRappAna;
import it.accenture.jnais.ws.redefines.WtitIdRappRete;
import it.accenture.jnais.ws.redefines.WtitImpAder;
import it.accenture.jnais.ws.redefines.WtitImpAz;
import it.accenture.jnais.ws.redefines.WtitImpPag;
import it.accenture.jnais.ws.redefines.WtitImpTfr;
import it.accenture.jnais.ws.redefines.WtitImpTfrStrc;
import it.accenture.jnais.ws.redefines.WtitImpTrasfe;
import it.accenture.jnais.ws.redefines.WtitImpVolo;
import it.accenture.jnais.ws.redefines.WtitNumRatAccorpate;
import it.accenture.jnais.ws.redefines.WtitProgTit;
import it.accenture.jnais.ws.redefines.WtitTotAcqExp;
import it.accenture.jnais.ws.redefines.WtitTotCarAcq;
import it.accenture.jnais.ws.redefines.WtitTotCarGest;
import it.accenture.jnais.ws.redefines.WtitTotCarIas;
import it.accenture.jnais.ws.redefines.WtitTotCarInc;
import it.accenture.jnais.ws.redefines.WtitTotCnbtAntirac;
import it.accenture.jnais.ws.redefines.WtitTotCommisInter;
import it.accenture.jnais.ws.redefines.WtitTotDir;
import it.accenture.jnais.ws.redefines.WtitTotIntrFraz;
import it.accenture.jnais.ws.redefines.WtitTotIntrMora;
import it.accenture.jnais.ws.redefines.WtitTotIntrPrest;
import it.accenture.jnais.ws.redefines.WtitTotIntrRetdt;
import it.accenture.jnais.ws.redefines.WtitTotIntrRiat;
import it.accenture.jnais.ws.redefines.WtitTotManfeeAntic;
import it.accenture.jnais.ws.redefines.WtitTotManfeeRec;
import it.accenture.jnais.ws.redefines.WtitTotManfeeRicor;
import it.accenture.jnais.ws.redefines.WtitTotPreNet;
import it.accenture.jnais.ws.redefines.WtitTotPrePpIas;
import it.accenture.jnais.ws.redefines.WtitTotPreSoloRsh;
import it.accenture.jnais.ws.redefines.WtitTotPreTot;
import it.accenture.jnais.ws.redefines.WtitTotProvAcq1aa;
import it.accenture.jnais.ws.redefines.WtitTotProvAcq2aa;
import it.accenture.jnais.ws.redefines.WtitTotProvDaRec;
import it.accenture.jnais.ws.redefines.WtitTotProvInc;
import it.accenture.jnais.ws.redefines.WtitTotProvRicor;
import it.accenture.jnais.ws.redefines.WtitTotRemunAss;
import it.accenture.jnais.ws.redefines.WtitTotSoprAlt;
import it.accenture.jnais.ws.redefines.WtitTotSoprProf;
import it.accenture.jnais.ws.redefines.WtitTotSoprSan;
import it.accenture.jnais.ws.redefines.WtitTotSoprSpo;
import it.accenture.jnais.ws.redefines.WtitTotSoprTec;
import it.accenture.jnais.ws.redefines.WtitTotSpeAge;
import it.accenture.jnais.ws.redefines.WtitTotSpeMed;
import it.accenture.jnais.ws.redefines.WtitTotTax;
import it.accenture.jnais.ws.redefines.WtitTpCausStor;

/**Original name: WTIT-DATI<br>
 * Variable: WTIT-DATI from copybook LCCVTIT1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WtitDati {

    //==== PROPERTIES ====
    //Original name: WTIT-ID-TIT-CONT
    private int wtitIdTitCont = DefaultValues.INT_VAL;
    //Original name: WTIT-ID-OGG
    private int wtitIdOgg = DefaultValues.INT_VAL;
    //Original name: WTIT-TP-OGG
    private String wtitTpOgg = DefaultValues.stringVal(Len.WTIT_TP_OGG);
    //Original name: WTIT-IB-RICH
    private String wtitIbRich = DefaultValues.stringVal(Len.WTIT_IB_RICH);
    //Original name: WTIT-ID-MOVI-CRZ
    private int wtitIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WTIT-ID-MOVI-CHIU
    private WtitIdMoviChiu wtitIdMoviChiu = new WtitIdMoviChiu();
    //Original name: WTIT-DT-INI-EFF
    private int wtitDtIniEff = DefaultValues.INT_VAL;
    //Original name: WTIT-DT-END-EFF
    private int wtitDtEndEff = DefaultValues.INT_VAL;
    //Original name: WTIT-COD-COMP-ANIA
    private int wtitCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WTIT-TP-TIT
    private String wtitTpTit = DefaultValues.stringVal(Len.WTIT_TP_TIT);
    //Original name: WTIT-PROG-TIT
    private WtitProgTit wtitProgTit = new WtitProgTit();
    //Original name: WTIT-TP-PRE-TIT
    private String wtitTpPreTit = DefaultValues.stringVal(Len.WTIT_TP_PRE_TIT);
    //Original name: WTIT-TP-STAT-TIT
    private String wtitTpStatTit = DefaultValues.stringVal(Len.WTIT_TP_STAT_TIT);
    //Original name: WTIT-DT-INI-COP
    private WtitDtIniCop wtitDtIniCop = new WtitDtIniCop();
    //Original name: WTIT-DT-END-COP
    private WtitDtEndCop wtitDtEndCop = new WtitDtEndCop();
    //Original name: WTIT-IMP-PAG
    private WtitImpPag wtitImpPag = new WtitImpPag();
    //Original name: WTIT-FL-SOLL
    private char wtitFlSoll = DefaultValues.CHAR_VAL;
    //Original name: WTIT-FRAZ
    private WtitFraz wtitFraz = new WtitFraz();
    //Original name: WTIT-DT-APPLZ-MORA
    private WtitDtApplzMora wtitDtApplzMora = new WtitDtApplzMora();
    //Original name: WTIT-FL-MORA
    private char wtitFlMora = DefaultValues.CHAR_VAL;
    //Original name: WTIT-ID-RAPP-RETE
    private WtitIdRappRete wtitIdRappRete = new WtitIdRappRete();
    //Original name: WTIT-ID-RAPP-ANA
    private WtitIdRappAna wtitIdRappAna = new WtitIdRappAna();
    //Original name: WTIT-COD-DVS
    private String wtitCodDvs = DefaultValues.stringVal(Len.WTIT_COD_DVS);
    //Original name: WTIT-DT-EMIS-TIT
    private WtitDtEmisTit wtitDtEmisTit = new WtitDtEmisTit();
    //Original name: WTIT-DT-ESI-TIT
    private WtitDtEsiTit wtitDtEsiTit = new WtitDtEsiTit();
    //Original name: WTIT-TOT-PRE-NET
    private WtitTotPreNet wtitTotPreNet = new WtitTotPreNet();
    //Original name: WTIT-TOT-INTR-FRAZ
    private WtitTotIntrFraz wtitTotIntrFraz = new WtitTotIntrFraz();
    //Original name: WTIT-TOT-INTR-MORA
    private WtitTotIntrMora wtitTotIntrMora = new WtitTotIntrMora();
    //Original name: WTIT-TOT-INTR-PREST
    private WtitTotIntrPrest wtitTotIntrPrest = new WtitTotIntrPrest();
    //Original name: WTIT-TOT-INTR-RETDT
    private WtitTotIntrRetdt wtitTotIntrRetdt = new WtitTotIntrRetdt();
    //Original name: WTIT-TOT-INTR-RIAT
    private WtitTotIntrRiat wtitTotIntrRiat = new WtitTotIntrRiat();
    //Original name: WTIT-TOT-DIR
    private WtitTotDir wtitTotDir = new WtitTotDir();
    //Original name: WTIT-TOT-SPE-MED
    private WtitTotSpeMed wtitTotSpeMed = new WtitTotSpeMed();
    //Original name: WTIT-TOT-TAX
    private WtitTotTax wtitTotTax = new WtitTotTax();
    //Original name: WTIT-TOT-SOPR-SAN
    private WtitTotSoprSan wtitTotSoprSan = new WtitTotSoprSan();
    //Original name: WTIT-TOT-SOPR-TEC
    private WtitTotSoprTec wtitTotSoprTec = new WtitTotSoprTec();
    //Original name: WTIT-TOT-SOPR-SPO
    private WtitTotSoprSpo wtitTotSoprSpo = new WtitTotSoprSpo();
    //Original name: WTIT-TOT-SOPR-PROF
    private WtitTotSoprProf wtitTotSoprProf = new WtitTotSoprProf();
    //Original name: WTIT-TOT-SOPR-ALT
    private WtitTotSoprAlt wtitTotSoprAlt = new WtitTotSoprAlt();
    //Original name: WTIT-TOT-PRE-TOT
    private WtitTotPreTot wtitTotPreTot = new WtitTotPreTot();
    //Original name: WTIT-TOT-PRE-PP-IAS
    private WtitTotPrePpIas wtitTotPrePpIas = new WtitTotPrePpIas();
    //Original name: WTIT-TOT-CAR-ACQ
    private WtitTotCarAcq wtitTotCarAcq = new WtitTotCarAcq();
    //Original name: WTIT-TOT-CAR-GEST
    private WtitTotCarGest wtitTotCarGest = new WtitTotCarGest();
    //Original name: WTIT-TOT-CAR-INC
    private WtitTotCarInc wtitTotCarInc = new WtitTotCarInc();
    //Original name: WTIT-TOT-PRE-SOLO-RSH
    private WtitTotPreSoloRsh wtitTotPreSoloRsh = new WtitTotPreSoloRsh();
    //Original name: WTIT-TOT-PROV-ACQ-1AA
    private WtitTotProvAcq1aa wtitTotProvAcq1aa = new WtitTotProvAcq1aa();
    //Original name: WTIT-TOT-PROV-ACQ-2AA
    private WtitTotProvAcq2aa wtitTotProvAcq2aa = new WtitTotProvAcq2aa();
    //Original name: WTIT-TOT-PROV-RICOR
    private WtitTotProvRicor wtitTotProvRicor = new WtitTotProvRicor();
    //Original name: WTIT-TOT-PROV-INC
    private WtitTotProvInc wtitTotProvInc = new WtitTotProvInc();
    //Original name: WTIT-TOT-PROV-DA-REC
    private WtitTotProvDaRec wtitTotProvDaRec = new WtitTotProvDaRec();
    //Original name: WTIT-IMP-AZ
    private WtitImpAz wtitImpAz = new WtitImpAz();
    //Original name: WTIT-IMP-ADER
    private WtitImpAder wtitImpAder = new WtitImpAder();
    //Original name: WTIT-IMP-TFR
    private WtitImpTfr wtitImpTfr = new WtitImpTfr();
    //Original name: WTIT-IMP-VOLO
    private WtitImpVolo wtitImpVolo = new WtitImpVolo();
    //Original name: WTIT-TOT-MANFEE-ANTIC
    private WtitTotManfeeAntic wtitTotManfeeAntic = new WtitTotManfeeAntic();
    //Original name: WTIT-TOT-MANFEE-RICOR
    private WtitTotManfeeRicor wtitTotManfeeRicor = new WtitTotManfeeRicor();
    //Original name: WTIT-TOT-MANFEE-REC
    private WtitTotManfeeRec wtitTotManfeeRec = new WtitTotManfeeRec();
    //Original name: WTIT-TP-MEZ-PAG-ADD
    private String wtitTpMezPagAdd = DefaultValues.stringVal(Len.WTIT_TP_MEZ_PAG_ADD);
    //Original name: WTIT-ESTR-CNT-CORR-ADD
    private String wtitEstrCntCorrAdd = DefaultValues.stringVal(Len.WTIT_ESTR_CNT_CORR_ADD);
    //Original name: WTIT-DT-VLT
    private WtitDtVlt wtitDtVlt = new WtitDtVlt();
    //Original name: WTIT-FL-FORZ-DT-VLT
    private char wtitFlForzDtVlt = DefaultValues.CHAR_VAL;
    //Original name: WTIT-DT-CAMBIO-VLT
    private WtitDtCambioVlt wtitDtCambioVlt = new WtitDtCambioVlt();
    //Original name: WTIT-TOT-SPE-AGE
    private WtitTotSpeAge wtitTotSpeAge = new WtitTotSpeAge();
    //Original name: WTIT-TOT-CAR-IAS
    private WtitTotCarIas wtitTotCarIas = new WtitTotCarIas();
    //Original name: WTIT-NUM-RAT-ACCORPATE
    private WtitNumRatAccorpate wtitNumRatAccorpate = new WtitNumRatAccorpate();
    //Original name: WTIT-DS-RIGA
    private long wtitDsRiga = DefaultValues.LONG_VAL;
    //Original name: WTIT-DS-OPER-SQL
    private char wtitDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WTIT-DS-VER
    private int wtitDsVer = DefaultValues.INT_VAL;
    //Original name: WTIT-DS-TS-INI-CPTZ
    private long wtitDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WTIT-DS-TS-END-CPTZ
    private long wtitDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WTIT-DS-UTENTE
    private String wtitDsUtente = DefaultValues.stringVal(Len.WTIT_DS_UTENTE);
    //Original name: WTIT-DS-STATO-ELAB
    private char wtitDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WTIT-FL-TIT-DA-REINVST
    private char wtitFlTitDaReinvst = DefaultValues.CHAR_VAL;
    //Original name: WTIT-DT-RICH-ADD-RID
    private WtitDtRichAddRid wtitDtRichAddRid = new WtitDtRichAddRid();
    //Original name: WTIT-TP-ESI-RID
    private String wtitTpEsiRid = DefaultValues.stringVal(Len.WTIT_TP_ESI_RID);
    //Original name: WTIT-COD-IBAN
    private String wtitCodIban = DefaultValues.stringVal(Len.WTIT_COD_IBAN);
    //Original name: WTIT-IMP-TRASFE
    private WtitImpTrasfe wtitImpTrasfe = new WtitImpTrasfe();
    //Original name: WTIT-IMP-TFR-STRC
    private WtitImpTfrStrc wtitImpTfrStrc = new WtitImpTfrStrc();
    //Original name: WTIT-DT-CERT-FISC
    private WtitDtCertFisc wtitDtCertFisc = new WtitDtCertFisc();
    //Original name: WTIT-TP-CAUS-STOR
    private WtitTpCausStor wtitTpCausStor = new WtitTpCausStor();
    //Original name: WTIT-TP-CAUS-DISP-STOR
    private String wtitTpCausDispStor = DefaultValues.stringVal(Len.WTIT_TP_CAUS_DISP_STOR);
    //Original name: WTIT-TP-TIT-MIGRAZ
    private String wtitTpTitMigraz = DefaultValues.stringVal(Len.WTIT_TP_TIT_MIGRAZ);
    //Original name: WTIT-TOT-ACQ-EXP
    private WtitTotAcqExp wtitTotAcqExp = new WtitTotAcqExp();
    //Original name: WTIT-TOT-REMUN-ASS
    private WtitTotRemunAss wtitTotRemunAss = new WtitTotRemunAss();
    //Original name: WTIT-TOT-COMMIS-INTER
    private WtitTotCommisInter wtitTotCommisInter = new WtitTotCommisInter();
    //Original name: WTIT-TP-CAUS-RIMB
    private String wtitTpCausRimb = DefaultValues.stringVal(Len.WTIT_TP_CAUS_RIMB);
    //Original name: WTIT-TOT-CNBT-ANTIRAC
    private WtitTotCnbtAntirac wtitTotCnbtAntirac = new WtitTotCnbtAntirac();
    //Original name: WTIT-FL-INC-AUTOGEN
    private char wtitFlIncAutogen = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wtitIdTitCont = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTIT_ID_TIT_CONT, 0);
        position += Len.WTIT_ID_TIT_CONT;
        wtitIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTIT_ID_OGG, 0);
        position += Len.WTIT_ID_OGG;
        wtitTpOgg = MarshalByte.readString(buffer, position, Len.WTIT_TP_OGG);
        position += Len.WTIT_TP_OGG;
        wtitIbRich = MarshalByte.readString(buffer, position, Len.WTIT_IB_RICH);
        position += Len.WTIT_IB_RICH;
        wtitIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTIT_ID_MOVI_CRZ, 0);
        position += Len.WTIT_ID_MOVI_CRZ;
        wtitIdMoviChiu.setWtitIdMoviChiuFromBuffer(buffer, position);
        position += WtitIdMoviChiu.Len.WTIT_ID_MOVI_CHIU;
        wtitDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTIT_DT_INI_EFF, 0);
        position += Len.WTIT_DT_INI_EFF;
        wtitDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTIT_DT_END_EFF, 0);
        position += Len.WTIT_DT_END_EFF;
        wtitCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTIT_COD_COMP_ANIA, 0);
        position += Len.WTIT_COD_COMP_ANIA;
        wtitTpTit = MarshalByte.readString(buffer, position, Len.WTIT_TP_TIT);
        position += Len.WTIT_TP_TIT;
        wtitProgTit.setWtitProgTitFromBuffer(buffer, position);
        position += WtitProgTit.Len.WTIT_PROG_TIT;
        wtitTpPreTit = MarshalByte.readString(buffer, position, Len.WTIT_TP_PRE_TIT);
        position += Len.WTIT_TP_PRE_TIT;
        wtitTpStatTit = MarshalByte.readString(buffer, position, Len.WTIT_TP_STAT_TIT);
        position += Len.WTIT_TP_STAT_TIT;
        wtitDtIniCop.setWtitDtIniCopFromBuffer(buffer, position);
        position += WtitDtIniCop.Len.WTIT_DT_INI_COP;
        wtitDtEndCop.setWtitDtEndCopFromBuffer(buffer, position);
        position += WtitDtEndCop.Len.WTIT_DT_END_COP;
        wtitImpPag.setWtitImpPagFromBuffer(buffer, position);
        position += WtitImpPag.Len.WTIT_IMP_PAG;
        wtitFlSoll = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtitFraz.setWtitFrazFromBuffer(buffer, position);
        position += WtitFraz.Len.WTIT_FRAZ;
        wtitDtApplzMora.setWtitDtApplzMoraFromBuffer(buffer, position);
        position += WtitDtApplzMora.Len.WTIT_DT_APPLZ_MORA;
        wtitFlMora = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtitIdRappRete.setWtitIdRappReteFromBuffer(buffer, position);
        position += WtitIdRappRete.Len.WTIT_ID_RAPP_RETE;
        wtitIdRappAna.setWtitIdRappAnaFromBuffer(buffer, position);
        position += WtitIdRappAna.Len.WTIT_ID_RAPP_ANA;
        wtitCodDvs = MarshalByte.readString(buffer, position, Len.WTIT_COD_DVS);
        position += Len.WTIT_COD_DVS;
        wtitDtEmisTit.setWtitDtEmisTitFromBuffer(buffer, position);
        position += WtitDtEmisTit.Len.WTIT_DT_EMIS_TIT;
        wtitDtEsiTit.setWtitDtEsiTitFromBuffer(buffer, position);
        position += WtitDtEsiTit.Len.WTIT_DT_ESI_TIT;
        wtitTotPreNet.setWtitTotPreNetFromBuffer(buffer, position);
        position += WtitTotPreNet.Len.WTIT_TOT_PRE_NET;
        wtitTotIntrFraz.setWtitTotIntrFrazFromBuffer(buffer, position);
        position += WtitTotIntrFraz.Len.WTIT_TOT_INTR_FRAZ;
        wtitTotIntrMora.setWtitTotIntrMoraFromBuffer(buffer, position);
        position += WtitTotIntrMora.Len.WTIT_TOT_INTR_MORA;
        wtitTotIntrPrest.setWtitTotIntrPrestFromBuffer(buffer, position);
        position += WtitTotIntrPrest.Len.WTIT_TOT_INTR_PREST;
        wtitTotIntrRetdt.setWtitTotIntrRetdtFromBuffer(buffer, position);
        position += WtitTotIntrRetdt.Len.WTIT_TOT_INTR_RETDT;
        wtitTotIntrRiat.setWtitTotIntrRiatFromBuffer(buffer, position);
        position += WtitTotIntrRiat.Len.WTIT_TOT_INTR_RIAT;
        wtitTotDir.setWtitTotDirFromBuffer(buffer, position);
        position += WtitTotDir.Len.WTIT_TOT_DIR;
        wtitTotSpeMed.setWtitTotSpeMedFromBuffer(buffer, position);
        position += WtitTotSpeMed.Len.WTIT_TOT_SPE_MED;
        wtitTotTax.setWtitTotTaxFromBuffer(buffer, position);
        position += WtitTotTax.Len.WTIT_TOT_TAX;
        wtitTotSoprSan.setWtitTotSoprSanFromBuffer(buffer, position);
        position += WtitTotSoprSan.Len.WTIT_TOT_SOPR_SAN;
        wtitTotSoprTec.setWtitTotSoprTecFromBuffer(buffer, position);
        position += WtitTotSoprTec.Len.WTIT_TOT_SOPR_TEC;
        wtitTotSoprSpo.setWtitTotSoprSpoFromBuffer(buffer, position);
        position += WtitTotSoprSpo.Len.WTIT_TOT_SOPR_SPO;
        wtitTotSoprProf.setWtitTotSoprProfFromBuffer(buffer, position);
        position += WtitTotSoprProf.Len.WTIT_TOT_SOPR_PROF;
        wtitTotSoprAlt.setWtitTotSoprAltFromBuffer(buffer, position);
        position += WtitTotSoprAlt.Len.WTIT_TOT_SOPR_ALT;
        wtitTotPreTot.setWtitTotPreTotFromBuffer(buffer, position);
        position += WtitTotPreTot.Len.WTIT_TOT_PRE_TOT;
        wtitTotPrePpIas.setWtitTotPrePpIasFromBuffer(buffer, position);
        position += WtitTotPrePpIas.Len.WTIT_TOT_PRE_PP_IAS;
        wtitTotCarAcq.setWtitTotCarAcqFromBuffer(buffer, position);
        position += WtitTotCarAcq.Len.WTIT_TOT_CAR_ACQ;
        wtitTotCarGest.setWtitTotCarGestFromBuffer(buffer, position);
        position += WtitTotCarGest.Len.WTIT_TOT_CAR_GEST;
        wtitTotCarInc.setWtitTotCarIncFromBuffer(buffer, position);
        position += WtitTotCarInc.Len.WTIT_TOT_CAR_INC;
        wtitTotPreSoloRsh.setWtitTotPreSoloRshFromBuffer(buffer, position);
        position += WtitTotPreSoloRsh.Len.WTIT_TOT_PRE_SOLO_RSH;
        wtitTotProvAcq1aa.setWtitTotProvAcq1aaFromBuffer(buffer, position);
        position += WtitTotProvAcq1aa.Len.WTIT_TOT_PROV_ACQ1AA;
        wtitTotProvAcq2aa.setWtitTotProvAcq2aaFromBuffer(buffer, position);
        position += WtitTotProvAcq2aa.Len.WTIT_TOT_PROV_ACQ2AA;
        wtitTotProvRicor.setWtitTotProvRicorFromBuffer(buffer, position);
        position += WtitTotProvRicor.Len.WTIT_TOT_PROV_RICOR;
        wtitTotProvInc.setWtitTotProvIncFromBuffer(buffer, position);
        position += WtitTotProvInc.Len.WTIT_TOT_PROV_INC;
        wtitTotProvDaRec.setWtitTotProvDaRecFromBuffer(buffer, position);
        position += WtitTotProvDaRec.Len.WTIT_TOT_PROV_DA_REC;
        wtitImpAz.setWtitImpAzFromBuffer(buffer, position);
        position += WtitImpAz.Len.WTIT_IMP_AZ;
        wtitImpAder.setWtitImpAderFromBuffer(buffer, position);
        position += WtitImpAder.Len.WTIT_IMP_ADER;
        wtitImpTfr.setWtitImpTfrFromBuffer(buffer, position);
        position += WtitImpTfr.Len.WTIT_IMP_TFR;
        wtitImpVolo.setWtitImpVoloFromBuffer(buffer, position);
        position += WtitImpVolo.Len.WTIT_IMP_VOLO;
        wtitTotManfeeAntic.setWtitTotManfeeAnticFromBuffer(buffer, position);
        position += WtitTotManfeeAntic.Len.WTIT_TOT_MANFEE_ANTIC;
        wtitTotManfeeRicor.setWtitTotManfeeRicorFromBuffer(buffer, position);
        position += WtitTotManfeeRicor.Len.WTIT_TOT_MANFEE_RICOR;
        wtitTotManfeeRec.setWtitTotManfeeRecFromBuffer(buffer, position);
        position += WtitTotManfeeRec.Len.WTIT_TOT_MANFEE_REC;
        wtitTpMezPagAdd = MarshalByte.readString(buffer, position, Len.WTIT_TP_MEZ_PAG_ADD);
        position += Len.WTIT_TP_MEZ_PAG_ADD;
        wtitEstrCntCorrAdd = MarshalByte.readString(buffer, position, Len.WTIT_ESTR_CNT_CORR_ADD);
        position += Len.WTIT_ESTR_CNT_CORR_ADD;
        wtitDtVlt.setWtitDtVltFromBuffer(buffer, position);
        position += WtitDtVlt.Len.WTIT_DT_VLT;
        wtitFlForzDtVlt = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtitDtCambioVlt.setWtitDtCambioVltFromBuffer(buffer, position);
        position += WtitDtCambioVlt.Len.WTIT_DT_CAMBIO_VLT;
        wtitTotSpeAge.setWtitTotSpeAgeFromBuffer(buffer, position);
        position += WtitTotSpeAge.Len.WTIT_TOT_SPE_AGE;
        wtitTotCarIas.setWtitTotCarIasFromBuffer(buffer, position);
        position += WtitTotCarIas.Len.WTIT_TOT_CAR_IAS;
        wtitNumRatAccorpate.setWtitNumRatAccorpateFromBuffer(buffer, position);
        position += WtitNumRatAccorpate.Len.WTIT_NUM_RAT_ACCORPATE;
        wtitDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WTIT_DS_RIGA, 0);
        position += Len.WTIT_DS_RIGA;
        wtitDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtitDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTIT_DS_VER, 0);
        position += Len.WTIT_DS_VER;
        wtitDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WTIT_DS_TS_INI_CPTZ, 0);
        position += Len.WTIT_DS_TS_INI_CPTZ;
        wtitDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WTIT_DS_TS_END_CPTZ, 0);
        position += Len.WTIT_DS_TS_END_CPTZ;
        wtitDsUtente = MarshalByte.readString(buffer, position, Len.WTIT_DS_UTENTE);
        position += Len.WTIT_DS_UTENTE;
        wtitDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtitFlTitDaReinvst = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtitDtRichAddRid.setWtitDtRichAddRidFromBuffer(buffer, position);
        position += WtitDtRichAddRid.Len.WTIT_DT_RICH_ADD_RID;
        wtitTpEsiRid = MarshalByte.readString(buffer, position, Len.WTIT_TP_ESI_RID);
        position += Len.WTIT_TP_ESI_RID;
        wtitCodIban = MarshalByte.readString(buffer, position, Len.WTIT_COD_IBAN);
        position += Len.WTIT_COD_IBAN;
        wtitImpTrasfe.setWtitImpTrasfeFromBuffer(buffer, position);
        position += WtitImpTrasfe.Len.WTIT_IMP_TRASFE;
        wtitImpTfrStrc.setWtitImpTfrStrcFromBuffer(buffer, position);
        position += WtitImpTfrStrc.Len.WTIT_IMP_TFR_STRC;
        wtitDtCertFisc.setWtitDtCertFiscFromBuffer(buffer, position);
        position += WtitDtCertFisc.Len.WTIT_DT_CERT_FISC;
        wtitTpCausStor.setWtitTpCausStorFromBuffer(buffer, position);
        position += WtitTpCausStor.Len.WTIT_TP_CAUS_STOR;
        wtitTpCausDispStor = MarshalByte.readString(buffer, position, Len.WTIT_TP_CAUS_DISP_STOR);
        position += Len.WTIT_TP_CAUS_DISP_STOR;
        wtitTpTitMigraz = MarshalByte.readString(buffer, position, Len.WTIT_TP_TIT_MIGRAZ);
        position += Len.WTIT_TP_TIT_MIGRAZ;
        wtitTotAcqExp.setWtitTotAcqExpFromBuffer(buffer, position);
        position += WtitTotAcqExp.Len.WTIT_TOT_ACQ_EXP;
        wtitTotRemunAss.setWtitTotRemunAssFromBuffer(buffer, position);
        position += WtitTotRemunAss.Len.WTIT_TOT_REMUN_ASS;
        wtitTotCommisInter.setWtitTotCommisInterFromBuffer(buffer, position);
        position += WtitTotCommisInter.Len.WTIT_TOT_COMMIS_INTER;
        wtitTpCausRimb = MarshalByte.readString(buffer, position, Len.WTIT_TP_CAUS_RIMB);
        position += Len.WTIT_TP_CAUS_RIMB;
        wtitTotCnbtAntirac.setWtitTotCnbtAntiracFromBuffer(buffer, position);
        position += WtitTotCnbtAntirac.Len.WTIT_TOT_CNBT_ANTIRAC;
        wtitFlIncAutogen = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wtitIdTitCont, Len.Int.WTIT_ID_TIT_CONT, 0);
        position += Len.WTIT_ID_TIT_CONT;
        MarshalByte.writeIntAsPacked(buffer, position, wtitIdOgg, Len.Int.WTIT_ID_OGG, 0);
        position += Len.WTIT_ID_OGG;
        MarshalByte.writeString(buffer, position, wtitTpOgg, Len.WTIT_TP_OGG);
        position += Len.WTIT_TP_OGG;
        MarshalByte.writeString(buffer, position, wtitIbRich, Len.WTIT_IB_RICH);
        position += Len.WTIT_IB_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, wtitIdMoviCrz, Len.Int.WTIT_ID_MOVI_CRZ, 0);
        position += Len.WTIT_ID_MOVI_CRZ;
        wtitIdMoviChiu.getWtitIdMoviChiuAsBuffer(buffer, position);
        position += WtitIdMoviChiu.Len.WTIT_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wtitDtIniEff, Len.Int.WTIT_DT_INI_EFF, 0);
        position += Len.WTIT_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wtitDtEndEff, Len.Int.WTIT_DT_END_EFF, 0);
        position += Len.WTIT_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wtitCodCompAnia, Len.Int.WTIT_COD_COMP_ANIA, 0);
        position += Len.WTIT_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wtitTpTit, Len.WTIT_TP_TIT);
        position += Len.WTIT_TP_TIT;
        wtitProgTit.getWtitProgTitAsBuffer(buffer, position);
        position += WtitProgTit.Len.WTIT_PROG_TIT;
        MarshalByte.writeString(buffer, position, wtitTpPreTit, Len.WTIT_TP_PRE_TIT);
        position += Len.WTIT_TP_PRE_TIT;
        MarshalByte.writeString(buffer, position, wtitTpStatTit, Len.WTIT_TP_STAT_TIT);
        position += Len.WTIT_TP_STAT_TIT;
        wtitDtIniCop.getWtitDtIniCopAsBuffer(buffer, position);
        position += WtitDtIniCop.Len.WTIT_DT_INI_COP;
        wtitDtEndCop.getWtitDtEndCopAsBuffer(buffer, position);
        position += WtitDtEndCop.Len.WTIT_DT_END_COP;
        wtitImpPag.getWtitImpPagAsBuffer(buffer, position);
        position += WtitImpPag.Len.WTIT_IMP_PAG;
        MarshalByte.writeChar(buffer, position, wtitFlSoll);
        position += Types.CHAR_SIZE;
        wtitFraz.getWtitFrazAsBuffer(buffer, position);
        position += WtitFraz.Len.WTIT_FRAZ;
        wtitDtApplzMora.getWtitDtApplzMoraAsBuffer(buffer, position);
        position += WtitDtApplzMora.Len.WTIT_DT_APPLZ_MORA;
        MarshalByte.writeChar(buffer, position, wtitFlMora);
        position += Types.CHAR_SIZE;
        wtitIdRappRete.getWtitIdRappReteAsBuffer(buffer, position);
        position += WtitIdRappRete.Len.WTIT_ID_RAPP_RETE;
        wtitIdRappAna.getWtitIdRappAnaAsBuffer(buffer, position);
        position += WtitIdRappAna.Len.WTIT_ID_RAPP_ANA;
        MarshalByte.writeString(buffer, position, wtitCodDvs, Len.WTIT_COD_DVS);
        position += Len.WTIT_COD_DVS;
        wtitDtEmisTit.getWtitDtEmisTitAsBuffer(buffer, position);
        position += WtitDtEmisTit.Len.WTIT_DT_EMIS_TIT;
        wtitDtEsiTit.getWtitDtEsiTitAsBuffer(buffer, position);
        position += WtitDtEsiTit.Len.WTIT_DT_ESI_TIT;
        wtitTotPreNet.getWtitTotPreNetAsBuffer(buffer, position);
        position += WtitTotPreNet.Len.WTIT_TOT_PRE_NET;
        wtitTotIntrFraz.getWtitTotIntrFrazAsBuffer(buffer, position);
        position += WtitTotIntrFraz.Len.WTIT_TOT_INTR_FRAZ;
        wtitTotIntrMora.getWtitTotIntrMoraAsBuffer(buffer, position);
        position += WtitTotIntrMora.Len.WTIT_TOT_INTR_MORA;
        wtitTotIntrPrest.getWtitTotIntrPrestAsBuffer(buffer, position);
        position += WtitTotIntrPrest.Len.WTIT_TOT_INTR_PREST;
        wtitTotIntrRetdt.getWtitTotIntrRetdtAsBuffer(buffer, position);
        position += WtitTotIntrRetdt.Len.WTIT_TOT_INTR_RETDT;
        wtitTotIntrRiat.getWtitTotIntrRiatAsBuffer(buffer, position);
        position += WtitTotIntrRiat.Len.WTIT_TOT_INTR_RIAT;
        wtitTotDir.getWtitTotDirAsBuffer(buffer, position);
        position += WtitTotDir.Len.WTIT_TOT_DIR;
        wtitTotSpeMed.getWtitTotSpeMedAsBuffer(buffer, position);
        position += WtitTotSpeMed.Len.WTIT_TOT_SPE_MED;
        wtitTotTax.getWtitTotTaxAsBuffer(buffer, position);
        position += WtitTotTax.Len.WTIT_TOT_TAX;
        wtitTotSoprSan.getWtitTotSoprSanAsBuffer(buffer, position);
        position += WtitTotSoprSan.Len.WTIT_TOT_SOPR_SAN;
        wtitTotSoprTec.getWtitTotSoprTecAsBuffer(buffer, position);
        position += WtitTotSoprTec.Len.WTIT_TOT_SOPR_TEC;
        wtitTotSoprSpo.getWtitTotSoprSpoAsBuffer(buffer, position);
        position += WtitTotSoprSpo.Len.WTIT_TOT_SOPR_SPO;
        wtitTotSoprProf.getWtitTotSoprProfAsBuffer(buffer, position);
        position += WtitTotSoprProf.Len.WTIT_TOT_SOPR_PROF;
        wtitTotSoprAlt.getWtitTotSoprAltAsBuffer(buffer, position);
        position += WtitTotSoprAlt.Len.WTIT_TOT_SOPR_ALT;
        wtitTotPreTot.getWtitTotPreTotAsBuffer(buffer, position);
        position += WtitTotPreTot.Len.WTIT_TOT_PRE_TOT;
        wtitTotPrePpIas.getWtitTotPrePpIasAsBuffer(buffer, position);
        position += WtitTotPrePpIas.Len.WTIT_TOT_PRE_PP_IAS;
        wtitTotCarAcq.getWtitTotCarAcqAsBuffer(buffer, position);
        position += WtitTotCarAcq.Len.WTIT_TOT_CAR_ACQ;
        wtitTotCarGest.getWtitTotCarGestAsBuffer(buffer, position);
        position += WtitTotCarGest.Len.WTIT_TOT_CAR_GEST;
        wtitTotCarInc.getWtitTotCarIncAsBuffer(buffer, position);
        position += WtitTotCarInc.Len.WTIT_TOT_CAR_INC;
        wtitTotPreSoloRsh.getWtitTotPreSoloRshAsBuffer(buffer, position);
        position += WtitTotPreSoloRsh.Len.WTIT_TOT_PRE_SOLO_RSH;
        wtitTotProvAcq1aa.getWtitTotProvAcq1aaAsBuffer(buffer, position);
        position += WtitTotProvAcq1aa.Len.WTIT_TOT_PROV_ACQ1AA;
        wtitTotProvAcq2aa.getWtitTotProvAcq2aaAsBuffer(buffer, position);
        position += WtitTotProvAcq2aa.Len.WTIT_TOT_PROV_ACQ2AA;
        wtitTotProvRicor.getWtitTotProvRicorAsBuffer(buffer, position);
        position += WtitTotProvRicor.Len.WTIT_TOT_PROV_RICOR;
        wtitTotProvInc.getWtitTotProvIncAsBuffer(buffer, position);
        position += WtitTotProvInc.Len.WTIT_TOT_PROV_INC;
        wtitTotProvDaRec.getWtitTotProvDaRecAsBuffer(buffer, position);
        position += WtitTotProvDaRec.Len.WTIT_TOT_PROV_DA_REC;
        wtitImpAz.getWtitImpAzAsBuffer(buffer, position);
        position += WtitImpAz.Len.WTIT_IMP_AZ;
        wtitImpAder.getWtitImpAderAsBuffer(buffer, position);
        position += WtitImpAder.Len.WTIT_IMP_ADER;
        wtitImpTfr.getWtitImpTfrAsBuffer(buffer, position);
        position += WtitImpTfr.Len.WTIT_IMP_TFR;
        wtitImpVolo.getWtitImpVoloAsBuffer(buffer, position);
        position += WtitImpVolo.Len.WTIT_IMP_VOLO;
        wtitTotManfeeAntic.getWtitTotManfeeAnticAsBuffer(buffer, position);
        position += WtitTotManfeeAntic.Len.WTIT_TOT_MANFEE_ANTIC;
        wtitTotManfeeRicor.getWtitTotManfeeRicorAsBuffer(buffer, position);
        position += WtitTotManfeeRicor.Len.WTIT_TOT_MANFEE_RICOR;
        wtitTotManfeeRec.getWtitTotManfeeRecAsBuffer(buffer, position);
        position += WtitTotManfeeRec.Len.WTIT_TOT_MANFEE_REC;
        MarshalByte.writeString(buffer, position, wtitTpMezPagAdd, Len.WTIT_TP_MEZ_PAG_ADD);
        position += Len.WTIT_TP_MEZ_PAG_ADD;
        MarshalByte.writeString(buffer, position, wtitEstrCntCorrAdd, Len.WTIT_ESTR_CNT_CORR_ADD);
        position += Len.WTIT_ESTR_CNT_CORR_ADD;
        wtitDtVlt.getWtitDtVltAsBuffer(buffer, position);
        position += WtitDtVlt.Len.WTIT_DT_VLT;
        MarshalByte.writeChar(buffer, position, wtitFlForzDtVlt);
        position += Types.CHAR_SIZE;
        wtitDtCambioVlt.getWtitDtCambioVltAsBuffer(buffer, position);
        position += WtitDtCambioVlt.Len.WTIT_DT_CAMBIO_VLT;
        wtitTotSpeAge.getWtitTotSpeAgeAsBuffer(buffer, position);
        position += WtitTotSpeAge.Len.WTIT_TOT_SPE_AGE;
        wtitTotCarIas.getWtitTotCarIasAsBuffer(buffer, position);
        position += WtitTotCarIas.Len.WTIT_TOT_CAR_IAS;
        wtitNumRatAccorpate.getWtitNumRatAccorpateAsBuffer(buffer, position);
        position += WtitNumRatAccorpate.Len.WTIT_NUM_RAT_ACCORPATE;
        MarshalByte.writeLongAsPacked(buffer, position, wtitDsRiga, Len.Int.WTIT_DS_RIGA, 0);
        position += Len.WTIT_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wtitDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wtitDsVer, Len.Int.WTIT_DS_VER, 0);
        position += Len.WTIT_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wtitDsTsIniCptz, Len.Int.WTIT_DS_TS_INI_CPTZ, 0);
        position += Len.WTIT_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wtitDsTsEndCptz, Len.Int.WTIT_DS_TS_END_CPTZ, 0);
        position += Len.WTIT_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wtitDsUtente, Len.WTIT_DS_UTENTE);
        position += Len.WTIT_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wtitDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, wtitFlTitDaReinvst);
        position += Types.CHAR_SIZE;
        wtitDtRichAddRid.getWtitDtRichAddRidAsBuffer(buffer, position);
        position += WtitDtRichAddRid.Len.WTIT_DT_RICH_ADD_RID;
        MarshalByte.writeString(buffer, position, wtitTpEsiRid, Len.WTIT_TP_ESI_RID);
        position += Len.WTIT_TP_ESI_RID;
        MarshalByte.writeString(buffer, position, wtitCodIban, Len.WTIT_COD_IBAN);
        position += Len.WTIT_COD_IBAN;
        wtitImpTrasfe.getWtitImpTrasfeAsBuffer(buffer, position);
        position += WtitImpTrasfe.Len.WTIT_IMP_TRASFE;
        wtitImpTfrStrc.getWtitImpTfrStrcAsBuffer(buffer, position);
        position += WtitImpTfrStrc.Len.WTIT_IMP_TFR_STRC;
        wtitDtCertFisc.getWtitDtCertFiscAsBuffer(buffer, position);
        position += WtitDtCertFisc.Len.WTIT_DT_CERT_FISC;
        wtitTpCausStor.getWtitTpCausStorAsBuffer(buffer, position);
        position += WtitTpCausStor.Len.WTIT_TP_CAUS_STOR;
        MarshalByte.writeString(buffer, position, wtitTpCausDispStor, Len.WTIT_TP_CAUS_DISP_STOR);
        position += Len.WTIT_TP_CAUS_DISP_STOR;
        MarshalByte.writeString(buffer, position, wtitTpTitMigraz, Len.WTIT_TP_TIT_MIGRAZ);
        position += Len.WTIT_TP_TIT_MIGRAZ;
        wtitTotAcqExp.getWtitTotAcqExpAsBuffer(buffer, position);
        position += WtitTotAcqExp.Len.WTIT_TOT_ACQ_EXP;
        wtitTotRemunAss.getWtitTotRemunAssAsBuffer(buffer, position);
        position += WtitTotRemunAss.Len.WTIT_TOT_REMUN_ASS;
        wtitTotCommisInter.getWtitTotCommisInterAsBuffer(buffer, position);
        position += WtitTotCommisInter.Len.WTIT_TOT_COMMIS_INTER;
        MarshalByte.writeString(buffer, position, wtitTpCausRimb, Len.WTIT_TP_CAUS_RIMB);
        position += Len.WTIT_TP_CAUS_RIMB;
        wtitTotCnbtAntirac.getWtitTotCnbtAntiracAsBuffer(buffer, position);
        position += WtitTotCnbtAntirac.Len.WTIT_TOT_CNBT_ANTIRAC;
        MarshalByte.writeChar(buffer, position, wtitFlIncAutogen);
        return buffer;
    }

    public void initDatiSpaces() {
        wtitIdTitCont = Types.INVALID_INT_VAL;
        wtitIdOgg = Types.INVALID_INT_VAL;
        wtitTpOgg = "";
        wtitIbRich = "";
        wtitIdMoviCrz = Types.INVALID_INT_VAL;
        wtitIdMoviChiu.initWtitIdMoviChiuSpaces();
        wtitDtIniEff = Types.INVALID_INT_VAL;
        wtitDtEndEff = Types.INVALID_INT_VAL;
        wtitCodCompAnia = Types.INVALID_INT_VAL;
        wtitTpTit = "";
        wtitProgTit.initWtitProgTitSpaces();
        wtitTpPreTit = "";
        wtitTpStatTit = "";
        wtitDtIniCop.initWtitDtIniCopSpaces();
        wtitDtEndCop.initWtitDtEndCopSpaces();
        wtitImpPag.initWtitImpPagSpaces();
        wtitFlSoll = Types.SPACE_CHAR;
        wtitFraz.initWtitFrazSpaces();
        wtitDtApplzMora.initWtitDtApplzMoraSpaces();
        wtitFlMora = Types.SPACE_CHAR;
        wtitIdRappRete.initWtitIdRappReteSpaces();
        wtitIdRappAna.initWtitIdRappAnaSpaces();
        wtitCodDvs = "";
        wtitDtEmisTit.initWtitDtEmisTitSpaces();
        wtitDtEsiTit.initWtitDtEsiTitSpaces();
        wtitTotPreNet.initWtitTotPreNetSpaces();
        wtitTotIntrFraz.initWtitTotIntrFrazSpaces();
        wtitTotIntrMora.initWtitTotIntrMoraSpaces();
        wtitTotIntrPrest.initWtitTotIntrPrestSpaces();
        wtitTotIntrRetdt.initWtitTotIntrRetdtSpaces();
        wtitTotIntrRiat.initWtitTotIntrRiatSpaces();
        wtitTotDir.initWtitTotDirSpaces();
        wtitTotSpeMed.initWtitTotSpeMedSpaces();
        wtitTotTax.initWtitTotTaxSpaces();
        wtitTotSoprSan.initWtitTotSoprSanSpaces();
        wtitTotSoprTec.initWtitTotSoprTecSpaces();
        wtitTotSoprSpo.initWtitTotSoprSpoSpaces();
        wtitTotSoprProf.initWtitTotSoprProfSpaces();
        wtitTotSoprAlt.initWtitTotSoprAltSpaces();
        wtitTotPreTot.initWtitTotPreTotSpaces();
        wtitTotPrePpIas.initWtitTotPrePpIasSpaces();
        wtitTotCarAcq.initWtitTotCarAcqSpaces();
        wtitTotCarGest.initWtitTotCarGestSpaces();
        wtitTotCarInc.initWtitTotCarIncSpaces();
        wtitTotPreSoloRsh.initWtitTotPreSoloRshSpaces();
        wtitTotProvAcq1aa.initWtitTotProvAcq1aaSpaces();
        wtitTotProvAcq2aa.initWtitTotProvAcq2aaSpaces();
        wtitTotProvRicor.initWtitTotProvRicorSpaces();
        wtitTotProvInc.initWtitTotProvIncSpaces();
        wtitTotProvDaRec.initWtitTotProvDaRecSpaces();
        wtitImpAz.initWtitImpAzSpaces();
        wtitImpAder.initWtitImpAderSpaces();
        wtitImpTfr.initWtitImpTfrSpaces();
        wtitImpVolo.initWtitImpVoloSpaces();
        wtitTotManfeeAntic.initWtitTotManfeeAnticSpaces();
        wtitTotManfeeRicor.initWtitTotManfeeRicorSpaces();
        wtitTotManfeeRec.initWtitTotManfeeRecSpaces();
        wtitTpMezPagAdd = "";
        wtitEstrCntCorrAdd = "";
        wtitDtVlt.initWtitDtVltSpaces();
        wtitFlForzDtVlt = Types.SPACE_CHAR;
        wtitDtCambioVlt.initWtitDtCambioVltSpaces();
        wtitTotSpeAge.initWtitTotSpeAgeSpaces();
        wtitTotCarIas.initWtitTotCarIasSpaces();
        wtitNumRatAccorpate.initWtitNumRatAccorpateSpaces();
        wtitDsRiga = Types.INVALID_LONG_VAL;
        wtitDsOperSql = Types.SPACE_CHAR;
        wtitDsVer = Types.INVALID_INT_VAL;
        wtitDsTsIniCptz = Types.INVALID_LONG_VAL;
        wtitDsTsEndCptz = Types.INVALID_LONG_VAL;
        wtitDsUtente = "";
        wtitDsStatoElab = Types.SPACE_CHAR;
        wtitFlTitDaReinvst = Types.SPACE_CHAR;
        wtitDtRichAddRid.initWtitDtRichAddRidSpaces();
        wtitTpEsiRid = "";
        wtitCodIban = "";
        wtitImpTrasfe.initWtitImpTrasfeSpaces();
        wtitImpTfrStrc.initWtitImpTfrStrcSpaces();
        wtitDtCertFisc.initWtitDtCertFiscSpaces();
        wtitTpCausStor.initWtitTpCausStorSpaces();
        wtitTpCausDispStor = "";
        wtitTpTitMigraz = "";
        wtitTotAcqExp.initWtitTotAcqExpSpaces();
        wtitTotRemunAss.initWtitTotRemunAssSpaces();
        wtitTotCommisInter.initWtitTotCommisInterSpaces();
        wtitTpCausRimb = "";
        wtitTotCnbtAntirac.initWtitTotCnbtAntiracSpaces();
        wtitFlIncAutogen = Types.SPACE_CHAR;
    }

    public void setWtitIdTitCont(int wtitIdTitCont) {
        this.wtitIdTitCont = wtitIdTitCont;
    }

    public int getWtitIdTitCont() {
        return this.wtitIdTitCont;
    }

    public void setWtitIdOgg(int wtitIdOgg) {
        this.wtitIdOgg = wtitIdOgg;
    }

    public int getWtitIdOgg() {
        return this.wtitIdOgg;
    }

    public void setWtitTpOgg(String wtitTpOgg) {
        this.wtitTpOgg = Functions.subString(wtitTpOgg, Len.WTIT_TP_OGG);
    }

    public String getWtitTpOgg() {
        return this.wtitTpOgg;
    }

    public void setWtitIbRich(String wtitIbRich) {
        this.wtitIbRich = Functions.subString(wtitIbRich, Len.WTIT_IB_RICH);
    }

    public String getWtitIbRich() {
        return this.wtitIbRich;
    }

    public void setWtitIdMoviCrz(int wtitIdMoviCrz) {
        this.wtitIdMoviCrz = wtitIdMoviCrz;
    }

    public int getWtitIdMoviCrz() {
        return this.wtitIdMoviCrz;
    }

    public void setWtitDtIniEff(int wtitDtIniEff) {
        this.wtitDtIniEff = wtitDtIniEff;
    }

    public int getWtitDtIniEff() {
        return this.wtitDtIniEff;
    }

    public void setWtitDtEndEff(int wtitDtEndEff) {
        this.wtitDtEndEff = wtitDtEndEff;
    }

    public int getWtitDtEndEff() {
        return this.wtitDtEndEff;
    }

    public void setWtitCodCompAnia(int wtitCodCompAnia) {
        this.wtitCodCompAnia = wtitCodCompAnia;
    }

    public int getWtitCodCompAnia() {
        return this.wtitCodCompAnia;
    }

    public void setWtitTpTit(String wtitTpTit) {
        this.wtitTpTit = Functions.subString(wtitTpTit, Len.WTIT_TP_TIT);
    }

    public String getWtitTpTit() {
        return this.wtitTpTit;
    }

    public void setWtitTpPreTit(String wtitTpPreTit) {
        this.wtitTpPreTit = Functions.subString(wtitTpPreTit, Len.WTIT_TP_PRE_TIT);
    }

    public String getWtitTpPreTit() {
        return this.wtitTpPreTit;
    }

    public void setWtitTpStatTit(String wtitTpStatTit) {
        this.wtitTpStatTit = Functions.subString(wtitTpStatTit, Len.WTIT_TP_STAT_TIT);
    }

    public String getWtitTpStatTit() {
        return this.wtitTpStatTit;
    }

    public void setWtitFlSoll(char wtitFlSoll) {
        this.wtitFlSoll = wtitFlSoll;
    }

    public char getWtitFlSoll() {
        return this.wtitFlSoll;
    }

    public void setWtitFlMora(char wtitFlMora) {
        this.wtitFlMora = wtitFlMora;
    }

    public char getWtitFlMora() {
        return this.wtitFlMora;
    }

    public void setWtitCodDvs(String wtitCodDvs) {
        this.wtitCodDvs = Functions.subString(wtitCodDvs, Len.WTIT_COD_DVS);
    }

    public String getWtitCodDvs() {
        return this.wtitCodDvs;
    }

    public void setWtitTpMezPagAdd(String wtitTpMezPagAdd) {
        this.wtitTpMezPagAdd = Functions.subString(wtitTpMezPagAdd, Len.WTIT_TP_MEZ_PAG_ADD);
    }

    public String getWtitTpMezPagAdd() {
        return this.wtitTpMezPagAdd;
    }

    public void setWtitEstrCntCorrAdd(String wtitEstrCntCorrAdd) {
        this.wtitEstrCntCorrAdd = Functions.subString(wtitEstrCntCorrAdd, Len.WTIT_ESTR_CNT_CORR_ADD);
    }

    public String getWtitEstrCntCorrAdd() {
        return this.wtitEstrCntCorrAdd;
    }

    public void setWtitFlForzDtVlt(char wtitFlForzDtVlt) {
        this.wtitFlForzDtVlt = wtitFlForzDtVlt;
    }

    public char getWtitFlForzDtVlt() {
        return this.wtitFlForzDtVlt;
    }

    public void setWtitDsRiga(long wtitDsRiga) {
        this.wtitDsRiga = wtitDsRiga;
    }

    public long getWtitDsRiga() {
        return this.wtitDsRiga;
    }

    public void setWtitDsOperSql(char wtitDsOperSql) {
        this.wtitDsOperSql = wtitDsOperSql;
    }

    public char getWtitDsOperSql() {
        return this.wtitDsOperSql;
    }

    public void setWtitDsVer(int wtitDsVer) {
        this.wtitDsVer = wtitDsVer;
    }

    public int getWtitDsVer() {
        return this.wtitDsVer;
    }

    public void setWtitDsTsIniCptz(long wtitDsTsIniCptz) {
        this.wtitDsTsIniCptz = wtitDsTsIniCptz;
    }

    public long getWtitDsTsIniCptz() {
        return this.wtitDsTsIniCptz;
    }

    public void setWtitDsTsEndCptz(long wtitDsTsEndCptz) {
        this.wtitDsTsEndCptz = wtitDsTsEndCptz;
    }

    public long getWtitDsTsEndCptz() {
        return this.wtitDsTsEndCptz;
    }

    public void setWtitDsUtente(String wtitDsUtente) {
        this.wtitDsUtente = Functions.subString(wtitDsUtente, Len.WTIT_DS_UTENTE);
    }

    public String getWtitDsUtente() {
        return this.wtitDsUtente;
    }

    public void setWtitDsStatoElab(char wtitDsStatoElab) {
        this.wtitDsStatoElab = wtitDsStatoElab;
    }

    public char getWtitDsStatoElab() {
        return this.wtitDsStatoElab;
    }

    public void setWtitFlTitDaReinvst(char wtitFlTitDaReinvst) {
        this.wtitFlTitDaReinvst = wtitFlTitDaReinvst;
    }

    public char getWtitFlTitDaReinvst() {
        return this.wtitFlTitDaReinvst;
    }

    public void setWtitTpEsiRid(String wtitTpEsiRid) {
        this.wtitTpEsiRid = Functions.subString(wtitTpEsiRid, Len.WTIT_TP_ESI_RID);
    }

    public String getWtitTpEsiRid() {
        return this.wtitTpEsiRid;
    }

    public void setWtitCodIban(String wtitCodIban) {
        this.wtitCodIban = Functions.subString(wtitCodIban, Len.WTIT_COD_IBAN);
    }

    public String getWtitCodIban() {
        return this.wtitCodIban;
    }

    public void setWtitTpCausDispStor(String wtitTpCausDispStor) {
        this.wtitTpCausDispStor = Functions.subString(wtitTpCausDispStor, Len.WTIT_TP_CAUS_DISP_STOR);
    }

    public String getWtitTpCausDispStor() {
        return this.wtitTpCausDispStor;
    }

    public void setWtitTpTitMigraz(String wtitTpTitMigraz) {
        this.wtitTpTitMigraz = Functions.subString(wtitTpTitMigraz, Len.WTIT_TP_TIT_MIGRAZ);
    }

    public String getWtitTpTitMigraz() {
        return this.wtitTpTitMigraz;
    }

    public void setWtitTpCausRimb(String wtitTpCausRimb) {
        this.wtitTpCausRimb = Functions.subString(wtitTpCausRimb, Len.WTIT_TP_CAUS_RIMB);
    }

    public String getWtitTpCausRimb() {
        return this.wtitTpCausRimb;
    }

    public void setWtitFlIncAutogen(char wtitFlIncAutogen) {
        this.wtitFlIncAutogen = wtitFlIncAutogen;
    }

    public char getWtitFlIncAutogen() {
        return this.wtitFlIncAutogen;
    }

    public WtitDtApplzMora getWtitDtApplzMora() {
        return wtitDtApplzMora;
    }

    public WtitDtCambioVlt getWtitDtCambioVlt() {
        return wtitDtCambioVlt;
    }

    public WtitDtCertFisc getWtitDtCertFisc() {
        return wtitDtCertFisc;
    }

    public WtitDtEmisTit getWtitDtEmisTit() {
        return wtitDtEmisTit;
    }

    public WtitDtEndCop getWtitDtEndCop() {
        return wtitDtEndCop;
    }

    public WtitDtEsiTit getWtitDtEsiTit() {
        return wtitDtEsiTit;
    }

    public WtitDtIniCop getWtitDtIniCop() {
        return wtitDtIniCop;
    }

    public WtitDtRichAddRid getWtitDtRichAddRid() {
        return wtitDtRichAddRid;
    }

    public WtitDtVlt getWtitDtVlt() {
        return wtitDtVlt;
    }

    public WtitFraz getWtitFraz() {
        return wtitFraz;
    }

    public WtitIdMoviChiu getWtitIdMoviChiu() {
        return wtitIdMoviChiu;
    }

    public WtitIdRappAna getWtitIdRappAna() {
        return wtitIdRappAna;
    }

    public WtitIdRappRete getWtitIdRappRete() {
        return wtitIdRappRete;
    }

    public WtitImpAder getWtitImpAder() {
        return wtitImpAder;
    }

    public WtitImpAz getWtitImpAz() {
        return wtitImpAz;
    }

    public WtitImpPag getWtitImpPag() {
        return wtitImpPag;
    }

    public WtitImpTfr getWtitImpTfr() {
        return wtitImpTfr;
    }

    public WtitImpTfrStrc getWtitImpTfrStrc() {
        return wtitImpTfrStrc;
    }

    public WtitImpTrasfe getWtitImpTrasfe() {
        return wtitImpTrasfe;
    }

    public WtitImpVolo getWtitImpVolo() {
        return wtitImpVolo;
    }

    public WtitNumRatAccorpate getWtitNumRatAccorpate() {
        return wtitNumRatAccorpate;
    }

    public WtitProgTit getWtitProgTit() {
        return wtitProgTit;
    }

    public WtitTotAcqExp getWtitTotAcqExp() {
        return wtitTotAcqExp;
    }

    public WtitTotCarAcq getWtitTotCarAcq() {
        return wtitTotCarAcq;
    }

    public WtitTotCarGest getWtitTotCarGest() {
        return wtitTotCarGest;
    }

    public WtitTotCarIas getWtitTotCarIas() {
        return wtitTotCarIas;
    }

    public WtitTotCarInc getWtitTotCarInc() {
        return wtitTotCarInc;
    }

    public WtitTotCnbtAntirac getWtitTotCnbtAntirac() {
        return wtitTotCnbtAntirac;
    }

    public WtitTotCommisInter getWtitTotCommisInter() {
        return wtitTotCommisInter;
    }

    public WtitTotDir getWtitTotDir() {
        return wtitTotDir;
    }

    public WtitTotIntrFraz getWtitTotIntrFraz() {
        return wtitTotIntrFraz;
    }

    public WtitTotIntrMora getWtitTotIntrMora() {
        return wtitTotIntrMora;
    }

    public WtitTotIntrPrest getWtitTotIntrPrest() {
        return wtitTotIntrPrest;
    }

    public WtitTotIntrRetdt getWtitTotIntrRetdt() {
        return wtitTotIntrRetdt;
    }

    public WtitTotIntrRiat getWtitTotIntrRiat() {
        return wtitTotIntrRiat;
    }

    public WtitTotManfeeAntic getWtitTotManfeeAntic() {
        return wtitTotManfeeAntic;
    }

    public WtitTotManfeeRec getWtitTotManfeeRec() {
        return wtitTotManfeeRec;
    }

    public WtitTotManfeeRicor getWtitTotManfeeRicor() {
        return wtitTotManfeeRicor;
    }

    public WtitTotPreNet getWtitTotPreNet() {
        return wtitTotPreNet;
    }

    public WtitTotPrePpIas getWtitTotPrePpIas() {
        return wtitTotPrePpIas;
    }

    public WtitTotPreSoloRsh getWtitTotPreSoloRsh() {
        return wtitTotPreSoloRsh;
    }

    public WtitTotPreTot getWtitTotPreTot() {
        return wtitTotPreTot;
    }

    public WtitTotProvAcq1aa getWtitTotProvAcq1aa() {
        return wtitTotProvAcq1aa;
    }

    public WtitTotProvAcq2aa getWtitTotProvAcq2aa() {
        return wtitTotProvAcq2aa;
    }

    public WtitTotProvDaRec getWtitTotProvDaRec() {
        return wtitTotProvDaRec;
    }

    public WtitTotProvInc getWtitTotProvInc() {
        return wtitTotProvInc;
    }

    public WtitTotProvRicor getWtitTotProvRicor() {
        return wtitTotProvRicor;
    }

    public WtitTotRemunAss getWtitTotRemunAss() {
        return wtitTotRemunAss;
    }

    public WtitTotSoprAlt getWtitTotSoprAlt() {
        return wtitTotSoprAlt;
    }

    public WtitTotSoprProf getWtitTotSoprProf() {
        return wtitTotSoprProf;
    }

    public WtitTotSoprSan getWtitTotSoprSan() {
        return wtitTotSoprSan;
    }

    public WtitTotSoprSpo getWtitTotSoprSpo() {
        return wtitTotSoprSpo;
    }

    public WtitTotSoprTec getWtitTotSoprTec() {
        return wtitTotSoprTec;
    }

    public WtitTotSpeAge getWtitTotSpeAge() {
        return wtitTotSpeAge;
    }

    public WtitTotSpeMed getWtitTotSpeMed() {
        return wtitTotSpeMed;
    }

    public WtitTotTax getWtitTotTax() {
        return wtitTotTax;
    }

    public WtitTpCausStor getWtitTpCausStor() {
        return wtitTpCausStor;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_ID_TIT_CONT = 5;
        public static final int WTIT_ID_OGG = 5;
        public static final int WTIT_TP_OGG = 2;
        public static final int WTIT_IB_RICH = 40;
        public static final int WTIT_ID_MOVI_CRZ = 5;
        public static final int WTIT_DT_INI_EFF = 5;
        public static final int WTIT_DT_END_EFF = 5;
        public static final int WTIT_COD_COMP_ANIA = 3;
        public static final int WTIT_TP_TIT = 2;
        public static final int WTIT_TP_PRE_TIT = 2;
        public static final int WTIT_TP_STAT_TIT = 2;
        public static final int WTIT_FL_SOLL = 1;
        public static final int WTIT_FL_MORA = 1;
        public static final int WTIT_COD_DVS = 20;
        public static final int WTIT_TP_MEZ_PAG_ADD = 2;
        public static final int WTIT_ESTR_CNT_CORR_ADD = 20;
        public static final int WTIT_FL_FORZ_DT_VLT = 1;
        public static final int WTIT_DS_RIGA = 6;
        public static final int WTIT_DS_OPER_SQL = 1;
        public static final int WTIT_DS_VER = 5;
        public static final int WTIT_DS_TS_INI_CPTZ = 10;
        public static final int WTIT_DS_TS_END_CPTZ = 10;
        public static final int WTIT_DS_UTENTE = 20;
        public static final int WTIT_DS_STATO_ELAB = 1;
        public static final int WTIT_FL_TIT_DA_REINVST = 1;
        public static final int WTIT_TP_ESI_RID = 2;
        public static final int WTIT_COD_IBAN = 34;
        public static final int WTIT_TP_CAUS_DISP_STOR = 10;
        public static final int WTIT_TP_TIT_MIGRAZ = 2;
        public static final int WTIT_TP_CAUS_RIMB = 2;
        public static final int WTIT_FL_INC_AUTOGEN = 1;
        public static final int DATI = WTIT_ID_TIT_CONT + WTIT_ID_OGG + WTIT_TP_OGG + WTIT_IB_RICH + WTIT_ID_MOVI_CRZ + WtitIdMoviChiu.Len.WTIT_ID_MOVI_CHIU + WTIT_DT_INI_EFF + WTIT_DT_END_EFF + WTIT_COD_COMP_ANIA + WTIT_TP_TIT + WtitProgTit.Len.WTIT_PROG_TIT + WTIT_TP_PRE_TIT + WTIT_TP_STAT_TIT + WtitDtIniCop.Len.WTIT_DT_INI_COP + WtitDtEndCop.Len.WTIT_DT_END_COP + WtitImpPag.Len.WTIT_IMP_PAG + WTIT_FL_SOLL + WtitFraz.Len.WTIT_FRAZ + WtitDtApplzMora.Len.WTIT_DT_APPLZ_MORA + WTIT_FL_MORA + WtitIdRappRete.Len.WTIT_ID_RAPP_RETE + WtitIdRappAna.Len.WTIT_ID_RAPP_ANA + WTIT_COD_DVS + WtitDtEmisTit.Len.WTIT_DT_EMIS_TIT + WtitDtEsiTit.Len.WTIT_DT_ESI_TIT + WtitTotPreNet.Len.WTIT_TOT_PRE_NET + WtitTotIntrFraz.Len.WTIT_TOT_INTR_FRAZ + WtitTotIntrMora.Len.WTIT_TOT_INTR_MORA + WtitTotIntrPrest.Len.WTIT_TOT_INTR_PREST + WtitTotIntrRetdt.Len.WTIT_TOT_INTR_RETDT + WtitTotIntrRiat.Len.WTIT_TOT_INTR_RIAT + WtitTotDir.Len.WTIT_TOT_DIR + WtitTotSpeMed.Len.WTIT_TOT_SPE_MED + WtitTotTax.Len.WTIT_TOT_TAX + WtitTotSoprSan.Len.WTIT_TOT_SOPR_SAN + WtitTotSoprTec.Len.WTIT_TOT_SOPR_TEC + WtitTotSoprSpo.Len.WTIT_TOT_SOPR_SPO + WtitTotSoprProf.Len.WTIT_TOT_SOPR_PROF + WtitTotSoprAlt.Len.WTIT_TOT_SOPR_ALT + WtitTotPreTot.Len.WTIT_TOT_PRE_TOT + WtitTotPrePpIas.Len.WTIT_TOT_PRE_PP_IAS + WtitTotCarAcq.Len.WTIT_TOT_CAR_ACQ + WtitTotCarGest.Len.WTIT_TOT_CAR_GEST + WtitTotCarInc.Len.WTIT_TOT_CAR_INC + WtitTotPreSoloRsh.Len.WTIT_TOT_PRE_SOLO_RSH + WtitTotProvAcq1aa.Len.WTIT_TOT_PROV_ACQ1AA + WtitTotProvAcq2aa.Len.WTIT_TOT_PROV_ACQ2AA + WtitTotProvRicor.Len.WTIT_TOT_PROV_RICOR + WtitTotProvInc.Len.WTIT_TOT_PROV_INC + WtitTotProvDaRec.Len.WTIT_TOT_PROV_DA_REC + WtitImpAz.Len.WTIT_IMP_AZ + WtitImpAder.Len.WTIT_IMP_ADER + WtitImpTfr.Len.WTIT_IMP_TFR + WtitImpVolo.Len.WTIT_IMP_VOLO + WtitTotManfeeAntic.Len.WTIT_TOT_MANFEE_ANTIC + WtitTotManfeeRicor.Len.WTIT_TOT_MANFEE_RICOR + WtitTotManfeeRec.Len.WTIT_TOT_MANFEE_REC + WTIT_TP_MEZ_PAG_ADD + WTIT_ESTR_CNT_CORR_ADD + WtitDtVlt.Len.WTIT_DT_VLT + WTIT_FL_FORZ_DT_VLT + WtitDtCambioVlt.Len.WTIT_DT_CAMBIO_VLT + WtitTotSpeAge.Len.WTIT_TOT_SPE_AGE + WtitTotCarIas.Len.WTIT_TOT_CAR_IAS + WtitNumRatAccorpate.Len.WTIT_NUM_RAT_ACCORPATE + WTIT_DS_RIGA + WTIT_DS_OPER_SQL + WTIT_DS_VER + WTIT_DS_TS_INI_CPTZ + WTIT_DS_TS_END_CPTZ + WTIT_DS_UTENTE + WTIT_DS_STATO_ELAB + WTIT_FL_TIT_DA_REINVST + WtitDtRichAddRid.Len.WTIT_DT_RICH_ADD_RID + WTIT_TP_ESI_RID + WTIT_COD_IBAN + WtitImpTrasfe.Len.WTIT_IMP_TRASFE + WtitImpTfrStrc.Len.WTIT_IMP_TFR_STRC + WtitDtCertFisc.Len.WTIT_DT_CERT_FISC + WtitTpCausStor.Len.WTIT_TP_CAUS_STOR + WTIT_TP_CAUS_DISP_STOR + WTIT_TP_TIT_MIGRAZ + WtitTotAcqExp.Len.WTIT_TOT_ACQ_EXP + WtitTotRemunAss.Len.WTIT_TOT_REMUN_ASS + WtitTotCommisInter.Len.WTIT_TOT_COMMIS_INTER + WTIT_TP_CAUS_RIMB + WtitTotCnbtAntirac.Len.WTIT_TOT_CNBT_ANTIRAC + WTIT_FL_INC_AUTOGEN;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_ID_TIT_CONT = 9;
            public static final int WTIT_ID_OGG = 9;
            public static final int WTIT_ID_MOVI_CRZ = 9;
            public static final int WTIT_DT_INI_EFF = 8;
            public static final int WTIT_DT_END_EFF = 8;
            public static final int WTIT_COD_COMP_ANIA = 5;
            public static final int WTIT_DS_RIGA = 10;
            public static final int WTIT_DS_VER = 9;
            public static final int WTIT_DS_TS_INI_CPTZ = 18;
            public static final int WTIT_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
