package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVF321<br>
 * Variable: LDBVF321 from copybook LDBVF321<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbvf321 {

    //==== PROPERTIES ====
    //Original name: LDBVF321-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LDBVF321-DT-RIV-DA
    private int dtRivDa = DefaultValues.INT_VAL;
    //Original name: LDBVF321-DT-RIV-DA-DB
    private String dtRivDaDb = DefaultValues.stringVal(Len.DT_RIV_DA_DB);
    //Original name: LDBVF321-DT-RIV-A
    private int dtRivA = DefaultValues.INT_VAL;
    //Original name: LDBVF321-DT-RIV-A-DB
    private String dtRivADb = DefaultValues.stringVal(Len.DT_RIV_A_DB);
    //Original name: LDBVF321-COD-TARI
    private String codTari = DefaultValues.stringVal(Len.COD_TARI);

    //==== METHODS ====
    public void setLdbvf321Formatted(String data) {
        byte[] buffer = new byte[Len.LDBVF321];
        MarshalByte.writeString(buffer, 1, data, Len.LDBVF321);
        setLdbvf321Bytes(buffer, 1);
    }

    public String getLdbvf321Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvf321Bytes());
    }

    public byte[] getLdbvf321Bytes() {
        byte[] buffer = new byte[Len.LDBVF321];
        return getLdbvf321Bytes(buffer, 1);
    }

    public void setLdbvf321Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        dtRivDa = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_RIV_DA, 0);
        position += Len.DT_RIV_DA;
        dtRivDaDb = MarshalByte.readString(buffer, position, Len.DT_RIV_DA_DB);
        position += Len.DT_RIV_DA_DB;
        dtRivA = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_RIV_A, 0);
        position += Len.DT_RIV_A;
        dtRivADb = MarshalByte.readString(buffer, position, Len.DT_RIV_A_DB);
        position += Len.DT_RIV_A_DB;
        codTari = MarshalByte.readString(buffer, position, Len.COD_TARI);
    }

    public byte[] getLdbvf321Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, dtRivDa, Len.Int.DT_RIV_DA, 0);
        position += Len.DT_RIV_DA;
        MarshalByte.writeString(buffer, position, dtRivDaDb, Len.DT_RIV_DA_DB);
        position += Len.DT_RIV_DA_DB;
        MarshalByte.writeIntAsPacked(buffer, position, dtRivA, Len.Int.DT_RIV_A, 0);
        position += Len.DT_RIV_A;
        MarshalByte.writeString(buffer, position, dtRivADb, Len.DT_RIV_A_DB);
        position += Len.DT_RIV_A_DB;
        MarshalByte.writeString(buffer, position, codTari, Len.COD_TARI);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setDtRivDa(int dtRivDa) {
        this.dtRivDa = dtRivDa;
    }

    public int getDtRivDa() {
        return this.dtRivDa;
    }

    public void setDtRivDaDb(String dtRivDaDb) {
        this.dtRivDaDb = Functions.subString(dtRivDaDb, Len.DT_RIV_DA_DB);
    }

    public String getDtRivDaDb() {
        return this.dtRivDaDb;
    }

    public void setDtRivA(int dtRivA) {
        this.dtRivA = dtRivA;
    }

    public int getDtRivA() {
        return this.dtRivA;
    }

    public void setDtRivADb(String dtRivADb) {
        this.dtRivADb = Functions.subString(dtRivADb, Len.DT_RIV_A_DB);
    }

    public String getDtRivADb() {
        return this.dtRivADb;
    }

    public void setCodTari(String codTari) {
        this.codTari = Functions.subString(codTari, Len.COD_TARI);
    }

    public String getCodTari() {
        return this.codTari;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_RIV_DA_DB = 10;
        public static final int DT_RIV_A_DB = 10;
        public static final int COD_TARI = 12;
        public static final int ID_POLI = 5;
        public static final int DT_RIV_DA = 5;
        public static final int DT_RIV_A = 5;
        public static final int LDBVF321 = ID_POLI + DT_RIV_DA + DT_RIV_DA_DB + DT_RIV_A + DT_RIV_A_DB + COD_TARI;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int DT_RIV_DA = 8;
            public static final int DT_RIV_A = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
