package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WmfzCosOprz;
import it.accenture.jnais.ws.redefines.WmfzDtEffMoviFinrio;
import it.accenture.jnais.ws.redefines.WmfzDtElab;
import it.accenture.jnais.ws.redefines.WmfzDtRichMovi;
import it.accenture.jnais.ws.redefines.WmfzIdAdes;
import it.accenture.jnais.ws.redefines.WmfzIdLiq;
import it.accenture.jnais.ws.redefines.WmfzIdMoviChiu;
import it.accenture.jnais.ws.redefines.WmfzIdTitCont;

/**Original name: WMFZ-DATI<br>
 * Variable: WMFZ-DATI from copybook LCCVMFZ1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WmfzDati {

    //==== PROPERTIES ====
    //Original name: WMFZ-ID-MOVI-FINRIO
    private int wmfzIdMoviFinrio = DefaultValues.INT_VAL;
    //Original name: WMFZ-ID-ADES
    private WmfzIdAdes wmfzIdAdes = new WmfzIdAdes();
    //Original name: WMFZ-ID-LIQ
    private WmfzIdLiq wmfzIdLiq = new WmfzIdLiq();
    //Original name: WMFZ-ID-TIT-CONT
    private WmfzIdTitCont wmfzIdTitCont = new WmfzIdTitCont();
    //Original name: WMFZ-ID-MOVI-CRZ
    private int wmfzIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WMFZ-ID-MOVI-CHIU
    private WmfzIdMoviChiu wmfzIdMoviChiu = new WmfzIdMoviChiu();
    //Original name: WMFZ-DT-INI-EFF
    private int wmfzDtIniEff = DefaultValues.INT_VAL;
    //Original name: WMFZ-DT-END-EFF
    private int wmfzDtEndEff = DefaultValues.INT_VAL;
    //Original name: WMFZ-COD-COMP-ANIA
    private int wmfzCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WMFZ-TP-MOVI-FINRIO
    private String wmfzTpMoviFinrio = DefaultValues.stringVal(Len.WMFZ_TP_MOVI_FINRIO);
    //Original name: WMFZ-DT-EFF-MOVI-FINRIO
    private WmfzDtEffMoviFinrio wmfzDtEffMoviFinrio = new WmfzDtEffMoviFinrio();
    //Original name: WMFZ-DT-ELAB
    private WmfzDtElab wmfzDtElab = new WmfzDtElab();
    //Original name: WMFZ-DT-RICH-MOVI
    private WmfzDtRichMovi wmfzDtRichMovi = new WmfzDtRichMovi();
    //Original name: WMFZ-COS-OPRZ
    private WmfzCosOprz wmfzCosOprz = new WmfzCosOprz();
    //Original name: WMFZ-STAT-MOVI
    private String wmfzStatMovi = DefaultValues.stringVal(Len.WMFZ_STAT_MOVI);
    //Original name: WMFZ-DS-RIGA
    private long wmfzDsRiga = DefaultValues.LONG_VAL;
    //Original name: WMFZ-DS-OPER-SQL
    private char wmfzDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WMFZ-DS-VER
    private int wmfzDsVer = DefaultValues.INT_VAL;
    //Original name: WMFZ-DS-TS-INI-CPTZ
    private long wmfzDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WMFZ-DS-TS-END-CPTZ
    private long wmfzDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WMFZ-DS-UTENTE
    private String wmfzDsUtente = DefaultValues.stringVal(Len.WMFZ_DS_UTENTE);
    //Original name: WMFZ-DS-STATO-ELAB
    private char wmfzDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WMFZ-TP-CAUS-RETTIFICA
    private String wmfzTpCausRettifica = DefaultValues.stringVal(Len.WMFZ_TP_CAUS_RETTIFICA);

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wmfzIdMoviFinrio = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WMFZ_ID_MOVI_FINRIO, 0);
        position += Len.WMFZ_ID_MOVI_FINRIO;
        wmfzIdAdes.setWmfzIdAdesFromBuffer(buffer, position);
        position += WmfzIdAdes.Len.WMFZ_ID_ADES;
        wmfzIdLiq.setWmfzIdLiqFromBuffer(buffer, position);
        position += WmfzIdLiq.Len.WMFZ_ID_LIQ;
        wmfzIdTitCont.setWmfzIdTitContFromBuffer(buffer, position);
        position += WmfzIdTitCont.Len.WMFZ_ID_TIT_CONT;
        wmfzIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WMFZ_ID_MOVI_CRZ, 0);
        position += Len.WMFZ_ID_MOVI_CRZ;
        wmfzIdMoviChiu.setWmfzIdMoviChiuFromBuffer(buffer, position);
        position += WmfzIdMoviChiu.Len.WMFZ_ID_MOVI_CHIU;
        wmfzDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WMFZ_DT_INI_EFF, 0);
        position += Len.WMFZ_DT_INI_EFF;
        wmfzDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WMFZ_DT_END_EFF, 0);
        position += Len.WMFZ_DT_END_EFF;
        wmfzCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WMFZ_COD_COMP_ANIA, 0);
        position += Len.WMFZ_COD_COMP_ANIA;
        wmfzTpMoviFinrio = MarshalByte.readString(buffer, position, Len.WMFZ_TP_MOVI_FINRIO);
        position += Len.WMFZ_TP_MOVI_FINRIO;
        wmfzDtEffMoviFinrio.setWmfzDtEffMoviFinrioFromBuffer(buffer, position);
        position += WmfzDtEffMoviFinrio.Len.WMFZ_DT_EFF_MOVI_FINRIO;
        wmfzDtElab.setWmfzDtElabFromBuffer(buffer, position);
        position += WmfzDtElab.Len.WMFZ_DT_ELAB;
        wmfzDtRichMovi.setWmfzDtRichMoviFromBuffer(buffer, position);
        position += WmfzDtRichMovi.Len.WMFZ_DT_RICH_MOVI;
        wmfzCosOprz.setWmfzCosOprzFromBuffer(buffer, position);
        position += WmfzCosOprz.Len.WMFZ_COS_OPRZ;
        wmfzStatMovi = MarshalByte.readString(buffer, position, Len.WMFZ_STAT_MOVI);
        position += Len.WMFZ_STAT_MOVI;
        wmfzDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WMFZ_DS_RIGA, 0);
        position += Len.WMFZ_DS_RIGA;
        wmfzDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wmfzDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WMFZ_DS_VER, 0);
        position += Len.WMFZ_DS_VER;
        wmfzDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WMFZ_DS_TS_INI_CPTZ, 0);
        position += Len.WMFZ_DS_TS_INI_CPTZ;
        wmfzDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WMFZ_DS_TS_END_CPTZ, 0);
        position += Len.WMFZ_DS_TS_END_CPTZ;
        wmfzDsUtente = MarshalByte.readString(buffer, position, Len.WMFZ_DS_UTENTE);
        position += Len.WMFZ_DS_UTENTE;
        wmfzDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wmfzTpCausRettifica = MarshalByte.readString(buffer, position, Len.WMFZ_TP_CAUS_RETTIFICA);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wmfzIdMoviFinrio, Len.Int.WMFZ_ID_MOVI_FINRIO, 0);
        position += Len.WMFZ_ID_MOVI_FINRIO;
        wmfzIdAdes.getWmfzIdAdesAsBuffer(buffer, position);
        position += WmfzIdAdes.Len.WMFZ_ID_ADES;
        wmfzIdLiq.getWmfzIdLiqAsBuffer(buffer, position);
        position += WmfzIdLiq.Len.WMFZ_ID_LIQ;
        wmfzIdTitCont.getWmfzIdTitContAsBuffer(buffer, position);
        position += WmfzIdTitCont.Len.WMFZ_ID_TIT_CONT;
        MarshalByte.writeIntAsPacked(buffer, position, wmfzIdMoviCrz, Len.Int.WMFZ_ID_MOVI_CRZ, 0);
        position += Len.WMFZ_ID_MOVI_CRZ;
        wmfzIdMoviChiu.getWmfzIdMoviChiuAsBuffer(buffer, position);
        position += WmfzIdMoviChiu.Len.WMFZ_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wmfzDtIniEff, Len.Int.WMFZ_DT_INI_EFF, 0);
        position += Len.WMFZ_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wmfzDtEndEff, Len.Int.WMFZ_DT_END_EFF, 0);
        position += Len.WMFZ_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wmfzCodCompAnia, Len.Int.WMFZ_COD_COMP_ANIA, 0);
        position += Len.WMFZ_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wmfzTpMoviFinrio, Len.WMFZ_TP_MOVI_FINRIO);
        position += Len.WMFZ_TP_MOVI_FINRIO;
        wmfzDtEffMoviFinrio.getWmfzDtEffMoviFinrioAsBuffer(buffer, position);
        position += WmfzDtEffMoviFinrio.Len.WMFZ_DT_EFF_MOVI_FINRIO;
        wmfzDtElab.getWmfzDtElabAsBuffer(buffer, position);
        position += WmfzDtElab.Len.WMFZ_DT_ELAB;
        wmfzDtRichMovi.getWmfzDtRichMoviAsBuffer(buffer, position);
        position += WmfzDtRichMovi.Len.WMFZ_DT_RICH_MOVI;
        wmfzCosOprz.getWmfzCosOprzAsBuffer(buffer, position);
        position += WmfzCosOprz.Len.WMFZ_COS_OPRZ;
        MarshalByte.writeString(buffer, position, wmfzStatMovi, Len.WMFZ_STAT_MOVI);
        position += Len.WMFZ_STAT_MOVI;
        MarshalByte.writeLongAsPacked(buffer, position, wmfzDsRiga, Len.Int.WMFZ_DS_RIGA, 0);
        position += Len.WMFZ_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wmfzDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wmfzDsVer, Len.Int.WMFZ_DS_VER, 0);
        position += Len.WMFZ_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wmfzDsTsIniCptz, Len.Int.WMFZ_DS_TS_INI_CPTZ, 0);
        position += Len.WMFZ_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wmfzDsTsEndCptz, Len.Int.WMFZ_DS_TS_END_CPTZ, 0);
        position += Len.WMFZ_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wmfzDsUtente, Len.WMFZ_DS_UTENTE);
        position += Len.WMFZ_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wmfzDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wmfzTpCausRettifica, Len.WMFZ_TP_CAUS_RETTIFICA);
        return buffer;
    }

    public void initDatiSpaces() {
        wmfzIdMoviFinrio = Types.INVALID_INT_VAL;
        wmfzIdAdes.initWmfzIdAdesSpaces();
        wmfzIdLiq.initWmfzIdLiqSpaces();
        wmfzIdTitCont.initWmfzIdTitContSpaces();
        wmfzIdMoviCrz = Types.INVALID_INT_VAL;
        wmfzIdMoviChiu.initWmfzIdMoviChiuSpaces();
        wmfzDtIniEff = Types.INVALID_INT_VAL;
        wmfzDtEndEff = Types.INVALID_INT_VAL;
        wmfzCodCompAnia = Types.INVALID_INT_VAL;
        wmfzTpMoviFinrio = "";
        wmfzDtEffMoviFinrio.initWmfzDtEffMoviFinrioSpaces();
        wmfzDtElab.initWmfzDtElabSpaces();
        wmfzDtRichMovi.initWmfzDtRichMoviSpaces();
        wmfzCosOprz.initWmfzCosOprzSpaces();
        wmfzStatMovi = "";
        wmfzDsRiga = Types.INVALID_LONG_VAL;
        wmfzDsOperSql = Types.SPACE_CHAR;
        wmfzDsVer = Types.INVALID_INT_VAL;
        wmfzDsTsIniCptz = Types.INVALID_LONG_VAL;
        wmfzDsTsEndCptz = Types.INVALID_LONG_VAL;
        wmfzDsUtente = "";
        wmfzDsStatoElab = Types.SPACE_CHAR;
        wmfzTpCausRettifica = "";
    }

    public void setWmfzIdMoviFinrio(int wmfzIdMoviFinrio) {
        this.wmfzIdMoviFinrio = wmfzIdMoviFinrio;
    }

    public int getWmfzIdMoviFinrio() {
        return this.wmfzIdMoviFinrio;
    }

    public void setWmfzIdMoviCrz(int wmfzIdMoviCrz) {
        this.wmfzIdMoviCrz = wmfzIdMoviCrz;
    }

    public int getWmfzIdMoviCrz() {
        return this.wmfzIdMoviCrz;
    }

    public void setWmfzDtIniEff(int wmfzDtIniEff) {
        this.wmfzDtIniEff = wmfzDtIniEff;
    }

    public int getWmfzDtIniEff() {
        return this.wmfzDtIniEff;
    }

    public void setWmfzDtEndEff(int wmfzDtEndEff) {
        this.wmfzDtEndEff = wmfzDtEndEff;
    }

    public int getWmfzDtEndEff() {
        return this.wmfzDtEndEff;
    }

    public void setWmfzCodCompAnia(int wmfzCodCompAnia) {
        this.wmfzCodCompAnia = wmfzCodCompAnia;
    }

    public int getWmfzCodCompAnia() {
        return this.wmfzCodCompAnia;
    }

    public void setWmfzTpMoviFinrio(String wmfzTpMoviFinrio) {
        this.wmfzTpMoviFinrio = Functions.subString(wmfzTpMoviFinrio, Len.WMFZ_TP_MOVI_FINRIO);
    }

    public String getWmfzTpMoviFinrio() {
        return this.wmfzTpMoviFinrio;
    }

    public void setWmfzStatMovi(String wmfzStatMovi) {
        this.wmfzStatMovi = Functions.subString(wmfzStatMovi, Len.WMFZ_STAT_MOVI);
    }

    public String getWmfzStatMovi() {
        return this.wmfzStatMovi;
    }

    public void setWmfzDsRiga(long wmfzDsRiga) {
        this.wmfzDsRiga = wmfzDsRiga;
    }

    public long getWmfzDsRiga() {
        return this.wmfzDsRiga;
    }

    public void setWmfzDsOperSql(char wmfzDsOperSql) {
        this.wmfzDsOperSql = wmfzDsOperSql;
    }

    public char getWmfzDsOperSql() {
        return this.wmfzDsOperSql;
    }

    public void setWmfzDsVer(int wmfzDsVer) {
        this.wmfzDsVer = wmfzDsVer;
    }

    public int getWmfzDsVer() {
        return this.wmfzDsVer;
    }

    public void setWmfzDsTsIniCptz(long wmfzDsTsIniCptz) {
        this.wmfzDsTsIniCptz = wmfzDsTsIniCptz;
    }

    public long getWmfzDsTsIniCptz() {
        return this.wmfzDsTsIniCptz;
    }

    public void setWmfzDsTsEndCptz(long wmfzDsTsEndCptz) {
        this.wmfzDsTsEndCptz = wmfzDsTsEndCptz;
    }

    public long getWmfzDsTsEndCptz() {
        return this.wmfzDsTsEndCptz;
    }

    public void setWmfzDsUtente(String wmfzDsUtente) {
        this.wmfzDsUtente = Functions.subString(wmfzDsUtente, Len.WMFZ_DS_UTENTE);
    }

    public String getWmfzDsUtente() {
        return this.wmfzDsUtente;
    }

    public void setWmfzDsStatoElab(char wmfzDsStatoElab) {
        this.wmfzDsStatoElab = wmfzDsStatoElab;
    }

    public char getWmfzDsStatoElab() {
        return this.wmfzDsStatoElab;
    }

    public void setWmfzTpCausRettifica(String wmfzTpCausRettifica) {
        this.wmfzTpCausRettifica = Functions.subString(wmfzTpCausRettifica, Len.WMFZ_TP_CAUS_RETTIFICA);
    }

    public String getWmfzTpCausRettifica() {
        return this.wmfzTpCausRettifica;
    }

    public WmfzCosOprz getWmfzCosOprz() {
        return wmfzCosOprz;
    }

    public WmfzDtEffMoviFinrio getWmfzDtEffMoviFinrio() {
        return wmfzDtEffMoviFinrio;
    }

    public WmfzDtElab getWmfzDtElab() {
        return wmfzDtElab;
    }

    public WmfzDtRichMovi getWmfzDtRichMovi() {
        return wmfzDtRichMovi;
    }

    public WmfzIdAdes getWmfzIdAdes() {
        return wmfzIdAdes;
    }

    public WmfzIdLiq getWmfzIdLiq() {
        return wmfzIdLiq;
    }

    public WmfzIdMoviChiu getWmfzIdMoviChiu() {
        return wmfzIdMoviChiu;
    }

    public WmfzIdTitCont getWmfzIdTitCont() {
        return wmfzIdTitCont;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WMFZ_ID_MOVI_FINRIO = 5;
        public static final int WMFZ_ID_MOVI_CRZ = 5;
        public static final int WMFZ_DT_INI_EFF = 5;
        public static final int WMFZ_DT_END_EFF = 5;
        public static final int WMFZ_COD_COMP_ANIA = 3;
        public static final int WMFZ_TP_MOVI_FINRIO = 2;
        public static final int WMFZ_STAT_MOVI = 2;
        public static final int WMFZ_DS_RIGA = 6;
        public static final int WMFZ_DS_OPER_SQL = 1;
        public static final int WMFZ_DS_VER = 5;
        public static final int WMFZ_DS_TS_INI_CPTZ = 10;
        public static final int WMFZ_DS_TS_END_CPTZ = 10;
        public static final int WMFZ_DS_UTENTE = 20;
        public static final int WMFZ_DS_STATO_ELAB = 1;
        public static final int WMFZ_TP_CAUS_RETTIFICA = 2;
        public static final int DATI = WMFZ_ID_MOVI_FINRIO + WmfzIdAdes.Len.WMFZ_ID_ADES + WmfzIdLiq.Len.WMFZ_ID_LIQ + WmfzIdTitCont.Len.WMFZ_ID_TIT_CONT + WMFZ_ID_MOVI_CRZ + WmfzIdMoviChiu.Len.WMFZ_ID_MOVI_CHIU + WMFZ_DT_INI_EFF + WMFZ_DT_END_EFF + WMFZ_COD_COMP_ANIA + WMFZ_TP_MOVI_FINRIO + WmfzDtEffMoviFinrio.Len.WMFZ_DT_EFF_MOVI_FINRIO + WmfzDtElab.Len.WMFZ_DT_ELAB + WmfzDtRichMovi.Len.WMFZ_DT_RICH_MOVI + WmfzCosOprz.Len.WMFZ_COS_OPRZ + WMFZ_STAT_MOVI + WMFZ_DS_RIGA + WMFZ_DS_OPER_SQL + WMFZ_DS_VER + WMFZ_DS_TS_INI_CPTZ + WMFZ_DS_TS_END_CPTZ + WMFZ_DS_UTENTE + WMFZ_DS_STATO_ELAB + WMFZ_TP_CAUS_RETTIFICA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMFZ_ID_MOVI_FINRIO = 9;
            public static final int WMFZ_ID_MOVI_CRZ = 9;
            public static final int WMFZ_DT_INI_EFF = 8;
            public static final int WMFZ_DT_END_EFF = 8;
            public static final int WMFZ_COD_COMP_ANIA = 5;
            public static final int WMFZ_DS_RIGA = 10;
            public static final int WMFZ_DS_VER = 9;
            public static final int WMFZ_DS_TS_INI_CPTZ = 18;
            public static final int WMFZ_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
