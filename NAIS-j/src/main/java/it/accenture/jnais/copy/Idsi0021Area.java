package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Idsi0021IdentitaChiamante;
import it.accenture.jnais.ws.enums.Idsi0021Initialize;
import it.accenture.jnais.ws.enums.Idsi0021ModalitaEsecutiva;
import it.accenture.jnais.ws.enums.Idsi0021Operazione;

/**Original name: IDSI0021-AREA<br>
 * Variable: IDSI0021-AREA from copybook IDSI0021<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idsi0021Area {

    //==== PROPERTIES ====
    //Original name: IDSI0021-TIPO-MOVIMENTO
    private int tipoMovimento = DefaultValues.INT_VAL;
    //Original name: IDSI0021-MODALITA-ESECUTIVA
    private Idsi0021ModalitaEsecutiva modalitaEsecutiva = new Idsi0021ModalitaEsecutiva();
    /**Original name: IDSI0021-OPERAZIONE<br>
	 * <pre> -- CAMPI OPERAZIONE
	 *     COPY IDSV0008 REPLACING ==(DS)== BY ==IDSI0021==.</pre>*/
    private Idsi0021Operazione operazione = new Idsi0021Operazione();
    /**Original name: IDSI0021-IDENTITA-CHIAMANTE<br>
	 * <pre>     fine COPY IDSV0008</pre>*/
    private Idsi0021IdentitaChiamante identitaChiamante = new Idsi0021IdentitaChiamante();
    //Original name: IDSI0021-CODICE-COMPAGNIA-ANIA
    private String codiceCompagniaAnia = DefaultValues.stringVal(Len.CODICE_COMPAGNIA_ANIA);
    //Original name: IDSI0021-CODICE-STR-DATO
    private String codiceStrDato = DefaultValues.stringVal(Len.CODICE_STR_DATO);
    //Original name: IDSI0021-INITIALIZE
    private Idsi0021Initialize initialize = new Idsi0021Initialize();

    //==== METHODS ====
    public void setIdsi0021AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        tipoMovimento = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TIPO_MOVIMENTO, 0);
        position += Len.TIPO_MOVIMENTO;
        modalitaEsecutiva.setModalitaEsecutiva(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        setOperBytes(buffer, position);
        position += Len.OPER;
        identitaChiamante.setIdentitaChiamante(MarshalByte.readString(buffer, position, Idsi0021IdentitaChiamante.Len.IDENTITA_CHIAMANTE));
        position += Idsi0021IdentitaChiamante.Len.IDENTITA_CHIAMANTE;
        codiceCompagniaAnia = MarshalByte.readFixedString(buffer, position, Len.CODICE_COMPAGNIA_ANIA);
        position += Len.CODICE_COMPAGNIA_ANIA;
        codiceStrDato = MarshalByte.readString(buffer, position, Len.CODICE_STR_DATO);
        position += Len.CODICE_STR_DATO;
        initialize.setInitialize(MarshalByte.readChar(buffer, position));
    }

    public byte[] getIdsi0021AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, tipoMovimento, Len.Int.TIPO_MOVIMENTO, 0);
        position += Len.TIPO_MOVIMENTO;
        MarshalByte.writeChar(buffer, position, modalitaEsecutiva.getModalitaEsecutiva());
        position += Types.CHAR_SIZE;
        getOperBytes(buffer, position);
        position += Len.OPER;
        MarshalByte.writeString(buffer, position, identitaChiamante.getIdentitaChiamante(), Idsi0021IdentitaChiamante.Len.IDENTITA_CHIAMANTE);
        position += Idsi0021IdentitaChiamante.Len.IDENTITA_CHIAMANTE;
        MarshalByte.writeString(buffer, position, codiceCompagniaAnia, Len.CODICE_COMPAGNIA_ANIA);
        position += Len.CODICE_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, codiceStrDato, Len.CODICE_STR_DATO);
        position += Len.CODICE_STR_DATO;
        MarshalByte.writeChar(buffer, position, initialize.getInitialize());
        return buffer;
    }

    public void initIdsi0021AreaSpaces() {
        tipoMovimento = Types.INVALID_INT_VAL;
        modalitaEsecutiva.setModalitaEsecutiva(Types.SPACE_CHAR);
        initOperSpaces();
        identitaChiamante.setIdentitaChiamante("");
        codiceCompagniaAnia = "";
        codiceStrDato = "";
        initialize.setInitialize(Types.SPACE_CHAR);
    }

    public void setTipoMovimento(int tipoMovimento) {
        this.tipoMovimento = tipoMovimento;
    }

    public int getTipoMovimento() {
        return this.tipoMovimento;
    }

    public void setOperBytes(byte[] buffer, int offset) {
        int position = offset;
        operazione.setOperazione(MarshalByte.readString(buffer, position, Idsi0021Operazione.Len.OPERAZIONE));
    }

    public byte[] getOperBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, operazione.getOperazione(), Idsi0021Operazione.Len.OPERAZIONE);
        return buffer;
    }

    public void initOperSpaces() {
        operazione.setOperazione("");
    }

    public void setCodiceCompagniaAniaFormatted(String codiceCompagniaAnia) {
        this.codiceCompagniaAnia = Trunc.toUnsignedNumeric(codiceCompagniaAnia, Len.CODICE_COMPAGNIA_ANIA);
    }

    public int getCodiceCompagniaAnia() {
        return NumericDisplay.asInt(this.codiceCompagniaAnia);
    }

    public void setCodiceStrDato(String codiceStrDato) {
        this.codiceStrDato = Functions.subString(codiceStrDato, Len.CODICE_STR_DATO);
    }

    public String getCodiceStrDato() {
        return this.codiceStrDato;
    }

    public Idsi0021IdentitaChiamante getIdentitaChiamante() {
        return identitaChiamante;
    }

    public Idsi0021Initialize getInitialize() {
        return initialize;
    }

    public Idsi0021ModalitaEsecutiva getModalitaEsecutiva() {
        return modalitaEsecutiva;
    }

    public Idsi0021Operazione getOperazione() {
        return operazione;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_COMPAGNIA_ANIA = 5;
        public static final int CODICE_STR_DATO = 30;
        public static final int TIPO_MOVIMENTO = 3;
        public static final int OPER = Idsi0021Operazione.Len.OPERAZIONE;
        public static final int IDSI0021_AREA = TIPO_MOVIMENTO + Idsi0021ModalitaEsecutiva.Len.MODALITA_ESECUTIVA + OPER + Idsi0021IdentitaChiamante.Len.IDENTITA_CHIAMANTE + CODICE_COMPAGNIA_ANIA + CODICE_STR_DATO + Idsi0021Initialize.Len.INITIALIZE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIPO_MOVIMENTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
