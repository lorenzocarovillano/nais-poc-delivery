package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.A25CodPersSecond;
import it.accenture.jnais.ws.redefines.A25Dt1aAtvt;
import it.accenture.jnais.ws.redefines.A25DtAcqsPers;
import it.accenture.jnais.ws.redefines.A25DtBlocCli;
import it.accenture.jnais.ws.redefines.A25DtDeadPers;
import it.accenture.jnais.ws.redefines.A25DtEndVldtPers;
import it.accenture.jnais.ws.redefines.A25DtNascCli;
import it.accenture.jnais.ws.redefines.A25DtSegnalPartner;
import it.accenture.jnais.ws.redefines.A25IdSegmentazCli;
import it.accenture.jnais.ws.redefines.A25TstamAggmRiga;
import it.accenture.jnais.ws.redefines.A25TstamEndVldt;

/**Original name: PERS<br>
 * Variable: PERS from copybook IDBVA251<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class PersLlbs0230 {

    //==== PROPERTIES ====
    //Original name: A25-ID-PERS
    private int a25IdPers = DefaultValues.INT_VAL;
    //Original name: A25-TSTAM-INI-VLDT
    private long a25TstamIniVldt = DefaultValues.LONG_VAL;
    //Original name: A25-TSTAM-END-VLDT
    private A25TstamEndVldt a25TstamEndVldt = new A25TstamEndVldt();
    //Original name: A25-COD-PERS
    private long a25CodPers = DefaultValues.LONG_VAL;
    //Original name: A25-RIFTO-RETE
    private String a25RiftoRete = DefaultValues.stringVal(Len.A25_RIFTO_RETE);
    //Original name: A25-COD-PRT-IVA
    private String a25CodPrtIva = DefaultValues.stringVal(Len.A25_COD_PRT_IVA);
    //Original name: A25-IND-PVCY-PRSNL
    private char a25IndPvcyPrsnl = DefaultValues.CHAR_VAL;
    //Original name: A25-IND-PVCY-CMMRC
    private char a25IndPvcyCmmrc = DefaultValues.CHAR_VAL;
    //Original name: A25-IND-PVCY-INDST
    private char a25IndPvcyIndst = DefaultValues.CHAR_VAL;
    //Original name: A25-DT-NASC-CLI
    private A25DtNascCli a25DtNascCli = new A25DtNascCli();
    //Original name: A25-DT-ACQS-PERS
    private A25DtAcqsPers a25DtAcqsPers = new A25DtAcqsPers();
    //Original name: A25-IND-CLI
    private char a25IndCli = DefaultValues.CHAR_VAL;
    //Original name: A25-COD-CMN
    private String a25CodCmn = DefaultValues.stringVal(Len.A25_COD_CMN);
    //Original name: A25-COD-FRM-GIURD
    private String a25CodFrmGiurd = DefaultValues.stringVal(Len.A25_COD_FRM_GIURD);
    //Original name: A25-COD-ENTE-PUBB
    private String a25CodEntePubb = DefaultValues.stringVal(Len.A25_COD_ENTE_PUBB);
    //Original name: A25-DEN-RGN-SOC-LEN
    private short a25DenRgnSocLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: A25-DEN-RGN-SOC
    private String a25DenRgnSoc = DefaultValues.stringVal(Len.A25_DEN_RGN_SOC);
    //Original name: A25-DEN-SIG-RGN-SOC-LEN
    private short a25DenSigRgnSocLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: A25-DEN-SIG-RGN-SOC
    private String a25DenSigRgnSoc = DefaultValues.stringVal(Len.A25_DEN_SIG_RGN_SOC);
    //Original name: A25-IND-ESE-FISC
    private char a25IndEseFisc = DefaultValues.CHAR_VAL;
    //Original name: A25-COD-STAT-CVL
    private String a25CodStatCvl = DefaultValues.stringVal(Len.A25_COD_STAT_CVL);
    //Original name: A25-DEN-NOME-LEN
    private short a25DenNomeLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: A25-DEN-NOME
    private String a25DenNome = DefaultValues.stringVal(Len.A25_DEN_NOME);
    //Original name: A25-DEN-COGN-LEN
    private short a25DenCognLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: A25-DEN-COGN
    private String a25DenCogn = DefaultValues.stringVal(Len.A25_DEN_COGN);
    //Original name: A25-COD-FISC
    private String a25CodFisc = DefaultValues.stringVal(Len.A25_COD_FISC);
    //Original name: A25-IND-SEX
    private char a25IndSex = DefaultValues.CHAR_VAL;
    //Original name: A25-IND-CPCT-GIURD
    private char a25IndCpctGiurd = DefaultValues.CHAR_VAL;
    //Original name: A25-IND-PORT-HDCP
    private char a25IndPortHdcp = DefaultValues.CHAR_VAL;
    //Original name: A25-COD-USER-INS
    private String a25CodUserIns = DefaultValues.stringVal(Len.A25_COD_USER_INS);
    //Original name: A25-TSTAM-INS-RIGA
    private long a25TstamInsRiga = DefaultValues.LONG_VAL;
    //Original name: A25-COD-USER-AGGM
    private String a25CodUserAggm = DefaultValues.stringVal(Len.A25_COD_USER_AGGM);
    //Original name: A25-TSTAM-AGGM-RIGA
    private A25TstamAggmRiga a25TstamAggmRiga = new A25TstamAggmRiga();
    //Original name: A25-DEN-CMN-NASC-STRN-LEN
    private short a25DenCmnNascStrnLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: A25-DEN-CMN-NASC-STRN
    private String a25DenCmnNascStrn = DefaultValues.stringVal(Len.A25_DEN_CMN_NASC_STRN);
    //Original name: A25-COD-RAMO-STGR
    private String a25CodRamoStgr = DefaultValues.stringVal(Len.A25_COD_RAMO_STGR);
    //Original name: A25-COD-STGR-ATVT-UIC
    private String a25CodStgrAtvtUic = DefaultValues.stringVal(Len.A25_COD_STGR_ATVT_UIC);
    //Original name: A25-COD-RAMO-ATVT-UIC
    private String a25CodRamoAtvtUic = DefaultValues.stringVal(Len.A25_COD_RAMO_ATVT_UIC);
    //Original name: A25-DT-END-VLDT-PERS
    private A25DtEndVldtPers a25DtEndVldtPers = new A25DtEndVldtPers();
    //Original name: A25-DT-DEAD-PERS
    private A25DtDeadPers a25DtDeadPers = new A25DtDeadPers();
    //Original name: A25-TP-STAT-CLI
    private String a25TpStatCli = DefaultValues.stringVal(Len.A25_TP_STAT_CLI);
    //Original name: A25-DT-BLOC-CLI
    private A25DtBlocCli a25DtBlocCli = new A25DtBlocCli();
    //Original name: A25-COD-PERS-SECOND
    private A25CodPersSecond a25CodPersSecond = new A25CodPersSecond();
    //Original name: A25-ID-SEGMENTAZ-CLI
    private A25IdSegmentazCli a25IdSegmentazCli = new A25IdSegmentazCli();
    //Original name: A25-DT-1A-ATVT
    private A25Dt1aAtvt a25Dt1aAtvt = new A25Dt1aAtvt();
    //Original name: A25-DT-SEGNAL-PARTNER
    private A25DtSegnalPartner a25DtSegnalPartner = new A25DtSegnalPartner();

    //==== METHODS ====
    public void setPersFormatted(String data) {
        byte[] buffer = new byte[Len.PERS];
        MarshalByte.writeString(buffer, 1, data, Len.PERS);
        setPersBytes(buffer, 1);
    }

    public void setPersBytes(byte[] buffer, int offset) {
        int position = offset;
        a25IdPers = MarshalByte.readPackedAsInt(buffer, position, Len.Int.A25_ID_PERS, 0);
        position += Len.A25_ID_PERS;
        a25TstamIniVldt = MarshalByte.readPackedAsLong(buffer, position, Len.Int.A25_TSTAM_INI_VLDT, 0);
        position += Len.A25_TSTAM_INI_VLDT;
        a25TstamEndVldt.setA25TstamEndVldtFromBuffer(buffer, position);
        position += A25TstamEndVldt.Len.A25_TSTAM_END_VLDT;
        a25CodPers = MarshalByte.readPackedAsLong(buffer, position, Len.Int.A25_COD_PERS, 0);
        position += Len.A25_COD_PERS;
        a25RiftoRete = MarshalByte.readString(buffer, position, Len.A25_RIFTO_RETE);
        position += Len.A25_RIFTO_RETE;
        a25CodPrtIva = MarshalByte.readString(buffer, position, Len.A25_COD_PRT_IVA);
        position += Len.A25_COD_PRT_IVA;
        a25IndPvcyPrsnl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25IndPvcyCmmrc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25IndPvcyIndst = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25DtNascCli.setA25DtNascCliFromBuffer(buffer, position);
        position += A25DtNascCli.Len.A25_DT_NASC_CLI;
        a25DtAcqsPers.setA25DtAcqsPersFromBuffer(buffer, position);
        position += A25DtAcqsPers.Len.A25_DT_ACQS_PERS;
        a25IndCli = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25CodCmn = MarshalByte.readString(buffer, position, Len.A25_COD_CMN);
        position += Len.A25_COD_CMN;
        a25CodFrmGiurd = MarshalByte.readString(buffer, position, Len.A25_COD_FRM_GIURD);
        position += Len.A25_COD_FRM_GIURD;
        a25CodEntePubb = MarshalByte.readString(buffer, position, Len.A25_COD_ENTE_PUBB);
        position += Len.A25_COD_ENTE_PUBB;
        setA25DenRgnSocVcharBytes(buffer, position);
        position += Len.A25_DEN_RGN_SOC_VCHAR;
        setA25DenSigRgnSocVcharBytes(buffer, position);
        position += Len.A25_DEN_SIG_RGN_SOC_VCHAR;
        a25IndEseFisc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25CodStatCvl = MarshalByte.readString(buffer, position, Len.A25_COD_STAT_CVL);
        position += Len.A25_COD_STAT_CVL;
        setA25DenNomeVcharBytes(buffer, position);
        position += Len.A25_DEN_NOME_VCHAR;
        setA25DenCognVcharBytes(buffer, position);
        position += Len.A25_DEN_COGN_VCHAR;
        a25CodFisc = MarshalByte.readString(buffer, position, Len.A25_COD_FISC);
        position += Len.A25_COD_FISC;
        a25IndSex = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25IndCpctGiurd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25IndPortHdcp = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25CodUserIns = MarshalByte.readString(buffer, position, Len.A25_COD_USER_INS);
        position += Len.A25_COD_USER_INS;
        a25TstamInsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.A25_TSTAM_INS_RIGA, 0);
        position += Len.A25_TSTAM_INS_RIGA;
        a25CodUserAggm = MarshalByte.readString(buffer, position, Len.A25_COD_USER_AGGM);
        position += Len.A25_COD_USER_AGGM;
        a25TstamAggmRiga.setA25TstamAggmRigaFromBuffer(buffer, position);
        position += A25TstamAggmRiga.Len.A25_TSTAM_AGGM_RIGA;
        setA25DenCmnNascStrnVcharBytes(buffer, position);
        position += Len.A25_DEN_CMN_NASC_STRN_VCHAR;
        a25CodRamoStgr = MarshalByte.readString(buffer, position, Len.A25_COD_RAMO_STGR);
        position += Len.A25_COD_RAMO_STGR;
        a25CodStgrAtvtUic = MarshalByte.readString(buffer, position, Len.A25_COD_STGR_ATVT_UIC);
        position += Len.A25_COD_STGR_ATVT_UIC;
        a25CodRamoAtvtUic = MarshalByte.readString(buffer, position, Len.A25_COD_RAMO_ATVT_UIC);
        position += Len.A25_COD_RAMO_ATVT_UIC;
        a25DtEndVldtPers.setA25DtEndVldtPersFromBuffer(buffer, position);
        position += A25DtEndVldtPers.Len.A25_DT_END_VLDT_PERS;
        a25DtDeadPers.setA25DtDeadPersFromBuffer(buffer, position);
        position += A25DtDeadPers.Len.A25_DT_DEAD_PERS;
        a25TpStatCli = MarshalByte.readString(buffer, position, Len.A25_TP_STAT_CLI);
        position += Len.A25_TP_STAT_CLI;
        a25DtBlocCli.setA25DtBlocCliFromBuffer(buffer, position);
        position += A25DtBlocCli.Len.A25_DT_BLOC_CLI;
        a25CodPersSecond.setA25CodPersSecondFromBuffer(buffer, position);
        position += A25CodPersSecond.Len.A25_COD_PERS_SECOND;
        a25IdSegmentazCli.setA25IdSegmentazCliFromBuffer(buffer, position);
        position += A25IdSegmentazCli.Len.A25_ID_SEGMENTAZ_CLI;
        a25Dt1aAtvt.setA25Dt1aAtvtFromBuffer(buffer, position);
        position += A25Dt1aAtvt.Len.A25_DT1A_ATVT;
        a25DtSegnalPartner.setA25DtSegnalPartnerFromBuffer(buffer, position);
    }

    public void setA25IdPers(int a25IdPers) {
        this.a25IdPers = a25IdPers;
    }

    public int getA25IdPers() {
        return this.a25IdPers;
    }

    public void setA25TstamIniVldt(long a25TstamIniVldt) {
        this.a25TstamIniVldt = a25TstamIniVldt;
    }

    public long getA25TstamIniVldt() {
        return this.a25TstamIniVldt;
    }

    public void setA25CodPers(long a25CodPers) {
        this.a25CodPers = a25CodPers;
    }

    public long getA25CodPers() {
        return this.a25CodPers;
    }

    public void setA25RiftoRete(String a25RiftoRete) {
        this.a25RiftoRete = Functions.subString(a25RiftoRete, Len.A25_RIFTO_RETE);
    }

    public String getA25RiftoRete() {
        return this.a25RiftoRete;
    }

    public void setA25CodPrtIva(String a25CodPrtIva) {
        this.a25CodPrtIva = Functions.subString(a25CodPrtIva, Len.A25_COD_PRT_IVA);
    }

    public String getA25CodPrtIva() {
        return this.a25CodPrtIva;
    }

    public String getA25CodPrtIvaFormatted() {
        return Functions.padBlanks(getA25CodPrtIva(), Len.A25_COD_PRT_IVA);
    }

    public void setA25IndPvcyPrsnl(char a25IndPvcyPrsnl) {
        this.a25IndPvcyPrsnl = a25IndPvcyPrsnl;
    }

    public char getA25IndPvcyPrsnl() {
        return this.a25IndPvcyPrsnl;
    }

    public void setA25IndPvcyCmmrc(char a25IndPvcyCmmrc) {
        this.a25IndPvcyCmmrc = a25IndPvcyCmmrc;
    }

    public char getA25IndPvcyCmmrc() {
        return this.a25IndPvcyCmmrc;
    }

    public void setA25IndPvcyIndst(char a25IndPvcyIndst) {
        this.a25IndPvcyIndst = a25IndPvcyIndst;
    }

    public char getA25IndPvcyIndst() {
        return this.a25IndPvcyIndst;
    }

    public void setA25IndCli(char a25IndCli) {
        this.a25IndCli = a25IndCli;
    }

    public char getA25IndCli() {
        return this.a25IndCli;
    }

    public void setA25CodCmn(String a25CodCmn) {
        this.a25CodCmn = Functions.subString(a25CodCmn, Len.A25_COD_CMN);
    }

    public String getA25CodCmn() {
        return this.a25CodCmn;
    }

    public void setA25CodFrmGiurd(String a25CodFrmGiurd) {
        this.a25CodFrmGiurd = Functions.subString(a25CodFrmGiurd, Len.A25_COD_FRM_GIURD);
    }

    public String getA25CodFrmGiurd() {
        return this.a25CodFrmGiurd;
    }

    public void setA25CodEntePubb(String a25CodEntePubb) {
        this.a25CodEntePubb = Functions.subString(a25CodEntePubb, Len.A25_COD_ENTE_PUBB);
    }

    public String getA25CodEntePubb() {
        return this.a25CodEntePubb;
    }

    public void setA25DenRgnSocVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        a25DenRgnSocLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        a25DenRgnSoc = MarshalByte.readString(buffer, position, Len.A25_DEN_RGN_SOC);
    }

    public void setA25DenRgnSocLen(short a25DenRgnSocLen) {
        this.a25DenRgnSocLen = a25DenRgnSocLen;
    }

    public short getA25DenRgnSocLen() {
        return this.a25DenRgnSocLen;
    }

    public void setA25DenRgnSoc(String a25DenRgnSoc) {
        this.a25DenRgnSoc = Functions.subString(a25DenRgnSoc, Len.A25_DEN_RGN_SOC);
    }

    public String getA25DenRgnSoc() {
        return this.a25DenRgnSoc;
    }

    public void setA25DenSigRgnSocVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        a25DenSigRgnSocLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        a25DenSigRgnSoc = MarshalByte.readString(buffer, position, Len.A25_DEN_SIG_RGN_SOC);
    }

    public void setA25DenSigRgnSocLen(short a25DenSigRgnSocLen) {
        this.a25DenSigRgnSocLen = a25DenSigRgnSocLen;
    }

    public short getA25DenSigRgnSocLen() {
        return this.a25DenSigRgnSocLen;
    }

    public void setA25DenSigRgnSoc(String a25DenSigRgnSoc) {
        this.a25DenSigRgnSoc = Functions.subString(a25DenSigRgnSoc, Len.A25_DEN_SIG_RGN_SOC);
    }

    public String getA25DenSigRgnSoc() {
        return this.a25DenSigRgnSoc;
    }

    public void setA25IndEseFisc(char a25IndEseFisc) {
        this.a25IndEseFisc = a25IndEseFisc;
    }

    public char getA25IndEseFisc() {
        return this.a25IndEseFisc;
    }

    public void setA25CodStatCvl(String a25CodStatCvl) {
        this.a25CodStatCvl = Functions.subString(a25CodStatCvl, Len.A25_COD_STAT_CVL);
    }

    public String getA25CodStatCvl() {
        return this.a25CodStatCvl;
    }

    public void setA25DenNomeVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        a25DenNomeLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        a25DenNome = MarshalByte.readString(buffer, position, Len.A25_DEN_NOME);
    }

    public void setA25DenNomeLen(short a25DenNomeLen) {
        this.a25DenNomeLen = a25DenNomeLen;
    }

    public short getA25DenNomeLen() {
        return this.a25DenNomeLen;
    }

    public void setA25DenNome(String a25DenNome) {
        this.a25DenNome = Functions.subString(a25DenNome, Len.A25_DEN_NOME);
    }

    public String getA25DenNome() {
        return this.a25DenNome;
    }

    public void setA25DenCognVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        a25DenCognLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        a25DenCogn = MarshalByte.readString(buffer, position, Len.A25_DEN_COGN);
    }

    public void setA25DenCognLen(short a25DenCognLen) {
        this.a25DenCognLen = a25DenCognLen;
    }

    public short getA25DenCognLen() {
        return this.a25DenCognLen;
    }

    public void setA25DenCogn(String a25DenCogn) {
        this.a25DenCogn = Functions.subString(a25DenCogn, Len.A25_DEN_COGN);
    }

    public String getA25DenCogn() {
        return this.a25DenCogn;
    }

    public void setA25CodFisc(String a25CodFisc) {
        this.a25CodFisc = Functions.subString(a25CodFisc, Len.A25_COD_FISC);
    }

    public String getA25CodFisc() {
        return this.a25CodFisc;
    }

    public String getA25CodFiscFormatted() {
        return Functions.padBlanks(getA25CodFisc(), Len.A25_COD_FISC);
    }

    public void setA25IndSex(char a25IndSex) {
        this.a25IndSex = a25IndSex;
    }

    public char getA25IndSex() {
        return this.a25IndSex;
    }

    public void setA25IndCpctGiurd(char a25IndCpctGiurd) {
        this.a25IndCpctGiurd = a25IndCpctGiurd;
    }

    public char getA25IndCpctGiurd() {
        return this.a25IndCpctGiurd;
    }

    public void setA25IndPortHdcp(char a25IndPortHdcp) {
        this.a25IndPortHdcp = a25IndPortHdcp;
    }

    public char getA25IndPortHdcp() {
        return this.a25IndPortHdcp;
    }

    public void setA25CodUserIns(String a25CodUserIns) {
        this.a25CodUserIns = Functions.subString(a25CodUserIns, Len.A25_COD_USER_INS);
    }

    public String getA25CodUserIns() {
        return this.a25CodUserIns;
    }

    public void setA25TstamInsRiga(long a25TstamInsRiga) {
        this.a25TstamInsRiga = a25TstamInsRiga;
    }

    public long getA25TstamInsRiga() {
        return this.a25TstamInsRiga;
    }

    public void setA25CodUserAggm(String a25CodUserAggm) {
        this.a25CodUserAggm = Functions.subString(a25CodUserAggm, Len.A25_COD_USER_AGGM);
    }

    public String getA25CodUserAggm() {
        return this.a25CodUserAggm;
    }

    public void setA25DenCmnNascStrnVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        a25DenCmnNascStrnLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        a25DenCmnNascStrn = MarshalByte.readString(buffer, position, Len.A25_DEN_CMN_NASC_STRN);
    }

    public void setA25DenCmnNascStrnLen(short a25DenCmnNascStrnLen) {
        this.a25DenCmnNascStrnLen = a25DenCmnNascStrnLen;
    }

    public short getA25DenCmnNascStrnLen() {
        return this.a25DenCmnNascStrnLen;
    }

    public void setA25DenCmnNascStrn(String a25DenCmnNascStrn) {
        this.a25DenCmnNascStrn = Functions.subString(a25DenCmnNascStrn, Len.A25_DEN_CMN_NASC_STRN);
    }

    public String getA25DenCmnNascStrn() {
        return this.a25DenCmnNascStrn;
    }

    public void setA25CodRamoStgr(String a25CodRamoStgr) {
        this.a25CodRamoStgr = Functions.subString(a25CodRamoStgr, Len.A25_COD_RAMO_STGR);
    }

    public String getA25CodRamoStgr() {
        return this.a25CodRamoStgr;
    }

    public void setA25CodStgrAtvtUic(String a25CodStgrAtvtUic) {
        this.a25CodStgrAtvtUic = Functions.subString(a25CodStgrAtvtUic, Len.A25_COD_STGR_ATVT_UIC);
    }

    public String getA25CodStgrAtvtUic() {
        return this.a25CodStgrAtvtUic;
    }

    public void setA25CodRamoAtvtUic(String a25CodRamoAtvtUic) {
        this.a25CodRamoAtvtUic = Functions.subString(a25CodRamoAtvtUic, Len.A25_COD_RAMO_ATVT_UIC);
    }

    public String getA25CodRamoAtvtUic() {
        return this.a25CodRamoAtvtUic;
    }

    public void setA25TpStatCli(String a25TpStatCli) {
        this.a25TpStatCli = Functions.subString(a25TpStatCli, Len.A25_TP_STAT_CLI);
    }

    public String getA25TpStatCli() {
        return this.a25TpStatCli;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_RIFTO_RETE = 20;
        public static final int A25_COD_PRT_IVA = 11;
        public static final int A25_COD_CMN = 4;
        public static final int A25_COD_FRM_GIURD = 4;
        public static final int A25_COD_ENTE_PUBB = 4;
        public static final int A25_DEN_RGN_SOC = 100;
        public static final int A25_DEN_SIG_RGN_SOC = 100;
        public static final int A25_COD_STAT_CVL = 4;
        public static final int A25_DEN_NOME = 100;
        public static final int A25_DEN_COGN = 100;
        public static final int A25_COD_FISC = 16;
        public static final int A25_COD_USER_INS = 20;
        public static final int A25_COD_USER_AGGM = 20;
        public static final int A25_DEN_CMN_NASC_STRN = 100;
        public static final int A25_COD_RAMO_STGR = 4;
        public static final int A25_COD_STGR_ATVT_UIC = 4;
        public static final int A25_COD_RAMO_ATVT_UIC = 4;
        public static final int A25_TP_STAT_CLI = 2;
        public static final int A25_ID_PERS = 5;
        public static final int A25_TSTAM_INI_VLDT = 10;
        public static final int A25_COD_PERS = 6;
        public static final int A25_IND_PVCY_PRSNL = 1;
        public static final int A25_IND_PVCY_CMMRC = 1;
        public static final int A25_IND_PVCY_INDST = 1;
        public static final int A25_IND_CLI = 1;
        public static final int A25_DEN_RGN_SOC_LEN = 2;
        public static final int A25_DEN_RGN_SOC_VCHAR = A25_DEN_RGN_SOC_LEN + A25_DEN_RGN_SOC;
        public static final int A25_DEN_SIG_RGN_SOC_LEN = 2;
        public static final int A25_DEN_SIG_RGN_SOC_VCHAR = A25_DEN_SIG_RGN_SOC_LEN + A25_DEN_SIG_RGN_SOC;
        public static final int A25_IND_ESE_FISC = 1;
        public static final int A25_DEN_NOME_LEN = 2;
        public static final int A25_DEN_NOME_VCHAR = A25_DEN_NOME_LEN + A25_DEN_NOME;
        public static final int A25_DEN_COGN_LEN = 2;
        public static final int A25_DEN_COGN_VCHAR = A25_DEN_COGN_LEN + A25_DEN_COGN;
        public static final int A25_IND_SEX = 1;
        public static final int A25_IND_CPCT_GIURD = 1;
        public static final int A25_IND_PORT_HDCP = 1;
        public static final int A25_TSTAM_INS_RIGA = 10;
        public static final int A25_DEN_CMN_NASC_STRN_LEN = 2;
        public static final int A25_DEN_CMN_NASC_STRN_VCHAR = A25_DEN_CMN_NASC_STRN_LEN + A25_DEN_CMN_NASC_STRN;
        public static final int PERS = A25_ID_PERS + A25_TSTAM_INI_VLDT + A25TstamEndVldt.Len.A25_TSTAM_END_VLDT + A25_COD_PERS + A25_RIFTO_RETE + A25_COD_PRT_IVA + A25_IND_PVCY_PRSNL + A25_IND_PVCY_CMMRC + A25_IND_PVCY_INDST + A25DtNascCli.Len.A25_DT_NASC_CLI + A25DtAcqsPers.Len.A25_DT_ACQS_PERS + A25_IND_CLI + A25_COD_CMN + A25_COD_FRM_GIURD + A25_COD_ENTE_PUBB + A25_DEN_RGN_SOC_VCHAR + A25_DEN_SIG_RGN_SOC_VCHAR + A25_IND_ESE_FISC + A25_COD_STAT_CVL + A25_DEN_NOME_VCHAR + A25_DEN_COGN_VCHAR + A25_COD_FISC + A25_IND_SEX + A25_IND_CPCT_GIURD + A25_IND_PORT_HDCP + A25_COD_USER_INS + A25_TSTAM_INS_RIGA + A25_COD_USER_AGGM + A25TstamAggmRiga.Len.A25_TSTAM_AGGM_RIGA + A25_DEN_CMN_NASC_STRN_VCHAR + A25_COD_RAMO_STGR + A25_COD_STGR_ATVT_UIC + A25_COD_RAMO_ATVT_UIC + A25DtEndVldtPers.Len.A25_DT_END_VLDT_PERS + A25DtDeadPers.Len.A25_DT_DEAD_PERS + A25_TP_STAT_CLI + A25DtBlocCli.Len.A25_DT_BLOC_CLI + A25CodPersSecond.Len.A25_COD_PERS_SECOND + A25IdSegmentazCli.Len.A25_ID_SEGMENTAZ_CLI + A25Dt1aAtvt.Len.A25_DT1A_ATVT + A25DtSegnalPartner.Len.A25_DT_SEGNAL_PARTNER;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_ID_PERS = 9;
            public static final int A25_TSTAM_INI_VLDT = 18;
            public static final int A25_COD_PERS = 11;
            public static final int A25_TSTAM_INS_RIGA = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
