package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBV3401-DATI-INPUT<br>
 * Variable: LDBV3401-DATI-INPUT from copybook LDBV3401<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ldbv3401DatiInput {

    //==== PROPERTIES ====
    //Original name: LDBV3401-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LDBV3401-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LDBV3401-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV3401-TP-STAT-BUS-1
    private String tpStatBus1 = DefaultValues.stringVal(Len.TP_STAT_BUS1);
    //Original name: LDBV3401-TP-STAT-BUS-2
    private String tpStatBus2 = DefaultValues.stringVal(Len.TP_STAT_BUS2);
    //Original name: LDBV3401-RAMO1
    private String ramo1 = DefaultValues.stringVal(Len.RAMO1);
    //Original name: LDBV3401-RAMO2
    private String ramo2 = DefaultValues.stringVal(Len.RAMO2);
    //Original name: LDBV3401-RAMO3
    private String ramo3 = DefaultValues.stringVal(Len.RAMO3);
    //Original name: LDBV3401-RAMO4
    private String ramo4 = DefaultValues.stringVal(Len.RAMO4);
    //Original name: LDBV3401-RAMO5
    private String ramo5 = DefaultValues.stringVal(Len.RAMO5);
    //Original name: LDBV3401-RAMO6
    private String ramo6 = DefaultValues.stringVal(Len.RAMO6);

    //==== METHODS ====
    public void setLdbv3401DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpStatBus1 = MarshalByte.readString(buffer, position, Len.TP_STAT_BUS1);
        position += Len.TP_STAT_BUS1;
        tpStatBus2 = MarshalByte.readString(buffer, position, Len.TP_STAT_BUS2);
        position += Len.TP_STAT_BUS2;
        ramo1 = MarshalByte.readString(buffer, position, Len.RAMO1);
        position += Len.RAMO1;
        ramo2 = MarshalByte.readString(buffer, position, Len.RAMO2);
        position += Len.RAMO2;
        ramo3 = MarshalByte.readString(buffer, position, Len.RAMO3);
        position += Len.RAMO3;
        ramo4 = MarshalByte.readString(buffer, position, Len.RAMO4);
        position += Len.RAMO4;
        ramo5 = MarshalByte.readString(buffer, position, Len.RAMO5);
        position += Len.RAMO5;
        ramo6 = MarshalByte.readString(buffer, position, Len.RAMO6);
    }

    public byte[] getLdbv3401DatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tpStatBus1, Len.TP_STAT_BUS1);
        position += Len.TP_STAT_BUS1;
        MarshalByte.writeString(buffer, position, tpStatBus2, Len.TP_STAT_BUS2);
        position += Len.TP_STAT_BUS2;
        MarshalByte.writeString(buffer, position, ramo1, Len.RAMO1);
        position += Len.RAMO1;
        MarshalByte.writeString(buffer, position, ramo2, Len.RAMO2);
        position += Len.RAMO2;
        MarshalByte.writeString(buffer, position, ramo3, Len.RAMO3);
        position += Len.RAMO3;
        MarshalByte.writeString(buffer, position, ramo4, Len.RAMO4);
        position += Len.RAMO4;
        MarshalByte.writeString(buffer, position, ramo5, Len.RAMO5);
        position += Len.RAMO5;
        MarshalByte.writeString(buffer, position, ramo6, Len.RAMO6);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpStatBus1(String tpStatBus1) {
        this.tpStatBus1 = Functions.subString(tpStatBus1, Len.TP_STAT_BUS1);
    }

    public String getTpStatBus1() {
        return this.tpStatBus1;
    }

    public void setTpStatBus2(String tpStatBus2) {
        this.tpStatBus2 = Functions.subString(tpStatBus2, Len.TP_STAT_BUS2);
    }

    public String getTpStatBus2() {
        return this.tpStatBus2;
    }

    public void setRamo1(String ramo1) {
        this.ramo1 = Functions.subString(ramo1, Len.RAMO1);
    }

    public String getRamo1() {
        return this.ramo1;
    }

    public void setRamo2(String ramo2) {
        this.ramo2 = Functions.subString(ramo2, Len.RAMO2);
    }

    public String getRamo2() {
        return this.ramo2;
    }

    public void setRamo3(String ramo3) {
        this.ramo3 = Functions.subString(ramo3, Len.RAMO3);
    }

    public String getRamo3() {
        return this.ramo3;
    }

    public void setRamo4(String ramo4) {
        this.ramo4 = Functions.subString(ramo4, Len.RAMO4);
    }

    public String getRamo4() {
        return this.ramo4;
    }

    public void setRamo5(String ramo5) {
        this.ramo5 = Functions.subString(ramo5, Len.RAMO5);
    }

    public String getRamo5() {
        return this.ramo5;
    }

    public void setRamo6(String ramo6) {
        this.ramo6 = Functions.subString(ramo6, Len.RAMO6);
    }

    public String getRamo6() {
        return this.ramo6;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POLI = 5;
        public static final int ID_ADES = 5;
        public static final int TP_OGG = 2;
        public static final int TP_STAT_BUS1 = 2;
        public static final int TP_STAT_BUS2 = 2;
        public static final int RAMO1 = 2;
        public static final int RAMO2 = 2;
        public static final int RAMO3 = 2;
        public static final int RAMO4 = 2;
        public static final int RAMO5 = 2;
        public static final int RAMO6 = 2;
        public static final int LDBV3401_DATI_INPUT = ID_POLI + ID_ADES + TP_OGG + TP_STAT_BUS1 + TP_STAT_BUS2 + RAMO1 + RAMO2 + RAMO3 + RAMO4 + RAMO5 + RAMO6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
