package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IDSV0003-CAMPI-ESITO<br>
 * Variable: IDSV0003-CAMPI-ESITO from copybook IDSV0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Idsv0003CampiEsito {

    //==== PROPERTIES ====
    //Original name: IDSV0003-DESCRIZ-ERR-DB2
    private String descrizErrDb2 = DefaultValues.stringVal(Len.DESCRIZ_ERR_DB2);
    //Original name: IDSV0003-COD-SERVIZIO-BE
    private String codServizioBe = DefaultValues.stringVal(Len.COD_SERVIZIO_BE);
    //Original name: IDSV0003-NOME-TABELLA
    private String nomeTabella = DefaultValues.stringVal(Len.NOME_TABELLA);
    //Original name: IDSV0003-KEY-TABELLA
    private String keyTabella = DefaultValues.stringVal(Len.KEY_TABELLA);
    //Original name: IDSV0003-NUM-RIGHE-LETTE
    private String numRigheLette = DefaultValues.stringVal(Len.NUM_RIGHE_LETTE);

    //==== METHODS ====
    public void setCampiEsitoBytes(byte[] buffer) {
        setCampiEsitoBytes(buffer, 1);
    }

    public byte[] getCampiEsitoBytes() {
        byte[] buffer = new byte[Len.CAMPI_ESITO];
        return getCampiEsitoBytes(buffer, 1);
    }

    public void setCampiEsitoBytes(byte[] buffer, int offset) {
        int position = offset;
        descrizErrDb2 = MarshalByte.readString(buffer, position, Len.DESCRIZ_ERR_DB2);
        position += Len.DESCRIZ_ERR_DB2;
        codServizioBe = MarshalByte.readString(buffer, position, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        nomeTabella = MarshalByte.readString(buffer, position, Len.NOME_TABELLA);
        position += Len.NOME_TABELLA;
        keyTabella = MarshalByte.readString(buffer, position, Len.KEY_TABELLA);
        position += Len.KEY_TABELLA;
        numRigheLette = MarshalByte.readFixedString(buffer, position, Len.NUM_RIGHE_LETTE);
    }

    public byte[] getCampiEsitoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, descrizErrDb2, Len.DESCRIZ_ERR_DB2);
        position += Len.DESCRIZ_ERR_DB2;
        MarshalByte.writeString(buffer, position, codServizioBe, Len.COD_SERVIZIO_BE);
        position += Len.COD_SERVIZIO_BE;
        MarshalByte.writeString(buffer, position, nomeTabella, Len.NOME_TABELLA);
        position += Len.NOME_TABELLA;
        MarshalByte.writeString(buffer, position, keyTabella, Len.KEY_TABELLA);
        position += Len.KEY_TABELLA;
        MarshalByte.writeString(buffer, position, numRigheLette, Len.NUM_RIGHE_LETTE);
        return buffer;
    }

    public void setDescrizErrDb2(String descrizErrDb2) {
        this.descrizErrDb2 = Functions.subString(descrizErrDb2, Len.DESCRIZ_ERR_DB2);
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public String getDescrizErrDb2Formatted() {
        return Functions.padBlanks(getDescrizErrDb2(), Len.DESCRIZ_ERR_DB2);
    }

    public void setCodServizioBe(String codServizioBe) {
        this.codServizioBe = Functions.subString(codServizioBe, Len.COD_SERVIZIO_BE);
    }

    public String getCodServizioBe() {
        return this.codServizioBe;
    }

    public void setNomeTabella(String nomeTabella) {
        this.nomeTabella = Functions.subString(nomeTabella, Len.NOME_TABELLA);
    }

    public String getNomeTabella() {
        return this.nomeTabella;
    }

    public String getNomeTabellaFormatted() {
        return Functions.padBlanks(getNomeTabella(), Len.NOME_TABELLA);
    }

    public void setKeyTabella(String keyTabella) {
        this.keyTabella = Functions.subString(keyTabella, Len.KEY_TABELLA);
    }

    public String getKeyTabella() {
        return this.keyTabella;
    }

    public void setNumRigheLette(short numRigheLette) {
        this.numRigheLette = NumericDisplay.asString(numRigheLette, Len.NUM_RIGHE_LETTE);
    }

    public void setIdsv0003NumRigheLetteFormatted(String idsv0003NumRigheLette) {
        this.numRigheLette = Trunc.toUnsignedNumeric(idsv0003NumRigheLette, Len.NUM_RIGHE_LETTE);
    }

    public short getNumRigheLette() {
        return NumericDisplay.asShort(this.numRigheLette);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESCRIZ_ERR_DB2 = 300;
        public static final int COD_SERVIZIO_BE = 8;
        public static final int NOME_TABELLA = 18;
        public static final int KEY_TABELLA = 20;
        public static final int NUM_RIGHE_LETTE = 2;
        public static final int CAMPI_ESITO = DESCRIZ_ERR_DB2 + COD_SERVIZIO_BE + NOME_TABELLA + KEY_TABELLA + NUM_RIGHE_LETTE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
