package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-ANAG-DATO<br>
 * Variable: IND-ANAG-DATO from copybook IDBVADA2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndAnagDato {

    //==== PROPERTIES ====
    //Original name: IND-ADA-DESC-DATO
    private short descDato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADA-TIPO-DATO
    private short tipoDato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADA-LUNGHEZZA-DATO
    private short lunghezzaDato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADA-PRECISIONE-DATO
    private short precisioneDato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADA-COD-DOMINIO
    private short codDominio = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-ADA-FORMATTAZIONE-DATO
    private short formattazioneDato = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setFlContiguousData(short flContiguousData) {
        this.descDato = flContiguousData;
    }

    public short getDescDato() {
        return this.descDato;
    }

    public void setTipoDato(short tipoDato) {
        this.tipoDato = tipoDato;
    }

    public short getTipoDato() {
        return this.tipoDato;
    }

    public void setLunghezzaDato(short lunghezzaDato) {
        this.lunghezzaDato = lunghezzaDato;
    }

    public short getLunghezzaDato() {
        return this.lunghezzaDato;
    }

    public void setPrecisioneDato(short precisioneDato) {
        this.precisioneDato = precisioneDato;
    }

    public short getPrecisioneDato() {
        return this.precisioneDato;
    }

    public void setTypeRecord(short typeRecord) {
        this.codDominio = typeRecord;
    }

    public short getCodDominio() {
        return this.codDominio;
    }

    public void setFormattazioneDato(short formattazioneDato) {
        this.formattazioneDato = formattazioneDato;
    }

    public short getFormattazioneDato() {
        return this.formattazioneDato;
    }
}
