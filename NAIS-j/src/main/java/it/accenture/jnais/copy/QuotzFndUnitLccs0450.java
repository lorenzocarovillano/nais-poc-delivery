package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.L19DtRilevazioneNav;
import it.accenture.jnais.ws.redefines.L19ValQuo;
import it.accenture.jnais.ws.redefines.L19ValQuoAcq;
import it.accenture.jnais.ws.redefines.L19ValQuoManfee;

/**Original name: QUOTZ-FND-UNIT<br>
 * Variable: QUOTZ-FND-UNIT from copybook IDBVL191<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class QuotzFndUnitLccs0450 {

    //==== PROPERTIES ====
    //Original name: L19-COD-COMP-ANIA
    private int l19CodCompAnia = DefaultValues.INT_VAL;
    //Original name: L19-COD-FND
    private String l19CodFnd = DefaultValues.stringVal(Len.L19_COD_FND);
    //Original name: L19-DT-QTZ
    private int l19DtQtz = DefaultValues.INT_VAL;
    //Original name: L19-VAL-QUO
    private L19ValQuo l19ValQuo = new L19ValQuo();
    //Original name: L19-VAL-QUO-MANFEE
    private L19ValQuoManfee l19ValQuoManfee = new L19ValQuoManfee();
    //Original name: L19-DS-OPER-SQL
    private char l19DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: L19-DS-VER
    private int l19DsVer = DefaultValues.INT_VAL;
    //Original name: L19-DS-TS-CPTZ
    private long l19DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: L19-DS-UTENTE
    private String l19DsUtente = DefaultValues.stringVal(Len.L19_DS_UTENTE);
    //Original name: L19-DS-STATO-ELAB
    private char l19DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: L19-TP-FND
    private char l19TpFnd = DefaultValues.CHAR_VAL;
    //Original name: L19-DT-RILEVAZIONE-NAV
    private L19DtRilevazioneNav l19DtRilevazioneNav = new L19DtRilevazioneNav();
    //Original name: L19-VAL-QUO-ACQ
    private L19ValQuoAcq l19ValQuoAcq = new L19ValQuoAcq();
    //Original name: L19-FL-NO-NAV
    private char l19FlNoNav = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setQuotzFndUnitFormatted(String data) {
        byte[] buffer = new byte[Len.QUOTZ_FND_UNIT];
        MarshalByte.writeString(buffer, 1, data, Len.QUOTZ_FND_UNIT);
        setQuotzFndUnitBytes(buffer, 1);
    }

    public String getQuotzFndUnitFormatted() {
        return MarshalByteExt.bufferToStr(getQuotzFndUnitBytes());
    }

    public byte[] getQuotzFndUnitBytes() {
        byte[] buffer = new byte[Len.QUOTZ_FND_UNIT];
        return getQuotzFndUnitBytes(buffer, 1);
    }

    public void setQuotzFndUnitBytes(byte[] buffer, int offset) {
        int position = offset;
        l19CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L19_COD_COMP_ANIA, 0);
        position += Len.L19_COD_COMP_ANIA;
        l19CodFnd = MarshalByte.readString(buffer, position, Len.L19_COD_FND);
        position += Len.L19_COD_FND;
        l19DtQtz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L19_DT_QTZ, 0);
        position += Len.L19_DT_QTZ;
        l19ValQuo.setL19ValQuoFromBuffer(buffer, position);
        position += L19ValQuo.Len.L19_VAL_QUO;
        l19ValQuoManfee.setL19ValQuoManfeeFromBuffer(buffer, position);
        position += L19ValQuoManfee.Len.L19_VAL_QUO_MANFEE;
        l19DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l19DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L19_DS_VER, 0);
        position += Len.L19_DS_VER;
        l19DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L19_DS_TS_CPTZ, 0);
        position += Len.L19_DS_TS_CPTZ;
        l19DsUtente = MarshalByte.readString(buffer, position, Len.L19_DS_UTENTE);
        position += Len.L19_DS_UTENTE;
        l19DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l19TpFnd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l19DtRilevazioneNav.setL19DtRilevazioneNavFromBuffer(buffer, position);
        position += L19DtRilevazioneNav.Len.L19_DT_RILEVAZIONE_NAV;
        l19ValQuoAcq.setL19ValQuoAcqFromBuffer(buffer, position);
        position += L19ValQuoAcq.Len.L19_VAL_QUO_ACQ;
        l19FlNoNav = MarshalByte.readChar(buffer, position);
    }

    public byte[] getQuotzFndUnitBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, l19CodCompAnia, Len.Int.L19_COD_COMP_ANIA, 0);
        position += Len.L19_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, l19CodFnd, Len.L19_COD_FND);
        position += Len.L19_COD_FND;
        MarshalByte.writeIntAsPacked(buffer, position, l19DtQtz, Len.Int.L19_DT_QTZ, 0);
        position += Len.L19_DT_QTZ;
        l19ValQuo.getL19ValQuoAsBuffer(buffer, position);
        position += L19ValQuo.Len.L19_VAL_QUO;
        l19ValQuoManfee.getL19ValQuoManfeeAsBuffer(buffer, position);
        position += L19ValQuoManfee.Len.L19_VAL_QUO_MANFEE;
        MarshalByte.writeChar(buffer, position, l19DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, l19DsVer, Len.Int.L19_DS_VER, 0);
        position += Len.L19_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, l19DsTsCptz, Len.Int.L19_DS_TS_CPTZ, 0);
        position += Len.L19_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, l19DsUtente, Len.L19_DS_UTENTE);
        position += Len.L19_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, l19DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, l19TpFnd);
        position += Types.CHAR_SIZE;
        l19DtRilevazioneNav.getL19DtRilevazioneNavAsBuffer(buffer, position);
        position += L19DtRilevazioneNav.Len.L19_DT_RILEVAZIONE_NAV;
        l19ValQuoAcq.getL19ValQuoAcqAsBuffer(buffer, position);
        position += L19ValQuoAcq.Len.L19_VAL_QUO_ACQ;
        MarshalByte.writeChar(buffer, position, l19FlNoNav);
        return buffer;
    }

    public void setL19CodCompAnia(int l19CodCompAnia) {
        this.l19CodCompAnia = l19CodCompAnia;
    }

    public int getL19CodCompAnia() {
        return this.l19CodCompAnia;
    }

    public void setL19CodFnd(String l19CodFnd) {
        this.l19CodFnd = Functions.subString(l19CodFnd, Len.L19_COD_FND);
    }

    public String getL19CodFnd() {
        return this.l19CodFnd;
    }

    public void setL19DtQtz(int l19DtQtz) {
        this.l19DtQtz = l19DtQtz;
    }

    public int getL19DtQtz() {
        return this.l19DtQtz;
    }

    public void setL19DsOperSql(char l19DsOperSql) {
        this.l19DsOperSql = l19DsOperSql;
    }

    public char getL19DsOperSql() {
        return this.l19DsOperSql;
    }

    public void setL19DsVer(int l19DsVer) {
        this.l19DsVer = l19DsVer;
    }

    public int getL19DsVer() {
        return this.l19DsVer;
    }

    public void setL19DsTsCptz(long l19DsTsCptz) {
        this.l19DsTsCptz = l19DsTsCptz;
    }

    public long getL19DsTsCptz() {
        return this.l19DsTsCptz;
    }

    public void setL19DsUtente(String l19DsUtente) {
        this.l19DsUtente = Functions.subString(l19DsUtente, Len.L19_DS_UTENTE);
    }

    public String getL19DsUtente() {
        return this.l19DsUtente;
    }

    public void setL19DsStatoElab(char l19DsStatoElab) {
        this.l19DsStatoElab = l19DsStatoElab;
    }

    public char getL19DsStatoElab() {
        return this.l19DsStatoElab;
    }

    public void setL19TpFnd(char l19TpFnd) {
        this.l19TpFnd = l19TpFnd;
    }

    public char getL19TpFnd() {
        return this.l19TpFnd;
    }

    public void setL19FlNoNav(char l19FlNoNav) {
        this.l19FlNoNav = l19FlNoNav;
    }

    public char getL19FlNoNav() {
        return this.l19FlNoNav;
    }

    public L19DtRilevazioneNav getL19DtRilevazioneNav() {
        return l19DtRilevazioneNav;
    }

    public L19ValQuo getL19ValQuo() {
        return l19ValQuo;
    }

    public L19ValQuoAcq getL19ValQuoAcq() {
        return l19ValQuoAcq;
    }

    public L19ValQuoManfee getL19ValQuoManfee() {
        return l19ValQuoManfee;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L19_COD_FND = 12;
        public static final int L19_DS_UTENTE = 20;
        public static final int L19_COD_COMP_ANIA = 3;
        public static final int L19_DT_QTZ = 5;
        public static final int L19_DS_OPER_SQL = 1;
        public static final int L19_DS_VER = 5;
        public static final int L19_DS_TS_CPTZ = 10;
        public static final int L19_DS_STATO_ELAB = 1;
        public static final int L19_TP_FND = 1;
        public static final int L19_FL_NO_NAV = 1;
        public static final int QUOTZ_FND_UNIT = L19_COD_COMP_ANIA + L19_COD_FND + L19_DT_QTZ + L19ValQuo.Len.L19_VAL_QUO + L19ValQuoManfee.Len.L19_VAL_QUO_MANFEE + L19_DS_OPER_SQL + L19_DS_VER + L19_DS_TS_CPTZ + L19_DS_UTENTE + L19_DS_STATO_ELAB + L19_TP_FND + L19DtRilevazioneNav.Len.L19_DT_RILEVAZIONE_NAV + L19ValQuoAcq.Len.L19_VAL_QUO_ACQ + L19_FL_NO_NAV;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L19_COD_COMP_ANIA = 5;
            public static final int L19_DT_QTZ = 8;
            public static final int L19_DS_VER = 9;
            public static final int L19_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
