package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WsdiDtFis;
import it.accenture.jnais.ws.redefines.WsdiFrqValut;
import it.accenture.jnais.ws.redefines.WsdiIdMoviChiu;
import it.accenture.jnais.ws.redefines.WsdiModGest;
import it.accenture.jnais.ws.redefines.WsdiPcProtezione;

/**Original name: WSDI-DATI<br>
 * Variable: WSDI-DATI from copybook LCCVSDI1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WsdiDati {

    //==== PROPERTIES ====
    //Original name: WSDI-ID-STRA-DI-INVST
    private int wsdiIdStraDiInvst = DefaultValues.INT_VAL;
    //Original name: WSDI-ID-OGG
    private int wsdiIdOgg = DefaultValues.INT_VAL;
    //Original name: WSDI-TP-OGG
    private String wsdiTpOgg = DefaultValues.stringVal(Len.WSDI_TP_OGG);
    //Original name: WSDI-ID-MOVI-CRZ
    private int wsdiIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WSDI-ID-MOVI-CHIU
    private WsdiIdMoviChiu wsdiIdMoviChiu = new WsdiIdMoviChiu();
    //Original name: WSDI-DT-INI-EFF
    private int wsdiDtIniEff = DefaultValues.INT_VAL;
    //Original name: WSDI-DT-END-EFF
    private int wsdiDtEndEff = DefaultValues.INT_VAL;
    //Original name: WSDI-COD-COMP-ANIA
    private int wsdiCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WSDI-COD-STRA
    private String wsdiCodStra = DefaultValues.stringVal(Len.WSDI_COD_STRA);
    //Original name: WSDI-MOD-GEST
    private WsdiModGest wsdiModGest = new WsdiModGest();
    //Original name: WSDI-VAR-RIFTO
    private String wsdiVarRifto = DefaultValues.stringVal(Len.WSDI_VAR_RIFTO);
    //Original name: WSDI-VAL-VAR-RIFTO
    private String wsdiValVarRifto = DefaultValues.stringVal(Len.WSDI_VAL_VAR_RIFTO);
    //Original name: WSDI-DT-FIS
    private WsdiDtFis wsdiDtFis = new WsdiDtFis();
    //Original name: WSDI-FRQ-VALUT
    private WsdiFrqValut wsdiFrqValut = new WsdiFrqValut();
    //Original name: WSDI-FL-INI-END-PER
    private char wsdiFlIniEndPer = DefaultValues.CHAR_VAL;
    //Original name: WSDI-DS-RIGA
    private long wsdiDsRiga = DefaultValues.LONG_VAL;
    //Original name: WSDI-DS-OPER-SQL
    private char wsdiDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WSDI-DS-VER
    private int wsdiDsVer = DefaultValues.INT_VAL;
    //Original name: WSDI-DS-TS-INI-CPTZ
    private long wsdiDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WSDI-DS-TS-END-CPTZ
    private long wsdiDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WSDI-DS-UTENTE
    private String wsdiDsUtente = DefaultValues.stringVal(Len.WSDI_DS_UTENTE);
    //Original name: WSDI-DS-STATO-ELAB
    private char wsdiDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WSDI-TP-STRA
    private String wsdiTpStra = DefaultValues.stringVal(Len.WSDI_TP_STRA);
    //Original name: WSDI-TP-PROVZA-STRA
    private String wsdiTpProvzaStra = DefaultValues.stringVal(Len.WSDI_TP_PROVZA_STRA);
    //Original name: WSDI-PC-PROTEZIONE
    private WsdiPcProtezione wsdiPcProtezione = new WsdiPcProtezione();

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wsdiIdStraDiInvst = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSDI_ID_STRA_DI_INVST, 0);
        position += Len.WSDI_ID_STRA_DI_INVST;
        wsdiIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSDI_ID_OGG, 0);
        position += Len.WSDI_ID_OGG;
        wsdiTpOgg = MarshalByte.readString(buffer, position, Len.WSDI_TP_OGG);
        position += Len.WSDI_TP_OGG;
        wsdiIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSDI_ID_MOVI_CRZ, 0);
        position += Len.WSDI_ID_MOVI_CRZ;
        wsdiIdMoviChiu.setWsdiIdMoviChiuFromBuffer(buffer, position);
        position += WsdiIdMoviChiu.Len.WSDI_ID_MOVI_CHIU;
        wsdiDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSDI_DT_INI_EFF, 0);
        position += Len.WSDI_DT_INI_EFF;
        wsdiDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSDI_DT_END_EFF, 0);
        position += Len.WSDI_DT_END_EFF;
        wsdiCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSDI_COD_COMP_ANIA, 0);
        position += Len.WSDI_COD_COMP_ANIA;
        wsdiCodStra = MarshalByte.readString(buffer, position, Len.WSDI_COD_STRA);
        position += Len.WSDI_COD_STRA;
        wsdiModGest.setWsdiModGestFromBuffer(buffer, position);
        position += WsdiModGest.Len.WSDI_MOD_GEST;
        wsdiVarRifto = MarshalByte.readString(buffer, position, Len.WSDI_VAR_RIFTO);
        position += Len.WSDI_VAR_RIFTO;
        wsdiValVarRifto = MarshalByte.readString(buffer, position, Len.WSDI_VAL_VAR_RIFTO);
        position += Len.WSDI_VAL_VAR_RIFTO;
        wsdiDtFis.setWsdiDtFisFromBuffer(buffer, position);
        position += WsdiDtFis.Len.WSDI_DT_FIS;
        wsdiFrqValut.setWsdiFrqValutFromBuffer(buffer, position);
        position += WsdiFrqValut.Len.WSDI_FRQ_VALUT;
        wsdiFlIniEndPer = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wsdiDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WSDI_DS_RIGA, 0);
        position += Len.WSDI_DS_RIGA;
        wsdiDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wsdiDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WSDI_DS_VER, 0);
        position += Len.WSDI_DS_VER;
        wsdiDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WSDI_DS_TS_INI_CPTZ, 0);
        position += Len.WSDI_DS_TS_INI_CPTZ;
        wsdiDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WSDI_DS_TS_END_CPTZ, 0);
        position += Len.WSDI_DS_TS_END_CPTZ;
        wsdiDsUtente = MarshalByte.readString(buffer, position, Len.WSDI_DS_UTENTE);
        position += Len.WSDI_DS_UTENTE;
        wsdiDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wsdiTpStra = MarshalByte.readString(buffer, position, Len.WSDI_TP_STRA);
        position += Len.WSDI_TP_STRA;
        wsdiTpProvzaStra = MarshalByte.readString(buffer, position, Len.WSDI_TP_PROVZA_STRA);
        position += Len.WSDI_TP_PROVZA_STRA;
        wsdiPcProtezione.setWsdiPcProtezioneFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wsdiIdStraDiInvst, Len.Int.WSDI_ID_STRA_DI_INVST, 0);
        position += Len.WSDI_ID_STRA_DI_INVST;
        MarshalByte.writeIntAsPacked(buffer, position, wsdiIdOgg, Len.Int.WSDI_ID_OGG, 0);
        position += Len.WSDI_ID_OGG;
        MarshalByte.writeString(buffer, position, wsdiTpOgg, Len.WSDI_TP_OGG);
        position += Len.WSDI_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, wsdiIdMoviCrz, Len.Int.WSDI_ID_MOVI_CRZ, 0);
        position += Len.WSDI_ID_MOVI_CRZ;
        wsdiIdMoviChiu.getWsdiIdMoviChiuAsBuffer(buffer, position);
        position += WsdiIdMoviChiu.Len.WSDI_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wsdiDtIniEff, Len.Int.WSDI_DT_INI_EFF, 0);
        position += Len.WSDI_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wsdiDtEndEff, Len.Int.WSDI_DT_END_EFF, 0);
        position += Len.WSDI_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wsdiCodCompAnia, Len.Int.WSDI_COD_COMP_ANIA, 0);
        position += Len.WSDI_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wsdiCodStra, Len.WSDI_COD_STRA);
        position += Len.WSDI_COD_STRA;
        wsdiModGest.getWsdiModGestAsBuffer(buffer, position);
        position += WsdiModGest.Len.WSDI_MOD_GEST;
        MarshalByte.writeString(buffer, position, wsdiVarRifto, Len.WSDI_VAR_RIFTO);
        position += Len.WSDI_VAR_RIFTO;
        MarshalByte.writeString(buffer, position, wsdiValVarRifto, Len.WSDI_VAL_VAR_RIFTO);
        position += Len.WSDI_VAL_VAR_RIFTO;
        wsdiDtFis.getWsdiDtFisAsBuffer(buffer, position);
        position += WsdiDtFis.Len.WSDI_DT_FIS;
        wsdiFrqValut.getWsdiFrqValutAsBuffer(buffer, position);
        position += WsdiFrqValut.Len.WSDI_FRQ_VALUT;
        MarshalByte.writeChar(buffer, position, wsdiFlIniEndPer);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, wsdiDsRiga, Len.Int.WSDI_DS_RIGA, 0);
        position += Len.WSDI_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wsdiDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wsdiDsVer, Len.Int.WSDI_DS_VER, 0);
        position += Len.WSDI_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wsdiDsTsIniCptz, Len.Int.WSDI_DS_TS_INI_CPTZ, 0);
        position += Len.WSDI_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wsdiDsTsEndCptz, Len.Int.WSDI_DS_TS_END_CPTZ, 0);
        position += Len.WSDI_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wsdiDsUtente, Len.WSDI_DS_UTENTE);
        position += Len.WSDI_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wsdiDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wsdiTpStra, Len.WSDI_TP_STRA);
        position += Len.WSDI_TP_STRA;
        MarshalByte.writeString(buffer, position, wsdiTpProvzaStra, Len.WSDI_TP_PROVZA_STRA);
        position += Len.WSDI_TP_PROVZA_STRA;
        wsdiPcProtezione.getWsdiPcProtezioneAsBuffer(buffer, position);
        return buffer;
    }

    public void initDatiSpaces() {
        wsdiIdStraDiInvst = Types.INVALID_INT_VAL;
        wsdiIdOgg = Types.INVALID_INT_VAL;
        wsdiTpOgg = "";
        wsdiIdMoviCrz = Types.INVALID_INT_VAL;
        wsdiIdMoviChiu.initWsdiIdMoviChiuSpaces();
        wsdiDtIniEff = Types.INVALID_INT_VAL;
        wsdiDtEndEff = Types.INVALID_INT_VAL;
        wsdiCodCompAnia = Types.INVALID_INT_VAL;
        wsdiCodStra = "";
        wsdiModGest.initWsdiModGestSpaces();
        wsdiVarRifto = "";
        wsdiValVarRifto = "";
        wsdiDtFis.initWsdiDtFisSpaces();
        wsdiFrqValut.initWsdiFrqValutSpaces();
        wsdiFlIniEndPer = Types.SPACE_CHAR;
        wsdiDsRiga = Types.INVALID_LONG_VAL;
        wsdiDsOperSql = Types.SPACE_CHAR;
        wsdiDsVer = Types.INVALID_INT_VAL;
        wsdiDsTsIniCptz = Types.INVALID_LONG_VAL;
        wsdiDsTsEndCptz = Types.INVALID_LONG_VAL;
        wsdiDsUtente = "";
        wsdiDsStatoElab = Types.SPACE_CHAR;
        wsdiTpStra = "";
        wsdiTpProvzaStra = "";
        wsdiPcProtezione.initWsdiPcProtezioneSpaces();
    }

    public void setWsdiIdStraDiInvst(int wsdiIdStraDiInvst) {
        this.wsdiIdStraDiInvst = wsdiIdStraDiInvst;
    }

    public int getWsdiIdStraDiInvst() {
        return this.wsdiIdStraDiInvst;
    }

    public void setWsdiIdOgg(int wsdiIdOgg) {
        this.wsdiIdOgg = wsdiIdOgg;
    }

    public int getWsdiIdOgg() {
        return this.wsdiIdOgg;
    }

    public void setWsdiTpOgg(String wsdiTpOgg) {
        this.wsdiTpOgg = Functions.subString(wsdiTpOgg, Len.WSDI_TP_OGG);
    }

    public String getWsdiTpOgg() {
        return this.wsdiTpOgg;
    }

    public void setWsdiIdMoviCrz(int wsdiIdMoviCrz) {
        this.wsdiIdMoviCrz = wsdiIdMoviCrz;
    }

    public int getWsdiIdMoviCrz() {
        return this.wsdiIdMoviCrz;
    }

    public void setWsdiDtIniEff(int wsdiDtIniEff) {
        this.wsdiDtIniEff = wsdiDtIniEff;
    }

    public int getWsdiDtIniEff() {
        return this.wsdiDtIniEff;
    }

    public void setWsdiDtEndEff(int wsdiDtEndEff) {
        this.wsdiDtEndEff = wsdiDtEndEff;
    }

    public int getWsdiDtEndEff() {
        return this.wsdiDtEndEff;
    }

    public void setWsdiCodCompAnia(int wsdiCodCompAnia) {
        this.wsdiCodCompAnia = wsdiCodCompAnia;
    }

    public int getWsdiCodCompAnia() {
        return this.wsdiCodCompAnia;
    }

    public void setWsdiCodStra(String wsdiCodStra) {
        this.wsdiCodStra = Functions.subString(wsdiCodStra, Len.WSDI_COD_STRA);
    }

    public String getWsdiCodStra() {
        return this.wsdiCodStra;
    }

    public void setWsdiVarRifto(String wsdiVarRifto) {
        this.wsdiVarRifto = Functions.subString(wsdiVarRifto, Len.WSDI_VAR_RIFTO);
    }

    public String getWsdiVarRifto() {
        return this.wsdiVarRifto;
    }

    public void setWsdiValVarRifto(String wsdiValVarRifto) {
        this.wsdiValVarRifto = Functions.subString(wsdiValVarRifto, Len.WSDI_VAL_VAR_RIFTO);
    }

    public String getWsdiValVarRifto() {
        return this.wsdiValVarRifto;
    }

    public void setWsdiFlIniEndPer(char wsdiFlIniEndPer) {
        this.wsdiFlIniEndPer = wsdiFlIniEndPer;
    }

    public char getWsdiFlIniEndPer() {
        return this.wsdiFlIniEndPer;
    }

    public void setWsdiDsRiga(long wsdiDsRiga) {
        this.wsdiDsRiga = wsdiDsRiga;
    }

    public long getWsdiDsRiga() {
        return this.wsdiDsRiga;
    }

    public void setWsdiDsOperSql(char wsdiDsOperSql) {
        this.wsdiDsOperSql = wsdiDsOperSql;
    }

    public char getWsdiDsOperSql() {
        return this.wsdiDsOperSql;
    }

    public void setWsdiDsVer(int wsdiDsVer) {
        this.wsdiDsVer = wsdiDsVer;
    }

    public int getWsdiDsVer() {
        return this.wsdiDsVer;
    }

    public void setWsdiDsTsIniCptz(long wsdiDsTsIniCptz) {
        this.wsdiDsTsIniCptz = wsdiDsTsIniCptz;
    }

    public long getWsdiDsTsIniCptz() {
        return this.wsdiDsTsIniCptz;
    }

    public void setWsdiDsTsEndCptz(long wsdiDsTsEndCptz) {
        this.wsdiDsTsEndCptz = wsdiDsTsEndCptz;
    }

    public long getWsdiDsTsEndCptz() {
        return this.wsdiDsTsEndCptz;
    }

    public void setWsdiDsUtente(String wsdiDsUtente) {
        this.wsdiDsUtente = Functions.subString(wsdiDsUtente, Len.WSDI_DS_UTENTE);
    }

    public String getWsdiDsUtente() {
        return this.wsdiDsUtente;
    }

    public void setWsdiDsStatoElab(char wsdiDsStatoElab) {
        this.wsdiDsStatoElab = wsdiDsStatoElab;
    }

    public char getWsdiDsStatoElab() {
        return this.wsdiDsStatoElab;
    }

    public void setWsdiTpStra(String wsdiTpStra) {
        this.wsdiTpStra = Functions.subString(wsdiTpStra, Len.WSDI_TP_STRA);
    }

    public String getWsdiTpStra() {
        return this.wsdiTpStra;
    }

    public void setWsdiTpProvzaStra(String wsdiTpProvzaStra) {
        this.wsdiTpProvzaStra = Functions.subString(wsdiTpProvzaStra, Len.WSDI_TP_PROVZA_STRA);
    }

    public String getWsdiTpProvzaStra() {
        return this.wsdiTpProvzaStra;
    }

    public WsdiDtFis getWsdiDtFis() {
        return wsdiDtFis;
    }

    public WsdiFrqValut getWsdiFrqValut() {
        return wsdiFrqValut;
    }

    public WsdiIdMoviChiu getWsdiIdMoviChiu() {
        return wsdiIdMoviChiu;
    }

    public WsdiModGest getWsdiModGest() {
        return wsdiModGest;
    }

    public WsdiPcProtezione getWsdiPcProtezione() {
        return wsdiPcProtezione;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WSDI_ID_STRA_DI_INVST = 5;
        public static final int WSDI_ID_OGG = 5;
        public static final int WSDI_TP_OGG = 2;
        public static final int WSDI_ID_MOVI_CRZ = 5;
        public static final int WSDI_DT_INI_EFF = 5;
        public static final int WSDI_DT_END_EFF = 5;
        public static final int WSDI_COD_COMP_ANIA = 3;
        public static final int WSDI_COD_STRA = 30;
        public static final int WSDI_VAR_RIFTO = 12;
        public static final int WSDI_VAL_VAR_RIFTO = 12;
        public static final int WSDI_FL_INI_END_PER = 1;
        public static final int WSDI_DS_RIGA = 6;
        public static final int WSDI_DS_OPER_SQL = 1;
        public static final int WSDI_DS_VER = 5;
        public static final int WSDI_DS_TS_INI_CPTZ = 10;
        public static final int WSDI_DS_TS_END_CPTZ = 10;
        public static final int WSDI_DS_UTENTE = 20;
        public static final int WSDI_DS_STATO_ELAB = 1;
        public static final int WSDI_TP_STRA = 2;
        public static final int WSDI_TP_PROVZA_STRA = 2;
        public static final int DATI = WSDI_ID_STRA_DI_INVST + WSDI_ID_OGG + WSDI_TP_OGG + WSDI_ID_MOVI_CRZ + WsdiIdMoviChiu.Len.WSDI_ID_MOVI_CHIU + WSDI_DT_INI_EFF + WSDI_DT_END_EFF + WSDI_COD_COMP_ANIA + WSDI_COD_STRA + WsdiModGest.Len.WSDI_MOD_GEST + WSDI_VAR_RIFTO + WSDI_VAL_VAR_RIFTO + WsdiDtFis.Len.WSDI_DT_FIS + WsdiFrqValut.Len.WSDI_FRQ_VALUT + WSDI_FL_INI_END_PER + WSDI_DS_RIGA + WSDI_DS_OPER_SQL + WSDI_DS_VER + WSDI_DS_TS_INI_CPTZ + WSDI_DS_TS_END_CPTZ + WSDI_DS_UTENTE + WSDI_DS_STATO_ELAB + WSDI_TP_STRA + WSDI_TP_PROVZA_STRA + WsdiPcProtezione.Len.WSDI_PC_PROTEZIONE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WSDI_ID_STRA_DI_INVST = 9;
            public static final int WSDI_ID_OGG = 9;
            public static final int WSDI_ID_MOVI_CRZ = 9;
            public static final int WSDI_DT_INI_EFF = 8;
            public static final int WSDI_DT_END_EFF = 8;
            public static final int WSDI_COD_COMP_ANIA = 5;
            public static final int WSDI_DS_RIGA = 10;
            public static final int WSDI_DS_VER = 9;
            public static final int WSDI_DS_TS_INI_CPTZ = 18;
            public static final int WSDI_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
