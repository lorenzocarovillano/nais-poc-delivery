package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.WcomDerFlagStFinale;
import it.accenture.jnais.ws.enums.WcomMovFlagStFinale;
import it.accenture.jnais.ws.enums.WcomRicFlagStFinale;

/**Original name: WCOM-STATI<br>
 * Variable: WCOM-STATI from copybook LCCC0001<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WcomStati {

    //==== PROPERTIES ====
    //Original name: WCOM-STATO-MOVIMENTO
    private String statoMovimento = DefaultValues.stringVal(Len.STATO_MOVIMENTO);
    //Original name: WCOM-MOV-FLAG-ST-FINALE
    private WcomMovFlagStFinale movFlagStFinale = new WcomMovFlagStFinale();
    //Original name: WCOM-STATO-OGG-DEROGA
    private String statoOggDeroga = DefaultValues.stringVal(Len.STATO_OGG_DEROGA);
    //Original name: WCOM-DER-FLAG-ST-FINALE
    private WcomDerFlagStFinale derFlagStFinale = new WcomDerFlagStFinale();
    //Original name: WCOM-STATO-RICHIESTA-EST
    private String statoRichiestaEst = DefaultValues.stringVal(Len.STATO_RICHIESTA_EST);
    //Original name: WCOM-RIC-FLAG-ST-FINALE
    private WcomRicFlagStFinale ricFlagStFinale = new WcomRicFlagStFinale();

    //==== METHODS ====
    public void setStatiBytes(byte[] buffer, int offset) {
        int position = offset;
        statoMovimento = MarshalByte.readString(buffer, position, Len.STATO_MOVIMENTO);
        position += Len.STATO_MOVIMENTO;
        movFlagStFinale.setMovFlagStFinale(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        statoOggDeroga = MarshalByte.readString(buffer, position, Len.STATO_OGG_DEROGA);
        position += Len.STATO_OGG_DEROGA;
        derFlagStFinale.setDerFlagStFinale(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        statoRichiestaEst = MarshalByte.readString(buffer, position, Len.STATO_RICHIESTA_EST);
        position += Len.STATO_RICHIESTA_EST;
        ricFlagStFinale.setRicFlagStFinale(MarshalByte.readChar(buffer, position));
    }

    public byte[] getStatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, statoMovimento, Len.STATO_MOVIMENTO);
        position += Len.STATO_MOVIMENTO;
        MarshalByte.writeChar(buffer, position, movFlagStFinale.getMovFlagStFinale());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, statoOggDeroga, Len.STATO_OGG_DEROGA);
        position += Len.STATO_OGG_DEROGA;
        MarshalByte.writeChar(buffer, position, derFlagStFinale.getDerFlagStFinale());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, statoRichiestaEst, Len.STATO_RICHIESTA_EST);
        position += Len.STATO_RICHIESTA_EST;
        MarshalByte.writeChar(buffer, position, ricFlagStFinale.getRicFlagStFinale());
        return buffer;
    }

    public void setStatoMovimento(String statoMovimento) {
        this.statoMovimento = Functions.subString(statoMovimento, Len.STATO_MOVIMENTO);
    }

    public String getStatoMovimento() {
        return this.statoMovimento;
    }

    public void setStatoOggDeroga(String statoOggDeroga) {
        this.statoOggDeroga = Functions.subString(statoOggDeroga, Len.STATO_OGG_DEROGA);
    }

    public String getStatoOggDeroga() {
        return this.statoOggDeroga;
    }

    public void setStatoRichiestaEst(String statoRichiestaEst) {
        this.statoRichiestaEst = Functions.subString(statoRichiestaEst, Len.STATO_RICHIESTA_EST);
    }

    public String getStatoRichiestaEst() {
        return this.statoRichiestaEst;
    }

    public WcomDerFlagStFinale getDerFlagStFinale() {
        return derFlagStFinale;
    }

    public WcomMovFlagStFinale getMovFlagStFinale() {
        return movFlagStFinale;
    }

    public WcomRicFlagStFinale getRicFlagStFinale() {
        return ricFlagStFinale;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int STATO_MOVIMENTO = 2;
        public static final int STATO_OGG_DEROGA = 2;
        public static final int STATO_RICHIESTA_EST = 2;
        public static final int STATI = STATO_MOVIMENTO + WcomMovFlagStFinale.Len.MOV_FLAG_ST_FINALE + STATO_OGG_DEROGA + WcomDerFlagStFinale.Len.DER_FLAG_ST_FINALE + STATO_RICHIESTA_EST + WcomRicFlagStFinale.Len.RIC_FLAG_ST_FINALE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
