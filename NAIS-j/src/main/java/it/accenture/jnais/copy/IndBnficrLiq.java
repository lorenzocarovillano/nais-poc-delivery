package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IND-BNFICR-LIQ<br>
 * Variable: IND-BNFICR-LIQ from copybook IDBVBEL2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndBnficrLiq {

    //==== PROPERTIES ====
    //Original name: IND-BEL-ID-MOVI-CHIU
    private short idMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-ID-RAPP-ANA
    private short idRappAna = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-COD-BNFICR
    private short codBnficr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-DESC-BNFICR
    private short descBnficr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-PC-LIQ
    private short pcLiq = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-ESRCN-ATTVT-IMPRS
    private short esrcnAttvtImprs = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-TP-IND-BNFICR
    private short tpIndBnficr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-FL-ESE
    private short flEse = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-FL-IRREV
    private short flIrrev = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMP-LRD-LIQTO
    private short impLrdLiqto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMPST-IPT
    private short impstIpt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMP-NET-LIQTO
    private short impNetLiqto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-RIT-ACC-IPT
    private short ritAccIpt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-RIT-VIS-IPT
    private short ritVisIpt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-RIT-TFR-IPT
    private short ritTfrIpt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMPST-IRPEF-IPT
    private short impstIrpefIpt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMPST-SOST-IPT
    private short impstSostIpt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMPST-PRVR-IPT
    private short impstPrvrIpt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMPST-252-IPT
    private short impst252Ipt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-ID-ASSTO
    private short idAssto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-TAX-SEP
    private short taxSep = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-DT-RISERVE-SOM-P
    private short dtRiserveSomP = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-DT-VLT
    private short dtVlt = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-TP-STAT-LIQ-BNFICR
    private short tpStatLiqBnficr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-RICH-CALC-CNBT-INP
    private short richCalcCnbtInp = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMP-INTR-RIT-PAG
    private short impIntrRitPag = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-DT-ULT-DOCTO
    private short dtUltDocto = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-DT-DORMIENZA
    private short dtDormienza = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMPST-BOLLO-TOT-V
    private short impstBolloTotV = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMPST-VIS-1382011
    private short impstVis1382011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMPST-SOST-1382011
    private short impstSost1382011 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMPST-VIS-662014
    private short impstVis662014 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-BEL-IMPST-SOST-662014
    private short impstSost662014 = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIdMoviChiu(short idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public short getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setIdRappAna(short idRappAna) {
        this.idRappAna = idRappAna;
    }

    public short getIdRappAna() {
        return this.idRappAna;
    }

    public void setCodBnficr(short codBnficr) {
        this.codBnficr = codBnficr;
    }

    public short getCodBnficr() {
        return this.codBnficr;
    }

    public void setDescBnficr(short descBnficr) {
        this.descBnficr = descBnficr;
    }

    public short getDescBnficr() {
        return this.descBnficr;
    }

    public void setPcLiq(short pcLiq) {
        this.pcLiq = pcLiq;
    }

    public short getPcLiq() {
        return this.pcLiq;
    }

    public void setEsrcnAttvtImprs(short esrcnAttvtImprs) {
        this.esrcnAttvtImprs = esrcnAttvtImprs;
    }

    public short getEsrcnAttvtImprs() {
        return this.esrcnAttvtImprs;
    }

    public void setTpIndBnficr(short tpIndBnficr) {
        this.tpIndBnficr = tpIndBnficr;
    }

    public short getTpIndBnficr() {
        return this.tpIndBnficr;
    }

    public void setFlEse(short flEse) {
        this.flEse = flEse;
    }

    public short getFlEse() {
        return this.flEse;
    }

    public void setFlIrrev(short flIrrev) {
        this.flIrrev = flIrrev;
    }

    public short getFlIrrev() {
        return this.flIrrev;
    }

    public void setImpLrdLiqto(short impLrdLiqto) {
        this.impLrdLiqto = impLrdLiqto;
    }

    public short getImpLrdLiqto() {
        return this.impLrdLiqto;
    }

    public void setImpstIpt(short impstIpt) {
        this.impstIpt = impstIpt;
    }

    public short getImpstIpt() {
        return this.impstIpt;
    }

    public void setImpNetLiqto(short impNetLiqto) {
        this.impNetLiqto = impNetLiqto;
    }

    public short getImpNetLiqto() {
        return this.impNetLiqto;
    }

    public void setRitAccIpt(short ritAccIpt) {
        this.ritAccIpt = ritAccIpt;
    }

    public short getRitAccIpt() {
        return this.ritAccIpt;
    }

    public void setRitVisIpt(short ritVisIpt) {
        this.ritVisIpt = ritVisIpt;
    }

    public short getRitVisIpt() {
        return this.ritVisIpt;
    }

    public void setRitTfrIpt(short ritTfrIpt) {
        this.ritTfrIpt = ritTfrIpt;
    }

    public short getRitTfrIpt() {
        return this.ritTfrIpt;
    }

    public void setImpstIrpefIpt(short impstIrpefIpt) {
        this.impstIrpefIpt = impstIrpefIpt;
    }

    public short getImpstIrpefIpt() {
        return this.impstIrpefIpt;
    }

    public void setImpstSostIpt(short impstSostIpt) {
        this.impstSostIpt = impstSostIpt;
    }

    public short getImpstSostIpt() {
        return this.impstSostIpt;
    }

    public void setImpstPrvrIpt(short impstPrvrIpt) {
        this.impstPrvrIpt = impstPrvrIpt;
    }

    public short getImpstPrvrIpt() {
        return this.impstPrvrIpt;
    }

    public void setImpst252Ipt(short impst252Ipt) {
        this.impst252Ipt = impst252Ipt;
    }

    public short getImpst252Ipt() {
        return this.impst252Ipt;
    }

    public void setIdAssto(short idAssto) {
        this.idAssto = idAssto;
    }

    public short getIdAssto() {
        return this.idAssto;
    }

    public void setTaxSep(short taxSep) {
        this.taxSep = taxSep;
    }

    public short getTaxSep() {
        return this.taxSep;
    }

    public void setDtRiserveSomP(short dtRiserveSomP) {
        this.dtRiserveSomP = dtRiserveSomP;
    }

    public short getDtRiserveSomP() {
        return this.dtRiserveSomP;
    }

    public void setDtVlt(short dtVlt) {
        this.dtVlt = dtVlt;
    }

    public short getDtVlt() {
        return this.dtVlt;
    }

    public void setTpStatLiqBnficr(short tpStatLiqBnficr) {
        this.tpStatLiqBnficr = tpStatLiqBnficr;
    }

    public short getTpStatLiqBnficr() {
        return this.tpStatLiqBnficr;
    }

    public void setRichCalcCnbtInp(short richCalcCnbtInp) {
        this.richCalcCnbtInp = richCalcCnbtInp;
    }

    public short getRichCalcCnbtInp() {
        return this.richCalcCnbtInp;
    }

    public void setImpIntrRitPag(short impIntrRitPag) {
        this.impIntrRitPag = impIntrRitPag;
    }

    public short getImpIntrRitPag() {
        return this.impIntrRitPag;
    }

    public void setDtUltDocto(short dtUltDocto) {
        this.dtUltDocto = dtUltDocto;
    }

    public short getDtUltDocto() {
        return this.dtUltDocto;
    }

    public void setDtDormienza(short dtDormienza) {
        this.dtDormienza = dtDormienza;
    }

    public short getDtDormienza() {
        return this.dtDormienza;
    }

    public void setImpstBolloTotV(short impstBolloTotV) {
        this.impstBolloTotV = impstBolloTotV;
    }

    public short getImpstBolloTotV() {
        return this.impstBolloTotV;
    }

    public void setImpstVis1382011(short impstVis1382011) {
        this.impstVis1382011 = impstVis1382011;
    }

    public short getImpstVis1382011() {
        return this.impstVis1382011;
    }

    public void setImpstSost1382011(short impstSost1382011) {
        this.impstSost1382011 = impstSost1382011;
    }

    public short getImpstSost1382011() {
        return this.impstSost1382011;
    }

    public void setImpstVis662014(short impstVis662014) {
        this.impstVis662014 = impstVis662014;
    }

    public short getImpstVis662014() {
        return this.impstVis662014;
    }

    public void setImpstSost662014(short impstSost662014) {
        this.impstSost662014 = impstSost662014;
    }

    public short getImpstSost662014() {
        return this.impstSost662014;
    }
}
