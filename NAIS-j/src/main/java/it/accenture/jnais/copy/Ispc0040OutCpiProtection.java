package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.Ispc0040DatiPacchetto;

/**Original name: ISPC0040-OUT-CPI-PROTECTION<br>
 * Variable: ISPC0040-OUT-CPI-PROTECTION from copybook ISPC0040<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ispc0040OutCpiProtection {

    //==== PROPERTIES ====
    public static final int DATI_PACCHETTO_MAXOCCURS = 10;
    /**Original name: ISPC0040-AMM-CONTR-RES-EST<br>
	 * <pre>-- ammissibilità contraente residente all'estero (dominio 1/0)</pre>*/
    private char ammContrResEst = DefaultValues.CHAR_VAL;
    /**Original name: ISPC0040-FL-CPI-PROTECTION<br>
	 * <pre>-- flag che indica se il prodotto è Cpi&Protection  (dominio 1/0)</pre>*/
    private char flCpiProtection = DefaultValues.CHAR_VAL;
    /**Original name: ISPC0040-FL-BUNDLING<br>
	 * <pre>-- flag che indica se il prodotto è Bundling  (dominio 1/0)</pre>*/
    private char flBundling = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-FL-BUNDLING-TRATT
    private char flBundlingTratt = DefaultValues.CHAR_VAL;
    /**Original name: ISPC0040-FL-PRINC-COLL<br>
	 * <pre>-- flag che indica se il prodotto è un prodotto principale o un
	 * -- collegato (dominio P=Principale/C=Collegato)</pre>*/
    private char flPrincColl = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-TRATT-PACCHETTO
    private char trattPacchetto = DefaultValues.CHAR_VAL;
    //Original name: ISPC0040-ELE-MAX-PACCHETTO
    private String eleMaxPacchetto = DefaultValues.stringVal(Len.ELE_MAX_PACCHETTO);
    //Original name: ISPC0040-DATI-PACCHETTO
    private Ispc0040DatiPacchetto[] datiPacchetto = new Ispc0040DatiPacchetto[DATI_PACCHETTO_MAXOCCURS];
    /**Original name: ISPC0040-FL-CONT-COINC-ASST<br>
	 * <pre>-- flag che indica se il prodotto prevede obbligatoriamente
	 * -- un assicurato coincidente con il contraente (dominio 1/0)</pre>*/
    private char flContCoincAsst = DefaultValues.CHAR_VAL;

    //==== CONSTRUCTORS ====
    public Ispc0040OutCpiProtection() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int datiPacchettoIdx = 1; datiPacchettoIdx <= DATI_PACCHETTO_MAXOCCURS; datiPacchettoIdx++) {
            datiPacchetto[datiPacchettoIdx - 1] = new Ispc0040DatiPacchetto();
        }
    }

    public void setIspc0040OutCpiProtectionBytes(byte[] buffer, int offset) {
        int position = offset;
        ammContrResEst = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flCpiProtection = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flBundling = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flBundlingTratt = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flPrincColl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        trattPacchetto = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        eleMaxPacchetto = MarshalByte.readFixedString(buffer, position, Len.ELE_MAX_PACCHETTO);
        position += Len.ELE_MAX_PACCHETTO;
        for (int idx = 1; idx <= DATI_PACCHETTO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                datiPacchetto[idx - 1].setDatiPacchettoBytes(buffer, position);
                position += Ispc0040DatiPacchetto.Len.DATI_PACCHETTO;
            }
            else {
                datiPacchetto[idx - 1].initDatiPacchettoSpaces();
                position += Ispc0040DatiPacchetto.Len.DATI_PACCHETTO;
            }
        }
        flContCoincAsst = MarshalByte.readChar(buffer, position);
    }

    public byte[] getIspc0040OutCpiProtectionBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, ammContrResEst);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flCpiProtection);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flBundling);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flBundlingTratt);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flPrincColl);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, trattPacchetto);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, eleMaxPacchetto, Len.ELE_MAX_PACCHETTO);
        position += Len.ELE_MAX_PACCHETTO;
        for (int idx = 1; idx <= DATI_PACCHETTO_MAXOCCURS; idx++) {
            datiPacchetto[idx - 1].getDatiPacchettoBytes(buffer, position);
            position += Ispc0040DatiPacchetto.Len.DATI_PACCHETTO;
        }
        MarshalByte.writeChar(buffer, position, flContCoincAsst);
        return buffer;
    }

    public void setAmmContrResEst(char ammContrResEst) {
        this.ammContrResEst = ammContrResEst;
    }

    public char getAmmContrResEst() {
        return this.ammContrResEst;
    }

    public void setFlCpiProtection(char flCpiProtection) {
        this.flCpiProtection = flCpiProtection;
    }

    public char getFlCpiProtection() {
        return this.flCpiProtection;
    }

    public void setFlBundling(char flBundling) {
        this.flBundling = flBundling;
    }

    public char getFlBundling() {
        return this.flBundling;
    }

    public void setFlBundlingTratt(char flBundlingTratt) {
        this.flBundlingTratt = flBundlingTratt;
    }

    public char getFlBundlingTratt() {
        return this.flBundlingTratt;
    }

    public void setFlPrincColl(char flPrincColl) {
        this.flPrincColl = flPrincColl;
    }

    public char getFlPrincColl() {
        return this.flPrincColl;
    }

    public void setTrattPacchetto(char trattPacchetto) {
        this.trattPacchetto = trattPacchetto;
    }

    public char getTrattPacchetto() {
        return this.trattPacchetto;
    }

    public void setIspc0040EleMaxPacchettoFormatted(String ispc0040EleMaxPacchetto) {
        this.eleMaxPacchetto = Trunc.toUnsignedNumeric(ispc0040EleMaxPacchetto, Len.ELE_MAX_PACCHETTO);
    }

    public short getIspc0040EleMaxPacchetto() {
        return NumericDisplay.asShort(this.eleMaxPacchetto);
    }

    public void setFlContCoincAsst(char flContCoincAsst) {
        this.flContCoincAsst = flContCoincAsst;
    }

    public char getFlContCoincAsst() {
        return this.flContCoincAsst;
    }

    public Ispc0040DatiPacchetto getDatiPacchetto(int idx) {
        return datiPacchetto[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AMM_CONTR_RES_EST = 1;
        public static final int FL_CPI_PROTECTION = 1;
        public static final int FL_BUNDLING = 1;
        public static final int FL_BUNDLING_TRATT = 1;
        public static final int FL_PRINC_COLL = 1;
        public static final int TRATT_PACCHETTO = 1;
        public static final int ELE_MAX_PACCHETTO = 3;
        public static final int FL_CONT_COINC_ASST = 1;
        public static final int ISPC0040_OUT_CPI_PROTECTION = AMM_CONTR_RES_EST + FL_CPI_PROTECTION + FL_BUNDLING + FL_BUNDLING_TRATT + FL_PRINC_COLL + TRATT_PACCHETTO + ELE_MAX_PACCHETTO + Ispc0040OutCpiProtection.DATI_PACCHETTO_MAXOCCURS * Ispc0040DatiPacchetto.Len.DATI_PACCHETTO + FL_CONT_COINC_ASST;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
