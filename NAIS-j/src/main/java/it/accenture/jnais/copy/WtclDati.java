package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTCL-DATI<br>
 * Variable: WTCL-DATI from copybook LCCVTCL1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WtclDati {

    //==== PROPERTIES ====
    //Original name: WTCL-ID-TCONT-LIQ
    private int idTcontLiq = DefaultValues.INT_VAL;
    //Original name: WTCL-ID-PERC-LIQ
    private int idPercLiq = DefaultValues.INT_VAL;
    //Original name: WTCL-ID-BNFICR-LIQ
    private int idBnficrLiq = DefaultValues.INT_VAL;
    //Original name: WTCL-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: WTCL-ID-MOVI-CHIU
    private int idMoviChiu = DefaultValues.INT_VAL;
    //Original name: WTCL-DT-INI-EFF
    private int dtIniEff = DefaultValues.INT_VAL;
    //Original name: WTCL-DT-END-EFF
    private int dtEndEff = DefaultValues.INT_VAL;
    //Original name: WTCL-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: WTCL-DT-VLT
    private int dtVlt = DefaultValues.INT_VAL;
    //Original name: WTCL-IMP-LRD-LIQTO
    private AfDecimal impLrdLiqto = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-PREST
    private AfDecimal impPrest = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-INTR-PREST
    private AfDecimal impIntrPrest = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-RAT
    private AfDecimal impRat = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-UTI
    private AfDecimal impUti = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-RIT-TFR
    private AfDecimal impRitTfr = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-RIT-ACC
    private AfDecimal impRitAcc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-RIT-VIS
    private AfDecimal impRitVis = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPB-TFR
    private AfDecimal impbTfr = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPB-ACC
    private AfDecimal impbAcc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPB-VIS
    private AfDecimal impbVis = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-RIMB
    private AfDecimal impRimb = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-CORTVO
    private AfDecimal impCortvo = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPB-IMPST-PRVR
    private AfDecimal impbImpstPrvr = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPST-PRVR
    private AfDecimal impstPrvr = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPB-IMPST-252
    private AfDecimal impbImpst252 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPST-252
    private AfDecimal impst252 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-IS
    private AfDecimal impIs = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-DIR-LIQ
    private AfDecimal impDirLiq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-NET-LIQTO
    private AfDecimal impNetLiqto = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-EFFLQ
    private AfDecimal impEfflq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-COD-DVS
    private String codDvs = DefaultValues.stringVal(Len.COD_DVS);
    //Original name: WTCL-TP-MEZ-PAG-ACCR
    private String tpMezPagAccr = DefaultValues.stringVal(Len.TP_MEZ_PAG_ACCR);
    //Original name: WTCL-ESTR-CNT-CORR-ACCR
    private String estrCntCorrAccr = DefaultValues.stringVal(Len.ESTR_CNT_CORR_ACCR);
    //Original name: WTCL-DS-RIGA
    private long dsRiga = DefaultValues.LONG_VAL;
    //Original name: WTCL-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WTCL-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: WTCL-DS-TS-INI-CPTZ
    private long dsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WTCL-DS-TS-END-CPTZ
    private long dsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WTCL-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: WTCL-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WTCL-TP-STAT-TIT
    private String tpStatTit = DefaultValues.stringVal(Len.TP_STAT_TIT);
    //Original name: WTCL-IMPB-VIS-1382011
    private AfDecimal impbVis1382011 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPST-VIS-1382011
    private AfDecimal impstVis1382011 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPST-SOST-1382011
    private AfDecimal impstSost1382011 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMP-INTR-RIT-PAG
    private AfDecimal impIntrRitPag = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPB-IS
    private AfDecimal impbIs = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPB-IS-1382011
    private AfDecimal impbIs1382011 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPB-VIS-662014
    private AfDecimal impbVis662014 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPST-VIS-662014
    private AfDecimal impstVis662014 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPB-IS-662014
    private AfDecimal impbIs662014 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WTCL-IMPST-SOST-662014
    private AfDecimal impstSost662014 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        idTcontLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_TCONT_LIQ, 0);
        position += Len.ID_TCONT_LIQ;
        idPercLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_PERC_LIQ, 0);
        position += Len.ID_PERC_LIQ;
        idBnficrLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_BNFICR_LIQ, 0);
        position += Len.ID_BNFICR_LIQ;
        idMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        idMoviChiu = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        dtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        dtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        dtVlt = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_VLT, 0);
        position += Len.DT_VLT;
        impLrdLiqto.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_LRD_LIQTO, Len.Fract.IMP_LRD_LIQTO));
        position += Len.IMP_LRD_LIQTO;
        impPrest.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_PREST, Len.Fract.IMP_PREST));
        position += Len.IMP_PREST;
        impIntrPrest.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_INTR_PREST, Len.Fract.IMP_INTR_PREST));
        position += Len.IMP_INTR_PREST;
        impRat.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_RAT, Len.Fract.IMP_RAT));
        position += Len.IMP_RAT;
        impUti.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_UTI, Len.Fract.IMP_UTI));
        position += Len.IMP_UTI;
        impRitTfr.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_RIT_TFR, Len.Fract.IMP_RIT_TFR));
        position += Len.IMP_RIT_TFR;
        impRitAcc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_RIT_ACC, Len.Fract.IMP_RIT_ACC));
        position += Len.IMP_RIT_ACC;
        impRitVis.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_RIT_VIS, Len.Fract.IMP_RIT_VIS));
        position += Len.IMP_RIT_VIS;
        impbTfr.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_TFR, Len.Fract.IMPB_TFR));
        position += Len.IMPB_TFR;
        impbAcc.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_ACC, Len.Fract.IMPB_ACC));
        position += Len.IMPB_ACC;
        impbVis.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_VIS, Len.Fract.IMPB_VIS));
        position += Len.IMPB_VIS;
        impRimb.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_RIMB, Len.Fract.IMP_RIMB));
        position += Len.IMP_RIMB;
        impCortvo.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_CORTVO, Len.Fract.IMP_CORTVO));
        position += Len.IMP_CORTVO;
        impbImpstPrvr.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_IMPST_PRVR, Len.Fract.IMPB_IMPST_PRVR));
        position += Len.IMPB_IMPST_PRVR;
        impstPrvr.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPST_PRVR, Len.Fract.IMPST_PRVR));
        position += Len.IMPST_PRVR;
        impbImpst252.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_IMPST252, Len.Fract.IMPB_IMPST252));
        position += Len.IMPB_IMPST252;
        impst252.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPST252, Len.Fract.IMPST252));
        position += Len.IMPST252;
        impIs.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_IS, Len.Fract.IMP_IS));
        position += Len.IMP_IS;
        impDirLiq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_DIR_LIQ, Len.Fract.IMP_DIR_LIQ));
        position += Len.IMP_DIR_LIQ;
        impNetLiqto.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_NET_LIQTO, Len.Fract.IMP_NET_LIQTO));
        position += Len.IMP_NET_LIQTO;
        impEfflq.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_EFFLQ, Len.Fract.IMP_EFFLQ));
        position += Len.IMP_EFFLQ;
        codDvs = MarshalByte.readString(buffer, position, Len.COD_DVS);
        position += Len.COD_DVS;
        tpMezPagAccr = MarshalByte.readString(buffer, position, Len.TP_MEZ_PAG_ACCR);
        position += Len.TP_MEZ_PAG_ACCR;
        estrCntCorrAccr = MarshalByte.readString(buffer, position, Len.ESTR_CNT_CORR_ACCR);
        position += Len.ESTR_CNT_CORR_ACCR;
        dsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        dsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpStatTit = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT);
        position += Len.TP_STAT_TIT;
        impbVis1382011.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_VIS1382011, Len.Fract.IMPB_VIS1382011));
        position += Len.IMPB_VIS1382011;
        impstVis1382011.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPST_VIS1382011, Len.Fract.IMPST_VIS1382011));
        position += Len.IMPST_VIS1382011;
        impstSost1382011.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPST_SOST1382011, Len.Fract.IMPST_SOST1382011));
        position += Len.IMPST_SOST1382011;
        impIntrRitPag.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_INTR_RIT_PAG, Len.Fract.IMP_INTR_RIT_PAG));
        position += Len.IMP_INTR_RIT_PAG;
        impbIs.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_IS, Len.Fract.IMPB_IS));
        position += Len.IMPB_IS;
        impbIs1382011.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_IS1382011, Len.Fract.IMPB_IS1382011));
        position += Len.IMPB_IS1382011;
        impbVis662014.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_VIS662014, Len.Fract.IMPB_VIS662014));
        position += Len.IMPB_VIS662014;
        impstVis662014.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPST_VIS662014, Len.Fract.IMPST_VIS662014));
        position += Len.IMPST_VIS662014;
        impbIs662014.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_IS662014, Len.Fract.IMPB_IS662014));
        position += Len.IMPB_IS662014;
        impstSost662014.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPST_SOST662014, Len.Fract.IMPST_SOST662014));
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idTcontLiq, Len.Int.ID_TCONT_LIQ, 0);
        position += Len.ID_TCONT_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, idPercLiq, Len.Int.ID_PERC_LIQ, 0);
        position += Len.ID_PERC_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, idBnficrLiq, Len.Int.ID_BNFICR_LIQ, 0);
        position += Len.ID_BNFICR_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviCrz, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviChiu, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniEff, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtEndEff, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, dtVlt, Len.Int.DT_VLT, 0);
        position += Len.DT_VLT;
        MarshalByte.writeDecimalAsPacked(buffer, position, impLrdLiqto.copy());
        position += Len.IMP_LRD_LIQTO;
        MarshalByte.writeDecimalAsPacked(buffer, position, impPrest.copy());
        position += Len.IMP_PREST;
        MarshalByte.writeDecimalAsPacked(buffer, position, impIntrPrest.copy());
        position += Len.IMP_INTR_PREST;
        MarshalByte.writeDecimalAsPacked(buffer, position, impRat.copy());
        position += Len.IMP_RAT;
        MarshalByte.writeDecimalAsPacked(buffer, position, impUti.copy());
        position += Len.IMP_UTI;
        MarshalByte.writeDecimalAsPacked(buffer, position, impRitTfr.copy());
        position += Len.IMP_RIT_TFR;
        MarshalByte.writeDecimalAsPacked(buffer, position, impRitAcc.copy());
        position += Len.IMP_RIT_ACC;
        MarshalByte.writeDecimalAsPacked(buffer, position, impRitVis.copy());
        position += Len.IMP_RIT_VIS;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbTfr.copy());
        position += Len.IMPB_TFR;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbAcc.copy());
        position += Len.IMPB_ACC;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbVis.copy());
        position += Len.IMPB_VIS;
        MarshalByte.writeDecimalAsPacked(buffer, position, impRimb.copy());
        position += Len.IMP_RIMB;
        MarshalByte.writeDecimalAsPacked(buffer, position, impCortvo.copy());
        position += Len.IMP_CORTVO;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbImpstPrvr.copy());
        position += Len.IMPB_IMPST_PRVR;
        MarshalByte.writeDecimalAsPacked(buffer, position, impstPrvr.copy());
        position += Len.IMPST_PRVR;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbImpst252.copy());
        position += Len.IMPB_IMPST252;
        MarshalByte.writeDecimalAsPacked(buffer, position, impst252.copy());
        position += Len.IMPST252;
        MarshalByte.writeDecimalAsPacked(buffer, position, impIs.copy());
        position += Len.IMP_IS;
        MarshalByte.writeDecimalAsPacked(buffer, position, impDirLiq.copy());
        position += Len.IMP_DIR_LIQ;
        MarshalByte.writeDecimalAsPacked(buffer, position, impNetLiqto.copy());
        position += Len.IMP_NET_LIQTO;
        MarshalByte.writeDecimalAsPacked(buffer, position, impEfflq.copy());
        position += Len.IMP_EFFLQ;
        MarshalByte.writeString(buffer, position, codDvs, Len.COD_DVS);
        position += Len.COD_DVS;
        MarshalByte.writeString(buffer, position, tpMezPagAccr, Len.TP_MEZ_PAG_ACCR);
        position += Len.TP_MEZ_PAG_ACCR;
        MarshalByte.writeString(buffer, position, estrCntCorrAccr, Len.ESTR_CNT_CORR_ACCR);
        position += Len.ESTR_CNT_CORR_ACCR;
        MarshalByte.writeLongAsPacked(buffer, position, dsRiga, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpStatTit, Len.TP_STAT_TIT);
        position += Len.TP_STAT_TIT;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbVis1382011.copy());
        position += Len.IMPB_VIS1382011;
        MarshalByte.writeDecimalAsPacked(buffer, position, impstVis1382011.copy());
        position += Len.IMPST_VIS1382011;
        MarshalByte.writeDecimalAsPacked(buffer, position, impstSost1382011.copy());
        position += Len.IMPST_SOST1382011;
        MarshalByte.writeDecimalAsPacked(buffer, position, impIntrRitPag.copy());
        position += Len.IMP_INTR_RIT_PAG;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbIs.copy());
        position += Len.IMPB_IS;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbIs1382011.copy());
        position += Len.IMPB_IS1382011;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbVis662014.copy());
        position += Len.IMPB_VIS662014;
        MarshalByte.writeDecimalAsPacked(buffer, position, impstVis662014.copy());
        position += Len.IMPST_VIS662014;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbIs662014.copy());
        position += Len.IMPB_IS662014;
        MarshalByte.writeDecimalAsPacked(buffer, position, impstSost662014.copy());
        return buffer;
    }

    public void setIdTcontLiq(int idTcontLiq) {
        this.idTcontLiq = idTcontLiq;
    }

    public int getIdTcontLiq() {
        return this.idTcontLiq;
    }

    public void setIdPercLiq(int idPercLiq) {
        this.idPercLiq = idPercLiq;
    }

    public int getIdPercLiq() {
        return this.idPercLiq;
    }

    public void setIdBnficrLiq(int idBnficrLiq) {
        this.idBnficrLiq = idBnficrLiq;
    }

    public int getIdBnficrLiq() {
        return this.idBnficrLiq;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setIdMoviChiu(int idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public int getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtIniEff(int dtIniEff) {
        this.dtIniEff = dtIniEff;
    }

    public int getDtIniEff() {
        return this.dtIniEff;
    }

    public void setDtEndEff(int dtEndEff) {
        this.dtEndEff = dtEndEff;
    }

    public int getDtEndEff() {
        return this.dtEndEff;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setDtVlt(int dtVlt) {
        this.dtVlt = dtVlt;
    }

    public int getDtVlt() {
        return this.dtVlt;
    }

    public void setImpLrdLiqto(AfDecimal impLrdLiqto) {
        this.impLrdLiqto.assign(impLrdLiqto);
    }

    public AfDecimal getImpLrdLiqto() {
        return this.impLrdLiqto.copy();
    }

    public void setImpPrest(AfDecimal impPrest) {
        this.impPrest.assign(impPrest);
    }

    public AfDecimal getImpPrest() {
        return this.impPrest.copy();
    }

    public void setImpIntrPrest(AfDecimal impIntrPrest) {
        this.impIntrPrest.assign(impIntrPrest);
    }

    public AfDecimal getImpIntrPrest() {
        return this.impIntrPrest.copy();
    }

    public void setImpRat(AfDecimal impRat) {
        this.impRat.assign(impRat);
    }

    public AfDecimal getImpRat() {
        return this.impRat.copy();
    }

    public void setImpUti(AfDecimal impUti) {
        this.impUti.assign(impUti);
    }

    public AfDecimal getImpUti() {
        return this.impUti.copy();
    }

    public void setImpRitTfr(AfDecimal impRitTfr) {
        this.impRitTfr.assign(impRitTfr);
    }

    public AfDecimal getImpRitTfr() {
        return this.impRitTfr.copy();
    }

    public void setImpRitAcc(AfDecimal impRitAcc) {
        this.impRitAcc.assign(impRitAcc);
    }

    public AfDecimal getImpRitAcc() {
        return this.impRitAcc.copy();
    }

    public void setImpRitVis(AfDecimal impRitVis) {
        this.impRitVis.assign(impRitVis);
    }

    public AfDecimal getImpRitVis() {
        return this.impRitVis.copy();
    }

    public void setImpbTfr(AfDecimal impbTfr) {
        this.impbTfr.assign(impbTfr);
    }

    public AfDecimal getImpbTfr() {
        return this.impbTfr.copy();
    }

    public void setImpbAcc(AfDecimal impbAcc) {
        this.impbAcc.assign(impbAcc);
    }

    public AfDecimal getImpbAcc() {
        return this.impbAcc.copy();
    }

    public void setImpbVis(AfDecimal impbVis) {
        this.impbVis.assign(impbVis);
    }

    public AfDecimal getImpbVis() {
        return this.impbVis.copy();
    }

    public void setImpRimb(AfDecimal impRimb) {
        this.impRimb.assign(impRimb);
    }

    public AfDecimal getImpRimb() {
        return this.impRimb.copy();
    }

    public void setImpCortvo(AfDecimal impCortvo) {
        this.impCortvo.assign(impCortvo);
    }

    public AfDecimal getImpCortvo() {
        return this.impCortvo.copy();
    }

    public void setImpbImpstPrvr(AfDecimal impbImpstPrvr) {
        this.impbImpstPrvr.assign(impbImpstPrvr);
    }

    public AfDecimal getImpbImpstPrvr() {
        return this.impbImpstPrvr.copy();
    }

    public void setImpstPrvr(AfDecimal impstPrvr) {
        this.impstPrvr.assign(impstPrvr);
    }

    public AfDecimal getImpstPrvr() {
        return this.impstPrvr.copy();
    }

    public void setImpbImpst252(AfDecimal impbImpst252) {
        this.impbImpst252.assign(impbImpst252);
    }

    public AfDecimal getImpbImpst252() {
        return this.impbImpst252.copy();
    }

    public void setImpst252(AfDecimal impst252) {
        this.impst252.assign(impst252);
    }

    public AfDecimal getImpst252() {
        return this.impst252.copy();
    }

    public void setImpIs(AfDecimal impIs) {
        this.impIs.assign(impIs);
    }

    public AfDecimal getImpIs() {
        return this.impIs.copy();
    }

    public void setImpDirLiq(AfDecimal impDirLiq) {
        this.impDirLiq.assign(impDirLiq);
    }

    public AfDecimal getImpDirLiq() {
        return this.impDirLiq.copy();
    }

    public void setImpNetLiqto(AfDecimal impNetLiqto) {
        this.impNetLiqto.assign(impNetLiqto);
    }

    public AfDecimal getImpNetLiqto() {
        return this.impNetLiqto.copy();
    }

    public void setImpEfflq(AfDecimal impEfflq) {
        this.impEfflq.assign(impEfflq);
    }

    public AfDecimal getImpEfflq() {
        return this.impEfflq.copy();
    }

    public void setCodDvs(String codDvs) {
        this.codDvs = Functions.subString(codDvs, Len.COD_DVS);
    }

    public String getCodDvs() {
        return this.codDvs;
    }

    public void setTpMezPagAccr(String tpMezPagAccr) {
        this.tpMezPagAccr = Functions.subString(tpMezPagAccr, Len.TP_MEZ_PAG_ACCR);
    }

    public String getTpMezPagAccr() {
        return this.tpMezPagAccr;
    }

    public void setEstrCntCorrAccr(String estrCntCorrAccr) {
        this.estrCntCorrAccr = Functions.subString(estrCntCorrAccr, Len.ESTR_CNT_CORR_ACCR);
    }

    public String getEstrCntCorrAccr() {
        return this.estrCntCorrAccr;
    }

    public void setDsRiga(long dsRiga) {
        this.dsRiga = dsRiga;
    }

    public long getDsRiga() {
        return this.dsRiga;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dsTsIniCptz = dsTsIniCptz;
    }

    public long getDsTsIniCptz() {
        return this.dsTsIniCptz;
    }

    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dsTsEndCptz = dsTsEndCptz;
    }

    public long getDsTsEndCptz() {
        return this.dsTsEndCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setTpStatTit(String tpStatTit) {
        this.tpStatTit = Functions.subString(tpStatTit, Len.TP_STAT_TIT);
    }

    public String getTpStatTit() {
        return this.tpStatTit;
    }

    public void setImpbVis1382011(AfDecimal impbVis1382011) {
        this.impbVis1382011.assign(impbVis1382011);
    }

    public AfDecimal getImpbVis1382011() {
        return this.impbVis1382011.copy();
    }

    public void setImpstVis1382011(AfDecimal impstVis1382011) {
        this.impstVis1382011.assign(impstVis1382011);
    }

    public AfDecimal getImpstVis1382011() {
        return this.impstVis1382011.copy();
    }

    public void setImpstSost1382011(AfDecimal impstSost1382011) {
        this.impstSost1382011.assign(impstSost1382011);
    }

    public AfDecimal getImpstSost1382011() {
        return this.impstSost1382011.copy();
    }

    public void setImpIntrRitPag(AfDecimal impIntrRitPag) {
        this.impIntrRitPag.assign(impIntrRitPag);
    }

    public AfDecimal getImpIntrRitPag() {
        return this.impIntrRitPag.copy();
    }

    public void setImpbIs(AfDecimal impbIs) {
        this.impbIs.assign(impbIs);
    }

    public AfDecimal getImpbIs() {
        return this.impbIs.copy();
    }

    public void setImpbIs1382011(AfDecimal impbIs1382011) {
        this.impbIs1382011.assign(impbIs1382011);
    }

    public AfDecimal getImpbIs1382011() {
        return this.impbIs1382011.copy();
    }

    public void setImpbVis662014(AfDecimal impbVis662014) {
        this.impbVis662014.assign(impbVis662014);
    }

    public AfDecimal getImpbVis662014() {
        return this.impbVis662014.copy();
    }

    public void setImpstVis662014(AfDecimal impstVis662014) {
        this.impstVis662014.assign(impstVis662014);
    }

    public AfDecimal getImpstVis662014() {
        return this.impstVis662014.copy();
    }

    public void setImpbIs662014(AfDecimal impbIs662014) {
        this.impbIs662014.assign(impbIs662014);
    }

    public AfDecimal getImpbIs662014() {
        return this.impbIs662014.copy();
    }

    public void setImpstSost662014(AfDecimal impstSost662014) {
        this.impstSost662014.assign(impstSost662014);
    }

    public AfDecimal getImpstSost662014() {
        return this.impstSost662014.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_TCONT_LIQ = 5;
        public static final int ID_PERC_LIQ = 5;
        public static final int ID_BNFICR_LIQ = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int DT_VLT = 5;
        public static final int IMP_LRD_LIQTO = 8;
        public static final int IMP_PREST = 8;
        public static final int IMP_INTR_PREST = 8;
        public static final int IMP_RAT = 8;
        public static final int IMP_UTI = 8;
        public static final int IMP_RIT_TFR = 8;
        public static final int IMP_RIT_ACC = 8;
        public static final int IMP_RIT_VIS = 8;
        public static final int IMPB_TFR = 8;
        public static final int IMPB_ACC = 8;
        public static final int IMPB_VIS = 8;
        public static final int IMP_RIMB = 8;
        public static final int IMP_CORTVO = 8;
        public static final int IMPB_IMPST_PRVR = 8;
        public static final int IMPST_PRVR = 8;
        public static final int IMPB_IMPST252 = 8;
        public static final int IMPST252 = 8;
        public static final int IMP_IS = 8;
        public static final int IMP_DIR_LIQ = 8;
        public static final int IMP_NET_LIQTO = 8;
        public static final int IMP_EFFLQ = 8;
        public static final int COD_DVS = 20;
        public static final int TP_MEZ_PAG_ACCR = 2;
        public static final int ESTR_CNT_CORR_ACCR = 20;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int TP_STAT_TIT = 2;
        public static final int IMPB_VIS1382011 = 8;
        public static final int IMPST_VIS1382011 = 8;
        public static final int IMPST_SOST1382011 = 8;
        public static final int IMP_INTR_RIT_PAG = 8;
        public static final int IMPB_IS = 8;
        public static final int IMPB_IS1382011 = 8;
        public static final int IMPB_VIS662014 = 8;
        public static final int IMPST_VIS662014 = 8;
        public static final int IMPB_IS662014 = 8;
        public static final int IMPST_SOST662014 = 8;
        public static final int DATI = ID_TCONT_LIQ + ID_PERC_LIQ + ID_BNFICR_LIQ + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + DT_VLT + IMP_LRD_LIQTO + IMP_PREST + IMP_INTR_PREST + IMP_RAT + IMP_UTI + IMP_RIT_TFR + IMP_RIT_ACC + IMP_RIT_VIS + IMPB_TFR + IMPB_ACC + IMPB_VIS + IMP_RIMB + IMP_CORTVO + IMPB_IMPST_PRVR + IMPST_PRVR + IMPB_IMPST252 + IMPST252 + IMP_IS + IMP_DIR_LIQ + IMP_NET_LIQTO + IMP_EFFLQ + COD_DVS + TP_MEZ_PAG_ACCR + ESTR_CNT_CORR_ACCR + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + TP_STAT_TIT + IMPB_VIS1382011 + IMPST_VIS1382011 + IMPST_SOST1382011 + IMP_INTR_RIT_PAG + IMPB_IS + IMPB_IS1382011 + IMPB_VIS662014 + IMPST_VIS662014 + IMPB_IS662014 + IMPST_SOST662014;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_TCONT_LIQ = 9;
            public static final int ID_PERC_LIQ = 9;
            public static final int ID_BNFICR_LIQ = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_VLT = 8;
            public static final int IMP_LRD_LIQTO = 12;
            public static final int IMP_PREST = 12;
            public static final int IMP_INTR_PREST = 12;
            public static final int IMP_RAT = 12;
            public static final int IMP_UTI = 12;
            public static final int IMP_RIT_TFR = 12;
            public static final int IMP_RIT_ACC = 12;
            public static final int IMP_RIT_VIS = 12;
            public static final int IMPB_TFR = 12;
            public static final int IMPB_ACC = 12;
            public static final int IMPB_VIS = 12;
            public static final int IMP_RIMB = 12;
            public static final int IMP_CORTVO = 12;
            public static final int IMPB_IMPST_PRVR = 12;
            public static final int IMPST_PRVR = 12;
            public static final int IMPB_IMPST252 = 12;
            public static final int IMPST252 = 12;
            public static final int IMP_IS = 12;
            public static final int IMP_DIR_LIQ = 12;
            public static final int IMP_NET_LIQTO = 12;
            public static final int IMP_EFFLQ = 12;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int IMPB_VIS1382011 = 12;
            public static final int IMPST_VIS1382011 = 12;
            public static final int IMPST_SOST1382011 = 12;
            public static final int IMP_INTR_RIT_PAG = 12;
            public static final int IMPB_IS = 12;
            public static final int IMPB_IS1382011 = 12;
            public static final int IMPB_VIS662014 = 12;
            public static final int IMPST_VIS662014 = 12;
            public static final int IMPB_IS662014 = 12;
            public static final int IMPST_SOST662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMP_LRD_LIQTO = 3;
            public static final int IMP_PREST = 3;
            public static final int IMP_INTR_PREST = 3;
            public static final int IMP_RAT = 3;
            public static final int IMP_UTI = 3;
            public static final int IMP_RIT_TFR = 3;
            public static final int IMP_RIT_ACC = 3;
            public static final int IMP_RIT_VIS = 3;
            public static final int IMPB_TFR = 3;
            public static final int IMPB_ACC = 3;
            public static final int IMPB_VIS = 3;
            public static final int IMP_RIMB = 3;
            public static final int IMP_CORTVO = 3;
            public static final int IMPB_IMPST_PRVR = 3;
            public static final int IMPST_PRVR = 3;
            public static final int IMPB_IMPST252 = 3;
            public static final int IMPST252 = 3;
            public static final int IMP_IS = 3;
            public static final int IMP_DIR_LIQ = 3;
            public static final int IMP_NET_LIQTO = 3;
            public static final int IMP_EFFLQ = 3;
            public static final int IMPB_VIS1382011 = 3;
            public static final int IMPST_VIS1382011 = 3;
            public static final int IMPST_SOST1382011 = 3;
            public static final int IMP_INTR_RIT_PAG = 3;
            public static final int IMPB_IS = 3;
            public static final int IMPB_IS1382011 = 3;
            public static final int IMPB_VIS662014 = 3;
            public static final int IMPST_VIS662014 = 3;
            public static final int IMPB_IS662014 = 3;
            public static final int IMPST_SOST662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
