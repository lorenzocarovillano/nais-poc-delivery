package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WdeqIdCompQuest;
import it.accenture.jnais.ws.redefines.WdeqIdMoviChiu;
import it.accenture.jnais.ws.redefines.WdeqRispDt;
import it.accenture.jnais.ws.redefines.WdeqRispImp;
import it.accenture.jnais.ws.redefines.WdeqRispNum;
import it.accenture.jnais.ws.redefines.WdeqRispPc;
import it.accenture.jnais.ws.redefines.WdeqRispTs;

/**Original name: WDEQ-DATI<br>
 * Variable: WDEQ-DATI from copybook LCCVDEQ1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WdeqDati {

    //==== PROPERTIES ====
    //Original name: WDEQ-ID-DETT-QUEST
    private int wdeqIdDettQuest = DefaultValues.INT_VAL;
    //Original name: WDEQ-ID-QUEST
    private int wdeqIdQuest = DefaultValues.INT_VAL;
    //Original name: WDEQ-ID-MOVI-CRZ
    private int wdeqIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WDEQ-ID-MOVI-CHIU
    private WdeqIdMoviChiu wdeqIdMoviChiu = new WdeqIdMoviChiu();
    //Original name: WDEQ-DT-INI-EFF
    private int wdeqDtIniEff = DefaultValues.INT_VAL;
    //Original name: WDEQ-DT-END-EFF
    private int wdeqDtEndEff = DefaultValues.INT_VAL;
    //Original name: WDEQ-COD-COMP-ANIA
    private int wdeqCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WDEQ-COD-QUEST
    private String wdeqCodQuest = DefaultValues.stringVal(Len.WDEQ_COD_QUEST);
    //Original name: WDEQ-COD-DOM
    private String wdeqCodDom = DefaultValues.stringVal(Len.WDEQ_COD_DOM);
    //Original name: WDEQ-COD-DOM-COLLG
    private String wdeqCodDomCollg = DefaultValues.stringVal(Len.WDEQ_COD_DOM_COLLG);
    //Original name: WDEQ-RISP-NUM
    private WdeqRispNum wdeqRispNum = new WdeqRispNum();
    //Original name: WDEQ-RISP-FL
    private char wdeqRispFl = DefaultValues.CHAR_VAL;
    //Original name: WDEQ-RISP-TXT
    private String wdeqRispTxt = DefaultValues.stringVal(Len.WDEQ_RISP_TXT);
    //Original name: WDEQ-RISP-TS
    private WdeqRispTs wdeqRispTs = new WdeqRispTs();
    //Original name: WDEQ-RISP-IMP
    private WdeqRispImp wdeqRispImp = new WdeqRispImp();
    //Original name: WDEQ-RISP-PC
    private WdeqRispPc wdeqRispPc = new WdeqRispPc();
    //Original name: WDEQ-RISP-DT
    private WdeqRispDt wdeqRispDt = new WdeqRispDt();
    //Original name: WDEQ-RISP-KEY
    private String wdeqRispKey = DefaultValues.stringVal(Len.WDEQ_RISP_KEY);
    //Original name: WDEQ-TP-RISP
    private String wdeqTpRisp = DefaultValues.stringVal(Len.WDEQ_TP_RISP);
    //Original name: WDEQ-ID-COMP-QUEST
    private WdeqIdCompQuest wdeqIdCompQuest = new WdeqIdCompQuest();
    //Original name: WDEQ-VAL-RISP
    private String wdeqValRisp = DefaultValues.stringVal(Len.WDEQ_VAL_RISP);
    //Original name: WDEQ-DS-RIGA
    private long wdeqDsRiga = DefaultValues.LONG_VAL;
    //Original name: WDEQ-DS-OPER-SQL
    private char wdeqDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WDEQ-DS-VER
    private int wdeqDsVer = DefaultValues.INT_VAL;
    //Original name: WDEQ-DS-TS-INI-CPTZ
    private long wdeqDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WDEQ-DS-TS-END-CPTZ
    private long wdeqDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WDEQ-DS-UTENTE
    private String wdeqDsUtente = DefaultValues.stringVal(Len.WDEQ_DS_UTENTE);
    //Original name: WDEQ-DS-STATO-ELAB
    private char wdeqDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public String getDatiFormatted() {
        return MarshalByteExt.bufferToStr(getDatiBytes());
    }

    public byte[] getDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wdeqIdDettQuest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDEQ_ID_DETT_QUEST, 0);
        position += Len.WDEQ_ID_DETT_QUEST;
        wdeqIdQuest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDEQ_ID_QUEST, 0);
        position += Len.WDEQ_ID_QUEST;
        wdeqIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDEQ_ID_MOVI_CRZ, 0);
        position += Len.WDEQ_ID_MOVI_CRZ;
        wdeqIdMoviChiu.setWdeqIdMoviChiuFromBuffer(buffer, position);
        position += WdeqIdMoviChiu.Len.WDEQ_ID_MOVI_CHIU;
        wdeqDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDEQ_DT_INI_EFF, 0);
        position += Len.WDEQ_DT_INI_EFF;
        wdeqDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDEQ_DT_END_EFF, 0);
        position += Len.WDEQ_DT_END_EFF;
        wdeqCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDEQ_COD_COMP_ANIA, 0);
        position += Len.WDEQ_COD_COMP_ANIA;
        wdeqCodQuest = MarshalByte.readString(buffer, position, Len.WDEQ_COD_QUEST);
        position += Len.WDEQ_COD_QUEST;
        wdeqCodDom = MarshalByte.readString(buffer, position, Len.WDEQ_COD_DOM);
        position += Len.WDEQ_COD_DOM;
        wdeqCodDomCollg = MarshalByte.readString(buffer, position, Len.WDEQ_COD_DOM_COLLG);
        position += Len.WDEQ_COD_DOM_COLLG;
        wdeqRispNum.setWdeqRispNumFromBuffer(buffer, position);
        position += WdeqRispNum.Len.WDEQ_RISP_NUM;
        wdeqRispFl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdeqRispTxt = MarshalByte.readString(buffer, position, Len.WDEQ_RISP_TXT);
        position += Len.WDEQ_RISP_TXT;
        wdeqRispTs.setWdeqRispTsFromBuffer(buffer, position);
        position += WdeqRispTs.Len.WDEQ_RISP_TS;
        wdeqRispImp.setWdeqRispImpFromBuffer(buffer, position);
        position += WdeqRispImp.Len.WDEQ_RISP_IMP;
        wdeqRispPc.setWdeqRispPcFromBuffer(buffer, position);
        position += WdeqRispPc.Len.WDEQ_RISP_PC;
        wdeqRispDt.setWdeqRispDtFromBuffer(buffer, position);
        position += WdeqRispDt.Len.WDEQ_RISP_DT;
        wdeqRispKey = MarshalByte.readString(buffer, position, Len.WDEQ_RISP_KEY);
        position += Len.WDEQ_RISP_KEY;
        wdeqTpRisp = MarshalByte.readString(buffer, position, Len.WDEQ_TP_RISP);
        position += Len.WDEQ_TP_RISP;
        wdeqIdCompQuest.setWdeqIdCompQuestFromBuffer(buffer, position);
        position += WdeqIdCompQuest.Len.WDEQ_ID_COMP_QUEST;
        wdeqValRisp = MarshalByte.readString(buffer, position, Len.WDEQ_VAL_RISP);
        position += Len.WDEQ_VAL_RISP;
        wdeqDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDEQ_DS_RIGA, 0);
        position += Len.WDEQ_DS_RIGA;
        wdeqDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wdeqDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WDEQ_DS_VER, 0);
        position += Len.WDEQ_DS_VER;
        wdeqDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDEQ_DS_TS_INI_CPTZ, 0);
        position += Len.WDEQ_DS_TS_INI_CPTZ;
        wdeqDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WDEQ_DS_TS_END_CPTZ, 0);
        position += Len.WDEQ_DS_TS_END_CPTZ;
        wdeqDsUtente = MarshalByte.readString(buffer, position, Len.WDEQ_DS_UTENTE);
        position += Len.WDEQ_DS_UTENTE;
        wdeqDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wdeqIdDettQuest, Len.Int.WDEQ_ID_DETT_QUEST, 0);
        position += Len.WDEQ_ID_DETT_QUEST;
        MarshalByte.writeIntAsPacked(buffer, position, wdeqIdQuest, Len.Int.WDEQ_ID_QUEST, 0);
        position += Len.WDEQ_ID_QUEST;
        MarshalByte.writeIntAsPacked(buffer, position, wdeqIdMoviCrz, Len.Int.WDEQ_ID_MOVI_CRZ, 0);
        position += Len.WDEQ_ID_MOVI_CRZ;
        wdeqIdMoviChiu.getWdeqIdMoviChiuAsBuffer(buffer, position);
        position += WdeqIdMoviChiu.Len.WDEQ_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wdeqDtIniEff, Len.Int.WDEQ_DT_INI_EFF, 0);
        position += Len.WDEQ_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wdeqDtEndEff, Len.Int.WDEQ_DT_END_EFF, 0);
        position += Len.WDEQ_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wdeqCodCompAnia, Len.Int.WDEQ_COD_COMP_ANIA, 0);
        position += Len.WDEQ_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, wdeqCodQuest, Len.WDEQ_COD_QUEST);
        position += Len.WDEQ_COD_QUEST;
        MarshalByte.writeString(buffer, position, wdeqCodDom, Len.WDEQ_COD_DOM);
        position += Len.WDEQ_COD_DOM;
        MarshalByte.writeString(buffer, position, wdeqCodDomCollg, Len.WDEQ_COD_DOM_COLLG);
        position += Len.WDEQ_COD_DOM_COLLG;
        wdeqRispNum.getWdeqRispNumAsBuffer(buffer, position);
        position += WdeqRispNum.Len.WDEQ_RISP_NUM;
        MarshalByte.writeChar(buffer, position, wdeqRispFl);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wdeqRispTxt, Len.WDEQ_RISP_TXT);
        position += Len.WDEQ_RISP_TXT;
        wdeqRispTs.getWdeqRispTsAsBuffer(buffer, position);
        position += WdeqRispTs.Len.WDEQ_RISP_TS;
        wdeqRispImp.getWdeqRispImpAsBuffer(buffer, position);
        position += WdeqRispImp.Len.WDEQ_RISP_IMP;
        wdeqRispPc.getWdeqRispPcAsBuffer(buffer, position);
        position += WdeqRispPc.Len.WDEQ_RISP_PC;
        wdeqRispDt.getWdeqRispDtAsBuffer(buffer, position);
        position += WdeqRispDt.Len.WDEQ_RISP_DT;
        MarshalByte.writeString(buffer, position, wdeqRispKey, Len.WDEQ_RISP_KEY);
        position += Len.WDEQ_RISP_KEY;
        MarshalByte.writeString(buffer, position, wdeqTpRisp, Len.WDEQ_TP_RISP);
        position += Len.WDEQ_TP_RISP;
        wdeqIdCompQuest.getWdeqIdCompQuestAsBuffer(buffer, position);
        position += WdeqIdCompQuest.Len.WDEQ_ID_COMP_QUEST;
        MarshalByte.writeString(buffer, position, wdeqValRisp, Len.WDEQ_VAL_RISP);
        position += Len.WDEQ_VAL_RISP;
        MarshalByte.writeLongAsPacked(buffer, position, wdeqDsRiga, Len.Int.WDEQ_DS_RIGA, 0);
        position += Len.WDEQ_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wdeqDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wdeqDsVer, Len.Int.WDEQ_DS_VER, 0);
        position += Len.WDEQ_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wdeqDsTsIniCptz, Len.Int.WDEQ_DS_TS_INI_CPTZ, 0);
        position += Len.WDEQ_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wdeqDsTsEndCptz, Len.Int.WDEQ_DS_TS_END_CPTZ, 0);
        position += Len.WDEQ_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wdeqDsUtente, Len.WDEQ_DS_UTENTE);
        position += Len.WDEQ_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wdeqDsStatoElab);
        return buffer;
    }

    public void initDatiSpaces() {
        wdeqIdDettQuest = Types.INVALID_INT_VAL;
        wdeqIdQuest = Types.INVALID_INT_VAL;
        wdeqIdMoviCrz = Types.INVALID_INT_VAL;
        wdeqIdMoviChiu.initWdeqIdMoviChiuSpaces();
        wdeqDtIniEff = Types.INVALID_INT_VAL;
        wdeqDtEndEff = Types.INVALID_INT_VAL;
        wdeqCodCompAnia = Types.INVALID_INT_VAL;
        wdeqCodQuest = "";
        wdeqCodDom = "";
        wdeqCodDomCollg = "";
        wdeqRispNum.initWdeqRispNumSpaces();
        wdeqRispFl = Types.SPACE_CHAR;
        wdeqRispTxt = "";
        wdeqRispTs.initWdeqRispTsSpaces();
        wdeqRispImp.initWdeqRispImpSpaces();
        wdeqRispPc.initWdeqRispPcSpaces();
        wdeqRispDt.initWdeqRispDtSpaces();
        wdeqRispKey = "";
        wdeqTpRisp = "";
        wdeqIdCompQuest.initWdeqIdCompQuestSpaces();
        wdeqValRisp = "";
        wdeqDsRiga = Types.INVALID_LONG_VAL;
        wdeqDsOperSql = Types.SPACE_CHAR;
        wdeqDsVer = Types.INVALID_INT_VAL;
        wdeqDsTsIniCptz = Types.INVALID_LONG_VAL;
        wdeqDsTsEndCptz = Types.INVALID_LONG_VAL;
        wdeqDsUtente = "";
        wdeqDsStatoElab = Types.SPACE_CHAR;
    }

    public void setWdeqIdDettQuest(int wdeqIdDettQuest) {
        this.wdeqIdDettQuest = wdeqIdDettQuest;
    }

    public int getWdeqIdDettQuest() {
        return this.wdeqIdDettQuest;
    }

    public void setWdeqIdQuest(int wdeqIdQuest) {
        this.wdeqIdQuest = wdeqIdQuest;
    }

    public int getWdeqIdQuest() {
        return this.wdeqIdQuest;
    }

    public void setWdeqIdMoviCrz(int wdeqIdMoviCrz) {
        this.wdeqIdMoviCrz = wdeqIdMoviCrz;
    }

    public int getWdeqIdMoviCrz() {
        return this.wdeqIdMoviCrz;
    }

    public void setWdeqDtIniEff(int wdeqDtIniEff) {
        this.wdeqDtIniEff = wdeqDtIniEff;
    }

    public int getWdeqDtIniEff() {
        return this.wdeqDtIniEff;
    }

    public void setWdeqDtEndEff(int wdeqDtEndEff) {
        this.wdeqDtEndEff = wdeqDtEndEff;
    }

    public int getWdeqDtEndEff() {
        return this.wdeqDtEndEff;
    }

    public void setWdeqCodCompAnia(int wdeqCodCompAnia) {
        this.wdeqCodCompAnia = wdeqCodCompAnia;
    }

    public int getWdeqCodCompAnia() {
        return this.wdeqCodCompAnia;
    }

    public void setWdeqCodQuest(String wdeqCodQuest) {
        this.wdeqCodQuest = Functions.subString(wdeqCodQuest, Len.WDEQ_COD_QUEST);
    }

    public String getWdeqCodQuest() {
        return this.wdeqCodQuest;
    }

    public void setWdeqCodDom(String wdeqCodDom) {
        this.wdeqCodDom = Functions.subString(wdeqCodDom, Len.WDEQ_COD_DOM);
    }

    public String getWdeqCodDom() {
        return this.wdeqCodDom;
    }

    public void setWdeqCodDomCollg(String wdeqCodDomCollg) {
        this.wdeqCodDomCollg = Functions.subString(wdeqCodDomCollg, Len.WDEQ_COD_DOM_COLLG);
    }

    public String getWdeqCodDomCollg() {
        return this.wdeqCodDomCollg;
    }

    public void setWdeqRispFl(char wdeqRispFl) {
        this.wdeqRispFl = wdeqRispFl;
    }

    public char getWdeqRispFl() {
        return this.wdeqRispFl;
    }

    public void setWdeqRispTxt(String wdeqRispTxt) {
        this.wdeqRispTxt = Functions.subString(wdeqRispTxt, Len.WDEQ_RISP_TXT);
    }

    public String getWdeqRispTxt() {
        return this.wdeqRispTxt;
    }

    public void setWdeqRispKey(String wdeqRispKey) {
        this.wdeqRispKey = Functions.subString(wdeqRispKey, Len.WDEQ_RISP_KEY);
    }

    public String getWdeqRispKey() {
        return this.wdeqRispKey;
    }

    public void setWdeqTpRisp(String wdeqTpRisp) {
        this.wdeqTpRisp = Functions.subString(wdeqTpRisp, Len.WDEQ_TP_RISP);
    }

    public String getWdeqTpRisp() {
        return this.wdeqTpRisp;
    }

    public void setWdeqValRisp(String wdeqValRisp) {
        this.wdeqValRisp = Functions.subString(wdeqValRisp, Len.WDEQ_VAL_RISP);
    }

    public String getWdeqValRisp() {
        return this.wdeqValRisp;
    }

    public void setWdeqDsRiga(long wdeqDsRiga) {
        this.wdeqDsRiga = wdeqDsRiga;
    }

    public long getWdeqDsRiga() {
        return this.wdeqDsRiga;
    }

    public void setWdeqDsOperSql(char wdeqDsOperSql) {
        this.wdeqDsOperSql = wdeqDsOperSql;
    }

    public char getWdeqDsOperSql() {
        return this.wdeqDsOperSql;
    }

    public void setWdeqDsVer(int wdeqDsVer) {
        this.wdeqDsVer = wdeqDsVer;
    }

    public int getWdeqDsVer() {
        return this.wdeqDsVer;
    }

    public void setWdeqDsTsIniCptz(long wdeqDsTsIniCptz) {
        this.wdeqDsTsIniCptz = wdeqDsTsIniCptz;
    }

    public long getWdeqDsTsIniCptz() {
        return this.wdeqDsTsIniCptz;
    }

    public void setWdeqDsTsEndCptz(long wdeqDsTsEndCptz) {
        this.wdeqDsTsEndCptz = wdeqDsTsEndCptz;
    }

    public long getWdeqDsTsEndCptz() {
        return this.wdeqDsTsEndCptz;
    }

    public void setWdeqDsUtente(String wdeqDsUtente) {
        this.wdeqDsUtente = Functions.subString(wdeqDsUtente, Len.WDEQ_DS_UTENTE);
    }

    public String getWdeqDsUtente() {
        return this.wdeqDsUtente;
    }

    public void setWdeqDsStatoElab(char wdeqDsStatoElab) {
        this.wdeqDsStatoElab = wdeqDsStatoElab;
    }

    public char getWdeqDsStatoElab() {
        return this.wdeqDsStatoElab;
    }

    public WdeqIdCompQuest getWdeqIdCompQuest() {
        return wdeqIdCompQuest;
    }

    public WdeqIdMoviChiu getWdeqIdMoviChiu() {
        return wdeqIdMoviChiu;
    }

    public WdeqRispDt getWdeqRispDt() {
        return wdeqRispDt;
    }

    public WdeqRispImp getWdeqRispImp() {
        return wdeqRispImp;
    }

    public WdeqRispNum getWdeqRispNum() {
        return wdeqRispNum;
    }

    public WdeqRispPc getWdeqRispPc() {
        return wdeqRispPc;
    }

    public WdeqRispTs getWdeqRispTs() {
        return wdeqRispTs;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDEQ_ID_DETT_QUEST = 5;
        public static final int WDEQ_ID_QUEST = 5;
        public static final int WDEQ_ID_MOVI_CRZ = 5;
        public static final int WDEQ_DT_INI_EFF = 5;
        public static final int WDEQ_DT_END_EFF = 5;
        public static final int WDEQ_COD_COMP_ANIA = 3;
        public static final int WDEQ_COD_QUEST = 20;
        public static final int WDEQ_COD_DOM = 20;
        public static final int WDEQ_COD_DOM_COLLG = 20;
        public static final int WDEQ_RISP_FL = 1;
        public static final int WDEQ_RISP_TXT = 100;
        public static final int WDEQ_RISP_KEY = 20;
        public static final int WDEQ_TP_RISP = 2;
        public static final int WDEQ_VAL_RISP = 250;
        public static final int WDEQ_DS_RIGA = 6;
        public static final int WDEQ_DS_OPER_SQL = 1;
        public static final int WDEQ_DS_VER = 5;
        public static final int WDEQ_DS_TS_INI_CPTZ = 10;
        public static final int WDEQ_DS_TS_END_CPTZ = 10;
        public static final int WDEQ_DS_UTENTE = 20;
        public static final int WDEQ_DS_STATO_ELAB = 1;
        public static final int DATI = WDEQ_ID_DETT_QUEST + WDEQ_ID_QUEST + WDEQ_ID_MOVI_CRZ + WdeqIdMoviChiu.Len.WDEQ_ID_MOVI_CHIU + WDEQ_DT_INI_EFF + WDEQ_DT_END_EFF + WDEQ_COD_COMP_ANIA + WDEQ_COD_QUEST + WDEQ_COD_DOM + WDEQ_COD_DOM_COLLG + WdeqRispNum.Len.WDEQ_RISP_NUM + WDEQ_RISP_FL + WDEQ_RISP_TXT + WdeqRispTs.Len.WDEQ_RISP_TS + WdeqRispImp.Len.WDEQ_RISP_IMP + WdeqRispPc.Len.WDEQ_RISP_PC + WdeqRispDt.Len.WDEQ_RISP_DT + WDEQ_RISP_KEY + WDEQ_TP_RISP + WdeqIdCompQuest.Len.WDEQ_ID_COMP_QUEST + WDEQ_VAL_RISP + WDEQ_DS_RIGA + WDEQ_DS_OPER_SQL + WDEQ_DS_VER + WDEQ_DS_TS_INI_CPTZ + WDEQ_DS_TS_END_CPTZ + WDEQ_DS_UTENTE + WDEQ_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDEQ_ID_DETT_QUEST = 9;
            public static final int WDEQ_ID_QUEST = 9;
            public static final int WDEQ_ID_MOVI_CRZ = 9;
            public static final int WDEQ_DT_INI_EFF = 8;
            public static final int WDEQ_DT_END_EFF = 8;
            public static final int WDEQ_COD_COMP_ANIA = 5;
            public static final int WDEQ_DS_RIGA = 10;
            public static final int WDEQ_DS_VER = 9;
            public static final int WDEQ_DS_TS_INI_CPTZ = 18;
            public static final int WDEQ_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
