package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LDBVD511<br>
 * Variable: LDBVD511 from copybook LDBVD511<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ldbvd511 {

    //==== PROPERTIES ====
    //Original name: LDBVD511-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LDBVD511-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LDBVD511-TP-STAT-BUS
    private String tpStatBus = DefaultValues.stringVal(Len.TP_STAT_BUS);
    //Original name: LDBVD511-TP-CAUS
    private String tpCaus = DefaultValues.stringVal(Len.TP_CAUS);
    //Original name: LDBVD511-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);

    //==== METHODS ====
    public void setLdbvd511Formatted(String data) {
        byte[] buffer = new byte[Len.LDBVD511];
        MarshalByte.writeString(buffer, 1, data, Len.LDBVD511);
        setLdbvd511Bytes(buffer, 1);
    }

    public String getLdbvd511Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvd511Bytes());
    }

    public byte[] getLdbvd511Bytes() {
        byte[] buffer = new byte[Len.LDBVD511];
        return getLdbvd511Bytes(buffer, 1);
    }

    public void setLdbvd511Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        tpStatBus = MarshalByte.readString(buffer, position, Len.TP_STAT_BUS);
        position += Len.TP_STAT_BUS;
        tpCaus = MarshalByte.readString(buffer, position, Len.TP_CAUS);
        position += Len.TP_CAUS;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
    }

    public byte[] getLdbvd511Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeString(buffer, position, tpStatBus, Len.TP_STAT_BUS);
        position += Len.TP_STAT_BUS;
        MarshalByte.writeString(buffer, position, tpCaus, Len.TP_CAUS);
        position += Len.TP_CAUS;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setTpStatBus(String tpStatBus) {
        this.tpStatBus = Functions.subString(tpStatBus, Len.TP_STAT_BUS);
    }

    public String getTpStatBus() {
        return this.tpStatBus;
    }

    public void setTpCaus(String tpCaus) {
        this.tpCaus = Functions.subString(tpCaus, Len.TP_CAUS);
    }

    public String getTpCaus() {
        return this.tpCaus;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_STAT_BUS = 2;
        public static final int TP_CAUS = 2;
        public static final int TP_OGG = 2;
        public static final int ID_POLI = 5;
        public static final int ID_ADES = 5;
        public static final int LDBVD511 = ID_POLI + ID_ADES + TP_STAT_BUS + TP_CAUS + TP_OGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
