package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Idso0011FlagCodaTs;
import it.accenture.jnais.ws.enums.Idso0011ReturnCode;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.occurs.Idso0011ElementiBuffer;

/**Original name: IDSO0011-AREA<br>
 * Variable: IDSO0011-AREA from copybook IDSO0011<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idso0011AreaLdbs1390 {

    //==== PROPERTIES ====
    public static final int ELEMENTI_BUFFER_MAXOCCURS = 30;
    //Original name: IDSO0011-MAX
    private String max = DefaultValues.stringVal(Len.MAX);
    //Original name: IDSO0011-ELEMENTI-BUFFER
    private Idso0011ElementiBuffer[] elementiBuffer = new Idso0011ElementiBuffer[ELEMENTI_BUFFER_MAXOCCURS];
    //Original name: IDSO0011-FLAG-CODA-TS
    private Idso0011FlagCodaTs flagCodaTs = new Idso0011FlagCodaTs();
    /**Original name: IDSO0011-RETURN-CODE<br>
	 * <pre> --  CAMPI ERRORI
	 *       COPY IDSV0006 REPLACING ==IDSO0011== BY ==IDSO0011==.</pre>*/
    private Idso0011ReturnCode returnCode = new Idso0011ReturnCode();
    /**Original name: IDSO0011-SQLCODE-SIGNED<br>
	 * <pre>   fine COPY IDSV0004</pre>*/
    private Idso0011SqlcodeSigned sqlcodeSigned = new Idso0011SqlcodeSigned();
    //Original name: IDSO0011-SQLCODE
    private String sqlcode = DefaultValues.stringVal(Len.SQLCODE);
    //Original name: IDSO0011-CAMPI-ESITO
    private Idsv0003CampiEsito campiEsito = new Idsv0003CampiEsito();

    //==== CONSTRUCTORS ====
    public Idso0011AreaLdbs1390() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int elementiBufferIdx = 1; elementiBufferIdx <= ELEMENTI_BUFFER_MAXOCCURS; elementiBufferIdx++) {
            elementiBuffer[elementiBufferIdx - 1] = new Idso0011ElementiBuffer();
        }
    }

    public void setIdso0011AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        max = MarshalByte.readFixedString(buffer, position, Len.MAX);
        position += Len.MAX;
        setComposizioneBufferBytes(buffer, position);
        position += Len.COMPOSIZIONE_BUFFER;
        flagCodaTs.setFlagCodaTs(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        returnCode.setReturnCode(MarshalByte.readString(buffer, position, Idso0011ReturnCode.Len.RETURN_CODE));
        position += Idso0011ReturnCode.Len.RETURN_CODE;
        sqlcodeSigned.setSqlcodeSigned(MarshalByte.readInt(buffer, position, Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED));
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        sqlcode = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.SQLCODE), Len.SQLCODE);
        position += Len.SQLCODE;
        campiEsito.setCampiEsitoBytes(buffer, position);
    }

    public byte[] getIdso0011AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, max, Len.MAX);
        position += Len.MAX;
        getComposizioneBufferBytes(buffer, position);
        position += Len.COMPOSIZIONE_BUFFER;
        MarshalByte.writeChar(buffer, position, flagCodaTs.getFlagCodaTs());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, returnCode.getReturnCode(), Idso0011ReturnCode.Len.RETURN_CODE);
        position += Idso0011ReturnCode.Len.RETURN_CODE;
        MarshalByte.writeInt(buffer, position, sqlcodeSigned.getSqlcodeSigned(), Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED);
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
        position += Len.SQLCODE;
        campiEsito.getCampiEsitoBytes(buffer, position);
        return buffer;
    }

    public void setComposizioneBufferBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ELEMENTI_BUFFER_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                elementiBuffer[idx - 1].setElementiBufferBytes(buffer, position);
                position += Idso0011ElementiBuffer.Len.ELEMENTI_BUFFER;
            }
            else {
                elementiBuffer[idx - 1].initElementiBufferSpaces();
                position += Idso0011ElementiBuffer.Len.ELEMENTI_BUFFER;
            }
        }
    }

    public byte[] getComposizioneBufferBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ELEMENTI_BUFFER_MAXOCCURS; idx++) {
            elementiBuffer[idx - 1].getElementiBufferBytes(buffer, position);
            position += Idso0011ElementiBuffer.Len.ELEMENTI_BUFFER;
        }
        return buffer;
    }

    public Idsv0003CampiEsito getCampiEsito() {
        return campiEsito;
    }

    public Idso0011ReturnCode getReturnCode() {
        return returnCode;
    }

    public Idso0011SqlcodeSigned getSqlcodeSigned() {
        return sqlcodeSigned;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MAX = 2;
        public static final int COMPOSIZIONE_BUFFER = Idso0011AreaLdbs1390.ELEMENTI_BUFFER_MAXOCCURS * Idso0011ElementiBuffer.Len.ELEMENTI_BUFFER;
        public static final int SQLCODE = 9;
        public static final int IDSO0011_AREA = MAX + COMPOSIZIONE_BUFFER + Idso0011FlagCodaTs.Len.FLAG_CODA_TS + Idso0011ReturnCode.Len.RETURN_CODE + Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED + SQLCODE + Idsv0003CampiEsito.Len.CAMPI_ESITO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
