package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.ws.redefines.WtgaAbbAnnuUlt;
import it.accenture.jnais.ws.redefines.WtgaAbbTotIni;
import it.accenture.jnais.ws.redefines.WtgaAbbTotUlt;
import it.accenture.jnais.ws.redefines.WtgaAcqExp;
import it.accenture.jnais.ws.redefines.WtgaAlqCommisInter;
import it.accenture.jnais.ws.redefines.WtgaAlqProvAcq;
import it.accenture.jnais.ws.redefines.WtgaAlqProvInc;
import it.accenture.jnais.ws.redefines.WtgaAlqProvRicor;
import it.accenture.jnais.ws.redefines.WtgaAlqRemunAss;
import it.accenture.jnais.ws.redefines.WtgaAlqScon;
import it.accenture.jnais.ws.redefines.WtgaBnsGiaLiqto;
import it.accenture.jnais.ws.redefines.WtgaCommisGest;
import it.accenture.jnais.ws.redefines.WtgaCommisInter;
import it.accenture.jnais.ws.redefines.WtgaCosRunAssva;
import it.accenture.jnais.ws.redefines.WtgaCosRunAssvaIdc;
import it.accenture.jnais.ws.redefines.WtgaCptInOpzRivto;
import it.accenture.jnais.ws.redefines.WtgaCptMinScad;
import it.accenture.jnais.ws.redefines.WtgaCptRshMor;
import it.accenture.jnais.ws.redefines.WtgaDtEffStab;
import it.accenture.jnais.ws.redefines.WtgaDtEmis;
import it.accenture.jnais.ws.redefines.WtgaDtIniValTar;
import it.accenture.jnais.ws.redefines.WtgaDtScad;
import it.accenture.jnais.ws.redefines.WtgaDtUltAdegPrePr;
import it.accenture.jnais.ws.redefines.WtgaDtVldtProd;
import it.accenture.jnais.ws.redefines.WtgaDurAa;
import it.accenture.jnais.ws.redefines.WtgaDurAbb;
import it.accenture.jnais.ws.redefines.WtgaDurGg;
import it.accenture.jnais.ws.redefines.WtgaDurMm;
import it.accenture.jnais.ws.redefines.WtgaEtaAa1oAssto;
import it.accenture.jnais.ws.redefines.WtgaEtaAa2oAssto;
import it.accenture.jnais.ws.redefines.WtgaEtaAa3oAssto;
import it.accenture.jnais.ws.redefines.WtgaEtaMm1oAssto;
import it.accenture.jnais.ws.redefines.WtgaEtaMm2oAssto;
import it.accenture.jnais.ws.redefines.WtgaEtaMm3oAssto;
import it.accenture.jnais.ws.redefines.WtgaIdMoviChiu;
import it.accenture.jnais.ws.redefines.WtgaImpAder;
import it.accenture.jnais.ws.redefines.WtgaImpAltSopr;
import it.accenture.jnais.ws.redefines.WtgaImpAz;
import it.accenture.jnais.ws.redefines.WtgaImpbCommisInter;
import it.accenture.jnais.ws.redefines.WtgaImpBns;
import it.accenture.jnais.ws.redefines.WtgaImpBnsAntic;
import it.accenture.jnais.ws.redefines.WtgaImpbProvAcq;
import it.accenture.jnais.ws.redefines.WtgaImpbProvInc;
import it.accenture.jnais.ws.redefines.WtgaImpbProvRicor;
import it.accenture.jnais.ws.redefines.WtgaImpbRemunAss;
import it.accenture.jnais.ws.redefines.WtgaImpbVisEnd2000;
import it.accenture.jnais.ws.redefines.WtgaImpCarAcq;
import it.accenture.jnais.ws.redefines.WtgaImpCarGest;
import it.accenture.jnais.ws.redefines.WtgaImpCarInc;
import it.accenture.jnais.ws.redefines.WtgaImpScon;
import it.accenture.jnais.ws.redefines.WtgaImpSoprProf;
import it.accenture.jnais.ws.redefines.WtgaImpSoprSan;
import it.accenture.jnais.ws.redefines.WtgaImpSoprSpo;
import it.accenture.jnais.ws.redefines.WtgaImpSoprTec;
import it.accenture.jnais.ws.redefines.WtgaImpTfr;
import it.accenture.jnais.ws.redefines.WtgaImpTfrStrc;
import it.accenture.jnais.ws.redefines.WtgaImpTrasfe;
import it.accenture.jnais.ws.redefines.WtgaImpVolo;
import it.accenture.jnais.ws.redefines.WtgaIncrPre;
import it.accenture.jnais.ws.redefines.WtgaIncrPrstz;
import it.accenture.jnais.ws.redefines.WtgaIntrMora;
import it.accenture.jnais.ws.redefines.WtgaManfeeAntic;
import it.accenture.jnais.ws.redefines.WtgaManfeeRicor;
import it.accenture.jnais.ws.redefines.WtgaMatuEnd2000;
import it.accenture.jnais.ws.redefines.WtgaMinGarto;
import it.accenture.jnais.ws.redefines.WtgaMinTrnut;
import it.accenture.jnais.ws.redefines.WtgaNumGgRival;
import it.accenture.jnais.ws.redefines.WtgaOldTsTec;
import it.accenture.jnais.ws.redefines.WtgaPcCommisGest;
import it.accenture.jnais.ws.redefines.WtgaPcIntrRiat;
import it.accenture.jnais.ws.redefines.WtgaPcRetr;
import it.accenture.jnais.ws.redefines.WtgaPcRipPre;
import it.accenture.jnais.ws.redefines.WtgaPreAttDiTrch;
import it.accenture.jnais.ws.redefines.WtgaPreCasoMor;
import it.accenture.jnais.ws.redefines.WtgaPreIniNet;
import it.accenture.jnais.ws.redefines.WtgaPreInvrioIni;
import it.accenture.jnais.ws.redefines.WtgaPreInvrioUlt;
import it.accenture.jnais.ws.redefines.WtgaPreLrd;
import it.accenture.jnais.ws.redefines.WtgaPrePattuito;
import it.accenture.jnais.ws.redefines.WtgaPrePpIni;
import it.accenture.jnais.ws.redefines.WtgaPrePpUlt;
import it.accenture.jnais.ws.redefines.WtgaPreRivto;
import it.accenture.jnais.ws.redefines.WtgaPreStab;
import it.accenture.jnais.ws.redefines.WtgaPreTariIni;
import it.accenture.jnais.ws.redefines.WtgaPreTariUlt;
import it.accenture.jnais.ws.redefines.WtgaPreUniRivto;
import it.accenture.jnais.ws.redefines.WtgaProv1aaAcq;
import it.accenture.jnais.ws.redefines.WtgaProv2aaAcq;
import it.accenture.jnais.ws.redefines.WtgaProvInc;
import it.accenture.jnais.ws.redefines.WtgaProvRicor;
import it.accenture.jnais.ws.redefines.WtgaPrstzAggIni;
import it.accenture.jnais.ws.redefines.WtgaPrstzAggUlt;
import it.accenture.jnais.ws.redefines.WtgaPrstzIni;
import it.accenture.jnais.ws.redefines.WtgaPrstzIniNewfis;
import it.accenture.jnais.ws.redefines.WtgaPrstzIniNforz;
import it.accenture.jnais.ws.redefines.WtgaPrstzIniStab;
import it.accenture.jnais.ws.redefines.WtgaPrstzRidIni;
import it.accenture.jnais.ws.redefines.WtgaPrstzUlt;
import it.accenture.jnais.ws.redefines.WtgaRatLrd;
import it.accenture.jnais.ws.redefines.WtgaRemunAss;
import it.accenture.jnais.ws.redefines.WtgaRendtoLrd;
import it.accenture.jnais.ws.redefines.WtgaRendtoRetr;
import it.accenture.jnais.ws.redefines.WtgaRenIniTsTec0;
import it.accenture.jnais.ws.redefines.WtgaRisMat;
import it.accenture.jnais.ws.redefines.WtgaTsRivalFis;
import it.accenture.jnais.ws.redefines.WtgaTsRivalIndiciz;
import it.accenture.jnais.ws.redefines.WtgaTsRivalNet;
import it.accenture.jnais.ws.redefines.WtgaVisEnd2000;
import it.accenture.jnais.ws.redefines.WtgaVisEnd2000Nforz;

/**Original name: WTGA-DATI<br>
 * Variable: WTGA-DATI from copybook LCCVTGA1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WtgaDati implements ICopyable<WtgaDati> {

    //==== PROPERTIES ====
    //Original name: WTGA-ID-TRCH-DI-GAR
    private int wtgaIdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: WTGA-ID-GAR
    private int wtgaIdGar = DefaultValues.INT_VAL;
    //Original name: WTGA-ID-ADES
    private int wtgaIdAdes = DefaultValues.INT_VAL;
    //Original name: WTGA-ID-POLI
    private int wtgaIdPoli = DefaultValues.INT_VAL;
    //Original name: WTGA-ID-MOVI-CRZ
    private int wtgaIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WTGA-ID-MOVI-CHIU
    private WtgaIdMoviChiu wtgaIdMoviChiu = new WtgaIdMoviChiu();
    //Original name: WTGA-DT-INI-EFF
    private int wtgaDtIniEff = DefaultValues.INT_VAL;
    //Original name: WTGA-DT-END-EFF
    private int wtgaDtEndEff = DefaultValues.INT_VAL;
    //Original name: WTGA-COD-COMP-ANIA
    private int wtgaCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WTGA-DT-DECOR
    private int wtgaDtDecor = DefaultValues.INT_VAL;
    //Original name: WTGA-DT-SCAD
    private WtgaDtScad wtgaDtScad = new WtgaDtScad();
    //Original name: WTGA-IB-OGG
    private String wtgaIbOgg = DefaultValues.stringVal(Len.WTGA_IB_OGG);
    //Original name: WTGA-TP-RGM-FISC
    private String wtgaTpRgmFisc = DefaultValues.stringVal(Len.WTGA_TP_RGM_FISC);
    //Original name: WTGA-DT-EMIS
    private WtgaDtEmis wtgaDtEmis = new WtgaDtEmis();
    //Original name: WTGA-TP-TRCH
    private String wtgaTpTrch = DefaultValues.stringVal(Len.WTGA_TP_TRCH);
    //Original name: WTGA-DUR-AA
    private WtgaDurAa wtgaDurAa = new WtgaDurAa();
    //Original name: WTGA-DUR-MM
    private WtgaDurMm wtgaDurMm = new WtgaDurMm();
    //Original name: WTGA-DUR-GG
    private WtgaDurGg wtgaDurGg = new WtgaDurGg();
    //Original name: WTGA-PRE-CASO-MOR
    private WtgaPreCasoMor wtgaPreCasoMor = new WtgaPreCasoMor();
    //Original name: WTGA-PC-INTR-RIAT
    private WtgaPcIntrRiat wtgaPcIntrRiat = new WtgaPcIntrRiat();
    //Original name: WTGA-IMP-BNS-ANTIC
    private WtgaImpBnsAntic wtgaImpBnsAntic = new WtgaImpBnsAntic();
    //Original name: WTGA-PRE-INI-NET
    private WtgaPreIniNet wtgaPreIniNet = new WtgaPreIniNet();
    //Original name: WTGA-PRE-PP-INI
    private WtgaPrePpIni wtgaPrePpIni = new WtgaPrePpIni();
    //Original name: WTGA-PRE-PP-ULT
    private WtgaPrePpUlt wtgaPrePpUlt = new WtgaPrePpUlt();
    //Original name: WTGA-PRE-TARI-INI
    private WtgaPreTariIni wtgaPreTariIni = new WtgaPreTariIni();
    //Original name: WTGA-PRE-TARI-ULT
    private WtgaPreTariUlt wtgaPreTariUlt = new WtgaPreTariUlt();
    //Original name: WTGA-PRE-INVRIO-INI
    private WtgaPreInvrioIni wtgaPreInvrioIni = new WtgaPreInvrioIni();
    //Original name: WTGA-PRE-INVRIO-ULT
    private WtgaPreInvrioUlt wtgaPreInvrioUlt = new WtgaPreInvrioUlt();
    //Original name: WTGA-PRE-RIVTO
    private WtgaPreRivto wtgaPreRivto = new WtgaPreRivto();
    //Original name: WTGA-IMP-SOPR-PROF
    private WtgaImpSoprProf wtgaImpSoprProf = new WtgaImpSoprProf();
    //Original name: WTGA-IMP-SOPR-SAN
    private WtgaImpSoprSan wtgaImpSoprSan = new WtgaImpSoprSan();
    //Original name: WTGA-IMP-SOPR-SPO
    private WtgaImpSoprSpo wtgaImpSoprSpo = new WtgaImpSoprSpo();
    //Original name: WTGA-IMP-SOPR-TEC
    private WtgaImpSoprTec wtgaImpSoprTec = new WtgaImpSoprTec();
    //Original name: WTGA-IMP-ALT-SOPR
    private WtgaImpAltSopr wtgaImpAltSopr = new WtgaImpAltSopr();
    //Original name: WTGA-PRE-STAB
    private WtgaPreStab wtgaPreStab = new WtgaPreStab();
    //Original name: WTGA-DT-EFF-STAB
    private WtgaDtEffStab wtgaDtEffStab = new WtgaDtEffStab();
    //Original name: WTGA-TS-RIVAL-FIS
    private WtgaTsRivalFis wtgaTsRivalFis = new WtgaTsRivalFis();
    //Original name: WTGA-TS-RIVAL-INDICIZ
    private WtgaTsRivalIndiciz wtgaTsRivalIndiciz = new WtgaTsRivalIndiciz();
    //Original name: WTGA-OLD-TS-TEC
    private WtgaOldTsTec wtgaOldTsTec = new WtgaOldTsTec();
    //Original name: WTGA-RAT-LRD
    private WtgaRatLrd wtgaRatLrd = new WtgaRatLrd();
    //Original name: WTGA-PRE-LRD
    private WtgaPreLrd wtgaPreLrd = new WtgaPreLrd();
    //Original name: WTGA-PRSTZ-INI
    private WtgaPrstzIni wtgaPrstzIni = new WtgaPrstzIni();
    //Original name: WTGA-PRSTZ-ULT
    private WtgaPrstzUlt wtgaPrstzUlt = new WtgaPrstzUlt();
    //Original name: WTGA-CPT-IN-OPZ-RIVTO
    private WtgaCptInOpzRivto wtgaCptInOpzRivto = new WtgaCptInOpzRivto();
    //Original name: WTGA-PRSTZ-INI-STAB
    private WtgaPrstzIniStab wtgaPrstzIniStab = new WtgaPrstzIniStab();
    //Original name: WTGA-CPT-RSH-MOR
    private WtgaCptRshMor wtgaCptRshMor = new WtgaCptRshMor();
    //Original name: WTGA-PRSTZ-RID-INI
    private WtgaPrstzRidIni wtgaPrstzRidIni = new WtgaPrstzRidIni();
    //Original name: WTGA-FL-CAR-CONT
    private char wtgaFlCarCont = DefaultValues.CHAR_VAL;
    //Original name: WTGA-BNS-GIA-LIQTO
    private WtgaBnsGiaLiqto wtgaBnsGiaLiqto = new WtgaBnsGiaLiqto();
    //Original name: WTGA-IMP-BNS
    private WtgaImpBns wtgaImpBns = new WtgaImpBns();
    //Original name: WTGA-COD-DVS
    private String wtgaCodDvs = DefaultValues.stringVal(Len.WTGA_COD_DVS);
    //Original name: WTGA-PRSTZ-INI-NEWFIS
    private WtgaPrstzIniNewfis wtgaPrstzIniNewfis = new WtgaPrstzIniNewfis();
    //Original name: WTGA-IMP-SCON
    private WtgaImpScon wtgaImpScon = new WtgaImpScon();
    //Original name: WTGA-ALQ-SCON
    private WtgaAlqScon wtgaAlqScon = new WtgaAlqScon();
    //Original name: WTGA-IMP-CAR-ACQ
    private WtgaImpCarAcq wtgaImpCarAcq = new WtgaImpCarAcq();
    //Original name: WTGA-IMP-CAR-INC
    private WtgaImpCarInc wtgaImpCarInc = new WtgaImpCarInc();
    //Original name: WTGA-IMP-CAR-GEST
    private WtgaImpCarGest wtgaImpCarGest = new WtgaImpCarGest();
    //Original name: WTGA-ETA-AA-1O-ASSTO
    private WtgaEtaAa1oAssto wtgaEtaAa1oAssto = new WtgaEtaAa1oAssto();
    //Original name: WTGA-ETA-MM-1O-ASSTO
    private WtgaEtaMm1oAssto wtgaEtaMm1oAssto = new WtgaEtaMm1oAssto();
    //Original name: WTGA-ETA-AA-2O-ASSTO
    private WtgaEtaAa2oAssto wtgaEtaAa2oAssto = new WtgaEtaAa2oAssto();
    //Original name: WTGA-ETA-MM-2O-ASSTO
    private WtgaEtaMm2oAssto wtgaEtaMm2oAssto = new WtgaEtaMm2oAssto();
    //Original name: WTGA-ETA-AA-3O-ASSTO
    private WtgaEtaAa3oAssto wtgaEtaAa3oAssto = new WtgaEtaAa3oAssto();
    //Original name: WTGA-ETA-MM-3O-ASSTO
    private WtgaEtaMm3oAssto wtgaEtaMm3oAssto = new WtgaEtaMm3oAssto();
    //Original name: WTGA-RENDTO-LRD
    private WtgaRendtoLrd wtgaRendtoLrd = new WtgaRendtoLrd();
    //Original name: WTGA-PC-RETR
    private WtgaPcRetr wtgaPcRetr = new WtgaPcRetr();
    //Original name: WTGA-RENDTO-RETR
    private WtgaRendtoRetr wtgaRendtoRetr = new WtgaRendtoRetr();
    //Original name: WTGA-MIN-GARTO
    private WtgaMinGarto wtgaMinGarto = new WtgaMinGarto();
    //Original name: WTGA-MIN-TRNUT
    private WtgaMinTrnut wtgaMinTrnut = new WtgaMinTrnut();
    //Original name: WTGA-PRE-ATT-DI-TRCH
    private WtgaPreAttDiTrch wtgaPreAttDiTrch = new WtgaPreAttDiTrch();
    //Original name: WTGA-MATU-END2000
    private WtgaMatuEnd2000 wtgaMatuEnd2000 = new WtgaMatuEnd2000();
    //Original name: WTGA-ABB-TOT-INI
    private WtgaAbbTotIni wtgaAbbTotIni = new WtgaAbbTotIni();
    //Original name: WTGA-ABB-TOT-ULT
    private WtgaAbbTotUlt wtgaAbbTotUlt = new WtgaAbbTotUlt();
    //Original name: WTGA-ABB-ANNU-ULT
    private WtgaAbbAnnuUlt wtgaAbbAnnuUlt = new WtgaAbbAnnuUlt();
    //Original name: WTGA-DUR-ABB
    private WtgaDurAbb wtgaDurAbb = new WtgaDurAbb();
    //Original name: WTGA-TP-ADEG-ABB
    private char wtgaTpAdegAbb = DefaultValues.CHAR_VAL;
    //Original name: WTGA-MOD-CALC
    private String wtgaModCalc = DefaultValues.stringVal(Len.WTGA_MOD_CALC);
    //Original name: WTGA-IMP-AZ
    private WtgaImpAz wtgaImpAz = new WtgaImpAz();
    //Original name: WTGA-IMP-ADER
    private WtgaImpAder wtgaImpAder = new WtgaImpAder();
    //Original name: WTGA-IMP-TFR
    private WtgaImpTfr wtgaImpTfr = new WtgaImpTfr();
    //Original name: WTGA-IMP-VOLO
    private WtgaImpVolo wtgaImpVolo = new WtgaImpVolo();
    //Original name: WTGA-VIS-END2000
    private WtgaVisEnd2000 wtgaVisEnd2000 = new WtgaVisEnd2000();
    //Original name: WTGA-DT-VLDT-PROD
    private WtgaDtVldtProd wtgaDtVldtProd = new WtgaDtVldtProd();
    //Original name: WTGA-DT-INI-VAL-TAR
    private WtgaDtIniValTar wtgaDtIniValTar = new WtgaDtIniValTar();
    //Original name: WTGA-IMPB-VIS-END2000
    private WtgaImpbVisEnd2000 wtgaImpbVisEnd2000 = new WtgaImpbVisEnd2000();
    //Original name: WTGA-REN-INI-TS-TEC-0
    private WtgaRenIniTsTec0 wtgaRenIniTsTec0 = new WtgaRenIniTsTec0();
    //Original name: WTGA-PC-RIP-PRE
    private WtgaPcRipPre wtgaPcRipPre = new WtgaPcRipPre();
    //Original name: WTGA-FL-IMPORTI-FORZ
    private char wtgaFlImportiForz = DefaultValues.CHAR_VAL;
    //Original name: WTGA-PRSTZ-INI-NFORZ
    private WtgaPrstzIniNforz wtgaPrstzIniNforz = new WtgaPrstzIniNforz();
    //Original name: WTGA-VIS-END2000-NFORZ
    private WtgaVisEnd2000Nforz wtgaVisEnd2000Nforz = new WtgaVisEnd2000Nforz();
    //Original name: WTGA-INTR-MORA
    private WtgaIntrMora wtgaIntrMora = new WtgaIntrMora();
    //Original name: WTGA-MANFEE-ANTIC
    private WtgaManfeeAntic wtgaManfeeAntic = new WtgaManfeeAntic();
    //Original name: WTGA-MANFEE-RICOR
    private WtgaManfeeRicor wtgaManfeeRicor = new WtgaManfeeRicor();
    //Original name: WTGA-PRE-UNI-RIVTO
    private WtgaPreUniRivto wtgaPreUniRivto = new WtgaPreUniRivto();
    //Original name: WTGA-PROV-1AA-ACQ
    private WtgaProv1aaAcq wtgaProv1aaAcq = new WtgaProv1aaAcq();
    //Original name: WTGA-PROV-2AA-ACQ
    private WtgaProv2aaAcq wtgaProv2aaAcq = new WtgaProv2aaAcq();
    //Original name: WTGA-PROV-RICOR
    private WtgaProvRicor wtgaProvRicor = new WtgaProvRicor();
    //Original name: WTGA-PROV-INC
    private WtgaProvInc wtgaProvInc = new WtgaProvInc();
    //Original name: WTGA-ALQ-PROV-ACQ
    private WtgaAlqProvAcq wtgaAlqProvAcq = new WtgaAlqProvAcq();
    //Original name: WTGA-ALQ-PROV-INC
    private WtgaAlqProvInc wtgaAlqProvInc = new WtgaAlqProvInc();
    //Original name: WTGA-ALQ-PROV-RICOR
    private WtgaAlqProvRicor wtgaAlqProvRicor = new WtgaAlqProvRicor();
    //Original name: WTGA-IMPB-PROV-ACQ
    private WtgaImpbProvAcq wtgaImpbProvAcq = new WtgaImpbProvAcq();
    //Original name: WTGA-IMPB-PROV-INC
    private WtgaImpbProvInc wtgaImpbProvInc = new WtgaImpbProvInc();
    //Original name: WTGA-IMPB-PROV-RICOR
    private WtgaImpbProvRicor wtgaImpbProvRicor = new WtgaImpbProvRicor();
    //Original name: WTGA-FL-PROV-FORZ
    private char wtgaFlProvForz = DefaultValues.CHAR_VAL;
    //Original name: WTGA-PRSTZ-AGG-INI
    private WtgaPrstzAggIni wtgaPrstzAggIni = new WtgaPrstzAggIni();
    //Original name: WTGA-INCR-PRE
    private WtgaIncrPre wtgaIncrPre = new WtgaIncrPre();
    //Original name: WTGA-INCR-PRSTZ
    private WtgaIncrPrstz wtgaIncrPrstz = new WtgaIncrPrstz();
    //Original name: WTGA-DT-ULT-ADEG-PRE-PR
    private WtgaDtUltAdegPrePr wtgaDtUltAdegPrePr = new WtgaDtUltAdegPrePr();
    //Original name: WTGA-PRSTZ-AGG-ULT
    private WtgaPrstzAggUlt wtgaPrstzAggUlt = new WtgaPrstzAggUlt();
    //Original name: WTGA-TS-RIVAL-NET
    private WtgaTsRivalNet wtgaTsRivalNet = new WtgaTsRivalNet();
    //Original name: WTGA-PRE-PATTUITO
    private WtgaPrePattuito wtgaPrePattuito = new WtgaPrePattuito();
    //Original name: WTGA-TP-RIVAL
    private String wtgaTpRival = DefaultValues.stringVal(Len.WTGA_TP_RIVAL);
    //Original name: WTGA-RIS-MAT
    private WtgaRisMat wtgaRisMat = new WtgaRisMat();
    //Original name: WTGA-CPT-MIN-SCAD
    private WtgaCptMinScad wtgaCptMinScad = new WtgaCptMinScad();
    //Original name: WTGA-COMMIS-GEST
    private WtgaCommisGest wtgaCommisGest = new WtgaCommisGest();
    //Original name: WTGA-TP-MANFEE-APPL
    private String wtgaTpManfeeAppl = DefaultValues.stringVal(Len.WTGA_TP_MANFEE_APPL);
    //Original name: WTGA-DS-RIGA
    private long wtgaDsRiga = DefaultValues.LONG_VAL;
    //Original name: WTGA-DS-OPER-SQL
    private char wtgaDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WTGA-DS-VER
    private int wtgaDsVer = DefaultValues.INT_VAL;
    //Original name: WTGA-DS-TS-INI-CPTZ
    private long wtgaDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WTGA-DS-TS-END-CPTZ
    private long wtgaDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WTGA-DS-UTENTE
    private String wtgaDsUtente = DefaultValues.stringVal(Len.WTGA_DS_UTENTE);
    //Original name: WTGA-DS-STATO-ELAB
    private char wtgaDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WTGA-PC-COMMIS-GEST
    private WtgaPcCommisGest wtgaPcCommisGest = new WtgaPcCommisGest();
    //Original name: WTGA-NUM-GG-RIVAL
    private WtgaNumGgRival wtgaNumGgRival = new WtgaNumGgRival();
    //Original name: WTGA-IMP-TRASFE
    private WtgaImpTrasfe wtgaImpTrasfe = new WtgaImpTrasfe();
    //Original name: WTGA-IMP-TFR-STRC
    private WtgaImpTfrStrc wtgaImpTfrStrc = new WtgaImpTfrStrc();
    //Original name: WTGA-ACQ-EXP
    private WtgaAcqExp wtgaAcqExp = new WtgaAcqExp();
    //Original name: WTGA-REMUN-ASS
    private WtgaRemunAss wtgaRemunAss = new WtgaRemunAss();
    //Original name: WTGA-COMMIS-INTER
    private WtgaCommisInter wtgaCommisInter = new WtgaCommisInter();
    //Original name: WTGA-ALQ-REMUN-ASS
    private WtgaAlqRemunAss wtgaAlqRemunAss = new WtgaAlqRemunAss();
    //Original name: WTGA-ALQ-COMMIS-INTER
    private WtgaAlqCommisInter wtgaAlqCommisInter = new WtgaAlqCommisInter();
    //Original name: WTGA-IMPB-REMUN-ASS
    private WtgaImpbRemunAss wtgaImpbRemunAss = new WtgaImpbRemunAss();
    //Original name: WTGA-IMPB-COMMIS-INTER
    private WtgaImpbCommisInter wtgaImpbCommisInter = new WtgaImpbCommisInter();
    //Original name: WTGA-COS-RUN-ASSVA
    private WtgaCosRunAssva wtgaCosRunAssva = new WtgaCosRunAssva();
    //Original name: WTGA-COS-RUN-ASSVA-IDC
    private WtgaCosRunAssvaIdc wtgaCosRunAssvaIdc = new WtgaCosRunAssvaIdc();

    //==== CONSTRUCTORS ====
    public WtgaDati() {
    }

    public WtgaDati(WtgaDati dati) {
        this();
        this.wtgaIdTrchDiGar = dati.wtgaIdTrchDiGar;
        this.wtgaIdGar = dati.wtgaIdGar;
        this.wtgaIdAdes = dati.wtgaIdAdes;
        this.wtgaIdPoli = dati.wtgaIdPoli;
        this.wtgaIdMoviCrz = dati.wtgaIdMoviCrz;
        this.wtgaIdMoviChiu = ((WtgaIdMoviChiu)dati.wtgaIdMoviChiu.copy());
        this.wtgaDtIniEff = dati.wtgaDtIniEff;
        this.wtgaDtEndEff = dati.wtgaDtEndEff;
        this.wtgaCodCompAnia = dati.wtgaCodCompAnia;
        this.wtgaDtDecor = dati.wtgaDtDecor;
        this.wtgaDtScad = ((WtgaDtScad)dati.wtgaDtScad.copy());
        this.wtgaIbOgg = dati.wtgaIbOgg;
        this.wtgaTpRgmFisc = dati.wtgaTpRgmFisc;
        this.wtgaDtEmis = ((WtgaDtEmis)dati.wtgaDtEmis.copy());
        this.wtgaTpTrch = dati.wtgaTpTrch;
        this.wtgaDurAa = ((WtgaDurAa)dati.wtgaDurAa.copy());
        this.wtgaDurMm = ((WtgaDurMm)dati.wtgaDurMm.copy());
        this.wtgaDurGg = ((WtgaDurGg)dati.wtgaDurGg.copy());
        this.wtgaPreCasoMor = ((WtgaPreCasoMor)dati.wtgaPreCasoMor.copy());
        this.wtgaPcIntrRiat = ((WtgaPcIntrRiat)dati.wtgaPcIntrRiat.copy());
        this.wtgaImpBnsAntic = ((WtgaImpBnsAntic)dati.wtgaImpBnsAntic.copy());
        this.wtgaPreIniNet = ((WtgaPreIniNet)dati.wtgaPreIniNet.copy());
        this.wtgaPrePpIni = ((WtgaPrePpIni)dati.wtgaPrePpIni.copy());
        this.wtgaPrePpUlt = ((WtgaPrePpUlt)dati.wtgaPrePpUlt.copy());
        this.wtgaPreTariIni = ((WtgaPreTariIni)dati.wtgaPreTariIni.copy());
        this.wtgaPreTariUlt = ((WtgaPreTariUlt)dati.wtgaPreTariUlt.copy());
        this.wtgaPreInvrioIni = ((WtgaPreInvrioIni)dati.wtgaPreInvrioIni.copy());
        this.wtgaPreInvrioUlt = ((WtgaPreInvrioUlt)dati.wtgaPreInvrioUlt.copy());
        this.wtgaPreRivto = ((WtgaPreRivto)dati.wtgaPreRivto.copy());
        this.wtgaImpSoprProf = ((WtgaImpSoprProf)dati.wtgaImpSoprProf.copy());
        this.wtgaImpSoprSan = ((WtgaImpSoprSan)dati.wtgaImpSoprSan.copy());
        this.wtgaImpSoprSpo = ((WtgaImpSoprSpo)dati.wtgaImpSoprSpo.copy());
        this.wtgaImpSoprTec = ((WtgaImpSoprTec)dati.wtgaImpSoprTec.copy());
        this.wtgaImpAltSopr = ((WtgaImpAltSopr)dati.wtgaImpAltSopr.copy());
        this.wtgaPreStab = ((WtgaPreStab)dati.wtgaPreStab.copy());
        this.wtgaDtEffStab = ((WtgaDtEffStab)dati.wtgaDtEffStab.copy());
        this.wtgaTsRivalFis = ((WtgaTsRivalFis)dati.wtgaTsRivalFis.copy());
        this.wtgaTsRivalIndiciz = ((WtgaTsRivalIndiciz)dati.wtgaTsRivalIndiciz.copy());
        this.wtgaOldTsTec = ((WtgaOldTsTec)dati.wtgaOldTsTec.copy());
        this.wtgaRatLrd = ((WtgaRatLrd)dati.wtgaRatLrd.copy());
        this.wtgaPreLrd = ((WtgaPreLrd)dati.wtgaPreLrd.copy());
        this.wtgaPrstzIni = ((WtgaPrstzIni)dati.wtgaPrstzIni.copy());
        this.wtgaPrstzUlt = ((WtgaPrstzUlt)dati.wtgaPrstzUlt.copy());
        this.wtgaCptInOpzRivto = ((WtgaCptInOpzRivto)dati.wtgaCptInOpzRivto.copy());
        this.wtgaPrstzIniStab = ((WtgaPrstzIniStab)dati.wtgaPrstzIniStab.copy());
        this.wtgaCptRshMor = ((WtgaCptRshMor)dati.wtgaCptRshMor.copy());
        this.wtgaPrstzRidIni = ((WtgaPrstzRidIni)dati.wtgaPrstzRidIni.copy());
        this.wtgaFlCarCont = dati.wtgaFlCarCont;
        this.wtgaBnsGiaLiqto = ((WtgaBnsGiaLiqto)dati.wtgaBnsGiaLiqto.copy());
        this.wtgaImpBns = ((WtgaImpBns)dati.wtgaImpBns.copy());
        this.wtgaCodDvs = dati.wtgaCodDvs;
        this.wtgaPrstzIniNewfis = ((WtgaPrstzIniNewfis)dati.wtgaPrstzIniNewfis.copy());
        this.wtgaImpScon = ((WtgaImpScon)dati.wtgaImpScon.copy());
        this.wtgaAlqScon = ((WtgaAlqScon)dati.wtgaAlqScon.copy());
        this.wtgaImpCarAcq = ((WtgaImpCarAcq)dati.wtgaImpCarAcq.copy());
        this.wtgaImpCarInc = ((WtgaImpCarInc)dati.wtgaImpCarInc.copy());
        this.wtgaImpCarGest = ((WtgaImpCarGest)dati.wtgaImpCarGest.copy());
        this.wtgaEtaAa1oAssto = ((WtgaEtaAa1oAssto)dati.wtgaEtaAa1oAssto.copy());
        this.wtgaEtaMm1oAssto = ((WtgaEtaMm1oAssto)dati.wtgaEtaMm1oAssto.copy());
        this.wtgaEtaAa2oAssto = ((WtgaEtaAa2oAssto)dati.wtgaEtaAa2oAssto.copy());
        this.wtgaEtaMm2oAssto = ((WtgaEtaMm2oAssto)dati.wtgaEtaMm2oAssto.copy());
        this.wtgaEtaAa3oAssto = ((WtgaEtaAa3oAssto)dati.wtgaEtaAa3oAssto.copy());
        this.wtgaEtaMm3oAssto = ((WtgaEtaMm3oAssto)dati.wtgaEtaMm3oAssto.copy());
        this.wtgaRendtoLrd = ((WtgaRendtoLrd)dati.wtgaRendtoLrd.copy());
        this.wtgaPcRetr = ((WtgaPcRetr)dati.wtgaPcRetr.copy());
        this.wtgaRendtoRetr = ((WtgaRendtoRetr)dati.wtgaRendtoRetr.copy());
        this.wtgaMinGarto = ((WtgaMinGarto)dati.wtgaMinGarto.copy());
        this.wtgaMinTrnut = ((WtgaMinTrnut)dati.wtgaMinTrnut.copy());
        this.wtgaPreAttDiTrch = ((WtgaPreAttDiTrch)dati.wtgaPreAttDiTrch.copy());
        this.wtgaMatuEnd2000 = ((WtgaMatuEnd2000)dati.wtgaMatuEnd2000.copy());
        this.wtgaAbbTotIni = ((WtgaAbbTotIni)dati.wtgaAbbTotIni.copy());
        this.wtgaAbbTotUlt = ((WtgaAbbTotUlt)dati.wtgaAbbTotUlt.copy());
        this.wtgaAbbAnnuUlt = ((WtgaAbbAnnuUlt)dati.wtgaAbbAnnuUlt.copy());
        this.wtgaDurAbb = ((WtgaDurAbb)dati.wtgaDurAbb.copy());
        this.wtgaTpAdegAbb = dati.wtgaTpAdegAbb;
        this.wtgaModCalc = dati.wtgaModCalc;
        this.wtgaImpAz = ((WtgaImpAz)dati.wtgaImpAz.copy());
        this.wtgaImpAder = ((WtgaImpAder)dati.wtgaImpAder.copy());
        this.wtgaImpTfr = ((WtgaImpTfr)dati.wtgaImpTfr.copy());
        this.wtgaImpVolo = ((WtgaImpVolo)dati.wtgaImpVolo.copy());
        this.wtgaVisEnd2000 = ((WtgaVisEnd2000)dati.wtgaVisEnd2000.copy());
        this.wtgaDtVldtProd = ((WtgaDtVldtProd)dati.wtgaDtVldtProd.copy());
        this.wtgaDtIniValTar = ((WtgaDtIniValTar)dati.wtgaDtIniValTar.copy());
        this.wtgaImpbVisEnd2000 = ((WtgaImpbVisEnd2000)dati.wtgaImpbVisEnd2000.copy());
        this.wtgaRenIniTsTec0 = ((WtgaRenIniTsTec0)dati.wtgaRenIniTsTec0.copy());
        this.wtgaPcRipPre = ((WtgaPcRipPre)dati.wtgaPcRipPre.copy());
        this.wtgaFlImportiForz = dati.wtgaFlImportiForz;
        this.wtgaPrstzIniNforz = ((WtgaPrstzIniNforz)dati.wtgaPrstzIniNforz.copy());
        this.wtgaVisEnd2000Nforz = ((WtgaVisEnd2000Nforz)dati.wtgaVisEnd2000Nforz.copy());
        this.wtgaIntrMora = ((WtgaIntrMora)dati.wtgaIntrMora.copy());
        this.wtgaManfeeAntic = ((WtgaManfeeAntic)dati.wtgaManfeeAntic.copy());
        this.wtgaManfeeRicor = ((WtgaManfeeRicor)dati.wtgaManfeeRicor.copy());
        this.wtgaPreUniRivto = ((WtgaPreUniRivto)dati.wtgaPreUniRivto.copy());
        this.wtgaProv1aaAcq = ((WtgaProv1aaAcq)dati.wtgaProv1aaAcq.copy());
        this.wtgaProv2aaAcq = ((WtgaProv2aaAcq)dati.wtgaProv2aaAcq.copy());
        this.wtgaProvRicor = ((WtgaProvRicor)dati.wtgaProvRicor.copy());
        this.wtgaProvInc = ((WtgaProvInc)dati.wtgaProvInc.copy());
        this.wtgaAlqProvAcq = ((WtgaAlqProvAcq)dati.wtgaAlqProvAcq.copy());
        this.wtgaAlqProvInc = ((WtgaAlqProvInc)dati.wtgaAlqProvInc.copy());
        this.wtgaAlqProvRicor = ((WtgaAlqProvRicor)dati.wtgaAlqProvRicor.copy());
        this.wtgaImpbProvAcq = ((WtgaImpbProvAcq)dati.wtgaImpbProvAcq.copy());
        this.wtgaImpbProvInc = ((WtgaImpbProvInc)dati.wtgaImpbProvInc.copy());
        this.wtgaImpbProvRicor = ((WtgaImpbProvRicor)dati.wtgaImpbProvRicor.copy());
        this.wtgaFlProvForz = dati.wtgaFlProvForz;
        this.wtgaPrstzAggIni = ((WtgaPrstzAggIni)dati.wtgaPrstzAggIni.copy());
        this.wtgaIncrPre = ((WtgaIncrPre)dati.wtgaIncrPre.copy());
        this.wtgaIncrPrstz = ((WtgaIncrPrstz)dati.wtgaIncrPrstz.copy());
        this.wtgaDtUltAdegPrePr = ((WtgaDtUltAdegPrePr)dati.wtgaDtUltAdegPrePr.copy());
        this.wtgaPrstzAggUlt = ((WtgaPrstzAggUlt)dati.wtgaPrstzAggUlt.copy());
        this.wtgaTsRivalNet = ((WtgaTsRivalNet)dati.wtgaTsRivalNet.copy());
        this.wtgaPrePattuito = ((WtgaPrePattuito)dati.wtgaPrePattuito.copy());
        this.wtgaTpRival = dati.wtgaTpRival;
        this.wtgaRisMat = ((WtgaRisMat)dati.wtgaRisMat.copy());
        this.wtgaCptMinScad = ((WtgaCptMinScad)dati.wtgaCptMinScad.copy());
        this.wtgaCommisGest = ((WtgaCommisGest)dati.wtgaCommisGest.copy());
        this.wtgaTpManfeeAppl = dati.wtgaTpManfeeAppl;
        this.wtgaDsRiga = dati.wtgaDsRiga;
        this.wtgaDsOperSql = dati.wtgaDsOperSql;
        this.wtgaDsVer = dati.wtgaDsVer;
        this.wtgaDsTsIniCptz = dati.wtgaDsTsIniCptz;
        this.wtgaDsTsEndCptz = dati.wtgaDsTsEndCptz;
        this.wtgaDsUtente = dati.wtgaDsUtente;
        this.wtgaDsStatoElab = dati.wtgaDsStatoElab;
        this.wtgaPcCommisGest = ((WtgaPcCommisGest)dati.wtgaPcCommisGest.copy());
        this.wtgaNumGgRival = ((WtgaNumGgRival)dati.wtgaNumGgRival.copy());
        this.wtgaImpTrasfe = ((WtgaImpTrasfe)dati.wtgaImpTrasfe.copy());
        this.wtgaImpTfrStrc = ((WtgaImpTfrStrc)dati.wtgaImpTfrStrc.copy());
        this.wtgaAcqExp = ((WtgaAcqExp)dati.wtgaAcqExp.copy());
        this.wtgaRemunAss = ((WtgaRemunAss)dati.wtgaRemunAss.copy());
        this.wtgaCommisInter = ((WtgaCommisInter)dati.wtgaCommisInter.copy());
        this.wtgaAlqRemunAss = ((WtgaAlqRemunAss)dati.wtgaAlqRemunAss.copy());
        this.wtgaAlqCommisInter = ((WtgaAlqCommisInter)dati.wtgaAlqCommisInter.copy());
        this.wtgaImpbRemunAss = ((WtgaImpbRemunAss)dati.wtgaImpbRemunAss.copy());
        this.wtgaImpbCommisInter = ((WtgaImpbCommisInter)dati.wtgaImpbCommisInter.copy());
        this.wtgaCosRunAssva = ((WtgaCosRunAssva)dati.wtgaCosRunAssva.copy());
        this.wtgaCosRunAssvaIdc = ((WtgaCosRunAssvaIdc)dati.wtgaCosRunAssvaIdc.copy());
    }

    //==== METHODS ====
    public String getWtgaDatiFormatted() {
        return MarshalByteExt.bufferToStr(getWtgaDatiBytes());
    }

    public void setWtgaDatiBytes(byte[] buffer) {
        setDatiBytes(buffer, 1);
    }

    public byte[] getWtgaDatiBytes() {
        byte[] buffer = new byte[Len.DATI];
        return getDatiBytes(buffer, 1);
    }

    public void setDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        wtgaIdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTGA_ID_TRCH_DI_GAR, 0);
        position += Len.WTGA_ID_TRCH_DI_GAR;
        wtgaIdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTGA_ID_GAR, 0);
        position += Len.WTGA_ID_GAR;
        wtgaIdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTGA_ID_ADES, 0);
        position += Len.WTGA_ID_ADES;
        wtgaIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTGA_ID_POLI, 0);
        position += Len.WTGA_ID_POLI;
        wtgaIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTGA_ID_MOVI_CRZ, 0);
        position += Len.WTGA_ID_MOVI_CRZ;
        wtgaIdMoviChiu.setWtgaIdMoviChiuFromBuffer(buffer, position);
        position += WtgaIdMoviChiu.Len.WTGA_ID_MOVI_CHIU;
        wtgaDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTGA_DT_INI_EFF, 0);
        position += Len.WTGA_DT_INI_EFF;
        wtgaDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTGA_DT_END_EFF, 0);
        position += Len.WTGA_DT_END_EFF;
        wtgaCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTGA_COD_COMP_ANIA, 0);
        position += Len.WTGA_COD_COMP_ANIA;
        wtgaDtDecor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTGA_DT_DECOR, 0);
        position += Len.WTGA_DT_DECOR;
        wtgaDtScad.setWtgaDtScadFromBuffer(buffer, position);
        position += WtgaDtScad.Len.WTGA_DT_SCAD;
        wtgaIbOgg = MarshalByte.readString(buffer, position, Len.WTGA_IB_OGG);
        position += Len.WTGA_IB_OGG;
        wtgaTpRgmFisc = MarshalByte.readString(buffer, position, Len.WTGA_TP_RGM_FISC);
        position += Len.WTGA_TP_RGM_FISC;
        wtgaDtEmis.setWtgaDtEmisFromBuffer(buffer, position);
        position += WtgaDtEmis.Len.WTGA_DT_EMIS;
        wtgaTpTrch = MarshalByte.readString(buffer, position, Len.WTGA_TP_TRCH);
        position += Len.WTGA_TP_TRCH;
        wtgaDurAa.setWtgaDurAaFromBuffer(buffer, position);
        position += WtgaDurAa.Len.WTGA_DUR_AA;
        wtgaDurMm.setWtgaDurMmFromBuffer(buffer, position);
        position += WtgaDurMm.Len.WTGA_DUR_MM;
        wtgaDurGg.setWtgaDurGgFromBuffer(buffer, position);
        position += WtgaDurGg.Len.WTGA_DUR_GG;
        wtgaPreCasoMor.setWtgaPreCasoMorFromBuffer(buffer, position);
        position += WtgaPreCasoMor.Len.WTGA_PRE_CASO_MOR;
        wtgaPcIntrRiat.setWtgaPcIntrRiatFromBuffer(buffer, position);
        position += WtgaPcIntrRiat.Len.WTGA_PC_INTR_RIAT;
        wtgaImpBnsAntic.setWtgaImpBnsAnticFromBuffer(buffer, position);
        position += WtgaImpBnsAntic.Len.WTGA_IMP_BNS_ANTIC;
        wtgaPreIniNet.setWtgaPreIniNetFromBuffer(buffer, position);
        position += WtgaPreIniNet.Len.WTGA_PRE_INI_NET;
        wtgaPrePpIni.setWtgaPrePpIniFromBuffer(buffer, position);
        position += WtgaPrePpIni.Len.WTGA_PRE_PP_INI;
        wtgaPrePpUlt.setWtgaPrePpUltFromBuffer(buffer, position);
        position += WtgaPrePpUlt.Len.WTGA_PRE_PP_ULT;
        wtgaPreTariIni.setWtgaPreTariIniFromBuffer(buffer, position);
        position += WtgaPreTariIni.Len.WTGA_PRE_TARI_INI;
        wtgaPreTariUlt.setWtgaPreTariUltFromBuffer(buffer, position);
        position += WtgaPreTariUlt.Len.WTGA_PRE_TARI_ULT;
        wtgaPreInvrioIni.setWtgaPreInvrioIniFromBuffer(buffer, position);
        position += WtgaPreInvrioIni.Len.WTGA_PRE_INVRIO_INI;
        wtgaPreInvrioUlt.setWtgaPreInvrioUltFromBuffer(buffer, position);
        position += WtgaPreInvrioUlt.Len.WTGA_PRE_INVRIO_ULT;
        wtgaPreRivto.setWtgaPreRivtoFromBuffer(buffer, position);
        position += WtgaPreRivto.Len.WTGA_PRE_RIVTO;
        wtgaImpSoprProf.setWtgaImpSoprProfFromBuffer(buffer, position);
        position += WtgaImpSoprProf.Len.WTGA_IMP_SOPR_PROF;
        wtgaImpSoprSan.setWtgaImpSoprSanFromBuffer(buffer, position);
        position += WtgaImpSoprSan.Len.WTGA_IMP_SOPR_SAN;
        wtgaImpSoprSpo.setWtgaImpSoprSpoFromBuffer(buffer, position);
        position += WtgaImpSoprSpo.Len.WTGA_IMP_SOPR_SPO;
        wtgaImpSoprTec.setWtgaImpSoprTecFromBuffer(buffer, position);
        position += WtgaImpSoprTec.Len.WTGA_IMP_SOPR_TEC;
        wtgaImpAltSopr.setWtgaImpAltSoprFromBuffer(buffer, position);
        position += WtgaImpAltSopr.Len.WTGA_IMP_ALT_SOPR;
        wtgaPreStab.setWtgaPreStabFromBuffer(buffer, position);
        position += WtgaPreStab.Len.WTGA_PRE_STAB;
        wtgaDtEffStab.setWtgaDtEffStabFromBuffer(buffer, position);
        position += WtgaDtEffStab.Len.WTGA_DT_EFF_STAB;
        wtgaTsRivalFis.setWtgaTsRivalFisFromBuffer(buffer, position);
        position += WtgaTsRivalFis.Len.WTGA_TS_RIVAL_FIS;
        wtgaTsRivalIndiciz.setWtgaTsRivalIndicizFromBuffer(buffer, position);
        position += WtgaTsRivalIndiciz.Len.WTGA_TS_RIVAL_INDICIZ;
        wtgaOldTsTec.setWtgaOldTsTecFromBuffer(buffer, position);
        position += WtgaOldTsTec.Len.WTGA_OLD_TS_TEC;
        wtgaRatLrd.setWtgaRatLrdFromBuffer(buffer, position);
        position += WtgaRatLrd.Len.WTGA_RAT_LRD;
        wtgaPreLrd.setWtgaPreLrdFromBuffer(buffer, position);
        position += WtgaPreLrd.Len.WTGA_PRE_LRD;
        wtgaPrstzIni.setWtgaPrstzIniFromBuffer(buffer, position);
        position += WtgaPrstzIni.Len.WTGA_PRSTZ_INI;
        wtgaPrstzUlt.setWtgaPrstzUltFromBuffer(buffer, position);
        position += WtgaPrstzUlt.Len.WTGA_PRSTZ_ULT;
        wtgaCptInOpzRivto.setWtgaCptInOpzRivtoFromBuffer(buffer, position);
        position += WtgaCptInOpzRivto.Len.WTGA_CPT_IN_OPZ_RIVTO;
        wtgaPrstzIniStab.setWtgaPrstzIniStabFromBuffer(buffer, position);
        position += WtgaPrstzIniStab.Len.WTGA_PRSTZ_INI_STAB;
        wtgaCptRshMor.setWtgaCptRshMorFromBuffer(buffer, position);
        position += WtgaCptRshMor.Len.WTGA_CPT_RSH_MOR;
        wtgaPrstzRidIni.setWtgaPrstzRidIniFromBuffer(buffer, position);
        position += WtgaPrstzRidIni.Len.WTGA_PRSTZ_RID_INI;
        wtgaFlCarCont = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtgaBnsGiaLiqto.setWtgaBnsGiaLiqtoFromBuffer(buffer, position);
        position += WtgaBnsGiaLiqto.Len.WTGA_BNS_GIA_LIQTO;
        wtgaImpBns.setWtgaImpBnsFromBuffer(buffer, position);
        position += WtgaImpBns.Len.WTGA_IMP_BNS;
        wtgaCodDvs = MarshalByte.readString(buffer, position, Len.WTGA_COD_DVS);
        position += Len.WTGA_COD_DVS;
        wtgaPrstzIniNewfis.setWtgaPrstzIniNewfisFromBuffer(buffer, position);
        position += WtgaPrstzIniNewfis.Len.WTGA_PRSTZ_INI_NEWFIS;
        wtgaImpScon.setWtgaImpSconFromBuffer(buffer, position);
        position += WtgaImpScon.Len.WTGA_IMP_SCON;
        wtgaAlqScon.setWtgaAlqSconFromBuffer(buffer, position);
        position += WtgaAlqScon.Len.WTGA_ALQ_SCON;
        wtgaImpCarAcq.setWtgaImpCarAcqFromBuffer(buffer, position);
        position += WtgaImpCarAcq.Len.WTGA_IMP_CAR_ACQ;
        wtgaImpCarInc.setWtgaImpCarIncFromBuffer(buffer, position);
        position += WtgaImpCarInc.Len.WTGA_IMP_CAR_INC;
        wtgaImpCarGest.setWtgaImpCarGestFromBuffer(buffer, position);
        position += WtgaImpCarGest.Len.WTGA_IMP_CAR_GEST;
        wtgaEtaAa1oAssto.setWtgaEtaAa1oAsstoFromBuffer(buffer, position);
        position += WtgaEtaAa1oAssto.Len.WTGA_ETA_AA1O_ASSTO;
        wtgaEtaMm1oAssto.setWtgaEtaMm1oAsstoFromBuffer(buffer, position);
        position += WtgaEtaMm1oAssto.Len.WTGA_ETA_MM1O_ASSTO;
        wtgaEtaAa2oAssto.setWtgaEtaAa2oAsstoFromBuffer(buffer, position);
        position += WtgaEtaAa2oAssto.Len.WTGA_ETA_AA2O_ASSTO;
        wtgaEtaMm2oAssto.setWtgaEtaMm2oAsstoFromBuffer(buffer, position);
        position += WtgaEtaMm2oAssto.Len.WTGA_ETA_MM2O_ASSTO;
        wtgaEtaAa3oAssto.setWtgaEtaAa3oAsstoFromBuffer(buffer, position);
        position += WtgaEtaAa3oAssto.Len.WTGA_ETA_AA3O_ASSTO;
        wtgaEtaMm3oAssto.setWtgaEtaMm3oAsstoFromBuffer(buffer, position);
        position += WtgaEtaMm3oAssto.Len.WTGA_ETA_MM3O_ASSTO;
        wtgaRendtoLrd.setWtgaRendtoLrdFromBuffer(buffer, position);
        position += WtgaRendtoLrd.Len.WTGA_RENDTO_LRD;
        wtgaPcRetr.setWtgaPcRetrFromBuffer(buffer, position);
        position += WtgaPcRetr.Len.WTGA_PC_RETR;
        wtgaRendtoRetr.setWtgaRendtoRetrFromBuffer(buffer, position);
        position += WtgaRendtoRetr.Len.WTGA_RENDTO_RETR;
        wtgaMinGarto.setWtgaMinGartoFromBuffer(buffer, position);
        position += WtgaMinGarto.Len.WTGA_MIN_GARTO;
        wtgaMinTrnut.setWtgaMinTrnutFromBuffer(buffer, position);
        position += WtgaMinTrnut.Len.WTGA_MIN_TRNUT;
        wtgaPreAttDiTrch.setWtgaPreAttDiTrchFromBuffer(buffer, position);
        position += WtgaPreAttDiTrch.Len.WTGA_PRE_ATT_DI_TRCH;
        wtgaMatuEnd2000.setWtgaMatuEnd2000FromBuffer(buffer, position);
        position += WtgaMatuEnd2000.Len.WTGA_MATU_END2000;
        wtgaAbbTotIni.setWtgaAbbTotIniFromBuffer(buffer, position);
        position += WtgaAbbTotIni.Len.WTGA_ABB_TOT_INI;
        wtgaAbbTotUlt.setWtgaAbbTotUltFromBuffer(buffer, position);
        position += WtgaAbbTotUlt.Len.WTGA_ABB_TOT_ULT;
        wtgaAbbAnnuUlt.setWtgaAbbAnnuUltFromBuffer(buffer, position);
        position += WtgaAbbAnnuUlt.Len.WTGA_ABB_ANNU_ULT;
        wtgaDurAbb.setWtgaDurAbbFromBuffer(buffer, position);
        position += WtgaDurAbb.Len.WTGA_DUR_ABB;
        wtgaTpAdegAbb = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtgaModCalc = MarshalByte.readString(buffer, position, Len.WTGA_MOD_CALC);
        position += Len.WTGA_MOD_CALC;
        wtgaImpAz.setWtgaImpAzFromBuffer(buffer, position);
        position += WtgaImpAz.Len.WTGA_IMP_AZ;
        wtgaImpAder.setWtgaImpAderFromBuffer(buffer, position);
        position += WtgaImpAder.Len.WTGA_IMP_ADER;
        wtgaImpTfr.setWtgaImpTfrFromBuffer(buffer, position);
        position += WtgaImpTfr.Len.WTGA_IMP_TFR;
        wtgaImpVolo.setWtgaImpVoloFromBuffer(buffer, position);
        position += WtgaImpVolo.Len.WTGA_IMP_VOLO;
        wtgaVisEnd2000.setWtgaVisEnd2000FromBuffer(buffer, position);
        position += WtgaVisEnd2000.Len.WTGA_VIS_END2000;
        wtgaDtVldtProd.setWtgaDtVldtProdFromBuffer(buffer, position);
        position += WtgaDtVldtProd.Len.WTGA_DT_VLDT_PROD;
        wtgaDtIniValTar.setWtgaDtIniValTarFromBuffer(buffer, position);
        position += WtgaDtIniValTar.Len.WTGA_DT_INI_VAL_TAR;
        wtgaImpbVisEnd2000.setWtgaImpbVisEnd2000FromBuffer(buffer, position);
        position += WtgaImpbVisEnd2000.Len.WTGA_IMPB_VIS_END2000;
        wtgaRenIniTsTec0.setWtgaRenIniTsTec0FromBuffer(buffer, position);
        position += WtgaRenIniTsTec0.Len.WTGA_REN_INI_TS_TEC0;
        wtgaPcRipPre.setWtgaPcRipPreFromBuffer(buffer, position);
        position += WtgaPcRipPre.Len.WTGA_PC_RIP_PRE;
        wtgaFlImportiForz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtgaPrstzIniNforz.setWtgaPrstzIniNforzFromBuffer(buffer, position);
        position += WtgaPrstzIniNforz.Len.WTGA_PRSTZ_INI_NFORZ;
        wtgaVisEnd2000Nforz.setWtgaVisEnd2000NforzFromBuffer(buffer, position);
        position += WtgaVisEnd2000Nforz.Len.WTGA_VIS_END2000_NFORZ;
        wtgaIntrMora.setWtgaIntrMoraFromBuffer(buffer, position);
        position += WtgaIntrMora.Len.WTGA_INTR_MORA;
        wtgaManfeeAntic.setWtgaManfeeAnticFromBuffer(buffer, position);
        position += WtgaManfeeAntic.Len.WTGA_MANFEE_ANTIC;
        wtgaManfeeRicor.setWtgaManfeeRicorFromBuffer(buffer, position);
        position += WtgaManfeeRicor.Len.WTGA_MANFEE_RICOR;
        wtgaPreUniRivto.setWtgaPreUniRivtoFromBuffer(buffer, position);
        position += WtgaPreUniRivto.Len.WTGA_PRE_UNI_RIVTO;
        wtgaProv1aaAcq.setWtgaProv1aaAcqFromBuffer(buffer, position);
        position += WtgaProv1aaAcq.Len.WTGA_PROV1AA_ACQ;
        wtgaProv2aaAcq.setWtgaProv2aaAcqFromBuffer(buffer, position);
        position += WtgaProv2aaAcq.Len.WTGA_PROV2AA_ACQ;
        wtgaProvRicor.setWtgaProvRicorFromBuffer(buffer, position);
        position += WtgaProvRicor.Len.WTGA_PROV_RICOR;
        wtgaProvInc.setWtgaProvIncFromBuffer(buffer, position);
        position += WtgaProvInc.Len.WTGA_PROV_INC;
        wtgaAlqProvAcq.setWtgaAlqProvAcqFromBuffer(buffer, position);
        position += WtgaAlqProvAcq.Len.WTGA_ALQ_PROV_ACQ;
        wtgaAlqProvInc.setWtgaAlqProvIncFromBuffer(buffer, position);
        position += WtgaAlqProvInc.Len.WTGA_ALQ_PROV_INC;
        wtgaAlqProvRicor.setWtgaAlqProvRicorFromBuffer(buffer, position);
        position += WtgaAlqProvRicor.Len.WTGA_ALQ_PROV_RICOR;
        wtgaImpbProvAcq.setWtgaImpbProvAcqFromBuffer(buffer, position);
        position += WtgaImpbProvAcq.Len.WTGA_IMPB_PROV_ACQ;
        wtgaImpbProvInc.setWtgaImpbProvIncFromBuffer(buffer, position);
        position += WtgaImpbProvInc.Len.WTGA_IMPB_PROV_INC;
        wtgaImpbProvRicor.setWtgaImpbProvRicorFromBuffer(buffer, position);
        position += WtgaImpbProvRicor.Len.WTGA_IMPB_PROV_RICOR;
        wtgaFlProvForz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtgaPrstzAggIni.setWtgaPrstzAggIniFromBuffer(buffer, position);
        position += WtgaPrstzAggIni.Len.WTGA_PRSTZ_AGG_INI;
        wtgaIncrPre.setWtgaIncrPreFromBuffer(buffer, position);
        position += WtgaIncrPre.Len.WTGA_INCR_PRE;
        wtgaIncrPrstz.setWtgaIncrPrstzFromBuffer(buffer, position);
        position += WtgaIncrPrstz.Len.WTGA_INCR_PRSTZ;
        wtgaDtUltAdegPrePr.setWtgaDtUltAdegPrePrFromBuffer(buffer, position);
        position += WtgaDtUltAdegPrePr.Len.WTGA_DT_ULT_ADEG_PRE_PR;
        wtgaPrstzAggUlt.setWtgaPrstzAggUltFromBuffer(buffer, position);
        position += WtgaPrstzAggUlt.Len.WTGA_PRSTZ_AGG_ULT;
        wtgaTsRivalNet.setWtgaTsRivalNetFromBuffer(buffer, position);
        position += WtgaTsRivalNet.Len.WTGA_TS_RIVAL_NET;
        wtgaPrePattuito.setWtgaPrePattuitoFromBuffer(buffer, position);
        position += WtgaPrePattuito.Len.WTGA_PRE_PATTUITO;
        wtgaTpRival = MarshalByte.readString(buffer, position, Len.WTGA_TP_RIVAL);
        position += Len.WTGA_TP_RIVAL;
        wtgaRisMat.setWtgaRisMatFromBuffer(buffer, position);
        position += WtgaRisMat.Len.WTGA_RIS_MAT;
        wtgaCptMinScad.setWtgaCptMinScadFromBuffer(buffer, position);
        position += WtgaCptMinScad.Len.WTGA_CPT_MIN_SCAD;
        wtgaCommisGest.setWtgaCommisGestFromBuffer(buffer, position);
        position += WtgaCommisGest.Len.WTGA_COMMIS_GEST;
        wtgaTpManfeeAppl = MarshalByte.readString(buffer, position, Len.WTGA_TP_MANFEE_APPL);
        position += Len.WTGA_TP_MANFEE_APPL;
        wtgaDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WTGA_DS_RIGA, 0);
        position += Len.WTGA_DS_RIGA;
        wtgaDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtgaDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.WTGA_DS_VER, 0);
        position += Len.WTGA_DS_VER;
        wtgaDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WTGA_DS_TS_INI_CPTZ, 0);
        position += Len.WTGA_DS_TS_INI_CPTZ;
        wtgaDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.WTGA_DS_TS_END_CPTZ, 0);
        position += Len.WTGA_DS_TS_END_CPTZ;
        wtgaDsUtente = MarshalByte.readString(buffer, position, Len.WTGA_DS_UTENTE);
        position += Len.WTGA_DS_UTENTE;
        wtgaDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        wtgaPcCommisGest.setWtgaPcCommisGestFromBuffer(buffer, position);
        position += WtgaPcCommisGest.Len.WTGA_PC_COMMIS_GEST;
        wtgaNumGgRival.setWtgaNumGgRivalFromBuffer(buffer, position);
        position += WtgaNumGgRival.Len.WTGA_NUM_GG_RIVAL;
        wtgaImpTrasfe.setWtgaImpTrasfeFromBuffer(buffer, position);
        position += WtgaImpTrasfe.Len.WTGA_IMP_TRASFE;
        wtgaImpTfrStrc.setWtgaImpTfrStrcFromBuffer(buffer, position);
        position += WtgaImpTfrStrc.Len.WTGA_IMP_TFR_STRC;
        wtgaAcqExp.setWtgaAcqExpFromBuffer(buffer, position);
        position += WtgaAcqExp.Len.WTGA_ACQ_EXP;
        wtgaRemunAss.setWtgaRemunAssFromBuffer(buffer, position);
        position += WtgaRemunAss.Len.WTGA_REMUN_ASS;
        wtgaCommisInter.setWtgaCommisInterFromBuffer(buffer, position);
        position += WtgaCommisInter.Len.WTGA_COMMIS_INTER;
        wtgaAlqRemunAss.setWtgaAlqRemunAssFromBuffer(buffer, position);
        position += WtgaAlqRemunAss.Len.WTGA_ALQ_REMUN_ASS;
        wtgaAlqCommisInter.setWtgaAlqCommisInterFromBuffer(buffer, position);
        position += WtgaAlqCommisInter.Len.WTGA_ALQ_COMMIS_INTER;
        wtgaImpbRemunAss.setWtgaImpbRemunAssFromBuffer(buffer, position);
        position += WtgaImpbRemunAss.Len.WTGA_IMPB_REMUN_ASS;
        wtgaImpbCommisInter.setWtgaImpbCommisInterFromBuffer(buffer, position);
        position += WtgaImpbCommisInter.Len.WTGA_IMPB_COMMIS_INTER;
        wtgaCosRunAssva.setWtgaCosRunAssvaFromBuffer(buffer, position);
        position += WtgaCosRunAssva.Len.WTGA_COS_RUN_ASSVA;
        wtgaCosRunAssvaIdc.setWtgaCosRunAssvaIdcFromBuffer(buffer, position);
    }

    public byte[] getDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, wtgaIdTrchDiGar, Len.Int.WTGA_ID_TRCH_DI_GAR, 0);
        position += Len.WTGA_ID_TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, wtgaIdGar, Len.Int.WTGA_ID_GAR, 0);
        position += Len.WTGA_ID_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, wtgaIdAdes, Len.Int.WTGA_ID_ADES, 0);
        position += Len.WTGA_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, wtgaIdPoli, Len.Int.WTGA_ID_POLI, 0);
        position += Len.WTGA_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, wtgaIdMoviCrz, Len.Int.WTGA_ID_MOVI_CRZ, 0);
        position += Len.WTGA_ID_MOVI_CRZ;
        wtgaIdMoviChiu.getWtgaIdMoviChiuAsBuffer(buffer, position);
        position += WtgaIdMoviChiu.Len.WTGA_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, wtgaDtIniEff, Len.Int.WTGA_DT_INI_EFF, 0);
        position += Len.WTGA_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wtgaDtEndEff, Len.Int.WTGA_DT_END_EFF, 0);
        position += Len.WTGA_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, wtgaCodCompAnia, Len.Int.WTGA_COD_COMP_ANIA, 0);
        position += Len.WTGA_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, wtgaDtDecor, Len.Int.WTGA_DT_DECOR, 0);
        position += Len.WTGA_DT_DECOR;
        wtgaDtScad.getWtgaDtScadAsBuffer(buffer, position);
        position += WtgaDtScad.Len.WTGA_DT_SCAD;
        MarshalByte.writeString(buffer, position, wtgaIbOgg, Len.WTGA_IB_OGG);
        position += Len.WTGA_IB_OGG;
        MarshalByte.writeString(buffer, position, wtgaTpRgmFisc, Len.WTGA_TP_RGM_FISC);
        position += Len.WTGA_TP_RGM_FISC;
        wtgaDtEmis.getWtgaDtEmisAsBuffer(buffer, position);
        position += WtgaDtEmis.Len.WTGA_DT_EMIS;
        MarshalByte.writeString(buffer, position, wtgaTpTrch, Len.WTGA_TP_TRCH);
        position += Len.WTGA_TP_TRCH;
        wtgaDurAa.getWtgaDurAaAsBuffer(buffer, position);
        position += WtgaDurAa.Len.WTGA_DUR_AA;
        wtgaDurMm.getWtgaDurMmAsBuffer(buffer, position);
        position += WtgaDurMm.Len.WTGA_DUR_MM;
        wtgaDurGg.getWtgaDurGgAsBuffer(buffer, position);
        position += WtgaDurGg.Len.WTGA_DUR_GG;
        wtgaPreCasoMor.getWtgaPreCasoMorAsBuffer(buffer, position);
        position += WtgaPreCasoMor.Len.WTGA_PRE_CASO_MOR;
        wtgaPcIntrRiat.getWtgaPcIntrRiatAsBuffer(buffer, position);
        position += WtgaPcIntrRiat.Len.WTGA_PC_INTR_RIAT;
        wtgaImpBnsAntic.getWtgaImpBnsAnticAsBuffer(buffer, position);
        position += WtgaImpBnsAntic.Len.WTGA_IMP_BNS_ANTIC;
        wtgaPreIniNet.getWtgaPreIniNetAsBuffer(buffer, position);
        position += WtgaPreIniNet.Len.WTGA_PRE_INI_NET;
        wtgaPrePpIni.getWtgaPrePpIniAsBuffer(buffer, position);
        position += WtgaPrePpIni.Len.WTGA_PRE_PP_INI;
        wtgaPrePpUlt.getWtgaPrePpUltAsBuffer(buffer, position);
        position += WtgaPrePpUlt.Len.WTGA_PRE_PP_ULT;
        wtgaPreTariIni.getWtgaPreTariIniAsBuffer(buffer, position);
        position += WtgaPreTariIni.Len.WTGA_PRE_TARI_INI;
        wtgaPreTariUlt.getWtgaPreTariUltAsBuffer(buffer, position);
        position += WtgaPreTariUlt.Len.WTGA_PRE_TARI_ULT;
        wtgaPreInvrioIni.getWtgaPreInvrioIniAsBuffer(buffer, position);
        position += WtgaPreInvrioIni.Len.WTGA_PRE_INVRIO_INI;
        wtgaPreInvrioUlt.getWtgaPreInvrioUltAsBuffer(buffer, position);
        position += WtgaPreInvrioUlt.Len.WTGA_PRE_INVRIO_ULT;
        wtgaPreRivto.getWtgaPreRivtoAsBuffer(buffer, position);
        position += WtgaPreRivto.Len.WTGA_PRE_RIVTO;
        wtgaImpSoprProf.getWtgaImpSoprProfAsBuffer(buffer, position);
        position += WtgaImpSoprProf.Len.WTGA_IMP_SOPR_PROF;
        wtgaImpSoprSan.getWtgaImpSoprSanAsBuffer(buffer, position);
        position += WtgaImpSoprSan.Len.WTGA_IMP_SOPR_SAN;
        wtgaImpSoprSpo.getWtgaImpSoprSpoAsBuffer(buffer, position);
        position += WtgaImpSoprSpo.Len.WTGA_IMP_SOPR_SPO;
        wtgaImpSoprTec.getWtgaImpSoprTecAsBuffer(buffer, position);
        position += WtgaImpSoprTec.Len.WTGA_IMP_SOPR_TEC;
        wtgaImpAltSopr.getWtgaImpAltSoprAsBuffer(buffer, position);
        position += WtgaImpAltSopr.Len.WTGA_IMP_ALT_SOPR;
        wtgaPreStab.getWtgaPreStabAsBuffer(buffer, position);
        position += WtgaPreStab.Len.WTGA_PRE_STAB;
        wtgaDtEffStab.getWtgaDtEffStabAsBuffer(buffer, position);
        position += WtgaDtEffStab.Len.WTGA_DT_EFF_STAB;
        wtgaTsRivalFis.getWtgaTsRivalFisAsBuffer(buffer, position);
        position += WtgaTsRivalFis.Len.WTGA_TS_RIVAL_FIS;
        wtgaTsRivalIndiciz.getWtgaTsRivalIndicizAsBuffer(buffer, position);
        position += WtgaTsRivalIndiciz.Len.WTGA_TS_RIVAL_INDICIZ;
        wtgaOldTsTec.getWtgaOldTsTecAsBuffer(buffer, position);
        position += WtgaOldTsTec.Len.WTGA_OLD_TS_TEC;
        wtgaRatLrd.getWtgaRatLrdAsBuffer(buffer, position);
        position += WtgaRatLrd.Len.WTGA_RAT_LRD;
        wtgaPreLrd.getWtgaPreLrdAsBuffer(buffer, position);
        position += WtgaPreLrd.Len.WTGA_PRE_LRD;
        wtgaPrstzIni.getWtgaPrstzIniAsBuffer(buffer, position);
        position += WtgaPrstzIni.Len.WTGA_PRSTZ_INI;
        wtgaPrstzUlt.getWtgaPrstzUltAsBuffer(buffer, position);
        position += WtgaPrstzUlt.Len.WTGA_PRSTZ_ULT;
        wtgaCptInOpzRivto.getWtgaCptInOpzRivtoAsBuffer(buffer, position);
        position += WtgaCptInOpzRivto.Len.WTGA_CPT_IN_OPZ_RIVTO;
        wtgaPrstzIniStab.getWtgaPrstzIniStabAsBuffer(buffer, position);
        position += WtgaPrstzIniStab.Len.WTGA_PRSTZ_INI_STAB;
        wtgaCptRshMor.getWtgaCptRshMorAsBuffer(buffer, position);
        position += WtgaCptRshMor.Len.WTGA_CPT_RSH_MOR;
        wtgaPrstzRidIni.getWtgaPrstzRidIniAsBuffer(buffer, position);
        position += WtgaPrstzRidIni.Len.WTGA_PRSTZ_RID_INI;
        MarshalByte.writeChar(buffer, position, wtgaFlCarCont);
        position += Types.CHAR_SIZE;
        wtgaBnsGiaLiqto.getWtgaBnsGiaLiqtoAsBuffer(buffer, position);
        position += WtgaBnsGiaLiqto.Len.WTGA_BNS_GIA_LIQTO;
        wtgaImpBns.getWtgaImpBnsAsBuffer(buffer, position);
        position += WtgaImpBns.Len.WTGA_IMP_BNS;
        MarshalByte.writeString(buffer, position, wtgaCodDvs, Len.WTGA_COD_DVS);
        position += Len.WTGA_COD_DVS;
        wtgaPrstzIniNewfis.getWtgaPrstzIniNewfisAsBuffer(buffer, position);
        position += WtgaPrstzIniNewfis.Len.WTGA_PRSTZ_INI_NEWFIS;
        wtgaImpScon.getWtgaImpSconAsBuffer(buffer, position);
        position += WtgaImpScon.Len.WTGA_IMP_SCON;
        wtgaAlqScon.getWtgaAlqSconAsBuffer(buffer, position);
        position += WtgaAlqScon.Len.WTGA_ALQ_SCON;
        wtgaImpCarAcq.getWtgaImpCarAcqAsBuffer(buffer, position);
        position += WtgaImpCarAcq.Len.WTGA_IMP_CAR_ACQ;
        wtgaImpCarInc.getWtgaImpCarIncAsBuffer(buffer, position);
        position += WtgaImpCarInc.Len.WTGA_IMP_CAR_INC;
        wtgaImpCarGest.getWtgaImpCarGestAsBuffer(buffer, position);
        position += WtgaImpCarGest.Len.WTGA_IMP_CAR_GEST;
        wtgaEtaAa1oAssto.getWtgaEtaAa1oAsstoAsBuffer(buffer, position);
        position += WtgaEtaAa1oAssto.Len.WTGA_ETA_AA1O_ASSTO;
        wtgaEtaMm1oAssto.getWtgaEtaMm1oAsstoAsBuffer(buffer, position);
        position += WtgaEtaMm1oAssto.Len.WTGA_ETA_MM1O_ASSTO;
        wtgaEtaAa2oAssto.getWtgaEtaAa2oAsstoAsBuffer(buffer, position);
        position += WtgaEtaAa2oAssto.Len.WTGA_ETA_AA2O_ASSTO;
        wtgaEtaMm2oAssto.getWtgaEtaMm2oAsstoAsBuffer(buffer, position);
        position += WtgaEtaMm2oAssto.Len.WTGA_ETA_MM2O_ASSTO;
        wtgaEtaAa3oAssto.getWtgaEtaAa3oAsstoAsBuffer(buffer, position);
        position += WtgaEtaAa3oAssto.Len.WTGA_ETA_AA3O_ASSTO;
        wtgaEtaMm3oAssto.getWtgaEtaMm3oAsstoAsBuffer(buffer, position);
        position += WtgaEtaMm3oAssto.Len.WTGA_ETA_MM3O_ASSTO;
        wtgaRendtoLrd.getWtgaRendtoLrdAsBuffer(buffer, position);
        position += WtgaRendtoLrd.Len.WTGA_RENDTO_LRD;
        wtgaPcRetr.getWtgaPcRetrAsBuffer(buffer, position);
        position += WtgaPcRetr.Len.WTGA_PC_RETR;
        wtgaRendtoRetr.getWtgaRendtoRetrAsBuffer(buffer, position);
        position += WtgaRendtoRetr.Len.WTGA_RENDTO_RETR;
        wtgaMinGarto.getWtgaMinGartoAsBuffer(buffer, position);
        position += WtgaMinGarto.Len.WTGA_MIN_GARTO;
        wtgaMinTrnut.getWtgaMinTrnutAsBuffer(buffer, position);
        position += WtgaMinTrnut.Len.WTGA_MIN_TRNUT;
        wtgaPreAttDiTrch.getWtgaPreAttDiTrchAsBuffer(buffer, position);
        position += WtgaPreAttDiTrch.Len.WTGA_PRE_ATT_DI_TRCH;
        wtgaMatuEnd2000.getWtgaMatuEnd2000AsBuffer(buffer, position);
        position += WtgaMatuEnd2000.Len.WTGA_MATU_END2000;
        wtgaAbbTotIni.getWtgaAbbTotIniAsBuffer(buffer, position);
        position += WtgaAbbTotIni.Len.WTGA_ABB_TOT_INI;
        wtgaAbbTotUlt.getWtgaAbbTotUltAsBuffer(buffer, position);
        position += WtgaAbbTotUlt.Len.WTGA_ABB_TOT_ULT;
        wtgaAbbAnnuUlt.getWtgaAbbAnnuUltAsBuffer(buffer, position);
        position += WtgaAbbAnnuUlt.Len.WTGA_ABB_ANNU_ULT;
        wtgaDurAbb.getWtgaDurAbbAsBuffer(buffer, position);
        position += WtgaDurAbb.Len.WTGA_DUR_ABB;
        MarshalByte.writeChar(buffer, position, wtgaTpAdegAbb);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, wtgaModCalc, Len.WTGA_MOD_CALC);
        position += Len.WTGA_MOD_CALC;
        wtgaImpAz.getWtgaImpAzAsBuffer(buffer, position);
        position += WtgaImpAz.Len.WTGA_IMP_AZ;
        wtgaImpAder.getWtgaImpAderAsBuffer(buffer, position);
        position += WtgaImpAder.Len.WTGA_IMP_ADER;
        wtgaImpTfr.getWtgaImpTfrAsBuffer(buffer, position);
        position += WtgaImpTfr.Len.WTGA_IMP_TFR;
        wtgaImpVolo.getWtgaImpVoloAsBuffer(buffer, position);
        position += WtgaImpVolo.Len.WTGA_IMP_VOLO;
        wtgaVisEnd2000.getWtgaVisEnd2000AsBuffer(buffer, position);
        position += WtgaVisEnd2000.Len.WTGA_VIS_END2000;
        wtgaDtVldtProd.getWtgaDtVldtProdAsBuffer(buffer, position);
        position += WtgaDtVldtProd.Len.WTGA_DT_VLDT_PROD;
        wtgaDtIniValTar.getWtgaDtIniValTarAsBuffer(buffer, position);
        position += WtgaDtIniValTar.Len.WTGA_DT_INI_VAL_TAR;
        wtgaImpbVisEnd2000.getWtgaImpbVisEnd2000AsBuffer(buffer, position);
        position += WtgaImpbVisEnd2000.Len.WTGA_IMPB_VIS_END2000;
        wtgaRenIniTsTec0.getWtgaRenIniTsTec0AsBuffer(buffer, position);
        position += WtgaRenIniTsTec0.Len.WTGA_REN_INI_TS_TEC0;
        wtgaPcRipPre.getWtgaPcRipPreAsBuffer(buffer, position);
        position += WtgaPcRipPre.Len.WTGA_PC_RIP_PRE;
        MarshalByte.writeChar(buffer, position, wtgaFlImportiForz);
        position += Types.CHAR_SIZE;
        wtgaPrstzIniNforz.getWtgaPrstzIniNforzAsBuffer(buffer, position);
        position += WtgaPrstzIniNforz.Len.WTGA_PRSTZ_INI_NFORZ;
        wtgaVisEnd2000Nforz.getWtgaVisEnd2000NforzAsBuffer(buffer, position);
        position += WtgaVisEnd2000Nforz.Len.WTGA_VIS_END2000_NFORZ;
        wtgaIntrMora.getWtgaIntrMoraAsBuffer(buffer, position);
        position += WtgaIntrMora.Len.WTGA_INTR_MORA;
        wtgaManfeeAntic.getWtgaManfeeAnticAsBuffer(buffer, position);
        position += WtgaManfeeAntic.Len.WTGA_MANFEE_ANTIC;
        wtgaManfeeRicor.getWtgaManfeeRicorAsBuffer(buffer, position);
        position += WtgaManfeeRicor.Len.WTGA_MANFEE_RICOR;
        wtgaPreUniRivto.getWtgaPreUniRivtoAsBuffer(buffer, position);
        position += WtgaPreUniRivto.Len.WTGA_PRE_UNI_RIVTO;
        wtgaProv1aaAcq.getWtgaProv1aaAcqAsBuffer(buffer, position);
        position += WtgaProv1aaAcq.Len.WTGA_PROV1AA_ACQ;
        wtgaProv2aaAcq.getWtgaProv2aaAcqAsBuffer(buffer, position);
        position += WtgaProv2aaAcq.Len.WTGA_PROV2AA_ACQ;
        wtgaProvRicor.getWtgaProvRicorAsBuffer(buffer, position);
        position += WtgaProvRicor.Len.WTGA_PROV_RICOR;
        wtgaProvInc.getWtgaProvIncAsBuffer(buffer, position);
        position += WtgaProvInc.Len.WTGA_PROV_INC;
        wtgaAlqProvAcq.getWtgaAlqProvAcqAsBuffer(buffer, position);
        position += WtgaAlqProvAcq.Len.WTGA_ALQ_PROV_ACQ;
        wtgaAlqProvInc.getWtgaAlqProvIncAsBuffer(buffer, position);
        position += WtgaAlqProvInc.Len.WTGA_ALQ_PROV_INC;
        wtgaAlqProvRicor.getWtgaAlqProvRicorAsBuffer(buffer, position);
        position += WtgaAlqProvRicor.Len.WTGA_ALQ_PROV_RICOR;
        wtgaImpbProvAcq.getWtgaImpbProvAcqAsBuffer(buffer, position);
        position += WtgaImpbProvAcq.Len.WTGA_IMPB_PROV_ACQ;
        wtgaImpbProvInc.getWtgaImpbProvIncAsBuffer(buffer, position);
        position += WtgaImpbProvInc.Len.WTGA_IMPB_PROV_INC;
        wtgaImpbProvRicor.getWtgaImpbProvRicorAsBuffer(buffer, position);
        position += WtgaImpbProvRicor.Len.WTGA_IMPB_PROV_RICOR;
        MarshalByte.writeChar(buffer, position, wtgaFlProvForz);
        position += Types.CHAR_SIZE;
        wtgaPrstzAggIni.getWtgaPrstzAggIniAsBuffer(buffer, position);
        position += WtgaPrstzAggIni.Len.WTGA_PRSTZ_AGG_INI;
        wtgaIncrPre.getWtgaIncrPreAsBuffer(buffer, position);
        position += WtgaIncrPre.Len.WTGA_INCR_PRE;
        wtgaIncrPrstz.getWtgaIncrPrstzAsBuffer(buffer, position);
        position += WtgaIncrPrstz.Len.WTGA_INCR_PRSTZ;
        wtgaDtUltAdegPrePr.getWtgaDtUltAdegPrePrAsBuffer(buffer, position);
        position += WtgaDtUltAdegPrePr.Len.WTGA_DT_ULT_ADEG_PRE_PR;
        wtgaPrstzAggUlt.getWtgaPrstzAggUltAsBuffer(buffer, position);
        position += WtgaPrstzAggUlt.Len.WTGA_PRSTZ_AGG_ULT;
        wtgaTsRivalNet.getWtgaTsRivalNetAsBuffer(buffer, position);
        position += WtgaTsRivalNet.Len.WTGA_TS_RIVAL_NET;
        wtgaPrePattuito.getWtgaPrePattuitoAsBuffer(buffer, position);
        position += WtgaPrePattuito.Len.WTGA_PRE_PATTUITO;
        MarshalByte.writeString(buffer, position, wtgaTpRival, Len.WTGA_TP_RIVAL);
        position += Len.WTGA_TP_RIVAL;
        wtgaRisMat.getWtgaRisMatAsBuffer(buffer, position);
        position += WtgaRisMat.Len.WTGA_RIS_MAT;
        wtgaCptMinScad.getWtgaCptMinScadAsBuffer(buffer, position);
        position += WtgaCptMinScad.Len.WTGA_CPT_MIN_SCAD;
        wtgaCommisGest.getWtgaCommisGestAsBuffer(buffer, position);
        position += WtgaCommisGest.Len.WTGA_COMMIS_GEST;
        MarshalByte.writeString(buffer, position, wtgaTpManfeeAppl, Len.WTGA_TP_MANFEE_APPL);
        position += Len.WTGA_TP_MANFEE_APPL;
        MarshalByte.writeLongAsPacked(buffer, position, wtgaDsRiga, Len.Int.WTGA_DS_RIGA, 0);
        position += Len.WTGA_DS_RIGA;
        MarshalByte.writeChar(buffer, position, wtgaDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, wtgaDsVer, Len.Int.WTGA_DS_VER, 0);
        position += Len.WTGA_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, wtgaDsTsIniCptz, Len.Int.WTGA_DS_TS_INI_CPTZ, 0);
        position += Len.WTGA_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, wtgaDsTsEndCptz, Len.Int.WTGA_DS_TS_END_CPTZ, 0);
        position += Len.WTGA_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, wtgaDsUtente, Len.WTGA_DS_UTENTE);
        position += Len.WTGA_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, wtgaDsStatoElab);
        position += Types.CHAR_SIZE;
        wtgaPcCommisGest.getWtgaPcCommisGestAsBuffer(buffer, position);
        position += WtgaPcCommisGest.Len.WTGA_PC_COMMIS_GEST;
        wtgaNumGgRival.getWtgaNumGgRivalAsBuffer(buffer, position);
        position += WtgaNumGgRival.Len.WTGA_NUM_GG_RIVAL;
        wtgaImpTrasfe.getWtgaImpTrasfeAsBuffer(buffer, position);
        position += WtgaImpTrasfe.Len.WTGA_IMP_TRASFE;
        wtgaImpTfrStrc.getWtgaImpTfrStrcAsBuffer(buffer, position);
        position += WtgaImpTfrStrc.Len.WTGA_IMP_TFR_STRC;
        wtgaAcqExp.getWtgaAcqExpAsBuffer(buffer, position);
        position += WtgaAcqExp.Len.WTGA_ACQ_EXP;
        wtgaRemunAss.getWtgaRemunAssAsBuffer(buffer, position);
        position += WtgaRemunAss.Len.WTGA_REMUN_ASS;
        wtgaCommisInter.getWtgaCommisInterAsBuffer(buffer, position);
        position += WtgaCommisInter.Len.WTGA_COMMIS_INTER;
        wtgaAlqRemunAss.getWtgaAlqRemunAssAsBuffer(buffer, position);
        position += WtgaAlqRemunAss.Len.WTGA_ALQ_REMUN_ASS;
        wtgaAlqCommisInter.getWtgaAlqCommisInterAsBuffer(buffer, position);
        position += WtgaAlqCommisInter.Len.WTGA_ALQ_COMMIS_INTER;
        wtgaImpbRemunAss.getWtgaImpbRemunAssAsBuffer(buffer, position);
        position += WtgaImpbRemunAss.Len.WTGA_IMPB_REMUN_ASS;
        wtgaImpbCommisInter.getWtgaImpbCommisInterAsBuffer(buffer, position);
        position += WtgaImpbCommisInter.Len.WTGA_IMPB_COMMIS_INTER;
        wtgaCosRunAssva.getWtgaCosRunAssvaAsBuffer(buffer, position);
        position += WtgaCosRunAssva.Len.WTGA_COS_RUN_ASSVA;
        wtgaCosRunAssvaIdc.getWtgaCosRunAssvaIdcAsBuffer(buffer, position);
        return buffer;
    }

    public void initDatiSpaces() {
        wtgaIdTrchDiGar = Types.INVALID_INT_VAL;
        wtgaIdGar = Types.INVALID_INT_VAL;
        wtgaIdAdes = Types.INVALID_INT_VAL;
        wtgaIdPoli = Types.INVALID_INT_VAL;
        wtgaIdMoviCrz = Types.INVALID_INT_VAL;
        wtgaIdMoviChiu.initWtgaIdMoviChiuSpaces();
        wtgaDtIniEff = Types.INVALID_INT_VAL;
        wtgaDtEndEff = Types.INVALID_INT_VAL;
        wtgaCodCompAnia = Types.INVALID_INT_VAL;
        wtgaDtDecor = Types.INVALID_INT_VAL;
        wtgaDtScad.initWtgaDtScadSpaces();
        wtgaIbOgg = "";
        wtgaTpRgmFisc = "";
        wtgaDtEmis.initWtgaDtEmisSpaces();
        wtgaTpTrch = "";
        wtgaDurAa.initWtgaDurAaSpaces();
        wtgaDurMm.initWtgaDurMmSpaces();
        wtgaDurGg.initWtgaDurGgSpaces();
        wtgaPreCasoMor.initWtgaPreCasoMorSpaces();
        wtgaPcIntrRiat.initWtgaPcIntrRiatSpaces();
        wtgaImpBnsAntic.initWtgaImpBnsAnticSpaces();
        wtgaPreIniNet.initWtgaPreIniNetSpaces();
        wtgaPrePpIni.initWtgaPrePpIniSpaces();
        wtgaPrePpUlt.initWtgaPrePpUltSpaces();
        wtgaPreTariIni.initWtgaPreTariIniSpaces();
        wtgaPreTariUlt.initWtgaPreTariUltSpaces();
        wtgaPreInvrioIni.initWtgaPreInvrioIniSpaces();
        wtgaPreInvrioUlt.initWtgaPreInvrioUltSpaces();
        wtgaPreRivto.initWtgaPreRivtoSpaces();
        wtgaImpSoprProf.initWtgaImpSoprProfSpaces();
        wtgaImpSoprSan.initWtgaImpSoprSanSpaces();
        wtgaImpSoprSpo.initWtgaImpSoprSpoSpaces();
        wtgaImpSoprTec.initWtgaImpSoprTecSpaces();
        wtgaImpAltSopr.initWtgaImpAltSoprSpaces();
        wtgaPreStab.initWtgaPreStabSpaces();
        wtgaDtEffStab.initWtgaDtEffStabSpaces();
        wtgaTsRivalFis.initWtgaTsRivalFisSpaces();
        wtgaTsRivalIndiciz.initWtgaTsRivalIndicizSpaces();
        wtgaOldTsTec.initWtgaOldTsTecSpaces();
        wtgaRatLrd.initWtgaRatLrdSpaces();
        wtgaPreLrd.initWtgaPreLrdSpaces();
        wtgaPrstzIni.initWtgaPrstzIniSpaces();
        wtgaPrstzUlt.initWtgaPrstzUltSpaces();
        wtgaCptInOpzRivto.initWtgaCptInOpzRivtoSpaces();
        wtgaPrstzIniStab.initWtgaPrstzIniStabSpaces();
        wtgaCptRshMor.initWtgaCptRshMorSpaces();
        wtgaPrstzRidIni.initWtgaPrstzRidIniSpaces();
        wtgaFlCarCont = Types.SPACE_CHAR;
        wtgaBnsGiaLiqto.initWtgaBnsGiaLiqtoSpaces();
        wtgaImpBns.initWtgaImpBnsSpaces();
        wtgaCodDvs = "";
        wtgaPrstzIniNewfis.initWtgaPrstzIniNewfisSpaces();
        wtgaImpScon.initWtgaImpSconSpaces();
        wtgaAlqScon.initWtgaAlqSconSpaces();
        wtgaImpCarAcq.initWtgaImpCarAcqSpaces();
        wtgaImpCarInc.initWtgaImpCarIncSpaces();
        wtgaImpCarGest.initWtgaImpCarGestSpaces();
        wtgaEtaAa1oAssto.initWtgaEtaAa1oAsstoSpaces();
        wtgaEtaMm1oAssto.initWtgaEtaMm1oAsstoSpaces();
        wtgaEtaAa2oAssto.initWtgaEtaAa2oAsstoSpaces();
        wtgaEtaMm2oAssto.initWtgaEtaMm2oAsstoSpaces();
        wtgaEtaAa3oAssto.initWtgaEtaAa3oAsstoSpaces();
        wtgaEtaMm3oAssto.initWtgaEtaMm3oAsstoSpaces();
        wtgaRendtoLrd.initWtgaRendtoLrdSpaces();
        wtgaPcRetr.initWtgaPcRetrSpaces();
        wtgaRendtoRetr.initWtgaRendtoRetrSpaces();
        wtgaMinGarto.initWtgaMinGartoSpaces();
        wtgaMinTrnut.initWtgaMinTrnutSpaces();
        wtgaPreAttDiTrch.initWtgaPreAttDiTrchSpaces();
        wtgaMatuEnd2000.initWtgaMatuEnd2000Spaces();
        wtgaAbbTotIni.initWtgaAbbTotIniSpaces();
        wtgaAbbTotUlt.initWtgaAbbTotUltSpaces();
        wtgaAbbAnnuUlt.initWtgaAbbAnnuUltSpaces();
        wtgaDurAbb.initWtgaDurAbbSpaces();
        wtgaTpAdegAbb = Types.SPACE_CHAR;
        wtgaModCalc = "";
        wtgaImpAz.initWtgaImpAzSpaces();
        wtgaImpAder.initWtgaImpAderSpaces();
        wtgaImpTfr.initWtgaImpTfrSpaces();
        wtgaImpVolo.initWtgaImpVoloSpaces();
        wtgaVisEnd2000.initWtgaVisEnd2000Spaces();
        wtgaDtVldtProd.initWtgaDtVldtProdSpaces();
        wtgaDtIniValTar.initWtgaDtIniValTarSpaces();
        wtgaImpbVisEnd2000.initWtgaImpbVisEnd2000Spaces();
        wtgaRenIniTsTec0.initWtgaRenIniTsTec0Spaces();
        wtgaPcRipPre.initWtgaPcRipPreSpaces();
        wtgaFlImportiForz = Types.SPACE_CHAR;
        wtgaPrstzIniNforz.initWtgaPrstzIniNforzSpaces();
        wtgaVisEnd2000Nforz.initWtgaVisEnd2000NforzSpaces();
        wtgaIntrMora.initWtgaIntrMoraSpaces();
        wtgaManfeeAntic.initWtgaManfeeAnticSpaces();
        wtgaManfeeRicor.initWtgaManfeeRicorSpaces();
        wtgaPreUniRivto.initWtgaPreUniRivtoSpaces();
        wtgaProv1aaAcq.initWtgaProv1aaAcqSpaces();
        wtgaProv2aaAcq.initWtgaProv2aaAcqSpaces();
        wtgaProvRicor.initWtgaProvRicorSpaces();
        wtgaProvInc.initWtgaProvIncSpaces();
        wtgaAlqProvAcq.initWtgaAlqProvAcqSpaces();
        wtgaAlqProvInc.initWtgaAlqProvIncSpaces();
        wtgaAlqProvRicor.initWtgaAlqProvRicorSpaces();
        wtgaImpbProvAcq.initWtgaImpbProvAcqSpaces();
        wtgaImpbProvInc.initWtgaImpbProvIncSpaces();
        wtgaImpbProvRicor.initWtgaImpbProvRicorSpaces();
        wtgaFlProvForz = Types.SPACE_CHAR;
        wtgaPrstzAggIni.initWtgaPrstzAggIniSpaces();
        wtgaIncrPre.initWtgaIncrPreSpaces();
        wtgaIncrPrstz.initWtgaIncrPrstzSpaces();
        wtgaDtUltAdegPrePr.initWtgaDtUltAdegPrePrSpaces();
        wtgaPrstzAggUlt.initWtgaPrstzAggUltSpaces();
        wtgaTsRivalNet.initWtgaTsRivalNetSpaces();
        wtgaPrePattuito.initWtgaPrePattuitoSpaces();
        wtgaTpRival = "";
        wtgaRisMat.initWtgaRisMatSpaces();
        wtgaCptMinScad.initWtgaCptMinScadSpaces();
        wtgaCommisGest.initWtgaCommisGestSpaces();
        wtgaTpManfeeAppl = "";
        wtgaDsRiga = Types.INVALID_LONG_VAL;
        wtgaDsOperSql = Types.SPACE_CHAR;
        wtgaDsVer = Types.INVALID_INT_VAL;
        wtgaDsTsIniCptz = Types.INVALID_LONG_VAL;
        wtgaDsTsEndCptz = Types.INVALID_LONG_VAL;
        wtgaDsUtente = "";
        wtgaDsStatoElab = Types.SPACE_CHAR;
        wtgaPcCommisGest.initWtgaPcCommisGestSpaces();
        wtgaNumGgRival.initWtgaNumGgRivalSpaces();
        wtgaImpTrasfe.initWtgaImpTrasfeSpaces();
        wtgaImpTfrStrc.initWtgaImpTfrStrcSpaces();
        wtgaAcqExp.initWtgaAcqExpSpaces();
        wtgaRemunAss.initWtgaRemunAssSpaces();
        wtgaCommisInter.initWtgaCommisInterSpaces();
        wtgaAlqRemunAss.initWtgaAlqRemunAssSpaces();
        wtgaAlqCommisInter.initWtgaAlqCommisInterSpaces();
        wtgaImpbRemunAss.initWtgaImpbRemunAssSpaces();
        wtgaImpbCommisInter.initWtgaImpbCommisInterSpaces();
        wtgaCosRunAssva.initWtgaCosRunAssvaSpaces();
        wtgaCosRunAssvaIdc.initWtgaCosRunAssvaIdcSpaces();
    }

    public void setWtgaIdTrchDiGar(int wtgaIdTrchDiGar) {
        this.wtgaIdTrchDiGar = wtgaIdTrchDiGar;
    }

    public int getWtgaIdTrchDiGar() {
        return this.wtgaIdTrchDiGar;
    }

    public void setWtgaIdGar(int wtgaIdGar) {
        this.wtgaIdGar = wtgaIdGar;
    }

    public int getWtgaIdGar() {
        return this.wtgaIdGar;
    }

    public void setWtgaIdAdes(int wtgaIdAdes) {
        this.wtgaIdAdes = wtgaIdAdes;
    }

    public int getWtgaIdAdes() {
        return this.wtgaIdAdes;
    }

    public void setWtgaIdPoli(int wtgaIdPoli) {
        this.wtgaIdPoli = wtgaIdPoli;
    }

    public int getWtgaIdPoli() {
        return this.wtgaIdPoli;
    }

    public void setWtgaIdMoviCrz(int wtgaIdMoviCrz) {
        this.wtgaIdMoviCrz = wtgaIdMoviCrz;
    }

    public int getWtgaIdMoviCrz() {
        return this.wtgaIdMoviCrz;
    }

    public void setWtgaDtIniEff(int wtgaDtIniEff) {
        this.wtgaDtIniEff = wtgaDtIniEff;
    }

    public int getWtgaDtIniEff() {
        return this.wtgaDtIniEff;
    }

    public void setWtgaDtEndEff(int wtgaDtEndEff) {
        this.wtgaDtEndEff = wtgaDtEndEff;
    }

    public int getWtgaDtEndEff() {
        return this.wtgaDtEndEff;
    }

    public void setWtgaCodCompAnia(int wtgaCodCompAnia) {
        this.wtgaCodCompAnia = wtgaCodCompAnia;
    }

    public int getWtgaCodCompAnia() {
        return this.wtgaCodCompAnia;
    }

    public void setWtgaDtDecor(int wtgaDtDecor) {
        this.wtgaDtDecor = wtgaDtDecor;
    }

    public int getWtgaDtDecor() {
        return this.wtgaDtDecor;
    }

    public String getWtgaDtDecorFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getWtgaDtDecor()).toString();
    }

    public void setWtgaIbOgg(String wtgaIbOgg) {
        this.wtgaIbOgg = Functions.subString(wtgaIbOgg, Len.WTGA_IB_OGG);
    }

    public String getWtgaIbOgg() {
        return this.wtgaIbOgg;
    }

    public void setWtgaTpRgmFisc(String wtgaTpRgmFisc) {
        this.wtgaTpRgmFisc = Functions.subString(wtgaTpRgmFisc, Len.WTGA_TP_RGM_FISC);
    }

    public String getWtgaTpRgmFisc() {
        return this.wtgaTpRgmFisc;
    }

    public void setWtgaTpTrch(String wtgaTpTrch) {
        this.wtgaTpTrch = Functions.subString(wtgaTpTrch, Len.WTGA_TP_TRCH);
    }

    public String getWtgaTpTrch() {
        return this.wtgaTpTrch;
    }

    public String getDtgaTpTrchFormatted() {
        return Functions.padBlanks(getWtgaTpTrch(), Len.WTGA_TP_TRCH);
    }

    public void setWtgaFlCarCont(char wtgaFlCarCont) {
        this.wtgaFlCarCont = wtgaFlCarCont;
    }

    public char getWtgaFlCarCont() {
        return this.wtgaFlCarCont;
    }

    public void setWtgaCodDvs(String wtgaCodDvs) {
        this.wtgaCodDvs = Functions.subString(wtgaCodDvs, Len.WTGA_COD_DVS);
    }

    public String getWtgaCodDvs() {
        return this.wtgaCodDvs;
    }

    public void setWtgaTpAdegAbb(char wtgaTpAdegAbb) {
        this.wtgaTpAdegAbb = wtgaTpAdegAbb;
    }

    public char getWtgaTpAdegAbb() {
        return this.wtgaTpAdegAbb;
    }

    public void setWtgaModCalc(String wtgaModCalc) {
        this.wtgaModCalc = Functions.subString(wtgaModCalc, Len.WTGA_MOD_CALC);
    }

    public String getWtgaModCalc() {
        return this.wtgaModCalc;
    }

    public String getWtgaModCalcFormatted() {
        return Functions.padBlanks(getWtgaModCalc(), Len.WTGA_MOD_CALC);
    }

    public void setWtgaFlImportiForz(char wtgaFlImportiForz) {
        this.wtgaFlImportiForz = wtgaFlImportiForz;
    }

    public char getWtgaFlImportiForz() {
        return this.wtgaFlImportiForz;
    }

    public void setWtgaFlProvForz(char wtgaFlProvForz) {
        this.wtgaFlProvForz = wtgaFlProvForz;
    }

    public char getWtgaFlProvForz() {
        return this.wtgaFlProvForz;
    }

    public void setWtgaTpRival(String wtgaTpRival) {
        this.wtgaTpRival = Functions.subString(wtgaTpRival, Len.WTGA_TP_RIVAL);
    }

    public String getWtgaTpRival() {
        return this.wtgaTpRival;
    }

    public String getWtgaTpRivalFormatted() {
        return Functions.padBlanks(getWtgaTpRival(), Len.WTGA_TP_RIVAL);
    }

    public void setWtgaTpManfeeAppl(String wtgaTpManfeeAppl) {
        this.wtgaTpManfeeAppl = Functions.subString(wtgaTpManfeeAppl, Len.WTGA_TP_MANFEE_APPL);
    }

    public String getWtgaTpManfeeAppl() {
        return this.wtgaTpManfeeAppl;
    }

    public String getWtgaTpManfeeApplFormatted() {
        return Functions.padBlanks(getWtgaTpManfeeAppl(), Len.WTGA_TP_MANFEE_APPL);
    }

    public void setWtgaDsRiga(long wtgaDsRiga) {
        this.wtgaDsRiga = wtgaDsRiga;
    }

    public long getWtgaDsRiga() {
        return this.wtgaDsRiga;
    }

    public void setWtgaDsOperSql(char wtgaDsOperSql) {
        this.wtgaDsOperSql = wtgaDsOperSql;
    }

    public char getWtgaDsOperSql() {
        return this.wtgaDsOperSql;
    }

    public void setWtgaDsVer(int wtgaDsVer) {
        this.wtgaDsVer = wtgaDsVer;
    }

    public int getWtgaDsVer() {
        return this.wtgaDsVer;
    }

    public void setWtgaDsTsIniCptz(long wtgaDsTsIniCptz) {
        this.wtgaDsTsIniCptz = wtgaDsTsIniCptz;
    }

    public long getWtgaDsTsIniCptz() {
        return this.wtgaDsTsIniCptz;
    }

    public void setWtgaDsTsEndCptz(long wtgaDsTsEndCptz) {
        this.wtgaDsTsEndCptz = wtgaDsTsEndCptz;
    }

    public long getWtgaDsTsEndCptz() {
        return this.wtgaDsTsEndCptz;
    }

    public void setWtgaDsUtente(String wtgaDsUtente) {
        this.wtgaDsUtente = Functions.subString(wtgaDsUtente, Len.WTGA_DS_UTENTE);
    }

    public String getWtgaDsUtente() {
        return this.wtgaDsUtente;
    }

    public void setWtgaDsStatoElab(char wtgaDsStatoElab) {
        this.wtgaDsStatoElab = wtgaDsStatoElab;
    }

    public char getWtgaDsStatoElab() {
        return this.wtgaDsStatoElab;
    }

    public WtgaAbbAnnuUlt getWtgaAbbAnnuUlt() {
        return wtgaAbbAnnuUlt;
    }

    public WtgaAbbTotIni getWtgaAbbTotIni() {
        return wtgaAbbTotIni;
    }

    public WtgaAbbTotUlt getWtgaAbbTotUlt() {
        return wtgaAbbTotUlt;
    }

    public WtgaAcqExp getWtgaAcqExp() {
        return wtgaAcqExp;
    }

    public WtgaAlqCommisInter getWtgaAlqCommisInter() {
        return wtgaAlqCommisInter;
    }

    public WtgaAlqProvAcq getWtgaAlqProvAcq() {
        return wtgaAlqProvAcq;
    }

    public WtgaAlqProvInc getWtgaAlqProvInc() {
        return wtgaAlqProvInc;
    }

    public WtgaAlqProvRicor getWtgaAlqProvRicor() {
        return wtgaAlqProvRicor;
    }

    public WtgaAlqRemunAss getWtgaAlqRemunAss() {
        return wtgaAlqRemunAss;
    }

    public WtgaAlqScon getWtgaAlqScon() {
        return wtgaAlqScon;
    }

    public WtgaBnsGiaLiqto getWtgaBnsGiaLiqto() {
        return wtgaBnsGiaLiqto;
    }

    public WtgaCommisGest getWtgaCommisGest() {
        return wtgaCommisGest;
    }

    public WtgaCommisInter getWtgaCommisInter() {
        return wtgaCommisInter;
    }

    public WtgaCosRunAssva getWtgaCosRunAssva() {
        return wtgaCosRunAssva;
    }

    public WtgaCosRunAssvaIdc getWtgaCosRunAssvaIdc() {
        return wtgaCosRunAssvaIdc;
    }

    public WtgaCptInOpzRivto getWtgaCptInOpzRivto() {
        return wtgaCptInOpzRivto;
    }

    public WtgaCptMinScad getWtgaCptMinScad() {
        return wtgaCptMinScad;
    }

    public WtgaCptRshMor getWtgaCptRshMor() {
        return wtgaCptRshMor;
    }

    public WtgaDtEffStab getWtgaDtEffStab() {
        return wtgaDtEffStab;
    }

    public WtgaDtEmis getWtgaDtEmis() {
        return wtgaDtEmis;
    }

    public WtgaDtIniValTar getWtgaDtIniValTar() {
        return wtgaDtIniValTar;
    }

    public WtgaDtScad getWtgaDtScad() {
        return wtgaDtScad;
    }

    public WtgaDtUltAdegPrePr getWtgaDtUltAdegPrePr() {
        return wtgaDtUltAdegPrePr;
    }

    public WtgaDtVldtProd getWtgaDtVldtProd() {
        return wtgaDtVldtProd;
    }

    public WtgaDurAa getWtgaDurAa() {
        return wtgaDurAa;
    }

    public WtgaDurAbb getWtgaDurAbb() {
        return wtgaDurAbb;
    }

    public WtgaDurGg getWtgaDurGg() {
        return wtgaDurGg;
    }

    public WtgaDurMm getWtgaDurMm() {
        return wtgaDurMm;
    }

    public WtgaEtaAa1oAssto getWtgaEtaAa1oAssto() {
        return wtgaEtaAa1oAssto;
    }

    public WtgaEtaAa2oAssto getWtgaEtaAa2oAssto() {
        return wtgaEtaAa2oAssto;
    }

    public WtgaEtaAa3oAssto getWtgaEtaAa3oAssto() {
        return wtgaEtaAa3oAssto;
    }

    public WtgaEtaMm1oAssto getWtgaEtaMm1oAssto() {
        return wtgaEtaMm1oAssto;
    }

    public WtgaEtaMm2oAssto getWtgaEtaMm2oAssto() {
        return wtgaEtaMm2oAssto;
    }

    public WtgaEtaMm3oAssto getWtgaEtaMm3oAssto() {
        return wtgaEtaMm3oAssto;
    }

    public WtgaIdMoviChiu getWtgaIdMoviChiu() {
        return wtgaIdMoviChiu;
    }

    public WtgaImpAder getWtgaImpAder() {
        return wtgaImpAder;
    }

    public WtgaImpAltSopr getWtgaImpAltSopr() {
        return wtgaImpAltSopr;
    }

    public WtgaImpAz getWtgaImpAz() {
        return wtgaImpAz;
    }

    public WtgaImpBns getWtgaImpBns() {
        return wtgaImpBns;
    }

    public WtgaImpBnsAntic getWtgaImpBnsAntic() {
        return wtgaImpBnsAntic;
    }

    public WtgaImpCarAcq getWtgaImpCarAcq() {
        return wtgaImpCarAcq;
    }

    public WtgaImpCarGest getWtgaImpCarGest() {
        return wtgaImpCarGest;
    }

    public WtgaImpCarInc getWtgaImpCarInc() {
        return wtgaImpCarInc;
    }

    public WtgaImpScon getWtgaImpScon() {
        return wtgaImpScon;
    }

    public WtgaImpSoprProf getWtgaImpSoprProf() {
        return wtgaImpSoprProf;
    }

    public WtgaImpSoprSan getWtgaImpSoprSan() {
        return wtgaImpSoprSan;
    }

    public WtgaImpSoprSpo getWtgaImpSoprSpo() {
        return wtgaImpSoprSpo;
    }

    public WtgaImpSoprTec getWtgaImpSoprTec() {
        return wtgaImpSoprTec;
    }

    public WtgaImpTfr getWtgaImpTfr() {
        return wtgaImpTfr;
    }

    public WtgaImpTfrStrc getWtgaImpTfrStrc() {
        return wtgaImpTfrStrc;
    }

    public WtgaImpTrasfe getWtgaImpTrasfe() {
        return wtgaImpTrasfe;
    }

    public WtgaImpVolo getWtgaImpVolo() {
        return wtgaImpVolo;
    }

    public WtgaImpbCommisInter getWtgaImpbCommisInter() {
        return wtgaImpbCommisInter;
    }

    public WtgaImpbProvAcq getWtgaImpbProvAcq() {
        return wtgaImpbProvAcq;
    }

    public WtgaImpbProvInc getWtgaImpbProvInc() {
        return wtgaImpbProvInc;
    }

    public WtgaImpbProvRicor getWtgaImpbProvRicor() {
        return wtgaImpbProvRicor;
    }

    public WtgaImpbRemunAss getWtgaImpbRemunAss() {
        return wtgaImpbRemunAss;
    }

    public WtgaImpbVisEnd2000 getWtgaImpbVisEnd2000() {
        return wtgaImpbVisEnd2000;
    }

    public WtgaIncrPre getWtgaIncrPre() {
        return wtgaIncrPre;
    }

    public WtgaIncrPrstz getWtgaIncrPrstz() {
        return wtgaIncrPrstz;
    }

    public WtgaIntrMora getWtgaIntrMora() {
        return wtgaIntrMora;
    }

    public WtgaManfeeAntic getWtgaManfeeAntic() {
        return wtgaManfeeAntic;
    }

    public WtgaManfeeRicor getWtgaManfeeRicor() {
        return wtgaManfeeRicor;
    }

    public WtgaMatuEnd2000 getWtgaMatuEnd2000() {
        return wtgaMatuEnd2000;
    }

    public WtgaMinGarto getWtgaMinGarto() {
        return wtgaMinGarto;
    }

    public WtgaMinTrnut getWtgaMinTrnut() {
        return wtgaMinTrnut;
    }

    public WtgaNumGgRival getWtgaNumGgRival() {
        return wtgaNumGgRival;
    }

    public WtgaOldTsTec getWtgaOldTsTec() {
        return wtgaOldTsTec;
    }

    public WtgaPcCommisGest getWtgaPcCommisGest() {
        return wtgaPcCommisGest;
    }

    public WtgaPcIntrRiat getWtgaPcIntrRiat() {
        return wtgaPcIntrRiat;
    }

    public WtgaPcRetr getWtgaPcRetr() {
        return wtgaPcRetr;
    }

    public WtgaPcRipPre getWtgaPcRipPre() {
        return wtgaPcRipPre;
    }

    public WtgaPreAttDiTrch getWtgaPreAttDiTrch() {
        return wtgaPreAttDiTrch;
    }

    public WtgaPreCasoMor getWtgaPreCasoMor() {
        return wtgaPreCasoMor;
    }

    public WtgaPreIniNet getWtgaPreIniNet() {
        return wtgaPreIniNet;
    }

    public WtgaPreInvrioIni getWtgaPreInvrioIni() {
        return wtgaPreInvrioIni;
    }

    public WtgaPreInvrioUlt getWtgaPreInvrioUlt() {
        return wtgaPreInvrioUlt;
    }

    public WtgaPreLrd getWtgaPreLrd() {
        return wtgaPreLrd;
    }

    public WtgaPrePattuito getWtgaPrePattuito() {
        return wtgaPrePattuito;
    }

    public WtgaPrePpIni getWtgaPrePpIni() {
        return wtgaPrePpIni;
    }

    public WtgaPrePpUlt getWtgaPrePpUlt() {
        return wtgaPrePpUlt;
    }

    public WtgaPreRivto getWtgaPreRivto() {
        return wtgaPreRivto;
    }

    public WtgaPreStab getWtgaPreStab() {
        return wtgaPreStab;
    }

    public WtgaPreTariIni getWtgaPreTariIni() {
        return wtgaPreTariIni;
    }

    public WtgaPreTariUlt getWtgaPreTariUlt() {
        return wtgaPreTariUlt;
    }

    public WtgaPreUniRivto getWtgaPreUniRivto() {
        return wtgaPreUniRivto;
    }

    public WtgaProv1aaAcq getWtgaProv1aaAcq() {
        return wtgaProv1aaAcq;
    }

    public WtgaProv2aaAcq getWtgaProv2aaAcq() {
        return wtgaProv2aaAcq;
    }

    public WtgaProvInc getWtgaProvInc() {
        return wtgaProvInc;
    }

    public WtgaProvRicor getWtgaProvRicor() {
        return wtgaProvRicor;
    }

    public WtgaPrstzAggIni getWtgaPrstzAggIni() {
        return wtgaPrstzAggIni;
    }

    public WtgaPrstzAggUlt getWtgaPrstzAggUlt() {
        return wtgaPrstzAggUlt;
    }

    public WtgaPrstzIni getWtgaPrstzIni() {
        return wtgaPrstzIni;
    }

    public WtgaPrstzIniNewfis getWtgaPrstzIniNewfis() {
        return wtgaPrstzIniNewfis;
    }

    public WtgaPrstzIniNforz getWtgaPrstzIniNforz() {
        return wtgaPrstzIniNforz;
    }

    public WtgaPrstzIniStab getWtgaPrstzIniStab() {
        return wtgaPrstzIniStab;
    }

    public WtgaPrstzRidIni getWtgaPrstzRidIni() {
        return wtgaPrstzRidIni;
    }

    public WtgaPrstzUlt getWtgaPrstzUlt() {
        return wtgaPrstzUlt;
    }

    public WtgaRatLrd getWtgaRatLrd() {
        return wtgaRatLrd;
    }

    public WtgaRemunAss getWtgaRemunAss() {
        return wtgaRemunAss;
    }

    public WtgaRenIniTsTec0 getWtgaRenIniTsTec0() {
        return wtgaRenIniTsTec0;
    }

    public WtgaRendtoLrd getWtgaRendtoLrd() {
        return wtgaRendtoLrd;
    }

    public WtgaRendtoRetr getWtgaRendtoRetr() {
        return wtgaRendtoRetr;
    }

    public WtgaRisMat getWtgaRisMat() {
        return wtgaRisMat;
    }

    public WtgaTsRivalFis getWtgaTsRivalFis() {
        return wtgaTsRivalFis;
    }

    public WtgaTsRivalIndiciz getWtgaTsRivalIndiciz() {
        return wtgaTsRivalIndiciz;
    }

    public WtgaTsRivalNet getWtgaTsRivalNet() {
        return wtgaTsRivalNet;
    }

    public WtgaVisEnd2000 getWtgaVisEnd2000() {
        return wtgaVisEnd2000;
    }

    public WtgaVisEnd2000Nforz getWtgaVisEnd2000Nforz() {
        return wtgaVisEnd2000Nforz;
    }

    public WtgaDati copy() {
        return new WtgaDati(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ID_TRCH_DI_GAR = 5;
        public static final int WTGA_ID_GAR = 5;
        public static final int WTGA_ID_ADES = 5;
        public static final int WTGA_ID_POLI = 5;
        public static final int WTGA_ID_MOVI_CRZ = 5;
        public static final int WTGA_DT_INI_EFF = 5;
        public static final int WTGA_DT_END_EFF = 5;
        public static final int WTGA_COD_COMP_ANIA = 3;
        public static final int WTGA_DT_DECOR = 5;
        public static final int WTGA_IB_OGG = 40;
        public static final int WTGA_TP_RGM_FISC = 2;
        public static final int WTGA_TP_TRCH = 2;
        public static final int WTGA_FL_CAR_CONT = 1;
        public static final int WTGA_COD_DVS = 20;
        public static final int WTGA_TP_ADEG_ABB = 1;
        public static final int WTGA_MOD_CALC = 2;
        public static final int WTGA_FL_IMPORTI_FORZ = 1;
        public static final int WTGA_FL_PROV_FORZ = 1;
        public static final int WTGA_TP_RIVAL = 2;
        public static final int WTGA_TP_MANFEE_APPL = 2;
        public static final int WTGA_DS_RIGA = 6;
        public static final int WTGA_DS_OPER_SQL = 1;
        public static final int WTGA_DS_VER = 5;
        public static final int WTGA_DS_TS_INI_CPTZ = 10;
        public static final int WTGA_DS_TS_END_CPTZ = 10;
        public static final int WTGA_DS_UTENTE = 20;
        public static final int WTGA_DS_STATO_ELAB = 1;
        public static final int DATI = WTGA_ID_TRCH_DI_GAR + WTGA_ID_GAR + WTGA_ID_ADES + WTGA_ID_POLI + WTGA_ID_MOVI_CRZ + WtgaIdMoviChiu.Len.WTGA_ID_MOVI_CHIU + WTGA_DT_INI_EFF + WTGA_DT_END_EFF + WTGA_COD_COMP_ANIA + WTGA_DT_DECOR + WtgaDtScad.Len.WTGA_DT_SCAD + WTGA_IB_OGG + WTGA_TP_RGM_FISC + WtgaDtEmis.Len.WTGA_DT_EMIS + WTGA_TP_TRCH + WtgaDurAa.Len.WTGA_DUR_AA + WtgaDurMm.Len.WTGA_DUR_MM + WtgaDurGg.Len.WTGA_DUR_GG + WtgaPreCasoMor.Len.WTGA_PRE_CASO_MOR + WtgaPcIntrRiat.Len.WTGA_PC_INTR_RIAT + WtgaImpBnsAntic.Len.WTGA_IMP_BNS_ANTIC + WtgaPreIniNet.Len.WTGA_PRE_INI_NET + WtgaPrePpIni.Len.WTGA_PRE_PP_INI + WtgaPrePpUlt.Len.WTGA_PRE_PP_ULT + WtgaPreTariIni.Len.WTGA_PRE_TARI_INI + WtgaPreTariUlt.Len.WTGA_PRE_TARI_ULT + WtgaPreInvrioIni.Len.WTGA_PRE_INVRIO_INI + WtgaPreInvrioUlt.Len.WTGA_PRE_INVRIO_ULT + WtgaPreRivto.Len.WTGA_PRE_RIVTO + WtgaImpSoprProf.Len.WTGA_IMP_SOPR_PROF + WtgaImpSoprSan.Len.WTGA_IMP_SOPR_SAN + WtgaImpSoprSpo.Len.WTGA_IMP_SOPR_SPO + WtgaImpSoprTec.Len.WTGA_IMP_SOPR_TEC + WtgaImpAltSopr.Len.WTGA_IMP_ALT_SOPR + WtgaPreStab.Len.WTGA_PRE_STAB + WtgaDtEffStab.Len.WTGA_DT_EFF_STAB + WtgaTsRivalFis.Len.WTGA_TS_RIVAL_FIS + WtgaTsRivalIndiciz.Len.WTGA_TS_RIVAL_INDICIZ + WtgaOldTsTec.Len.WTGA_OLD_TS_TEC + WtgaRatLrd.Len.WTGA_RAT_LRD + WtgaPreLrd.Len.WTGA_PRE_LRD + WtgaPrstzIni.Len.WTGA_PRSTZ_INI + WtgaPrstzUlt.Len.WTGA_PRSTZ_ULT + WtgaCptInOpzRivto.Len.WTGA_CPT_IN_OPZ_RIVTO + WtgaPrstzIniStab.Len.WTGA_PRSTZ_INI_STAB + WtgaCptRshMor.Len.WTGA_CPT_RSH_MOR + WtgaPrstzRidIni.Len.WTGA_PRSTZ_RID_INI + WTGA_FL_CAR_CONT + WtgaBnsGiaLiqto.Len.WTGA_BNS_GIA_LIQTO + WtgaImpBns.Len.WTGA_IMP_BNS + WTGA_COD_DVS + WtgaPrstzIniNewfis.Len.WTGA_PRSTZ_INI_NEWFIS + WtgaImpScon.Len.WTGA_IMP_SCON + WtgaAlqScon.Len.WTGA_ALQ_SCON + WtgaImpCarAcq.Len.WTGA_IMP_CAR_ACQ + WtgaImpCarInc.Len.WTGA_IMP_CAR_INC + WtgaImpCarGest.Len.WTGA_IMP_CAR_GEST + WtgaEtaAa1oAssto.Len.WTGA_ETA_AA1O_ASSTO + WtgaEtaMm1oAssto.Len.WTGA_ETA_MM1O_ASSTO + WtgaEtaAa2oAssto.Len.WTGA_ETA_AA2O_ASSTO + WtgaEtaMm2oAssto.Len.WTGA_ETA_MM2O_ASSTO + WtgaEtaAa3oAssto.Len.WTGA_ETA_AA3O_ASSTO + WtgaEtaMm3oAssto.Len.WTGA_ETA_MM3O_ASSTO + WtgaRendtoLrd.Len.WTGA_RENDTO_LRD + WtgaPcRetr.Len.WTGA_PC_RETR + WtgaRendtoRetr.Len.WTGA_RENDTO_RETR + WtgaMinGarto.Len.WTGA_MIN_GARTO + WtgaMinTrnut.Len.WTGA_MIN_TRNUT + WtgaPreAttDiTrch.Len.WTGA_PRE_ATT_DI_TRCH + WtgaMatuEnd2000.Len.WTGA_MATU_END2000 + WtgaAbbTotIni.Len.WTGA_ABB_TOT_INI + WtgaAbbTotUlt.Len.WTGA_ABB_TOT_ULT + WtgaAbbAnnuUlt.Len.WTGA_ABB_ANNU_ULT + WtgaDurAbb.Len.WTGA_DUR_ABB + WTGA_TP_ADEG_ABB + WTGA_MOD_CALC + WtgaImpAz.Len.WTGA_IMP_AZ + WtgaImpAder.Len.WTGA_IMP_ADER + WtgaImpTfr.Len.WTGA_IMP_TFR + WtgaImpVolo.Len.WTGA_IMP_VOLO + WtgaVisEnd2000.Len.WTGA_VIS_END2000 + WtgaDtVldtProd.Len.WTGA_DT_VLDT_PROD + WtgaDtIniValTar.Len.WTGA_DT_INI_VAL_TAR + WtgaImpbVisEnd2000.Len.WTGA_IMPB_VIS_END2000 + WtgaRenIniTsTec0.Len.WTGA_REN_INI_TS_TEC0 + WtgaPcRipPre.Len.WTGA_PC_RIP_PRE + WTGA_FL_IMPORTI_FORZ + WtgaPrstzIniNforz.Len.WTGA_PRSTZ_INI_NFORZ + WtgaVisEnd2000Nforz.Len.WTGA_VIS_END2000_NFORZ + WtgaIntrMora.Len.WTGA_INTR_MORA + WtgaManfeeAntic.Len.WTGA_MANFEE_ANTIC + WtgaManfeeRicor.Len.WTGA_MANFEE_RICOR + WtgaPreUniRivto.Len.WTGA_PRE_UNI_RIVTO + WtgaProv1aaAcq.Len.WTGA_PROV1AA_ACQ + WtgaProv2aaAcq.Len.WTGA_PROV2AA_ACQ + WtgaProvRicor.Len.WTGA_PROV_RICOR + WtgaProvInc.Len.WTGA_PROV_INC + WtgaAlqProvAcq.Len.WTGA_ALQ_PROV_ACQ + WtgaAlqProvInc.Len.WTGA_ALQ_PROV_INC + WtgaAlqProvRicor.Len.WTGA_ALQ_PROV_RICOR + WtgaImpbProvAcq.Len.WTGA_IMPB_PROV_ACQ + WtgaImpbProvInc.Len.WTGA_IMPB_PROV_INC + WtgaImpbProvRicor.Len.WTGA_IMPB_PROV_RICOR + WTGA_FL_PROV_FORZ + WtgaPrstzAggIni.Len.WTGA_PRSTZ_AGG_INI + WtgaIncrPre.Len.WTGA_INCR_PRE + WtgaIncrPrstz.Len.WTGA_INCR_PRSTZ + WtgaDtUltAdegPrePr.Len.WTGA_DT_ULT_ADEG_PRE_PR + WtgaPrstzAggUlt.Len.WTGA_PRSTZ_AGG_ULT + WtgaTsRivalNet.Len.WTGA_TS_RIVAL_NET + WtgaPrePattuito.Len.WTGA_PRE_PATTUITO + WTGA_TP_RIVAL + WtgaRisMat.Len.WTGA_RIS_MAT + WtgaCptMinScad.Len.WTGA_CPT_MIN_SCAD + WtgaCommisGest.Len.WTGA_COMMIS_GEST + WTGA_TP_MANFEE_APPL + WTGA_DS_RIGA + WTGA_DS_OPER_SQL + WTGA_DS_VER + WTGA_DS_TS_INI_CPTZ + WTGA_DS_TS_END_CPTZ + WTGA_DS_UTENTE + WTGA_DS_STATO_ELAB + WtgaPcCommisGest.Len.WTGA_PC_COMMIS_GEST + WtgaNumGgRival.Len.WTGA_NUM_GG_RIVAL + WtgaImpTrasfe.Len.WTGA_IMP_TRASFE + WtgaImpTfrStrc.Len.WTGA_IMP_TFR_STRC + WtgaAcqExp.Len.WTGA_ACQ_EXP + WtgaRemunAss.Len.WTGA_REMUN_ASS + WtgaCommisInter.Len.WTGA_COMMIS_INTER + WtgaAlqRemunAss.Len.WTGA_ALQ_REMUN_ASS + WtgaAlqCommisInter.Len.WTGA_ALQ_COMMIS_INTER + WtgaImpbRemunAss.Len.WTGA_IMPB_REMUN_ASS + WtgaImpbCommisInter.Len.WTGA_IMPB_COMMIS_INTER + WtgaCosRunAssva.Len.WTGA_COS_RUN_ASSVA + WtgaCosRunAssvaIdc.Len.WTGA_COS_RUN_ASSVA_IDC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ID_TRCH_DI_GAR = 9;
            public static final int WTGA_ID_GAR = 9;
            public static final int WTGA_ID_ADES = 9;
            public static final int WTGA_ID_POLI = 9;
            public static final int WTGA_ID_MOVI_CRZ = 9;
            public static final int WTGA_DT_INI_EFF = 8;
            public static final int WTGA_DT_END_EFF = 8;
            public static final int WTGA_COD_COMP_ANIA = 5;
            public static final int WTGA_DT_DECOR = 8;
            public static final int WTGA_DS_RIGA = 10;
            public static final int WTGA_DS_VER = 9;
            public static final int WTGA_DS_TS_INI_CPTZ = 18;
            public static final int WTGA_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
