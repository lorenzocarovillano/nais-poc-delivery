package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: LCCVP861<br>
 * Variable: LCCVP861 from copybook LCCVP861<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Lccvp861 {

    //==== PROPERTIES ====
    /**Original name: WP86-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA MOT_LIQ
	 *    ALIAS P86
	 *    ULTIMO AGG. 05 DIC 2014
	 * ------------------------------------------------------------</pre>*/
    private WpolStatus status = new WpolStatus();
    //Original name: WP86-ID-PTF
    private int idPtf = DefaultValues.INT_VAL;
    //Original name: WP86-DATI
    private Wp86Dati dati = new Wp86Dati();

    //==== METHODS ====
    public void initLccvp861Spaces() {
        status.setStatus(Types.SPACE_CHAR);
        idPtf = Types.INVALID_INT_VAL;
        dati.initDatiSpaces();
    }

    public void setIdPtf(int idPtf) {
        this.idPtf = idPtf;
    }

    public int getIdPtf() {
        return this.idPtf;
    }

    public Wp86Dati getDati() {
        return dati;
    }

    public WpolStatus getStatus() {
        return status;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_PTF = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
