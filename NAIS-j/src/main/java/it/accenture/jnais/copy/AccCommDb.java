package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ACC-COMM-DB<br>
 * Variable: ACC-COMM-DB from copybook IDBVP633<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AccCommDb {

    //==== PROPERTIES ====
    //Original name: P63-DT-FIRMA-DB
    private String firmaDb = DefaultValues.stringVal(Len.FIRMA_DB);
    //Original name: P63-DT-INI-VLDT-DB
    private String iniVldtDb = DefaultValues.stringVal(Len.INI_VLDT_DB);
    //Original name: P63-DT-END-VLDT-DB
    private String endVldtDb = DefaultValues.stringVal(Len.END_VLDT_DB);

    //==== METHODS ====
    public void setFirmaDb(String firmaDb) {
        this.firmaDb = Functions.subString(firmaDb, Len.FIRMA_DB);
    }

    public String getFirmaDb() {
        return this.firmaDb;
    }

    public void setIniVldtDb(String iniVldtDb) {
        this.iniVldtDb = Functions.subString(iniVldtDb, Len.INI_VLDT_DB);
    }

    public String getIniVldtDb() {
        return this.iniVldtDb;
    }

    public void setEndVldtDb(String endVldtDb) {
        this.endVldtDb = Functions.subString(endVldtDb, Len.END_VLDT_DB);
    }

    public String getEndVldtDb() {
        return this.endVldtDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FIRMA_DB = 10;
        public static final int INI_VLDT_DB = 10;
        public static final int END_VLDT_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
