package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.ws.redefines.WvasDtValzz;
import it.accenture.jnais.ws.redefines.WvasDtValzzCalc;
import it.accenture.jnais.ws.redefines.WvasIdMoviChiu;
import it.accenture.jnais.ws.redefines.WvasIdRichDisFnd;
import it.accenture.jnais.ws.redefines.WvasIdRichInvstFnd;
import it.accenture.jnais.ws.redefines.WvasIdTrchDiGar;
import it.accenture.jnais.ws.redefines.WvasImpMovto;
import it.accenture.jnais.ws.redefines.WvasMinusValenza;
import it.accenture.jnais.ws.redefines.WvasNumQuo;
import it.accenture.jnais.ws.redefines.WvasNumQuoLorde;
import it.accenture.jnais.ws.redefines.WvasPcInvDis;
import it.accenture.jnais.ws.redefines.WvasPlusValenza;
import it.accenture.jnais.ws.redefines.WvasPreMovto;
import it.accenture.jnais.ws.redefines.WvasValQuo;

/**Original name: WVAS-DATI<br>
 * Variable: WVAS-DATI from copybook LCCVVAS1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class WvasDati implements ICopyable<WvasDati> {

    //==== PROPERTIES ====
    //Original name: WVAS-ID-VAL-AST
    private int wvasIdValAst = DefaultValues.INT_VAL;
    //Original name: WVAS-ID-TRCH-DI-GAR
    private WvasIdTrchDiGar wvasIdTrchDiGar = new WvasIdTrchDiGar();
    //Original name: WVAS-ID-MOVI-FINRIO
    private int wvasIdMoviFinrio = DefaultValues.INT_VAL;
    //Original name: WVAS-ID-MOVI-CRZ
    private int wvasIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: WVAS-ID-MOVI-CHIU
    private WvasIdMoviChiu wvasIdMoviChiu = new WvasIdMoviChiu();
    //Original name: WVAS-DT-INI-EFF
    private int wvasDtIniEff = DefaultValues.INT_VAL;
    //Original name: WVAS-DT-END-EFF
    private int wvasDtEndEff = DefaultValues.INT_VAL;
    //Original name: WVAS-COD-COMP-ANIA
    private int wvasCodCompAnia = DefaultValues.INT_VAL;
    //Original name: WVAS-COD-FND
    private String wvasCodFnd = DefaultValues.stringVal(Len.WVAS_COD_FND);
    //Original name: WVAS-NUM-QUO
    private WvasNumQuo wvasNumQuo = new WvasNumQuo();
    //Original name: WVAS-VAL-QUO
    private WvasValQuo wvasValQuo = new WvasValQuo();
    //Original name: WVAS-DT-VALZZ
    private WvasDtValzz wvasDtValzz = new WvasDtValzz();
    //Original name: WVAS-TP-VAL-AST
    private String wvasTpValAst = DefaultValues.stringVal(Len.WVAS_TP_VAL_AST);
    //Original name: WVAS-ID-RICH-INVST-FND
    private WvasIdRichInvstFnd wvasIdRichInvstFnd = new WvasIdRichInvstFnd();
    //Original name: WVAS-ID-RICH-DIS-FND
    private WvasIdRichDisFnd wvasIdRichDisFnd = new WvasIdRichDisFnd();
    //Original name: WVAS-DS-RIGA
    private long wvasDsRiga = DefaultValues.LONG_VAL;
    //Original name: WVAS-DS-OPER-SQL
    private char wvasDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WVAS-DS-VER
    private int wvasDsVer = DefaultValues.INT_VAL;
    //Original name: WVAS-DS-TS-INI-CPTZ
    private long wvasDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WVAS-DS-TS-END-CPTZ
    private long wvasDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WVAS-DS-UTENTE
    private String wvasDsUtente = DefaultValues.stringVal(Len.WVAS_DS_UTENTE);
    //Original name: WVAS-DS-STATO-ELAB
    private char wvasDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WVAS-PRE-MOVTO
    private WvasPreMovto wvasPreMovto = new WvasPreMovto();
    //Original name: WVAS-IMP-MOVTO
    private WvasImpMovto wvasImpMovto = new WvasImpMovto();
    //Original name: WVAS-PC-INV-DIS
    private WvasPcInvDis wvasPcInvDis = new WvasPcInvDis();
    //Original name: WVAS-NUM-QUO-LORDE
    private WvasNumQuoLorde wvasNumQuoLorde = new WvasNumQuoLorde();
    //Original name: WVAS-DT-VALZZ-CALC
    private WvasDtValzzCalc wvasDtValzzCalc = new WvasDtValzzCalc();
    //Original name: WVAS-MINUS-VALENZA
    private WvasMinusValenza wvasMinusValenza = new WvasMinusValenza();
    //Original name: WVAS-PLUS-VALENZA
    private WvasPlusValenza wvasPlusValenza = new WvasPlusValenza();

    //==== CONSTRUCTORS ====
    public WvasDati() {
    }

    public WvasDati(WvasDati dati) {
        this();
        this.wvasIdValAst = dati.wvasIdValAst;
        this.wvasIdTrchDiGar = ((WvasIdTrchDiGar)dati.wvasIdTrchDiGar.copy());
        this.wvasIdMoviFinrio = dati.wvasIdMoviFinrio;
        this.wvasIdMoviCrz = dati.wvasIdMoviCrz;
        this.wvasIdMoviChiu = ((WvasIdMoviChiu)dati.wvasIdMoviChiu.copy());
        this.wvasDtIniEff = dati.wvasDtIniEff;
        this.wvasDtEndEff = dati.wvasDtEndEff;
        this.wvasCodCompAnia = dati.wvasCodCompAnia;
        this.wvasCodFnd = dati.wvasCodFnd;
        this.wvasNumQuo = ((WvasNumQuo)dati.wvasNumQuo.copy());
        this.wvasValQuo = ((WvasValQuo)dati.wvasValQuo.copy());
        this.wvasDtValzz = ((WvasDtValzz)dati.wvasDtValzz.copy());
        this.wvasTpValAst = dati.wvasTpValAst;
        this.wvasIdRichInvstFnd = ((WvasIdRichInvstFnd)dati.wvasIdRichInvstFnd.copy());
        this.wvasIdRichDisFnd = ((WvasIdRichDisFnd)dati.wvasIdRichDisFnd.copy());
        this.wvasDsRiga = dati.wvasDsRiga;
        this.wvasDsOperSql = dati.wvasDsOperSql;
        this.wvasDsVer = dati.wvasDsVer;
        this.wvasDsTsIniCptz = dati.wvasDsTsIniCptz;
        this.wvasDsTsEndCptz = dati.wvasDsTsEndCptz;
        this.wvasDsUtente = dati.wvasDsUtente;
        this.wvasDsStatoElab = dati.wvasDsStatoElab;
        this.wvasPreMovto = ((WvasPreMovto)dati.wvasPreMovto.copy());
        this.wvasImpMovto = ((WvasImpMovto)dati.wvasImpMovto.copy());
        this.wvasPcInvDis = ((WvasPcInvDis)dati.wvasPcInvDis.copy());
        this.wvasNumQuoLorde = ((WvasNumQuoLorde)dati.wvasNumQuoLorde.copy());
        this.wvasDtValzzCalc = ((WvasDtValzzCalc)dati.wvasDtValzzCalc.copy());
        this.wvasMinusValenza = ((WvasMinusValenza)dati.wvasMinusValenza.copy());
        this.wvasPlusValenza = ((WvasPlusValenza)dati.wvasPlusValenza.copy());
    }

    //==== METHODS ====
    public void setWvasIdValAst(int wvasIdValAst) {
        this.wvasIdValAst = wvasIdValAst;
    }

    public int getWvasIdValAst() {
        return this.wvasIdValAst;
    }

    public void setWvasIdMoviFinrio(int wvasIdMoviFinrio) {
        this.wvasIdMoviFinrio = wvasIdMoviFinrio;
    }

    public int getWvasIdMoviFinrio() {
        return this.wvasIdMoviFinrio;
    }

    public void setWvasIdMoviCrz(int wvasIdMoviCrz) {
        this.wvasIdMoviCrz = wvasIdMoviCrz;
    }

    public int getWvasIdMoviCrz() {
        return this.wvasIdMoviCrz;
    }

    public void setWvasDtIniEff(int wvasDtIniEff) {
        this.wvasDtIniEff = wvasDtIniEff;
    }

    public int getWvasDtIniEff() {
        return this.wvasDtIniEff;
    }

    public void setWvasDtEndEff(int wvasDtEndEff) {
        this.wvasDtEndEff = wvasDtEndEff;
    }

    public int getWvasDtEndEff() {
        return this.wvasDtEndEff;
    }

    public void setWvasCodCompAnia(int wvasCodCompAnia) {
        this.wvasCodCompAnia = wvasCodCompAnia;
    }

    public int getWvasCodCompAnia() {
        return this.wvasCodCompAnia;
    }

    public void setWvasCodFnd(String wvasCodFnd) {
        this.wvasCodFnd = Functions.subString(wvasCodFnd, Len.WVAS_COD_FND);
    }

    public String getWvasCodFnd() {
        return this.wvasCodFnd;
    }

    public void setWvasTpValAst(String wvasTpValAst) {
        this.wvasTpValAst = Functions.subString(wvasTpValAst, Len.WVAS_TP_VAL_AST);
    }

    public String getWvasTpValAst() {
        return this.wvasTpValAst;
    }

    public void setWvasDsRiga(long wvasDsRiga) {
        this.wvasDsRiga = wvasDsRiga;
    }

    public long getWvasDsRiga() {
        return this.wvasDsRiga;
    }

    public void setWvasDsOperSql(char wvasDsOperSql) {
        this.wvasDsOperSql = wvasDsOperSql;
    }

    public char getWvasDsOperSql() {
        return this.wvasDsOperSql;
    }

    public void setWvasDsVer(int wvasDsVer) {
        this.wvasDsVer = wvasDsVer;
    }

    public int getWvasDsVer() {
        return this.wvasDsVer;
    }

    public void setWvasDsTsIniCptz(long wvasDsTsIniCptz) {
        this.wvasDsTsIniCptz = wvasDsTsIniCptz;
    }

    public long getWvasDsTsIniCptz() {
        return this.wvasDsTsIniCptz;
    }

    public void setWvasDsTsEndCptz(long wvasDsTsEndCptz) {
        this.wvasDsTsEndCptz = wvasDsTsEndCptz;
    }

    public long getWvasDsTsEndCptz() {
        return this.wvasDsTsEndCptz;
    }

    public void setWvasDsUtente(String wvasDsUtente) {
        this.wvasDsUtente = Functions.subString(wvasDsUtente, Len.WVAS_DS_UTENTE);
    }

    public String getWvasDsUtente() {
        return this.wvasDsUtente;
    }

    public void setWvasDsStatoElab(char wvasDsStatoElab) {
        this.wvasDsStatoElab = wvasDsStatoElab;
    }

    public char getWvasDsStatoElab() {
        return this.wvasDsStatoElab;
    }

    public WvasDtValzz getWvasDtValzz() {
        return wvasDtValzz;
    }

    public WvasDtValzzCalc getWvasDtValzzCalc() {
        return wvasDtValzzCalc;
    }

    public WvasIdMoviChiu getWvasIdMoviChiu() {
        return wvasIdMoviChiu;
    }

    public WvasIdRichDisFnd getWvasIdRichDisFnd() {
        return wvasIdRichDisFnd;
    }

    public WvasIdRichInvstFnd getWvasIdRichInvstFnd() {
        return wvasIdRichInvstFnd;
    }

    public WvasIdTrchDiGar getWvasIdTrchDiGar() {
        return wvasIdTrchDiGar;
    }

    public WvasImpMovto getWvasImpMovto() {
        return wvasImpMovto;
    }

    public WvasMinusValenza getWvasMinusValenza() {
        return wvasMinusValenza;
    }

    public WvasNumQuo getWvasNumQuo() {
        return wvasNumQuo;
    }

    public WvasNumQuoLorde getWvasNumQuoLorde() {
        return wvasNumQuoLorde;
    }

    public WvasPcInvDis getWvasPcInvDis() {
        return wvasPcInvDis;
    }

    public WvasPlusValenza getWvasPlusValenza() {
        return wvasPlusValenza;
    }

    public WvasPreMovto getWvasPreMovto() {
        return wvasPreMovto;
    }

    public WvasValQuo getWvasValQuo() {
        return wvasValQuo;
    }

    public WvasDati copy() {
        return new WvasDati(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_COD_FND = 20;
        public static final int WVAS_TP_VAL_AST = 2;
        public static final int WVAS_DS_UTENTE = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
