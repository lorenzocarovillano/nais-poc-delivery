package it.accenture.jnais.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDBVBEP3<br>
 * Copybook: IDBVBEP3 from copybook IDBVBEP3<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Idbvbep3 {

    //==== PROPERTIES ====
    //Original name: BEP-DT-INI-EFF-DB
    private String bepDtIniEffDb = DefaultValues.stringVal(Len.BEP_DT_INI_EFF_DB);
    //Original name: BEP-DT-END-EFF-DB
    private String bepDtEndEffDb = DefaultValues.stringVal(Len.BEP_DT_END_EFF_DB);

    //==== METHODS ====
    public void setBepDtIniEffDb(String bepDtIniEffDb) {
        this.bepDtIniEffDb = Functions.subString(bepDtIniEffDb, Len.BEP_DT_INI_EFF_DB);
    }

    public String getBepDtIniEffDb() {
        return this.bepDtIniEffDb;
    }

    public void setBepDtEndEffDb(String bepDtEndEffDb) {
        this.bepDtEndEffDb = Functions.subString(bepDtEndEffDb, Len.BEP_DT_END_EFF_DB);
    }

    public String getBepDtEndEffDb() {
        return this.bepDtEndEffDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BEP_DT_INI_EFF_DB = 10;
        public static final int BEP_DT_END_EFF_DB = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
