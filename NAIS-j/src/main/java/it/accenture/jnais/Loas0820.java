package it.accenture.jnais;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.bphx.ctu.af.io.file.FileBufferedDAO;
import com.bphx.ctu.af.io.file.FileRecordBuffer;
import com.bphx.ctu.af.io.file.OpenMode;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.date.CalendarUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.batch.DdCard;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.W820AreaDati;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaPassaggio;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Loas0820Data;
import it.accenture.jnais.ws.WadeAreaAdesioneLoas0800;
import it.accenture.jnais.ws.WapplNumElab;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.WpmoAreaParamMoviLoas0800;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WtgaAreaTrancheLoas0800;

/**Original name: LOAS0820<br>
 * <pre>  ============================================================= *
 *                                                                 *
 *         PORTAFOGLIO VITA ITALIA  VER 1.0                        *
 *                                                                 *
 *   ============================================================= *
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2009.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *      PROGRAMMA ..... LOAS0820                                   *
 *      TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
 *      PROCESSO....... MANAGEMENT FEE                             *
 *      FUNZIONE....... BATCH                                      *
 *      DESCRIZIONE.... SERVIZIO SCRITTURA FLUSSO OUTPUT           *
 *      PAGINA WEB..... N.A.                                       *
 * ----------------------------------------------------------------*</pre>*/
public class Loas0820 extends BatchProgram {

    //==== PROPERTIES ====
    public FileRecordBuffer oumanfeeTo = new FileRecordBuffer(Len.OUMANFEE_REC);
    public FileBufferedDAO oumanfeeDAO = new FileBufferedDAO(new FileAccessStatus(), "OUMANFEE", Len.OUMANFEE_REC);
    //Original name: WORKING-STORAGE
    private Loas0820Data ws = new Loas0820Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WPMO-AREA-PARAM-MOVI
    private WpmoAreaParamMoviLoas0800 wpmoAreaParamMovi;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLoas0800 wadeAreaAdesione;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTrancheLoas0800 wtgaAreaTranche;
    //Original name: WCOM-IO-STATI
    private AreaPassaggio wcomIoStati;
    //Original name: WAPPL-NUM-ELAB
    private WapplNumElab wapplNumElab;

    //==== METHODS ====
    /**Original name: PROGRAM_LOAS0820_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, AreaPassaggio wcomIoStati, WpmoAreaParamMoviLoas0800 wpmoAreaParamMovi, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WadeAreaAdesioneLoas0800 wadeAreaAdesione, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WtgaAreaTrancheLoas0800 wtgaAreaTranche, WapplNumElab wapplNumElab) {
        this.areaIdsv0001 = areaIdsv0001;
        this.wcomIoStati = wcomIoStati;
        this.wpmoAreaParamMovi = wpmoAreaParamMovi;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaTranche = wtgaAreaTranche;
        this.wapplNumElab = wapplNumElab;
        // COB_CODE: PERFORM S00000-OPERAZ-INIZIALI
        //              THRU S00000-OPERAZ-INIZIALI-EX.
        s00000OperazIniziali();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10000-ELABORAZIONE-EX
        //           *
        //                END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10000-ELABORAZIONE
            //              THRU S10000-ELABORAZIONE-EX
            s10000Elaborazione();
            //
        }
        //
        // COB_CODE: PERFORM S90000-OPERAZ-FINALI
        //              THRU S90000-OPERAZ-FINALI-EX.
        s90000OperazFinali();
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Loas0820 getInstance() {
        return ((Loas0820)Programs.getInstance(Loas0820.class));
    }

    /**Original name: S00000-OPERAZ-INIZIALI<br>
	 * <pre> ============================================================== *
	 *  -->           O P E R Z I O N I   I N I Z I A L I          <-- *
	 *  ============================================================== *</pre>*/
    private void s00000OperazIniziali() {
        // COB_CODE: MOVE 'S00000-OPERAZ-INIZIALI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00000-OPERAZ-INIZIALI");
        //
        // COB_CODE: INITIALIZE WK-MOV-VETTORE
        //                      WK-IND-MOV
        //                      WK-IND-MOV-MAX
        initWkMovVettore();
        ws.setWkIndMov(((short)0));
        ws.setWkIndMovMax(((short)0));
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: PERFORM S00100-OPEN-OUT
        //              THRU S00100-OPEN-OUT-EX.
        s00100OpenOut();
        //
        // COB_CODE: PERFORM S00200-CTRL-INPUT
        //              THRU S00200-CTRL-INPUT-EX.
        s00200CtrlInput();
        //
        // COB_CODE: ACCEPT WK-DT-ELAB
        //             FROM DATE YYYYMMDD.
        ws.setWkDtElabFormatted(CalendarUtil.getDateYYYYMMDD());
    }

    /**Original name: S00100-OPEN-OUT<br>
	 * <pre>----------------------------------------------------------------*
	 *  APERTURA DEL FILE DI OUTPUT OUMANFEE
	 * ----------------------------------------------------------------*</pre>*/
    private void s00100OpenOut() {
        // COB_CODE: MOVE 'S00100-OPEN-OUT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00100-OPEN-OUT");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: OPEN OUTPUT OUMANFEE.
        oumanfeeDAO.open(OpenMode.WRITE, "Loas0820");
        ws.setFsOut(oumanfeeDAO.getFileStatus().getStatusCodeFormatted());
        //
        // COB_CODE:      IF FS-OUT NOT = '00'
        //           *
        //                      THRU EX-S0300
        //           *
        //                ELSE
        //           *
        //                   SET WK-OPEN                 TO TRUE
        //           *
        //                END-IF.
        if (!Conditions.eq(ws.getFsOut(), "00")) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'ERRORE APERTURA FILE DI OUTPUT'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE APERTURA FILE DI OUTPUT");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        else {
            //
            // COB_CODE: SET WK-OPEN                 TO TRUE
            ws.getWkOutriva().setOpen();
            //
        }
    }

    /**Original name: S00200-CTRL-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLO DATI DI INPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void s00200CtrlInput() {
        // COB_CODE: MOVE 'S00200-CTRL-INPUT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00200-CTRL-INPUT");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //             TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        //
        // COB_CODE:      IF IDSV0001-TIPO-MOVIMENTO NOT = 6006 AND
        //                                           NOT = 6024 AND
        //                                           NOT = 6025 AND
        //                                           NOT = 6026 AND
        //                                           NOT = 1011
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() != 6006 && areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() != 6024 && areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() != 6025 && areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() != 6026 && areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() != 1011) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'TIPO MOVIMENTO ERRATO'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("TIPO MOVIMENTO ERRATO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF WPMO-ELE-PARAM-MOV-MAX = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wpmoAreaParamMovi.getEleParamMovMax() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'NESSUNA OCCORRENZA DI PARAM. MOV. ELABORABILE'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("NESSUNA OCCORRENZA DI PARAM. MOV. ELABORABILE");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF WPOL-ELE-POLI-MAX = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wpolAreaPolizza.getWpolElePoliMax() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'OCCORRENZA DI POLIZZA NON VALORIZZATA'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("OCCORRENZA DI POLIZZA NON VALORIZZATA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF WADE-ELE-ADES-MAX = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wadeAreaAdesione.getEleAdesMax() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'OCCORRENZA DI ADESIONE NON VALORIZZATA'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("OCCORRENZA DI ADESIONE NON VALORIZZATA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF WGRZ-ELE-GAR-MAX = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wgrzAreaGaranzia.getEleGarMax() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'NESSUNA OCCORRENZA DI GARANZIA ELABORABILE'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("NESSUNA OCCORRENZA DI GARANZIA ELABORABILE");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF WTGA-ELE-TRAN-MAX = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wtgaAreaTranche.getEleTranMax() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'NESSUNA OCCORRENZA DI TRANCHE ELABORABILE'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("NESSUNA OCCORRENZA DI TRANCHE ELABORABILE");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
    }

    /**Original name: S10000-ELABORAZIONE<br>
	 * <pre> ============================================================== *
	 *  -->               E L A B O R A Z I O N E                  <-- *
	 *  ============================================================== *</pre>*/
    private void s10000Elaborazione() {
        // COB_CODE: MOVE 'S10000-ELABORAZIONE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10000-ELABORAZIONE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: IF WAPPL-INIZIO
        //               MOVE '0' TO        WAPPL-ELAB-INIZIATA
        //           END-IF
        if (wapplNumElab.isInizio()) {
            // COB_CODE: PERFORM S11000-RECORD-TESTATA
            //              THRU S11000-RECORD-TESTATA-EX
            s11000RecordTestata();
            // COB_CODE: MOVE '0' TO        WAPPL-ELAB-INIZIATA
            wapplNumElab.setElabIniziataFormatted("0");
        }
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
        //                  UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
        //           *
        //                       THRU S12000-RECORD-DATI-EX
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setGrz(((short)1));
        while (!(ws.getIxIndici().getGrz() > wgrzAreaGaranzia.getEleGarMax())) {
            //
            // COB_CODE: PERFORM S12000-RECORD-DATI
            //              THRU S12000-RECORD-DATI-EX
            s12000RecordDati();
            //
            ws.getIxIndici().setGrz(Trunc.toShort(ws.getIxIndici().getGrz() + 1, 4));
        }
    }

    /**Original name: S11000-RECORD-TESTATA<br>
	 * <pre>----------------------------------------------------------------*
	 *  SCRITTURA DEL RECORD DI TESTATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s11000RecordTestata() {
        // COB_CODE: MOVE 'S11000-RECORD-TESTATA'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S11000-RECORD-TESTATA");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: PERFORM S11999-WRITE-REC-T
        //              THRU S11999-WRITE-REC-T-EX.
        s11999WriteRecT();
    }

    /**Original name: S11999-WRITE-REC-T<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPERAZIONE DI WRITE (TESTATA)
	 * ----------------------------------------------------------------*</pre>*/
    private void s11999WriteRecT() {
        // COB_CODE: MOVE 'S11999-WRITE-REC-T'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S11999-WRITE-REC-T");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: code not available
        oumanfeeTo.setVariable(ws.getLoar0820().getTestata().getTestataFormatted());
        oumanfeeDAO.write(oumanfeeTo);
        ws.setFsOut(oumanfeeDAO.getFileStatus().getStatusCodeFormatted());
        //
        // COB_CODE:      IF FS-OUT NOT = '00'
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (!Conditions.eq(ws.getFsOut(), "00")) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'ERRORE SCRITTURA FILE DI OUTPUT'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE SCRITTURA FILE DI OUTPUT");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
    }

    /**Original name: S12000-RECORD-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *  SCRITTURA DI UN RECORD PER OGNI GARANZIA ELABORATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s12000RecordDati() {
        // COB_CODE: MOVE 'S12000-RECORD-DATI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12000-RECORD-DATI");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: INITIALIZE W820-AREA-DATI.
        initDati();
        //
        // COB_CODE: MOVE WK-DT-ELAB
        //             TO WK-APPO-DT-NO-SEGNO.
        ws.setWkAppoDtNoSegnoFormatted(ws.getWkDtElabFormatted());
        // COB_CODE: MOVE WK-APPO-DT-NO-SEGNO
        //             TO WK-APPO-DT-N.
        ws.getWkAppoDtN().setWkAppoDtNFormatted(ws.getWkAppoDtNoSegnoFormatted());
        // COB_CODE: PERFORM S99999-CONV-N-TO-X
        //              THRU S99999-CONV-N-TO-X-EX
        s99999ConvNToX();
        // COB_CODE: MOVE WK-APPO-DT-X
        //             TO W820-DT-ELAB.
        ws.getLoar0820().getDati().setDtElab(ws.getWkAppoDtX().getWkAppoDtXFormatted());
        //
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO W820-COD-ANIA.
        ws.getLoar0820().getDati().setCodAniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        //
        // COB_CODE:      IF WPOL-COD-RAMO-NULL NOT = HIGH-VALUES
        //           *
        //                     TO W820-RAMO-GEST
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodRamoFormatted())) {
            //
            // COB_CODE: MOVE WPOL-COD-RAMO
            //             TO W820-RAMO-GEST
            ws.getLoar0820().getDati().setRamoGest(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodRamo());
            //
        }
        //
        // COB_CODE:      IF WPOL-IB-OGG-NULL NOT = HIGH-VALUES
        //           *
        //                     TO W820-NUM-POLI
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbOgg(), WpolDati.Len.WPOL_IB_OGG)) {
            //
            // COB_CODE: MOVE WPOL-IB-OGG(4:9)
            //             TO W820-NUM-POLI
            ws.getLoar0820().getDati().setNumPoli(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbOggFormatted().substring((4) - 1, 12));
            //
        }
        //
        // COB_CODE: MOVE WGRZ-COD-TARI(IX-TAB-GRZ)
        //             TO W820-COD-TARI.
        ws.getLoar0820().getDati().setCodTari(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzCodTari());
        //
        // COB_CODE: MOVE WPOL-DT-EMIS
        //             TO WK-APPO-DT-NO-SEGNO.
        ws.setWkAppoDtNoSegno(TruncAbs.toInt(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtEmis(), 8));
        // COB_CODE: MOVE WK-APPO-DT-NO-SEGNO
        //             TO WK-APPO-DT-N.
        ws.getWkAppoDtN().setWkAppoDtNFormatted(ws.getWkAppoDtNoSegnoFormatted());
        // COB_CODE: PERFORM S99999-CONV-N-TO-X
        //              THRU S99999-CONV-N-TO-X-EX
        s99999ConvNToX();
        // COB_CODE: MOVE WK-APPO-DT-X
        //             TO W820-DT-EMIS.
        ws.getLoar0820().getDati().setDtEmis(ws.getWkAppoDtX().getWkAppoDtXFormatted());
        //
        // COB_CODE: MOVE WPOL-DT-DECOR
        //             TO WK-APPO-DT-NO-SEGNO.
        ws.setWkAppoDtNoSegno(TruncAbs.toInt(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtDecor(), 8));
        // COB_CODE: MOVE WK-APPO-DT-NO-SEGNO
        //             TO WK-APPO-DT-N.
        ws.getWkAppoDtN().setWkAppoDtNFormatted(ws.getWkAppoDtNoSegnoFormatted());
        // COB_CODE: PERFORM S99999-CONV-N-TO-X
        //              THRU S99999-CONV-N-TO-X-EX
        s99999ConvNToX();
        // COB_CODE: MOVE WK-APPO-DT-X
        //             TO W820-DT-EFF.
        ws.getLoar0820().getDati().setDtEff(ws.getWkAppoDtX().getWkAppoDtXFormatted());
        //
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
        //             TO WK-APPO-DT-NO-SEGNO.
        ws.setWkAppoDtNoSegnoFormatted(areaIdsv0001.getAreaComune().getIdsv0001DataEffettoFormatted());
        // COB_CODE: MOVE WK-APPO-DT-NO-SEGNO
        //             TO WK-APPO-DT-N.
        ws.getWkAppoDtN().setWkAppoDtNFormatted(ws.getWkAppoDtNoSegnoFormatted());
        // COB_CODE: PERFORM S99999-CONV-N-TO-X
        //              THRU S99999-CONV-N-TO-X-EX
        s99999ConvNToX();
        // COB_CODE: MOVE WK-APPO-DT-X
        //             TO WK-APPO-DT-7
        ws.setWkAppoDt7Bytes(ws.getWkAppoDtX().getWkAppoDtXBytes());
        // COB_CODE: MOVE WK-DT-7
        //             TO W820-DT-RICOR.
        ws.getLoar0820().getDati().setDtRicor(ws.getWkDt7());
        //
        //
        // COB_CODE:      EVALUATE IDSV0001-TIPO-MOVIMENTO
        //           *
        //                    WHEN 1011
        //                       SET CREAZ-INDIVI TO TRUE
        //           *
        //                    WHEN 6006
        //                    WHEN 6024
        //                         TO W820-TP-ELAB
        //           *
        //                    WHEN 6025
        //                    WHEN 6026
        //                         TO W820-TP-ELAB
        //           *
        //                END-EVALUATE.
        switch (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento()) {

            case 1011:// COB_CODE: MOVE 'ANTICIPATO'
                //             TO W820-TP-ELAB
                ws.getLoar0820().getDati().setTpElab("ANTICIPATO");
                // COB_CODE: SET CREAZ-INDIVI TO TRUE
                ws.getWsMovimento().setCreazIndivi();
                //
                break;

            case 6006:
            case 6024:// COB_CODE: MOVE 'A RICORRENZA'
                //             TO W820-TP-ELAB
                ws.getLoar0820().getDati().setTpElab("A RICORRENZA");
                //
                break;

            case 6025:
            case 6026:// COB_CODE: MOVE 'CONGUAGLIO'
                //             TO W820-TP-ELAB
                ws.getLoar0820().getDati().setTpElab("CONGUAGLIO");
                //
                break;

            default:break;
        }
        //
        // COB_CODE: MOVE ZEROES
        //             TO W820-PC-REMUNER.
        ws.getLoar0820().getDati().setPcRemuner(new AfDecimal(0));
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S12100-DATI-PARAM-MOVI-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S12100-DATI-PARAM-MOVI
            //              THRU S12100-DATI-PARAM-MOVI-EX
            s12100DatiParamMovi();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S12200-DATI-TRANCHE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S12200-DATI-TRANCHE
            //              THRU S12200-DATI-TRANCHE-EX
            s12200DatiTranche();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S12300-DATI-RAPP-RETE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S12300-DATI-RAPP-RETE
            //              THRU S12300-DATI-RAPP-RETE-EX
            s12300DatiRappRete();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S12400-DATI-MOVI-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S12400-DATI-MOVI
            //              THRU S12400-DATI-MOVI-EX
            s12400DatiMovi();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S12999-WRITE-REC-D-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S12999-WRITE-REC-D
            //              THRU S12999-WRITE-REC-D-EX
            s12999WriteRecD();
            //
        }
    }

    /**Original name: S12100-DATI-PARAM-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *  DATI PROVENIENTI DALLA TABELLA PARAMETRO MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s12100DatiParamMovi() {
        // COB_CODE: MOVE 'S12100-DATI-PARAM-MOVI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12100-DATI-PARAM-MOVI");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
        //                  UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
        //           *
        //                    END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setPmo(((short)1));
        while (!(ws.getIxIndici().getPmo() > wpmoAreaParamMovi.getEleParamMovMax())) {
            //
            // COB_CODE:          IF WGRZ-ID-GAR(IX-TAB-GRZ) =
            //                       WPMO-ID-OGG(IX-TAB-PMO)
            //           *
            //                       END-IF
            //           *
            //                    END-IF
            if (wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar() == wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getPmo()).getLccvpmo1().getDati().getWpmoIdOgg()) {
                //
                //               IDSV0001-TIPO-MOVIMENTO = 1011)   OR
                // COB_CODE:             IF ((WPMO-ST-INV(IX-TAB-PMO)           AND
                //           *               IDSV0001-TIPO-MOVIMENTO = 1011)   OR
                //                           CREAZ-INDIVI)                     OR
                //                           WPMO-ST-ADD(IX-TAB-PMO))
                //           *
                //                          END-IF
                //           *
                //                       END-IF
                if (wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getPmo()).getLccvpmo1().getStatus().isInv() && ws.getWsMovimento().isCreazIndivi() || wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getPmo()).getLccvpmo1().getStatus().isAdd()) {
                    //
                    // COB_CODE:                IF WPMO-DT-RICOR-SUCC-NULL(IX-TAB-PMO)
                    //                             NOT = HIGH-VALUES
                    //           *
                    //                               TO W820-DT-PAG
                    //           *
                    //                          END-IF
                    if (!Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSuccNullFormatted())) {
                        //
                        // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                        //             TO WK-APPO-DT-NO-SEGNO
                        ws.setWkAppoDtNoSegno(TruncAbs.toInt(wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc(), 8));
                        // COB_CODE: MOVE WK-APPO-DT-NO-SEGNO
                        //             TO WK-APPO-DT-N
                        ws.getWkAppoDtN().setWkAppoDtNFormatted(ws.getWkAppoDtNoSegnoFormatted());
                        // COB_CODE: PERFORM S99999-CONV-N-TO-X
                        //              THRU S99999-CONV-N-TO-X-EX
                        s99999ConvNToX();
                        // COB_CODE: MOVE WK-APPO-DT-X
                        //             TO W820-DT-PAG
                        ws.getLoar0820().getDati().setDtPag(ws.getWkAppoDtX().getWkAppoDtXFormatted());
                        //
                    }
                    //
                    // COB_CODE:                IF WPMO-IMP-RAT-MANFEE-NULL(IX-TAB-PMO)
                    //                             NOT = HIGH-VALUES
                    //           *
                    //                               TO W820-IMP-REMUNER
                    //           *
                    //                          END-IF
                    if (!Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getPmo()).getLccvpmo1().getDati().getWpmoImpRatManfee().getWpmoImpRatManfeeNullFormatted())) {
                        //
                        // COB_CODE: MOVE WPMO-IMP-RAT-MANFEE(IX-TAB-PMO)
                        //             TO W820-IMP-REMUNER
                        ws.getLoar0820().getDati().setImpRemuner(wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getPmo()).getLccvpmo1().getDati().getWpmoImpRatManfee().getWpmoImpRatManfee());
                        //
                    }
                    //
                }
                //
            }
            //
            ws.getIxIndici().setPmo(Trunc.toShort(ws.getIxIndici().getPmo() + 1, 4));
        }
    }

    /**Original name: S12200-DATI-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *  DATI PROVENIENTI DALLA TABELLA TRANCHE DI GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void s12200DatiTranche() {
        // COB_CODE: MOVE 'S12200-DATI-TRANCHE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12200-DATI-TRANCHE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE ZEROES
        //             TO WK-PRE-NET
        //                WK-RIS-MAT
        //                WK-PRSTZ-ULT
        //                WK-INCR-PRSTZ.
        ws.setWkPreNet(new AfDecimal(0, 15, 3));
        ws.setWkRisMat(new AfDecimal(0, 15, 3));
        ws.setWkPrstzUlt(new AfDecimal(0, 15, 3));
        ws.setWkIncrPrstz(new AfDecimal(0, 15, 3));
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //           *
        //                    END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTga(((short)1));
        while (!(ws.getIxIndici().getTga() > wtgaAreaTranche.getEleTranMax())) {
            //
            // COB_CODE:          IF WTGA-PRE-INI-NET-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
            //                    AND WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)
            //           *
            //                              (WK-PRE-NET + WTGA-PRE-INI-NET(IX-TAB-TGA))
            //           *
            //                    END-IF
            if (!Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreIniNet().getWtgaPreIniNetNullFormatted()) && wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdGar() == wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                //
                // COB_CODE: COMPUTE WK-PRE-NET =
                //                  (WK-PRE-NET + WTGA-PRE-INI-NET(IX-TAB-TGA))
                ws.setWkPreNet(Trunc.toDecimal(ws.getWkPreNet().add(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreIniNet().getWtgaPreIniNet()), 15, 3));
                //
            }
            //
            // COB_CODE:          IF WTGA-RIS-MAT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
            //                    AND WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)
            //           *
            //                              (WK-RIS-MAT + WTGA-RIS-MAT(IX-TAB-TGA))
            //           *
            //                    END-IF
            if (!Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRisMat().getWtgaRisMatNullFormatted()) && wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdGar() == wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                //
                // COB_CODE: COMPUTE WK-RIS-MAT =
                //                  (WK-RIS-MAT + WTGA-RIS-MAT(IX-TAB-TGA))
                ws.setWkRisMat(Trunc.toDecimal(ws.getWkRisMat().add(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRisMat().getWtgaRisMat()), 15, 3));
                //
            }
            //
            // COB_CODE:          IF WTGA-PRSTZ-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
            //                    AND WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)
            //           *
            //                              (WK-PRSTZ-ULT + WTGA-PRSTZ-ULT(IX-TAB-TGA))
            //           *
            //                    END-IF
            if (!Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzUlt().getWtgaPrstzUltNullFormatted()) && wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdGar() == wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                //
                // COB_CODE: COMPUTE WK-PRSTZ-ULT =
                //                  (WK-PRSTZ-ULT + WTGA-PRSTZ-ULT(IX-TAB-TGA))
                ws.setWkPrstzUlt(Trunc.toDecimal(ws.getWkPrstzUlt().add(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzUlt().getWtgaPrstzUlt()), 15, 3));
                //
            }
            //
            // COB_CODE:          IF WTGA-INCR-PRSTZ-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
            //                    AND WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)
            //           *
            //                              (WK-INCR-PRSTZ + WTGA-INCR-PRSTZ(IX-TAB-TGA))
            //           *
            //                    END-IF
            if (!Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIncrPrstz().getWtgaIncrPrstzNullFormatted()) && wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdGar() == wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                //
                // COB_CODE: COMPUTE WK-INCR-PRSTZ =
                //                  (WK-INCR-PRSTZ + WTGA-INCR-PRSTZ(IX-TAB-TGA))
                ws.setWkIncrPrstz(Trunc.toDecimal(ws.getWkIncrPrstz().add(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIncrPrstz().getWtgaIncrPrstz()), 15, 3));
                //
            }
            //
            ws.getIxIndici().setTga(Trunc.toShort(ws.getIxIndici().getTga() + 1, 4));
        }
        //
        // COB_CODE: MOVE WK-PRE-NET
        //             TO W820-PREMIO-NET.
        ws.getLoar0820().getDati().setPremioNet(ws.getWkPreNet());
        //
        // COB_CODE: MOVE WK-RIS-MAT
        //             TO W820-RISERVA.
        ws.getLoar0820().getDati().setRiserva(ws.getWkRisMat());
        //
        // COB_CODE: MOVE WK-PRSTZ-ULT
        //             TO W820-CPT-RIVTO.
        ws.getLoar0820().getDati().setCptRivto(ws.getWkPrstzUlt());
        //
        // COB_CODE: MOVE WK-INCR-PRSTZ
        //             TO W820-ULT-INCR.
        ws.getLoar0820().getDati().setUltIncr(ws.getWkIncrPrstz());
    }

    /**Original name: S12300-DATI-RAPP-RETE<br>
	 * <pre>----------------------------------------------------------------*
	 *  DATI PROVENIENTI DALLA TABELLA RAPPORTO RETE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12300DatiRappRete() {
        // COB_CODE: MOVE 'S12300-DATI-RAPP-RETE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12300-DATI-RAPP-RETE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: PERFORM S12310-IMPOSTA-RRE
        //              THRU S12310-IMPOSTA-RRE-EX.
        s12310ImpostaRre();
        //
        // COB_CODE: PERFORM S12320-LEGGI-RRE
        //              THRU S12320-LEGGI-RRE-EX.
        s12320LeggiRre();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF RRE-COD-PNT-RETE-INI-NULL NOT = HIGH-VALUES
            //           *
            //                        TO W820-COD-AGE
            //           *
            //                   END-IF
            if (!Characters.EQ_HIGH.test(ws.getRappRete().getRreCodPntReteIni().getRreCodPntReteIniNullFormatted())) {
                //
                // COB_CODE: MOVE RRE-COD-PNT-RETE-INI
                //             TO W820-COD-AGE
                ws.getLoar0820().getDati().setCodAge(TruncAbs.toInt(ws.getRappRete().getRreCodPntReteIni().getRreCodPntReteIni(), 5));
                //
            }
            //
        }
    }

    /**Original name: S12310-IMPOSTA-RRE<br>
	 * <pre>----------------------------------------------------------------*
	 *   VALORIZZAZIONI PER LETTURA RAPPORTO RETE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12310ImpostaRre() {
        // COB_CODE: MOVE 'S12310-IMPOSTA-RRE'          TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12310-IMPOSTA-RRE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE WPOL-ID-POLI                  TO RRE-ID-OGG.
        ws.getRappRete().getRreIdOgg().setRreIdOgg(wpolAreaPolizza.getLccvpol1().getDati().getWpolIdPoli());
        //
        // COB_CODE: SET POLIZZA                        TO TRUE
        ws.getWsTpOgg().setPolizza();
        // COB_CODE: MOVE WS-TP-OGG                     TO RRE-TP-OGG.
        ws.getRappRete().setRreTpOgg(ws.getWsTpOgg().getWsTpOgg());
        //
        // COB_CODE: SET GESTIONE                       TO TRUE
        ws.getWsTpRete().setGestione();
        // COB_CODE: MOVE WS-TP-RETE                    TO RRE-TP-RETE.
        ws.getRappRete().setRreTpRete(ws.getWsTpRete().getWsTpRete());
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZEROES
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE LDBS4240                TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getLdbs4240());
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE SPACES                  TO IDSI0011-BUFFER-WHERE-COND
        //                                           IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE RAPP-RETE               TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getRappRete().getRappReteFormatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-SELECT          TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S12320-LEGGI-RRE<br>
	 * <pre>----------------------------------------------------------------*
	 *   LETTURA DELLA TABELLA RAPPORTO RETE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12320LeggiRre() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S12320-LEGGI-RRE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12320-LEGGI-RRE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore Dispatcher
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                               THRU EX-S0300
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //                              TO RAPP-RETE
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore Lettura
            //           *
            //                               THRU EX-S0300
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: MOVE '005019'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005019");
                    // COB_CODE: MOVE 'RAPPORTO RETE NON TROVATA'
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("RAPPORTO RETE NON TROVATA");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO RAPP-RETE
                    ws.getRappRete().setRappReteFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //
                    break;

                default://
                    //  --> Errore Lettura
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: MOVE '005015'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: STRING 'ERRORE LETTURA RAPPORTO RETE ' ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE LETTURA RAPPORTO RETE ", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore Dispatcher
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA RAPPORTO RETE'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE DISPATCHER LETTURA RAPPORTO RETE");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
    }

    /**Original name: S12400-DATI-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *  DATI PROVENIENTI DALLA TABELLA MOVI
	 * ----------------------------------------------------------------*</pre>*/
    private void s12400DatiMovi() {
        // COB_CODE: MOVE 'S12400-DATI-MOVI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12400-DATI-MOVI");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE ZEROES
        //             TO WK-IMP-LRD-EFFLQ.
        ws.setWkImpLrdEfflq(new AfDecimal(0, 15, 3));
        //
        // COB_CODE: SET WK-VERS-AGG-NO
        //             TO TRUE.
        ws.getWkVersAgg().setNo();
        //
        // COB_CODE: SET WK-RISC-PARZ-NO
        //             TO TRUE.
        ws.getWkRiscParz().setNo();
        //
        // COB_CODE: SET WK-FINE-MOV-NO
        //             TO TRUE.
        ws.getWkFineMov().setNo();
        //
        // COB_CODE: SET IDSI0011-FETCH-FIRST
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        //
        // COB_CODE: PERFORM S12410-IMPOSTA-MOVI
        //              THRU S12410-IMPOSTA-MOVI-EX.
        s12410ImpostaMovi();
        //
        // COB_CODE: PERFORM S12420-LEGGI-MOVI
        //              THRU S12420-LEGGI-MOVI-EX
        //             UNTIL WK-FINE-MOV-SI
        //                OR IDSV0001-ESITO-KO.
        while (!(ws.getWkFineMov().isSi() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            s12420LeggiMovi();
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                     TO W820-IMP-RISC
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF WK-VERS-AGG-SI
            //           *
            //                        TO W820-FLAG-VERS-AGG
            //           *
            //                   END-IF
            if (ws.getWkVersAgg().isSi()) {
                //
                // COB_CODE: MOVE 'S'
                //             TO W820-FLAG-VERS-AGG
                ws.getLoar0820().getDati().setFlagVersAggFormatted("S");
                //
            }
            //
            // COB_CODE:         IF WK-RISC-PARZ-SI
            //           *
            //                        TO W820-FLAG-RIS-PARZ
            //           *
            //                   END-IF
            if (ws.getWkRiscParz().isSi()) {
                //
                // COB_CODE: MOVE 'S'
                //             TO W820-FLAG-RIS-PARZ
                ws.getLoar0820().getDati().setFlagRisParzFormatted("S");
                //
            }
            //
            // COB_CODE: MOVE WK-IMP-LRD-EFFLQ
            //             TO W820-IMP-RISC
            ws.getLoar0820().getDati().setImpRisc(ws.getWkImpLrdEfflq());
            //
        }
    }

    /**Original name: S12410-IMPOSTA-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZAZIONI PER LETTURA TABELLA MOVI
	 * ----------------------------------------------------------------*</pre>*/
    private void s12410ImpostaMovi() {
        // COB_CODE: MOVE 'S12410-IMPOSTA-MOVI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12410-IMPOSTA-MOVI");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: INITIALIZE LDBV3611.
        initLdbv3611();
        //
        // COB_CODE: MOVE WPOL-ID-POLI
        //             TO LDBV3611-ID-OGG.
        ws.getLdbv3611().setIdOgg(wpolAreaPolizza.getLccvpol1().getDati().getWpolIdPoli());
        //
        // COB_CODE: SET POLIZZA
        //             TO TRUE
        ws.getWsTpOgg().setPolizza();
        // COB_CODE: MOVE WS-TP-OGG
        //             TO LDBV3611-TP-OGG.
        ws.getLdbv3611().setTpOgg(ws.getWsTpOgg().getWsTpOgg());
        //
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
        //             TO WK-APPO-DT-NO-SEGNO.
        ws.setWkAppoDtNoSegnoFormatted(areaIdsv0001.getAreaComune().getIdsv0001DataEffettoFormatted());
        // COB_CODE: MOVE WK-APPO-DT-NO-SEGNO
        //             TO WK-APPO-DT-N.
        ws.getWkAppoDtN().setWkAppoDtNFormatted(ws.getWkAppoDtNoSegnoFormatted());
        //
        // COB_CODE: MOVE 01
        //             TO WK-DT-N-GG.
        ws.getWkAppoDtN().setGg(((short)1));
        // COB_CODE: MOVE 01
        //             TO WK-DT-N-MM.
        ws.getWkAppoDtN().setMm(((short)1));
        // COB_CODE: MOVE WK-APPO-DT-N
        //             TO WK-APPO-DT-NO-SEGNO.
        ws.setWkAppoDtNoSegnoFromBuffer(ws.getWkAppoDtN().getWkAppoDtNBytes());
        // COB_CODE: MOVE WK-APPO-DT-NO-SEGNO
        //             TO LDBV3611-DT-DA.
        ws.getLdbv3611().setDtDa(ws.getWkAppoDtNoSegno());
        //
        // COB_CODE: MOVE 31
        //             TO WK-DT-N-GG.
        ws.getWkAppoDtN().setGg(((short)31));
        // COB_CODE: MOVE 12
        //             TO WK-DT-N-MM.
        ws.getWkAppoDtN().setMm(((short)12));
        // COB_CODE: MOVE WK-APPO-DT-N
        //             TO WK-APPO-DT-NO-SEGNO.
        ws.setWkAppoDtNoSegnoFromBuffer(ws.getWkAppoDtN().getWkAppoDtNBytes());
        // COB_CODE: MOVE WK-APPO-DT-NO-SEGNO
        //             TO LDBV3611-DT-A.
        ws.getLdbv3611().setDtA(ws.getWkAppoDtNoSegno());
        //
        // COB_CODE: MOVE 1065
        //             TO LDBV3611-TP-MOVI-1.
        ws.getLdbv3611().setTpMovi1(1065);
        //
        // COB_CODE: MOVE 5006
        //             TO LDBV3611-TP-MOVI-2.
        ws.getLdbv3611().setTpMovi2(5006);
        //
        // COB_CODE: MOVE 5007
        //             TO LDBV3611-TP-MOVI-3.
        ws.getLdbv3611().setTpMovi3(5007);
        //
        // COB_CODE: MOVE 6005
        //             TO LDBV3611-TP-MOVI-4.
        ws.getLdbv3611().setTpMovi4(6005);
        //
        // COB_CODE: MOVE 5005
        //             TO LDBV3611-TP-MOVI-5.
        ws.getLdbv3611().setTpMovi5(5005);
        // COB_CODE: MOVE 2318
        //             TO LDBV3611-TP-MOVI-6.
        ws.getLdbv3611().setTpMovi6(2318);
        // COB_CODE: MOVE 2319
        //             TO LDBV3611-TP-MOVI-7.
        ws.getLdbv3611().setTpMovi7(2319);
        // COB_CODE: MOVE 2316
        //             TO LDBV3611-TP-MOVI-8.
        ws.getLdbv3611().setTpMovi8(2316);
        // COB_CODE: MOVE 2317
        //             TO LDBV3611-TP-MOVI-9.
        ws.getLdbv3611().setTpMovi9(2317);
        //
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZEROES
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE LDBS3610
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getLdbs3610());
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE LDBV3611
        //             TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv3611().getLdbv3611Formatted());
        // COB_CODE: MOVE SPACES
        //              TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S12420-LEGGI-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *  LETTURA TABELLA MOVI
	 * ----------------------------------------------------------------*</pre>*/
    private void s12420LeggiMovi() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S12420-LEGGI-MOVI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12420-LEGGI-MOVI");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non TROVATO
            //           *
            //                              TO TRUE
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                              TO TRUE
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU EX-S0300
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non TROVATO
                    //
                    // COB_CODE: CONTINUE
                    //continue
                    //
                    //  --> Fine occorrenze fetch
                    //
                    // COB_CODE: SET WK-FINE-MOV-SI
                    //             TO TRUE
                    ws.getWkFineMov().setSi();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO MOVI
                    ws.getMovi().setMoviFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //
                    // COB_CODE: PERFORM S12430-TEST-MOVI
                    //              THRU S12430-TEST-MOVI-EX
                    s12430TestMovi();
                    //
                    // COB_CODE: SET IDSI0011-FETCH-NEXT
                    //             TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: STRING 'ERRORE LETTURA MOVIMENTO' ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE LETTURA MOVIMENTO", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: STRING 'ERRORE DISPATCHER LETTURA MOVIMENTO' ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE DISPATCHER LETTURA MOVIMENTO", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE: PERFORM S12410-IMPOSTA-MOVI
        //              THRU S12410-IMPOSTA-MOVI-EX.
        s12410ImpostaMovi();
    }

    /**Original name: S12430-TEST-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *  TEST SUL TIPO MOVIMENTO ESTRATTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s12430TestMovi() {
        // COB_CODE: MOVE 'S12430-TEST-MOVI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12430-TEST-MOVI");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      IF MOV-TP-MOVI = 1065
        //           *
        //                     TO TRUE
        //           *
        //                ELSE
        //           *
        //                      THRU S12440-CERCA-IMP-RISC-PARZ-EX
        //           *
        //                END-IF.
        if (ws.getMovi().getMovTpMovi().getMovTpMovi() == 1065) {
            //
            // COB_CODE: SET WK-VERS-AGG-SI
            //             TO TRUE
            ws.getWkVersAgg().setSi();
            //
        }
        else {
            //
            // COB_CODE: PERFORM S12440-CERCA-IMP-RISC-PARZ
            //              THRU S12440-CERCA-IMP-RISC-PARZ-EX
            s12440CercaImpRiscParz();
            //
        }
    }

    /**Original name: S12440-CERCA-IMP-RISC-PARZ<br>
	 * <pre>----------------------------------------------------------------*
	 *  VENGONO CERCATE LE OCCORRENZE TRANCHE DI
	 *  LIQUIDAZIONE RELATIVE A TRANCHE DI GARANZIA
	 *  APPARTENENTI ALLA GARANZIA IN ESAME
	 * ----------------------------------------------------------------*</pre>*/
    private void s12440CercaImpRiscParz() {
        // COB_CODE: MOVE 'S12440-CERCA-IMP-RISC-PARZ'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12440-CERCA-IMP-RISC-PARZ");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: SET WK-FINE-TLI-NO
        //             TO TRUE.
        ws.getWkFineTli().setNo();
        //
        // COB_CODE: SET IDSI0011-FETCH-FIRST
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        //
        // COB_CODE: PERFORM S12450-IMPOSTA-TRANCHE-LIQ
        //              THRU S12450-IMPOSTA-TRANCHE-LIQ-EX
        s12450ImpostaTrancheLiq();
        //
        // COB_CODE: PERFORM S12460-LEGGI-TRANCHE-LIQ
        //              THRU S12460-LEGGI-TRANCHE-LIQ-EX
        //             UNTIL WK-FINE-TLI-SI
        //                OR IDSV0001-ESITO-KO.
        while (!(ws.getWkFineTli().isSi() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            s12460LeggiTrancheLiq();
        }
    }

    /**Original name: S12450-IMPOSTA-TRANCHE-LIQ<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZAZIONI PER LETTURA TRRANCHE DI LIQUIDAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12450ImpostaTrancheLiq() {
        // COB_CODE: MOVE 'S12450-IMPOSTA-TRANCHE-LIQ'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12450-IMPOSTA-TRANCHE-LIQ");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: INITIALIZE LDBV3621.
        initLdbv3621();
        //
        // COB_CODE: MOVE MOV-ID-MOVI
        //             TO LDBV3621-ID-MOVI-CRZ.
        ws.setLdbv3621IdMoviCrz(ws.getMovi().getMovIdMovi());
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZEROES
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE LDBS3620
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getLdbs3620());
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE LDBV3621
        //             TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv3621Formatted());
        // COB_CODE: MOVE SPACES
        //              TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S12460-LEGGI-TRANCHE-LIQ<br>
	 * <pre>----------------------------------------------------------------*
	 *  LETTURA TRRANCHE DI LIQUIDAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12460LeggiTrancheLiq() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S12460-LEGGI-TRANCHE-LIQ'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12460-LEGGI-TRANCHE-LIQ");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non TROVATO
            //           *
            //                              TO TRUE
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                               THRU S12470-TEST-LEGAME-GAR-EX
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU EX-S0300
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non TROVATO
                    //
                    // COB_CODE:                  IF IDSI0011-FETCH-FIRST
                    //           *
                    //                               END-IF
                    //                            END-IF
                    if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                        //
                        // COB_CODE: SET WK-MOV-NON-TROVATO TO TRUE
                        ws.getWkMov().setNonTrovato();
                        //
                        // COB_CODE: PERFORM S12465-CONTROLLA-MOVIMENTO
                        //              THRU S12465-EX
                        s12465ControllaMovimento();
                        //
                        // COB_CODE:                     IF WK-MOV-NON-TROVATO
                        //                                  THRU EX-S0300
                        //           *
                        //                               END-IF
                        if (ws.getWkMov().isNonTrovato()) {
                            // COB_CODE: MOVE WK-PGM
                            //             TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                            // COB_CODE: MOVE WK-LABEL-ERR
                            //             TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                            // COB_CODE: STRING 'TRANCHE LIQ NON TROVATA' ';'
                            //                  IDSO0011-RETURN-CODE ';'
                            //                  IDSO0011-SQLCODE
                            //                  DELIMITED BY SIZE
                            //                  INTO IEAI9901-PARAMETRI-ERR
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "TRANCHE LIQ NON TROVATA", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                            // COB_CODE: MOVE '005069'
                            //             TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("005069");
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //              THRU EX-S0300
                            s0300RicercaGravitaErrore();
                            //
                        }
                    }
                    //
                    //  --> Fine occorrenze fetch
                    //
                    // COB_CODE: SET WK-FINE-TLI-SI
                    //             TO TRUE
                    ws.getWkFineTli().setSi();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: PERFORM S12466-MOV-TROVATI
                    //              THRU S12466-EX
                    s12466MovTrovati();
                    //
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO TRCH-LIQ
                    ws.getTrchLiq().setTrchLiqFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //
                    // COB_CODE: SET IDSI0011-FETCH-NEXT
                    //             TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                    //
                    // COB_CODE: PERFORM S12470-TEST-LEGAME-GAR
                    //              THRU S12470-TEST-LEGAME-GAR-EX
                    s12470TestLegameGar();
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: STRING 'ERRORE LETTURA TRANCHE LIQ' ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE LETTURA TRANCHE LIQ", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: STRING 'ERRORE DISPATCHER LETTURA TRANCHE LIQ' ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE DISPATCHER LETTURA TRANCHE LIQ", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
    }

    /**Original name: S12465-CONTROLLA-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLO IL MOVIMENTO NON TROVATO NELLE TRANCHE DI LIQUIDAZIONE
	 * ----------------------------------------------------------------*
	 *  SE IL MOVIMENTO NON TROVATO E' DI LIQUIDAZIONE E' NECESSARIO
	 *  VERIFICARE L'ESISTENZA DEL MOVIMENTO DI COMUNICAZIONE PER
	 *  LA STESSA DATA EFFETTO</pre>*/
    private void s12465ControllaMovimento() {
        // COB_CODE: IF MOV-TP-MOVI = 5006
        //              END-PERFORM
        //           END-IF.
        if (ws.getMovi().getMovTpMovi().getMovTpMovi() == 5006) {
            // COB_CODE: PERFORM VARYING WK-IND-MOV FROM 1 BY 1
            //                     UNTIL WK-IND-MOV > WK-IND-MOV-MAX
            //                        OR WK-MOV-TROVATO
            //              END-IF
            //           END-PERFORM
            ws.setWkIndMov(((short)1));
            while (!(ws.getWkIndMov() > ws.getWkIndMovMax() || ws.getWkMov().isTrovato())) {
                // COB_CODE: IF WK-DT-EFF(WK-IND-MOV) = MOV-DT-EFF
                //              AND WK-TP-MOVI(WK-IND-MOV) = 5005
                //              SET WK-MOV-TROVATO TO TRUE
                //           END-IF
                if (ws.getWkMovEle(ws.getWkIndMov()).getDtEff() == ws.getMovi().getMovDtEff() && Conditions.eq(ws.getWkMovEle(ws.getWkIndMov()).getTpMovi(), "5005")) {
                    // COB_CODE: SET WK-MOV-TROVATO TO TRUE
                    ws.getWkMov().setTrovato();
                }
                ws.setWkIndMov(Trunc.toShort(ws.getWkIndMov() + 1, 4));
            }
        }
        // COB_CODE: IF MOV-TP-MOVI = 2319
        //              END-PERFORM
        //           END-IF.
        if (ws.getMovi().getMovTpMovi().getMovTpMovi() == 2319) {
            // COB_CODE: PERFORM VARYING WK-IND-MOV FROM 1 BY 1
            //                     UNTIL WK-IND-MOV > WK-IND-MOV-MAX
            //                        OR WK-MOV-TROVATO
            //              END-IF
            //           END-PERFORM
            ws.setWkIndMov(((short)1));
            while (!(ws.getWkIndMov() > ws.getWkIndMovMax() || ws.getWkMov().isTrovato())) {
                // COB_CODE: IF WK-DT-EFF(WK-IND-MOV) = MOV-DT-EFF
                //              AND WK-TP-MOVI(WK-IND-MOV) = 2318
                //              SET WK-MOV-TROVATO TO TRUE
                //           END-IF
                if (ws.getWkMovEle(ws.getWkIndMov()).getDtEff() == ws.getMovi().getMovDtEff() && Conditions.eq(ws.getWkMovEle(ws.getWkIndMov()).getTpMovi(), "2318")) {
                    // COB_CODE: SET WK-MOV-TROVATO TO TRUE
                    ws.getWkMov().setTrovato();
                }
                ws.setWkIndMov(Trunc.toShort(ws.getWkIndMov() + 1, 4));
            }
        }
        // COB_CODE: IF MOV-TP-MOVI = 2317
        //              END-PERFORM
        //           END-IF.
        if (ws.getMovi().getMovTpMovi().getMovTpMovi() == 2317) {
            // COB_CODE: PERFORM VARYING WK-IND-MOV FROM 1 BY 1
            //                     UNTIL WK-IND-MOV > WK-IND-MOV-MAX
            //                        OR WK-MOV-TROVATO
            //              END-IF
            //           END-PERFORM
            ws.setWkIndMov(((short)1));
            while (!(ws.getWkIndMov() > ws.getWkIndMovMax() || ws.getWkMov().isTrovato())) {
                // COB_CODE: IF WK-DT-EFF(WK-IND-MOV) = MOV-DT-EFF
                //              AND WK-TP-MOVI(WK-IND-MOV) = 2316
                //              SET WK-MOV-TROVATO TO TRUE
                //           END-IF
                if (ws.getWkMovEle(ws.getWkIndMov()).getDtEff() == ws.getMovi().getMovDtEff() && Conditions.eq(ws.getWkMovEle(ws.getWkIndMov()).getTpMovi(), "2316")) {
                    // COB_CODE: SET WK-MOV-TROVATO TO TRUE
                    ws.getWkMov().setTrovato();
                }
                ws.setWkIndMov(Trunc.toShort(ws.getWkIndMov() + 1, 4));
            }
        }
        // SE IL MOVIMENTO NON TROVATO E' DI COMUNICAZIONE SI IMPOSTA
        // IL MOVIMENTO A TROVATO PER PERMETTERE AL PROGRAMMA DI ELABORARE
        // IL MOVIMENTO DI LIQUIDAZIONE
        // COB_CODE: IF MOV-TP-MOVI = 5005
        //           OR MOV-TP-MOVI = 2318
        //           OR MOV-TP-MOVI = 2316
        //              SET WK-MOV-TROVATO TO TRUE
        //           END-IF.
        if (ws.getMovi().getMovTpMovi().getMovTpMovi() == 5005 || ws.getMovi().getMovTpMovi().getMovTpMovi() == 2318 || ws.getMovi().getMovTpMovi().getMovTpMovi() == 2316) {
            // COB_CODE: SET WK-MOV-TROVATO TO TRUE
            ws.getWkMov().setTrovato();
        }
    }

    /**Original name: S12466-MOV-TROVATI<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLO IL MOVIMENTO TROVATO NELLE TRANCHE DI LIQUIDAZIONE
	 * ----------------------------------------------------------------*
	 *  SE LA TRANCHE DI LIQUIDAZIONE E' STATA STORICIZZATA DA UN
	 *  MOVIMENTO DI COMUNICAZIONE LO SALVO IN UN VETTORE</pre>*/
    private void s12466MovTrovati() {
        // COB_CODE: IF MOV-TP-MOVI = 5005
        //           OR MOV-TP-MOVI = 2318
        //           OR MOV-TP-MOVI = 2316
        //              MOVE MOV-DT-EFF  TO WK-DT-EFF(WK-IND-MOV-MAX)
        //           END-IF.
        if (ws.getMovi().getMovTpMovi().getMovTpMovi() == 5005 || ws.getMovi().getMovTpMovi().getMovTpMovi() == 2318 || ws.getMovi().getMovTpMovi().getMovTpMovi() == 2316) {
            // COB_CODE: ADD 1 TO WK-IND-MOV-MAX
            ws.setWkIndMovMax(Trunc.toShort(1 + ws.getWkIndMovMax(), 4));
            // COB_CODE: MOVE MOV-TP-MOVI TO WK-TP-MOVI(WK-IND-MOV-MAX)
            ws.getWkMovEle(ws.getWkIndMovMax()).setTpMovi(ws.getMovi().getMovTpMovi().getMovTpMoviFormatted());
            // COB_CODE: MOVE MOV-DT-EFF  TO WK-DT-EFF(WK-IND-MOV-MAX)
            ws.getWkMovEle(ws.getWkIndMovMax()).setDtEff(ws.getMovi().getMovDtEff());
        }
    }

    /**Original name: S12470-TEST-LEGAME-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *  VENGONO CONSIDERATE SOLO LE TRANCHE DI LIQ. RELATIVE ALLA GAR
	 * ----------------------------------------------------------------*</pre>*/
    private void s12470TestLegameGar() {
        // COB_CODE: MOVE 'S12470-TEST-LEGAME-GAR'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12470-TEST-LEGAME-GAR");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //           *
        //                    END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTga(((short)1));
        while (!(ws.getIxIndici().getTga() > wtgaAreaTranche.getEleTranMax())) {
            //
            // COB_CODE:          IF TLI-ID-TRCH-DI-GAR = WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
            //           *
            //                       END-IF
            //           *
            //                    END-IF
            if (ws.getTrchLiq().getTliIdTrchDiGar() == wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar()) {
                //
                // COB_CODE:             IF WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)
                //           *
                //                                 (WK-IMP-LRD-EFFLQ + TLI-IMP-LRD-EFFLQ)
                //           *
                //                       END-IF
                if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdGar() == wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                    //
                    // COB_CODE: SET WK-RISC-PARZ-SI
                    //             TO TRUE
                    ws.getWkRiscParz().setSi();
                    //
                    // COB_CODE: COMPUTE WK-IMP-LRD-EFFLQ =
                    //                  (WK-IMP-LRD-EFFLQ + TLI-IMP-LRD-EFFLQ)
                    ws.setWkImpLrdEfflq(Trunc.toDecimal(ws.getWkImpLrdEfflq().add(ws.getTrchLiq().getTliImpLrdEfflq()), 15, 3));
                    //
                }
                //
            }
            //
            ws.getIxIndici().setTga(Trunc.toShort(ws.getIxIndici().getTga() + 1, 4));
        }
    }

    /**Original name: S12999-WRITE-REC-D<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPERAZIONE DI WRITE (DATI)
	 * ----------------------------------------------------------------*</pre>*/
    private void s12999WriteRecD() {
        // COB_CODE: MOVE 'S12999-WRITE-REC-D'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12999-WRITE-REC-D");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: code not available
        oumanfeeTo.setVariable(ws.getLoar0820().getDati().getDatiFormatted());
        oumanfeeDAO.write(oumanfeeTo);
        ws.setFsOut(oumanfeeDAO.getFileStatus().getStatusCodeFormatted());
        //
        // COB_CODE:      IF FS-OUT NOT = '00'
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (!Conditions.eq(ws.getFsOut(), "00")) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'ERRORE SCRITTURA FILE DI OUTPUT'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE SCRITTURA FILE DI OUTPUT");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
    }

    /**Original name: S90000-OPERAZ-FINALI<br>
	 * <pre> ============================================================== *
	 *  -->           O P E R Z I O N I   F I N A L I              <-- *
	 *  ============================================================== *</pre>*/
    private void s90000OperazFinali() {
        // COB_CODE: MOVE 'S90000-OPERAZ-FINALI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S90000-OPERAZ-FINALI");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: PERFORM S90100-CLOSE-OUT
        //              THRU S90100-CLOSE-OUT-EX.
        s90100CloseOut();
    }

    /**Original name: S90100-CLOSE-OUT<br>
	 * <pre>----------------------------------------------------------------*
	 *  CHIUSURA DEL FILE DI OUTPUT OUMANFEE
	 * ----------------------------------------------------------------*</pre>*/
    private void s90100CloseOut() {
        // COB_CODE: MOVE 'S90100-CLOSE-OUT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S90100-CLOSE-OUT");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      IF WK-OPEN
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (ws.getWkOutriva().isOpen()) {
            //
            // COB_CODE: CLOSE OUMANFEE
            oumanfeeDAO.close();
            ws.setFsOut(oumanfeeDAO.getFileStatus().getStatusCodeFormatted());
            //
            // COB_CODE:         IF FS-OUT NOT = '00'
            //           *
            //                         THRU EX-S0300
            //           *
            //                   END-IF
            if (!Conditions.eq(ws.getFsOut(), "00")) {
                //
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL-ERR
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                // COB_CODE: MOVE '001114'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("001114");
                // COB_CODE: MOVE 'ERRORE CHIUSURA FILE DI OUTPUT'
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("ERRORE CHIUSURA FILE DI OUTPUT");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                //
            }
            //
        }
    }

    /**Original name: S99999-CONV-N-TO-X<br>
	 * <pre>----------------------------------------------------------------*
	 *  ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*</pre>*/
    private void s99999ConvNToX() {
        // COB_CODE: MOVE 'S99999-CONV-N-TO-X'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S99999-CONV-N-TO-X");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE WK-DT-N-AAAA
        //             TO WK-DT-X-AAAA.
        ws.getWkAppoDtX().setAaaaFormatted(ws.getWkAppoDtN().getAaaaFormatted());
        //
        // COB_CODE: MOVE WK-DT-N-MM
        //             TO WK-DT-X-MM.
        ws.getWkAppoDtX().setMmFormatted(ws.getWkAppoDtN().getMmFormatted());
        //
        // COB_CODE: MOVE WK-DT-N-GG
        //             TO WK-DT-X-GG.
        ws.getWkAppoDtX().setGgFormatted(ws.getWkAppoDtN().getGgFormatted());
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *  ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *   ROUTINES GESTIONE ERRORI                                      *
	 * ----------------------------------------------------------------*
	 * MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: DISPLAY-LABEL<br>
	 * <pre>----------------------------------------------------------------*
	 *  DIPLAY LABEL
	 * ----------------------------------------------------------------*</pre>*/
    private void displayLabel() {
        // COB_CODE:      IF (IDSV0001-DEBUG-BASSO OR IDSV0001-DEBUG-ESASPERATO) AND
        //                NOT IDSV0001-ON-LINE
        //           *
        //                   CONTINUE
        //           *       DISPLAY WK-LABEL-ERR
        //           *
        //                END-IF.
        if ((areaIdsv0001.getAreaComune().getLivelloDebug().isIdsv0001DebugBasso() || areaIdsv0001.getAreaComune().getLivelloDebug().isIdsv0001DebugEsasperato()) && !areaIdsv0001.getAreaComune().getModalitaEsecutiva().isIdsv0001OnLine()) {
        //
        // COB_CODE: CONTINUE
        //continue
        //       DISPLAY WK-LABEL-ERR
        //
        }
    }

    @Override
    public DdCard[] getDefaultDdCards() {
        return new DdCard[] {new DdCard("OUMANFEE", 400)};
    }

    public void initWkMovVettore() {
        for (int idx0 = 1; idx0 <= Loas0820Data.WK_MOV_ELE_MAXOCCURS; idx0++) {
            ws.getWkMovEle(idx0).setTpMovi("");
            ws.getWkMovEle(idx0).setDtEff(0);
        }
    }

    public void initDati() {
        ws.getLoar0820().getDati().setDtElab("");
        ws.getLoar0820().getDati().setCodAniaFormatted("00000");
        ws.getLoar0820().getDati().setCodAgeFormatted("00000");
        ws.getLoar0820().getDati().setRamoGest("");
        ws.getLoar0820().getDati().setNumPoli("");
        ws.getLoar0820().getDati().setCodTari("");
        ws.getLoar0820().getDati().setDtEmis("");
        ws.getLoar0820().getDati().setDtEff("");
        ws.getLoar0820().getDati().setDtRicor("");
        ws.getLoar0820().getDati().setDtPag("");
        ws.getLoar0820().getDati().setPremioNet(new AfDecimal(0));
        ws.getLoar0820().getDati().setCptRivto(new AfDecimal(0));
        ws.getLoar0820().getDati().setUltIncr(new AfDecimal(0));
        ws.getLoar0820().getDati().setRiserva(new AfDecimal(0));
        ws.getLoar0820().getDati().setFlagVersAgg(Types.SPACE_CHAR);
        ws.getLoar0820().getDati().setFlagRisParz(Types.SPACE_CHAR);
        ws.getLoar0820().getDati().setImpRisc(new AfDecimal(0));
        ws.getLoar0820().getDati().setPcRemunerFormatted("0000000");
        ws.getLoar0820().getDati().setImpRemuner(new AfDecimal(0));
        ws.getLoar0820().getDati().setTpElab("");
    }

    public void initLdbv3611() {
        ws.getLdbv3611().setIdOgg(0);
        ws.getLdbv3611().setTpOgg("");
        ws.getLdbv3611().setDtA(0);
        ws.getLdbv3611().setDtADb("");
        ws.getLdbv3611().setDtDa(0);
        ws.getLdbv3611().setDtDaDb("");
        ws.getLdbv3611().setTpMovi1(0);
        ws.getLdbv3611().setTpMovi2(0);
        ws.getLdbv3611().setTpMovi3(0);
        ws.getLdbv3611().setTpMovi4(0);
        ws.getLdbv3611().setTpMovi5(0);
        ws.getLdbv3611().setTpMovi6(0);
        ws.getLdbv3611().setTpMovi7(0);
        ws.getLdbv3611().setTpMovi8(0);
        ws.getLdbv3611().setTpMovi9(0);
        ws.getLdbv3611().setTpMovi10(0);
        ws.getLdbv3611().setTpMovi11(0);
        ws.getLdbv3611().setTpMovi12(0);
        ws.getLdbv3611().setTpMovi13(0);
        ws.getLdbv3611().setTpMovi14(0);
        ws.getLdbv3611().setTpMovi15(0);
    }

    public void initLdbv3621() {
        ws.setLdbv3621IdMoviCrz(0);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OUMANFEE_REC = 400;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
