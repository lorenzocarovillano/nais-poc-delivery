package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.PrestDao;
import it.accenture.jnais.commons.data.to.IPrest;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs2820Data;
import it.accenture.jnais.ws.Ldbv2821;

/**Original name: LDBS2820<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  23 NOV 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs2820 extends Program implements IPrest {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private PrestDao prestDao = new PrestDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs2820Data ws = new Ldbs2820Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LDBV2821
    private Ldbv2821 ldbv2821;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS2820_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ldbv2821 ldbv2821) {
        this.idsv0003 = idsv0003;
        this.ldbv2821 = ldbv2821;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs2820 getInstance() {
        return ((Ldbs2820)Programs.getInstance(Ldbs2820.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS2820'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS2820");
        // COB_CODE: MOVE ' LDBV2821' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella(" LDBV2821");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT SUM (VALUE(IMP_PREST, 0))
        //               INTO :LDBV2821-IMP-PREST
        //                    :IND-PRE-IMP-PREST
        //            FROM PREST
        //              WHERE ID_OGG = :LDBV2821-ID-OGG
        //                AND TP_OGG = :LDBV2821-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        prestDao.selectRec4(ldbv2821.getIdOgg(), ldbv2821.getTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT SUM (VALUE(IMP_PREST, 0))
        //               INTO :LDBV2821-IMP-PREST
        //                    :IND-PRE-IMP-PREST
        //            FROM PREST
        //              WHERE ID_OGG = :LDBV2821-ID-OGG
        //                AND TP_OGG = :LDBV2821-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        prestDao.selectRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public String getCodDvs() {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public void setCodDvs(String codDvs) {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public String getCodDvsObj() {
        return getCodDvs();
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        setCodDvs(codDvsObj);
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsEndCptz() {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public long getDsTsIniCptz() {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtConcsPrestDb() {
        throw new FieldNotMappedException("dtConcsPrestDb");
    }

    @Override
    public void setDtConcsPrestDb(String dtConcsPrestDb) {
        throw new FieldNotMappedException("dtConcsPrestDb");
    }

    @Override
    public String getDtConcsPrestDbObj() {
        return getDtConcsPrestDb();
    }

    @Override
    public void setDtConcsPrestDbObj(String dtConcsPrestDbObj) {
        setDtConcsPrestDb(dtConcsPrestDbObj);
    }

    @Override
    public String getDtDecorPrestDb() {
        throw new FieldNotMappedException("dtDecorPrestDb");
    }

    @Override
    public void setDtDecorPrestDb(String dtDecorPrestDb) {
        throw new FieldNotMappedException("dtDecorPrestDb");
    }

    @Override
    public String getDtDecorPrestDbObj() {
        return getDtDecorPrestDb();
    }

    @Override
    public void setDtDecorPrestDbObj(String dtDecorPrestDbObj) {
        setDtDecorPrestDb(dtDecorPrestDbObj);
    }

    @Override
    public String getDtEndEffDb() {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public String getDtIniEffDb() {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public String getDtRichPrestDb() {
        throw new FieldNotMappedException("dtRichPrestDb");
    }

    @Override
    public void setDtRichPrestDb(String dtRichPrestDb) {
        throw new FieldNotMappedException("dtRichPrestDb");
    }

    @Override
    public String getDtRimbDb() {
        throw new FieldNotMappedException("dtRimbDb");
    }

    @Override
    public void setDtRimbDb(String dtRimbDb) {
        throw new FieldNotMappedException("dtRimbDb");
    }

    @Override
    public String getDtRimbDbObj() {
        return getDtRimbDb();
    }

    @Override
    public void setDtRimbDbObj(String dtRimbDbObj) {
        setDtRimbDb(dtRimbDbObj);
    }

    @Override
    public int getFrazPagIntr() {
        throw new FieldNotMappedException("frazPagIntr");
    }

    @Override
    public void setFrazPagIntr(int frazPagIntr) {
        throw new FieldNotMappedException("frazPagIntr");
    }

    @Override
    public Integer getFrazPagIntrObj() {
        return ((Integer)getFrazPagIntr());
    }

    @Override
    public void setFrazPagIntrObj(Integer frazPagIntrObj) {
        setFrazPagIntr(((int)frazPagIntrObj));
    }

    @Override
    public int getIdMoviChiu() {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public Integer getIdMoviChiuObj() {
        return ((Integer)getIdMoviChiu());
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        setIdMoviChiu(((int)idMoviChiuObj));
    }

    @Override
    public int getIdMoviCrz() {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public int getIdPrest() {
        throw new FieldNotMappedException("idPrest");
    }

    @Override
    public void setIdPrest(int idPrest) {
        throw new FieldNotMappedException("idPrest");
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpPrest() {
        throw new FieldNotMappedException("impPrest");
    }

    @Override
    public void setImpPrest(AfDecimal impPrest) {
        throw new FieldNotMappedException("impPrest");
    }

    @Override
    public AfDecimal getImpPrestLiqto() {
        throw new FieldNotMappedException("impPrestLiqto");
    }

    @Override
    public void setImpPrestLiqto(AfDecimal impPrestLiqto) {
        throw new FieldNotMappedException("impPrestLiqto");
    }

    @Override
    public AfDecimal getImpPrestLiqtoObj() {
        return getImpPrestLiqto();
    }

    @Override
    public void setImpPrestLiqtoObj(AfDecimal impPrestLiqtoObj) {
        setImpPrestLiqto(new AfDecimal(impPrestLiqtoObj, 15, 3));
    }

    @Override
    public AfDecimal getImpPrestObj() {
        return getImpPrest();
    }

    @Override
    public void setImpPrestObj(AfDecimal impPrestObj) {
        setImpPrest(new AfDecimal(impPrestObj, 15, 3));
    }

    @Override
    public AfDecimal getImpRimb() {
        throw new FieldNotMappedException("impRimb");
    }

    @Override
    public void setImpRimb(AfDecimal impRimb) {
        throw new FieldNotMappedException("impRimb");
    }

    @Override
    public AfDecimal getImpRimbObj() {
        return getImpRimb();
    }

    @Override
    public void setImpRimbObj(AfDecimal impRimbObj) {
        setImpRimb(new AfDecimal(impRimbObj, 15, 3));
    }

    @Override
    public AfDecimal getIntrPrest() {
        throw new FieldNotMappedException("intrPrest");
    }

    @Override
    public void setIntrPrest(AfDecimal intrPrest) {
        throw new FieldNotMappedException("intrPrest");
    }

    @Override
    public AfDecimal getIntrPrestObj() {
        return getIntrPrest();
    }

    @Override
    public void setIntrPrestObj(AfDecimal intrPrestObj) {
        setIntrPrest(new AfDecimal(intrPrestObj, 6, 3));
    }

    @Override
    public int getLdbv2821IdOgg() {
        return ldbv2821.getIdOgg();
    }

    @Override
    public void setLdbv2821IdOgg(int ldbv2821IdOgg) {
        this.ldbv2821.setIdOgg(ldbv2821IdOgg);
    }

    @Override
    public AfDecimal getLdbv2821ImpPrest() {
        return ldbv2821.getImpPrest();
    }

    @Override
    public void setLdbv2821ImpPrest(AfDecimal ldbv2821ImpPrest) {
        this.ldbv2821.setImpPrest(ldbv2821ImpPrest.copy());
    }

    @Override
    public AfDecimal getLdbv2821ImpPrestObj() {
        if (ws.getIndPrest().getImpPrest() >= 0) {
            return getLdbv2821ImpPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setLdbv2821ImpPrestObj(AfDecimal ldbv2821ImpPrestObj) {
        if (ldbv2821ImpPrestObj != null) {
            setLdbv2821ImpPrest(new AfDecimal(ldbv2821ImpPrestObj, 15, 3));
            ws.getIndPrest().setImpPrest(((short)0));
        }
        else {
            ws.getIndPrest().setImpPrest(((short)-1));
        }
    }

    @Override
    public String getLdbv2821TpOgg() {
        return ldbv2821.getTpOgg();
    }

    @Override
    public void setLdbv2821TpOgg(String ldbv2821TpOgg) {
        this.ldbv2821.setTpOgg(ldbv2821TpOgg);
    }

    @Override
    public String getModIntrPrest() {
        throw new FieldNotMappedException("modIntrPrest");
    }

    @Override
    public void setModIntrPrest(String modIntrPrest) {
        throw new FieldNotMappedException("modIntrPrest");
    }

    @Override
    public String getModIntrPrestObj() {
        return getModIntrPrest();
    }

    @Override
    public void setModIntrPrestObj(String modIntrPrestObj) {
        setModIntrPrest(modIntrPrestObj);
    }

    @Override
    public long getPreDsRiga() {
        throw new FieldNotMappedException("preDsRiga");
    }

    @Override
    public void setPreDsRiga(long preDsRiga) {
        throw new FieldNotMappedException("preDsRiga");
    }

    @Override
    public int getPreIdOgg() {
        throw new FieldNotMappedException("preIdOgg");
    }

    @Override
    public void setPreIdOgg(int preIdOgg) {
        throw new FieldNotMappedException("preIdOgg");
    }

    @Override
    public String getPreTpOgg() {
        throw new FieldNotMappedException("preTpOgg");
    }

    @Override
    public void setPreTpOgg(String preTpOgg) {
        throw new FieldNotMappedException("preTpOgg");
    }

    @Override
    public AfDecimal getPrestResEff() {
        throw new FieldNotMappedException("prestResEff");
    }

    @Override
    public void setPrestResEff(AfDecimal prestResEff) {
        throw new FieldNotMappedException("prestResEff");
    }

    @Override
    public AfDecimal getPrestResEffObj() {
        return getPrestResEff();
    }

    @Override
    public void setPrestResEffObj(AfDecimal prestResEffObj) {
        setPrestResEff(new AfDecimal(prestResEffObj, 15, 3));
    }

    @Override
    public AfDecimal getRimbEff() {
        throw new FieldNotMappedException("rimbEff");
    }

    @Override
    public void setRimbEff(AfDecimal rimbEff) {
        throw new FieldNotMappedException("rimbEff");
    }

    @Override
    public AfDecimal getRimbEffObj() {
        return getRimbEff();
    }

    @Override
    public void setRimbEffObj(AfDecimal rimbEffObj) {
        setRimbEff(new AfDecimal(rimbEffObj, 15, 3));
    }

    @Override
    public AfDecimal getSdoIntr() {
        throw new FieldNotMappedException("sdoIntr");
    }

    @Override
    public void setSdoIntr(AfDecimal sdoIntr) {
        throw new FieldNotMappedException("sdoIntr");
    }

    @Override
    public AfDecimal getSdoIntrObj() {
        return getSdoIntr();
    }

    @Override
    public void setSdoIntrObj(AfDecimal sdoIntrObj) {
        setSdoIntr(new AfDecimal(sdoIntrObj, 15, 3));
    }

    @Override
    public AfDecimal getSpePrest() {
        throw new FieldNotMappedException("spePrest");
    }

    @Override
    public void setSpePrest(AfDecimal spePrest) {
        throw new FieldNotMappedException("spePrest");
    }

    @Override
    public AfDecimal getSpePrestObj() {
        return getSpePrest();
    }

    @Override
    public void setSpePrestObj(AfDecimal spePrestObj) {
        setSpePrest(new AfDecimal(spePrestObj, 15, 3));
    }

    @Override
    public String getTpPrest() {
        throw new FieldNotMappedException("tpPrest");
    }

    @Override
    public void setTpPrest(String tpPrest) {
        throw new FieldNotMappedException("tpPrest");
    }

    @Override
    public String getTpPrestObj() {
        return getTpPrest();
    }

    @Override
    public void setTpPrestObj(String tpPrestObj) {
        setTpPrest(tpPrestObj);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
