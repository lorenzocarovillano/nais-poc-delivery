package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.LiqDao;
import it.accenture.jnais.commons.data.to.ILiq;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbslqu0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.LiqIdbslqu0;
import it.accenture.jnais.ws.redefines.LquAddizComun;
import it.accenture.jnais.ws.redefines.LquAddizRegion;
import it.accenture.jnais.ws.redefines.LquBnsNonGoduto;
import it.accenture.jnais.ws.redefines.LquCnbtInpstfm;
import it.accenture.jnais.ws.redefines.LquComponTaxRimb;
import it.accenture.jnais.ws.redefines.LquCosTunnelUscita;
import it.accenture.jnais.ws.redefines.LquDtDen;
import it.accenture.jnais.ws.redefines.LquDtEndIstr;
import it.accenture.jnais.ws.redefines.LquDtLiq;
import it.accenture.jnais.ws.redefines.LquDtMor;
import it.accenture.jnais.ws.redefines.LquDtPervDen;
import it.accenture.jnais.ws.redefines.LquDtRich;
import it.accenture.jnais.ws.redefines.LquDtVlt;
import it.accenture.jnais.ws.redefines.LquIdMoviChiu;
import it.accenture.jnais.ws.redefines.LquImpbAddizComun;
import it.accenture.jnais.ws.redefines.LquImpbAddizRegion;
import it.accenture.jnais.ws.redefines.LquImpbBolloDettC;
import it.accenture.jnais.ws.redefines.LquImpbCnbtInpstfm;
import it.accenture.jnais.ws.redefines.LquImpbImpst252;
import it.accenture.jnais.ws.redefines.LquImpbImpstPrvr;
import it.accenture.jnais.ws.redefines.LquImpbIntrSuPrest;
import it.accenture.jnais.ws.redefines.LquImpbIrpef;
import it.accenture.jnais.ws.redefines.LquImpbIs;
import it.accenture.jnais.ws.redefines.LquImpbIs1382011;
import it.accenture.jnais.ws.redefines.LquImpbIs662014;
import it.accenture.jnais.ws.redefines.LquImpbTaxSep;
import it.accenture.jnais.ws.redefines.LquImpbVis1382011;
import it.accenture.jnais.ws.redefines.LquImpbVis662014;
import it.accenture.jnais.ws.redefines.LquImpDirDaRimb;
import it.accenture.jnais.ws.redefines.LquImpDirLiq;
import it.accenture.jnais.ws.redefines.LquImpExcontr;
import it.accenture.jnais.ws.redefines.LquImpIntrRitPag;
import it.accenture.jnais.ws.redefines.LquImpLrdCalcCp;
import it.accenture.jnais.ws.redefines.LquImpLrdDaRimb;
import it.accenture.jnais.ws.redefines.LquImpLrdLiqtoRilt;
import it.accenture.jnais.ws.redefines.LquImpOnerLiq;
import it.accenture.jnais.ws.redefines.LquImpPnl;
import it.accenture.jnais.ws.redefines.LquImpRenK1;
import it.accenture.jnais.ws.redefines.LquImpRenK2;
import it.accenture.jnais.ws.redefines.LquImpRenK3;
import it.accenture.jnais.ws.redefines.LquImpst252;
import it.accenture.jnais.ws.redefines.LquImpstApplRilt;
import it.accenture.jnais.ws.redefines.LquImpstBolloDettC;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotAa;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotSw;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotV;
import it.accenture.jnais.ws.redefines.LquImpstDaRimb;
import it.accenture.jnais.ws.redefines.LquImpstIrpef;
import it.accenture.jnais.ws.redefines.LquImpstPrvr;
import it.accenture.jnais.ws.redefines.LquImpstSost1382011;
import it.accenture.jnais.ws.redefines.LquImpstSost662014;
import it.accenture.jnais.ws.redefines.LquImpstVis1382011;
import it.accenture.jnais.ws.redefines.LquImpstVis662014;
import it.accenture.jnais.ws.redefines.LquMontDal2007;
import it.accenture.jnais.ws.redefines.LquMontEnd2000;
import it.accenture.jnais.ws.redefines.LquMontEnd2006;
import it.accenture.jnais.ws.redefines.LquPcAbbTitStat;
import it.accenture.jnais.ws.redefines.LquPcAbbTs662014;
import it.accenture.jnais.ws.redefines.LquPcRen;
import it.accenture.jnais.ws.redefines.LquPcRenK1;
import it.accenture.jnais.ws.redefines.LquPcRenK2;
import it.accenture.jnais.ws.redefines.LquPcRenK3;
import it.accenture.jnais.ws.redefines.LquPcRiscParz;
import it.accenture.jnais.ws.redefines.LquRisMat;
import it.accenture.jnais.ws.redefines.LquRisSpe;
import it.accenture.jnais.ws.redefines.LquSpeRcs;
import it.accenture.jnais.ws.redefines.LquTaxSep;
import it.accenture.jnais.ws.redefines.LquTotIasMggSin;
import it.accenture.jnais.ws.redefines.LquTotIasOnerPrvnt;
import it.accenture.jnais.ws.redefines.LquTotIasPnl;
import it.accenture.jnais.ws.redefines.LquTotIasRstDpst;
import it.accenture.jnais.ws.redefines.LquTotImpbAcc;
import it.accenture.jnais.ws.redefines.LquTotImpbTfr;
import it.accenture.jnais.ws.redefines.LquTotImpbVis;
import it.accenture.jnais.ws.redefines.LquTotImpIntrPrest;
import it.accenture.jnais.ws.redefines.LquTotImpIs;
import it.accenture.jnais.ws.redefines.LquTotImpLrdLiqto;
import it.accenture.jnais.ws.redefines.LquTotImpNetLiqto;
import it.accenture.jnais.ws.redefines.LquTotImpPrest;
import it.accenture.jnais.ws.redefines.LquTotImpRimb;
import it.accenture.jnais.ws.redefines.LquTotImpRitAcc;
import it.accenture.jnais.ws.redefines.LquTotImpRitTfr;
import it.accenture.jnais.ws.redefines.LquTotImpRitVis;
import it.accenture.jnais.ws.redefines.LquTotImpUti;
import it.accenture.jnais.ws.redefines.LquTpMetRisc;

/**Original name: IDBSLQU0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  03 GIU 2019.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbslqu0 extends Program implements ILiq {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private LiqDao liqDao = new LiqDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbslqu0Data ws = new Idbslqu0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LIQ
    private LiqIdbslqu0 liq;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSLQU0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, LiqIdbslqu0 liq) {
        this.idsv0003 = idsv0003;
        this.liq = liq;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbslqu0 getInstance() {
        return ((Idbslqu0)Programs.getInstance(Idbslqu0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSLQU0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSLQU0");
        // COB_CODE: MOVE 'LIQ' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("LIQ");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_LIQ
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,TP_LIQ
        //                ,DESC_CAU_EVE_SIN
        //                ,COD_CAU_SIN
        //                ,COD_SIN_CATSTRF
        //                ,DT_MOR
        //                ,DT_DEN
        //                ,DT_PERV_DEN
        //                ,DT_RICH
        //                ,TP_SIN
        //                ,TP_RISC
        //                ,TP_MET_RISC
        //                ,DT_LIQ
        //                ,COD_DVS
        //                ,TOT_IMP_LRD_LIQTO
        //                ,TOT_IMP_PREST
        //                ,TOT_IMP_INTR_PREST
        //                ,TOT_IMP_UTI
        //                ,TOT_IMP_RIT_TFR
        //                ,TOT_IMP_RIT_ACC
        //                ,TOT_IMP_RIT_VIS
        //                ,TOT_IMPB_TFR
        //                ,TOT_IMPB_ACC
        //                ,TOT_IMPB_VIS
        //                ,TOT_IMP_RIMB
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,TOT_IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,TOT_IMP_NET_LIQTO
        //                ,MONT_END2000
        //                ,MONT_END2006
        //                ,PC_REN
        //                ,IMP_PNL
        //                ,IMPB_IRPEF
        //                ,IMPST_IRPEF
        //                ,DT_VLT
        //                ,DT_END_ISTR
        //                ,TP_RIMB
        //                ,SPE_RCS
        //                ,IB_LIQ
        //                ,TOT_IAS_ONER_PRVNT
        //                ,TOT_IAS_MGG_SIN
        //                ,TOT_IAS_RST_DPST
        //                ,IMP_ONER_LIQ
        //                ,COMPON_TAX_RIMB
        //                ,TP_MEZ_PAG
        //                ,IMP_EXCONTR
        //                ,IMP_INTR_RIT_PAG
        //                ,BNS_NON_GODUTO
        //                ,CNBT_INPSTFM
        //                ,IMPST_DA_RIMB
        //                ,IMPB_IS
        //                ,TAX_SEP
        //                ,IMPB_TAX_SEP
        //                ,IMPB_INTR_SU_PREST
        //                ,ADDIZ_COMUN
        //                ,IMPB_ADDIZ_COMUN
        //                ,ADDIZ_REGION
        //                ,IMPB_ADDIZ_REGION
        //                ,MONT_DAL2007
        //                ,IMPB_CNBT_INPSTFM
        //                ,IMP_LRD_DA_RIMB
        //                ,IMP_DIR_DA_RIMB
        //                ,RIS_MAT
        //                ,RIS_SPE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TOT_IAS_PNL
        //                ,FL_EVE_GARTO
        //                ,IMP_REN_K1
        //                ,IMP_REN_K2
        //                ,IMP_REN_K3
        //                ,PC_REN_K1
        //                ,PC_REN_K2
        //                ,PC_REN_K3
        //                ,TP_CAUS_ANTIC
        //                ,IMP_LRD_LIQTO_RILT
        //                ,IMPST_APPL_RILT
        //                ,PC_RISC_PARZ
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_TOT_SW
        //                ,IMPST_BOLLO_TOT_AA
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPB_IS_1382011
        //                ,IMPST_SOST_1382011
        //                ,PC_ABB_TIT_STAT
        //                ,IMPB_BOLLO_DETT_C
        //                ,FL_PRE_COMP
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //                ,PC_ABB_TS_662014
        //                ,IMP_LRD_CALC_CP
        //                ,COS_TUNNEL_USCITA
        //             INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //             FROM LIQ
        //             WHERE     DS_RIGA = :LQU-DS-RIGA
        //           END-EXEC.
        liqDao.selectByLquDsRiga(liq.getLquDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO LIQ
            //                  (
            //                     ID_LIQ
            //                    ,ID_OGG
            //                    ,TP_OGG
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,IB_OGG
            //                    ,TP_LIQ
            //                    ,DESC_CAU_EVE_SIN
            //                    ,COD_CAU_SIN
            //                    ,COD_SIN_CATSTRF
            //                    ,DT_MOR
            //                    ,DT_DEN
            //                    ,DT_PERV_DEN
            //                    ,DT_RICH
            //                    ,TP_SIN
            //                    ,TP_RISC
            //                    ,TP_MET_RISC
            //                    ,DT_LIQ
            //                    ,COD_DVS
            //                    ,TOT_IMP_LRD_LIQTO
            //                    ,TOT_IMP_PREST
            //                    ,TOT_IMP_INTR_PREST
            //                    ,TOT_IMP_UTI
            //                    ,TOT_IMP_RIT_TFR
            //                    ,TOT_IMP_RIT_ACC
            //                    ,TOT_IMP_RIT_VIS
            //                    ,TOT_IMPB_TFR
            //                    ,TOT_IMPB_ACC
            //                    ,TOT_IMPB_VIS
            //                    ,TOT_IMP_RIMB
            //                    ,IMPB_IMPST_PRVR
            //                    ,IMPST_PRVR
            //                    ,IMPB_IMPST_252
            //                    ,IMPST_252
            //                    ,TOT_IMP_IS
            //                    ,IMP_DIR_LIQ
            //                    ,TOT_IMP_NET_LIQTO
            //                    ,MONT_END2000
            //                    ,MONT_END2006
            //                    ,PC_REN
            //                    ,IMP_PNL
            //                    ,IMPB_IRPEF
            //                    ,IMPST_IRPEF
            //                    ,DT_VLT
            //                    ,DT_END_ISTR
            //                    ,TP_RIMB
            //                    ,SPE_RCS
            //                    ,IB_LIQ
            //                    ,TOT_IAS_ONER_PRVNT
            //                    ,TOT_IAS_MGG_SIN
            //                    ,TOT_IAS_RST_DPST
            //                    ,IMP_ONER_LIQ
            //                    ,COMPON_TAX_RIMB
            //                    ,TP_MEZ_PAG
            //                    ,IMP_EXCONTR
            //                    ,IMP_INTR_RIT_PAG
            //                    ,BNS_NON_GODUTO
            //                    ,CNBT_INPSTFM
            //                    ,IMPST_DA_RIMB
            //                    ,IMPB_IS
            //                    ,TAX_SEP
            //                    ,IMPB_TAX_SEP
            //                    ,IMPB_INTR_SU_PREST
            //                    ,ADDIZ_COMUN
            //                    ,IMPB_ADDIZ_COMUN
            //                    ,ADDIZ_REGION
            //                    ,IMPB_ADDIZ_REGION
            //                    ,MONT_DAL2007
            //                    ,IMPB_CNBT_INPSTFM
            //                    ,IMP_LRD_DA_RIMB
            //                    ,IMP_DIR_DA_RIMB
            //                    ,RIS_MAT
            //                    ,RIS_SPE
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,TOT_IAS_PNL
            //                    ,FL_EVE_GARTO
            //                    ,IMP_REN_K1
            //                    ,IMP_REN_K2
            //                    ,IMP_REN_K3
            //                    ,PC_REN_K1
            //                    ,PC_REN_K2
            //                    ,PC_REN_K3
            //                    ,TP_CAUS_ANTIC
            //                    ,IMP_LRD_LIQTO_RILT
            //                    ,IMPST_APPL_RILT
            //                    ,PC_RISC_PARZ
            //                    ,IMPST_BOLLO_TOT_V
            //                    ,IMPST_BOLLO_DETT_C
            //                    ,IMPST_BOLLO_TOT_SW
            //                    ,IMPST_BOLLO_TOT_AA
            //                    ,IMPB_VIS_1382011
            //                    ,IMPST_VIS_1382011
            //                    ,IMPB_IS_1382011
            //                    ,IMPST_SOST_1382011
            //                    ,PC_ABB_TIT_STAT
            //                    ,IMPB_BOLLO_DETT_C
            //                    ,FL_PRE_COMP
            //                    ,IMPB_VIS_662014
            //                    ,IMPST_VIS_662014
            //                    ,IMPB_IS_662014
            //                    ,IMPST_SOST_662014
            //                    ,PC_ABB_TS_662014
            //                    ,IMP_LRD_CALC_CP
            //                    ,COS_TUNNEL_USCITA
            //                  )
            //              VALUES
            //                  (
            //                    :LQU-ID-LIQ
            //                    ,:LQU-ID-OGG
            //                    ,:LQU-TP-OGG
            //                    ,:LQU-ID-MOVI-CRZ
            //                    ,:LQU-ID-MOVI-CHIU
            //                     :IND-LQU-ID-MOVI-CHIU
            //                    ,:LQU-DT-INI-EFF-DB
            //                    ,:LQU-DT-END-EFF-DB
            //                    ,:LQU-COD-COMP-ANIA
            //                    ,:LQU-IB-OGG
            //                     :IND-LQU-IB-OGG
            //                    ,:LQU-TP-LIQ
            //                    ,:LQU-DESC-CAU-EVE-SIN-VCHAR
            //                     :IND-LQU-DESC-CAU-EVE-SIN
            //                    ,:LQU-COD-CAU-SIN
            //                     :IND-LQU-COD-CAU-SIN
            //                    ,:LQU-COD-SIN-CATSTRF
            //                     :IND-LQU-COD-SIN-CATSTRF
            //                    ,:LQU-DT-MOR-DB
            //                     :IND-LQU-DT-MOR
            //                    ,:LQU-DT-DEN-DB
            //                     :IND-LQU-DT-DEN
            //                    ,:LQU-DT-PERV-DEN-DB
            //                     :IND-LQU-DT-PERV-DEN
            //                    ,:LQU-DT-RICH-DB
            //                     :IND-LQU-DT-RICH
            //                    ,:LQU-TP-SIN
            //                     :IND-LQU-TP-SIN
            //                    ,:LQU-TP-RISC
            //                     :IND-LQU-TP-RISC
            //                    ,:LQU-TP-MET-RISC
            //                     :IND-LQU-TP-MET-RISC
            //                    ,:LQU-DT-LIQ-DB
            //                     :IND-LQU-DT-LIQ
            //                    ,:LQU-COD-DVS
            //                     :IND-LQU-COD-DVS
            //                    ,:LQU-TOT-IMP-LRD-LIQTO
            //                     :IND-LQU-TOT-IMP-LRD-LIQTO
            //                    ,:LQU-TOT-IMP-PREST
            //                     :IND-LQU-TOT-IMP-PREST
            //                    ,:LQU-TOT-IMP-INTR-PREST
            //                     :IND-LQU-TOT-IMP-INTR-PREST
            //                    ,:LQU-TOT-IMP-UTI
            //                     :IND-LQU-TOT-IMP-UTI
            //                    ,:LQU-TOT-IMP-RIT-TFR
            //                     :IND-LQU-TOT-IMP-RIT-TFR
            //                    ,:LQU-TOT-IMP-RIT-ACC
            //                     :IND-LQU-TOT-IMP-RIT-ACC
            //                    ,:LQU-TOT-IMP-RIT-VIS
            //                     :IND-LQU-TOT-IMP-RIT-VIS
            //                    ,:LQU-TOT-IMPB-TFR
            //                     :IND-LQU-TOT-IMPB-TFR
            //                    ,:LQU-TOT-IMPB-ACC
            //                     :IND-LQU-TOT-IMPB-ACC
            //                    ,:LQU-TOT-IMPB-VIS
            //                     :IND-LQU-TOT-IMPB-VIS
            //                    ,:LQU-TOT-IMP-RIMB
            //                     :IND-LQU-TOT-IMP-RIMB
            //                    ,:LQU-IMPB-IMPST-PRVR
            //                     :IND-LQU-IMPB-IMPST-PRVR
            //                    ,:LQU-IMPST-PRVR
            //                     :IND-LQU-IMPST-PRVR
            //                    ,:LQU-IMPB-IMPST-252
            //                     :IND-LQU-IMPB-IMPST-252
            //                    ,:LQU-IMPST-252
            //                     :IND-LQU-IMPST-252
            //                    ,:LQU-TOT-IMP-IS
            //                     :IND-LQU-TOT-IMP-IS
            //                    ,:LQU-IMP-DIR-LIQ
            //                     :IND-LQU-IMP-DIR-LIQ
            //                    ,:LQU-TOT-IMP-NET-LIQTO
            //                     :IND-LQU-TOT-IMP-NET-LIQTO
            //                    ,:LQU-MONT-END2000
            //                     :IND-LQU-MONT-END2000
            //                    ,:LQU-MONT-END2006
            //                     :IND-LQU-MONT-END2006
            //                    ,:LQU-PC-REN
            //                     :IND-LQU-PC-REN
            //                    ,:LQU-IMP-PNL
            //                     :IND-LQU-IMP-PNL
            //                    ,:LQU-IMPB-IRPEF
            //                     :IND-LQU-IMPB-IRPEF
            //                    ,:LQU-IMPST-IRPEF
            //                     :IND-LQU-IMPST-IRPEF
            //                    ,:LQU-DT-VLT-DB
            //                     :IND-LQU-DT-VLT
            //                    ,:LQU-DT-END-ISTR-DB
            //                     :IND-LQU-DT-END-ISTR
            //                    ,:LQU-TP-RIMB
            //                    ,:LQU-SPE-RCS
            //                     :IND-LQU-SPE-RCS
            //                    ,:LQU-IB-LIQ
            //                     :IND-LQU-IB-LIQ
            //                    ,:LQU-TOT-IAS-ONER-PRVNT
            //                     :IND-LQU-TOT-IAS-ONER-PRVNT
            //                    ,:LQU-TOT-IAS-MGG-SIN
            //                     :IND-LQU-TOT-IAS-MGG-SIN
            //                    ,:LQU-TOT-IAS-RST-DPST
            //                     :IND-LQU-TOT-IAS-RST-DPST
            //                    ,:LQU-IMP-ONER-LIQ
            //                     :IND-LQU-IMP-ONER-LIQ
            //                    ,:LQU-COMPON-TAX-RIMB
            //                     :IND-LQU-COMPON-TAX-RIMB
            //                    ,:LQU-TP-MEZ-PAG
            //                     :IND-LQU-TP-MEZ-PAG
            //                    ,:LQU-IMP-EXCONTR
            //                     :IND-LQU-IMP-EXCONTR
            //                    ,:LQU-IMP-INTR-RIT-PAG
            //                     :IND-LQU-IMP-INTR-RIT-PAG
            //                    ,:LQU-BNS-NON-GODUTO
            //                     :IND-LQU-BNS-NON-GODUTO
            //                    ,:LQU-CNBT-INPSTFM
            //                     :IND-LQU-CNBT-INPSTFM
            //                    ,:LQU-IMPST-DA-RIMB
            //                     :IND-LQU-IMPST-DA-RIMB
            //                    ,:LQU-IMPB-IS
            //                     :IND-LQU-IMPB-IS
            //                    ,:LQU-TAX-SEP
            //                     :IND-LQU-TAX-SEP
            //                    ,:LQU-IMPB-TAX-SEP
            //                     :IND-LQU-IMPB-TAX-SEP
            //                    ,:LQU-IMPB-INTR-SU-PREST
            //                     :IND-LQU-IMPB-INTR-SU-PREST
            //                    ,:LQU-ADDIZ-COMUN
            //                     :IND-LQU-ADDIZ-COMUN
            //                    ,:LQU-IMPB-ADDIZ-COMUN
            //                     :IND-LQU-IMPB-ADDIZ-COMUN
            //                    ,:LQU-ADDIZ-REGION
            //                     :IND-LQU-ADDIZ-REGION
            //                    ,:LQU-IMPB-ADDIZ-REGION
            //                     :IND-LQU-IMPB-ADDIZ-REGION
            //                    ,:LQU-MONT-DAL2007
            //                     :IND-LQU-MONT-DAL2007
            //                    ,:LQU-IMPB-CNBT-INPSTFM
            //                     :IND-LQU-IMPB-CNBT-INPSTFM
            //                    ,:LQU-IMP-LRD-DA-RIMB
            //                     :IND-LQU-IMP-LRD-DA-RIMB
            //                    ,:LQU-IMP-DIR-DA-RIMB
            //                     :IND-LQU-IMP-DIR-DA-RIMB
            //                    ,:LQU-RIS-MAT
            //                     :IND-LQU-RIS-MAT
            //                    ,:LQU-RIS-SPE
            //                     :IND-LQU-RIS-SPE
            //                    ,:LQU-DS-RIGA
            //                    ,:LQU-DS-OPER-SQL
            //                    ,:LQU-DS-VER
            //                    ,:LQU-DS-TS-INI-CPTZ
            //                    ,:LQU-DS-TS-END-CPTZ
            //                    ,:LQU-DS-UTENTE
            //                    ,:LQU-DS-STATO-ELAB
            //                    ,:LQU-TOT-IAS-PNL
            //                     :IND-LQU-TOT-IAS-PNL
            //                    ,:LQU-FL-EVE-GARTO
            //                     :IND-LQU-FL-EVE-GARTO
            //                    ,:LQU-IMP-REN-K1
            //                     :IND-LQU-IMP-REN-K1
            //                    ,:LQU-IMP-REN-K2
            //                     :IND-LQU-IMP-REN-K2
            //                    ,:LQU-IMP-REN-K3
            //                     :IND-LQU-IMP-REN-K3
            //                    ,:LQU-PC-REN-K1
            //                     :IND-LQU-PC-REN-K1
            //                    ,:LQU-PC-REN-K2
            //                     :IND-LQU-PC-REN-K2
            //                    ,:LQU-PC-REN-K3
            //                     :IND-LQU-PC-REN-K3
            //                    ,:LQU-TP-CAUS-ANTIC
            //                     :IND-LQU-TP-CAUS-ANTIC
            //                    ,:LQU-IMP-LRD-LIQTO-RILT
            //                     :IND-LQU-IMP-LRD-LIQTO-RILT
            //                    ,:LQU-IMPST-APPL-RILT
            //                     :IND-LQU-IMPST-APPL-RILT
            //                    ,:LQU-PC-RISC-PARZ
            //                     :IND-LQU-PC-RISC-PARZ
            //                    ,:LQU-IMPST-BOLLO-TOT-V
            //                     :IND-LQU-IMPST-BOLLO-TOT-V
            //                    ,:LQU-IMPST-BOLLO-DETT-C
            //                     :IND-LQU-IMPST-BOLLO-DETT-C
            //                    ,:LQU-IMPST-BOLLO-TOT-SW
            //                     :IND-LQU-IMPST-BOLLO-TOT-SW
            //                    ,:LQU-IMPST-BOLLO-TOT-AA
            //                     :IND-LQU-IMPST-BOLLO-TOT-AA
            //                    ,:LQU-IMPB-VIS-1382011
            //                     :IND-LQU-IMPB-VIS-1382011
            //                    ,:LQU-IMPST-VIS-1382011
            //                     :IND-LQU-IMPST-VIS-1382011
            //                    ,:LQU-IMPB-IS-1382011
            //                     :IND-LQU-IMPB-IS-1382011
            //                    ,:LQU-IMPST-SOST-1382011
            //                     :IND-LQU-IMPST-SOST-1382011
            //                    ,:LQU-PC-ABB-TIT-STAT
            //                     :IND-LQU-PC-ABB-TIT-STAT
            //                    ,:LQU-IMPB-BOLLO-DETT-C
            //                     :IND-LQU-IMPB-BOLLO-DETT-C
            //                    ,:LQU-FL-PRE-COMP
            //                     :IND-LQU-FL-PRE-COMP
            //                    ,:LQU-IMPB-VIS-662014
            //                     :IND-LQU-IMPB-VIS-662014
            //                    ,:LQU-IMPST-VIS-662014
            //                     :IND-LQU-IMPST-VIS-662014
            //                    ,:LQU-IMPB-IS-662014
            //                     :IND-LQU-IMPB-IS-662014
            //                    ,:LQU-IMPST-SOST-662014
            //                     :IND-LQU-IMPST-SOST-662014
            //                    ,:LQU-PC-ABB-TS-662014
            //                     :IND-LQU-PC-ABB-TS-662014
            //                    ,:LQU-IMP-LRD-CALC-CP
            //                     :IND-LQU-IMP-LRD-CALC-CP
            //                    ,:LQU-COS-TUNNEL-USCITA
            //                     :IND-LQU-COS-TUNNEL-USCITA
            //                  )
            //           END-EXEC
            liqDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE LIQ SET
        //                   ID_LIQ                 =
        //                :LQU-ID-LIQ
        //                  ,ID_OGG                 =
        //                :LQU-ID-OGG
        //                  ,TP_OGG                 =
        //                :LQU-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :LQU-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :LQU-ID-MOVI-CHIU
        //                                       :IND-LQU-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :LQU-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :LQU-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :LQU-COD-COMP-ANIA
        //                  ,IB_OGG                 =
        //                :LQU-IB-OGG
        //                                       :IND-LQU-IB-OGG
        //                  ,TP_LIQ                 =
        //                :LQU-TP-LIQ
        //                  ,DESC_CAU_EVE_SIN       =
        //                :LQU-DESC-CAU-EVE-SIN-VCHAR
        //                                       :IND-LQU-DESC-CAU-EVE-SIN
        //                  ,COD_CAU_SIN            =
        //                :LQU-COD-CAU-SIN
        //                                       :IND-LQU-COD-CAU-SIN
        //                  ,COD_SIN_CATSTRF        =
        //                :LQU-COD-SIN-CATSTRF
        //                                       :IND-LQU-COD-SIN-CATSTRF
        //                  ,DT_MOR                 =
        //           :LQU-DT-MOR-DB
        //                                       :IND-LQU-DT-MOR
        //                  ,DT_DEN                 =
        //           :LQU-DT-DEN-DB
        //                                       :IND-LQU-DT-DEN
        //                  ,DT_PERV_DEN            =
        //           :LQU-DT-PERV-DEN-DB
        //                                       :IND-LQU-DT-PERV-DEN
        //                  ,DT_RICH                =
        //           :LQU-DT-RICH-DB
        //                                       :IND-LQU-DT-RICH
        //                  ,TP_SIN                 =
        //                :LQU-TP-SIN
        //                                       :IND-LQU-TP-SIN
        //                  ,TP_RISC                =
        //                :LQU-TP-RISC
        //                                       :IND-LQU-TP-RISC
        //                  ,TP_MET_RISC            =
        //                :LQU-TP-MET-RISC
        //                                       :IND-LQU-TP-MET-RISC
        //                  ,DT_LIQ                 =
        //           :LQU-DT-LIQ-DB
        //                                       :IND-LQU-DT-LIQ
        //                  ,COD_DVS                =
        //                :LQU-COD-DVS
        //                                       :IND-LQU-COD-DVS
        //                  ,TOT_IMP_LRD_LIQTO      =
        //                :LQU-TOT-IMP-LRD-LIQTO
        //                                       :IND-LQU-TOT-IMP-LRD-LIQTO
        //                  ,TOT_IMP_PREST          =
        //                :LQU-TOT-IMP-PREST
        //                                       :IND-LQU-TOT-IMP-PREST
        //                  ,TOT_IMP_INTR_PREST     =
        //                :LQU-TOT-IMP-INTR-PREST
        //                                       :IND-LQU-TOT-IMP-INTR-PREST
        //                  ,TOT_IMP_UTI            =
        //                :LQU-TOT-IMP-UTI
        //                                       :IND-LQU-TOT-IMP-UTI
        //                  ,TOT_IMP_RIT_TFR        =
        //                :LQU-TOT-IMP-RIT-TFR
        //                                       :IND-LQU-TOT-IMP-RIT-TFR
        //                  ,TOT_IMP_RIT_ACC        =
        //                :LQU-TOT-IMP-RIT-ACC
        //                                       :IND-LQU-TOT-IMP-RIT-ACC
        //                  ,TOT_IMP_RIT_VIS        =
        //                :LQU-TOT-IMP-RIT-VIS
        //                                       :IND-LQU-TOT-IMP-RIT-VIS
        //                  ,TOT_IMPB_TFR           =
        //                :LQU-TOT-IMPB-TFR
        //                                       :IND-LQU-TOT-IMPB-TFR
        //                  ,TOT_IMPB_ACC           =
        //                :LQU-TOT-IMPB-ACC
        //                                       :IND-LQU-TOT-IMPB-ACC
        //                  ,TOT_IMPB_VIS           =
        //                :LQU-TOT-IMPB-VIS
        //                                       :IND-LQU-TOT-IMPB-VIS
        //                  ,TOT_IMP_RIMB           =
        //                :LQU-TOT-IMP-RIMB
        //                                       :IND-LQU-TOT-IMP-RIMB
        //                  ,IMPB_IMPST_PRVR        =
        //                :LQU-IMPB-IMPST-PRVR
        //                                       :IND-LQU-IMPB-IMPST-PRVR
        //                  ,IMPST_PRVR             =
        //                :LQU-IMPST-PRVR
        //                                       :IND-LQU-IMPST-PRVR
        //                  ,IMPB_IMPST_252         =
        //                :LQU-IMPB-IMPST-252
        //                                       :IND-LQU-IMPB-IMPST-252
        //                  ,IMPST_252              =
        //                :LQU-IMPST-252
        //                                       :IND-LQU-IMPST-252
        //                  ,TOT_IMP_IS             =
        //                :LQU-TOT-IMP-IS
        //                                       :IND-LQU-TOT-IMP-IS
        //                  ,IMP_DIR_LIQ            =
        //                :LQU-IMP-DIR-LIQ
        //                                       :IND-LQU-IMP-DIR-LIQ
        //                  ,TOT_IMP_NET_LIQTO      =
        //                :LQU-TOT-IMP-NET-LIQTO
        //                                       :IND-LQU-TOT-IMP-NET-LIQTO
        //                  ,MONT_END2000           =
        //                :LQU-MONT-END2000
        //                                       :IND-LQU-MONT-END2000
        //                  ,MONT_END2006           =
        //                :LQU-MONT-END2006
        //                                       :IND-LQU-MONT-END2006
        //                  ,PC_REN                 =
        //                :LQU-PC-REN
        //                                       :IND-LQU-PC-REN
        //                  ,IMP_PNL                =
        //                :LQU-IMP-PNL
        //                                       :IND-LQU-IMP-PNL
        //                  ,IMPB_IRPEF             =
        //                :LQU-IMPB-IRPEF
        //                                       :IND-LQU-IMPB-IRPEF
        //                  ,IMPST_IRPEF            =
        //                :LQU-IMPST-IRPEF
        //                                       :IND-LQU-IMPST-IRPEF
        //                  ,DT_VLT                 =
        //           :LQU-DT-VLT-DB
        //                                       :IND-LQU-DT-VLT
        //                  ,DT_END_ISTR            =
        //           :LQU-DT-END-ISTR-DB
        //                                       :IND-LQU-DT-END-ISTR
        //                  ,TP_RIMB                =
        //                :LQU-TP-RIMB
        //                  ,SPE_RCS                =
        //                :LQU-SPE-RCS
        //                                       :IND-LQU-SPE-RCS
        //                  ,IB_LIQ                 =
        //                :LQU-IB-LIQ
        //                                       :IND-LQU-IB-LIQ
        //                  ,TOT_IAS_ONER_PRVNT     =
        //                :LQU-TOT-IAS-ONER-PRVNT
        //                                       :IND-LQU-TOT-IAS-ONER-PRVNT
        //                  ,TOT_IAS_MGG_SIN        =
        //                :LQU-TOT-IAS-MGG-SIN
        //                                       :IND-LQU-TOT-IAS-MGG-SIN
        //                  ,TOT_IAS_RST_DPST       =
        //                :LQU-TOT-IAS-RST-DPST
        //                                       :IND-LQU-TOT-IAS-RST-DPST
        //                  ,IMP_ONER_LIQ           =
        //                :LQU-IMP-ONER-LIQ
        //                                       :IND-LQU-IMP-ONER-LIQ
        //                  ,COMPON_TAX_RIMB        =
        //                :LQU-COMPON-TAX-RIMB
        //                                       :IND-LQU-COMPON-TAX-RIMB
        //                  ,TP_MEZ_PAG             =
        //                :LQU-TP-MEZ-PAG
        //                                       :IND-LQU-TP-MEZ-PAG
        //                  ,IMP_EXCONTR            =
        //                :LQU-IMP-EXCONTR
        //                                       :IND-LQU-IMP-EXCONTR
        //                  ,IMP_INTR_RIT_PAG       =
        //                :LQU-IMP-INTR-RIT-PAG
        //                                       :IND-LQU-IMP-INTR-RIT-PAG
        //                  ,BNS_NON_GODUTO         =
        //                :LQU-BNS-NON-GODUTO
        //                                       :IND-LQU-BNS-NON-GODUTO
        //                  ,CNBT_INPSTFM           =
        //                :LQU-CNBT-INPSTFM
        //                                       :IND-LQU-CNBT-INPSTFM
        //                  ,IMPST_DA_RIMB          =
        //                :LQU-IMPST-DA-RIMB
        //                                       :IND-LQU-IMPST-DA-RIMB
        //                  ,IMPB_IS                =
        //                :LQU-IMPB-IS
        //                                       :IND-LQU-IMPB-IS
        //                  ,TAX_SEP                =
        //                :LQU-TAX-SEP
        //                                       :IND-LQU-TAX-SEP
        //                  ,IMPB_TAX_SEP           =
        //                :LQU-IMPB-TAX-SEP
        //                                       :IND-LQU-IMPB-TAX-SEP
        //                  ,IMPB_INTR_SU_PREST     =
        //                :LQU-IMPB-INTR-SU-PREST
        //                                       :IND-LQU-IMPB-INTR-SU-PREST
        //                  ,ADDIZ_COMUN            =
        //                :LQU-ADDIZ-COMUN
        //                                       :IND-LQU-ADDIZ-COMUN
        //                  ,IMPB_ADDIZ_COMUN       =
        //                :LQU-IMPB-ADDIZ-COMUN
        //                                       :IND-LQU-IMPB-ADDIZ-COMUN
        //                  ,ADDIZ_REGION           =
        //                :LQU-ADDIZ-REGION
        //                                       :IND-LQU-ADDIZ-REGION
        //                  ,IMPB_ADDIZ_REGION      =
        //                :LQU-IMPB-ADDIZ-REGION
        //                                       :IND-LQU-IMPB-ADDIZ-REGION
        //                  ,MONT_DAL2007           =
        //                :LQU-MONT-DAL2007
        //                                       :IND-LQU-MONT-DAL2007
        //                  ,IMPB_CNBT_INPSTFM      =
        //                :LQU-IMPB-CNBT-INPSTFM
        //                                       :IND-LQU-IMPB-CNBT-INPSTFM
        //                  ,IMP_LRD_DA_RIMB        =
        //                :LQU-IMP-LRD-DA-RIMB
        //                                       :IND-LQU-IMP-LRD-DA-RIMB
        //                  ,IMP_DIR_DA_RIMB        =
        //                :LQU-IMP-DIR-DA-RIMB
        //                                       :IND-LQU-IMP-DIR-DA-RIMB
        //                  ,RIS_MAT                =
        //                :LQU-RIS-MAT
        //                                       :IND-LQU-RIS-MAT
        //                  ,RIS_SPE                =
        //                :LQU-RIS-SPE
        //                                       :IND-LQU-RIS-SPE
        //                  ,DS_RIGA                =
        //                :LQU-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :LQU-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :LQU-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :LQU-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :LQU-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :LQU-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :LQU-DS-STATO-ELAB
        //                  ,TOT_IAS_PNL            =
        //                :LQU-TOT-IAS-PNL
        //                                       :IND-LQU-TOT-IAS-PNL
        //                  ,FL_EVE_GARTO           =
        //                :LQU-FL-EVE-GARTO
        //                                       :IND-LQU-FL-EVE-GARTO
        //                  ,IMP_REN_K1             =
        //                :LQU-IMP-REN-K1
        //                                       :IND-LQU-IMP-REN-K1
        //                  ,IMP_REN_K2             =
        //                :LQU-IMP-REN-K2
        //                                       :IND-LQU-IMP-REN-K2
        //                  ,IMP_REN_K3             =
        //                :LQU-IMP-REN-K3
        //                                       :IND-LQU-IMP-REN-K3
        //                  ,PC_REN_K1              =
        //                :LQU-PC-REN-K1
        //                                       :IND-LQU-PC-REN-K1
        //                  ,PC_REN_K2              =
        //                :LQU-PC-REN-K2
        //                                       :IND-LQU-PC-REN-K2
        //                  ,PC_REN_K3              =
        //                :LQU-PC-REN-K3
        //                                       :IND-LQU-PC-REN-K3
        //                  ,TP_CAUS_ANTIC          =
        //                :LQU-TP-CAUS-ANTIC
        //                                       :IND-LQU-TP-CAUS-ANTIC
        //                  ,IMP_LRD_LIQTO_RILT     =
        //                :LQU-IMP-LRD-LIQTO-RILT
        //                                       :IND-LQU-IMP-LRD-LIQTO-RILT
        //                  ,IMPST_APPL_RILT        =
        //                :LQU-IMPST-APPL-RILT
        //                                       :IND-LQU-IMPST-APPL-RILT
        //                  ,PC_RISC_PARZ           =
        //                :LQU-PC-RISC-PARZ
        //                                       :IND-LQU-PC-RISC-PARZ
        //                  ,IMPST_BOLLO_TOT_V      =
        //                :LQU-IMPST-BOLLO-TOT-V
        //                                       :IND-LQU-IMPST-BOLLO-TOT-V
        //                  ,IMPST_BOLLO_DETT_C     =
        //                :LQU-IMPST-BOLLO-DETT-C
        //                                       :IND-LQU-IMPST-BOLLO-DETT-C
        //                  ,IMPST_BOLLO_TOT_SW     =
        //                :LQU-IMPST-BOLLO-TOT-SW
        //                                       :IND-LQU-IMPST-BOLLO-TOT-SW
        //                  ,IMPST_BOLLO_TOT_AA     =
        //                :LQU-IMPST-BOLLO-TOT-AA
        //                                       :IND-LQU-IMPST-BOLLO-TOT-AA
        //                  ,IMPB_VIS_1382011       =
        //                :LQU-IMPB-VIS-1382011
        //                                       :IND-LQU-IMPB-VIS-1382011
        //                  ,IMPST_VIS_1382011      =
        //                :LQU-IMPST-VIS-1382011
        //                                       :IND-LQU-IMPST-VIS-1382011
        //                  ,IMPB_IS_1382011        =
        //                :LQU-IMPB-IS-1382011
        //                                       :IND-LQU-IMPB-IS-1382011
        //                  ,IMPST_SOST_1382011     =
        //                :LQU-IMPST-SOST-1382011
        //                                       :IND-LQU-IMPST-SOST-1382011
        //                  ,PC_ABB_TIT_STAT        =
        //                :LQU-PC-ABB-TIT-STAT
        //                                       :IND-LQU-PC-ABB-TIT-STAT
        //                  ,IMPB_BOLLO_DETT_C      =
        //                :LQU-IMPB-BOLLO-DETT-C
        //                                       :IND-LQU-IMPB-BOLLO-DETT-C
        //                  ,FL_PRE_COMP            =
        //                :LQU-FL-PRE-COMP
        //                                       :IND-LQU-FL-PRE-COMP
        //                  ,IMPB_VIS_662014        =
        //                :LQU-IMPB-VIS-662014
        //                                       :IND-LQU-IMPB-VIS-662014
        //                  ,IMPST_VIS_662014       =
        //                :LQU-IMPST-VIS-662014
        //                                       :IND-LQU-IMPST-VIS-662014
        //                  ,IMPB_IS_662014         =
        //                :LQU-IMPB-IS-662014
        //                                       :IND-LQU-IMPB-IS-662014
        //                  ,IMPST_SOST_662014      =
        //                :LQU-IMPST-SOST-662014
        //                                       :IND-LQU-IMPST-SOST-662014
        //                  ,PC_ABB_TS_662014       =
        //                :LQU-PC-ABB-TS-662014
        //                                       :IND-LQU-PC-ABB-TS-662014
        //                  ,IMP_LRD_CALC_CP        =
        //                :LQU-IMP-LRD-CALC-CP
        //                                       :IND-LQU-IMP-LRD-CALC-CP
        //                  ,COS_TUNNEL_USCITA      =
        //                :LQU-COS-TUNNEL-USCITA
        //                                       :IND-LQU-COS-TUNNEL-USCITA
        //                WHERE     DS_RIGA = :LQU-DS-RIGA
        //           END-EXEC.
        liqDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM LIQ
        //                WHERE     DS_RIGA = :LQU-DS-RIGA
        //           END-EXEC.
        liqDao.deleteByLquDsRiga(liq.getLquDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-LQU CURSOR FOR
        //              SELECT
        //                     ID_LIQ
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,TP_LIQ
        //                    ,DESC_CAU_EVE_SIN
        //                    ,COD_CAU_SIN
        //                    ,COD_SIN_CATSTRF
        //                    ,DT_MOR
        //                    ,DT_DEN
        //                    ,DT_PERV_DEN
        //                    ,DT_RICH
        //                    ,TP_SIN
        //                    ,TP_RISC
        //                    ,TP_MET_RISC
        //                    ,DT_LIQ
        //                    ,COD_DVS
        //                    ,TOT_IMP_LRD_LIQTO
        //                    ,TOT_IMP_PREST
        //                    ,TOT_IMP_INTR_PREST
        //                    ,TOT_IMP_UTI
        //                    ,TOT_IMP_RIT_TFR
        //                    ,TOT_IMP_RIT_ACC
        //                    ,TOT_IMP_RIT_VIS
        //                    ,TOT_IMPB_TFR
        //                    ,TOT_IMPB_ACC
        //                    ,TOT_IMPB_VIS
        //                    ,TOT_IMP_RIMB
        //                    ,IMPB_IMPST_PRVR
        //                    ,IMPST_PRVR
        //                    ,IMPB_IMPST_252
        //                    ,IMPST_252
        //                    ,TOT_IMP_IS
        //                    ,IMP_DIR_LIQ
        //                    ,TOT_IMP_NET_LIQTO
        //                    ,MONT_END2000
        //                    ,MONT_END2006
        //                    ,PC_REN
        //                    ,IMP_PNL
        //                    ,IMPB_IRPEF
        //                    ,IMPST_IRPEF
        //                    ,DT_VLT
        //                    ,DT_END_ISTR
        //                    ,TP_RIMB
        //                    ,SPE_RCS
        //                    ,IB_LIQ
        //                    ,TOT_IAS_ONER_PRVNT
        //                    ,TOT_IAS_MGG_SIN
        //                    ,TOT_IAS_RST_DPST
        //                    ,IMP_ONER_LIQ
        //                    ,COMPON_TAX_RIMB
        //                    ,TP_MEZ_PAG
        //                    ,IMP_EXCONTR
        //                    ,IMP_INTR_RIT_PAG
        //                    ,BNS_NON_GODUTO
        //                    ,CNBT_INPSTFM
        //                    ,IMPST_DA_RIMB
        //                    ,IMPB_IS
        //                    ,TAX_SEP
        //                    ,IMPB_TAX_SEP
        //                    ,IMPB_INTR_SU_PREST
        //                    ,ADDIZ_COMUN
        //                    ,IMPB_ADDIZ_COMUN
        //                    ,ADDIZ_REGION
        //                    ,IMPB_ADDIZ_REGION
        //                    ,MONT_DAL2007
        //                    ,IMPB_CNBT_INPSTFM
        //                    ,IMP_LRD_DA_RIMB
        //                    ,IMP_DIR_DA_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TOT_IAS_PNL
        //                    ,FL_EVE_GARTO
        //                    ,IMP_REN_K1
        //                    ,IMP_REN_K2
        //                    ,IMP_REN_K3
        //                    ,PC_REN_K1
        //                    ,PC_REN_K2
        //                    ,PC_REN_K3
        //                    ,TP_CAUS_ANTIC
        //                    ,IMP_LRD_LIQTO_RILT
        //                    ,IMPST_APPL_RILT
        //                    ,PC_RISC_PARZ
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_TOT_SW
        //                    ,IMPST_BOLLO_TOT_AA
        //                    ,IMPB_VIS_1382011
        //                    ,IMPST_VIS_1382011
        //                    ,IMPB_IS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,PC_ABB_TIT_STAT
        //                    ,IMPB_BOLLO_DETT_C
        //                    ,FL_PRE_COMP
        //                    ,IMPB_VIS_662014
        //                    ,IMPST_VIS_662014
        //                    ,IMPB_IS_662014
        //                    ,IMPST_SOST_662014
        //                    ,PC_ABB_TS_662014
        //                    ,IMP_LRD_CALC_CP
        //                    ,COS_TUNNEL_USCITA
        //              FROM LIQ
        //              WHERE     ID_LIQ = :LQU-ID-LIQ
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_LIQ
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,TP_LIQ
        //                ,DESC_CAU_EVE_SIN
        //                ,COD_CAU_SIN
        //                ,COD_SIN_CATSTRF
        //                ,DT_MOR
        //                ,DT_DEN
        //                ,DT_PERV_DEN
        //                ,DT_RICH
        //                ,TP_SIN
        //                ,TP_RISC
        //                ,TP_MET_RISC
        //                ,DT_LIQ
        //                ,COD_DVS
        //                ,TOT_IMP_LRD_LIQTO
        //                ,TOT_IMP_PREST
        //                ,TOT_IMP_INTR_PREST
        //                ,TOT_IMP_UTI
        //                ,TOT_IMP_RIT_TFR
        //                ,TOT_IMP_RIT_ACC
        //                ,TOT_IMP_RIT_VIS
        //                ,TOT_IMPB_TFR
        //                ,TOT_IMPB_ACC
        //                ,TOT_IMPB_VIS
        //                ,TOT_IMP_RIMB
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,TOT_IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,TOT_IMP_NET_LIQTO
        //                ,MONT_END2000
        //                ,MONT_END2006
        //                ,PC_REN
        //                ,IMP_PNL
        //                ,IMPB_IRPEF
        //                ,IMPST_IRPEF
        //                ,DT_VLT
        //                ,DT_END_ISTR
        //                ,TP_RIMB
        //                ,SPE_RCS
        //                ,IB_LIQ
        //                ,TOT_IAS_ONER_PRVNT
        //                ,TOT_IAS_MGG_SIN
        //                ,TOT_IAS_RST_DPST
        //                ,IMP_ONER_LIQ
        //                ,COMPON_TAX_RIMB
        //                ,TP_MEZ_PAG
        //                ,IMP_EXCONTR
        //                ,IMP_INTR_RIT_PAG
        //                ,BNS_NON_GODUTO
        //                ,CNBT_INPSTFM
        //                ,IMPST_DA_RIMB
        //                ,IMPB_IS
        //                ,TAX_SEP
        //                ,IMPB_TAX_SEP
        //                ,IMPB_INTR_SU_PREST
        //                ,ADDIZ_COMUN
        //                ,IMPB_ADDIZ_COMUN
        //                ,ADDIZ_REGION
        //                ,IMPB_ADDIZ_REGION
        //                ,MONT_DAL2007
        //                ,IMPB_CNBT_INPSTFM
        //                ,IMP_LRD_DA_RIMB
        //                ,IMP_DIR_DA_RIMB
        //                ,RIS_MAT
        //                ,RIS_SPE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TOT_IAS_PNL
        //                ,FL_EVE_GARTO
        //                ,IMP_REN_K1
        //                ,IMP_REN_K2
        //                ,IMP_REN_K3
        //                ,PC_REN_K1
        //                ,PC_REN_K2
        //                ,PC_REN_K3
        //                ,TP_CAUS_ANTIC
        //                ,IMP_LRD_LIQTO_RILT
        //                ,IMPST_APPL_RILT
        //                ,PC_RISC_PARZ
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_TOT_SW
        //                ,IMPST_BOLLO_TOT_AA
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPB_IS_1382011
        //                ,IMPST_SOST_1382011
        //                ,PC_ABB_TIT_STAT
        //                ,IMPB_BOLLO_DETT_C
        //                ,FL_PRE_COMP
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //                ,PC_ABB_TS_662014
        //                ,IMP_LRD_CALC_CP
        //                ,COS_TUNNEL_USCITA
        //             INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //             FROM LIQ
        //             WHERE     ID_LIQ = :LQU-ID-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        liqDao.selectRec(liq.getLquIdLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE LIQ SET
        //                   ID_LIQ                 =
        //                :LQU-ID-LIQ
        //                  ,ID_OGG                 =
        //                :LQU-ID-OGG
        //                  ,TP_OGG                 =
        //                :LQU-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :LQU-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :LQU-ID-MOVI-CHIU
        //                                       :IND-LQU-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :LQU-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :LQU-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :LQU-COD-COMP-ANIA
        //                  ,IB_OGG                 =
        //                :LQU-IB-OGG
        //                                       :IND-LQU-IB-OGG
        //                  ,TP_LIQ                 =
        //                :LQU-TP-LIQ
        //                  ,DESC_CAU_EVE_SIN       =
        //                :LQU-DESC-CAU-EVE-SIN-VCHAR
        //                                       :IND-LQU-DESC-CAU-EVE-SIN
        //                  ,COD_CAU_SIN            =
        //                :LQU-COD-CAU-SIN
        //                                       :IND-LQU-COD-CAU-SIN
        //                  ,COD_SIN_CATSTRF        =
        //                :LQU-COD-SIN-CATSTRF
        //                                       :IND-LQU-COD-SIN-CATSTRF
        //                  ,DT_MOR                 =
        //           :LQU-DT-MOR-DB
        //                                       :IND-LQU-DT-MOR
        //                  ,DT_DEN                 =
        //           :LQU-DT-DEN-DB
        //                                       :IND-LQU-DT-DEN
        //                  ,DT_PERV_DEN            =
        //           :LQU-DT-PERV-DEN-DB
        //                                       :IND-LQU-DT-PERV-DEN
        //                  ,DT_RICH                =
        //           :LQU-DT-RICH-DB
        //                                       :IND-LQU-DT-RICH
        //                  ,TP_SIN                 =
        //                :LQU-TP-SIN
        //                                       :IND-LQU-TP-SIN
        //                  ,TP_RISC                =
        //                :LQU-TP-RISC
        //                                       :IND-LQU-TP-RISC
        //                  ,TP_MET_RISC            =
        //                :LQU-TP-MET-RISC
        //                                       :IND-LQU-TP-MET-RISC
        //                  ,DT_LIQ                 =
        //           :LQU-DT-LIQ-DB
        //                                       :IND-LQU-DT-LIQ
        //                  ,COD_DVS                =
        //                :LQU-COD-DVS
        //                                       :IND-LQU-COD-DVS
        //                  ,TOT_IMP_LRD_LIQTO      =
        //                :LQU-TOT-IMP-LRD-LIQTO
        //                                       :IND-LQU-TOT-IMP-LRD-LIQTO
        //                  ,TOT_IMP_PREST          =
        //                :LQU-TOT-IMP-PREST
        //                                       :IND-LQU-TOT-IMP-PREST
        //                  ,TOT_IMP_INTR_PREST     =
        //                :LQU-TOT-IMP-INTR-PREST
        //                                       :IND-LQU-TOT-IMP-INTR-PREST
        //                  ,TOT_IMP_UTI            =
        //                :LQU-TOT-IMP-UTI
        //                                       :IND-LQU-TOT-IMP-UTI
        //                  ,TOT_IMP_RIT_TFR        =
        //                :LQU-TOT-IMP-RIT-TFR
        //                                       :IND-LQU-TOT-IMP-RIT-TFR
        //                  ,TOT_IMP_RIT_ACC        =
        //                :LQU-TOT-IMP-RIT-ACC
        //                                       :IND-LQU-TOT-IMP-RIT-ACC
        //                  ,TOT_IMP_RIT_VIS        =
        //                :LQU-TOT-IMP-RIT-VIS
        //                                       :IND-LQU-TOT-IMP-RIT-VIS
        //                  ,TOT_IMPB_TFR           =
        //                :LQU-TOT-IMPB-TFR
        //                                       :IND-LQU-TOT-IMPB-TFR
        //                  ,TOT_IMPB_ACC           =
        //                :LQU-TOT-IMPB-ACC
        //                                       :IND-LQU-TOT-IMPB-ACC
        //                  ,TOT_IMPB_VIS           =
        //                :LQU-TOT-IMPB-VIS
        //                                       :IND-LQU-TOT-IMPB-VIS
        //                  ,TOT_IMP_RIMB           =
        //                :LQU-TOT-IMP-RIMB
        //                                       :IND-LQU-TOT-IMP-RIMB
        //                  ,IMPB_IMPST_PRVR        =
        //                :LQU-IMPB-IMPST-PRVR
        //                                       :IND-LQU-IMPB-IMPST-PRVR
        //                  ,IMPST_PRVR             =
        //                :LQU-IMPST-PRVR
        //                                       :IND-LQU-IMPST-PRVR
        //                  ,IMPB_IMPST_252         =
        //                :LQU-IMPB-IMPST-252
        //                                       :IND-LQU-IMPB-IMPST-252
        //                  ,IMPST_252              =
        //                :LQU-IMPST-252
        //                                       :IND-LQU-IMPST-252
        //                  ,TOT_IMP_IS             =
        //                :LQU-TOT-IMP-IS
        //                                       :IND-LQU-TOT-IMP-IS
        //                  ,IMP_DIR_LIQ            =
        //                :LQU-IMP-DIR-LIQ
        //                                       :IND-LQU-IMP-DIR-LIQ
        //                  ,TOT_IMP_NET_LIQTO      =
        //                :LQU-TOT-IMP-NET-LIQTO
        //                                       :IND-LQU-TOT-IMP-NET-LIQTO
        //                  ,MONT_END2000           =
        //                :LQU-MONT-END2000
        //                                       :IND-LQU-MONT-END2000
        //                  ,MONT_END2006           =
        //                :LQU-MONT-END2006
        //                                       :IND-LQU-MONT-END2006
        //                  ,PC_REN                 =
        //                :LQU-PC-REN
        //                                       :IND-LQU-PC-REN
        //                  ,IMP_PNL                =
        //                :LQU-IMP-PNL
        //                                       :IND-LQU-IMP-PNL
        //                  ,IMPB_IRPEF             =
        //                :LQU-IMPB-IRPEF
        //                                       :IND-LQU-IMPB-IRPEF
        //                  ,IMPST_IRPEF            =
        //                :LQU-IMPST-IRPEF
        //                                       :IND-LQU-IMPST-IRPEF
        //                  ,DT_VLT                 =
        //           :LQU-DT-VLT-DB
        //                                       :IND-LQU-DT-VLT
        //                  ,DT_END_ISTR            =
        //           :LQU-DT-END-ISTR-DB
        //                                       :IND-LQU-DT-END-ISTR
        //                  ,TP_RIMB                =
        //                :LQU-TP-RIMB
        //                  ,SPE_RCS                =
        //                :LQU-SPE-RCS
        //                                       :IND-LQU-SPE-RCS
        //                  ,IB_LIQ                 =
        //                :LQU-IB-LIQ
        //                                       :IND-LQU-IB-LIQ
        //                  ,TOT_IAS_ONER_PRVNT     =
        //                :LQU-TOT-IAS-ONER-PRVNT
        //                                       :IND-LQU-TOT-IAS-ONER-PRVNT
        //                  ,TOT_IAS_MGG_SIN        =
        //                :LQU-TOT-IAS-MGG-SIN
        //                                       :IND-LQU-TOT-IAS-MGG-SIN
        //                  ,TOT_IAS_RST_DPST       =
        //                :LQU-TOT-IAS-RST-DPST
        //                                       :IND-LQU-TOT-IAS-RST-DPST
        //                  ,IMP_ONER_LIQ           =
        //                :LQU-IMP-ONER-LIQ
        //                                       :IND-LQU-IMP-ONER-LIQ
        //                  ,COMPON_TAX_RIMB        =
        //                :LQU-COMPON-TAX-RIMB
        //                                       :IND-LQU-COMPON-TAX-RIMB
        //                  ,TP_MEZ_PAG             =
        //                :LQU-TP-MEZ-PAG
        //                                       :IND-LQU-TP-MEZ-PAG
        //                  ,IMP_EXCONTR            =
        //                :LQU-IMP-EXCONTR
        //                                       :IND-LQU-IMP-EXCONTR
        //                  ,IMP_INTR_RIT_PAG       =
        //                :LQU-IMP-INTR-RIT-PAG
        //                                       :IND-LQU-IMP-INTR-RIT-PAG
        //                  ,BNS_NON_GODUTO         =
        //                :LQU-BNS-NON-GODUTO
        //                                       :IND-LQU-BNS-NON-GODUTO
        //                  ,CNBT_INPSTFM           =
        //                :LQU-CNBT-INPSTFM
        //                                       :IND-LQU-CNBT-INPSTFM
        //                  ,IMPST_DA_RIMB          =
        //                :LQU-IMPST-DA-RIMB
        //                                       :IND-LQU-IMPST-DA-RIMB
        //                  ,IMPB_IS                =
        //                :LQU-IMPB-IS
        //                                       :IND-LQU-IMPB-IS
        //                  ,TAX_SEP                =
        //                :LQU-TAX-SEP
        //                                       :IND-LQU-TAX-SEP
        //                  ,IMPB_TAX_SEP           =
        //                :LQU-IMPB-TAX-SEP
        //                                       :IND-LQU-IMPB-TAX-SEP
        //                  ,IMPB_INTR_SU_PREST     =
        //                :LQU-IMPB-INTR-SU-PREST
        //                                       :IND-LQU-IMPB-INTR-SU-PREST
        //                  ,ADDIZ_COMUN            =
        //                :LQU-ADDIZ-COMUN
        //                                       :IND-LQU-ADDIZ-COMUN
        //                  ,IMPB_ADDIZ_COMUN       =
        //                :LQU-IMPB-ADDIZ-COMUN
        //                                       :IND-LQU-IMPB-ADDIZ-COMUN
        //                  ,ADDIZ_REGION           =
        //                :LQU-ADDIZ-REGION
        //                                       :IND-LQU-ADDIZ-REGION
        //                  ,IMPB_ADDIZ_REGION      =
        //                :LQU-IMPB-ADDIZ-REGION
        //                                       :IND-LQU-IMPB-ADDIZ-REGION
        //                  ,MONT_DAL2007           =
        //                :LQU-MONT-DAL2007
        //                                       :IND-LQU-MONT-DAL2007
        //                  ,IMPB_CNBT_INPSTFM      =
        //                :LQU-IMPB-CNBT-INPSTFM
        //                                       :IND-LQU-IMPB-CNBT-INPSTFM
        //                  ,IMP_LRD_DA_RIMB        =
        //                :LQU-IMP-LRD-DA-RIMB
        //                                       :IND-LQU-IMP-LRD-DA-RIMB
        //                  ,IMP_DIR_DA_RIMB        =
        //                :LQU-IMP-DIR-DA-RIMB
        //                                       :IND-LQU-IMP-DIR-DA-RIMB
        //                  ,RIS_MAT                =
        //                :LQU-RIS-MAT
        //                                       :IND-LQU-RIS-MAT
        //                  ,RIS_SPE                =
        //                :LQU-RIS-SPE
        //                                       :IND-LQU-RIS-SPE
        //                  ,DS_RIGA                =
        //                :LQU-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :LQU-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :LQU-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :LQU-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :LQU-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :LQU-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :LQU-DS-STATO-ELAB
        //                  ,TOT_IAS_PNL            =
        //                :LQU-TOT-IAS-PNL
        //                                       :IND-LQU-TOT-IAS-PNL
        //                  ,FL_EVE_GARTO           =
        //                :LQU-FL-EVE-GARTO
        //                                       :IND-LQU-FL-EVE-GARTO
        //                  ,IMP_REN_K1             =
        //                :LQU-IMP-REN-K1
        //                                       :IND-LQU-IMP-REN-K1
        //                  ,IMP_REN_K2             =
        //                :LQU-IMP-REN-K2
        //                                       :IND-LQU-IMP-REN-K2
        //                  ,IMP_REN_K3             =
        //                :LQU-IMP-REN-K3
        //                                       :IND-LQU-IMP-REN-K3
        //                  ,PC_REN_K1              =
        //                :LQU-PC-REN-K1
        //                                       :IND-LQU-PC-REN-K1
        //                  ,PC_REN_K2              =
        //                :LQU-PC-REN-K2
        //                                       :IND-LQU-PC-REN-K2
        //                  ,PC_REN_K3              =
        //                :LQU-PC-REN-K3
        //                                       :IND-LQU-PC-REN-K3
        //                  ,TP_CAUS_ANTIC          =
        //                :LQU-TP-CAUS-ANTIC
        //                                       :IND-LQU-TP-CAUS-ANTIC
        //                  ,IMP_LRD_LIQTO_RILT     =
        //                :LQU-IMP-LRD-LIQTO-RILT
        //                                       :IND-LQU-IMP-LRD-LIQTO-RILT
        //                  ,IMPST_APPL_RILT        =
        //                :LQU-IMPST-APPL-RILT
        //                                       :IND-LQU-IMPST-APPL-RILT
        //                  ,PC_RISC_PARZ           =
        //                :LQU-PC-RISC-PARZ
        //                                       :IND-LQU-PC-RISC-PARZ
        //                  ,IMPST_BOLLO_TOT_V      =
        //                :LQU-IMPST-BOLLO-TOT-V
        //                                       :IND-LQU-IMPST-BOLLO-TOT-V
        //                  ,IMPST_BOLLO_DETT_C     =
        //                :LQU-IMPST-BOLLO-DETT-C
        //                                       :IND-LQU-IMPST-BOLLO-DETT-C
        //                  ,IMPST_BOLLO_TOT_SW     =
        //                :LQU-IMPST-BOLLO-TOT-SW
        //                                       :IND-LQU-IMPST-BOLLO-TOT-SW
        //                  ,IMPST_BOLLO_TOT_AA     =
        //                :LQU-IMPST-BOLLO-TOT-AA
        //                                       :IND-LQU-IMPST-BOLLO-TOT-AA
        //                  ,IMPB_VIS_1382011       =
        //                :LQU-IMPB-VIS-1382011
        //                                       :IND-LQU-IMPB-VIS-1382011
        //                  ,IMPST_VIS_1382011      =
        //                :LQU-IMPST-VIS-1382011
        //                                       :IND-LQU-IMPST-VIS-1382011
        //                  ,IMPB_IS_1382011        =
        //                :LQU-IMPB-IS-1382011
        //                                       :IND-LQU-IMPB-IS-1382011
        //                  ,IMPST_SOST_1382011     =
        //                :LQU-IMPST-SOST-1382011
        //                                       :IND-LQU-IMPST-SOST-1382011
        //                  ,PC_ABB_TIT_STAT        =
        //                :LQU-PC-ABB-TIT-STAT
        //                                       :IND-LQU-PC-ABB-TIT-STAT
        //                  ,IMPB_BOLLO_DETT_C      =
        //                :LQU-IMPB-BOLLO-DETT-C
        //                                       :IND-LQU-IMPB-BOLLO-DETT-C
        //                  ,FL_PRE_COMP            =
        //                :LQU-FL-PRE-COMP
        //                                       :IND-LQU-FL-PRE-COMP
        //                  ,IMPB_VIS_662014        =
        //                :LQU-IMPB-VIS-662014
        //                                       :IND-LQU-IMPB-VIS-662014
        //                  ,IMPST_VIS_662014       =
        //                :LQU-IMPST-VIS-662014
        //                                       :IND-LQU-IMPST-VIS-662014
        //                  ,IMPB_IS_662014         =
        //                :LQU-IMPB-IS-662014
        //                                       :IND-LQU-IMPB-IS-662014
        //                  ,IMPST_SOST_662014      =
        //                :LQU-IMPST-SOST-662014
        //                                       :IND-LQU-IMPST-SOST-662014
        //                  ,PC_ABB_TS_662014       =
        //                :LQU-PC-ABB-TS-662014
        //                                       :IND-LQU-PC-ABB-TS-662014
        //                  ,IMP_LRD_CALC_CP        =
        //                :LQU-IMP-LRD-CALC-CP
        //                                       :IND-LQU-IMP-LRD-CALC-CP
        //                  ,COS_TUNNEL_USCITA      =
        //                :LQU-COS-TUNNEL-USCITA
        //                                       :IND-LQU-COS-TUNNEL-USCITA
        //                WHERE     DS_RIGA = :LQU-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        liqDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-LQU
        //           END-EXEC.
        liqDao.openCIdUpdEffLqu(liq.getLquIdLiq(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-LQU
        //           END-EXEC.
        liqDao.closeCIdUpdEffLqu();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-LQU
        //           INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //           END-EXEC.
        liqDao.fetchCIdUpdEffLqu(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-EFF-LQU CURSOR FOR
        //              SELECT
        //                     ID_LIQ
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,TP_LIQ
        //                    ,DESC_CAU_EVE_SIN
        //                    ,COD_CAU_SIN
        //                    ,COD_SIN_CATSTRF
        //                    ,DT_MOR
        //                    ,DT_DEN
        //                    ,DT_PERV_DEN
        //                    ,DT_RICH
        //                    ,TP_SIN
        //                    ,TP_RISC
        //                    ,TP_MET_RISC
        //                    ,DT_LIQ
        //                    ,COD_DVS
        //                    ,TOT_IMP_LRD_LIQTO
        //                    ,TOT_IMP_PREST
        //                    ,TOT_IMP_INTR_PREST
        //                    ,TOT_IMP_UTI
        //                    ,TOT_IMP_RIT_TFR
        //                    ,TOT_IMP_RIT_ACC
        //                    ,TOT_IMP_RIT_VIS
        //                    ,TOT_IMPB_TFR
        //                    ,TOT_IMPB_ACC
        //                    ,TOT_IMPB_VIS
        //                    ,TOT_IMP_RIMB
        //                    ,IMPB_IMPST_PRVR
        //                    ,IMPST_PRVR
        //                    ,IMPB_IMPST_252
        //                    ,IMPST_252
        //                    ,TOT_IMP_IS
        //                    ,IMP_DIR_LIQ
        //                    ,TOT_IMP_NET_LIQTO
        //                    ,MONT_END2000
        //                    ,MONT_END2006
        //                    ,PC_REN
        //                    ,IMP_PNL
        //                    ,IMPB_IRPEF
        //                    ,IMPST_IRPEF
        //                    ,DT_VLT
        //                    ,DT_END_ISTR
        //                    ,TP_RIMB
        //                    ,SPE_RCS
        //                    ,IB_LIQ
        //                    ,TOT_IAS_ONER_PRVNT
        //                    ,TOT_IAS_MGG_SIN
        //                    ,TOT_IAS_RST_DPST
        //                    ,IMP_ONER_LIQ
        //                    ,COMPON_TAX_RIMB
        //                    ,TP_MEZ_PAG
        //                    ,IMP_EXCONTR
        //                    ,IMP_INTR_RIT_PAG
        //                    ,BNS_NON_GODUTO
        //                    ,CNBT_INPSTFM
        //                    ,IMPST_DA_RIMB
        //                    ,IMPB_IS
        //                    ,TAX_SEP
        //                    ,IMPB_TAX_SEP
        //                    ,IMPB_INTR_SU_PREST
        //                    ,ADDIZ_COMUN
        //                    ,IMPB_ADDIZ_COMUN
        //                    ,ADDIZ_REGION
        //                    ,IMPB_ADDIZ_REGION
        //                    ,MONT_DAL2007
        //                    ,IMPB_CNBT_INPSTFM
        //                    ,IMP_LRD_DA_RIMB
        //                    ,IMP_DIR_DA_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TOT_IAS_PNL
        //                    ,FL_EVE_GARTO
        //                    ,IMP_REN_K1
        //                    ,IMP_REN_K2
        //                    ,IMP_REN_K3
        //                    ,PC_REN_K1
        //                    ,PC_REN_K2
        //                    ,PC_REN_K3
        //                    ,TP_CAUS_ANTIC
        //                    ,IMP_LRD_LIQTO_RILT
        //                    ,IMPST_APPL_RILT
        //                    ,PC_RISC_PARZ
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_TOT_SW
        //                    ,IMPST_BOLLO_TOT_AA
        //                    ,IMPB_VIS_1382011
        //                    ,IMPST_VIS_1382011
        //                    ,IMPB_IS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,PC_ABB_TIT_STAT
        //                    ,IMPB_BOLLO_DETT_C
        //                    ,FL_PRE_COMP
        //                    ,IMPB_VIS_662014
        //                    ,IMPST_VIS_662014
        //                    ,IMPB_IS_662014
        //                    ,IMPST_SOST_662014
        //                    ,PC_ABB_TS_662014
        //                    ,IMP_LRD_CALC_CP
        //                    ,COS_TUNNEL_USCITA
        //              FROM LIQ
        //              WHERE     IB_OGG = :LQU-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_LIQ ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_LIQ
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,TP_LIQ
        //                ,DESC_CAU_EVE_SIN
        //                ,COD_CAU_SIN
        //                ,COD_SIN_CATSTRF
        //                ,DT_MOR
        //                ,DT_DEN
        //                ,DT_PERV_DEN
        //                ,DT_RICH
        //                ,TP_SIN
        //                ,TP_RISC
        //                ,TP_MET_RISC
        //                ,DT_LIQ
        //                ,COD_DVS
        //                ,TOT_IMP_LRD_LIQTO
        //                ,TOT_IMP_PREST
        //                ,TOT_IMP_INTR_PREST
        //                ,TOT_IMP_UTI
        //                ,TOT_IMP_RIT_TFR
        //                ,TOT_IMP_RIT_ACC
        //                ,TOT_IMP_RIT_VIS
        //                ,TOT_IMPB_TFR
        //                ,TOT_IMPB_ACC
        //                ,TOT_IMPB_VIS
        //                ,TOT_IMP_RIMB
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,TOT_IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,TOT_IMP_NET_LIQTO
        //                ,MONT_END2000
        //                ,MONT_END2006
        //                ,PC_REN
        //                ,IMP_PNL
        //                ,IMPB_IRPEF
        //                ,IMPST_IRPEF
        //                ,DT_VLT
        //                ,DT_END_ISTR
        //                ,TP_RIMB
        //                ,SPE_RCS
        //                ,IB_LIQ
        //                ,TOT_IAS_ONER_PRVNT
        //                ,TOT_IAS_MGG_SIN
        //                ,TOT_IAS_RST_DPST
        //                ,IMP_ONER_LIQ
        //                ,COMPON_TAX_RIMB
        //                ,TP_MEZ_PAG
        //                ,IMP_EXCONTR
        //                ,IMP_INTR_RIT_PAG
        //                ,BNS_NON_GODUTO
        //                ,CNBT_INPSTFM
        //                ,IMPST_DA_RIMB
        //                ,IMPB_IS
        //                ,TAX_SEP
        //                ,IMPB_TAX_SEP
        //                ,IMPB_INTR_SU_PREST
        //                ,ADDIZ_COMUN
        //                ,IMPB_ADDIZ_COMUN
        //                ,ADDIZ_REGION
        //                ,IMPB_ADDIZ_REGION
        //                ,MONT_DAL2007
        //                ,IMPB_CNBT_INPSTFM
        //                ,IMP_LRD_DA_RIMB
        //                ,IMP_DIR_DA_RIMB
        //                ,RIS_MAT
        //                ,RIS_SPE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TOT_IAS_PNL
        //                ,FL_EVE_GARTO
        //                ,IMP_REN_K1
        //                ,IMP_REN_K2
        //                ,IMP_REN_K3
        //                ,PC_REN_K1
        //                ,PC_REN_K2
        //                ,PC_REN_K3
        //                ,TP_CAUS_ANTIC
        //                ,IMP_LRD_LIQTO_RILT
        //                ,IMPST_APPL_RILT
        //                ,PC_RISC_PARZ
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_TOT_SW
        //                ,IMPST_BOLLO_TOT_AA
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPB_IS_1382011
        //                ,IMPST_SOST_1382011
        //                ,PC_ABB_TIT_STAT
        //                ,IMPB_BOLLO_DETT_C
        //                ,FL_PRE_COMP
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //                ,PC_ABB_TS_662014
        //                ,IMP_LRD_CALC_CP
        //                ,COS_TUNNEL_USCITA
        //             INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //             FROM LIQ
        //             WHERE     IB_OGG = :LQU-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        liqDao.selectRec1(liq.getLquIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-EFF-LQU
        //           END-EXEC.
        liqDao.openCIboEffLqu(liq.getLquIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-EFF-LQU
        //           END-EXEC.
        liqDao.closeCIboEffLqu();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-EFF-LQU
        //           INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //           END-EXEC.
        liqDao.fetchCIboEffLqu(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX
            a570CloseCursorIbo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A605-DCL-CUR-IBS-LIQ<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DclCurIbsLiq() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-EFF-LQU-0 CURSOR FOR
    //              SELECT
    //                     ID_LIQ
    //                    ,ID_OGG
    //                    ,TP_OGG
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,IB_OGG
    //                    ,TP_LIQ
    //                    ,DESC_CAU_EVE_SIN
    //                    ,COD_CAU_SIN
    //                    ,COD_SIN_CATSTRF
    //                    ,DT_MOR
    //                    ,DT_DEN
    //                    ,DT_PERV_DEN
    //                    ,DT_RICH
    //                    ,TP_SIN
    //                    ,TP_RISC
    //                    ,TP_MET_RISC
    //                    ,DT_LIQ
    //                    ,COD_DVS
    //                    ,TOT_IMP_LRD_LIQTO
    //                    ,TOT_IMP_PREST
    //                    ,TOT_IMP_INTR_PREST
    //                    ,TOT_IMP_UTI
    //                    ,TOT_IMP_RIT_TFR
    //                    ,TOT_IMP_RIT_ACC
    //                    ,TOT_IMP_RIT_VIS
    //                    ,TOT_IMPB_TFR
    //                    ,TOT_IMPB_ACC
    //                    ,TOT_IMPB_VIS
    //                    ,TOT_IMP_RIMB
    //                    ,IMPB_IMPST_PRVR
    //                    ,IMPST_PRVR
    //                    ,IMPB_IMPST_252
    //                    ,IMPST_252
    //                    ,TOT_IMP_IS
    //                    ,IMP_DIR_LIQ
    //                    ,TOT_IMP_NET_LIQTO
    //                    ,MONT_END2000
    //                    ,MONT_END2006
    //                    ,PC_REN
    //                    ,IMP_PNL
    //                    ,IMPB_IRPEF
    //                    ,IMPST_IRPEF
    //                    ,DT_VLT
    //                    ,DT_END_ISTR
    //                    ,TP_RIMB
    //                    ,SPE_RCS
    //                    ,IB_LIQ
    //                    ,TOT_IAS_ONER_PRVNT
    //                    ,TOT_IAS_MGG_SIN
    //                    ,TOT_IAS_RST_DPST
    //                    ,IMP_ONER_LIQ
    //                    ,COMPON_TAX_RIMB
    //                    ,TP_MEZ_PAG
    //                    ,IMP_EXCONTR
    //                    ,IMP_INTR_RIT_PAG
    //                    ,BNS_NON_GODUTO
    //                    ,CNBT_INPSTFM
    //                    ,IMPST_DA_RIMB
    //                    ,IMPB_IS
    //                    ,TAX_SEP
    //                    ,IMPB_TAX_SEP
    //                    ,IMPB_INTR_SU_PREST
    //                    ,ADDIZ_COMUN
    //                    ,IMPB_ADDIZ_COMUN
    //                    ,ADDIZ_REGION
    //                    ,IMPB_ADDIZ_REGION
    //                    ,MONT_DAL2007
    //                    ,IMPB_CNBT_INPSTFM
    //                    ,IMP_LRD_DA_RIMB
    //                    ,IMP_DIR_DA_RIMB
    //                    ,RIS_MAT
    //                    ,RIS_SPE
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,TOT_IAS_PNL
    //                    ,FL_EVE_GARTO
    //                    ,IMP_REN_K1
    //                    ,IMP_REN_K2
    //                    ,IMP_REN_K3
    //                    ,PC_REN_K1
    //                    ,PC_REN_K2
    //                    ,PC_REN_K3
    //                    ,TP_CAUS_ANTIC
    //                    ,IMP_LRD_LIQTO_RILT
    //                    ,IMPST_APPL_RILT
    //                    ,PC_RISC_PARZ
    //                    ,IMPST_BOLLO_TOT_V
    //                    ,IMPST_BOLLO_DETT_C
    //                    ,IMPST_BOLLO_TOT_SW
    //                    ,IMPST_BOLLO_TOT_AA
    //                    ,IMPB_VIS_1382011
    //                    ,IMPST_VIS_1382011
    //                    ,IMPB_IS_1382011
    //                    ,IMPST_SOST_1382011
    //                    ,PC_ABB_TIT_STAT
    //                    ,IMPB_BOLLO_DETT_C
    //                    ,FL_PRE_COMP
    //                    ,IMPB_VIS_662014
    //                    ,IMPST_VIS_662014
    //                    ,IMPB_IS_662014
    //                    ,IMPST_SOST_662014
    //                    ,PC_ABB_TS_662014
    //                    ,IMP_LRD_CALC_CP
    //                    ,COS_TUNNEL_USCITA
    //              FROM LIQ
    //              WHERE     IB_LIQ = :LQU-IB-LIQ
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND ID_MOVI_CHIU IS NULL
    //              ORDER BY ID_LIQ ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF LQU-IB-LIQ NOT = HIGH-VALUES
        //                  THRU A605-LIQ-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(liq.getLquIbLiq(), LiqIdbslqu0.Len.LQU_IB_LIQ)) {
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-LIQ
            //              THRU A605-LIQ-EX
            a605DclCurIbsLiq();
        }
    }

    /**Original name: A610-SELECT-IBS-LIQ<br>*/
    private void a610SelectIbsLiq() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_LIQ
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,TP_LIQ
        //                ,DESC_CAU_EVE_SIN
        //                ,COD_CAU_SIN
        //                ,COD_SIN_CATSTRF
        //                ,DT_MOR
        //                ,DT_DEN
        //                ,DT_PERV_DEN
        //                ,DT_RICH
        //                ,TP_SIN
        //                ,TP_RISC
        //                ,TP_MET_RISC
        //                ,DT_LIQ
        //                ,COD_DVS
        //                ,TOT_IMP_LRD_LIQTO
        //                ,TOT_IMP_PREST
        //                ,TOT_IMP_INTR_PREST
        //                ,TOT_IMP_UTI
        //                ,TOT_IMP_RIT_TFR
        //                ,TOT_IMP_RIT_ACC
        //                ,TOT_IMP_RIT_VIS
        //                ,TOT_IMPB_TFR
        //                ,TOT_IMPB_ACC
        //                ,TOT_IMPB_VIS
        //                ,TOT_IMP_RIMB
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,TOT_IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,TOT_IMP_NET_LIQTO
        //                ,MONT_END2000
        //                ,MONT_END2006
        //                ,PC_REN
        //                ,IMP_PNL
        //                ,IMPB_IRPEF
        //                ,IMPST_IRPEF
        //                ,DT_VLT
        //                ,DT_END_ISTR
        //                ,TP_RIMB
        //                ,SPE_RCS
        //                ,IB_LIQ
        //                ,TOT_IAS_ONER_PRVNT
        //                ,TOT_IAS_MGG_SIN
        //                ,TOT_IAS_RST_DPST
        //                ,IMP_ONER_LIQ
        //                ,COMPON_TAX_RIMB
        //                ,TP_MEZ_PAG
        //                ,IMP_EXCONTR
        //                ,IMP_INTR_RIT_PAG
        //                ,BNS_NON_GODUTO
        //                ,CNBT_INPSTFM
        //                ,IMPST_DA_RIMB
        //                ,IMPB_IS
        //                ,TAX_SEP
        //                ,IMPB_TAX_SEP
        //                ,IMPB_INTR_SU_PREST
        //                ,ADDIZ_COMUN
        //                ,IMPB_ADDIZ_COMUN
        //                ,ADDIZ_REGION
        //                ,IMPB_ADDIZ_REGION
        //                ,MONT_DAL2007
        //                ,IMPB_CNBT_INPSTFM
        //                ,IMP_LRD_DA_RIMB
        //                ,IMP_DIR_DA_RIMB
        //                ,RIS_MAT
        //                ,RIS_SPE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TOT_IAS_PNL
        //                ,FL_EVE_GARTO
        //                ,IMP_REN_K1
        //                ,IMP_REN_K2
        //                ,IMP_REN_K3
        //                ,PC_REN_K1
        //                ,PC_REN_K2
        //                ,PC_REN_K3
        //                ,TP_CAUS_ANTIC
        //                ,IMP_LRD_LIQTO_RILT
        //                ,IMPST_APPL_RILT
        //                ,PC_RISC_PARZ
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_TOT_SW
        //                ,IMPST_BOLLO_TOT_AA
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPB_IS_1382011
        //                ,IMPST_SOST_1382011
        //                ,PC_ABB_TIT_STAT
        //                ,IMPB_BOLLO_DETT_C
        //                ,FL_PRE_COMP
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //                ,PC_ABB_TS_662014
        //                ,IMP_LRD_CALC_CP
        //                ,COS_TUNNEL_USCITA
        //             INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //             FROM LIQ
        //             WHERE     IB_LIQ = :LQU-IB-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        liqDao.selectRec2(liq.getLquIbLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF LQU-IB-LIQ NOT = HIGH-VALUES
        //                  THRU A610-LIQ-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(liq.getLquIbLiq(), LiqIdbslqu0.Len.LQU_IB_LIQ)) {
            // COB_CODE: PERFORM A610-SELECT-IBS-LIQ
            //              THRU A610-LIQ-EX
            a610SelectIbsLiq();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: IF LQU-IB-LIQ NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(liq.getLquIbLiq(), LiqIdbslqu0.Len.LQU_IB_LIQ)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-EFF-LQU-0
            //           END-EXEC
            liqDao.openCIbsEffLqu0(liq.getLquIbLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: IF LQU-IB-LIQ NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(liq.getLquIbLiq(), LiqIdbslqu0.Len.LQU_IB_LIQ)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-EFF-LQU-0
            //           END-EXEC
            liqDao.closeCIbsEffLqu0();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FN-IBS-LIQ<br>*/
    private void a690FnIbsLiq() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-EFF-LQU-0
        //           INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //           END-EXEC.
        liqDao.fetchCIbsEffLqu0(this);
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: IF LQU-IB-LIQ NOT = HIGH-VALUES
        //                  THRU A690-LIQ-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(liq.getLquIbLiq(), LiqIdbslqu0.Len.LQU_IB_LIQ)) {
            // COB_CODE: PERFORM A690-FN-IBS-LIQ
            //              THRU A690-LIQ-EX
            a690FnIbsLiq();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX
            a670CloseCursorIbs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-EFF-LQU CURSOR FOR
        //              SELECT
        //                     ID_LIQ
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,TP_LIQ
        //                    ,DESC_CAU_EVE_SIN
        //                    ,COD_CAU_SIN
        //                    ,COD_SIN_CATSTRF
        //                    ,DT_MOR
        //                    ,DT_DEN
        //                    ,DT_PERV_DEN
        //                    ,DT_RICH
        //                    ,TP_SIN
        //                    ,TP_RISC
        //                    ,TP_MET_RISC
        //                    ,DT_LIQ
        //                    ,COD_DVS
        //                    ,TOT_IMP_LRD_LIQTO
        //                    ,TOT_IMP_PREST
        //                    ,TOT_IMP_INTR_PREST
        //                    ,TOT_IMP_UTI
        //                    ,TOT_IMP_RIT_TFR
        //                    ,TOT_IMP_RIT_ACC
        //                    ,TOT_IMP_RIT_VIS
        //                    ,TOT_IMPB_TFR
        //                    ,TOT_IMPB_ACC
        //                    ,TOT_IMPB_VIS
        //                    ,TOT_IMP_RIMB
        //                    ,IMPB_IMPST_PRVR
        //                    ,IMPST_PRVR
        //                    ,IMPB_IMPST_252
        //                    ,IMPST_252
        //                    ,TOT_IMP_IS
        //                    ,IMP_DIR_LIQ
        //                    ,TOT_IMP_NET_LIQTO
        //                    ,MONT_END2000
        //                    ,MONT_END2006
        //                    ,PC_REN
        //                    ,IMP_PNL
        //                    ,IMPB_IRPEF
        //                    ,IMPST_IRPEF
        //                    ,DT_VLT
        //                    ,DT_END_ISTR
        //                    ,TP_RIMB
        //                    ,SPE_RCS
        //                    ,IB_LIQ
        //                    ,TOT_IAS_ONER_PRVNT
        //                    ,TOT_IAS_MGG_SIN
        //                    ,TOT_IAS_RST_DPST
        //                    ,IMP_ONER_LIQ
        //                    ,COMPON_TAX_RIMB
        //                    ,TP_MEZ_PAG
        //                    ,IMP_EXCONTR
        //                    ,IMP_INTR_RIT_PAG
        //                    ,BNS_NON_GODUTO
        //                    ,CNBT_INPSTFM
        //                    ,IMPST_DA_RIMB
        //                    ,IMPB_IS
        //                    ,TAX_SEP
        //                    ,IMPB_TAX_SEP
        //                    ,IMPB_INTR_SU_PREST
        //                    ,ADDIZ_COMUN
        //                    ,IMPB_ADDIZ_COMUN
        //                    ,ADDIZ_REGION
        //                    ,IMPB_ADDIZ_REGION
        //                    ,MONT_DAL2007
        //                    ,IMPB_CNBT_INPSTFM
        //                    ,IMP_LRD_DA_RIMB
        //                    ,IMP_DIR_DA_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TOT_IAS_PNL
        //                    ,FL_EVE_GARTO
        //                    ,IMP_REN_K1
        //                    ,IMP_REN_K2
        //                    ,IMP_REN_K3
        //                    ,PC_REN_K1
        //                    ,PC_REN_K2
        //                    ,PC_REN_K3
        //                    ,TP_CAUS_ANTIC
        //                    ,IMP_LRD_LIQTO_RILT
        //                    ,IMPST_APPL_RILT
        //                    ,PC_RISC_PARZ
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_TOT_SW
        //                    ,IMPST_BOLLO_TOT_AA
        //                    ,IMPB_VIS_1382011
        //                    ,IMPST_VIS_1382011
        //                    ,IMPB_IS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,PC_ABB_TIT_STAT
        //                    ,IMPB_BOLLO_DETT_C
        //                    ,FL_PRE_COMP
        //                    ,IMPB_VIS_662014
        //                    ,IMPST_VIS_662014
        //                    ,IMPB_IS_662014
        //                    ,IMPST_SOST_662014
        //                    ,PC_ABB_TS_662014
        //                    ,IMP_LRD_CALC_CP
        //                    ,COS_TUNNEL_USCITA
        //              FROM LIQ
        //              WHERE     ID_OGG = :LQU-ID-OGG
        //                    AND TP_OGG = :LQU-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_LIQ ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_LIQ
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,TP_LIQ
        //                ,DESC_CAU_EVE_SIN
        //                ,COD_CAU_SIN
        //                ,COD_SIN_CATSTRF
        //                ,DT_MOR
        //                ,DT_DEN
        //                ,DT_PERV_DEN
        //                ,DT_RICH
        //                ,TP_SIN
        //                ,TP_RISC
        //                ,TP_MET_RISC
        //                ,DT_LIQ
        //                ,COD_DVS
        //                ,TOT_IMP_LRD_LIQTO
        //                ,TOT_IMP_PREST
        //                ,TOT_IMP_INTR_PREST
        //                ,TOT_IMP_UTI
        //                ,TOT_IMP_RIT_TFR
        //                ,TOT_IMP_RIT_ACC
        //                ,TOT_IMP_RIT_VIS
        //                ,TOT_IMPB_TFR
        //                ,TOT_IMPB_ACC
        //                ,TOT_IMPB_VIS
        //                ,TOT_IMP_RIMB
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,TOT_IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,TOT_IMP_NET_LIQTO
        //                ,MONT_END2000
        //                ,MONT_END2006
        //                ,PC_REN
        //                ,IMP_PNL
        //                ,IMPB_IRPEF
        //                ,IMPST_IRPEF
        //                ,DT_VLT
        //                ,DT_END_ISTR
        //                ,TP_RIMB
        //                ,SPE_RCS
        //                ,IB_LIQ
        //                ,TOT_IAS_ONER_PRVNT
        //                ,TOT_IAS_MGG_SIN
        //                ,TOT_IAS_RST_DPST
        //                ,IMP_ONER_LIQ
        //                ,COMPON_TAX_RIMB
        //                ,TP_MEZ_PAG
        //                ,IMP_EXCONTR
        //                ,IMP_INTR_RIT_PAG
        //                ,BNS_NON_GODUTO
        //                ,CNBT_INPSTFM
        //                ,IMPST_DA_RIMB
        //                ,IMPB_IS
        //                ,TAX_SEP
        //                ,IMPB_TAX_SEP
        //                ,IMPB_INTR_SU_PREST
        //                ,ADDIZ_COMUN
        //                ,IMPB_ADDIZ_COMUN
        //                ,ADDIZ_REGION
        //                ,IMPB_ADDIZ_REGION
        //                ,MONT_DAL2007
        //                ,IMPB_CNBT_INPSTFM
        //                ,IMP_LRD_DA_RIMB
        //                ,IMP_DIR_DA_RIMB
        //                ,RIS_MAT
        //                ,RIS_SPE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TOT_IAS_PNL
        //                ,FL_EVE_GARTO
        //                ,IMP_REN_K1
        //                ,IMP_REN_K2
        //                ,IMP_REN_K3
        //                ,PC_REN_K1
        //                ,PC_REN_K2
        //                ,PC_REN_K3
        //                ,TP_CAUS_ANTIC
        //                ,IMP_LRD_LIQTO_RILT
        //                ,IMPST_APPL_RILT
        //                ,PC_RISC_PARZ
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_TOT_SW
        //                ,IMPST_BOLLO_TOT_AA
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPB_IS_1382011
        //                ,IMPST_SOST_1382011
        //                ,PC_ABB_TIT_STAT
        //                ,IMPB_BOLLO_DETT_C
        //                ,FL_PRE_COMP
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //                ,PC_ABB_TS_662014
        //                ,IMP_LRD_CALC_CP
        //                ,COS_TUNNEL_USCITA
        //             INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //             FROM LIQ
        //             WHERE     ID_OGG = :LQU-ID-OGG
        //                    AND TP_OGG = :LQU-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        liqDao.selectRec3(liq.getLquIdOgg(), liq.getLquTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-EFF-LQU
        //           END-EXEC.
        liqDao.openCIdoEffLqu(liq.getLquIdOgg(), liq.getLquTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-EFF-LQU
        //           END-EXEC.
        liqDao.closeCIdoEffLqu();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-EFF-LQU
        //           INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //           END-EXEC.
        liqDao.fetchCIdoEffLqu(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX
            a770CloseCursorIdo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_LIQ
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,TP_LIQ
        //                ,DESC_CAU_EVE_SIN
        //                ,COD_CAU_SIN
        //                ,COD_SIN_CATSTRF
        //                ,DT_MOR
        //                ,DT_DEN
        //                ,DT_PERV_DEN
        //                ,DT_RICH
        //                ,TP_SIN
        //                ,TP_RISC
        //                ,TP_MET_RISC
        //                ,DT_LIQ
        //                ,COD_DVS
        //                ,TOT_IMP_LRD_LIQTO
        //                ,TOT_IMP_PREST
        //                ,TOT_IMP_INTR_PREST
        //                ,TOT_IMP_UTI
        //                ,TOT_IMP_RIT_TFR
        //                ,TOT_IMP_RIT_ACC
        //                ,TOT_IMP_RIT_VIS
        //                ,TOT_IMPB_TFR
        //                ,TOT_IMPB_ACC
        //                ,TOT_IMPB_VIS
        //                ,TOT_IMP_RIMB
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,TOT_IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,TOT_IMP_NET_LIQTO
        //                ,MONT_END2000
        //                ,MONT_END2006
        //                ,PC_REN
        //                ,IMP_PNL
        //                ,IMPB_IRPEF
        //                ,IMPST_IRPEF
        //                ,DT_VLT
        //                ,DT_END_ISTR
        //                ,TP_RIMB
        //                ,SPE_RCS
        //                ,IB_LIQ
        //                ,TOT_IAS_ONER_PRVNT
        //                ,TOT_IAS_MGG_SIN
        //                ,TOT_IAS_RST_DPST
        //                ,IMP_ONER_LIQ
        //                ,COMPON_TAX_RIMB
        //                ,TP_MEZ_PAG
        //                ,IMP_EXCONTR
        //                ,IMP_INTR_RIT_PAG
        //                ,BNS_NON_GODUTO
        //                ,CNBT_INPSTFM
        //                ,IMPST_DA_RIMB
        //                ,IMPB_IS
        //                ,TAX_SEP
        //                ,IMPB_TAX_SEP
        //                ,IMPB_INTR_SU_PREST
        //                ,ADDIZ_COMUN
        //                ,IMPB_ADDIZ_COMUN
        //                ,ADDIZ_REGION
        //                ,IMPB_ADDIZ_REGION
        //                ,MONT_DAL2007
        //                ,IMPB_CNBT_INPSTFM
        //                ,IMP_LRD_DA_RIMB
        //                ,IMP_DIR_DA_RIMB
        //                ,RIS_MAT
        //                ,RIS_SPE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TOT_IAS_PNL
        //                ,FL_EVE_GARTO
        //                ,IMP_REN_K1
        //                ,IMP_REN_K2
        //                ,IMP_REN_K3
        //                ,PC_REN_K1
        //                ,PC_REN_K2
        //                ,PC_REN_K3
        //                ,TP_CAUS_ANTIC
        //                ,IMP_LRD_LIQTO_RILT
        //                ,IMPST_APPL_RILT
        //                ,PC_RISC_PARZ
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_TOT_SW
        //                ,IMPST_BOLLO_TOT_AA
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPB_IS_1382011
        //                ,IMPST_SOST_1382011
        //                ,PC_ABB_TIT_STAT
        //                ,IMPB_BOLLO_DETT_C
        //                ,FL_PRE_COMP
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //                ,PC_ABB_TS_662014
        //                ,IMP_LRD_CALC_CP
        //                ,COS_TUNNEL_USCITA
        //             INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //             FROM LIQ
        //             WHERE     ID_LIQ = :LQU-ID-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        liqDao.selectRec4(liq.getLquIdLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-CPZ-LQU CURSOR FOR
        //              SELECT
        //                     ID_LIQ
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,TP_LIQ
        //                    ,DESC_CAU_EVE_SIN
        //                    ,COD_CAU_SIN
        //                    ,COD_SIN_CATSTRF
        //                    ,DT_MOR
        //                    ,DT_DEN
        //                    ,DT_PERV_DEN
        //                    ,DT_RICH
        //                    ,TP_SIN
        //                    ,TP_RISC
        //                    ,TP_MET_RISC
        //                    ,DT_LIQ
        //                    ,COD_DVS
        //                    ,TOT_IMP_LRD_LIQTO
        //                    ,TOT_IMP_PREST
        //                    ,TOT_IMP_INTR_PREST
        //                    ,TOT_IMP_UTI
        //                    ,TOT_IMP_RIT_TFR
        //                    ,TOT_IMP_RIT_ACC
        //                    ,TOT_IMP_RIT_VIS
        //                    ,TOT_IMPB_TFR
        //                    ,TOT_IMPB_ACC
        //                    ,TOT_IMPB_VIS
        //                    ,TOT_IMP_RIMB
        //                    ,IMPB_IMPST_PRVR
        //                    ,IMPST_PRVR
        //                    ,IMPB_IMPST_252
        //                    ,IMPST_252
        //                    ,TOT_IMP_IS
        //                    ,IMP_DIR_LIQ
        //                    ,TOT_IMP_NET_LIQTO
        //                    ,MONT_END2000
        //                    ,MONT_END2006
        //                    ,PC_REN
        //                    ,IMP_PNL
        //                    ,IMPB_IRPEF
        //                    ,IMPST_IRPEF
        //                    ,DT_VLT
        //                    ,DT_END_ISTR
        //                    ,TP_RIMB
        //                    ,SPE_RCS
        //                    ,IB_LIQ
        //                    ,TOT_IAS_ONER_PRVNT
        //                    ,TOT_IAS_MGG_SIN
        //                    ,TOT_IAS_RST_DPST
        //                    ,IMP_ONER_LIQ
        //                    ,COMPON_TAX_RIMB
        //                    ,TP_MEZ_PAG
        //                    ,IMP_EXCONTR
        //                    ,IMP_INTR_RIT_PAG
        //                    ,BNS_NON_GODUTO
        //                    ,CNBT_INPSTFM
        //                    ,IMPST_DA_RIMB
        //                    ,IMPB_IS
        //                    ,TAX_SEP
        //                    ,IMPB_TAX_SEP
        //                    ,IMPB_INTR_SU_PREST
        //                    ,ADDIZ_COMUN
        //                    ,IMPB_ADDIZ_COMUN
        //                    ,ADDIZ_REGION
        //                    ,IMPB_ADDIZ_REGION
        //                    ,MONT_DAL2007
        //                    ,IMPB_CNBT_INPSTFM
        //                    ,IMP_LRD_DA_RIMB
        //                    ,IMP_DIR_DA_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TOT_IAS_PNL
        //                    ,FL_EVE_GARTO
        //                    ,IMP_REN_K1
        //                    ,IMP_REN_K2
        //                    ,IMP_REN_K3
        //                    ,PC_REN_K1
        //                    ,PC_REN_K2
        //                    ,PC_REN_K3
        //                    ,TP_CAUS_ANTIC
        //                    ,IMP_LRD_LIQTO_RILT
        //                    ,IMPST_APPL_RILT
        //                    ,PC_RISC_PARZ
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_TOT_SW
        //                    ,IMPST_BOLLO_TOT_AA
        //                    ,IMPB_VIS_1382011
        //                    ,IMPST_VIS_1382011
        //                    ,IMPB_IS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,PC_ABB_TIT_STAT
        //                    ,IMPB_BOLLO_DETT_C
        //                    ,FL_PRE_COMP
        //                    ,IMPB_VIS_662014
        //                    ,IMPST_VIS_662014
        //                    ,IMPB_IS_662014
        //                    ,IMPST_SOST_662014
        //                    ,PC_ABB_TS_662014
        //                    ,IMP_LRD_CALC_CP
        //                    ,COS_TUNNEL_USCITA
        //              FROM LIQ
        //              WHERE     IB_OGG = :LQU-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_LIQ ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_LIQ
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,TP_LIQ
        //                ,DESC_CAU_EVE_SIN
        //                ,COD_CAU_SIN
        //                ,COD_SIN_CATSTRF
        //                ,DT_MOR
        //                ,DT_DEN
        //                ,DT_PERV_DEN
        //                ,DT_RICH
        //                ,TP_SIN
        //                ,TP_RISC
        //                ,TP_MET_RISC
        //                ,DT_LIQ
        //                ,COD_DVS
        //                ,TOT_IMP_LRD_LIQTO
        //                ,TOT_IMP_PREST
        //                ,TOT_IMP_INTR_PREST
        //                ,TOT_IMP_UTI
        //                ,TOT_IMP_RIT_TFR
        //                ,TOT_IMP_RIT_ACC
        //                ,TOT_IMP_RIT_VIS
        //                ,TOT_IMPB_TFR
        //                ,TOT_IMPB_ACC
        //                ,TOT_IMPB_VIS
        //                ,TOT_IMP_RIMB
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,TOT_IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,TOT_IMP_NET_LIQTO
        //                ,MONT_END2000
        //                ,MONT_END2006
        //                ,PC_REN
        //                ,IMP_PNL
        //                ,IMPB_IRPEF
        //                ,IMPST_IRPEF
        //                ,DT_VLT
        //                ,DT_END_ISTR
        //                ,TP_RIMB
        //                ,SPE_RCS
        //                ,IB_LIQ
        //                ,TOT_IAS_ONER_PRVNT
        //                ,TOT_IAS_MGG_SIN
        //                ,TOT_IAS_RST_DPST
        //                ,IMP_ONER_LIQ
        //                ,COMPON_TAX_RIMB
        //                ,TP_MEZ_PAG
        //                ,IMP_EXCONTR
        //                ,IMP_INTR_RIT_PAG
        //                ,BNS_NON_GODUTO
        //                ,CNBT_INPSTFM
        //                ,IMPST_DA_RIMB
        //                ,IMPB_IS
        //                ,TAX_SEP
        //                ,IMPB_TAX_SEP
        //                ,IMPB_INTR_SU_PREST
        //                ,ADDIZ_COMUN
        //                ,IMPB_ADDIZ_COMUN
        //                ,ADDIZ_REGION
        //                ,IMPB_ADDIZ_REGION
        //                ,MONT_DAL2007
        //                ,IMPB_CNBT_INPSTFM
        //                ,IMP_LRD_DA_RIMB
        //                ,IMP_DIR_DA_RIMB
        //                ,RIS_MAT
        //                ,RIS_SPE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TOT_IAS_PNL
        //                ,FL_EVE_GARTO
        //                ,IMP_REN_K1
        //                ,IMP_REN_K2
        //                ,IMP_REN_K3
        //                ,PC_REN_K1
        //                ,PC_REN_K2
        //                ,PC_REN_K3
        //                ,TP_CAUS_ANTIC
        //                ,IMP_LRD_LIQTO_RILT
        //                ,IMPST_APPL_RILT
        //                ,PC_RISC_PARZ
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_TOT_SW
        //                ,IMPST_BOLLO_TOT_AA
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPB_IS_1382011
        //                ,IMPST_SOST_1382011
        //                ,PC_ABB_TIT_STAT
        //                ,IMPB_BOLLO_DETT_C
        //                ,FL_PRE_COMP
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //                ,PC_ABB_TS_662014
        //                ,IMP_LRD_CALC_CP
        //                ,COS_TUNNEL_USCITA
        //             INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //             FROM LIQ
        //             WHERE     IB_OGG = :LQU-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        liqDao.selectRec5(liq.getLquIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-CPZ-LQU
        //           END-EXEC.
        liqDao.openCIboCpzLqu(liq.getLquIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-CPZ-LQU
        //           END-EXEC.
        liqDao.closeCIboCpzLqu();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-CPZ-LQU
        //           INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //           END-EXEC.
        liqDao.fetchCIboCpzLqu(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX
            b570CloseCursorIboCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B605-DCL-CUR-IBS-LIQ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DclCurIbsLiq() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-CPZ-LQU-0 CURSOR FOR
    //              SELECT
    //                     ID_LIQ
    //                    ,ID_OGG
    //                    ,TP_OGG
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,IB_OGG
    //                    ,TP_LIQ
    //                    ,DESC_CAU_EVE_SIN
    //                    ,COD_CAU_SIN
    //                    ,COD_SIN_CATSTRF
    //                    ,DT_MOR
    //                    ,DT_DEN
    //                    ,DT_PERV_DEN
    //                    ,DT_RICH
    //                    ,TP_SIN
    //                    ,TP_RISC
    //                    ,TP_MET_RISC
    //                    ,DT_LIQ
    //                    ,COD_DVS
    //                    ,TOT_IMP_LRD_LIQTO
    //                    ,TOT_IMP_PREST
    //                    ,TOT_IMP_INTR_PREST
    //                    ,TOT_IMP_UTI
    //                    ,TOT_IMP_RIT_TFR
    //                    ,TOT_IMP_RIT_ACC
    //                    ,TOT_IMP_RIT_VIS
    //                    ,TOT_IMPB_TFR
    //                    ,TOT_IMPB_ACC
    //                    ,TOT_IMPB_VIS
    //                    ,TOT_IMP_RIMB
    //                    ,IMPB_IMPST_PRVR
    //                    ,IMPST_PRVR
    //                    ,IMPB_IMPST_252
    //                    ,IMPST_252
    //                    ,TOT_IMP_IS
    //                    ,IMP_DIR_LIQ
    //                    ,TOT_IMP_NET_LIQTO
    //                    ,MONT_END2000
    //                    ,MONT_END2006
    //                    ,PC_REN
    //                    ,IMP_PNL
    //                    ,IMPB_IRPEF
    //                    ,IMPST_IRPEF
    //                    ,DT_VLT
    //                    ,DT_END_ISTR
    //                    ,TP_RIMB
    //                    ,SPE_RCS
    //                    ,IB_LIQ
    //                    ,TOT_IAS_ONER_PRVNT
    //                    ,TOT_IAS_MGG_SIN
    //                    ,TOT_IAS_RST_DPST
    //                    ,IMP_ONER_LIQ
    //                    ,COMPON_TAX_RIMB
    //                    ,TP_MEZ_PAG
    //                    ,IMP_EXCONTR
    //                    ,IMP_INTR_RIT_PAG
    //                    ,BNS_NON_GODUTO
    //                    ,CNBT_INPSTFM
    //                    ,IMPST_DA_RIMB
    //                    ,IMPB_IS
    //                    ,TAX_SEP
    //                    ,IMPB_TAX_SEP
    //                    ,IMPB_INTR_SU_PREST
    //                    ,ADDIZ_COMUN
    //                    ,IMPB_ADDIZ_COMUN
    //                    ,ADDIZ_REGION
    //                    ,IMPB_ADDIZ_REGION
    //                    ,MONT_DAL2007
    //                    ,IMPB_CNBT_INPSTFM
    //                    ,IMP_LRD_DA_RIMB
    //                    ,IMP_DIR_DA_RIMB
    //                    ,RIS_MAT
    //                    ,RIS_SPE
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,TOT_IAS_PNL
    //                    ,FL_EVE_GARTO
    //                    ,IMP_REN_K1
    //                    ,IMP_REN_K2
    //                    ,IMP_REN_K3
    //                    ,PC_REN_K1
    //                    ,PC_REN_K2
    //                    ,PC_REN_K3
    //                    ,TP_CAUS_ANTIC
    //                    ,IMP_LRD_LIQTO_RILT
    //                    ,IMPST_APPL_RILT
    //                    ,PC_RISC_PARZ
    //                    ,IMPST_BOLLO_TOT_V
    //                    ,IMPST_BOLLO_DETT_C
    //                    ,IMPST_BOLLO_TOT_SW
    //                    ,IMPST_BOLLO_TOT_AA
    //                    ,IMPB_VIS_1382011
    //                    ,IMPST_VIS_1382011
    //                    ,IMPB_IS_1382011
    //                    ,IMPST_SOST_1382011
    //                    ,PC_ABB_TIT_STAT
    //                    ,IMPB_BOLLO_DETT_C
    //                    ,FL_PRE_COMP
    //                    ,IMPB_VIS_662014
    //                    ,IMPST_VIS_662014
    //                    ,IMPB_IS_662014
    //                    ,IMPST_SOST_662014
    //                    ,PC_ABB_TS_662014
    //                    ,IMP_LRD_CALC_CP
    //                    ,COS_TUNNEL_USCITA
    //              FROM LIQ
    //              WHERE     IB_LIQ = :LQU-IB-LIQ
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DS_TS_INI_CPTZ <=
    //                         :WS-TS-COMPETENZA
    //                    AND DS_TS_END_CPTZ >
    //                         :WS-TS-COMPETENZA
    //              ORDER BY ID_LIQ ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF LQU-IB-LIQ NOT = HIGH-VALUES
        //                  THRU B605-LIQ-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(liq.getLquIbLiq(), LiqIdbslqu0.Len.LQU_IB_LIQ)) {
            // COB_CODE: PERFORM B605-DCL-CUR-IBS-LIQ
            //              THRU B605-LIQ-EX
            b605DclCurIbsLiq();
        }
    }

    /**Original name: B610-SELECT-IBS-LIQ<br>*/
    private void b610SelectIbsLiq() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_LIQ
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,TP_LIQ
        //                ,DESC_CAU_EVE_SIN
        //                ,COD_CAU_SIN
        //                ,COD_SIN_CATSTRF
        //                ,DT_MOR
        //                ,DT_DEN
        //                ,DT_PERV_DEN
        //                ,DT_RICH
        //                ,TP_SIN
        //                ,TP_RISC
        //                ,TP_MET_RISC
        //                ,DT_LIQ
        //                ,COD_DVS
        //                ,TOT_IMP_LRD_LIQTO
        //                ,TOT_IMP_PREST
        //                ,TOT_IMP_INTR_PREST
        //                ,TOT_IMP_UTI
        //                ,TOT_IMP_RIT_TFR
        //                ,TOT_IMP_RIT_ACC
        //                ,TOT_IMP_RIT_VIS
        //                ,TOT_IMPB_TFR
        //                ,TOT_IMPB_ACC
        //                ,TOT_IMPB_VIS
        //                ,TOT_IMP_RIMB
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,TOT_IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,TOT_IMP_NET_LIQTO
        //                ,MONT_END2000
        //                ,MONT_END2006
        //                ,PC_REN
        //                ,IMP_PNL
        //                ,IMPB_IRPEF
        //                ,IMPST_IRPEF
        //                ,DT_VLT
        //                ,DT_END_ISTR
        //                ,TP_RIMB
        //                ,SPE_RCS
        //                ,IB_LIQ
        //                ,TOT_IAS_ONER_PRVNT
        //                ,TOT_IAS_MGG_SIN
        //                ,TOT_IAS_RST_DPST
        //                ,IMP_ONER_LIQ
        //                ,COMPON_TAX_RIMB
        //                ,TP_MEZ_PAG
        //                ,IMP_EXCONTR
        //                ,IMP_INTR_RIT_PAG
        //                ,BNS_NON_GODUTO
        //                ,CNBT_INPSTFM
        //                ,IMPST_DA_RIMB
        //                ,IMPB_IS
        //                ,TAX_SEP
        //                ,IMPB_TAX_SEP
        //                ,IMPB_INTR_SU_PREST
        //                ,ADDIZ_COMUN
        //                ,IMPB_ADDIZ_COMUN
        //                ,ADDIZ_REGION
        //                ,IMPB_ADDIZ_REGION
        //                ,MONT_DAL2007
        //                ,IMPB_CNBT_INPSTFM
        //                ,IMP_LRD_DA_RIMB
        //                ,IMP_DIR_DA_RIMB
        //                ,RIS_MAT
        //                ,RIS_SPE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TOT_IAS_PNL
        //                ,FL_EVE_GARTO
        //                ,IMP_REN_K1
        //                ,IMP_REN_K2
        //                ,IMP_REN_K3
        //                ,PC_REN_K1
        //                ,PC_REN_K2
        //                ,PC_REN_K3
        //                ,TP_CAUS_ANTIC
        //                ,IMP_LRD_LIQTO_RILT
        //                ,IMPST_APPL_RILT
        //                ,PC_RISC_PARZ
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_TOT_SW
        //                ,IMPST_BOLLO_TOT_AA
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPB_IS_1382011
        //                ,IMPST_SOST_1382011
        //                ,PC_ABB_TIT_STAT
        //                ,IMPB_BOLLO_DETT_C
        //                ,FL_PRE_COMP
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //                ,PC_ABB_TS_662014
        //                ,IMP_LRD_CALC_CP
        //                ,COS_TUNNEL_USCITA
        //             INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //             FROM LIQ
        //             WHERE     IB_LIQ = :LQU-IB-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        liqDao.selectRec6(liq.getLquIbLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF LQU-IB-LIQ NOT = HIGH-VALUES
        //                  THRU B610-LIQ-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(liq.getLquIbLiq(), LiqIdbslqu0.Len.LQU_IB_LIQ)) {
            // COB_CODE: PERFORM B610-SELECT-IBS-LIQ
            //              THRU B610-LIQ-EX
            b610SelectIbsLiq();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: IF LQU-IB-LIQ NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(liq.getLquIbLiq(), LiqIdbslqu0.Len.LQU_IB_LIQ)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-CPZ-LQU-0
            //           END-EXEC
            liqDao.openCIbsCpzLqu0(liq.getLquIbLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: IF LQU-IB-LIQ NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(liq.getLquIbLiq(), LiqIdbslqu0.Len.LQU_IB_LIQ)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-CPZ-LQU-0
            //           END-EXEC
            liqDao.closeCIbsCpzLqu0();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FN-IBS-LIQ<br>*/
    private void b690FnIbsLiq() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-CPZ-LQU-0
        //           INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //           END-EXEC.
        liqDao.fetchCIbsCpzLqu0(this);
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: IF LQU-IB-LIQ NOT = HIGH-VALUES
        //                  THRU B690-LIQ-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(liq.getLquIbLiq(), LiqIdbslqu0.Len.LQU_IB_LIQ)) {
            // COB_CODE: PERFORM B690-FN-IBS-LIQ
            //              THRU B690-LIQ-EX
            b690FnIbsLiq();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX
            b670CloseCursorIbsCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-CPZ-LQU CURSOR FOR
        //              SELECT
        //                     ID_LIQ
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,TP_LIQ
        //                    ,DESC_CAU_EVE_SIN
        //                    ,COD_CAU_SIN
        //                    ,COD_SIN_CATSTRF
        //                    ,DT_MOR
        //                    ,DT_DEN
        //                    ,DT_PERV_DEN
        //                    ,DT_RICH
        //                    ,TP_SIN
        //                    ,TP_RISC
        //                    ,TP_MET_RISC
        //                    ,DT_LIQ
        //                    ,COD_DVS
        //                    ,TOT_IMP_LRD_LIQTO
        //                    ,TOT_IMP_PREST
        //                    ,TOT_IMP_INTR_PREST
        //                    ,TOT_IMP_UTI
        //                    ,TOT_IMP_RIT_TFR
        //                    ,TOT_IMP_RIT_ACC
        //                    ,TOT_IMP_RIT_VIS
        //                    ,TOT_IMPB_TFR
        //                    ,TOT_IMPB_ACC
        //                    ,TOT_IMPB_VIS
        //                    ,TOT_IMP_RIMB
        //                    ,IMPB_IMPST_PRVR
        //                    ,IMPST_PRVR
        //                    ,IMPB_IMPST_252
        //                    ,IMPST_252
        //                    ,TOT_IMP_IS
        //                    ,IMP_DIR_LIQ
        //                    ,TOT_IMP_NET_LIQTO
        //                    ,MONT_END2000
        //                    ,MONT_END2006
        //                    ,PC_REN
        //                    ,IMP_PNL
        //                    ,IMPB_IRPEF
        //                    ,IMPST_IRPEF
        //                    ,DT_VLT
        //                    ,DT_END_ISTR
        //                    ,TP_RIMB
        //                    ,SPE_RCS
        //                    ,IB_LIQ
        //                    ,TOT_IAS_ONER_PRVNT
        //                    ,TOT_IAS_MGG_SIN
        //                    ,TOT_IAS_RST_DPST
        //                    ,IMP_ONER_LIQ
        //                    ,COMPON_TAX_RIMB
        //                    ,TP_MEZ_PAG
        //                    ,IMP_EXCONTR
        //                    ,IMP_INTR_RIT_PAG
        //                    ,BNS_NON_GODUTO
        //                    ,CNBT_INPSTFM
        //                    ,IMPST_DA_RIMB
        //                    ,IMPB_IS
        //                    ,TAX_SEP
        //                    ,IMPB_TAX_SEP
        //                    ,IMPB_INTR_SU_PREST
        //                    ,ADDIZ_COMUN
        //                    ,IMPB_ADDIZ_COMUN
        //                    ,ADDIZ_REGION
        //                    ,IMPB_ADDIZ_REGION
        //                    ,MONT_DAL2007
        //                    ,IMPB_CNBT_INPSTFM
        //                    ,IMP_LRD_DA_RIMB
        //                    ,IMP_DIR_DA_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TOT_IAS_PNL
        //                    ,FL_EVE_GARTO
        //                    ,IMP_REN_K1
        //                    ,IMP_REN_K2
        //                    ,IMP_REN_K3
        //                    ,PC_REN_K1
        //                    ,PC_REN_K2
        //                    ,PC_REN_K3
        //                    ,TP_CAUS_ANTIC
        //                    ,IMP_LRD_LIQTO_RILT
        //                    ,IMPST_APPL_RILT
        //                    ,PC_RISC_PARZ
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_TOT_SW
        //                    ,IMPST_BOLLO_TOT_AA
        //                    ,IMPB_VIS_1382011
        //                    ,IMPST_VIS_1382011
        //                    ,IMPB_IS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,PC_ABB_TIT_STAT
        //                    ,IMPB_BOLLO_DETT_C
        //                    ,FL_PRE_COMP
        //                    ,IMPB_VIS_662014
        //                    ,IMPST_VIS_662014
        //                    ,IMPB_IS_662014
        //                    ,IMPST_SOST_662014
        //                    ,PC_ABB_TS_662014
        //                    ,IMP_LRD_CALC_CP
        //                    ,COS_TUNNEL_USCITA
        //              FROM LIQ
        //              WHERE     ID_OGG = :LQU-ID-OGG
        //           AND TP_OGG = :LQU-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_LIQ ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_LIQ
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,TP_LIQ
        //                ,DESC_CAU_EVE_SIN
        //                ,COD_CAU_SIN
        //                ,COD_SIN_CATSTRF
        //                ,DT_MOR
        //                ,DT_DEN
        //                ,DT_PERV_DEN
        //                ,DT_RICH
        //                ,TP_SIN
        //                ,TP_RISC
        //                ,TP_MET_RISC
        //                ,DT_LIQ
        //                ,COD_DVS
        //                ,TOT_IMP_LRD_LIQTO
        //                ,TOT_IMP_PREST
        //                ,TOT_IMP_INTR_PREST
        //                ,TOT_IMP_UTI
        //                ,TOT_IMP_RIT_TFR
        //                ,TOT_IMP_RIT_ACC
        //                ,TOT_IMP_RIT_VIS
        //                ,TOT_IMPB_TFR
        //                ,TOT_IMPB_ACC
        //                ,TOT_IMPB_VIS
        //                ,TOT_IMP_RIMB
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,TOT_IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,TOT_IMP_NET_LIQTO
        //                ,MONT_END2000
        //                ,MONT_END2006
        //                ,PC_REN
        //                ,IMP_PNL
        //                ,IMPB_IRPEF
        //                ,IMPST_IRPEF
        //                ,DT_VLT
        //                ,DT_END_ISTR
        //                ,TP_RIMB
        //                ,SPE_RCS
        //                ,IB_LIQ
        //                ,TOT_IAS_ONER_PRVNT
        //                ,TOT_IAS_MGG_SIN
        //                ,TOT_IAS_RST_DPST
        //                ,IMP_ONER_LIQ
        //                ,COMPON_TAX_RIMB
        //                ,TP_MEZ_PAG
        //                ,IMP_EXCONTR
        //                ,IMP_INTR_RIT_PAG
        //                ,BNS_NON_GODUTO
        //                ,CNBT_INPSTFM
        //                ,IMPST_DA_RIMB
        //                ,IMPB_IS
        //                ,TAX_SEP
        //                ,IMPB_TAX_SEP
        //                ,IMPB_INTR_SU_PREST
        //                ,ADDIZ_COMUN
        //                ,IMPB_ADDIZ_COMUN
        //                ,ADDIZ_REGION
        //                ,IMPB_ADDIZ_REGION
        //                ,MONT_DAL2007
        //                ,IMPB_CNBT_INPSTFM
        //                ,IMP_LRD_DA_RIMB
        //                ,IMP_DIR_DA_RIMB
        //                ,RIS_MAT
        //                ,RIS_SPE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TOT_IAS_PNL
        //                ,FL_EVE_GARTO
        //                ,IMP_REN_K1
        //                ,IMP_REN_K2
        //                ,IMP_REN_K3
        //                ,PC_REN_K1
        //                ,PC_REN_K2
        //                ,PC_REN_K3
        //                ,TP_CAUS_ANTIC
        //                ,IMP_LRD_LIQTO_RILT
        //                ,IMPST_APPL_RILT
        //                ,PC_RISC_PARZ
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_TOT_SW
        //                ,IMPST_BOLLO_TOT_AA
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPB_IS_1382011
        //                ,IMPST_SOST_1382011
        //                ,PC_ABB_TIT_STAT
        //                ,IMPB_BOLLO_DETT_C
        //                ,FL_PRE_COMP
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //                ,PC_ABB_TS_662014
        //                ,IMP_LRD_CALC_CP
        //                ,COS_TUNNEL_USCITA
        //             INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //             FROM LIQ
        //             WHERE     ID_OGG = :LQU-ID-OGG
        //                    AND TP_OGG = :LQU-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        liqDao.selectRec7(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-CPZ-LQU
        //           END-EXEC.
        liqDao.openCIdoCpzLqu(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-CPZ-LQU
        //           END-EXEC.
        liqDao.closeCIdoCpzLqu();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-CPZ-LQU
        //           INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //           END-EXEC.
        liqDao.fetchCIdoCpzLqu(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX
            b770CloseCursorIdoCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-LQU-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO LQU-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndLiq().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-ID-MOVI-CHIU-NULL
            liq.getLquIdMoviChiu().setLquIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquIdMoviChiu.Len.LQU_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-LQU-IB-OGG = -1
        //              MOVE HIGH-VALUES TO LQU-IB-OGG-NULL
        //           END-IF
        if (ws.getIndLiq().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IB-OGG-NULL
            liq.setLquIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_IB_OGG));
        }
        // COB_CODE: IF IND-LQU-DESC-CAU-EVE-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-DESC-CAU-EVE-SIN
        //           END-IF
        if (ws.getIndLiq().getDescCauEveSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DESC-CAU-EVE-SIN
            liq.setLquDescCauEveSin(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_DESC_CAU_EVE_SIN));
        }
        // COB_CODE: IF IND-LQU-COD-CAU-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-COD-CAU-SIN-NULL
        //           END-IF
        if (ws.getIndLiq().getCodCauSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COD-CAU-SIN-NULL
            liq.setLquCodCauSin(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_COD_CAU_SIN));
        }
        // COB_CODE: IF IND-LQU-COD-SIN-CATSTRF = -1
        //              MOVE HIGH-VALUES TO LQU-COD-SIN-CATSTRF-NULL
        //           END-IF
        if (ws.getIndLiq().getCodSinCatstrf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COD-SIN-CATSTRF-NULL
            liq.setLquCodSinCatstrf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_COD_SIN_CATSTRF));
        }
        // COB_CODE: IF IND-LQU-DT-MOR = -1
        //              MOVE HIGH-VALUES TO LQU-DT-MOR-NULL
        //           END-IF
        if (ws.getIndLiq().getDtMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-MOR-NULL
            liq.getLquDtMor().setLquDtMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtMor.Len.LQU_DT_MOR_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-DEN = -1
        //              MOVE HIGH-VALUES TO LQU-DT-DEN-NULL
        //           END-IF
        if (ws.getIndLiq().getDtDen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-DEN-NULL
            liq.getLquDtDen().setLquDtDenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtDen.Len.LQU_DT_DEN_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-PERV-DEN = -1
        //              MOVE HIGH-VALUES TO LQU-DT-PERV-DEN-NULL
        //           END-IF
        if (ws.getIndLiq().getDtPervDen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-PERV-DEN-NULL
            liq.getLquDtPervDen().setLquDtPervDenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtPervDen.Len.LQU_DT_PERV_DEN_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-RICH = -1
        //              MOVE HIGH-VALUES TO LQU-DT-RICH-NULL
        //           END-IF
        if (ws.getIndLiq().getDtRich() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-RICH-NULL
            liq.getLquDtRich().setLquDtRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtRich.Len.LQU_DT_RICH_NULL));
        }
        // COB_CODE: IF IND-LQU-TP-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-TP-SIN-NULL
        //           END-IF
        if (ws.getIndLiq().getTpSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-SIN-NULL
            liq.setLquTpSin(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_TP_SIN));
        }
        // COB_CODE: IF IND-LQU-TP-RISC = -1
        //              MOVE HIGH-VALUES TO LQU-TP-RISC-NULL
        //           END-IF
        if (ws.getIndLiq().getTpRisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-RISC-NULL
            liq.setLquTpRisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_TP_RISC));
        }
        // COB_CODE: IF IND-LQU-TP-MET-RISC = -1
        //              MOVE HIGH-VALUES TO LQU-TP-MET-RISC-NULL
        //           END-IF
        if (ws.getIndLiq().getTpMetRisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-MET-RISC-NULL
            liq.getLquTpMetRisc().setLquTpMetRiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTpMetRisc.Len.LQU_TP_MET_RISC_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-DT-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getDtLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-LIQ-NULL
            liq.getLquDtLiq().setLquDtLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtLiq.Len.LQU_DT_LIQ_NULL));
        }
        // COB_CODE: IF IND-LQU-COD-DVS = -1
        //              MOVE HIGH-VALUES TO LQU-COD-DVS-NULL
        //           END-IF
        if (ws.getIndLiq().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COD-DVS-NULL
            liq.setLquCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_COD_DVS));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-LRD-LIQTO = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-LRD-LIQTO-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpLrdLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-LRD-LIQTO-NULL
            liq.getLquTotImpLrdLiqto().setLquTotImpLrdLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpLrdLiqto.Len.LQU_TOT_IMP_LRD_LIQTO_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-PREST = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-PREST-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-PREST-NULL
            liq.getLquTotImpPrest().setLquTotImpPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpPrest.Len.LQU_TOT_IMP_PREST_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-INTR-PREST-NULL
            liq.getLquTotImpIntrPrest().setLquTotImpIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpIntrPrest.Len.LQU_TOT_IMP_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-UTI = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-UTI-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpUti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-UTI-NULL
            liq.getLquTotImpUti().setLquTotImpUtiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpUti.Len.LQU_TOT_IMP_UTI_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIT-TFR = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-TFR-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRitTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-TFR-NULL
            liq.getLquTotImpRitTfr().setLquTotImpRitTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRitTfr.Len.LQU_TOT_IMP_RIT_TFR_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIT-ACC = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-ACC-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRitAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-ACC-NULL
            liq.getLquTotImpRitAcc().setLquTotImpRitAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRitAcc.Len.LQU_TOT_IMP_RIT_ACC_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIT-VIS = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-VIS-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRitVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-VIS-NULL
            liq.getLquTotImpRitVis().setLquTotImpRitVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRitVis.Len.LQU_TOT_IMP_RIT_VIS_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMPB-TFR = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMPB-TFR-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpbTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMPB-TFR-NULL
            liq.getLquTotImpbTfr().setLquTotImpbTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpbTfr.Len.LQU_TOT_IMPB_TFR_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMPB-ACC = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMPB-ACC-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpbAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMPB-ACC-NULL
            liq.getLquTotImpbAcc().setLquTotImpbAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpbAcc.Len.LQU_TOT_IMPB_ACC_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMPB-VIS = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMPB-VIS-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpbVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMPB-VIS-NULL
            liq.getLquTotImpbVis().setLquTotImpbVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpbVis.Len.LQU_TOT_IMPB_VIS_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIMB-NULL
            liq.getLquTotImpRimb().setLquTotImpRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRimb.Len.LQU_TOT_IMP_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IMPST-PRVR = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IMPST-PRVR-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbImpstPrvr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IMPST-PRVR-NULL
            liq.getLquImpbImpstPrvr().setLquImpbImpstPrvrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbImpstPrvr.Len.LQU_IMPB_IMPST_PRVR_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-PRVR = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-PRVR-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstPrvr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-PRVR-NULL
            liq.getLquImpstPrvr().setLquImpstPrvrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstPrvr.Len.LQU_IMPST_PRVR_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IMPST-252 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IMPST-252-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbImpst252() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IMPST-252-NULL
            liq.getLquImpbImpst252().setLquImpbImpst252Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbImpst252.Len.LQU_IMPB_IMPST252_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-252 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-252-NULL
        //           END-IF
        if (ws.getIndLiq().getImpst252() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-252-NULL
            liq.getLquImpst252().setLquImpst252Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpst252.Len.LQU_IMPST252_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-IS = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-IS-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-IS-NULL
            liq.getLquTotImpIs().setLquTotImpIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpIs.Len.LQU_TOT_IMP_IS_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-DIR-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-DIR-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getImpDirLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-DIR-LIQ-NULL
            liq.getLquImpDirLiq().setLquImpDirLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpDirLiq.Len.LQU_IMP_DIR_LIQ_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-NET-LIQTO = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-NET-LIQTO-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpNetLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-NET-LIQTO-NULL
            liq.getLquTotImpNetLiqto().setLquTotImpNetLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpNetLiqto.Len.LQU_TOT_IMP_NET_LIQTO_NULL));
        }
        // COB_CODE: IF IND-LQU-MONT-END2000 = -1
        //              MOVE HIGH-VALUES TO LQU-MONT-END2000-NULL
        //           END-IF
        if (ws.getIndLiq().getMontEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-MONT-END2000-NULL
            liq.getLquMontEnd2000().setLquMontEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquMontEnd2000.Len.LQU_MONT_END2000_NULL));
        }
        // COB_CODE: IF IND-LQU-MONT-END2006 = -1
        //              MOVE HIGH-VALUES TO LQU-MONT-END2006-NULL
        //           END-IF
        if (ws.getIndLiq().getMontEnd2006() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-MONT-END2006-NULL
            liq.getLquMontEnd2006().setLquMontEnd2006Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquMontEnd2006.Len.LQU_MONT_END2006_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-NULL
            liq.getLquPcRen().setLquPcRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRen.Len.LQU_PC_REN_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-PNL = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-PNL-NULL
        //           END-IF
        if (ws.getIndLiq().getImpPnl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-PNL-NULL
            liq.getLquImpPnl().setLquImpPnlNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpPnl.Len.LQU_IMP_PNL_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IRPEF = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IRPEF-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIrpef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IRPEF-NULL
            liq.getLquImpbIrpef().setLquImpbIrpefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIrpef.Len.LQU_IMPB_IRPEF_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-IRPEF = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-IRPEF-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstIrpef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-IRPEF-NULL
            liq.getLquImpstIrpef().setLquImpstIrpefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstIrpef.Len.LQU_IMPST_IRPEF_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-VLT = -1
        //              MOVE HIGH-VALUES TO LQU-DT-VLT-NULL
        //           END-IF
        if (ws.getIndLiq().getDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-VLT-NULL
            liq.getLquDtVlt().setLquDtVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtVlt.Len.LQU_DT_VLT_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-END-ISTR = -1
        //              MOVE HIGH-VALUES TO LQU-DT-END-ISTR-NULL
        //           END-IF
        if (ws.getIndLiq().getDtEndIstr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-END-ISTR-NULL
            liq.getLquDtEndIstr().setLquDtEndIstrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtEndIstr.Len.LQU_DT_END_ISTR_NULL));
        }
        // COB_CODE: IF IND-LQU-SPE-RCS = -1
        //              MOVE HIGH-VALUES TO LQU-SPE-RCS-NULL
        //           END-IF
        if (ws.getIndLiq().getSpeRcs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-SPE-RCS-NULL
            liq.getLquSpeRcs().setLquSpeRcsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquSpeRcs.Len.LQU_SPE_RCS_NULL));
        }
        // COB_CODE: IF IND-LQU-IB-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-IB-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getIbLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IB-LIQ-NULL
            liq.setLquIbLiq(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_IB_LIQ));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-ONER-PRVNT = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-ONER-PRVNT-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasOnerPrvnt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-ONER-PRVNT-NULL
            liq.getLquTotIasOnerPrvnt().setLquTotIasOnerPrvntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasOnerPrvnt.Len.LQU_TOT_IAS_ONER_PRVNT_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-MGG-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-MGG-SIN-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasMggSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-MGG-SIN-NULL
            liq.getLquTotIasMggSin().setLquTotIasMggSinNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasMggSin.Len.LQU_TOT_IAS_MGG_SIN_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-RST-DPST = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-RST-DPST-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasRstDpst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-RST-DPST-NULL
            liq.getLquTotIasRstDpst().setLquTotIasRstDpstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasRstDpst.Len.LQU_TOT_IAS_RST_DPST_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-ONER-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-ONER-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getImpOnerLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-ONER-LIQ-NULL
            liq.getLquImpOnerLiq().setLquImpOnerLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpOnerLiq.Len.LQU_IMP_ONER_LIQ_NULL));
        }
        // COB_CODE: IF IND-LQU-COMPON-TAX-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-COMPON-TAX-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getComponTaxRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COMPON-TAX-RIMB-NULL
            liq.getLquComponTaxRimb().setLquComponTaxRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquComponTaxRimb.Len.LQU_COMPON_TAX_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-TP-MEZ-PAG = -1
        //              MOVE HIGH-VALUES TO LQU-TP-MEZ-PAG-NULL
        //           END-IF
        if (ws.getIndLiq().getTpMezPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-MEZ-PAG-NULL
            liq.setLquTpMezPag(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_TP_MEZ_PAG));
        }
        // COB_CODE: IF IND-LQU-IMP-EXCONTR = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-EXCONTR-NULL
        //           END-IF
        if (ws.getIndLiq().getImpExcontr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-EXCONTR-NULL
            liq.getLquImpExcontr().setLquImpExcontrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpExcontr.Len.LQU_IMP_EXCONTR_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-INTR-RIT-PAG = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-INTR-RIT-PAG-NULL
        //           END-IF
        if (ws.getIndLiq().getImpIntrRitPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-INTR-RIT-PAG-NULL
            liq.getLquImpIntrRitPag().setLquImpIntrRitPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpIntrRitPag.Len.LQU_IMP_INTR_RIT_PAG_NULL));
        }
        // COB_CODE: IF IND-LQU-BNS-NON-GODUTO = -1
        //              MOVE HIGH-VALUES TO LQU-BNS-NON-GODUTO-NULL
        //           END-IF
        if (ws.getIndLiq().getBnsNonGoduto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-BNS-NON-GODUTO-NULL
            liq.getLquBnsNonGoduto().setLquBnsNonGodutoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquBnsNonGoduto.Len.LQU_BNS_NON_GODUTO_NULL));
        }
        // COB_CODE: IF IND-LQU-CNBT-INPSTFM = -1
        //              MOVE HIGH-VALUES TO LQU-CNBT-INPSTFM-NULL
        //           END-IF
        if (ws.getIndLiq().getCnbtInpstfm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-CNBT-INPSTFM-NULL
            liq.getLquCnbtInpstfm().setLquCnbtInpstfmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquCnbtInpstfm.Len.LQU_CNBT_INPSTFM_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-DA-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-DA-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstDaRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-DA-RIMB-NULL
            liq.getLquImpstDaRimb().setLquImpstDaRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstDaRimb.Len.LQU_IMPST_DA_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IS = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IS-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IS-NULL
            liq.getLquImpbIs().setLquImpbIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIs.Len.LQU_IMPB_IS_NULL));
        }
        // COB_CODE: IF IND-LQU-TAX-SEP = -1
        //              MOVE HIGH-VALUES TO LQU-TAX-SEP-NULL
        //           END-IF
        if (ws.getIndLiq().getTaxSep() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TAX-SEP-NULL
            liq.getLquTaxSep().setLquTaxSepNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTaxSep.Len.LQU_TAX_SEP_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-TAX-SEP = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-TAX-SEP-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbTaxSep() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-TAX-SEP-NULL
            liq.getLquImpbTaxSep().setLquImpbTaxSepNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbTaxSep.Len.LQU_IMPB_TAX_SEP_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-INTR-SU-PREST = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-INTR-SU-PREST-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIntrSuPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-INTR-SU-PREST-NULL
            liq.getLquImpbIntrSuPrest().setLquImpbIntrSuPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIntrSuPrest.Len.LQU_IMPB_INTR_SU_PREST_NULL));
        }
        // COB_CODE: IF IND-LQU-ADDIZ-COMUN = -1
        //              MOVE HIGH-VALUES TO LQU-ADDIZ-COMUN-NULL
        //           END-IF
        if (ws.getIndLiq().getAddizComun() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-ADDIZ-COMUN-NULL
            liq.getLquAddizComun().setLquAddizComunNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquAddizComun.Len.LQU_ADDIZ_COMUN_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-ADDIZ-COMUN = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-COMUN-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbAddizComun() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-COMUN-NULL
            liq.getLquImpbAddizComun().setLquImpbAddizComunNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbAddizComun.Len.LQU_IMPB_ADDIZ_COMUN_NULL));
        }
        // COB_CODE: IF IND-LQU-ADDIZ-REGION = -1
        //              MOVE HIGH-VALUES TO LQU-ADDIZ-REGION-NULL
        //           END-IF
        if (ws.getIndLiq().getAddizRegion() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-ADDIZ-REGION-NULL
            liq.getLquAddizRegion().setLquAddizRegionNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquAddizRegion.Len.LQU_ADDIZ_REGION_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-ADDIZ-REGION = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-REGION-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbAddizRegion() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-REGION-NULL
            liq.getLquImpbAddizRegion().setLquImpbAddizRegionNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbAddizRegion.Len.LQU_IMPB_ADDIZ_REGION_NULL));
        }
        // COB_CODE: IF IND-LQU-MONT-DAL2007 = -1
        //              MOVE HIGH-VALUES TO LQU-MONT-DAL2007-NULL
        //           END-IF
        if (ws.getIndLiq().getMontDal2007() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-MONT-DAL2007-NULL
            liq.getLquMontDal2007().setLquMontDal2007Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquMontDal2007.Len.LQU_MONT_DAL2007_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-CNBT-INPSTFM = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-CNBT-INPSTFM-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbCnbtInpstfm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-CNBT-INPSTFM-NULL
            liq.getLquImpbCnbtInpstfm().setLquImpbCnbtInpstfmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbCnbtInpstfm.Len.LQU_IMPB_CNBT_INPSTFM_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-LRD-DA-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-LRD-DA-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getImpLrdDaRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-LRD-DA-RIMB-NULL
            liq.getLquImpLrdDaRimb().setLquImpLrdDaRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpLrdDaRimb.Len.LQU_IMP_LRD_DA_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-DIR-DA-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-DIR-DA-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getImpDirDaRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-DIR-DA-RIMB-NULL
            liq.getLquImpDirDaRimb().setLquImpDirDaRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpDirDaRimb.Len.LQU_IMP_DIR_DA_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-RIS-MAT = -1
        //              MOVE HIGH-VALUES TO LQU-RIS-MAT-NULL
        //           END-IF
        if (ws.getIndLiq().getRisMat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-RIS-MAT-NULL
            liq.getLquRisMat().setLquRisMatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquRisMat.Len.LQU_RIS_MAT_NULL));
        }
        // COB_CODE: IF IND-LQU-RIS-SPE = -1
        //              MOVE HIGH-VALUES TO LQU-RIS-SPE-NULL
        //           END-IF
        if (ws.getIndLiq().getRisSpe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-RIS-SPE-NULL
            liq.getLquRisSpe().setLquRisSpeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquRisSpe.Len.LQU_RIS_SPE_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-PNL = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-PNL-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasPnl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-PNL-NULL
            liq.getLquTotIasPnl().setLquTotIasPnlNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasPnl.Len.LQU_TOT_IAS_PNL_NULL));
        }
        // COB_CODE: IF IND-LQU-FL-EVE-GARTO = -1
        //              MOVE HIGH-VALUES TO LQU-FL-EVE-GARTO-NULL
        //           END-IF
        if (ws.getIndLiq().getFlEveGarto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-FL-EVE-GARTO-NULL
            liq.setLquFlEveGarto(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-LQU-IMP-REN-K1 = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-REN-K1-NULL
        //           END-IF
        if (ws.getIndLiq().getImpRenK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-REN-K1-NULL
            liq.getLquImpRenK1().setLquImpRenK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpRenK1.Len.LQU_IMP_REN_K1_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-REN-K2 = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-REN-K2-NULL
        //           END-IF
        if (ws.getIndLiq().getImpRenK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-REN-K2-NULL
            liq.getLquImpRenK2().setLquImpRenK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpRenK2.Len.LQU_IMP_REN_K2_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-REN-K3 = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-REN-K3-NULL
        //           END-IF
        if (ws.getIndLiq().getImpRenK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-REN-K3-NULL
            liq.getLquImpRenK3().setLquImpRenK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpRenK3.Len.LQU_IMP_REN_K3_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN-K1 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-K1-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRenK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-K1-NULL
            liq.getLquPcRenK1().setLquPcRenK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRenK1.Len.LQU_PC_REN_K1_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN-K2 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-K2-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRenK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-K2-NULL
            liq.getLquPcRenK2().setLquPcRenK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRenK2.Len.LQU_PC_REN_K2_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN-K3 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-K3-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRenK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-K3-NULL
            liq.getLquPcRenK3().setLquPcRenK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRenK3.Len.LQU_PC_REN_K3_NULL));
        }
        // COB_CODE: IF IND-LQU-TP-CAUS-ANTIC = -1
        //              MOVE HIGH-VALUES TO LQU-TP-CAUS-ANTIC-NULL
        //           END-IF
        if (ws.getIndLiq().getTpCausAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-CAUS-ANTIC-NULL
            liq.setLquTpCausAntic(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_TP_CAUS_ANTIC));
        }
        // COB_CODE: IF IND-LQU-IMP-LRD-LIQTO-RILT = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-LRD-LIQTO-RILT-NULL
        //           END-IF
        if (ws.getIndLiq().getImpLrdLiqtoRilt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-LRD-LIQTO-RILT-NULL
            liq.getLquImpLrdLiqtoRilt().setLquImpLrdLiqtoRiltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpLrdLiqtoRilt.Len.LQU_IMP_LRD_LIQTO_RILT_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-APPL-RILT = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-APPL-RILT-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstApplRilt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-APPL-RILT-NULL
            liq.getLquImpstApplRilt().setLquImpstApplRiltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstApplRilt.Len.LQU_IMPST_APPL_RILT_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-RISC-PARZ = -1
        //              MOVE HIGH-VALUES TO LQU-PC-RISC-PARZ-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRiscParz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-RISC-PARZ-NULL
            liq.getLquPcRiscParz().setLquPcRiscParzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRiscParz.Len.LQU_PC_RISC_PARZ_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-TOT-V = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-V-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloTotV() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-V-NULL
            liq.getLquImpstBolloTotV().setLquImpstBolloTotVNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloTotV.Len.LQU_IMPST_BOLLO_TOT_V_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-DETT-C = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-DETT-C-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloDettC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-DETT-C-NULL
            liq.getLquImpstBolloDettC().setLquImpstBolloDettCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloDettC.Len.LQU_IMPST_BOLLO_DETT_C_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-TOT-SW = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-SW-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloTotSw() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-SW-NULL
            liq.getLquImpstBolloTotSw().setLquImpstBolloTotSwNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloTotSw.Len.LQU_IMPST_BOLLO_TOT_SW_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-TOT-AA = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-AA-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloTotAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-AA-NULL
            liq.getLquImpstBolloTotAa().setLquImpstBolloTotAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloTotAa.Len.LQU_IMPST_BOLLO_TOT_AA_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-VIS-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-VIS-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbVis1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-VIS-1382011-NULL
            liq.getLquImpbVis1382011().setLquImpbVis1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbVis1382011.Len.LQU_IMPB_VIS1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-VIS-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-VIS-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstVis1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-VIS-1382011-NULL
            liq.getLquImpstVis1382011().setLquImpstVis1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstVis1382011.Len.LQU_IMPST_VIS1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IS-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IS-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIs1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IS-1382011-NULL
            liq.getLquImpbIs1382011().setLquImpbIs1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIs1382011.Len.LQU_IMPB_IS1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-SOST-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-SOST-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstSost1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-SOST-1382011-NULL
            liq.getLquImpstSost1382011().setLquImpstSost1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstSost1382011.Len.LQU_IMPST_SOST1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-ABB-TIT-STAT = -1
        //              MOVE HIGH-VALUES TO LQU-PC-ABB-TIT-STAT-NULL
        //           END-IF
        if (ws.getIndLiq().getPcAbbTitStat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-ABB-TIT-STAT-NULL
            liq.getLquPcAbbTitStat().setLquPcAbbTitStatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcAbbTitStat.Len.LQU_PC_ABB_TIT_STAT_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-BOLLO-DETT-C = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-BOLLO-DETT-C-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbBolloDettC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-BOLLO-DETT-C-NULL
            liq.getLquImpbBolloDettC().setLquImpbBolloDettCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbBolloDettC.Len.LQU_IMPB_BOLLO_DETT_C_NULL));
        }
        // COB_CODE: IF IND-LQU-FL-PRE-COMP = -1
        //              MOVE HIGH-VALUES TO LQU-FL-PRE-COMP-NULL
        //           END-IF
        if (ws.getIndLiq().getFlPreComp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-FL-PRE-COMP-NULL
            liq.setLquFlPreComp(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-LQU-IMPB-VIS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-VIS-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbVis662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-VIS-662014-NULL
            liq.getLquImpbVis662014().setLquImpbVis662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbVis662014.Len.LQU_IMPB_VIS662014_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-VIS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-VIS-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstVis662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-VIS-662014-NULL
            liq.getLquImpstVis662014().setLquImpstVis662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstVis662014.Len.LQU_IMPST_VIS662014_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IS-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIs662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IS-662014-NULL
            liq.getLquImpbIs662014().setLquImpbIs662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIs662014.Len.LQU_IMPB_IS662014_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-SOST-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-SOST-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstSost662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-SOST-662014-NULL
            liq.getLquImpstSost662014().setLquImpstSost662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstSost662014.Len.LQU_IMPST_SOST662014_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-ABB-TS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-ABB-TS-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getPcAbbTs662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-ABB-TS-662014-NULL
            liq.getLquPcAbbTs662014().setLquPcAbbTs662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcAbbTs662014.Len.LQU_PC_ABB_TS662014_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-LRD-CALC-CP = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-LRD-CALC-CP-NULL
        //           END-IF
        if (ws.getIndLiq().getImpLrdCalcCp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-LRD-CALC-CP-NULL
            liq.getLquImpLrdCalcCp().setLquImpLrdCalcCpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpLrdCalcCp.Len.LQU_IMP_LRD_CALC_CP_NULL));
        }
        // COB_CODE: IF IND-LQU-COS-TUNNEL-USCITA = -1
        //              MOVE HIGH-VALUES TO LQU-COS-TUNNEL-USCITA-NULL
        //           END-IF.
        if (ws.getIndLiq().getCosTunnelUscita() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COS-TUNNEL-USCITA-NULL
            liq.getLquCosTunnelUscita().setLquCosTunnelUscitaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquCosTunnelUscita.Len.LQU_COS_TUNNEL_USCITA_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO LQU-DS-OPER-SQL
        liq.setLquDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO LQU-DS-VER
        liq.setLquDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO LQU-DS-UTENTE
        liq.setLquDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO LQU-DS-STATO-ELAB.
        liq.setLquDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO LQU-DS-OPER-SQL
        liq.setLquDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO LQU-DS-UTENTE.
        liq.setLquDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF LQU-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-LQU-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquIdMoviChiu().getLquIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-ID-MOVI-CHIU
            ws.getIndLiq().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-ID-MOVI-CHIU
            ws.getIndLiq().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF LQU-IB-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IB-OGG
        //           ELSE
        //              MOVE 0 TO IND-LQU-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquIbOgg(), LiqIdbslqu0.Len.LQU_IB_OGG)) {
            // COB_CODE: MOVE -1 TO IND-LQU-IB-OGG
            ws.getIndLiq().setIbOgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IB-OGG
            ws.getIndLiq().setIbOgg(((short)0));
        }
        // COB_CODE: IF LQU-DESC-CAU-EVE-SIN = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-DESC-CAU-EVE-SIN
        //           ELSE
        //              MOVE 0 TO IND-LQU-DESC-CAU-EVE-SIN
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquDescCauEveSin(), LiqIdbslqu0.Len.LQU_DESC_CAU_EVE_SIN)) {
            // COB_CODE: MOVE -1 TO IND-LQU-DESC-CAU-EVE-SIN
            ws.getIndLiq().setDescCauEveSin(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-DESC-CAU-EVE-SIN
            ws.getIndLiq().setDescCauEveSin(((short)0));
        }
        // COB_CODE: IF LQU-COD-CAU-SIN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-COD-CAU-SIN
        //           ELSE
        //              MOVE 0 TO IND-LQU-COD-CAU-SIN
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquCodCauSinFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-COD-CAU-SIN
            ws.getIndLiq().setCodCauSin(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-COD-CAU-SIN
            ws.getIndLiq().setCodCauSin(((short)0));
        }
        // COB_CODE: IF LQU-COD-SIN-CATSTRF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-COD-SIN-CATSTRF
        //           ELSE
        //              MOVE 0 TO IND-LQU-COD-SIN-CATSTRF
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquCodSinCatstrfFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-COD-SIN-CATSTRF
            ws.getIndLiq().setCodSinCatstrf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-COD-SIN-CATSTRF
            ws.getIndLiq().setCodSinCatstrf(((short)0));
        }
        // COB_CODE: IF LQU-DT-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-DT-MOR
        //           ELSE
        //              MOVE 0 TO IND-LQU-DT-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquDtMor().getLquDtMorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-DT-MOR
            ws.getIndLiq().setDtMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-DT-MOR
            ws.getIndLiq().setDtMor(((short)0));
        }
        // COB_CODE: IF LQU-DT-DEN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-DT-DEN
        //           ELSE
        //              MOVE 0 TO IND-LQU-DT-DEN
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquDtDen().getLquDtDenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-DT-DEN
            ws.getIndLiq().setDtDen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-DT-DEN
            ws.getIndLiq().setDtDen(((short)0));
        }
        // COB_CODE: IF LQU-DT-PERV-DEN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-DT-PERV-DEN
        //           ELSE
        //              MOVE 0 TO IND-LQU-DT-PERV-DEN
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquDtPervDen().getLquDtPervDenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-DT-PERV-DEN
            ws.getIndLiq().setDtPervDen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-DT-PERV-DEN
            ws.getIndLiq().setDtPervDen(((short)0));
        }
        // COB_CODE: IF LQU-DT-RICH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-DT-RICH
        //           ELSE
        //              MOVE 0 TO IND-LQU-DT-RICH
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquDtRich().getLquDtRichNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-DT-RICH
            ws.getIndLiq().setDtRich(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-DT-RICH
            ws.getIndLiq().setDtRich(((short)0));
        }
        // COB_CODE: IF LQU-TP-SIN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TP-SIN
        //           ELSE
        //              MOVE 0 TO IND-LQU-TP-SIN
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTpSinFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TP-SIN
            ws.getIndLiq().setTpSin(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TP-SIN
            ws.getIndLiq().setTpSin(((short)0));
        }
        // COB_CODE: IF LQU-TP-RISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TP-RISC
        //           ELSE
        //              MOVE 0 TO IND-LQU-TP-RISC
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTpRiscFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TP-RISC
            ws.getIndLiq().setTpRisc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TP-RISC
            ws.getIndLiq().setTpRisc(((short)0));
        }
        // COB_CODE: IF LQU-TP-MET-RISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TP-MET-RISC
        //           ELSE
        //              MOVE 0 TO IND-LQU-TP-MET-RISC
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTpMetRisc().getLquTpMetRiscNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TP-MET-RISC
            ws.getIndLiq().setTpMetRisc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TP-MET-RISC
            ws.getIndLiq().setTpMetRisc(((short)0));
        }
        // COB_CODE: IF LQU-DT-LIQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-DT-LIQ
        //           ELSE
        //              MOVE 0 TO IND-LQU-DT-LIQ
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquDtLiq().getLquDtLiqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-DT-LIQ
            ws.getIndLiq().setDtLiq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-DT-LIQ
            ws.getIndLiq().setDtLiq(((short)0));
        }
        // COB_CODE: IF LQU-COD-DVS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-COD-DVS
        //           ELSE
        //              MOVE 0 TO IND-LQU-COD-DVS
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquCodDvsFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-COD-DVS
            ws.getIndLiq().setCodDvs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-COD-DVS
            ws.getIndLiq().setCodDvs(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMP-LRD-LIQTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMP-LRD-LIQTO
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMP-LRD-LIQTO
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpLrdLiqto().getLquTotImpLrdLiqtoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMP-LRD-LIQTO
            ws.getIndLiq().setTotImpLrdLiqto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMP-LRD-LIQTO
            ws.getIndLiq().setTotImpLrdLiqto(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMP-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMP-PREST
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMP-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpPrest().getLquTotImpPrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMP-PREST
            ws.getIndLiq().setTotImpPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMP-PREST
            ws.getIndLiq().setTotImpPrest(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMP-INTR-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMP-INTR-PREST
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMP-INTR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpIntrPrest().getLquTotImpIntrPrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMP-INTR-PREST
            ws.getIndLiq().setTotImpIntrPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMP-INTR-PREST
            ws.getIndLiq().setTotImpIntrPrest(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMP-UTI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMP-UTI
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMP-UTI
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpUti().getLquTotImpUtiNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMP-UTI
            ws.getIndLiq().setTotImpUti(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMP-UTI
            ws.getIndLiq().setTotImpUti(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMP-RIT-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMP-RIT-TFR
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMP-RIT-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpRitTfr().getLquTotImpRitTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMP-RIT-TFR
            ws.getIndLiq().setTotImpRitTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMP-RIT-TFR
            ws.getIndLiq().setTotImpRitTfr(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMP-RIT-ACC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMP-RIT-ACC
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMP-RIT-ACC
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpRitAcc().getLquTotImpRitAccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMP-RIT-ACC
            ws.getIndLiq().setTotImpRitAcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMP-RIT-ACC
            ws.getIndLiq().setTotImpRitAcc(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMP-RIT-VIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMP-RIT-VIS
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMP-RIT-VIS
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpRitVis().getLquTotImpRitVisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMP-RIT-VIS
            ws.getIndLiq().setTotImpRitVis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMP-RIT-VIS
            ws.getIndLiq().setTotImpRitVis(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMPB-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMPB-TFR
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMPB-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpbTfr().getLquTotImpbTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMPB-TFR
            ws.getIndLiq().setTotImpbTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMPB-TFR
            ws.getIndLiq().setTotImpbTfr(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMPB-ACC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMPB-ACC
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMPB-ACC
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpbAcc().getLquTotImpbAccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMPB-ACC
            ws.getIndLiq().setTotImpbAcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMPB-ACC
            ws.getIndLiq().setTotImpbAcc(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMPB-VIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMPB-VIS
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMPB-VIS
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpbVis().getLquTotImpbVisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMPB-VIS
            ws.getIndLiq().setTotImpbVis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMPB-VIS
            ws.getIndLiq().setTotImpbVis(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMP-RIMB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMP-RIMB
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMP-RIMB
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpRimb().getLquTotImpRimbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMP-RIMB
            ws.getIndLiq().setTotImpRimb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMP-RIMB
            ws.getIndLiq().setTotImpRimb(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-IMPST-PRVR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-IMPST-PRVR
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-IMPST-PRVR
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbImpstPrvr().getLquImpbImpstPrvrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-IMPST-PRVR
            ws.getIndLiq().setImpbImpstPrvr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-IMPST-PRVR
            ws.getIndLiq().setImpbImpstPrvr(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-PRVR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-PRVR
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-PRVR
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstPrvr().getLquImpstPrvrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-PRVR
            ws.getIndLiq().setImpstPrvr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-PRVR
            ws.getIndLiq().setImpstPrvr(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-IMPST-252-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-IMPST-252
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-IMPST-252
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbImpst252().getLquImpbImpst252NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-IMPST-252
            ws.getIndLiq().setImpbImpst252(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-IMPST-252
            ws.getIndLiq().setImpbImpst252(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-252-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-252
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-252
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpst252().getLquImpst252NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-252
            ws.getIndLiq().setImpst252(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-252
            ws.getIndLiq().setImpst252(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMP-IS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMP-IS
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMP-IS
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpIs().getLquTotImpIsNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMP-IS
            ws.getIndLiq().setTotImpIs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMP-IS
            ws.getIndLiq().setTotImpIs(((short)0));
        }
        // COB_CODE: IF LQU-IMP-DIR-LIQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-DIR-LIQ
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-DIR-LIQ
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpDirLiq().getLquImpDirLiqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-DIR-LIQ
            ws.getIndLiq().setImpDirLiq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-DIR-LIQ
            ws.getIndLiq().setImpDirLiq(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IMP-NET-LIQTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IMP-NET-LIQTO
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IMP-NET-LIQTO
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotImpNetLiqto().getLquTotImpNetLiqtoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IMP-NET-LIQTO
            ws.getIndLiq().setTotImpNetLiqto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IMP-NET-LIQTO
            ws.getIndLiq().setTotImpNetLiqto(((short)0));
        }
        // COB_CODE: IF LQU-MONT-END2000-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-MONT-END2000
        //           ELSE
        //              MOVE 0 TO IND-LQU-MONT-END2000
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquMontEnd2000().getLquMontEnd2000NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-MONT-END2000
            ws.getIndLiq().setMontEnd2000(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-MONT-END2000
            ws.getIndLiq().setMontEnd2000(((short)0));
        }
        // COB_CODE: IF LQU-MONT-END2006-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-MONT-END2006
        //           ELSE
        //              MOVE 0 TO IND-LQU-MONT-END2006
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquMontEnd2006().getLquMontEnd2006NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-MONT-END2006
            ws.getIndLiq().setMontEnd2006(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-MONT-END2006
            ws.getIndLiq().setMontEnd2006(((short)0));
        }
        // COB_CODE: IF LQU-PC-REN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-PC-REN
        //           ELSE
        //              MOVE 0 TO IND-LQU-PC-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquPcRen().getLquPcRenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-PC-REN
            ws.getIndLiq().setPcRen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-PC-REN
            ws.getIndLiq().setPcRen(((short)0));
        }
        // COB_CODE: IF LQU-IMP-PNL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-PNL
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-PNL
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpPnl().getLquImpPnlNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-PNL
            ws.getIndLiq().setImpPnl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-PNL
            ws.getIndLiq().setImpPnl(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-IRPEF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-IRPEF
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-IRPEF
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbIrpef().getLquImpbIrpefNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-IRPEF
            ws.getIndLiq().setImpbIrpef(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-IRPEF
            ws.getIndLiq().setImpbIrpef(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-IRPEF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-IRPEF
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-IRPEF
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstIrpef().getLquImpstIrpefNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-IRPEF
            ws.getIndLiq().setImpstIrpef(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-IRPEF
            ws.getIndLiq().setImpstIrpef(((short)0));
        }
        // COB_CODE: IF LQU-DT-VLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-DT-VLT
        //           ELSE
        //              MOVE 0 TO IND-LQU-DT-VLT
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquDtVlt().getLquDtVltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-DT-VLT
            ws.getIndLiq().setDtVlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-DT-VLT
            ws.getIndLiq().setDtVlt(((short)0));
        }
        // COB_CODE: IF LQU-DT-END-ISTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-DT-END-ISTR
        //           ELSE
        //              MOVE 0 TO IND-LQU-DT-END-ISTR
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquDtEndIstr().getLquDtEndIstrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-DT-END-ISTR
            ws.getIndLiq().setDtEndIstr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-DT-END-ISTR
            ws.getIndLiq().setDtEndIstr(((short)0));
        }
        // COB_CODE: IF LQU-SPE-RCS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-SPE-RCS
        //           ELSE
        //              MOVE 0 TO IND-LQU-SPE-RCS
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquSpeRcs().getLquSpeRcsNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-SPE-RCS
            ws.getIndLiq().setSpeRcs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-SPE-RCS
            ws.getIndLiq().setSpeRcs(((short)0));
        }
        // COB_CODE: IF LQU-IB-LIQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IB-LIQ
        //           ELSE
        //              MOVE 0 TO IND-LQU-IB-LIQ
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquIbLiq(), LiqIdbslqu0.Len.LQU_IB_LIQ)) {
            // COB_CODE: MOVE -1 TO IND-LQU-IB-LIQ
            ws.getIndLiq().setIbLiq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IB-LIQ
            ws.getIndLiq().setIbLiq(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IAS-ONER-PRVNT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IAS-ONER-PRVNT
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IAS-ONER-PRVNT
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotIasOnerPrvnt().getLquTotIasOnerPrvntNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IAS-ONER-PRVNT
            ws.getIndLiq().setTotIasOnerPrvnt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IAS-ONER-PRVNT
            ws.getIndLiq().setTotIasOnerPrvnt(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IAS-MGG-SIN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IAS-MGG-SIN
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IAS-MGG-SIN
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotIasMggSin().getLquTotIasMggSinNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IAS-MGG-SIN
            ws.getIndLiq().setTotIasMggSin(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IAS-MGG-SIN
            ws.getIndLiq().setTotIasMggSin(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IAS-RST-DPST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IAS-RST-DPST
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IAS-RST-DPST
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotIasRstDpst().getLquTotIasRstDpstNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IAS-RST-DPST
            ws.getIndLiq().setTotIasRstDpst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IAS-RST-DPST
            ws.getIndLiq().setTotIasRstDpst(((short)0));
        }
        // COB_CODE: IF LQU-IMP-ONER-LIQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-ONER-LIQ
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-ONER-LIQ
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpOnerLiq().getLquImpOnerLiqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-ONER-LIQ
            ws.getIndLiq().setImpOnerLiq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-ONER-LIQ
            ws.getIndLiq().setImpOnerLiq(((short)0));
        }
        // COB_CODE: IF LQU-COMPON-TAX-RIMB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-COMPON-TAX-RIMB
        //           ELSE
        //              MOVE 0 TO IND-LQU-COMPON-TAX-RIMB
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquComponTaxRimb().getLquComponTaxRimbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-COMPON-TAX-RIMB
            ws.getIndLiq().setComponTaxRimb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-COMPON-TAX-RIMB
            ws.getIndLiq().setComponTaxRimb(((short)0));
        }
        // COB_CODE: IF LQU-TP-MEZ-PAG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TP-MEZ-PAG
        //           ELSE
        //              MOVE 0 TO IND-LQU-TP-MEZ-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTpMezPagFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TP-MEZ-PAG
            ws.getIndLiq().setTpMezPag(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TP-MEZ-PAG
            ws.getIndLiq().setTpMezPag(((short)0));
        }
        // COB_CODE: IF LQU-IMP-EXCONTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-EXCONTR
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-EXCONTR
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpExcontr().getLquImpExcontrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-EXCONTR
            ws.getIndLiq().setImpExcontr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-EXCONTR
            ws.getIndLiq().setImpExcontr(((short)0));
        }
        // COB_CODE: IF LQU-IMP-INTR-RIT-PAG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-INTR-RIT-PAG
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-INTR-RIT-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpIntrRitPag().getLquImpIntrRitPagNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-INTR-RIT-PAG
            ws.getIndLiq().setImpIntrRitPag(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-INTR-RIT-PAG
            ws.getIndLiq().setImpIntrRitPag(((short)0));
        }
        // COB_CODE: IF LQU-BNS-NON-GODUTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-BNS-NON-GODUTO
        //           ELSE
        //              MOVE 0 TO IND-LQU-BNS-NON-GODUTO
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquBnsNonGoduto().getLquBnsNonGodutoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-BNS-NON-GODUTO
            ws.getIndLiq().setBnsNonGoduto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-BNS-NON-GODUTO
            ws.getIndLiq().setBnsNonGoduto(((short)0));
        }
        // COB_CODE: IF LQU-CNBT-INPSTFM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-CNBT-INPSTFM
        //           ELSE
        //              MOVE 0 TO IND-LQU-CNBT-INPSTFM
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquCnbtInpstfm().getLquCnbtInpstfmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-CNBT-INPSTFM
            ws.getIndLiq().setCnbtInpstfm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-CNBT-INPSTFM
            ws.getIndLiq().setCnbtInpstfm(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-DA-RIMB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-DA-RIMB
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-DA-RIMB
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstDaRimb().getLquImpstDaRimbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-DA-RIMB
            ws.getIndLiq().setImpstDaRimb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-DA-RIMB
            ws.getIndLiq().setImpstDaRimb(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-IS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-IS
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-IS
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbIs().getLquImpbIsNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-IS
            ws.getIndLiq().setImpbIs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-IS
            ws.getIndLiq().setImpbIs(((short)0));
        }
        // COB_CODE: IF LQU-TAX-SEP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TAX-SEP
        //           ELSE
        //              MOVE 0 TO IND-LQU-TAX-SEP
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTaxSep().getLquTaxSepNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TAX-SEP
            ws.getIndLiq().setTaxSep(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TAX-SEP
            ws.getIndLiq().setTaxSep(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-TAX-SEP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-TAX-SEP
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-TAX-SEP
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbTaxSep().getLquImpbTaxSepNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-TAX-SEP
            ws.getIndLiq().setImpbTaxSep(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-TAX-SEP
            ws.getIndLiq().setImpbTaxSep(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-INTR-SU-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-INTR-SU-PREST
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-INTR-SU-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbIntrSuPrest().getLquImpbIntrSuPrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-INTR-SU-PREST
            ws.getIndLiq().setImpbIntrSuPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-INTR-SU-PREST
            ws.getIndLiq().setImpbIntrSuPrest(((short)0));
        }
        // COB_CODE: IF LQU-ADDIZ-COMUN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-ADDIZ-COMUN
        //           ELSE
        //              MOVE 0 TO IND-LQU-ADDIZ-COMUN
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquAddizComun().getLquAddizComunNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-ADDIZ-COMUN
            ws.getIndLiq().setAddizComun(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-ADDIZ-COMUN
            ws.getIndLiq().setAddizComun(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-ADDIZ-COMUN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-ADDIZ-COMUN
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-ADDIZ-COMUN
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbAddizComun().getLquImpbAddizComunNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-ADDIZ-COMUN
            ws.getIndLiq().setImpbAddizComun(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-ADDIZ-COMUN
            ws.getIndLiq().setImpbAddizComun(((short)0));
        }
        // COB_CODE: IF LQU-ADDIZ-REGION-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-ADDIZ-REGION
        //           ELSE
        //              MOVE 0 TO IND-LQU-ADDIZ-REGION
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquAddizRegion().getLquAddizRegionNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-ADDIZ-REGION
            ws.getIndLiq().setAddizRegion(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-ADDIZ-REGION
            ws.getIndLiq().setAddizRegion(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-ADDIZ-REGION-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-ADDIZ-REGION
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-ADDIZ-REGION
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbAddizRegion().getLquImpbAddizRegionNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-ADDIZ-REGION
            ws.getIndLiq().setImpbAddizRegion(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-ADDIZ-REGION
            ws.getIndLiq().setImpbAddizRegion(((short)0));
        }
        // COB_CODE: IF LQU-MONT-DAL2007-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-MONT-DAL2007
        //           ELSE
        //              MOVE 0 TO IND-LQU-MONT-DAL2007
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquMontDal2007().getLquMontDal2007NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-MONT-DAL2007
            ws.getIndLiq().setMontDal2007(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-MONT-DAL2007
            ws.getIndLiq().setMontDal2007(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-CNBT-INPSTFM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-CNBT-INPSTFM
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-CNBT-INPSTFM
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbCnbtInpstfm().getLquImpbCnbtInpstfmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-CNBT-INPSTFM
            ws.getIndLiq().setImpbCnbtInpstfm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-CNBT-INPSTFM
            ws.getIndLiq().setImpbCnbtInpstfm(((short)0));
        }
        // COB_CODE: IF LQU-IMP-LRD-DA-RIMB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-LRD-DA-RIMB
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-LRD-DA-RIMB
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpLrdDaRimb().getLquImpLrdDaRimbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-LRD-DA-RIMB
            ws.getIndLiq().setImpLrdDaRimb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-LRD-DA-RIMB
            ws.getIndLiq().setImpLrdDaRimb(((short)0));
        }
        // COB_CODE: IF LQU-IMP-DIR-DA-RIMB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-DIR-DA-RIMB
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-DIR-DA-RIMB
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpDirDaRimb().getLquImpDirDaRimbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-DIR-DA-RIMB
            ws.getIndLiq().setImpDirDaRimb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-DIR-DA-RIMB
            ws.getIndLiq().setImpDirDaRimb(((short)0));
        }
        // COB_CODE: IF LQU-RIS-MAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-RIS-MAT
        //           ELSE
        //              MOVE 0 TO IND-LQU-RIS-MAT
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquRisMat().getLquRisMatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-RIS-MAT
            ws.getIndLiq().setRisMat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-RIS-MAT
            ws.getIndLiq().setRisMat(((short)0));
        }
        // COB_CODE: IF LQU-RIS-SPE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-RIS-SPE
        //           ELSE
        //              MOVE 0 TO IND-LQU-RIS-SPE
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquRisSpe().getLquRisSpeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-RIS-SPE
            ws.getIndLiq().setRisSpe(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-RIS-SPE
            ws.getIndLiq().setRisSpe(((short)0));
        }
        // COB_CODE: IF LQU-TOT-IAS-PNL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TOT-IAS-PNL
        //           ELSE
        //              MOVE 0 TO IND-LQU-TOT-IAS-PNL
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTotIasPnl().getLquTotIasPnlNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TOT-IAS-PNL
            ws.getIndLiq().setTotIasPnl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TOT-IAS-PNL
            ws.getIndLiq().setTotIasPnl(((short)0));
        }
        // COB_CODE: IF LQU-FL-EVE-GARTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-FL-EVE-GARTO
        //           ELSE
        //              MOVE 0 TO IND-LQU-FL-EVE-GARTO
        //           END-IF
        if (Conditions.eq(liq.getLquFlEveGarto(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-LQU-FL-EVE-GARTO
            ws.getIndLiq().setFlEveGarto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-FL-EVE-GARTO
            ws.getIndLiq().setFlEveGarto(((short)0));
        }
        // COB_CODE: IF LQU-IMP-REN-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-REN-K1
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-REN-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpRenK1().getLquImpRenK1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-REN-K1
            ws.getIndLiq().setImpRenK1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-REN-K1
            ws.getIndLiq().setImpRenK1(((short)0));
        }
        // COB_CODE: IF LQU-IMP-REN-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-REN-K2
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-REN-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpRenK2().getLquImpRenK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-REN-K2
            ws.getIndLiq().setImpRenK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-REN-K2
            ws.getIndLiq().setImpRenK2(((short)0));
        }
        // COB_CODE: IF LQU-IMP-REN-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-REN-K3
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-REN-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpRenK3().getLquImpRenK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-REN-K3
            ws.getIndLiq().setImpRenK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-REN-K3
            ws.getIndLiq().setImpRenK3(((short)0));
        }
        // COB_CODE: IF LQU-PC-REN-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-PC-REN-K1
        //           ELSE
        //              MOVE 0 TO IND-LQU-PC-REN-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquPcRenK1().getLquPcRenK1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-PC-REN-K1
            ws.getIndLiq().setPcRenK1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-PC-REN-K1
            ws.getIndLiq().setPcRenK1(((short)0));
        }
        // COB_CODE: IF LQU-PC-REN-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-PC-REN-K2
        //           ELSE
        //              MOVE 0 TO IND-LQU-PC-REN-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquPcRenK2().getLquPcRenK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-PC-REN-K2
            ws.getIndLiq().setPcRenK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-PC-REN-K2
            ws.getIndLiq().setPcRenK2(((short)0));
        }
        // COB_CODE: IF LQU-PC-REN-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-PC-REN-K3
        //           ELSE
        //              MOVE 0 TO IND-LQU-PC-REN-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquPcRenK3().getLquPcRenK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-PC-REN-K3
            ws.getIndLiq().setPcRenK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-PC-REN-K3
            ws.getIndLiq().setPcRenK3(((short)0));
        }
        // COB_CODE: IF LQU-TP-CAUS-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-TP-CAUS-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-LQU-TP-CAUS-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquTpCausAnticFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-TP-CAUS-ANTIC
            ws.getIndLiq().setTpCausAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-TP-CAUS-ANTIC
            ws.getIndLiq().setTpCausAntic(((short)0));
        }
        // COB_CODE: IF LQU-IMP-LRD-LIQTO-RILT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-LRD-LIQTO-RILT
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-LRD-LIQTO-RILT
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpLrdLiqtoRilt().getLquImpLrdLiqtoRiltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-LRD-LIQTO-RILT
            ws.getIndLiq().setImpLrdLiqtoRilt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-LRD-LIQTO-RILT
            ws.getIndLiq().setImpLrdLiqtoRilt(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-APPL-RILT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-APPL-RILT
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-APPL-RILT
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstApplRilt().getLquImpstApplRiltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-APPL-RILT
            ws.getIndLiq().setImpstApplRilt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-APPL-RILT
            ws.getIndLiq().setImpstApplRilt(((short)0));
        }
        // COB_CODE: IF LQU-PC-RISC-PARZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-PC-RISC-PARZ
        //           ELSE
        //              MOVE 0 TO IND-LQU-PC-RISC-PARZ
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquPcRiscParz().getLquPcRiscParzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-PC-RISC-PARZ
            ws.getIndLiq().setPcRiscParz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-PC-RISC-PARZ
            ws.getIndLiq().setPcRiscParz(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-BOLLO-TOT-V-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-BOLLO-TOT-V
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-BOLLO-TOT-V
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstBolloTotV().getLquImpstBolloTotVNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-BOLLO-TOT-V
            ws.getIndLiq().setImpstBolloTotV(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-BOLLO-TOT-V
            ws.getIndLiq().setImpstBolloTotV(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-BOLLO-DETT-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-BOLLO-DETT-C
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-BOLLO-DETT-C
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstBolloDettC().getLquImpstBolloDettCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-BOLLO-DETT-C
            ws.getIndLiq().setImpstBolloDettC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-BOLLO-DETT-C
            ws.getIndLiq().setImpstBolloDettC(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-BOLLO-TOT-SW-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-BOLLO-TOT-SW
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-BOLLO-TOT-SW
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstBolloTotSw().getLquImpstBolloTotSwNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-BOLLO-TOT-SW
            ws.getIndLiq().setImpstBolloTotSw(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-BOLLO-TOT-SW
            ws.getIndLiq().setImpstBolloTotSw(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-BOLLO-TOT-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-BOLLO-TOT-AA
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-BOLLO-TOT-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstBolloTotAa().getLquImpstBolloTotAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-BOLLO-TOT-AA
            ws.getIndLiq().setImpstBolloTotAa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-BOLLO-TOT-AA
            ws.getIndLiq().setImpstBolloTotAa(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-VIS-1382011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-VIS-1382011
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-VIS-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbVis1382011().getLquImpbVis1382011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-VIS-1382011
            ws.getIndLiq().setImpbVis1382011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-VIS-1382011
            ws.getIndLiq().setImpbVis1382011(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-VIS-1382011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-VIS-1382011
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-VIS-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstVis1382011().getLquImpstVis1382011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-VIS-1382011
            ws.getIndLiq().setImpstVis1382011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-VIS-1382011
            ws.getIndLiq().setImpstVis1382011(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-IS-1382011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-IS-1382011
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-IS-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbIs1382011().getLquImpbIs1382011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-IS-1382011
            ws.getIndLiq().setImpbIs1382011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-IS-1382011
            ws.getIndLiq().setImpbIs1382011(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-SOST-1382011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-SOST-1382011
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-SOST-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstSost1382011().getLquImpstSost1382011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-SOST-1382011
            ws.getIndLiq().setImpstSost1382011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-SOST-1382011
            ws.getIndLiq().setImpstSost1382011(((short)0));
        }
        // COB_CODE: IF LQU-PC-ABB-TIT-STAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-PC-ABB-TIT-STAT
        //           ELSE
        //              MOVE 0 TO IND-LQU-PC-ABB-TIT-STAT
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquPcAbbTitStat().getLquPcAbbTitStatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-PC-ABB-TIT-STAT
            ws.getIndLiq().setPcAbbTitStat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-PC-ABB-TIT-STAT
            ws.getIndLiq().setPcAbbTitStat(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-BOLLO-DETT-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-BOLLO-DETT-C
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-BOLLO-DETT-C
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbBolloDettC().getLquImpbBolloDettCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-BOLLO-DETT-C
            ws.getIndLiq().setImpbBolloDettC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-BOLLO-DETT-C
            ws.getIndLiq().setImpbBolloDettC(((short)0));
        }
        // COB_CODE: IF LQU-FL-PRE-COMP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-FL-PRE-COMP
        //           ELSE
        //              MOVE 0 TO IND-LQU-FL-PRE-COMP
        //           END-IF
        if (Conditions.eq(liq.getLquFlPreComp(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-LQU-FL-PRE-COMP
            ws.getIndLiq().setFlPreComp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-FL-PRE-COMP
            ws.getIndLiq().setFlPreComp(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-VIS-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-VIS-662014
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-VIS-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbVis662014().getLquImpbVis662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-VIS-662014
            ws.getIndLiq().setImpbVis662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-VIS-662014
            ws.getIndLiq().setImpbVis662014(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-VIS-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-VIS-662014
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-VIS-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstVis662014().getLquImpstVis662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-VIS-662014
            ws.getIndLiq().setImpstVis662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-VIS-662014
            ws.getIndLiq().setImpstVis662014(((short)0));
        }
        // COB_CODE: IF LQU-IMPB-IS-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPB-IS-662014
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPB-IS-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpbIs662014().getLquImpbIs662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPB-IS-662014
            ws.getIndLiq().setImpbIs662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPB-IS-662014
            ws.getIndLiq().setImpbIs662014(((short)0));
        }
        // COB_CODE: IF LQU-IMPST-SOST-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMPST-SOST-662014
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMPST-SOST-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpstSost662014().getLquImpstSost662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMPST-SOST-662014
            ws.getIndLiq().setImpstSost662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMPST-SOST-662014
            ws.getIndLiq().setImpstSost662014(((short)0));
        }
        // COB_CODE: IF LQU-PC-ABB-TS-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-PC-ABB-TS-662014
        //           ELSE
        //              MOVE 0 TO IND-LQU-PC-ABB-TS-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquPcAbbTs662014().getLquPcAbbTs662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-PC-ABB-TS-662014
            ws.getIndLiq().setPcAbbTs662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-PC-ABB-TS-662014
            ws.getIndLiq().setPcAbbTs662014(((short)0));
        }
        // COB_CODE: IF LQU-IMP-LRD-CALC-CP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-IMP-LRD-CALC-CP
        //           ELSE
        //              MOVE 0 TO IND-LQU-IMP-LRD-CALC-CP
        //           END-IF
        if (Characters.EQ_HIGH.test(liq.getLquImpLrdCalcCp().getLquImpLrdCalcCpNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-IMP-LRD-CALC-CP
            ws.getIndLiq().setImpLrdCalcCp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-IMP-LRD-CALC-CP
            ws.getIndLiq().setImpLrdCalcCp(((short)0));
        }
        // COB_CODE: IF LQU-COS-TUNNEL-USCITA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LQU-COS-TUNNEL-USCITA
        //           ELSE
        //              MOVE 0 TO IND-LQU-COS-TUNNEL-USCITA
        //           END-IF.
        if (Characters.EQ_HIGH.test(liq.getLquCosTunnelUscita().getLquCosTunnelUscitaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LQU-COS-TUNNEL-USCITA
            ws.getIndLiq().setCosTunnelUscita(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LQU-COS-TUNNEL-USCITA
            ws.getIndLiq().setCosTunnelUscita(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : LQU-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE LIQ TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(liq.getLiqFormatted());
        // COB_CODE: MOVE LQU-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(liq.getLquIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO LQU-ID-MOVI-CHIU
                liq.getLquIdMoviChiu().setLquIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO LQU-DS-TS-END-CPTZ
                liq.setLquDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO LQU-ID-MOVI-CRZ
                    liq.setLquIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO LQU-ID-MOVI-CHIU-NULL
                    liq.getLquIdMoviChiu().setLquIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquIdMoviChiu.Len.LQU_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO LQU-DT-END-EFF
                    liq.setLquDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO LQU-DS-TS-INI-CPTZ
                    liq.setLquDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO LQU-DS-TS-END-CPTZ
                    liq.setLquDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE LIQ TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(liq.getLiqFormatted());
        // COB_CODE: MOVE LQU-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(liq.getLquIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO LIQ.
        liq.setLiqFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO LQU-ID-MOVI-CRZ.
        liq.setLquIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO LQU-ID-MOVI-CHIU-NULL.
        liq.getLquIdMoviChiu().setLquIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquIdMoviChiu.Len.LQU_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO LQU-DT-INI-EFF.
        liq.setLquDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO LQU-DT-END-EFF.
        liq.setLquDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO LQU-DS-TS-INI-CPTZ.
        liq.setLquDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO LQU-DS-TS-END-CPTZ.
        liq.setLquDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO LQU-COD-COMP-ANIA.
        liq.setLquCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE LQU-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(liq.getLquDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO LQU-DT-INI-EFF-DB
        ws.getLiqDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE LQU-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(liq.getLquDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO LQU-DT-END-EFF-DB
        ws.getLiqDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-LQU-DT-MOR = 0
        //               MOVE WS-DATE-X      TO LQU-DT-MOR-DB
        //           END-IF
        if (ws.getIndLiq().getDtMor() == 0) {
            // COB_CODE: MOVE LQU-DT-MOR TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(liq.getLquDtMor().getLquDtMor(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO LQU-DT-MOR-DB
            ws.getLiqDb().setDecorDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-LQU-DT-DEN = 0
        //               MOVE WS-DATE-X      TO LQU-DT-DEN-DB
        //           END-IF
        if (ws.getIndLiq().getDtDen() == 0) {
            // COB_CODE: MOVE LQU-DT-DEN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(liq.getLquDtDen().getLquDtDen(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO LQU-DT-DEN-DB
            ws.getLiqDb().setScadDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-LQU-DT-PERV-DEN = 0
        //               MOVE WS-DATE-X      TO LQU-DT-PERV-DEN-DB
        //           END-IF
        if (ws.getIndLiq().getDtPervDen() == 0) {
            // COB_CODE: MOVE LQU-DT-PERV-DEN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(liq.getLquDtPervDen().getLquDtPervDen(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO LQU-DT-PERV-DEN-DB
            ws.getLiqDb().setEmisDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-LQU-DT-RICH = 0
        //               MOVE WS-DATE-X      TO LQU-DT-RICH-DB
        //           END-IF
        if (ws.getIndLiq().getDtRich() == 0) {
            // COB_CODE: MOVE LQU-DT-RICH TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(liq.getLquDtRich().getLquDtRich(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO LQU-DT-RICH-DB
            ws.getLiqDb().setEffStabDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-LQU-DT-LIQ = 0
        //               MOVE WS-DATE-X      TO LQU-DT-LIQ-DB
        //           END-IF
        if (ws.getIndLiq().getDtLiq() == 0) {
            // COB_CODE: MOVE LQU-DT-LIQ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(liq.getLquDtLiq().getLquDtLiq(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO LQU-DT-LIQ-DB
            ws.getLiqDb().setVldtProdDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-LQU-DT-VLT = 0
        //               MOVE WS-DATE-X      TO LQU-DT-VLT-DB
        //           END-IF
        if (ws.getIndLiq().getDtVlt() == 0) {
            // COB_CODE: MOVE LQU-DT-VLT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(liq.getLquDtVlt().getLquDtVlt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO LQU-DT-VLT-DB
            ws.getLiqDb().setIniValTarDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-LQU-DT-END-ISTR = 0
        //               MOVE WS-DATE-X      TO LQU-DT-END-ISTR-DB
        //           END-IF.
        if (ws.getIndLiq().getDtEndIstr() == 0) {
            // COB_CODE: MOVE LQU-DT-END-ISTR TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(liq.getLquDtEndIstr().getLquDtEndIstr(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO LQU-DT-END-ISTR-DB
            ws.getLiqDb().setUltAdegPrePrDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE LQU-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getLiqDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-INI-EFF
        liq.setLquDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE LQU-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getLiqDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-END-EFF
        liq.setLquDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-LQU-DT-MOR = 0
        //               MOVE WS-DATE-N      TO LQU-DT-MOR
        //           END-IF
        if (ws.getIndLiq().getDtMor() == 0) {
            // COB_CODE: MOVE LQU-DT-MOR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getDecorDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-MOR
            liq.getLquDtMor().setLquDtMor(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-DEN = 0
        //               MOVE WS-DATE-N      TO LQU-DT-DEN
        //           END-IF
        if (ws.getIndLiq().getDtDen() == 0) {
            // COB_CODE: MOVE LQU-DT-DEN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getScadDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-DEN
            liq.getLquDtDen().setLquDtDen(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-PERV-DEN = 0
        //               MOVE WS-DATE-N      TO LQU-DT-PERV-DEN
        //           END-IF
        if (ws.getIndLiq().getDtPervDen() == 0) {
            // COB_CODE: MOVE LQU-DT-PERV-DEN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getEmisDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-PERV-DEN
            liq.getLquDtPervDen().setLquDtPervDen(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-RICH = 0
        //               MOVE WS-DATE-N      TO LQU-DT-RICH
        //           END-IF
        if (ws.getIndLiq().getDtRich() == 0) {
            // COB_CODE: MOVE LQU-DT-RICH-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getEffStabDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-RICH
            liq.getLquDtRich().setLquDtRich(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-LIQ = 0
        //               MOVE WS-DATE-N      TO LQU-DT-LIQ
        //           END-IF
        if (ws.getIndLiq().getDtLiq() == 0) {
            // COB_CODE: MOVE LQU-DT-LIQ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getVldtProdDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-LIQ
            liq.getLquDtLiq().setLquDtLiq(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-VLT = 0
        //               MOVE WS-DATE-N      TO LQU-DT-VLT
        //           END-IF
        if (ws.getIndLiq().getDtVlt() == 0) {
            // COB_CODE: MOVE LQU-DT-VLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getIniValTarDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-VLT
            liq.getLquDtVlt().setLquDtVlt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-END-ISTR = 0
        //               MOVE WS-DATE-N      TO LQU-DT-END-ISTR
        //           END-IF.
        if (ws.getIndLiq().getDtEndIstr() == 0) {
            // COB_CODE: MOVE LQU-DT-END-ISTR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getUltAdegPrePrDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-END-ISTR
            liq.getLquDtEndIstr().setLquDtEndIstr(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF LQU-DESC-CAU-EVE-SIN
        //                       TO LQU-DESC-CAU-EVE-SIN-LEN.
        liq.setLquDescCauEveSinLen(((short)LiqIdbslqu0.Len.LQU_DESC_CAU_EVE_SIN));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getAddizComun() {
        return liq.getLquAddizComun().getLquAddizComun();
    }

    @Override
    public void setAddizComun(AfDecimal addizComun) {
        this.liq.getLquAddizComun().setLquAddizComun(addizComun.copy());
    }

    @Override
    public AfDecimal getAddizComunObj() {
        if (ws.getIndLiq().getAddizComun() >= 0) {
            return getAddizComun();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAddizComunObj(AfDecimal addizComunObj) {
        if (addizComunObj != null) {
            setAddizComun(new AfDecimal(addizComunObj, 15, 3));
            ws.getIndLiq().setAddizComun(((short)0));
        }
        else {
            ws.getIndLiq().setAddizComun(((short)-1));
        }
    }

    @Override
    public AfDecimal getAddizRegion() {
        return liq.getLquAddizRegion().getLquAddizRegion();
    }

    @Override
    public void setAddizRegion(AfDecimal addizRegion) {
        this.liq.getLquAddizRegion().setLquAddizRegion(addizRegion.copy());
    }

    @Override
    public AfDecimal getAddizRegionObj() {
        if (ws.getIndLiq().getAddizRegion() >= 0) {
            return getAddizRegion();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAddizRegionObj(AfDecimal addizRegionObj) {
        if (addizRegionObj != null) {
            setAddizRegion(new AfDecimal(addizRegionObj, 15, 3));
            ws.getIndLiq().setAddizRegion(((short)0));
        }
        else {
            ws.getIndLiq().setAddizRegion(((short)-1));
        }
    }

    @Override
    public AfDecimal getBnsNonGoduto() {
        return liq.getLquBnsNonGoduto().getLquBnsNonGoduto();
    }

    @Override
    public void setBnsNonGoduto(AfDecimal bnsNonGoduto) {
        this.liq.getLquBnsNonGoduto().setLquBnsNonGoduto(bnsNonGoduto.copy());
    }

    @Override
    public AfDecimal getBnsNonGodutoObj() {
        if (ws.getIndLiq().getBnsNonGoduto() >= 0) {
            return getBnsNonGoduto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setBnsNonGodutoObj(AfDecimal bnsNonGodutoObj) {
        if (bnsNonGodutoObj != null) {
            setBnsNonGoduto(new AfDecimal(bnsNonGodutoObj, 15, 3));
            ws.getIndLiq().setBnsNonGoduto(((short)0));
        }
        else {
            ws.getIndLiq().setBnsNonGoduto(((short)-1));
        }
    }

    @Override
    public AfDecimal getCnbtInpstfm() {
        return liq.getLquCnbtInpstfm().getLquCnbtInpstfm();
    }

    @Override
    public void setCnbtInpstfm(AfDecimal cnbtInpstfm) {
        this.liq.getLquCnbtInpstfm().setLquCnbtInpstfm(cnbtInpstfm.copy());
    }

    @Override
    public AfDecimal getCnbtInpstfmObj() {
        if (ws.getIndLiq().getCnbtInpstfm() >= 0) {
            return getCnbtInpstfm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCnbtInpstfmObj(AfDecimal cnbtInpstfmObj) {
        if (cnbtInpstfmObj != null) {
            setCnbtInpstfm(new AfDecimal(cnbtInpstfmObj, 15, 3));
            ws.getIndLiq().setCnbtInpstfm(((short)0));
        }
        else {
            ws.getIndLiq().setCnbtInpstfm(((short)-1));
        }
    }

    @Override
    public String getCodCauSin() {
        return liq.getLquCodCauSin();
    }

    @Override
    public void setCodCauSin(String codCauSin) {
        this.liq.setLquCodCauSin(codCauSin);
    }

    @Override
    public String getCodCauSinObj() {
        if (ws.getIndLiq().getCodCauSin() >= 0) {
            return getCodCauSin();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodCauSinObj(String codCauSinObj) {
        if (codCauSinObj != null) {
            setCodCauSin(codCauSinObj);
            ws.getIndLiq().setCodCauSin(((short)0));
        }
        else {
            ws.getIndLiq().setCodCauSin(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return liq.getLquCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.liq.setLquCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return liq.getLquCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.liq.setLquCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndLiq().getCodDvs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndLiq().setCodDvs(((short)0));
        }
        else {
            ws.getIndLiq().setCodDvs(((short)-1));
        }
    }

    @Override
    public String getCodSinCatstrf() {
        return liq.getLquCodSinCatstrf();
    }

    @Override
    public void setCodSinCatstrf(String codSinCatstrf) {
        this.liq.setLquCodSinCatstrf(codSinCatstrf);
    }

    @Override
    public String getCodSinCatstrfObj() {
        if (ws.getIndLiq().getCodSinCatstrf() >= 0) {
            return getCodSinCatstrf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodSinCatstrfObj(String codSinCatstrfObj) {
        if (codSinCatstrfObj != null) {
            setCodSinCatstrf(codSinCatstrfObj);
            ws.getIndLiq().setCodSinCatstrf(((short)0));
        }
        else {
            ws.getIndLiq().setCodSinCatstrf(((short)-1));
        }
    }

    @Override
    public AfDecimal getComponTaxRimb() {
        return liq.getLquComponTaxRimb().getLquComponTaxRimb();
    }

    @Override
    public void setComponTaxRimb(AfDecimal componTaxRimb) {
        this.liq.getLquComponTaxRimb().setLquComponTaxRimb(componTaxRimb.copy());
    }

    @Override
    public AfDecimal getComponTaxRimbObj() {
        if (ws.getIndLiq().getComponTaxRimb() >= 0) {
            return getComponTaxRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setComponTaxRimbObj(AfDecimal componTaxRimbObj) {
        if (componTaxRimbObj != null) {
            setComponTaxRimb(new AfDecimal(componTaxRimbObj, 15, 3));
            ws.getIndLiq().setComponTaxRimb(((short)0));
        }
        else {
            ws.getIndLiq().setComponTaxRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getCosTunnelUscita() {
        return liq.getLquCosTunnelUscita().getLquCosTunnelUscita();
    }

    @Override
    public void setCosTunnelUscita(AfDecimal cosTunnelUscita) {
        this.liq.getLquCosTunnelUscita().setLquCosTunnelUscita(cosTunnelUscita.copy());
    }

    @Override
    public AfDecimal getCosTunnelUscitaObj() {
        if (ws.getIndLiq().getCosTunnelUscita() >= 0) {
            return getCosTunnelUscita();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCosTunnelUscitaObj(AfDecimal cosTunnelUscitaObj) {
        if (cosTunnelUscitaObj != null) {
            setCosTunnelUscita(new AfDecimal(cosTunnelUscitaObj, 15, 3));
            ws.getIndLiq().setCosTunnelUscita(((short)0));
        }
        else {
            ws.getIndLiq().setCosTunnelUscita(((short)-1));
        }
    }

    @Override
    public String getDescCauEveSinVchar() {
        return liq.getLquDescCauEveSinVcharFormatted();
    }

    @Override
    public void setDescCauEveSinVchar(String descCauEveSinVchar) {
        this.liq.setLquDescCauEveSinVcharFormatted(descCauEveSinVchar);
    }

    @Override
    public String getDescCauEveSinVcharObj() {
        if (ws.getIndLiq().getDescCauEveSin() >= 0) {
            return getDescCauEveSinVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescCauEveSinVcharObj(String descCauEveSinVcharObj) {
        if (descCauEveSinVcharObj != null) {
            setDescCauEveSinVchar(descCauEveSinVcharObj);
            ws.getIndLiq().setDescCauEveSin(((short)0));
        }
        else {
            ws.getIndLiq().setDescCauEveSin(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return liq.getLquDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.liq.setLquDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return liq.getLquDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.liq.setLquDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return liq.getLquDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.liq.setLquDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return liq.getLquDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.liq.setLquDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return liq.getLquDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.liq.setLquDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return liq.getLquDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.liq.setLquDsVer(dsVer);
    }

    @Override
    public String getDtDenDb() {
        return ws.getLiqDb().getScadDb();
    }

    @Override
    public void setDtDenDb(String dtDenDb) {
        this.ws.getLiqDb().setScadDb(dtDenDb);
    }

    @Override
    public String getDtDenDbObj() {
        if (ws.getIndLiq().getDtDen() >= 0) {
            return getDtDenDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDenDbObj(String dtDenDbObj) {
        if (dtDenDbObj != null) {
            setDtDenDb(dtDenDbObj);
            ws.getIndLiq().setDtDen(((short)0));
        }
        else {
            ws.getIndLiq().setDtDen(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getLiqDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getLiqDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtEndIstrDb() {
        return ws.getLiqDb().getUltAdegPrePrDb();
    }

    @Override
    public void setDtEndIstrDb(String dtEndIstrDb) {
        this.ws.getLiqDb().setUltAdegPrePrDb(dtEndIstrDb);
    }

    @Override
    public String getDtEndIstrDbObj() {
        if (ws.getIndLiq().getDtEndIstr() >= 0) {
            return getDtEndIstrDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndIstrDbObj(String dtEndIstrDbObj) {
        if (dtEndIstrDbObj != null) {
            setDtEndIstrDb(dtEndIstrDbObj);
            ws.getIndLiq().setDtEndIstr(((short)0));
        }
        else {
            ws.getIndLiq().setDtEndIstr(((short)-1));
        }
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getLiqDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getLiqDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtLiqDb() {
        return ws.getLiqDb().getVldtProdDb();
    }

    @Override
    public void setDtLiqDb(String dtLiqDb) {
        this.ws.getLiqDb().setVldtProdDb(dtLiqDb);
    }

    @Override
    public String getDtLiqDbObj() {
        if (ws.getIndLiq().getDtLiq() >= 0) {
            return getDtLiqDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtLiqDbObj(String dtLiqDbObj) {
        if (dtLiqDbObj != null) {
            setDtLiqDb(dtLiqDbObj);
            ws.getIndLiq().setDtLiq(((short)0));
        }
        else {
            ws.getIndLiq().setDtLiq(((short)-1));
        }
    }

    @Override
    public String getDtMorDb() {
        return ws.getLiqDb().getDecorDb();
    }

    @Override
    public void setDtMorDb(String dtMorDb) {
        this.ws.getLiqDb().setDecorDb(dtMorDb);
    }

    @Override
    public String getDtMorDbObj() {
        if (ws.getIndLiq().getDtMor() >= 0) {
            return getDtMorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtMorDbObj(String dtMorDbObj) {
        if (dtMorDbObj != null) {
            setDtMorDb(dtMorDbObj);
            ws.getIndLiq().setDtMor(((short)0));
        }
        else {
            ws.getIndLiq().setDtMor(((short)-1));
        }
    }

    @Override
    public String getDtPervDenDb() {
        return ws.getLiqDb().getEmisDb();
    }

    @Override
    public void setDtPervDenDb(String dtPervDenDb) {
        this.ws.getLiqDb().setEmisDb(dtPervDenDb);
    }

    @Override
    public String getDtPervDenDbObj() {
        if (ws.getIndLiq().getDtPervDen() >= 0) {
            return getDtPervDenDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtPervDenDbObj(String dtPervDenDbObj) {
        if (dtPervDenDbObj != null) {
            setDtPervDenDb(dtPervDenDbObj);
            ws.getIndLiq().setDtPervDen(((short)0));
        }
        else {
            ws.getIndLiq().setDtPervDen(((short)-1));
        }
    }

    @Override
    public String getDtRichDb() {
        return ws.getLiqDb().getEffStabDb();
    }

    @Override
    public void setDtRichDb(String dtRichDb) {
        this.ws.getLiqDb().setEffStabDb(dtRichDb);
    }

    @Override
    public String getDtRichDbObj() {
        if (ws.getIndLiq().getDtRich() >= 0) {
            return getDtRichDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRichDbObj(String dtRichDbObj) {
        if (dtRichDbObj != null) {
            setDtRichDb(dtRichDbObj);
            ws.getIndLiq().setDtRich(((short)0));
        }
        else {
            ws.getIndLiq().setDtRich(((short)-1));
        }
    }

    @Override
    public String getDtVltDb() {
        return ws.getLiqDb().getIniValTarDb();
    }

    @Override
    public void setDtVltDb(String dtVltDb) {
        this.ws.getLiqDb().setIniValTarDb(dtVltDb);
    }

    @Override
    public String getDtVltDbObj() {
        if (ws.getIndLiq().getDtVlt() >= 0) {
            return getDtVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtVltDbObj(String dtVltDbObj) {
        if (dtVltDbObj != null) {
            setDtVltDb(dtVltDbObj);
            ws.getIndLiq().setDtVlt(((short)0));
        }
        else {
            ws.getIndLiq().setDtVlt(((short)-1));
        }
    }

    @Override
    public char getFlEveGarto() {
        return liq.getLquFlEveGarto();
    }

    @Override
    public void setFlEveGarto(char flEveGarto) {
        this.liq.setLquFlEveGarto(flEveGarto);
    }

    @Override
    public Character getFlEveGartoObj() {
        if (ws.getIndLiq().getFlEveGarto() >= 0) {
            return ((Character)getFlEveGarto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlEveGartoObj(Character flEveGartoObj) {
        if (flEveGartoObj != null) {
            setFlEveGarto(((char)flEveGartoObj));
            ws.getIndLiq().setFlEveGarto(((short)0));
        }
        else {
            ws.getIndLiq().setFlEveGarto(((short)-1));
        }
    }

    @Override
    public char getFlPreComp() {
        return liq.getLquFlPreComp();
    }

    @Override
    public void setFlPreComp(char flPreComp) {
        this.liq.setLquFlPreComp(flPreComp);
    }

    @Override
    public Character getFlPreCompObj() {
        if (ws.getIndLiq().getFlPreComp() >= 0) {
            return ((Character)getFlPreComp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPreCompObj(Character flPreCompObj) {
        if (flPreCompObj != null) {
            setFlPreComp(((char)flPreCompObj));
            ws.getIndLiq().setFlPreComp(((short)0));
        }
        else {
            ws.getIndLiq().setFlPreComp(((short)-1));
        }
    }

    @Override
    public String getIbLiq() {
        return liq.getLquIbLiq();
    }

    @Override
    public void setIbLiq(String ibLiq) {
        this.liq.setLquIbLiq(ibLiq);
    }

    @Override
    public String getIbLiqObj() {
        if (ws.getIndLiq().getIbLiq() >= 0) {
            return getIbLiq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbLiqObj(String ibLiqObj) {
        if (ibLiqObj != null) {
            setIbLiq(ibLiqObj);
            ws.getIndLiq().setIbLiq(((short)0));
        }
        else {
            ws.getIndLiq().setIbLiq(((short)-1));
        }
    }

    @Override
    public String getIbOgg() {
        return liq.getLquIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.liq.setLquIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndLiq().getIbOgg() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndLiq().setIbOgg(((short)0));
        }
        else {
            ws.getIndLiq().setIbOgg(((short)-1));
        }
    }

    @Override
    public int getIdLiq() {
        return liq.getLquIdLiq();
    }

    @Override
    public void setIdLiq(int idLiq) {
        this.liq.setLquIdLiq(idLiq);
    }

    @Override
    public int getIdMoviChiu() {
        return liq.getLquIdMoviChiu().getLquIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.liq.getLquIdMoviChiu().setLquIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndLiq().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndLiq().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndLiq().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpDirDaRimb() {
        return liq.getLquImpDirDaRimb().getLquImpDirDaRimb();
    }

    @Override
    public void setImpDirDaRimb(AfDecimal impDirDaRimb) {
        this.liq.getLquImpDirDaRimb().setLquImpDirDaRimb(impDirDaRimb.copy());
    }

    @Override
    public AfDecimal getImpDirDaRimbObj() {
        if (ws.getIndLiq().getImpDirDaRimb() >= 0) {
            return getImpDirDaRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpDirDaRimbObj(AfDecimal impDirDaRimbObj) {
        if (impDirDaRimbObj != null) {
            setImpDirDaRimb(new AfDecimal(impDirDaRimbObj, 15, 3));
            ws.getIndLiq().setImpDirDaRimb(((short)0));
        }
        else {
            ws.getIndLiq().setImpDirDaRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpDirLiq() {
        return liq.getLquImpDirLiq().getLquImpDirLiq();
    }

    @Override
    public void setImpDirLiq(AfDecimal impDirLiq) {
        this.liq.getLquImpDirLiq().setLquImpDirLiq(impDirLiq.copy());
    }

    @Override
    public AfDecimal getImpDirLiqObj() {
        if (ws.getIndLiq().getImpDirLiq() >= 0) {
            return getImpDirLiq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpDirLiqObj(AfDecimal impDirLiqObj) {
        if (impDirLiqObj != null) {
            setImpDirLiq(new AfDecimal(impDirLiqObj, 15, 3));
            ws.getIndLiq().setImpDirLiq(((short)0));
        }
        else {
            ws.getIndLiq().setImpDirLiq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpExcontr() {
        return liq.getLquImpExcontr().getLquImpExcontr();
    }

    @Override
    public void setImpExcontr(AfDecimal impExcontr) {
        this.liq.getLquImpExcontr().setLquImpExcontr(impExcontr.copy());
    }

    @Override
    public AfDecimal getImpExcontrObj() {
        if (ws.getIndLiq().getImpExcontr() >= 0) {
            return getImpExcontr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpExcontrObj(AfDecimal impExcontrObj) {
        if (impExcontrObj != null) {
            setImpExcontr(new AfDecimal(impExcontrObj, 15, 3));
            ws.getIndLiq().setImpExcontr(((short)0));
        }
        else {
            ws.getIndLiq().setImpExcontr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpIntrRitPag() {
        return liq.getLquImpIntrRitPag().getLquImpIntrRitPag();
    }

    @Override
    public void setImpIntrRitPag(AfDecimal impIntrRitPag) {
        this.liq.getLquImpIntrRitPag().setLquImpIntrRitPag(impIntrRitPag.copy());
    }

    @Override
    public AfDecimal getImpIntrRitPagObj() {
        if (ws.getIndLiq().getImpIntrRitPag() >= 0) {
            return getImpIntrRitPag();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpIntrRitPagObj(AfDecimal impIntrRitPagObj) {
        if (impIntrRitPagObj != null) {
            setImpIntrRitPag(new AfDecimal(impIntrRitPagObj, 15, 3));
            ws.getIndLiq().setImpIntrRitPag(((short)0));
        }
        else {
            ws.getIndLiq().setImpIntrRitPag(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdCalcCp() {
        return liq.getLquImpLrdCalcCp().getLquImpLrdCalcCp();
    }

    @Override
    public void setImpLrdCalcCp(AfDecimal impLrdCalcCp) {
        this.liq.getLquImpLrdCalcCp().setLquImpLrdCalcCp(impLrdCalcCp.copy());
    }

    @Override
    public AfDecimal getImpLrdCalcCpObj() {
        if (ws.getIndLiq().getImpLrdCalcCp() >= 0) {
            return getImpLrdCalcCp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdCalcCpObj(AfDecimal impLrdCalcCpObj) {
        if (impLrdCalcCpObj != null) {
            setImpLrdCalcCp(new AfDecimal(impLrdCalcCpObj, 15, 3));
            ws.getIndLiq().setImpLrdCalcCp(((short)0));
        }
        else {
            ws.getIndLiq().setImpLrdCalcCp(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdDaRimb() {
        return liq.getLquImpLrdDaRimb().getLquImpLrdDaRimb();
    }

    @Override
    public void setImpLrdDaRimb(AfDecimal impLrdDaRimb) {
        this.liq.getLquImpLrdDaRimb().setLquImpLrdDaRimb(impLrdDaRimb.copy());
    }

    @Override
    public AfDecimal getImpLrdDaRimbObj() {
        if (ws.getIndLiq().getImpLrdDaRimb() >= 0) {
            return getImpLrdDaRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdDaRimbObj(AfDecimal impLrdDaRimbObj) {
        if (impLrdDaRimbObj != null) {
            setImpLrdDaRimb(new AfDecimal(impLrdDaRimbObj, 15, 3));
            ws.getIndLiq().setImpLrdDaRimb(((short)0));
        }
        else {
            ws.getIndLiq().setImpLrdDaRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdLiqtoRilt() {
        return liq.getLquImpLrdLiqtoRilt().getLquImpLrdLiqtoRilt();
    }

    @Override
    public void setImpLrdLiqtoRilt(AfDecimal impLrdLiqtoRilt) {
        this.liq.getLquImpLrdLiqtoRilt().setLquImpLrdLiqtoRilt(impLrdLiqtoRilt.copy());
    }

    @Override
    public AfDecimal getImpLrdLiqtoRiltObj() {
        if (ws.getIndLiq().getImpLrdLiqtoRilt() >= 0) {
            return getImpLrdLiqtoRilt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdLiqtoRiltObj(AfDecimal impLrdLiqtoRiltObj) {
        if (impLrdLiqtoRiltObj != null) {
            setImpLrdLiqtoRilt(new AfDecimal(impLrdLiqtoRiltObj, 15, 3));
            ws.getIndLiq().setImpLrdLiqtoRilt(((short)0));
        }
        else {
            ws.getIndLiq().setImpLrdLiqtoRilt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpOnerLiq() {
        return liq.getLquImpOnerLiq().getLquImpOnerLiq();
    }

    @Override
    public void setImpOnerLiq(AfDecimal impOnerLiq) {
        this.liq.getLquImpOnerLiq().setLquImpOnerLiq(impOnerLiq.copy());
    }

    @Override
    public AfDecimal getImpOnerLiqObj() {
        if (ws.getIndLiq().getImpOnerLiq() >= 0) {
            return getImpOnerLiq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpOnerLiqObj(AfDecimal impOnerLiqObj) {
        if (impOnerLiqObj != null) {
            setImpOnerLiq(new AfDecimal(impOnerLiqObj, 15, 3));
            ws.getIndLiq().setImpOnerLiq(((short)0));
        }
        else {
            ws.getIndLiq().setImpOnerLiq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPnl() {
        return liq.getLquImpPnl().getLquImpPnl();
    }

    @Override
    public void setImpPnl(AfDecimal impPnl) {
        this.liq.getLquImpPnl().setLquImpPnl(impPnl.copy());
    }

    @Override
    public AfDecimal getImpPnlObj() {
        if (ws.getIndLiq().getImpPnl() >= 0) {
            return getImpPnl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPnlObj(AfDecimal impPnlObj) {
        if (impPnlObj != null) {
            setImpPnl(new AfDecimal(impPnlObj, 15, 3));
            ws.getIndLiq().setImpPnl(((short)0));
        }
        else {
            ws.getIndLiq().setImpPnl(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRenK1() {
        return liq.getLquImpRenK1().getLquImpRenK1();
    }

    @Override
    public void setImpRenK1(AfDecimal impRenK1) {
        this.liq.getLquImpRenK1().setLquImpRenK1(impRenK1.copy());
    }

    @Override
    public AfDecimal getImpRenK1Obj() {
        if (ws.getIndLiq().getImpRenK1() >= 0) {
            return getImpRenK1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRenK1Obj(AfDecimal impRenK1Obj) {
        if (impRenK1Obj != null) {
            setImpRenK1(new AfDecimal(impRenK1Obj, 15, 3));
            ws.getIndLiq().setImpRenK1(((short)0));
        }
        else {
            ws.getIndLiq().setImpRenK1(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRenK2() {
        return liq.getLquImpRenK2().getLquImpRenK2();
    }

    @Override
    public void setImpRenK2(AfDecimal impRenK2) {
        this.liq.getLquImpRenK2().setLquImpRenK2(impRenK2.copy());
    }

    @Override
    public AfDecimal getImpRenK2Obj() {
        if (ws.getIndLiq().getImpRenK2() >= 0) {
            return getImpRenK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRenK2Obj(AfDecimal impRenK2Obj) {
        if (impRenK2Obj != null) {
            setImpRenK2(new AfDecimal(impRenK2Obj, 15, 3));
            ws.getIndLiq().setImpRenK2(((short)0));
        }
        else {
            ws.getIndLiq().setImpRenK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRenK3() {
        return liq.getLquImpRenK3().getLquImpRenK3();
    }

    @Override
    public void setImpRenK3(AfDecimal impRenK3) {
        this.liq.getLquImpRenK3().setLquImpRenK3(impRenK3.copy());
    }

    @Override
    public AfDecimal getImpRenK3Obj() {
        if (ws.getIndLiq().getImpRenK3() >= 0) {
            return getImpRenK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRenK3Obj(AfDecimal impRenK3Obj) {
        if (impRenK3Obj != null) {
            setImpRenK3(new AfDecimal(impRenK3Obj, 15, 3));
            ws.getIndLiq().setImpRenK3(((short)0));
        }
        else {
            ws.getIndLiq().setImpRenK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbAddizComun() {
        return liq.getLquImpbAddizComun().getLquImpbAddizComun();
    }

    @Override
    public void setImpbAddizComun(AfDecimal impbAddizComun) {
        this.liq.getLquImpbAddizComun().setLquImpbAddizComun(impbAddizComun.copy());
    }

    @Override
    public AfDecimal getImpbAddizComunObj() {
        if (ws.getIndLiq().getImpbAddizComun() >= 0) {
            return getImpbAddizComun();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbAddizComunObj(AfDecimal impbAddizComunObj) {
        if (impbAddizComunObj != null) {
            setImpbAddizComun(new AfDecimal(impbAddizComunObj, 15, 3));
            ws.getIndLiq().setImpbAddizComun(((short)0));
        }
        else {
            ws.getIndLiq().setImpbAddizComun(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbAddizRegion() {
        return liq.getLquImpbAddizRegion().getLquImpbAddizRegion();
    }

    @Override
    public void setImpbAddizRegion(AfDecimal impbAddizRegion) {
        this.liq.getLquImpbAddizRegion().setLquImpbAddizRegion(impbAddizRegion.copy());
    }

    @Override
    public AfDecimal getImpbAddizRegionObj() {
        if (ws.getIndLiq().getImpbAddizRegion() >= 0) {
            return getImpbAddizRegion();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbAddizRegionObj(AfDecimal impbAddizRegionObj) {
        if (impbAddizRegionObj != null) {
            setImpbAddizRegion(new AfDecimal(impbAddizRegionObj, 15, 3));
            ws.getIndLiq().setImpbAddizRegion(((short)0));
        }
        else {
            ws.getIndLiq().setImpbAddizRegion(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbBolloDettC() {
        return liq.getLquImpbBolloDettC().getLquImpbBolloDettC();
    }

    @Override
    public void setImpbBolloDettC(AfDecimal impbBolloDettC) {
        this.liq.getLquImpbBolloDettC().setLquImpbBolloDettC(impbBolloDettC.copy());
    }

    @Override
    public AfDecimal getImpbBolloDettCObj() {
        if (ws.getIndLiq().getImpbBolloDettC() >= 0) {
            return getImpbBolloDettC();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbBolloDettCObj(AfDecimal impbBolloDettCObj) {
        if (impbBolloDettCObj != null) {
            setImpbBolloDettC(new AfDecimal(impbBolloDettCObj, 15, 3));
            ws.getIndLiq().setImpbBolloDettC(((short)0));
        }
        else {
            ws.getIndLiq().setImpbBolloDettC(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbCnbtInpstfm() {
        return liq.getLquImpbCnbtInpstfm().getLquImpbCnbtInpstfm();
    }

    @Override
    public void setImpbCnbtInpstfm(AfDecimal impbCnbtInpstfm) {
        this.liq.getLquImpbCnbtInpstfm().setLquImpbCnbtInpstfm(impbCnbtInpstfm.copy());
    }

    @Override
    public AfDecimal getImpbCnbtInpstfmObj() {
        if (ws.getIndLiq().getImpbCnbtInpstfm() >= 0) {
            return getImpbCnbtInpstfm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbCnbtInpstfmObj(AfDecimal impbCnbtInpstfmObj) {
        if (impbCnbtInpstfmObj != null) {
            setImpbCnbtInpstfm(new AfDecimal(impbCnbtInpstfmObj, 15, 3));
            ws.getIndLiq().setImpbCnbtInpstfm(((short)0));
        }
        else {
            ws.getIndLiq().setImpbCnbtInpstfm(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbImpst252() {
        return liq.getLquImpbImpst252().getLquImpbImpst252();
    }

    @Override
    public void setImpbImpst252(AfDecimal impbImpst252) {
        this.liq.getLquImpbImpst252().setLquImpbImpst252(impbImpst252.copy());
    }

    @Override
    public AfDecimal getImpbImpst252Obj() {
        if (ws.getIndLiq().getImpbImpst252() >= 0) {
            return getImpbImpst252();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbImpst252Obj(AfDecimal impbImpst252Obj) {
        if (impbImpst252Obj != null) {
            setImpbImpst252(new AfDecimal(impbImpst252Obj, 15, 3));
            ws.getIndLiq().setImpbImpst252(((short)0));
        }
        else {
            ws.getIndLiq().setImpbImpst252(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbImpstPrvr() {
        return liq.getLquImpbImpstPrvr().getLquImpbImpstPrvr();
    }

    @Override
    public void setImpbImpstPrvr(AfDecimal impbImpstPrvr) {
        this.liq.getLquImpbImpstPrvr().setLquImpbImpstPrvr(impbImpstPrvr.copy());
    }

    @Override
    public AfDecimal getImpbImpstPrvrObj() {
        if (ws.getIndLiq().getImpbImpstPrvr() >= 0) {
            return getImpbImpstPrvr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbImpstPrvrObj(AfDecimal impbImpstPrvrObj) {
        if (impbImpstPrvrObj != null) {
            setImpbImpstPrvr(new AfDecimal(impbImpstPrvrObj, 15, 3));
            ws.getIndLiq().setImpbImpstPrvr(((short)0));
        }
        else {
            ws.getIndLiq().setImpbImpstPrvr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIntrSuPrest() {
        return liq.getLquImpbIntrSuPrest().getLquImpbIntrSuPrest();
    }

    @Override
    public void setImpbIntrSuPrest(AfDecimal impbIntrSuPrest) {
        this.liq.getLquImpbIntrSuPrest().setLquImpbIntrSuPrest(impbIntrSuPrest.copy());
    }

    @Override
    public AfDecimal getImpbIntrSuPrestObj() {
        if (ws.getIndLiq().getImpbIntrSuPrest() >= 0) {
            return getImpbIntrSuPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIntrSuPrestObj(AfDecimal impbIntrSuPrestObj) {
        if (impbIntrSuPrestObj != null) {
            setImpbIntrSuPrest(new AfDecimal(impbIntrSuPrestObj, 15, 3));
            ws.getIndLiq().setImpbIntrSuPrest(((short)0));
        }
        else {
            ws.getIndLiq().setImpbIntrSuPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIrpef() {
        return liq.getLquImpbIrpef().getLquImpbIrpef();
    }

    @Override
    public void setImpbIrpef(AfDecimal impbIrpef) {
        this.liq.getLquImpbIrpef().setLquImpbIrpef(impbIrpef.copy());
    }

    @Override
    public AfDecimal getImpbIrpefObj() {
        if (ws.getIndLiq().getImpbIrpef() >= 0) {
            return getImpbIrpef();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIrpefObj(AfDecimal impbIrpefObj) {
        if (impbIrpefObj != null) {
            setImpbIrpef(new AfDecimal(impbIrpefObj, 15, 3));
            ws.getIndLiq().setImpbIrpef(((short)0));
        }
        else {
            ws.getIndLiq().setImpbIrpef(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs1382011() {
        return liq.getLquImpbIs1382011().getLquImpbIs1382011();
    }

    @Override
    public void setImpbIs1382011(AfDecimal impbIs1382011) {
        this.liq.getLquImpbIs1382011().setLquImpbIs1382011(impbIs1382011.copy());
    }

    @Override
    public AfDecimal getImpbIs1382011Obj() {
        if (ws.getIndLiq().getImpbIs1382011() >= 0) {
            return getImpbIs1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs1382011Obj(AfDecimal impbIs1382011Obj) {
        if (impbIs1382011Obj != null) {
            setImpbIs1382011(new AfDecimal(impbIs1382011Obj, 15, 3));
            ws.getIndLiq().setImpbIs1382011(((short)0));
        }
        else {
            ws.getIndLiq().setImpbIs1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs662014() {
        return liq.getLquImpbIs662014().getLquImpbIs662014();
    }

    @Override
    public void setImpbIs662014(AfDecimal impbIs662014) {
        this.liq.getLquImpbIs662014().setLquImpbIs662014(impbIs662014.copy());
    }

    @Override
    public AfDecimal getImpbIs662014Obj() {
        if (ws.getIndLiq().getImpbIs662014() >= 0) {
            return getImpbIs662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs662014Obj(AfDecimal impbIs662014Obj) {
        if (impbIs662014Obj != null) {
            setImpbIs662014(new AfDecimal(impbIs662014Obj, 15, 3));
            ws.getIndLiq().setImpbIs662014(((short)0));
        }
        else {
            ws.getIndLiq().setImpbIs662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs() {
        return liq.getLquImpbIs().getLquImpbIs();
    }

    @Override
    public void setImpbIs(AfDecimal impbIs) {
        this.liq.getLquImpbIs().setLquImpbIs(impbIs.copy());
    }

    @Override
    public AfDecimal getImpbIsObj() {
        if (ws.getIndLiq().getImpbIs() >= 0) {
            return getImpbIs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsObj(AfDecimal impbIsObj) {
        if (impbIsObj != null) {
            setImpbIs(new AfDecimal(impbIsObj, 15, 3));
            ws.getIndLiq().setImpbIs(((short)0));
        }
        else {
            ws.getIndLiq().setImpbIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbTaxSep() {
        return liq.getLquImpbTaxSep().getLquImpbTaxSep();
    }

    @Override
    public void setImpbTaxSep(AfDecimal impbTaxSep) {
        this.liq.getLquImpbTaxSep().setLquImpbTaxSep(impbTaxSep.copy());
    }

    @Override
    public AfDecimal getImpbTaxSepObj() {
        if (ws.getIndLiq().getImpbTaxSep() >= 0) {
            return getImpbTaxSep();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbTaxSepObj(AfDecimal impbTaxSepObj) {
        if (impbTaxSepObj != null) {
            setImpbTaxSep(new AfDecimal(impbTaxSepObj, 15, 3));
            ws.getIndLiq().setImpbTaxSep(((short)0));
        }
        else {
            ws.getIndLiq().setImpbTaxSep(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis1382011() {
        return liq.getLquImpbVis1382011().getLquImpbVis1382011();
    }

    @Override
    public void setImpbVis1382011(AfDecimal impbVis1382011) {
        this.liq.getLquImpbVis1382011().setLquImpbVis1382011(impbVis1382011.copy());
    }

    @Override
    public AfDecimal getImpbVis1382011Obj() {
        if (ws.getIndLiq().getImpbVis1382011() >= 0) {
            return getImpbVis1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis1382011Obj(AfDecimal impbVis1382011Obj) {
        if (impbVis1382011Obj != null) {
            setImpbVis1382011(new AfDecimal(impbVis1382011Obj, 15, 3));
            ws.getIndLiq().setImpbVis1382011(((short)0));
        }
        else {
            ws.getIndLiq().setImpbVis1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis662014() {
        return liq.getLquImpbVis662014().getLquImpbVis662014();
    }

    @Override
    public void setImpbVis662014(AfDecimal impbVis662014) {
        this.liq.getLquImpbVis662014().setLquImpbVis662014(impbVis662014.copy());
    }

    @Override
    public AfDecimal getImpbVis662014Obj() {
        if (ws.getIndLiq().getImpbVis662014() >= 0) {
            return getImpbVis662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis662014Obj(AfDecimal impbVis662014Obj) {
        if (impbVis662014Obj != null) {
            setImpbVis662014(new AfDecimal(impbVis662014Obj, 15, 3));
            ws.getIndLiq().setImpbVis662014(((short)0));
        }
        else {
            ws.getIndLiq().setImpbVis662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpst252() {
        return liq.getLquImpst252().getLquImpst252();
    }

    @Override
    public void setImpst252(AfDecimal impst252) {
        this.liq.getLquImpst252().setLquImpst252(impst252.copy());
    }

    @Override
    public AfDecimal getImpst252Obj() {
        if (ws.getIndLiq().getImpst252() >= 0) {
            return getImpst252();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpst252Obj(AfDecimal impst252Obj) {
        if (impst252Obj != null) {
            setImpst252(new AfDecimal(impst252Obj, 15, 3));
            ws.getIndLiq().setImpst252(((short)0));
        }
        else {
            ws.getIndLiq().setImpst252(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstApplRilt() {
        return liq.getLquImpstApplRilt().getLquImpstApplRilt();
    }

    @Override
    public void setImpstApplRilt(AfDecimal impstApplRilt) {
        this.liq.getLquImpstApplRilt().setLquImpstApplRilt(impstApplRilt.copy());
    }

    @Override
    public AfDecimal getImpstApplRiltObj() {
        if (ws.getIndLiq().getImpstApplRilt() >= 0) {
            return getImpstApplRilt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstApplRiltObj(AfDecimal impstApplRiltObj) {
        if (impstApplRiltObj != null) {
            setImpstApplRilt(new AfDecimal(impstApplRiltObj, 15, 3));
            ws.getIndLiq().setImpstApplRilt(((short)0));
        }
        else {
            ws.getIndLiq().setImpstApplRilt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloDettC() {
        return liq.getLquImpstBolloDettC().getLquImpstBolloDettC();
    }

    @Override
    public void setImpstBolloDettC(AfDecimal impstBolloDettC) {
        this.liq.getLquImpstBolloDettC().setLquImpstBolloDettC(impstBolloDettC.copy());
    }

    @Override
    public AfDecimal getImpstBolloDettCObj() {
        if (ws.getIndLiq().getImpstBolloDettC() >= 0) {
            return getImpstBolloDettC();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloDettCObj(AfDecimal impstBolloDettCObj) {
        if (impstBolloDettCObj != null) {
            setImpstBolloDettC(new AfDecimal(impstBolloDettCObj, 15, 3));
            ws.getIndLiq().setImpstBolloDettC(((short)0));
        }
        else {
            ws.getIndLiq().setImpstBolloDettC(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotAa() {
        return liq.getLquImpstBolloTotAa().getLquImpstBolloTotAa();
    }

    @Override
    public void setImpstBolloTotAa(AfDecimal impstBolloTotAa) {
        this.liq.getLquImpstBolloTotAa().setLquImpstBolloTotAa(impstBolloTotAa.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotAaObj() {
        if (ws.getIndLiq().getImpstBolloTotAa() >= 0) {
            return getImpstBolloTotAa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotAaObj(AfDecimal impstBolloTotAaObj) {
        if (impstBolloTotAaObj != null) {
            setImpstBolloTotAa(new AfDecimal(impstBolloTotAaObj, 15, 3));
            ws.getIndLiq().setImpstBolloTotAa(((short)0));
        }
        else {
            ws.getIndLiq().setImpstBolloTotAa(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotSw() {
        return liq.getLquImpstBolloTotSw().getLquImpstBolloTotSw();
    }

    @Override
    public void setImpstBolloTotSw(AfDecimal impstBolloTotSw) {
        this.liq.getLquImpstBolloTotSw().setLquImpstBolloTotSw(impstBolloTotSw.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotSwObj() {
        if (ws.getIndLiq().getImpstBolloTotSw() >= 0) {
            return getImpstBolloTotSw();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotSwObj(AfDecimal impstBolloTotSwObj) {
        if (impstBolloTotSwObj != null) {
            setImpstBolloTotSw(new AfDecimal(impstBolloTotSwObj, 15, 3));
            ws.getIndLiq().setImpstBolloTotSw(((short)0));
        }
        else {
            ws.getIndLiq().setImpstBolloTotSw(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotV() {
        return liq.getLquImpstBolloTotV().getLquImpstBolloTotV();
    }

    @Override
    public void setImpstBolloTotV(AfDecimal impstBolloTotV) {
        this.liq.getLquImpstBolloTotV().setLquImpstBolloTotV(impstBolloTotV.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotVObj() {
        if (ws.getIndLiq().getImpstBolloTotV() >= 0) {
            return getImpstBolloTotV();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotVObj(AfDecimal impstBolloTotVObj) {
        if (impstBolloTotVObj != null) {
            setImpstBolloTotV(new AfDecimal(impstBolloTotVObj, 15, 3));
            ws.getIndLiq().setImpstBolloTotV(((short)0));
        }
        else {
            ws.getIndLiq().setImpstBolloTotV(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstDaRimb() {
        return liq.getLquImpstDaRimb().getLquImpstDaRimb();
    }

    @Override
    public void setImpstDaRimb(AfDecimal impstDaRimb) {
        this.liq.getLquImpstDaRimb().setLquImpstDaRimb(impstDaRimb.copy());
    }

    @Override
    public AfDecimal getImpstDaRimbObj() {
        if (ws.getIndLiq().getImpstDaRimb() >= 0) {
            return getImpstDaRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstDaRimbObj(AfDecimal impstDaRimbObj) {
        if (impstDaRimbObj != null) {
            setImpstDaRimb(new AfDecimal(impstDaRimbObj, 15, 3));
            ws.getIndLiq().setImpstDaRimb(((short)0));
        }
        else {
            ws.getIndLiq().setImpstDaRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstIrpef() {
        return liq.getLquImpstIrpef().getLquImpstIrpef();
    }

    @Override
    public void setImpstIrpef(AfDecimal impstIrpef) {
        this.liq.getLquImpstIrpef().setLquImpstIrpef(impstIrpef.copy());
    }

    @Override
    public AfDecimal getImpstIrpefObj() {
        if (ws.getIndLiq().getImpstIrpef() >= 0) {
            return getImpstIrpef();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstIrpefObj(AfDecimal impstIrpefObj) {
        if (impstIrpefObj != null) {
            setImpstIrpef(new AfDecimal(impstIrpefObj, 15, 3));
            ws.getIndLiq().setImpstIrpef(((short)0));
        }
        else {
            ws.getIndLiq().setImpstIrpef(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstPrvr() {
        return liq.getLquImpstPrvr().getLquImpstPrvr();
    }

    @Override
    public void setImpstPrvr(AfDecimal impstPrvr) {
        this.liq.getLquImpstPrvr().setLquImpstPrvr(impstPrvr.copy());
    }

    @Override
    public AfDecimal getImpstPrvrObj() {
        if (ws.getIndLiq().getImpstPrvr() >= 0) {
            return getImpstPrvr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstPrvrObj(AfDecimal impstPrvrObj) {
        if (impstPrvrObj != null) {
            setImpstPrvr(new AfDecimal(impstPrvrObj, 15, 3));
            ws.getIndLiq().setImpstPrvr(((short)0));
        }
        else {
            ws.getIndLiq().setImpstPrvr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSost1382011() {
        return liq.getLquImpstSost1382011().getLquImpstSost1382011();
    }

    @Override
    public void setImpstSost1382011(AfDecimal impstSost1382011) {
        this.liq.getLquImpstSost1382011().setLquImpstSost1382011(impstSost1382011.copy());
    }

    @Override
    public AfDecimal getImpstSost1382011Obj() {
        if (ws.getIndLiq().getImpstSost1382011() >= 0) {
            return getImpstSost1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSost1382011Obj(AfDecimal impstSost1382011Obj) {
        if (impstSost1382011Obj != null) {
            setImpstSost1382011(new AfDecimal(impstSost1382011Obj, 15, 3));
            ws.getIndLiq().setImpstSost1382011(((short)0));
        }
        else {
            ws.getIndLiq().setImpstSost1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSost662014() {
        return liq.getLquImpstSost662014().getLquImpstSost662014();
    }

    @Override
    public void setImpstSost662014(AfDecimal impstSost662014) {
        this.liq.getLquImpstSost662014().setLquImpstSost662014(impstSost662014.copy());
    }

    @Override
    public AfDecimal getImpstSost662014Obj() {
        if (ws.getIndLiq().getImpstSost662014() >= 0) {
            return getImpstSost662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSost662014Obj(AfDecimal impstSost662014Obj) {
        if (impstSost662014Obj != null) {
            setImpstSost662014(new AfDecimal(impstSost662014Obj, 15, 3));
            ws.getIndLiq().setImpstSost662014(((short)0));
        }
        else {
            ws.getIndLiq().setImpstSost662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis1382011() {
        return liq.getLquImpstVis1382011().getLquImpstVis1382011();
    }

    @Override
    public void setImpstVis1382011(AfDecimal impstVis1382011) {
        this.liq.getLquImpstVis1382011().setLquImpstVis1382011(impstVis1382011.copy());
    }

    @Override
    public AfDecimal getImpstVis1382011Obj() {
        if (ws.getIndLiq().getImpstVis1382011() >= 0) {
            return getImpstVis1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis1382011Obj(AfDecimal impstVis1382011Obj) {
        if (impstVis1382011Obj != null) {
            setImpstVis1382011(new AfDecimal(impstVis1382011Obj, 15, 3));
            ws.getIndLiq().setImpstVis1382011(((short)0));
        }
        else {
            ws.getIndLiq().setImpstVis1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis662014() {
        return liq.getLquImpstVis662014().getLquImpstVis662014();
    }

    @Override
    public void setImpstVis662014(AfDecimal impstVis662014) {
        this.liq.getLquImpstVis662014().setLquImpstVis662014(impstVis662014.copy());
    }

    @Override
    public AfDecimal getImpstVis662014Obj() {
        if (ws.getIndLiq().getImpstVis662014() >= 0) {
            return getImpstVis662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis662014Obj(AfDecimal impstVis662014Obj) {
        if (impstVis662014Obj != null) {
            setImpstVis662014(new AfDecimal(impstVis662014Obj, 15, 3));
            ws.getIndLiq().setImpstVis662014(((short)0));
        }
        else {
            ws.getIndLiq().setImpstVis662014(((short)-1));
        }
    }

    @Override
    public int getLdbv2271IdOgg() {
        throw new FieldNotMappedException("ldbv2271IdOgg");
    }

    @Override
    public void setLdbv2271IdOgg(int ldbv2271IdOgg) {
        throw new FieldNotMappedException("ldbv2271IdOgg");
    }

    @Override
    public AfDecimal getLdbv2271ImpLrdLiqto() {
        throw new FieldNotMappedException("ldbv2271ImpLrdLiqto");
    }

    @Override
    public void setLdbv2271ImpLrdLiqto(AfDecimal ldbv2271ImpLrdLiqto) {
        throw new FieldNotMappedException("ldbv2271ImpLrdLiqto");
    }

    @Override
    public AfDecimal getLdbv2271ImpLrdLiqtoObj() {
        return getLdbv2271ImpLrdLiqto();
    }

    @Override
    public void setLdbv2271ImpLrdLiqtoObj(AfDecimal ldbv2271ImpLrdLiqtoObj) {
        setLdbv2271ImpLrdLiqto(new AfDecimal(ldbv2271ImpLrdLiqtoObj, 15, 3));
    }

    @Override
    public String getLdbv2271TpLiq() {
        throw new FieldNotMappedException("ldbv2271TpLiq");
    }

    @Override
    public void setLdbv2271TpLiq(String ldbv2271TpLiq) {
        throw new FieldNotMappedException("ldbv2271TpLiq");
    }

    @Override
    public String getLdbv2271TpOgg() {
        throw new FieldNotMappedException("ldbv2271TpOgg");
    }

    @Override
    public void setLdbv2271TpOgg(String ldbv2271TpOgg) {
        throw new FieldNotMappedException("ldbv2271TpOgg");
    }

    @Override
    public long getLquDsRiga() {
        return liq.getLquDsRiga();
    }

    @Override
    public void setLquDsRiga(long lquDsRiga) {
        this.liq.setLquDsRiga(lquDsRiga);
    }

    @Override
    public int getLquIdMoviCrz() {
        return liq.getLquIdMoviCrz();
    }

    @Override
    public void setLquIdMoviCrz(int lquIdMoviCrz) {
        this.liq.setLquIdMoviCrz(lquIdMoviCrz);
    }

    @Override
    public int getLquIdOgg() {
        return liq.getLquIdOgg();
    }

    @Override
    public void setLquIdOgg(int lquIdOgg) {
        this.liq.setLquIdOgg(lquIdOgg);
    }

    @Override
    public String getLquTpOgg() {
        return liq.getLquTpOgg();
    }

    @Override
    public void setLquTpOgg(String lquTpOgg) {
        this.liq.setLquTpOgg(lquTpOgg);
    }

    @Override
    public AfDecimal getMontDal2007() {
        return liq.getLquMontDal2007().getLquMontDal2007();
    }

    @Override
    public void setMontDal2007(AfDecimal montDal2007) {
        this.liq.getLquMontDal2007().setLquMontDal2007(montDal2007.copy());
    }

    @Override
    public AfDecimal getMontDal2007Obj() {
        if (ws.getIndLiq().getMontDal2007() >= 0) {
            return getMontDal2007();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontDal2007Obj(AfDecimal montDal2007Obj) {
        if (montDal2007Obj != null) {
            setMontDal2007(new AfDecimal(montDal2007Obj, 15, 3));
            ws.getIndLiq().setMontDal2007(((short)0));
        }
        else {
            ws.getIndLiq().setMontDal2007(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontEnd2000() {
        return liq.getLquMontEnd2000().getLquMontEnd2000();
    }

    @Override
    public void setMontEnd2000(AfDecimal montEnd2000) {
        this.liq.getLquMontEnd2000().setLquMontEnd2000(montEnd2000.copy());
    }

    @Override
    public AfDecimal getMontEnd2000Obj() {
        if (ws.getIndLiq().getMontEnd2000() >= 0) {
            return getMontEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontEnd2000Obj(AfDecimal montEnd2000Obj) {
        if (montEnd2000Obj != null) {
            setMontEnd2000(new AfDecimal(montEnd2000Obj, 15, 3));
            ws.getIndLiq().setMontEnd2000(((short)0));
        }
        else {
            ws.getIndLiq().setMontEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontEnd2006() {
        return liq.getLquMontEnd2006().getLquMontEnd2006();
    }

    @Override
    public void setMontEnd2006(AfDecimal montEnd2006) {
        this.liq.getLquMontEnd2006().setLquMontEnd2006(montEnd2006.copy());
    }

    @Override
    public AfDecimal getMontEnd2006Obj() {
        if (ws.getIndLiq().getMontEnd2006() >= 0) {
            return getMontEnd2006();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontEnd2006Obj(AfDecimal montEnd2006Obj) {
        if (montEnd2006Obj != null) {
            setMontEnd2006(new AfDecimal(montEnd2006Obj, 15, 3));
            ws.getIndLiq().setMontEnd2006(((short)0));
        }
        else {
            ws.getIndLiq().setMontEnd2006(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcAbbTitStat() {
        return liq.getLquPcAbbTitStat().getLquPcAbbTitStat();
    }

    @Override
    public void setPcAbbTitStat(AfDecimal pcAbbTitStat) {
        this.liq.getLquPcAbbTitStat().setLquPcAbbTitStat(pcAbbTitStat.copy());
    }

    @Override
    public AfDecimal getPcAbbTitStatObj() {
        if (ws.getIndLiq().getPcAbbTitStat() >= 0) {
            return getPcAbbTitStat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcAbbTitStatObj(AfDecimal pcAbbTitStatObj) {
        if (pcAbbTitStatObj != null) {
            setPcAbbTitStat(new AfDecimal(pcAbbTitStatObj, 6, 3));
            ws.getIndLiq().setPcAbbTitStat(((short)0));
        }
        else {
            ws.getIndLiq().setPcAbbTitStat(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcAbbTs662014() {
        return liq.getLquPcAbbTs662014().getLquPcAbbTs662014();
    }

    @Override
    public void setPcAbbTs662014(AfDecimal pcAbbTs662014) {
        this.liq.getLquPcAbbTs662014().setLquPcAbbTs662014(pcAbbTs662014.copy());
    }

    @Override
    public AfDecimal getPcAbbTs662014Obj() {
        if (ws.getIndLiq().getPcAbbTs662014() >= 0) {
            return getPcAbbTs662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcAbbTs662014Obj(AfDecimal pcAbbTs662014Obj) {
        if (pcAbbTs662014Obj != null) {
            setPcAbbTs662014(new AfDecimal(pcAbbTs662014Obj, 6, 3));
            ws.getIndLiq().setPcAbbTs662014(((short)0));
        }
        else {
            ws.getIndLiq().setPcAbbTs662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRen() {
        return liq.getLquPcRen().getLquPcRen();
    }

    @Override
    public void setPcRen(AfDecimal pcRen) {
        this.liq.getLquPcRen().setLquPcRen(pcRen.copy());
    }

    @Override
    public AfDecimal getPcRenK1() {
        return liq.getLquPcRenK1().getLquPcRenK1();
    }

    @Override
    public void setPcRenK1(AfDecimal pcRenK1) {
        this.liq.getLquPcRenK1().setLquPcRenK1(pcRenK1.copy());
    }

    @Override
    public AfDecimal getPcRenK1Obj() {
        if (ws.getIndLiq().getPcRenK1() >= 0) {
            return getPcRenK1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRenK1Obj(AfDecimal pcRenK1Obj) {
        if (pcRenK1Obj != null) {
            setPcRenK1(new AfDecimal(pcRenK1Obj, 6, 3));
            ws.getIndLiq().setPcRenK1(((short)0));
        }
        else {
            ws.getIndLiq().setPcRenK1(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRenK2() {
        return liq.getLquPcRenK2().getLquPcRenK2();
    }

    @Override
    public void setPcRenK2(AfDecimal pcRenK2) {
        this.liq.getLquPcRenK2().setLquPcRenK2(pcRenK2.copy());
    }

    @Override
    public AfDecimal getPcRenK2Obj() {
        if (ws.getIndLiq().getPcRenK2() >= 0) {
            return getPcRenK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRenK2Obj(AfDecimal pcRenK2Obj) {
        if (pcRenK2Obj != null) {
            setPcRenK2(new AfDecimal(pcRenK2Obj, 6, 3));
            ws.getIndLiq().setPcRenK2(((short)0));
        }
        else {
            ws.getIndLiq().setPcRenK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRenK3() {
        return liq.getLquPcRenK3().getLquPcRenK3();
    }

    @Override
    public void setPcRenK3(AfDecimal pcRenK3) {
        this.liq.getLquPcRenK3().setLquPcRenK3(pcRenK3.copy());
    }

    @Override
    public AfDecimal getPcRenK3Obj() {
        if (ws.getIndLiq().getPcRenK3() >= 0) {
            return getPcRenK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRenK3Obj(AfDecimal pcRenK3Obj) {
        if (pcRenK3Obj != null) {
            setPcRenK3(new AfDecimal(pcRenK3Obj, 6, 3));
            ws.getIndLiq().setPcRenK3(((short)0));
        }
        else {
            ws.getIndLiq().setPcRenK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRenObj() {
        if (ws.getIndLiq().getPcRen() >= 0) {
            return getPcRen();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRenObj(AfDecimal pcRenObj) {
        if (pcRenObj != null) {
            setPcRen(new AfDecimal(pcRenObj, 6, 3));
            ws.getIndLiq().setPcRen(((short)0));
        }
        else {
            ws.getIndLiq().setPcRen(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRiscParz() {
        return liq.getLquPcRiscParz().getLquPcRiscParz();
    }

    @Override
    public void setPcRiscParz(AfDecimal pcRiscParz) {
        this.liq.getLquPcRiscParz().setLquPcRiscParz(pcRiscParz.copy());
    }

    @Override
    public AfDecimal getPcRiscParzObj() {
        if (ws.getIndLiq().getPcRiscParz() >= 0) {
            return getPcRiscParz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRiscParzObj(AfDecimal pcRiscParzObj) {
        if (pcRiscParzObj != null) {
            setPcRiscParz(new AfDecimal(pcRiscParzObj, 12, 5));
            ws.getIndLiq().setPcRiscParz(((short)0));
        }
        else {
            ws.getIndLiq().setPcRiscParz(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMat() {
        return liq.getLquRisMat().getLquRisMat();
    }

    @Override
    public void setRisMat(AfDecimal risMat) {
        this.liq.getLquRisMat().setLquRisMat(risMat.copy());
    }

    @Override
    public AfDecimal getRisMatObj() {
        if (ws.getIndLiq().getRisMat() >= 0) {
            return getRisMat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatObj(AfDecimal risMatObj) {
        if (risMatObj != null) {
            setRisMat(new AfDecimal(risMatObj, 15, 3));
            ws.getIndLiq().setRisMat(((short)0));
        }
        else {
            ws.getIndLiq().setRisMat(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisSpe() {
        return liq.getLquRisSpe().getLquRisSpe();
    }

    @Override
    public void setRisSpe(AfDecimal risSpe) {
        this.liq.getLquRisSpe().setLquRisSpe(risSpe.copy());
    }

    @Override
    public AfDecimal getRisSpeObj() {
        if (ws.getIndLiq().getRisSpe() >= 0) {
            return getRisSpe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisSpeObj(AfDecimal risSpeObj) {
        if (risSpeObj != null) {
            setRisSpe(new AfDecimal(risSpeObj, 15, 3));
            ws.getIndLiq().setRisSpe(((short)0));
        }
        else {
            ws.getIndLiq().setRisSpe(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpeRcs() {
        return liq.getLquSpeRcs().getLquSpeRcs();
    }

    @Override
    public void setSpeRcs(AfDecimal speRcs) {
        this.liq.getLquSpeRcs().setLquSpeRcs(speRcs.copy());
    }

    @Override
    public AfDecimal getSpeRcsObj() {
        if (ws.getIndLiq().getSpeRcs() >= 0) {
            return getSpeRcs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpeRcsObj(AfDecimal speRcsObj) {
        if (speRcsObj != null) {
            setSpeRcs(new AfDecimal(speRcsObj, 15, 3));
            ws.getIndLiq().setSpeRcs(((short)0));
        }
        else {
            ws.getIndLiq().setSpeRcs(((short)-1));
        }
    }

    @Override
    public AfDecimal getTaxSep() {
        return liq.getLquTaxSep().getLquTaxSep();
    }

    @Override
    public void setTaxSep(AfDecimal taxSep) {
        this.liq.getLquTaxSep().setLquTaxSep(taxSep.copy());
    }

    @Override
    public AfDecimal getTaxSepObj() {
        if (ws.getIndLiq().getTaxSep() >= 0) {
            return getTaxSep();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTaxSepObj(AfDecimal taxSepObj) {
        if (taxSepObj != null) {
            setTaxSep(new AfDecimal(taxSepObj, 15, 3));
            ws.getIndLiq().setTaxSep(((short)0));
        }
        else {
            ws.getIndLiq().setTaxSep(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIasMggSin() {
        return liq.getLquTotIasMggSin().getLquTotIasMggSin();
    }

    @Override
    public void setTotIasMggSin(AfDecimal totIasMggSin) {
        this.liq.getLquTotIasMggSin().setLquTotIasMggSin(totIasMggSin.copy());
    }

    @Override
    public AfDecimal getTotIasMggSinObj() {
        if (ws.getIndLiq().getTotIasMggSin() >= 0) {
            return getTotIasMggSin();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIasMggSinObj(AfDecimal totIasMggSinObj) {
        if (totIasMggSinObj != null) {
            setTotIasMggSin(new AfDecimal(totIasMggSinObj, 15, 3));
            ws.getIndLiq().setTotIasMggSin(((short)0));
        }
        else {
            ws.getIndLiq().setTotIasMggSin(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIasOnerPrvnt() {
        return liq.getLquTotIasOnerPrvnt().getLquTotIasOnerPrvnt();
    }

    @Override
    public void setTotIasOnerPrvnt(AfDecimal totIasOnerPrvnt) {
        this.liq.getLquTotIasOnerPrvnt().setLquTotIasOnerPrvnt(totIasOnerPrvnt.copy());
    }

    @Override
    public AfDecimal getTotIasOnerPrvntObj() {
        if (ws.getIndLiq().getTotIasOnerPrvnt() >= 0) {
            return getTotIasOnerPrvnt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIasOnerPrvntObj(AfDecimal totIasOnerPrvntObj) {
        if (totIasOnerPrvntObj != null) {
            setTotIasOnerPrvnt(new AfDecimal(totIasOnerPrvntObj, 15, 3));
            ws.getIndLiq().setTotIasOnerPrvnt(((short)0));
        }
        else {
            ws.getIndLiq().setTotIasOnerPrvnt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIasPnl() {
        return liq.getLquTotIasPnl().getLquTotIasPnl();
    }

    @Override
    public void setTotIasPnl(AfDecimal totIasPnl) {
        this.liq.getLquTotIasPnl().setLquTotIasPnl(totIasPnl.copy());
    }

    @Override
    public AfDecimal getTotIasPnlObj() {
        if (ws.getIndLiq().getTotIasPnl() >= 0) {
            return getTotIasPnl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIasPnlObj(AfDecimal totIasPnlObj) {
        if (totIasPnlObj != null) {
            setTotIasPnl(new AfDecimal(totIasPnlObj, 15, 3));
            ws.getIndLiq().setTotIasPnl(((short)0));
        }
        else {
            ws.getIndLiq().setTotIasPnl(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIasRstDpst() {
        return liq.getLquTotIasRstDpst().getLquTotIasRstDpst();
    }

    @Override
    public void setTotIasRstDpst(AfDecimal totIasRstDpst) {
        this.liq.getLquTotIasRstDpst().setLquTotIasRstDpst(totIasRstDpst.copy());
    }

    @Override
    public AfDecimal getTotIasRstDpstObj() {
        if (ws.getIndLiq().getTotIasRstDpst() >= 0) {
            return getTotIasRstDpst();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIasRstDpstObj(AfDecimal totIasRstDpstObj) {
        if (totIasRstDpstObj != null) {
            setTotIasRstDpst(new AfDecimal(totIasRstDpstObj, 15, 3));
            ws.getIndLiq().setTotIasRstDpst(((short)0));
        }
        else {
            ws.getIndLiq().setTotIasRstDpst(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpIntrPrest() {
        return liq.getLquTotImpIntrPrest().getLquTotImpIntrPrest();
    }

    @Override
    public void setTotImpIntrPrest(AfDecimal totImpIntrPrest) {
        this.liq.getLquTotImpIntrPrest().setLquTotImpIntrPrest(totImpIntrPrest.copy());
    }

    @Override
    public AfDecimal getTotImpIntrPrestObj() {
        if (ws.getIndLiq().getTotImpIntrPrest() >= 0) {
            return getTotImpIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpIntrPrestObj(AfDecimal totImpIntrPrestObj) {
        if (totImpIntrPrestObj != null) {
            setTotImpIntrPrest(new AfDecimal(totImpIntrPrestObj, 15, 3));
            ws.getIndLiq().setTotImpIntrPrest(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpIntrPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpIs() {
        return liq.getLquTotImpIs().getLquTotImpIs();
    }

    @Override
    public void setTotImpIs(AfDecimal totImpIs) {
        this.liq.getLquTotImpIs().setLquTotImpIs(totImpIs.copy());
    }

    @Override
    public AfDecimal getTotImpIsObj() {
        if (ws.getIndLiq().getTotImpIs() >= 0) {
            return getTotImpIs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpIsObj(AfDecimal totImpIsObj) {
        if (totImpIsObj != null) {
            setTotImpIs(new AfDecimal(totImpIsObj, 15, 3));
            ws.getIndLiq().setTotImpIs(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpLrdLiqto() {
        return liq.getLquTotImpLrdLiqto().getLquTotImpLrdLiqto();
    }

    @Override
    public void setTotImpLrdLiqto(AfDecimal totImpLrdLiqto) {
        this.liq.getLquTotImpLrdLiqto().setLquTotImpLrdLiqto(totImpLrdLiqto.copy());
    }

    @Override
    public AfDecimal getTotImpLrdLiqtoObj() {
        if (ws.getIndLiq().getTotImpLrdLiqto() >= 0) {
            return getTotImpLrdLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpLrdLiqtoObj(AfDecimal totImpLrdLiqtoObj) {
        if (totImpLrdLiqtoObj != null) {
            setTotImpLrdLiqto(new AfDecimal(totImpLrdLiqtoObj, 15, 3));
            ws.getIndLiq().setTotImpLrdLiqto(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpLrdLiqto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpNetLiqto() {
        return liq.getLquTotImpNetLiqto().getLquTotImpNetLiqto();
    }

    @Override
    public void setTotImpNetLiqto(AfDecimal totImpNetLiqto) {
        this.liq.getLquTotImpNetLiqto().setLquTotImpNetLiqto(totImpNetLiqto.copy());
    }

    @Override
    public AfDecimal getTotImpNetLiqtoObj() {
        if (ws.getIndLiq().getTotImpNetLiqto() >= 0) {
            return getTotImpNetLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpNetLiqtoObj(AfDecimal totImpNetLiqtoObj) {
        if (totImpNetLiqtoObj != null) {
            setTotImpNetLiqto(new AfDecimal(totImpNetLiqtoObj, 15, 3));
            ws.getIndLiq().setTotImpNetLiqto(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpNetLiqto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpPrest() {
        return liq.getLquTotImpPrest().getLquTotImpPrest();
    }

    @Override
    public void setTotImpPrest(AfDecimal totImpPrest) {
        this.liq.getLquTotImpPrest().setLquTotImpPrest(totImpPrest.copy());
    }

    @Override
    public AfDecimal getTotImpPrestObj() {
        if (ws.getIndLiq().getTotImpPrest() >= 0) {
            return getTotImpPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpPrestObj(AfDecimal totImpPrestObj) {
        if (totImpPrestObj != null) {
            setTotImpPrest(new AfDecimal(totImpPrestObj, 15, 3));
            ws.getIndLiq().setTotImpPrest(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpRimb() {
        return liq.getLquTotImpRimb().getLquTotImpRimb();
    }

    @Override
    public void setTotImpRimb(AfDecimal totImpRimb) {
        this.liq.getLquTotImpRimb().setLquTotImpRimb(totImpRimb.copy());
    }

    @Override
    public AfDecimal getTotImpRimbObj() {
        if (ws.getIndLiq().getTotImpRimb() >= 0) {
            return getTotImpRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpRimbObj(AfDecimal totImpRimbObj) {
        if (totImpRimbObj != null) {
            setTotImpRimb(new AfDecimal(totImpRimbObj, 15, 3));
            ws.getIndLiq().setTotImpRimb(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpRitAcc() {
        return liq.getLquTotImpRitAcc().getLquTotImpRitAcc();
    }

    @Override
    public void setTotImpRitAcc(AfDecimal totImpRitAcc) {
        this.liq.getLquTotImpRitAcc().setLquTotImpRitAcc(totImpRitAcc.copy());
    }

    @Override
    public AfDecimal getTotImpRitAccObj() {
        if (ws.getIndLiq().getTotImpRitAcc() >= 0) {
            return getTotImpRitAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpRitAccObj(AfDecimal totImpRitAccObj) {
        if (totImpRitAccObj != null) {
            setTotImpRitAcc(new AfDecimal(totImpRitAccObj, 15, 3));
            ws.getIndLiq().setTotImpRitAcc(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpRitAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpRitTfr() {
        return liq.getLquTotImpRitTfr().getLquTotImpRitTfr();
    }

    @Override
    public void setTotImpRitTfr(AfDecimal totImpRitTfr) {
        this.liq.getLquTotImpRitTfr().setLquTotImpRitTfr(totImpRitTfr.copy());
    }

    @Override
    public AfDecimal getTotImpRitTfrObj() {
        if (ws.getIndLiq().getTotImpRitTfr() >= 0) {
            return getTotImpRitTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpRitTfrObj(AfDecimal totImpRitTfrObj) {
        if (totImpRitTfrObj != null) {
            setTotImpRitTfr(new AfDecimal(totImpRitTfrObj, 15, 3));
            ws.getIndLiq().setTotImpRitTfr(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpRitTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpRitVis() {
        return liq.getLquTotImpRitVis().getLquTotImpRitVis();
    }

    @Override
    public void setTotImpRitVis(AfDecimal totImpRitVis) {
        this.liq.getLquTotImpRitVis().setLquTotImpRitVis(totImpRitVis.copy());
    }

    @Override
    public AfDecimal getTotImpRitVisObj() {
        if (ws.getIndLiq().getTotImpRitVis() >= 0) {
            return getTotImpRitVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpRitVisObj(AfDecimal totImpRitVisObj) {
        if (totImpRitVisObj != null) {
            setTotImpRitVis(new AfDecimal(totImpRitVisObj, 15, 3));
            ws.getIndLiq().setTotImpRitVis(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpRitVis(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpUti() {
        return liq.getLquTotImpUti().getLquTotImpUti();
    }

    @Override
    public void setTotImpUti(AfDecimal totImpUti) {
        this.liq.getLquTotImpUti().setLquTotImpUti(totImpUti.copy());
    }

    @Override
    public AfDecimal getTotImpUtiObj() {
        if (ws.getIndLiq().getTotImpUti() >= 0) {
            return getTotImpUti();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpUtiObj(AfDecimal totImpUtiObj) {
        if (totImpUtiObj != null) {
            setTotImpUti(new AfDecimal(totImpUtiObj, 15, 3));
            ws.getIndLiq().setTotImpUti(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpUti(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpbAcc() {
        return liq.getLquTotImpbAcc().getLquTotImpbAcc();
    }

    @Override
    public void setTotImpbAcc(AfDecimal totImpbAcc) {
        this.liq.getLquTotImpbAcc().setLquTotImpbAcc(totImpbAcc.copy());
    }

    @Override
    public AfDecimal getTotImpbAccObj() {
        if (ws.getIndLiq().getTotImpbAcc() >= 0) {
            return getTotImpbAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpbAccObj(AfDecimal totImpbAccObj) {
        if (totImpbAccObj != null) {
            setTotImpbAcc(new AfDecimal(totImpbAccObj, 15, 3));
            ws.getIndLiq().setTotImpbAcc(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpbAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpbTfr() {
        return liq.getLquTotImpbTfr().getLquTotImpbTfr();
    }

    @Override
    public void setTotImpbTfr(AfDecimal totImpbTfr) {
        this.liq.getLquTotImpbTfr().setLquTotImpbTfr(totImpbTfr.copy());
    }

    @Override
    public AfDecimal getTotImpbTfrObj() {
        if (ws.getIndLiq().getTotImpbTfr() >= 0) {
            return getTotImpbTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpbTfrObj(AfDecimal totImpbTfrObj) {
        if (totImpbTfrObj != null) {
            setTotImpbTfr(new AfDecimal(totImpbTfrObj, 15, 3));
            ws.getIndLiq().setTotImpbTfr(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpbTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpbVis() {
        return liq.getLquTotImpbVis().getLquTotImpbVis();
    }

    @Override
    public void setTotImpbVis(AfDecimal totImpbVis) {
        this.liq.getLquTotImpbVis().setLquTotImpbVis(totImpbVis.copy());
    }

    @Override
    public AfDecimal getTotImpbVisObj() {
        if (ws.getIndLiq().getTotImpbVis() >= 0) {
            return getTotImpbVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpbVisObj(AfDecimal totImpbVisObj) {
        if (totImpbVisObj != null) {
            setTotImpbVis(new AfDecimal(totImpbVisObj, 15, 3));
            ws.getIndLiq().setTotImpbVis(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpbVis(((short)-1));
        }
    }

    @Override
    public String getTpCausAntic() {
        return liq.getLquTpCausAntic();
    }

    @Override
    public void setTpCausAntic(String tpCausAntic) {
        this.liq.setLquTpCausAntic(tpCausAntic);
    }

    @Override
    public String getTpCausAnticObj() {
        if (ws.getIndLiq().getTpCausAntic() >= 0) {
            return getTpCausAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCausAnticObj(String tpCausAnticObj) {
        if (tpCausAnticObj != null) {
            setTpCausAntic(tpCausAnticObj);
            ws.getIndLiq().setTpCausAntic(((short)0));
        }
        else {
            ws.getIndLiq().setTpCausAntic(((short)-1));
        }
    }

    @Override
    public String getTpLiq() {
        return liq.getLquTpLiq();
    }

    @Override
    public void setTpLiq(String tpLiq) {
        this.liq.setLquTpLiq(tpLiq);
    }

    @Override
    public short getTpMetRisc() {
        return liq.getLquTpMetRisc().getLquTpMetRisc();
    }

    @Override
    public void setTpMetRisc(short tpMetRisc) {
        this.liq.getLquTpMetRisc().setLquTpMetRisc(tpMetRisc);
    }

    @Override
    public Short getTpMetRiscObj() {
        if (ws.getIndLiq().getTpMetRisc() >= 0) {
            return ((Short)getTpMetRisc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMetRiscObj(Short tpMetRiscObj) {
        if (tpMetRiscObj != null) {
            setTpMetRisc(((short)tpMetRiscObj));
            ws.getIndLiq().setTpMetRisc(((short)0));
        }
        else {
            ws.getIndLiq().setTpMetRisc(((short)-1));
        }
    }

    @Override
    public String getTpMezPag() {
        return liq.getLquTpMezPag();
    }

    @Override
    public void setTpMezPag(String tpMezPag) {
        this.liq.setLquTpMezPag(tpMezPag);
    }

    @Override
    public String getTpMezPagObj() {
        if (ws.getIndLiq().getTpMezPag() >= 0) {
            return getTpMezPag();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMezPagObj(String tpMezPagObj) {
        if (tpMezPagObj != null) {
            setTpMezPag(tpMezPagObj);
            ws.getIndLiq().setTpMezPag(((short)0));
        }
        else {
            ws.getIndLiq().setTpMezPag(((short)-1));
        }
    }

    @Override
    public String getTpRimb() {
        return liq.getLquTpRimb();
    }

    @Override
    public void setTpRimb(String tpRimb) {
        this.liq.setLquTpRimb(tpRimb);
    }

    @Override
    public String getTpRisc() {
        return liq.getLquTpRisc();
    }

    @Override
    public void setTpRisc(String tpRisc) {
        this.liq.setLquTpRisc(tpRisc);
    }

    @Override
    public String getTpRiscObj() {
        if (ws.getIndLiq().getTpRisc() >= 0) {
            return getTpRisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRiscObj(String tpRiscObj) {
        if (tpRiscObj != null) {
            setTpRisc(tpRiscObj);
            ws.getIndLiq().setTpRisc(((short)0));
        }
        else {
            ws.getIndLiq().setTpRisc(((short)-1));
        }
    }

    @Override
    public String getTpSin() {
        return liq.getLquTpSin();
    }

    @Override
    public void setTpSin(String tpSin) {
        this.liq.setLquTpSin(tpSin);
    }

    @Override
    public String getTpSinObj() {
        if (ws.getIndLiq().getTpSin() >= 0) {
            return getTpSin();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpSinObj(String tpSinObj) {
        if (tpSinObj != null) {
            setTpSin(tpSinObj);
            ws.getIndLiq().setTpSin(((short)0));
        }
        else {
            ws.getIndLiq().setTpSin(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
