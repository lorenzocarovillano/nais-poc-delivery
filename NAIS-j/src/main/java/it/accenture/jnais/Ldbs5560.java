package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.PoliRappAnaDao;
import it.accenture.jnais.commons.data.to.IPoliRappAna;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs5560Data;
import it.accenture.jnais.ws.PoliIdbspol0;
import it.accenture.jnais.ws.redefines.PolAaDiffProrDflt;
import it.accenture.jnais.ws.redefines.PolDir1oVers;
import it.accenture.jnais.ws.redefines.PolDirEmis;
import it.accenture.jnais.ws.redefines.PolDirQuiet;
import it.accenture.jnais.ws.redefines.PolDirVersAgg;
import it.accenture.jnais.ws.redefines.PolDtApplzConv;
import it.accenture.jnais.ws.redefines.PolDtIniVldtConv;
import it.accenture.jnais.ws.redefines.PolDtPresc;
import it.accenture.jnais.ws.redefines.PolDtProp;
import it.accenture.jnais.ws.redefines.PolDtScad;
import it.accenture.jnais.ws.redefines.PolDurAa;
import it.accenture.jnais.ws.redefines.PolDurGg;
import it.accenture.jnais.ws.redefines.PolDurMm;
import it.accenture.jnais.ws.redefines.PolIdAccComm;
import it.accenture.jnais.ws.redefines.PolIdMoviChiu;
import it.accenture.jnais.ws.redefines.PolSpeMed;

/**Original name: LDBS5560<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  11 DIC 2017.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs5560 extends Program implements IPoliRappAna {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private PoliRappAnaDao poliRappAnaDao = new PoliRappAnaDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs5560Data ws = new Ldbs5560Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: POLI
    private PoliIdbspol0 poli;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS5560_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, PoliIdbspol0 poli) {
        this.idsv0003 = idsv0003;
        this.poli = poli;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV5561.
        ws.setLdbv5561Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: MOVE LDBV5561 TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getLdbv5561Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs5560 getInstance() {
        return ((Ldbs5560)Programs.getInstance(Ldbs5560.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS5560'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS5560");
        // COB_CODE: MOVE 'POLI' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("POLI");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     A.ID_POLI
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.IB_OGG
        //                    ,A.IB_PROP
        //                    ,A.DT_PROP
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.DT_DECOR
        //                    ,A.DT_EMIS
        //                    ,A.TP_POLI
        //                    ,A.DUR_AA
        //                    ,A.DUR_MM
        //                    ,A.DT_SCAD
        //                    ,A.COD_PROD
        //                    ,A.DT_INI_VLDT_PROD
        //                    ,A.COD_CONV
        //                    ,A.COD_RAMO
        //                    ,A.DT_INI_VLDT_CONV
        //                    ,A.DT_APPLZ_CONV
        //                    ,A.TP_FRM_ASSVA
        //                    ,A.TP_RGM_FISC
        //                    ,A.FL_ESTAS
        //                    ,A.FL_RSH_COMUN
        //                    ,A.FL_RSH_COMUN_COND
        //                    ,A.TP_LIV_GENZ_TIT
        //                    ,A.FL_COP_FINANZ
        //                    ,A.TP_APPLZ_DIR
        //                    ,A.SPE_MED
        //                    ,A.DIR_EMIS
        //                    ,A.DIR_1O_VERS
        //                    ,A.DIR_VERS_AGG
        //                    ,A.COD_DVS
        //                    ,A.FL_FNT_AZ
        //                    ,A.FL_FNT_ADER
        //                    ,A.FL_FNT_TFR
        //                    ,A.FL_FNT_VOLO
        //                    ,A.TP_OPZ_A_SCAD
        //                    ,A.AA_DIFF_PROR_DFLT
        //                    ,A.FL_VER_PROD
        //                    ,A.DUR_GG
        //                    ,A.DIR_QUIET
        //                    ,A.TP_PTF_ESTNO
        //                    ,A.FL_CUM_PRE_CNTR
        //                    ,A.FL_AMMB_MOVI
        //                    ,A.CONV_GECO
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.FL_SCUDO_FISC
        //                    ,A.FL_TRASFE
        //                    ,A.FL_TFR_STRC
        //                    ,A.DT_PRESC
        //                    ,A.COD_CONV_AGG
        //                    ,A.SUBCAT_PROD
        //                    ,A.FL_QUEST_ADEGZ_ASS
        //                    ,A.COD_TPA
        //                    ,A.ID_ACC_COMM
        //                    ,A.FL_POLI_CPI_PR
        //                    ,A.FL_POLI_BUNDLING
        //                    ,A.IND_POLI_PRIN_COLL
        //                    ,A.FL_VND_BUNDLE
        //                    ,A.IB_BS
        //                    ,A.FL_POLI_IFP
        //              FROM POLI A,
        //                   RAPP_ANA B
        //              WHERE   B.COD_SOGG   = :LDBV5561-COD-SOGG
        //                    AND B.ID_OGG       = A.ID_POLI
        //                    AND B.TP_OGG       = 'PO'
        //                    AND B.TP_RAPP_ANA  = 'CO'
        //                    AND A.TP_FRM_ASSVA = 'IN'
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_POLI
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.IB_OGG
        //                    ,A.IB_PROP
        //                    ,A.DT_PROP
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.DT_DECOR
        //                    ,A.DT_EMIS
        //                    ,A.TP_POLI
        //                    ,A.DUR_AA
        //                    ,A.DUR_MM
        //                    ,A.DT_SCAD
        //                    ,A.COD_PROD
        //                    ,A.DT_INI_VLDT_PROD
        //                    ,A.COD_CONV
        //                    ,A.COD_RAMO
        //                    ,A.DT_INI_VLDT_CONV
        //                    ,A.DT_APPLZ_CONV
        //                    ,A.TP_FRM_ASSVA
        //                    ,A.TP_RGM_FISC
        //                    ,A.FL_ESTAS
        //                    ,A.FL_RSH_COMUN
        //                    ,A.FL_RSH_COMUN_COND
        //                    ,A.TP_LIV_GENZ_TIT
        //                    ,A.FL_COP_FINANZ
        //                    ,A.TP_APPLZ_DIR
        //                    ,A.SPE_MED
        //                    ,A.DIR_EMIS
        //                    ,A.DIR_1O_VERS
        //                    ,A.DIR_VERS_AGG
        //                    ,A.COD_DVS
        //                    ,A.FL_FNT_AZ
        //                    ,A.FL_FNT_ADER
        //                    ,A.FL_FNT_TFR
        //                    ,A.FL_FNT_VOLO
        //                    ,A.TP_OPZ_A_SCAD
        //                    ,A.AA_DIFF_PROR_DFLT
        //                    ,A.FL_VER_PROD
        //                    ,A.DUR_GG
        //                    ,A.DIR_QUIET
        //                    ,A.TP_PTF_ESTNO
        //                    ,A.FL_CUM_PRE_CNTR
        //                    ,A.FL_AMMB_MOVI
        //                    ,A.CONV_GECO
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.FL_SCUDO_FISC
        //                    ,A.FL_TRASFE
        //                    ,A.FL_TFR_STRC
        //                    ,A.DT_PRESC
        //                    ,A.COD_CONV_AGG
        //                    ,A.SUBCAT_PROD
        //                    ,A.FL_QUEST_ADEGZ_ASS
        //                    ,A.COD_TPA
        //                    ,A.ID_ACC_COMM
        //                    ,A.FL_POLI_CPI_PR
        //                    ,A.FL_POLI_BUNDLING
        //                    ,A.IND_POLI_PRIN_COLL
        //                    ,A.FL_VND_BUNDLE
        //                    ,A.IB_BS
        //                    ,A.FL_POLI_IFP
        //             INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //             FROM POLI A,
        //                  RAPP_ANA B
        //             WHERE   B.COD_SOGG   = :LDBV5561-COD-SOGG
        //                    AND B.ID_OGG       = A.ID_POLI
        //                    AND B.TP_OGG       = 'PO'
        //                    AND B.TP_RAPP_ANA  = 'CO'
        //                    AND A.TP_FRM_ASSVA = 'IN'
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        poliRappAnaDao.selectRec(ws.getLdbv5561CodSogg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        poliRappAnaDao.openCEff28(ws.getLdbv5561CodSogg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        poliRappAnaDao.closeCEff28();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //           END-EXEC.
        poliRappAnaDao.fetchCEff28(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     A.ID_POLI
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.IB_OGG
        //                    ,A.IB_PROP
        //                    ,A.DT_PROP
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.DT_DECOR
        //                    ,A.DT_EMIS
        //                    ,A.TP_POLI
        //                    ,A.DUR_AA
        //                    ,A.DUR_MM
        //                    ,A.DT_SCAD
        //                    ,A.COD_PROD
        //                    ,A.DT_INI_VLDT_PROD
        //                    ,A.COD_CONV
        //                    ,A.COD_RAMO
        //                    ,A.DT_INI_VLDT_CONV
        //                    ,A.DT_APPLZ_CONV
        //                    ,A.TP_FRM_ASSVA
        //                    ,A.TP_RGM_FISC
        //                    ,A.FL_ESTAS
        //                    ,A.FL_RSH_COMUN
        //                    ,A.FL_RSH_COMUN_COND
        //                    ,A.TP_LIV_GENZ_TIT
        //                    ,A.FL_COP_FINANZ
        //                    ,A.TP_APPLZ_DIR
        //                    ,A.SPE_MED
        //                    ,A.DIR_EMIS
        //                    ,A.DIR_1O_VERS
        //                    ,A.DIR_VERS_AGG
        //                    ,A.COD_DVS
        //                    ,A.FL_FNT_AZ
        //                    ,A.FL_FNT_ADER
        //                    ,A.FL_FNT_TFR
        //                    ,A.FL_FNT_VOLO
        //                    ,A.TP_OPZ_A_SCAD
        //                    ,A.AA_DIFF_PROR_DFLT
        //                    ,A.FL_VER_PROD
        //                    ,A.DUR_GG
        //                    ,A.DIR_QUIET
        //                    ,A.TP_PTF_ESTNO
        //                    ,A.FL_CUM_PRE_CNTR
        //                    ,A.FL_AMMB_MOVI
        //                    ,A.CONV_GECO
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.FL_SCUDO_FISC
        //                    ,A.FL_TRASFE
        //                    ,A.FL_TFR_STRC
        //                    ,A.DT_PRESC
        //                    ,A.COD_CONV_AGG
        //                    ,A.SUBCAT_PROD
        //                    ,A.FL_QUEST_ADEGZ_ASS
        //                    ,A.COD_TPA
        //                    ,A.ID_ACC_COMM
        //                    ,A.FL_POLI_CPI_PR
        //                    ,A.FL_POLI_BUNDLING
        //                    ,A.IND_POLI_PRIN_COLL
        //                    ,A.FL_VND_BUNDLE
        //                    ,A.IB_BS
        //                    ,A.FL_POLI_IFP
        //              FROM POLI A,
        //                   RAPP_ANA B
        //              WHERE   B.COD_SOGG   = :LDBV5561-COD-SOGG
        //                        AND B.ID_OGG       = A.ID_POLI
        //                        AND B.TP_OGG       = 'PO'
        //                        AND B.TP_RAPP_ANA  = 'CO'
        //                        AND A.TP_FRM_ASSVA = 'IN'
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_POLI
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.IB_OGG
        //                    ,A.IB_PROP
        //                    ,A.DT_PROP
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.DT_DECOR
        //                    ,A.DT_EMIS
        //                    ,A.TP_POLI
        //                    ,A.DUR_AA
        //                    ,A.DUR_MM
        //                    ,A.DT_SCAD
        //                    ,A.COD_PROD
        //                    ,A.DT_INI_VLDT_PROD
        //                    ,A.COD_CONV
        //                    ,A.COD_RAMO
        //                    ,A.DT_INI_VLDT_CONV
        //                    ,A.DT_APPLZ_CONV
        //                    ,A.TP_FRM_ASSVA
        //                    ,A.TP_RGM_FISC
        //                    ,A.FL_ESTAS
        //                    ,A.FL_RSH_COMUN
        //                    ,A.FL_RSH_COMUN_COND
        //                    ,A.TP_LIV_GENZ_TIT
        //                    ,A.FL_COP_FINANZ
        //                    ,A.TP_APPLZ_DIR
        //                    ,A.SPE_MED
        //                    ,A.DIR_EMIS
        //                    ,A.DIR_1O_VERS
        //                    ,A.DIR_VERS_AGG
        //                    ,A.COD_DVS
        //                    ,A.FL_FNT_AZ
        //                    ,A.FL_FNT_ADER
        //                    ,A.FL_FNT_TFR
        //                    ,A.FL_FNT_VOLO
        //                    ,A.TP_OPZ_A_SCAD
        //                    ,A.AA_DIFF_PROR_DFLT
        //                    ,A.FL_VER_PROD
        //                    ,A.DUR_GG
        //                    ,A.DIR_QUIET
        //                    ,A.TP_PTF_ESTNO
        //                    ,A.FL_CUM_PRE_CNTR
        //                    ,A.FL_AMMB_MOVI
        //                    ,A.CONV_GECO
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.FL_SCUDO_FISC
        //                    ,A.FL_TRASFE
        //                    ,A.FL_TFR_STRC
        //                    ,A.DT_PRESC
        //                    ,A.COD_CONV_AGG
        //                    ,A.SUBCAT_PROD
        //                    ,A.FL_QUEST_ADEGZ_ASS
        //                    ,A.COD_TPA
        //                    ,A.ID_ACC_COMM
        //                    ,A.FL_POLI_CPI_PR
        //                    ,A.FL_POLI_BUNDLING
        //                    ,A.IND_POLI_PRIN_COLL
        //                    ,A.FL_VND_BUNDLE
        //                    ,A.IB_BS
        //                    ,A.FL_POLI_IFP
        //             INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //             FROM POLI A,
        //                  RAPP_ANA B
        //             WHERE   B.COD_SOGG   = :LDBV5561-COD-SOGG
        //                    AND B.ID_OGG       = A.ID_POLI
        //                    AND B.TP_OGG       = 'PO'
        //                    AND B.TP_RAPP_ANA  = 'CO'
        //                    AND A.TP_FRM_ASSVA = 'IN'
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        poliRappAnaDao.selectRec1(ws.getLdbv5561CodSogg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        poliRappAnaDao.openCCpz28(ws.getLdbv5561CodSogg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        poliRappAnaDao.closeCCpz28();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //           END-EXEC.
        poliRappAnaDao.fetchCCpz28(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicit`
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilit` comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-POL-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndPoli().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
            poli.getPolIdMoviChiu().setPolIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdMoviChiu.Len.POL_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-POL-IB-OGG = -1
        //              MOVE HIGH-VALUES TO POL-IB-OGG-NULL
        //           END-IF
        if (ws.getIndPoli().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IB-OGG-NULL
            poli.setPolIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_IB_OGG));
        }
        // COB_CODE: IF IND-POL-DT-PROP = -1
        //              MOVE HIGH-VALUES TO POL-DT-PROP-NULL
        //           END-IF
        if (ws.getIndPoli().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-PROP-NULL
            poli.getPolDtProp().setPolDtPropNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtProp.Len.POL_DT_PROP_NULL));
        }
        // COB_CODE: IF IND-POL-DUR-AA = -1
        //              MOVE HIGH-VALUES TO POL-DUR-AA-NULL
        //           END-IF
        if (ws.getIndPoli().getPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-AA-NULL
            poli.getPolDurAa().setPolDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurAa.Len.POL_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-POL-DUR-MM = -1
        //              MOVE HIGH-VALUES TO POL-DUR-MM-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-MM-NULL
            poli.getPolDurMm().setPolDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurMm.Len.POL_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-POL-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
            poli.getPolDtScad().setPolDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtScad.Len.POL_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-POL-COD-CONV = -1
        //              MOVE HIGH-VALUES TO POL-COD-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-CONV-NULL
            poli.setPolCodConv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_CONV));
        }
        // COB_CODE: IF IND-POL-COD-RAMO = -1
        //              MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
            poli.setPolCodRamo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_RAMO));
        }
        // COB_CODE: IF IND-POL-DT-INI-VLDT-CONV = -1
        //              MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
            poli.getPolDtIniVldtConv().setPolDtIniVldtConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtIniVldtConv.Len.POL_DT_INI_VLDT_CONV_NULL));
        }
        // COB_CODE: IF IND-POL-DT-APPLZ-CONV = -1
        //              MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
            poli.getPolDtApplzConv().setPolDtApplzConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtApplzConv.Len.POL_DT_APPLZ_CONV_NULL));
        }
        // COB_CODE: IF IND-POL-TP-RGM-FISC = -1
        //              MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
        //           END-IF
        if (ws.getIndPoli().getTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
            poli.setPolTpRgmFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_RGM_FISC));
        }
        // COB_CODE: IF IND-POL-FL-ESTAS = -1
        //              MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
            poli.setPolFlEstas(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-RSH-COMUN = -1
        //              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
            poli.setPolFlRshComun(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-RSH-COMUN-COND = -1
        //              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
            poli.setPolFlRshComunCond(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-COP-FINANZ = -1
        //              MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
            poli.setPolFlCopFinanz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-TP-APPLZ-DIR = -1
        //              MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
            poli.setPolTpApplzDir(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_APPLZ_DIR));
        }
        // COB_CODE: IF IND-POL-SPE-MED = -1
        //              MOVE HIGH-VALUES TO POL-SPE-MED-NULL
        //           END-IF
        if (ws.getIndPoli().getPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-SPE-MED-NULL
            poli.getPolSpeMed().setPolSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolSpeMed.Len.POL_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-EMIS = -1
        //              MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
        //           END-IF
        if (ws.getIndPoli().getPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
            poli.getPolDirEmis().setPolDirEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirEmis.Len.POL_DIR_EMIS_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-1O-VERS = -1
        //              MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
        //           END-IF
        if (ws.getIndPoli().getPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
            poli.getPolDir1oVers().setPolDir1oVersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDir1oVers.Len.POL_DIR1O_VERS_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-VERS-AGG = -1
        //              MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
        //           END-IF
        if (ws.getIndPoli().getCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
            poli.getPolDirVersAgg().setPolDirVersAggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirVersAgg.Len.POL_DIR_VERS_AGG_NULL));
        }
        // COB_CODE: IF IND-POL-COD-DVS = -1
        //              MOVE HIGH-VALUES TO POL-COD-DVS-NULL
        //           END-IF
        if (ws.getIndPoli().getCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-DVS-NULL
            poli.setPolCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_DVS));
        }
        // COB_CODE: IF IND-POL-FL-FNT-AZ = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
        //           END-IF
        if (ws.getIndPoli().getCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
            poli.setPolFlFntAz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-ADER = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
        //           END-IF
        if (ws.getIndPoli().getProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
            poli.setPolFlFntAder(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-TFR = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
        //           END-IF
        if (ws.getIndPoli().getProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
            poli.setPolFlFntTfr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-VOLO = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
        //           END-IF
        if (ws.getIndPoli().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
            poli.setPolFlFntVolo(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-TP-OPZ-A-SCAD = -1
        //              MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
        //           END-IF
        if (ws.getIndPoli().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
            poli.setPolTpOpzAScad(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_OPZ_A_SCAD));
        }
        // COB_CODE: IF IND-POL-AA-DIFF-PROR-DFLT = -1
        //              MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
        //           END-IF
        if (ws.getIndPoli().getProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
            poli.getPolAaDiffProrDflt().setPolAaDiffProrDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolAaDiffProrDflt.Len.POL_AA_DIFF_PROR_DFLT_NULL));
        }
        // COB_CODE: IF IND-POL-FL-VER-PROD = -1
        //              MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
        //           END-IF
        if (ws.getIndPoli().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
            poli.setPolFlVerProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_FL_VER_PROD));
        }
        // COB_CODE: IF IND-POL-DUR-GG = -1
        //              MOVE HIGH-VALUES TO POL-DUR-GG-NULL
        //           END-IF
        if (ws.getIndPoli().getFrqMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-GG-NULL
            poli.getPolDurGg().setPolDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurGg.Len.POL_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-QUIET = -1
        //              MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
        //           END-IF
        if (ws.getIndPoli().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
            poli.getPolDirQuiet().setPolDirQuietNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirQuiet.Len.POL_DIR_QUIET_NULL));
        }
        // COB_CODE: IF IND-POL-TP-PTF-ESTNO = -1
        //              MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
        //           END-IF
        if (ws.getIndPoli().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
            poli.setPolTpPtfEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_PTF_ESTNO));
        }
        // COB_CODE: IF IND-POL-FL-CUM-PRE-CNTR = -1
        //              MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
        //           END-IF
        if (ws.getIndPoli().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
            poli.setPolFlCumPreCntr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-AMMB-MOVI = -1
        //              MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
            poli.setPolFlAmmbMovi(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-CONV-GECO = -1
        //              MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
        //           END-IF
        if (ws.getIndPoli().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
            poli.setPolConvGeco(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_CONV_GECO));
        }
        // COB_CODE: IF IND-POL-FL-SCUDO-FISC = -1
        //              MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
            poli.setPolFlScudoFisc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-TRASFE = -1
        //              MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
            poli.setPolFlTrasfe(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
            poli.setPolFlTfrStrc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-DT-PRESC = -1
        //              MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
        //           END-IF
        if (ws.getIndPoli().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
            poli.getPolDtPresc().setPolDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtPresc.Len.POL_DT_PRESC_NULL));
        }
        // COB_CODE: IF IND-POL-COD-CONV-AGG = -1
        //              MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
        //           END-IF
        if (ws.getIndPoli().getSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
            poli.setPolCodConvAgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_CONV_AGG));
        }
        // COB_CODE: IF IND-POL-SUBCAT-PROD = -1
        //              MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
        //           END-IF
        if (ws.getIndPoli().getCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
            poli.setPolSubcatProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_SUBCAT_PROD));
        }
        // COB_CODE: IF IND-POL-FL-QUEST-ADEGZ-ASS = -1
        //              MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
        //           END-IF
        if (ws.getIndPoli().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
            poli.setPolFlQuestAdegzAss(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-COD-TPA = -1
        //              MOVE HIGH-VALUES TO POL-COD-TPA-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-TPA-NULL
            poli.setPolCodTpa(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_TPA));
        }
        // COB_CODE: IF IND-POL-ID-ACC-COMM = -1
        //              MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
            poli.getPolIdAccComm().setPolIdAccCommNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdAccComm.Len.POL_ID_ACC_COMM_NULL));
        }
        // COB_CODE: IF IND-POL-FL-POLI-CPI-PR = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
        //           END-IF
        if (ws.getIndPoli().getNumGgRitardoPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
            poli.setPolFlPoliCpiPr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-POLI-BUNDLING = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
        //           END-IF
        if (ws.getIndPoli().getNumGgRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
            poli.setPolFlPoliBundling(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-IND-POLI-PRIN-COLL = -1
        //              MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
        //           END-IF
        if (ws.getIndPoli().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
            poli.setPolIndPoliPrinColl(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-VND-BUNDLE = -1
        //              MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
        //           END-IF
        if (ws.getIndPoli().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
            poli.setPolFlVndBundle(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-IB-BS = -1
        //              MOVE HIGH-VALUES TO POL-IB-BS-NULL
        //           END-IF
        if (ws.getIndPoli().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IB-BS-NULL
            poli.setPolIbBs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_IB_BS));
        }
        // COB_CODE: IF IND-POL-FL-POLI-IFP = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
        //           END-IF.
        if (ws.getIndPoli().getCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
            poli.setPolFlPoliIfp(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: IF IND-POL-DT-PROP = 0
        //               MOVE WS-DATE-N      TO POL-DT-PROP
        //           END-IF
        if (ws.getIndPoli().getDtEndCop() == 0) {
            // COB_CODE: MOVE POL-DT-PROP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getIniEffDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-PROP
            poli.getPolDtProp().setPolDtProp(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE POL-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-EFF
        poli.setPolDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getDecorDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-END-EFF
        poli.setPolDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-DECOR-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getScadDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-DECOR
        poli.setPolDtDecor(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-EMIS-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getVarzTpIasDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-EMIS
        poli.setPolDtEmis(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-POL-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO POL-DT-SCAD
        //           END-IF
        if (ws.getIndPoli().getIntrMora() == 0) {
            // COB_CODE: MOVE POL-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getNovaRgmFiscDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-SCAD
            poli.getPolDtScad().setPolDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE POL-DT-INI-VLDT-PROD-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getDecorPrestBanDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-VLDT-PROD
        poli.setPolDtIniVldtProd(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-POL-DT-INI-VLDT-CONV = 0
        //               MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
        //           END-IF
        if (ws.getIndPoli().getDir() == 0) {
            // COB_CODE: MOVE POL-DT-INI-VLDT-CONV-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getEffVarzStatTDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
            poli.getPolDtIniVldtConv().setPolDtIniVldtConv(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-POL-DT-APPLZ-CONV = 0
        //               MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
        //           END-IF
        if (ws.getIndPoli().getSpeMed() == 0) {
            // COB_CODE: MOVE POL-DT-APPLZ-CONV-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getUltConsCnbtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
            poli.getPolDtApplzConv().setPolDtApplzConv(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-POL-DT-PRESC = 0
        //               MOVE WS-DATE-N      TO POL-DT-PRESC
        //           END-IF.
        if (ws.getIndPoli().getDtEsiTit() == 0) {
            // COB_CODE: MOVE POL-DT-PRESC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getPrescDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-PRESC
            poli.getPolDtPresc().setPolDtPresc(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getAaDiffProrDflt() {
        return poli.getPolAaDiffProrDflt().getPolAaDiffProrDflt();
    }

    @Override
    public void setAaDiffProrDflt(int aaDiffProrDflt) {
        this.poli.getPolAaDiffProrDflt().setPolAaDiffProrDflt(aaDiffProrDflt);
    }

    @Override
    public Integer getAaDiffProrDfltObj() {
        if (ws.getIndPoli().getProvDaRec() >= 0) {
            return ((Integer)getAaDiffProrDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaDiffProrDfltObj(Integer aaDiffProrDfltObj) {
        if (aaDiffProrDfltObj != null) {
            setAaDiffProrDflt(((int)aaDiffProrDfltObj));
            ws.getIndPoli().setProvDaRec(((short)0));
        }
        else {
            ws.getIndPoli().setProvDaRec(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return poli.getPolCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.poli.setPolCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodConv() {
        return poli.getPolCodConv();
    }

    @Override
    public String getCodConvAgg() {
        return poli.getPolCodConvAgg();
    }

    @Override
    public void setCodConvAgg(String codConvAgg) {
        this.poli.setPolCodConvAgg(codConvAgg);
    }

    @Override
    public String getCodConvAggObj() {
        if (ws.getIndPoli().getSpeAge() >= 0) {
            return getCodConvAgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodConvAggObj(String codConvAggObj) {
        if (codConvAggObj != null) {
            setCodConvAgg(codConvAggObj);
            ws.getIndPoli().setSpeAge(((short)0));
        }
        else {
            ws.getIndPoli().setSpeAge(((short)-1));
        }
    }

    @Override
    public void setCodConv(String codConv) {
        this.poli.setPolCodConv(codConv);
    }

    @Override
    public String getCodConvObj() {
        if (ws.getIndPoli().getIntrRetdt() >= 0) {
            return getCodConv();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodConvObj(String codConvObj) {
        if (codConvObj != null) {
            setCodConv(codConvObj);
            ws.getIndPoli().setIntrRetdt(((short)0));
        }
        else {
            ws.getIndPoli().setIntrRetdt(((short)-1));
        }
    }

    @Override
    public String getCodDvs() {
        return poli.getPolCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.poli.setPolCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndPoli().getCarGest() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndPoli().setCarGest(((short)0));
        }
        else {
            ws.getIndPoli().setCarGest(((short)-1));
        }
    }

    @Override
    public String getCodProd() {
        return poli.getPolCodProd();
    }

    @Override
    public void setCodProd(String codProd) {
        this.poli.setPolCodProd(codProd);
    }

    @Override
    public String getCodRamo() {
        return poli.getPolCodRamo();
    }

    @Override
    public void setCodRamo(String codRamo) {
        this.poli.setPolCodRamo(codRamo);
    }

    @Override
    public String getCodRamoObj() {
        if (ws.getIndPoli().getIntrRiat() >= 0) {
            return getCodRamo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodRamoObj(String codRamoObj) {
        if (codRamoObj != null) {
            setCodRamo(codRamoObj);
            ws.getIndPoli().setIntrRiat(((short)0));
        }
        else {
            ws.getIndPoli().setIntrRiat(((short)-1));
        }
    }

    @Override
    public String getCodTpa() {
        return poli.getPolCodTpa();
    }

    @Override
    public void setCodTpa(String codTpa) {
        this.poli.setPolCodTpa(codTpa);
    }

    @Override
    public String getCodTpaObj() {
        if (ws.getIndPoli().getImpTrasfe() >= 0) {
            return getCodTpa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTpaObj(String codTpaObj) {
        if (codTpaObj != null) {
            setCodTpa(codTpaObj);
            ws.getIndPoli().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndPoli().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public String getConvGeco() {
        return poli.getPolConvGeco();
    }

    @Override
    public void setConvGeco(String convGeco) {
        this.poli.setPolConvGeco(convGeco);
    }

    @Override
    public String getConvGecoObj() {
        if (ws.getIndPoli().getImpVolo() >= 0) {
            return getConvGeco();
        }
        else {
            return null;
        }
    }

    @Override
    public void setConvGecoObj(String convGecoObj) {
        if (convGecoObj != null) {
            setConvGeco(convGecoObj);
            ws.getIndPoli().setImpVolo(((short)0));
        }
        else {
            ws.getIndPoli().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getDir1oVers() {
        return poli.getPolDir1oVers().getPolDir1oVers();
    }

    @Override
    public void setDir1oVers(AfDecimal dir1oVers) {
        this.poli.getPolDir1oVers().setPolDir1oVers(dir1oVers.copy());
    }

    @Override
    public AfDecimal getDir1oVersObj() {
        if (ws.getIndPoli().getPreSoloRsh() >= 0) {
            return getDir1oVers();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDir1oVersObj(AfDecimal dir1oVersObj) {
        if (dir1oVersObj != null) {
            setDir1oVers(new AfDecimal(dir1oVersObj, 15, 3));
            ws.getIndPoli().setPreSoloRsh(((short)0));
        }
        else {
            ws.getIndPoli().setPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getDirEmis() {
        return poli.getPolDirEmis().getPolDirEmis();
    }

    @Override
    public void setDirEmis(AfDecimal dirEmis) {
        this.poli.getPolDirEmis().setPolDirEmis(dirEmis.copy());
    }

    @Override
    public AfDecimal getDirEmisObj() {
        if (ws.getIndPoli().getPrePpIas() >= 0) {
            return getDirEmis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDirEmisObj(AfDecimal dirEmisObj) {
        if (dirEmisObj != null) {
            setDirEmis(new AfDecimal(dirEmisObj, 15, 3));
            ws.getIndPoli().setPrePpIas(((short)0));
        }
        else {
            ws.getIndPoli().setPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getDirQuiet() {
        return poli.getPolDirQuiet().getPolDirQuiet();
    }

    @Override
    public void setDirQuiet(AfDecimal dirQuiet) {
        this.poli.getPolDirQuiet().setPolDirQuiet(dirQuiet.copy());
    }

    @Override
    public AfDecimal getDirQuietObj() {
        if (ws.getIndPoli().getCodTari() >= 0) {
            return getDirQuiet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDirQuietObj(AfDecimal dirQuietObj) {
        if (dirQuietObj != null) {
            setDirQuiet(new AfDecimal(dirQuietObj, 15, 3));
            ws.getIndPoli().setCodTari(((short)0));
        }
        else {
            ws.getIndPoli().setCodTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getDirVersAgg() {
        return poli.getPolDirVersAgg().getPolDirVersAgg();
    }

    @Override
    public void setDirVersAgg(AfDecimal dirVersAgg) {
        this.poli.getPolDirVersAgg().setPolDirVersAgg(dirVersAgg.copy());
    }

    @Override
    public AfDecimal getDirVersAggObj() {
        if (ws.getIndPoli().getCarAcq() >= 0) {
            return getDirVersAgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDirVersAggObj(AfDecimal dirVersAggObj) {
        if (dirVersAggObj != null) {
            setDirVersAgg(new AfDecimal(dirVersAggObj, 15, 3));
            ws.getIndPoli().setCarAcq(((short)0));
        }
        else {
            ws.getIndPoli().setCarAcq(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return poli.getPolDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.poli.setPolDsOperSql(dsOperSql);
    }

    @Override
    public long getDsRiga() {
        return poli.getPolDsRiga();
    }

    @Override
    public void setDsRiga(long dsRiga) {
        this.poli.setPolDsRiga(dsRiga);
    }

    @Override
    public char getDsStatoElab() {
        return poli.getPolDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.poli.setPolDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return poli.getPolDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.poli.setPolDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return poli.getPolDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.poli.setPolDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return poli.getPolDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.poli.setPolDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return poli.getPolDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.poli.setPolDsVer(dsVer);
    }

    @Override
    public String getDtApplzConvDb() {
        return ws.getPoliDb().getUltConsCnbtDb();
    }

    @Override
    public void setDtApplzConvDb(String dtApplzConvDb) {
        this.ws.getPoliDb().setUltConsCnbtDb(dtApplzConvDb);
    }

    @Override
    public String getDtApplzConvDbObj() {
        if (ws.getIndPoli().getSpeMed() >= 0) {
            return getDtApplzConvDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtApplzConvDbObj(String dtApplzConvDbObj) {
        if (dtApplzConvDbObj != null) {
            setDtApplzConvDb(dtApplzConvDbObj);
            ws.getIndPoli().setSpeMed(((short)0));
        }
        else {
            ws.getIndPoli().setSpeMed(((short)-1));
        }
    }

    @Override
    public String getDtDecorDb() {
        return ws.getPoliDb().getScadDb();
    }

    @Override
    public void setDtDecorDb(String dtDecorDb) {
        this.ws.getPoliDb().setScadDb(dtDecorDb);
    }

    @Override
    public String getDtEmisDb() {
        return ws.getPoliDb().getVarzTpIasDb();
    }

    @Override
    public void setDtEmisDb(String dtEmisDb) {
        this.ws.getPoliDb().setVarzTpIasDb(dtEmisDb);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getPoliDb().getDecorDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getPoliDb().setDecorDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getPoliDb().getEndEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getPoliDb().setEndEffDb(dtIniEffDb);
    }

    @Override
    public String getDtIniVldtConvDb() {
        return ws.getPoliDb().getEffVarzStatTDb();
    }

    @Override
    public void setDtIniVldtConvDb(String dtIniVldtConvDb) {
        this.ws.getPoliDb().setEffVarzStatTDb(dtIniVldtConvDb);
    }

    @Override
    public String getDtIniVldtConvDbObj() {
        if (ws.getIndPoli().getDir() >= 0) {
            return getDtIniVldtConvDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniVldtConvDbObj(String dtIniVldtConvDbObj) {
        if (dtIniVldtConvDbObj != null) {
            setDtIniVldtConvDb(dtIniVldtConvDbObj);
            ws.getIndPoli().setDir(((short)0));
        }
        else {
            ws.getIndPoli().setDir(((short)-1));
        }
    }

    @Override
    public String getDtIniVldtProdDb() {
        return ws.getPoliDb().getDecorPrestBanDb();
    }

    @Override
    public void setDtIniVldtProdDb(String dtIniVldtProdDb) {
        this.ws.getPoliDb().setDecorPrestBanDb(dtIniVldtProdDb);
    }

    @Override
    public String getDtPrescDb() {
        return ws.getPoliDb().getPrescDb();
    }

    @Override
    public void setDtPrescDb(String dtPrescDb) {
        this.ws.getPoliDb().setPrescDb(dtPrescDb);
    }

    @Override
    public String getDtPrescDbObj() {
        if (ws.getIndPoli().getDtEsiTit() >= 0) {
            return getDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtPrescDbObj(String dtPrescDbObj) {
        if (dtPrescDbObj != null) {
            setDtPrescDb(dtPrescDbObj);
            ws.getIndPoli().setDtEsiTit(((short)0));
        }
        else {
            ws.getIndPoli().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getDtPropDb() {
        return ws.getPoliDb().getIniEffDb();
    }

    @Override
    public void setDtPropDb(String dtPropDb) {
        this.ws.getPoliDb().setIniEffDb(dtPropDb);
    }

    @Override
    public String getDtPropDbObj() {
        if (ws.getIndPoli().getDtEndCop() >= 0) {
            return getDtPropDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtPropDbObj(String dtPropDbObj) {
        if (dtPropDbObj != null) {
            setDtPropDb(dtPropDbObj);
            ws.getIndPoli().setDtEndCop(((short)0));
        }
        else {
            ws.getIndPoli().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getDtScadDb() {
        return ws.getPoliDb().getNovaRgmFiscDb();
    }

    @Override
    public void setDtScadDb(String dtScadDb) {
        this.ws.getPoliDb().setNovaRgmFiscDb(dtScadDb);
    }

    @Override
    public String getDtScadDbObj() {
        if (ws.getIndPoli().getIntrMora() >= 0) {
            return getDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadDbObj(String dtScadDbObj) {
        if (dtScadDbObj != null) {
            setDtScadDb(dtScadDbObj);
            ws.getIndPoli().setIntrMora(((short)0));
        }
        else {
            ws.getIndPoli().setIntrMora(((short)-1));
        }
    }

    @Override
    public int getDurAa() {
        return poli.getPolDurAa().getPolDurAa();
    }

    @Override
    public void setDurAa(int durAa) {
        this.poli.getPolDurAa().setPolDurAa(durAa);
    }

    @Override
    public Integer getDurAaObj() {
        if (ws.getIndPoli().getPreNet() >= 0) {
            return ((Integer)getDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        if (durAaObj != null) {
            setDurAa(((int)durAaObj));
            ws.getIndPoli().setPreNet(((short)0));
        }
        else {
            ws.getIndPoli().setPreNet(((short)-1));
        }
    }

    @Override
    public int getDurGg() {
        return poli.getPolDurGg().getPolDurGg();
    }

    @Override
    public void setDurGg(int durGg) {
        this.poli.getPolDurGg().setPolDurGg(durGg);
    }

    @Override
    public Integer getDurGgObj() {
        if (ws.getIndPoli().getFrqMovi() >= 0) {
            return ((Integer)getDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        if (durGgObj != null) {
            setDurGg(((int)durGgObj));
            ws.getIndPoli().setFrqMovi(((short)0));
        }
        else {
            ws.getIndPoli().setFrqMovi(((short)-1));
        }
    }

    @Override
    public int getDurMm() {
        return poli.getPolDurMm().getPolDurMm();
    }

    @Override
    public void setDurMm(int durMm) {
        this.poli.getPolDurMm().setPolDurMm(durMm);
    }

    @Override
    public Integer getDurMmObj() {
        if (ws.getIndPoli().getIntrFraz() >= 0) {
            return ((Integer)getDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        if (durMmObj != null) {
            setDurMm(((int)durMmObj));
            ws.getIndPoli().setIntrFraz(((short)0));
        }
        else {
            ws.getIndPoli().setIntrFraz(((short)-1));
        }
    }

    @Override
    public char getFlAmmbMovi() {
        return poli.getPolFlAmmbMovi();
    }

    @Override
    public void setFlAmmbMovi(char flAmmbMovi) {
        this.poli.setPolFlAmmbMovi(flAmmbMovi);
    }

    @Override
    public Character getFlAmmbMoviObj() {
        if (ws.getIndPoli().getImpTfr() >= 0) {
            return ((Character)getFlAmmbMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlAmmbMoviObj(Character flAmmbMoviObj) {
        if (flAmmbMoviObj != null) {
            setFlAmmbMovi(((char)flAmmbMoviObj));
            ws.getIndPoli().setImpTfr(((short)0));
        }
        else {
            ws.getIndPoli().setImpTfr(((short)-1));
        }
    }

    @Override
    public char getFlCopFinanz() {
        return poli.getPolFlCopFinanz();
    }

    @Override
    public void setFlCopFinanz(char flCopFinanz) {
        this.poli.setPolFlCopFinanz(flCopFinanz);
    }

    @Override
    public Character getFlCopFinanzObj() {
        if (ws.getIndPoli().getSoprProf() >= 0) {
            return ((Character)getFlCopFinanz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCopFinanzObj(Character flCopFinanzObj) {
        if (flCopFinanzObj != null) {
            setFlCopFinanz(((char)flCopFinanzObj));
            ws.getIndPoli().setSoprProf(((short)0));
        }
        else {
            ws.getIndPoli().setSoprProf(((short)-1));
        }
    }

    @Override
    public char getFlCumPreCntr() {
        return poli.getPolFlCumPreCntr();
    }

    @Override
    public void setFlCumPreCntr(char flCumPreCntr) {
        this.poli.setPolFlCumPreCntr(flCumPreCntr);
    }

    @Override
    public Character getFlCumPreCntrObj() {
        if (ws.getIndPoli().getImpAder() >= 0) {
            return ((Character)getFlCumPreCntr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCumPreCntrObj(Character flCumPreCntrObj) {
        if (flCumPreCntrObj != null) {
            setFlCumPreCntr(((char)flCumPreCntrObj));
            ws.getIndPoli().setImpAder(((short)0));
        }
        else {
            ws.getIndPoli().setImpAder(((short)-1));
        }
    }

    @Override
    public char getFlEstas() {
        return poli.getPolFlEstas();
    }

    @Override
    public void setFlEstas(char flEstas) {
        this.poli.setPolFlEstas(flEstas);
    }

    @Override
    public Character getFlEstasObj() {
        if (ws.getIndPoli().getSoprSan() >= 0) {
            return ((Character)getFlEstas());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlEstasObj(Character flEstasObj) {
        if (flEstasObj != null) {
            setFlEstas(((char)flEstasObj));
            ws.getIndPoli().setSoprSan(((short)0));
        }
        else {
            ws.getIndPoli().setSoprSan(((short)-1));
        }
    }

    @Override
    public char getFlFntAder() {
        return poli.getPolFlFntAder();
    }

    @Override
    public void setFlFntAder(char flFntAder) {
        this.poli.setPolFlFntAder(flFntAder);
    }

    @Override
    public Character getFlFntAderObj() {
        if (ws.getIndPoli().getProvAcq1aa() >= 0) {
            return ((Character)getFlFntAder());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlFntAderObj(Character flFntAderObj) {
        if (flFntAderObj != null) {
            setFlFntAder(((char)flFntAderObj));
            ws.getIndPoli().setProvAcq1aa(((short)0));
        }
        else {
            ws.getIndPoli().setProvAcq1aa(((short)-1));
        }
    }

    @Override
    public char getFlFntAz() {
        return poli.getPolFlFntAz();
    }

    @Override
    public void setFlFntAz(char flFntAz) {
        this.poli.setPolFlFntAz(flFntAz);
    }

    @Override
    public Character getFlFntAzObj() {
        if (ws.getIndPoli().getCarInc() >= 0) {
            return ((Character)getFlFntAz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlFntAzObj(Character flFntAzObj) {
        if (flFntAzObj != null) {
            setFlFntAz(((char)flFntAzObj));
            ws.getIndPoli().setCarInc(((short)0));
        }
        else {
            ws.getIndPoli().setCarInc(((short)-1));
        }
    }

    @Override
    public char getFlFntTfr() {
        return poli.getPolFlFntTfr();
    }

    @Override
    public void setFlFntTfr(char flFntTfr) {
        this.poli.setPolFlFntTfr(flFntTfr);
    }

    @Override
    public Character getFlFntTfrObj() {
        if (ws.getIndPoli().getProvAcq2aa() >= 0) {
            return ((Character)getFlFntTfr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlFntTfrObj(Character flFntTfrObj) {
        if (flFntTfrObj != null) {
            setFlFntTfr(((char)flFntTfrObj));
            ws.getIndPoli().setProvAcq2aa(((short)0));
        }
        else {
            ws.getIndPoli().setProvAcq2aa(((short)-1));
        }
    }

    @Override
    public char getFlFntVolo() {
        return poli.getPolFlFntVolo();
    }

    @Override
    public void setFlFntVolo(char flFntVolo) {
        this.poli.setPolFlFntVolo(flFntVolo);
    }

    @Override
    public Character getFlFntVoloObj() {
        if (ws.getIndPoli().getProvRicor() >= 0) {
            return ((Character)getFlFntVolo());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlFntVoloObj(Character flFntVoloObj) {
        if (flFntVoloObj != null) {
            setFlFntVolo(((char)flFntVoloObj));
            ws.getIndPoli().setProvRicor(((short)0));
        }
        else {
            ws.getIndPoli().setProvRicor(((short)-1));
        }
    }

    @Override
    public char getFlPoliBundling() {
        return poli.getPolFlPoliBundling();
    }

    @Override
    public void setFlPoliBundling(char flPoliBundling) {
        this.poli.setPolFlPoliBundling(flPoliBundling);
    }

    @Override
    public Character getFlPoliBundlingObj() {
        if (ws.getIndPoli().getNumGgRival() >= 0) {
            return ((Character)getFlPoliBundling());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPoliBundlingObj(Character flPoliBundlingObj) {
        if (flPoliBundlingObj != null) {
            setFlPoliBundling(((char)flPoliBundlingObj));
            ws.getIndPoli().setNumGgRival(((short)0));
        }
        else {
            ws.getIndPoli().setNumGgRival(((short)-1));
        }
    }

    @Override
    public char getFlPoliCpiPr() {
        return poli.getPolFlPoliCpiPr();
    }

    @Override
    public void setFlPoliCpiPr(char flPoliCpiPr) {
        this.poli.setPolFlPoliCpiPr(flPoliCpiPr);
    }

    @Override
    public Character getFlPoliCpiPrObj() {
        if (ws.getIndPoli().getNumGgRitardoPag() >= 0) {
            return ((Character)getFlPoliCpiPr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPoliCpiPrObj(Character flPoliCpiPrObj) {
        if (flPoliCpiPrObj != null) {
            setFlPoliCpiPr(((char)flPoliCpiPrObj));
            ws.getIndPoli().setNumGgRitardoPag(((short)0));
        }
        else {
            ws.getIndPoli().setNumGgRitardoPag(((short)-1));
        }
    }

    @Override
    public char getFlPoliIfp() {
        return poli.getPolFlPoliIfp();
    }

    @Override
    public void setFlPoliIfp(char flPoliIfp) {
        this.poli.setPolFlPoliIfp(flPoliIfp);
    }

    @Override
    public Character getFlPoliIfpObj() {
        if (ws.getIndPoli().getCnbtAntirac() >= 0) {
            return ((Character)getFlPoliIfp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPoliIfpObj(Character flPoliIfpObj) {
        if (flPoliIfpObj != null) {
            setFlPoliIfp(((char)flPoliIfpObj));
            ws.getIndPoli().setCnbtAntirac(((short)0));
        }
        else {
            ws.getIndPoli().setCnbtAntirac(((short)-1));
        }
    }

    @Override
    public char getFlQuestAdegzAss() {
        return poli.getPolFlQuestAdegzAss();
    }

    @Override
    public void setFlQuestAdegzAss(char flQuestAdegzAss) {
        this.poli.setPolFlQuestAdegzAss(flQuestAdegzAss);
    }

    @Override
    public Character getFlQuestAdegzAssObj() {
        if (ws.getIndPoli().getTotIntrPrest() >= 0) {
            return ((Character)getFlQuestAdegzAss());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlQuestAdegzAssObj(Character flQuestAdegzAssObj) {
        if (flQuestAdegzAssObj != null) {
            setFlQuestAdegzAss(((char)flQuestAdegzAssObj));
            ws.getIndPoli().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndPoli().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public char getFlRshComun() {
        return poli.getPolFlRshComun();
    }

    @Override
    public void setFlRshComun(char flRshComun) {
        this.poli.setPolFlRshComun(flRshComun);
    }

    @Override
    public char getFlRshComunCond() {
        return poli.getPolFlRshComunCond();
    }

    @Override
    public void setFlRshComunCond(char flRshComunCond) {
        this.poli.setPolFlRshComunCond(flRshComunCond);
    }

    @Override
    public Character getFlRshComunCondObj() {
        if (ws.getIndPoli().getSoprTec() >= 0) {
            return ((Character)getFlRshComunCond());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlRshComunCondObj(Character flRshComunCondObj) {
        if (flRshComunCondObj != null) {
            setFlRshComunCond(((char)flRshComunCondObj));
            ws.getIndPoli().setSoprTec(((short)0));
        }
        else {
            ws.getIndPoli().setSoprTec(((short)-1));
        }
    }

    @Override
    public Character getFlRshComunObj() {
        if (ws.getIndPoli().getSoprSpo() >= 0) {
            return ((Character)getFlRshComun());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlRshComunObj(Character flRshComunObj) {
        if (flRshComunObj != null) {
            setFlRshComun(((char)flRshComunObj));
            ws.getIndPoli().setSoprSpo(((short)0));
        }
        else {
            ws.getIndPoli().setSoprSpo(((short)-1));
        }
    }

    @Override
    public char getFlScudoFisc() {
        return poli.getPolFlScudoFisc();
    }

    @Override
    public void setFlScudoFisc(char flScudoFisc) {
        this.poli.setPolFlScudoFisc(flScudoFisc);
    }

    @Override
    public Character getFlScudoFiscObj() {
        if (ws.getIndPoli().getManfeeAntic() >= 0) {
            return ((Character)getFlScudoFisc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlScudoFiscObj(Character flScudoFiscObj) {
        if (flScudoFiscObj != null) {
            setFlScudoFisc(((char)flScudoFiscObj));
            ws.getIndPoli().setManfeeAntic(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public char getFlTfrStrc() {
        return poli.getPolFlTfrStrc();
    }

    @Override
    public void setFlTfrStrc(char flTfrStrc) {
        this.poli.setPolFlTfrStrc(flTfrStrc);
    }

    @Override
    public Character getFlTfrStrcObj() {
        if (ws.getIndPoli().getManfeeRec() >= 0) {
            return ((Character)getFlTfrStrc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlTfrStrcObj(Character flTfrStrcObj) {
        if (flTfrStrcObj != null) {
            setFlTfrStrc(((char)flTfrStrcObj));
            ws.getIndPoli().setManfeeRec(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeRec(((short)-1));
        }
    }

    @Override
    public char getFlTrasfe() {
        return poli.getPolFlTrasfe();
    }

    @Override
    public void setFlTrasfe(char flTrasfe) {
        this.poli.setPolFlTrasfe(flTrasfe);
    }

    @Override
    public Character getFlTrasfeObj() {
        if (ws.getIndPoli().getManfeeRicor() >= 0) {
            return ((Character)getFlTrasfe());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlTrasfeObj(Character flTrasfeObj) {
        if (flTrasfeObj != null) {
            setFlTrasfe(((char)flTrasfeObj));
            ws.getIndPoli().setManfeeRicor(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public String getFlVerProd() {
        return poli.getPolFlVerProd();
    }

    @Override
    public void setFlVerProd(String flVerProd) {
        this.poli.setPolFlVerProd(flVerProd);
    }

    @Override
    public String getFlVerProdObj() {
        if (ws.getIndPoli().getCodDvs() >= 0) {
            return getFlVerProd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlVerProdObj(String flVerProdObj) {
        if (flVerProdObj != null) {
            setFlVerProd(flVerProdObj);
            ws.getIndPoli().setCodDvs(((short)0));
        }
        else {
            ws.getIndPoli().setCodDvs(((short)-1));
        }
    }

    @Override
    public char getFlVndBundle() {
        return poli.getPolFlVndBundle();
    }

    @Override
    public void setFlVndBundle(char flVndBundle) {
        this.poli.setPolFlVndBundle(flVndBundle);
    }

    @Override
    public Character getFlVndBundleObj() {
        if (ws.getIndPoli().getRemunAss() >= 0) {
            return ((Character)getFlVndBundle());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlVndBundleObj(Character flVndBundleObj) {
        if (flVndBundleObj != null) {
            setFlVndBundle(((char)flVndBundleObj));
            ws.getIndPoli().setRemunAss(((short)0));
        }
        else {
            ws.getIndPoli().setRemunAss(((short)-1));
        }
    }

    @Override
    public String getIbBs() {
        return poli.getPolIbBs();
    }

    @Override
    public void setIbBs(String ibBs) {
        this.poli.setPolIbBs(ibBs);
    }

    @Override
    public String getIbBsObj() {
        if (ws.getIndPoli().getCommisInter() >= 0) {
            return getIbBs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbBsObj(String ibBsObj) {
        if (ibBsObj != null) {
            setIbBs(ibBsObj);
            ws.getIndPoli().setCommisInter(((short)0));
        }
        else {
            ws.getIndPoli().setCommisInter(((short)-1));
        }
    }

    @Override
    public String getIbOgg() {
        return poli.getPolIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.poli.setPolIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndPoli().getDtIniCop() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndPoli().setDtIniCop(((short)0));
        }
        else {
            ws.getIndPoli().setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getIbProp() {
        return poli.getPolIbProp();
    }

    @Override
    public void setIbProp(String ibProp) {
        this.poli.setPolIbProp(ibProp);
    }

    @Override
    public int getIdAccComm() {
        return poli.getPolIdAccComm().getPolIdAccComm();
    }

    @Override
    public void setIdAccComm(int idAccComm) {
        this.poli.getPolIdAccComm().setPolIdAccComm(idAccComm);
    }

    @Override
    public Integer getIdAccCommObj() {
        if (ws.getIndPoli().getImpTfrStrc() >= 0) {
            return ((Integer)getIdAccComm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdAccCommObj(Integer idAccCommObj) {
        if (idAccCommObj != null) {
            setIdAccComm(((int)idAccCommObj));
            ws.getIndPoli().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndPoli().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return poli.getPolIdMoviChiu().getPolIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.poli.getPolIdMoviChiu().setPolIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndPoli().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndPoli().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndPoli().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return poli.getPolIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.poli.setPolIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPoli() {
        return poli.getPolIdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.poli.setPolIdPoli(idPoli);
    }

    @Override
    public char getIndPoliPrinColl() {
        return poli.getPolIndPoliPrinColl();
    }

    @Override
    public void setIndPoliPrinColl(char indPoliPrinColl) {
        this.poli.setPolIndPoliPrinColl(indPoliPrinColl);
    }

    @Override
    public Character getIndPoliPrinCollObj() {
        if (ws.getIndPoli().getAcqExp() >= 0) {
            return ((Character)getIndPoliPrinColl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPoliPrinCollObj(Character indPoliPrinCollObj) {
        if (indPoliPrinCollObj != null) {
            setIndPoliPrinColl(((char)indPoliPrinCollObj));
            ws.getIndPoli().setAcqExp(((short)0));
        }
        else {
            ws.getIndPoli().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpeMed() {
        return poli.getPolSpeMed().getPolSpeMed();
    }

    @Override
    public void setSpeMed(AfDecimal speMed) {
        this.poli.getPolSpeMed().setPolSpeMed(speMed.copy());
    }

    @Override
    public AfDecimal getSpeMedObj() {
        if (ws.getIndPoli().getPreTot() >= 0) {
            return getSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpeMedObj(AfDecimal speMedObj) {
        if (speMedObj != null) {
            setSpeMed(new AfDecimal(speMedObj, 15, 3));
            ws.getIndPoli().setPreTot(((short)0));
        }
        else {
            ws.getIndPoli().setPreTot(((short)-1));
        }
    }

    @Override
    public String getSubcatProd() {
        return poli.getPolSubcatProd();
    }

    @Override
    public void setSubcatProd(String subcatProd) {
        this.poli.setPolSubcatProd(subcatProd);
    }

    @Override
    public String getSubcatProdObj() {
        if (ws.getIndPoli().getCarIas() >= 0) {
            return getSubcatProd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSubcatProdObj(String subcatProdObj) {
        if (subcatProdObj != null) {
            setSubcatProd(subcatProdObj);
            ws.getIndPoli().setCarIas(((short)0));
        }
        else {
            ws.getIndPoli().setCarIas(((short)-1));
        }
    }

    @Override
    public String getTpApplzDir() {
        return poli.getPolTpApplzDir();
    }

    @Override
    public void setTpApplzDir(String tpApplzDir) {
        this.poli.setPolTpApplzDir(tpApplzDir);
    }

    @Override
    public String getTpApplzDirObj() {
        if (ws.getIndPoli().getSoprAlt() >= 0) {
            return getTpApplzDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpApplzDirObj(String tpApplzDirObj) {
        if (tpApplzDirObj != null) {
            setTpApplzDir(tpApplzDirObj);
            ws.getIndPoli().setSoprAlt(((short)0));
        }
        else {
            ws.getIndPoli().setSoprAlt(((short)-1));
        }
    }

    @Override
    public String getTpFrmAssva() {
        return poli.getPolTpFrmAssva();
    }

    @Override
    public void setTpFrmAssva(String tpFrmAssva) {
        this.poli.setPolTpFrmAssva(tpFrmAssva);
    }

    @Override
    public String getTpLivGenzTit() {
        return poli.getPolTpLivGenzTit();
    }

    @Override
    public void setTpLivGenzTit(String tpLivGenzTit) {
        this.poli.setPolTpLivGenzTit(tpLivGenzTit);
    }

    @Override
    public String getTpOpzAScad() {
        return poli.getPolTpOpzAScad();
    }

    @Override
    public void setTpOpzAScad(String tpOpzAScad) {
        this.poli.setPolTpOpzAScad(tpOpzAScad);
    }

    @Override
    public String getTpOpzAScadObj() {
        if (ws.getIndPoli().getProvInc() >= 0) {
            return getTpOpzAScad();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpOpzAScadObj(String tpOpzAScadObj) {
        if (tpOpzAScadObj != null) {
            setTpOpzAScad(tpOpzAScadObj);
            ws.getIndPoli().setProvInc(((short)0));
        }
        else {
            ws.getIndPoli().setProvInc(((short)-1));
        }
    }

    @Override
    public String getTpPoli() {
        return poli.getPolTpPoli();
    }

    @Override
    public void setTpPoli(String tpPoli) {
        this.poli.setPolTpPoli(tpPoli);
    }

    @Override
    public String getTpPtfEstno() {
        return poli.getPolTpPtfEstno();
    }

    @Override
    public void setTpPtfEstno(String tpPtfEstno) {
        this.poli.setPolTpPtfEstno(tpPtfEstno);
    }

    @Override
    public String getTpPtfEstnoObj() {
        if (ws.getIndPoli().getImpAz() >= 0) {
            return getTpPtfEstno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPtfEstnoObj(String tpPtfEstnoObj) {
        if (tpPtfEstnoObj != null) {
            setTpPtfEstno(tpPtfEstnoObj);
            ws.getIndPoli().setImpAz(((short)0));
        }
        else {
            ws.getIndPoli().setImpAz(((short)-1));
        }
    }

    @Override
    public String getTpRgmFisc() {
        return poli.getPolTpRgmFisc();
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        this.poli.setPolTpRgmFisc(tpRgmFisc);
    }

    @Override
    public String getTpRgmFiscObj() {
        if (ws.getIndPoli().getTax() >= 0) {
            return getTpRgmFisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRgmFiscObj(String tpRgmFiscObj) {
        if (tpRgmFiscObj != null) {
            setTpRgmFisc(tpRgmFiscObj);
            ws.getIndPoli().setTax(((short)0));
        }
        else {
            ws.getIndPoli().setTax(((short)-1));
        }
    }
}
