package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.Lvvc0000DatiInput2;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.InputLvvs0000;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0003Data;

/**Original name: LVVS0003<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0003
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0003 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0003Data ws = new Lvvs0003Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0003
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0003_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0003 getInstance() {
        return ((Lvvs0003)Programs.getInstance(Lvvs0003.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-TRCH
        //                      WK-DATA-OUTPUT.
        initAreaIoTrch();
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        //
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //
        // COB_CODE: PERFORM L450-LEGGI-POLIZZA
        //              THRU L450-LEGGI-POLIZZA-EX
        l450LeggiPolizza();
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //            END-IF
        //           END-IF
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF POL-TP-FRM-ASSVA = 'CO'
            //                 THRU L460-LEGGI-ADESIONE-EX
            //           END-IF
            if (Conditions.eq(ws.getPoli().getPolTpFrmAssva(), "CO")) {
                // COB_CODE: PERFORM L460-LEGGI-ADESIONE
                //              THRU L460-LEGGI-ADESIONE-EX
                l460LeggiAdesione();
            }
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1253-RECUP-MOVI            THRU S1253-EX
            s1253RecupMovi();
            // COB_CODE: IF KO-MOVI
            //              END-IF
            //           END-IF
            if (ws.getFlagMovimento().isKoMovi()) {
                // COB_CODE: IF POL-TP-FRM-ASSVA = 'CO'
                //                 THRU LEGGI-PMO-EX
                //           END-IF
                if (Conditions.eq(ws.getPoli().getPolTpFrmAssva(), "CO")) {
                    // COB_CODE: PERFORM LEGGI-PMO
                    //              THRU LEGGI-PMO-EX
                    leggiPmo();
                }
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //               END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: IF OK-MOVI
                //              PERFORM S1400-CALC-DATA       THRU EX-S1400
                //           ELSE
                //              END-EVALUATE
                //            END-IF
                if (ws.getFlagMovimento().isOkMovi()) {
                    // COB_CODE: PERFORM S1400-CALC-DATA       THRU EX-S1400
                    s1400CalcData();
                }
                else {
                    // COB_CODE: MOVE DTGA-DT-DECOR(IVVC0213-IX-TABB)
                    //             TO IVVC0213-VAL-STR-O
                    //                WK-DATA-OUT
                    ivvc0213.getTabOutput().setValStrO(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecorFormatted());
                    ws.setWkDataOut(TruncAbs.toInt(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecor(), 8));
                    //              IF IDSV0003-USER-NAME = 'MIGCOL'
                    // COB_CODE: IF DTGA-DT-ULT-ADEG-PRE-PR-NULL(IVVC0213-IX-TABB)
                    //                                           NOT = HIGH-VALUES
                    //                                             AND LOW-VALUES
                    //                                             AND SPACES
                    //                         WK-DATA-OUT
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePrNullFormatted()) && !Characters.EQ_LOW.test(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePrNullFormatted()) && !Characters.EQ_SPACE.test(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePrNull())) {
                        // COB_CODE: MOVE DTGA-DT-ULT-ADEG-PRE-PR(IVVC0213-IX-TABB)
                        //             TO IVVC0213-VAL-STR-O
                        //                WK-DATA-OUT
                        ivvc0213.getTabOutput().setValStrO(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getDtgaDtUltAdegPrePrFormatted());
                        ws.setWkDataOut(TruncAbs.toInt(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePr(), 8));
                    }
                    //              END-IF
                    // COB_CODE: EVALUATE IVVC0213-COD-VARIABILE-O
                    //               WHEN 'DATULTADEGPL'
                    //               WHEN 'DTULTADEGTL'
                    //                      THRU S1300-EX
                    //           END-EVALUATE
                    switch (ivvc0213.getTabOutput().getCodVariabileO()) {

                        case "DATULTADEGPL":
                        case "DTULTADEGTL":// COB_CODE: INITIALIZE INPUT-LVVS0000
                            initInputLvvs0000();
                            // COB_CODE: MOVE WK-DATA-OUT
                            //             TO LVVC0000-DATA-INPUT-1
                            ws.getInputLvvs0000().setDataInput1Formatted(ws.getWkDataOutFormatted());
                            // COB_CODE: PERFORM S1300-CALL-LVVS0000
                            //              THRU S1300-EX
                            s1300CallLvvs0000();
                            break;

                        default:break;
                    }
                }
            }
        }
    }

    /**Original name: L450-LEGGI-POLIZZA<br>
	 * <pre>----------------------------------------------------------------*
	 * -- LETTURA DELLA POLIZZA
	 * ----------------------------------------------------------------*</pre>*/
    private void l450LeggiPolizza() {
        Idbspol0 idbspol0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE POLI.
        initPoli();
        // COB_CODE: MOVE IVVC0213-ID-POLIZZA       TO POL-ID-POLI.
        ws.getPoli().setPolIdPoli(ivvc0213.getIdPolizza());
        // COB_CODE: MOVE 'IDBSPOL0'                TO WK-PGM-CALLED.
        ws.setWkPgmCalled("IDBSPOL0");
        //  --> Tipo operazione
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        idsv0003.getOperazione().setSelect();
        //  --> Tipo livello
        // COB_CODE: SET IDSV0003-ID                TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011Id();
        // COB_CODE: CALL WK-PGM-CALLED USING IDSV0003 POLI
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER   TO TRUE
        //           END-CALL.
        try {
            idbspol0 = Idbspol0.getInstance();
            idbspol0.run(idsv0003, ws.getPoli());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCalled());
            // COB_CODE: MOVE 'CALL-IDBSPOL0 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSPOL0 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              CONTINUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCalled());
            // COB_CODE: STRING 'CHIAMATA IDBSPOL0 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //                  DELIMITED BY SIZE INTO
            //                  IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA IDBSPOL0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER     TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: L460-LEGGI-ADESIONE<br>
	 * <pre>----------------------------------------------------------------*
	 * -- LETTURA DELL' ADESIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void l460LeggiAdesione() {
        Idbsade0 idbsade0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE ADES.
        initAdes();
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE      TO ADE-ID-ADES.
        ws.getAdes().setAdeIdAdes(ivvc0213.getIdAdesione());
        // COB_CODE: MOVE 'IDBSADE0'                TO WK-PGM-CALLED.
        ws.setWkPgmCalled("IDBSADE0");
        //  --> Tipo operazione
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        idsv0003.getOperazione().setSelect();
        //  --> Tipo livello
        // COB_CODE: SET IDSV0003-ID                TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011Id();
        // COB_CODE: CALL WK-PGM-CALLED USING IDSV0003 ADES
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER   TO TRUE
        //           END-CALL.
        try {
            idbsade0 = Idbsade0.getInstance();
            idbsade0.run(idsv0003, ws.getAdes());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCalled());
            // COB_CODE: MOVE 'CALL-IDBSADE0 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSADE0 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              CONTINUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCalled());
            // COB_CODE: STRING 'CHIAMATA IDBSADE0 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //                  DELIMITED BY SIZE INTO
            //                  IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA IDBSADE0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER     TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: LEGGI-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 * -- LETTURA DELLA PARAM MOVI
	 * ----------------------------------------------------------------*</pre>*/
    private void leggiPmo() {
        Ldbs7850 ldbs7850 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE LDBV7851
        initLdbv7851();
        // COB_CODE: INITIALIZE PARAM-MOVI
        initParamMovi();
        // COB_CODE: MOVE DTGA-ID-GAR(IVVC0213-IX-TABB)  TO LDBV7851-ID-OGG
        ws.getLdbv7851().setIdOgg(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaIdGar());
        // COB_CODE: MOVE 'GA'                           TO LDBV7851-TP-OGG
        ws.getLdbv7851().setTpOgg("GA");
        // COB_CODE: MOVE 06006                          TO LDBV7851-TP-MOVI-01
        ws.getLdbv7851().setTpMovi01(6006);
        // COB_CODE: MOVE ZERO                           TO LDBV7851-TP-MOVI-02
        ws.getLdbv7851().setTpMovi02(0);
        // COB_CODE: MOVE ZERO                           TO LDBV7851-TP-MOVI-03
        ws.getLdbv7851().setTpMovi03(0);
        // COB_CODE: MOVE LDBV7851                TO  IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond(ws.getLdbv7851().getLdbv7851Formatted());
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE 'LDBS7850'                TO WK-PGM-CALLED.
        ws.setWkPgmCalled("LDBS7850");
        //  --> Tipo operazione
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        idsv0003.getOperazione().setSelect();
        //  --> Tipo livello
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSV0003-WHERE-CONDITION       TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //    CALL WK-PGM-CALLED USING IDSV0003 LDBV7851
        // COB_CODE:      CALL WK-PGM-CALLED USING IDSV0003 PARAM-MOVI
        //           *    CALL WK-PGM-CALLED USING IDSV0003 LDBV7851
        //                ON EXCEPTION
        //                   SET IDSV0003-INVALID-OPER   TO TRUE
        //                END-CALL.
        try {
            ldbs7850 = Ldbs7850.getInstance();
            ldbs7850.run(idsv0003, ws.getParamMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCalled());
            // COB_CODE: MOVE 'CALL-IDBSPMO0 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSPMO0 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              SET OK-MOVI TO TRUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: SET OK-MOVI TO TRUE
            ws.getFlagMovimento().setOkMovi();
        }
        else {
            // COB_CODE: MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCalled());
            // COB_CODE: STRING 'CHIAMATA IDBSPMO0 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //                  DELIMITED BY SIZE INTO
            //                  IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA IDBSPMO0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER     TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-TRCH-GAR
        //                TO DTGA-AREA-TRCH
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTrchGar())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DTGA-AREA-TRCH
            ws.setDtgaAreaTrchFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1253-RECUP-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *  RECUPERO I DATI RELATIVI AL MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1253RecupMovi() {
        Ldbse060 ldbse060 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET KO-MOVI                     TO TRUE
        ws.getFlagMovimento().setKoMovi();
        // COB_CODE: MOVE ZEROES                     TO WK-MOV-DT-EFF
        //                                              WK-MOV-DS-TS-CPTZ
        ws.setWkMovDtEff(0);
        ws.setWkMovDsTsCptz(0);
        // COB_CODE: INITIALIZE MOVI
        initMovi();
        // COB_CODE: IF POL-TP-FRM-ASSVA = 'CO'
        //              MOVE WS-TP-OGG                 TO LDBVE061-TP-OGG
        //           ELSE
        //              MOVE WS-TP-OGG                 TO LDBVE061-TP-OGG
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolTpFrmAssva(), "CO")) {
            // COB_CODE: MOVE IVVC0213-ID-ADESIONE      TO LDBVE061-ID-OGG
            ws.getLdbve061().setIdOgg(ivvc0213.getIdAdesione());
            // COB_CODE: SET  ADESIONE                  TO TRUE
            ws.getWsTpOgg().setAdesione();
            // COB_CODE: MOVE WS-TP-OGG                 TO LDBVE061-TP-OGG
            ws.getLdbve061().setTpOgg(ws.getWsTpOgg().getWsTpOgg());
        }
        else {
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA       TO LDBVE061-ID-OGG
            ws.getLdbve061().setIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: SET  POLIZZA                   TO TRUE
            ws.getWsTpOgg().setPolizza();
            // COB_CODE: MOVE WS-TP-OGG                 TO LDBVE061-TP-OGG
            ws.getLdbve061().setTpOgg(ws.getWsTpOgg().getWsTpOgg());
        }
        // COB_CODE: SET ADPRE-PRESTA               TO TRUE
        ws.getWsMovimento().setAdprePresta();
        // COB_CODE: MOVE WS-MOVIMENTO              TO LDBVE061-TP-MOVI
        ws.getLdbve061().setTpMovi(ws.getWsMovimento().getWsMovimento());
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO  TO
        //                                     LDBVE061-DT-EFF-I
        ws.getLdbve061().setDtEffI(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE ZEROES TO                  IDSV0003-DATA-FINE-EFFETTO
        idsv0003.setDataFineEffetto(0);
        // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO MOV-DS-TS-CPTZ
        ws.getMovi().setMovDsTsCptz(idsv0003.getDataCompetenza());
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET  IDSV0003-TRATT-SENZA-STOR TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSV0003-WHERE-CONDITION   TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET  IDSV0003-SELECT           TO TRUE.
        idsv0003.getOperazione().setSelect();
        //
        //
        // COB_CODE:      CALL PGM-LDBSE060  USING  IDSV0003 LDBVE061
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER TO TRUE
        //                END-CALL.
        try {
            ldbse060 = Ldbse060.getInstance();
            ldbse060.run(idsv0003, ws.getLdbve061());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE PGM-LDBSE060
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getPgmLdbse060());
            // COB_CODE: MOVE 'CALL-LDBSE060 - ERRORE CHIAMATA'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSE060 - ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE  LDBVE061-DS-CPTZ-O    TO WK-MOV-DS-TS-CPTZ
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: SET OK-MOVI                 TO TRUE
            ws.getFlagMovimento().setOkMovi();
            // COB_CODE: MOVE  LDBVE061-DT-EFF-O     TO WK-MOV-DT-EFF
            ws.setWkMovDtEff(ws.getLdbve061().getDtEffO());
            // COB_CODE: MOVE  LDBVE061-DS-CPTZ-O    TO WK-MOV-DS-TS-CPTZ
            ws.setWkMovDsTsCptz(ws.getLdbve061().getDsCptzO());
        }
        else if (idsv0003.getSqlcode().getSqlcode() == 100) {
            // COB_CODE: IF IDSV0003-SQLCODE = +100
            //              SET KO-MOVI                    TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER       TO TRUE
            //           END-IF
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: MOVE ZERO                      TO IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(0);
            // COB_CODE: SET KO-MOVI                    TO TRUE
            ws.getFlagMovimento().setKoMovi();
        }
        else {
            // COB_CODE: MOVE PGM-LDBSE060        TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getPgmLdbse060());
            // COB_CODE: STRING 'CHIAMATA LDBSE060  '
            //               IDSV0003-RETURN-CODE ';'
            //               IDSV0003-SQLCODE
            //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBSE060  ", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: SET IDSV0003-INVALID-OPER       TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: S1300-CALL-LVVS0000<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIAMATA LVVS0000
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZA CODICE DI RITORNO</pre>*/
    private void s1300CallLvvs0000() {
        Lvvs0000 lvvs0000 = null;
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE 'LVVS0000'                TO WK-CALL-PGM.
        ws.setWkCallPgm("LVVS0000");
        //
        // COB_CODE: CALL WK-CALL-PGM USING         IDSV0003
        //                                          INPUT-LVVS0000.
        lvvs0000 = Lvvs0000.getInstance();
        lvvs0000.run(idsv0003, ws.getInputLvvs0000());
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               MOVE LVVC0000-DATA-OUTPUT  TO IVVC0213-VAL-IMP-O
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LVVC0000-DATA-OUTPUT  TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getInputLvvs0000().getDataOutput(), 18, 7));
        }
    }

    /**Original name: S1400-CALC-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLA DATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1400CalcData() {
        // COB_CODE: IF POL-TP-FRM-ASSVA = 'IN'
        //              END-IF
        //           ELSE
        //            END-IF
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolTpFrmAssva(), "IN")) {
            // COB_CODE: IF WK-MOV-DT-EFF >
            //              DTGA-DT-DECOR(IVVC0213-IX-TABB)
            //                     WK-DATA-OUT
            //           ELSE
            //                     WK-DATA-OUT
            //           END-IF
            if (ws.getWkMovDtEff() > ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecor()) {
                // COB_CODE: MOVE WK-MOV-DT-EFF
                //             TO IVVC0213-VAL-STR-O
                //                WK-DATA-OUT
                ivvc0213.getTabOutput().setValStrO(ws.getWkMovDtEffFormatted());
                ws.setWkDataOut(TruncAbs.toInt(ws.getWkMovDtEff(), 8));
            }
            else {
                // COB_CODE: MOVE DTGA-DT-DECOR(IVVC0213-IX-TABB)
                //             TO IVVC0213-VAL-STR-O
                //                WK-DATA-OUT
                ivvc0213.getTabOutput().setValStrO(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecorFormatted());
                ws.setWkDataOut(TruncAbs.toInt(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecor(), 8));
            }
        }
        else if (!Characters.EQ_HIGH.test(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePrNullFormatted()) && !Characters.EQ_LOW.test(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePrNullFormatted()) && !Characters.EQ_SPACE.test(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePrNull())) {
            // COB_CODE: IF DTGA-DT-ULT-ADEG-PRE-PR-NULL(IVVC0213-IX-TABB)
            //                                           NOT = HIGH-VALUES
            //                                             AND LOW-VALUES
            //                                             AND SPACES
            //               TO WK-DATA-OUT
            //           ELSE
            //               TO WK-DATA-OUT
            //           END-IF
            // COB_CODE: MOVE DTGA-DT-ULT-ADEG-PRE-PR(IVVC0213-IX-TABB)
            //             TO WK-DATA-OUT
            ws.setWkDataOut(TruncAbs.toInt(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePr(), 8));
        }
        else {
            // COB_CODE: MOVE PMO-DT-RICOR-SUCC
            //             TO WK-DATA-OUT
            ws.setWkDataOut(TruncAbs.toInt(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSucc(), 8));
        }
        // COB_CODE: EVALUATE IVVC0213-COD-VARIABILE-O
        //              WHEN 'DATULTADEGPL'
        //              WHEN 'DTULTADEGTL'
        //                 PERFORM S1300-CALL-LVVS0000   THRU S1300-EX
        //           END-EVALUATE.
        switch (ivvc0213.getTabOutput().getCodVariabileO()) {

            case "DATULTADEGPL":
            case "DTULTADEGTL":// COB_CODE: INITIALIZE INPUT-LVVS0000
                initInputLvvs0000();
                // COB_CODE: MOVE WK-DATA-OUT              TO LVVC0000-DATA-INPUT-1
                ws.getInputLvvs0000().setDataInput1Formatted(ws.getWkDataOutFormatted());
                // COB_CODE: PERFORM S1300-CALL-LVVS0000   THRU S1300-EX
                s1300CallLvvs0000();
                break;

            default:break;
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE IVVC0213-DATA-EFFETTO       TO
        //                IDSV0003-DATA-INIZIO-EFFETTO
        idsv0003.setDataInizioEffetto(ivvc0213.getDataEffetto());
        // COB_CODE: MOVE IVVC0213-DATA-COMPETENZA    TO
        //                IDSV0003-DATA-COMPETENZA
        idsv0003.setDataCompetenza(ivvc0213.getDataCompetenza());
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoTrch() {
        ws.setDtgaEleTrchMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0003Data.DTGA_TAB_TRAN_MAXOCCURS; idx0++) {
            ws.getDtgaTabTran(idx0).getLccvtga1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().setIdPtf(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdTrchDiGar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdGar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdAdes(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdPoli(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdMoviCrz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtIniEff(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtEndEff(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodCompAnia(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtDecor(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIbOgg("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRgmFisc("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpTrch("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlCarCont(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodDvs("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpAdegAbb(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaModCalc("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlImportiForz(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlProvForz(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRival("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpManfeeAppl("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsRiga(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsOperSql(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsVer(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsIniCptz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsEndCptz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsUtente("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsStatoElab(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
        }
    }

    public void initInputLvvs0000() {
        ws.getInputLvvs0000().getFormatDate().setFormatDate(Types.SPACE_CHAR);
        ws.getInputLvvs0000().setDataInputFormatted("00000000");
        ws.getInputLvvs0000().setDataInput1Formatted("00000000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000AnniInput2Formatted("00000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000MesiInput2Formatted("00000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000GiorniInput2Formatted("00000");
        ws.getInputLvvs0000().setAnniInput3Formatted("00000");
        ws.getInputLvvs0000().setMesiInput3Formatted("00000");
        ws.getInputLvvs0000().setDataOutput(new AfDecimal(0, 11, 7));
    }

    public void initPoli() {
        ws.getPoli().setPolIdPoli(0);
        ws.getPoli().setPolIdMoviCrz(0);
        ws.getPoli().getPolIdMoviChiu().setPolIdMoviChiu(0);
        ws.getPoli().setPolIbOgg("");
        ws.getPoli().setPolIbProp("");
        ws.getPoli().getPolDtProp().setPolDtProp(0);
        ws.getPoli().setPolDtIniEff(0);
        ws.getPoli().setPolDtEndEff(0);
        ws.getPoli().setPolCodCompAnia(0);
        ws.getPoli().setPolDtDecor(0);
        ws.getPoli().setPolDtEmis(0);
        ws.getPoli().setPolTpPoli("");
        ws.getPoli().getPolDurAa().setPolDurAa(0);
        ws.getPoli().getPolDurMm().setPolDurMm(0);
        ws.getPoli().getPolDtScad().setPolDtScad(0);
        ws.getPoli().setPolCodProd("");
        ws.getPoli().setPolDtIniVldtProd(0);
        ws.getPoli().setPolCodConv("");
        ws.getPoli().setPolCodRamo("");
        ws.getPoli().getPolDtIniVldtConv().setPolDtIniVldtConv(0);
        ws.getPoli().getPolDtApplzConv().setPolDtApplzConv(0);
        ws.getPoli().setPolTpFrmAssva("");
        ws.getPoli().setPolTpRgmFisc("");
        ws.getPoli().setPolFlEstas(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComun(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComunCond(Types.SPACE_CHAR);
        ws.getPoli().setPolTpLivGenzTit("");
        ws.getPoli().setPolFlCopFinanz(Types.SPACE_CHAR);
        ws.getPoli().setPolTpApplzDir("");
        ws.getPoli().getPolSpeMed().setPolSpeMed(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirEmis().setPolDirEmis(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDir1oVers().setPolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirVersAgg().setPolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolCodDvs("");
        ws.getPoli().setPolFlFntAz(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntAder(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntTfr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntVolo(Types.SPACE_CHAR);
        ws.getPoli().setPolTpOpzAScad("");
        ws.getPoli().getPolAaDiffProrDflt().setPolAaDiffProrDflt(0);
        ws.getPoli().setPolFlVerProd("");
        ws.getPoli().getPolDurGg().setPolDurGg(0);
        ws.getPoli().getPolDirQuiet().setPolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolTpPtfEstno("");
        ws.getPoli().setPolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getPoli().setPolConvGeco("");
        ws.getPoli().setPolDsRiga(0);
        ws.getPoli().setPolDsOperSql(Types.SPACE_CHAR);
        ws.getPoli().setPolDsVer(0);
        ws.getPoli().setPolDsTsIniCptz(0);
        ws.getPoli().setPolDsTsEndCptz(0);
        ws.getPoli().setPolDsUtente("");
        ws.getPoli().setPolDsStatoElab(Types.SPACE_CHAR);
        ws.getPoli().setPolFlScudoFisc(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTrasfe(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTfrStrc(Types.SPACE_CHAR);
        ws.getPoli().getPolDtPresc().setPolDtPresc(0);
        ws.getPoli().setPolCodConvAgg("");
        ws.getPoli().setPolSubcatProd("");
        ws.getPoli().setPolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getPoli().setPolCodTpa("");
        ws.getPoli().getPolIdAccComm().setPolIdAccComm(0);
        ws.getPoli().setPolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlPoliBundling(Types.SPACE_CHAR);
        ws.getPoli().setPolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getPoli().setPolFlVndBundle(Types.SPACE_CHAR);
        ws.getPoli().setPolIbBs("");
        ws.getPoli().setPolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initAdes() {
        ws.getAdes().setAdeIdAdes(0);
        ws.getAdes().setAdeIdPoli(0);
        ws.getAdes().setAdeIdMoviCrz(0);
        ws.getAdes().getAdeIdMoviChiu().setAdeIdMoviChiu(0);
        ws.getAdes().setAdeDtIniEff(0);
        ws.getAdes().setAdeDtEndEff(0);
        ws.getAdes().setAdeIbPrev("");
        ws.getAdes().setAdeIbOgg("");
        ws.getAdes().setAdeCodCompAnia(0);
        ws.getAdes().getAdeDtDecor().setAdeDtDecor(0);
        ws.getAdes().getAdeDtScad().setAdeDtScad(0);
        ws.getAdes().getAdeEtaAScad().setAdeEtaAScad(0);
        ws.getAdes().getAdeDurAa().setAdeDurAa(0);
        ws.getAdes().getAdeDurMm().setAdeDurMm(0);
        ws.getAdes().getAdeDurGg().setAdeDurGg(0);
        ws.getAdes().setAdeTpRgmFisc("");
        ws.getAdes().setAdeTpRiat("");
        ws.getAdes().setAdeTpModPagTit("");
        ws.getAdes().setAdeTpIas("");
        ws.getAdes().getAdeDtVarzTpIas().setAdeDtVarzTpIas(0);
        ws.getAdes().getAdePreNetInd().setAdePreNetInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePreLrdInd().setAdePreLrdInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeRatLrdInd().setAdeRatLrdInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePrstzIniInd().setAdePrstzIniInd(new AfDecimal(0, 15, 3));
        ws.getAdes().setAdeFlCoincAssto(Types.SPACE_CHAR);
        ws.getAdes().setAdeIbDflt("");
        ws.getAdes().setAdeModCalc("");
        ws.getAdes().setAdeTpFntCnbtva("");
        ws.getAdes().getAdeImpAz().setAdeImpAz(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpAder().setAdeImpAder(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpTfr().setAdeImpTfr(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpVolo().setAdeImpVolo(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePcAz().setAdePcAz(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcAder().setAdePcAder(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcTfr().setAdePcTfr(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcVolo().setAdePcVolo(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdeDtNovaRgmFisc().setAdeDtNovaRgmFisc(0);
        ws.getAdes().setAdeFlAttiv(Types.SPACE_CHAR);
        ws.getAdes().getAdeImpRecRitVis().setAdeImpRecRitVis(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpRecRitAcc().setAdeImpRecRitAcc(new AfDecimal(0, 15, 3));
        ws.getAdes().setAdeFlVarzStatTbgc(Types.SPACE_CHAR);
        ws.getAdes().setAdeFlProvzaMigraz(Types.SPACE_CHAR);
        ws.getAdes().getAdeImpbVisDaRec().setAdeImpbVisDaRec(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeDtDecorPrestBan().setAdeDtDecorPrestBan(0);
        ws.getAdes().getAdeDtEffVarzStatT().setAdeDtEffVarzStatT(0);
        ws.getAdes().setAdeDsRiga(0);
        ws.getAdes().setAdeDsOperSql(Types.SPACE_CHAR);
        ws.getAdes().setAdeDsVer(0);
        ws.getAdes().setAdeDsTsIniCptz(0);
        ws.getAdes().setAdeDsTsEndCptz(0);
        ws.getAdes().setAdeDsUtente("");
        ws.getAdes().setAdeDsStatoElab(Types.SPACE_CHAR);
        ws.getAdes().getAdeCumCnbtCap().setAdeCumCnbtCap(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpGarCnbt().setAdeImpGarCnbt(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeDtUltConsCnbt().setAdeDtUltConsCnbt(0);
        ws.getAdes().setAdeIdenIscFnd("");
        ws.getAdes().getAdeNumRatPian().setAdeNumRatPian(new AfDecimal(0, 12, 5));
        ws.getAdes().getAdeDtPresc().setAdeDtPresc(0);
        ws.getAdes().setAdeConcsPrest(Types.SPACE_CHAR);
    }

    public void initLdbv7851() {
        ws.getLdbv7851().setIdOgg(0);
        ws.getLdbv7851().setTpOgg("");
        ws.getLdbv7851().setTpMovi01(0);
        ws.getLdbv7851().setTpMovi02(0);
        ws.getLdbv7851().setTpMovi03(0);
    }

    public void initParamMovi() {
        ws.getParamMovi().setPmoIdParamMovi(0);
        ws.getParamMovi().setPmoIdOgg(0);
        ws.getParamMovi().setPmoTpOgg("");
        ws.getParamMovi().setPmoIdMoviCrz(0);
        ws.getParamMovi().getPmoIdMoviChiu().setPmoIdMoviChiu(0);
        ws.getParamMovi().setPmoDtIniEff(0);
        ws.getParamMovi().setPmoDtEndEff(0);
        ws.getParamMovi().setPmoCodCompAnia(0);
        ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(0);
        ws.getParamMovi().getPmoFrqMovi().setPmoFrqMovi(0);
        ws.getParamMovi().getPmoDurAa().setPmoDurAa(0);
        ws.getParamMovi().getPmoDurMm().setPmoDurMm(0);
        ws.getParamMovi().getPmoDurGg().setPmoDurGg(0);
        ws.getParamMovi().getPmoDtRicorPrec().setPmoDtRicorPrec(0);
        ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSucc(0);
        ws.getParamMovi().getPmoPcIntrFraz().setPmoPcIntrFraz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpBnsDaSco().setPmoImpBnsDaSco(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcAnticBns().setPmoPcAnticBns(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoTpRinnColl("");
        ws.getParamMovi().setPmoTpRivalPre("");
        ws.getParamMovi().setPmoTpRivalPrstz("");
        ws.getParamMovi().setPmoFlEvidRival(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoUltPcPerd().setPmoUltPcPerd(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoTotAaGiaPror().setPmoTotAaGiaPror(0);
        ws.getParamMovi().setPmoTpOpz("");
        ws.getParamMovi().getPmoAaRenCer().setPmoAaRenCer(0);
        ws.getParamMovi().getPmoPcRevrsb().setPmoPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpLrdDiRat().setPmoImpLrdDiRat(new AfDecimal(0, 15, 3));
        ws.getParamMovi().setPmoIbOgg("");
        ws.getParamMovi().getPmoCosOner().setPmoCosOner(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoSpePc().setPmoSpePc(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoFlAttivGar(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCambioVerProd(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoMmDiff().setPmoMmDiff(((short)0));
        ws.getParamMovi().getPmoImpRatManfee().setPmoImpRatManfee(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoDtUltErogManfee().setPmoDtUltErogManfee(0);
        ws.getParamMovi().setPmoTpOggRival("");
        ws.getParamMovi().getPmoSomAsstaGarac().setPmoSomAsstaGarac(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcApplzOpz().setPmoPcApplzOpz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoIdAdes().setPmoIdAdes(0);
        ws.getParamMovi().setPmoIdPoli(0);
        ws.getParamMovi().setPmoTpFrmAssva("");
        ws.getParamMovi().setPmoDsRiga(0);
        ws.getParamMovi().setPmoDsOperSql(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoDsVer(0);
        ws.getParamMovi().setPmoDsTsIniCptz(0);
        ws.getParamMovi().setPmoDsTsEndCptz(0);
        ws.getParamMovi().setPmoDsUtente("");
        ws.getParamMovi().setPmoDsStatoElab(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoTpEstrCnt("");
        ws.getParamMovi().setPmoCodRamo("");
        ws.getParamMovi().setPmoGenDaSin(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCodTari("");
        ws.getParamMovi().getPmoNumRatPagPre().setPmoNumRatPagPre(0);
        ws.getParamMovi().getPmoPcServVal().setPmoPcServVal(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficr(((short)0));
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }
}
