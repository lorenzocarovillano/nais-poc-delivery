package it.accenture.jnais;

import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.date.CalendarUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Ispc0211DatiInput;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaIoIsps0211;
import it.accenture.jnais.ws.AreaPassaggio;
import it.accenture.jnais.ws.enums.WsTpDato;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Loas0660Data;
import it.accenture.jnais.ws.occurs.Ispc0211TabErrori;
import it.accenture.jnais.ws.ptr.Idsv8888StrPerformanceDbg;
import it.accenture.jnais.ws.redefines.Ispc0211TabValP;
import it.accenture.jnais.ws.redefines.Ispc0211TabValT;
import it.accenture.jnais.ws.redefines.WtgaPcCommisGest;
import it.accenture.jnais.ws.redefines.WtgaPreInvrioUlt;
import it.accenture.jnais.ws.redefines.WtgaPreUniRivto;
import it.accenture.jnais.ws.W660AreaPag;
import it.accenture.jnais.ws.WadeAreaAdesioneLoas0800;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.WmovAreaMovimento;
import it.accenture.jnais.ws.WpmoAreaParamMoviLoas0800;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WskdAreaScheda;
import it.accenture.jnais.ws.WtgaAreaTrancheLoas0800;
import javax.inject.Inject;
import static com.bphx.ctu.af.lang.types.AfDecimal.abs;

/**Original name: LOAS0660<br>
 * <pre>  ============================================================= *
 *                                                                 *
 *         PORTAFOGLIO VITA ITALIA  VER 1.0                        *
 *                                                                 *
 *   ============================================================= *
 * AUTHOR.             AIS&S.
 * DATE-WRITTEN.       GENNAIO 2009.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *      PROGRAMMA ..... LOAS0660                                   *
 *      TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
 *      PROCESSO....... ADEGUAMENTO PREMIO PRESTAZIONE             *
 *      FUNZIONE....... BATCH                                      *
 *      DESCRIZIONE.... SERVIZIO DI CALCOLO ADEGUAMENTO PREMIO     *
 *                      PRESTAZIONE                                *
 *      PAGINA WEB..... N.A.                                       *
 * ----------------------------------------------------------------*</pre>*/
public class Loas0660 extends Program {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    //Original name: WORKING-STORAGE
    private Loas0660Data ws = new Loas0660Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-IO-STATI
    private AreaPassaggio wcomIoStati;
    //Original name: WPMO-AREA-PARAM-MOVI
    private WpmoAreaParamMoviLoas0800 wpmoAreaParamMovi;
    //Original name: WMOV-AREA-MOVIMENTO
    private WmovAreaMovimento wmovAreaMovimento;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLoas0800 wadeAreaAdesione;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTrancheLoas0800 wtgaAreaTranche;
    //Original name: WSKD-AREA-SCHEDA
    private WskdAreaScheda wskdAreaScheda;
    //Original name: W660-AREA-PAG
    private W660AreaPag w660AreaPag;

    //==== METHODS ====
    /**Original name: PROGRAM_LOAS0660_FIRST_SENTENCES<br>
	 * <pre>    DISPLAY '-->LOAS0660'</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, AreaPassaggio wcomIoStati, WpmoAreaParamMoviLoas0800 wpmoAreaParamMovi, WmovAreaMovimento wmovAreaMovimento, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WadeAreaAdesioneLoas0800 wadeAreaAdesione, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WtgaAreaTrancheLoas0800 wtgaAreaTranche, WskdAreaScheda wskdAreaScheda, W660AreaPag w660AreaPag) {
        this.areaIdsv0001 = areaIdsv0001;
        this.wcomIoStati = wcomIoStati;
        this.wpmoAreaParamMovi = wpmoAreaParamMovi;
        this.wmovAreaMovimento = wmovAreaMovimento;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaTranche = wtgaAreaTranche;
        this.wskdAreaScheda = wskdAreaScheda;
        this.w660AreaPag = w660AreaPag;
        // COB_CODE: PERFORM S00000-OPERAZ-INIZIALI
        //              THRU S00000-OPERAZ-INIZIALI-EX.
        s00000OperazIniziali();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10000-ELABORAZIONE-EX
        //           *
        //                END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10000-ELABORAZIONE
            //              THRU S10000-ELABORAZIONE-EX
            s10000Elaborazione();
            //
        }
        //
        // COB_CODE: PERFORM S90000-OPERAZ-FINALI
        //              THRU S90000-OPERAZ-FINALI-EX.
        s90000OperazFinali();
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Loas0660 getInstance() {
        return ((Loas0660)Programs.getInstance(Loas0660.class));
    }

    /**Original name: S00000-OPERAZ-INIZIALI<br>
	 * <pre> ============================================================== *
	 *  -->           O P E R Z I O N I   I N I Z I A L I          <-- *
	 *  ============================================================== *</pre>*/
    private void s00000OperazIniziali() {
        // COB_CODE: MOVE 'S00000-OPERAZ-INIZIALI'   TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S00000-OPERAZ-INIZIALI");
        // COB_CODE: PERFORM S00050-INIZIALIZZA-WORK
        //              THRU S00050-INIZIALIZZA-WORK-EX.
        s00050InizializzaWork();
        // COB_CODE: PERFORM S00100-CTRL-INPUT
        //              THRU S00100-CTRL-INPUT-EX.
        s00100CtrlInput();
    }

    /**Original name: S00050-INIZIALIZZA-WORK<br>
	 * <pre>----------------------------------------------------------------*
	 *                         CONTROLLO DATI DI INPUT                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00050InizializzaWork() {
        // COB_CODE: MOVE 'S00050-INIZIALIZZA-WORK'   TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00050-INIZIALIZZA-WORK");
        // COB_CODE: INITIALIZE               IX-INDICI.
        initIxIndici();
        // COB_CODE: SET WK-MANFEE-NO
        //             TO TRUE
        ws.getWkManfee().setNo();
        // COB_CODE: ACCEPT WK-CURRENT-DATE
        //             FROM DATE YYYYMMDD.
        ws.setWkCurrentDateFormatted(CalendarUtil.getDateYYYYMMDD());
    }

    /**Original name: S00100-CTRL-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *                         CONTROLLO DATI DI INPUT                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00100CtrlInput() {
        // COB_CODE: MOVE 'S00010-CTRL-INPUT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00010-CTRL-INPUT");
        //
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //             TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE:      IF NOT ADPRE-PRESTA
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (!ws.getWsMovimento().isAdprePresta()) {
            //
            // COB_CODE: IF IDSV0001-TIPO-MOVIMENTO = 6996
            //              CONTINUE
            //           ELSE
            //              THRU GESTIONE-ERR-STD-EX
            //           END-IF
            if (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6996) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE 'TIPO MOVIMENTO ERRATO'
                //             TO WK-STRING
                ws.getWkGestioneMsgErr().setStringFld("TIPO MOVIMENTO ERRATO");
                // COB_CODE: MOVE '001114'
                //             TO WK-COD-ERR
                ws.getWkGestioneMsgErr().setCodErr("001114");
                // COB_CODE: PERFORM GESTIONE-ERR-STD
                //              THRU GESTIONE-ERR-STD-EX
                gestioneErrStd();
            }
            //
        }
        //
        // COB_CODE:      IF WPMO-ELE-PARAM-MOV-MAX = 0
        //           *
        //                   END-IF
        //                END-IF.
        if (wpmoAreaParamMovi.getEleParamMovMax() == 0) {
            //
            // COB_CODE:         IF IDSV0001-TIPO-MOVIMENTO = 6996
            //                      CONTINUE
            //                   ELSE
            //                      THRU GESTIONE-ERR-STD-EX
            //           *
            //                   END-IF
            if (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6996) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE 'NESSUNA OCCORRENZA DI PARAM. MOV. ELABORABILE'
                //             TO WK-STRING
                ws.getWkGestioneMsgErr().setStringFld("NESSUNA OCCORRENZA DI PARAM. MOV. ELABORABILE");
                // COB_CODE: MOVE '001114'
                //             TO WK-COD-ERR
                ws.getWkGestioneMsgErr().setCodErr("001114");
                // COB_CODE: PERFORM GESTIONE-ERR-STD
                //              THRU GESTIONE-ERR-STD-EX
                gestioneErrStd();
                //
            }
        }
        // COB_CODE:      IF WMOV-ELE-MOVI-MAX = 0
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (wmovAreaMovimento.getWmovEleMovMax() == 0) {
            //
            // COB_CODE: MOVE 'OCCORRENZA DI MOVIMENTO NON VALORIZZATA'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("OCCORRENZA DI MOVIMENTO NON VALORIZZATA");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
        // COB_CODE:      IF WPOL-ELE-POLI-MAX = 0
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (wpolAreaPolizza.getWpolElePoliMax() == 0) {
            //
            // COB_CODE: MOVE 'OCCORRENZA DI POLIZZA NON VALORIZZATA'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("OCCORRENZA DI POLIZZA NON VALORIZZATA");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
        // COB_CODE:      IF WADE-ELE-ADES-MAX = 0
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (wadeAreaAdesione.getEleAdesMax() == 0) {
            //
            // COB_CODE: MOVE 'OCCORRENZA DI ADESIONE NON VALORIZZATA'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("OCCORRENZA DI ADESIONE NON VALORIZZATA");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
        // COB_CODE:      IF WGRZ-ELE-GAR-MAX = 0
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (wgrzAreaGaranzia.getEleGarMax() == 0) {
            //
            // COB_CODE: MOVE 'NESSUNA OCCORRENZA DI GARANZIA ELABORABILE'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("NESSUNA OCCORRENZA DI GARANZIA ELABORABILE");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
        // COB_CODE:      IF WTGA-ELE-TRAN-MAX = 0
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (wtgaAreaTranche.getEleTranMax() == 0) {
            //
            // COB_CODE: MOVE 'NESSUNA OCCORRENZA DI TRANCHE ELABORABILE'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("NESSUNA OCCORRENZA DI TRANCHE ELABORABILE");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
        // COB_CODE:      IF  WSKD-ELE-LIVELLO-MAX-P = 0
        //                AND WSKD-ELE-LIVELLO-MAX-T = 0
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (wskdAreaScheda.getWskdEleLivelloMaxP() == 0 && wskdAreaScheda.getWskdEleLivelloMaxT() == 0) {
            //
            // COB_CODE: MOVE 'AREA VARIABILI NON VALORIZZATA'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("AREA VARIABILI NON VALORIZZATA");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S10000-ELABORAZIONE<br>
	 * <pre> ============================================================== *
	 *  -->               E L A B O R A Z I O N E                  <-- *
	 *  ============================================================== *</pre>*/
    private void s10000Elaborazione() {
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10600-GESTIONE-ISPS0211-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10600-GESTIONE-ISPS0211
            //              THRU S10600-GESTIONE-ISPS0211-EX
            s10600GestioneIsps0211();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: IF WK-MANFEE-YES
            //              END-IF
            //           END-IF
            if (ws.getWkManfee().isYes()) {
                // COB_CODE:            IF IDSV0001-TIPO-MOVIMENTO = 6996
                //                         CONTINUE
                //                      ELSE
                //           *
                //                         THRU S10700-CALL-LOAS0800-EX
                //           *
                //                      END-IF
                if (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6996) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    //
                    // COB_CODE: SET W660-MF-CALCOLATO-SI
                    //             TO TRUE
                    w660AreaPag.getMfCalcolato().setW660MfCalcolatoSi();
                    //
                    // COB_CODE: PERFORM S10700-CALL-LOAS0800
                    //              THRU S10700-CALL-LOAS0800-EX
                    s10700CallLoas0800();
                    //
                }
            }
            //
        }
    }

    /**Original name: S10600-GESTIONE-ISPS0211<br>
	 * <pre>----------------------------------------------------------------*
	 *   GESTIONE CHIAMATA AL SERVIZIO CALC. NOTEVOLI E CONTR. DI LIQ. *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10600GestioneIsps0211() {
        // COB_CODE: MOVE 'S10600-GESTIONE-ISPS0211'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10600-GESTIONE-ISPS0211");
        // COB_CODE: PERFORM S10610-PREPARA-ISPS0211
        //              THRU S10610-PREPARA-ISPS0211-EX.
        s10610PreparaIsps0211();
        //
        // COB_CODE: PERFORM S10615-CALL-ISPS0211
        //              THRU S10615-CALL-ISPS0211-EX.
        s10615CallIsps0211();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //           *  -->    Routine valorizzazione dclgen tabelle con
        //           *  -->    l'output del servizio ISPS0211
        //           *
        //                        OR IDSV0001-ESITO-KO
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            //  -->    Routine valorizzazione dclgen tabelle con
            //  -->    l'output del servizio ISPS0211
            //
            // COB_CODE: PERFORM S10640-SCORRI-OUT-ISPS0211
            //              THRU S10640-SCORRI-OUT-ISPS0211-EX
            //           VARYING IX-AREA-ISPC0211 FROM 1 BY 1
            //             UNTIL IX-AREA-ISPC0211 >
            //                   ISPC0211-ELE-MAX-SCHEDA-T
            //                OR IDSV0001-ESITO-KO
            ws.getIxIndici().setAreaIspc0211(((short)1));
            while (!(ws.getIxIndici().getAreaIspc0211() > ws.getAreaIoIsps0211().getIspc0211EleMaxSchedaT() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                s10640ScorriOutIsps0211();
                ws.getIxIndici().setAreaIspc0211(Trunc.toShort(ws.getIxIndici().getAreaIspc0211() + 1, 4));
            }
            //
        }
    }

    /**Original name: S10610-PREPARA-ISPS0211<br>
	 * <pre>----------------------------------------------------------------*
	 *          VALORIZZAZIONE DELL'INPUT DEL SERVIZIO ISPS0211        *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10610PreparaIsps0211() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S10610-PREPARA-ISPS0211' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S10610-PREPARA-ISPS0211");
        //
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0660'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0660");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE 'ISPC0211'                 TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("ISPC0211");
        // COB_CODE: STRING 'Iniz.tab. work ISPC0211'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Iniz.tab. work ISPC0211");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        //
        //    INITIALIZE AREA-IO-ISPS0211.
        // COB_CODE: INITIALIZE ISPC0211-DATI-INPUT
        //                      ISPC0211-ELE-MAX-SCHEDA-P
        //                      ISPC0211-ELE-MAX-SCHEDA-T
        //                      ISPC0211-AREA-ERRORI
        //                      ISPC0211-TIPO-LIVELLO-P      (1)
        //                      ISPC0211-TIPO-LIVELLO-T      (1)
        //                      ISPC0211-CODICE-LIVELLO-P    (1)
        //                      ISPC0211-CODICE-LIVELLO-T    (1)
        //                      ISPC0211-IDENT-LIVELLO-P     (1)
        //                      ISPC0211-IDENT-LIVELLO-T     (1)
        //                      ISPC0211-DT-INIZ-TARI        (1)
        //                      ISPC0211-DT-INIZ-PROD        (1)
        //                      ISPC0211-COD-RGM-FISC        (1)
        //                      ISPC0211-DT-DECOR-TRCH       (1)
        //                      ISPC0211-FLG-LIQ-P           (1)
        //                      ISPC0211-FLG-LIQ-T           (1)
        //                      ISPC0211-CODICE-OPZIONE-P    (1)
        //                      ISPC0211-CODICE-OPZIONE-T    (1)
        //                      ISPC0211-NUM-COMPON-MAX-ELE-P(1)
        //                      ISPC0211-NUM-COMPON-MAX-ELE-T(1)
        initIspc0211DatiInput();
        ws.getAreaIoIsps0211().setIspc0211EleMaxSchedaPFormatted("0000");
        ws.getAreaIoIsps0211().setIspc0211EleMaxSchedaTFormatted("0000");
        initIspc0211AreaErrori();
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211TipoLivelloP(1, Types.SPACE_CHAR);
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211TipoLivelloT(1, Types.SPACE_CHAR);
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211CodiceLivelloP(1, "");
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211CodiceLivelloT(1, "");
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211IdentLivelloPFormatted(1, "000000000");
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211IdentLivelloTFormatted(1, "000000000");
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211DtInizTari(1, "");
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211DtInizProd(1, "");
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211CodRgmFiscFormatted(1, "00");
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211DtDecorTrch(1, "");
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211FlgLiqP(1, "");
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211FlgLiqT(1, "");
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211CodiceOpzioneP(1, "");
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211CodiceOpzioneT(1, "");
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211NumComponMaxElePFormatted(1, "000");
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211NumComponMaxEleTFormatted(1, "0000");
        // COB_CODE: PERFORM VARYING IX-TAB-ISPS0211 FROM 1 BY 1
        //                   UNTIL   IX-TAB-ISPS0211 >
        //                           WK-ISPC0211-NUM-COMPON-MAX-P
        //                               (1,IX-TAB-ISPS0211)
        //           END-PERFORM
        ws.getIxIndici().setTabIsps0211(((short)1));
        while (!(ws.getIxIndici().getTabIsps0211() > ws.getWkIspcMax().getIspc0211NumComponMaxP())) {
            // COB_CODE: MOVE SPACES  TO  ISPC0211-CODICE-VARIABILE-P
            //                            (1,IX-TAB-ISPS0211)
            //                            ISPC0211-TIPO-DATO-P
            //                            (1,IX-TAB-ISPS0211)
            //                            ISPC0211-VAL-GENERICO-P
            //                            (1,IX-TAB-ISPS0211)
            ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211CodiceVariabileP(1, ws.getIxIndici().getTabIsps0211(), "");
            ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211TipoDatoP(1, ws.getIxIndici().getTabIsps0211(), Types.SPACE_CHAR);
            ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211ValGenericoP(1, ws.getIxIndici().getTabIsps0211(), "");
            ws.getIxIndici().setTabIsps0211(Trunc.toShort(ws.getIxIndici().getTabIsps0211() + 1, 4));
        }
        // COB_CODE: PERFORM VARYING IX-TAB-ISPS0211 FROM 1 BY 1
        //                   UNTIL   IX-TAB-ISPS0211 >
        //                           WK-ISPC0211-NUM-COMPON-MAX-T
        //                               (1,IX-TAB-ISPS0211)
        //           END-PERFORM
        ws.getIxIndici().setTabIsps0211(((short)1));
        while (!(ws.getIxIndici().getTabIsps0211() > ws.getWkIspcMax().getIspc0211NumComponMaxT())) {
            // COB_CODE: MOVE SPACES  TO  ISPC0211-CODICE-VARIABILE-T
            //                            (1,IX-TAB-ISPS0211)
            //                            ISPC0211-TIPO-DATO-T
            //                            (1,IX-TAB-ISPS0211)
            //                            ISPC0211-VAL-GENERICO-T
            //                            (1,IX-TAB-ISPS0211)
            ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211CodiceVariabileT(1, ws.getIxIndici().getTabIsps0211(), "");
            ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211TipoDatoT(1, ws.getIxIndici().getTabIsps0211(), Types.SPACE_CHAR);
            ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211ValGenericoT(1, ws.getIxIndici().getTabIsps0211(), "");
            ws.getIxIndici().setTabIsps0211(Trunc.toShort(ws.getIxIndici().getTabIsps0211() + 1, 4));
        }
        // COB_CODE: MOVE ISPC0211-TAB-SCHEDA-R-P TO ISPC0211-RESTO-TAB-SCHEDA-P
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211RestoTabSchedaP(ws.getAreaIoIsps0211().getIspc0211TabValP().getIspc0211TabSchedaRPFormatted());
        // COB_CODE: MOVE ISPC0211-TAB-SCHEDA-R-T TO ISPC0211-RESTO-TAB-SCHEDA-T
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211RestoTabSchedaT(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211TabSchedaRTFormatted());
        //
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0660'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0660");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE 'ISPC0211'                 TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("ISPC0211");
        // COB_CODE: STRING 'Iniz.tab. work ISPC0211'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Iniz.tab. work ISPC0211");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO ISPC0211-COD-COMPAGNIA.
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setIspc0211CodCompagniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        //
        // COB_CODE: MOVE WPOL-COD-PROD
        //             TO ISPC0211-COD-PRODOTTO.
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setCodProdotto(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodProd());
        //
        // COB_CODE:      IF WPOL-COD-CONV-NULL = HIGH-VALUES
        //           *
        //                     TO ISPC0211-COD-CONVENZIONE
        //           *
        //                ELSE
        //           *
        //                     TO ISPC0211-COD-CONVENZIONE
        //           *
        //                END-IF.
        if (Characters.EQ_HIGH.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodConvFormatted())) {
            //
            // COB_CODE: MOVE SPACES
            //             TO ISPC0211-COD-CONVENZIONE
            ws.getAreaIoIsps0211().getIspc0211DatiInput().setCodConvenzione("");
            //
        }
        else {
            //
            // COB_CODE: MOVE WPOL-COD-CONV
            //             TO ISPC0211-COD-CONVENZIONE
            ws.getAreaIoIsps0211().getIspc0211DatiInput().setCodConvenzione(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodConv());
            //
        }
        // COB_CODE: MOVE WSKD-DEE                  TO ISPC0211-DEE
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setDee(wskdAreaScheda.getWskdDee());
        // COB_CODE: IF WPOL-IB-OGG-NULL = HIGH-VALUES
        //                TO ISPC0211-NUM-POLIZZA
        //           ELSE
        //                TO ISPC0211-NUM-POLIZZA
        //           END-IF.
        if (Characters.EQ_HIGH.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbOgg(), WpolDati.Len.WPOL_IB_OGG)) {
            // COB_CODE: MOVE SPACES
            //             TO ISPC0211-NUM-POLIZZA
            ws.getAreaIoIsps0211().getIspc0211DatiInput().setNumPolizza("");
        }
        else {
            // COB_CODE: MOVE WPOL-IB-OGG
            //             TO ISPC0211-NUM-POLIZZA
            ws.getAreaIoIsps0211().getIspc0211DatiInput().setNumPolizza(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbOgg());
        }
        //
        // COB_CODE:      IF WPOL-DT-INI-VLDT-CONV-NULL = HIGH-VALUE
        //           *
        //                     TO ISPC0211-DATA-INIZ-VALID-CONV
        //           *
        //                ELSE
        //           *
        //                     TO ISPC0211-DATA-INIZ-VALID-CONV
        //           *
        //                END-IF.
        if (Characters.EQ_HIGH.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtIniVldtConv().getWpolDtIniVldtConvNullFormatted())) {
            //
            // COB_CODE: MOVE SPACES
            //             TO ISPC0211-DATA-INIZ-VALID-CONV
            ws.getAreaIoIsps0211().getIspc0211DatiInput().setDataInizValidConv("");
            //
        }
        else {
            //
            // COB_CODE: MOVE WPOL-DT-INI-VLDT-CONV
            //             TO ISPC0211-DATA-INIZ-VALID-CONV
            ws.getAreaIoIsps0211().getIspc0211DatiInput().setDataInizValidConv(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtIniVldtConv().getWpolDtIniVldtConvFormatted());
            //
        }
        //
        // COB_CODE: MOVE WCOM-DT-ULT-VERS-PROD
        //             TO ISPC0211-DATA-RIFERIMENTO.
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setDataRiferimento(wcomIoStati.getLccc0001().getWcomDtUltVersProdFormatted());
        //
        // COB_CODE: MOVE WPOL-DT-DECOR
        //             TO ISPC0211-DATA-DECORR-POLIZZA.
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setDataDecorrPolizza(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtDecorFormatted());
        //
        // COB_CODE: MOVE WCOM-COD-LIV-AUT-PROFIL
        //             TO ISPC0211-LIVELLO-UTENTE.
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setIspc0211LivelloUtente(TruncAbs.toShort(wcomIoStati.getLccc0001().getDatiDeroghe().getCodLivAutProfil(), 2));
        //
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //             TO ISPC0211-SESSION-ID.
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setSessionId(areaIdsv0001.getAreaComune().getSessione());
        //
        // COB_CODE: MOVE 'N'
        //             TO ISPC0211-FLG-REC-PROV.
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setIspc0211FlgRecProvFormatted("N");
        //
        // COB_CODE: MOVE WS-MOVIMENTO
        //             TO LCCV0021-TP-MOV-PTF.
        ws.getLccv0021().setTpMovPtfFormatted(ws.getWsMovimento().getWsMovimentoFormatted());
        //
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LCCS0020'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0020");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Lett. matrice movimento'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Lett. matrice movimento");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE: PERFORM CALL-MATR-MOVIMENTO
        //              THRU CALL-MATR-MOVIMENTO-EX.
        callMatrMovimento();
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LCCS0020'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0020");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Lett. matrice movimento'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Lett. matrice movimento");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                     TO ISPC0211-FUNZIONALITA
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: MOVE LCCV0021-TP-MOV-ACT
            //             TO ISPC0211-FUNZIONALITA
            ws.getAreaIoIsps0211().getIspc0211DatiInput().setIspc0211FunzionalitaFormatted(ws.getLccv0021().getAreaOutput().getLccv0021TpMovActFormatted());
            //
        }
        // COB_CODE: PERFORM VAL-SCHEDE-ISPC0211
        //              THRU VAL-SCHEDE-ISPC0211-EX.
        valSchedeIspc0211();
    }

    /**Original name: S10615-CALL-ISPS0211<br>
	 * <pre>----------------------------------------------------------------*
	 *                 CHIAMATA AL SERVIZIO - ISPS0211 -               *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10615CallIsps0211() {
        Isps0211 isps0211 = null;
        // COB_CODE: MOVE 'S10615-CALL-ISPS0211' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S10615-CALL-ISPS0211");
        //
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'ISPS0211'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("ISPS0211");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Serv. calcoli notevoli e liq'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv. calcoli notevoli e liq");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE:      CALL ISPS0211          USING AREA-IDSV0001
        //                                             WCOM-AREA-STATI
        //                                             AREA-IO-ISPS0211
        //                ON EXCEPTION
        //           *
        //                         THRU GESTIONE-ERR-SIST-EX
        //           *
        //                END-CALL.
        try {
            isps0211 = Isps0211.getInstance();
            isps0211.run(areaIdsv0001, wcomIoStati, ws.getAreaIoIsps0211());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'ISPS0211'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("ISPS0211");
            // COB_CODE: MOVE 'CALCOLI - ISPS0211'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("CALCOLI - ISPS0211");
            // COB_CODE: MOVE 'CALL-ISPS0211'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("CALL-ISPS0211");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
            //
        }
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'ISPS0211'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("ISPS0211");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Serv. calcoli notevoli e liq'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv. calcoli notevoli e liq");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: S10640-SCORRI-OUT-ISPS0211<br>
	 * <pre>----------------------------------------------------------------*
	 *      GESTIONE DELL'OUTPUT DEL SERVIZIO DI PRODOTTO ISPS0211     *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10640ScorriOutIsps0211() {
        // COB_CODE: MOVE 'S10640-SCORRI-OUT-ISPS0211' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S10640-SCORRI-OUT-ISPS0211");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
        displayLabel();
        //
        // COB_CODE:      IF ISPC0211-TIPO-LIVELLO-T  (IX-AREA-ISPC0211) = 'G' AND
        //                   ISPC0211-IDENT-LIVELLO-T (IX-AREA-ISPC0211) >  0  AND
        //                   ISPC0211-FLG-LIQ-T       (IX-AREA-ISPC0211) = 'NO'
        //           *
        //                      THRU S10650-ALLINEA-AREA-TRANCHE-EX
        //           *
        //                END-IF.
        if (ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211TipoLivelloT(ws.getIxIndici().getAreaIspc0211()) == 'G' && ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211IdentLivelloT(ws.getIxIndici().getAreaIspc0211()) > 0 && Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211FlgLiqT(ws.getIxIndici().getAreaIspc0211()), "NO")) {
            //
            // COB_CODE: PERFORM S10650-ALLINEA-AREA-TRANCHE
            //              THRU S10650-ALLINEA-AREA-TRANCHE-EX
            s10650AllineaAreaTranche();
            //
        }
    }

    /**Original name: S10650-ALLINEA-AREA-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *      GESTIONE DELL'OUTPUT DEL SERVIZIO DI GARANZIA ISPS0211     *
	 *     - DETT. TIT. CONT. - TRANCHE DI GAR. - RISERVA DI TR. -     *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10650AllineaAreaTranche() {
        // COB_CODE: MOVE 'S10650-ALLINEA-AREA-TRANCHE' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S10650-ALLINEA-AREA-TRANCHE");
        //
        // -->> Gestione Tranche Di Granzia
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                     END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTabTga(((short)1));
        while (!(ws.getIxIndici().getTabTga() > wtgaAreaTranche.getEleTranMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE:           IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
            //                        ISPC0211-IDENT-LIVELLO-T(IX-AREA-ISPC0211)
            //           *
            //                           THRU S10660-GESTIONE-LIV-GAR-EX
            //           *
            //                     END-IF
            if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar() == ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211IdentLivelloT(ws.getIxIndici().getAreaIspc0211())) {
                //
                // COB_CODE: PERFORM S10660-GESTIONE-LIV-GAR
                //              THRU S10660-GESTIONE-LIV-GAR-EX
                s10660GestioneLivGar();
                //
            }
            //
            ws.getIxIndici().setTabTga(Trunc.toShort(ws.getIxIndici().getTabTga() + 1, 4));
        }
    }

    /**Original name: S10660-GESTIONE-LIV-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *          GESTIONE DEL LIVELLO DI GARANZIA                       *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10660GestioneLivGar() {
        // COB_CODE: SET WK-WTGA-VAL-N
        //             TO TRUE.
        ws.getWkWtgaVal().setN();
        // COB_CODE: SET WK-INTMORA-VAL-N
        //            TO TRUE.
        ws.getWkIntmoraVal().setN();
        // COB_CODE: PERFORM S10670-INIZIALIZZA-TRCH
        //             THRU S10670-INIZIALIZZA-TRCH-EX
        s10670InizializzaTrch();
        // COB_CODE: INITIALIZE WK-PERCINDU
        //                      WK-PRESTLORD
        //                      WK-COSTO-ABS
        //                      WK-PPI
        ws.setWkPercindu(new AfDecimal(0, 6, 3));
        ws.setWkPrestlord(new AfDecimal(0, 15, 3));
        ws.setWkCostoAbs(new AfDecimal(0, 15, 3));
        ws.setWkPpi(new AfDecimal(0, 15, 3));
        //
        //    DISPLAY 'NUMERO COMPONENTI: '
        //    ISPC0211-NUM-COMPON-MAX-ELE(IX-AREA-ISPC0211)
        // COB_CODE: PERFORM S10680-CONTR-LIV-GAR
        //              THRU S10680-CONTR-LIV-GAR-EX
        //           VARYING IX-COMP-ISPC0211 FROM 1 BY 1
        //             UNTIL IX-COMP-ISPC0211 >
        //                   ISPC0211-NUM-COMPON-MAX-ELE-T(IX-AREA-ISPC0211)
        //                OR IDSV0001-ESITO-KO.
        ws.getIxIndici().setCompIspc0211(((short)1));
        while (!(ws.getIxIndici().getCompIspc0211() > ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211NumComponMaxEleT(ws.getIxIndici().getAreaIspc0211()) || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            s10680ContrLivGar();
            ws.getIxIndici().setCompIspc0211(Trunc.toShort(ws.getIxIndici().getCompIspc0211() + 1, 4));
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //           END-IF
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF WTGA-PRSTZ-ULT-NULL(IX-TAB-TGA)
            //                      NOT EQUAL HIGH-VALUES
            //           *    DISPLAY 'WK-PRESTLORD: ' WK-PRESTLORD
            //           *    DISPLAY 'WTGA-PRSTZ-ULT(IX-TAB-TGA):'
            //           *             WTGA-PRSTZ-ULT(IX-TAB-TGA)
            //                      END-IF
            //                   ELSE
            //                   END-IF
            //                   END-IF
            if (!Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().getWtgaPrstzUltNullFormatted())) {
                //    DISPLAY 'WK-PRESTLORD: ' WK-PRESTLORD
                //    DISPLAY 'WTGA-PRSTZ-ULT(IX-TAB-TGA):'
                //             WTGA-PRSTZ-ULT(IX-TAB-TGA)
                // COB_CODE: IF WK-PRESTLORD IS NUMERIC
                //           END-IF
                //           END-IF
                if (Functions.isNumber(ws.getWkPrestlord())) {
                    // COB_CODE: COMPUTE WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
                    //                   = WK-PRESTLORD
                    //                   - WTGA-PRSTZ-ULT(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(Trunc.toDecimal(ws.getWkPrestlord().subtract(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().getWtgaPrstzUlt()), 15, 3));
                    // COB_CODE: IF WTGA-COS-RUN-ASSVA(IX-TAB-TGA) < 0
                    //              MOVE 0 TO WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
                    //           END-IF
                    if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().getWtgaCosRunAssva().compareTo(0) < 0) {
                        // COB_CODE: MOVE 0 TO WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
                        wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(Trunc.toDecimal(0, 15, 3));
                    }
                }
            }
            else if (Functions.isNumber(ws.getWkPrestlord())) {
                // COB_CODE:    IF WK-PRESTLORD IS NUMERIC
                //                      = WK-PRESTLORD
                //           END-IF
                // COB_CODE: COMPUTE WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
                //                   = WK-PRESTLORD
                wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(Trunc.toDecimal(ws.getWkPrestlord(), 15, 3));
            }
            //
            // COB_CODE: IF WK-PPI IS NUMERIC
            //           END-IF
            //           END-IF
            if (Functions.isNumber(ws.getWkPpi())) {
                // COB_CODE: IF WK-PPI > 0
                //              END-IF
                //           END-IF
                if (ws.getWkPpi().compareTo(0) > 0) {
                    // COB_CODE: IF WTGA-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
                    //           NOT EQUAL HIGH-VALUES
                    //               END-IF
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().getWtgaCosRunAssvaNullFormatted())) {
                        // COB_CODE: IF WTGA-COS-RUN-ASSVA(IX-TAB-TGA) > 0
                        //                  * WK-PPI)
                        //           END-IF
                        if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().getWtgaCosRunAssva().compareTo(0) > 0) {
                            // COB_CODE: COMPUTE WTGA-COS-RUN-ASSVA(IX-TAB-TGA) =
                            //                  (WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
                            //                  * WK-PPI)
                            wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().getWtgaCosRunAssva().multiply(ws.getWkPpi()), 15, 3));
                        }
                    }
                }
            }
            //
            // COB_CODE:    IF WTGA-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
            //              NOT EQUAL HIGH-VALUES
            //              END-IF
            //           END-IF
            if (!Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().getWtgaCosRunAssvaNullFormatted())) {
                // COB_CODE:     IF WK-PERCINDU IS NUMERIC
                //                   * (WK-PERCINDU / 100)
                //           END-IF
                if (Functions.isNumber(ws.getWkPercindu())) {
                    // COB_CODE: COMPUTE WK-COSTO-ABS
                    //                   = WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
                    ws.setWkCostoAbs(Trunc.toDecimal(abs(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().getWtgaCosRunAssva()), 15, 3));
                    // COB_CODE: COMPUTE WTGA-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
                    //                   = WK-COSTO-ABS
                    //                   * (WK-PERCINDU / 100)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(Trunc.toDecimal(ws.getWkCostoAbs().multiply((new AfDecimal((ws.getWkPercindu().divide(100)), 6, 3))), 15, 3));
                }
            }
        }
        //
        // --> Valorizzo il flag di inser/modif. solo se la tabella h stata
        // --> effettivamente valorizzata con le componenti del servizio
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF WK-WTGA-VAL-Y
            //           *          SIR CQPrd00020310: Se Actuator non fornisce la
            //           *          componente INTMORA, allora valorizzare a ZERO
            //           *          l'attributo di tranche
            //                      END-IF
            //                   END-IF
            if (ws.getWkWtgaVal().isY()) {
                //          SIR CQPrd00020310: Se Actuator non fornisce la
                //          componente INTMORA, allora valorizzare a ZERO
                //          l'attributo di tranche
                // COB_CODE: IF WK-INTMORA-VAL-N
                //                TO WTGA-INTR-MORA(IX-TAB-TGA)
                //           END-IF
                if (ws.getWkIntmoraVal().isN()) {
                    // COB_CODE: MOVE ZEROES
                    //             TO WTGA-INTR-MORA(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(new AfDecimal(0, 15, 3));
                }
                // COB_CODE: SET  WTGA-ST-MOD(IX-TAB-TGA)
                //             TO TRUE
                wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getStatus().setMod();
                //
                // COB_CODE: IF IDSV0001-TIPO-MOVIMENTO = 6996
                //              CONTINUE
                //           ELSE
                //              THRU CALCOLA-GNT-EX
                //           END-IF
                if (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6996) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(1)
                    //             TO WTGA-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(wpmoAreaParamMovi.getTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc());
                    //
                    // COB_CODE: PERFORM CALCOLA-GNT
                    //              THRU CALCOLA-GNT-EX
                    calcolaGnt();
                }
            }
            //
        }
    }

    /**Original name: S10670-INIZIALIZZA-TRCH<br>
	 * <pre>----------------------------------------------------------------*
	 *          INIZIALIZZAZIONE TRCH DI GAR                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10670InizializzaTrch() {
        // COB_CODE: MOVE HIGH-VALUE  TO
        //                         WTGA-PC-COMMIS-GEST-NULL(IX-TAB-TGA).
        wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPcCommisGest.Len.WTGA_PC_COMMIS_GEST_NULL));
    }

    /**Original name: S10680-CONTR-LIV-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *          TEST SUI COMPONENTI DELLA TABELLA TGA
	 * ----------------------------------------------------------------*</pre>*/
    private void s10680ContrLivGar() {
        // COB_CODE: MOVE 'S10680-CONTR-LIV-GAR' TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10680-CONTR-LIV-GAR");
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10690-CONTR-COMP-TRANCHE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10690-CONTR-COMP-TRANCHE
            //              THRU S10690-CONTR-COMP-TRANCHE-EX
            s10690ContrCompTranche();
            //
        }
    }

    /**Original name: S10690-CONTR-COMP-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *          VALORIZZAZIONE DELL'AREA DI TRANCHE DI GARANZIA        *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10690ContrCompTranche() {
        // COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE' TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.
        displayLabel();
        //    DISPLAY '********* COMPONENTE OUTPUT ACTUATOR ********'
        //    DISPLAY '* POLIZZA                   = ' WPOL-IB-OGG
        //    DISPLAY '* ISPC0211-CODICE-VARIABILE = '
        //    ISPC0211-CODICE-VARIABILE(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
        //    DISPLAY '* ISPC0211-TIPO-DATO        = '
        //    ISPC0211-TIPO-DATO(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
        //    DISPLAY '* ISPC0211-VALORE-IMP       = '
        //    ISPC0211-VALORE-IMP(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
        //    DISPLAY '* ISPC0211-VALORE-PERC      = '
        //    ISPC0211-VALORE-PERC(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
        //    DISPLAY '************************************************'
        // COB_CODE: MOVE ISPC0211-TIPO-DATO-T
        //                (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
        //             TO WS-TP-DATO.
        ws.getWsTpDato().setWsTpDato(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211TipoDatoT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()));
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PREMORT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "PREMORT")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-PRE-CASO-MOR(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP. PREMORT ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP. PREMORT ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'CAPMORTE'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "CAPMORTE")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //              TO WTGA-CPT-RSH-MOR(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP CAPMORTE ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP CAPMORTE ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PPUROU'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "PPUROU")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-PRE-PP-ULT(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP PPUROU ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP PPUROU ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PTARIFU'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "PTARIFU")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-PRE-TARI-ULT(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP PTARIFU ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP PTARIFU ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PINVENU'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "PINVENU")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               END-IF
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-PRE-INVRIO-ULT(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    // COB_CODE: IF WTGA-PRE-INVRIO-ULT(IX-TAB-TGA) = ZEROES
                    //                TO WTGA-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
                    //           END-IF
                    if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().getWtgaPreInvrioUlt().compareTo(0) == 0) {
                        // COB_CODE: MOVE HIGH-VALUE
                        //             TO WTGA-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
                        wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreInvrioUlt.Len.WTGA_PRE_INVRIO_ULT_NULL));
                    }
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP PINVENU ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP PINVENU ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'SOPPRO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "SOPPRO")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-IMP-SOPR-PROF(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP SOPPRO ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP SOPPRO ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'SOPSAN'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "SOPSAN")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-IMP-SOPR-SAN(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP SOPSAN ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP SOPSAN ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'SOPSPO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "SOPSPO")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-IMP-SOPR-SPO(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP SOPSPO ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP SOPSPO ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'SOPTEC'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "SOPTEC")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-IMP-SOPR-TEC(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP SOPTEC ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP SOPTEC ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'SOPALT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "SOPALT")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-IMP-ALT-SOPR(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP SOPALT ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP SOPALT ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PRESTRIV'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "PRESTRIV")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-PRSTZ-ULT(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP PRESTRIV ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP PRESTRIV ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'CAPOPZ'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "CAPOPZ")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP CAPOPZ ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP CAPOPZ ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'TRN'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "TRN")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-PERC-CENTESIMI
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.PERC_CENTESIMI:// COB_CODE: MOVE ISPC0211-VALORE-PERC-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-RENDTO-LRD(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValorePercT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 14, 9));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-RENDTO-LRD(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 14, 9));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP TRN ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP TRN ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PERCRETR'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "PERCRETR")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-PERC-CENTESIMI
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.PERC_CENTESIMI:// COB_CODE: MOVE ISPC0211-VALORE-PERC-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-PC-RETR(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValorePercT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 6, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-PC-RETR(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 6, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP PERCRETR ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP PERCRETR ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'RENDRETR'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "RENDRETR")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-PERC-CENTESIMI
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.PERC_CENTESIMI:// COB_CODE: MOVE ISPC0211-VALORE-PERC-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-RENDTO-RETR(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValorePercT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 14, 9));
                    //         DISPLAY 'RENDRETR=>'
                    //         ISPC0211-VALORE-PERC(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERRORE COMP RENDRETR ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE COMP RENDRETR ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'MINGAR'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "MINGAR")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-PERC-CENTESIMI
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.PERC_CENTESIMI:// COB_CODE: MOVE ISPC0211-VALORE-PERC-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-MIN-GARTO(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValorePercT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 14, 9));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-MIN-GARTO(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 14, 9));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERRORE COMP MINGAR ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE COMP MINGAR ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'MINTRATT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "MINTRATT")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-PERC-CENTESIMI
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.PERC_CENTESIMI:// COB_CODE: MOVE ISPC0211-VALORE-PERC-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-MIN-TRNUT(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValorePercT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 14, 9));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-MIN-TRNUT(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 14, 9));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP MINTRATT ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP MINTRATT ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'ABBULTIMO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "ABBULTIMO")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-ABB-ANNU-ULT(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP ABBULTIMO ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP ABBULTIMO ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'INTMORA'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "INTMORA")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-INTMORA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-INTR-MORA(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y       TO TRUE
                    ws.getWkWtgaVal().setY();
                    // COB_CODE: SET WK-INTMORA-VAL-Y    TO TRUE
                    ws.getWkIntmoraVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP INTMORA ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP INTMORA ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'MANFEE'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "MANFEE")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-MANFEE-RICOR(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    //
                    // COB_CODE:             IF WTGA-MANFEE-RICOR(IX-TAB-TGA) > 0
                    //           *
                    //                            TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
                    //           *
                    //                       END-IF
                    if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().getWtgaManfeeRicor().compareTo(0) > 0) {
                        //
                        // COB_CODE: SET WK-MANFEE-YES TO TRUE
                        ws.getWkManfee().setYes();
                        //
                        // COB_CODE: MOVE 'RI'
                        //             TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
                        wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl("RI");
                        //
                    }
                    //
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP. MANFEE ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP. MANFEE ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PREURIV'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "PREURIV")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               END-IF
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-PRE-UNI-RIVTO(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    // COB_CODE: IF WTGA-PRE-UNI-RIVTO(IX-TAB-TGA) = ZEROES
                    //                TO WTGA-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
                    //           END-IF
                    if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().getWtgaPreUniRivto().compareTo(0) == 0) {
                        // COB_CODE: MOVE HIGH-VALUE
                        //             TO WTGA-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
                        wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtgaPreUniRivto.Len.WTGA_PRE_UNI_RIVTO_NULL));
                    }
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP PREURIV ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP PREURIV ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'INCPRE'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "INCPRE")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-INCR-PRE(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP INCPRE ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP INCPRE ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'INCPREST'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "INCPREST")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-INCR-PRSTZ(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP INCPREST ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP INCPREST ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'RIVAGG'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "RIVAGG")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-PRSTZ-AGG-ULT(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP RIVAGG ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP RIVAGG ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'TASSONETTO'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "TASSONETTO")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-PERC-CENTESIMI
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.PERC_CENTESIMI:// COB_CODE: MOVE ISPC0211-VALORE-PERC-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-TS-RIVAL-NET(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValorePercT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 14, 9));
                    //          DISPLAY 'TASSONETTO=>'
                    //         ISPC0211-VALORE-PERC(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP TASSONETTO ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP TASSONETTO ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'RISMAT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "RISMAT")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-RIS-MAT(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP RISMAT ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP RISMAT ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //                 (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PREULT'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "PREULT")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-PRE-RIVTO(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP PREULT ISPC0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP PREULT ISPC0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //                (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'CAPMINSCA'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "CAPMINSCA")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //                SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-CPT-MIN-SCAD(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP CAPMINSCA ISPC0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP CAPMINSCA ISPC0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        // COB_CODE:      IF ISPC0211-CODICE-VARIABILE-T
        //                     (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'GESTIONE'
        //           *       DISPLAY 'WS-TP-DATO:' WS-TP-DATO
        //                   END-EVALUATE
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "GESTIONE")) {
            //       DISPLAY 'WS-TP-DATO:' WS-TP-DATO
            // COB_CODE:         EVALUATE TRUE
            //                     WHEN TD-IMPORTO
            //                        SET WK-WTGA-VAL-Y    TO TRUE
            //                     WHEN TD-PERC-CENTESIMI
            //                       SET WK-WTGA-VAL-Y    TO TRUE
            //                     WHEN OTHER
            //           *           DISPLAY '* COMPONENTE GESTIONE TIPO DATO -> '
            //           *                                          WS-TP-DATO
            //                          THRU GESTIONE-ERR-STD-EX
            //                   END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO:// COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WTGA-COMMIS-GEST(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    //          DISPLAY 'GESTIONE X IMPORTO=>'
                    //         ISPC0211-VALORE-IMP(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                case WsTpDato.PERC_CENTESIMI://             TO WTGA-COMMIS-GEST(IX-TAB-TGA)
                    // COB_CODE:             MOVE ISPC0211-VALORE-PERC-T
                    //                           (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //           *             TO WTGA-COMMIS-GEST(IX-TAB-TGA)
                    //                         TO WTGA-PC-COMMIS-GEST(IX-TAB-TGA)
                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValorePercT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 6, 3));
                    //          DISPLAY 'GESTIONE X PERCENT=>'
                    //         ISPC0211-VALORE-PERC(IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default://           DISPLAY '* COMPONENTE GESTIONE TIPO DATO -> '
                    //                                          WS-TP-DATO
                    // COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP GESTIONE ISPC0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP GESTIONE ISPC0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //--> COSTI IDD FASE 2
        // COB_CODE:      IF ISPC0211-CODICE-VARIABILE-T
        //                  (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PRESTLORD'
        //           *       DISPLAY 'TROVATA PRESTLORD'
        //                   END-EVALUATE
        //                END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "PRESTLORD")) {
            //       DISPLAY 'TROVATA PRESTLORD'
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO://             TO WTGA-PRSTZ-ULT(IX-TAB-TGA)
                    // COB_CODE:             MOVE ISPC0211-VALORE-IMP-T
                    //                           (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //           *             TO WTGA-PRSTZ-ULT(IX-TAB-TGA)
                    //                         TO WK-PRESTLORD
                    ws.setWkPrestlord(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP PRESTLORD ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP PRESTLORD ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        //--> COSTI IDD FASE 2
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PERCINDU'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "PERCINDU")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-PERC-CENTESIMI
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.PERC_CENTESIMI:// COB_CODE: MOVE ISPC0211-VALORE-PERC-T
                    //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //             TO WK-PERCINDU
                    ws.setWkPercindu(Trunc.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValorePercT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 6, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP PRESTLORD ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP PRESTLORD ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
        //
        //--> COSTI IDD FASE 2 - PPI
        // COB_CODE: IF ISPC0211-CODICE-VARIABILE-T
        //             (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'PPI'
        //              END-EVALUATE
        //           END-IF.
        if (Conditions.eq(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "PPI")) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN TD-IMPORTO
            //               SET WK-WTGA-VAL-Y    TO TRUE
            //             WHEN OTHER
            //                  THRU GESTIONE-ERR-STD-EX
            //           END-EVALUATE
            switch (ws.getWsTpDato().getWsTpDato()) {

                case WsTpDato.IMPORTO://             TO WTGA-PRSTZ-ULT(IX-TAB-TGA)
                    // COB_CODE:             MOVE ISPC0211-VALORE-IMP-T
                    //                           (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                    //           *             TO WTGA-PRSTZ-ULT(IX-TAB-TGA)
                    //                         TO WK-PPI
                    ws.setWkPpi(TruncAbs.toDecimal(ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                    // COB_CODE: SET WK-WTGA-VAL-Y    TO TRUE
                    ws.getWkWtgaVal().setY();
                    break;

                default:// COB_CODE: MOVE 'S10690-CONTR-COMP-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10690-CONTR-COMP-TRANCHE");
                    // COB_CODE: MOVE 'ERR COMP PRESTLORD ISPS0211'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERR COMP PRESTLORD ISPS0211");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    break;
            }
        }
    }

    /**Original name: S10700-CALL-LOAS0800<br>
	 * <pre>----------------------------------------------------------------*
	 *  CHIAMATA AL MODULO DI GESTIONE DEL MANAGEMENT FEE
	 * ----------------------------------------------------------------*</pre>*/
    private void s10700CallLoas0800() {
        Loas0800 loas0800 = null;
        // COB_CODE: MOVE 'S10700-CALL-LOAS0800'  TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10700-CALL-LOAS0800");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: SET W800-SENZA-CALCOLO
        //             TO TRUE.
        ws.getW800AreaPag().getModChiamataServizio().setW800SenzaCalcolo();
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0800'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0800");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'GESTIONE MANAGEMENT FEE'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("GESTIONE MANAGEMENT FEE");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        //
        //
        // COB_CODE:      CALL LOAS0800  USING AREA-IDSV0001
        //                                     WCOM-IO-STATI
        //                                     WPMO-AREA-PARAM-MOVI
        //                                     WMOV-AREA-MOVIMENTO
        //                                     WPOL-AREA-POLIZZA
        //                                     WADE-AREA-ADESIONE
        //                                     WGRZ-AREA-GARANZIA
        //                                     WTGA-AREA-TRANCHE
        //                                     WSKD-AREA-SCHEDA
        //                                     W660-AREA-PAG
        //                                     W800-AREA-PAG
        //           *
        //                 ON EXCEPTION
        //           *
        //                       THRU EX-S0290
        //           *
        //                 END-CALL.
        try {
            loas0800 = Loas0800.getInstance();
            loas0800.run(new Object[] {areaIdsv0001, wcomIoStati, wpmoAreaParamMovi, wmovAreaMovimento, wpolAreaPolizza, wadeAreaAdesione, wgrzAreaGaranzia, wtgaAreaTranche, wskdAreaScheda, w660AreaPag, ws.getW800AreaPag()});
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'LOAS0800'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LOAS0800");
            // COB_CODE: MOVE 'ERRORE CALL MODULO LOAS0800'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE CALL MODULO LOAS0800");
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
            //
        }
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0800'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0800");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'GESTIONE MANAGEMENT FEE'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("GESTIONE MANAGEMENT FEE");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: CALCOLA-GNT<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA-GNT
	 * ----------------------------------------------------------------*</pre>*/
    private void calcolaGnt() {
        Lccs0010 lccs0010 = null;
        GenericParam formato = null;
        GenericParam ggDiff = null;
        GenericParam codiceRitorno = null;
        // COB_CODE: MOVE 'A'                            TO FORMATO
        ws.setFormatoFormatted("A");
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO          TO DATA-SUPERIORE
        ws.getDataSuperiore().setDataSuperioreFormatted(areaIdsv0001.getAreaComune().getIdsv0001DataEffettoFormatted());
        // COB_CODE:      IF WPMO-DT-RICOR-PREC-NULL(1) = HIGH-VALUE
        //                   MOVE WK-DATA                     TO DATA-INFERIORE
        //                ELSE
        //           *SIR BNL 12887
        //                  END-IF
        //                END-IF.
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrecNullFormatted())) {
            // COB_CODE: MOVE WTGA-DT-DECOR(IX-TAB-TGA)   TO WK-DATA
            ws.setWkData(TruncAbs.toInt(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtDecor(), 8));
            // COB_CODE: MOVE WK-DATA                     TO DATA-INFERIORE
            ws.getDataInferiore().setDataInferioreFormatted(ws.getWkDataFormatted());
        }
        else if (wpmoAreaParamMovi.getTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrec() < wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtDecor()) {
            //SIR BNL 12887
            // COB_CODE: IF WPMO-DT-RICOR-PREC(1) < WTGA-DT-DECOR(IX-TAB-TGA)
            //              MOVE WK-DATA                   TO DATA-INFERIORE
            //           ELSE
            //              MOVE WK-DATA                   TO DATA-INFERIORE
            //           END-IF
            // COB_CODE: MOVE WTGA-DT-DECOR(IX-TAB-TGA) TO WK-DATA
            ws.setWkData(TruncAbs.toInt(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaDtDecor(), 8));
            // COB_CODE: MOVE WK-DATA                   TO DATA-INFERIORE
            ws.getDataInferiore().setDataInferioreFormatted(ws.getWkDataFormatted());
        }
        else {
            // COB_CODE: MOVE WPMO-DT-RICOR-PREC(1)     TO WK-DATA
            ws.setWkData(TruncAbs.toInt(wpmoAreaParamMovi.getTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrec(), 8));
            // COB_CODE: MOVE WK-DATA                   TO DATA-INFERIORE
            ws.getDataInferiore().setDataInferioreFormatted(ws.getWkDataFormatted());
        }
        //
        // COB_CODE: CALL LCCS0010     USING FORMATO,
        //                                   DATA-INFERIORE,
        //                                   DATA-SUPERIORE,
        //                                   GG-DIFF,
        //                                   CODICE-RITORNO
        //           ON EXCEPTION
        //                    THRU GESTIONE-ERR-SIST-EX
        //           END-CALL.
        try {
            lccs0010 = Lccs0010.getInstance();
            formato = new GenericParam(MarshalByteExt.chToBuffer(ws.getFormato()));
            ggDiff = new GenericParam(MarshalByteExt.strToBuffer(ws.getGgDiffFormatted(), Loas0660Data.Len.GG_DIFF));
            codiceRitorno = new GenericParam(MarshalByteExt.chToBuffer(ws.getCodiceRitorno()));
            lccs0010.run(formato, ws.getDataInferiore(), ws.getDataSuperiore(), ggDiff, codiceRitorno);
            ws.setFormatoFromBuffer(formato.getByteData());
            ws.setGgDiffFromBuffer(ggDiff.getByteData());
            ws.setCodiceRitornoFromBuffer(codiceRitorno.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE 'LCCS0010'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LCCS0010");
            // COB_CODE: MOVE 'DIFFERENZA TRA DATE - LCCS0010'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("DIFFERENZA TRA DATE - LCCS0010");
            // COB_CODE: MOVE 'CALCOLA-GNT'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("CALCOLA-GNT");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
        }
        //
        // COB_CODE: IF CODICE-RITORNO = ZERO
        //              MOVE GG-DIFF              TO WTGA-NUM-GG-RIVAL(IX-TAB-TGA)
        //           ELSE
        //                 THRU GESTIONE-ERR-SIST-EX
        //           END-IF.
        if (Conditions.eq(ws.getCodiceRitorno(), '0')) {
            // COB_CODE: MOVE GG-DIFF              TO WTGA-NUM-GG-RIVAL(IX-TAB-TGA)
            wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(ws.getGgDiff());
        }
        else {
            // COB_CODE: MOVE 'LCCS0010'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LCCS0010");
            // COB_CODE: MOVE 'DIFFERENZA TRA DATE - LCCS0010'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("DIFFERENZA TRA DATE - LCCS0010");
            // COB_CODE: MOVE 'CALCOLA-GNT'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("CALCOLA-GNT");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
        }
    }

    /**Original name: S90000-OPERAZ-FINALI<br>
	 * <pre> ============================================================== *
	 *  -->           O P E R Z I O N I   F I N A L I              <-- *
	 *  ============================================================== *</pre>*/
    private void s90000OperazFinali() {
        // COB_CODE: MOVE 'S90000-OPERAZ-FINALI'  TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S90000-OPERAZ-FINALI");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.
        displayLabel();
    }

    /**Original name: GESTIONE-ERR-STD<br>
	 * <pre>----------------------------------------------------------------*
	 *                 GESTIONE STANDARD DELL'ERRORE                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneErrStd() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-PGM
        //             TO IEAI9901-COD-SERVIZIO-BE.
        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
        // COB_CODE: MOVE WK-LABEL-ERR
        //             TO IEAI9901-LABEL-ERR.
        ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
        // COB_CODE: MOVE WK-COD-ERR
        //             TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted(ws.getWkGestioneMsgErr().getCodErrFormatted());
        // COB_CODE: STRING WK-STRING ';'
        //                  IDSO0011-RETURN-CODE ';'
        //                  IDSO0011-SQLCODE
        //                  DELIMITED BY SIZE
        //                  INTO IEAI9901-PARAMETRI-ERR
        //           END-STRING.
        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkGestioneMsgErr().getStringFldFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
        //
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: GESTIONE-ERR-SIST<br>
	 * <pre>----------------------------------------------------------------*
	 *            GESTIONE STANDARD DELL'ERRORE DI SISTEMA             *
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneErrSist() {
        // COB_CODE: MOVE WK-LABEL-ERR
        //             TO IEAI9901-LABEL-ERR
        ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
        //
        // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
        //              THRU EX-S0290.
        s0290ErroreDiSistema();
    }

    /**Original name: CALL-MATR-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PER LETTURA MATRICE MOVIMENTO
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     CALL MATRICE MOVIMENTO
	 * *****************************************************************</pre>*/
    private void callMatrMovimento() {
        Lccs0020 lccs0020 = null;
        // COB_CODE: MOVE 'LCCS0020' TO LCCV0021-PGM
        ws.getLccv0021().setPgm("LCCS0020");
        // COB_CODE: CALL LCCV0021-PGM USING AREA-IDSV0001 LCCV0021
        //           ON EXCEPTION
        //                     THRU EX-S0290
        //           END-CALL.
        try {
            lccs0020 = Lccs0020.getInstance();
            lccs0020.run(areaIdsv0001, ws.getLccv0021());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CALL-MATR-MOVIMENTO'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("CALL-MATR-MOVIMENTO");
            // COB_CODE: MOVE 'CALL-MATR-MOVIMENTO'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-MATR-MOVIMENTO");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF NOT LCCV0021-SUCCESSFUL-RC
            //           OR NOT LCCV0021-SUCCESSFUL-SQL
            //                 THRU EX-S0290
            //           END-IF
            if (!ws.getLccv0021().getAreaOutput().getReturnCode().isSuccessfulRc() || !ws.getLccv0021().getAreaOutput().getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0001-ESITO-KO
                //            TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // COB_CODE: MOVE LCCV0021-COD-SERVIZIO-BE
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getLccv0021().getAreaOutput().getCodServizioBe());
                // COB_CODE: MOVE LCCV0021-DESCRIZ-ERR
                //             TO CALL-DESC
                ws.getIdsv0002().setCallDesc(ws.getLccv0021().getAreaOutput().getDescrizErr());
                // COB_CODE: MOVE 'CALL-MATR-MOVIMENTO'
                //              TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("CALL-MATR-MOVIMENTO");
                // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
                //              THRU EX-S0290
                s0290ErroreDiSistema();
            }
        }
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *   ROUTINES GESTIONE ERRORI                                      *
	 * ----------------------------------------------------------------*
	 * MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>**-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: DISPLAY-LABEL<br>
	 * <pre>-----------------------------------------------------------------
	 *   ROUTINE DEBUG
	 * -----------------------------------------------------------------</pre>*/
    private void displayLabel() {
        // COB_CODE:      IF (IDSV0001-DEBUG-BASSO OR IDSV0001-DEBUG-ESASPERATO) AND
        //                NOT IDSV0001-ON-LINE
        //           *
        //           *        DISPLAY WK-LABEL-ERR
        //                    CONTINUE
        //           *
        //                END-IF.
        if ((areaIdsv0001.getAreaComune().getLivelloDebug().isIdsv0001DebugBasso() || areaIdsv0001.getAreaComune().getLivelloDebug().isIdsv0001DebugEsasperato()) && !areaIdsv0001.getAreaComune().getModalitaEsecutiva().isIdsv0001OnLine()) {
        //
        //        DISPLAY WK-LABEL-ERR
        // COB_CODE: CONTINUE
        //continue
        //
        }
    }

    /**Original name: ESEGUI-DISPLAY<br>
	 * <pre>DISPLAY-ESITO-PROD.
	 *     IF IDSV0001-DEBUG-ESASPERATO AND
	 *     NOT IDSV0001-ON-LINE
	 *       DISPLAY 'AREA-ERRORI: ' AREA-PRODUCT-SERVICES
	 *       DISPLAY 'ESITO PRODOTTO: ' IJCCMQ03-ESITO
	 *       DISPLAY 'JAVA SERVICE NAME: ' IJCCMQ03-JAVA-SERVICE-NAME
	 *       DISPLAY 'NUMERO ELEMENTI: ' IJCCMQ03-MAX-ELE-ERRORI
	 *       PERFORM VARYING IX-TAB-ERR FROM 1 BY 1
	 *          UNTIL IX-TAB-ERR > IJCCMQ03-MAX-ELE-ERRORI
	 *           DISPLAY 'COD-ERRORE: '
	 *               IJCCMQ03-COD-ERRORE(IX-TAB-ERR)
	 *           DISPLAY 'DESC-ERRORE: '
	 *               IJCCMQ03-DESC-ERRORE(IX-TAB-ERR)
	 *           DISPLAY 'LIV-ERRORE: '
	 *               IJCCMQ03-LIV-GRAVITA-BE(IX-TAB-ERR)
	 *       END-PERFORM.
	 *     END-IF.
	 * DISPLAY-ESITO-PROD-EX.
	 *     EXIT.
	 * DISPLAY-COMPONENTE.
	 *     IF (IDSV0001-DEBUG-MEDIO OR IDSV0001-DEBUG-ESASPERATO) AND
	 *     NOT IDSV0001-ON-LINE
	 *        DISPLAY ' TIPO-LIVELLO : '
	 *            ISPC0211-TIPO-LIVELLO(IX-AREA-ISPC0211)
	 *        DISPLAY ' CODICE-LIVELLO: '
	 *          ISPC0211-CODICE-LIVELLO(IX-AREA-ISPC0211)
	 *        MOVE ISPC0211-IDENT-LIVELLO(IX-AREA-ISPC0211)
	 *          TO WK-ID
	 *        DISPLAY ' ID-LIVELLO :'   WK-ID
	 *        MOVE ISPC0211-TIPO-DATO
	 *         (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
	 *          TO WS-TP-DATO
	 *        DISPLAY 'TIPO-DATO: ' WS-TP-DATO
	 *        DISPLAY 'COMPONENTE: '
	 *          ISPC0211-CODICE-VARIABILE
	 *            (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
	 *        EVALUATE TRUE
	 *           WHEN TD-IMPORTO
	 *              DISPLAY ' IMPORTO : '
	 *              ISPC0211-VALORE-IMP
	 *                 (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
	 *           WHEN TD-PERC-CENTESIMI
	 *              DISPLAY ' PERCENTUALE : '
	 *              ISPC0211-VALORE-PERC
	 *                 (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
	 *           WHEN OTHER
	 *              DISPLAY '? ' WS-TP-DATO
	 *        END-EVALUATE
	 *     END-IF.
	 * DISPLAY-COMPONENTE-EX.
	 *     EXIT.
	 * ----------------------------------------------------------------*
	 *     ROUTINES DI GESTIONE DISPLAY
	 * ----------------------------------------------------------------*</pre>*/
    private void eseguiDisplay() {
        Idss8880 idss8880 = null;
        GenericParam idsv8888DisplayAddress = null;
        // COB_CODE: IF IDSV0001-ANY-TUNING-DBG AND
        //              IDSV8888-ANY-TUNING-DBG
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyTuningDbg() && ws.getIdsv8888().getLivelloDebug().isAnyTuningDbg()) {
            // COB_CODE: IF IDSV0001-TOT-TUNING-DBG
            //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
            //           ELSE
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLivelloDebug().isTotTuningDbg()) {
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isArchBatchDbg()) {
                    // COB_CODE: IF IDSV8888-ARCH-BATCH-DBG
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           ELSE
                    //              MOVE 'ERRO'   TO IDSV8888-FASE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                else {
                    // COB_CODE: MOVE 'ERRO'   TO IDSV8888-FASE
                    ws.getIdsv8888().getStrPerformanceDbg().setFase("ERRO");
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //                TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
            else if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() == areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: IF IDSV8888-LIVELLO-DEBUG =
                //              IDSV0001-LIVELLO-DEBUG
                //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                //           END-IF
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else {
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
        }
        else if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyApplDbg() && ws.getIdsv8888().getLivelloDebug().isAnyApplDbg()) {
            // COB_CODE: IF IDSV0001-ANY-APPL-DBG AND
            //              IDSV8888-ANY-APPL-DBG
            //               END-IF
            //           END-IF
            // COB_CODE: IF IDSV8888-LIVELLO-DEBUG <=
            //              IDSV0001-LIVELLO-DEBUG
            //              MOVE SPACES TO IDSV8888-AREA-DISPLAY
            //           END-IF
            if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() <= areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getAreaDisplay()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: MOVE SPACES TO IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().getAreaDisplay().setAreaDisplay("");
            }
        }
        // COB_CODE: SET IDSV8888-NO-DEBUG              TO TRUE.
        ws.getIdsv8888().getLivelloDebug().setNoDebug();
    }

    /**Original name: VAL-SCHEDE-ISPC0211<br>
	 * <pre> COPY SWAP ISPS0211
	 * ----------------------------------------------------------------*
	 *     VALORIZZAZIONE SCHEDE ISPC0211
	 * ----------------------------------------------------------------*
	 * --> VALORIZZAZIONE OCCURS AREA INPUT SERVIZIO CALCOLI E CONTROLLI
	 * --> CON L'OUTPUT DEL VALORIZZATORE VARIABILI</pre>*/
    private void valSchedeIspc0211() {
        // COB_CODE: MOVE WSKD-DEE
        //             TO ISPC0211-DEE
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setDee(wskdAreaScheda.getWskdDee());
        // COB_CODE: PERFORM AREA-SCHEDA-P-ISPC0211
        //              THRU AREA-SCHEDA-P-ISPC0211-EX
        //           VARYING IX-AREA-SCHEDA-P FROM 1 BY 1
        //             UNTIL IX-AREA-SCHEDA-P > WSKD-ELE-LIVELLO-MAX-P
        ws.getIxIndici().setAreaSchedaP(((short)1));
        while (!(ws.getIxIndici().getAreaSchedaP() > wskdAreaScheda.getWskdEleLivelloMaxP())) {
            areaSchedaPIspc0211();
            ws.getIxIndici().setAreaSchedaP(Trunc.toShort(ws.getIxIndici().getAreaSchedaP() + 1, 4));
        }
        // COB_CODE: MOVE WSKD-ELE-LIVELLO-MAX-P   TO ISPC0211-ELE-MAX-SCHEDA-P
        ws.getAreaIoIsps0211().setIspc0211EleMaxSchedaP(TruncAbs.toShort(wskdAreaScheda.getWskdEleLivelloMaxP(), 4));
        // COB_CODE: PERFORM AREA-SCHEDA-T-ISPC0211
        //              THRU AREA-SCHEDA-T-ISPC0211-EX
        //           VARYING IX-AREA-SCHEDA-T FROM 1 BY 1
        //             UNTIL IX-AREA-SCHEDA-T > WSKD-ELE-LIVELLO-MAX-T
        ws.getIxIndici().setAreaSchedaT(((short)1));
        while (!(ws.getIxIndici().getAreaSchedaT() > wskdAreaScheda.getWskdEleLivelloMaxT())) {
            areaSchedaTIspc0211();
            ws.getIxIndici().setAreaSchedaT(Trunc.toShort(ws.getIxIndici().getAreaSchedaT() + 1, 4));
        }
        // COB_CODE: MOVE WSKD-ELE-LIVELLO-MAX-T   TO ISPC0211-ELE-MAX-SCHEDA-T.
        ws.getAreaIoIsps0211().setIspc0211EleMaxSchedaT(TruncAbs.toShort(wskdAreaScheda.getWskdEleLivelloMaxT(), 4));
    }

    /**Original name: AREA-SCHEDA-P-ISPC0211<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE AREA SCHEDA P
	 * ----------------------------------------------------------------*</pre>*/
    private void areaSchedaPIspc0211() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WSKD-TP-LIVELLO-P(IX-AREA-SCHEDA-P)
        //             TO ISPC0211-TIPO-LIVELLO-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211TipoLivelloP(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getTpLivelloP(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE: MOVE WSKD-COD-LIVELLO-P(IX-AREA-SCHEDA-P)
        //             TO ISPC0211-CODICE-LIVELLO-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211CodiceLivelloP(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getCodLivelloP(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE: MOVE WSKD-ID-LIVELLO-P(IX-AREA-SCHEDA-P)
        //             TO ISPC0211-IDENT-LIVELLO-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211IdentLivelloPFormatted(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getIdLivelloPFormatted(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE: MOVE WSKD-DT-INIZ-PROD-P(IX-AREA-SCHEDA-P)
        //             TO ISPC0211-DT-INIZ-PROD(IX-AREA-SCHEDA-P)
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211DtInizProd(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getDtInizProdP(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE: MOVE WSKD-COD-TIPO-OPZIONE-P(IX-AREA-SCHEDA-P)
        //             TO ISPC0211-CODICE-OPZIONE-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211CodiceOpzioneP(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getCodTipoOpzioneP(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE:      IF WSKD-COD-RGM-FISC-P(IX-AREA-SCHEDA-P) NOT EQUAL SPACES
        //                                                         AND LOW-VALUE
        //                                                         AND HIGH-VALUE
        //           *
        //                    TO ISPC0211-COD-RGM-FISC(IX-AREA-SCHEDA-P)
        //           *
        //                END-IF
        if (!Characters.EQ_SPACE.test(wskdAreaScheda.getWskdTabValP().getCodRgmFiscP(ws.getIxIndici().getAreaSchedaP())) && !Characters.EQ_LOW.test(wskdAreaScheda.getWskdTabValP().getCodRgmFiscPFormatted(ws.getIxIndici().getAreaSchedaP())) && !Characters.EQ_HIGH.test(wskdAreaScheda.getWskdTabValP().getCodRgmFiscPFormatted(ws.getIxIndici().getAreaSchedaP()))) {
            //
            // COB_CODE: MOVE WSKD-COD-RGM-FISC-P(IX-AREA-SCHEDA-P)
            //             TO ISPC0211-COD-RGM-FISC(IX-AREA-SCHEDA-P)
            ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211CodRgmFiscFormatted(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getCodRgmFiscPFormatted(ws.getIxIndici().getAreaSchedaP()));
            //
        }
        //
        //--> VALORIZZAZIONE DELL'AREA VARIABILI DI INPUT DEL SERVIZIO
        //--> CALCOLI E CONTROLLI
        //
        // COB_CODE: MOVE ZERO
        //             TO ISPC0211-NUM-COMPON-MAX-ELE-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211NumComponMaxEleP(ws.getIxIndici().getAreaSchedaP(), ((short)0));
        // COB_CODE: PERFORM AREA-VAR-P-ISPC0211
        //              THRU AREA-VAR-P-ISPC0211-EX
        //           VARYING IX-TAB-VAR-P FROM 1 BY 1
        //             UNTIL IX-TAB-VAR-P >
        //                   WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA-P)
        //                OR IX-TAB-VAR-P > WK-ISPC0211-NUM-COMPON-MAX-P
        ws.getIxIndici().setTabVarP(((short)1));
        while (!(ws.getIxIndici().getTabVarP() > wskdAreaScheda.getWskdTabValP().getEleVariabiliMaxP(ws.getIxIndici().getAreaSchedaP()) || ws.getIxIndici().getTabVarP() > ws.getWkIspcMax().getIspc0211NumComponMaxP())) {
            areaVarPIspc0211();
            ws.getIxIndici().setTabVarP(Trunc.toShort(ws.getIxIndici().getTabVarP() + 1, 4));
        }
        //
        //--> SEGNALO CHE LE VARIABILI FORNITEMI SONO IN NUMERO
        //--> MAGGIORE DEL NUMERO CONSENTITO DALL'AREA ISPC0211
        //
        // COB_CODE: IF WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA-P) >
        //                             WK-ISPC0211-NUM-COMPON-MAX-P
        //                     THRU EX-S0300
        //           END-IF.
        if (wskdAreaScheda.getWskdTabValP().getEleVariabiliMaxP(ws.getIxIndici().getAreaSchedaP()) > ws.getWkIspcMax().getIspc0211NumComponMaxP()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S12100-AREA-SCHEDA-P'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S12100-AREA-SCHEDA-P");
            // COB_CODE: MOVE '005247'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005247");
            // COB_CODE: STRING 'NUMERO VARIABILI DI PRODOTTO '
            //                  'SUPERA LIMITE PREVISTO'
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "NUMERO VARIABILI DI PRODOTTO ", "SUPERA LIMITE PREVISTO");
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: AREA-VAR-P-ISPC0211<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELL'AREA VARIABILI DI PRODOTTO
	 *     DI INPUT DEL SERVIZIO CALCOLI E CONTROLLI
	 * ----------------------------------------------------------------*</pre>*/
    private void areaVarPIspc0211() {
        // COB_CODE: ADD 1 TO ISPC0211-NUM-COMPON-MAX-ELE-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211NumComponMaxEleP(ws.getIxIndici().getAreaSchedaP(), Trunc.toShort(1 + ws.getAreaIoIsps0211().getIspc0211TabValP().getIspc0211NumComponMaxEleP(ws.getIxIndici().getAreaSchedaP()), 3));
        // COB_CODE: MOVE WSKD-COD-VARIABILE-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        //             TO ISPC0211-CODICE-VARIABILE-P
        //               (IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211CodiceVariabileP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP(), wskdAreaScheda.getWskdTabValP().getCodVariabileP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP()));
        // COB_CODE: MOVE WSKD-TP-DATO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        //             TO ISPC0211-TIPO-DATO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211TipoDatoP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP(), wskdAreaScheda.getWskdTabValP().getTpDatoP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP()));
        // COB_CODE: MOVE WSKD-VAL-GENERICO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        //             TO ISPC0211-VAL-GENERICO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P).
        ws.getAreaIoIsps0211().getIspc0211TabValP().setIspc0211ValGenericoP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP(), wskdAreaScheda.getWskdTabValP().getValGenericoP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP()));
    }

    /**Original name: AREA-SCHEDA-T-ISPC0211<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE AREA SCHEDA T
	 * ----------------------------------------------------------------*</pre>*/
    private void areaSchedaTIspc0211() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WSKD-TP-LIVELLO-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-TIPO-LIVELLO-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211TipoLivelloT(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getTpLivelloT(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-COD-LIVELLO-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-CODICE-LIVELLO-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211CodiceLivelloT(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getCodLivelloT(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-ID-LIVELLO-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-IDENT-LIVELLO-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211IdentLivelloTFormatted(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getIdLivelloTFormatted(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-COD-TIPO-OPZIONE-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-CODICE-OPZIONE-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211CodiceOpzioneT(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getCodTipoOpzioneT(ws.getIxIndici().getAreaSchedaT()));
        //
        // COB_CODE:      IF WSKD-COD-RGM-FISC-T(IX-AREA-SCHEDA-T) NOT EQUAL SPACES
        //                                                         AND LOW-VALUE
        //                                                         AND HIGH-VALUE
        //           *
        //                    TO ISPC0211-COD-RGM-FISC-T(IX-AREA-SCHEDA-T)
        //           *
        //                END-IF
        if (!Characters.EQ_SPACE.test(wskdAreaScheda.getWskdTabValT().getCodRgmFiscT(ws.getIxIndici().getAreaSchedaT())) && !Characters.EQ_LOW.test(wskdAreaScheda.getWskdTabValT().getCodRgmFiscTFormatted(ws.getIxIndici().getAreaSchedaT())) && !Characters.EQ_HIGH.test(wskdAreaScheda.getWskdTabValT().getCodRgmFiscTFormatted(ws.getIxIndici().getAreaSchedaT()))) {
            //
            // COB_CODE: MOVE WSKD-COD-RGM-FISC-T(IX-AREA-SCHEDA-T)
            //             TO ISPC0211-COD-RGM-FISC-T(IX-AREA-SCHEDA-T)
            ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211CodRgmFiscTFormatted(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getCodRgmFiscTFormatted(ws.getIxIndici().getAreaSchedaT()));
            //
        }
        //
        // COB_CODE: MOVE WSKD-DT-INIZ-TARI-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-DT-INIZ-TARI(IX-AREA-SCHEDA-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211DtInizTari(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getDtInizTariT(ws.getIxIndici().getAreaSchedaT()));
        //
        // COB_CODE: MOVE WSKD-DT-DECOR-TRCH-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-DT-DECOR-TRCH(IX-AREA-SCHEDA-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211DtDecorTrch(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getDtDecorTrchT(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-TIPO-TRCH(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-TIPO-TRCH(IX-AREA-SCHEDA-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211TipoTrchFormatted(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getTipoTrchFormatted(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-FLG-ITN(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-FLG-ITN(IX-AREA-SCHEDA-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211FlgItnFormatted(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getFlgItnFormatted(ws.getIxIndici().getAreaSchedaT()));
        //--> VALORIZZAZIONE DELL'AREA VARIABILI DI INPUT DEL SERVIZIO
        //--> CALCOLI E CONTROLLI
        //
        // COB_CODE: MOVE ZERO
        //             TO ISPC0211-NUM-COMPON-MAX-ELE-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211NumComponMaxEleT(ws.getIxIndici().getAreaSchedaT(), ((short)0));
        // COB_CODE: PERFORM AREA-VAR-T-ISPC0211
        //              THRU AREA-VAR-T-ISPC0211-EX
        //           VARYING IX-TAB-VAR-T FROM 1 BY 1
        //             UNTIL IX-TAB-VAR-T >
        //                   WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA-T)
        //                OR IX-TAB-VAR-T > WK-ISPC0211-NUM-COMPON-MAX-T
        ws.getIxIndici().setTabVarT(((short)1));
        while (!(ws.getIxIndici().getTabVarT() > wskdAreaScheda.getWskdTabValT().getEleVariabiliMaxT(ws.getIxIndici().getAreaSchedaT()) || ws.getIxIndici().getTabVarT() > ws.getWkIspcMax().getIspc0211NumComponMaxT())) {
            areaVarTIspc0211();
            ws.getIxIndici().setTabVarT(Trunc.toShort(ws.getIxIndici().getTabVarT() + 1, 4));
        }
        //
        //--> SEGNALO CHE LE VARIABILI FORNITEMI SONO IN NUMERO
        //--> MAGGIORE DEL NUMERO CONSENTITO DALL'AREA ISPC0211
        //
        // COB_CODE: IF WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA-T) >
        //                             WK-ISPC0211-NUM-COMPON-MAX-T
        //                     THRU EX-S0300
        //           END-IF.
        if (wskdAreaScheda.getWskdTabValT().getEleVariabiliMaxT(ws.getIxIndici().getAreaSchedaT()) > ws.getWkIspcMax().getIspc0211NumComponMaxT()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S12110-AREA-SCHEDA-T'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S12110-AREA-SCHEDA-T");
            // COB_CODE: MOVE '005247'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005247");
            // COB_CODE: STRING 'NUMERO VARIABILI DI TARIFFA '
            //                  'SUPERA LIMITE PREVISTO'
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "NUMERO VARIABILI DI TARIFFA ", "SUPERA LIMITE PREVISTO");
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: AREA-VAR-T-ISPC0211<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELL'AREA VARIABILI DI TARIFFA
	 *     DI INPUT DEL SERVIZIO CALCOLI E CONTROLLI
	 * ----------------------------------------------------------------*</pre>*/
    private void areaVarTIspc0211() {
        // COB_CODE: ADD 1 TO ISPC0211-NUM-COMPON-MAX-ELE-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211NumComponMaxEleT(ws.getIxIndici().getAreaSchedaT(), Trunc.toShort(1 + ws.getAreaIoIsps0211().getIspc0211TabValT().getIspc0211NumComponMaxEleT(ws.getIxIndici().getAreaSchedaT()), 4));
        // COB_CODE: MOVE WSKD-COD-VARIABILE-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        //             TO ISPC0211-CODICE-VARIABILE-T
        //               (IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211CodiceVariabileT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT(), wskdAreaScheda.getWskdTabValT().getCodVariabileT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT()));
        // COB_CODE: MOVE WSKD-TP-DATO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        //             TO ISPC0211-TIPO-DATO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211TipoDatoT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT(), wskdAreaScheda.getWskdTabValT().getTpDatoT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT()));
        // COB_CODE: MOVE WSKD-VAL-GENERICO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        //             TO ISPC0211-VAL-GENERICO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T).
        ws.getAreaIoIsps0211().getIspc0211TabValT().setIspc0211ValGenericoT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT(), wskdAreaScheda.getWskdTabValT().getValGenericoT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT()));
    }

    public void initIxIndici() {
        ws.getIxIndici().setTabPmo(((short)0));
        ws.getIxIndici().setTabGrz(((short)0));
        ws.getIxIndici().setTabTga(((short)0));
        ws.getIxIndici().setTabPog(((short)0));
        ws.getIxIndici().setAreaIspc0211(((short)0));
        ws.getIxIndici().setCompIspc0211(((short)0));
        ws.getIxIndici().setAreaScheda(((short)0));
        ws.getIxIndici().setTabVar(((short)0));
        ws.getIxIndici().setTabErr(((short)0));
        ws.getIxIndici().setFraz(((short)0));
        ws.getIxIndici().setTabIsps0211(((short)0));
        ws.getIxIndici().setTabVarP(((short)0));
        ws.getIxIndici().setTabVarT(((short)0));
        ws.getIxIndici().setAreaSchedaP(((short)0));
        ws.getIxIndici().setAreaSchedaT(((short)0));
    }

    public void initIspc0211DatiInput() {
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setIspc0211CodCompagniaFormatted("00000");
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setNumPolizza("");
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setCodProdotto("");
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setDataDecorrPolizza("");
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setCodConvenzione("");
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setDataInizValidConv("");
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setDee("");
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setDataRiferimento("");
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setIspc0211LivelloUtenteFormatted("00");
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setIspc0211OpzNumMaxEleFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0211DatiInput.OPZIONI_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0211().getIspc0211DatiInput().getOpzioni(idx0).setIspc0211OpzEsercitata("");
        }
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setSessionId("");
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setFlgRecProv(Types.SPACE_CHAR);
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setIspc0211FunzionalitaFormatted("00000");
        ws.getAreaIoIsps0211().getIspc0211DatiInput().setCodIniziativa("");
    }

    public void initIspc0211AreaErrori() {
        ws.getAreaIoIsps0211().setIspc0211EsitoFormatted("00");
        ws.getAreaIoIsps0211().setIspc0211ErrNumEleFormatted("000");
        for (int idx0 = 1; idx0 <= AreaIoIsps0211.ISPC0211_TAB_ERRORI_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0211().getIspc0211TabErrori(idx0).setCodErr("");
            ws.getAreaIoIsps0211().getIspc0211TabErrori(idx0).setDescrizioneErr("");
            ws.getAreaIoIsps0211().getIspc0211TabErrori(idx0).setIspc0211GravitaErrFormatted("00");
            ws.getAreaIoIsps0211().getIspc0211TabErrori(idx0).setIspc0211LivelloDerogaFormatted("00");
        }
    }

    public void initStrPerformanceDbg() {
        ws.getIdsv8888().getStrPerformanceDbg().setFase("");
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setTimestampFld("");
        ws.getIdsv8888().getStrPerformanceDbg().setStato("");
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getIdsv8888().getStrPerformanceDbg().setUserName("");
    }
}
